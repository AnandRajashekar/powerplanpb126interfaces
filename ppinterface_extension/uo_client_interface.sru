HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'uo_client_interface.exe'

nvo_wo_control i_nvo_wo_control
nvo_cpr_control i_nvo_cpr_control

// ----------------------- CR ALLOCATIONS -----------------------
longlong i_allocation_id
longlong i_month_number
longlong i_month_period
longlong i_clearing_indicator, i_apply_wo_types
longlong i_cr_company_id
string       i_source_sqls, i_balance_sqls, i_grouping_sqls
boolean      i_run_as_a_test
boolean      i_whole_month
string       i_interco_trigger_col
string       i_original_interco_credit_sql
longlong i_ytd_indicator
string		 i_department_field, i_work_order_field, i_base_limit_detail, i_source_amount_types, &
				 i_balance_amount_types, i_source_table_name, i_balance_table_name, i_gl_journal_category, &
				 i_alloc_stg_table, i_alloc_dtl_table, i_disable_fast
longlong i_elim_flag, i_base_limit, i_derivation_flag, i_result_amount_type, i_allocate_quantity, &
				 i_num_elements, i_target_id, i_exclude_negatives, i_session_id, i_absolute_value_calcs

uo_cr_derivation i_uo_cr_derivation
uo_ds_top i_cr_deriver_type_sources, i_ds_count, i_cr_deriver_type_sources_alloc, &
			 i_cr_deriver_override2, i_cr_deriver_override2_alloc, i_ds_elements

string 	i_use_new_structures_table, i_concurrent_processing, i_suspense_accounting, &
	i_test_or_actual, i_balance_where_clause, i_company_field, i_me_validations, &
	i_run_as_commitments, i_combo_validations, i_combo_or_control, i_proj_based_validations, i_mn_validations, i_custom_validations, &
	i_run_by_dept_credits_method, i_budget_version, i_priority, i_priority2, i_allow_priority_range, &
	i_default_gljc_alloc, i_default_gljc_interco
uo_cr_validation	i_uo_cr_validation
boolean i_suspense_processed, i_rate_field_exists
longlong i_run_as_commitments_flag
uo_ds_top i_ds_sum_amount

uo_ds_top 	i_ds_where_clause, &
	i_ds_source, i_ds_credit, i_ds_credit_insert, i_ds_target, i_ds_interco, i_ds_interco_insert, &
	i_ds_group_by, i_ds_alloc_list, i_ds_company, i_ds_where_clause_sources, i_ds_cr_sources, &
	i_ds_target_trpo, i_ds_credit_trpo, i_ds_distinct_x_cc_values, i_ds_rates_input, i_ds_rates, i_ds_source_totals, &
	i_ds_incr_alloc

uo_cr_allocations_cleanup	i_uo_cleanup
uo_cr_cost_repository i_uo_cr_cost_repository

longlong i_start_month, i_end_month, i_elim_start_month, i_elim_end_month

longlong i_ds_alloc_list_row

string i_balancing_cv
string	i_description
longlong	i_balance_where_clause_id, i_where_clause_id, i_group_by_id, i_credit_id, i_dollar_quantity_indicator, &
	i_clearing_factor_start_month, i_clearing_factor_end_month, i_clearing_id, i_intercompany_accounting, i_intercompany_id, &
	i_dw_id, i_clearing_balance_start_month, i_clearing_Balance_end_month, i_run_by_department, i_auto_reverse, i_rate_id
boolean i_run_for_all_companies, i_have_results
string i_inter_co_sqls, i_external_company_id

longlong i_init_mo_fy

boolean i_fast_allocation

longlong i_incremental_allocation, i_curr_incr_run_id, i_curr_month_run_id
string i_source_value_field, i_incr_month_source_sql
uo_ds_top i_ds_where_clause_incr
boolean i_incr_run_field_exists
// ----------------------- CR ALLOCATIONS -----------------------

// ----------------------- BUDGET ALLOCATIONS -----------------------
longlong i_clearing_mn[]
longlong i_clearing_mp[]
string            i_clearing_periods_wc
string            i_wo_sqls

boolean      i_run_all

boolean      i_msgbox

string		 i_staging_table, i_insert_table, &
				 i_prefixed_where
longlong i_interco_acctg, i_auto_reverse_cross_years, i_ignore_header_co

uo_ds_top i_ds_oob
uo_ds_top 	i_ds_cr_alloc_months,i_ds_balance,i_ds_1,i_ds_2,i_ds_credit_co, i_ds_target_val

uo_cr_allocations_cleanup_bdg	i_uo_cleanup_bdg

string i_closings_where_clause, i_afudc_calc_on_negative_base,  &
	 i_wo_field_for_uf_cap, i_ifb_id
longlong i_budget_afudc_transfers, i_where_source_id, i_actuals_month, i_start_year, i_max_late_chg_wait_period
boolean  i_ifb_field_exists, i_validation_kickouts
uo_ds_top i_ds_sum_amount2

uo_cr_validation_bdg i_uo_cr_validation_bdg

boolean i_errors = false

longlong i_fy_start_month
// ----------------------- BUDGET ALLOCATIONS -----------------------

// ----------------------- CR BALANCES -----------------------
string	i_amount_types
// ----------------------- CR BALANCES -----------------------

// ----------------------- CR BALANCES BDG -----------------------
string	i_co_field
string	i_actual_field[], i_budget_field[], i_element_table_arr[], i_element_column_arr[]
longlong i_values_by_company[]
string i_element_string, i_join_on_ack

uo_cr_where_from_ds i_uo_cr_where_from_ds
// ----------------------- CR BALANCES BDG -----------------------

// ----------------------- CR BATCH DERIVATION -----------------------
longlong 			i_bd_queue_count, i_source_id, i_max_id_processed, i_sort_order
string 		i_type, i_co, i_detail_table, i_stg_table, i_wo_field, i_proj_field, &
				i_mn_string, i_ce_field, i_ce_me_table, i_ce_me_table_field, i_posting_approval
uo_ds_top	i_ds_bd_queue, i_ds_counter, i_ds_ifb_id, i_ds_max_id, i_ds_interface_dates, i_ds_distinct_source_id, &
				i_ds_null_rollup
string			i_basis_mn, i_posting_mn
longlong	i_posted_source_ids[]
string			i_type_array[], i_co_array[]

longlong	i_ce_me_by_company
longlong	i_derivation_type_on_stg
uo_cr_interco_balancing i_uo_cr_interco_balancing
uo_ds_top i_ds_months
// ----------------------- CR BATCH DERIVATION -----------------------

// ----------------------- CR BATCH DERIVATION BDG -----------------------
string i_bv, i_dt, i_co_arg
longlong i_start_mn, i_end_mn
longlong i_basis_mn2, i_posting_mn2

longlong  i_i_records
uo_ds_top	i_ds_all_tab_columns_bdg
// ----------------------- CR BATCH DERIVATION BDG -----------------------

// ----------------------- CR BUDGET ENTRY CALC ALL -----------------------
longlong i_bv_id, i_year, i_component_id, i_cr_budget_factor_id, i_cr_budget_fixed_factor_id, &
		  i_bv_start_year, i_bv_budget_year, i_bv_end_year, i_template_id, &
		  i_step2_row_index, i_step2_column_index, i_open_for_entry, i_rclick_row, &
		  i_skip_value, i_formatting_start, i_end_year, i_version_years[], i_spread_years[], &
		  i_structure_id, i_handles[], i_clicked_item_handle
string  i_dept_element, i_dept_value, i_element_for_rows, i_element_table_for_rows,i_sle_dept, &
		  i_element_for_cols, i_bdg_element_for_cols, i_element_table_for_cols, i_dd_key,i_element_value,  &
		  i_step2_row, i_step2_column,  i_company_value, i_step2_alloc_field, &
		  i_alternate_entry, i_col_array[], i_sort_string, i_alt_entry_direct_table, &
		  i_text_band_computed[], i_col_array_for_summary_band[], i_alias_array[], i_dept_element_bdg, i_dept_element_act, &
		  i_values[], i_label, i_rollup_value_id_list, i_click_or_hover, i_enforce_active_entry, i_default_company, i_col_name
boolean i_from_buttons, i_step1_btns_enabled, i_updated = true, i_step2_validate, &
		  i_cb_compute_ran, i_finish_button, i_labor_all_years = true, i_calc_all_in_progress, i_dw_entry_created, i_called_from_finish
decimal {2} i_step2_control_total, i_alloc_amount, i_alloc_row, i_amount_to_spread
string      i_menu_items[], i_menu_events[], i_rclick_column, i_rclick_rowval, i_amt_qty_both_filter, i_year_filter, i_spread_all_or_one
string 	i_element_group, i_element_column, i_element_table, i_spread_amt_qty_both, i_selection_screen_sqls, i_selection_screen_sqls_where, &
			i_derivations_or_validations, i_selection_screen_dept_sqls, i_selection_screen_labor_sqls, i_selection_screen_dept_where, &
			i_element_datawindow, i_selection_screen_dept_suppla_where, i_selection_screen_dept_supplb_where, i_selection_screen_dept_where_no_yr, &
			i_selection_screen_sqls_no_yr
longlong	i_element_id, i_labor_year,i_search_row,i_row, i_view_only, i_calc_all_rtn, i_budget_value_type_id, i_template_year_restriction, i_y1, i_y2, i_appr_counter, i_addl_fields_clicked_row
string	i_coltype, i_colname, i_search_table, i_search_field, i_return_value, i_element_array_descr[], i_element_table_array_descr[], i_ack_filter, i_addl_fields_clicked, &
			i_addl_sql_price_qty, i_gross_up_filter
boolean	i_do_column, i_called_from_calc_all, i_called_from_calc_validations, i_called_from_calc_derivations, i_control_total_filter, &
			i_retrieve_entire_group, i_previous, i_view_options, i_send_for_approval, i_called_from_send_for_appr, i_using_approvals, i_approved,&
			i_item_changed_rtn_val, i_gross_up_filter_applied, i_zero_out_row
			
boolean	i_obey_disable_month = false, i_have_disabled_month
			
uo_ds_top  i_ds_where
uo_ds_top i_cr_deriver_type_sources_budget
uo_ds_top i_cr_element_descr_lookup
uo_ds_top i_ds_labels
// ----------------------- CR BUDGET ENTRY CALC ALL -----------------------

// ----------------------- CR DELETE BUDGET ALLOCATIONS -----------------------
string i_company
// ----------------------- CR DELETE BUDGET ALLOCATIONS -----------------------

// ----------------------- CR FLATTEN STRUCTURES -----------------------
uo_ds_top 				i_ds_structure
uo_ds_top 				i_ds_points
uo_ds_top				i_ds_levels
uo_ds_top				i_ds_descr
// ----------------------- CR FLATTEN STRUCTURES -----------------------

// ----------------------- CR POSTING -----------------------
string i_output_location
string i_stg_table_name, i_detail_posting
longlong 	 i_obey_gl_rollup, i_output_method, i_posting_id, i_array_counter
uo_ds_top i_ds_posting_audit, i_ds_sources
longlong	i_posting_id_array[]
string					i_where_clause_array[]
string i_obey_approval

string i_use_standard_sap, i_sap_pop_tables[]
uo_ds_top i_ds_sap_posting_cols
longlong i_c = 1
// ----------------------- CR POSTING -----------------------

// ----------------------- CR SAP TRUEUP -----------------------
longlong	 i_min_open_month, i_num_types, i_num_virt, &
			 i_elig_accts_count
string	  i_derivation_type, i_derived_ack_fields, &
			  i_join_fields, i_join_fields_array[], i_fp_field, &
			 i_rollup_element, i_rollup_element_table, i_rollup_element_column, &
			 i_account_field,  i_ignore_rule, &
			 i_pp_external_company, i_rate_field
datetime	 i_datetime_last_processed, i_upper_datetime_limit
decimal {4} i_pos_neg_ratio
uo_ds_top i_ds_all_tab_cols, i_ds_cr_txn_types, &
			 i_ds_balancing, i_ds_virt, i_ds_open_month
uo_cr_derivation i_uo_deriver
boolean i_warnings

longlong	i_rollup_element_by_company
// ----------------------- CR SAP TRUEUP -----------------------

// ----------------------- CR FINANCIAL REPORTS BUILD -----------------------
longlong i_tt_dollar_type_sort_order[], i_structure_id_or[], i_report_id, &
		 i_dollar_columns
string i_temp_tables[], i_temp_tables_or[], i_tt_dollar_types[], &
       i_temp_table, i_col_col, i_col_val_for_insert
		 
string i_calling_user_id, i_default_bv
longlong i_num_reports_to_build, i_num_errors, i_calling_session_id

uo_ds_top i_ds_generic_string_audit, i_ds_formula_audit1, i_ds_formula_audit2, &
			 i_ds_formula_audit3, i_ds_dollar_types, i_ds_str_override, &
			 i_ds_formulas, i_ds_formulas2
// ----------------------- CR FINANCIAL REPORTS BUILD -----------------------

// ----------------------- CR TO COMMITMENTS -----------------------
longlong 		i_start_record, i_max_month, i_open_month, i_num_comps, i_min_month
longlong	i_mn_to_process, i_min_month_number, i_max_month_number

longlong i_min_commitment_id_loaded, i_min_commitment_id

uo_ds_top i_ds_detail_tables
longlong	i_num_detail_tables

uo_ds_top i_ds_cc_errors, i_ds_trans_clause, i_ds_trans_tables, i_ds_cwip_mapping, i_ds_audit
decimal {2} i_amount_to_process
boolean i_kickouts
longlong i_records_to_process
// ----------------------- CR TO COMMITMENTS -----------------------

// ----------------------- CWIP CHARGE -----------------------
string i_company_arr[]
longlong	i_min_charge_id_loaded, i_min_charge_id
// ----------------------- CWIP CHARGE -----------------------

// ----------------------- POPULATE MATLREC -----------------------
longlong i_wo_id
// ----------------------- POPULATE MATLREC -----------------------

// ----------------------- PP INTEGRATION -----------------------
longlong i_batch_cnt, i_args_cnt

longlong i_ppint_process_id, i_prior_process_id, i_prior_occurrence_id, i_prior_msg_order
string i_process_descr, i_exe_version
// ----------------------- PP INTEGRATION -----------------------

// ----------------------- PP TO CR -----------------------
string i_table_name, i_staging_table_name
uo_cr_validation i_uo_cr_validations
// ----------------------- PP TO CR -----------------------

// ----------------------- SSP PREVIEW JE -----------------------
longlong i_company_idx[]
datetime i_month

boolean i_run_accruals, i_run_afudc, i_run_aro, i_run_lease, i_run_depr

uo_ds_top i_ds_depr_activity

string i_curr_comp_descr
// ----------------------- SSP PREVIEW JE -----------------------


end variables
forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;


return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

