HA$PBExportHeader$nvo_ls_mec.sru
forward
global type nvo_ls_mec from nonvisualobject
end type
end forward

global type nvo_ls_mec from nonvisualobject
end type
global nvo_ls_mec nvo_ls_mec

forward prototypes
public function longlong of_importinvoices (date a_gl_posting_mo_yr, longlong a_company_id, ref string a_msg)
public function longlong of_payinvoicerecon (date a_gl_posting_mo_yr, longlong a_company_id, ref string a_msg)
public function longlong of_sendpayapproval (date a_gl_posting_mo_yr, longlong a_company_id, ref string a_msg)
public function longlong of_closelease (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function longlong of_calcdepr (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function longlong of_approvedepr (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function longlong of_calcaccruals (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function longlong of_approveaccruals (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function longlong of_calcpayments (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function longlong of_approvepayments (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function string of_opennext (date a_gl_posting_mo_yr, longlong a_company_id)
public function longlong of_opennext (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg)
public function string of_approvedepr (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log)
private function string of_closelease (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log)
public function string of_calcaccruals (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log)
public function string of_approveaccruals (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log)
public function string of_calcpayments (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log)
public function string of_approvepayments (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log)
public function string of_adjust_taxes (longlong a_invoice_id, decimal a_adjust_amount, longlong a_tax_local_id, longlong a_line_type, longlong a_ls_asset_id)
public subroutine of_cpm ()
end prototypes

public function longlong of_importinvoices (date a_gl_posting_mo_yr, longlong a_company_id, ref string a_msg);
//Specific functionality here



UPDATE LS_PROCESS_CONTROL SET import_invoices = sysdate
WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
AND company_id = :a_company_id;
IF sqlca.sqlcode <> 0 THEN
	f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
	rollback;
	RETURN -1
END IF

COMMIT;
RETURN 1
end function

public function longlong of_payinvoicerecon (date a_gl_posting_mo_yr, longlong a_company_id, ref string a_msg);
//Specific functionality here



//UPDATE LS_PROCESS_CONTROL SET payment_invoice_recon = sysdate
//WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
//AND company_id = :a_company_id;
//IF sqlca.sqlcode <> 0 THEN
//	f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
//	rollback;
//	RETURN -1
//END IF
//
COMMIT;
RETURN 1
end function

public function longlong of_sendpayapproval (date a_gl_posting_mo_yr, longlong a_company_id, ref string a_msg);
//Specific functionality here



UPDATE LS_PROCESS_CONTROL SET payment_sent_approval = sysdate
WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
AND company_id = :a_company_id;
IF sqlca.sqlcode <> 0 THEN
	f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
	rollback;
	RETURN -1
END IF

COMMIT;
RETURN 1
end function

public function longlong of_closelease (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);string str_ret
longlong i, num, to_ret, validation_check
integer end_log

to_ret = 1
a_msg = "OK"
num = upperBound( a_company_id )
for i = 1 to num
	
	//WMD We can't run this process twice for one company
	select count(1)
	into :validation_check 
	from ls_process_control
	where lam_closed is null
		and gl_posting_mo_yr = :a_gl_posting_mo_yr
		and company_id = :a_company_id[i];
	
	if validation_check <> 1 then 
		f_ppbase_infobox("Process Already Complete", "Lock Month already complete for Company: " + string(a_company_id[i]))
		continue 
	end if 
	
	//WMD 
	if i = num then end_log = 1
	
	str_ret = of_closeLease( a_gl_posting_mo_yr, a_company_id[ i ], end_log )
	if str_ret <> "OK" then
		a_msg = str_ret
		//WMD we need to exit right when we fail, not continue looping
		return -1
		exit
	end if
next

if to_ret = 1 then
	commit;
else
	rollback;
end if

RETURN to_ret
end function

public function longlong of_calcdepr (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);//Variables passed in should contain company ID and month in yyyymm format

s_ppbase_parm_arrays s_parms
s_ppbase_parm_arrays_labels s_labels
nvo_server_side_request nvo_ss
longlong group_id
//WMD
longlong company_list[], validation_count, i, j=1 

nvo_ss = CREATE nvo_server_side_request

//WMD we need to make sure all the companies here have not had depreciation already approved
for i = 1 to upperbound(a_company_id)
select count(1) 
into :validation_count 
from ls_process_control
where company_id = :a_company_id[i]
	and gl_posting_mo_yr=:a_gl_posting_mo_yr
	and depr_approved is null;

if validation_count <> 1 then 
	f_ppbase_infobox("Depreciation Already Approved!", "Depreciation already approved for Company ID: " + string(a_company_id[i]) + ". Cannot Re-Calculate Depreciation for this company!")
	continue
else 
	//Have to populate a new array in case some companies drop off
	company_list[j] = a_company_id[i]
	j=j+1
end if 

next

s_parms.long_arg = company_list
s_parms.long_arg2[1]= -100
s_parms.date_arg [1]= a_gl_posting_mo_yr
s_parms.string_arg [1]= "INDIVIDUAL"
s_labels.long_label = "Companies"
s_labels.long_label2 = "Subledger Type"
s_labels.date_label= "Month"
s_labels.string_label = "Depreciation Type"

select DEPR_CALC_PROCESS_CONTROL_SEQ.nextval
into :group_id
from dual;

s_parms.long_arg3[1] = group_id
s_labels.long_label3 = 'Process Control Group'

insert into DEPR_CALC_PROCESS_CONTROL (GROUP_ID, TYPE, IS_PROCESSED)
values (:group_id, 'INDIVIDUAL: -100', 0);

commit;

nvo_ss.uf_request_job ("ssp_depr_calc.exe", "Month End Depreciation - Lease", s_parms, s_labels, false, true)
//End of call


COMMIT;
RETURN 1
end function

public function longlong of_approvedepr (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);string str_ret
longlong i, num, to_ret, validation_check
integer end_log
//WMD adding end log 

to_ret = 1
a_msg = "OK"
num = upperBound( a_company_id )
for i = 1 to num
	
	//WMD We can't run this process twice for one company
	select count(1)
	into :validation_check 
	from ls_process_control
	where depr_approved is null
		and gl_posting_mo_yr = :a_gl_posting_mo_yr
		and company_id = :a_company_id[i];
	
	if validation_check <> 1 then 
		f_ppbase_infobox("Process Already Complete", "Depr Approval already complete for Company: " + string(a_company_id[i]))
		continue 
	end if 
	
	//WMD
	if i=num then end_log = 1
	
	str_ret = of_approveDepr( a_gl_posting_mo_yr, a_company_id[ i ], end_log )
	
	if str_ret <> "OK" then
		a_msg = str_ret
		//WMD we need to exit right when we fail, not continue looping
		return -1
	end if
next

RETURN to_ret
end function

public function longlong of_calcaccruals (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);string str_ret
longlong i, num, to_ret, validation_check
integer end_log

to_ret = 1
a_msg = "OK"
num = upperBound( a_company_id )
for i = 1 to num
	
	//WMD We can't run this process if we've already sent the JE's 
	select count(1)
	into :validation_check 
	from ls_process_control
	where ACCR_APPROVED is null
		and gl_posting_mo_yr = :a_gl_posting_mo_yr
		and company_id = :a_company_id[i];
	
	if validation_check <> 1 then 
		f_ppbase_infobox("Accrual JE's Already Sent", "Accrual JE's have already been sent for Company: " + string(a_company_id[i]) + ". Cannot Re-Calculate!")
		continue 
	end if 
	
	
	//WMD 
	if i=num then end_log=1
	str_ret = of_calcaccruals( a_gl_posting_mo_yr, a_company_id[ i ], end_log )
	if str_ret <> "OK" then
		a_msg = str_ret
		//WMD We need to leave right away
		return -1
	end if
next

RETURN to_ret
end function

public function longlong of_approveaccruals (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);string str_ret
longlong i, num, to_ret, validation_check
integer end_log

to_ret = 1
a_msg = "OK"
num = upperBound( a_company_id )
for i = 1 to num
	
	//WMD We can't run this process if we've already sent the JE's before
	select count(1)
	into :validation_check 
	from ls_process_control
	where ACCR_APPROVED is null
		and gl_posting_mo_yr = :a_gl_posting_mo_yr
		and company_id = :a_company_id[i];
	
	if validation_check <> 1 then 
		f_ppbase_infobox("Accrual JE's Already Sent", "Accrual JE's have already been sent for Company: " + string(a_company_id[i]) + ". Cannot Re-Send!")
		continue 
	end if 
	
	
	//WMD 
	if i = num then end_log = 1
	str_ret = of_approveAccruals( a_gl_posting_mo_yr, a_company_id[ i ], end_log )
	if str_ret <> "OK" then
		a_msg = str_ret
		to_ret = -1
	end if
next

RETURN to_ret
end function

public function longlong of_calcpayments (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);string str_ret
longlong i, num, to_ret, validation_check
integer end_log

to_ret = 1
a_msg = "OK"
num = upperBound( a_company_id )
for i = 1 to num
	
	//WMD We can't process payments if we've already approved them 
	select count(1)
	into :validation_check 
	from ls_process_control
	where PAYMENT_APPROVED is null
		and gl_posting_mo_yr = :a_gl_posting_mo_yr
		and company_id = :a_company_id[i];
	
	if validation_check <> 1 then 
		f_ppbase_infobox("Process Already Complete", "Payment Approval already complete for Company: " + string(a_company_id[i]) + ". Skipping.")
		continue 
	end if 
	
	//WMD
	if i=num then end_log = 1
	str_ret = of_calcPayments( a_gl_posting_mo_yr, a_company_id[ i ], end_log )
	if str_ret <> "OK" then
		a_msg = str_ret
		to_ret = -1
	end if
next

RETURN to_ret
end function

public function longlong of_approvepayments (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);string str_ret
integer end_log
longlong i, num, to_ret, validation_check

to_ret = 1
a_msg = "OK"
num = upperBound( a_company_id )
for i = 1 to num
	
	
	//WMD We can't run this process twice for one company
	select count(1)
	into :validation_check 
	from ls_process_control
	where PAYMENT_APPROVED is null
		and gl_posting_mo_yr = :a_gl_posting_mo_yr
		and company_id = :a_company_id[i];
	
	if validation_check <> 1 then 
		f_ppbase_infobox("Process Already Complete", "Payment Approval already complete for Company: " + string(a_company_id[i]))
		continue 
	end if 
	
	if i=num then end_log = 1
	
	str_ret = of_approvepayments( a_gl_posting_mo_yr, a_company_id[ i ], end_log )
	if str_ret <> "OK" then
		a_msg = str_ret
		//WMD We need to exit if we get a failure
		return -1
	end if
next

RETURN to_ret
end function

public function string of_opennext (date a_gl_posting_mo_yr, longlong a_company_id);longlong counter
string ret
integer rtn
//Specific functionality here

//Check that there are no outstanding pending transactions
select count(*)
into :counter
from PEND_TRANSACTION
where SUBLEDGER_INDICATOR = -100
and GL_POSTING_MO_YR = :a_gl_posting_mo_yr
and COMPANY_ID = :a_company_id;
if counter > 0 then
	ret = sqlca.sqlErrText
	f_ppbase_infobox('Error','There are outstanding lease pending transactions. All lease transactions must be posted before closing the lease module.')
	rollback;
	RETURN ret
end if

UPDATE LS_PROCESS_CONTROL SET open_next = sysdate
WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
AND company_id = :a_company_id;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
	rollback;
	RETURN ret
END IF

SELECT count(*)
INTO :counter
FROM LS_PROCESS_CONTROL
WHERE company_id = :a_company_id
AND gl_posting_mo_Yr = add_months(:a_gl_posting_mo_yr,1);
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
	rollback;
	RETURN ret
END IF

IF counter = 0 THEN
	INSERT INTO LS_PROCESS_CONTROL (Company_id, gl_posting_mo_yr)
	(SELECT :a_company_id, add_months(:a_gl_posting_mo_yr,1) from dual);
	IF sqlca.sqlcode <> 0 THEN
		ret = sqlca.sqlErrText
		f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
		rollback;
		RETURN ret
	END IF
END IF

rtn = f_calc_cpr_depr_end_bals(a_company_id, datetime(a_gl_posting_mo_yr))
if rtn <> 1 then 
	ret = "Error calculating CPR depreciation end balances"
	rollback;
	return ret
end if 

rtn = f_calc_cpr_depr_roll_forward(a_company_id, datetime(a_gl_posting_mo_yr))
if rtn <> 1 then 
	ret = "Error calculating CPR depreciation rollforward"
	rollback;
	return ret
end if 

COMMIT;
RETURN "OK"
end function

public function longlong of_opennext (date a_gl_posting_mo_yr, longlong a_company_id[], ref string a_msg);string str_ret
longlong i, num, to_ret

to_ret = 1
a_msg = "OK"
num = upperBound( a_company_id )
for i = 1 to num
	str_ret = of_opennext( a_gl_posting_mo_yr, a_company_id[ i ] )
	if str_ret <> "OK" then
		a_msg = str_ret
		to_ret = -1
	end if
next

RETURN to_ret
end function

public function string of_approvedepr (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log);string str_ret, msg
//WMD added end log 
DECLARE F_DEPR_APPROVE PROCEDURE FOR
	PKG_LEASE_CALC.F_DEPR_APPROVE (:a_company_id, :a_gl_posting_mo_yr, :a_end_log);
	
EXECUTE F_DEPR_APPROVE;
IF sqlca.sqlcode <> 0 THEN
	msg = sqlca.sqlErrText
	messagebox("ERROR", "Error creating ORACLE function F_DEPR_APPROVE: " + sqlca.sqlErrText)
	rollback;
	RETURN msg
END IF

FETCH F_DEPR_APPROVE INTO :str_ret;
IF sqlca.sqlcode <> 0 THEN
	msg = sqlca.sqlErrText
	messagebox("ERROR", "Error executing ORACLE function F_DEPR_APPROVE: " + sqlca.sqlErrText)
	rollback;
	RETURN msg
END IF

CLOSE F_DEPR_APPROVE;

IF str_ret = 'OK' THEN
	UPDATE LS_PROCESS_CONTROL SET depr_approved = sysdate
	WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
	AND company_id = :a_company_id;
	IF sqlca.sqlcode <> 0 THEN
		f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
		rollback;
		RETURN "Error"
	END IF
	
	COMMIT;
	RETURN "OK"
ELSE
	msg = str_ret
	rollback;
	RETURN msg
END IF
end function

private function string of_closelease (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log);string str_ret, sqls, msg
longlong counter
//WMD added a_end_log

msg = "OK"

//Check that there are no outstanding pending transactions
select count(*)
into :counter
from PEND_TRANSACTION
where SUBLEDGER_INDICATOR = -100
and GL_POSTING_MO_YR = :a_gl_posting_mo_yr
and COMPANY_ID = :a_company_id
and Trim(ACTIVITY_CODE) not in ('URET','URGL'); //CBS maint-40764 - allow retirements to be posted after the month is locked. 

if counter = 0 then
	DECLARE F_LAM_CLOSED PROCEDURE FOR
	PKG_LEASE_CALC.F_LAM_CLOSED (:a_company_id, :a_gl_posting_mo_yr, :a_end_log);
	
	EXECUTE F_LAM_CLOSED;
	IF sqlca.sqlcode <> 0 THEN
		msg = "Error creating ORACLE function F_LAM_CLOSED"
		RETURN msg
	END IF
	
	FETCH F_LAM_CLOSED INTO :str_ret;
	IF sqlca.sqlcode <> 0 THEN
		msg = "Error executing ORACLE function F_LAM_CLOSED"
		RETURN msg
	END IF
	
	CLOSE F_LAM_CLOSED;
	
	
	if str_ret = 'OK' then
		UPDATE LS_PROCESS_CONTROL SET LAM_CLOSED = sysdate
		WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
		AND company_id = :a_company_id;
		IF sqlca.sqlcode <> 0 THEN
			msg = "Error updating LS_PROCESS_CONTROL"
			RETURN msg
		END IF
		
		sqls = "MERGE into LS_PROCESS_CONTROL P "+&
					"using (Select add_months(to_date('"+string(a_gl_posting_mo_yr)+"', 'mm/dd/yyyy'),1) as month, "+string(a_company_id)+" as company_id from dual) S "+&
					"on (P.GL_POSTING_MO_YR = S.MONTH and P.COMPANY_ID = S.COMPANY_ID) "+&
					"when matched then update set P.LAM_CLOSED = null "+&
					"when not matched then insert (P.COMPANY_ID, P.GL_POSTING_MO_YR) "+&
					"values (S.COMPANY_ID, S.MONTH) "
					
		if not isNull(sqls) then 
			execute immediate :sqls;
			IF sqlca.sqlcode <> 0 THEN
				msg = "Error loading next month"
			END IF
		end if
	else
		msg = str_ret
	end if
else	
	msg = 'There are outstanding lease transfers in pending transactions. All lease transfer transactions must be posted before locking the lease module.'
end if

return msg
end function

public function string of_calcaccruals (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log);string str_ret, ret
datetime tax_date
//WMD Added a_end_log

DECLARE F_ACCRUALS_CALC PROCEDURE FOR
	PKG_LEASE_CALC.F_ACCRUALS_CALC (:a_company_id, :a_gl_posting_mo_yr, :a_end_log);
	
EXECUTE F_ACCRUALS_CALC;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	messagebox("ERROR", "Error creating ORACLE function F_ACCRUALS_CALC: " + sqlca.sqlErrText)
	rollback;
	RETURN ret
END IF

FETCH F_ACCRUALS_CALC INTO :str_ret;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	messagebox("ERROR", "Error executing ORACLE function F_ACCRUALS_CALC: " + sqlca.sqlErrText)
	rollback;
	RETURN ret
END IF

CLOSE F_ACCRUALS_CALC;

IF str_ret = 'OK' THEN
	UPDATE LS_PROCESS_CONTROL SET accr_calc = sysdate
	WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
	AND company_id = :a_company_id;
	
	COMMIT;
	RETURN "OK"
ELSE
	ret = str_ret
	rollback;
	RETURN ret
END IF




end function

public function string of_approveaccruals (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log);string str_ret, ret

DECLARE F_ACCRUALS_APPROVE PROCEDURE FOR
	PKG_LEASE_CALC.F_ACCRUALS_APPROVE (:a_company_id, :a_gl_posting_mo_yr, :a_end_log);
	
EXECUTE F_ACCRUALS_APPROVE;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrtext
	messagebox("ERROR", "Error creating ORACLE function F_ACCRUALS_APPROVE: " + ret)
	rollback;
	RETURN ret
END IF

FETCH F_ACCRUALS_APPROVE INTO :str_ret;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrtext
	messagebox("ERROR", "Error executing ORACLE function F_ACCRUALS_APPROVE: " + ret)
	rollback;
	RETURN ret
END IF

CLOSE F_ACCRUALS_APPROVE;

IF str_ret = 'OK' THEN
	UPDATE LS_PROCESS_CONTROL SET accr_approved = sysdate
	WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
	AND company_id = :a_company_id;

	COMMIT;
	RETURN "OK"
ELSE
	ret = str_ret
	rollback;
	RETURN ret
END IF
end function

public function string of_calcpayments (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log);string str_ret, ret

DECLARE F_PAYMENT_CALC PROCEDURE FOR
	PKG_LEASE_CALC.F_PAYMENT_CALC (:a_company_id, :a_gl_posting_mo_yr, :a_end_log);
	
EXECUTE F_PAYMENT_CALC;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	messagebox("ERROR", "Error creating ORACLE function F_PAYMENT_CALC: " + ret)
	rollback;
	RETURN ret
END IF

FETCH F_PAYMENT_CALC INTO :str_ret;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	messagebox("ERROR", "Error executing ORACLE function F_PAYMENT_CALC: " + ret)
	rollback;
	RETURN ret
END IF

CLOSE F_PAYMENT_CALC;

IF str_ret = 'OK' THEN
	UPDATE LS_PROCESS_CONTROL SET payment_calc = sysdate
	WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
	AND company_id = :a_company_id;
	IF sqlca.sqlcode <> 0 THEN
		ret = sqlca.sqlErrText
		f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
		rollback;
		RETURN ret
	END IF
	
	COMMIT;
	RETURN "OK"
ELSE
	ret = str_ret
	rollback;
	RETURN ret
END IF
end function

public function string of_approvepayments (date a_gl_posting_mo_yr, longlong a_company_id, integer a_end_log);string str_ret, ret

DECLARE F_PAYMENT_APPROVE PROCEDURE FOR
	PKG_LEASE_CALC.F_PAYMENT_APPROVE (:a_company_id, :a_gl_posting_mo_yr, :a_end_log);
	
EXECUTE F_PAYMENT_APPROVE;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	messagebox("ERROR", "Error creating ORACLE function F_PAYMENT_APPROVE: " + sqlca.sqlErrText)
	rollback;
	RETURN ret
END IF

FETCH F_PAYMENT_APPROVE INTO :str_ret;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrText
	messagebox("ERROR", "Error executing ORACLE function F_PAYMENT_APPROVE: " + sqlca.sqlErrText)
	rollback;
	RETURN ret
END IF

CLOSE F_PAYMENT_APPROVE;

IF str_ret = 'OK' THEN
	UPDATE LS_PROCESS_CONTROL SET payment_approved = sysdate
	WHERE gl_posting_mo_yr = :a_gl_posting_mo_yr
	AND company_id = :a_company_id;
	IF sqlca.sqlcode <> 0 THEN
		ret = sqlca.sqlErrText
		f_ppbase_infobox('Error','Error updating LS_PROCESS_CONTROL')
		rollback;
		RETURN ret
	END IF
	
	COMMIT;
	RETURN "OK"
ELSE
	ret = str_ret
	rollback;
	RETURN ret
END IF
end function

public function string of_adjust_taxes (longlong a_invoice_id, decimal a_adjust_amount, longlong a_tax_local_id, longlong a_line_type, longlong a_ls_asset_id);string str_ret, ret

DECLARE F_ADJUST_TAXES PROCEDURE FOR
	PKG_LEASE_CALC.F_ADJUST_TAXES (:a_invoice_id, :a_adjust_amount, :a_tax_local_id, :a_line_type, :a_ls_asset_id);
	
EXECUTE F_ADJUST_TAXES;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrtext
	messagebox("ERROR", "Error creating ORACLE function F_ADJUST_TAXES: " + ret)
	rollback;
	RETURN ret
END IF

FETCH F_ADJUST_TAXES INTO :str_ret;
IF sqlca.sqlcode <> 0 THEN
	ret = sqlca.sqlErrtext
	messagebox("ERROR", "Error executing ORACLE function F_ADJUST_TAXES: " + ret)
	rollback;
	RETURN ret
END IF

CLOSE F_ADJUST_TAXES;

IF str_ret <> "OK" then 
	ret = str_ret
	rollback;
	RETURN ret
END IF


return "OK"
end function

public subroutine of_cpm ();longlong v_process_id, v_occurrence_id
// Kill any sessions in the DB that are active for the given user

DECLARE P_END_LOG PROCEDURE FOR
	PKG_PP_LOG.P_END_LOG;

//Procedures return SQLcode 100...
EXECUTE P_END_LOG;
IF sqlca.sqlcode <> 100 THEN
	messagebox("ERROR", "Error ending active logs before starting CPM: " + sqlca.sqlErrText)
	return
END IF

CLOSE P_END_LOG;


//WMD
select process_id 
into :v_process_id
from pp_processes 
where upper(trim(description))='LESSEE CALCULATIONS';

//Try to grab the occurrence that is going to be selected in the pkg_pp_log of our PL/SQL process
select PP_PROCESSES_OCURRENCES_SEQ.NEXTVAL + 1 
into :v_occurrence_id 
from dual;

if v_occurrence_id <> 0 and not isnull(v_occurrence_id) and not isnull(v_process_id) and v_process_id <> 0 then 
	nvo_server_side_request nvo_ssp
	nvo_ssp = create nvo_server_side_request
	nvo_ssp.uf_start_cpm(v_process_id, v_occurrence_id)
end if 
end subroutine

on nvo_ls_mec.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_ls_mec.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

