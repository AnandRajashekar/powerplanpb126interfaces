HA$PBExportHeader$w_budget_dw_references.srw
forward
global type w_budget_dw_references from window
end type
type dw_cr_element_definitions from datawindow within w_budget_dw_references
end type
end forward

global type w_budget_dw_references from window
integer width = 4754
integer height = 2200
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_cr_element_definitions dw_cr_element_definitions
end type
global w_budget_dw_references w_budget_dw_references

on w_budget_dw_references.create
this.dw_cr_element_definitions=create dw_cr_element_definitions
this.Control[]={this.dw_cr_element_definitions}
end on

on w_budget_dw_references.destroy
destroy(this.dw_cr_element_definitions)
end on

type dw_cr_element_definitions from datawindow within w_budget_dw_references
integer x = 73
integer y = 60
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

