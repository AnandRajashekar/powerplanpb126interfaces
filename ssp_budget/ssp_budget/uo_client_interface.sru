HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_budget.exe', i_process_level, i_table_name, i_process_name, i_wo_sqls
longlong i_session_id, i_prior_process_id, i_prior_occurrence_id, i_prior_msg_order, i_bv_id, i_synchronous
boolean i_budget_version

// Variables for Load Budget Dollars
boolean i_load_update_with_act

// Variables for Escalations
boolean i_deescalate

// Variables for Budget to CR
longlong i_posting_id, i_insert_where_clause_id, i_delete_where_clause_id
boolean i_only_in_bv
string i_cr_table, i_cr_budget_version
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function longlong uf_read_afudc ()
public function longlong uf_read_uwa ()
public function longlong uf_read_esc ()
public function longlong uf_read_oh ()
public function longlong uf_read_load ()
public function longlong uf_read_wip ()
public function longlong uf_read_to_cr ()
public function longlong uf_setup_process (string a_process_name, string a_start_stop)
public function longlong uf_setup_wo_est_processing_temp ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();string conc_check, args, descr, sqls, str_array[]
longlong idx, wo_id, rev, p_id, o_id, cnt, comma_pos, from_pos, l_bv_id, delimiter, pid, rtn
datastore ds
ds = create datastore

w_budget_dw_references w_dw

//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id 
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;


pid = uo_winapi.uf_GetCurrentProcessId()
update pp_job_request set windows_process_id = :pid where process_id = :g_process_id and occurrence_id = :g_occurrence_id;
commit;

select synchronous into :i_synchronous from pp_job_request where process_id = :g_process_id and occurrence_id = :g_occurrence_id;
if isnull(i_synchronous) then i_synchronous = 0

i_prior_process_id = g_process_id
i_prior_occurrence_id = g_occurrence_id
i_prior_msg_order = g_msg_order


//
// Set variables:
//	i_process_level
//	i_budget_version
//	i_bv_id
//	i_wo_sqls
//
if g_ssp then
	i_process_level = g_ssp_parms.string_arg[1]
	
	//if g_ssp_labels.long_label = 'Budget Version' then
	if pos(g_ssp_labels.long_label,'Budget Version') > 0 then //AWW JIRA PC-1260
		i_budget_version = true
		i_bv_id = g_ssp_parms.long_arg[1]
		i_wo_sqls = ''
	else
		i_budget_version = false
		i_bv_id = 0
		i_wo_sqls = lower(trim(g_ssp_parms.string_arg[2]))  //"select wo_id, rev from dual union all ..."
	end if
else
	args = trim(g_command_line_args)
	delimiter = pos(args,' ')
	i_process_level = upper(trim(left(args, delimiter)))
	i_wo_sqls = trim(mid(args, delimiter))
	
	if i_process_level = '' or i_wo_sqls = '' then
		f_pp_msgs('Error: Bad command line arguments. You must specify (1) a table name and (2) either budget version id or list of work orders.')
		return rtn_failure
	end if
	
	if lower(left(i_wo_sqls, 6)) = 'select' then
		i_budget_version = false
		i_bv_id = 0
		i_wo_sqls = i_wo_sqls
	else
		i_budget_version = true
		i_bv_id = long(i_wo_sqls)
		i_wo_sqls = ''
	end if
end if


//
// Set variable:
//	i_table_name
//
if upper(i_process_level) = 'BI' then
	i_table_name = 'BUDGET_MONTHLY_DATA'
else
	i_table_name = 'WO_EST_MONTHLY'
end if


//
// Set variables:
//	i_process_name
//	conc_check
// Check Concurrency
// Set Audit Context
//
if i_budget_version then
	// Set variable i_process_name
	i_process_name = 'budget_'+string(i_bv_id)
	conc_check = 'budget_'+string(i_bv_id)
	f_create_dynamic_ds(ds, 'grid','select work_order_id, revision from budget_version_fund_proj where budget_version_id = '+string(i_bv_id)+' and active = 1',sqlca,true)
	for idx = 1 to ds.rowcount( )
		wo_id = ds.getitemnumber(idx, 'work_order_id')
		rev = ds.getitemnumber(idx, 'revision')
		conc_check += ';budget_' + string(wo_id) + '_' + string(rev)
	next
	
	// Check Concurrency
	if sqlca.pp_set_process(i_process_name,conc_check) <> 'OK' then
		f_pp_msgs('Error: Concurrency check failed.')
		return rtn_failure
	end if
	
	// Set audit context
	sqlca.SetContext('audit','no')
else
	// Set variable i_process_name
	f_parsestringintostringarray(lower(i_wo_sqls),'union all',str_array)
	i_process_name = ''
	conc_check = ''
	for idx = 1 to upperbound(str_array)
		comma_pos = pos(str_array[idx],',')
		from_pos = pos(str_array[idx],'from')
		wo_id = long(trim(mid(str_array[idx], 8, comma_pos - 8)))
		rev = long(trim(mid(str_array[idx], comma_pos + 1, from_pos - (comma_pos + 1))))
		i_process_name += 'budget_'+string(wo_id)+'_'+string(rev)+';'
		conc_check += 'budget_'+string(wo_id)+'_'+string(rev)+';'
	next
	f_create_dynamic_ds(ds, 'grid','select budget_version_id from budget_version_fund_proj where (work_order_id, revision) in ('+i_wo_sqls+') and active = 1',sqlca,true)
	for idx = 1 to ds.rowcount( )
		l_bv_id = ds.getitemnumber(idx, 'budget_version_id')
		conc_check += 'budget_'+string(l_bv_id)+';'
	next
	if right(i_process_name,1) = ';' then
		i_process_name = left(i_process_name,len(i_process_name) - 1)
	end if
	if right(conc_check,1) = ';' then
		conc_check = left(conc_check,len(conc_check) - 1)
	end if
	
	// Check Concurrency
	if sqlca.pp_set_process(i_process_name,conc_check) <> 'OK' then
		f_pp_msgs('Error: Concurrency check failed.')
		return rtn_failure
	end if
	
	// Set audit context
	sqlca.SetContext('audit','yes')
end if


//
// Populate WO_EST_PROCESSING_TEMP
//
if uf_setup_wo_est_processing_temp() = -1 then
	return rtn_failure
end if


//
// Begin Processing
//
if i_budget_version then
	select description
	into :descr
	from budget_version
	where budget_version_id = :i_bv_id;
	
	f_pp_msgs('Starting Budget Processing for Budget Version "'+descr+'" (ID='+string(i_bv_id)+').')
	if upper(i_process_level) = 'BI' then
		f_pp_msgs('Process level: Budget Items')
	else
		f_pp_msgs('Process level: Funding Projects')
	end if
else
	if upper(i_process_level) = 'BI' then 	
		f_pp_msgs('Starting Budget Processing for selected Budget Items: '+i_wo_sqls+';')
	else
		f_pp_msgs('Starting Budget Processing for selected Funding Projects: '+i_wo_sqls+';')
	end if
end if


//
// Count the number of processes we are running
//	If we are running only one process then to not write to the "Automatic Processing Logs"
//
cnt = 0
if g_ssp_parms.boolean_arg[1] then
	cnt++
end if
if g_ssp_parms.boolean_arg[2] then
	cnt++
end if
if g_ssp_parms.boolean_arg[3] then
	cnt++
end if
if g_ssp_parms.boolean_arg[4] then
	cnt++
end if
if g_ssp_parms.boolean_arg[5] then
	cnt++
end if
if g_ssp_parms.boolean_arg[6] then
	cnt++
end if
if g_ssp_parms.boolean_arg[7] then
	cnt++
end if


//
// Load Budget Dollars from FP
//
if g_ssp_parms.boolean_arg[1] then
	i_load_update_with_act = g_ssp_parms.boolean_arg2[1] // Whether to automatically update with actuals
	
	select process_id into :p_id from pp_processes where description = 'Load Budget Dollars';
	select max(occurrence_id) into :o_id from pp_processes_occurrences where process_id = :p_id;
	if isnull(o_id) then o_id = 0
	if cnt > 1 then f_pp_msgs('Starting Load Budget Dollars, process '+string(p_id)+' occurrence '+string(o_id+1)+' (drill down for details):')
	if cnt > 1 then uf_setup_process('Load','START')
	rtn = uf_read_load()
	if rtn < 0 then 
		rtn = rtn_failure
	else 
		rtn = rtn_success
	end if 
	if cnt > 1 then 
		update pp_processes_occurrences set return_value = :rtn where process_id = :p_id and occurrence_id = :o_id + 1 using g_sqlca_logs;
		commit using g_sqlca_logs;
	end if		
	if cnt > 1 then uf_setup_process('Load','STOP')
	if cnt > 1 then f_pp_msgs('Complete. Return value = '+string(rtn)+'.')
end if

//
// Update with Actuals
//
if g_ssp_parms.boolean_arg[2] and rtn <> rtn_failure then
	select process_id into :p_id from pp_processes where description = 'Budget Update with Actuals';
	select max(occurrence_id) into :o_id from pp_processes_occurrences where process_id = :p_id;
	if isnull(o_id) then o_id = 0
	if cnt > 1 then f_pp_msgs('Starting Update with Actuals, process '+string(p_id)+' occurrence '+string(o_id+1)+' (drill down for details):')
	if cnt > 1 then uf_setup_process('UWA','START')
	rtn = uf_read_uwa()
	if rtn < 0 then 
		rtn = rtn_failure
	else 
		rtn = rtn_success
	end if 
	if cnt > 1 then 
		update pp_processes_occurrences set return_value = :rtn where process_id = :p_id and occurrence_id = :o_id + 1 using g_sqlca_logs;
		commit using g_sqlca_logs;
	end if
	if cnt > 1 then uf_setup_process('UWA','STOP')
	if cnt > 1 then f_pp_msgs('Complete. Return value = '+string(rtn)+'.')
end if

//
// Escalations
//
if g_ssp_parms.boolean_arg[3] and rtn <> rtn_failure then
	if g_ssp_labels.string_label2 = 'Deescalate' then
		i_deescalate = true // Run deescalations
	else
		i_deescalate = false // Run escalations
	end if
	select process_id into :p_id from pp_processes where description = 'Budget Escalations';
	select max(occurrence_id) into :o_id from pp_processes_occurrences where process_id = :p_id;
	if isnull(o_id) then o_id = 0
	if cnt > 1 then f_pp_msgs('Starting Budget Escalations, process '+string(p_id)+' occurrence '+string(o_id+1)+' (drill down for details):')
	if cnt > 1 then uf_setup_process('Esc','START')
	rtn = uf_read_esc()
	if rtn < 0 then 
		rtn = rtn_failure
	else 
		rtn = rtn_success
	end if 
	if cnt > 1 then 
		update pp_processes_occurrences set return_value = :rtn where process_id = :p_id and occurrence_id = :o_id + 1 using g_sqlca_logs;
		commit using g_sqlca_logs;
	end if
	if cnt > 1 then uf_setup_process('Esc','STOP')
	if cnt > 1 then f_pp_msgs('Complete. Return value = '+string(rtn)+'.')
end if

//
// Overheads
//
if g_ssp_parms.boolean_arg[4] and rtn <> rtn_failure then
	select process_id into :p_id from pp_processes where description = 'Budget Overheads';
	select max(occurrence_id) into :o_id from pp_processes_occurrences where process_id = :p_id;
	if isnull(o_id) then o_id = 0
	if cnt > 1 then f_pp_msgs('Starting Budget Overheads, process '+string(p_id)+' occurrence '+string(o_id+1)+' (drill down for details):')
	if cnt > 1 then uf_setup_process('OH','START')
	rtn = uf_read_oh()
	if rtn < 0 then 
		rtn = rtn_failure
	else 
		rtn = rtn_success
	end if 
	if cnt > 1 then 
		update pp_processes_occurrences set return_value = :rtn where process_id = :p_id and occurrence_id = :o_id + 1 using g_sqlca_logs;
		commit using g_sqlca_logs;
	end if
	if cnt > 1 then uf_setup_process('OH','STOP')
	if cnt > 1 then f_pp_msgs('Complete. Return value = '+string(rtn)+'.')
end if

//
// AFUDC
//
if g_ssp_parms.boolean_arg[5] and rtn <> rtn_failure then
	select process_id into :p_id from pp_processes where description = 'Budget AFUDC';
	select max(occurrence_id) into :o_id from pp_processes_occurrences where process_id = :p_id;
	if isnull(o_id) then o_id = 0
	if cnt > 1 then f_pp_msgs('Starting Budget AFUDC, process '+string(p_id)+' occurrence '+string(o_id+1)+' (drill down for details):')
	if cnt > 1 then uf_setup_process('AFUDC','START')
	rtn = uf_read_afudc()
	if rtn < 0 then 
		rtn = rtn_failure
	else 
		rtn = rtn_success
	end if 
	if cnt > 1 then 
		update pp_processes_occurrences set return_value = :rtn where process_id = :p_id and occurrence_id = :o_id + 1 using g_sqlca_logs;
		commit using g_sqlca_logs;
	end if
	if cnt > 1 then uf_setup_process('AFUDC','STOP')
	if cnt > 1 then f_pp_msgs('Complete. Return value = '+string(rtn)+'.')
end if

//
// WIP Comps
//
if g_ssp_parms.boolean_arg[6] and rtn <> rtn_failure then
	select process_id into :p_id from pp_processes where description = 'Budget WIP Comp';
	select max(occurrence_id) into :o_id from pp_processes_occurrences where process_id = :p_id;
	if isnull(o_id) then o_id = 0
	if cnt > 1 then f_pp_msgs('Starting Budget WIP Computations, process '+string(p_id)+' occurrence '+string(o_id+1)+' (drill down for details):')
	if cnt > 1 then uf_setup_process('WIP','START')
	rtn = uf_read_wip()
	if rtn < 0 then 
		rtn = rtn_failure
	else 
		rtn = rtn_success
	end if 
	if cnt > 1 then 
		update pp_processes_occurrences set return_value = :rtn where process_id = :p_id and occurrence_id = :o_id + 1 using g_sqlca_logs;
		commit using g_sqlca_logs;
	end if
	if cnt > 1 then uf_setup_process('WIP','STOP')
	if cnt > 1 then f_pp_msgs('Complete. Return value = '+string(rtn)+'.')
end if

//
// Budget to CR
//
if g_ssp_parms.boolean_arg[7] and rtn <> rtn_failure then //This one can't be run in-line with the others. That is to say, if bool[7] is true then bool[1] through bool[6] must all be false.
	i_insert_where_clause_id = g_ssp_parms.long_arg[2]
	i_delete_where_clause_id = g_ssp_parms.long_arg[3]
	i_posting_id = g_ssp_parms.long_arg[4]
	
	i_cr_table = g_ssp_parms.string_arg[3]
	i_cr_budget_version = g_ssp_parms.string_arg[4]
	
	i_only_in_bv = g_ssp_parms.boolean_arg2[2]
	
	select process_id into :p_id from pp_processes where description = 'Budget To CR';
	select max(occurrence_id) into :o_id from pp_processes_occurrences where process_id = :p_id;
	if isnull(o_id) then o_id = 0
	if cnt > 1 then f_pp_msgs('Starting Budget to CR, process '+string(p_id)+' occurrence '+string(o_id+1)+' (drill down for details):')
	if cnt > 1 then uf_setup_process('CR','START')
	rtn = uf_read_to_cr()
	if rtn < 0 then 
		rtn = rtn_failure
	else 
		rtn = rtn_success
	end if 
	if cnt > 1 then 
		update pp_processes_occurrences set return_value = :rtn where process_id = :p_id and occurrence_id = :o_id + 1 using g_sqlca_logs;
		commit using g_sqlca_logs;
	end if
	if cnt > 1 then uf_setup_process('CR','STOP')
	if cnt > 1 then f_pp_msgs('Complete. Return value = '+string(rtn)+'.')
end if

// Set audit context
sqlca.SetContext('audit','yes')

sqlca.pp_set_process(i_process_name,' ') //remove concurrent process lock

return rtn
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function longlong uf_read_afudc ();longlong rtn
uo_budget_afudc bafudc

f_pp_msgs("********")
f_pp_msgs("Start Budget AFUDC")
f_pp_msgs('Level of Processing = '+i_process_level)
f_pp_msgs("********")

//Logic to call afudc process
bafudc = create uo_budget_afudc
bafudc.i_progress_bar = true
bafudc.i_pp_msgs = true
bafudc.i_messagebox_onerror = false
bafudc.i_statusbox_onerror = false
bafudc.i_add_time_to_messages = false
if i_budget_version then
	bafudc.i_budget_version = i_bv_id
end if
rtn = bafudc.uf_read(i_table_name, 0)

destroy bafudc

if rtn = -1 then
	f_pp_msgs("********")
	f_pp_msgs("ERRORS OCCURRED. CHECK LOG FOR ADDITIONAL INFORMATION")
	f_pp_msgs("********")
	rollback;
	return -1
else
	f_pp_msgs("********")
	f_pp_msgs("End Budget AFUDC Processing Logic")
	f_pp_msgs("Successful Calculation")
	f_pp_msgs("********")
	
	if i_budget_version then
		update budget_process_control
		set afudc_calc = sysdate
		where budget_version_id = :i_bv_id;
	end if

	commit;
	return 1
end if

end function

public function longlong uf_read_uwa ();longlong act_month, rtn
uo_budget_revision uo_revision

f_pp_msgs("********")
f_pp_msgs("Start Budget Update with Actuals")
f_pp_msgs('Level of Processing = '+i_process_level)
f_pp_msgs("********")

//Run the process
if upper(i_process_level) = 'FP'  then
	select actuals_month into :act_month
	from budget_version
	where budget_version_id = :i_bv_id;
	
	uo_revision = create uo_budget_revision
	
	uo_revision.i_messagebox_onerror = false
	uo_revision.i_status_box = false
	uo_revision.i_add_time_to_messages = false
	uo_revision.i_statusbox_onerror = false
	uo_revision.i_pp_msgs = true
	uo_revision.i_progress_bar = true
	
	rtn = uo_revision.uf_update_with_act('fp',act_month)
	
else
	
	rtn = f_budget_update_with_actuals(i_bv_id)
	
end if

if rtn = -1 then
	f_pp_msgs("********")
	f_pp_msgs("ERRORS OCCURRED. CHECK LOG FOR ADDITIONAL INFORMATION")
	f_pp_msgs("********")
	rollback;
	return -1
else
	f_pp_msgs("********")
	f_pp_msgs("End Update with actuals Processing Logic")
	f_pp_msgs("Successful Calculation")
	f_pp_msgs("********")
	
	if i_budget_version then
		update budget_process_control
		set update_with_actuals = sysdate
		where budget_version_id = :i_bv_id;
	end if

	commit;
	return 1
end if

end function

public function longlong uf_read_esc ();longlong funding_wo, rtn
uo_budget_escalation uo_escalate

f_pp_msgs("********")
f_pp_msgs("Start Budget Escalations")
f_pp_msgs('Level of Processing = '+i_process_level)
f_pp_msgs("********")

if upper(i_process_level) = 'BI' then 
	funding_wo = 2
	sqlca.analyze_table('BUDGET_MONTHLY_DATA_ESCALATION')
else
	funding_wo = 1
	sqlca.analyze_table('WO_EST_MONTHLY_ESCALATION')
end if

//Run the process
uo_escalate = create uo_budget_escalation
uo_escalate.i_messagebox_onerror = false
uo_escalate.i_status_box = false
uo_escalate.i_add_time_to_messages = false
uo_escalate.i_statusbox_onerror = false
uo_escalate.i_pp_msgs = true
uo_escalate.i_budget_version = i_bv_id
uo_escalate.i_progress_bar = true

if i_deescalate then
	rtn = uo_escalate.uf_deescalate(funding_wo)
else //Default to escalate because that is the more common case
	rtn = uo_escalate.uf_escalate(funding_wo)
end if

if rtn = -1 then
	f_pp_msgs("********")
	f_pp_msgs("ERRORS OCCURRED. CHECK LOG FOR ADDITIONAL INFORMATION")
	f_pp_msgs("********")
	rollback;
	return -1
else
	f_pp_msgs("********")
	f_pp_msgs("End Budget Escalations Processing Logic")
	f_pp_msgs("Successful Calculation")
	f_pp_msgs("********")
	
	if i_budget_version then
		update budget_process_control
		set escalate_bv = sysdate
		where budget_version_id = :i_bv_id;
	end if

	commit;
	return 1
end if

end function

public function longlong uf_read_oh ();longlong rtn
uo_budget_overheads uo_oh

f_pp_msgs("********")
f_pp_msgs("Start Budget Overheads")
f_pp_msgs('Level of Processing = '+i_process_level)
f_pp_msgs("********")

//Run the process
uo_oh = create uo_budget_overheads
uo_oh.i_status_box = false
uo_oh.i_pp_msgs = true
uo_oh.i_allow_clearings = true
uo_oh.i_status_box = false
uo_oh.i_progress_bar = true
uo_oh.i_add_time_to_messages = false
uo_oh.i_messagebox_onerror = false
uo_oh.i_statusbox_onerror = false
uo_oh.i_budget_version = i_bv_id
rtn = uo_oh.uf_read('w_budget_process_control',i_table_name)

if rtn = -1 then
	f_pp_msgs("********")
	f_pp_msgs("ERRORS OCCURRED. CHECK LOG FOR ADDITIONAL INFORMATION")
	f_pp_msgs("********")
	rollback;
	return -1
else
	f_pp_msgs("********")
	f_pp_msgs("End Budget Overhead Processing Logic")
	f_pp_msgs("Successful Calculation")
	f_pp_msgs("********")
	
	if i_budget_version then
		update budget_process_control
		set run_overheads = sysdate
		where budget_version_id = :i_bv_id;
	end if

	commit;
	return 1
end if

end function

public function longlong uf_read_load ();longlong rtn
uo_budget_revision uo_revision

//
// Need to populate WO_EST_PROCESSING_TEMP a particular way
//
delete from wo_est_processing_temp;

insert into wo_est_processing_temp (work_order_id, revision)
select distinct woc.budget_id, :i_bv_id
from budget_version_fund_proj bvfp, work_order_control woc
where bvfp.budget_version_id = :i_bv_id
and bvfp.work_order_id = woc.work_order_id
and bvfp.active = 1;

if sqlca.sqlcode < 0 then
	f_pp_msgs('ERROR: Building List of Budget Items to process:~n~n'+sqlca.sqlerrtext)
	rtn = -1
	goto return_val
end if

sqlca.analyze_table('WO_EST_PROCESSING_TEMP')

//
// Load Budget Dollars from FP
//
f_pp_msgs("********")
f_pp_msgs("Start Load Budget Dollars")
f_pp_msgs("********")
f_pp_msgs("")

uo_revision = create uo_budget_revision

uo_revision.i_messagebox_onerror = false
uo_revision.i_status_box = false
uo_revision.i_pp_msgs = true
uo_revision.i_add_time_to_messages = false
uo_revision.i_statusbox_onerror = false
uo_revision.i_progress_bar = true

rtn = uo_revision.uf_load_fp_to_bi()

if rtn = -1 then
	rtn = -1
	goto return_val
end if

//
// Restore WO_EST_PROCESSING_TEMP
//
if uf_setup_wo_est_processing_temp() = -1 then
	rtn = -1
	goto return_val
end if

//
//  UPDATE WITH ACTUALS:
//
if i_load_update_with_act then
	rtn = f_budget_update_with_actuals(i_bv_id)
	
	if rtn <> 1 then
		rtn = -1
		goto return_val
	end if
end if

return_val:

if rtn = -1 then
	f_pp_msgs("********")
	f_pp_msgs("ERRORS OCCURRED. CHECK LOG FOR ADDITIONAL INFORMATION")
	f_pp_msgs("********")
	rollback;
	return -1
else
	f_pp_msgs("********")
	f_pp_msgs("End Load Budget Dollars Processing Logic")
	f_pp_msgs("Successful Calculation")
	f_pp_msgs("********")
	
	if i_budget_version then
		update budget_process_control
		set load_budget_dollars = sysdate
		where budget_version_id = :i_bv_id;
	end if

	commit;
	return 1
end if

end function

public function longlong uf_read_wip ();longlong rtn
uo_budget_wip_computations uo_wip

f_pp_msgs("********")
f_pp_msgs("Start Budget WIP Computations")
f_pp_msgs('Level of Processing = '+i_process_level)
f_pp_msgs("********")

//Run the process
uo_wip = create uo_budget_wip_computations
uo_wip.i_status_box = false
uo_wip.i_pp_msgs = true
uo_wip.i_status_box = false
uo_wip.i_progress_bar = true
uo_wip.i_add_time_to_messages = false
uo_wip.i_messagebox_onerror = false
uo_wip.i_statusbox_onerror = false
uo_wip.i_budget_version = i_bv_id
rtn = uo_wip.uf_wip_comp_calc(i_table_name)

if rtn = -1 then
	f_pp_msgs("********")
	f_pp_msgs("ERRORS OCCURRED. CHECK LOG FOR ADDITIONAL INFORMATION")
	f_pp_msgs("********")
	rollback;
	return -1
else
	f_pp_msgs("********")
	f_pp_msgs("End Budget WIP Computation Processing Logic")
	f_pp_msgs("Successful Calculation")
	f_pp_msgs("********")
	
	if i_budget_version then
		update budget_process_control
		set wip_comp = sysdate
		where budget_version_id = :i_bv_id;
	end if

	commit;
	return 1
end if

end function

public function longlong uf_read_to_cr ();longlong rtn, to_projects
string budget_version_descr, projects_table
uo_cr_to_budget_to_cr uo_cr

select to_projects
into :to_projects
from cr_to_budget_to_cr_control
where id = :i_posting_id;

select replace(replace(replace(projects_table,' ','_'),'-','_'),'/','_')
  into :projects_table
  from cr_to_budget_to_cr_control
 where id = :i_posting_id;

if upper(projects_table) = 'FCST_DEPR_LEDGER' or upper(projects_table) = 'FCST_CPR_DEPR' then
select description
into :budget_version_descr
	from fcst_depr_version
	where fcst_depr_version_id = :i_bv_id;
else
	select description
	into :budget_version_descr
from budget_version
where budget_version_id = :i_bv_id;
end if

f_pp_msgs("********")
if to_projects = 1 then
	f_pp_msgs("Start Pull Budget from CR")
else
	f_pp_msgs("Start Send Budget to CR")
end if
f_pp_msgs('Level of Processing = '+i_process_level)
f_pp_msgs("********")

uo_cr = CREATE uo_cr_to_budget_to_cr
uo_cr.i_only_in_bv = i_only_in_bv
uo_cr.i_cr_table = upper(i_cr_table)
uo_cr.i_cr_budget_version = i_cr_budget_version

rtn = uo_cr.uf_read(i_posting_id, i_insert_where_clause_id, i_delete_where_clause_id, budget_version_descr)

if rtn = -1 then
	f_pp_msgs("********")
	f_pp_msgs("ERRORS OCCURRED. CHECK LOG FOR ADDITIONAL INFORMATION")
	f_pp_msgs("********")
	rollback;
	return -1
else
	f_pp_msgs("********")
	if to_projects = 1 then
		f_pp_msgs("End Pull Budget from CR Processing Logic")
	else
		f_pp_msgs("End Send Budget to CR Processing Logic")
	end if
	f_pp_msgs("Successful Calculation")
	f_pp_msgs("********")
	
	if i_budget_version then
		if to_projects = 1 then
			update budget_process_control
			set overheads_calc = sysdate
			where budget_version_id = :i_bv_id;
		else
			update budget_process_control
			set stage_for_alloc = sysdate
			where budget_version_id = :i_bv_id;
		end if
	end if

	commit;
	return 1
end if

end function

public function longlong uf_setup_process (string a_process_name, string a_start_stop);longlong process_id, running_session_id
a_start_stop = upper(trim(a_start_stop))

if isnull(i_session_id) or i_session_id = 0 then
	select userenv('sessionid') into :i_session_id from dual;
end if

if a_start_stop = 'START' then
	
	i_prior_process_id = g_process_id
	i_prior_occurrence_id = g_occurrence_id
	
	// Go find the new process_id for the batch
	select process_id
		into :process_id
		from pp_processes
		where executable_file = 'ssp_budget.exe ' || :a_process_name
		using g_sqlca_logs;

	// Found the process_id, now get the occurrence_id
	//	f_pp_msgs_start_log sets g_process_id, g_msg_order, g_occurrence_id
	f_pp_msgs_start_log(process_id)
	
	// Copy the header logs from the old process to the new process.
	insert into pp_processes_messages
		(process_id, occurrence_id, msg, msg_order)
		select :g_process_id, :g_occurrence_id, msg, msg_order
		from pp_processes_messages 
		where process_id = :i_prior_process_id
		and occurrence_id = :i_prior_occurrence_id
		and msg_order < :i_prior_msg_order
	using g_sqlca_logs;
		
	if g_sqlca_logs.SQLCode < 0 then
		g_rte.msg(-1,'Copying header logs to new occurrence',{g_sqlca_logs.SQLErrText})
		throw g_rte
	else
		commit using g_sqlca_logs;
		g_msg_order = i_prior_msg_order + 1
	end if

	update pp_processes_occurrences 
	set suppress_indicator = 
		(select suppress_indicator 
		from pp_processes_occurrences
		where process_id = :i_prior_process_id
		and occurrence_id = :i_prior_occurrence_id)
	where process_id = :g_process_id
	and occurrence_id = :g_occurrence_id
	using g_sqlca_logs;

//	// Check the running session id now.
//	select running_session_id into :running_session_id from pp_processes
//		where process_id = :g_process_id
//		for update wait 60
//		using g_sqlca_logs;
//	
//	if isnull(running_session_id) or running_session_id = 0 then
//		//  NOT CURRENTLY RUNNING:  update the record with this session_id.		
//		update pp_processes 
//		set running_session_id = :i_session_id
//		where process_id = :g_process_id
//		using g_sqlca_logs;
//		
//		if g_sqlca_logs.SQLCode < 0 then
//			g_rte.msg(-1,'updating pp_processes.running_session_id',{g_sqlca_logs.SQLErrText})
//			throw g_rte
//		else
//			commit using g_sqlca_logs;
//		end if			
//	else
//		g_rte.msg(-1,'Another session is currently running this process.',{a_process_name,"Session ID = " + string(running_session_id)})
//		throw g_rte
//	end if
//	

else
	if i_prior_process_id <> g_process_id then
//		update pp_processes set running_session_id = null
//		where process_id = :g_process_id
//		and running_session_id = :i_session_id
//		using g_sqlca_logs;
				
		f_pp_msgs_end_log()

		g_process_id = i_prior_process_id
		g_occurrence_id = i_prior_occurrence_id
		
		select max(msg_order) + 1 into :g_msg_order from pp_processes_messages where process_id = :g_process_id and occurrence_id = :g_occurrence_id;
	end if
end if

return 0
end function

public function longlong uf_setup_wo_est_processing_temp ();string sqls

//
// Populate WO_EST_PROCESSING_TEMP
//
if i_budget_version then
	// Populate WO_EST_PROCESSING_TEMP
	delete from wo_est_processing_temp;
	if upper(i_process_level) = 'BI' then 	
		insert into wo_est_processing_temp (work_order_id, revision)
		select budget_id, budget_version_id
		from budget_amounts
		where budget_version_id = :i_bv_id;
	else //FP
		insert into wo_est_processing_temp (work_order_id, revision)
		select work_order_id, revision
		from budget_version_fund_proj
		where budget_version_id = :i_bv_id
		and active = 1;
	end if
	if sqlca.sqlcode < 0 then
		f_pp_msgs('ERROR Building wo_est_processing_temp.')
		rollback;
		return -1
	end if
else
	// Populate WO_EST_PROCESSING_TEMP
	delete from wo_est_processing_temp;
	if upper(i_process_level) = 'BI' then 	
		sqls = &
		"insert into wo_est_processing_temp (work_order_id, revision) " +&
		"select budget_id, budget_version_id " +&
		"from budget_amounts " +&
		"where (budget_id, budget_version_id) in ("+i_wo_sqls+")"
	else //FP
		sqls = &
		"insert into wo_est_processing_temp (work_order_id, revision) " +&
		"select work_order_id, revision " +&
		"from work_order_approval " +&
		"where (work_order_id, revision) in ("+i_wo_sqls+")"
	end if
	execute immediate :sqls;
	if sqlca.sqlcode < 0 then
		f_pp_msgs('ERROR Building wo_est_processing_temp.')
		rollback;
		return -1
	end if
end if

sqlca.analyze_table('WO_EST_PROCESSING_TEMP')

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppprojct_custom_version nvo_ppprojct_custom_version

choose case a_pbd_name
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

