HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_cpr_new_month.exe'

nvo_cpr_control i_nvo_cpr_control

end variables

forward prototypes
public function longlong uf_read ()
public function boolean uf_new_month_main ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
//	
//	
//	
longlong rtn_success, rtn_failure, rtn, i, num
string process_msg
w_dws w

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE';

if not uf_new_month_main() then
	i_nvo_cpr_control.of_log_failed_companies("OPEN NEW CPR MONTH")
	rtn = rtn_failure
elseif upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
	i_nvo_cpr_control.of_log_failed_companies("OPEN NEW CPR MONTH")
	rtn = rtn_failure
else
	rtn = rtn_success
end if

// Release the concurrency lock
i_nvo_cpr_control.of_releaseProcess(process_msg)
f_pp_msgs("Release Process Status: " + process_msg)

return rtn
end function

public function boolean uf_new_month_main ();/************************************************************************************************************************************************************
**
**	uf_new_month_main()
**	
**	This function corresponds to w_cpr_control.cb_new_month.event clicked().
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
long curr_month_mm, curr_month_yyyy, new_month_mm, new_month_yyyy
longlong i, rc, rows, depr, curs,tablepos, items, j, new_row, sub_type, vintage 
datetime  new_month_date, curr_month_date
decimal {5} rem_life
decimal {6} factor
string month_string, original_sql, sqls, template, replacestring, ret,subledger, comp_descr

string company_description, company_string
longlong group_id, books_id, num_rows, cc, total_company, subledger_id,check_count

longlong rtn,  hw_max_year, process_id, pp_stat_id, check, num_failed_companies
string msg, user_id, find_str, display_month_string

// Change datawindows to datastores
string rtns

uo_ds_top ds_calc_subledger_depr // (dw_calc_subledger_depr)
ds_calc_subledger_depr = CREATE uo_ds_top

uo_ds_top ds_hide // (dw_hide)
ds_hide = CREATE uo_ds_top

// Construct the nvo
i_nvo_cpr_control.of_constructor()

// Get the ssp parameters
i_nvo_cpr_control.i_company_idx[]  = g_ssp_parms.long_arg[]
i_nvo_cpr_control.i_original_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_months[1] = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_original_col_num = 1
i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return false
end if

// Get the descriptions
rtn = i_nvo_cpr_control.of_getDescriptionsFromIds(i_nvo_cpr_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_cpr_control.of_selectedCompanies(i_nvo_cpr_control.i_company_idx) = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do " + &
				"not line up")
	return false
end if 

f_set_large_rollback_segment()

total_company = upperbound(i_nvo_cpr_control.i_company_idx)

for cc = 1 to total_company
	
	rc = i_nvo_cpr_control.of_setupCompany(cc)
	
	if rc < 1 then goto next_company // next company
	
	// do not allow more than 2 open months
	curr_month_date = i_nvo_cpr_control.i_month
	
	//maint-44892: display the current month
	select to_char(:i_nvo_cpr_control.i_month , 'MON-DD-YYYY') into :display_month_string from dual;	
	f_pp_msgs("Current month = "+ string(display_month_string)) 
	
	select count(*) into :check from cpr_control
	where cpr_closed is null and company_id = :i_nvo_cpr_control.i_company and
	accounting_month = add_months(:curr_month_date, -1);
	
	if check = 1 then
		f_pp_msgs("Cannot have more than two months open")
		f_pp_msgs("Cannot open new month")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		DESTROY ds_calc_subledger_depr
		DESTROY ds_hide
		
		return false
	end if 

	curr_month_mm = month(date(i_nvo_cpr_control.i_months[1]))	
	curr_month_yyyy  = year(date(i_nvo_cpr_control.i_months[1]))
	f_pp_msgs("    current month mm= "+ string(curr_month_mm))
	f_pp_msgs("    current month yyyy = "+ string(curr_month_yyyy))
	
	new_month_mm = curr_month_mm + 1	
	if new_month_mm = 13 then 
		new_month_mm = 1
		new_month_yyyy = curr_month_yyyy + 1
	end if
	f_pp_msgs("    new month mm= "+ string(new_month_mm))
	f_pp_msgs("    new month yyyy = "+ string(curr_month_yyyy))

	//maint-44892 bad:  /*new_month = datetime(date(string(month) + '/1/' + string(year)))*/
	//maint-44892: use oracle function to add one month to get the next month's date
	select add_months(:i_nvo_cpr_control.i_month, 1) into :new_month_date from dual;
	
	//maint-44892:  display the month date to be opened
	select to_char(:new_month_date , 'MON-DD-YYYY') into :display_month_string from dual;	
	f_pp_msgs("Month to open = "+ string(display_month_string)) 

//	// SEB: Rewritten in a way that makes sense for the interface instead of referencing all of the datawindows from w_cpr_control
//	i_nvo_cpr_control.i_months[2] = i_nvo_cpr_control.i_months[1]
//	i_nvo_cpr_control.i_months[1] = datetime(new_month_date)
//	i_nvo_cpr_control.i_month     = datetime(new_month_date)

	insert into CPR_CONTROL (COMPANY_ID, ACCOUNTING_MONTH)
	values (:i_nvo_cpr_control.i_company,  :new_month_date);
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Error inserting new row into CPR_CONTROL: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		DESTROY ds_calc_subledger_depr
		DESTROY ds_hide
		
		return false
	end if 

	insert into PP_INTERFACE_DATES (INTERFACE_ID, COMPANY_ID, ACCOUNTING_MONTH, LAST_RUN)
	(select PID.INTERFACE_ID, PID.COMPANY_ID, :new_month_date, null
	from PP_INTERFACE_DATES PID
	join PP_INTERFACE PI on PID.INTERFACE_ID = PI.INTERFACE_ID and PID.COMPANY_ID = PI.COMPANY_ID
	where PI.NEEDED_FOR_CLOSING = 1 and PI.SUBSYSTEM = 'Asset Management'
	and PID.COMPANY_ID = :i_nvo_cpr_control.i_company and PID.ACCOUNTING_MONTH = :curr_month_date);
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Error inserting new rows into PP_INTERFACE_DATES: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		DESTROY ds_calc_subledger_depr
		DESTROY ds_hide
		
		return false
	end if
	// SEB: Rewritten in a way that makes sense for the interface instead of referencing all of the datawindows from w_cpr_control
	
	// if opening january, copy the handy whitman rates to new year
	if new_month_mm = 1 then
		
		select max(year) into :hw_max_year
		from handy_whitman_rates; 
		
		if hw_max_year = curr_month_yyyy then
			f_pp_msgs( "Copying HW rates to new year.."+ ' ' + string(now()))
			
			insert into handy_whitman_rates(hw_table_line_id, hw_region_id, year, rate)
			(select hw_table_line_id, hw_region_id, :new_month_yyyy, rate
			from handy_whitman_rates where year = :curr_month_yyyy);
			
			if sqlca.sqlcode < 0 then
				f_pp_msgs("Cannot copy HW rates to new year. " + sqlca.sqlerrtext)
			end if
	
			f_pp_msgs("    hw rates insert rowcount = "+ string(sqlca.sqlnrows ))
		
		elseif  hw_max_year < curr_month_yyyy then
			//Maint 6308:  better message
		   f_pp_msgs( "Cannot find HW rates to copy from prior year.  If you use Handy Whitman for Trend Retirements or Escalated/Isurable value reporting, you will need to load rates for use in the new year.")
			  
			//Maint 6308:  the code needs to continue to create new month data.  stopping here is very bad!
			//return
		end if
		
	end if 
	
	comp_descr = i_nvo_cpr_control.i_company_descr[cc]
	
	f_pp_msgs("Add New Month started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
	
	pp_stat_id = f_pp_statistics_start("Add New Month", "Close PowerPlant: " + comp_descr)
	
	// Add a new month to depreciation ledger
	f_pp_msgs(  "Adding New Month to Depreciation..."+ ' ' + string(now()))
		
	// maint-43820 Use Inserts instead of datastores to prevent memory leak
	insert into depr_ledger(depr_group_id, set_of_books_id, gl_post_mo_yr, depr_ledger_status)
	select dl.depr_group_id, dl.set_of_books_id, :new_month_date, 9
	from depr_ledger dl, depr_group dg
	where dl.depr_group_id = dg.depr_group_id
	and dg.company_id = :i_nvo_cpr_control.i_company
	and dl.gl_post_mo_yr = :curr_month_date;
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Error Adding month to the Depreciation Ledger: " + SQLCA.SQLErrText)
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			 
			rollback;
			goto next_company
	end if
	
	f_pp_msgs("    depr_ledger insert rowcount = "+ string(sqlca.sqlnrows ))
	
	//sqlca.sqlnrows 

	// copy 	// copy depr_net_salvage_amort
	// Maint 2615: include NOT EXISTS to avoid duplicates when using the monthly amort option
	INSERT INTO depr_net_salvage_amort (
		set_of_books_id, depr_group_id, vintage, gl_post_mo_yr, 
		cor_treatment, salvage_treatment, net_salvage_amort_life, 
		cost_of_removal_bal, cost_of_removal_reserve, salvage_bal, salvage_reserve)
	SELECT set_of_books_id, depr_net_salvage_amort.depr_group_id, vintage, :new_month_date, 
		depr_net_salvage_amort.cor_treatment, depr_net_salvage_amort.salvage_treatment, depr_net_salvage_amort.net_salvage_amort_life, 
		cost_of_removal_bal, cost_of_removal_reserve, salvage_bal, salvage_reserve
	FROM depr_net_salvage_amort, depr_group
	WHERE depr_net_salvage_amort.depr_group_id = depr_group.depr_group_id 
	AND company_id = :i_nvo_cpr_control.i_company 
	AND gl_post_mo_yr = :curr_month_date 
	AND NOT EXISTS (
		SELECT 1 FROM depr_net_salvage_amort d 
		WHERE d.set_of_books_id = depr_net_salvage_amort.set_of_books_id 
			AND d.depr_group_id = depr_net_salvage_amort.depr_group_id
			AND d.gl_post_mo_yr = :new_month_date
			AND d.vintage = depr_net_salvage_amort.vintage
	); 	
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Depr Net Salvage Amort Insert Error: " + SQLCA.SQLErrText)
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		 
		rollback;
		goto next_company
	end if
	
	f_pp_msgs("    depr_net_salvage_amort insert rowcount = "+ string(sqlca.sqlnrows ))
	

	// Add a new month to depr reserve allocation factors	
//	ds_depr_res_allo_factors.settransobject(sqlca)
	
	month_string = "where to_char(month,'mm-yyyy') = '" + string(curr_month_date,'mm-yyyy') + "'"
	
	// maint 29786: BSB delete the records that exist
	delete from depr_res_allo_factors df
	where df.depr_group_id in
	(
		select dg.depr_group_id
		from depr_group dg
		where dg.company_id = :i_nvo_cpr_control.i_company
	)
	and month = :new_month_date;
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Depr Res Allo Factor Delete Error: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		goto next_company
	end if
	
	INSERT INTO DEPR_RES_ALLO_FACTORS
	(SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, MONTH, FACTOR, THEO_FACTOR, 
	 remaining_life, life_factor, cor_factor)
	SELECT SET_OF_BOOKS_ID, DEPR_RES_ALLO_FACTORS.DEPR_GROUP_ID, VINTAGE, :new_month_date, 
			 NVL(FACTOR,0) , NVL(THEO_FACTOR,0), remaining_life, life_factor, cor_factor
	FROM DEPR_RES_ALLO_FACTORS, DEPR_GROUP
	WHERE DEPR_RES_ALLO_FACTORS.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID AND
	COMPANY_ID = :i_nvo_cpr_control.I_COMPANY AND 
	MONTH = :curr_month_date; 
	
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Depr Res Allo Factor Insert Error: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		goto next_company
	end if
	
	f_pp_msgs("    depr_res_allo_factors insert rowcount = "+ string(sqlca.sqlnrows ))
	
	
	// maint 29786: BSB delete the records that exist
	delete from depr_vintage_summary df
	where df.depr_group_id in
	(
		select dg.depr_group_id
		from depr_group dg
		where dg.company_id = :i_nvo_cpr_control.i_company
	)
	and accounting_month = :new_month_date;
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Depr Vintage Summary Delete Error: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		goto next_company
	end if
	
	INSERT INTO DEPR_VINTAGE_SUMMARY
	(SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, ACCOUNTING_MONTH, ACCUM_COST, combined_depr_group_id)
	SELECT SET_OF_BOOKS_ID, DEPR_VINTAGE_SUMMARY.DEPR_GROUP_ID, VINTAGE, :new_month_date, NVL(ACCUM_COST,0) 
			, depr_vintage_summary.combined_depr_group_id
	FROM DEPR_VINTAGE_SUMMARY, DEPR_GROUP
	WHERE DEPR_VINTAGE_SUMMARY.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID AND
	COMPANY_ID = :i_nvo_cpr_control.I_COMPANY AND  
	ACCOUNTING_MONTH = :curr_month_date; 
	
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs( "Depr Vintage Summary Insert Error: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		goto next_company
	end if
	
	
	f_pp_msgs("    depr_vintage_summary insert rowcount = "+ string(sqlca.sqlnrows ))
	
	// Add a new month to Account Summary
	f_pp_msgs( "Adding New Month to Account Summary..."+ ' ' + string(now()))
	
	//// 20060407 Chris Kaiser crf 135
	DELETE FROM account_summary 
	WHERE company_id = :i_nvo_cpr_control.i_company AND gl_posting_mo_yr = :new_month_date;
	
	if sqlca.sqlcode <> 0 then
		f_pp_msgs( "DELETE FROM account_summary Error: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		goto next_company
	end if
	
	// maint-43820 Use Inserts instead of datastores to prevent memory leak
	insert into account_summary
 	 (company_id, gl_account_id, bus_segment_id, sub_account_id,
  	 utility_account_id, major_location_id, set_of_books_id, gl_posting_mo_yr,
	   acct_summ_status, beginning_balance, additions, retirements, transfers_in,
	   transfers_out, adjustments, ending_balance)
	  select company_id, gl_account_id, bus_segment_id, sub_account_id,
	         utility_account_id, major_location_id, set_of_books_id,
	         :new_month_date , 1 acct_summ_status,
	         ending_balance beginning_balance, 0 additions, 0 retirements,
	         0 transfers_in, 0 transfers_out, 0 adjustments, 0 ending_balance
	    from account_summary
	   where company_id = :i_nvo_cpr_control.i_company
	     and gl_posting_mo_yr = :curr_month_date;
	  
	if sqlca.sqlcode <> 0 then
		f_pp_msgs(  "Error Adding month to the Account Summary Table: " + SQLCA.SQLErrText)
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		goto next_company
	
	end if  
	
	f_pp_msgs("    account_summary insert rowcount = "+ string(sqlca.sqlnrows ))
	
	// Add a new month to depreciation subledgers
	f_pp_msgs( "Adding New Month to Subledgers..."+ ' ' + string(now()))
	
	ds_calc_subledger_depr.DataObject = "dw_calc_subledger_depr"
	ds_calc_subledger_depr.settransobject(sqlca)
	
	original_sql = ds_calc_subledger_depr.describe("datawindow.table.select") 
	
	original_sql = original_sql + " and cpr_ledger.company_id = " + string(i_nvo_cpr_control.i_company) + &
					 + " and (to_number(to_char(~~~"DEPR_SUBLEDGER_TEMPLATE~~~".~~~"CPR_POSTING_MO_YR~~~",~'J~'))" &
					 + " =  to_number(to_char(to_date('" + string(curr_month_date,'mm/dd/yyyy') + "' ,'mm/dd/yyyy'),~'J~'))) "   
	
	
	
	sqls = "select subledger_name, depreciation_indicator, subledger_type_id from subledger_control"
	rtns = f_create_dynamic_ds(ds_hide,'grid',sqls,sqlca,true)
	
	if rtns <> 'OK' then
		// SEB: Added error checking
		f_pp_msgs("ERROR: Creating ds_hide: " + rtns)
	end if
	
	rows = ds_hide.rowcount()
	
	for i = 1 to rows
	
		depr = ds_hide.getitemnumber(i,'depreciation_indicator')
		subledger_id = ds_hide.getitemnumber(i,'subledger_type_id')
		
		if depr = 1 then
	
			subledger = upper(trim(ds_hide.getitemstring(i,'subledger_name')))
		  
			f_pp_msgs( "   " +  subledger + "...") 
	
			sqls = original_sql
	
			//Find tablename throughout SQL and replace; Check DBMS for necessary case changes
	
			template = 'depr_subledger_template'
	
			choose case s_sys_info.dbms
				case "sybase"
				template = lower(template)
				replacestring = lower(subledger+'_depr')
				case "oracle"
				template = upper(template)
				replacestring = upper(subledger+'_depr')
			case else
			end choose
	
			curs = 1
			tablepos = pos(sqls, template, curs)
			do until tablepos = 0
				sqls = replace(sqls, tablepos, 23, replacestring)		
				curs = curs + len(replacestring)
				tablepos = pos(sqls, template, curs)
			loop
	
			ds_calc_subledger_depr.modify("Datawindow.Table.Updatetable='"+replacestring+"'")
	
			//Prepare SQL for Retrieve
	 
			sqls = 'datawindow.table.select = "' + sqls  +  '"'
			ret = ds_calc_subledger_depr.Modify(sqls)
	
			if ret = "" then
	
				ds_calc_subledger_depr.settransobject(sqlca)
	
				num_rows = ds_calc_subledger_depr.retrieve(i_nvo_cpr_control.i_company)
				
				if num_rows = 0 then continue
	
				sub_type = ds_calc_subledger_depr.getitemnumber(1,'subledger_type_id') 
	
				items = ds_calc_subledger_depr.rowcount()
	
				for j = 1 to items 
					 new_row = ds_calc_subledger_depr.Insertrow(0)
	
					 ds_calc_subledger_depr.Setitem(new_row,'subledger_item_id', &
						 ds_calc_subledger_depr.getitemnumber(j,'subledger_item_id'))
	
					 ds_calc_subledger_depr.setitem(new_row,'asset_id', &
						 ds_calc_subledger_depr.getitemnumber(j,'asset_id'))
	
					 ds_calc_subledger_depr.setitem(new_row,'subledger_type_id', sub_type)
		
					 ds_calc_subledger_depr.setitem(new_row,'cpr_posting_mo_yr', new_month_date)
	
					 ds_calc_subledger_depr.setitem(new_row,'estimated_salvage', &
						 ds_calc_subledger_depr.getitemdecimal(j,'estimated_salvage'))

					 ds_calc_subledger_depr.setitem(new_row,'init_life', &
						 ds_calc_subledger_depr.getitemdecimal(j,'init_life'))
	
					 rem_life = ds_calc_subledger_depr.getitemdecimal(j,'remaining_life') 
	 
	 				 if rem_life > 0 then
						 rem_life = rem_life - 1
					 end if	
			
					 ds_calc_subledger_depr.setitem(new_row,'remaining_life', rem_life)
	
				next
	
	
			rc = ds_calc_subledger_depr.update()
	
			if rc > 0 then
				ds_calc_subledger_depr.Reset()
				ds_calc_subledger_depr.Resetupdate()
			else
				f_pp_msgs( "Error Adding month to the " + subledger + " Depreciation Sub Ledger: " + SQLCA.SQLErrText)
				rollback;
				
				num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
				i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
				goto next_company
	
			end if  
	
			end if 
		elseif depr = 3 then
			//Insert Rows into CPR_DEPR for new month
			//f_status_box("Information","Company_id: CPR Depr" + string(i_company) + " Month: " + string(i_month) )
			f_calc_cpr_depr_end_bals(i_nvo_cpr_control.i_company, curr_month_date)
			
			// PP-48182: copy all books if any book as $$ for plant or reserve
			insert into cpr_depr
			(asset_id, set_of_books_id, gl_posting_mo_yr, init_life, remaining_life, estimated_salvage, beg_asset_dollars,
			beg_reserve_month, acct_distrib, company_id, ytd_depr_expense, ytd_depr_exp_adjust,
						prior_ytd_depr_expense, prior_ytd_depr_exp_adjust, beg_reserve_year, mid_period_method, mid_period_conv,
						depr_group_id)
			(select a.asset_id, set_of_books_id, :new_month_date, init_life, decode(remaining_life,0,0,remaining_life - 1), 
			estimated_salvage, asset_dollars,
			depr_reserve, acct_distrib, a.company_id, ytd_depr_expense, ytd_depr_exp_adjust,
						prior_ytd_depr_expense, prior_ytd_depr_exp_adjust, beg_reserve_year, mid_period_method, mid_period_conv,
						a.depr_group_id
			from cpr_depr a, cpr_ledger l
			where a.gl_posting_mo_yr = :curr_month_date
			and exists (
				select 1 from cpr_depr x
				where x.asset_id = a.asset_id
				and x.gl_posting_mo_yr = a.gl_posting_mo_yr
				and NOT (asset_dollars = 0 and depr_reserve = 0)
				)						
			and a.company_id = :i_nvo_cpr_control.i_company
			and a.asset_id = l.asset_id
			and l.subledger_indicator = :subledger_id
			and not exists
				(select 1 from cpr_depr z
					where z.asset_id=a.asset_id
					and z.set_of_books_id=a.set_of_books_id
					and z.gl_posting_mo_yr=:new_month_date
				)
			)
			;
			
			if sqlca.sqlcode <> 0 then
					f_pp_msgs( "Error Adding New Month on CPR_DEPR!  SQL: " + sqlca.sqlerrtext)
					rollback using sqlca;
					
					num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
					i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
					
					DESTROY ds_calc_subledger_depr
					DESTROY ds_hide
					
					return false
			end if
	
			f_pp_msgs("    cpr_depr " + string(subledger_id) + " insert rowcount = "+ string(sqlca.sqlnrows ))
			
			f_calc_cpr_depr_end_bals(i_nvo_cpr_control.i_company, new_month_date)
		end if
	
		next

			//INSERT NEW ROWS INTO THE ARO_LIABILITY TABLE
			f_pp_msgs( "Adding New Month to ARO..."+ ' ' + string(now()))
			
			insert into aro_liability (aro_id, layer_id, MONTH_YR,BEG_LIABILITY, NEW_INCURRED, 
				SETTLED, ACCRETED, REVISED, gain_loss,input_gain_loss, END_LIABILITY,STATUS, ACCRETION_ADJUST,LIABILITY_ADJUST)
			(select a.aro_id, a.layer_id, :new_month_date, END_LIABILITY, 0, 
				0, 0, 0, 0,0,END_LIABILITY,9,0,0
			from aro_liability a
			where month_yr = :curr_month_date
			and aro_id in (select aro_id from aro
								where company_id = :i_nvo_cpr_control.i_company)
			)			;
			
			
			
			
			if sqlca.sqlcode <> 0 then
					f_pp_msgs( "Error Adding New Month on ARO_LIABILITY!  SQL: " + sqlca.sqlerrtext)
					rollback using sqlca;
					
					DESTROY ds_calc_subledger_depr
					DESTROY ds_hide
					
					num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
					i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
					
					return false
			end if
	
			f_pp_msgs("    aro_liability insert rowcount = "+ string(sqlca.sqlnrows ))
						
						
			//CALL FUNCTION FOR AUTO VINTAGE GROUPS
			if curr_month_mm = 12 then
				rc = f_auto_vintage_groups(curr_month_yyyy, i_nvo_cpr_control.i_company)
				if rc < 0 then
					f_pp_msgs( "Error Adding Auto Vintage Groups")
					
					DESTROY ds_calc_subledger_depr
					DESTROY ds_hide
					
					num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
					i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
					
					return false
				end if
			end if

			
		// commit all at once
	
	commit;
	
	f_pp_msgs( "Finished Adding New Month."+ ' ' + string(now()))
	
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
	
next_company:
//	ds_cpr_control_hide.Reset()

	ds_calc_subledger_depr.Reset()
	ds_hide.Reset()
	
next // company

commit;

DESTROY ds_calc_subledger_depr
DESTROY ds_hide

i_nvo_cpr_control.of_cleanUp(10, 'email cpr close: new month', 'New Month')

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppdepr_interface_custom_version nvo_ppdepr_interface_custom_version

choose case a_pbd_name
	case 'ppdepr_interface_custom.pbd'
		return nvo_ppdepr_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

