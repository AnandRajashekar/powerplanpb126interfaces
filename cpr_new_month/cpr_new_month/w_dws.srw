HA$PBExportHeader$w_dws.srw
forward
global type w_dws from window
end type
type dw_16 from datawindow within w_dws
end type
type dw_15 from datawindow within w_dws
end type
type dw_14 from datawindow within w_dws
end type
type dw_13 from datawindow within w_dws
end type
type dw_12 from datawindow within w_dws
end type
type dw_11 from datawindow within w_dws
end type
type dw_10 from datawindow within w_dws
end type
type dw_9 from datawindow within w_dws
end type
type dw_8 from datawindow within w_dws
end type
type dw_7 from datawindow within w_dws
end type
type dw_6 from datawindow within w_dws
end type
type dw_5 from datawindow within w_dws
end type
type dw_4 from datawindow within w_dws
end type
type dw_3 from datawindow within w_dws
end type
type dw_2 from datawindow within w_dws
end type
type dw_1 from datawindow within w_dws
end type
end forward

global type w_dws from window
integer width = 4754
integer height = 1980
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_16 dw_16
dw_15 dw_15
dw_14 dw_14
dw_13 dw_13
dw_12 dw_12
dw_11 dw_11
dw_10 dw_10
dw_9 dw_9
dw_8 dw_8
dw_7 dw_7
dw_6 dw_6
dw_5 dw_5
dw_4 dw_4
dw_3 dw_3
dw_2 dw_2
dw_1 dw_1
end type
global w_dws w_dws

on w_dws.create
this.dw_16=create dw_16
this.dw_15=create dw_15
this.dw_14=create dw_14
this.dw_13=create dw_13
this.dw_12=create dw_12
this.dw_11=create dw_11
this.dw_10=create dw_10
this.dw_9=create dw_9
this.dw_8=create dw_8
this.dw_7=create dw_7
this.dw_6=create dw_6
this.dw_5=create dw_5
this.dw_4=create dw_4
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_1=create dw_1
this.Control[]={this.dw_16,&
this.dw_15,&
this.dw_14,&
this.dw_13,&
this.dw_12,&
this.dw_11,&
this.dw_10,&
this.dw_9,&
this.dw_8,&
this.dw_7,&
this.dw_6,&
this.dw_5,&
this.dw_4,&
this.dw_3,&
this.dw_2,&
this.dw_1}
end on

on w_dws.destroy
destroy(this.dw_16)
destroy(this.dw_15)
destroy(this.dw_14)
destroy(this.dw_13)
destroy(this.dw_12)
destroy(this.dw_11)
destroy(this.dw_10)
destroy(this.dw_9)
destroy(this.dw_8)
destroy(this.dw_7)
destroy(this.dw_6)
destroy(this.dw_5)
destroy(this.dw_4)
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_1)
end on

type dw_16 from datawindow within w_dws
integer x = 3095
integer y = 1072
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_15 from datawindow within w_dws
integer x = 2359
integer y = 1072
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_14 from datawindow within w_dws
integer x = 1577
integer y = 1068
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_cpr_company"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_13 from datawindow within w_dws
integer x = 823
integer y = 1068
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_12 from datawindow within w_dws
integer x = 64
integer y = 1072
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_11 from datawindow within w_dws
integer x = 3113
integer y = 620
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_10 from datawindow within w_dws
integer x = 2350
integer y = 624
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_9 from datawindow within w_dws
integer x = 1605
integer y = 616
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_auto_vint_rates"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_8 from datawindow within w_dws
integer x = 823
integer y = 596
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_auto_vint_methods"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_7 from datawindow within w_dws
integer x = 46
integer y = 580
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_6 from datawindow within w_dws
integer x = 3689
integer y = 12
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_auto_vint_groups"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_5 from datawindow within w_dws
integer x = 2958
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_auto_vint_controls"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_4 from datawindow within w_dws
integer x = 2194
integer y = 8
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_acct_summary"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_3 from datawindow within w_dws
integer x = 1458
integer y = 8
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_calc_subledger_depr"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_2 from datawindow within w_dws
integer x = 731
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_depr_ledger_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_dws
integer x = 27
integer y = 16
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_temp_dynamic"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

