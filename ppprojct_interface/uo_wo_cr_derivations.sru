HA$PBExportHeader$uo_wo_cr_derivations.sru
$PBExportComments$v10.2.1.7 maint 5792,6508: Exclude OCR / Tax Only Flags
forward
global type uo_wo_cr_derivations from nonvisualobject
end type
end forward

global type uo_wo_cr_derivations from nonvisualobject
end type
global uo_wo_cr_derivations uo_wo_cr_derivations

type variables
// instance variables that provide feedback to the user/caller
string i_sqls
longlong i_sqlcode
string i_error
string i_error_location
string i_warnings[]
string i_empty_str_array[]
string i_custom_loc

// instance variables the user may specify that will affect processing
string i_derivation_message
string i_derivation_offset_message
longlong	 i_work_order_id
longlong i_revision
string i_cr_deriver_type

string i_acct_key_columns[], i_acct_key_concat_string, i_cdc_columns[]
string i_cv_ocr, i_cv_tax
longlong i_add_revision, i_cdrp_count
string i_debug

end variables

forward prototypes
public function string uf_get_cr_deriver_lookup (longlong a_work_order_id, string a_project_basis)
public function longlong uf_build_wo_cr_derivations (longlong a_work_order_id, string a_project_basis)
protected function integer uf_setup ()
protected function longlong uf_build_wo_cr_derivations (longlong a_work_order_id, longlong a_revision, string a_project_basis, string a_cr_deriver_type)
end prototypes

public function string uf_get_cr_deriver_lookup (longlong a_work_order_id, string a_project_basis);string to_ret, wo_num, cr_deriver_type, cr_deriver_string, gl_company_no
longlong string_co, string_wo, string_task, string_ect

// get the WO Number and CR derivation type
if lower(a_project_basis) = 'unit estimate' or &
	lower(a_project_basis) = 'dollar estimate' then
	
	select work_order_number, cr_deriver_type_estimate, gl_company_no
	into :wo_num, :cr_deriver_type, :gl_company_no
	from work_order_control woc, work_order_account woa, company c
	where woc.work_order_id = :a_work_order_id
	and woc.work_order_id = woa.work_order_id
	and c.company_id = woc.company_id
	;
	
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** In uf_get_cr_deriver_lookup - work_order_number = " + string(wo_num) )
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** In uf_get_cr_deriver_lookup - cr_deriver_type_estimate = " + string(cr_deriver_type) )
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** In uf_get_cr_deriver_lookup - gl_company_no = " + string(gl_company_no) )
	
	if sqlca.sqlcode <> 0 then 
		return 'ERROR - Getting cr_deriver_type(1): ' + sqlca.sqlerrtext
	end if
		
	
elseif lower(a_project_basis) = 'detail' then
	
	select work_order_number, cr_deriver_type_detail, gl_company_no
	into :wo_num, :cr_deriver_type, :gl_company_no
	from work_order_control woc, work_order_account woa, company c
	where woc.work_order_id = :a_work_order_id
	and woc.work_order_id = woa.work_order_id
	and c.company_id = woc.company_id
	;
	
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** In uf_get_cr_deriver_lookup - work_order_number = " + string(wo_num) )
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** In uf_get_cr_deriver_lookup - cr_deriver_type_estimate = " + string(cr_deriver_type) )
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** In uf_get_cr_deriver_lookup - gl_company_no = " + string(gl_company_no) )
	
	if sqlca.sqlcode <> 0 then 
		return 'ERROR - Getting cr_deriver_type(2): ' + sqlca.sqlerrtext
	end if

// Invalid Project Basis Argument Cases!!!
elseif isNull(a_project_basis) then
	return 'ERROR - Null Project Basis'
else
	return 'ERROR - Invalid Project Basis: ' + a_project_basis	
end if


select string_co, string_wo, string_task, string_ect
into :string_co, :string_wo, :string_task, :string_ect
from cr_deriver_wo_link_control
where cr_derivation_type = :cr_deriver_type
and lower(project_basis) = lower(:a_project_basis)
;

if sqlca.sqlcode <> 0 then 
	return 'ERROR - Select from cr_deriver_wo_link_control: ' + sqlca.sqlerrtext
end if

// Assume CO+WO is the order of the cr_deriver_control.string field
to_ret = ""

if string_co = 1 then
	to_ret += gl_company_no
end if

if string_wo = 1 then
	to_ret += wo_num 
end if

if len(to_ret) = 0 then
	return 'ERROR - Wildcard for searching cr_deriver_control is empty.  Be sure to include at least WO.'
end if

// let the calling function do this?
//// add a percent to make it wildcard
//to_ret += '%' 


return to_ret
end function

public function longlong uf_build_wo_cr_derivations (longlong a_work_order_id, string a_project_basis);string wo_num, cr_deriver_type_estimate, cr_deriver_type_detail, cr_deriver_type
longlong ret, estn


// store instance variables
i_work_order_id = a_work_order_id

// **********************************************************************
//
// validate the setup supports this...
//
// **********************************************************************
// Enforce WORK_ORDER_ID as a column on CR_DERIVER_CONTROL
select count(*)
into :ret
from all_tab_columns
where owner = 'PWRPLANT'
and table_name = 'CR_DERIVER_CONTROL'
and column_name = 'WORK_ORDER_ID';

if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:cr_deriver_control.work_order_id check"
	return -1
elseif isNull(ret) or ret = 0 then
	i_error 				= "CR_DERIVER_CONTROL must have column WORK_ORDER_ID"
	i_error_location 	= "uf_build_wo_cr_derivations:cr_deriver_control.work_order_id check"
	return -1
end if


// **********************************************************************
//
// call the custom function before processing
//
// **********************************************************************
i_custom_loc = 'begin'
ret = f_wo_cr_derivations_custom(this)
if ret < 0 then
	return ret
end if

// get the WO Number and CR derivation type
select work_order_number, cr_deriver_type_estimate, cr_deriver_type_detail
into :wo_num, :cr_deriver_type_estimate, :cr_deriver_type_detail
from work_order_control woc, work_order_account woa
where woc.work_order_id = :i_work_order_id
and woc.work_order_id = woa.work_order_id;

if i_debug = "YES" then f_pp_msgs("***DEBUG*** wo_num = "+ string(wo_num) )
if i_debug = "YES" then f_pp_msgs("***DEBUG*** cr_deriver_type_estimate = " + string(cr_deriver_type_estimate))
if i_debug = "YES" then f_pp_msgs("***DEBUG*** cr_deriver_type_detail = " + string(cr_deriver_type_detail) + '~r~n' )

if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:cr_deriver_type_estimate lookup"
	return -1
end if
	
if lower(a_project_basis) = 'unit estimate' then
	cr_deriver_type = cr_deriver_type_estimate
	
	// whoever created this UO may specify a revision, otherwise use max()
	if isNull(i_revision) or i_revision = 0 then  // RJO 
		// check wo_estimate instead of work_order_approval because an as-built (revision=1000) may exist
		select max(revision)
		into :i_revision
		from wo_estimate
		where work_order_id = :i_work_order_id;
		
		if i_debug = "YES" then f_pp_msgs("***DEBUG*** i_revision = " + string(i_revision) )
		
		if sqlca.sqlcode <> 0 then 
			i_sqlcode			= sqlca.sqlcode
			i_error 				= "SQL Error: " + sqlca.sqlerrtext
			i_error_location 	= "uf_build_wo_cr_derivations:unit_estimate.revision lookup"
			return -1
		end if
	end if

	//skip everything if there are no results MYUAN 8/12/2008 
	select count(*) into :estn
	from wo_estimate where
	work_order_id = :a_work_order_id
	and revision = :i_revision;
	
	if i_debug = "YES" then f_pp_msgs("***DEBUG***  wo_estimate count = " + string(estn) )
	
elseif lower(a_project_basis) = 'dollar estimate' then
	cr_deriver_type = cr_deriver_type_estimate
	
	// whoever created this UO may specify a revision, otherwise use max()
	if isNull(i_revision) or i_revision = 0 then  // RJO 
	
		select max(revision)
		into :i_revision
		from wo_est_monthly
		where work_order_id = :i_work_order_id;
		
		if i_debug = "YES" then f_pp_msgs("***DEBUG*** i_revision = " + string(i_revision) )
		
		if sqlca.sqlcode <> 0 then 
			i_sqlcode			= sqlca.sqlcode
			i_error 				= "SQL Error: " + sqlca.sqlerrtext
			i_error_location 	= "uf_build_wo_cr_derivations:dollar_estimate.revision lookup"
			return -1
		end if
	end if
	
	//skip everything if there are no results MYUAN 8/12/2008 
	select count(*) into :estn
	from wo_est_monthly where
	work_order_id = :a_work_order_id
	and revision = :i_revision;
	
	if i_debug = "YES" then f_pp_msgs("***DEBUG***  wo_estimate count = " + string(estn) )
	
elseif lower(a_project_basis) = 'detail' then
	cr_deriver_type = cr_deriver_type_detail
	estn = 1 // so the build function runs below
	
else
	// Invalid Project Basis Argument!!!
	i_error 				= "Project Basis Argument is null or invalid" 
	i_error_location 	= "uf_build_wo_cr_derivations:cr_deriver_type lookup"
	return -1
end if

if estn > 0 then
	// Build the cr_deriver_control records for derivation type.  Function will interatively call itself for the offset
	ret = uf_build_wo_cr_derivations(a_work_order_id, i_revision, a_project_basis, cr_deriver_type)
	
	if ret < 0 then
		return ret
	end if
end if

// call the custom function at the end
i_custom_loc = 'end'
ret = f_wo_cr_derivations_custom(this) 
	
if ret < 0 then
	return ret
else
	return 0
end if
end function

protected function integer uf_setup ();any temp_array[]
longlong i

//  CHECK THE DEBUG VARIABLE.
setnull(i_debug)
select upper(trim(control_value)) into :i_debug from cr_system_control
 where upper(trim(control_name)) = 'CR BUILD DERIVER - DEBUG';
 
If i_debug <> "YES" Then i_debug = "NO"

// Only retrieve this the first time
if upperbound(i_acct_key_columns) > 0 then return 0

// ### 6508: Exclude OCR / Tax Only: Default to yes if they controls aren't here...
i_cv_ocr = lower(trim(f_pp_system_control('WOEST-WO Deriver Exclude OCR')))
if isNull(i_cv_ocr) or i_cv_ocr <> 'no' then i_cv_ocr = 'yes'
i_cv_tax = lower(trim(f_pp_system_control('WOEST-WO Deriver Exclude Tax Only')))
if isNull(i_cv_tax) or i_cv_tax <> 'no' then i_cv_tax = 'yes'

if i_debug = "YES" then f_pp_msgs("***DEBUG*** WOEST-WO Deriver Exclude OCR = " + i_cv_ocr)
if i_debug = "YES" then f_pp_msgs("***DEBUG*** WOEST-WO Deriver Exclude Tax Only = " + i_cv_tax )

// build a SQL string of the CR AK elements
i_acct_key_concat_string = ''
f_get_column(temp_array, 'select upper(trim(description)) from cr_elements order by "ORDER"')
for i = 1 to upperbound(temp_array)
	i_acct_key_columns[i] = f_cr_clean_string(temp_array[i])
	i_acct_key_concat_string += i_acct_key_columns[i] + ', '
next 

// Get the columns for CR_DERIVER_CONTROL
f_get_column(temp_array, "select column_name from all_tab_columns where owner = 'PWRPLANT' and table_name = 'CR_DERIVER_CONTROL' and column_name not in ('SCO_BILLING_TYPE_ID','TIME_STAMP','USER_ID') order by column_id")
i_cdc_columns = temp_array

// Check for REVISION as a column on CR_DERIVER_CONTROL
select count(*)
into :i_add_revision
from all_tab_columns
where owner = 'PWRPLANT'
and table_name = 'CR_DERIVER_CONTROL'
and column_name = 'REVISION';

if i_debug = "YES" then f_pp_msgs("***DEBUG*** CR_DERIVER_CONTROL.REVISION = " + string(i_add_revision) )

select count(*)
into :i_cdrp_count
from cr_derivation_rollup_priority;

if i_debug = "YES" then f_pp_msgs("***DEBUG*** cr_derivation_rollup_priority count = " + string(i_add_revision)  + '~r~n' )

return 0
end function

protected function longlong uf_build_wo_cr_derivations (longlong a_work_order_id, longlong a_revision, string a_project_basis, string a_cr_deriver_type);boolean b_cur_rollup_priorities, b_cur_rollup_list, b_rollup_priority, b_offset, b_estimate
string wo_num, wo_dept, wo_fp, wo_co
string missing_rollup
string sqls_estimate_table, sqls, sqls_select, sqls_from, sqls_where, sqls_group, sqls_cr_der_wo_link
string cr_deriver_offset_type, acct_key_codes[], str_value, sqls_select_ak
longlong string_co, string_wo, string_task, string_ect, i, field_num, ret, inserted_rows

string before_string, cr_derivation_rollup, cr_derivation_rollup_default
longlong j

any task_array[]
longlong max_t, t

// **********************************************************************
//
//	SETUP
//
// **********************************************************************
uf_setup()

b_cur_rollup_priorities = false
b_cur_rollup_list = false
b_rollup_priority = false  // a variable to track whether we are building the standard CDC records/offsets or looking for any strings missing CR Derivation Rollups
if pos(upper(trim(a_cr_deriver_type)),'OFFSET') > 0 then
	b_offset = true
else 
	b_offset = false
end if

// clear certain instance variables
i_sqls 				= ""
i_sqlcode				= 0
i_error 				= ""
i_error_location 	= ""
i_warnings[] 		= i_empty_str_array[]

// store instance variables
i_work_order_id = a_work_order_id
i_revision = a_revision
i_cr_deriver_type = a_cr_deriver_type

select w.work_order_number, d.external_department_code, 
	(select fp.work_order_number from work_order_control fp where fp.work_order_id = w.funding_wo_id), 
	c.gl_company_no
into :wo_num, :wo_dept, :wo_fp, :wo_co
from work_order_control w, department d, company_setup c
where w.work_order_id = :i_work_order_id
and d.department_id = w.department_id
and c.company_id = w.company_id;

if i_debug = "YES" then f_pp_msgs("***DEBUG***  work_order_control.work_order_number = " + string(wo_num))
if i_debug = "YES" then f_pp_msgs("***DEBUG*** department.external_department_code = " + wo_dept )
if i_debug = "YES" then f_pp_msgs("***DEBUG*** work_order_control.work_order_number" + string(wo_fp) )
if i_debug = "YES" then f_pp_msgs("***DEBUG*** company_setup.gl_company_no" + wo_co  + '~r~n' )

if lower(a_project_basis) = 'unit estimate' then
	sqls_estimate_table = 'wo_estimate'
	b_estimate = true
	
elseif lower(a_project_basis) = 'dollar estimate' then
	sqls_estimate_table = 'wo_est_monthly'
	b_estimate = true
	
elseif lower(a_project_basis) = 'detail' then
	sqls_estimate_table = ''
	b_estimate = false
	
end if

// Delete derivations for this WO's derivation type 
delete from cr_deriver_control 
where "TYPE" = :a_cr_deriver_type
and work_order_id = :i_work_order_id;

if i_debug = "YES" then f_pp_msgs("***DEBUG*** delete from cr_deriver_control where 'TYPE' = :a_cr_deriver_type and work_order_id = " + string(i_work_order_id) + '~r~n')

if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:delete from cr_deriver_control"
	return -1
end if

sqls  = 'INSERT INTO cr_deriver_control (ID, "TYPE", "WORK_ORDER_ID", "STRING", '
sqls += i_acct_key_concat_string + '"PERCENT", DESCRIPTION'
if i_add_revision = 0 then
	sqls += ') '
else
	sqls += ', REVISION) '
end if

if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls + '~r~n' ) 

// build the SELECT part of the SQL statement that will insert the data into CR_DERIVER_CONTROL
sqls_select  = ' SELECT crdetail.nextval, "TYPE", "WORK_ORDER_ID", "STRING", ' + i_acct_key_concat_string + ' "PERCENT", DESCRIPTION '
if i_add_revision = 0 then
	sqls_select += ' FROM ( '
else
	sqls_select += ', ' + string(a_revision) + ' as REVISION FROM ( '
end if
sqls_select += '	 SELECT ~'' + a_cr_deriver_type + '~' as "TYPE", ' + string(i_work_order_id) + ' as "WORK_ORDER_ID", '

if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls_select + '~r~n' )

// build the FROM 
sqls_from    = ' FROM work_order_control '
if b_estimate then
	sqls_from += ', ' + sqls_estimate_table + ' '
end if

if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls_from + '~r~n' )

// build the WHERE
sqls_where   = ' WHERE work_order_control.work_order_id = ' + string(i_work_order_id)
if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls_where + '~r~n' )

if b_estimate then
	sqls_where += ' AND work_order_control.work_order_id = ' + sqls_estimate_table + '.work_order_id '
	sqls_where += ' AND ' + sqls_estimate_table + '.revision = ' + string(i_revision)
	
	// ### 11562: JAK: 2012-12-18:  if using dollar estimates, need to reference total and not amount.
	if  lower(a_project_basis) = 'unit estimate' then 
		sqls_where += " AND " + sqls_estimate_table + ".amount <> 0 "
	elseif  lower(a_project_basis) = 'dollar estimate' then
		sqls_where += " AND " + sqls_estimate_table + ".total <> 0 "
	end if
		
	// ### 6508: Exclude OCR / Tax Only
	if i_cv_ocr = 'yes' then sqls_where  += " and " + sqls_estimate_table + ".est_chg_type_id not in " + &
		"(select est_chg_type_id from estimate_charge_type where processing_type_id = 1) "
	if i_cv_tax = 'yes' then sqls_where  += " and " + sqls_estimate_table + ".est_chg_type_id not in " + &
		"(select est_chg_type_id from estimate_charge_type where processing_type_id = 5) "
		
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls_where + '~r~n' )
	
end if

// build the GROUP BY 
sqls_group   = ' GROUP BY '

if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls_group + '~r~n' )

// Check the CR_DERIVER_WO_LINK_CONTROL Parameters: WO, ECT, and TASK
// This will determine the pieces used to build the CR_DERIVER_CONTORL "STRING"
//
sqls_cr_der_wo_link  = " select string_co, string_wo, string_task, string_ect, offset_type, "
sqls_cr_der_wo_link += left(i_acct_key_concat_string, len(i_acct_key_concat_string) - 2) // removes the ', ' from the end of the sqls_acct_key string
sqls_cr_der_wo_link += " from CR_DERIVER_WO_LINK_CONTROL "
sqls_cr_der_wo_link += " where cr_derivation_type = '" + a_cr_deriver_type + "'"
sqls_cr_der_wo_link += " and lower(project_basis) = lower('" + a_project_basis + "')"

//i_sqls = sqls_cr_der_wo_link // save the SQL used

if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls_cr_der_wo_link + '~r~n' )

PREPARE SQLSA FROM :sqls_cr_der_wo_link ;
if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:PREPARE sqls_cr_der_wo_link CURSOR:SQL - " + sqls_cr_der_wo_link
	return -1
end if

DESCRIBE SQLSA INTO SQLDA ;
if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:DESCRIBE sqls_cr_der_wo_link CURSOR:SQL - " + sqls_cr_der_wo_link
	return -1
end if

DECLARE cur_cr_der_wo_link DYNAMIC CURSOR FOR SQLSA ;
if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:DECLARE sqls_cr_der_wo_link CURSOR:SQL - " + sqls_cr_der_wo_link
	return -1
end if

OPEN DYNAMIC cur_cr_der_wo_link USING DESCRIPTOR SQLDA ;
if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:OPEN sqls_cr_der_wo_link CURSOR:SQL - " + sqls_cr_der_wo_link
	return -1
end if

FETCH cur_cr_der_wo_link USING DESCRIPTOR SQLDA ;
if sqlca.sqlcode = -1 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:FETCH sqls_cr_der_wo_link CURSOR:SQL - " + sqls_cr_der_wo_link
	return -1
elseif sqlca.sqlcode = 100 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL fetch returned no rows: " + sqls_cr_der_wo_link
	i_error_location 	= "uf_build_wo_cr_derivations:FETCH sqls_cr_der_wo_link CURSOR:SQL - " + sqls_cr_der_wo_link
	return -1
end if


//
// If the FETCH is successful, the output descriptor array will contain returned 
// values from the first row of the result set.
//
// SQLDA.NumOutputs contains the number of output descriptors.
//
// The SQLDA.OutParmType array will contain NumOutput entries and each entry will contain
// an value of the enumerated datatype ParmType (such as TypeInteger!, or TypeString!).
//

// the first 5 values returned are fixed
string_co   				= GetDynamicNumber(SQLDA, 1)
string_wo   				= GetDynamicNumber(SQLDA, 2)
string_task 				= GetDynamicNumber(SQLDA, 3)
string_ect  				= GetDynamicNumber(SQLDA, 4)
cr_deriver_offset_type  = GetDynamicString(SQLDA, 5)

if i_debug = "YES" then f_pp_msgs("***DEBUG*** co = " + string(string_co) )
if i_debug = "YES" then f_pp_msgs("***DEBUG*** wo = " + string(string_wo) )
if i_debug = "YES" then f_pp_msgs("***DEBUG*** task" + string(string_task) )
if i_debug = "YES" then f_pp_msgs("***DEBUG*** ect" + string(string_ect ))
if i_debug = "YES" then f_pp_msgs("***DEBUG*** cr_deriver_offset_type" + cr_deriver_offset_type + '~r~n'  )

// values 6 through SQLDA.NumOutputs tell us how to handle each AK element
for i = 6 to SQLDA.NumOutputs
	acct_key_codes[i - 5] = GetDynamicString(SQLDA, i)
next 

CLOSE cur_cr_der_wo_link ;

// can't use task or ECT option without estimates
if not b_estimate then string_task = 0
if not b_estimate then string_ect = 0

// do not insert any CDC records for the offset type if:
//   (a) the type is null or 
//   (b) all strings are excluded
if string_co = 0 and string_wo = 0 and string_task = 0 and string_ect = 0 then return 0

//
// Now build the SQL that puts the CR_DERIVER_CONTORL."STRING" together
//  - Work Order is the only required piece; CO, ECT, and TASK are optional
//  - Order is always CO+WO+TASK+ECT
//

// company is optional - if selected, put gl_company_no 
// from company_setup in the front of the string
if string_co = 1 then
	sqls_select += "'" + wo_co + "' || " // should we RPAD this value???
	sqls_group  += "'" + wo_co + "' || " // should we RPAD this value???
end if

// this should always be true - errors will occur if not
if string_wo = 1 then
	// ### 5792:  BSB 20101104: Task only in the string is causing it to break.
	sqls_select += "'" + wo_num + "' || " // should we RPAD this value???
	sqls_group  += "'" + wo_num + "' || " // should we RPAD this value???
end if

// must be an estimate-based derivation to use JOB_TASK in the dervation STRING
// outer join and NVL the select because job_task_id is not a required field
if string_task = 1 then
	// ### BSB 200812
	// add job_task_list to get the external_job_task
	// ### 5792:  BSB 20101104: Task only in the string is causing it to break.
	sqls_select += "nvl(nvl(job_task_list.external_job_task, job_task.job_task_id), '') || "
	sqls_group  += "nvl(nvl(job_task_list.external_job_task, job_task.job_task_id), '') || "
	sqls_from   += ", job_task, job_task_list  "
	sqls_where  += " AND job_task.job_task_id (+) = " + sqls_estimate_table + ".job_task_id "
	sqls_where  += " AND job_task.work_order_id (+) = " + sqls_estimate_table + ".work_order_id "  
	sqls_where  += " AND job_task_list.job_task_id (+) = job_task.job_task_id "
	// ### END BSB
end if

// if using Estimate Charge Type, then use the new CR_DERIVATION_ROLLUPS
// must be an estimate-based derivation to use the ECT in the dervation STRING
if string_ect = 1 then
	sqls_select += " estimate_charge_type.cr_derivation_rollup || "
	sqls_group  += " estimate_charge_type.cr_derivation_rollup || "
	sqls_from   += ", estimate_charge_type "	
	sqls_where  += " AND estimate_charge_type.est_chg_type_id = " + sqls_estimate_table + ".est_chg_type_id "
	sqls_where  += " AND estimate_charge_type.cr_derivation_rollup is not null " 	
end if

// ### 5792:  BSB 20101104: Task only in the string is causing it to break.
// select and group will now end with '|| ' so get rid of last 3 chars
sqls_select = left(sqls_select, len(sqls_select) - 3)
sqls_group = left(sqls_group, len(sqls_group) - 3)

// end the STRING
sqls_select += ' as "STRING", ' 
sqls_group  += ', ' 

//
// Process the Accounting Key Values from CR_DERIVER_WO_LINK_CONTROL 
//
for i = 1 to upperbound(acct_key_codes)
		
	sqls_select_ak = ""
	field_num = 0
	
	// Parse the numeric piece of the AK reference out of the string
	// this prevents the need for listing multiple values in the case statements
	// and it allows for future flexibility to add more "extra fields"
	
	//MYUAN 20090701 Don't trim if it starts with a ', signifying hardcoding
	if left(acct_key_codes[i],1) <> "'" then
		if isNumber(mid(acct_key_codes[i], lastpos(acct_key_codes[i], '_')+1)) then
			
			// get the numeric part at the end of the string (last piece after the underscore)
			field_num = long(mid(acct_key_codes[i], lastpos(acct_key_codes[i], '_')+1))
			
			// trim the number off of the AK Code 
			acct_key_codes[i] = left(acct_key_codes[i], lastpos(acct_key_codes[i], '_') - 1)
		end if
	end if
			
 	choose case acct_key_codes[i]
			
		// work_order_control.work_order_number
		// Always one static value per WO, so put in a literal into the SQL
		case 'WO_NUM' 
			sqls_select_ak = "'" + wo_num + "'" 
			
		// External Department from work_order_control.department_id
		// Always one static value per WO, so put in a literal into the SQL
		case 'WO_DEPT' 
			
			sqls_select_ak = "'" + wo_dept + "'"
			
		// Funding Project Number
		// Always one static value per WO, so put in a literal into the SQL
		case 'FP_NUM' 
			sqls_select_ak = "'" + wo_fp + "'"
			
			
		// GL Company Number from company_setup
		// Always one static value per WO, so put in a literal into the SQL
		case 'GL_CO' 
			sqls_select_ak = "'" + wo_co + "'"
			
		// GL Account from work_order_account
		// Use the expenditure_type_id from the estimate to choose a GL Account off of work_order_acount
		//
		//    expenditure_type_id 		work_order_acount field
		// 	1	(Additions)					CWIP_GL_ACCOUNT
		// 	2	(Retirements)				check ECT.processing_type_id 
		//											if 2,6,7,8 then use 	SALVAGE_GL_ACCOUNT
		//											else use 				REMOVAL_GL_ACCOUNT
		// 	3	(Expense/Revenue)		EXPENSE_GL_ACCOUNT
		// 	4	(Jobbing)						JOBBING_GL_ACCOUNT
		//
		//      Other Accounts on work_order_acount
		//				UNITIZED_GL_ACCOUNT
		//				NON_UNITIZED_GL_ACCOUNT
		//				RETIREMENT_GL_ACCOUNT
		case 'EXP_TYPE'  
			// This only makes sense for Estimates
			if b_estimate then 
				sqls_select_ak = "gl_account_" + string(i) + ".external_account_code"
				
				sqls_from   += ", work_order_account"
				sqls_from   += ", gl_account gl_account_" + string(i)
				sqls_from   += ", estimate_charge_type estimate_charge_type_" + string(i)
				
				sqls_where  += " and work_order_control.work_order_id = work_order_account.work_order_id "
				sqls_where  += " and " + sqls_estimate_table + ".est_chg_type_id = estimate_charge_type_" + string(i) + ".est_chg_type_id "
				sqls_where  += " and gl_account_" + string(i) + ".gl_account_id = "
				sqls_where  += "    decode(" + sqls_estimate_table + ".expenditure_type_id, "
				sqls_where  += "           1, work_order_account.CWIP_GL_ACCOUNT, "
				sqls_where  += "           2, decode(estimate_charge_type_" + string(i) + ".processing_type_id, "
				sqls_where  += "           			2, work_order_account.SALVAGE_GL_ACCOUNT, "
				sqls_where  += "           			6, work_order_account.SALVAGE_GL_ACCOUNT, "
				sqls_where  += "           			7, work_order_account.SALVAGE_GL_ACCOUNT, "
				sqls_where  += "           			8, work_order_account.SALVAGE_GL_ACCOUNT, "
				sqls_where  += "           			work_order_account.REMOVAL_GL_ACCOUNT), "
				sqls_where  += "           3, work_order_account.EXPENSE_GL_ACCOUNT, "
				sqls_where  += "           4, work_order_account.JOBBING_GL_ACCOUNT, "
				sqls_where  += "           '*') "
							
			else
				sqls_select_ak = "'*'"
			end if
		
		// ### BSB
		// 200812.  Add it so the add and ret exp type uses work order account
		// for expense / jobbing use a wo_est_custom field
		case "EXP_TYPE_EST_FIELD"
			// This only makes sense for Estimates
			if b_estimate then 
				sqls_select_ak = "gl_account_" + string(i) + ".external_account_code"
				
				sqls_from   += ", work_order_account"
				sqls_from   += ", gl_account gl_account_" + string(i)
				sqls_from   += ", estimate_charge_type estimate_charge_type_" + string(i)
				
				sqls_where  += " and work_order_control.work_order_id = work_order_account.work_order_id "
				sqls_where  += " and " + sqls_estimate_table + ".est_chg_type_id = estimate_charge_type_" + string(i) + ".est_chg_type_id "
				sqls_where  += " and case when " + sqls_estimate_table + ".expenditure_type_id = 1 then to_char(gl_account_" + string(i) + ".gl_account_id) "
				sqls_where  += " 		when " + sqls_estimate_table + ".expenditure_type_id = 2 then to_char(gl_account_" + string(i) + ".gl_account_id) "
				sqls_where  += " 		else gl_account_" + string(i) + ".external_account_code end = "
				sqls_where  += "    decode(" + sqls_estimate_table + ".expenditure_type_id, "
				sqls_where  += "           1, to_char(work_order_account.CWIP_GL_ACCOUNT), "
				sqls_where  += "           2, decode(estimate_charge_type_" + string(i) + ".processing_type_id, "
				sqls_where  += "           			2, to_char(work_order_account.SALVAGE_GL_ACCOUNT), "
				sqls_where  += "           			6, to_char(work_order_account.SALVAGE_GL_ACCOUNT), "
				sqls_where  += "           			7, to_char(work_order_account.SALVAGE_GL_ACCOUNT), "
				sqls_where  += "           			8, to_char(work_order_account.SALVAGE_GL_ACCOUNT), "
				sqls_where  += "           			to_char(work_order_account.REMOVAL_GL_ACCOUNT)), "
				sqls_where  += "           3, nvl(wo_estimate.field_" + string(field_num) + ", '*'), "
				sqls_where  += "           4, nvl(wo_estimate.field_" + string(field_num) + ", '*'), "
				sqls_where  += "           '*') "
			else
				sqls_select_ak = "'*'"
			end if
		// ###
		// END BSB
				
		// job_task_id from estimte		
		case 'JOB_TASK' 
			
			// must be an estimate-based derivation to use JOB_TASK 		
			if b_estimate then
				
				sqls_select_ak = "nvl(job_task.job_task_id, '*')"
				
				// if task is not in the CR Derivation STRING, then add the job_task table to the SQL
				if string_task <> 1 then
					sqls_from   += ", job_task "
					sqls_where  += " AND job_task.job_task_id (+) = " + sqls_estimate_table + ".job_task_id "
					sqls_where  += " AND job_task.work_order_id (+) = " + sqls_estimate_table + ".work_order_id "  
				end if
				
			else
				sqls_select_ak = "'*'"
			end if
			
		// exteranl_job_task from estimte		
		case 'EXT_JOB_TASK' 
			// ### BSB 200812
			// add job_task_list to get the external_job_task
			// must be an estimate-based derivation to use JOB_TASK 		
			if b_estimate then
				sqls_select_ak = "nvl(job_task_list.external_job_task, '*')"
				
				// if task is not in the CR Derivation STRING, then add the job_task table to the SQL
				if string_task <> 1 then
					sqls_from   += ", job_task, job_task_list "
					sqls_where  += " AND job_task.job_task_id (+) = " + sqls_estimate_table + ".job_task_id "
					sqls_where  += " AND job_task.work_order_id (+) = " + sqls_estimate_table + ".work_order_id "  
					sqls_where  += " AND job_task_list.job_task_id (+) = job_task.job_task_id "
				end if
			// ### END BSB
				
			else
				sqls_select_ak = "'*'"
			end if
			
		case 'TASK_FIELD'
			sqls_select_ak = "'*'"
			
		// WO Class Code Value
		case 'CC'
			
			select value
			into :str_value
			from work_order_class_code
			where work_order_id = :i_work_order_id
			and class_code_id = :field_num;
						
			if isNull(str_value) then				
				sqls_select_ak = "'*'"
			else
				sqls_select_ak = "'" + str_value + "'"
			end if
			
		// WO External Class Code Value	
		case 'ECC'
			
			select b.ext_class_code_value
			into :str_value
			from work_order_class_code a, class_code_values b
			where a.work_order_id = :i_work_order_id
			and a.class_code_id = :field_num
			and a.class_code_id = b.class_code_id
			and a.value = b.value	;

			if isNull(str_value) then				
				sqls_select_ak = "'*'"
			else
				sqls_select_ak = "'" + str_value + "'"
			end if
			
		// New WO_ESTIMATE fields (field_1, field_2, etc)	
		// if they choose one of these new fields, 
		// then it may be a CR_ELEMENT field or a freeform field
		// either way, the value is already stored directly on WO_ESTIMATE
		case 'EST_FIELD'
			// only applicable for unit estimates
			if lower(a_project_basis) = 'unit estimate' then
				sqls_select_ak = "nvl(wo_estimate.field_" + string(field_num) + ", '*')"
			else
				sqls_select_ak = "'*'"
			end if
			
		case '*' // keep value from original transaction
			sqls_select_ak = "'*'"
			
		case else
			sqls_select_ak =  acct_key_codes[i] 
			
	end choose
	
	// check the SQL from the various choose case statement for errors
	if sqlca.sqlcode <> 0 then 
		i_sqlcode			= sqlca.sqlcode
		i_error 				= "SQL Error: " + sqlca.sqlerrtext
		i_error_location 	= "uf_build_wo_cr_derivations:AK Code (" + string(i) + "):" + acct_key_codes[i]
		return -1
	end if

	sqls_select += sqls_select_ak + ' as ' + i_acct_key_columns[i] + ', ' // add a comma
	sqls_group  += sqls_select_ak + ', ' // add a comma
	
next 

// figure out the percentage splits
// just use amount because that is what unitization will use
// percent field is just an easy way to build the numbers in the amount field
if lower(a_project_basis) = 'unit estimate' then
	
	if b_offset then 
		sqls_select += '-1*'
	end if
	
	// RJO  should we NVL this???
	sqls_select += 'round(sum( wo_estimate.amount / '
	sqls_select += 'to_number(('
	sqls_select += 'select sum(woe_sub.amount) '
	sqls_select += 'from wo_estimate woe_sub '
	
	if string_ect = 1 then
		sqls_select += ', estimate_charge_type ect_sub '
	end if		
	
	// must join on WO and revision
	sqls_select += 'where woe_sub.work_order_id = ' + string(i_work_order_id) + ' '
	sqls_select += 'and woe_sub.amount <> 0 '
	sqls_select += 'and woe_sub.revision = ' + string(i_revision) + ' '
	
	// ### 6508: Exclude OCR / Tax Only
	if i_cv_ocr = 'yes' then sqls_select  += " and woe_sub.est_chg_type_id not in " + &
		"(select est_chg_type_id from estimate_charge_type where processing_type_id = 1) "
	if i_cv_tax = 'yes' then sqls_select  += " and woe_sub.est_chg_type_id not in " + &
		"(select est_chg_type_id from estimate_charge_type where processing_type_id = 5) "
	
	// build the rest of the join between the subselect tables and the outer tables 
	// based on the options selected in CR_DERIVER_WO_LINK_CONTROL
	if string_ect = 1 then
		sqls_select += 'and woe_sub.est_chg_type_id = ect_sub.est_chg_type_id ' 
		if b_rollup_priority then

			sqls_select += 'and ect_sub.cr_derivation_rollup = cr_derivation_rollup_priority.default_rollup ' 

		else
			sqls_select += 'and ect_sub.cr_derivation_rollup = estimate_charge_type.cr_derivation_rollup ' 
		end if	
	end if
	
	if string_task = 1 then
		sqls_select += 'and ((woe_sub.job_task_id = wo_estimate.job_task_id) or ' 
		sqls_select += '     (woe_sub.job_task_id is null and wo_estimate.job_task_id is null))' 
	end if
	
	sqls_select += ')) '
	sqls_select += ' ), 8) as percent, ' 
elseif lower(a_project_basis) = 'dollar estimate' then
	
	if b_offset then 
		sqls_select += '-1*'
	end if
	
	// RJO  should we NVL this???
	sqls_select += 'round(sum( wo_est_monthly.total / '
	sqls_select += 'to_number(('
	sqls_select += 'select sum(woe_sub.total) '
	sqls_select += 'from wo_est_monthly woe_sub '
	
	if string_ect = 1 then
		sqls_select += ', estimate_charge_type ect_sub '
	end if		
	
	// must join on WO and revision
	sqls_select += 'where woe_sub.work_order_id = ' + string(i_work_order_id) + ' '
	sqls_select += 'and woe_sub.revision = ' + string(i_revision) + ' '
	
	// ### 6508: Exclude OCR / Tax Only
	if i_cv_ocr = 'yes' then sqls_select  += " and woe_sub.est_chg_type_id not in " + &
		"(select est_chg_type_id from estimate_charge_type where processing_type_id = 1) "
	if i_cv_tax = 'yes' then sqls_select  += " and woe_sub.est_chg_type_id not in " + &
		"(select est_chg_type_id from estimate_charge_type where processing_type_id = 5) "
	
	// build the rest of the join between the subselect tables and the outer tables 
	// based on the options selected in CR_DERIVER_WO_LINK_CONTROL
	if string_ect = 1 then
		sqls_select += 'and woe_sub.est_chg_type_id = ect_sub.est_chg_type_id ' 
		if b_rollup_priority then
			sqls_select += 'and ect_sub.cr_derivation_rollup = cr_derivation_rollup_priority.default_rollup ' 

		else
			sqls_select += 'and ect_sub.cr_derivation_rollup = estimate_charge_type.cr_derivation_rollup ' 
		end if	
	end if
	
	if string_task = 1 then
		sqls_select += 'and ((woe_sub.job_task_id = wo_est_monthly.job_task_id) or ' 
		sqls_select += '     (woe_sub.job_task_id is null and wo_est_monthly.job_task_id is null))' 
	end if
	
	sqls_select += ' and total <> 0)) '
	sqls_select += ' ), 8) as percent, ' 
else
	sqls_select += '1 as percent, ' 
end if

if b_offset then
	if isNull(i_derivation_offset_message) or len(trim(i_derivation_offset_message)) = 0 then
		sqls_select += "substr('Generated from WO " + a_project_basis + "',1,35) as description " // description	
	else
		sqls_select += "'" + left(i_derivation_offset_message,35) + "' as description "
	end if
else
	if isNull(i_derivation_message) or len(trim(i_derivation_message)) = 0 then
		sqls_select += "substr('Generated from WO " + a_project_basis + "',1,35) as description " // description	
	else
		sqls_select += "'" + left(i_derivation_message,35) + "' as description "
	end if
end if

// don't really need to group by this, but it prevents the dangling comma from causing a SQL error
sqls_group  += "'Generated' " 

sqls += sqls_select + sqls_from + sqls_where + sqls_group + ')'

i_sqls = sqls // save the SQL used

if i_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls + '~r~n' )

execute immediate :sqls;
if sqlca.sqlcode <> 0 then 
	i_sqlcode			= sqlca.sqlcode
	i_error 				= "SQL Error: " + sqlca.sqlerrtext
	i_error_location 	= "uf_build_wo_cr_derivations:insert into cr_deriver_control"
	return -1
else
	// check how many records we inserted - used to check if we need to go to the next CR Derivation Rollup Priority
	inserted_rows = sqlca.sqlnrows
	
	if i_debug = "YES" then f_pp_msgs("***DEBUG*** No. of rows Inserted = " + string (inserted_rows)  + '~r~n' )
	
	// call the custom function after each insert into CDC
	i_custom_loc = 'after insert'
	ret = f_wo_cr_derivations_custom(this)
	if ret < 0 then
		return ret
	end if
end if

// if ECT is in the STRING, we have records in cr_derivation_rollup_priority, and we created
//	some derivation rows, then we need to check the CR_Derivation_Rollup_Priority table 
// 	to see if any rollup values are missing
if string_ect = 1 and inserted_rows > 0 and i_cdrp_count > 0 then 
	if string_task = 1 then 
		// ### 37688: JAK: 2014-04-10:  The prior approach for job tasks did not work.  Will
		//	get a list of job tasks from job_task / job_task_list and loop and perform defaulting for each.
		sqls = "select distinct nvl(nvl(job_task_list.external_job_task, job_task.job_task_id), '') "
		sqls += sqls_from + sqls_where	// Note that string_ect can only be used from an estimate table so we can rely on sqls_estimate_table.  
															// The "DETAIL" basis could never be used for rollup priority
		
		if i_debug = "YES" then f_pp_msgs("***DEBUG*** string_task = 1 - " + sqls + '~r~n' )
		f_get_column(task_array, sqls)
		if upperbound(task_array) <= 0 or isnull(upperbound(task_array)) then task_array = {''}
	else
		task_array = {''}
	end if
	
	max_t = upperbound(task_array)

	for t = 1 to max_t
		// Build the before string
		before_string = ''
		if string_co = 1 then before_string += wo_co
		if string_wo = 1 then before_string += wo_num
		if string_task = 1 then 
			if isnull(task_array[t]) then task_array[t] = ''
			before_string += task_array[t]
		end if
		
		for i = 1 to 25 // maximum of 25 layers so we don't loop forever...
			select cr_derivation_rollup, default_rollup 
				into :cr_derivation_rollup, :cr_derivation_rollup_default
			from 
				(select cr_derivation_rollup, default_rollup, priority, rank() over (order by priority, cr_derivation_rollup) priority_rank from cr_derivation_rollup_priority
				where not exists (select 1 from cr_deriver_control where "TYPE" = :a_cr_deriver_type and work_order_id = :a_work_order_id and string like :before_string || cr_derivation_rollup)
				and exists (select 1 from cr_deriver_control where "TYPE" = :a_cr_deriver_type and work_order_id = :a_work_order_id and string like :before_string || default_rollup))
			where priority_rank = 1;
			
			if i_debug = "YES" then f_pp_msgs("***DEBUG*** Select  - select cr_derivation_rollup, default_rollup into :cr_derivation_rollup, :cr_derivation_rollup_default	from ")
			if i_debug = "YES" then f_pp_msgs("	(select cr_derivation_rollup, default_rollup, priority, rank() over (order by priority, cr_derivation_rollup) priority_rank from cr_derivation_rollup_priority " )
			if i_debug = "YES" then f_pp_msgs("	where not exists (select 1 from cr_deriver_control where 'TYPE' = :a_cr_deriver_type and work_order_id = :a_work_order_id and string like :before_string || cr_derivation_rollup) " )
			if i_debug = "YES" then f_pp_msgs("	and exists (select 1 from cr_deriver_control where 'TYPE' = :a_cr_deriver_type and work_order_id = :a_work_order_id and string like :before_string || default_rollup))	where priority_rank = 1; " + '~r~n' )
			
			if sqlca.sqlcode = 100 then 
				exit // no more defaults needed
			end if
			
			sqls = 'insert into cr_deriver_control ('
			
			for j = 1 to upperbound(i_cdc_columns)
				sqls += '"' + i_cdc_columns[j] + '", '
			next
			
			sqls = mid(sqls, 1, len(sqls) - 2)
			sqls += ') select '
			
			for j = 1 to upperbound(i_cdc_columns)
				choose case i_cdc_columns[j]
					case 'ID'
						sqls += 'crdetail.nextval, '
						
					case 'STRING' 
						sqls += 'replace("STRING",' + "'" + cr_derivation_rollup_default + "','" + cr_derivation_rollup + "'), "
						
					case 'DESCRIPTION'
						if b_offset then
							if isNull(i_derivation_offset_message) or len(trim(i_derivation_offset_message)) = 0 then
								sqls += "substr('Generated from CDRP " + a_project_basis + "',1,35) as description, " // description	
							else
								sqls += "'" + left(i_derivation_offset_message,35) + "' as description, "
							end if
						else
							if isNull(i_derivation_message) or len(trim(i_derivation_message)) = 0 then
								sqls += "substr('Generated from CDRP " + a_project_basis + "',1,35) as description, " // description	
							else
								sqls += "'" + left(i_derivation_message,35) + "' as description, "
							end if
						end if
					case else
						sqls += '"' + i_cdc_columns[j] + '", '
				end choose
			next
			
			sqls = mid(sqls, 1, len(sqls) - 2)
			sqls += 'from cr_deriver_control where "TYPE" = ' + "'" + a_cr_deriver_type + "' and work_order_id = " + string(a_work_order_id)
			sqls += ' and "STRING" like ' + "'" + before_string + cr_derivation_rollup_default + "'"
			
			if i_debug = "YES" then f_pp_msgs("***DEBUG*** Insert = " + sqls + '~r~n' )
			
			i_sqls = sqls
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then 
				i_sqlcode			= sqlca.sqlcode
				i_error 				= "SQL Error: " + sqlca.sqlerrtext
				i_error_location 	= "uf_build_wo_cr_derivations:insert into cr_deriver_control for CDRP: " + cr_derivation_rollup
				return -1
			end if
		next
	next 
end if

	
if not isnull(cr_deriver_offset_type ) and trim(cr_deriver_offset_type) <> '' then
	// Need to build the offset now.
	return uf_build_wo_cr_derivations(a_work_order_id, a_revision, a_project_basis, cr_deriver_offset_type)
else
	return 0
end if
end function

on uo_wo_cr_derivations.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wo_cr_derivations.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

