forward
global type uo_ps_interface_approvals from nonvisualobject
end type
type s_search from structure within uo_ps_interface_approvals
end type
type s_search_info from structure within uo_ps_interface_approvals
end type
type s_object_loc from structure within uo_ps_interface_approvals
end type
type s_device_mode from structure within uo_ps_interface_approvals
end type
type s_printer_defaults from structure within uo_ps_interface_approvals
end type
type s_printer_info2 from structure within uo_ps_interface_approvals
end type
type s_driver_info2 from structure within uo_ps_interface_approvals
end type
type s_printer_info2_add from structure within uo_ps_interface_approvals
end type
end forward

type s_search from structure
	longlong	page
	longlong	x
	longlong	y
	longlong	h
	longlong	w
end type

type s_search_info from structure
	longlong	count
	string		str
	longlong	option
	s_search		searchlist[200]
end type

type s_object_loc from structure
	string		name
	longlong	x
	longlong	y
	longlong	h
	longlong	w
	string		font
	string		weight
	string		height
	string		family
	string		pitch
end type

type s_device_mode from structure
	string		devicename
	unsignedinteger		specversion
	unsignedinteger		driverversion
	unsignedinteger		size
	unsignedinteger		driverextra
	unsignedlong		field
	unsignedinteger		orientation
	unsignedinteger		papersize
	unsignedinteger		paperlength
	unsignedinteger		paperwidth
	unsignedinteger		scale
	unsignedinteger		copies
	unsignedinteger		defaultsource
	unsignedinteger		printquality
	unsignedinteger		color
	unsignedinteger		duplex
	unsignedinteger		yresolution
	unsignedinteger		collate
	string		formname
	unsignedinteger		logpixels
	unsignedlong		bitsperpel
	unsignedlong		pelswidth
	unsignedlong		pelsheight
	unsignedlong		displayflags
	unsignedlong		displayfrequency
	unsignedlong		cmmethod
	unsignedlong		cmintent
	unsignedlong		mediatype
	unsignedlong		dithertype
	unsignedlong		reserved1
	unsignedlong		reserved2
end type

type s_printer_defaults from structure
	string		datatype
	s_device_mode		devmode
	longlong	access
end type

type s_printer_info2 from structure
	string		ServerName
	string		Printername
	string		sharename
	string		portname
	string		drivername
	string		comment
	string		location
	string		devmode
	string		sepfile
	string		printprocessor
	string		datatype
	string		parameters
	string		security
	string		attributes
	string		priority
	string		defaultpriority
	string		starttime
	string		uitltime
	string		status
	string		cjobs
	string		averageppm
end type

type s_driver_info2 from structure
	longlong	version
	string		name
	string		env
	string		driver_path
	string		data_file
	string		config_file
end type

type s_printer_info2_add from structure
	longlong	servername
	string		printername
	longlong	sharename
	string		portname
	string		drivername
	longlong	comment
	longlong	location
	longlong	devmode
	longlong	sepfile
	string		printprocessor
	string		datatype
	longlong	parameters
	longlong	security
	longlong	attributes
	longlong	priority
	longlong	defaultpriority
	longlong	starttime
	longlong	uitltime
	longlong	status
	longlong	cjobs
	longlong	averageppm
end type

global type uo_ps_interface_approvals from nonvisualobject
end type
global uo_ps_interface_approvals uo_ps_interface_approvals

type prototypes
Function long PSInterface(string dir,string infile,string outfile,long typex, long orientation,ref s_search_info search_info) Library "psconvrt.dll" alias for "PSInterface;Ansi"
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long CreateDCA( string lpszDriver, string lpszDevice, string lpszOutput, long data) Library "gdi32.dll" alias for "CreateDCA;Ansi"
Function long DeleteDC( long hdc) Library "gdi32.dll"
Function long GetDeviceCaps(long hdc,long x) Library "gdi32.dll"
Function long GetPrinterOrientation() Library "psconvrt.dll" 
Function long GetPrintDef(ref long orig ,ref long postscript) Library "psconvrt.dll" 
Function long GetPostscriptPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPostscriptPrinter;Ansi" 
Function long GetPrinterInfo( string printer,ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPrinterInfo;Ansi" 
Function long FindAddPostPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "FindAddPostPrinter;Ansi" 
Function long AddPrinterDriverA(long name, long Level, s_driver_info2 DriverInfo ) Library "winspool.drv" alias for "AddPrinterDriverA;Ansi"
Function long DeletePrinterDriverA(long name, long config, string driver) Library "winspool.drv" alias for "DeletePrinterDriverA;Ansi"
Function long AddPrinterA(long name, long Level, s_printer_info2_add DriverInfo ) Library "winspool.drv" alias for "AddPrinterA;Ansi"
Function long GetLastError() Library "kernel32.dll"
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long  GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
Function long  CopyFileA( string path,string  desination,long stat ) Library "KERNEL32.DLL" alias for "CopyFileA;Ansi"
Function long GetPrintDefPCL(ref long orig ,ref long postscript) Library "pclcnvrt.dll" 
Function long GetPCLPrinter(ref string name,ref string drv,ref string port) Library  "pclcnvrt.dll" alias for "GetPCLPrinter;Ansi" 
Function long PCLInterface(string dir,string infile,string outfile,long typex, long orientation,ref s_search_info search_info) Library "pclcnvrt.dll" alias for "PCLInterface;Ansi" 
Function long GetPrinterOrientationPCL() Library "pclcnvrt.dll" 
Function long SetDefPrinter(string pPrinterName)  Library "psconvrt.dll" alias for "SetDefPrinter;Ansi"
Function long GetDefPrinter(ref string pPrinterName,ref  long pdwBufferSize) Library "psconvrt.dll" alias for "GetDefPrinter;Ansi"
Function long GetPrinter(long hwnd,long ptype ,s_printer_info2  printer_info2 ,long num, ref long rnum) Library "KERNEL32.DLL" alias for "GetPrinter;Ansi"
Function long GetProcAddress(long hmod ,string procname) Library "KERNEL32.DLL" alias for "GetProcAddress;Ansi"
Function long Add_Printer(long apost, string ptr ,string drv) Library "ppaddptr.dll" alias for "Add_Printer;Ansi"
Function long GetShortPathA(string longpath ,ref string shortpath,long buflen) Library "KERNEL32.DLL" alias for "GetShortPathA;Ansi"
Function long GetLongPathNameA(string shortpath ,ref string longpath,long buflen) Library "KERNEL32.DLL" alias for "GetLongPathNameA;Ansi"

end prototypes

type variables


longlong SEARCH_TYPE = 2
longlong TEXT_TYPE = 1
longlong PDF_TYPE = 3
longlong PRN_TYPE = 6
longlong FIX_TYPE = 5
longlong LANDSCAPE = 1
longlong PORTRAIT = 0
longlong POST_TYPE =4
longlong MDI_TYPE = 7
longlong search_pos
string i_dir
boolean i_initialize_dll=false
boolean i_initialize_pcl=false
boolean i_initialize_ps=false
boolean i_initialize_print=false
boolean i_initialize_print_pcl = false
string i_print_file

longlong i_orig
longlong LOGPIXELSX   = 88
longlong LOGPIXELSY =  90 
s_mail_print mail_arg
boolean redraw = true
int i_default_printer_orig
string i_path
boolean i_add_printer = false
private: 
s_search_info search_info
s_object_loc i_object_loc[]
end variables

forward prototypes
public function integer uf_load_library (string a_lib)
public function string uf_get_temp_dir ()
public function string uf_get_default_printer ()
public function integer uf_get_objects (uo_ds_top a_dw)
public function longlong uf_get_printer (string a_printer, ref string a_name, ref string a_device, ref string a_port, uo_ds_top a_dw)
public function integer uf_print_file (uo_ds_top a_dw)
public function integer uf_selected_printer (longlong a_printer_type, uo_ds_top a_dw)
public function longlong uf_set_default_printer (string a_printer, uo_ds_top a_dw)
public function integer uf_mail_print (uo_ds_top a_dw, string a_msg, string a_to_user, string a_msg_subject, longlong a_company_id, string a_file_name)
public function integer uf_print_file (uo_ds_top a_dw, longlong a_company_id, string a_file_name, ref string a_pathname)
end prototypes

public function integer uf_load_library (string a_lib);string ini_file,str
string path, path_l
string err
string exe_path
longlong ret, status,len, mod
integer i
integer str_len
integer not_found = 1
environment env

getenvironment(env)

// 16 bit enironment not supported
if env.win16 = true then
	return 0
end if

ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")

path = ProfileString(ini_file, "Application", "PostscriptConverter", "")
i_dir = path
if len(path) > 0 then
	path = path + '\\'
	goto end_of_check
end if
not_found = 0

end_of_check:

if not_found = 0 then
   return -1
else
  
	setnull(str)
	mod =  GetModuleHandleA(str)
	if mod <> 0 then
		path_l = space(601)
		status = GetModuleFileNameA(mod,path_l,600)
		if status <> 0 then
			len = status
			for i = len to 1 step -1
				if mid(path_l,i,1) = '\' then
					exe_path = mid(path_l,1,i)
					exit
				end if
			next
		end if
	end if  
  
  str = path + a_lib
  
  if FileExists(exe_path + a_lib) = true then
	 str = a_lib
	  i_dir = exe_path
	  i_path = exe_path
  elseif FileExists(str) = true then
	  i_path = path
	 
  else
	  messagebox("Warning","Warning the DLL library " + str + " does not exist.",exclamation!,Ok!)
	  return -1
  end if
  
  ret = uo_winapi.uf_loadLibrarya(str) //###SRM 30149 loadlibrary is now separate from loadlibrarya
 
  choose case ret
        case 0
           	err = 'System was out of memory, executable file was corrupt, or relocations ' + &
                  'were invalid.' 
	     case 2
            err = 'File was not found.' 
        case 3	
            err = 'Path was not found.' 
        case 5	
            err = 'Attempt was made to dynamically link to a task, or there ' + &
                  'was a sharing or network-protection error.' 
        case 6	
            err = 'Library required separate data segments for each task.' 
        case 8	
            err = 'There was insufficient memory to start the application.' 
        case 10	
            err = 'Windows version was incorrect.' 
        case 11	
            err = 'Executable file was invalid. Either it was not a ' + &
                   'Windows application or there was an error in the .EXE image.' 
        case 12	
            err = 'Application was designed for a different operating system.' 
        case 13	
            err = 'Application was designed for MS-DOS 4.0.' 
        case 14	
            err = 'Type of executable file was unknown.' 
        case 15	
            err = 'Attempt was made to load a real-mode application (developed for an ' + &
                  'earlier version of Windows).' 
        case 16	
            err = 'Attempt was made to load a second instance of an executable file containing ' + &
                  'multiple data segments that were not marked read-only.' 
        case 19	
            err = 'Attempt was made to load a compressed executable file. The file must ' + &
                  'be decompressed before it can be loaded.' 
        case 20	
            err = 'Dynamic-link library (DLL) file was invalid. One of the DLLs required ' + &
                  'to run this application was corrupt.' 
        case 21	
            err = 'Application requires Microsoft Windows 32-bit extensions. '
        case is > 32 
			   i_initialize_dll=true
            return 1
        case is < 0
			   i_initialize_dll=true
            return 1 
        case else
            err = 'Unknown Error Code = ' + string(ret)
     end choose
MessageBox("Error",err)
return -1  
end if

end function

public function string uf_get_temp_dir ();string str
longlong status 
string ini_file
string temp
string str1
longlong len 

str = space(200)
str1 = space(600)
status = GetEnvironmentVariableA('TEMP',str,200)
if status = 0 then
	status = GetLastError()
	return ""
end if

ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")


temp = ProfileString(ini_file, "Application", "Temp", "None")
if temp <> 'None' then
	str = temp
end if
len = GetLongPathNameA(str,str1,600)
if len < 1 then
	return str
end if
return str1
end function

public function string uf_get_default_printer ();string ptr
longlong status
longlong hmod
string funct
long buflen = 200

hmod =  GetModuleHandleA('psconvrt.dll')
funct = 'GetDefPrinter'
if GetProcAddress(hmod,funct) = 0 then
	ptr = ProfileString("win.ini","windows","device","")
else
	ptr = space(buflen)
   status = GetDefPrinter(ptr,buflen);
end if


return ptr
end function

public function integer uf_get_objects (uo_ds_top a_dw);string objects
string name,edit_str
string object_type,str
longlong i,num,pos,start_pos
string object_list[]
longlong count = 0
s_object_loc object_null[]

i_object_loc =  object_null

str = a_dw.describe("DataWindow.syntax")

objects = a_dw.Object.DataWindow.Objects
pos = pos(objects,"~t")
do while pos > 1
	count++
	object_list[count] = mid(objects,start_pos,pos - start_pos)
	start_pos = pos + 1
	pos = pos(objects,"~t",start_pos)
loop

count++
object_list[count] = mid(objects,start_pos)
for i = 1 to count
	i_object_loc[i].x = long(a_dw.describe(object_list[i] + ".x"))
	i_object_loc[i].y = long(a_dw.describe(object_list[i] + ".x"))
	i_object_loc[i].w = long(a_dw.describe(object_list[i] + ".width"))
	i_object_loc[i].h = long(a_dw.describe(object_list[i] + ".height"))
	i_object_loc[i].name = object_list[i]
next

return upperbound(i_object_loc)

end function

public function longlong uf_get_printer (string a_printer, ref string a_name, ref string a_device, ref string a_port, uo_ds_top a_dw);

string ptr
longlong status
longlong hmod
string funct
environment env


getenvironment(env)
if env.PBMajorRevision	 > 8 then
	a_dw.Object.DataWindow.Printer = a_printer
end if


return 1
end function

public function integer uf_print_file (uo_ds_top a_dw);string temp_dir,str
string temp_file
longlong status, ret, i
long orig,postscript
string printer,drv,port
string ptr1,ptr2
string drv_name,name
s_object_loc i_object_loc_null[]
ptr2 = ''


// Check for a postscipt printer
ptr1 = uf_get_default_printer()
// Does the user have a pre-selected printer from the ini file
if uf_selected_printer(POST_TYPE,a_dw) = 1 then
	ptr2 = 'change'
// Is the standard PowerPlant printer defined	
elseif uf_get_printer('ppc_ps',name,drv_name,port,a_dw) = 1 then
	ptr2 = 'change'
else
	// Get the default printer and is it a postscript printer
	GetPrintDef(orig,postscript)
	if postscript <> 1 then
		printer = space(200)
		drv = space(200)
		port = space(100)
		// Find a postscript printer 
		if GetPostscriptPrinter(printer,drv,port) = -1 then
			string ptr_name
			ret = MessageBox("Information","Can't find a postscript printer, do you want to add a printer",Question!,YesNo! )
			if ret = 2 then return -1
			drv_name = 'ppc_drv5'
			ptr_name = 'ppc_ps'
			// Add the PowerPlant standard printer to the system
			if uf_load_library("ppaddptr.dll") <> 1 then return -1
			if Add_Printer(1, ptr_name, drv_name) = -1 then return -1
			
			ptr1 = ProfileString("win.ini","windows","device","")
			ptr2 = ptr_name + ',' + 'winprint'+ ',' + 'LPT1:'			
			uf_set_default_printer(ptr2,a_dw)
		else
			ptr1 = ProfileString("win.ini","windows","device","")
			ptr2 = printer + ',' +'winprint' + ',' + port
			uf_set_default_printer(ptr2,a_dw) 
		end if
	end if
end if
   


temp_dir = uf_get_temp_dir()
if temp_dir = '' then
  messagebox("Error","Can't find the temp directory.")
  return -1
end if
if mid(temp_dir, len(temp_dir), 1) <> '\' then
	temp_file = temp_dir + '\pstemp.ps'
else		
	temp_file = temp_dir + 'pstemp.ps'
end if
a_dw.object.datawindow.print.filename = temp_file
  
i_object_loc = i_object_loc_null

// Set Fonts
if upperbound(i_object_loc) < 1 then
  uf_get_objects(a_dw)
  
  for i = 1 to upperbound(i_object_loc)
	  i_object_loc[i].font = a_dw.describe(i_object_loc[i].name + ".Font.Face")
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='Helvetica'")   // 'Helvetica' 'MS Sans Serif'
	next
end if


i_orig = long(a_dw.Object.DataWindow.print.Orientation)	
if i_orig = 0 then
	status = GetPrinterOrientation()
	if status = -1 then
		i_orig = landscape
		str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	elseif status = 1 then
		i_orig = portrait
	else
		i_orig = landscape
	end if
elseif i_orig = 2 then
	i_orig = portrait
end if

a_dw.print()

for i = 1 to upperbound(i_object_loc)
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='" + i_object_loc[i].font + "'")
next

// set printer back to the default
if ptr2 <> '' and len(trim(ptr1)) > 0 then
	uf_set_default_printer(ptr1,a_dw)
end if

a_dw.object.datawindow.print.filename = ''
i_initialize_print = true
i_print_file = temp_file
setpointer(hourglass!)

return 1
end function

public function integer uf_selected_printer (longlong a_printer_type, uo_ds_top a_dw);string name,printer,drv,port
string ptr2,funct
string ini_file
longlong hmod
longlong num,rnum,status
s_printer_info2 printer_info2
ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")

if a_printer_type = POST_TYPE then
	printer = ProfileString(ini_file, "Application", "Postscript_Printer", "")
else
	printer = ProfileString(ini_file, "Application", "HP_Printer", "")
end if
if printer = "" then
	return -1
end if

return uf_get_printer(printer,name,drv,port,a_dw)

end function

public function longlong uf_set_default_printer (string a_printer, uo_ds_top a_dw);
string ptr
longlong status
longlong hmod
string funct
environment env

hmod =  GetModuleHandleA('psconvrt.dll')
funct = 'SetDefPrinter'
getenvironment(env)
if env.PBMajorRevision	 > 8 then
	a_dw.Object.DataWindow.Printer = ptr
end if

return status
end function

public function integer uf_mail_print (uo_ds_top a_dw, string a_msg, string a_to_user, string a_msg_subject, longlong a_company_id, string a_file_name);string pathname, from_user
longlong rtn

from_user = sqlca.logid

select mail_id into :from_user
from pp_security_users 
where lower(trim(users)) = lower(trim(:from_user)); 

if sqlca.sqlcode <> 0 then
	MessageBox ("f_send_mail ERROR", 'No user id exists for ' + from_user, Exclamation!)
	Return -1
end if 

if IsNull(from_user) then
	from_user = 'Pwrplant'
end if	

rtn = uf_print_file(a_dw, a_company_id, a_file_name, pathname)

if rtn = -1 then
	return -1
end if

g_msmail.sendfile(from_user, '', a_msg_subject, a_msg, a_to_user, '', pathname)

return 1
end function

public function integer uf_print_file (uo_ds_top a_dw, longlong a_company_id, string a_file_name, ref string a_pathname);string print_type, temp_file
longlong long_print_type

i_orig = long(a_dw.Object.DataWindow.print.Orientation)

if i_orig = 1 then
	i_orig = LANDSCAPE
else
	i_orig = portrait
end if

print_type = upper(f_pp_system_control_company('APPREMAIL- DW FILE TYPE', a_company_id))

if print_type = 'TEXT' then
	long_print_type = TEXT_TYPE
elseif print_type = 'MDI' then
	long_print_type = MDI_TYPE
else // includes 'PDF' value
	long_print_type = PDF_TYPE	
end if


if long_print_type = TEXT_TYPE  then
  if i_initialize_print=false then
	  uf_print_file(a_dw)
	end if 
	a_pathname = uf_get_temp_dir()
	if mid(a_pathname, len(a_pathname), 1) <> '\' then
		a_pathname += '\'+a_file_name+'.txt'
	else		
		a_pathname += a_file_name+'.txt'
	end if
	PSInterface(i_dir, i_print_file, a_pathname, TEXT_TYPE, i_orig, search_info)

elseif long_print_type = PDF_TYPE then 
  if i_initialize_print=false then
		 uf_print_file(a_dw)
	end if
	a_pathname = uf_get_temp_dir()
	if mid(a_pathname, len(a_pathname), 1) <> '\' then
		a_pathname += '\'+a_file_name+'.pdf'
	else		
		a_pathname += a_file_name+'.pdf'
	end if
	PSInterface(i_dir, i_print_file, a_pathname, PDF_TYPE, i_orig, search_info)
elseif mail_arg.print_type = MDI_TYPE then 
   a_pathname = uf_get_temp_dir() + '\report.mdi'
	a_dw.object.datawindow.print.filename = a_pathname
	a_dw.Object.DataWindow.Printer ="Microsoft Office Document Image Writer"
   a_dw.print()
	i_initialize_print=false
	temp_file = a_pathname
	i_print_file =temp_file
	a_dw.object.datawindow.printer =""
else
	MessageBox("Error","Unknown file type.")
	return -1
end if

return 1

end function

on uo_ps_interface_approvals.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ps_interface_approvals.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;if uf_load_library("psconvrt.dll") = 1 then
	i_initialize_ps = true
end if

if uf_load_library("pclcnvrt.dll") = 1 then
	i_initialize_pcl = true
end if

if i_initialize_pcl = true or i_initialize_ps = true then
	i_initialize_dll = true
end if
end event

event destructor;boolean status 
status = FileDelete ( i_print_file)
return
end event

