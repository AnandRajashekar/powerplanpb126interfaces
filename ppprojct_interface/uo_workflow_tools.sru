HA$PBExportHeader$uo_workflow_tools.sru
$PBExportComments$8922-Workflow authority limit variable issue
forward
global type uo_workflow_tools from nonvisualobject
end type
end forward

global type uo_workflow_tools from nonvisualobject
end type
global uo_workflow_tools uo_workflow_tools

type variables
longlong i_workflow_id = -1, i_company_id, i_use_limits, i_workflow_type_id, i_allow_cascading, i_subsystem_send_email
string i_subsystem, i_subsystem_filter, i_unreject_sql, i_send_sql, i_approve_sql, i_reject_sql, i_unsend_sql, &
	i_update_workflow_type_sql
string i_id_field1, i_id_field2, i_id_field3, i_id_field4, i_id_field5
string i_id_field_where_clause
decimal i_approval_amount
boolean i_sending = false, i_send_approves = false
boolean i_workflow_uo = false

// Messaging Instance variables
boolean i_messagebox_onerror = true
boolean i_statusbox_onerror = false
boolean i_status_box = true
boolean i_messagebox_confirm = true
boolean i_add_time_to_messages = false
boolean i_pp_msgs = false
boolean i_send_notification = true
boolean i_statusbox_warning = true

//String to store the error message so that it can be referenced from the calling object and displayed as desired
boolean i_store_error_string = false
string	i_error_message
end variables

forward prototypes
public function longlong uf_check_sql (string msg)
public function longlong uf_get_workflow (string a_subsystem, longlong a_company_id, string a_id_field1, string a_id_field2, string a_id_field3, string a_id_field4, string a_id_field5)
public function longlong uf_new_workflow (longlong a_workflow_type)
public function longlong uf_send_next_level ()
public function longlong uf_workflow_send ()
public function longlong uf_workflow_unreject ()
public function longlong uf_workflow_approve ()
public function longlong uf_user_approve (string a_users)
public function longlong uf_workflow_reject ()
public function longlong uf_user_reject (string a_users)
public function longlong uf_workflow_validate (uo_ds_top a_ds, longlong a_workflow_id, string a_task)
public function longlong uf_new_workflow_detail (longlong a_workflow_type)
public function longlong uf_default_users (longlong a_workflow_type)
public function longlong uf_workflow_unsend ()
public function longlong uf_delete_unnecessary_levels ()
public function longlong uf_call_custom_fxn (string a_call_name)
public subroutine uf_msg (string msg, string str_error)
public function longlong uf_user_approve (string a_users, string a_approving_user)
public function longlong uf_user_reject (string a_users, string a_approving_user)
public function boolean uf_check_level (longlong a_level)
public function longlong uf_user_approve (string a_users, string a_approving_user, string a_notes)
public function longlong uf_user_reject (string a_users, string a_approving_user, string a_notes)
public function longlong uf_user_notes (string a_users, string a_notes)
public function longlong uf_workflow_send_multi ()
end prototypes

public function longlong uf_check_sql (string msg);if sqlca.sqlcode < 0 then
	uf_msg('SQL Error: '+msg+"~r~n"+sqlca.sqlerrtext, 'E')
	rollback;
	return -1
end if

return 1
end function

public function longlong uf_get_workflow (string a_subsystem, longlong a_company_id, string a_id_field1, string a_id_field2, string a_id_field3, string a_id_field4, string a_id_field5);string sqls
any results[], null_any_arr[]
longlong cnt_missing_rules, cnt_workflow, rtn
uo_ds_top ds_new_workflow_type

//
// Set instance variables from arguments
//
i_subsystem = a_subsystem
i_company_id = a_company_id
if isnull(i_company_id) then i_company_id = -1
i_id_field1 = a_id_field1
i_id_field2 = a_id_field2
i_id_field3 = a_id_field3
i_id_field4 = a_id_field4
i_id_field5 = a_id_field5

//
// Set instance variables from the subsystem values
//
select workflow_type_filter, unreject_sql, send_sql, approve_sql, reject_sql, unsend_sql, update_workflow_type_sql, nvl(send_emails,1), nvl(cascade_approvals,0)
into :i_subsystem_filter, :i_unreject_sql, :i_send_sql, :i_approve_sql, :i_reject_sql, :i_unsend_sql, :i_update_workflow_type_sql, :i_subsystem_send_email, :i_allow_cascading
from workflow_subsystem
where lower(trim(subsystem)) = lower(trim(:i_subsystem));

if isnull(i_subsystem_filter) or trim(i_subsystem_filter) = "" then
	i_subsystem_filter = ""
else
	i_subsystem_filter = f_replace_string(i_subsystem_filter,"<<id_field1>>",i_id_field1,"all")
	i_subsystem_filter = f_replace_string(i_subsystem_filter,"<<id_field2>>",i_id_field2,"all")
	i_subsystem_filter = f_replace_string(i_subsystem_filter,"<<id_field3>>",i_id_field3,"all")
	i_subsystem_filter = f_replace_string(i_subsystem_filter,"<<id_field4>>",i_id_field4,"all")
	i_subsystem_filter = f_replace_string(i_subsystem_filter,"<<id_field5>>",i_id_field5,"all")
	i_subsystem_filter = f_replace_string(i_subsystem_filter,"<<co_id>>",string(i_company_id),"all")
end if
if isnull(i_unreject_sql) or trim(i_unreject_sql) = "" then
	i_unreject_sql = ""
else
	i_unreject_sql = f_replace_string(i_unreject_sql,"<<id_field1>>",i_id_field1,"all")
	i_unreject_sql = f_replace_string(i_unreject_sql,"<<id_field2>>",i_id_field2,"all")
	i_unreject_sql = f_replace_string(i_unreject_sql,"<<id_field3>>",i_id_field3,"all")
	i_unreject_sql = f_replace_string(i_unreject_sql,"<<id_field4>>",i_id_field4,"all")
	i_unreject_sql = f_replace_string(i_unreject_sql,"<<id_field5>>",i_id_field5,"all")
	i_unreject_sql = f_replace_string(i_unreject_sql,"<<co_id>>",string(i_company_id),"all")
end if
if isnull(i_send_sql) or trim(i_send_sql) = "" then
	i_send_sql = ""
else
	i_send_sql = f_replace_string(i_send_sql,"<<id_field1>>",i_id_field1,"all")
	i_send_sql = f_replace_string(i_send_sql,"<<id_field2>>",i_id_field2,"all")
	i_send_sql = f_replace_string(i_send_sql,"<<id_field3>>",i_id_field3,"all")
	i_send_sql = f_replace_string(i_send_sql,"<<id_field4>>",i_id_field4,"all")
	i_send_sql = f_replace_string(i_send_sql,"<<id_field5>>",i_id_field5,"all")
	i_send_sql = f_replace_string(i_send_sql,"<<co_id>>",string(i_company_id),"all")
end if
if isnull(i_approve_sql) or trim(i_approve_sql) = "" then
	i_approve_sql = ""
else
	i_approve_sql = f_replace_string(i_approve_sql,"<<id_field1>>",i_id_field1,"all")
	i_approve_sql = f_replace_string(i_approve_sql,"<<id_field2>>",i_id_field2,"all")
	i_approve_sql = f_replace_string(i_approve_sql,"<<id_field3>>",i_id_field3,"all")
	i_approve_sql = f_replace_string(i_approve_sql,"<<id_field4>>",i_id_field4,"all")
	i_approve_sql = f_replace_string(i_approve_sql,"<<id_field5>>",i_id_field5,"all")
	i_approve_sql = f_replace_string(i_approve_sql,"<<co_id>>",string(i_company_id),"all")
end if
if isnull(i_reject_sql) or trim(i_reject_sql) = "" then
	i_reject_sql = ""
else
	i_reject_sql = f_replace_string(i_reject_sql,"<<id_field1>>",i_id_field1,"all")
	i_reject_sql = f_replace_string(i_reject_sql,"<<id_field2>>",i_id_field2,"all")
	i_reject_sql = f_replace_string(i_reject_sql,"<<id_field3>>",i_id_field3,"all")
	i_reject_sql = f_replace_string(i_reject_sql,"<<id_field4>>",i_id_field4,"all")
	i_reject_sql = f_replace_string(i_reject_sql,"<<id_field5>>",i_id_field5,"all")
	i_reject_sql = f_replace_string(i_reject_sql,"<<co_id>>",string(i_company_id),"all")
end if
if isnull(i_unsend_sql) or trim(i_unsend_sql) = "" then
	i_unsend_sql = ""
else
	i_unsend_sql = f_replace_string(i_unsend_sql,"<<id_field1>>",i_id_field1,"all")
	i_unsend_sql = f_replace_string(i_unsend_sql,"<<id_field2>>",i_id_field2,"all")
	i_unsend_sql = f_replace_string(i_unsend_sql,"<<id_field3>>",i_id_field3,"all")
	i_unsend_sql = f_replace_string(i_unsend_sql,"<<id_field4>>",i_id_field4,"all")
	i_unsend_sql = f_replace_string(i_unsend_sql,"<<id_field5>>",i_id_field5,"all")
	i_unsend_sql = f_replace_string(i_unsend_sql,"<<co_id>>",string(i_company_id),"all")
end if
if isnull(i_update_workflow_type_sql) or trim(i_update_workflow_type_sql) = "" then
	i_update_workflow_type_sql = ""
else
	i_update_workflow_type_sql = f_replace_string(i_update_workflow_type_sql,"<<id_field1>>",i_id_field1,"all")
	i_update_workflow_type_sql = f_replace_string(i_update_workflow_type_sql,"<<id_field2>>",i_id_field2,"all")
	i_update_workflow_type_sql = f_replace_string(i_update_workflow_type_sql,"<<id_field3>>",i_id_field3,"all")
	i_update_workflow_type_sql = f_replace_string(i_update_workflow_type_sql,"<<id_field4>>",i_id_field4,"all")
	i_update_workflow_type_sql = f_replace_string(i_update_workflow_type_sql,"<<id_field5>>",i_id_field5,"all")
	i_update_workflow_type_sql = f_replace_string(i_update_workflow_type_sql,"<<co_id>>",string(i_company_id),"all")
end if

//
// Get the workflow_id according to the id_fields
//
if isnull(i_id_field1) or i_id_field1 = '' then
	i_id_field_where_clause = " and id_field1 is null "
else
	i_id_field_where_clause = " and id_field1 = '"+i_id_field1+"' "
end if
if isnull(i_id_field2) or i_id_field2 = '' then
	i_id_field_where_clause += " and id_field2 is null "
else
	i_id_field_where_clause += " and id_field2 = '"+i_id_field2+"' "
end if
if isnull(i_id_field3) or i_id_field3 = '' then
	i_id_field_where_clause += " and id_field3 is null "
else
	i_id_field_where_clause += " and id_field3 = '"+i_id_field3+"' "
end if
if isnull(i_id_field4) or i_id_field4 = '' then
	i_id_field_where_clause += " and id_field4 is null "
else
	i_id_field_where_clause += " and id_field4 = '"+i_id_field4+"' "
end if
if isnull(i_id_field5) or i_id_field5 = '' then
	i_id_field_where_clause += " and id_field5 is null "
else
	i_id_field_where_clause += " and id_field5 = '"+i_id_field5+"' "
end if
i_id_field_where_clause += " and subsystem = '"+i_subsystem+"' "

results = null_any_arr
sqls = &
	"select max(workflow_id) " +&
	"from workflow " +&
	"where 1 = 1 " +&
	i_id_field_where_clause+" "
f_get_column(results,sqls)
i_workflow_id = longlong(results[1])

//
// Get the workflow type
//
if i_workflow_id > 0 then
	results = null_any_arr
	sqls = &
		"select max(workflow_type_id) " +&
		"from workflow " +&
		"where workflow_id = " + string(i_workflow_id) + " "
	f_get_column(results,sqls)
	i_workflow_type_id = longlong(results[1])
end if

//
// Insert any missing users/levels for the workflow, if any
//
if i_workflow_id > 0 and i_workflow_type_id > 0 then
	// ### CDM - Maint 8233 - flag to indicate visual workflow userobject is calling uo_workflow_tools (performance fix)
	if i_workflow_uo = false then
		rtn = uf_new_workflow_detail(i_workflow_type_id)
		if rtn = -1 then return rtn
	end if
end if

//
// Check if workflow doesn't exist
// Re-create the workflow, if not
//
select count(*)
into :cnt_workflow
from workflow
where workflow_id = :i_workflow_id;

// Check if the workflow levels don't exist or are stale
//select count(*)
//into :cnt_missing_rules
//from (
//	select tr.workflow_rule_id
//	from workflow w, workflow_type_rule tr
//	where w.workflow_type_id = tr.workflow_type_id
//	and w.workflow_id = :i_workflow_id
//	and w.approval_status_id in (1,7) /*initiated, unrejected*/
//	minus
//	select wd.workflow_rule_id
//	from workflow w, workflow_detail wd
//	where w.workflow_id = wd.workflow_id
//	and w.workflow_id = :i_workflow_id
//	);

if cnt_missing_rules > 0 or cnt_workflow = 0 then
	if i_workflow_uo = false and (isnull(i_workflow_type_id) or i_workflow_type_id = 0) then
		ds_new_workflow_type = create uo_ds_top
		ds_new_workflow_type.dataobject = "dw_workflow_type_active"
		ds_new_workflow_type.settransobject(sqlca)
		sqls = ds_new_workflow_type.getsqlselect()
		sqls += " and instr(nvl(subsystem,'"+i_subsystem+"'),'"+i_subsystem+"') > 0 "  //Only those for the applicable subsystem
		sqls += " "+i_subsystem_filter+" "  //Use subsystem sql_where to filter out workflow types
		ds_new_workflow_type.setsqlselect(sqls)
		ds_new_workflow_type.retrieve()
		if ds_new_workflow_type.rowcount() = 1 then
			i_workflow_type_id = ds_new_workflow_type.getitemnumber(1,"workflow_type_id")
		end if
		destroy ds_new_workflow_type
		if isnull(i_workflow_type_id) or i_workflow_type_id = 0 then
			return -1
		end if
	end if
	
	rtn = uf_new_workflow(i_workflow_type_id)
	if rtn = -1 then return rtn
	
	results = null_any_arr
	sqls = &
		"select max(workflow_id) " +&
		"from workflow " +&
		"where 1 = 1 " +&
		i_id_field_where_clause+" "
	f_get_column(results,sqls)
	i_workflow_id = longlong(results[1])
end if

//
// Set some more variables
//
select use_limits, approval_amount
into :i_use_limits, :i_approval_amount
from workflow
where workflow_id = :i_workflow_id;

return 1
end function

public function longlong uf_new_workflow (longlong a_workflow_type);string workflow_desc, sql_approval_amount, ret, sqls, rule_desc, rule_sql, temp_sql_approval_amount
longlong use_limits, i, rule_id, num_approvers, num_required, rule_order, temp_workflow_id, rtn
decimal approval_amount, authority_limit, total_authority_to_approve
boolean level_approves
uo_ds_top ds_approval_amount, ds_approval_rules

if isnull(a_workflow_type) then a_workflow_type = 0
//
// Make sure the object has been initialized
//
if isnull(i_id_field_where_clause) or i_id_field_where_clause = "" then
	uf_msg('Send for Approval: You must first call "uf_get_workflow" before you can call "uf_new_workflow."', 'E')
	return -1
end if

//
// Get the workflow type information
//
if a_workflow_type = 0 then
	setnull(workflow_desc)
	use_limits = 1
	setnull(sql_approval_amount)
else
	select description, use_limits, sql_approval_amount
	into :workflow_desc, :use_limits, :sql_approval_amount
	from workflow_type
	where workflow_type_id = :a_workflow_type;
end if

if trim(sql_approval_amount) <> "" and not isnull(sql_approval_amount) then
	temp_sql_approval_amount = sql_approval_amount
	if isnull(i_id_field1) or trim(i_id_field1) = "" then
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field1>>","0","all")
	else
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field1>>",i_id_field1,"all")
	end if
	if isnull(i_id_field2) or trim(i_id_field2) = "" then
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field2>>","0","all")
	else
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field2>>",i_id_field2,"all")
	end if
	if isnull(i_id_field3) or trim(i_id_field3) = "" then
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field3>>","0","all")
	else
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field3>>",i_id_field3,"all")
	end if
	if isnull(i_id_field4) or trim(i_id_field4) = "" then
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field4>>","0","all")
	else
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field4>>",i_id_field4,"all")
	end if
	if isnull(i_id_field5) or trim(i_id_field5) = "" then
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field5>>","0","all")
	else
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<id_field5>>",i_id_field5,"all")
	end if
	if isnull(i_company_id) or i_company_id = -1 then
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<co_id>>","-1","all")
	else
		temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<co_id>>",string(i_company_id),"all")
	end if
	temp_sql_approval_amount = f_replace_string(temp_sql_approval_amount,"<<subsystem>>",i_subsystem,"all")
	ds_approval_amount = create uo_ds_top
	ret = f_create_dynamic_ds(ds_approval_amount,'grid',temp_sql_approval_amount,sqlca,true)
	if ret <> 'OK' then
		//f_status_box('Approvals',temp_sql_approval_amount)
		//f_status_box('Approvals',' ')
		//f_status_box('Approvals','Error retrieving approval amount')
		uf_msg(temp_sql_approval_amount, 'W')
		uf_msg(' ', 'W')
		uf_msg('Error retrieving approval amount', 'W')
		rollback;
		return -1
	end if
	if ds_approval_amount.rowcount() > 0 then
		approval_amount = ds_approval_amount.getitemdecimal(1,1)
	else
		//f_status_box('Approvals',temp_sql_approval_amount)
		//f_status_box('Approvals',' ')
		//f_status_box('Approvals','Could not retrieve an approval amount')
		uf_msg(temp_sql_approval_amount, 'W')
		uf_msg(' ', 'W')
		uf_msg('Could not retrieve an approval amount', 'W')
		rollback;
		return -1
	end if
	if isnull(approval_amount) then approval_amount = 0
	destroy ds_approval_amount
else
	approval_amount = 0
end if

//
// Delete the existing workflow 
//
sqls = &
	"delete from workflow_detail " +&
	"where workflow_id in ( " +&
	"	select workflow_id " +&
	"	from workflow " +&
	"	where 1 = 1 " +&
	"	"+i_id_field_where_clause+" " +&
	"	) "
execute immediate :sqls;

if uf_check_sql('Deleting from workflow_detail table (uo_workflow_tools.uf_new_workflow)') = -1 then return -1

sqls = &
	"delete from workflow " +&
	"where 1 = 1 " +&
	i_id_field_where_clause+" "
execute immediate :sqls;

if uf_check_sql('Deleting from workflow table (uo_workflow_tools.uf_new_workflow)') = -1 then return -1

// ### CDM - Maint 8637 - Do not reset i_workflow_id until successfully creating new workflow
temp_workflow_id = i_workflow_id
select pwrplant1.nextval //nvl(max(workflow_id),0) + 1
into :i_workflow_id
// ## Maint 6211 - 20101214 JRB - change to select from dual instead of workflow
//from workflow;
from dual;
// ## End Maint 6211

//
// Insert the new workflow
//
insert into workflow (
	workflow_id,
	workflow_type_id, workflow_type_desc, subsystem,
	id_field1, id_field2, id_field3, id_field4, id_field5,
	approval_amount,
	approval_status_id,
	use_limits, sql_approval_amount )
select :i_workflow_id,
	:a_workflow_type, :workflow_desc, :i_subsystem,
	:i_id_field1, :i_id_field2, :i_id_field3, :i_id_field4, :i_id_field5,
	:approval_amount,
	decode(:a_workflow_type,0,1,decode((select count(*) from workflow_type_rule where workflow_type_id = :a_workflow_type), 0, 6 /*approved - auto*/, 1/*initiated*/)) approval_status_id,
	:use_limits, :sql_approval_amount
from dual;

if uf_check_sql('Inserting into workflow table (uo_workflow_tools.uf_new_workflow)') = -1 then
	// ### CDM - Maint 8637 - reset i_workflow_id if necessary
	i_workflow_id = temp_workflow_id
	return -1
end if

// ### CDM - Maint 8233 - This gets called in uf_instanciate of uo_workflow
if i_workflow_uo = false then
	rtn = uf_new_workflow_detail(a_workflow_type)
	// ### CDM - Maint 8637 - check return code and reset i_workflow_id if necessary
	if rtn = -1 then
		i_workflow_id = temp_workflow_id
		return -1
	end if
end if

// ### CDM - Maint 8637 - Need to reset instance variables after successfully creating new workflow
i_workflow_type_id = a_workflow_type

////
//// Loop through rules and insert levels accordingly
////
//sqls = &
//	"select r.workflow_rule_id, r.description, nvl(r.authority_limit,0) authority_limit, " +&
//	"	r.num_approvers, nvl(r.num_required,1) num_required, r.sql, " +&
//	"	tr.rule_order " +&
//	"from workflow_rule r, workflow_type_rule tr " +&
//	"where r.workflow_rule_id = tr.workflow_rule_id " +&
//	"and tr.workflow_type_id = "+string(a_workflow_type)+" " +&
//	"order by tr.rule_order "
//ds_approval_rules = create uo_ds_top
//ret = f_create_dynamic_ds(ds_approval_rules,'grid',sqls,sqlca,true)
//if ret <> 'OK' then
//	f_status_box('Approvals',sqls)
//	f_status_box('Approvals',' ')
//	f_status_box('Approvals','Error retrieving approval rules')
//	rollback;
//	return -1
//end if
//
//for i = 1 to ds_approval_rules.rowcount()
//	setnull(rule_id)
//	setnull(rule_desc)
//	setnull(authority_limit)
//	setnull(num_approvers)
//	setnull(num_required)
//	setnull(rule_sql)
//	setnull(rule_order)
//	
//	rule_id = ds_approval_rules.getitemnumber(i,"workflow_rule_id")
//	rule_desc = ds_approval_rules.getitemstring(i,"description")
//	authority_limit = ds_approval_rules.getitemdecimal(i,"authority_limit")
//	num_approvers = ds_approval_rules.getitemnumber(i,"num_approvers")
//	num_required = ds_approval_rules.getitemnumber(i,"num_required")
//	rule_sql = ds_approval_rules.getitemstring(i,"sql")
//	rule_order = ds_approval_rules.getitemnumber(i,"rule_order")
//	
//	if num_approvers < 1 then
//		setnull(num_approvers)
//	end if
//	if num_approvers >= 1 and num_required >= 1 then
//		if num_required > num_approvers then
//			f_status_box('Approvals','Error initializing level "'+rule_desc+'."')
//			f_status_box('Approvals',' ')
//			f_status_box('Approvals',string(num_required)+' approvers are required, but this level is setup for only '+string(num_approvers)+' approvers.')
//			f_status_box('Approvals','Please check your setup for the "'+workflow_desc+'" workflow type.')
//			rollback;
//			return -1
//		end if
//	end if
//	
//	if use_limits = 1 then
//		//if authority_limit = 0 or approval_amount > authority_limit then
//			level_approves = true
//		//else
//		//	if total_authority_to_approve > approval_amount then
//		//		level_approves = false
//		//	else
//		//		level_approves = true
//		//	end if
//		//end if
//	else
//		level_approves = true
//		
//		setnull(authority_limit)
//		setnull(num_approvers)
//		setnull(num_required)
//	end if
//	
//	if level_approves then
//		insert into workflow_detail (
//			id,
//			workflow_id, workflow_rule_id, workflow_rule_desc,
//			rule_order, approval_status_id, authority_limit, required,
//			num_approvers, num_required, sql,
//			users )
//		select (select nvl(max(id),0) from workflow_detail where workflow_id = :i_workflow_id)+rownum,
//			:i_workflow_id, :rule_id, :rule_desc,
//			:rule_order, 1, :authority_limit, 1,
//			:num_approvers, :num_required, :rule_sql,
//			(	select a.users
//				from approval_auth_level a
//				where a.approval_type_id = :a_workflow_type
//				and a.auth_level = :rule_id
//				and a.default_user = pp.row_num
//			)
//		from (select rownum row_num from pp_table_years) pp
//		where rownum <= nvl(:num_approvers,1);
//		
//		if uf_check_sql('Inserting into workflow_detail table (uo_workflow_tools.uf_new_workflow)') = -1 then return -1
//		
//		if num_required > 0 then
//			if authority_limit > total_authority_to_approve then
//				total_authority_to_approve = authority_limit
//			end if
//		end if
//	end if
//	
//next

//commit;

return 1
end function

public function longlong uf_send_next_level ();longlong next_level, cnt_required, i, rtn
string sqls, ret, users, pending_notification_type, args[], user_name
boolean send_next_level = false
uo_ds_top ds_users
ds_users = create uo_ds_top

//
// Make sure the object has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Send for Approval: You must first call "uf_get_workflow" before you can call "uf_send_next_level."', 'E')
	return -1
end if

//
// Set some variables for sending emails
//
select pending_notification_type
into :pending_notification_type
from workflow_subsystem
where subsystem = :i_subsystem;
if not isnull(i_id_field1) and trim(i_id_field1) <> "" then args[1] = i_id_field1
if not isnull(i_id_field2) and trim(i_id_field2) <> "" then args[2] = i_id_field2
if not isnull(i_id_field3) and trim(i_id_field3) <> "" then args[3] = i_id_field3
if not isnull(i_id_field4) and trim(i_id_field4) <> "" then args[4] = i_id_field4
if not isnull(i_id_field5) and trim(i_id_field5) <> "" then args[5] = i_id_field5

//
// Initialize datastore
//
sqls = "select users from workflow_detail where 1 = 2 "
ret = f_create_dynamic_ds(ds_users, 'grid', sqls, sqlca, true)
if ret <> 'OK' then
	uf_msg('Send for Approval: Error creating datastore (uo_workflow_tools.uf_send_next_level):~r~n'+sqls, 'E')
	return -1
end if

do
	//
	// Look for the next level to send emails
	//
	setnull(next_level)
	select distinct workflow_rule_id
	into :next_level
	from workflow_detail
	where workflow_id = :i_workflow_id
	and rule_order = (
		select min(rule_order)
		from workflow_detail
		where nvl(approval_status_id,1) <> 3
		and workflow_id = :i_workflow_id
		);
	
	if next_level > 0 then
		//
		// Set statuses to pending approval
		//
		update workflow_detail
		set approval_status_id = 2 // pending approval
		where workflow_rule_id = :next_level
		and workflow_id = :i_workflow_id
		and nvl(approval_status_id,1) <> 3;
		
		if uf_check_sql('Sending for Approval workflow_detail table (uo_workflow_tools.uf_send_next_level)') = -1 then return -1
		
		//
		// Get the list of approvers to send emails to
		//
		sqls = &
			"select users " +&
			"from workflow_detail " +&
			"where workflow_rule_id = "+string(next_level)+" " +&
			"and workflow_id = "+string(i_workflow_id)+" " +&
			"and nvl(approval_status_id,1) = 2 " +&
			"and trim(users) is not null "
		rtn = ds_users.setsqlselect(sqls)
		rtn = ds_users.retrieve()
		
		//
		// Loop through users and send emails
		//
		for i = 1 to ds_users.rowcount()
			users = ds_users.getitemstring(i,"users")
			// ### CDM - Maint 8637 - Williams wants the ability to surpress the pending notification emails
			if i_send_notification and i_subsystem_send_email = 1 then
				rtn = f_wo_approval_notification(pending_notification_type, i_company_id, args[], users, ' ')
				
				if rtn = 1 then
					select nvl(trim(first_name || nvl2(trim(last_name),nvl2(trim(first_name),' ',''),'')  || last_name), users)
					into :user_name
					from pp_security_users
					where lower(trim(users)) = lower(trim(:users));
					
					//f_status_box("Pending Approval", "Pending notification sent to "+user_name+".")
					uf_msg('Pending notification sent to '+user_name+'.', 'I')
				end if
			end if
		next
		
		//
		// Check to see if the next level has any approvers
		//
		send_next_level = false
		send_next_level = uf_check_level(next_level)
		
		//
		// If no users are required at this level, then mark all as approved at this level and loop to look for the next level
		//
		if send_next_level then
			update workflow_detail
			set approval_status_id = 3
			where workflow_rule_id = :next_level
			and workflow_id = :i_workflow_id;
			
			if uf_check_sql('Approving workflow_detail table (uo_workflow_tools.uf_send_next_level)') = -1 then return -1
		end if
		
	elseif isnull(next_level) or next_level = 0 then
		//
		// No levels exist that aren't approved...mark the entire workflow as approved
		//
		if not i_sending then
			// Only approve if we are not still in the midst of sending for approval
			rtn = uf_workflow_approve()
			if rtn = -1 then
				return -1
			end if
		else
			// If we are still in the midst of sending for approval, queue this workflow to be approved once sending is complete
			i_send_approves = true
		end if
	end if
	
loop while send_next_level and next_level > 0

return 1
end function

public function longlong uf_workflow_send ();longlong cnt_able_to_send, rtn, i
string sqls_arr[], ret
uo_ds_top ds_workflow_detail
ds_workflow_detail = create uo_ds_top
datawindowchild dw_child

//
// Make sure the object has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Send for Approval: You must first call "uf_get_workflow" before you can call "uf_workflow_send."', 'E')
	return -1
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('send_before')
if rtn = -1 then
	return -1
end if

//
// Confirm with the user they want to send
//
if i_messagebox_onerror and i_messagebox_confirm then
	rtn = messagebox("Confirm Send for Approval", "Are you sure you want to send for approval?", question!, yesno!)
	if rtn <> 1 then
		return -1
	end if
end if

//
// Check that this workflow is in a status that it can be sent for approval
//
select count(*)
into :cnt_able_to_send
from workflow
where approval_status_id in (1,7) // initiated, un-rejected
and workflow_id = :i_workflow_id;

if cnt_able_to_send <> 1 then
	uf_msg('Send for Approval: This workflow is not in a status to be able to send for approval...cannot send.', 'E')
	return -1
end if

//
// Validate that the workflow is ready for sending
//
ds_workflow_detail.dataobject = 'dw_workflow_detail'
rtn = ds_workflow_detail.settransobject(sqlca)
rtn = ds_workflow_detail.retrieve(i_workflow_id)
// ### CDM - Maint 6125 - validate approver appears only once
rtn = ds_workflow_detail.getchild("users",dw_child)
rtn = dw_child.settransobject(sqlca)
rtn = dw_child.retrieve(i_company_id, i_workflow_type_id)
rtn = uf_workflow_validate(ds_workflow_detail, i_workflow_id, 'send')
if rtn = -1 then
	return -1
end if

//
// If this workflow uses the authority limits, then delete any levels that are unnecessary for approval
//
rtn = uf_delete_unnecessary_levels()
if rtn = -1 then
	return -1
end if

//
// Update the approval status on the workflow
//
update workflow
set approval_status_id = 2, // pending_approval
	date_sent = sysdate,
	user_sent = user
where workflow_id = :i_workflow_id;

if uf_check_sql('Sending for Approval workflow table (uo_workflow_tools.uf_workflow_send)') = -1 then return -1

//
// Send email to the users on the next level
//
i_sending = true
rtn = uf_send_next_level()
i_sending = false
if rtn = -1 then
	return -1
end if

// ### CDM - Maint 6734 - Move up in script to be able to leverage "user_sent" in notification emails
////
//// Update the approval status on the workflow
////
//update workflow
//set approval_status_id = 2, // pending_approval
//	date_sent = sysdate,
//	user_sent = user
//where workflow_id = :i_workflow_id;
//
//if uf_check_sql('Sending for Approval workflow table (uo_workflow_tools.uf_workflow_send)') = -1 then return -1

//
// Run the associated custom sql for status updates, etc.
//
if i_send_sql <> "" then
	rtn = f_parsestringintostringarray(i_send_sql, ';', sqls_arr)
	
	for i = 1 to upperbound(sqls_arr)
		if not isnull(sqls_arr[i]) and trim(sqls_arr[i]) <> "" then
			execute immediate :sqls_arr[i];
			
			if uf_check_sql('Send for Approval custom sql (uo_workflow_tools.uf_workflow_send):~r~n'+sqls_arr[i]) = -1 then return -1
		end if
	next
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('send_after')
if rtn = -1 then
	return -1
end if

//
// If no levels had any required approvers when sending for approval, this should automatically approve the workflow
//
if i_send_approves then
	i_send_approves = false
	rtn = uf_workflow_approve()
	if rtn = -1 then
		return -1
	end if
	uf_msg('Successfully approved.','I')
else
	uf_msg('Successfully sent for approval.','I')
end if

return 1

end function

public function longlong uf_workflow_unreject ();longlong cnt_rejected, rtn, i
string sqls_arr[]

//
// Make sure the objects has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Un-Reject: You must first call "uf_get_workflow" before you can call "uf_workflow_unreject."', 'E')
	return -1
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('unreject_before')
if rtn = -1 then
	return -1
end if

//
// Confirm with the user they want to send
//
if i_messagebox_onerror and i_messagebox_confirm then
	rtn = messagebox("Confirm Un-Reject", "Are you sure you want to reset this approval, allowing the workflow to start again from the beginning?", question!, yesno!)
	
	if rtn <> 1 then
		return -1
	end if
end if

//
// Check that this workflow is in a rejected status so that it can be un-rejected
//
select count(*)
into :cnt_rejected
from workflow
where approval_status_id = 4
and workflow_id = :i_workflow_id;

if cnt_rejected <> 1 then
	uf_msg('Un-Reject: This workflow is not in a "Rejected" status...cannot Un-Reject.', 'E')
	return -1
end if

//
// Update the approval status on the workflow
//
update workflow
set approval_status_id = 7, // unrejected
	date_sent = null,
	date_approved = null,
	user_sent = null
where workflow_id = :i_workflow_id;

if uf_check_sql('Un-Rejecting workflow table (uo_budget_tools.uf_workflow_unreject)') = -1 then return -1

update workflow_detail
set date_approved = null,
	user_approved = null,
	approval_status_id = 1, // initiated
	notes = null
where workflow_id = :i_workflow_id;

if uf_check_sql('Un-Rejecting workflow_detail table (uo_budget_tools.uf_workflow_unreject)') = -1 then return -1

//
// Run the associated custom sql for status updates, etc.
//
if i_unreject_sql <> "" then
	rtn = f_parsestringintostringarray(i_unreject_sql, ';', sqls_arr)
	
	for i = 1 to upperbound(sqls_arr)
		if not isnull(sqls_arr[i]) and trim(sqls_arr[i]) <> "" then
			execute immediate :sqls_arr[i];
			
			if uf_check_sql('Un-Rejecting custom sql (uo_workflow_tools.uf_workflow_unreject):~r~n'+sqls_arr[i]) = -1 then return -1
		end if
	next
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('unreject_after')
if rtn = -1 then
	return -1
end if

return 1
end function

public function longlong uf_workflow_approve ();longlong cnt_pending, rtn, i
string sqls_arr[]

//
// Make sure the objects has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Approve: You must first call "uf_get_workflow" before you can call "uf_workflow_approve."', 'E')
	return -1
end if

//
// Check that this workflow is in a pending approval status so that it can be approved
//
select count(*)
into :cnt_pending
from workflow
where approval_status_id = 2
and workflow_id = :i_workflow_id;

if cnt_pending <> 1 then
	uf_msg('Approve: This workflow is not in a "Pending Approval" status...cannot Approve.', 'E')
	return -1
end if

//
// Update the approval status on the workflow
//
update workflow_detail
set approval_status_id = 3 // approved
where workflow_id = :i_workflow_id;

if uf_check_sql('Approving workflow_detail table (uo_workflow_tools.uf_workflow_approve)') = -1 then return -1

update workflow
set approval_status_id = 3, // approved
	date_approved = sysdate
where workflow_id = :i_workflow_id;

if uf_check_sql('Approving workflow table (uo_workflow_tools.uf_workflow_approve)') = -1 then return -1

//
// Run the associated custom sql for status updates, etc.
//
if i_approve_sql <> "" then
	rtn = f_parsestringintostringarray(i_approve_sql, ';', sqls_arr)
	
	for i = 1 to upperbound(sqls_arr)
		if not isnull(sqls_arr[i]) and trim(sqls_arr[i]) <> "" then
			execute immediate :sqls_arr[i];
			
			if uf_check_sql('Approving custom sql (uo_workflow_tools.uf_workflow_approve):~r~n'+sqls_arr[i]) = -1 then return -1
		end if
	next
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('approved')
if rtn = -1 then
	return -1
end if

return 1
end function

public function longlong uf_user_approve (string a_users);longlong rtn
string a_approving_user

// ### CDM - Maint 29680 - change userid to logid
a_approving_user = sqlca.logid

rtn = uf_user_approve(a_users, a_approving_user)

return rtn

end function

public function longlong uf_workflow_reject ();longlong cnt_rejected, rtn, i
string sqls_arr[]

//
// Make sure the objects has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Reject: You must first call "uf_get_workflow" before you can call "uf_workflow_reject."', 'E')
	return -1
end if

//
// Make sure a user has rejected
//
select count(*)
into :cnt_rejected
from workflow_detail
where workflow_id = :i_workflow_id
and approval_status_id = 4;

if cnt_rejected <> 1 then
	uf_msg('Reject: No approvers have a "Rejected" status...cannot Reject.', 'E')
	return -1
end if

//
// Update the approval status on the workflow
//
update workflow_detail
set approval_status_id = 5 // inactive - doesn't really mean anything, but it takes it out of the approval queue
where workflow_id = :i_workflow_id
and approval_status_id = 2; // pending approval

if uf_check_sql('Rejecting workflow_detail table (uo_workflow_tools.uf_workflow_reject)') = -1 then return -1

update workflow
set approval_status_id = 4, // rejected
	date_approved = sysdate
where workflow_id = :i_workflow_id;

if uf_check_sql('Rejecting workflow table (uo_workflow_tools.uf_workflow_reject)') = -1 then return -1

//
// Run the associated custom sql for status updates, etc.
//
if i_reject_sql <> "" then
	rtn = f_parsestringintostringarray(i_reject_sql, ';', sqls_arr)
	
	for i = 1 to upperbound(sqls_arr)
		if not isnull(sqls_arr[i]) and trim(sqls_arr[i]) <> "" then
			execute immediate :sqls_arr[i];
			
			if uf_check_sql('Rejecting custom sql (uo_workflow_tools.uf_workflow_reject):~r~n'+sqls_arr[i]) = -1 then return -1
		end if
	next
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('rejected')
if rtn = -1 then
	return -1
end if

return 1
end function

public function longlong uf_user_reject (string a_users);longlong rtn
string a_approving_user

// ### CDM - Maint 29680 - change userid to logid
a_approving_user = sqlca.logid

rtn = uf_user_reject(a_users, a_approving_user)

return rtn

end function

public function longlong uf_workflow_validate (uo_ds_top a_ds, longlong a_workflow_id, string a_task);//***********************************************************************************
//
//		Function:				f_workflow_validate
//
//		Arguments:			a_dw			dw_workflow_detail - Use a datawindow so that we can prevent Update if necessary
//								a_wf_id		workflow_id - identifier for this workflow
//								a_task		"update" - we are validating before updating (e.g. if approval status is "Pending Approval")
//												"send for approval" - we are validating before sending for approval
//
//		Purpose:				Validate various conditions are met before proceeding
//									- validate at least one required approver has the necessary authority limit to approve
//
//
//***********************************************************************************


longlong i, use_limits, old_rule_id, rule_id, cnt, num_required, num_required_arr[], required, approval_status_id, j, k, check_wf_type, ret_val, group_approval
decimal approval_amount, authority_limit, temp_approval_amount
// ### CDM - Maint 8922 - change max_required_authority from decimal to double
double max_required_authority
boolean found, found_dup, found2
string users, rule_desc, rule_desc_arr[], sql_approval_amount, ret, approvers[], duplicates[], sqls
any results[]
uo_ds_top ds_approval_amount

// ### CDM - Maint 8637 - run all validations before returning
ret_val = 1

//
// Retrieve variables
//
select use_limits, approval_amount, approval_status_id, sql_approval_amount
into :use_limits, :approval_amount, :approval_status_id, :sql_approval_amount
from workflow
where workflow_id = :a_workflow_id;

//
// Check to see if this approval type is still valid
//
if approval_status_id = 2 or approval_status_id = 3 then
	// Already pending approval or approved, so cannot change approval type
else
	sqls = &
		"select count(*) cnt " +&
		"from workflow_type " +&
		"where active = 1 " +&
		"and instr(nvl(subsystem,'"+i_subsystem+"'),'"+i_subsystem+"') > 0 " +&
		"and workflow_type_id = "+string(i_workflow_type_id)+" " +&
		i_subsystem_filter
	cnt = f_get_column(results, sqls)
	
	if long(results[1]) < 1 then
		//f_status_box("Validating Approvals Workflow","The current approval type is not valid for this workflow. Please select a valid approval type.")
		//f_status_box("Validating Approvals Workflow","")
		uf_msg("The current approval type is not valid for this workflow. Please select a valid approval type.", 'W')
		uf_msg(" ", 'W')
		//f_status_box("Validating Approvals Workflow","Cannot "+a_task+".")
		// ### CDM - Maint 8233 - need to return -1 in order to stop send for approval
		//return -1
		// ### CDM - Maint 8637 - run all validations before returning
		ret_val = -1
	end if
end if

//
// Validate that the approval_amount is the latest and greatest value
//
if not isnull(sql_approval_amount) and trim(sql_approval_amount) <> "" then
	if approval_status_id = 2 or approval_status_id = 3 then
		// If pending approval or approved, do not allow approval amount to change
	else
		// Check to ensure the approval amount reflects the latest amount
		if isnull(i_id_field1) or trim(i_id_field1) = "" then
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field1>>","0","all")
		else
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field1>>",i_id_field1,"all")
		end if
		if isnull(i_id_field2) or trim(i_id_field2) = "" then
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field2>>","0","all")
		else
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field2>>",i_id_field2,"all")
		end if
		if isnull(i_id_field3) or trim(i_id_field3) = "" then
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field3>>","0","all")
		else
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field3>>",i_id_field3,"all")
		end if
		if isnull(i_id_field4) or trim(i_id_field4) = "" then
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field4>>","0","all")
		else
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field4>>",i_id_field4,"all")
		end if
		if isnull(i_id_field5) or trim(i_id_field5) = "" then
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field5>>","0","all")
		else
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<id_field5>>",i_id_field5,"all")
		end if
		if isnull(i_company_id) or i_company_id = -1 then
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<co_id>>","-1","all")
		else
			sql_approval_amount = f_replace_string(sql_approval_amount,"<<co_id>>",string(i_company_id),"all")
		end if
		sql_approval_amount = f_replace_string(sql_approval_amount,"<<subsystem>>",i_subsystem,"all")
		ds_approval_amount = create uo_ds_top
		ret = f_create_dynamic_ds(ds_approval_amount,'grid',sql_approval_amount,sqlca,true)
		if ds_approval_amount.rowcount() > 0 then
			temp_approval_amount = ds_approval_amount.getitemdecimal(1,1)
			if temp_approval_amount <> approval_amount then
				//f_status_box('Approvals','Approval amount has changed')
				//f_status_box('Approvals','Populating new approval amount')
				update workflow
				set approval_amount = :temp_approval_amount
				where workflow_id = :i_workflow_id;
				if sqlca.sqlcode < 0 then
					uf_msg('SQL Error: Error updating workflow.approval_amount - '+sqlca.sqlerrtext, 'E')
					rollback;
					return -1
				end if
				approval_amount = temp_approval_amount
				a_ds.retrieve(a_workflow_id)
			end if
		else
			//f_status_box("Validating Approvals Workflow",sql_approval_amount)
			//f_status_box("Validating Approvals Workflow","ERROR: Could not retrieve an approval amount.")
			//f_status_box("Validating Approvals Workflow","")
			uf_msg(sql_approval_amount, 'W')
			uf_msg("ERROR: Could not retrieve an approval amount.", 'W')
			uf_msg(" ", 'W')
			//f_status_box("Validating Approvals Workflow","Cannot "+a_task+".")
			//return -1
			// ### CDM - Maint 8637 - run all validations before returning
			ret_val = -1
		end if
		destroy ds_approval_amount
	end if
end if

//
// Determine the authority that is required for this workflow to be approved
//
// ### CDM - Maint 8637 - get the max_required_authority from the datawindow in case any users have custom authority limits
//select nvl(least(
//	min(case when (wfd.num_required > 0 or (wfd.users is not null and wfd.required = 1)) and wfd.authority_limit >= wf.approval_amount then wfd.authority_limit else 999999999999 end),
//	max(wfd.authority_limit)
//	),999999999999)
////select nvl(min(wfd.authority_limit),999999999999)
//into :max_required_authority
//from workflow wf, workflow_detail wfd
//where wf.workflow_id = wfd.workflow_id
////and (wfd.num_required > 0 or (wfd.users is not null and wfd.required = 1))
////and wfd.authority_limit >= wf.approval_amount
//and wf.workflow_id = :a_workflow_id;
if a_ds.rowcount() > 0 then
	max_required_authority = a_ds.getitemnumber(1,"max_required_authority")
else
	max_required_authority = 999999999999
end if

//
// Validate that there is at least one required approver at the max required authority
//   Only applies if "use_limits" is turned on
//
if use_limits = 1 then
	found = false
	for i = 1 to a_ds.rowcount()
		//if (a_ds.getitemnumber(i,"num_required") > 0 or (not isnull(a_ds.getitemstring(i,"users")) and a_ds.getitemnumber(i,"required") = 1)) and a_ds.getitemdecimal(i,"authority_limit") >= approval_amount then
		if not isnull(a_ds.getitemstring(i,"users")) and a_ds.getitemnumber(i,"required") = 1 and a_ds.getitemdecimal(i,"authority_limit") = max_required_authority then
			found = true
			exit
		end if
	next
	
	if not found then
		// ### CDM - Maint 8637 - format max_required_authority as dollars
		//f_status_box("Validating Approvals Workflow","At least one approver must be required with an authority limit of "+string(max_required_authority,'$#,##0')+".")
		//f_status_box("Validating Approvals Workflow","")
		uf_msg("At least one approver must be required with an authority limit of "+string(max_required_authority,'$#,##0')+".", 'W')
		uf_msg(" ", 'W')
		//f_status_box("Validating Approvals Workflow","Cannot "+a_task+".")
		//return -1
		// ### CDM - Maint 8637 - run all validations before returning
		ret_val = -1
	end if
end if

//
// Validate that the approval levels have the appropriate number of required approvers (Null levels can be allowed)
//
old_rule_id = 0
for i = 1 to a_ds.rowcount()
	// Get the current rule/level
	rule_id = a_ds.getitemnumber(i,"workflow_rule_id")
	rule_desc = a_ds.getitemstring(i,"workflow_rule_desc")
	// Check if we are starting a new level.  If so, restart the count of required users
	if rule_id <> old_rule_id then
		cnt = 0
		setnull(num_required)
		num_required = a_ds.getitemnumber(i,"num_required")
		if isnull(num_required) then num_required = 1
		setnull(group_approval)
		group_approval = a_ds.getitemnumber(i,"group_approval")
		if isnull(group_approval) then group_approval = 0
		
		// ### CDM - Maint 8922 - needs to be getitemdecimal instead of getitemnumber
		authority_limit = a_ds.getitemdecimal(i,"authority_limit")
		if authority_limit > max_required_authority then
			num_required = 0
		end if
	end if
	
	users = trim(a_ds.getitemstring(i,"users"))
	if isnull(users) then users = ""
	required = a_ds.getitemnumber(i,"required")
	
	if (group_approval = 0 and users <> "" and required = 1) or (group_approval = 1 and users <> "") then
		cnt++
	end if
	
	if i < a_ds.rowcount() then
		if rule_id <> a_ds.getitemnumber(i+1,"workflow_rule_id") then
			if cnt < num_required then
				rule_desc_arr[upperbound(rule_desc_arr)+1] = rule_desc
				num_required_arr[upperbound(num_required_arr)+1] = num_required
			end if
		end if
	else
		if cnt < num_required then
			rule_desc_arr[upperbound(rule_desc_arr)+1] = rule_desc
			num_required_arr[upperbound(num_required_arr)+1] = num_required
		end if
	end if
	old_rule_id = rule_id
next

if upperbound(rule_desc_arr) > 0 then
	//f_status_box("Validating Approvals Workflow","The required number of users at each level has not been met.")
	uf_msg("The required number of users at each level has not been met.", 'W')
	for i = 1 to upperbound(rule_desc_arr)
		//f_status_box("Validating Approvals Workflow","  "+rule_desc_arr[i]+" requires "+string(num_required_arr[i])+" approvers.")
		uf_msg("  "+rule_desc_arr[i]+" requires "+string(num_required_arr[i])+" approvers.", 'W')
	next
	//f_status_box("Validating Approvals Workflow","")
	uf_msg(" ", 'W')
	//f_status_box("Validating Approvals Workflow","Cannot "+a_task+".")
	//return -1
	// ### CDM - Maint 8637 - run all validations before returning
	ret_val = -1
end if

if i_allow_cascading = 0 then
	//
	// Validate that no approver is in the workflow more than once
	// ### CDM - Maint 6125 - validate approver appears only once
	//
	found_dup = false
	for i = 1 to a_ds.rowcount()
		found = false
		for j = 1 to upperbound(approvers)
			if a_ds.getitemstring(i,"users") = approvers[j] then
				found = true
				found_dup = true
				found2 = false
				for k = 1 to upperbound(duplicates)
					if a_ds.describe('Evaluate("lookupdisplay(users)",'+string(i)+')') = duplicates[k] then
						found2 = true
					end if
				next
				if not found2 then
					duplicates[upperbound(duplicates)+1] = a_ds.describe('Evaluate("lookupdisplay(users)",'+string(i)+')')
				end if
			end if
		next
		if not found then
			approvers[upperbound(approvers)+1] = a_ds.getitemstring(i,"users")
		end if
	next
	
	if found_dup = true then
		//f_status_box("Validating Approvals Workflow","The following users appear more than once in the workflow:")
		uf_msg("The following users appear more than once in the workflow:", 'W')
		for i = 1 to upperbound(duplicates)
			//f_status_box("Validating Approvals Workflow","  "+duplicates[i])
			uf_msg("  "+duplicates[i], 'W')
		next
		//f_status_box("Validating Approvals Workflow","Please correct, such that no user appears in the workflow more than once.")
		//f_status_box("Validating Approvals Workflow","")
		uf_msg("Please correct, such that no user appears in the workflow more than once.", 'W')
		uf_msg(" ", 'W')
		//f_status_box("Validating Approvals Workflow","Cannot "+a_task+".")
		//return -1
		// ### CDM - Maint 8637 - run all validations before returning
		ret_val = -1
	end if
end if

// ### CDM - Maint 8637 - run all validations before returning
if ret_val = -1 then
	//f_status_box("Validating Approvals Workflow","Cannot "+a_task+".")
	uf_msg("Cannot "+a_task+".", 'W')
	return -1
end if


return 1

end function

public function longlong uf_new_workflow_detail (longlong a_workflow_type);string sqls, ret, rule_desc, temp_sqls, type_rule_sqls, rule_sqls
longlong i, rule_id, rtn
uo_ds_top ds_rules

// ### CDM - Maint 8233 - update configuration on workflow header, if applicable.
update workflow
set (workflow_type_desc, use_limits, sql_approval_amount) = (
	select description, use_limits, sql_approval_amount
	from workflow_type
	where workflow_type_id = :a_workflow_type
	)
where workflow_id = :i_workflow_id
and approval_status_id in (1,7); /*Initiated, Unrejected*/

if uf_check_sql('Updating workflow table (uo_workflow_tools.uf_new_workflow_detail)') = -1 then return -1

////
//// Insert the users/levels for the workflow
////
//insert into workflow_detail (
//	id,
//	workflow_id, workflow_rule_id, workflow_rule_desc,
//	rule_order, approval_status_id, authority_limit, required,
//	num_approvers, num_required, sql )
//select (select nvl(max(id),0) from workflow_detail where workflow_id = a.workflow_id)+rownum,
//	a.workflow_id, a.workflow_rule_id, a.workflow_rule_desc,
//	a.rule_order, a.approval_status_id, a.authority_limit, a.required,
//	a.num_approvers, a.num_required, a.sql
//from (
//	select
//		w.workflow_id, r.workflow_rule_id, r.description workflow_rule_desc,
//		tr.rule_order, 1 approval_status_id, decode(w.use_limits,0,null,nvl(r.authority_limit,0)) authority_limit, 1 required,
//		decode(w.use_limits,0,null,r.num_approvers) num_approvers, decode(w.use_limits,0,null,nvl(r.num_required,1)) num_required, r.sql
//	from workflow w, workflow_type_rule tr, workflow_rule r, pp_table_years pp
//	where w.workflow_id = :i_workflow_id
//	and tr.workflow_type_id = :a_workflow_type
//	and tr.workflow_rule_id = r.workflow_rule_id
//	and pp.year between 2001 and 2000 + nvl(r.num_approvers,decode(nvl(r.num_required,1),0,1,nvl(r.num_required,1)))
//	order by tr.rule_order, pp.year
//	) a
//where not exists (
//	select 1 from workflow_detail b
//	where b.workflow_id = a.workflow_id
//	and b.workflow_rule_id = a.workflow_rule_id
//	)
//and exists (
//	select 1 from workflow b
//	where b.workflow_id = a.workflow_id
//	and b.approval_status_id in (1,7) /*Initiated, Unrejected*/
//	);

sqls = &
	"select workflow_type_id, workflow_rule_id, sql " +&
	"from workflow_type_rule " +&
	"where workflow_type_id = "+string(a_workflow_type)+" " +&
	"and trim(sql) is not null "
ds_rules = create uo_ds_top
ret = f_create_dynamic_ds(ds_rules,'grid',sqls,sqlca,true)
if ret <> 'OK' then
	uf_msg("Error creating datastore:~r~n"+sqls, 'E')
	return -1
end if

type_rule_sqls = &
"select * from workflow_type_rule where workflow_type_id = "+string(a_workflow_type)+" and trim(sql) is null~r~n"
for i = 1 to ds_rules.rowcount()
	setnull(rule_id)
	setnull(temp_sqls)
	rule_id = ds_rules.getitemnumber(i,"workflow_rule_id")
	temp_sqls = ds_rules.getitemstring(i,"sql")
	if isnull(i_id_field1) or trim(i_id_field1) = "" then
		temp_sqls = f_replace_string(temp_sqls,"<<id_field1>>","0","all")
	else
		temp_sqls = f_replace_string(temp_sqls,"<<id_field1>>",i_id_field1,"all")
	end if
	if isnull(i_id_field2) or trim(i_id_field2) = "" then
		temp_sqls = f_replace_string(temp_sqls,"<<id_field2>>","0","all")
	else
		temp_sqls = f_replace_string(temp_sqls,"<<id_field2>>",i_id_field2,"all")
	end if
	if isnull(i_id_field3) or trim(i_id_field3) = "" then
		temp_sqls = f_replace_string(temp_sqls,"<<id_field3>>","0","all")
	else
		temp_sqls = f_replace_string(temp_sqls,"<<id_field3>>",i_id_field3,"all")
	end if
	if isnull(i_id_field4) or trim(i_id_field4) = "" then
		temp_sqls = f_replace_string(temp_sqls,"<<id_field4>>","0","all")
	else
		temp_sqls = f_replace_string(temp_sqls,"<<id_field4>>",i_id_field4,"all")
	end if
	if isnull(i_id_field5) or trim(i_id_field5) = "" then
		temp_sqls = f_replace_string(temp_sqls,"<<id_field5>>","0","all")
	else
		temp_sqls = f_replace_string(temp_sqls,"<<id_field5>>",i_id_field5,"all")
	end if
	if isnull(i_company_id) or i_company_id = -1 then
		temp_sqls = f_replace_string(temp_sqls,"<<co_id>>","-1","all")
	else
		temp_sqls = f_replace_string(temp_sqls,"<<co_id>>",string(i_company_id),"all")
	end if
	
	type_rule_sqls += &
	"			union all~r~n"+&
	"			select * from workflow_type_rule where workflow_type_id = "+string(a_workflow_type)+" and workflow_rule_id = "+string(rule_id)+" and 1 = ("+temp_sqls+")~r~n"
next
destroy ds_rules

sqls = &
	"select tr.workflow_rule_id, r.sql~r~n" +&
	"from workflow_type_rule tr, workflow_rule r~r~n" +&
	"where tr.workflow_type_id = "+string(a_workflow_type)+"~r~n" +&
	"and tr.workflow_rule_id = r.workflow_rule_id~r~n" +&
	"and nvl(r.group_approval,0) = 1~r~n" +&
	"order by tr.rule_order~r~n"
ds_rules = create uo_ds_top
ret = f_create_dynamic_ds(ds_rules,'grid',sqls,sqlca,true)
if ret <> 'OK' then
	uf_msg("Error creating datastore:~r~n"+sqls, 'E')
	return -1
end if

rule_sqls = &
"select r.*, 0 group_count~r~n" +&
"			from workflow_type_rule tr, workflow_rule r~r~n" +&
"			where tr.workflow_type_id = "+string(a_workflow_type)+"~r~n" +&
"			and tr.workflow_rule_id = r.workflow_rule_id~r~n" +&
"			and nvl(r.group_approval,0) = 0~r~n"
for i = 1 to ds_rules.rowcount()
	setnull(rule_id)
	setnull(temp_sqls)
	rule_id = ds_rules.getitemnumber(i,"workflow_rule_id")
	temp_sqls = ds_rules.getitemstring(i,"sql")
	if isnull(temp_sqls) or temp_sqls = "" then
		temp_sqls = &
			"select count(1) cnt from approval_auth_level where approval_type_id = "+string(a_workflow_type)+" and auth_level = "+string(rule_id)+" and default_user > 0"
	else
		if isnull(i_id_field1) or trim(i_id_field1) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field1>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field1>>",i_id_field1,"all")
		end if
		if isnull(i_id_field2) or trim(i_id_field2) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field2>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field2>>",i_id_field2,"all")
		end if
		if isnull(i_id_field3) or trim(i_id_field3) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field3>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field3>>",i_id_field3,"all")
		end if
		if isnull(i_id_field4) or trim(i_id_field4) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field4>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field4>>",i_id_field4,"all")
		end if
		if isnull(i_id_field5) or trim(i_id_field5) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field5>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field5>>",i_id_field5,"all")
		end if
		if isnull(i_company_id) or i_company_id = -1 then
			temp_sqls = f_replace_string(temp_sqls,"<<co_id>>","-1","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<co_id>>",string(i_company_id),"all")
		end if
		temp_sqls = &
			"select count(1) cnt from ("+temp_sqls+") where default_user > 0"
	end if
	
	rule_sqls += &
	"			union all~r~n" +&
	"			select r.*, ("+temp_sqls+") group_count from workflow_rule r where r.workflow_rule_id = "+string(rule_id)+"~r~n"
next
destroy ds_rules


//
// Delete records where the existing workflow_rule_id is no longer valid for the workflow_type
//
sqls = &
"delete from workflow_detail wd~r~n" +&
"where wd.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"and not exists (~r~n" +&
"	select 1 from~r~n" +&
"		(	~r~n" +&
"			"+type_rule_sqls+" " +&
"		) tr~r~n" +&
"	where tr.workflow_rule_id = wd.workflow_rule_id~r~n" +&
"	)~r~n" +&
"and exists (~r~n" +&
"	select 1 from workflow b~r~n" +&
"	where b.workflow_id = wd.workflow_id~r~n" +&
"	and b.approval_status_id in (1,7)~r~n" /*Initiated, Unrejected*/ +&
"	) "

sqls = f_sql_add_hint(sqls, 'uo_workflow_tools.uf_new_detail_1')

execute immediate :sqls;

if uf_check_sql('Deleting from workflow_detail table (1) (uo_workflow_tools.uf_new_workflow_detail)') = -1 then return -1


//
// Insert records where the missing workflow_rule_id is valid for the workflow_type
//
sqls = &
"insert into workflow_detail (~r~n" +&
"	id,~r~n" +&
"	workflow_id, workflow_rule_id, workflow_rule_desc,~r~n" +&
"	rule_order, approval_status_id, authority_limit, required,~r~n" +&
"	num_approvers, num_required, group_approval, sql, sql_users )~r~n" +&
"select (select nvl(max(id),0) from workflow_detail where workflow_id = a.workflow_id)+rownum,~r~n" +&
"	a.workflow_id, a.workflow_rule_id, a.workflow_rule_desc,~r~n" +&
"	a.rule_order, a.approval_status_id, a.authority_limit, a.required,~r~n" +&
"	a.num_approvers, a.num_required, a.group_approval, a.sql, a.sql_users~r~n" +&
"from (~r~n" +&
"	select~r~n" +&
"		w.workflow_id, r.workflow_rule_id, r.description workflow_rule_desc,~r~n" +&
"		tr.rule_order, 1 approval_status_id, decode(w.use_limits,0,null,nvl(r.authority_limit,0)) authority_limit, 1 required,~r~n" +&
/* ### CDM - Maint 8233 - use limits or not should not affect how num_approvers or num_required options work*/&
"		case when r.group_count > 0 then r.group_count else r.num_approvers end num_approvers, nvl(r.num_required,1) num_required, r.group_approval, r.sql, r.sql_users~r~n" +&
"	from workflow w, pp_table_years pp,~r~n" +&
"		(	~r~n" +&
"			"+type_rule_sqls+" " +&
"		) tr,~r~n" +&
"		(	~r~n" +&
"			"+rule_sqls+"~r~n" +&
"		) r~r~n" +&
"	where w.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"	and tr.workflow_type_id = "+string(a_workflow_type)+"~r~n" +&
"	and tr.workflow_rule_id = r.workflow_rule_id~r~n" +&
"	and pp.year between 2001 and 2000 + case when r.group_count > 0 then r.group_count else nvl(r.num_approvers,decode(nvl(r.num_required,1),0,1,nvl(r.num_required,1))) end~r~n" +&
"	order by tr.rule_order, pp.year~r~n" +&
"	) a~r~n" +&
"where not exists (~r~n" +&
"	select 1 from workflow_detail b~r~n" +&
"	where b.workflow_id = a.workflow_id~r~n" +&
"	and b.workflow_rule_id = a.workflow_rule_id~r~n" +&
"	)~r~n" +&
"and exists (~r~n" +&
"	select 1 from workflow b~r~n" +&
"	where b.workflow_id = a.workflow_id~r~n" +&
"	and b.approval_status_id in (1,7)~r~n" /*Initiated, Unrejected*/ +&
"	) "

sqls = f_sql_add_hint(sqls, 'uo_workflow_tools.uf_new_detail_2')

execute immediate :sqls;

if uf_check_sql('Inserting into workflow_detail table (1) (uo_workflow_tools.uf_new_workflow_detail)') = -1 then return -1


//
// Backfill workflow_type & workflow_rule attributes onto records in workflow_detail
//
sqls = &
"update workflow_detail wd~r~n" +&
"set (wd.workflow_rule_desc, wd.authority_limit, wd.num_approvers, wd.num_required, wd.group_approval, wd.sql, wd.sql_users, wd.rule_order, wd.orig_authority_limit) = (~r~n" +&
"	select wr.description, decode(wd.is_custom_limit,1,wd.authority_limit,wr.authority_limit), case when wr.group_count > 0 then wr.group_count else wr.num_approvers end num_approvers, wr.num_required, wr.group_approval, wr.sql, wr.sql_users, wtr.rule_order, wr.authority_limit~r~n" +&
"	from workflow wf, workflow_type_rule wtr,~r~n" +&
"		(	~r~n" +&
"			"+rule_sqls+"~r~n" +&
"		) wr~r~n" +&
"	where wf.workflow_id = wd.workflow_id~r~n" +&
"	and wtr.workflow_type_id = wf.workflow_type_id~r~n" +&
"	and wtr.workflow_rule_id = wd.workflow_rule_id~r~n" +&
"	and wr.workflow_rule_id = wd.workflow_rule_id~r~n" +&
"	)~r~n" +&
"where wd.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"and exists (~r~n" +&
"	select 1 from workflow wf, workflow_type_rule wtr,workflow_rule wr~r~n" +&
"	where wf.workflow_id = wd.workflow_id~r~n" +&
"	and wtr.workflow_type_id = wf.workflow_type_id~r~n" +&
"	and wtr.workflow_rule_id = wd.workflow_rule_id~r~n" +&
"	and wr.workflow_rule_id = wd.workflow_rule_id~r~n" +&
"	)~r~n" +&
"and exists (~r~n" +&
"	select 1 from workflow b~r~n" +&
"	where b.workflow_id = wd.workflow_id~r~n" +&
"	and b.approval_status_id in (1,7)~r~n" /*Initiated, Unrejected*/ +&
"	) "

sqls = f_sql_add_hint(sqls, 'uo_workflow_tools.uf_new_detail_3')

execute immediate :sqls;

if uf_check_sql('Updating workflow_detail table (uo_workflow_tools.uf_new_workflow_detail)') = -1 then return -1


sqls = &
"delete from workflow_detail~r~n" +&
"where (workflow_id, id) in (~r~n" +&
"	select wd.workflow_id, wd.id~r~n" +&
"	from (~r~n" +&
"		select wd.*, rank() over(partition by workflow_id, workflow_rule_id order by workflow_id, workflow_rule_id, id) row_num~r~n" +&
"		from workflow_detail wd~r~n" +&
"		where workflow_id = "+string(i_workflow_id)+"~r~n" +&
"		) wd~r~n" +&
"	where row_num > wd.num_approvers~r~n" +&
"	) "

sqls = f_sql_add_hint(sqls, 'uo_workflow_tools.uf_new_detail_4')

execute immediate :sqls;

if uf_check_sql('Deleting from workflow_detail table (2) (uo_workflow_tools.uf_new_workflow_detail)') = -1 then return -1


sqls = &
"insert into workflow_detail (~r~n" +&
"	id,~r~n" +&
"	workflow_id, workflow_rule_id, workflow_rule_desc,~r~n" +&
"	rule_order, approval_status_id, authority_limit, required,~r~n" +&
"	num_approvers, num_required, group_approval, sql, sql_users )~r~n" +&
"select (select nvl(max(id),0) from workflow_detail where workflow_id = z.workflow_id)+rownum,~r~n" +&
"	z.workflow_id, z.workflow_rule_id, z.workflow_rule_desc,~r~n" +&
"	z.rule_order, 1 approval_status_id, z.orig_authority_limit authority_limit, 1 required,~r~n" +&
"	z.num_approvers, z.num_required, z.group_approval, z.sql, z.sql_users~r~n" +&
"from workflow_detail z, pp_table_years pp~r~n" +&
"where z.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"and not exists (~r~n" +&
"	select 1 from (~r~n" +&
"		select workflow_rule_id, max(row_num) row_num~r~n" +&
"		from (~r~n" +&
"			select wd.*, rank() over(partition by workflow_id, workflow_rule_id order by workflow_id, workflow_rule_id, id) row_num~r~n" +&
"			from workflow_detail wd~r~n" +&
"			where workflow_id = "+string(i_workflow_id)+"~r~n" +&
"			) wd~r~n" +&
"		group by wd.workflow_rule_id~r~n" +&
"		) wr~r~n" +&
"	where wr.workflow_rule_id = z.workflow_rule_id~r~n" +&
"	and wr.row_num = nvl(z.num_approvers,wr.row_num)~r~n" +&
"	)~r~n" +&
"and id = (~r~n" +&
"	select max(wd.id)~r~n" +&
"	from workflow_detail wd~r~n" +&
"	where wd.workflow_id = z.workflow_id~r~n" +&
"	and wd.workflow_rule_id = z.workflow_rule_id~r~n" +&
"	)~r~n" +&
"and pp.year between 2001 and 2000 + (~r~n" +&
"	select z.num_approvers - wr.row_num~r~n" +&
"	from (~r~n" +&
"		select workflow_rule_id, max(row_num) row_num~r~n" +&
"		from (~r~n" +&
"			select wd.*, rank() over(partition by workflow_id, workflow_rule_id order by workflow_id, workflow_rule_id, id) row_num~r~n" +&
"			from workflow_detail wd~r~n" +&
"			where workflow_id = "+string(i_workflow_id)+"~r~n" +&
"			) wd~r~n" +&
"		group by wd.workflow_rule_id~r~n" +&
"		) wr~r~n" +&
"	where wr.workflow_rule_id = z.workflow_rule_id~r~n" +&
"	) "

sqls = f_sql_add_hint(sqls, 'uo_workflow_tools.uf_new_detail_5')

execute immediate :sqls;

if uf_check_sql('Inserting into workflow_detail table (2) (uo_workflow_tools.uf_new_workflow_detail)') = -1 then return -1


//
// Try to default users where possible
//
rtn = uf_default_users(a_workflow_type)
if rtn = -1 then return rtn

return 1

end function

public function longlong uf_default_users (longlong a_workflow_type);string sqls, ret, default_sqls, temp_sqls
longlong i, rule_id
uo_ds_top ds_rules

//
// Get the sqls for the workflow_rules
//
sqls = &
	"select tr.workflow_rule_id, r.sql~r~n" +&
	"from workflow_type_rule tr, workflow_rule r~r~n" +&
	"where tr.workflow_type_id = "+string(a_workflow_type)+"~r~n" +&
	"and tr.workflow_rule_id = r.workflow_rule_id~r~n" +&
	"order by tr.rule_order~r~n"
ds_rules = create uo_ds_top
ret = f_create_dynamic_ds(ds_rules,'grid',sqls,sqlca,true)
if ret <> 'OK' then
	uf_msg("Error creating datastore:~r~n"+sqls, 'E')
	return -1
end if

if ds_rules.rowcount() = 0 then
	return 1
end if

default_sqls = ""
for i = 1 to ds_rules.rowcount()
	if i = 1 then
		default_sqls = ""
	end if
	
	rule_id = ds_rules.getitemnumber(i,"workflow_rule_id")
	setnull(temp_sqls)
	temp_sqls = ds_rules.getitemstring(i,"sql")
	if isnull(temp_sqls) or temp_sqls = "" then
		temp_sqls = &
			"select approval_type_id, auth_level, lower(trim(users)) users, default_user, nvl(allow_edit,1) allow_edit, dollar_limit~r~n" +&
			"from approval_auth_level~r~n" +&
			"where approval_type_id = "+string(a_workflow_type)+"~r~n" +&
			"and auth_level = "+string(rule_id)+"~r~n" +&
			"and default_user > 0~r~n"
	else
		if isnull(i_id_field1) or trim(i_id_field1) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field1>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field1>>",i_id_field1,"all")
		end if
		if isnull(i_id_field2) or trim(i_id_field2) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field2>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field2>>",i_id_field2,"all")
		end if
		if isnull(i_id_field3) or trim(i_id_field3) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field3>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field3>>",i_id_field3,"all")
		end if
		if isnull(i_id_field4) or trim(i_id_field4) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field4>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field4>>",i_id_field4,"all")
		end if
		if isnull(i_id_field5) or trim(i_id_field5) = "" then
			temp_sqls = f_replace_string(temp_sqls,"<<id_field5>>","0","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<id_field5>>",i_id_field5,"all")
		end if
		if isnull(i_company_id) or i_company_id = -1 then
			temp_sqls = f_replace_string(temp_sqls,"<<co_id>>","-1","all")
		else
			temp_sqls = f_replace_string(temp_sqls,"<<co_id>>",string(i_company_id),"all")
		end if
		temp_sqls = &
			"select "+string(a_workflow_type)+" approval_type_id, "+string(rule_id)+" auth_level, lower(trim(users)) users, default_user, nvl(allow_edit,1) allow_edit, to_number(dollar_limit) dollar_limit~r~n" +&
			"from ("+temp_sqls+")~r~n" +&
			"where default_user > 0~r~n"
	end if
	
	if i < ds_rules.rowcount() then
		default_sqls += temp_sqls+"union all~r~n"
	else
		default_sqls += temp_sqls+""
	end if
next
destroy ds_rules

//
// Try to default users where they don't yet exist
//
sqls = &
"update workflow_detail z~r~n" +&
"set (z.users, z.allow_edit, z.authority_limit, z.is_custom_limit) = (~r~n" +&
"	select decode(z.users,null,a.users,z.users), decode(z.users,null,nvl(a.allow_edit,1),z.allow_edit), decode(z.users,null,nvl(a.dollar_limit,z.authority_limit),z.authority_limit), decode(z.users,null,nvl2(a.dollar_limit,1,0),0)~r~n" +&
"	from (~r~n" +&
"		select a.users, a.allow_edit, a.dollar_limit, wd.id~r~n" +&
"		from~r~n" +&
"			(	select rank() over(partition by workflow_rule_id order by workflow_id, id) row_num, wd.*~r~n" +&
"				from workflow_detail wd~r~n" +&
"				where wd.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"				and wd.users is null~r~n" +&
"			) wd,~r~n" +&
"			(	select rank() over(partition by auth_level order by auth_level, default_user, users) row_num, a.*~r~n" +&
"				from (~r~n" +&
"					select a.*~r~n" +&
"					from (~r~n" +&
"						select approval_type_id, auth_level, lower(trim(users)) users, default_user, allow_edit, dollar_limit from (~r~n"+default_sqls+") a~r~n" +&
"						) a~r~n" +&
"					where a.approval_type_id = "+string(a_workflow_type)+"~r~n" +&
"					and a.default_user > 0~r~n" +&
"					and not exists (~r~n" +&
"						select 1 from workflow_detail b~r~n" +&
"						where b.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"						and lower(trim(b.users)) = lower(trim(a.users))~r~n" +&
"						)~r~n" +&
"					order by a.default_user~r~n" +&
"					) a~r~n" +&
"			) a~r~n" +&
"		where wd.workflow_rule_id = a.auth_level~r~n" +&
"		and wd.row_num = a.row_num~r~n" +&
"		) a~r~n" +&
"	where a.id = z.id~r~n" +&
"	)~r~n" +&
"where z.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"and exists (~r~n" +&
"	select 1 from workflow b~r~n" +&
"	where b.workflow_id = z.workflow_id~r~n" +&
"	and b.approval_status_id in (1,7)~r~n" +&
"	)~r~n" +&
"and z.id in (~r~n" +&
"	select wd.id~r~n" +&
"	from~r~n" +&
"		(	select rank() over(partition by workflow_rule_id order by workflow_id, id) row_num, wd.*~r~n" +&
"			from workflow_detail wd~r~n" +&
"			where wd.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"			and wd.users is null~r~n" +&
"		) wd,~r~n" +&
"		(	select rank() over(partition by auth_level order by auth_level, default_user, users) row_num, a.*~r~n" +&
"			from (~r~n" +&
"				select a.*~r~n" +&
"				from (~r~n" +&
"					select approval_type_id, auth_level, lower(trim(users)) users, default_user, allow_edit, dollar_limit from (~r~n"+default_sqls+") a~r~n" +&
"					) a~r~n" +&
"				where a.approval_type_id = "+string(a_workflow_type)+"~r~n" +&
"				and a.default_user > 0~r~n" +&
"				and not exists (~r~n" +&
"					select 1 from workflow_detail b~r~n" +&
"					where b.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"					and lower(trim(b.users)) = lower(trim(a.users))~r~n" +&
"					)~r~n" +&
"				order by a.default_user~r~n" +&
"				) a~r~n" +&
"		) a~r~n" +&
"	where wd.workflow_rule_id = a.auth_level~r~n" +&
"	and wd.row_num = a.row_num~r~n" +&
"	)~r~n"

execute immediate :sqls;

if uf_check_sql('Updating defaults in workflow_detail table (uo_workflow_tools.uf_new_workflow)') = -1 then return -1



//
// Default users where they are not allowed to edit - even if a user already exists in that field
//
sqls = &
"update workflow_detail z~r~n" +&
"set (z.users, z.allow_edit, z.authority_limit, z.is_custom_limit) = (~r~n" +&
"	select decode(A.users,null,Z.users,A.users), decode(A.users,null,nvl(Z.allow_edit,1),A.allow_edit), decode(A.users,null,Z.AUTHORITY_limit,NVL(A.DOLLAR_limit,Z.AUTHORITY_LIMIT)), decode(A.users,null,0,NVL2(A.DOLLAR_limit,1,0))~r~n" +&
"	from (~r~n" +&
"		select a.users, a.allow_edit, a.dollar_limit, wd.id~r~n" +&
"		from~r~n" +&
"			(	select rank() over(partition by workflow_rule_id order by workflow_id, id) row_num, wd.*~r~n" +&
"				from workflow_detail wd~r~n" +&
"				where wd.workflow_id = "+string(i_workflow_id)+"~r~n" +&
/*"				and wd.users is null~r~n" +*/&
"			) wd,~r~n" +&
"			(	select rank() over(partition by auth_level order by auth_level, default_user, users) row_num, a.*~r~n" +&
"				from (~r~n" +&
"					select a.*~r~n" +&
"					from (~r~n" +&
"						select approval_type_id, auth_level, lower(trim(users)) users, default_user, allow_edit, dollar_limit from (~r~n"+default_sqls+") a~r~n" +&
"						) a~r~n" +&
"					where a.approval_type_id = "+string(a_workflow_type)+"~r~n" +&
"					and a.default_user > 0~r~n" +&
"					and not exists (~r~n" +&
"						select 1 from workflow_detail b~r~n" +&
"						where b.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"						and lower(trim(b.users)) = lower(trim(a.users))~r~n" +&
"						)~r~n" +&
"					AND A.ALLOW_EDIT = 0~r~n" +&
"					order by a.default_user~r~n" +&
"					) a~r~n" +&
"			) a~r~n" +&
"		where wd.workflow_rule_id = a.auth_level~r~n" +&
"		and wd.row_num = a.row_num~r~n" +&
"		) a~r~n" +&
"	where a.id = z.id~r~n" +&
"	)~r~n" +&
"where z.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"and exists (~r~n" +&
"	select 1 from workflow b~r~n" +&
"	where b.workflow_id = z.workflow_id~r~n" +&
"	and b.approval_status_id in (1,7)~r~n" +&
"	)~r~n" +&
"and z.id in (~r~n" +&
"	select wd.id~r~n" +&
"	from~r~n" +&
"		(	select rank() over(partition by workflow_rule_id order by workflow_id, id) row_num, wd.*~r~n" +&
"			from workflow_detail wd~r~n" +&
"			where wd.workflow_id = "+string(i_workflow_id)+"~r~n" +&
/*"			and wd.users is null~r~n" +*/&
"		) wd,~r~n" +&
"		(	select rank() over(partition by auth_level order by auth_level, default_user, users) row_num, a.*~r~n" +&
"			from (~r~n" +&
"				select a.*~r~n" +&
"				from (~r~n" +&
"					select approval_type_id, auth_level, lower(trim(users)) users, default_user, allow_edit, dollar_limit from (~r~n"+default_sqls+") a~r~n" +&
"					) a~r~n" +&
"				where a.approval_type_id = "+string(a_workflow_type)+"~r~n" +&
"				and a.default_user > 0~r~n" +&
"				and not exists (~r~n" +&
"					select 1 from workflow_detail b~r~n" +&
"					where b.workflow_id = "+string(i_workflow_id)+"~r~n" +&
"					and lower(trim(b.users)) = lower(trim(a.users))~r~n" +&
"					)~r~n" +&
"				AND A.ALLOW_EDIT = 0~r~n" +&
"				order by a.default_user~r~n" +&
"				) a~r~n" +&
"		) a~r~n" +&
"	where wd.workflow_rule_id = a.auth_level~r~n" +&
"	and wd.row_num = a.row_num~r~n" +&
"	)~r~n"

execute immediate :sqls;

if uf_check_sql('Updating defaults in workflow_detail table (uo_workflow_tools.uf_new_workflow)') = -1 then return -1



////
//// Update dollar limit
////
//sqls = &
//"update workflow_detail z " +&
//"set (z.authority_limit) = ( " +&
//"	select nvl(a.dollar_limit,z.authority_limit) " +&
//"	from ( " +&
//"		select a.users, a.allow_edit, a.dollar_limit, wd.id " +&
//"		from " +&
//"			(	select rank() over(partition by workflow_rule_id order by workflow_id, id) row_num, wd.* " +&
//"				from workflow_detail wd " +&
//"				where wd.workflow_id = "+string(i_workflow_id)+" " +&
///*"				and wd.users is null " +*/&
//"			) wd, " +&
//"			(	select rank() over(partition by auth_level order by auth_level, default_user, users) row_num, a.* " +&
//"				from ( " +&
//"					select a.* " +&
//"					from ( " +&
//"						select approval_type_id, auth_level, (select decode(count(*),0,a.users,null) from workflow_detail b where b.workflow_id = 21 and b.users = a.users) users, default_user, allow_edit, dollar_limit from ("+default_sqls+") a " +&
//"						) a " +&
//"					where a.approval_type_id = "+string(a_workflow_type)+" " +&
//"					and a.default_user > 0 " +&
///*"					and not exists ( " +&
//"						select 1 from workflow_detail b " +&
//"						where b.workflow_id = "+string(i_workflow_id)+" " +&
//"						and b.users = a.users " +&
//"						) " +*/&
//"					order by a.default_user " +&
//"					) a " +&
//"			) a " +&
//"		where wd.workflow_rule_id = a.auth_level " +&
//"		and wd.row_num = a.row_num " +&
//"		) a " +&
//"	where a.id = z.id " +&
//"	) " +&
//"where z.workflow_id = "+string(i_workflow_id)+" " +&
//"and exists ( " +&
//"	select 1 from workflow b " +&
//"	where b.workflow_id = z.workflow_id " +&
//"	and b.approval_status_id in (1,7) " +&
//"	) " +&
//"and z.id in ( " +&
//"	select wd.id " +&
//"	from " +&
//"		(	select rank() over(partition by workflow_rule_id order by workflow_id, id) row_num, wd.* " +&
//"			from workflow_detail wd " +&
//"			where wd.workflow_id = "+string(i_workflow_id)+" " +&
///*"			and wd.users is null " +*/&
//"		) wd, " +&
//"		(	select rank() over(partition by auth_level order by auth_level, default_user, users) row_num, a.* " +&
//"			from ( " +&
//"					select a.* " +&
//"					from ( " +&
//"						select approval_type_id, auth_level, (select decode(count(*),0,a.users,null) from workflow_detail b where b.workflow_id = 21 and b.users = a.users) users, default_user, allow_edit, dollar_limit from ("+default_sqls+") a " +&
//"						) a " +&
//"				where a.approval_type_id = "+string(a_workflow_type)+" " +&
//"				and a.default_user > 0 " +&
///*"				and not exists ( " +&
//"					select 1 from workflow_detail b " +&
//"					where b.workflow_id = "+string(i_workflow_id)+" " +&
//"					and b.users = a.users " +&
//"					) " +*/&
//"				order by a.default_user " +&
//"				) a " +&
//"		) a " +&
//"	where wd.workflow_rule_id = a.auth_level " +&
//"	and wd.row_num = a.row_num " +&
//"	) "
//
//execute immediate :sqls;
//
//if uf_check_sql('Updating defaults in workflow_detail table (uo_workflow_tools.uf_new_workflow)') = -1 then return -1

return 1
end function

public function longlong uf_workflow_unsend ();longlong cnt_pending, cnt_approved, rtn, i
string sqls_arr[]

//
// Make sure the objects has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Un-Send: You must first call "uf_get_workflow" before you can call "uf_workflow_unsend."', 'E')
	return -1
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('unsend_before')
if rtn = -1 then
	return -1
end if

//
// Confirm with the user they want to send
//
if i_messagebox_onerror and i_messagebox_confirm then
	rtn = messagebox("Confirm Un-Send", "Are you sure you want to reset this approval, allowing the workflow to start again from the beginning?", question!, yesno!)
	
	if rtn <> 1 then
		return -1
	end if
end if

//
// Check that this workflow is in a pending approval status so that it can be un-sent
//
select count(*)
into :cnt_pending
from workflow
where approval_status_id = 2
and workflow_id = :i_workflow_id;

if cnt_pending <> 1 then
	uf_msg('Un-Send: This workflow is not in a "Pending Approval" status...cannot Un-Send.', 'E')
	return -1
end if

//
// Check that this workflow has not been approved/rejected by anyone
//
select count(*)
into :cnt_approved
from workflow_detail
where approval_status_id in (3,4) /*approved, rejected*/
and workflow_id = :i_workflow_id;

if cnt_pending <> 1 then
	uf_msg('Un-Send: This workflow has been approved by another user...cannot Un-Send.', 'E')
	return -1
end if

//
// Update the approval status on the workflow
//
update workflow
set approval_status_id = 1, // initiated
	date_sent = null,
	date_approved = null,
	user_sent = null
where workflow_id = :i_workflow_id;

if uf_check_sql('Un-Sending workflow table (uo_budget_tools.uf_workflow_unsend)') = -1 then return -1

update workflow_detail
set date_approved = null,
	user_approved = null,
	approval_status_id = 1 // initiated
where workflow_id = :i_workflow_id;

if uf_check_sql('Un-Sending workflow_detail table (uo_budget_tools.uf_workflow_unsend)') = -1 then return -1

//
// Run the associated custom sql for status updates, etc.
//
if i_unsend_sql <> "" then
	rtn = f_parsestringintostringarray(i_unsend_sql, ';', sqls_arr)
	
	for i = 1 to upperbound(sqls_arr)
		if not isnull(sqls_arr[i]) and trim(sqls_arr[i]) <> "" then
			execute immediate :sqls_arr[i];
			
			if uf_check_sql('Un-Sending custom sql (uo_workflow_tools.uf_workflow_unsend):~r~n'+sqls_arr[i]) = -1 then return -1
		end if
	next
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('unsend_after')
if rtn = -1 then
	return -1
end if

uf_msg('Successfully un-sent approval.','I')

return 1
end function

public function longlong uf_delete_unnecessary_levels ();decimal max_required_authority

if i_use_limits = 1 then
	select nvl(least(
		min(case when (wfd.num_required > 0 or (wfd.users is not null and wfd.required = 1)) and wfd.authority_limit >= wf.approval_amount then wfd.authority_limit else 999999999999 end),
		max(wfd.authority_limit)
		),999999999999)
	//select nvl(min(wfd.authority_limit),999999999999)
	into :max_required_authority
	from workflow wf, workflow_detail wfd
	where wf.workflow_id = wfd.workflow_id
	//and (wfd.num_required > 0 or (wfd.users is not null and wfd.required = 1))
	//and wfd.authority_limit >= wf.approval_amount
	and wf.workflow_id = :i_workflow_id;
	
	// ### CDM - Maint 7422 - only check if max_required_authority is not null...zero is a valid value if the estimate to approve equals zero
	//if max_required_authority > 0 then
	if not isnull(max_required_authority) then
		delete from workflow_detail
		where workflow_id = :i_workflow_id
		and authority_limit > :max_required_authority
		and authority_limit <> 0;
		
		if uf_check_sql('Deleting unnecessary levels from workflow_detail (uo_workflow_tools.uf_workflow_send)') = -1 then return -1
	end if
end if

return 1
end function

public function longlong uf_call_custom_fxn (string a_call_name);longlong rtn

// ### CDM - Maint 6176 - call f_workflow_base instead of f_workflow_custom
rtn = f_workflow_base(a_call_name, this)

if rtn = -1 then
	return -1
end if

return 1
end function

public subroutine uf_msg (string msg, string str_error);string str_title, time_string

str_title = 'Workflow Processing'

if i_add_time_to_messages then
	time_string = string(now())
	msg = time_string+' '+msg
end if

if g_main_application then
	if upper(str_error) = 'E' then
		if i_messagebox_onerror then
			messagebox(str_title, msg)
		end if
		if i_statusbox_onerror then
			f_status_box(str_title, msg)
		end if
	elseif upper(str_error) = 'W' then
		if i_statusbox_warning then
			f_status_box(str_title, msg)
		end if
	else
		if i_status_box then
			f_status_box(str_title, msg)
		end if
	end if
end if

if i_pp_msgs or (not g_main_application) then
	f_pp_msgs(msg)
end if

if i_store_error_string and (upper(str_error) = "E" or upper(str_error) = "W") then
	if isNull( i_error_message ) then i_error_message = ""
	if i_error_message <> "" then i_error_message = i_error_message + "~r~n"
	i_error_message = i_error_message + msg
end if

end subroutine

public function longlong uf_user_approve (string a_users, string a_approving_user);longlong cnt_pending, cnt_still_pending, cur_level, rtn, cnt_authority_to_approve, &
	cnt_approved, num_required
boolean send_next_level = false

//
// Make sure the object has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Approve: You must first call "uf_get_workflow" before you can call "uf_user_approve."', 'E')
	return -1
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('user_approve_before')
if rtn = -1 then
	return -1
end if

//
// Make sure the user being approved is pending approval
//
select count(*)
into :cnt_pending
from workflow_detail
where workflow_id = :i_workflow_id
and lower(trim(users)) = lower(trim(:a_users))
and approval_status_id = 2;

if cnt_pending <> 1 then
	uf_msg('Approve: This user is not in a "Pending Approval" status...cannot Approve.', 'E')
	return -1
end if

//
// Find out what level we are approving
//
select workflow_rule_id
into :cur_level
from workflow_detail
where workflow_id = :i_workflow_id
and lower(trim(users)) = lower(trim(:a_users))
and approval_status_id = 2;

//
// Mark as approved
//
update 	workflow_detail
set	 		approval_status_id = 3, // approved
			user_approved = :a_approving_user,
			date_approved = sysdate
where 	workflow_id = :i_workflow_id
and 		lower(trim(users)) = lower(trim(:a_users))
and 		approval_status_id in (1,2);

if uf_check_sql('Approving workflow_detail table (uo_workflow_tools.uf_user_approve)') = -1 then return -1

//
// Check to see if this level has any approvers
//
send_next_level = false
send_next_level = uf_check_level(cur_level)
if send_next_level then
	//
	// No more required uses at this level...mark any residual users at this level as approved
	//
	update workflow_detail
	set approval_status_id = 3 // approved
	where workflow_id = :i_workflow_id
	and workflow_rule_id = :cur_level;
	
	//
	// Check if this level had enough authority to approve, and remove additional levels that may exist due to allowing null levels
	//
	rtn = uf_delete_unnecessary_levels()
	if rtn = -1 then
		return -1
	end if
	
	//
	// Move on to next level
	//
	rtn = uf_send_next_level()
	if rtn = -1 then
		return -1
	end if
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('user_approve_after')
if rtn = -1 then
	return -1
end if

uf_msg('Successfully Approved.','I')

return 1
end function

public function longlong uf_user_reject (string a_users, string a_approving_user);longlong cnt_pending, cnt_still_pending, cur_level, rtn

//
// Make sure the object has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Reject: You must first call "uf_get_workflow" before you can call "uf_user_reject."', 'E')
	return -1
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('user_reject_before')
if rtn = -1 then
	return -1
end if

//
// Make sure the user being approved is pending approval
//
select count(*)
into :cnt_pending
from workflow_detail
where workflow_id = :i_workflow_id
and lower(trim(users)) = lower(trim(:a_users))
and approval_status_id = 2;

if cnt_pending <> 1 then
	uf_msg('Reject: This user is not in a "Pending Approval" status...cannot Reject.', 'E')
	return -1
end if

//
// Mark as rejected
//
update workflow_detail
set approval_status_id = 4, // rejected
	user_approved = :a_approving_user,
	date_approved = sysdate
where workflow_id = :i_workflow_id
and lower(trim(users)) = lower(trim(:a_users))
and approval_status_id = 2;

if uf_check_sql('Rejecting workflow_detail table (uo_workflow_tools.uf_user_reject') = -1 then return -1

//
// Reject the entire workflow
//
rtn = uf_workflow_reject()
if rtn = -1 then
	return -1
end if

uf_msg('Successfully Rejected.','I')

return 1
end function

public function boolean uf_check_level (longlong a_level);longlong cnt_still_pending, cnt_approved, num_required, group_approval
boolean send_next_level = false

//
// Get some attributes from this rule
//
select nvl(max(num_required),1), nvl(max(group_approval),0)
into :num_required, :group_approval
from workflow_detail
where workflow_id = :i_workflow_id
and workflow_rule_id = :a_level;

//
// Check to see if there are any other required users at this level
//
select count(*)
into :cnt_still_pending
from workflow_detail
where workflow_id = :i_workflow_id
and workflow_rule_id = :a_level
and trim(users) is not null
and required = 1
and approval_status_id = 2 // pending approval
and nvl(group_approval,0) = 0;

if group_approval = 0 and cnt_still_pending = 0 then
	//
	// Move on to next level
	//
	send_next_level = true
	
end if

//
// For Group Approval, check to see if the minimum number of requried users have approved
//
select count(*)
into :cnt_approved
from workflow_detail
where workflow_id = :i_workflow_id
and workflow_rule_id = :a_level
and users is not null
//and required = 1
and approval_status_id = 3
and nvl(group_approval,0) = 1;

if group_approval > 0 and cnt_approved >= num_required then
	//
	// Move on to next level
	//
	send_next_level = true
	
end if

return send_next_level
end function

public function longlong uf_user_approve (string a_users, string a_approving_user, string a_notes);longlong	rtn

////
////	Call the user approve function.
////
		rtn = uf_user_approve( a_users, a_approving_user )
		if rtn = -1 then return rtn
		
////
////	Call the function to update the notes.
////
		rtn = uf_user_notes( a_users, a_notes )
		if rtn = -1 then return rtn

////
////	Function completed successfully.
////
		return 1
end function

public function longlong uf_user_reject (string a_users, string a_approving_user, string a_notes);longlong	rtn

////
////	Call the user reject function.
////
		rtn = uf_user_reject( a_users, a_approving_user )
		if rtn = -1 then return rtn
		
////
////	Call the function to update the notes.
////
		rtn = uf_user_notes( a_users, a_notes )
		if rtn = -1 then return rtn

////
////	Function completed successfully.
////
		return 1
end function

public function longlong uf_user_notes (string a_users, string a_notes);////
////	Update the workflow_detail table with notes for the current workflow and the passed-in user.
////	This user could potentially have more than one row for the current workflow (if they are an approver at multiple levels and cascading is allowed),
////	so only update the row for the highest level (i.e., the max rule order).
////	This function will replace any existing notes stored on the table.  If the notes passed are null or "", it will clear out the existing notes.
////

////
//// 	Trim the notes.  Make sure they're not null and not more than 4000 characters.
////
		a_notes = trim( a_notes )
		if isNull( a_notes ) then a_notes = ""
		a_notes = mid( a_notes, 1, 4000 )
		
////
////	Update in workflow_detail.
////
		update 	workflow_detail wd
		set	 		wd.notes = :a_notes
		where 	wd.workflow_id = :i_workflow_id
		and 		lower(trim(wd.users)) = lower(trim(:a_users))
		and		wd.rule_order =
					(	select		max( inwd.rule_order)
						from		workflow_detail inwd
						where	inwd.workflow_id = :i_workflow_id
						and		lower(trim(inwd.users)) = lower(trim(:a_users))
					);
		
		if uf_check_sql('Adding notes to workflow_detail table (uo_workflow_tools.uf_user_notes)') = -1 then return -1
	
////
////	Function completed successfully.
////
		return 1
end function

public function longlong uf_workflow_send_multi ();longlong cnt_able_to_send, rtn, i
string sqls_arr[], ret
uo_ds_top ds_workflow_detail
ds_workflow_detail = create uo_ds_top
datawindowchild dw_child

//
// Make sure the object has been initialized
//
if i_workflow_id = -1 then
	uf_msg('Send for Approval: You must first call "uf_get_workflow" before you can call "uf_workflow_send."', 'E')
	return -1
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('send_before')
if rtn = -1 then
	return -1
end if

//
// Confirm with the user they want to send
//
if i_messagebox_onerror and i_messagebox_confirm then
	rtn = messagebox("Confirm Send for Approval", "Are you sure you want to send for approval?", question!, yesno!)
	if rtn <> 1 then
		return -1
	end if
end if

//
// Check that this workflow is in a status that it can be sent for approval
//
select count(*)
into :cnt_able_to_send
from workflow
where approval_status_id in (1,7) // initiated, un-rejected
and workflow_id = :i_workflow_id;

if cnt_able_to_send <> 1 then
	uf_msg('Send for Approval: This workflow is not in a status to be able to send for approval...cannot send.', 'E')
	return -1
end if

//
// Validate that the workflow is ready for sending
//
ds_workflow_detail.dataobject = 'dw_workflow_detail'
rtn = ds_workflow_detail.settransobject(sqlca)
rtn = ds_workflow_detail.retrieve(i_workflow_id)
// ### CDM - Maint 6125 - validate approver appears only once
rtn = ds_workflow_detail.getchild("users",dw_child)
rtn = dw_child.settransobject(sqlca)
rtn = dw_child.retrieve(i_company_id, i_workflow_type_id)
rtn = uf_workflow_validate(ds_workflow_detail, i_workflow_id, 'send')
if rtn = -1 then
	return -1
end if

//
// If this workflow uses the authority limits, then delete any levels that are unnecessary for approval
//
rtn = uf_delete_unnecessary_levels()
if rtn = -1 then
	return -1
end if

//
// Update the approval status on the workflow
//
update workflow
set approval_status_id = 2, // pending_approval
	date_sent = sysdate,
	user_sent = user
where workflow_id = :i_workflow_id;

if uf_check_sql('Sending for Approval workflow table (uo_workflow_tools.uf_workflow_send)') = -1 then return -1

//
// Send email to the users on the next level
//
i_sending = true
rtn = uf_send_next_level()
i_sending = false
if rtn = -1 then
	return -1
end if

// ### CDM - Maint 6734 - Move up in script to be able to leverage "user_sent" in notification emails
////
//// Update the approval status on the workflow
////
//update workflow
//set approval_status_id = 2, // pending_approval
//	date_sent = sysdate,
//	user_sent = user
//where workflow_id = :i_workflow_id;
//
//if uf_check_sql('Sending for Approval workflow table (uo_workflow_tools.uf_workflow_send)') = -1 then return -1


//
// Run the associated custom sql for status updates, etc.
//
if i_send_sql <> "" then
	rtn = f_parsestringintostringarray(i_send_sql, ';', sqls_arr)
	
	for i = 1 to upperbound(sqls_arr)
		if not isnull(sqls_arr[i]) and trim(sqls_arr[i]) <> "" then
			execute immediate :sqls_arr[i];
			
			if uf_check_sql('Send for Approval custom sql (uo_workflow_tools.uf_workflow_send):~r~n'+sqls_arr[i]) = -1 then return -1
		end if
	next
end if

//
// Call to custom function
//
rtn = uf_call_custom_fxn('send_after')
if rtn = -1 then
	return -1
end if

//
// If no levels had any required approvers when sending for approval, this should automatically approve the workflow
//
if i_send_approves then
	i_send_approves = false
	rtn = uf_workflow_approve()
	if rtn = -1 then
		return -1
	end if
	uf_msg('Successfully approved.','I')
else
	uf_msg('Successfully sent for approval.','I')
end if

return 1

end function

on uo_workflow_tools.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_workflow_tools.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

