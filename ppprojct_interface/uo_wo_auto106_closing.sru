HA$PBExportHeader$uo_wo_auto106_closing.sru
$PBExportComments$36229
forward
global type uo_wo_auto106_closing from nonvisualobject
end type
end forward

global type uo_wo_auto106_closing from nonvisualobject
end type
global uo_wo_auto106_closing uo_wo_auto106_closing

type variables
string						i_sbox_title, i_user_id1, i_co_desc, i_gl_je_code
string						i_msg[],  i_null_arrary[]
string						i_reset_str[]
longlong	i_process_id, i_run_id, i_co_id, i_mo_num, i_num_buckets
boolean					i_validation_error
end variables

forward prototypes
public function integer of_logfile ()
public function longlong of_errors_update (string a_error_type)
public function longlong of_validations ()
public function longlong of_class_codes ()
public function longlong of_wos106 ()
public function longlong of_wos101failed ()
public function longlong of_check_pending ()
public function longlong of_balancing ()
public function longlong of_balancing_wip_comp ()
public function longlong of_remove_transactions ()
public function longlong of_check_charge_data ()
public function longlong of_allocation ()
public function longlong of_eligible_charges ()
public function longlong of_insert_pending ()
public function longlong of_wip_comp ()
public function longlong of_use_charges ()
public function longlong of_use_cpr ()
public function longlong of_use_estimates ()
public function longlong of_get_util_acct ()
public function longlong of_archive ()
public function longlong of_non_unitized_status ()
public function longlong of_wos_remove_no_nonunitized ()
public function longlong of__main (longlong a_company_id, longlong a_month_number, string a_type, longlong a_late_month_number)
public function longlong of_wos101late (longlong a_late_month_number)
end prototypes

public function integer of_logfile ();/****************************************************************************
 **	
 **	of_logfile		:	Creates a log of the events occurring within the uo
 ** 
 **	
 **	Returns			: 1
 **	
 ****************************************************************************/
 
longlong	num_msgs, i 
 
num_msgs	=	upperbound(i_msg[])

for i=1 to num_msgs
	
	////f_write_log(i_path, right(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),8) + '~t' + i_msg[i] )
	f_wo_status_box(i_sbox_title,LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),16) + ' ' + i_msg[i] )
	 if i_process_id > 0 then
		f_pp_msgs(  LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),16) + ' ' + i_msg[i] )
	 end if
	
next 

return 1
end function

public function longlong of_errors_update (string a_error_type);longlong max_id, num_nrows

//get max id; PRIMARY KEY ( "ID", "COMPANY_ID", "ACCOUNTING_MONTH" )
max_id = 0
select max(id) into :max_id from wo_auto106_control
where company_id = :i_co_id and accounting_month = to_date(:i_mo_num,'yyyymm');

if max_id = 0 or isnull(max_id) then 
	max_id = 1
else
	max_id++
end if

//Save error messages to wo_auto106_control and remove from wo_auto106_wos to make rest of processing faster...

choose case a_error_type
	case 'UNITS'

		insert into WO_AUTO106_CONTROL  (id, work_order_id, accounting_month, return_code, message, run, company_id, closing_option_id)
		select :max_id + rownum, work_order_id, to_date(:i_mo_num,'yyyymm'), -2, error_msg, :i_run_id, company_id, closing_option_id
		from (
				select distinct a.work_order_id work_order_id,  substrb(a.error_msg, 1, 2000) error_msg, b.closing_option_id closing_option_id, b.company_id company_id
				from wo_auto106_pending_trans a, wo_auto106_wos b
				where a.work_order_id = b.work_order_id
				and a.error_msg is not null
				);
				
		if sqlca.SQLCode <> 0 then
			i_msg[] = i_null_arrary[]
			i_msg[1]  = ": ERROR: insert into wo_auto106_control(UNITS): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		if num_nrows > 0 then
			i_validation_error = true
		end if 
		
		//////////////////////////////////////////////////////////////////////////////////////////////
		//remove data for work orders that have errors to make rest of processing faster. 		
		//////////////////////////////////////////////////////////////////////////////////////////////
		
		delete from WO_AUTO106_CHARGES a
		where exists (select 1 from wo_auto106_pending_trans x where x.work_order_id = a.work_order_id and x.error_msg is not null)
		and a.company_id = :i_co_id
		and a.accounting_month = to_date(:i_mo_num, 'yyyymm')
		and a.run = :i_run_id;
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =  ": ERROR: delete from WO_AUTO106_CHARGES(UNITS): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows

		delete from WO_AUTO106_CHARGES_SUMM a
		where exists (select 1 from wo_auto106_pending_trans x where x.work_order_id = a.work_order_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from wo_auto106_unit_calc(UNITS): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		delete from WO_AUTO106_WOS a
		where exists (select 1 from wo_auto106_pending_trans x where x.work_order_id = a.work_order_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =  ": ERROR: delete from wo_auto106_wos(UNITS): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		//need to delete all the unit items created for the work order that has an error on one of the unit items
		//this delete needs to be last because all prior deletes key off of WO_AUTO106_PENDING_TRANS
		delete from WO_AUTO106_PENDING_TRANS a 
		where exists (select 1 from wo_auto106_pending_trans x where x.work_order_id = a.work_order_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from wo_auto106_pending_trans(UNITS): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows		

//		//testing option instead of deleting above
//		UPDATE wo_auto106_wos a SET a.ERROR_MSG = decode(a.error_msg, null, 'Fast: ', a.error_msg||' :: ' )||'Unit Item validation errors on WO' 
//		where exists (select 1 from wo_auto106_pending_trans x where x.work_order_id = a.work_order_id and x.error_msg is not null);

	case 'WO'
		
		insert into WO_AUTO106_CONTROL  (id, work_order_id, accounting_month, return_code, message, run, company_id, closing_option_id)
		select :max_id + rownum, work_order_id, to_date(:i_mo_num,'yyyymm'), -2, substrb(error_msg, 1, 2000), :i_run_id, company_id, closing_option_id
		from wo_auto106_wos where error_msg is not null;
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: insert into wo_auto106_control(WO): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		if num_nrows > 0 then
			i_validation_error = true
		end if 		
		
		//////////////////////////////////////////////////////////////////////////////////////////////
		//remove data for work orders that have errors to make rest of processing faster. 		
		//////////////////////////////////////////////////////////////////////////////////////////////
		
		delete from WO_AUTO106_CHARGES a
		where exists (select 1 from wo_auto106_wos x where x.work_order_id = a.work_order_id and x.error_msg is not null)
		and a.company_id = :i_co_id
		and a.accounting_month = to_date(:i_mo_num, 'yyyymm')
		and a.run = :i_run_id;
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from WO_AUTO106_CHARGES(WO): " + sqlca.SQLErrText 
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		delete from WO_AUTO106_CHARGES_SUMM a
		where exists (select 1 from wo_auto106_wos x where x.work_order_id = a.work_order_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from WO_AUTO106_CHARGES_SUMM(WO): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		delete from WO_AUTO106_PENDING_TRANS a
		where exists (select 1 from wo_auto106_wos x where x.work_order_id = a.work_order_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =  ": ERROR: delete from wo_auto106_pending_trans(WO): " + sqlca.SQLErrText
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		//this delete needs to be last because all prior deletes key off of WO_AUTO106_WOS
		delete from WO_AUTO106_WOS where error_msg is not null;
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =  ": ERROR: delete from wo_auto106_wos(WO): " + sqlca.SQLErrText 
			of_logfile()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows

//		//testing option instead of deleting above
//		UPDATE wo_auto106_wos a SET a.ERROR_MSG = decode(a.error_msg, null, 'Fast: ', a.error_msg||' :: ' )||'Unit Item validation errors on WO' 
//		where exists (select 1 from wo_auto106_pending_trans x where x.work_order_id = a.work_order_id and x.error_msg is not null);

end choose 

return 1

end function

public function longlong of_validations ();////////////////////////////////////////////////////////////////////////////////////////////////
///this function defaults missing data and checks for data validations
////////////////////////////////////////////////////////////////////////////////////////////////
longlong check_count, num_nrows, rtn, wo_accr_ct, mo, yr
string check_unit_account, wo_accr_process_ans, custom_audit_error_msg


i_msg[] = i_null_arrary[] 
i_msg[1] =  ": Running Validations..."
of_logfile() 

////////////////////////////////
//Run WO level validations
////////////////////////////////
wo_accr_process_ans = lower(f_pp_system_control_company("Work Order Accrual Calculation",i_co_id))
if isnull(wo_accr_process_ans) or wo_accr_process_ans = "" then wo_accr_process_ans = 'no'

if wo_accr_process_ans = 'yes' then
	//logic modeled from f_accrual_106, which is a base function
	mo = long(right(string(i_mo_num), 2))
	yr = long(left(string(i_mo_num), 4))
	
	rtn = f_accrual_106(0, mo, yr)
	if rtn < 0 then
		if rtn = -9 then
			i_msg[] = i_null_arrary[] 
			i_msg[1] = ": ERROR in f_accrual_106 for uknown i_company: "  
			of_logfile() 
		else
			i_msg[] = i_null_arrary[] 
			i_msg[1] = ": ERROR in f_accrual_106: " + sqlca.SQLErrText
			of_logfile() 
		end if 
		return -1
	end if   
end if 

//f_non_unitization_audit is a customizable function, which needs to be specifically designed to handle data analysis and updates for all eligible work orders
//when the work_order_id arguement is a 0, the code in this function need to handle the custom validations for all work order in mass
//the f_non_unitization_audit custom function should never have a commit inside
rtn = f_non_unitization_audit(0, custom_audit_error_msg)
if rtn < 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR in f_non_unitization_audit: " + sqlca.SQLErrText
	of_logfile() 
	return -1
end if 

//f_wo_audit_tax_all is a customizable function, which needs to be specifically designed to handle data analysis and updates for all eligible work orders
//when the work_order_id arguement is a 0, the code in this function need to handle the custom validations for all work order in mass
//the f_wo_audit_tax custom function should never have a commit inside
rtn = f_wo_audit_tax("AUTO106", 0, custom_audit_error_msg)
if rtn < 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR in f_wo_audit_tax: " + sqlca.SQLErrText
	of_logfile() 
	return -1
end if 

//Tag and Filter out work orders that have errors thus far  
check_count = 0 
select count(1) into :check_count
from wo_AUTO106_wos where error_msg is not null;

if check_count > 0 then
	rtn = of_errors_update('WO')
	
	if rtn < 0 then
		return -1
	end if 
end if 

/////////////////////////////////////
//Run Unit Item level validations
////////////////////////////////////

//GL ACCOUNTS
check_unit_account = f_pp_system_control_company('Auto 106 - Check Accounts', i_co_id)
if isnull(check_unit_account) or check_unit_account = '' then check_unit_account = 'yes'

if check_unit_account = 'yes' then
	update wo_AUTO106_pending_trans a set a.error_msg = decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 303: The Non Unitized Account cannot be the same as the Unitized Account' 
	where exists (select 1 from work_order_account b
					where a.work_order_id = b.work_order_id
					and b.non_unitized_gl_account = unitized_gl_account);
		
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =": ERROR: error_msg(GL): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 

//WO DESCRIPTION
check_count = 0
select count(1) into :check_count
from wo_AUTO106_pending_trans where wo_description is null;

if check_count > 0 then 
	update wo_AUTO106_pending_trans a set a.error_msg = decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 318: WO Description is blank' 
	where wo_description is null; 
		
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =": ERROR: error_msg(Desc): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  
end if 

//ASSET LOCATION  (AL on Unit Item has already been defaulted from WORK_ORDER_CONTROL table if missing on Charges/Estimates)
check_count = 0
select count(1) into :check_count
from wo_AUTO106_pending_trans where asset_location_id is null;

if check_count > 0 then  
	update wo_AUTO106_pending_trans a
	set a.asset_location_id = (select min(c.asset_location_id) from work_order_control b, asset_location c
			where a.work_order_id = b.work_order_id
			and b.major_location_id = c.major_location_id
			group by c.major_location_id)
	where a.asset_location_id is null;		
	
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating asset_location_id(1): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
end if 

check_count = 0
select count(1) into :check_count
from wo_AUTO106_pending_trans where asset_location_id is null;

if check_count > 0 then 
	update wo_AUTO106_pending_trans a set a.error_msg = decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 309: No Asset Location found for WO *Add Location on WO or Estimates*' 
	where asset_location_id is null; 
		
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating asset_location_id(309): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 

//BUSINESS SEGMENT  (BS on Unit Item has already been defaulted from WORK_ORDER_CONTROL table if missing on Charges/Estimates and BS should never be NULL on WOC)
check_count = 0
select count(1) into :check_count
from wo_AUTO106_pending_trans where bus_segment_id is null; 
	
if check_count > 0 then 

	update wo_AUTO106_pending_trans a set a.error_msg = decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 310: No Bus Segment found for WO' 
	where bus_segment_id is null;	 
	
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating asset_location_id(310): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
end if  

//SUB ACCOUNT
check_count = 0
select count(1) into :check_count
from wo_AUTO106_pending_trans where sub_account_id is null;

if check_count > 0 then
	update wo_AUTO106_pending_trans a
	set a.sub_account_id = (select min(b.sub_account_id) from sub_account b
			where a.utility_account_id = b.utility_account_id
			and a.bus_segment_id = b.bus_segment_id)
	where a.sub_account_id is null;
		
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating sub_account_id(1): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
	
	check_count = 0
	select count(1) into :check_count
	from wo_AUTO106_pending_trans where sub_account_id is null;
	
	if check_count > 0 then   

		update wo_AUTO106_pending_trans a 
		set a.error_msg = substrb(decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 307: No Sub Account for UA ('||(select trim(b.description) from utility_account b 
																													where a.utility_account_id = b.utility_account_id
																													and a.bus_segment_id = b.bus_segment_id)||') *Review Utility Account Setup*' , 1, 2000)
		where sub_account_id is null;	 
		
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: updating sub_account_id(307): " + sqlca.SQLErrText
			of_logfile() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
	end if 
end if 

//verify SUB_ACCOUNT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID combination
update wo_AUTO106_pending_trans a 
set a.error_msg = substrb(decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 318: Invalid sub_account_id for UA/BS ('||to_char(a.sub_account_id)||' for '||
																									(select trim(c.description) from utility_account c 
																									where a.utility_account_id = c.utility_account_id
																									and a.bus_segment_id = c.bus_segment_id)||'/'||
																									(select trim(d.description) from business_segment d
																									where a.bus_segment_id = d.bus_segment_id)||') *Review Accounts*'	, 1, 2000)	
where not exists (select 1
from sub_account x
where x.sub_account_id = a.sub_account_id
and x.utility_account_id = a.utility_account_id
and x.bus_segment_id = a.bus_segment_id);
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating sub_account_id(318): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//FUNC CLASS
update wo_AUTO106_pending_trans a
set a.ua_func_class_id = (select b.func_class_id from utility_account b
			where a.utility_account_id = b.utility_account_id
			and a.bus_segment_id = b.bus_segment_id);
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating ua_func_class_id(1): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows
			
check_count = 0
select count(1) into :check_count
from wo_AUTO106_pending_trans where ua_func_class_id is null;

if check_count > 0 then	 

	update wo_AUTO106_pending_trans a 
	set a.error_msg = substrb(decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 311: No Func Class for UA('||(select trim(b.description) from utility_account b 
																													where a.utility_account_id = b.utility_account_id
																													and a.bus_segment_id = b.bus_segment_id)||') *Review Utility Account Setup*' , 1, 2000)																											
	where ua_func_class_id is null;	 
	
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: error_msg(FC 311): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 
	
//PROPERTY GROUP  (dw_wo_ru_prop_group)  
//property_group_id is inserted as NULL from the start
update wo_AUTO106_pending_trans a
set a.property_group_id = (select min(b.property_group_id) from prop_group_prop_unit b, func_class_prop_grp c, retirement_unit d
		where a.ua_func_class_id = c.func_class_id
		and d.retirement_unit_id = 1 
		and b.property_unit_id = d.property_unit_id
		and c.property_group_id = b.property_group_id
		)
where a.property_group_id is null
and a.ua_func_class_id is not null;
	
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating property_group_id(1): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

check_count = 0
select count(1) into :check_count
from wo_AUTO106_pending_trans where property_group_id is null;

if check_count > 0 then 

	update wo_AUTO106_pending_trans a 
	set a.error_msg = substrb(decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 308: No Property Group for Non Unitized Retirement Unit and Func Class ('||(select trim(c.description) from func_class c
																														where c.func_class_id = a.ua_func_class_id)||') *Review Property Unit Catalog & Functional Class/Property Group table*' , 1, 2000)
	where a.property_group_id is null
	and a.ua_func_class_id is not null;	 

	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating property_group_id(308): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  
end if 
 
	
//LT/FC combo  
update wo_AUTO106_pending_trans a 
set a.error_msg = substrb(decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 316: Invalid Location Type for Func Class ('||(select a.description
																								from location_type a, major_location b, asset_location c
																								where a.location_type_id = b.location_type_id  
																								and b.major_location_id = c.major_location_id 
																								and c.asset_location_id = a.asset_location_id)||' for '||
																								(select trim(d.description) from func_class d
																								where d.func_class_id = a.ua_func_class_id)||') *Review Functional Class/Location Type table*', 1, 2000)																													
where a.asset_location_id is not null 
and a.ua_func_class_id is not null 
and not exists (select 1
					from func_class_loc_type x, asset_location y, major_location z
					where a.asset_location_id = y.asset_location_id
					and a.ua_func_class_id = x.func_class_id 
					and y.major_location_id = z.major_location_id
					and x.location_type_id = z.location_type_id
					);
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: error_msg(LT/FC): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 	
					
//Asset Loc/Company combo  
update wo_AUTO106_pending_trans a 
set a.error_msg = substrb(decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 312: Invalid Asset Loc for Company ('||(select trim(c.long_description) from asset_location c
																					where c.asset_location_id = a.asset_location_id)||' for '||to_char(:i_co_desc)||' *Review Major Location Company Security Setup*', 1, 2000)				
where a.asset_location_id is not null  
and not exists (select 1
					from asset_location d, major_location e, company_major_location f
					where a.asset_location_id = d.asset_location_id 
					and e.major_location_id = d.major_location_id
					and e.major_location_id = f.major_location_id
					and f.company_id = a.company_id
					and f.company_id = :i_co_id
					);
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: error_msg(Asset Loc/Company): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 
	
//Tag and Filter out work orders that have errors thus far
check_count = 0  
select count(1) into :check_count
from wo_AUTO106_pending_trans where error_msg is not null;

if check_count > 0 then
	rtn = of_errors_update('UNITS')
	
	if rtn < 0 then
		return -1
	end if 
end if 

return 1
end function

public function longlong of_class_codes ();longlong num_nrows, check_count

//Class Codes... 
i_msg[] = i_null_arrary[]
i_msg[1] =    ": Processing Class Codes..."
of_logfile() 

//wo_AUTO106_class_codes is a global temp table 
delete from wo_AUTO106_class_codes;
		
if sqlca.SQLCode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ": ERROR: wo_AUTO106_class_codes delete: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows	

//insert using work_order_class_code modeled after dw_wo_class_code_update_args used in regular Auto 106 
insert into wo_AUTO106_class_codes (work_order_id, MAX_UNIT_ID, class_code_id, cc_value, time_stamp, user_id)
select distinct wc.work_order_id, t.MAX_UNIT_ID, wc.class_code_id, wc.value , sysdate, :i_user_id1
from work_order_class_code wc, class_code a,  wo_AUTO106_pending_trans t
where wc.work_order_id = t.work_order_id   
/*and t.error_msg is null <-- comment out: validate class codes for ones that already have other data errors*/ 
and wc.class_code_id = a.class_code_id  
and a.cpr_indicator  = 1     
and not exists (select 1 
				from wo_AUTO106_class_codes x
				where x.work_order_id = t.work_order_id
				and x.MAX_UNIT_ID = t.MAX_UNIT_ID
				and x.class_code_id = wc.class_code_id
				and x.cc_value = wc.value) ;
				
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =   ": ERROR: wo_AUTO106_class_codes insert(1): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows	
						
//insert using class codes in class_code_default, where the class_codes above takes priority over class_code_default
check_count = 0
select count(1) into :check_count
from class_code_default 
where property_unit_id in (select property_unit_id from retirement_unit where retirement_unit_id = 1);

if check_count > 0 then
	insert into wo_AUTO106_class_codes (work_order_id, MAX_UNIT_ID,  class_code_id, cc_value, time_stamp, user_id)
	select a.work_order_id, a.MAX_UNIT_ID, c.class_code_id, c.value, sysdate, :i_user_id1
	from wo_AUTO106_pending_trans a, retirement_unit b, class_code_default c, class_code d
	where b.retirement_unit_id  = 1 
	/*and a.error_msg is null <-- comment out: validate class codes for ones that already have other data errors*/ 
	and b.property_unit_id = c.property_unit_id   
	and c.class_code_id = d.class_code_id  
	and d.cwip_indicator = 0 and d.cpr_indicator = 1    
	and not exists (select 1 
					from wo_AUTO106_class_codes x
					where x.work_order_id = a.work_order_id
					and x.MAX_UNIT_ID = a.MAX_UNIT_ID
					and x.class_code_id = c.class_code_id
					and x.cc_value = c.value) ;
			
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[]
		i_msg[1] =   ": ERROR: wo_AUTO106_class_codes insert(3): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
end if 

////COMMENT THIS OUT FOR NOW...
////Check for required class codes, do not use class code security for the required check			
//			
//f_wo_status_box("", string(now()) + ": Checking Required Class Codes for WO...")
////Check the required class codes from class_code table
//check_count = 0
//select count(*) into :check_count
//from class_code where cpr_indicator = 1 and required  = 1;
//
//if check_count > 0 then 
//
//	update wo_AUTO106_pending_trans x
//	set x.error_msg = decode(x.error_msg, null, '', x.error_msg||' :: ' )||'Required WO class code = '||
//		(select cc_description
//		from(
//			(select cc_table.description cc_description, work_order_id, MAX_UNIT_ID
//			 from class_code cc_table, (
//			  select min(class_code_id) min_cc_id , work_order_id,MAX_UNIT_ID
//					from (
//						SELECT work_order_id, class_code_id, MAX_UNIT_ID
//						from (
//						select b.work_order_id, cc.class_code_id class_code_id, b.MAX_UNIT_ID 
//						from wo_AUTO106_pending_trans b, (select class_code_id from class_code where cpr_indicator = 1 and required  = 1) cc 
//						) required_cc
//						MINUS
//						SELECT bcc.work_order_id, bcc.class_code_id, bcc.MAX_UNIT_ID
//						from wo_AUTO106_class_codes bcc 
//						) 
//				group by work_order_id,MAX_UNIT_ID) missing_cc
//			where missing_cc.min_cc_id = cc_table.class_code_id)) missing_cc2
//
//			where missing_cc2.work_order_id = x.work_order_id
//			and missing_cc2.MAX_UNIT_ID = x.MAX_UNIT_ID 
//			)||' is missing *Review CC requirements*' 
//	where x.MAX_UNIT_ID in ( select MAX_UNIT_ID
//						from (
//							SELECT work_order_id, class_code_id, MAX_UNIT_ID
//							from (
//							select b.work_order_id, cc.class_code_id class_code_id, b.MAX_UNIT_ID 
//							from wo_AUTO106_pending_trans b, (select class_code_id from class_code where cpr_indicator = 1 and required  = 1) cc 
//							) required_cc
//							MINUS
//							SELECT bcc.work_order_id, bcc.class_code_id, bcc.MAX_UNIT_ID
//							from wo_AUTO106_class_codes bcc 
//							) 
//						);
//												
//	if sqlca.SQLCode <> 0 then
//		f_wo_status_box("", string(now()) + ": ERROR: error_msg(Required CC1): " + sqlca.SQLErrText)
//		rollback;		 
//		return -1
//	end if		
//	num_nrows = sqlca.sqlnrows	 	
//end if
//
//f_wo_status_box("", string(now()) + ": Checking Required Class Codes for Property Units...")
////Check the required class codes from class_code_display table
//check_count = 0
//select count(*) into :check_count
//from class_code_display
//where required = 1 
//and property_unit_id in (select property_unit_id from retirement_unit where retirement_unit_id = 1);
//
//if check_count > 0 then 
//	update wo_AUTO106_pending_trans x
//	set x.error_msg = decode(x.error_msg, null, '', x.error_msg||' :: ' )||'Required PU class code = '||
//		(select cc_description
//		from(
//			(select cc_table.description cc_description, work_order_id, MAX_UNIT_ID
//			 from class_code cc_table, (
//			  select min(class_code_id) min_cc_id , work_order_id,MAX_UNIT_ID
//					from ( 					
//							select b.work_order_id, ccd.class_code_id class_code_id, b.MAX_UNIT_ID  
//							from wo_AUTO106_pending_trans b, retirement_unit ru, class_code_display ccd  
//							where ccd.required = 1 
//							and ru.retirement_unit_id = 1 
//							and ru.property_unit_id = ccd.property_unit_id (+)  
//							MINUS
//							SELECT bcc.work_order_id, bcc.class_code_id, bcc.MAX_UNIT_ID
//							from wo_AUTO106_class_codes bcc 
//							) 
//					group by work_order_id,MAX_UNIT_ID) missing_cc
//			where missing_cc.min_cc_id = cc_table.class_code_id)) missing_cc2
//	
//			where missing_cc2.work_order_id = x.work_order_id
//			and missing_cc2.MAX_UNIT_ID = x.MAX_UNIT_ID 
//			)||' is missing *Review CC requirements*' 
//	where x.MAX_UNIT_ID in ( select MAX_UNIT_ID
//							from ( 
//								select b.work_order_id, ccd.class_code_id class_code_id, b.MAX_UNIT_ID  
//								from wo_AUTO106_pending_trans b, retirement_unit ru, class_code_display ccd  
//								where ccd.required = 1 
//								and ru.retirement_unit_id = 1
//								and ru.property_unit_id = ccd.property_unit_id (+)  
//								MINUS
//								SELECT bcc.work_order_id, bcc.class_code_id, bcc.MAX_UNIT_ID
//								from wo_AUTO106_class_codes bcc 
//								) 
//							);
//												
//	if sqlca.SQLCode <> 0 then
//		f_wo_status_box("", string(now()) + ": ERROR: error_msg(Required CC2): " + sqlca.SQLErrText)
//		rollback;		 
//		return -1
//	end if		
//	num_nrows = sqlca.sqlnrows	 	
//end if 
//
//	
////Take care of work orders that have errors thus far		 
//check_count = 0  
//select count(*) into :check_count
//from wo_AUTO106_pending_trans where error_msg is not null;
//
//if check_count > 0 then
//	rtn = of_errors_update('UNITS')
//	
//	if rtn < 0 then
//		return -1
//	end if 
//end if 

return 1
end function

public function longlong of_wos106 ();//of_wos106

longlong num_nrows

//Identifiy eligible work orders, use dw_wo_to_unitize_106

//global temporary table WO_AUTO106_WOS
delete from wo_AUTO106_wos;
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: wo_AUTO106_wos delete: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Maint 36229
insert into wo_AUTO106_wos (company_id, accounting_month, work_order_id, closing_option_id, wo_status_id, in_service_date, completion_date, late_chg_wait_period, non_unitized_gl_account, 
work_order_number, bus_segment_id, asset_location_id, time_stamp, user_id)
select woc.company_id, to_date(:i_mo_num, 'yyyymm'), woc.work_order_id, woa.closing_option_id, woc.wo_status_id, woc.in_service_date, woc.completion_date,  woc.late_chg_wait_period,
woa.non_unitized_gl_account, work_order_number, woc.bus_segment_id, woc.asset_location_id, sysdate, :i_user_id1
from work_order_control woc, work_order_account woa 
where woc.work_order_id = woa.work_order_id
and woc.funding_wo_indicator = 0 
and woc.company_id = :i_co_id 
and (
		(woa.CLOSING_OPTION_ID in (1,2) 
			and woc.wo_status_id in (4,5,8)    
			and woc.in_service_date is not null 
			and  to_number(to_char(in_service_date,'yyyymm')) <= :i_mo_num
		) 
	OR woa.CLOSING_OPTION_ID in (7,8,10) 
	)           
and exists (select 1 from cwip_charge 
         where cwip_charge.company_id = :i_co_id 
		and cwip_charge.expenditure_type_id = 1  
		and woa.work_order_id = cwip_charge.work_order_id 
		and cwip_charge.non_unitized_status is null 
		and cwip_charge.closed_month_number is null
		and cwip_charge.GL_ACCOUNT_ID IN (woa.cwip_gl_account, 1) 
		and cwip_charge.month_number <= :i_mo_num)   
and not exists (SELECT 1
				FROM wo_arc_results waro
				WHERE waro.arc_run_id = NVL((
				SELECT MAX (arc_run_id)
				FROM wo_arc_results wari
				WHERE waro.work_order_id = wari.work_order_id
				AND waro.arc_rule_id = wari.arc_rule_id
				)
				,0)
				AND (waro.pass_fail = 1
				AND NVL (waro.override, 0) = 0)
				AND waro.work_order_id = woc.work_order_id
				AND EXISTS (
				SELECT wac.wo_status_id
				, wac.arc_class_id
				, warc.arc_rule_id
				, wacco.closing_option_id
				, wacwot.work_order_type_id
				FROM wo_arc_class wac
				JOIN wo_arc_rule_class warc
				ON wac.arc_class_id = warc.arc_class_id
				JOIN wo_arc_class_cl_opt wacco
				ON wac.arc_class_id = wacco.arc_class_id
				JOIN wo_arc_class_wot wacwot
				ON wac.arc_class_id = wacwot.arc_class_id
				WHERE wac.non_unitized = 1 /*<--this needs to be changed for 106 vs 101 auto vs 101 manual*/
				AND woa.closing_option_id = wacco.closing_option_id
				AND woc.wo_status_id = wac.wo_status_id
				AND woc.work_order_type_id = wacwot.work_order_type_id
				AND waro.arc_rule_id = warc.arc_rule_id
				)
							
			)
;

//and not exists 
//	(select 1 from pend_transaction pt
//	where woc.work_order_number = pt.work_order_number
//	and woc.company_id = pt.company_id
//	and woc.funding_wo_indicator = 0
//	and trim(upper(pt.activity_code)) in ('UADD','MADD'))    
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: wo_AUTO106_wos insert: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Filter work orders out for December for Quarterly and Annual Close options as these need to go through Auto101
if right(string(i_mo_num), 2) = '12' then
	delete from wo_AUTO106_wos where closing_option_id in (7,8);
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: delete for closing_option_id in (7,8): " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
end if 

//Filter work orders out for March, June, Sept for Quarterly Close option as these need to go through Auto101
if right(string(i_mo_num), 2) = '03' or right(string(i_mo_num), 2) = '06' or right(string(i_mo_num), 2) = '09' then
	delete from wo_AUTO106_wos where closing_option_id =7;
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: delete for closing_option_id = 7: " + sqlca.SQLErrText
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
end if 

//Filter work orders out for Standard Close Auto option as these need to go through Auto101
delete from wo_AUTO106_wos where closing_option_id = 2
and completion_date is not null
and add_months(to_date(to_char(completion_date,'mm/yyyy'),'mm/yyyy'), nvl(late_chg_wait_period,0)) <= to_date(:i_mo_num, 'yyyymm');

if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete for standard close auto with complete dates: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1

end function

public function longlong of_wos101failed ();//of_wos101failed

longlong num_nrows

//Identifiy eligible work orders, use dw_wo_to_unitize_101 

//global temporary table WO_AUTO106_WOS
delete from wo_AUTO106_wos;
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: wo_AUTO106_wos delete: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into wo_AUTO106_wos (company_id, accounting_month, work_order_id, closing_option_id, wo_status_id, in_service_date, completion_date, late_chg_wait_period, non_unitized_gl_account, 
work_order_number, bus_segment_id, asset_location_id, time_stamp, user_id)
select woc.company_id, to_date(:i_mo_num, 'yyyymm'), woc.work_order_id, woa.closing_option_id, woc.wo_status_id, woc.in_service_date, woc.completion_date,  woc.late_chg_wait_period,
woa.non_unitized_gl_account, work_order_number,  woc.bus_segment_id, woc.asset_location_id, sysdate, :i_user_id1
from work_order_control woc, work_order_account woa 
where woc.work_order_id = woa.work_order_id
and woc.funding_wo_indicator = 0 
and woc.company_id = :i_co_id 
and (
		(woa.CLOSING_OPTION_ID = 2
			and woc.wo_status_id in (4,5)    
			and woc.completion_date is not null  
			and add_months(to_date(to_char(completion_date,'mm/yyyy'),'mm/yyyy'), nvl(late_chg_wait_period,0)) <= to_date(:i_mo_num, 'yyyymm')
		) 
	OR woa.CLOSING_OPTION_ID in (6,7,8) 
	)       
and woc.wo_status_id not in (3,8)	
and exists (select 1 from cwip_charge 
         where cwip_charge.company_id = :i_co_id 
		and cwip_charge.expenditure_type_id = 1  
		and woa.work_order_id = cwip_charge.work_order_id 
		and cwip_charge.non_unitized_status is null 
		and cwip_charge.closed_month_number is null
		and cwip_charge.GL_ACCOUNT_ID IN (woa.cwip_gl_account, 1) 
		and cwip_charge.month_number <= :i_mo_num)   

;

//and not exists 
//	(select 1 from pend_transaction pt
//	where woc.work_order_number = pt.work_order_number
//	and woc.company_id = pt.company_id
//	and woc.funding_wo_indicator = 0
//	and trim(upper(pt.activity_code)) in ('UADD','MADD'))     
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: wo_AUTO106_wos insert (101failed): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

 

return 1

end function

public function longlong of_check_pending ();longlong num_nrows, check_count, rtn 


i_msg[] = i_null_arrary[]
i_msg[1] =    ": Checking Pend Transactions..."
of_logfile() 

//Identify work orders with closing pending transactions that might have been made during this processing
update wo_AUTO106_wos x
set x.error_msg = decode(x.error_msg, null, '', x.error_msg||' :: ' )||'Error 302: other Addition type pending transactions found. *Delete or post transactions*' 
where exists 
	(select 1 from pend_transaction pt
	where x.work_order_number = pt.work_order_number
	and x.company_id = pt.company_id 
	and trim(upper(pt.activity_code)) in ('UADD','MADD') 
	);
		
if sqlca.SQLCode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =": ERROR: wo_AUTO106_wos insert: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Tag and Filter out work orders that have errors thus far  
check_count = 0 
select count(1) into :check_count
from wo_AUTO106_wos where error_msg is not null;

if check_count > 0 then
	rtn = of_errors_update('WO')
	
	if rtn < 0 then
		return -1
	end if 
end if 

return 1
end function

public function longlong of_balancing ();string basis_sqls, sqls
longlong i, num_nrows, check_count, rtn

//Balance back to cwip_charge by gl_account_id... 
i_msg[] = i_null_arrary[]
i_msg[1] =   ": Balancing..." 
of_logfile() 

//get the basis summarization, limit the summarization by num_buckets
basis_sqls = ''
for i = 1 to i_num_buckets 
	basis_sqls = basis_sqls + 'nvl(basis_' + string(i) + ',0) +'
next

basis_sqls = mid(basis_sqls, 1, len(basis_sqls) -1) 

//update the total_amount, which is just the mathematical sum of all the buckets
sqls = "UPDATE wo_AUTO106_pending_trans set total_amount =  " + basis_sqls 

execute immediate :sqls;
if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating wo_AUTO106_pending_trans.total_amount: ' + sqlca.sqlerrtext 
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows  

delete from wo_AUTO106_temp_wos;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = 'Error: wo_AUTO106_temp_wos delete: ' + sqlca.sqlerrtext 
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 

//USE WO_AUTO106_TEMP_WOS because it is cleaner and possibly faster to insert into WO_AUTO106_WOS
//    from WO_AUTO106_TEMP_WOS than to repeat the same data compare in the Update as in the subselect filter
insert into WO_AUTO106_TEMP_WOS (work_order_id, notes)
select charges.work_order_id,  'Error 306: Charge amount = '||charges.charge_amount||' does not equal Allocated amount = '||units.allocated_amount  
from 
	(select a.work_order_id, sum(a.amount) charge_amount 
	from wo_auto106_charges a
	where a.company_id = :i_co_id 
	and a.accounting_month = to_date(:i_mo_num, 'yyyymm') 
	and a.run = :i_run_id 
	group by a.work_order_id) charges,
	
	(select work_order_id, sum(total_amount)  allocated_amount
	from wo_AUTO106_pending_trans 
	group by work_order_id ) units

where charges.work_order_id = units.work_order_id
and charges.charge_amount <> units.allocated_amount ;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = 'Error: wo_AUTO106_temp_wo insert(balance1): ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 

update WO_AUTO106_WOS x 
set x.error_msg = decode(x.error_msg, null, '', x.error_msg||' :: ' )||(select a.notes
						from wo_AUTO106_temp_wos a
						where x.work_order_id = a.work_order_id)
where x.work_order_id in (select a.work_order_id from wo_AUTO106_temp_wos a); 

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = 'Error: updating wo_AUTO106_wos(balance2): ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 	
	
//Tag and Filter out work orders that have errors thus far 
check_count = 0 
select count(1) into :check_count
from wo_AUTO106_wos where error_msg is not null;

if check_count > 0 then
	rtn = of_errors_update('WO')
	
	if rtn < 0 then
		return -1
	end if 
end if 

return 1 

end function

public function longlong of_balancing_wip_comp ();string basis_sqls, sqls
longlong i, num_nrows, check_count, rtn

//Balance back to cwip_charge by for wip comp.. 
i_msg[] = i_null_arrary[]
i_msg[1] =   ": Balancing WIP Computations..."
of_logfile() 

delete from wo_AUTO106_temp_wos;

if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: wo_AUTO106_temp_wos delete: ' + sqlca.sqlerrtext 
	of_logfile()
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 

//USE WO_AUTO106_TEMP_WOS because it is cleaner and possibly faster to insert into WO_AUTO106_WOS
//    from WO_AUTO106_TEMP_WOS than to repeat the same data compare in the Update as in the subselect filter
insert into wo_AUTO106_temp_wos (work_order_id, notes)
select charges.work_order_id,  'Error 320: WIP Comp Charge amount = '||charges.charge_amount||' does not equal Allocated amount = '||units.allocated_amount  
from 
	(select a.work_order_id, sum(a.amount) charge_amount 
	from cwip_charge a, wo_AUTO106_wos c, wip_comp_charges d
	where c.work_order_id = a.work_order_id
	and a.expenditure_type_id = 5   
	and d.month_number <= :i_mo_num
	and a.company_id = :i_co_id 
	and d.current_gl_account = d.wip_gl_account
	and d.non_unitized_status = 100  
	and d.closed_month_number is null 
	and d.work_order_id = a.work_order_id  
	and d.charge_id = a.charge_id
	and d.company_id = a.company_id
	group by a.work_order_id) charges,
	
	(select c.work_order_id, sum(nvl(basis_1,0) + nvl(basis_2,0) + nvl(basis_3,0) + nvl(basis_4,0) +  nvl(basis_5,0) + 
	nvl(basis_6,0) + nvl(basis_7,0) + nvl(basis_8,0)  +  nvl(basis_9,0) + nvl(basis_10,0)  + 
	nvl(basis_11,0) + nvl(basis_12,0) + nvl(basis_13,0) + nvl(basis_14,0)  +  nvl(basis_15,0) + 
	nvl(basis_16,0) + nvl(basis_17,0) + nvl(basis_18,0)  + nvl(basis_19,0) + nvl(basis_20,0)  + 
	nvl(basis_21,0) + nvl(basis_22,0) + nvl(basis_23, 0) + nvl(basis_24,0)  +  nvl(basis_25, 0) + 
	nvl(basis_26,0) + nvl(basis_27,0) + nvl(basis_28,0)  + nvl(basis_29,0) + nvl(basis_30,0)  + 
	nvl(basis_31,0) + nvl(basis_32,0)  +  nvl(basis_33, 0) + nvl(basis_34,0) + nvl(basis_35, 0) + 
	nvl(basis_36,0) + nvl(basis_37,0) + nvl(basis_38,0)  +   nvl(basis_39,0) + nvl(basis_40,0)  + 
	nvl(basis_41,0) + nvl(basis_42,0)  +  nvl(basis_43, 0) + nvl(basis_44,0) + nvl(basis_45, 0) + 
	nvl(basis_46,0) + nvl(basis_47,0) + nvl(basis_48,0)  +   nvl(basis_49,0) + nvl(basis_50,0)  + 
	nvl(basis_51,0) + nvl(basis_52,0)  +  nvl(basis_53, 0) + nvl(basis_54,0) + nvl(basis_55, 0) + 
	nvl(basis_56,0) + nvl(basis_57,0) + nvl(basis_58,0)  +   nvl(basis_59,0) + nvl(basis_60,0) +
	nvl(basis_61,0) + nvl(basis_62,0)  +  nvl(basis_63, 0) + nvl(basis_64,0) + nvl(basis_65, 0) + 
	nvl(basis_66,0) + nvl(basis_67,0) + nvl(basis_68,0)  +   nvl(basis_69,0) + nvl(basis_70,0))  allocated_amount
	from pend_basis a, pend_transaction b, wo_AUTO106_wos c
	where a.pend_trans_id = b.pend_trans_id
	and c.work_order_number = b.work_order_number
	and c.company_id = b.company_id
	and TRIM(UPPER(b.activity_code)) = 'UADD' 
	and b.retirement_unit_id = 1  
	and b.wip_comp_transaction = 1
	group by c.work_order_id ) units

where charges.work_order_id = units.work_order_id
and charges.charge_amount <> units.allocated_amount ;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: wo_AUTO106_temp_wo insert(wip balance 1): ' + sqlca.sqlerrtext
	of_logfile()
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 

update wo_AUTO106_wos x 
set x.error_msg = decode(x.error_msg, null, '', x.error_msg||' :: ' )||(select a.notes
						from wo_AUTO106_temp_wos a
						where x.work_order_id = a.work_order_id)
where x.work_order_id in (select a.work_order_id from wo_AUTO106_temp_wos a); 

if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: wo_AUTO106_temp_wo insert(wip balance 2): ' + sqlca.sqlerrtext
	of_logfile()
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 	

//Check if any failed 
check_count = 0 
select count(1) into :check_count
from wo_AUTO106_wos where error_msg is not null;

if check_count > 0 then

	//Need to remove already inserted data from the pend transaction tables for the regular 107 closings
	rtn = of_remove_transactions()
	if rtn < 0 then
		return -1
	end if 
	
end if //check_count > 0 

return 1 

end function

public function longlong of_remove_transactions ();longlong num_nrows, rtn

//Need to undo updates on the WIP COMP tables
update cwip_charge a 
set a.non_unitized_status = null
where a.non_unitized_status = 100 
and a.company_id = :i_co_id
and a.expenditure_type_id = 1
and exists (select 1 from wo_AUTO106_wos b 
				where b.work_order_id = a.work_order_id 
				and b.error_msg is not null)
and exists (select 1 from WO_AUTO106_CHARGES c
		where c.company_id = :i_co_id
		and a.work_order_id = c.work_order_id
		and a.charge_id = c.charge_id
		and c.accounting_month = to_date(:i_mo_num, 'yyyymm')
		and c.run = :i_run_id);		 

if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = 'Error: updating cwip_charge.non_unitized_status = null: ' + sqlca.sqlerrtext 
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 	

//Data in WO_AUTO106_CHARGES will be removed in of_errors_update() for the regular CWIP charges.
//Data in WO_AUTO106_CHARGES_SUMM will be removed in of_errors_update() for the regular CWIP charges. 


//Need to undo updates on the WIP COMP tables
update wip_comp_charges a 
set a.non_unitized_status = null
where a.non_unitized_status = 100 
and exists (select 1 from wo_AUTO106_wos b 
				where b.work_order_id = a.work_order_id 
				and b.error_msg is not null)
and exists (select 1 from WO_AUTO106_CHARGES c
		where c.company_id = :i_co_id
		and a.work_order_id = c.work_order_id
		and a.charge_id = c.charge_id
		and a.wip_computation_id = c.wip_computation_id
		and c.accounting_month = to_date(:i_mo_num, 'yyyymm')
		and c.run = :i_run_id);		 

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = 'Error: updating wip_comp_charges.non_unitized_status = null: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 		

update wip_comp_unit_calc  a 
set a.pend_transaction  = null
where a.pend_transaction = 100 
and a.unit_type = 'non-unitized'	
and exists (select 1 from wo_AUTO106_wos c 
				where c.work_order_id = a.work_order_id 
				and c.error_msg is not null);

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = 'Error: updating wip_comp_charges.pend_transaction = null: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 			

update wip_comp_pending_trans a 
set a.pend_transaction  = null
where a.pend_transaction = 100 
and exists (select 1 from wo_AUTO106_wos c 
				where c.work_order_id = a.work_order_id 
				and c.error_msg is not null);

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = 'Error: updating wip_comp_pending_trans.pend_transaction = null: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 	

///////comment this out, the wip comp 106 closings does not insert into pend_related_asset.  106 assets should never be related.
//delete from pend_related_asset a 
//where exists (select 1 from wip_comp_pending_trans b, wo_AUTO106_wos c 
//				where a.pend_related_asset_id = b.new_pend_trans_id
//				and c.work_order_id = b.work_order_id 
//				and b.pend_transaction = 100 
//				and c.error_msg is not null);
//
//if sqlca.sqlcode <> 0 then 
//	i_msg[] = i_null_arrary[] 
//	i_msg[1] ='Error: deleting pend_related_asset(wip balance): ' + sqlca.sqlerrtext
//	of_logfile() 
//	rollback;		 
//	return -1
//end if
//num_nrows = sqlca.sqlnrows 	

//Need to remove already inserted pending data
delete from class_code_pending_trans a 
where exists (select 1 from pend_transaction b, wo_AUTO106_wos c 
				where a.pend_trans_id = b.pend_trans_id
				and c.work_order_number = b.work_order_number
				and c.company_id = b.company_id
				and b.retirement_unit_id = 1
				and b.posting_status <> 2
				and b.company_id = :i_co_id
				and c.error_msg is not null);

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] ='Error: deleting class_code_pending_trans(wip balance): ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 	


delete from pend_basis a 
where exists (select 1 from pend_transaction b, wo_AUTO106_wos c 
				where a.pend_trans_id = b.pend_trans_id
				and c.work_order_number = b.work_order_number
				and c.company_id = b.company_id
				and b.retirement_unit_id = 1
				and b.posting_status <> 2
				and b.company_id = :i_co_id
				and c.error_msg is not null);

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = 'Error: deleting pend_basis(wip balance): ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 	

delete from pend_transaction b 
where b.retirement_unit_id = 1
and b.posting_status <> 2
and b.company_id = :i_co_id
and exists (select 1 from wo_AUTO106_wos c 
				where c.work_order_number = b.work_order_number
				and c.company_id = b.company_id
				and c.error_msg is not null);

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] ='Error: deleting pend_transaction(wip balance): ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 	

//Take care of work orders that have errors 		 
rtn = of_errors_update('WO')

if rtn < 0 then
	return -1
end if 

return 1 

end function

public function longlong of_check_charge_data ();longlong num_nrows, check_count, rtn


i_msg[] = i_null_arrary[]
i_msg[1] =    ": Checking Charge Amounts..."
of_logfile() 

//filter and remove work orders from wo_AUTO106_wos where no eligible charges found
//not tagging and saving these as errors to show why a particular work order did not close to 106.  
//these work orders should simply be removed from the processing.  the old Auto106 simply skipped the work order without errors, too.
 delete from wo_AUTO106_wos a  
 where not exists 
 		(select 1 from WO_AUTO106_CHARGES_SUMM b
		 where a.work_order_id = b.work_order_id);
		
if sqlca.SQLCode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: wo_AUTO106_wos(no charges): " + sqlca.SQLErrText 
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//identify work orders that will be skipped in Auto106 because the charge amounts total is zero
//old Auto106 would simply skip these work order, but Support gets this question alot.
//it is better to tag the work order with an error message for why the wo did not qualify to close to 106 in this situation.
update wo_AUTO106_wos a 
set a.error_msg = decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Info 299: CWIP amount = $0 *No eligible charges to close*' 
where exists 
	(select 1 from WO_AUTO106_CHARGES_SUMM b
	 where a.work_order_id = b.work_order_id
	 group by b.work_order_id 
	 having sum(abs(total_amount)) = 0 );
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: wo_AUTO106_wos(299): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 
	
//Tag and Filter out work orders that have errors thus far 
check_count = 0 
select count(1) into :check_count
from wo_AUTO106_wos where error_msg is not null;

if check_count > 0 then
	rtn = of_errors_update('WO')
	
	if rtn < 0 then
		return -1
	end if 
end if 

return 1
end function

public function longlong of_allocation ();longlong num_nrows, i, check_count 
string sqls

//ALLOCATE TO UNIT ITEMS BY BOOK BASIS 
i_msg[] = i_null_arrary[]
i_msg[1] =   ": Assigning Charges..."
of_logfile()  

//Check if there are Unit Items created from Charges to perform direct assigns first
check_count = 0
select count(1) into :check_count
from wo_AUTO106_PENDING_TRANS
where SOURCE = 'charges' ;

if check_count > 0 then
	
	//bus segment and asset location were already defaulted from initial insert into wo_auto106_charges from work_order_control
	//sub_account_id and 2nd pass of asset_location_id need to be done because asset_location_id is not required on work_order_control 
	
	update wo_auto106_charges a 
	set a.sub_account_id = (select min(b.sub_account_id) from wo_AUTO106_pending_trans b
									where a.work_order_id = b.work_order_id
									and a.utility_account_id = b.utility_account_id
									and a.bus_segment_id = b.bus_segment_id
									and b.source = 'charges'
									group by b.work_order_id)
	where a.sub_account_id is null
	and a.accounting_month =  to_date(:i_mo_num, 'yyyymm')
	and a.run = :i_run_id;
//	and exists (select 1 from wo_AUTO106_pending_trans c
//					where a.work_order_id = c.work_order_id
//					and c.source = 'charges') ;
	
	if sqlca.sqlcode <> 0 then 
		i_msg[] = i_null_arrary[]
		i_msg[1] =   'Error: updating wo_auto106_charges for sub_account_id' +string(i) + ' :: ' + sqlca.sqlerrtext 
		of_logfile()  
		rollback;		 
		return -1 
	end if
	num_nrows = sqlca.sqlnrows	
	
	update wo_auto106_charges a 
	set a.asset_location_id = (select min(b.asset_location_id) from wo_AUTO106_pending_trans b
									where a.work_order_id = b.work_order_id
									and a.utility_account_id = b.utility_account_id
									and a.bus_segment_id = b.bus_segment_id
									and a.sub_account_id = b.sub_account_id
									and b.source = 'charges'
									group by b.work_order_id)
	where a.asset_location_id is null
	and a.accounting_month =  to_date(:i_mo_num, 'yyyymm')
	and a.run = :i_run_id;
//	and exists (select 1 from wo_AUTO106_pending_trans c
//					where a.work_order_id = c.work_order_id
//					and c.source = 'charges') ;
	
	if sqlca.sqlcode <> 0 then 
		i_msg[] = i_null_arrary[]
		i_msg[1] =   'Error: updating wo_auto106_charges for asset_location_id' +string(i) + ' :: ' + sqlca.sqlerrtext
		of_logfile()  
		rollback;		 
		return -1 
	end if
	num_nrows = sqlca.sqlnrows	
	
	//Start Direct Assign for each book summary 
	for i = 1 to i_num_buckets 
		i_msg[] = i_null_arrary[]
		i_msg[1] =    ":   direct assigning basis " + string(i) + "..."
		of_logfile()  
	
		//Peform direct assign where utility account, bus segment, sub account, and asset location match for the work order
		sqls = "update wo_AUTO106_pending_trans a  " + &
			" set a.basis_" + string(i) + " = nvl((select sum(b.amount) from wo_auto106_charges b  " + &
			"			where a.work_order_id = b.work_order_id " + &
			"			and a.utility_account_id = b.utility_account_id " + &
			"			and a.bus_segment_id = b.bus_segment_id " + &
			"			and a.sub_account_id = b.sub_account_id " + &
			"			and a.asset_location_id = b.asset_location_id " + &
			"			and to_number(to_char(b.accounting_month, 'yyyymm')) =  " + string(i_mo_num) + " " + &
			"			and b.run = " + string(i_run_id) + " " + &
			"			and a.source = 'charges' " + &
			"			and b.book_summary_id = " + string(i) + " " + & 
			"			group by b.work_order_id),0) " + &
			" where a.source = 'charges' "
			
		execute immediate :sqls;
		if sqlca.sqlcode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1] =  'Error: (A)direct assign for basis=' +string(i) + ' :: ' + sqlca.sqlerrtext
			i_msg[2] =  'SQL string:  ' + sqls 
			of_logfile()  
			rollback;		 
			return -1 
		end if
		num_nrows = sqlca.sqlnrows
	
		//Need to refresh the data in wo_auto106_charges_summ table when direct assign has taken place for the book_summary_id
		//Delete summary data because  > 0% to 100% of the book_summary_id might have been direct assigned and should not be used for allocation later
		sqls = "delete from wo_auto106_charges_summ a  " + &
		"where a.book_summary_id = " + string(i) + " " + & 
		"and exists (select 1  " + &
			"			from wo_AUTO106_pending_trans b  " + &
			"			where a.work_order_id = b.work_order_id  " + &
			"			and b.source = 'charges'  " + &
			"			and nvl(b.basis_" + string(i) + ",0) <> 0)"
			
		execute immediate :sqls;
		if sqlca.sqlcode <> 0 then  
			i_msg[] = i_null_arrary[]
			i_msg[1] =  'Error: (B)direct assign for basis=' +string(i) + ' :: ' + sqlca.sqlerrtext
			i_msg[2] =  'SQL string:  ' + sqls 
			of_logfile()  
			rollback;		 
			return -1 
		end if
		num_nrows = sqlca.sqlnrows

	next

	//Insert the charge amounts grouped by book_summary_id for remaining charge dollars not directly assigned and needing to be allocated
	insert into WO_AUTO106_CHARGES_SUMM (company_id, accounting_month, work_order_id, book_summary_id, total_amount, time_stamp, user_id )
	select a.company_id, a.accounting_month, a.work_order_id, a.book_summary_id, sum(a.amount) total_amount, sysdate, :i_user_id1
	from WO_AUTO106_CHARGES a
	where a.company_id = :i_co_id 
	and a.accounting_month = to_date(:i_mo_num,'yyyymm') 
	and a.run = :i_run_id 
	and a.utility_account_id is null  
	and exists (select 1 from wo_AUTO106_pending_trans b
					where a.work_order_id = b.work_order_id
					and b.source = 'charges') 
	and not exists (select 1 from WO_AUTO106_CHARGES_SUMM c
						where a.work_order_id = c.work_order_id
						and a.book_summary_id = c.book_summary_id)
	group by a.company_id, a.accounting_month, a.work_order_id, a.book_summary_id
	having sum(a.amount) <> 0;
	
	if sqlca.sqlcode <> 0 then 
		i_msg[] = i_null_arrary[]
		i_msg[1] =  'Error: WO_AUTO106_CHARGES_SUMM insert for allo remaining:: ' + sqlca.sqlerrtext 
		of_logfile()  
		rollback;		 
		return -1 
	end if
	num_nrows = sqlca.sqlnrows

end if 	//if check_count for  SOURCE = 'charges'  > 1


//Delete unit items that have amount = 0 and there are other non-zero unit items
delete from wo_AUTO106_pending_trans a  
where exists (select 1
		from wo_AUTO106_pending_trans b
		where a.work_order_id = b.work_order_id		
		and b.amount <> 0 )
and a.amount = 0;
 
if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: delete wo_AUTO106_pending_trans for amount = 0' +string(i) + ' :: ' + sqlca.sqlerrtext
	of_logfile()  
	rollback;		 
	return -1 
end if
num_nrows = sqlca.sqlnrows

//Update ALLOC_STATISTIC_AMT to be 1 for work orders where there is only a single unit item record and the amount is zero 
update wo_AUTO106_pending_trans a 
set a.ALLOC_STATISTIC_AMT = 1
where exists (select 1
		from wo_AUTO106_pending_trans b
		where a.work_order_id = b.work_order_id		
		group by b.work_order_id
		having count(1) = 1)
and a.amount = 0;
 
if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating wo_AUTO106_pending_trans for ALLOC_STATISTIC_AMT(1)' +string(i) + ' :: ' + sqlca.sqlerrtext
	of_logfile()  
	rollback;		 
	return -1 
end if
num_nrows = sqlca.sqlnrows


//Update ALLOC_STATISTIC_AMT to be 1 for work orders where total dollars sum to zero, but exclude any where the amount = 0
update wo_AUTO106_pending_trans a 
set a.ALLOC_STATISTIC_AMT = 1
where exists (select 1
		from wo_AUTO106_pending_trans b
		where a.work_order_id = b.work_order_id
		group by b.work_order_id 
		having sum(b.amount) = 0);
 
if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating wo_AUTO106_pending_trans for ALLOC_STATISTIC_AMT(2)' +string(i) + ' :: ' + sqlca.sqlerrtext
	of_logfile()  
	rollback;		 
	return -1 
end if
num_nrows = sqlca.sqlnrows		

//Update ALLOC_STATISTIC_AMT to be 1 for work orders with both negative and postive dollars  
update wo_AUTO106_pending_trans a 
set a.ALLOC_STATISTIC_AMT = 1
where sign(amount) = 1 
and exists (select 1
		from wo_AUTO106_pending_trans b
		where a.work_order_id = b.work_order_id
		and sign(amount) = -1);
 
if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating wo_AUTO106_pending_trans for ALLOC_STATISTIC_AMT(3)' +string(i) + ' :: ' + sqlca.sqlerrtext
	of_logfile()  
	rollback;		 
	return -1 
end if
num_nrows = sqlca.sqlnrows		 

update wo_AUTO106_pending_trans a 
set a.ALLOC_STATISTIC_AMT = 1
where sign(amount) = -1 
and exists (select 1
		from wo_AUTO106_pending_trans b
		where a.work_order_id = b.work_order_id
		and sign(amount) = 1);
 
if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating wo_AUTO106_pending_trans for ALLOC_STATISTIC_AMT(4)' +string(i) + ' :: ' + sqlca.sqlerrtext
	of_logfile()  
	rollback;		 
	return -1 
end if
num_nrows = sqlca.sqlnrows		 

//Allocate for each basis bucket for work orders that have Unit Items created from Estimate and CPR
//   this update statement handles the rounding differences by sorting the allocation_statistic in desc order and applying the rounding diff amount to the biggest allocation statistic value.
for i = 1 to i_num_buckets
	i_msg[] = i_null_arrary[]
	i_msg[1] = ":   allocating basis " + string(i) + "..." 
	of_logfile()  
	
	sqls = "UPDATE wo_AUTO106_pending_trans c  " + &
			" SET c.basis_" + string(i) + " = nvl(c.basis_" + string(i) + ",0) + " + &
			" 	(SELECT spread_with_rounding_with_plug " + &
			" 		FROM" + &
			" 			(select spread_to_wo, spread_to_est_id," + &
			" 			 amount_to_spread, allocation_statistics, rate, " + &
			" 			amount_to_spread*rate spread_no_rounding, " + &
			" 			round(amount_to_spread * rate,2) spread_with_rounding," + &
			" 			round(amount_to_spread * rate,2) - " + &
			" 				decode(row_number() over (partition by spread_to_wo order by abs(allocation_statistics) desc, spread_to_est_id),1, " + &
			" 				sum(round(amount_to_spread * rate,2)) over (partition by spread_to_wo) - amount_to_spread,0) spread_with_rounding_with_plug " + &
				" 		from (SELECT a.MAX_UNIT_ID spread_to_est_id, a.work_order_id spread_to_wo, " + &
				" 				 b.total_amount amount_to_spread, a.ALLOC_STATISTIC_AMT allocation_statistics, " + &
				" 				ratio_to_report(a.ALLOC_STATISTIC_AMT) over (partition by a.work_order_id) rate " + &
				" 				FROM wo_AUTO106_PENDING_TRANS A, WO_AUTO106_CHARGES_SUMM B " + &
				" 				WHERE A.WORK_ORDER_ID = B.WORK_ORDER_ID " + & 
				" 				and B.BOOK_SUMMARY_ID = " + string(i) + " " + & 
				" 				)" + &
				" 		) allocated " + &
			" 	where allocated.spread_to_wo = c.work_order_id " + & 
			" 	and allocated.spread_to_est_id = c.MAX_UNIT_ID " + & 
			" 	) " + &
			" WHERE EXISTS (select 1 from WO_AUTO106_CHARGES_SUMM x  where x.work_order_id = c.work_order_id and x.book_summary_id = " + string(i) + ")"
			
	execute immediate :sqls;
	if sqlca.sqlcode <> 0 then
		i_msg[] = i_null_arrary[]
		i_msg[1] = 'Error: allocation for basis=' +string(i) + ' :: ' + sqlca.sqlerrtext 
		i_msg[2] = 'SQL string:  ' + sqls 
		of_logfile()  
		rollback;		 
		return -1 
	end if
	num_nrows = sqlca.sqlnrows  
			
next 

return 1
end function

public function longlong of_eligible_charges ();longlong num_nrows


i_msg[] = i_null_arrary[]
i_msg[1] =    ": Getting Eligible Charges..."
of_logfile() 

//WO_AUTO106_CHARGES is a real table similiar to the WO_CHARGE_GROUP_CONTROL table used for 101
insert into WO_AUTO106_CHARGES (company_id, accounting_month, run, work_order_id, charge_id, charge_type_id, book_summary_id, amount, 
 bus_segment_id, utility_account_id, sub_account_id, asset_location_id,
expenditure_type_id, wip_computation_id, time_stamp, user_id)
select c.company_id, c.accounting_month, :i_run_id, a.work_order_id, a.charge_id, a.charge_type_id, e.book_summary_id, a.amount,
nvl(a.bus_segment_id, c.bus_segment_id), a.utility_account_id, a.sub_account_id, nvl(a.asset_location_id, c.asset_location_id), 
a.expenditure_type_id, null, sysdate, :i_user_id1
from cwip_charge a, wo_AUTO106_wos c, work_order_account d, charge_type e
where a.work_order_id = c.work_order_id 
and d.work_order_id = c.work_order_id 
and a.charge_type_id = e.charge_type_id 
and a.expenditure_type_id = 1  
and a.month_number <= :i_mo_num
and a.company_id = :i_co_id  
and a.non_unitized_status is null  
and a.closed_month_number is null 
and a.gl_account_id in (d.cwip_gl_account, 1) ;
		
if sqlca.SQLCode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ": ERROR: WO_AUTO106_CHARGES insert: " + sqlca.SQLErrText
	of_logfile()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//WO_AUTO106_CHARGES_SUMM is a global temp table used for staging data for processing only 
delete from WO_AUTO106_CHARGES_SUMM;
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: WO_AUTO106_CHARGES_SUMM delete: " + sqlca.SQLErrText
	of_logfile()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//create the charge amounts grouped by book_summary_id   
insert into WO_AUTO106_CHARGES_SUMM (company_id, accounting_month, work_order_id, book_summary_id, total_amount, time_stamp, user_id )
select company_id, accounting_month, work_order_id, book_summary_id, sum(amount) total_amount, sysdate, :i_user_id1
from WO_AUTO106_CHARGES
where company_id = :i_co_id 
and accounting_month = to_date(:i_mo_num,'yyyymm') 
and run = :i_run_id
group by company_id, accounting_month, work_order_id, book_summary_id
/*having sum(amount) <> 0  <-- comment this out to be able to id and flag the work orders that will get skipped if all bk summary amounts net to zero*/
;
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =": ERROR: WO_AUTO106_CHARGES_SUMM insert: " + sqlca.SQLErrText
	of_logfile()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1 
end function

public function longlong of_insert_pending ();longlong num_nrows, k
string sqls

//Need to determine posting_quantity for utility_account.ccnc_quantity (note: the quantity is always 1 for units created from charges and cpr)
update wo_AUTO106_pending_trans a   
set a.posting_quantity = decode(a.quantity, 0, 1, null, 1, a.quantity) 
where exists (select 1 from utility_account b 
	where b.utility_account_id = a.utility_account_id
	and b.bus_segment_id = a.bus_segment_id
	and b.ccnc_quantity = 1);
	
if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating posting_quantity(A): ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

update wo_AUTO106_pending_trans a   
set a.posting_quantity = 1
where a.posting_quantity is null;
	
if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating posting_quantity(B): ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////Insert into pend_transaction... 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

i_msg[] = i_null_arrary[]
i_msg[1] = ": Creating pending transactions..."
of_logfile() 

update wo_AUTO106_pending_trans
set pend_trans_id = pwrplant1.nextval;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating pend_trans_id =  pwrplant1.nextval: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

//pend_transaction insert... 
insert into pend_transaction
	(PEND_TRANS_ID,	LDG_ASSET_ID,	LDG_DEPR_GROUP_ID,	BOOKS_SCHEMA_ID,	RETIREMENT_UNIT_ID,	UTILITY_ACCOUNT_ID,	BUS_SEGMENT_ID,	FUNC_CLASS_ID,	SUB_ACCOUNT_ID,
	ASSET_LOCATION_ID,	GL_ACCOUNT_ID,	COMPANY_ID,	GL_POSTING_MO_YR,	SUBLEDGER_INDICATOR,	ACTIVITY_CODE, FERC_ACTIVITY_CODE, 
	GL_JE_CODE,	WORK_ORDER_NUMBER,	POSTING_QUANTITY,  
	user_id1,	 POSTING_AMOUNT,	IN_SERVICE_YEAR,	
	DESCRIPTION,	LONG_DESCRIPTION,	PROPERTY_GROUP_ID,	POSTING_STATUS )
select a.PEND_TRANS_ID, null, null,	1, 1, a.UTILITY_ACCOUNT_ID,	a.BUS_SEGMENT_ID,	a.UA_FUNC_CLASS_ID,	 a.SUB_ACCOUNT_ID,
	a.ASSET_LOCATION_ID,	b.non_unitized_gl_account, 	b.COMPANY_ID, b.accounting_month ,	0,	'UADD',	1, 
	:i_gl_je_code,	 b.WORK_ORDER_NUMBER,	a.posting_quantity,  
	:i_user_id1,	0,	DECODE(B.CLOSING_OPTION_ID, 6, b.accounting_month, 7, b.accounting_month,  8, b.accounting_month,  10, b.accounting_month, b.in_service_date),
	a.WO_DESCRIPTION,	a.WO_LONG_DESCRIPTION, a.PROPERTY_GROUP_ID,	1 
from wo_AUTO106_pending_trans a, wo_AUTO106_wos b 
where a.work_order_id = b.work_order_id  ;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: inserting into pend_transaction: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows
 
i_msg[] = i_null_arrary[]
i_msg[1] =  ":   transactions count = " + string(num_nrows)  //display data 
of_logfile() 

//pend_basis insert...
insert into pend_basis (PEND_TRANS_ID, BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,
BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,
BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,
BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,
BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,
BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,
BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70)
select PEND_TRANS_ID, BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,
BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,
BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,
BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,
BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,
BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,
BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70 
from wo_AUTO106_pending_trans;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: inserting into pend_basis: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

//Update Pend Transaction with Posting Amount
sqls = ''
sqls = 'update pend_transaction c set c.posting_amount = (select sum( '
for k = 1 to i_num_buckets
	if k = 1 then
		sqls+= 'nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	else
		sqls+= ' + nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	end if
next

sqls += ') from pend_basis a, set_of_books b  '
sqls += ' where c.pend_trans_id =  a.pend_trans_id '
sqls += ' and b.set_of_books_id = 1 '
sqls += ') ' // end of sum select
sqls += ' where exists (select 1 from wo_AUTO106_pending_trans bb where bb.pend_trans_id = c.pend_trans_id and bb.error_msg is null) '
	
execute immediate :sqls;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating posting_amount: ' + sqlca.sqlerrtext
	i_msg[2] =  ' SQL: ' + sqls 
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

//class_code_pending_trans
insert into class_code_pending_trans (class_code_id, pend_trans_id, value)
select a.class_code_id, b.pend_trans_id, a.cc_value
from wo_AUTO106_class_codes a, wo_AUTO106_pending_trans b
where a.work_order_id = b.work_order_id
and a.MAX_UNIT_ID = b.MAX_UNIT_ID  ;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = 'Error: inserting class_code_pending_trans: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows 

return 1 
end function

public function longlong of_wip_comp ();longlong check_wip, num_nrows, rtn, check_count, num_wos, wo_id, i 
string wo_list_sqls, rtn_string


check_wip = 0
select count(1) into :check_wip
from wip_computation
where company_id_from = :i_co_id
and NVL(close_to_asset, 1) = 1 ;

if check_wip = 0 then
	return 1 
end if  

uo_ds_top ds_wos_list

ds_wos_list = create uo_ds_top

wo_list_sqls = "select distinct b.work_order_id " + &
" from wip_comp_charges a, wo_AUTO106_wos b " + &
" where a.work_order_id = b.work_order_id  " + &
" and b.company_id = " + string(i_co_id)  + & 
" and b.error_msg is null " + & 
" and nvl(a.unitized_status,0) =  0 " + &
" and nvl(a.non_unitized_status,0) = 0  " + &
" and a.month_number <= " + string(i_mo_num)  + &
" and exists (select 1 from wip_computation j " + &
" 				where a.wip_computation_id = j.wip_computation_id " + &
" 				and nvl(j.close_to_asset,1) = 1 " + &
" 				and j.expenditure_type_id = 5)"
				
rtn_string = f_create_dynamic_ds(ds_wos_list, 'grid', wo_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_wos_list for wip comp 101 close: " + rtn_string 
	rollback;		  
	return -1 
end if 

num_wos = ds_wos_list.RowCount()

if num_wos > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": Processing WIP Computation charges..."
	of_logfile() 
else
	DESTROY ds_wos_list 
	return 1
end if 

uo_wip_computations uo_wip
uo_wip = CREATE uo_wip_computations 

for i = 1 to num_wos

	wo_id = ds_wos_list.getitemnumber(i, 1)
	
	rtn = uo_wip.uf_wip_comp_closing(i_co_id ,i_mo_num,'non-unitized', wo_id)
	if rtn = -1 then
		rollback; 		
		if isvalid(uo_wip) then DESTROY uo_wip 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = 'Error: uf_wip_comp_closing! Check PP logs messages.'
		of_logfile() 
		return -1
	end if 
next

DESTROY uo_wip
DESTROY ds_wos_list 

//Balance WIP Comp closings
rtn = of_balancing_wip_comp()
if rtn < 0 then
	return -1
end if 

//Check if there are still work orders to process
check_count = 0
select count(1) into :check_count 
from wo_AUTO106_wos;
 
//i_msg[] = i_null_arrary[] 
//i_msg[1] =  ": WO count = " + string(check_count)   //display data
//of_logfile() 

if check_count > 0 then
	//continue
else 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": No eligible work orders to process.  Please check the AUTO106 errors."
	of_logfile() 
	
	if i_validation_error then
		return 1
	else
		return 0
	end if
end if  

//save the list of charges_ids that have been included in the 106 WIP Comp closing
insert into WO_AUTO106_CHARGES ( company_id, accounting_month, run, work_order_id, charge_id, charge_type_id, book_summary_id, amount, 
expenditure_type_id, wip_computation_id, time_stamp, user_id) 
select x.company_id, to_date(:i_mo_num, 'yyyymm') , :i_run_id, x.work_order_id, x.charge_id, a.charge_type_id, b.book_summary_id, a.amount, 
a.expenditure_type_id, x.wip_computation_id, sysdate, :i_user_id1
from wip_comp_charges x, wo_AUTO106_wos y, cwip_charge a, charge_type b
where x.work_order_id = y.work_order_id
and x.charge_id = a.charge_id
and x.company_id = :i_co_id  
and x.month_number <= :i_mo_num  
and x.non_unitized_status = 100  
and a.charge_type_id = b.charge_type_id
;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = 'Error: insert into WO_AUTO106_CHARGES for WIP: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

return 1 

end function

public function longlong of_use_charges ();longlong num_nrows
string skip_charges

skip_charges = lower(f_pp_system_control_company('AUTO106 - IGNORE CHARGE UA', i_co_id))
if isnull(skip_charges) or skip_charges = '' then skip_charges = 'no'

if skip_charges = 'yes' then
	//nothing to do here 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =": Skip Utility Accounts from Charges (AUTO106 - IGNORE CHARGE UA set to Yes)"   //display data 
	of_logfile() 
	return 1
end if 

//this insert is modeled after dw_wo_charges_to_buckets_106  (note: the quantity is always 1 for units created from charges and cpr) 
i_msg[] = i_null_arrary[] 
i_msg[1] = ": Getting Utility Accounts from Charges..."
of_logfile() 

insert into wo_AUTO106_PENDING_TRANS (COMPANY_ID, WORK_ORDER_ID, WORK_ORDER_NUMBER, ACCOUNTING_MONTH,
MAX_UNIT_ID, REVISION, BUS_SEGMENT_ID,UTILITY_ACCOUNT_ID,SUB_ACCOUNT_ID, 
PROPERTY_GROUP_ID,ASSET_LOCATION_ID,  
quantity, AMOUNT,  ALLOC_STATISTIC_AMT,   
WO_DESCRIPTION, WO_LONG_DESCRIPTION, SOURCE,
BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,
BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,
BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,
BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,
BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,
BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,
BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70,
time_stamp, user_id)
select C.COMPANY_ID, C.WORK_ORDER_ID, C.WORK_ORDER_NUMBER, D.ACCOUNTING_MONTH, 
MAX(A.CHARGE_ID), 0, NVL(A.BUS_SEGMENT_ID, C.BUS_SEGMENT_ID), A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID, 
null, NVL(A.ASSET_LOCATION_ID, C.ASSET_LOCATION_ID), 
1, SUM(A.AMOUNT), SUM(A.AMOUNT),
substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254), 'charges',
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
sysdate, :i_user_id1
from CWIP_CHARGE A, WORK_ORDER_ACCOUNT B, WORK_ORDER_CONTROL C,  WO_AUTO106_WOS D 
WHERE A.WORK_ORDER_ID = C.WORK_ORDER_ID
and A.company_id = :i_co_id
and C.WORK_ORDER_ID = B.WORK_ORDER_ID
and D.WORK_ORDER_ID = C.WORK_ORDER_ID
and D.ERROR_MSG IS NULL   
and A.utility_account_id is not null 
and NVL(A.non_unitized_status, 0) = 0 
and A.EXPENDITURE_TYPE_ID  = 1 
AND A.GL_ACCOUNT_ID IN ( B.CWIP_GL_ACCOUNT , 1) 
and A.MONTH_NUMBER <= :i_mo_num
and not exists (select 1 from wo_AUTO106_PENDING_TRANS z
				where z.work_order_id = d.work_order_id)
GROUP BY  C.COMPANY_ID, C.WORK_ORDER_ID, C.WORK_ORDER_NUMBER, D.ACCOUNTING_MONTH, 
NVL(A.BUS_SEGMENT_ID, C.BUS_SEGMENT_ID), A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID, 
NVL(A.ASSET_LOCATION_ID, C.ASSET_LOCATION_ID),  
substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254);
	
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: wo_AUTO106_PENDING_TRANS insert for charges: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

i_msg[] = i_null_arrary[] 
i_msg[1] = ":   charges insert count = " + string(num_nrows)   //display data 
of_logfile() 

return 1
end function

public function longlong of_use_cpr ();longlong check_count, num_nrows_106, num_nrows_101, num_nrows_all

//this insert is modeled after dw_wo_cpr_106  (note: the quantity is always 1 for units created from charges and cpr) 
i_msg[] = i_null_arrary[] 
i_msg[1] = ": Getting Utility Accounts from CPR..."
of_logfile() 

insert into wo_AUTO106_PENDING_TRANS (COMPANY_ID, WORK_ORDER_ID, WORK_ORDER_NUMBER, ACCOUNTING_MONTH,
MAX_UNIT_ID, REVISION, BUS_SEGMENT_ID,UTILITY_ACCOUNT_ID,SUB_ACCOUNT_ID, 
PROPERTY_GROUP_ID,ASSET_LOCATION_ID,  
quantity, AMOUNT,  ALLOC_STATISTIC_AMT,   
WO_DESCRIPTION, WO_LONG_DESCRIPTION, SOURCE,
BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,
BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,
BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,
BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,
BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,
BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,
BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70,
time_stamp, user_id)
select C.COMPANY_ID, C.WORK_ORDER_ID,  C.WORK_ORDER_NUMBER, D.ACCOUNTING_MONTH, 
MAX(A.ASSET_ID), 0, A.BUS_SEGMENT_ID , A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID, 
null,  A.ASSET_LOCATION_ID, 
1, SUM(A.accum_cost), SUM(A.accum_cost),
substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254), 'cpr106',
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
sysdate, :i_user_id1
from CPR_LEDGER A, WORK_ORDER_CONTROL C,  WO_AUTO106_WOS D 
WHERE A.WORK_ORDER_NUMBER = C.WORK_ORDER_NUMBER
AND A.COMPANY_ID = C.COMPANY_ID
AND C.FUNDING_WO_INDICATOR = 0 
and D.WORK_ORDER_ID = C.WORK_ORDER_ID
and D.ERROR_MSG IS NULL    
and A.WIP_COMPUTATION_ID IS NULL /*maint 42159*/
and A.RETIREMENT_UNIT_ID  = 1  
and not exists (select 1 from wo_AUTO106_PENDING_TRANS z
				where z.work_order_id = d.work_order_id)
GROUP BY  C.COMPANY_ID, C.WORK_ORDER_ID, C.WORK_ORDER_NUMBER, D.ACCOUNTING_MONTH, 
A.BUS_SEGMENT_ID, A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID, 
A.ASSET_LOCATION_ID,
substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254)
;
	
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: wo_AUTO106_PENDING_TRANS insert for cpr: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows_106 = sqlca.sqlnrows  

check_count = 0
select count(1) into :check_count
from WO_AUTO106_WOS a
where a.closing_option_id in (6,7,8)
and not exists 
	(select 1 from WO_AUTO106_PENDING_TRANS b
	 where a.work_order_id = b.work_order_id);

if check_count > 0 then
	
	// no rows from cpr 106. For blankets, try searching CPR_ACTIVITY for 101 assets.  
	//this insert is modeled after dw_wo_cpr_106_use_101  (note: the quantity is always 1 for units created from charges and cpr)	 
	insert into wo_AUTO106_PENDING_TRANS (COMPANY_ID, WORK_ORDER_ID, WORK_ORDER_NUMBER, ACCOUNTING_MONTH,
	MAX_UNIT_ID, REVISION, BUS_SEGMENT_ID,UTILITY_ACCOUNT_ID,SUB_ACCOUNT_ID, 
	PROPERTY_GROUP_ID,ASSET_LOCATION_ID,  
	quantity, AMOUNT,  ALLOC_STATISTIC_AMT,   
	WO_DESCRIPTION, WO_LONG_DESCRIPTION, SOURCE,
	BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,
	BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,
	BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,
	BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,
	BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,
	BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,
	BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70,
	time_stamp, user_id)
	select C.COMPANY_ID, C.WORK_ORDER_ID, C.WORK_ORDER_NUMBER, D.ACCOUNTING_MONTH, 
	MAX(A.ASSET_ID), 0, A.BUS_SEGMENT_ID , A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID, 
	null,  A.ASSET_LOCATION_ID, 
	1, SUM(E.activity_cost), SUM(E.activity_cost),
	substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254), 'cpr101_12',
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	sysdate, :i_user_id1
	from  CPR_ACTIVITY E, CPR_LEDGER A, WORK_ORDER_CONTROL C,  WO_AUTO106_WOS D 
	WHERE E.WORK_ORDER_NUMBER = C.WORK_ORDER_NUMBER  /*get wo number from CPR_ACTIVITY*/
	AND E.ASSET_ID = A.ASSET_ID
	AND A.COMPANY_ID = C.COMPANY_ID
	AND C.FUNDING_WO_INDICATOR = 0 
	AND D.CLOSING_OPTION_ID IN (6,7,8)
	AND E.MONTH_NUMBER <= :i_mo_num 
	AND E.FERC_ACTIVITY_CODE = 1  
	and D.WORK_ORDER_ID = C.WORK_ORDER_ID
	and D.ERROR_MSG IS NULL    
	and A.RETIREMENT_UNIT_ID  >= 5  
	and A.WIP_COMPUTATION_ID IS NULL /*maint 42159*/
	and not exists (select 1 from wo_AUTO106_PENDING_TRANS z
					where z.work_order_id = d.work_order_id)
	GROUP BY  C.COMPANY_ID, C.WORK_ORDER_ID, C.WORK_ORDER_NUMBER, D.ACCOUNTING_MONTH, 
	A.BUS_SEGMENT_ID, A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID, 
	A.ASSET_LOCATION_ID,
	substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254);
		
	if sqlca.SQLCode <> 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: wo_AUTO106_PENDING_TRANS insert for cpr3: " + sqlca.SQLErrText 
		of_logfile() 
		rollback;		 
		return -1
	end if		
	num_nrows_101 = sqlca.sqlnrows  
	
end if  //if check_count > 0
 

if isnull(num_nrows_106) then num_nrows_106 = 0 
if isnull(num_nrows_101) then num_nrows_101 = 0

num_nrows_all = num_nrows_106 + num_nrows_101

i_msg[] = i_null_arrary[] 
i_msg[1] = ":   cpr insert count = " + string(num_nrows_all)  //display data 
of_logfile() 

return 1
end function

public function longlong of_use_estimates ();longlong num_nrows
string ignore_asbuilts, reuse_estimates, sqls


ignore_asbuilts = lower(f_pp_system_control_company('AUTO106 - IGNORE AS BUILTS', i_co_id))
if isnull(ignore_asbuilts) or ignore_asbuilts = '' then ignore_asbuilts = 'no'

reuse_estimates = lower(f_pp_system_control_company('AUTO106 - REUSE ESTIMATES', i_co_id))
if isnull(reuse_estimates) or reuse_estimates = '' then reuse_estimates = 'no'

//this insert is modeled after dw_wo_est_util_acct, dw_wo_est_util_acct_all_est, dw_wo_est_util_acct_unit_only, dw_wo_est_util_acct_unit_only_all_est 
i_msg[] = i_null_arrary[] 
i_msg[1] =   ": Getting Utility Accounts from Estimates..."
i_msg[2] =   ":    ignore estimate = " + ignore_asbuilts
i_msg[3] =   ":    reuse estimate = " + reuse_estimates
of_logfile() 

sqls = " INSERT into wo_AUTO106_PENDING_TRANS (COMPANY_ID, WORK_ORDER_ID, WORK_ORDER_NUMBER, ACCOUNTING_MONTH, " + &
" MAX_UNIT_ID, REVISION, BUS_SEGMENT_ID,UTILITY_ACCOUNT_ID,SUB_ACCOUNT_ID, " + &
" PROPERTY_GROUP_ID,ASSET_LOCATION_ID, "  + &
" quantity, AMOUNT,  ALLOC_STATISTIC_AMT,   " + &
" WO_DESCRIPTION, WO_LONG_DESCRIPTION, SOURCE,  " + &
" BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,   " + &
" BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,  " + &
" BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,  " + &
" BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,  " + &
" BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,  " + &
" BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,  " + &
" BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70,  " + &
" time_stamp, user_id)  " + &
" SELECT C.COMPANY_ID, C.WORK_ORDER_ID, C.WORK_ORDER_NUMBER, D.ACCOUNTING_MONTH,   " + &
" MAX(A.ESTIMATE_ID), A.REVISION, NVL(A.BUS_SEGMENT_ID, C.BUS_SEGMENT_ID), A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID,  " + & 
" null, NVL(A.ASSET_LOCATION_ID, C.ASSET_LOCATION_ID),   " + &
" sum(nvl(a.quantity,0)), SUM(nvl(A.AMOUNT,0)), SUM(nvl(A.AMOUNT,0)),  " + &
" substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254), 'estimates',  " + &
" 0,0,0,0,0,0,0,0,0,0,  " + &
" 0,0,0,0,0,0,0,0,0,0,  " + &
" 0,0,0,0,0,0,0,0,0,0,  " + &
" 0,0,0,0,0,0,0,0,0,0,  " + &
" 0,0,0,0,0,0,0,0,0,0,  " + &
" 0,0,0,0,0,0,0,0,0,0,  " + &
" 0,0,0,0,0,0,0,0,0,0,  " + &
" sysdate,  '" +  i_user_id1 + "'" +  &
" from WO_ESTIMATE A, WORK_ORDER_CONTROL C,  WO_AUTO106_WOS D   " + &
" WHERE A.WORK_ORDER_ID = C.WORK_ORDER_ID  " + & 
" and D.WORK_ORDER_ID = C.WORK_ORDER_ID  " + &
" and D.ERROR_MSG IS NULL     " + &
" and A.utility_account_id is not null   " + &
" and A.EXPENDITURE_TYPE_ID  = 1    " 

if ignore_asbuilts = 'yes' then
	sqls = sqls + " and A.REVISION = (SELECT MAX(Z.REVISION) FROM WO_ESTIMATE Z   " + &
	" 							WHERE A.WORK_ORDER_ID = Z.WORK_ORDER_ID   " + &
	" 							AND Z.EXPENDITURE_TYPE_ID  = 1    " + &
	" 							AND Z.REVISION < 1000  " + &
	" 							GROUP BY Z.WORK_ORDER_ID)  "  
else
	sqls = sqls + " and A.REVISION = (SELECT MAX(Z.REVISION) FROM WO_ESTIMATE Z   " + &
	" 							WHERE A.WORK_ORDER_ID = Z.WORK_ORDER_ID   " + &
	" 							AND Z.EXPENDITURE_TYPE_ID  = 1    " + & 
	" 							GROUP BY Z.WORK_ORDER_ID)  "  
end if

if reuse_estimates = 'no' then
	sqls = sqls + " and A.BATCH_UNIT_ITEM_ID IS NULL  " 
end if
	

sqls = sqls + " and not exists (select 1 from wo_AUTO106_PENDING_TRANS z  " + &
" 				where z.work_order_id = d.work_order_id)  " + &
" GROUP BY  C.COMPANY_ID, C.WORK_ORDER_ID, C.WORK_ORDER_NUMBER,  D.ACCOUNTING_MONTH,   " + &
" A.REVISION, NVL(A.BUS_SEGMENT_ID, C.BUS_SEGMENT_ID), A.UTILITY_ACCOUNT_ID, A.SUB_ACCOUNT_ID,   " + &
" NVL(A.ASSET_LOCATION_ID, C.ASSET_LOCATION_ID),   " + &
" substrb(NVL(TRIM(C.DESCRIPTION),'NONE'), 1, 35), substrb(NVL(TRIM(C.LONG_DESCRIPTION),NVL(TRIM(C.DESCRIPTION),'NONE')), 1, 254)"

execute immediate :sqls;
	
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =   ": ERROR: wo_AUTO106_PENDING_TRANS insert for estimates: " + sqlca.SQLErrText
	i_msg[2] =   "SQL string:  " + sqls
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

i_msg[] = i_null_arrary[] 
i_msg[1] =   ":   estimates insert count = " + string(num_nrows)  //display data 
of_logfile() 

return 1
end function

public function longlong of_get_util_acct ();longlong check_count, i, num_nrows, rtn
string skip_cpr, ut_accts_sequence[], ut_acct_method 

//wo_AUTO106_PENDING_TRANS is a global temp table used for staging data for processing only 
delete from wo_AUTO106_PENDING_TRANS;
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: wo_AUTO106_PENDING_TRANS delete: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//"Yes" will indicate the order for the allocation process to 106 will be Charges->Estimates->CPR,
//"No" will be the default and process in the following order Charges->CPR->Estimates
skip_cpr = lower(f_pp_system_control_company('AUTO106 - CHARGE/EST/CPR', i_co_id))
if isnull(skip_cpr) or skip_cpr = '' then skip_cpr = 'no'  

ut_accts_sequence[1] = 'charges'
if skip_cpr = 'yes' then
	ut_accts_sequence[2] = 'estimates'
	ut_accts_sequence[3] = 'cpr'
else
	ut_accts_sequence[2] = 'cpr'
	ut_accts_sequence[3] = 'estimates'
end if

//insert utility account and location units for non-unitized retirement units
for i = 1 to 3
	ut_acct_method = ut_accts_sequence[i]
	choose case ut_acct_method
		case 'charges'
			rtn = of_use_charges()
			if rtn < 0 then 
				return -1
			end if 
		case 'estimates'
			rtn = of_use_estimates()
			if rtn < 0 then 
				return -1
			end if 
		case 'cpr'
			rtn = of_use_cpr()
			if rtn < 0 then 
				return -1
			end if 
		case else
			//do nothing
	end choose
next 

//flag work orders that do not have any any utility account information inserted into WO_AUTO106_PENDING_TRANS table
update wo_AUTO106_wos a 
set a.error_msg = decode(a.error_msg, null, '', a.error_msg||' :: ' )||'Error 304: Utilty Accounts not found on Charges, Estimates, CPR *Add Utility Account Info*' 
where not exists 
	(select 1 from WO_AUTO106_PENDING_TRANS b
	 where a.work_order_id = b.work_order_id);
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =": ERROR: wo_AUTO106_wos(ut): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 
	
//Tag and Filter out work orders that have errors thus far 
check_count = 0 
select count(1) into :check_count
from wo_AUTO106_wos where error_msg is not null;

if check_count > 0 then
	rtn = of_errors_update('WO')
	
	if rtn < 0 then
		return -1
	end if 
end if 

return 1
end function

public function longlong of_archive ();longlong num_nrows

i_msg[] = i_null_arrary[]
i_msg[1] = ": Archiving auto106 allocations..."
of_logfile() 


insert into WO_AUTO106_PENDING_TRANS_ARC (USER_ID,COMPANY_ID,ACCOUNTING_MONTH,WORK_ORDER_ID,WORK_ORDER_NUMBER, MAX_UNIT_ID,REVISION,BUS_SEGMENT_ID,
UTILITY_ACCOUNT_ID,SUB_ACCOUNT_ID,PROPERTY_GROUP_ID,ASSET_LOCATION_ID,QUANTITY,POSTING_QUANTITY,AMOUNT,ALLOC_STATISTIC_AMT,
WO_DESCRIPTION,WO_LONG_DESCRIPTION,SOURCE,ERROR_MSG,UA_FUNC_CLASS_ID,
BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,
BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,
BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,
BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,
BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,
BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,
BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70,
TOTAL_AMOUNT,PEND_TRANS_ID,TIME_STAMP, RUN) 
select USER_ID,COMPANY_ID,ACCOUNTING_MONTH,WORK_ORDER_ID,WORK_ORDER_NUMBER, MAX_UNIT_ID,REVISION,BUS_SEGMENT_ID,UTILITY_ACCOUNT_ID,
SUB_ACCOUNT_ID,PROPERTY_GROUP_ID,ASSET_LOCATION_ID,QUANTITY,POSTING_QUANTITY,AMOUNT,ALLOC_STATISTIC_AMT,
WO_DESCRIPTION,WO_LONG_DESCRIPTION,SOURCE,ERROR_MSG,UA_FUNC_CLASS_ID,
BASIS_1,BASIS_2,BASIS_3,BASIS_4,BASIS_5,BASIS_6,BASIS_7,BASIS_8,BASIS_9,BASIS_10,
BASIS_11,BASIS_12,BASIS_13,BASIS_14,BASIS_15,BASIS_16,BASIS_17,BASIS_18,BASIS_19,BASIS_20,
BASIS_21,BASIS_22,BASIS_23,BASIS_24,BASIS_25,BASIS_26,BASIS_27,BASIS_28,BASIS_29,BASIS_30,
BASIS_31,BASIS_32,BASIS_33,BASIS_34,BASIS_35,BASIS_36,BASIS_37,BASIS_38,BASIS_39,BASIS_40,
BASIS_41,BASIS_42,BASIS_43,BASIS_44,BASIS_45,BASIS_46,BASIS_47,BASIS_48,BASIS_49,BASIS_50,
BASIS_51,BASIS_52,BASIS_53,BASIS_54,BASIS_55,BASIS_56,BASIS_57,BASIS_58,BASIS_59,BASIS_60,
BASIS_61,BASIS_62,BASIS_63,BASIS_64,BASIS_65,BASIS_66,BASIS_67,BASIS_68,BASIS_69,BASIS_70,
TOTAL_AMOUNT,PEND_TRANS_ID,TIME_STAMP , :i_run_id
from WO_AUTO106_PENDING_TRANS;

if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] = 'Error: insert into WO_AUTO106_PENDING_TRANS_ARC(1): ' + sqlca.sqlerrtext 
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

return 1 
end function

public function longlong of_non_unitized_status ();longlong num_nrows


i_msg[] = i_null_arrary[]
i_msg[1] = ": Updating status for CWIP charges..." 
of_logfile() 

update cwip_charge a 
set a.non_unitized_status = 100 
where a.company_id = :i_co_id
and a.expenditure_type_id = 1
and nvl(a.non_unitized_status,0) = 0
and exists (select 1 from wo_AUTO106_wos c 
				where c.work_order_id = a.work_order_id  
				and c.company_id = :i_co_id
				and c.error_msg is null)
and exists
	(select 1
	from WO_AUTO106_CHARGES b 
	where a.charge_id = b.charge_id
	and b.company_id = :i_co_id
	and b.accounting_month = to_date(:i_mo_num, 'yyyymm')
	and b.run = :i_run_id
	); 

if sqlca.sqlcode <> 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] = 'Error: updating cwip_charge.non_unitized_status: ' + sqlca.sqlerrtext
	of_logfile() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

return 1
end function

public function longlong of_wos_remove_no_nonunitized ();longlong num_nrows

//3	Standard No Non-Unitized
//4	Standard No Non-Unitized Auto
//11	Manual Blanket - No Non-Unitized

delete from wo_AUTO106_wos where closing_option_id in (3,4,11);
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete for no non-unitized closing options: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

delete from wo_AUTO106_wos where closing_option_id in (1,2) and in_service_date is null;
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete for in_service_date is null: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function longlong of__main (longlong a_company_id, longlong a_month_number, string a_type, longlong a_late_month_number);////////////////////////////////////////////////////////////////////////////////////
////Main function for Auto106 processing
////pass in the argruments:  a_company_id & a_month_number
////return 1 = success
//
//Global temporary tables:
//WO_AUTO106_WOS
//WO_AUTO106_CHARGES_SUMM
//WO_AUTO106_PENDING_TRANS 
//WO_AUTO106_TEMP_WOS
//////////////////////////////////////////////////////////////////////////////////// 
longlong check_count, i, num_nrows, je_id, rtn, max_bk_id, max_run1, max_run2
string obey_est 

i_co_id = a_company_id
i_mo_num = a_month_number

//if isvalid(w_wo_control) then
//	//this is used for online logs
//	i_process_id = w_wo_control.i_process_id
//end if 

select description into :i_co_desc
from company where company_id = :i_co_id;

//  "id" is the PK ... Get the max + 1 to save errors.  use max(run) from WO_AUTO106_CHARGES or WO_AUTO106_Control.  users could delete from WO_AUTO106_Control
select max(run) into :max_run1
  from WO_AUTO106_Control
 where accounting_month =  to_date(:i_mo_num,'yyyymm') and company_id = :i_co_id;
 
 if max_run1 = 0 or isnull(max_run1) then max_run1 = 1 
 
select max(run) into :max_run2
  from WO_AUTO106_PENDING_TRANS_ARC
 where accounting_month =  to_date(:i_mo_num,'yyyymm') and company_id = :i_co_id; 
 
 if max_run2 = 0 or isnull(max_run2) then max_run2 = 1 
 
 if max_run1 > max_run2 then
	i_run_id = max_run1
else
	i_run_id = max_run2
end if 

if i_run_id = 0 or isnull(i_run_id) then
   i_run_id = 1
else
   i_run_id++
end if

i_msg[] = i_null_arrary[]
i_msg[1] =   ": Starting Auto Non-Unitization..." 
i_msg[2] =   ":   Company = " + i_co_desc 
i_msg[3] =   ":   Month = " + string(i_mo_num)  
i_msg[4] =   ":   Run = " + string(i_run_id)  
i_msg[5] =   ":   Type = " + string(a_type)  
of_logfile()

//if this is "yes" cannot use the fast auto106 code
obey_est = lower(f_pp_system_control_company('AUTO106 - CHARGES FOLLOW ESTIMATES', i_co_id))

if obey_est = 'yes' then
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ": ERROR: Unable to run Fast Auto106 when AUTO106 - CHARGES FOLLOW ESTIMATES is set to Yes."
	of_logfile() 
	return -1
end if 

//  GL_JE_CODE ...
select je_id into :je_id
  from gl_je_control where upper(process_id) = 'AUTOMATIC 106';

if isnull(je_id) or je_id = 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ": ERROR: Unable to find GL_JE_CODE for AUTOMATIC 106" 
	of_logfile() 
	return -1
end if

select gl_je_code into :i_gl_je_code 
from standard_journal_entries where je_id = :je_id;


select count(1), max(book_summary_id) into :i_num_buckets, :max_bk_id
from book_summary;

if i_num_buckets <> max_bk_id then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: max(book_summary_id) = " + string(max_bk_id) + " vs count(*) = " + string(i_num_buckets) 
	i_msg[2] = ":     book_summary_id numbers should not be skipped!" 
	of_logfile() 
	return -1
end if 
  
i_user_id1 = s_user_info.user_id 
i_validation_error = false

i_msg[] = i_null_arrary[]
i_msg[1] = ": Getting Eligible Work Orders..." 
of_logfile() 

//Get list of eligible work orders
choose case upper(a_type )
	case '106'
		rtn = of_wos106()
		if rtn < 0 then 
			return -1
		end if 
	case '101 FAILED'
		rtn = of_wos101failed()
		if rtn < 0 then 
			return -1
		end if 
	case '101 LATE'
		rtn = of_wos101late(a_late_month_number)
		if rtn < 0 then 
			return -1
		end if 
	case else
		//there are not other cases 
		i_msg[] = i_null_arrary[]
		i_msg[1] = ": ERROR: Invalid type = " + a_type 
		of_logfile()
		return -1
end choose  

//remove work orders that have one of the "No non-unitized" closing options
rtn = of_wos_remove_no_nonunitized()
if rtn < 0 then
	return -1
end if   

//Check if these work orders have unposted addition type pending transactions here and right before the insert into pend_transaction
rtn = of_check_pending()
if rtn < 0 then
	return -1
end if   

//Check if work orders with no errors remaining to process
check_count = 0
select count(1) into :check_count 
from wo_AUTO106_wos;

i_msg[] = i_null_arrary[]
i_msg[1] =  ": WO count = " + string(check_count)  //display data
of_logfile()

if check_count > 0 then
	//continue
else
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": No eligible work orders to process.  Please check the AUTO106 errors." 
	of_logfile()
	
	if i_validation_error then
		return 1
	else
		return 0
	end if
end if  

//Get eligible charges to close to 106...
rtn = of_eligible_charges()
if rtn < 0 then
	return -1
end if 
	
//Check cwip_charge data to tag and filter work orders with no eligible charges to close to 106
rtn = of_check_charge_data()
if rtn < 0 then
	return -1
end if 

//Get Utility Account and Location info... 
rtn = of_get_util_acct()
if rtn < 0 then
	return -1
end if 

//Check if work orders with no errors remaining to process
check_count = 0
select count(1) into :check_count 
from wo_AUTO106_wos;

//i_msg[] = i_null_arrary[]
//i_msg[1] =  ": WO count = " + string(check_count)  //display data
//of_logfile()

if check_count > 0 then
	//continue
else
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": No eligible work orders to process.  Please check the AUTO106 errors." 
	of_logfile()
	
	if i_validation_error then
		return 1
	else
		return 0
	end if
end if  

//Defaults and Validations
rtn = of_validations()
if rtn < 0 then 
	return -1
end if  
 
//Check if work orders with no errors remaining to process
check_count = 0
select count(1) into :check_count 
from wo_AUTO106_wos;

//i_msg[] = i_null_arrary[]
//i_msg[1] =  ": WO count = " + string(check_count)  //display data
//of_logfile()

if check_count > 0 then
	//continue
else
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": No eligible work orders to process.  Please check the AUTO106 errors." 
	of_logfile()
	
	if i_validation_error then
		return 1
	else
		return 0
	end if
end if  

//Class Codes
rtn = of_class_codes()
if rtn < 0 then 
	return -1
end if   

//Allocation of CWIP charge dollars to 106 Unit Items
rtn = of_allocation()
if rtn < 0 then
	return -1
end if 
 
//Balance back to cwip_charge by gl_account_id for expenditure_type_id = 1
rtn = of_balancing()
if rtn < 0 then
	return -1
end if 

//Check if work orders with no errors remaining to process
check_count = 0
select count(1) into :check_count 
from wo_AUTO106_wos;

i_msg[] = i_null_arrary[]
i_msg[1] =  ": WO count = " + string(check_count)  //display data
of_logfile()

if check_count > 0 then
	//continue
else
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": No eligible work orders to process.  Please check the AUTO106 errors." 
	of_logfile()
	
	if i_validation_error then
		return 1
	else
		return 0
	end if
end if  

//Insert Pending data
rtn = of_insert_pending()
if rtn < 0 then
	return -1
end if 

//update cwip_charge.non_unitized_status for successfully allocated and made into pending charges 
rtn  = of_non_unitized_status()
if rtn < 0 then
	return -1
end if 
 
//WIP COMP CLOSE 
rtn = of_wip_comp()
if rtn < 0 then
	return -1
end if 

////Archive staging tables for the transactions processed from regular cwip charges (expenditure_type_id = 1)
rtn = of_archive()
if rtn < 0 then
	return -1
end if 

commit;
return 1
end function

public function longlong of_wos101late (longlong a_late_month_number);//of_wos101late
longlong num_nrows, late_mo_num, new_date

//Identifiy eligible work orders, use dw_wo_to_unitize_101_late 

////get late_mo_num
//if isvalid(w_wo_control) then
if a_late_month_number > 0 then
	late_mo_num = a_late_month_number
else
	//default to 1 month prior
	select to_number(to_char(  add_months(to_date(:i_mo_num, 'yyyymm'), - 1) ,'yyyymm'))
	into :new_date 
	from dual;
	
	late_mo_num = new_date
end if 


i_msg[] = i_null_arrary[] 
i_msg[1] =  ": late_mo_num = " + string(late_mo_num)
of_logfile() 

//global temporary table WO_AUTO106_WOS
delete from wo_AUTO106_wos;
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: wo_AUTO106_wos delete: " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into wo_AUTO106_wos (company_id, accounting_month, work_order_id, closing_option_id, wo_status_id, in_service_date, completion_date, late_chg_wait_period, non_unitized_gl_account, 
work_order_number,  bus_segment_id, asset_location_id, time_stamp, user_id)
select woc.company_id, to_date(:i_mo_num, 'yyyymm'), woc.work_order_id, woa.closing_option_id, woc.wo_status_id, woc.in_service_date, woc.completion_date,  woc.late_chg_wait_period,
woa.non_unitized_gl_account, work_order_number, woc.bus_segment_id, woc.asset_location_id, sysdate, :i_user_id1
from work_order_control woc, work_order_account woa 
where woc.work_order_id = woa.work_order_id
and woc.funding_wo_indicator = 0 
and woc.company_id = :i_co_id 
and woa.CLOSING_OPTION_ID in (1,2) 
and woc.wo_status_id = 7 
and exists (select 1 from cwip_charge 
         where cwip_charge.company_id = :i_co_id 
		and cwip_charge.expenditure_type_id = 1  
		and woa.work_order_id = cwip_charge.work_order_id 
		and cwip_charge.non_unitized_status is null 
		and cwip_charge.closed_month_number is null
		and cwip_charge.GL_ACCOUNT_ID IN (woa.cwip_gl_account, 1) 
		and cwip_charge.month_number >= :late_mo_num)   

;

//and not exists 
//	(select 1 from pend_transaction pt
//	where woc.work_order_number = pt.work_order_number
//	and woc.company_id = pt.company_id
//	and woc.funding_wo_indicator = 0
//	and trim(upper(pt.activity_code)) in ('UADD','MADD'))     
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: wo_AUTO106_wos insert (101late): " + sqlca.SQLErrText
	of_logfile() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

 

return 1

end function

on uo_wo_auto106_closing.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wo_auto106_closing.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

