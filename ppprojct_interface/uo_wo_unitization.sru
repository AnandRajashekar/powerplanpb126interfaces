HA$PBExportHeader$uo_wo_unitization.sru
$PBExportComments$41258,44146,44314,44271,44745,44954
forward
global type uo_wo_unitization from nonvisualobject
end type
end forward

global type uo_wo_unitization from nonvisualobject
end type
global uo_wo_unitization uo_wo_unitization

type variables
longlong i_process_id, i_run, i_single_wo_id, i_company_id, i_je_id, i_month_number, i_wo_accr_ct, i_ocr_charge_type_id, i_late_month_number, i_delete_month_number
longlong i_elig_add_wos, i_elig_rwip_wos, i_elig_ocr, i_cpr_equip_type_num, i_this_session_id
datetime i_month_yr, i_late_month_yr
boolean i_single_wo, i_visual, i_single_wo_pending_made
boolean i_first_priority, i_equip_ledger_processed, i_net_zero_inserting, i_has_replace_pct_wos, i_has_replace_pct_estimates
string i_sbox_title, i_msg[],  i_null_arrary[], i_co_desc, i_unitization_type, i_gl_je_code, i_wo_accr_calc_sc, i_wo_number, i_closing_type
string i_month_yr_string, i_month_yr_string_display, i_late_month_yr_string_display, i_std_alloc_precision_arrary[], i_pend_balance, i_run_time_string 
uo_ds_top i_ds_dw_required_cc


boolean ib_called_from_w_wo_close
boolean ib_called_from_w_wo_control
longlong i_late_charge_mos
string i_wos_to_run
longlong i_wo_close_late_month_number
end variables

forward prototypes
public function integer of_log ()
public function integer of_validate_company ()
public function integer of_validate_wos ()
public function integer of_validate_wos_joint ()
public function integer of_validate_wos_accrual ()
public function integer of_finished_msg ()
public function integer of_unit_item_defaults ()
public function integer of_unit_item_default_assetloc ()
public function integer of_unit_item_default_subacct ()
public function integer of_unit_item_staging ()
public function integer of_unit_item_default_propgroup ()
public function integer of_errors (string a_error_type)
public function integer of_remove_wos_without_charges ()
public function integer of_remove_wos_without_unit_items ()
public function integer of_validate_wos_dates ()
public function integer of_unitization_tolerance ()
public function integer of_unitization_tolerance_late ()
public function integer of_insert_into_unitized_work_order ()
public function integer of_insert_into_charge_group_control ()
public function integer of_unit_item_compare_to_uwo ()
public function integer of_unit_item_stg_validations ()
public function integer of_unit_item_inserted_validations ()
public function integer of_unit_item_class_codes ()
public function integer of_unit_item_class_codes_required (longlong a_wo_id)
public function integer of_unit_item_descriptions ()
public function integer of_unit_item_refresh_for_one4one ()
public function integer of_direct_assign ()
public function integer of_allocation_prepare ()
public function integer of_allocation_by_priorities ()
public function integer of_allocation_statistic (longlong a_priority_number)
public function integer of_allocation_statistic_actuals (longlong a_priority_number)
public function integer of_allocation_statistic_ratio (longlong a_priority_number)
public function integer of_allocate_charges_remaining (longlong a_priority_number)
public function integer of_unit_item_insert_prepare ()
public function integer of_allocation_results_missing (longlong a_priority_number)
public function integer of_allocation_statistic_estimates (longlong a_priority_number)
public function integer of_allocation_statistic_standards (longlong a_priority_number)
public function integer of_allocation_std_info ()
public function integer of_unitization_completion_check ()
public function integer of_update_unitized_wo_amount ()
public function integer of_unit_item_rwip_asset_id ()
public function integer of_insert_into_pend_transaction ()
public function integer of_prepare_pend_transaction ()
public function string of_wip_comp_closing101_wo (longlong a_company_id, longlong a_month_number, string a_closing_type, longlong a_wo_id)
public function integer of_wip_comp_closing101_check ()
public function integer of_eligible_wos ()
public function integer of_validate_correct_unitization_config (longlong a_process_id, boolean a_visual)
public function integer of_validate_pending_back_to_cwip ()
public function integer of_validate_cgc_back_to_cwip ()
public function integer of_allocation_limits ()
public function integer of_start_msg ()
public function integer of_insert_into_charge_group_control_allo (longlong a_priority_number)
public function integer of_allocation_validate_wo_est (longlong a_priority_number)
public function integer of_allocation_tolerance_check (longlong a_priority_number)
public function integer of_allocation_statistic_estimates_all ()
public function integer of_wo_estimate_defaults_backfill ()
public function integer of_wo_estimate_get_max_revision ()
public function integer of_initialize_variables ()
public function integer of_prepare_wo_list ()
public function integer of_validate_wos_extension ()
public function integer of_clear_wo_list_curr_session ()
public function integer of_clear_wo_list_prior_sessions ()
public function integer of_equipment_ledger (longlong a_wo_id, longlong a_revision, longlong a_est_unit_item_option)
public function integer of_wo_checks_and_updates ()
public function integer of_validate_wos_pending ()
public function integer of_unit_item_ocr_gainloss ()
public function integer of_allocation_refresh_staged_data ()
public function integer of_allocate_charges_by_acct (longlong a_priority_number)
public function integer of_unitization_final_updates ()
public function integer of_unit_item_late_rwip_reset ()
public function integer of_unit_item_staging_late_adds ()
public function integer of_unit_item_staging_late_rwip ()
public function integer of_unit_item_source_updates ()
public function integer of_unit_item_staging_reset ()
public function integer of_allocation_prepare_regular ()
public function integer of_allocation_prepare_late ()
public function integer of_allocation_late_stats_rwip ()
public function integer of_allocation_late_by_precision ()
public function integer of__main ()
public function integer of__main_late ()
public function integer of_start_company (longlong a_company_id, longlong a_month_number, longlong a_process_id, string a_unitize_regular, string a_unitize_late, boolean a_visual)
public function integer of_allocation_late_stats_rwip_ratio ()
public function integer of_allocation_late_stats_adds ()
public function integer of_allocation_late_stats_adds_ratio ()
public function integer of_allocate_late_charges_by_acct ()
public function integer of_allocate_late_charges_remaining ()
public function integer of_allocation_reconcile ()
public function integer of_identify_types_of_elig_charges ()
public function integer of_start_single_wo (longlong a_wo_id, longlong a_month_number, string a_unitize_regular, string a_unitize_late, boolean a_visual)
public function integer of_single_wo_auto101_control (string a_type_of_call)
public function integer of_unitization_elig_unit_items ()
public function integer of_unitization_net_zero ()
public function integer of_sync_wo_est_serial_number ()
public function integer of_update_charge_groups_unitized (string a_type_cgc)
public function integer of_old_net_zero_unitization ()
public function integer of_validate_wos_equip_ledger ()
public function integer of_archive_wo_list_temp ()
public function integer of_archive_alloc_list_temp ()
public function integer of_archive_elig_units_temp ()
public function integer of_archive_ratio_units_temp ()
public function integer of_archive_ratio_late_add_temp ()
public function integer of_archive_ratio_late_rwip_temp ()
public function integer of_archive_alloc_late_bk_temp ()
public function integer of_archive_alloc_late_pt_temp ()
public function integer of_archive_alloc_cgc_inserts_temp ()
public subroutine of_setinstancevariables (boolean ab_called_from_w_wo_close, boolean ab_called_from_w_wo_control, longlong a_late_charge_mos, string a_wos_to_run, longlong a_wo_close_late_month_number)
public function integer of_replacement_amt_as_pct ()
public function integer of_unitization_net_zero_rwip ()
public function integer of_unitization_net_zero_adds ()
end prototypes

public function integer of_log ();/****************************************************************************
 **	
 **	of_logfile		:	Creates a log of the events occurring within the uo
 ** 
 **	
 **	Returns			: 1
 **	
 ****************************************************************************/
 
long	num_msgs, i 
 
num_msgs	=	upperbound(i_msg[])

for i = 1 to num_msgs
	
	if i_single_wo then		
		//auto unitization of single wo will not insert into pp_processes_message and has limited info messages displayed, so if an error occurs, need to display errors.
		if mid(i_msg[i], 1,8) =  ": ERROR:" then 
			f_wo_status_box(i_sbox_title, "")  
			f_wo_status_box(i_sbox_title,LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),23) + ' ' + i_msg[i] ) 
		end if 
	else
		if i_visual then
			//display all the messages in the status window
			f_wo_status_box(i_sbox_title,LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),23) + ' ' + i_msg[i] ) 
		else
			//display only the error messages in the status window
			if mid(i_msg[i], 1,8) =  ": ERROR:" then 
				f_wo_status_box(i_sbox_title,LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),23) + ' ' + i_msg[i] )
			end if 
		end if  		
		
		if i_process_id > 0 then
			//log all the messages to Online Logs
			//the message column has a limit of 2000 characters	 
			f_pp_msgs(  mid(LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),23) + ' ' + i_msg[i], 1, 2000) )
		end if	
	end if

next

return 1
end function

public function integer of_validate_company ();//////////////////////////////////////////////////////////////////////////////////////////////////////
////COMPANY level validations
////1.  Need to verify closed control dates to prevent 101 pend transaction creation that cannot get posted
////2.  Need to verify calc vs approval dates to prevent unapproved charges from closing to 101 
////3.  Need to verify CWIP_CHARGE data vs approval dates to prevent unapproved charges from closing to 101
////4.  Verify company is not a dummy company.
//////////////////////////////////////////////////////////////////////////////////////////////////////
longlong check_overhead_calculation, check_afudc_calc, check_accrual_calc, check_before_oh_calc, check_after_oh_calc, check_external_oh_calc 
longlong check_overhead_approval, check_afudc_approval, check_accrual_approval, check_before_oh_approval, check_after_oh_approval, check_external_oh_approval 
longlong cc_check, powerplant_closed, powerplant_closed_prior_month, cpr_closed, split_check
longlong company_cpr_control_check, company_wo_control_check

if i_single_wo = false then
	//first setnull() for all variables  
	setnull(check_overhead_calculation) 
	setnull(check_afudc_calc)
	setnull(check_accrual_calc)
	setnull(check_before_oh_calc)
	setnull(check_after_oh_calc)
	setnull(check_external_oh_calc)
	setnull(check_overhead_approval)
	setnull(check_afudc_approval)
	setnull(check_accrual_approval)
	setnull(check_before_oh_approval)
	setnull(check_after_oh_approval)
	setnull(check_external_oh_approval)
	setnull(powerplant_closed)
	setnull(cpr_closed)
	
	//check if cpr is already closed
	select to_number(to_char(cpr_closed,'yyyymm')) into :cpr_closed
	from cpr_control 
	where company_id = :i_company_id and accounting_month = :i_month_yr;
	
	if not isnull(cpr_closed) then
		i_msg[] = i_null_arrary[]
		i_msg[1] = ": ERROR: The CPR is already closed for company_id = " + string(i_company_id)
		i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
		of_log() 
		return -1
	end if 
	
	if i_single_wo then
		//do not continue with the unapproved calculation validations below.
		//separate work order specific unapproved calculation validations already performed in w_wo_close
		return 1
	end if 
	
	//compare the wo_process_control dates for calc vs. approval
	select  to_number(to_char(overhead_calculation,'yyyymm')), 
	to_number(to_char(afudc_calc,  'yyyymm')), 
	to_number(to_char(before_oh_calc, 'yyyymm')), 
	to_number(to_char(after_oh_calc , 'yyyymm')), 
	to_number(to_char(overhead_approval, 'yyyymm')), 
	to_number(to_char(afudc_approval,  'yyyymm')), 
	to_number(to_char(before_oh_approval,'yyyymm')),  
	to_number(to_char(after_oh_approval ,'yyyymm')), 
	to_number(to_char(accrual_calc ,'yyyymm')),
	to_number(to_char(accrual_approval ,'yyyymm')),
	to_number(to_char(external_oh_calc ,'yyyymm')),
	to_number(to_char(external_oh_approval ,'yyyymm')) ,
	to_number(to_char(powerplant_closed ,'yyyymm'))
	into :check_overhead_calculation, :check_afudc_calc, :check_before_oh_calc, :check_after_oh_calc,   
	:check_overhead_approval, :check_afudc_approval,  :check_before_oh_approval, :check_after_oh_approval ,
	:check_accrual_calc, :check_accrual_approval, :check_external_oh_calc , :check_external_oh_approval, :powerplant_closed
	from wo_process_control 
	where company_id = :i_company_id and accounting_month = :i_month_yr;
	
	if not isnull(powerplant_closed) then
		i_msg[] = i_null_arrary[]
		i_msg[1] = ": ERROR: The WO Control is already closed for company_id = " + string(i_company_id)
		i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
		of_log() 
		return -1
	end if 
	
	if not isnull(check_overhead_calculation) and isnull(check_overhead_approval) then	
		i_msg[] = i_null_arrary[]
		i_msg[1] = ": ERROR: The Afudc/Overheads are not approved for company_id = " + string(i_company_id)
		i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
		of_log() 
		return -1
	end if 
		
	if lower(i_wo_accr_calc_sc) = 'yes' then
		if not isnull(check_accrual_calc) and isnull(check_accrual_approval) then
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: The Accruals are not approved for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			return -1
		end if
	end if 
	
	if not isnull(check_external_oh_calc) and isnull(check_external_oh_approval) then
		i_msg[] = i_null_arrary[]
		i_msg[1] = ": ERROR: The External Overheads are not approved for company_id = " + string(i_company_id)
		i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
		of_log() 
		return -1
	end if
	
	//need to check if company is setup for split afudc/overhead processing
	split_check = 0 
	select count(*) into :split_check 
	from afudc_oh_process_control
	where company_id = :i_company_id;
	
	//determine if afudc calculated charges are in the CWIP_CHARGE table
	cc_check = 0 
	select count(*) into :cc_check 
	from cwip_charge 
	where company_id = :i_company_id  
	and month_number = :i_month_number 
	and status = 1;
	
	if split_check > 0 then	
		//company is setup for split afudc processing, the afudc_approval date should be filled in
		if cc_check > 0 and isnull(check_afudc_approval) then
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: There are calculated Afudc charges found in Proj Mang that have not been approved for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			return -1 
		end if
	else	
		//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
		if cc_check > 0 and isnull(check_overhead_approval) then
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: There are Afudc calculated charges found in Proj Mang that have not been approved for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			return -1 
		end if
	end if 
	
	//determine if before overheads calculated charges are in the CWIP_CHARGE table
	cc_check = 0 
	select count(*) into :cc_check 
	from cwip_charge 
	where company_id = :i_company_id  
	and month_number = :i_month_number 
	and status in (select clearing_id from clearing_wo_control where before_afudc_indicator = 1)
	and charge_type_id <> :i_wo_accr_ct;	
	
	if split_check > 0 then	
		//company is setup for split afudc processing, the before_oh_approval date should be filled in
		if cc_check > 0 and isnull(check_before_oh_approval) then
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: There are calculated Before OH charges found in Proj Mang that have not been approved for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			return -1 
		end if
	else	
		//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
		if cc_check > 0 and isnull(check_overhead_approval) then
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: There are Before OH calculated charges found in Proj Mang that have not been approved for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			return -1 
		end if
	end if 
	
	//determine if After Overheads calculated charges are in the CWIP_CHARGE table
	cc_check = 0 
	select count(*) into :cc_check 
	from cwip_charge 
	where company_id = :i_company_id  
	and month_number = :i_month_number 
	and status in (select clearing_id from clearing_wo_control where before_afudc_indicator = 0)
	and charge_type_id <> :i_wo_accr_ct;	
	
	if split_check > 0 then	
		//company is setup for split afudc processing, the after_oh_approval date should be filled in
		if cc_check > 0 and isnull(check_after_oh_approval) then
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: There are calculated After OH charges found in Proj Mang that have not been approved for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			return -1 
		end if
	else	
		//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
		if cc_check > 0 and isnull(check_overhead_approval) then
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: There are After OH calculated charges found in Proj Mang that have not been approved for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			return -1 
		end if
	end if 
	
	//check if prior month is still open
	setnull(powerplant_closed_prior_month)
	select  to_number(to_char(powerplant_closed,'yyyymm')) 
	into :powerplant_closed_prior_month
	from wo_process_control where company_id = :i_company_id and add_months(accounting_month, 1) = :i_month_yr;
	
	if isnull(powerplant_closed_prior_month) then	
		//check if the calc has been run and not yet approved for prior month
		setnull(check_overhead_calculation)
		setnull(check_overhead_approval)
		select  to_number(to_char(overhead_calculation,'yyyymm')), to_number(to_char(overhead_approval, 'yyyymm')) 
		into :check_overhead_calculation, :check_overhead_approval
		from wo_process_control where company_id = :i_company_id and add_months(accounting_month, 1) = :i_month_yr;
	
		if not isnull(check_overhead_calculation) and isnull(check_overhead_approval) then		
			i_msg[] = i_null_arrary[]
			i_msg[1] = ": ERROR: There are unapproved charges for the prior month for company_id = " + string(i_company_id)
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log()
			return -1
		end if
	end if
end if //if i_single_wo = false then

//There is an expectation that dummy Companies will NOT be passed into this uo object for Auto Unitization.
//Dummy Companies are not setup in Admin WO Control/CPR Control.  Check if data is not setup to identify dummy companies.
company_wo_control_check = 0
select count(1) into :company_wo_control_check
from wo_process_control 
where company_id = :i_company_id
and accounting_month = :i_month_yr;

if company_wo_control_check = 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: The WO Process Control table is not set up for this Company and Month."
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
	of_log()
	return -1
end if 

company_cpr_control_check = 0
select count(1) into :company_cpr_control_check
from cpr_control 
where company_id = :i_company_id
and accounting_month = :i_month_yr;

if company_cpr_control_check = 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: The CPR Control table is not set up for this Company and Month."
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
	of_log()
	return -1
end if 

return 1
end function

public function integer of_validate_wos ();///////////////////////////////////////////////////////////////////////////////////////////////////////////
//WORK ORDER level validations:  tag work orders with validation errors to exclude from Unitization
///////////////////////////////////////////////////////////////////////////////////////////////////////////
integer of_rtn

of_rtn = of_validate_wos_dates()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

of_rtn = of_validate_wos_pending()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

of_rtn = of_validate_wos_joint()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

of_rtn = of_validate_wos_accrual()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

of_rtn = of_validate_wos_extension()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

of_rtn = of_validate_wos_equip_ledger()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

//Handle errors identified in the validate function above.  
of_rtn = of_errors('WO')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

return 1
end function

public function integer of_validate_wos_joint ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Important:  Joint WOs using Dummy Parent work orders are not allowed to be Auto Unitization!
//                 Dummy Parent Work Orders are setup on dummy Companies and must be Manually Unitized!
//                 Validation already performed in of_validate_company() to insure dummy work orders belonging to a dummy Company is not being unitized.
//                 JOINT WOs with dummy parent wos means the child charges are summarized and made into parent charges, where the parent charges are 
//                      a REPRESENTATION of the child charges and the parent wo is also a REPRESENTATION of the child work orders, which is why we say the parent wo is a dummy wo.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong num_nrows
string unitize_reg_child_sc, unitize_late_child_sc, joint_wo_sc

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": checking joint wo rules..."
of_log() 

//get Joint WO control values for regular close unitization (non-late)
unitize_reg_child_sc = lower(f_pp_system_control_company('WOCLOSE - UNITIZE CHILD WORK ORDERS', i_company_id)) 
if isnull(unitize_reg_child_sc) or unitize_reg_child_sc = "" then 
	unitize_reg_child_sc = "no"  
end if
joint_wo_sc = unitize_reg_child_sc

/////////////////////////////////////////////COMMENT OUT REFERENCES TO "LATE" option for JOINT WO////////////////////////////////////////////
////get Joint WO control values for late close unitization 
//if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then
//	unitize_late_child_sc = lower(f_pp_system_control_company('WOCLOSE - LATE UNITIZE CHILD WO', i_company_id)) 
//	if isnull(unitize_late_child_sc) or unitize_late_child_sc = "" then 
//		unitize_late_child_sc = "no"  
//	end if
//end if
//
////set the main variable that will drive the validations for the parent/child work orders
//if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then
//	joint_wo_sc = unitize_late_child_sc
//else
//	joint_wo_sc = unitize_reg_child_sc
//end if 
//
//if unitize_reg_child_sc = 'no' and unitize_late_child_sc = 'yes' then
//	//this is a bad combination, force the joint_wo_sc = no for validations if child wo not allowed to be unitized for regular close to 101.
//	//these two control values are only valid if unitize_reg_child_sc = yes and unitize_late_child_sc = no, where late child wo charges are not allowed to late close to 101.
//	joint_wo_sc = 'no'
//end if 
/////////////////////////////////////////////COMMENT OUT REFERENCES TO "LATE" option for JOINT WO////////////////////////////////////////////
//
//in Auto unitization, joint parent work order cannot be auto unitizated if the system control value <> "both".
choose case  lower(joint_wo_sc)
	case 'both'
		//okay for both parent and child work orders to be unitized, no other validations are needed.
		//this control value indicates the joint work orders are real work order for both parent and children.
	case 'no', 'yes'
		//child work order are not allowed to be unitized.
		//look for child work orders that are incorrectly identified as eligible for auto unitization
		//on CO_TENANCY_WO table, the CO_TENANT_WO column is the Parent 
		
		if lower(joint_wo_sc) = 'no' then
			update UNITIZE_WO_LIST_TEMP a
			set a.rtn_code = -1, a.error_msg = 'Unitization 151: Joint child work orders cannot be unitized.'
			where a.company_id = :i_company_id
			and a.error_msg is null
			and exists (select 1 from CO_TENANCY_WO b
							where a.work_order_id = b.co_tenant_wo);
	
			if sqlca.SQLCode < 0 then 
				i_msg[] = i_null_arrary[] 
				i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP joint child check: " + sqlca.SQLErrText
				of_log() 
				rollback;		 
				return -1
			end if		
			num_nrows = sqlca.sqlnrows   
		end if
		
		//look for parent work orders, which are not allowed to be auto unitized, regardless of the system control value being "yes" or "no."
		//joint work orders with a "yes" or "no" control value have a dummy work order involved in the joint relationship, and dummy wos are NEVER allowed to auto unitize if the control value is not "both."
		//on CO_TENANCY_WO table, the WORK_ORDER_ID column is the Parent.
		//parent work order are never allowed to go through Auto unitization unless both parent and child wos are configured to be unitized seprately using the "both" system control value.
		update UNITIZE_WO_LIST_TEMP a
		set a.rtn_code = -1, a.error_msg = 'Unitization 151: Joint parent work orders cannot be auto unitized. *Use Manual Unitization*'
		where a.company_id = :i_company_id
		and a.error_msg is null
		and exists (select 1 from CO_TENANCY_WO b
						where a.work_order_id = b.work_order_id);

		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP joint parent check: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
	case else
		//invalid control_value if not one of the 3 listed above for child wos
		update UNITIZE_WO_LIST_TEMP a
		set a.rtn_code = -1, a.error_msg = 'Unitization 151: Invalid control_value = '||:joint_wo_sc||' for WOCLOSE - UNITIZE CHILD WORK ORDERS.'
		where a.company_id = :i_company_id
		and a.error_msg is null
		and exists (select 1 from CO_TENANCY_WO b
						where a.work_order_id = b.co_tenant_wo);

		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP joint check1: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
		
		//invalid control_value if not one of the 3 listed above for parent wos
		update UNITIZE_WO_LIST_TEMP a
		set a.rtn_code = -1, a.error_msg = 'Unitization 151: Invalid control_value = '||:joint_wo_sc||' for WOCLOSE - UNITIZE CHILD WORK ORDERS.'
		where a.company_id = :i_company_id
		and a.error_msg is null
		and exists (select 1 from CO_TENANCY_WO b
						where a.work_order_id = b.work_order_id);

		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP joint check2: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
end choose 	

return 1
end function

public function integer of_validate_wos_accrual ();longlong num_nrows, accrual_type_count, max_accrual_type_id
string one_unitization_rule

if i_wo_accr_ct = 0 or isnull(i_wo_accr_ct) then
	//accruals are not setup; no need to run these validations
	return 1
end if

accrual_type_count = 0
select count(1), max(accrual_type_id) into :accrual_type_count, :max_accrual_type_id
from wo_accrual_type;

choose case accrual_type_count
	case 0
		//accruals are not setup; no need to run these validations
		return 1
	case 1
		//check the one unitization_rule
		select trim(lower(unitization_rule)) into :one_unitization_rule
		from wo_accrual_type;
		
		if one_unitization_rule = 'none' then
			//accruals unitization rules not setup; no need to run these validations
			return 1
		end if 
	case else
		//keep going to code below
end choose 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": validating accrual rules..."
of_log() 

//Identify work orders using ACCRUAL_TYPE_ID <> 1 (wo's unitization rule is used) where 101 close is not allowed
if accrual_type_count > 1 and max_accrual_type_id <> 1 then
	update UNITIZE_WO_LIST_TEMP a
	set a.rtn_code = -1, a.error_msg = 'Unitization 153: This work order will not be unitized because of the Accrual Type Unitization Rules. *Review Accrual Type Unitization Rules.'
	where a.company_id = :i_company_id
	and a.error_msg is null
	and a.accrual_type_id <> 1
	and exists (select 1 from WO_ACCRUAL_TYPE c
					where a.accrual_type_id = c.accrual_type_id
					and trim(lower(c.unitization_rule)) in ('allow 106 only',  'no closing with accrual')
					)
	and exists (select 1 from CWIP_CHARGE b
					where a.work_order_id = b.work_order_id
					and b.charge_type_id = :i_wo_accr_ct
					and b.month_number <= :i_month_number
					group by b.work_order_id
					having sum(b.amount) <> 0
					);
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP wo accrual wo check: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    
end if 

//Identify work orders using ACCRUAL_TYPE_ID <> 1 using the fp's unitization rule where 101 close is not allowed
update UNITIZE_WO_LIST_TEMP a
set a.rtn_code = -1, a.error_msg = 'Unitization 153: This work order will not be unitized because of the Accrual Type Unitization FP Rules. *Review Accrual Type Unitization Rules.'
where a.company_id = :i_company_id
and a.error_msg is null
and a.accrual_type_id = 1
and exists (select 1 from WO_ACCRUAL_TYPE c, WORK_ORDER_CONTROL d, WORK_ORDER_ACCOUNT e
				where a.funding_wo_id = d.work_order_id
				and d.funding_wo_indicator = 1
				and d.work_order_id = e.work_order_id
				and c.accrual_type_id = e.accrual_type_id
				and trim(lower(c.unitization_rule)) in ('allow 106 only',  'no closing with accrual')
				)
and exists (select 1 from CWIP_CHARGE b
				where a.work_order_id = b.work_order_id
				and b.charge_type_id = :i_wo_accr_ct
				and b.month_number <= :i_month_number
				group by b.work_order_id
				having sum(b.amount) <> 0
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP wo accrual fp check: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

		
return 1
end function

public function integer of_finished_msg ();////////////////////////////////////////////////////////////////////////////////////
//messaging for ending Unitization processing
///////////////////////////////////////////////////////////////////////////////////
longlong max_id, num_nrows, wo_id
string msg

//  "id" is the PK ... Get the max + 1 ...
max_id = 0
select max(id) into :max_id from wo_auto101_control
where company_id = :i_company_id and accounting_month = :i_month_yr;

if max_id = 0 or isnull(max_id) then 
	max_id = 1
else
	max_id++
end if


//online auto101 logs the same way as old code 
if i_single_wo then
	msg = "Single Work Order Unitization Finished at: " + string(today()) + ", " + string(now())
	wo_id = i_single_wo_id
else
	msg = "Finished at: " + string(today()) + ", " + string(now())
	wo_id = 0
end if 

insert into wo_auto101_control  (id, work_order_id, accounting_month, return_code, message, run, company_id, closing_option_id)
values ( :max_id, :wo_id, :i_month_yr, 1, :msg, :i_run, :i_company_id, null);
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1]  =   ": ERROR: insert into wo_auto101_control(finished): " + sqlca.SQLErrText
	of_log()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows	

commit;

return 1
end function

public function integer of_unit_item_defaults ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Since missing and default-able data is addressed earlier in the WO_ESTIMATE table updates, 
//  the Unit Items staged from the Estimate source will largely have all the unit attributes required already updated to have complete data; 
//   however, if no default data is available to update the WO_ESTIMATE table during the default portion of the processing logic, 
//   the work orders with incomplete data will be identified and tagged here during the Unit Item Defaulting functionality.  
//The Unit Items staged from Actuals will go through the defaulting logic for the first time.
//    but there is no backfill updating the CWIP_CHARGE table (unlike the WO_ESTIMATE table).  
//   The defaulting logic in this portion of the processing does not include updating the default data back to the CWIP_CHARGE table.  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong of_rtn, num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": defaulting unit attributes..."
of_log() 

//bus_segment_id is already defaulted during insert. the WORK_ORDER_CONTROL.BUS_SEGMENT_ID cannot be null at this point.

//asset_location_id defaulting may be needed 
of_rtn = of_unit_item_default_assetloc()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

//sub_account_id defaulting may be needed 
of_rtn = of_unit_item_default_subacct()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

//property_group_id defaulting may be needed
of_rtn = of_unit_item_default_propgroup()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

//Handle errors identified in the default functions above.  
of_rtn = of_errors('UNITS')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

//maint 44271: Check already made Units with no Sub Account to avoid oracle error when inserting into Pending Trans
update UNITIZE_WO_LIST_TEMP a
set a.error_msg = substrb('Unitization 170a: Unit Items with No Sub Account cannot be used. *Reset Unitization and try again*', 1, 2000), a.rtn_code = -1
where a.company_id = :i_company_id  
and exists (select 1 from UNITIZED_WORK_ORDER b 
				where a.work_order_id = b.work_order_id 
				and b.unit_item_id > 0
				and b.sub_account_id is null
				and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS c
									where b.work_order_id = c.work_order_id
									and b.unit_item_id = c.unit_item_id
									)
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP missing Sub Account: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//Handle errors identified in the validate function above.  
of_rtn = of_errors('WO')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

return 1
end function

public function integer of_unit_item_default_assetloc ();
longlong num_nrows 

//default for nulls
update UNITIZE_WO_STG_UNITS_TEMP a
set a.asset_location_id = (select min(b.asset_location_id) from ASSET_LOCATION b
								where a.wo_major_location_id = b.major_location_id
								and nvl(b.status_code_id,1) = 1  /*active*/
								group by b.major_location_id
								)
where a.company_id = :i_company_id 
and a.asset_location_id is null
and a.ldg_asset_id is null
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP default asset_location_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//error tag for nulls
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 157: No valid Asset Location found for WO *Add Location on WO* '||a.unit_item_source, 1, 2000)																						
where a.company_id = :i_company_id
and a.error_msg is null
and a.ldg_asset_id is null
and a.asset_location_id is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP missing asset_location_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//error tag for nulls on Minor Adds
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 157b: No Asset Location provided for asset_id = '||to_char(a.ldg_asset_id)||' *Remake estimate for this asset* '||a.unit_item_source, 1, 2000)																						
where a.company_id = :i_company_id
and a.error_msg is null
and a.ldg_asset_id is not null
and a.asset_location_id is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP asset missing asset_location_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//backfill the asset_location's major_location_id.  the major_location_id is used in combo validation logic later.
update UNITIZE_WO_STG_UNITS_TEMP a
set a.major_location_id = (select b.major_location_id from ASSET_LOCATION b
								where a.asset_location_id = b.asset_location_id 
								)
where a.company_id = :i_company_id 
and a.asset_location_id is not null
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null); 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP.major_location_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

return 1
end function

public function integer of_unit_item_default_subacct ();longlong num_nrows

//default for nulls
update UNITIZE_WO_STG_UNITS_TEMP a
set a.sub_account_id = (select min(b.sub_account_id) from sub_account b 
								where a.utility_account_id = b.utility_account_id
								and a.bus_segment_id = b.bus_segment_id
								and nvl(b.status_code_id,1) = 1  /*active*/
								)
where a.company_id = :i_company_id
and a.sub_account_id is null
and a.ldg_asset_id is null
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP default sub_account_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//error tag for nulls
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 170: No Sub Account for UA ('||(select trim(b.description) from utility_account b 
																						where a.utility_account_id = b.utility_account_id
																						and a.bus_segment_id = b.bus_segment_id)||') *Review Utility Account Setup* '||a.unit_item_source , 1, 2000)
where a.company_id = :i_company_id
and a.error_msg is null
and a.ldg_asset_id is null
and a.sub_account_id is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP missing sub_account_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//error tag for nulls on Minor Adds
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 170b: No Sub Account provided for asset_id = '||to_char(a.ldg_asset_id)||'  *Remake estimate for this asset* '||a.unit_item_source, 1, 2000)	 																			
where a.company_id = :i_company_id
and a.error_msg is null
and a.ldg_asset_id is not null
and a.sub_account_id is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP asset missing sub_account_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1

end function

public function integer of_unit_item_staging ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Stage new Unit Items from Actuals (CWIP_CHARGE) or Estimates (WO_ESTIMATE). 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": evaluating actuals and estimates..."
of_log()   

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
//1st:  Get Unit Items from ACTUALS
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//Filter to only CHARGE_GROUP_CONTROL records where BATCH_UNIT_ITEM_ID is null.
//join to WORK_ORDER_CHARGE_GROUP was slow instead use ALLOC_ID is not null.
//ALLOC_ID is null if created from Allocations or Target unitization.

//old datawindow:   dw_wo_charge_unit_items_bs
//note1:  If RU with asset_acct_meth_id = 5 incorrectly configured as non-memo would default to UADD
//note2:  Do NOT filter for unit_item_id is null.  logic needs to stage ACTUALS even for already unitized CGC to avoid creating ESTIMATE Unit Items since source can only be Actuals OR Estimate (never both)
//         Let the of_unit_item_compare_to_uwo() function get rid of the duplicate Unit Items staged. 
insert into UNITIZE_WO_STG_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_SOURCE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
PROPERTY_UNIT_ID, WO_MAJOR_LOCATION_ID, ASSET_LOCATION_ID, SERIAL_NUMBER, GL_ACCOUNT_ID, QUANTITY, AMOUNT,
ACTIVITY_CODE, DESCRIPTION, SHORT_DESCRIPTION, status
) 
select a.company_id, a.work_order_id, 'ACTUALS', c.retirement_unit_id, c.utility_account_id, nvl(c.bus_segment_id, a.bus_segment_id), c.sub_account_id,
d.property_unit_id, a.major_location_id, nvl(c.asset_location_id, a.asset_location_id), trim(c.serial_number), a.unitized_gl_account, sum(c.quantity), sum(c.amount),
decode(c.expenditure_type_id, 1, decode(e.asset_acct_meth_id, 2, 'MADD', 'UADD'), 'URET'), d.long_description, null, null 
from UNITIZE_WO_LIST_TEMP a, CHARGE_GROUP_CONTROL c, RETIREMENT_UNIT d, PROPERTY_UNIT e
where a.work_order_id = c.work_order_id 
and c.alloc_id is not null /*links to CWIP_CHARGE*/ 
and c.batch_unit_item_id is null 
/*and c.unit_item_id is null   <-- Do NOT filter for unit_item_id is null. */
and c.expenditure_type_id in (1, 2)
and c.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/
and c.retirement_unit_id = d.retirement_unit_id
and d.property_unit_id = e.property_unit_id
and nvl(d.memo_only,0) = 0  /*do not use memo only retirement units*/
and a.company_id = :i_company_id
and a.UNIT_ITEM_FROM_ESTIMATE = 0 /*try to use Actuals first (default) as long as this flag is not purposely ignoring Actuals*/
and c.utility_account_id is not null /*having UTILITY_ACCOUNT_ID is required*/
and c.retirement_unit_id > 5 /*having unitized RETIREMENT_UNIT required*/
and a.error_msg is null
group by a.company_id, a.work_order_id, c.retirement_unit_id, c.utility_account_id, nvl(c.bus_segment_id, a.bus_segment_id), c.sub_account_id,
d.property_unit_id, a.major_location_id, nvl(c.asset_location_id, a.asset_location_id), trim(c.serial_number), a.unitized_gl_account,
decode(c.expenditure_type_id, 1, decode(e.asset_acct_meth_id, 2, 'MADD', 'UADD'), 'URET'), d.long_description;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert from Actuals: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
//2nd:  Get Unit Items from ESTIMATES
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//the max(revision) is already known at this point.
//if the max_revision is null that means there are no estimates for the work order. 

//old unitization code used: dw_wo_est_unit_items or dw_wo_est_unit_items_no_sum
//note:  if RU with asset_acct_meth_id = 5 incorrectly configured as non-memo would default to UADD 

////////////////////////////////////////////////////////////////////////
//create 1 for 1 estimate to unit items.
////////////////////////////////////////////////////////////////////////

insert into UNITIZE_WO_STG_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_SOURCE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
PROPERTY_UNIT_ID, WO_MAJOR_LOCATION_ID, PROPERTY_GROUP_ID, 
STATUS, 
LDG_ASSET_ID, 
ASSET_LOCATION_ID, SERIAL_NUMBER, GL_ACCOUNT_ID, QUANTITY, AMOUNT, REPLACEMENT_AMOUNT, 
UNIT_ESTIMATE_ID, EST_REVISION, ACTIVITY_CODE, DESCRIPTION, SHORT_DESCRIPTION, IN_SERVICE_DATE
) 
select a.company_id, a.work_order_id, 'ESTIMATES', b.retirement_unit_id, b.utility_account_id, nvl(b.bus_segment_id, a.bus_segment_id), b.sub_account_id,
d.property_unit_id, a.major_location_id, b.property_group_id, 
decode(b.expenditure_type_id, 2, decode(nvl(b.asset_id, 0), 0, 11, 10), decode(b.wo_est_trans_type_id, 3, 11, 4, 11, decode(nvl(b.asset_id, 0), 0, 11, 10))),
/*wo_est_trans_type_id: 4 = Partial Replacement  3 = Replacement: do not keep the asset_id for ldg_asset_id for additions*/
decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)), 
nvl(b.asset_location_id, a.asset_location_id), trim(b.serial_number), a.unitized_gl_account, nvl(b.quantity,0), nvl(b.amount,0), 
nvl(decode(b.expenditure_type_id, 1, b.replacement_amount, 0), 0),
b.estimate_id, a.max_revision, decode(b.expenditure_type_id, 1, decode(e.asset_acct_meth_id, 2, 'MADD', 'UADD'), 'URET'), trim(b.unit_long_desc), trim(b.unit_desc), b.est_in_service_date
from UNITIZE_WO_LIST_TEMP a, WO_ESTIMATE b, RETIREMENT_UNIT d, PROPERTY_UNIT e
where a.work_order_id = b.work_order_id
and a.max_revision = b.revision  /*work orders where max_revision = null should fall out*/
and b.expenditure_type_id in (1, 2)
and d.retirement_unit_id = b.retirement_unit_id
and d.property_unit_id = e.property_unit_id
and a.est_unit_item_option = 1 /*1 for 1 estimates*/
and nvl(d.memo_only,0) = 0  /*do not use memo only retirement units*/
and b.batch_unit_item_id is null
and a.company_id = :i_company_id
and b.utility_account_id is not null /*having UTILITY_ACCOUNT_ID is required*/
and b.retirement_unit_id > 5 /*having unitized RETIREMENT_UNIT required*/
and not exists (select 1 from UNITIZE_WO_STG_UNITS_TEMP c
					where c.work_order_id = a.work_order_id  
					and c.company_id = :i_company_id 
					and c.unit_item_source = 'ACTUALS'  /*do not create more Unit Items if already created from Actuals*/
					)
and not exists (select 1 from unitized_work_order w
					where w.work_order_id = b.work_order_id
					and w.unit_estimate_id = b.estimate_id 
					)	/*avoid duplicating a 1 for 1 unit item already created in a prior unitization run*/
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert from Estimates(1for1): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//summarize ADDITION estimates (1 of 2) to unit items where replacement_percentage = 0 /*maint-41258:  0 = no = treate it the same as AMOUNT*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
insert into UNITIZE_WO_STG_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_SOURCE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
PROPERTY_UNIT_ID, WO_MAJOR_LOCATION_ID, PROPERTY_GROUP_ID, 
STATUS, 
LDG_ASSET_ID, 
ASSET_LOCATION_ID, SERIAL_NUMBER, GL_ACCOUNT_ID, QUANTITY, AMOUNT, REPLACEMENT_AMOUNT, 
UNIT_ESTIMATE_ID, EST_REVISION, ACTIVITY_CODE, DESCRIPTION
) 
select a.company_id, a.work_order_id, 'ESTIMATES', b.retirement_unit_id, b.utility_account_id, nvl(b.bus_segment_id, a.bus_segment_id), b.sub_account_id,
d.property_unit_id, a.major_location_id, b.property_group_id, 
decode(b.expenditure_type_id, 2, decode(nvl(b.asset_id, 0), 0, 11, 10), decode(b.wo_est_trans_type_id, 3, 11, 4, 11, decode(nvl(b.asset_id, 0), 0, 11, 10))),
/*wo_est_trans_type_id: 4 = Partial Replacement  3 = Replacement: do not keep the asset_id for ldg_asset_id for additions*/
decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)), 
nvl(b.asset_location_id, a.asset_location_id), trim(b.serial_number), a.unitized_gl_account, sum(nvl(b.quantity,0)), sum(nvl(b.amount,0)), sum(nvl(b.replacement_amount,0)),
NULL, a.max_revision, decode(e.asset_acct_meth_id, 2, 'MADD', 'UADD'), d.long_description
from UNITIZE_WO_LIST_TEMP a, WO_ESTIMATE b, RETIREMENT_UNIT d, PROPERTY_UNIT e
where a.work_order_id = b.work_order_id
and a.max_revision = b.revision  /*work orders where max_revision = null should fall out*/
and b.expenditure_type_id = 1
and d.retirement_unit_id = b.retirement_unit_id
and d.property_unit_id = e.property_unit_id
and a.est_unit_item_option = 0 /*summarize estimates*/
and nvl(d.memo_only,0) = 0  /*do not use memo only retirement units*/
and b.batch_unit_item_id is null 
and a.company_id = :i_company_id
and b.utility_account_id is not null /*having UTILITY_ACCOUNT_ID is required*/
and b.retirement_unit_id > 5 /*having unitized RETIREMENT_UNIT required*/
and a.replacement_percentage = 0 /*replacement_amount is not a unit attribute*/
and a.error_msg is null
and not exists (select 1 from UNITIZE_WO_STG_UNITS_TEMP c
					where c.work_order_id = a.work_order_id  
					and c.company_id = :i_company_id 
					and c.unit_item_source = 'ACTUALS'  /*do not create more Unit Items if already created from Actuals*/
					) 
group by a.company_id, a.work_order_id, b.retirement_unit_id, b.utility_account_id, nvl(b.bus_segment_id, a.bus_segment_id), b.sub_account_id,
d.property_unit_id, a.major_location_id, b.property_group_id, 
decode(b.expenditure_type_id, 2, decode(nvl(b.asset_id, 0), 0, 11, 10), decode(b.wo_est_trans_type_id, 3, 11, 4, 11, decode(nvl(b.asset_id, 0), 0, 11, 10))), 
decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)), 
nvl(b.asset_location_id, a.asset_location_id), trim(b.serial_number), a.unitized_gl_account, 
a.max_revision, decode(e.asset_acct_meth_id, 2, 'MADD', 'UADD'),  d.long_description;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert from Estimates(sum 1): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//summarize ADDITION estimates (2 of 2) to unit items where replacement_percentage = 1 /*maint-41258:  1 = yes = treate it the same as SERIAL_NUMBER*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
insert into UNITIZE_WO_STG_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_SOURCE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
PROPERTY_UNIT_ID, WO_MAJOR_LOCATION_ID, PROPERTY_GROUP_ID, 
STATUS, 
LDG_ASSET_ID, 
ASSET_LOCATION_ID, SERIAL_NUMBER, GL_ACCOUNT_ID, QUANTITY, AMOUNT, REPLACEMENT_AMOUNT, 
UNIT_ESTIMATE_ID, EST_REVISION, ACTIVITY_CODE, DESCRIPTION
) 
select a.company_id, a.work_order_id, 'ESTIMATES', b.retirement_unit_id, b.utility_account_id, nvl(b.bus_segment_id, a.bus_segment_id), b.sub_account_id,
d.property_unit_id, a.major_location_id, b.property_group_id, 
decode(b.expenditure_type_id, 2, decode(nvl(b.asset_id, 0), 0, 11, 10), decode(b.wo_est_trans_type_id, 3, 11, 4, 11, decode(nvl(b.asset_id, 0), 0, 11, 10))),
/*wo_est_trans_type_id: 4 = Partial Replacement  3 = Replacement: do not keep the asset_id for ldg_asset_id for additions*/
decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)),  
nvl(b.asset_location_id, a.asset_location_id), trim(b.serial_number), a.unitized_gl_account, sum(nvl(b.quantity,0)), sum(nvl(b.amount,0)), nvl(b.replacement_amount, 0),
NULL, a.max_revision, decode(e.asset_acct_meth_id, 2, 'MADD', 'UADD'), d.long_description
from UNITIZE_WO_LIST_TEMP a, WO_ESTIMATE b, RETIREMENT_UNIT d, PROPERTY_UNIT e
where a.work_order_id = b.work_order_id
and a.max_revision = b.revision  /*work orders where max_revision = null should fall out*/
and b.expenditure_type_id = 1
and d.retirement_unit_id = b.retirement_unit_id
and d.property_unit_id = e.property_unit_id
and a.est_unit_item_option = 0 /*summarize estimates*/
and nvl(d.memo_only,0) = 0  /*do not use memo only retirement units*/
and b.batch_unit_item_id is null 
and a.company_id = :i_company_id
and b.utility_account_id is not null /*having UTILITY_ACCOUNT_ID is required*/
and b.retirement_unit_id > 5 /*having unitized RETIREMENT_UNIT required*/
and a.replacement_percentage = 1 /*replacement_amount is a unit attribute for additions*/ 
and a.error_msg is null
and not exists (select 1 from UNITIZE_WO_STG_UNITS_TEMP c
					where c.work_order_id = a.work_order_id  
					and c.company_id = :i_company_id 
					and c.unit_item_source = 'ACTUALS'  /*do not create more Unit Items if already created from Actuals*/
					) 
group by a.company_id, a.work_order_id, b.retirement_unit_id, b.utility_account_id, nvl(b.bus_segment_id, a.bus_segment_id), b.sub_account_id,
d.property_unit_id, a.major_location_id, b.property_group_id, 
decode(b.expenditure_type_id, 2, decode(nvl(b.asset_id, 0), 0, 11, 10), decode(b.wo_est_trans_type_id, 3, 11, 4, 11, decode(nvl(b.asset_id, 0), 0, 11, 10))), 
decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)), 
nvl(b.asset_location_id, a.asset_location_id), trim(b.serial_number), a.unitized_gl_account, nvl(b.replacement_amount, 0), a.max_revision,
decode(e.asset_acct_meth_id, 2, 'MADD', 'UADD'),  d.long_description;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert from Estimates(sum 2): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//summarize RETIRE estimates (3 of 3) to unit items (replacement_amount is always 0 for retirement estimates)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
insert into UNITIZE_WO_STG_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_SOURCE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
PROPERTY_UNIT_ID, WO_MAJOR_LOCATION_ID, PROPERTY_GROUP_ID, 
STATUS, 
LDG_ASSET_ID, 
ASSET_LOCATION_ID, SERIAL_NUMBER, GL_ACCOUNT_ID, QUANTITY, AMOUNT, REPLACEMENT_AMOUNT, 
UNIT_ESTIMATE_ID, EST_REVISION, ACTIVITY_CODE, DESCRIPTION
) 
select a.company_id, a.work_order_id, 'ESTIMATES', b.retirement_unit_id, b.utility_account_id, nvl(b.bus_segment_id, a.bus_segment_id), b.sub_account_id,
d.property_unit_id, a.major_location_id, b.property_group_id, 
decode(b.expenditure_type_id, 2, decode(nvl(b.asset_id, 0), 0, 11, 10), decode(b.wo_est_trans_type_id, 3, 11, 4, 11, decode(nvl(b.asset_id, 0), 0, 11, 10))),
/*wo_est_trans_type_id: 4 = Partial Replacement  3 = Replacement: do not keep the asset_id for ldg_asset_id for additions*/
decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)), 
nvl(b.asset_location_id, a.asset_location_id), trim(b.serial_number), a.unitized_gl_account, sum(nvl(b.quantity,0)), sum(nvl(b.amount,0)), 0,
NULL, a.max_revision, 'URET', d.long_description
from UNITIZE_WO_LIST_TEMP a, WO_ESTIMATE b, RETIREMENT_UNIT d 
where a.work_order_id = b.work_order_id
and a.max_revision = b.revision  /*work orders where max_revision = null should fall out*/
and b.expenditure_type_id = 2
and d.retirement_unit_id = b.retirement_unit_id 
and a.est_unit_item_option = 0 /*summarize estimates*/
and nvl(d.memo_only,0) = 0  /*do not use memo only retirement units*/
and b.batch_unit_item_id is null 
and a.company_id = :i_company_id
and b.utility_account_id is not null /*having UTILITY_ACCOUNT_ID is required*/
and b.retirement_unit_id > 5 /*having unitized RETIREMENT_UNIT required*/ 
and a.error_msg is null
and not exists (select 1 from UNITIZE_WO_STG_UNITS_TEMP c
					where c.work_order_id = a.work_order_id  
					and c.company_id = :i_company_id 
					and c.unit_item_source = 'ACTUALS'  /*do not create more Unit Items if already created from Actuals*/
					) 
group by a.company_id, a.work_order_id, b.retirement_unit_id, b.utility_account_id, nvl(b.bus_segment_id, a.bus_segment_id), b.sub_account_id,
d.property_unit_id, a.major_location_id, b.property_group_id, 
decode(b.expenditure_type_id, 2, decode(nvl(b.asset_id, 0), 0, 11, 10), decode(b.wo_est_trans_type_id, 3, 11, 4, 11, decode(nvl(b.asset_id, 0), 0, 11, 10))), 
decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)),  
nvl(b.asset_location_id, a.asset_location_id), trim(b.serial_number), a.unitized_gl_account, a.max_revision, d.long_description;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert from Estimates(sum 3): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//determine if there are any work orders with estimates using the REPLACEMENT_AMOUNT as a Percentage where later unit item creation and alloc ratios would be impacted.
update UNITIZE_WO_LIST_TEMP a
set a.replace_amt_as_unit = 1
where a.company_id = :i_company_id
and a.replacement_percentage = 1 /*replacement_amount is a unit attribute*/
and a.est_unit_item_option = 0 /*summarize estimates*/
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_STG_UNITS_TEMP b
				where a.work_order_id = b.work_order_id
				and b.unit_item_source = 'ESTIMATES'
				and b.replacement_amount <> 0  /*if using this feature, the replacement_amount would be <> 0 right now*/
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: udpate UNITIZE_WO_LIST_TEMP.replace_amt_as_unit: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   
				
if num_nrows > 0 then
	i_has_replace_pct_wos = true
else
	i_has_replace_pct_wos = false 
end if 

return 1
end function

public function integer of_unit_item_default_propgroup ();longlong num_nrows

//first need to update the FUNC_CLASS_ID
update UNITIZE_WO_STG_UNITS_TEMP a
set a.ua_func_class_id = (select b.func_class_id from utility_account b
								where a.utility_account_id = b.utility_account_id
								and a.bus_segment_id = b.bus_segment_id)
where a.company_id = :i_company_id 
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP get ua_func_class_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//error trap unit items where func class is not found
update UNITIZE_WO_STG_UNITS_TEMP a 
set a.error_msg = substrb('Unitization 169a: No Func Class for UA = '||(select trim(b.description) from utility_account b 
																						where a.utility_account_id = b.utility_account_id
																						and a.bus_segment_id = b.bus_segment_id)||' *Review Utility Account Setup* '||a.unit_item_source , 1, 2000)																											
where a.company_id = :i_company_id
and a.ua_func_class_id is null 
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP missing ua_func_class_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//default for nulls
update UNITIZE_WO_STG_UNITS_TEMP a
set a.property_group_id = (select min(b.property_group_id) from prop_group_prop_unit b, func_class_prop_grp c, retirement_unit d
									where a.ua_func_class_id = c.func_class_id
									and d.retirement_unit_id = a.retirement_unit_id 
									and b.property_unit_id = d.property_unit_id
									and c.property_group_id = b.property_group_id
									)
where a.company_id = :i_company_id
and a.property_group_id is null 
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP default property_group_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//error tag for nulls
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 169: No valid prop groups found for RU and Func Class (RU ='||(select trim(b.description) from retirement_unit b where b.retirement_unit_id = a.retirement_unit_id)||' FC ='||
						(select trim(c.description) from func_class c where c.func_class_id = a.ua_func_class_id)||') *Review Property Unit Catalog & Functional Class/Property Group table* '||a.unit_item_source , 1, 2000)
where a.company_id = :i_company_id
and a.ldg_asset_id is null  /*minor additions or rwip to specific asset_id does not need combo validation*/
and a.error_msg is null
and a.property_group_id is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP missing property_group_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     
 
//error tag for nulls on Minor Adds
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 169b: No Property Group provided for asset_id = '||to_char(a.ldg_asset_id)||'  *Remake estimate for this asset* '||a.unit_item_source, 1, 2000)	 																			
where a.company_id = :i_company_id
and a.error_msg is null
and a.ldg_asset_id is not null
and a.property_group_id is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP asset missing property_group_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

return 1

end function

public function integer of_errors (string a_error_type);//////////////////////////////////////////////////////////////////////////////////////////////
//This function handles errors.
//////////////////////////////////////////////////////////////////////////////////////////////
longlong max_id, num_nrows
string single_wo_error, error_msg_prefix

//  "id" is the PK ... Get the max + 1 ...
max_id = 0
select max(id) into :max_id from wo_auto101_control
where company_id = :i_company_id and accounting_month = :i_month_yr;

if isnull(max_id) then 
	max_id = 0
end if 

////if max_id = 0 or isnull(max_id) then 
////	max_id = 1
////else
////	max_id++
////end if

if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then	
	error_msg_prefix = 'Late '
else
	error_msg_prefix = ''
end if 
	 
choose case a_error_type
	case 'WO'
		
		insert into wo_auto101_control  (id, work_order_id, accounting_month, return_code, message, run, company_id, closing_option_id)
		select :max_id + rownum, work_order_id, :i_month_yr, nvl(rtn_code,-1), substrb(:error_msg_prefix||error_msg, 1, 2000), :i_run, company_id, closing_option_id
		from UNITIZE_WO_LIST_TEMP 
		where company_id = :i_company_id
		and error_msg is not null;
				
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: insert into wo_auto101_control(WO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		delete from UNITIZE_WO_STG_UNITS_TEMP a 
		where a.company_id = :i_company_id  
		and exists (select 1 from UNITIZE_WO_LIST_TEMP b where b.work_order_id = a.work_order_id and b.company_id = :i_company_id  and b.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_WO_STG_UNITS_TEMP(WO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		delete from UNITIZE_ELIG_UNITS_TEMP a 
		where exists (select 1 from UNITIZE_WO_LIST_TEMP b where b.work_order_id = a.work_order_id and b.company_id = :i_company_id  and b.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_ELIG_UNITS_TEMP(WO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows

	case 'UNITS'
		
		insert into wo_auto101_control (id, work_order_id, accounting_month, return_code, message, run, company_id, closing_option_id)
		select :max_id + rownum, work_order_id, :i_month_yr, -1, error_msg, :i_run, company_id, closing_option_id
		from (
				select distinct a.work_order_id work_order_id, substrb(:error_msg_prefix||b.error_msg, 1, 2000) error_msg, a.closing_option_id closing_option_id, a.company_id company_id
				from  UNITIZE_WO_LIST_TEMP a, UNITIZE_WO_STG_UNITS_TEMP b
				where a.work_order_id = b.work_order_id
				and b.error_msg is not null
				);
				
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: insert into wo_auto101_control(UNITS): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		//need to update UNITIZE_WO_LIST_TEMP with error to be able to delete from it last
		update  UNITIZE_WO_LIST_TEMP a
		set a.error_msg = 'UNITS error'
		where a.company_id = :i_company_id
		and exists (select 1 from UNITIZE_WO_STG_UNITS_TEMP b where b.work_order_id = a.work_order_id and b.error_msg is not null);
				
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: update error_msg on UNITIZE_WO_LIST_TEMP(UNITS): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		//need to delete all the unit items created for the work order that has a single error on one of the unit items
		delete from UNITIZE_WO_STG_UNITS_TEMP a 
		where a.company_id = :i_company_id 
		and a.error_msg is not null;
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_WO_STG_UNITS_TEMP(UNITS): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		delete from UNITIZE_ELIG_UNITS_TEMP a 
		where exists (select 1 from UNITIZE_WO_LIST_TEMP b where b.work_order_id = a.work_order_id and b.company_id = :i_company_id  and b.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_ELIG_UNITS_TEMP(WO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	 
		
	case 'ALLO'
		
		insert into wo_auto101_control (id, work_order_id, accounting_month, return_code, run, company_id, closing_option_id, message)
		select :max_id + rownum, work_order_id, :i_month_yr, -1,:i_run, company_id, closing_option_id, error_msg
		from (
				select distinct a.work_order_id work_order_id, b.closing_option_id closing_option_id, b.company_id company_id, 
				substrb(:error_msg_prefix||a.error_msg, 1, 2000) error_msg
				from UNITIZE_ALLOC_ERRORS_TEMP a, UNITIZE_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id  
				and a.company_id = :i_company_id
				and a.error_msg is not null
				);
				
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: insert into wo_auto101_control(ALLO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
		
		//need to update UNITIZE_WO_LIST_TEMP with error to be able to delete from it last
		update  UNITIZE_WO_LIST_TEMP a
		set a.error_msg = 'ALLO error'
		where a.company_id = :i_company_id
		and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP b where b.work_order_id = a.work_order_id and b.company_id = :i_company_id and b.error_msg is not null);
				
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: update error_msg on UNITIZE_WO_LIST_TEMP(ALLO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then
			delete from UNITIZE_ALLOC_EST_TEMP a 
			where a.company_id = :i_company_id  
			and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
			
			if sqlca.SQLCode < 0 then 
				i_msg[] = i_null_arrary[] 
				i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_EST_TEMP (ALLO): " + sqlca.SQLErrText
				of_log() 
				rollback;		 
				return -1
			end if		
			num_nrows = sqlca.sqlnrows    		 
			
			delete from UNITIZE_ALLOC_EST_STG_TEMP a 
			where exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
					
			if sqlca.SQLCode <> 0 then 
				i_msg[] = i_null_arrary[]
				i_msg[1]  =   ": ERROR: delete from UNITIZE_ALLOC_EST_STG_TEMP(ALLO): " + sqlca.SQLErrText
				of_log()
				rollback;		 
				return -1
			end if		
			num_nrows = sqlca.sqlnrows
	
			delete from UNITIZE_ALLOC_STND_TEMP a 
			where a.company_id = :i_company_id  
			and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
			
			if sqlca.SQLCode < 0 then 
				i_msg[] = i_null_arrary[] 
				i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_STND_TEMP (ALLO): " + sqlca.SQLErrText
				of_log() 
				rollback;		 
				return -1
			end if		
			num_nrows = sqlca.sqlnrows  
		end if //if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then
		
		if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then	
			delete from UNITIZE_RATIO_LATE_ADD_TEMP a 
			where a.company_id = :i_company_id  
			and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
			
			if sqlca.SQLCode < 0 then 
				i_msg[] = i_null_arrary[] 
				i_msg[1] =  ": ERROR: delete from UNITIZE_RATIO_LATE_ADD_TEMP (ALLO): " + sqlca.SQLErrText
				of_log() 
				rollback;		 
				return -1
			end if		
			num_nrows = sqlca.sqlnrows  
	
			delete from UNITIZE_RATIO_LATE_RWIP_TEMP a 
			where a.company_id = :i_company_id  
			and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
			
			if sqlca.SQLCode < 0 then 
				i_msg[] = i_null_arrary[] 
				i_msg[1] =  ": ERROR: delete from UNITIZE_RATIO_LATE_RWIP_TEMP (ALLO): " + sqlca.SQLErrText
				of_log() 
				rollback;		 
				return -1
			end if		
			num_nrows = sqlca.sqlnrows  			
		end if //if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then
						
		delete from UNITIZE_WO_STG_UNITS_TEMP a 
		where a.company_id = :i_company_id  
		and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_WO_STG_UNITS_TEMP(ALLO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
//		//Do NOT delete from UNITIZE_RATIO_UNITS_TEMP, so that it can get archived into the UNITIZE_RATIO_UNITS_ARC table for each priority for troubleshooting.
//		delete from UNITIZE_RATIO_UNITS_TEMP a 
//		where a.company_id = :i_company_id  
//		and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
//				
//		if sqlca.SQLCode <> 0 then 
//			i_msg[] = i_null_arrary[]
//			i_msg[1]  =   ": ERROR: delete from UNITIZE_RATIO_UNITS_TEMP(ALLO): " + sqlca.SQLErrText
//			of_log()
//			rollback;		 
//			return -1
//		end if		
//		num_nrows = sqlca.sqlnrows
		
		delete from UNITIZE_ELIG_UNITS_TEMP a 
		where exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_ELIG_UNITS_TEMP(ALLO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP a 
		where a.company_id = :i_company_id  
		and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP(ALLO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		delete from UNITIZE_PEND_TRANS_TEMP a 
		where a.company_id = :i_company_id  
		and exists (select 1 from UNITIZE_ALLOC_ERRORS_TEMP x where x.work_order_id = a.work_order_id and x.company_id = :i_company_id and x.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_PEND_TRANS_TEMP (ALLO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows			
		
		delete from UNITIZE_ALLOC_ERRORS_TEMP a 
		where a.company_id = :i_company_id 
		and exists (select 1 from UNITIZE_WO_LIST_TEMP b where b.work_order_id = a.work_order_id and b.company_id = :i_company_id and b.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_ALLOC_ERRORS_TEMP (ALLO): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
		
	case 'WIPCOMP'
		
		insert into wo_auto101_control  (id, work_order_id, accounting_month, return_code, message, run, company_id, closing_option_id)
		select :max_id + rownum, work_order_id, :i_month_yr, nvl(rtn_code, -1), substrb(:error_msg_prefix||error_msg, 1, 2000), :i_run, company_id, closing_option_id
		from UNITIZE_WO_LIST_TEMP 
		where company_id = :i_company_id
		and error_msg is not null;
				
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: insert into wo_auto101_control(WIPCOMP): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		//need to delete all the PEND tables for the work orders that have an error. 
		delete from PEND_RELATED_ASSET a 
		where exists (select 1 from UNITIZE_PEND_TRANS_TEMP c 
						where c.company_id = :i_company_id 
						and c.pend_trans_id = a.pend_trans_id 
						and exists (select 1 from UNITIZE_WO_LIST_TEMP b
										where b.work_order_id = c.work_order_id
										and b.company_id = :i_company_id 
										and b.error_msg is not null
										)
						);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from pend_related_asset(WIPCOMP): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		delete from CLASS_CODE_PENDING_TRANS a 
		where exists (select 1 from UNITIZE_PEND_TRANS_TEMP c 
						where c.company_id = :i_company_id 
						and c.pend_trans_id = a.pend_trans_id 
						and exists (select 1 from UNITIZE_WO_LIST_TEMP b
										where b.work_order_id = c.work_order_id
										and b.company_id = :i_company_id 
										and b.error_msg is not null
										)
						);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from CLASS_CODE_PENDING_TRANS(WIPCOMP): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		delete from PEND_BASIS a 
		where exists (select 1 from UNITIZE_PEND_TRANS_TEMP c 
						where c.company_id = :i_company_id 
						and c.pend_trans_id = a.pend_trans_id 
						and exists (select 1 from UNITIZE_WO_LIST_TEMP b
										where b.work_order_id = c.work_order_id
										and b.company_id = :i_company_id 
										and b.error_msg is not null
										)
						);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from PEND_BASIS(WIPCOMP): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		delete from PEND_TRANSACTION_SET_OF_BOOKS a 
		where exists (select 1 from UNITIZE_PEND_TRANS_TEMP c 
						where c.company_id = :i_company_id 
						and c.pend_trans_id = a.pend_trans_id 
						and exists (select 1 from UNITIZE_WO_LIST_TEMP b
										where b.work_order_id = c.work_order_id
										and b.company_id = :i_company_id 
										and b.error_msg is not null
										)
						);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from PEND_TRANSACTION_SET_OF_BOOKS(WIPCOMP): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		delete from PEND_TRANSACTION a 
		where exists (select 1 from UNITIZE_PEND_TRANS_TEMP c 
						where c.company_id = :i_company_id 
						and c.pend_trans_id = a.pend_trans_id 
						and exists (select 1 from UNITIZE_WO_LIST_TEMP b
										where b.work_order_id = c.work_order_id
										and b.company_id = :i_company_id 
										and b.error_msg is not null
										)
						);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from PEND_TRANSACTION(WIPCOMP): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	
		
		//need to delete all the PEND tables for the work orders that have an error. 
		delete from UNITIZE_PEND_TRANS_TEMP a 
		where a.company_id = :i_company_id  
		and exists (select 1 from UNITIZE_WO_LIST_TEMP b where b.company_id = :i_company_id and b.work_order_id = a.work_order_id and b.error_msg is not null);
				
		if sqlca.SQLCode <> 0 then 
			i_msg[] = i_null_arrary[]
			i_msg[1]  =   ": ERROR: delete from UNITIZE_PEND_TRANS_TEMP(WIPCOMP): " + sqlca.SQLErrText
			of_log()
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows	 
		
	case else
	
		i_msg[] = i_null_arrary[]
		i_msg[1]  =  ": ERROR: invalid error type for errors:  " + a_error_type
		of_log()
		rollback;		 
		return -1
						
end choose 

//do these two table deletes for every type of error		
delete from UNITIZE_WO_LIST a
where a.company_id = :i_company_id
and db_session_id = :i_this_session_id 
and unitization_type = :i_unitization_type
and exists (select 1 from UNITIZE_WO_LIST_TEMP b where b.work_order_id = a.work_order_id and b.company_id = :i_company_id and b.error_msg is not null); 
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1]  =   ": ERROR: delete from UNITIZE_WO_LIST " + i_unitization_type + ": " + sqlca.SQLErrText
	of_log()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//do this last
delete from UNITIZE_WO_LIST_TEMP where company_id = :i_company_id and error_msg is not null;
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1]  =  ": ERROR: delete from UNITIZE_WO_LIST_TEMP " + i_unitization_type + ": " + sqlca.SQLErrText
	of_log()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

////check if any work order left in the process
//check_count = 0
//select count(1) into :check_count
//from UNITIZE_WO_LIST_TEMP where company_id = :i_company_id;
//
//if check_count <= 0 then
//	return 0
//else
//	return 1
//end if 

return 1
end function

public function integer of_remove_wos_without_charges ();////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Identify work orders that have no eligible charges available to unitize at this time.
////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": filtering wos without charges..."
of_log() 

//Silently remove work orders being unitized where no charges are eligible to be unitized.  No error tagging needed.
//Error tagging work orders without eligible charges causes users to have to investigate the error message, and this non-error could occur many times, resulting in wasted investigation time.
//If a user is expecting a particular work order to get unitized and it does not get unitized, they will seek out help on their own.
//PPC can help the user by informing them that the wo has nothing eligible to unitize because charges have not yet been loaded into CWIP_CHARGE. 
update UNITIZE_WO_LIST_TEMP a
set a.error_msg = 'No Charges'
where a.company_id = :i_company_id  
and nvl(a.has_elig_adds,0) = 0
and nvl(a.has_elig_rwip,0) = 0
and nvl(a.has_gain_loss_ocr,0) = 0
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP remove wos without elig charges: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

delete from UNITIZE_WO_LIST a
where a.company_id = :i_company_id
and db_session_id = :i_this_session_id 
and unitization_type = :i_unitization_type
and exists (select 1 from UNITIZE_WO_LIST_TEMP b where b.work_order_id = a.work_order_id and b.error_msg is not null); 
		
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1]  =   ": ERROR: delete from UNITIZE_WO_LIST for no charges " + i_unitization_type + ": " + sqlca.SQLErrText
	of_log()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//do this last
delete from UNITIZE_WO_LIST_TEMP where company_id = :i_company_id and error_msg is not null;
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1]  =  ": ERROR: delete from UNITIZE_WO_LIST_TEMP for no charges " + i_unitization_type + ": " + sqlca.SQLErrText
	of_log()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_remove_wos_without_unit_items ();////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Identify work orders that have no Unit Items available for unitization
//Needs to be called after Regular and Net $0 Direct Assignments
////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn
string error_text

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": filtering wos without units (adds)..."
of_log() 

//Error tag and remove work orders being unitized where no Unit Items are eligible to be used for unitizing charges.  
//There are eligible charges for unitization, but those charges have no where to go without Unit Items.

if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then
	error_text = 'Unitization 171: Unable to create Minor Addition Unit Items from prior unitization postings *Manual Unitization Recommended*'
else
	error_text = 'Unitization 176: Unable to create Addition Unit Items from either charges or estimates *UT Acct and Retirement Unit data Required*'
end if 

update UNITIZE_WO_LIST_TEMP a
set a.error_msg = substrb(:error_text, 1, 2000), a.rtn_code = -1
where a.company_id = :i_company_id  
and not exists (select 1 from UNITIZE_ELIG_UNITS_TEMP b 
				where a.work_order_id = b.work_order_id  
				and b.expenditure_type_id = 1
				)
and a.has_elig_adds = 1
and a.error_msg is null ;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP remove wos without Unit Items: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    


i_msg[] = i_null_arrary[] 
i_msg[1] =  ": filtering wos without units (rwip)..."
of_log() 

//Error tag and remove work orders being unitized where no Unit Items are eligible to be used for unitizing charges.  
//There are eligible charges for unitization, but those charges have no where to go without Unit Items.

if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then
	error_text = 'Unitization 173: Unable to create RWIP Unit Items from prior unitization postings *Manual Unitization Recommended*'
else
	error_text = 'Unitization 176R: Unable to create RWIP Unit Items from either charges or estimates *UT Acct and Retirement Unit data Required*'
end if 

update UNITIZE_WO_LIST_TEMP a
set a.error_msg = substrb(:error_text, 1, 2000), a.rtn_code = -1
where a.company_id = :i_company_id  
and not exists (select 1 from UNITIZE_ELIG_UNITS_TEMP b 
				where a.work_order_id = b.work_order_id  
				and b.expenditure_type_id = 2
				)
and a.has_elig_rwip = 1
and a.error_msg is null ;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP remove wos without RWIP Unit Items: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

if i_single_wo and num_nrows > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": INFO: This work order does not have necessary Unit Items available for unitizing charges."
	of_log()   
end if 

//Handle errors identified in the validate function above.  
of_rtn = of_errors('WO')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

return 1
end function

public function integer of_validate_wos_dates ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Must have dates to create pending transactions ... They are required only for specific work orders, not for blankets. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

datetime out_of_service_date, in_service_date, completion_date
longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": checking in service and complt dates..."
of_log() 

//check in_service_date
UPDATE UNITIZE_WO_LIST_TEMP a
SET a.rtn_code = -1, a.error_msg = 'Unitization 116a: Cannot create pending transactions without In Service Date. *Update In Service Date on wo.*'
where a.company_id = :i_company_id
and a.error_msg is null
and a.closing_option_id in (2, 4)
and a.in_service_date is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP in_service_date check: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//check completion_date
UPDATE UNITIZE_WO_LIST_TEMP a
SET a.rtn_code = -1, a.error_msg = 'Unitization 116b: Cannot create pending transactions without Completion Date. *Update Completion Date on wo.*'
where a.company_id = :i_company_id
and a.error_msg is null
and a.closing_option_id in (2, 4)
and a.completion_date is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP completion_date check: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

return 1

end function

public function integer of_unitization_tolerance ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Same code found in f_unitization_tolerance(a_wo_id, a_revision)!!! Logic/Code changes need to be made in both!!!
//Perform unitization tolerance checks for work orders that have Units created from Estimates, and TOLERANCE_ID not null in WOA.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
string control_value, error_text
longlong num_nrows, max_tolerance_id, of_rtn

//check if any work order in this company have unitization tolerance setup to be validated
select max(tolerance_id) into :max_tolerance_id
from UNITIZE_WO_LIST_TEMP 
where company_id = :i_company_id 
and unit_item_source = 'ESTIMATES'
and tolerance_id is not null
and error_msg is null;

if max_tolerance_id = 0 or isnull(max_tolerance_id) then
	//nothing to do
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": checking unitization tolerance..."
of_log() 

//update the tolerance threshold and percent to be applied to each work order with unit_item_source = estimates

//pct_tolerance:   This is the percentage tolerance given as a decimal.  (The percent is the difference over the estimate).   
//difference_threshold:   If the dollar difference between the estimate and actual is below the difference_threshold, given in dollars, the percent tolerance is ignored and the work order is unitized.
 

update UNITIZE_WO_LIST_TEMP a
set (a.tolerance_threshold, a.tolerance_pct) = (select nvl(difference_threshold,0), nvl(pct_tolerance,0) 
															from UNITIZATION_TOLERANCE b
															where a.tolerance_id = b.tolerance_id 
															)
where a.company_id = :i_company_id
and a.unit_item_source = 'ESTIMATES'
and a.tolerance_id is not null 
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance threshold and pct: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Obtain the max(revision) to use for work order using Estimates to create Unit Items
control_value = f_pp_system_control_company('UNITIZE-TOLERANCE USE UNIT EST ONLY', i_company_id)

if lower(control_value) = 'yes' then
	//do not include AsBuilt estimates
	update UNITIZE_WO_LIST_TEMP a
	set a.tolerance_revision = (select max(b.revision) 
									from WO_ESTIMATE b
									where a.work_order_id = b.work_order_id
									and b.revision < 1000
									group by b.work_order_id 
									)
	where a.company_id = :i_company_id
	and a.unit_item_source = 'ESTIMATES'
	and a.tolerance_id is not null 
	and a.error_msg is null;
						
	error_text = ": ERROR: updating tolerance_revision without asbuilts: " 
end if

//set the tolerance_revision = max_revision for any that are null at this point.
//max_revision already determine when Unit Items were created.
update UNITIZE_WO_LIST_TEMP 
set tolerance_revision = max_revision
where company_id = :i_company_id
and unit_item_source = 'ESTIMATES'
and tolerance_id is not null 
and tolerance_revision is null
and error_msg is null;
					
error_text = ": ERROR: updating tolerance_revision: " 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  error_text + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//update the tolerance estimate amount for each wo
update UNITIZE_WO_LIST_TEMP a
set a.tolerance_estimate_amt = (select sum(amount) 
										from WO_ESTIMATE b
										where a.work_order_id = b.work_order_id  
										and a.tolerance_revision = b.revision 
										and b.expenditure_type_id in (1, 2) 
										and b.est_chg_type_id not in (select est_chg_type_id from estimate_charge_type where processing_type_id in (1, 5))
										and not exists (select 1 from unitization_tolerance_ect_excl c
															where b.est_chg_type_id = c.est_chg_type_id
															and c.tolerance_id = a.tolerance_id)
											)
where a.company_id = :i_company_id
and a.unit_item_source = 'ESTIMATES'
and a.tolerance_id is not null 
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_estimate_amt: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the tolerance_estimate_amt = 0 where is null
update UNITIZE_WO_LIST_TEMP 
set tolerance_estimate_amt = 0
where company_id = :i_company_id
and unit_item_source = 'ESTIMATES'
and tolerance_id is not null 
and tolerance_estimate_amt is null
and error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_estimate_amt=0: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows
		
		
//update the tolerance actuals amount for each wo
update UNITIZE_WO_LIST_TEMP a
set a.tolerance_actual_amt = (select sum(wo_amount) 
										from WO_SUMMARY b
										where a.work_order_id = b.work_order_id   
										and b.expenditure_type_id in (1, 2) 
										and b.charge_type_id not in (select charge_type_id from charge_type where processing_type_id in (1, 5))
										and not exists (select 1 from unitization_tolerance_ct_excl c
															where b.charge_type_id = c.charge_type_id
															and c.tolerance_id = a.tolerance_id)
											)
where a.company_id = :i_company_id
and a.unit_item_source = 'ESTIMATES'
and a.tolerance_id is not null 
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_actual_amt: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the tolerance_actual_amt = 0 where is null
update UNITIZE_WO_LIST_TEMP 
set tolerance_actual_amt = 0
where company_id = :i_company_id
and unit_item_source = 'ESTIMATES'
and tolerance_id is not null 
and tolerance_actual_amt is null
and error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_estimate_amt=0: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//compare the estimate vs actuals
update UNITIZE_WO_LIST_TEMP 
set tolerance_est_act_diff = abs(tolerance_estimate_amt - tolerance_actual_amt)
where company_id = :i_company_id
and unit_item_source = 'ESTIMATES'
and tolerance_id is not null 
and error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_est_act_diff: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//identify the wos with tolerance failures
update UNITIZE_WO_LIST_TEMP a
set a.rtn_code = -1, a.error_msg = substrb('Unitization 165: Unitization Tolerance, '||(select trim(t.description) from unitization_tolerance t where a.tolerance_id = t.tolerance_id)||', failed. *Review Unitization Tolerance config*', 1, 2000)
where a.company_id = :i_company_id
and a.unit_item_source = 'ESTIMATES'
and a.tolerance_id is not null 
and a.tolerance_estimate_amt <> 0 
and a.tolerance_est_act_diff >= tolerance_threshold
and a.tolerance_est_act_diff/tolerance_estimate_amt >= tolerance_pct
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance error msg: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//save this information into the Archive table
update UNITIZE_WO_LIST_ARC a
set (a.tolerance_threshold, a.tolerance_pct, a.tolerance_revision, a.tolerance_estimate_amt, a.tolerance_actual_amt, a.tolerance_est_act_diff) = 
	(select b.tolerance_threshold, b.tolerance_pct, b.tolerance_revision, b.tolerance_estimate_amt, b.tolerance_actual_amt, b.tolerance_est_act_diff 
	from UNITIZE_WO_LIST_TEMP b
	where a.work_order_id = b.work_order_id
	and b.tolerance_id is not null 
	)
where a.company_id = :i_company_id
and a.db_session_id = :i_this_session_id
and a.tolerance_id is not null 
and a.unit_closed_month_number = :i_month_number
and a.run_time_string = :i_run_time_string
and a.run = :i_run
and a.unitization_type = :i_unitization_type
and a.tolerance_id is not null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_ARC for tolerance data: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

if num_nrows > 0 then
	//Handle errors identified in the tolerance checks above.  
	of_rtn = of_errors('WO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if 

return 1


end function

public function integer of_unitization_tolerance_late ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Same code found in f_unitization_tolerance_late(a_wo_id, a_revision)!!! Logic/Code changes need to be made in both!!!
//
//Perform unitization tolerance checks for work orders that have TOLERANCE_ID not null in WOA.
//the WO_ESTIMATE table is not used for the Late Close tolerance check.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
string control_value, error_text
longlong num_nrows, max_tolerance_id, of_rtn

//check if any work order in this company have unitization tolerance setup to be validated
select max(tolerance_id) into :max_tolerance_id
from UNITIZE_WO_LIST_TEMP 
where company_id = :i_company_id
and tolerance_id is not null
and error_msg is null;

if max_tolerance_id = 0 or isnull(max_tolerance_id) then
	//nothing to do
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": checking late unitization tolerance..."
of_log() 

//update the tolerance threshold and percent to be applied to each work order 
update UNITIZE_WO_LIST_TEMP a
set (a.tolerance_threshold, a.tolerance_pct) = (select nvl(late_diff_threshold,0), nvl(late_pct_tol,0) 
															from UNITIZATION_TOLERANCE b
															where a.tolerance_id = b.tolerance_id 
															)
where a.company_id = :i_company_id
and a.tolerance_id is not null
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating late tolerance threshold and pct: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//HYJACK the tolerance_estimate_amt for each wo by using the already posted unitization historical data as the "estimate" data
update UNITIZE_WO_LIST_TEMP a
set a.tolerance_estimate_amt = (select sum(b.amount) 
										from CHARGE_GROUP_CONTROL b
										where a.work_order_id = b.work_order_id   
										and b.expenditure_type_id in (1, 2)  
										and b.charge_type_id not in (select charge_type_id from charge_type  where processing_type_id in (1, 5))
										and not exists (select 1 from unitization_tolerance_ct_excl c
														where b.charge_type_id = c.charge_type_id
														and c.tolerance_id = a.tolerance_id)											
										and b.batch_unit_item_id is not null 
										and b.unit_item_id <> 0
										)
where a.company_id = :i_company_id
and a.tolerance_id is not null
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating late tolerance_estimate_amt: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows


//update the tolerance_estimate_amt = 0 where is null from update above
update UNITIZE_WO_LIST_TEMP 
set tolerance_estimate_amt = 0
where company_id = :i_company_id
and tolerance_id is not null
and tolerance_estimate_amt is null
and error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating late tolerance_estimate_amt=0: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 
		
//HYJACK the tolerance_actual_amt for each wo using the "available to unitized late charges" that have not yet been sent to pending
update UNITIZE_WO_LIST_TEMP a
set a.tolerance_actual_amt = (select sum(b.amount) 
										from CHARGE_GROUP_CONTROL b, WORK_ORDER_CHARGE_GROUP d
										 where a.work_order_id = b.work_order_id   
										and b.work_order_id = d.work_order_id
										and b.charge_group_id = d.charge_group_id
										and b.expenditure_type_id in (1, 2)  
										and b.charge_type_id not in (select charge_type_id from charge_type  where processing_type_id in (1, 5))
										and not exists (select 1 from unitization_tolerance_ct_excl c
															where b.charge_type_id = c.charge_type_id
															and c.tolerance_id = a.tolerance_id)	
										and b.batch_unit_item_id is null
										)
where a.company_id = :i_company_id
and a.tolerance_id is not null
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_actual_amt late: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the tolerance_actual_amt = 0 where is null from update above
update UNITIZE_WO_LIST_TEMP 
set tolerance_actual_amt = 0
where company_id = :i_company_id
and tolerance_id is not null
and tolerance_actual_amt is null
and error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_estimate_amt=0 late: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

////compare the "already unitized amount" vs "late (new) charges available to unitize"
update UNITIZE_WO_LIST_TEMP 
set tolerance_est_act_diff = abs(tolerance_estimate_amt - tolerance_actual_amt)
where company_id = :i_company_id
and tolerance_id is not null
and error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance_est_act_diff late: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//identify the wos with tolerance failures
update UNITIZE_WO_LIST_TEMP a
set a.error_msg = substrb('Unitization 174: Unitization Tolerance failed for ='||(select trim(t.description) from unitization_tolerance t where a.tolerance_id = t.tolerance_id)||' *Review Late Unitization Tolerance config *', 1, 2000)
where a.company_id = :i_company_id
and a.tolerance_id is not null
and a.tolerance_estimate_amt <> 0
and a.tolerance_est_act_diff >= tolerance_threshold
and a.tolerance_est_act_diff/tolerance_estimate_amt >= tolerance_pct
and a.error_msg is null;  

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating tolerance late error msg: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//save this information into the Archive table
update UNITIZE_WO_LIST_ARC a
set (a.tolerance_threshold, a.tolerance_pct, a.tolerance_revision, a.tolerance_estimate_amt, a.tolerance_actual_amt, a.tolerance_est_act_diff) = 
	(select b.tolerance_threshold, b.tolerance_pct, b.tolerance_revision, b.tolerance_estimate_amt, b.tolerance_actual_amt, b.tolerance_est_act_diff  
	from UNITIZE_WO_LIST_TEMP b
	where a.work_order_id = b.work_order_id	
	and b.tolerance_id is not null 
	)
where a.company_id = :i_company_id
and a.db_session_id = :i_this_session_id
and a.tolerance_id is not null 
and a.unit_closed_month_number = :i_month_number
and a.run_time_string = :i_run_time_string
and a.run = :i_run
and a.unitization_type = :i_unitization_type
and a.tolerance_id is not null ;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_ARC for tolerance data: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

if num_nrows > 0 then
	//Handle errors identified in the tolerance checks above.  
	of_rtn = of_errors('WO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if 

return 1


end function

public function integer of_insert_into_unitized_work_order ();longlong num_nrows

if i_net_zero_inserting then
	//no logging (Part of Net $0)
else
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": creating unit items..."
	of_log() 
end if 

//set a unique "ID" for each unit item staged
update UNITIZE_WO_STG_UNITS_TEMP a
set a.ID = rownum
where a.company_id = :i_company_id
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id
				and b.company_id = :i_company_id
				and b.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP.ID update failed: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//using the unqiue ID update the NEW_UNIT_ITEM_ID for each work order
update UNITIZE_WO_STG_UNITS_TEMP a
set a.new_unit_item_id = (select next_id 
							from (
								select b.work_order_id wo_id, b.id id, c.max_unit_item_id + row_number() over (partition by b.work_order_id order by b.work_order_id) next_id
								from UNITIZE_WO_STG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
								where c.company_id = :i_company_id
								and b.work_order_id = c.work_order_id
								and b.error_msg is null
								) inside
							where a.work_order_id = inside.wo_id
							and a.id = inside.id
							)
where a.company_id = :i_company_id
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id
				and b.company_id = :i_company_id
				and b.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: using next unit_item_id from UNITIZE_WO_LIST_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//insert into unitized_work_order table
insert into unitized_work_order
(unit_item_id, work_order_id, company_id, gl_account_id, asset_location_id, retirement_unit_id, property_group_id, status, in_service_date, serial_number,
utility_account_id, bus_segment_id, sub_account_id, ldg_asset_id, activity_code, description, short_description, amount, quantity, replacement_amount, unit_estimate_id )
select a.new_unit_item_id, a.work_order_id, a.company_id, a.gl_account_id, a.asset_location_id, a.retirement_unit_id, a.property_group_id, a.status, a.in_service_date, a.serial_number,
a.utility_account_id, a.bus_segment_id, a.sub_account_id, a.ldg_asset_id, a.activity_code, a.description, a.short_description, a.amount, a.quantity, a.replacement_amount, 
decode(b.est_unit_item_option, 1, a.unit_estimate_id, null )
from UNITIZE_WO_STG_UNITS_TEMP a, UNITIZE_WO_LIST_TEMP b
where a.work_order_id = b.work_order_id
and b.company_id = :i_company_id
and b.error_msg is null
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into unitized_work_order: " + sqlca.SQLErrText
	of_log() 
	rollback;		  
	return -1
end if		
num_nrows = sqlca.sqlnrows   

return 1
end function

public function integer of_insert_into_charge_group_control ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This function will insert data into the WORK_ORDER_CHARGE_GROUP and CHARGE_GROUP_CONTROL tables.
//
//The CHARGE_TYPE table has a GROUP_INDICATOR column, which is now used by both auto and manual unitization.
//The grouping must maintain uniqueness for the CWIP_CHARGE charge's UTILITY ACCOUNT, BUS SEGMENT, SUB ACCOUNT, RETIREMENT UNIT, ASSET_LOCATION, SERIAL NUMBER, and CHARGE_TYPE_ID.  
//All the columns listed ahead of CHARGE_TYPE_ID are actually Unit Item attributes, which is why they must not get summarized (part of the fix in Maint 9462).  
//Essentially the CHARGE_TYPE_ID is highest level of grouping available.  Below is the grouping "key" for the values in the GROUP_INDICATOR column on the CHARGE_TYPE table.
//
//0 = Summarize to CHARGE_TYPE_ID, VENDOR_INFORMATION, and JOURNAL_CODE level
//1 = Summarize to CHARGE_TYPE_ID only
//2 = Do not Summarize.  Create 1 CHARGE_GROUP_ID for 1 CHARGE_ID
//Null = same as 1 (Summarize to CHARGE_TYPE_ID only)
//
//CHARGE_GROUP_CONTROL's group_indicator '0' means this is an individual charge, '1' means it is a group of charges.
//
//system control, PREGROUP CHARGES, is no longer used and should not exist in the PP_SYSTEM_CONTROL_COMPANY table.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": grouping charges..."
of_log() 

//Get Max(charge_group_id) from each work order
update UNITIZE_WO_LIST_TEMP a
set a.max_charge_group_id = (select max(b.charge_group_id)
										from CHARGE_GROUP_CONTROL b
										where a.work_order_id = b.work_order_id
										and b.charge_group_id >= 0
										group by b.work_order_id
										)
where a.company_id = :i_company_id
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update max_charge_group_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

update UNITIZE_WO_LIST_TEMP a
set a.max_charge_group_id = 0
where a.company_id = :i_company_id
and a.error_msg is null
and a.max_charge_group_id is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update max_charge_group_id(null): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//clear out the global temp table
delete from WORK_ORDER_CHARGE_GROUP_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: reset WORK_ORDER_CHARGE_GROUP_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//A decode() used to determine "rwip_yes_no"
//      decode( nvl( WORK_ORDER_ACCOUNT.RWIP_TYPE_ID, WORK_ORDER_TYPE.RWIP_TYPE_ID), null, 'yes', -1, 'yes', 'no')  rwip_yes_no
//      "yes" means Include RWIP charges in Unitization.
//      "no" meands exclude RWIP charges in Unitization, because it will go through RWIP Closeout.

// insert data into a temp table of charge groups
// maint 43357: filter on cwip_charge.company_id and remove work_order_account
// maint 45072:  always summarize to charge_type_id level for auto unitization
insert into WORK_ORDER_CHARGE_GROUP_TEMP
	(charge_id, work_order_id, alloc_method_type_id, charge_type_id, charge_group_id)
select 
	a.charge_id, a.work_order_id, uwl.alloc_method_type_id, a.charge_type_id,
	uwl.max_charge_group_id + dense_rank() over (PARTITION BY a.WORK_ORDER_ID 
	order by a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.CHARGE_TYPE_ID, a.EXPENDITURE_TYPE_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, trim(a.SERIAL_NUMBER) ) charge_group_id
from cwip_charge a, 
charge_type c,
UNITIZE_WO_LIST_TEMP uwl
where a.company_id = :i_company_id
and uwl.company_id = :i_company_id
and uwl.work_order_id = a.work_order_id 
and a.month_number <= :i_month_number
and a.charge_type_id = c.charge_type_id
and a.expenditure_type_id in (1, decode(uwl.rwip_yes_no, 'yes', 2, -1))   
and c.processing_type_id <> 1 /*non-OCR*/
and a.unit_closed_month_number is null 
and uwl.error_msg is null
and not exists 
	(select 1 from work_order_charge_group z
	where z.work_order_id = a.work_order_id
	and z.charge_id = a.charge_id);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into WORK_ORDER_CHARGE_GROUP_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

// insert into charge_group_control everything in WORK_ORDER_CHARGE_GROUP_TEMP.
// maint 45072:  always summarize to charge_type_id level for auto unitization
insert into charge_group_control
	(charge_group_id, work_order_id, alloc_id, utility_account_id, bus_segment_id, sub_account_id, charge_type_id, expenditure_type_id, 
	retirement_unit_id, description, group_indicator, allocation_priority, unitization_summary, amount, quantity, book_summary_name, 
	vendor_information, journal_code, asset_location_id, serial_number)
select z.charge_group_id, a.work_order_id, f.alloc_id, a.utility_account_id, a.bus_segment_id, a.sub_account_id, a.charge_type_id, a.expenditure_type_id, 
	a.retirement_unit_id,  'SUMMARIZED' , 1 group_indicator, 
	f.priority allocation_priority, d.summary_name unitization_summary, sum(a.amount), sum(a.quantity), d.summary_name book_summary_name, 
	 'SUMMARIZED' vendor_information,  'SUMMARIZED' journal_code, a.asset_location_id, 
	trim(a.serial_number)
from work_order_charge_group_temp z, cwip_charge a, charge_type c, book_summary d, unit_alloc_meth_control f 
where z.work_order_id = a.work_order_id  
and z.charge_id = a.charge_id
and a.charge_type_id = c.charge_type_id
and c.book_summary_id = d.book_summary_id
and z.alloc_method_type_id = f.alloc_method_type_id (+)
and z.charge_type_id = f.charge_type_id (+)
group by z.charge_group_id, a.work_order_id, f.alloc_id, a.utility_account_id, a.bus_segment_id, a.sub_account_id, a.charge_type_id, a.expenditure_type_id, 
	a.retirement_unit_id,  
	f.priority, d.summary_name, d.summary_name, a.asset_location_id, 
	trim(a.serial_number);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into charge_group_control: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

// Insert into work_order_charge_group.  Only insert the charge groups that are in charge_group_control.
insert into work_order_charge_group  (charge_id, work_order_id, charge_group_id)
select z.charge_id, z.work_order_id, z.charge_group_id
from work_order_charge_group_temp z, charge_group_control g
where z.work_order_id = g.work_order_id
and z.charge_group_id = g.charge_group_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into work_order_charge_group: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 
	
return 1
end function

public function integer of_unit_item_compare_to_uwo ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Compare and remove newly staged Unit Items that are already in UNITIZED_WORK_ORDER table (uwo).
//1 for 1 estimates need to be compared by unit_estimate_id.
//Summarized Unit Items need to be compared by the unique combination of unit attributes, including asset_id.
//This function is never called for AUTO LATE.  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, num_units

//////////////////////////////////////////////////////////////
/////ESTIMATES: ONE FOR ONE
//////////////////////////////////////////////////////////////

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": comparing to prior made units (1to1)..."
of_log() 

//Estimate ONE for ONE:  compare for the 1 for 1 estimates option for both Adds and Retires.  
//the UNIT_ESTIMATE_ID is the significant matching item.
delete from UNITIZE_WO_STG_UNITS_TEMP a
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and a.company_id = :i_company_id 
and a.unit_estimate_id is not null
and a.unit_item_source = 'ESTIMATES'
and exists (select 1 from UNITIZED_WORK_ORDER b, UNITIZE_WO_LIST_TEMP c
				where b.work_order_id = c.work_order_id
				and c.company_id = :i_company_id
				and c.work_order_id = a.work_order_id
				and c.unit_item_source = a.unit_item_source
				and c.error_msg is null
				and c.est_unit_item_option = 1 /*1 for 1*/
				and a.unit_estimate_id = b.unit_estimate_id
				and not exists (select 1 from wo_unit_item_pend_trans pt
										where pt.work_order_id = b.work_order_id
										and pt.unit_item_id = b.unit_item_id)
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP delete for Units 1 for 1: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 



//////////////////////////////////////////////////////////////
/////ESTIMATES: SUMMARIZED
//////////////////////////////////////////////////////////////

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": comparing to prior made units (sum)..."
of_log() 

//Estimates SUMMARIZED:  compare ADDITION unit items (part 1 of 2)
delete from UNITIZE_WO_STG_UNITS_TEMP a
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and a.company_id = :i_company_id
and trim(a.activity_code) in ('UADD', 'MADD') 
and a.unit_item_source = 'ESTIMATES'
and a.unit_estimate_id is null
and exists (select 1 from UNITIZED_WORK_ORDER b, UNITIZE_WO_LIST_TEMP c
				where b.work_order_id = c.work_order_id
				and c.company_id = :i_company_id
				and c.error_msg is null
				and nvl(c.replace_amt_as_unit, 0) = 0  /*replacement_amount is not used for this compare*/
				and c.work_order_id = a.work_order_id
				and c.unit_item_source = a.unit_item_source
				and trim(b.activity_code) in ('UADD', 'MADD')
				and c.est_unit_item_option = 0 /*summarize*/
				and a.bus_segment_id = b.bus_segment_id
				and a.utility_account_id = b.utility_account_id
				and a.sub_account_id = b.sub_account_id
				and a.asset_location_id = b.asset_location_id
				and a.property_group_id = b.property_group_id
				and a.retirement_unit_id = b.retirement_unit_id 
				and nvl(a.ldg_asset_id, 0) = nvl(b.ldg_asset_id, 0)
				and nvl(trim(a.serial_number),'nullserialnumber') = nvl(trim(b.serial_number), 'nullserialnumber')
				and not exists (select 1 from wo_unit_item_pend_trans pt
										where pt.work_order_id = b.work_order_id
										and pt.unit_item_id = b.unit_item_id)
				)
and a.error_msg is null;


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP delete for Additions1: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

if i_has_replace_pct_wos then
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": comparing to prior made units (sum2)..."
	of_log() 

	//Estimates SUMMARIZED:  compare ADDITION unit items (part 2 of 2)
	delete from UNITIZE_WO_STG_UNITS_TEMP a
	where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
	and a.company_id = :i_company_id
	and trim(a.activity_code) in ('UADD', 'MADD') 
	and a.unit_item_source = 'ESTIMATES'
	and a.unit_estimate_id is null
	and exists (select 1 from UNITIZED_WORK_ORDER b, UNITIZE_WO_LIST_TEMP c
					where b.work_order_id = c.work_order_id
					and c.company_id = :i_company_id
					and c.error_msg is null
					and c.replace_amt_as_unit = 1 /*replacement_amount is treated the same as serial_number and is used for this compare*/
					and c.work_order_id = a.work_order_id
					and c.unit_item_source = a.unit_item_source
					and trim(b.activity_code) in ('UADD', 'MADD')
					and c.est_unit_item_option = 0 /*summarize*/
					and a.bus_segment_id = b.bus_segment_id
					and a.utility_account_id = b.utility_account_id
					and a.sub_account_id = b.sub_account_id
					and a.asset_location_id = b.asset_location_id
					and a.property_group_id = b.property_group_id
					and a.retirement_unit_id = b.retirement_unit_id 
					and nvl(a.ldg_asset_id, 0) = nvl(b.ldg_asset_id, 0)
					and nvl(trim(a.serial_number),'nullserialnumber') = nvl(trim(b.serial_number), 'nullserialnumber')
					and nvl(a.replacement_amount, 0) = nvl(b.replacement_amount, 0)  /*replacement_amount is treated the same as serial_number and is used for this compare*/
					and not exists (select 1 from wo_unit_item_pend_trans pt
											where pt.work_order_id = b.work_order_id
											and pt.unit_item_id = b.unit_item_id)
					)
	and a.error_msg is null;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP delete for Additions2: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
end if 

//Estimates SUMMARIZED:  compare RETIREMENT unit items  (replacement_amount only applies to ADDITION unit items from summarized method)
delete from UNITIZE_WO_STG_UNITS_TEMP a
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and  a.company_id = :i_company_id
and a.unit_estimate_id is null
and a.activity_code = 'URET' /*new staged Unit Items are always made as URET when expenditure_type_id = 2*/
and a.unit_item_source = 'ESTIMATES'
and exists (select 1 from UNITIZED_WORK_ORDER b, UNITIZE_WO_LIST_TEMP c
				where b.work_order_id = c.work_order_id
				and c.company_id = :i_company_id
				and c.error_msg is null 
				and c.work_order_id = a.work_order_id
				and c.unit_item_source = a.unit_item_source
				and trim(b.activity_code) in ( 'URET' , 'MRET', 'URGL', 'SAGL', 'SALE')  
				and c.est_unit_item_option = 0 /*summarize*/
				and a.bus_segment_id = b.bus_segment_id
				and a.utility_account_id = b.utility_account_id
				and a.sub_account_id = b.sub_account_id
				and a.asset_location_id = b.asset_location_id
				and a.property_group_id = b.property_group_id
				and a.retirement_unit_id = b.retirement_unit_id  
				and nvl(a.ldg_asset_id, 0) = nvl(b.ldg_asset_id, 0)
				and nvl(trim(a.serial_number),'nullserialnumber') = nvl(trim(b.serial_number), 'nullserialnumber')
				and not exists (select 1 from wo_unit_item_pend_trans pt
										where pt.work_order_id = b.work_order_id
										and pt.unit_item_id = b.unit_item_id)
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR:UNITIZE_WO_STG_UNITS_TEMP delete Retirements: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows


//////////////////////////////////////////////////////////////
/////ACTUALS
//////////////////////////////////////////////////////////////

//Actuals SUMMARIZED:  compare ADDITION unit items
delete from UNITIZE_WO_STG_UNITS_TEMP a
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and a.company_id = :i_company_id
and trim(a.activity_code) in ('UADD', 'MADD') 
and a.unit_item_source = 'ACTUALS'
and a.unit_estimate_id is null
and exists (select 1 from UNITIZED_WORK_ORDER b, UNITIZE_WO_LIST_TEMP c
				where b.work_order_id = c.work_order_id
				and c.company_id = :i_company_id
				and c.error_msg is null
				and c.work_order_id = a.work_order_id
				and c.unit_item_source = a.unit_item_source
				and trim(b.activity_code) in ('UADD', 'MADD')
				and c.est_unit_item_option in (0,1) /*must include both for Actuals, because est_unit_item_option does not apply for Actuals */
				and a.bus_segment_id = b.bus_segment_id
				and a.utility_account_id = b.utility_account_id
				and a.sub_account_id = b.sub_account_id
				and a.asset_location_id = b.asset_location_id
				and a.property_group_id = b.property_group_id
				and a.retirement_unit_id = b.retirement_unit_id 
				and nvl(trim(a.serial_number),'nullserialnumber') = nvl(trim(b.serial_number), 'nullserialnumber')
				and not exists (select 1 from wo_unit_item_pend_trans pt
										where pt.work_order_id = b.work_order_id
										and pt.unit_item_id = b.unit_item_id)
				)
and a.error_msg is null;


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP delete for Additions (Actuals): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Actuals SUMMARIZED:  compare RETIREMENT unit items. 
delete from UNITIZE_WO_STG_UNITS_TEMP a
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and  a.company_id = :i_company_id
and a.unit_estimate_id is null
and a.activity_code = 'URET' /*new staged Unit Items are always made as URET when expenditure_type_id = 2*/
and a.unit_item_source = 'ACTUALS'
and exists (select 1 from UNITIZED_WORK_ORDER b, UNITIZE_WO_LIST_TEMP c
				where b.work_order_id = c.work_order_id
				and c.company_id = :i_company_id
				and c.error_msg is null
				and c.work_order_id = a.work_order_id
				and c.unit_item_source = a.unit_item_source
				and trim(b.activity_code) in ( 'URET' , 'MRET', 'URGL', 'SAGL', 'SALE')  
				and c.est_unit_item_option  in (0,1) /*must include both for Actuals, because est_unit_item_option does not apply for Actuals */
				and a.bus_segment_id = b.bus_segment_id
				and a.utility_account_id = b.utility_account_id
				and a.sub_account_id = b.sub_account_id
				and a.asset_location_id = b.asset_location_id
				and a.property_group_id = b.property_group_id
				and a.retirement_unit_id = b.retirement_unit_id  
				and nvl(trim(a.serial_number),'nullserialnumber') = nvl(trim(b.serial_number), 'nullserialnumber')
				and not exists (select 1 from wo_unit_item_pend_trans pt
										where pt.work_order_id = b.work_order_id
										and pt.unit_item_id = b.unit_item_id)
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR:UNITIZE_WO_STG_UNITS_TEMP delete Retirements (Actuals): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1

//////////////////////////////////comment on bottom of page for ASSET_ID treatment/////////////////////////////////////////////
//In the old 101 code, the logic used to determine if a new Unit Item needs to be made included all attributes of the unit, including the ASSET_ID.   
//A Unit Item with an ASSET_ID > 0 is going to be a minor addition to the existing asset already in the CPR.  
//A Unit Item with an ASSET_ID < 0 is going to be a related asset. 
//Need to treat ASSET_ID as a part of the uniqueness attributes like asset_location_id.
//////////////////////////////////comment on bottom of page for ASSET_ID treatment/////////////////////////////////////////////
end function

public function integer of_unit_item_stg_validations ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Error tagging the Unit Items with bad data.  Later tag the WO with Unit Item errors and the WO will fail unitization.  
//Even minor additions (units with ASSET_ID) will go through these combo validations.
//This function is not called during Late Charge Closing, where all Units are Minor additions.
//This function should identify Units with invalid combinations, even for Minor Additions (units with ASSET_ID),
//   because if the set of account combinations are truly invalid,
//   it is undesirable to add more dollars to an asset already in the CPR that is considered to be an invalid set of account combinations. 
//The user must decide if the minor addition is incorrect or if the combination of accounts should be configured as a valid combination.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": validating new unit items..."
of_log() 

//Detect bad combinations of CO/AL
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 179: Invalid Company/Asset Location combo (asset_loc_id/maj_loc_id ='||to_char(a.asset_location_id)||'/'||to_char(a.major_location_id)||
								') AL desc = '||	(select trim(n.long_description) from asset_location n where a.asset_location_id = n.asset_location_id)||' *Review the Related Company on the Location Details* '||a.unit_item_source, 1, 2000)
where a.company_id = :i_company_id
and a.error_msg is null 
and not exists (select 1 from company_major_location b
					where a.major_location_id = b.major_location_id
					and a.company_id= b.company_id
					)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP invalid co_id/asset_loc_id Combo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     


//Detect bad combinations of BS/UT
//The unitization logic is going to default the BUS_SEGMENT_ID from WORK_ORDER_CONTROL table if one is not specified on the Actuals, and it could result in an invalid combo. 
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 166a: Invalid Bus Segment for Utility Account (bs_id/ua_id ='||to_char(a.bus_segment_id)||'/'||to_char(a.utility_account_id)||') *Review Utility Account Setup or Bus Segment* '||a.unit_item_source, 1, 2000)
where a.company_id = :i_company_id
and a.error_msg is null 
and not exists (select 1 from utility_account b
					where a.bus_segment_id = b.bus_segment_id
					and a.utility_account_id= b.utility_account_id
					)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP invalid bs_id/ua_id Combo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//Detect bad combinations of RU/UA/BS
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 166: Invalid RU/UA/BS combo (ru_id/ua_id/bs_id ='||to_char(a.retirement_unit_id)||'/'||to_char(a.utility_account_id)||'/'||to_char(a.bus_segment_id)||
								') RU desc = '||	(select trim(r.description) from retirement_unit r where a.retirement_unit_id = r.retirement_unit_id)||' *Review the Propery Unit Details* '||a.unit_item_source, 1, 2000)
where a.company_id = :i_company_id
and a.error_msg is null 
and not exists (select 1 from util_acct_prop_unit b
					where a.bus_segment_id = b.bus_segment_id
					and a.utility_account_id= b.utility_account_id
					and a.property_unit_id = b.property_unit_id
					)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP invalid RU/UA/BS Combo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    
  
//Detect bad combinations of RU/PG
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 167: Invalid RU/PG combo (ru_id/pg_id ='||to_char(a.retirement_unit_id)||'/'||to_char(a.property_group_id)||
								') RU desc = '||	(select trim(r.description) from retirement_unit r where a.retirement_unit_id = r.retirement_unit_id)||
								', PG desc = '||	(select trim(g.description) from property_group g where a.property_group_id = g.property_group_id)||' *Review the Propery Unit Details* '||a.unit_item_source, 1, 2000)
where a.company_id = :i_company_id
and a.error_msg is null 
and not exists (select 1 from prop_group_prop_unit b
					where a.property_unit_id = b.property_unit_id
					and a.property_group_id= b.property_group_id 
					)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP invalid RU/PG Combo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//Detect bad combinations of RU/Loc Type
//first update location_type_id
update UNITIZE_WO_STG_UNITS_TEMP a
set a.location_type_id = (select b.location_type_id from major_location b where a.major_location_id = b.major_location_id)
where a.company_id = :i_company_id
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP.location_type_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//validate the func class and loc type
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 190: Invalid UT/AL for Func Class/Loc Type (ut_id/al_id ='||to_char(a.utility_account_id)||'/'||to_char(a.asset_location_id)||
								') FC desc = '||	(select trim(f.description) from func_class f where a.ua_func_class_id = f.func_class_id)||
								', LT desc = '||	(select trim(t.description) from location_type t where a.location_type_id = t.location_type_id)||' *Review Functional Class/Location Type table* '||a.unit_item_source, 1, 2000)
where a.company_id = :i_company_id
and a.error_msg is null 
and not exists (select 1 from func_class_loc_type b
					where a.ua_func_class_id = b.func_class_id
					and a.location_type_id= b.location_type_id 
					)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null); 
					
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP invalid UT/AL Combo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//validate the asset_id
update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 287: Invalid asset_id = '||to_char(a.ldg_asset_id)||' *Review on Unit Item* '||a.unit_item_source, 1, 2000)
where a.company_id = :i_company_id
and a.error_msg is null 
and a.ldg_asset_id > 0
and not exists (select 1 from cpr_ledger b
					where a.ldg_asset_id = b.asset_id 
					);
					
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP invalid ldg_asset_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

update UNITIZE_WO_STG_UNITS_TEMP a
set a.error_msg = substrb('Unitization 183: Work orders with subledger retirement units must be unitized manually." *Use Manual Unitization*', 1, 2000) 
where a.company_id = :i_company_id  
and a.error_msg is null 
and exists ( select 1
				from retirement_unit d, property_unit e, subledger_control f
				where a.retirement_unit_id = d.retirement_unit_id
				and d.property_unit_id = e.property_unit_id  
				and e.subledger_type_id = f.subledger_type_id
				and f.depreciation_indicator not in (0, 3)
				) 
and a.error_msg is null;
					
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP subledger: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//Handle errors identified in the default functions above.  
of_rtn = of_errors('UNITS')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  


return 1

end function

public function integer of_unit_item_inserted_validations ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Verify critical data validations for unit items already made in a prior unitization run for the work orders being unitized.
//Already made Unit Items from a prior unitization run needs to be validated for 2 important attributes that could potentially be changed after unitization
//   has already been run and not yet posted for the work orders.  These 2 validations do not need to be performed for new Unit Items being made in the current run.
//   New Unit Items will automatically have the correct COMPANY_ID and RETIREMENT_UNIT_ID by the nature of the syntax used for Units creation.
//Use the UNITIZE_WO_STG_UNITS_TEMP table to store the bad Unit Items.  Hijack the NEW_UNIT_ITEM_ID column to be the UNIT_ITEM_ID from UNITIZED_WORK_ORDER table.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn
string error_message

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": validating existing unit items..."
of_log()  

///////////////////WO level validations//////////////////////////

//Detect bad combinations of CO/WO
update UNITIZE_WO_LIST_TEMP a
set a.error_msg = substrb('Unitization 118: Company Id from the Unit Items does not match the company being processed. *Reset Unitization and try again*', 1, 2000), a.rtn_code = -1
where a.company_id = :i_company_id  
and exists (select 1 from UNITIZED_WORK_ORDER b 
				where a.work_order_id = b.work_order_id 
				and b.unit_item_id > 0
				and b.company_id <> :i_company_id
				and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS c
									where b.work_order_id = c.work_order_id
									and b.unit_item_id = c.unit_item_id
									)
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP invalid co_id/wo Combo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//Detect bad use of retirement unit
update UNITIZE_WO_LIST_TEMP a
set a.error_msg = substrb('Unitization 119: NON-UNITIZED Retirement Unit from Unit Items cannot be used. *Reset Unitization and try again*', 1, 2000), a.rtn_code = -1
where a.company_id = :i_company_id  
and exists (select 1 from UNITIZED_WORK_ORDER b 
				where a.work_order_id = b.work_order_id 
				and b.unit_item_id > 0
				and b.retirement_unit_id < 5
				and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS c
									where b.work_order_id = c.work_order_id
									and b.unit_item_id = c.unit_item_id
									)
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP invalid Non-Unitized RU: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//Detect use of old subledger types for if not being Manual Unitized
update UNITIZE_WO_LIST_TEMP a
set a.error_msg = substrb('Unitization 183: Work orders with subledger retirement units must be unitized manually." *Use Manual Unitization*', 1, 2000), a.rtn_code = -1
where a.company_id = :i_company_id  
and exists (select 1 from UNITIZED_WORK_ORDER b 
				where a.work_order_id = b.work_order_id 
				and b.unit_item_id > 0
				and b.retirement_unit_id in (select d.retirement_unit_id 
													from retirement_unit d, property_unit e, subledger_control f
													where d.property_unit_id = e.property_unit_id  
													and e.subledger_type_id = f.subledger_type_id
													and f.depreciation_indicator not in (0, 3)
													)
				and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS c
									where b.work_order_id = c.work_order_id
									and b.unit_item_id = c.unit_item_id
									)
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP Subledger RU: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//Handle errors identified in the validate function above.  
of_rtn = of_errors('WO')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if   		

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//maint 44271: try to backfill for bad Unit Items already made that have missing bus segment or sub account (no asset_id).
//                   there is code later for backfilling null values for Unit Items that have asset_id > 0 found in CPR_LEDGER.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

update unitized_work_order a
set a.bus_segment_id = (select b.bus_segment_id from work_order_control b
								where a.work_order_id = b.work_order_id
								)
where a.company_id = :i_company_id
and a.unit_item_id > 0
and a.ldg_asset_id is null 
and a.bus_segment_id is null 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unitized_work_order default bus_segment_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

update unitized_work_order a
set a.sub_account_id = (select min(b.sub_account_id) from sub_account b 
								where a.utility_account_id = b.utility_account_id
								and a.bus_segment_id = b.bus_segment_id
								and nvl(b.status_code_id,1) = 1  /*active*/
								)
where a.company_id = :i_company_id
and a.unit_item_id > 0
and a.ldg_asset_id is null 
and a.sub_account_id is null 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unitized_work_order default sub_account_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

return 1
end function

public function integer of_unit_item_class_codes ();longlong num_nrows, class_code_default


i_msg[] = i_null_arrary[] 
i_msg[1] =  ": updating unit item class codes..."
of_log() 


//Reset (remove) old  class codes on and not yet posted units
delete from unit_item_class_code a
where exists ( select 1
				  from UNITIZE_ELIG_UNITS_TEMP b 
				  where a.work_order_id = b.work_order_id
				  and a.unit_item_id = b.unit_item_id  
				  )
;
					  
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: reset unit_item_class_code: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

			  
//1st priority:  1 for 1 unit items
insert into unit_Item_class_code (unit_item_id, work_ordeR_id, class_code_id, value)
select a.unit_item_id, a.work_order_id, c.class_code_id, c.value
from UNITIZE_ELIG_UNITS_TEMP a, wo_estimate_class_code c, class_code d, UNITIZE_WO_LIST_TEMP x
where a.work_order_id = x.work_order_id   
and a.unit_estimate_id = c.estimate_id 
and c.class_code_id = d.class_code_id
and x.company_id = :i_company_id
and x.has_elig_adds = 1
and x.est_unit_item_option = 1 /*1 for 1*/
and d.cpr_indicator  = 1    
and a.expenditure_type_id = 1
and x.error_msg is null;
					  
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into unit_item_class_code 1: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//2nd priority:  summarized work order level class codes
insert into unit_item_class_code (work_order_id, unit_item_id, class_code_id, value)
select distinct b.work_order_id, b.unit_item_id, wc.class_code_id, wc.value
from work_order_class_code wc, UNITIZE_WO_LIST_TEMP a, UNITIZE_ELIG_UNITS_TEMP b,  class_code c
where wc.work_order_id = a.work_order_id
and a.work_order_id = b.work_order_id
and a.company_id = :i_company_id 
and a.has_elig_adds = 1
and a.error_msg is null
and wc.class_code_id = c.class_code_id   
and c.cpr_indicator  = 1    
and b.expenditure_type_id = 1
and not exists ( select 1
				  from unit_item_class_code d
				  where wc.work_order_id = d.work_order_id
				  and wc.class_code_id = d.class_code_id
				  and b.unit_item_id = d.unit_item_id
				  )
and a.error_msg is null;
					  
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into unit_item_class_code wo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//determine if any class_code_default setup in the database
select count(*) into :class_code_default from class_code_default;  

if class_code_default > 0 then
		
	//last priority:  class_code_default
	insert into unit_item_class_code (work_order_id, unit_item_id, class_code_id, value)
	select distinct a.work_order_id, a.unit_item_id, c.class_code_id, c.value
	from UNITIZE_ELIG_UNITS_TEMP a, retirement_unit b, class_code_default c, class_code d, UNITIZE_WO_LIST_TEMP x
	where x.work_order_id = a.work_order_id
	and x.company_id = :i_company_id
	and x.has_elig_adds = 1
	and a.expenditure_type_id = 1
	and a.retirement_unit_id = b.retirement_unit_id 
	and b.property_unit_id = c.property_unit_id   
	and c.class_code_id = d.class_code_id  
	and d.cwip_indicator = 0 
	and d.cpr_indicator = 1    
	and not exists ( select 1
					  from unit_item_class_code e
					  where a.work_order_id = e.work_order_id
					  and a.unit_item_id = e.unit_item_id
					  and d.class_code_id = e.class_code_id
					  )
	and x.error_msg is null;
					  
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into unit_item_class_code defaults: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 
	
return 1
end function

public function integer of_unit_item_class_codes_required (longlong a_wo_id);longlong of_rtn, missing_cc_cnt, i, cc_required_id, cc_security_missing, num_nrows
string cc_desc, error_text, unit_description

//the datawindow,dw_required_class_codes, has the SQL to determine if the work order is missing required class codes
missing_cc_cnt = i_ds_dw_required_cc.retrieve(a_wo_id)

if missing_cc_cnt < 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  "Error: retrieving dw_required_class_codes for wo_id = " + string(a_wo_id) + " " + i_ds_dw_required_cc.i_sqlca_sqlerrtext
	of_log() 
	return -1
end if 
 
if missing_cc_cnt > 0 then 	
	
	for i = 1 to missing_cc_cnt 
		
		cc_desc  = i_ds_dw_required_cc.getitemstring(i, 'class_code_class_code')
		unit_description =  i_ds_dw_required_cc.getitemstring(i, 'description')
		
		error_text = "Unitization 200: Class Code = " + cc_desc + " is required for Unit Item = " + unit_description + " . " 
		
//		//comment out below:  if the user does not have security to see the required class code, it's a class security issue, not a unitization issue.
//		cc_required_id =  i_ds_required_cc.getitemnumber(i, 'class_code_id')
//		cc_security_missing = 0
//		select count(*) into :cc_security_missing
//		from pp_class_code_groups g, pp_security_users_groups s 
//		where g.groups = s.groups
//		and s.users = lower(user)
//		and g.class_code_id = :cc_required_id; 

		update UNITIZE_WO_LIST_TEMP a
		set a.error_msg = substrb(:error_text, 1, 2000), a.rtn_code = -1
		where a.company_id = :i_company_id  
		and a.work_order_id = :a_wo_id
		and a.error_msg is null ;

		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP required cc check: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows    
		
	next
   
end if

return 1
end function

public function integer of_unit_item_descriptions ();longlong num_nrows 

//use the cpr_ledger descriptions and gl_account_id for unit items with asset_id is not null.
//Note:  related asset logic uses negative asset_id, but this update should not update these attributes for related assets.  Only update for positive asset_ids
update UNITIZE_WO_STG_UNITS_TEMP a
set (a.description, a.short_description, a.gl_account_id) = 
									(select b.long_description, b.description, b.gl_account_id
									from cpr_ledger b
									where a.ldg_asset_id = b.asset_id 
									)
where a.company_id = :i_company_id
and a.ldg_asset_id > 0
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null)
and exists (select 1
				from cpr_ledger b
				where a.ldg_asset_id = b.asset_id 
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP asset descriptions: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    


//catch all for any new unit items missing descriptions
update UNITIZE_WO_STG_UNITS_TEMP a
set a.short_description = 'Work Order Addition'  
where a.company_id = :i_company_id
and a.short_description is null
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP missing short_description: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

update UNITIZE_WO_STG_UNITS_TEMP a
set a.description = 
						(select b.long_description 
						from retirement_unit b
						where a.retirement_unit_id = b.retirement_unit_id
						)
where a.company_id = :i_company_id
and a.description is null
and a.error_msg is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP missing long_description: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    


return 1
end function

public function integer of_unit_item_refresh_for_one4one ();////////////////////////////////////////////////////////////////////////////////////////////////
//Update the unit attributes for estimates that are directly linked to units "one for one"
////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": updating one4one estimates..."
of_log() 

/*wo_est_trans_type_id: 4 = Partial Replacement  3 = Replacement: do not update ldg_asset_id using the asset_id on wo_estimate for additions*/
update unitized_work_order a
set (a.retirement_unit_id, a.utility_account_id, a.bus_segment_id, a.sub_account_id, a.property_group_id, a.asset_location_id, a.quantity, a.replacement_amount, 
	a.gl_account_id, a.ldg_asset_id, a.serial_number, a.in_service_date, 
	a.short_description, a.description) =
	(select b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, b.property_group_id, b.asset_location_id, b.quantity, b.replacement_amount, 
	c.unitized_gl_account, decode(b.expenditure_type_id, 2, b.asset_id, decode(b.wo_est_trans_type_id, 3, null, 4, null, b.asset_id)), trim(b.serial_number), b.est_in_service_date, 
	decode(trim(b.unit_desc), '', a.short_description, null, a.short_description, b.unit_desc), decode(trim(b.unit_long_desc), '', a.description, null, a.description, b.unit_long_desc)
	from wo_estimate b,  UNITIZE_WO_LIST_TEMP c
	where a.unit_estimate_id = b.estimate_id  /*1 for 1 join*/
	and c.max_revision = b.revision /*need to include join on revision*/
	and a.work_order_id = b.work_order_id
	and b.work_order_id = c.work_order_id
	and b.batch_unit_item_id is null
	and b.expenditure_type_id in (1,2)
	and c.company_id = :i_company_id	
	and c.est_unit_item_option = 1 /*1 for 1 estimates*/
	and c.error_msg is null
	)
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and a.unit_estimate_id is not null  
and a.unit_item_id > 0 
and a.company_id = :i_company_id
and exists (select 1
			from wo_estimate b, UNITIZE_WO_LIST_TEMP c
			where a.unit_estimate_id = b.estimate_id /*1 for 1 join*/
			and c.max_revision = b.revision /*need to include join on revision*/
			and a.work_order_id = b.work_order_id
			and b.work_order_id = c.work_order_id
			and b.batch_unit_item_id is null
			and b.expenditure_type_id in (1,2)
			and c.company_id = :i_company_id	
			and c.est_unit_item_option = 1 /*1 for 1 estimates*/
			and c.error_msg is null
			)
and not exists (select 1 from wo_unit_item_pend_trans pt
					where pt.work_order_id = a.work_order_id
					and pt.unit_item_id = a.unit_item_id); 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: refresh unitized_work_order for 1for1 est option: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//maint 44314:  backfill missing sub account on unitized_work_order where asset_id is found in cpr_ledger, but include bus segment and utility account due to foreign key constraint
update unitized_work_order a
set (a.utility_account_id, a.bus_segment_id, a.sub_account_id) = 
							(select b.utility_account_id, b.bus_segment_id, b.sub_account_id 
								from cpr_ledger b
								where a.ldg_asset_id = b.asset_id
								)
where a.ldg_asset_id > 0
and a.unit_item_id > 0
and a.sub_account_id is null 
and not exists (select 1 from wo_unit_item_pend_trans pt
					where pt.work_order_id = a.work_order_id
					and pt.unit_item_id = a.unit_item_id)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and x.company_id = :i_company_id
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unitized_work_order.sub_account_id for asset_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//maint 44314: backfill missing property_group_id on unitized_work_order where asset_id is found in cpr_ledger
update unitized_work_order a
set a.property_group_id = (select b.property_group_id from cpr_ledger b
								where a.ldg_asset_id = b.asset_id
								)
where a.ldg_asset_id > 0
and a.unit_item_id > 0 
and a.property_group_id is null 
and not exists (select 1 from wo_unit_item_pend_trans pt
					where pt.work_order_id = a.work_order_id
					and pt.unit_item_id = a.unit_item_id)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and x.company_id = :i_company_id
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unitized_work_order.property_group_id for asset_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  
 
return 1

end function

public function integer of_direct_assign ();//In Unitization, Direct Assignment looks to match Charges that have minimum required Unit information on its Charge attributes to the Unit Items created for a work order.  
//The minimum required attributes are RETIREMENT UNIT and UTILITY ACCOUNT.  
//A work order must have both RETIREMENT UNIT and UTILITY ACCOUNT information on its Charges in order to Direct Assign to a Unit Item where a 
//          matching RETIREMENT UNIT and UTILITY ACCOUNT combination is found.  
//
//The Direct Assign logic does not use priority rules where if there is no match on the optional fields (sub account, asset location, serial number).  
//The matching will look for the next best match.  For example, if asset location is provided on the Charge Group, ASSET_LOCATION_ID = X, but there is 
//    no Unit Item on UWO table with ASSET_LOCATION_ID = X, then there is no match and the Direct Assign for that CGC record will not occur.  
//The CGC record will go through the Allocate Remaining part of the unitization processing. 
//There is no additional attempt in the Direct Assign logic to match to a UWO record where the asset location is excluded from the matching logic.
//The BUS_SEGMENT_ID is never used as part of the matching logic for Direct Assign.

//the old auto101 code does not join for BUS_SEGMENT_ID. there is an expectation for the UTILITY_ACCOUNT_ID to be unique, even though the
//   datamodel does not actuall have UTILITY_ACCOUNT_ID as a unique identifier.

//Direct Assign cannot occur if the following are true:
//1.  The Charges do not have UTILITY ACCOUNT information.
//2.  The Charges do not have RETIREMENT UNIT information.
//3.  The Unit Items have a UTILITY ACCOUNT/RETIREMENT UNIT combination that does not match the Charges UTILITY ACCOUNT/RETIREMENT UNIT combination.
//4.  Late Close Unitization

//property_group_id is never part of the matching logic because it is not on CWIP_CHARGE.
//use NVL() around the fields that provide precision for direct assign.  if isnull, those fields are not required to match.
longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": direct assigning charges..."
of_log() 

//Additions and RWIP
update charge_group_control a
set a.unit_item_id = 
		(select b.unit_item_id 
		from (select d.unit_item_id, d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, d.sub_account_id, d.asset_location_id, d.serial_number, 
		

				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_none,

				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, d.sub_account_id, d.asset_location_id, nvl(trim(d.serial_number), ' nullserial_number')
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_all,
					 
				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, d.sub_account_id
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_sa,

				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, d.sub_account_id, nvl(trim(d.serial_number), ' nullserial_number')
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_sa_sn,

				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, d.sub_account_id, d.asset_location_id
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_sa_al,

				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, d.asset_location_id
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_al,

				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, d.asset_location_id, nvl(trim(d.serial_number), ' nullserial_number')
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_al_sn,

				  DENSE_RANK() over (PARTITION BY d.work_order_id, d.expenditure_type_id, d.utility_account_id, d.retirement_unit_id, nvl(trim(d.serial_number), ' nullserial_number')
				    order by nvl(trim(d.serial_number), ' nullserial_number'), d.unit_item_id) ranking_sn

				from UNITIZE_ELIG_UNITS_TEMP d  
				) b
		where a.work_order_id = b.work_order_id  
		and a.expenditure_type_id = b.expenditure_type_id
		
		and decode(a.sub_account_id, null,
					decode(a.asset_location_id, null,
							decode(trim(a.serial_number),
										null, b.ranking_none, //sa is null : al is null : sn is null
										b.ranking_sn), //sa is null : al is null : sn is not null
							decode(trim(a.serial_number),
										null, b.ranking_al, //sa is null : al is not null : sn is null
										b.ranking_al_sn)), //sa is null : al is not null : sn is not null
					decode(a.asset_location_id, null,
							decode(trim(a.serial_number),
										null, b.ranking_sa, //sa is not null : al is null : sn is null
										b.ranking_sa_sn), //sa is not null : al is null : sn is not null
							decode(trim(a.serial_number),
										null, b.ranking_sa_al, //sa is not null : al is not null : sn is null
										b.ranking_all)) //sa is not null : al is not null : sn is not null
					) = 1
		
		
		and a.retirement_unit_id = b.retirement_unit_id  /*required*/
		and a.utility_account_id = b.utility_account_id  /*required*/ 
		and nvl(a.sub_account_id, b.sub_account_id) = b.sub_account_id /*optional if null on cgc*/
		and nvl(a.asset_location_id, b.asset_location_id) = b.asset_location_id /*optional if null on cgc*/
		and nvl(trim(a.serial_number), 'nullserial_number') =  nvl(trim(b.serial_number), 'nullserial_number')  /*optional if null on cgc*/
		)
		, a.orig_charge_group_id = a.charge_group_id /*flag the record as having been direct assigned by making orig_charge_group_id = charge_group_id*/
where exists (select 1 from UNITIZE_WO_LIST_TEMP d where a.work_order_id = d.work_order_id and d.company_id = :i_company_id and d.error_msg is null )  /*adding this makes the sql faster*/
and a.unit_item_id is null   
and a.batch_unit_item_id is null 
and a.expenditure_type_id in (1, 2)
and a.utility_account_id is not null
and a.retirement_unit_id is not null
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and exists (select 1
				from UNITIZE_ELIG_UNITS_TEMP d 
				where a.work_order_id = d.work_order_id 
				and a.expenditure_type_id = d.expenditure_type_id /*required*/
				and a.retirement_unit_id = d.retirement_unit_id  /*required*/
				and a.utility_account_id = d.utility_account_id   /*required*/
				and nvl(a.sub_account_id, d.sub_account_id) = d.sub_account_id  /*must match if not null*/
				and nvl(a.asset_location_id, d.asset_location_id) = d.asset_location_id  /*must match if not null*/
				and nvl(trim(a.serial_number), 'nullserial_number') = nvl(trim(d.serial_number), 'nullserial_number')  /*must use for matching:  null to null and abc123 to abc123*/
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update charge_group_control.unit_item_id best: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows        

//maint-43648: only call of_update_charge_groups_unitized() if Direct Assign happened
if num_nrows > 0 then
	//update the CGC columns to match to the UWO columns for Unitized charge groups.
	of_rtn = of_update_charge_groups_unitized('DIRECT')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1 
	end if
end if 

commit;

return 1
end function

public function integer of_allocation_prepare ();/////////////////////////////////////////////////////////////////////////////////////////////////////
//Reset/clear out old data for tables used by both Regular and Late Close Unitization
/////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, check_count_estimates, rtn, check_count_std, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": preparing allocation tables..."
of_log() 

delete from UNITIZE_RATIO_UNITS_TEMP;
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_RATIO_UNITS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

delete from UNITIZE_ALLOC_ERRORS_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_ERRORS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//more data preparation specific to Regular vs Late Close 
if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then	
	//Regular Close
	of_rtn  = of_allocation_prepare_regular()
	//error handling done in function call
	if of_rtn < 0 then 
		return -1
	end if 	
else
	//Late Close
	of_rtn  = of_allocation_prepare_late()
	//error handling done in function call
	if of_rtn < 0 then 
		return -1
	end if 
end if 

return 1
end function

public function integer of_allocation_by_priorities ();//////////////////////////////////////////////////////////////////////////////////
//This applies to regular Unitization Close.  Late Close does not use this.
//////////////////////////////////////////////////////////////////////////////////

string alloc_priorities_sql, rtn_string
longlong i, num_alloc_priorities, priority_number, of_rtn, check_count
uo_ds_top ds_alloc_priorities
ds_alloc_priorities = create uo_ds_top

alloc_priorities_sql = "select distinct alloc_priority from UNITIZE_ALLOC_LIST_TEMP where company_id = " + string(i_company_id) + 	" order by 1 "
rtn_string = f_create_dynamic_ds(ds_alloc_priorities, 'grid', alloc_priorities_sql, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_alloc_priorities for priority allocation: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_alloc_priorities = ds_alloc_priorities.RowCount()

for i = 1 to num_alloc_priorities
	 
	priority_number = ds_alloc_priorities.getitemnumber(i, 1) 
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": allocating for priority " + string(priority_number) + "..."
	of_log() 
	
	if i = 1 then
		i_first_priority = true
	else
		i_first_priority = false
	end if
	
	//need to RESET the staging tables.
	//update max(ids) and clear out the UNITIZE_ALLOC_CGC_INSERTS_TEMP and UNITIZE_ALLOC_ERRORS_TEMP tables for each new priority
	of_rtn = of_allocation_refresh_staged_data()	
	if of_rtn <> 1 then
		//errors are handled inside function call
		destroy ds_alloc_priorities
		return -1
	end if
	
	//get allocation statistics
	//of_errors('ALLO') function is called inside this function for "estimates" alloc type.
	of_rtn = of_allocation_statistic(priority_number)
	if of_rtn <> 1 then
		//errors are handled inside function call 
		destroy ds_alloc_priorities
		return -1
	end if  
	
	//update the RATIO columns for each group of records in the allocation set
	//of_errors('ALLO') function is called at the end of this function
	of_rtn = of_allocation_statistic_ratio(priority_number)	
	if of_rtn <> 1 then
		//errors are handled inside function call
		destroy ds_alloc_priorities
		return -1
	end if	
	
	//allocate charges first by ut account
	//of_errors('ALLO') function is called at the end of this function
	of_rtn = of_allocate_charges_by_acct(priority_number)	
	if of_rtn <> 1 then
		//errors are handled inside function call
		destroy ds_alloc_priorities
		return -1
	end if
	
	//allocate remaining charges
	//of_errors('ALLO') function is called at the end of this function
	of_rtn = of_allocate_charges_remaining(priority_number)	
	if of_rtn <> 1 then
		//errors are handled inside function call
		destroy ds_alloc_priorities
		return -1
	end if 

	//Insert into CHARGE_GROUP_CONTROL the split charges created from allocation
	of_rtn = of_insert_into_charge_group_control_allo(priority_number)
	if of_rtn <> 1 then
		//errors are handled inside function call
		destroy ds_alloc_priorities
		return -1 
	end if	
	
	//Save to real table for troubleshooting. maint 43357: archive after insert
	of_rtn = of_archive_alloc_cgc_inserts_temp()	
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if 
	
	//determine if work orders have CHARGE_GROUP_CONTROL records with ALLOC_IDs without any allocation results
	//this check for missing allocation results must occur after the insert into CHARGE_GROUP_CONTROL, because alloc types based on Actuals must get inserted into CGC first.
	//of_errors('ALLO') function is called at the end of this function 
	of_rtn = of_allocation_results_missing(priority_number)	
	if of_rtn <> 1 then
		//errors are handled inside function call
		destroy ds_alloc_priorities
		return -1
	end if 
		
next

destroy ds_alloc_priorities

/////maint-43648:  this is no longer needed.  unit attributes are now part of of_insert_into_charge_group_control_allo()
////update the CGC columns to match to the UWO columns for Unitized work orders.
//of_rtn = of_update_charge_groups_unitized('ALLO')
//if of_rtn <> 1 then
//	//errors are handled inside function call
//	return -1 
//end if 

//Save to real table for troubleshooting
of_rtn = of_archive_ratio_units_temp()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 
 
return 1
   
end function

public function integer of_allocation_statistic (longlong a_priority_number);string alloc_types_sqls, alloc_type, alloc_basis, rtn_string
longlong i, num_alloc_types, est_unit_item_option, rtn, of_rtn, num_nrows
uo_ds_top ds_alloc_types
ds_alloc_types = create uo_ds_top

//note:  if the 2st priority number has actuals and estimates/standards, where Direct Assign did not occur, then, the actuals based allocations will fail
//          then the client must change the priority number on actuals to be a number AFTER estimates/standards.
alloc_types_sqls = "select distinct alloc_type from UNITIZE_ALLOC_LIST_TEMP where company_id = " + string(i_company_id) + &
						" and alloc_priority = "  + string(a_priority_number) + "  "
rtn_string = f_create_dynamic_ds(ds_alloc_types, 'grid', alloc_types_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_alloc_types for priority allocation: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_alloc_types = ds_alloc_types.RowCount()

for i = 1 to num_alloc_types
	
	alloc_type = ds_alloc_types.getitemstring(i, 1)
	
	//there are only 4 types:  actuals, estimates, standards, late. 	
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   alloc type: " + alloc_type + "..."
	of_log() 
	
	choose case alloc_type
		case 'actuals'
			rtn = of_allocation_statistic_actuals(a_priority_number)
			if rtn <> 1 then
				//errors are handled inside function call
				destroy ds_alloc_types
				return -1
			end if 
		case 'standards'
			rtn = of_allocation_statistic_standards(a_priority_number)
			if rtn <> 1 then
				//errors are handled inside function call
				destroy ds_alloc_types
				return -1
			end if 
		case 'estimates' 
			
			//wo level validations for better error messaging prior to gathering allocation details.
			//unit items would already be made at this point.
			rtn = of_allocation_validate_wo_est(a_priority_number)
			if rtn <> 1 then
				//errors are handled inside function call
				destroy ds_alloc_types
				return -1
			end if 
	
			rtn = of_allocation_statistic_estimates(a_priority_number)
			if rtn <> 1 then
				//errors are handled inside function call
				destroy ds_alloc_types
				return -1
			end if 
			
			//determine if allocation neg vs pos tolerance limits are exceeded.
			//of_errors('ALLO') function is called at the end of this function
			rtn = of_allocation_tolerance_check(a_priority_number)
			if rtn <> 1 then
				//errors are handled inside function call
				destroy ds_alloc_types
				return -1
			end if 	
			
		case else
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: invalid allocation type: " + alloc_type
			of_log() 
			rollback;		  
			destroy ds_alloc_types
			return -1  
	end choose
	
next

destroy ds_alloc_types

return 1

end function

public function integer of_allocation_statistic_actuals (longlong a_priority_number);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//actuals based allocation statistics vary by priorities, because the update to CHARGE_GROUP_CONTROL result in different allocation ratios based on the timing of the unitization updates.
//setting up the allocation statistics for actuals needs to be done for each priority in the unitization processing. 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows

//old datawindow: dw_unit_allo_calc

insert into UNITIZE_RATIO_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
ALLOC_TYPE, ALLOC_BASIS, ALLOC_ID, ALLOC_PRIORITY, ALLOC_STATISTIC)
SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
	c.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
	TRIM(LOWER(e.alloc_type)), TRIM(LOWER(e.alloc_basis)), d.ALLOC_ID, :a_priority_number, 
	sum(decode(lower(trim(e.alloc_basis)), 'quantity', nvl(c.quantity,0), c.AMOUNT)) 
		
 FROM UNITIZE_ELIG_UNITS_TEMP b,
		CHARGE_GROUP_CONTROL c, 
		UNIT_ALLOC_BASIS d,
		UNIT_ALLOCATION e,
		UNITIZE_WO_LIST_TEMP x
		
WHERE b.WORK_ORDER_ID = c.WORK_ORDER_ID
	AND b.UNIT_ITEM_ID = c.UNIT_ITEM_ID    
	AND b.WORK_ORDER_ID = x.WORK_ORDER_ID    
	AND c.WORK_ORDER_ID = x.WORK_ORDER_ID    
	AND d.ALLOC_ID = e.ALLOC_ID 
	AND b.EXCLUDE_FROM_ALLOCATIONS = 0
	/*AND c.ALLOC_ID = e.ALLOC_ID <-- alloc_id on CGC is null for allocated records*/
	AND d.CHARGE_TYPE_ID = c.CHARGE_TYPE_ID      
	AND c.UNIT_ITEM_ID > 0
	AND x.COMPANY_ID = :i_company_id
	AND x.ERROR_MSG is null  
	AND c.BATCH_UNIT_ITEM_ID is null    
	AND e.ALLOC_ID in (select f.alloc_id from UNITIZE_ALLOC_LIST_TEMP f
							where f.company_id = :i_company_id 
							and f.alloc_priority = :a_priority_number
							and f.alloc_type = 'actuals') 	 
	AND c.expenditure_type_id in (1,2)    
	AND c.EXPENDITURE_TYPE_ID = b.EXPENDITURE_TYPE_ID
	AND exists (select 1 from CHARGE_GROUP_CONTROL cc
					where c.work_order_id = cc.work_order_id
					and c.expenditure_type_id = cc.expenditure_type_id
					and cc.batch_unit_item_id is null
					and cc.allocation_priority = :a_priority_number
					and cc.alloc_id in (select f.alloc_id from UNITIZE_ALLOC_LIST_TEMP f
							where f.company_id = :i_company_id 
							and f.alloc_priority = :a_priority_number
							and f.alloc_type = 'actuals') 	 
					)  /*maint 45082*/
	
GROUP BY  x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID,  
		c.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
		TRIM(LOWER(e.alloc_type)), TRIM(LOWER(e.alloc_basis)), d.ALLOC_ID;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_RATIO_UNITS_TEMP (actuals): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

return 1
end function

public function integer of_allocation_statistic_ratio (longlong a_priority_number);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Important fact about the SQLs below:  
//     If the "alloc_ratio" values are all 0 based on poor configuration, no divide by zero error will occur using RATIO_TO_REPORT,
//     because this oracle function does not divide in the tradional sense of numerator/denominator.  
//     The logic that determins the "alloc_ratio" must have additional logic to address situations where the
//     allocation records all have "alloc_ratio" = 0.
//     Without specific logic to catch scenarios where all records in the allocation
//     pool have "alloc_ratio" = 0, later functions that allocate the amounts will allocation 100% to the first
//     allocation record using the rounding syntax that compares amount to allocate vs.
//     total allocated amount.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn, num_nrows2, num_nrows1

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   calculating alloc ratios..."
of_log() 

//update the "alloc_ratio_by_sub" alloc_ratio to be used when CGC utility_account_id = not null and sub_account_id is not null and wo is configured FOR "unitize_by_account" = "YES."
update UNITIZE_RATIO_UNITS_TEMP a
set a.alloc_ratio_by_sub = (select ratio
								from 
									(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.alloc_id, b.utility_account_id, b.sub_account_id) ratio,
									b.company_id, b.work_order_id, b.expenditure_type_id, b.alloc_id, b.utility_account_id, b.sub_account_id, b.unit_item_id
									from UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP x
									where b.work_order_id = x.work_order_id
									and x.error_msg is null
									and x.company_id = :i_company_id 
									and b.alloc_priority = :a_priority_number 
									and b.utility_account_id is not null
									and b.sub_account_id is not null
									and b.unitize_by_account = 1 
									) c
								where a.company_id = c.company_id
								and a.work_order_id = c.work_order_id
								and a.expenditure_type_id = c.expenditure_type_id
								and a.alloc_id = c.alloc_id  
								and a.utility_account_id = c.utility_account_id
								and a.sub_account_id = c.sub_account_id
								and a.unit_item_id =c.unit_item_id
								)
where a.company_id = :i_company_id 
and a.alloc_priority = :a_priority_number 
and a.utility_account_id is not null
and a.sub_account_id is not null 
and a.unitize_by_account = 1
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio_by_sub: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the "alloc_ratio_by_ut" alloc_ratio to be used when CGC utility_account_id = not null and sub_account_id is null and wo is configured FOR "unitize_by_account" = "YES."
//do not include sub_account_id in the filtering to determine "alloc_ratio_by_ut."
update UNITIZE_RATIO_UNITS_TEMP a
set a.alloc_ratio_by_ut = (select ratio
								from 
									(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.alloc_id, b.utility_account_id) ratio,
									b.company_id, b.work_order_id, b.expenditure_type_id, b.alloc_id, b.utility_account_id, b.unit_item_id
									from UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP x
									where b.work_order_id = x.work_order_id
									and x.error_msg is null
									and x.company_id = :i_company_id 
									and b.alloc_priority = :a_priority_number 
									and b.utility_account_id is not null 
									and b.unitize_by_account = 1 
									) c
								where a.company_id = c.company_id
								and a.work_order_id = c.work_order_id
								and a.expenditure_type_id = c.expenditure_type_id
								and a.alloc_id = c.alloc_id  
								and a.utility_account_id = c.utility_account_id
								and a.unit_item_id =c.unit_item_id
								)
where a.company_id = :i_company_id 
and a.alloc_priority = :a_priority_number 
and a.utility_account_id is not null 
and a.unitize_by_account = 1
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio_by_ut: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the general alloc_ratio, which is used by the "Allocation Remaining" functionality and for all CGC data (regardless of the UTILITY_ACCOUNT_ID and SUB_ACCOUNT_ID data).
//do not include utility_account_id nor sub_account_id in the filtering to determine "alloc_ratio."
update UNITIZE_RATIO_UNITS_TEMP a
set a.alloc_ratio = (select ratio
						from 
							(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.alloc_id) ratio,
							b.company_id, b.work_order_id, b.expenditure_type_id, b.alloc_id, b.unit_item_id
							from UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP x
							where b.work_order_id = x.work_order_id
							and x.error_msg is null
							and x.company_id = :i_company_id 
							and b.alloc_priority = :a_priority_number 
							) c
						where a.company_id = c.company_id
						and a.work_order_id = c.work_order_id
						and a.expenditure_type_id = c.expenditure_type_id
						and a.alloc_id = c.alloc_id 
						and a.unit_item_id = c.unit_item_id
						)
where a.company_id = :i_company_id 
and a.alloc_priority = :a_priority_number
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Change alloc_ratio_by_sub from a 0 to a 1 for work orders there is only 1 allocation record and the alloc_ratio_by_sub = 0.
update UNITIZE_RATIO_UNITS_TEMP a 
set a.alloc_ratio_by_sub = 1
where a.company_id  = :i_company_id 
and nvl(a.alloc_ratio_by_sub,0) = 0 
and a.unitize_by_account = 1 
and a.alloc_priority = :a_priority_number
and exists (select 1
				from UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP x
				where b.work_order_id = x.work_order_id
				and x.company_id = :i_company_id
				and x.error_msg is null
				and a.company_id = b.company_id
				and a.work_order_id = b.work_order_id
				and a.unitize_by_account = b.unitize_by_account
				and a.expenditure_type_id = b.expenditure_type_id
				and a.utility_account_id = b.utility_account_id
				and a.sub_account_id = b.sub_account_id
				and a.alloc_id = b.alloc_id
				and a.alloc_priority = b.alloc_priority
				group by b.company_id, b.work_order_id,  b.expenditure_type_id, b.alloc_id, b.alloc_priority, b.utility_account_id 
				having count(b.work_order_id) = 1
				)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio_by_ut = 1: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Change alloc_ratio_by_ut from a 0 to a 1 for work orders there is only 1 allocation record and the alloc_ratio_by_ut = 0.
update UNITIZE_RATIO_UNITS_TEMP a 
set a.alloc_ratio_by_ut = 1
where a.company_id  = :i_company_id 
and nvl(a.alloc_ratio_by_ut,0) = 0 
and a.unitize_by_account = 1 
and a.alloc_priority = :a_priority_number
and exists (select 1
		from UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP x
		where b.work_order_id = x.work_order_id
		and x.company_id = :i_company_id
		and x.error_msg is null
		and a.company_id = b.company_id
		and a.work_order_id = b.work_order_id
		and a.unitize_by_account = b.unitize_by_account
		and a.expenditure_type_id = b.expenditure_type_id
		and a.utility_account_id = b.utility_account_id
		and a.alloc_id = b.alloc_id
		and a.alloc_priority = b.alloc_priority
		group by b.company_id, b.work_order_id,  b.expenditure_type_id, b.alloc_id, b.alloc_priority, b.utility_account_id 
		having count(b.work_order_id) = 1)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio_by_ut = 1: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Change alloc_ratio from a 0 to a 1 for work orders there is only 1 allocation record and the alloc_ratio = 0.  
//Do not filter for unitize_by_account = 0 only for this update, because a work order that is set to Unitize by Account = Yes and has no UTILITY_ACCOUNT_ID data on CGC still needs to go through regular unitization.
update UNITIZE_RATIO_UNITS_TEMP a 
set a.alloc_ratio = 1
where a.company_id  = :i_company_id 
and nvl(a.alloc_ratio,0) = 0  
and a.alloc_priority = :a_priority_number
and exists (select 1
		from UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP x
		where b.work_order_id = x.work_order_id
		and x.company_id = :i_company_id
		and x.error_msg is null 
		and a.company_id = b.company_id
		and a.work_order_id = b.work_order_id 
		and a.expenditure_type_id = b.expenditure_type_id
		and a.alloc_id = b.alloc_id
		and a.alloc_priority = b.alloc_priority
		group by b.company_id, b.work_order_id,  b.expenditure_type_id, b.alloc_id, b.alloc_priority
		having count(b.work_order_id) = 1)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio = 1: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Take care of nulls
update UNITIZE_RATIO_UNITS_TEMP a
set a.alloc_ratio = 0
where a.company_id = :i_company_id 
and a.alloc_priority = :a_priority_number 
and a.alloc_ratio is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

update UNITIZE_RATIO_UNITS_TEMP a
set a.alloc_ratio_by_sub = 0
where a.company_id = :i_company_id 
and a.alloc_priority = :a_priority_number 
and a.alloc_ratio_by_sub is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio_by_sub(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

update UNITIZE_RATIO_UNITS_TEMP a
set a.alloc_ratio_by_ut = 0
where a.company_id = :i_company_id 
and a.alloc_priority = :a_priority_number 
and a.alloc_ratio_by_ut is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_UNITS_TEMP.alloc_ratio_by_ut(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows
 

//Detect and capture errors with alloc_ratio being all 0 when there is more than 1 allocation record for wos.
//Do not identify this type of error for "alloc_ratio_by_ut" and "alloc_ratio_by_sub" ratios.
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id,  
substrb(decode(b.expenditure_type_id, 1, 
					'Unitization 147: All alloc ratios = 0; unable to perform Adds allocation for "'||c.description||'", priority = '||to_char(b.alloc_priority), 
						'Unitization 147R: All alloc ratios = 0; unable to perform RWIP allocation for "'||c.description||'", priority = '||to_char(b.alloc_priority) 
					
		), 1, 2000)
from UNITIZE_RATIO_UNITS_TEMP b, UNIT_ALLOCATION c,
	 (select count(1) count_by, sum(abs(x.alloc_ratio)) sum_ratio,  
	 	x.company_id, x.work_order_id,  x.expenditure_type_id, x.alloc_id, x.alloc_priority
		from UNITIZE_RATIO_UNITS_TEMP x, UNITIZE_WO_LIST_TEMP z
		where x.work_order_id = z.work_order_id
		and z.error_msg is null 
		and z.company_id = :i_company_id
		and x.alloc_priority = :a_priority_number 
		group by x.company_id, x.work_order_id,  x.expenditure_type_id, x.alloc_id, x.alloc_priority
		having count(1) > 1
		) check_ratio
where b.alloc_id = c.alloc_id
and c.alloc_type = 'estimates'   /*maint-45082*/
and b.company_id = :i_company_id
and b.alloc_priority = :a_priority_number 
and b.expenditure_type_id in (1,2)  
and check_ratio.work_order_id = b.work_order_id
and check_ratio.expenditure_type_id = b.expenditure_type_id
and check_ratio.alloc_id = b.alloc_id
and check_ratio.alloc_priority = b.alloc_priority
and check_ratio.sum_ratio = 0
and exists (select 1 from UNITIZE_WO_LIST_TEMP z
				where b.work_order_id = z.work_order_id
				and z.company_id = :i_company_id
				and z.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (estimates ratio): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows1 = sqlca.sqlnrows   

//Detect and capture errors specific for Standards with alloc_ratio being all 0 when there is more than 1 allocation record for wos.
//Do not identify this type of error for "alloc_ratio_by_ut" and "alloc_ratio_by_sub" ratios.
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id,  
substrb(decode(b.expenditure_type_id, 1, 
					'Unitization 147: All alloc ratios = 0; unable to perform Adds allocation for "'||c.description||'", priority = '||to_char(b.alloc_priority)||' *Check the Retire Units Standards Data*', 
						'Unitization 147R: All alloc ratios = 0; unable to perform RWIP allocation for "'||c.description||'", priority = '||to_char(b.alloc_priority)||' *Check the Retire Units Standards Data*' 
		), 1, 2000)
from UNITIZE_RATIO_UNITS_TEMP b, UNIT_ALLOCATION c,
	 (select count(1) count_by, sum(abs(x.alloc_ratio)) sum_ratio,  
	 	x.company_id, x.work_order_id,  x.expenditure_type_id, x.alloc_id, x.alloc_priority
		from UNITIZE_RATIO_UNITS_TEMP x, UNITIZE_WO_LIST_TEMP z
		where x.work_order_id = z.work_order_id
		and z.error_msg is null 
		and z.company_id = :i_company_id
		and x.alloc_priority = :a_priority_number 
		group by x.company_id, x.work_order_id,  x.expenditure_type_id, x.alloc_id, x.alloc_priority
		having count(1) > 1
		) check_ratio
where b.alloc_id = c.alloc_id
and c.alloc_type = 'standards'
and b.company_id = :i_company_id
and b.alloc_priority = :a_priority_number 
and b.expenditure_type_id in (1,2)  
and check_ratio.work_order_id = b.work_order_id
and check_ratio.expenditure_type_id = b.expenditure_type_id
and check_ratio.alloc_id = b.alloc_id
and check_ratio.alloc_priority = b.alloc_priority
and check_ratio.sum_ratio = 0
and exists (select 1 from UNITIZE_WO_LIST_TEMP z
				where b.work_order_id = z.work_order_id
				and z.company_id = :i_company_id
				and z.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (standards ratio): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows2 = sqlca.sqlnrows   


if num_nrows1 > 0 or num_nrows2 > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if

return 1
end function

public function integer of_allocate_charges_remaining (longlong a_priority_number);//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//This function creates the allocation data that will be inserted into CHARGE_GROUP_CONTROL.
//The data saved in the UNITIZE_ALLOC_CGC_INSERTS_TEMP table will alter be saved into the  UNITIZE_ALLOC_CGC_INSERTS_ARC table. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////last step in allocation of charges to unit items
////this function will occur after direct assign and allocation by utility account and sub account
////do not filter for unitize_by_account = 0 in this function.  
////this function should apply to all remaining charges not yet allocated.

longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   allocating remaining charges..."
of_log() 

insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
	(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id, 
	description, ratio_used, quantity, allocation_priority, 
	unitization_summary, book_summary_name, unit_item_id,
	amount  )
select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id, 
	substrb(decode(a.expenditure_type_id, 2, 'Allocated Removal/Salvage', 'Allocated '||a.book_summary_name), 1, 35), 'alloc_ratio', 0 quantity, allocation_priority, 
	a.unitization_summary, a.book_summary_name, b.unit_item_id,
	
	round(a.amount * b.alloc_ratio, 2) - 
		decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
		sum(round(a.amount * b.alloc_ratio,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
	
from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
where a.work_order_id = b.work_order_id  
and c.work_order_id = b.work_order_id
and c.company_id = :i_company_id
and c.error_msg is null
and a.expenditure_type_id = b.expenditure_type_id
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and a.alloc_id = b.alloc_id 
and a.allocation_priority = b.alloc_priority
and a.allocation_priority = :a_priority_number 
and a.unit_item_id is null
and a.batch_unit_item_id is null 
and not exists (select 1
					from UNITIZE_ALLOC_CGC_INSERTS_TEMP c
					where c.work_order_id = a.work_order_id
					and a.allocation_priority = c.allocation_priority 
					and a.alloc_id = c.alloc_id 
					and c.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
					and c.company_id = :i_company_id
					)
					; 
	
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP remaining: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//Estimates based allocation must have unit items that match wo_estimates data for allocations to work.
//A common problem is when a work order has utility account and retirement unit on Actuals, and by default, the unitization logic for unit item
//   creation uses the Actuals to create unit items, but work order's Allocation Method is configured to use Estimates for allocation.  
//   If those unit items created from Actuals do not match the wo_estimates for the work order, allocation cannot occur, and so it is an allocation error. 	
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id, 
substrb(decode(a.expenditure_type_id, 1, 
'Unitization 134: Cannot allocate Additions for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'." The estimates do not match the unit items. *Create matching estimates for UI created from Actuals or Change "UI From Est" to "Yes" and Reset Unitization*',
'Unitization 134R: Cannot allocate RWIP for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'." The estimates do not match the unit items. *Create matching estimates for UI created from Actuals or Change "UI From Est" to "Yes" and Reset Unitization*'
), 1, 2000)
from CHARGE_GROUP_CONTROL a, UNITIZE_WO_LIST_TEMP b, CHARGE_TYPE c, UNIT_ALLOCATION d
where a.work_order_id = b.work_order_id
and b.company_id = :i_company_id
and a.allocation_priority = :a_priority_number
and a.charge_type_id = c.charge_type_id
and a.alloc_id = d.alloc_id
and a.expenditure_type_id in (1,2) 
and c.processing_type_id <> 1  /*non-OCR*/
and a.amount <> 0
and a.batch_unit_item_id is null
and a.unit_item_id is null 
and b.error_msg is null
and lower(d.alloc_type) = 'estimates'
and NOT EXISTS (select 1 from UNITIZE_ALLOC_CGC_INSERTS_TEMP e
					 where b.work_order_id = e.work_order_id  
					 and a.expenditure_type_id = e.expenditure_type_id
				 	 and a.allocation_priority = e.allocation_priority 
				 	 and a.alloc_id = e.alloc_id 
					 and e.orig_charge_group_id = a.charge_group_id  /*already allocated charges*/ 
					 and e.company_id = :i_company_id 
					 );

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (134): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if

return 1
end function

public function integer of_unit_item_insert_prepare ();longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": preparing unit item data..."
of_log() 

//Get Max(unit_item_id) from each work order
update UNITIZE_WO_LIST_TEMP a
set a.max_unit_item_id = (select max(b.unit_item_id)
										from UNITIZED_WORK_ORDER b
										where a.work_order_id = b.work_order_id
										and b.unit_item_id >= 0
										group by b.work_order_id
								)
where a.company_id = :i_company_id
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update max_unit_item_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//update for nulls
update UNITIZE_WO_LIST_TEMP a
set a.max_unit_item_id = 0
where a.company_id = :i_company_id
and a.error_msg is null
and a.max_unit_item_id is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update max_unit_item_id(null): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

return 1
end function

public function integer of_allocation_results_missing (longlong a_priority_number);
longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   checking for allocation errors..."
of_log() 


//Identify all other allocation errors that could occur where charge_groups are unable to get assigned to a UNIT_ITEM_ID by assigning the generic "no basis for allocation" message.
//This is the "catch all" error validation.  More specific validation error catching is done inside "of_allocate_charges_by_acct" and "of_allocate_charges_remaining" functions.
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id, 
substrb(decode(a.expenditure_type_id, 1, 
					'Unitization 136: No basis to perform Adds allocation for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'"',
					'Unitization 136R: No basis to perform RWIP allocation for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'"' 
		), 1, 2000)
from CHARGE_GROUP_CONTROL a, UNITIZE_WO_LIST_TEMP b, CHARGE_TYPE c, UNIT_ALLOCATION d
where a.work_order_id = b.work_order_id
and a.charge_type_id = c.charge_type_id
and a.alloc_id = d.alloc_id
and b.company_id = :i_company_id
and a.allocation_priority = :a_priority_number
and a.expenditure_type_id in (1,2)
and a.batch_unit_item_id is null
and a.unit_item_id is null
and a.alloc_id is not null 
and b.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (136): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if
end if  

return 1
end function

public function integer of_allocation_statistic_estimates (longlong a_priority_number);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//setup the master table of allocation statistic for estimates based allocations for this priority
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, check_count

//Maint 37803: only insert for alloc_ids associated with charge groups needing to unitize in this run.

//insert into the main allocation statistics table,UNITIZE_RATIO_UNITS_TEMP, for the standards for this priority.
insert into UNITIZE_RATIO_UNITS_TEMP 
	(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
	UNITIZE_BY_ACCOUNT, ALLOC_ID, ALLOC_TYPE, ALLOC_BASIS, ALLOC_PRIORITY, ALLOC_STATISTIC)	
select company_id, work_order_id, unit_item_id, expenditure_type_id, utility_account_id, bus_segment_id, sub_account_id,
unitize_by_account, alloc_id, alloc_type, alloc_basis, :a_priority_number, alloc_statistic 
from UNITIZE_ALLOC_EST_TEMP a
where a.company_id = :i_company_id
and exists (select 1 from UNITIZE_ALLOC_LIST_TEMP b 
				where b.company_id = :i_company_id 
				and a.alloc_id = b.alloc_id
				and b.alloc_type = 'estimates'
				and b.alloc_priority = :a_priority_number)
and exists (select 1 from CHARGE_GROUP_CONTROL c
			where a.work_order_id = c.work_order_id
			and a.expenditure_type_id = c.expenditure_type_id
			and a.alloc_id = c.alloc_id
			and c.unit_item_id is null
			and c.batch_unit_item_id is null
			and c.allocation_priority = :a_priority_number)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_RATIO_UNITS_TEMP (EST by Priority): " + sqlca.SQLErrText
	of_log() 
	rollback;	 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

return 1
end function

public function integer of_allocation_statistic_standards (longlong a_priority_number);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//standards based allocation statistics do not vary by priorities, but multiple configurations of the Alloc Basis = standards could be different within the same priority number.
//setting up the allocation statistics for standards needs to be done for each priority in the unitization processing. 
//
////there are only 4 basis options for standards:  
//1.  material_cost for both adds and rwip
//2.  labor_cost for adds
//3.  labor_hours for adds
//4.  cor_cost for rwip
//5.  cor_hours for rwip
//
//logic for standards automatically switches the labor and cor statistics for Adds vs RWIP.
//Additions always use "standard_material_cost", "labor_cost" or "labor_hours."
//RWIP always use  "standard_material_cost", "cor_cost" or "cor_hours."
//"material_cost" is the only basis that can be used for both Adds and RWIP.
//i_std_alloc_precision_arrary[] used in this function is pre-populated in the of_allocation_std_info() function.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, i, p, num_std_basis, num_precision
string ds_standards_basis_list_sqls, current_alloc_basis, string_sqls, company_sql_filter, utility_account_sql_filter, current_precision, rtn_string

//get standard allocation basis options for the priority in this function
uo_ds_top ds_standards_basis_list  
ds_standards_basis_list = create uo_ds_top

ds_standards_basis_list_sqls = "select distinct alloc_basis from UNITIZE_ALLOC_LIST_TEMP " + & 
" where company_id = " + string(i_company_id) + & 
" and alloc_priority = " + string(a_priority_number) + & 
" and lower(alloc_type) = 'standards'" + & 
" and lower(alloc_basis) in ('material_cost',  'labor_cost', 'labor_hours', 'cor_cost',  'cor_hours') "

rtn_string =f_create_dynamic_ds(ds_standards_basis_list, 'grid', ds_standards_basis_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_standards_basis_list for std allocation: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_std_basis = ds_standards_basis_list.RowCount()
num_precision = upperbound(i_std_alloc_precision_arrary[])

//loop over each of the standard basis options
for i = 1 to num_std_basis

	current_alloc_basis = ds_standards_basis_list.getitemstring(i, 1)  
	
	//loop for each of the precision steps
	for p = 1 to num_precision 
		
		////right now only 1 for the "ru" option will exist. 
		////later if new precision code is designed, this code will need additional code to handle those precise scenarios.
		//current_precision = i_std_alloc_precision_arrary[p]
	
		//Additions always use "standard_material_cost", "labor_cost" or "labor_hours."
		insert into UNITIZE_ALLOC_STND_TEMP 
		(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
		ALLOC_ID, ALLOC_PRIORITY, ALLOC_TYPE, ALLOC_BASIS, ALLOC_STATISTIC, UNIT_QUANTITY)
		SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
			b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
			e.ALLOC_ID, :a_priority_number, TRIM(LOWER(e.alloc_type)), TRIM(LOWER(e.alloc_basis)), 
			
			sum(
				case 
					when TRIM(LOWER(e.alloc_basis)) = 'labor_cost' then nvl(a.standard_labor_cost,0) 
					when TRIM(LOWER(e.alloc_basis)) = 'labor_hours' then nvl(a.standard_labor_hours,0) 
					when TRIM(LOWER(e.alloc_basis)) = 'cor_cost' then nvl(a.standard_labor_cost,0) 
					when TRIM(LOWER(e.alloc_basis)) = 'cor_hours' then nvl(a.standard_labor_hours,0) 
				else nvl(a.standard_material_cost,0)
				end 
			) ALLOC_STATISTIC,
		
		b.QUANTITY
		
		 FROM RETIRE_UNIT_STD a,   
				UNITIZE_ELIG_UNITS_TEMP b, 
				UNIT_ALLOCATION e,
				UNITIZE_WO_LIST_TEMP x
				
		WHERE b.WORK_ORDER_ID = x.WORK_ORDER_ID    
			AND a.RETIREMENT_UNIT_ID = b.RETIREMENT_UNIT_ID
			AND b.EXPENDITURE_TYPE_ID = 1
			AND b.EXCLUDE_FROM_ALLOCATIONS = 0 
			AND x.COMPANY_ID = :i_company_id      
			AND x.ERROR_MSG is null   
			AND TRIM(LOWER(e.alloc_type)) = 'standards'
			AND TRIM(LOWER(e.alloc_basis)) = :current_alloc_basis	
			AND e.ALLOC_ID in (select f.alloc_id 
									from UNITIZE_ALLOC_LIST_TEMP f
									where f.company_id = :i_company_id 
									and f.alloc_priority = :a_priority_number
									and f.alloc_type = 'standards' and alloc_basis = :current_alloc_basis
									) 			 		
			AND a.effective_date = (select max(max.effective_date) max_date 
											from retire_unit_std max
											where b.RETIREMENT_UNIT_ID = max.retirement_unit_id  
											and max.effective_date <= to_date(:i_month_number, 'yyyymm') 
											group by max.retirement_unit_id
											)	
			AND exists (select 1 from CHARGE_GROUP_CONTROL c
							where x.work_order_id = c.work_order_id
							and c.alloc_id = e.alloc_id
							and c.BATCH_UNIT_ITEM_ID IS NULL
							and c.UNIT_ITEM_ID IS NULL
							and c.allocation_priority = :a_priority_number
							and c.EXPENDITURE_TYPE_ID = b.expenditure_type_id
							) 
			AND not exists (select 1 from UNITIZE_ALLOC_STND_TEMP d
							where b.work_order_id = d.work_order_id
							and b.unit_item_id = d.unit_item_id
							and e.ALLOC_ID = d.alloc_id
							and b.expenditure_type_id = d.expenditure_type_id
							and d.alloc_priority =  :a_priority_number
							)
		GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
			b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT,  b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
			e.ALLOC_ID, TRIM(LOWER(e.alloc_type)), TRIM(LOWER(e.alloc_basis)), 
			b.QUANTITY;
			
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_STND_TEMP(" + current_alloc_basis + " adds): " + sqlca.SQLErrText
			of_log() 
			rollback;	
			destroy ds_standards_basis_list
			return -1
		end if		
		num_nrows = sqlca.sqlnrows   
		
		//RWIP always use  "standard_material_cost", "cor_cost" or "cor_hours."
		insert into UNITIZE_ALLOC_STND_TEMP 
		(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
		ALLOC_ID, ALLOC_PRIORITY, ALLOC_TYPE, ALLOC_BASIS, ALLOC_STATISTIC, UNIT_QUANTITY)
		SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
			b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
			e.ALLOC_ID, :a_priority_number, TRIM(LOWER(e.alloc_type)), TRIM(LOWER(e.alloc_basis)), 
			
			sum( 
				case 
					when TRIM(LOWER(e.alloc_basis)) = 'labor_cost' then nvl(a.standard_cor_cost, 0) 
					when TRIM(LOWER(e.alloc_basis)) = 'labor_hours' then nvl(a.standard_cor_hours, 0) 
					when TRIM(LOWER(e.alloc_basis)) = 'cor_cost' then nvl(a.standard_cor_cost, 0) 
					when TRIM(LOWER(e.alloc_basis)) = 'cor_hours' then nvl(a.standard_cor_hours, 0) 
				else nvl(a.standard_material_cost, 0) 
				end 
			) ALLOC_STATISTIC,
		
		b.QUANTITY
		
		 FROM RETIRE_UNIT_STD a,   
				UNITIZE_ELIG_UNITS_TEMP b, 
				UNIT_ALLOCATION e,
				UNITIZE_WO_LIST_TEMP x
				
		WHERE b.WORK_ORDER_ID = x.WORK_ORDER_ID    
			AND a.RETIREMENT_UNIT_ID = b.RETIREMENT_UNIT_ID
			AND b.EXPENDITURE_TYPE_ID = 2
			AND b.EXCLUDE_FROM_ALLOCATIONS = 0
			AND x.COMPANY_ID = :i_company_id      
			AND x.ERROR_MSG is null   
			AND TRIM(LOWER(e.alloc_type)) = 'standards'
			AND TRIM(LOWER(e.alloc_basis)) = :current_alloc_basis	
			AND e.ALLOC_ID in (select alloc_id 
									from UNITIZE_ALLOC_LIST_TEMP f
									where f.company_id = :i_company_id 
									and f.alloc_priority = :a_priority_number
									and f.alloc_type = 'standards' and alloc_basis = :current_alloc_basis
									) 		
			AND a.effective_date = (select max(max.effective_date) max_date 
											from retire_unit_std max
											where b.RETIREMENT_UNIT_ID = max.retirement_unit_id  
											and max.effective_date <= to_date(:i_month_number, 'yyyymm') 
											group by max.retirement_unit_id
											)	
			AND exists (select 1 from CHARGE_GROUP_CONTROL c
							where x.work_order_id = c.work_order_id
							and c.alloc_id = e.alloc_id
							and c.BATCH_UNIT_ITEM_ID IS NULL
							and c.UNIT_ITEM_ID IS NULL
							and c.allocation_priority = :a_priority_number
							and c.EXPENDITURE_TYPE_ID = b.expenditure_type_id  
							and c.CHARGE_TYPE_ID <> :i_ocr_charge_type_id  /*non-OCR*/ 
							) 
			AND not exists (select 1 from UNITIZE_ALLOC_STND_TEMP d
							where b.work_order_id = d.work_order_id
							and b.unit_item_id = d.unit_item_id
							and e.ALLOC_ID = d.alloc_id
							and b.expenditure_type_id = d.expenditure_type_id
							and d.alloc_priority =  :a_priority_number
							)
		GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
			b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
			e.ALLOC_ID, TRIM(LOWER(e.alloc_type)), TRIM(LOWER(e.alloc_basis)), 
			b.QUANTITY;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_STND_TEMP(" + current_alloc_basis + " retires): " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			destroy ds_standards_basis_list
			return -1
		end if		
		num_nrows = sqlca.sqlnrows   
	
		
	next
	
next 

destroy ds_standards_basis_list


//insert into the main allocation statistics table,UNITIZE_RATIO_UNITS_TEMP, for the standards for this priority.
insert into UNITIZE_RATIO_UNITS_TEMP 
	(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
	UNITIZE_BY_ACCOUNT, ALLOC_ID, ALLOC_TYPE, ALLOC_BASIS, ALLOC_PRIORITY, ALLOC_STATISTIC)	
select  a.company_id, a.work_order_id, a.unit_item_id, a.expenditure_type_id, a.utility_account_id, a.bus_segment_id, a.sub_account_id,
	a.unitize_by_account, a.alloc_id, a.alloc_type, a.alloc_basis, a.alloc_priority, a.alloc_statistic*a.unit_quantity  /*need to multiply to quantity for stnds*/
	from UNITIZE_ALLOC_STND_TEMP a, UNITIZE_WO_LIST_TEMP x
	where a.work_order_id = x.work_order_id
	and a.company_id = x.company_id
	and x.error_msg is null
	and a.company_id = :i_company_id
	and a.alloc_priority = :a_priority_number;
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_RATIO_UNITS_TEMP (stnds): " + sqlca.SQLErrText
	of_log() 
	rollback;	 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

return 1
end function

public function integer of_allocation_std_info ();longlong check_count_std, i, num_std_co_not_null, num_std_ut_not_null, num_total
string precision_array[] 


check_count_std = 0
select count(1) into :check_count_std
from UNITIZE_ALLOC_LIST_TEMP where lower(alloc_type) = 'standards';

if check_count_std <= 0 then
	//nothing to do
	return 1
end if

i_std_alloc_precision_arrary[1] = 'ru'

//
////determine if precison fields are used
//num_std_ut_not_null = 0
//num_std_co_not_null = 0
//num_total = 0
//select sum(decode(utility_account_id, null, 0, 1)), sum(decode(company_id, null, 0, 1)), count(1)
//into :num_std_ut_not_null, :num_std_co_not_null, :num_total
//from RETIRE_UNIT_STD;
//
//i = 1
//
////first check for the most precision (ut and co)
//if num_std_ut_not_null > 0 and num_std_co_not_null > 0 then
//	precision_array[i] = 'ut_bs_co'
//	i = i + 1
//end if 
//
////next check for ut only
//if num_std_ut_not_null > 0 and num_std_co_not_null = 0 then
//	precision_array[i] = 'ut_bs'
//	i = i + 1
//end if 
//
////next check for co only
//if num_std_ut_not_null = 0 and num_std_co_not_null > 0 then
//	precision_array[i] = 'co'
//	i = i + 1
//end if  
//
////see if there is any need to check for standards that have no precision
//if num_total = num_std_ut_not_null or num_total = num_std_co_not_null then
//	//the data is always more precise than just retirement_unit_id
//else
//	precision_array[i] = 'ru'
//end if 
//
//i_std_alloc_precision_arrary = precision_array[]

return 1
end function

public function integer of_unitization_completion_check ();////////////////////////////////////////////////////////////////////////////////
//At this point, all charge groups should have UNIT_ITEM_ID assigned.
////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn, check_count
string error_msg

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": verifying unitization completion..."
of_log() 

if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then
	error_msg =  'Unitization 115: Incomplete unitization for this work order. *Reset Unitization and try again*'
else
	error_msg =  'Unitization 115: Incomplete unitization for this work order. *Reset Unitization and try again*'
end if 

//Detect incomplete unitization of eligible charges
update UNITIZE_WO_LIST_TEMP a
set a.rtn_code = -1, a.error_msg = :error_msg
where a.company_id = :i_company_id  
and exists (select 1 from CHARGE_GROUP_CONTROL b 
				where a.work_order_id = b.work_order_id 
				and b.unit_item_id is null
				and b.batch_unit_item_id is null
				and b.expenditure_type_id in (1,2) 
				)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP incomplete unitization: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//Handle errors identified above.
of_rtn = of_errors('WO')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

if i_single_wo then
	//check if any work order left in the process
	check_count = 0
	select count(1) into :check_count
	from UNITIZE_WO_LIST_TEMP where company_id = :i_company_id;
	
	if check_count > 0 then 
		f_wo_status_box("", "Charge Allocation for the Work Order is Complete.")
	end if
end if

return 1
end function

public function integer of_update_unitized_wo_amount ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//When an OCR Unit Item is  made, the amount and quantity is in sync for all of these tables:
//   1. UNITIZED_WORK_ORDER
//   2. CHARGE_GROUP_CONTROL
//   3. CWIP_CHARGE
//When an Addition Unit Item is made, the amount = 0, and the quantity equals the quantity of the charges or estimates.
//After Allocation, the AMOUNT column on UNITIZED_WORK_ORDER is updated to be the sum(AMOUNT) from CHARGE_GROUP_CONTROL for the matching UNIT_ITEM_IDs.
//The old unitization code would include the AMOUNT for the OCR charge group.  New code will not include the OCR charge group.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong num_nrows
string sqls

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": updating unitized amount..."
of_log() 

//maint-43648: Use merge statement here for better performance.

//update unitized_work_order a
//set a.amount = (select  nvl(sum(nvl(b.amount,0)),0) 
//					from charge_group_control b, UNITIZE_WO_LIST_TEMP d
//					where a.work_order_id = d.work_order_id
//					and d.company_id = :i_company_id
//					and d.error_msg is null
//					and a.work_order_id = b.work_order_id
//					and a.unit_item_id = b.unit_item_id
//					and b.unit_item_id > 0 
//					and b.expenditure_type_id in (1,2)
//					and b.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
//					)
//where a.company_id = :i_company_id
//and a.unit_item_id > 0
//and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS c
//					where a.work_order_id = c.work_order_id
//					and a.unit_item_id = c.unit_item_id
//					)
//and exists (select 1 from UNITIZE_WO_LIST_TEMP d
//				where a.work_order_id = d.work_order_id
//				and d.company_id = :i_company_id
//				and d.error_msg is null
//				);

sqls = ' merge into ( select * '
sqls += ' 					from unitized_work_order u '
sqls += ' 					where u.company_id = ' + string(i_company_id) + ' '
sqls += ' 					and u.unit_item_id > 0 '
sqls += ' 					and not exists ( select 1 '
sqls += ' 											from wo_unit_item_pend_trans c '
sqls += ' 											where u.work_order_id = c.work_order_id '
sqls += ' 											and u.unit_item_id = c.unit_item_id '
sqls += ' 									    ) '
sqls += ' 				) a '
sqls += ' using ( select b.work_order_id, b.unit_item_id, nvl(sum(nvl(b.amount,0)),0) amount '
sqls += ' 			from charge_group_control b, UNITIZE_WO_LIST_TEMP d '
sqls += ' 			where d.work_order_id = b.work_order_id '
sqls += ' 			and b.charge_type_id <> ' + string(i_ocr_charge_type_id) + ' '
sqls += ' 			and b.expenditure_type_id in (1,2) '
sqls += ' 			and b.unit_item_id > 0 and b.batch_unit_item_id is null '
sqls += ' 			and d.company_id = ' + string(i_company_id) + ' '
sqls += ' 			and d.error_msg is null '
sqls += ' 			group by b.work_order_id, b.unit_item_id '
sqls += ' 		 ) e '
sqls += ' on (	e.work_order_id = a.work_order_id '
sqls += ' 		and e.unit_item_id = a.unit_item_id '
sqls += '       ) '
sqls += ' when matched then '
sqls += ' update set '
sqls += ' a.amount = e.amount '

execute immediate :sqls;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unitized_work_order.amount: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    


return 1
end function

public function integer of_unit_item_rwip_asset_id ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Need to verify all RWIP unit items that have no asset_id map to a group depreciation depr group.
//It is an error to unitize RWIP to an individually depreciated depr group where a specific asset_id is not provided on the Unit Item.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong i, num_wo_units, gl_id2, maj_loc3, ut4, bs5, sa6, sl7, vintage8, al9, lt10, ru11, pu12, cc13, depr_group_id, depr_indicator
longlong wo_id, num_nrows, unit_item_id, of_rtn
string ds_wo_units_list_sqls, ccv14, msg, gl_year_string, rtn_string, unit_activity_code
dec {2} check_allocation_amt

if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then
	//this validation is not needed for Late Close
	return 1
end if 

if i_elig_rwip_wos = 0 then
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": evaluating RWIP units..."
of_log()     

gl_year_string = mid(string(i_month_number), 1, 4)

uo_ds_top ds_wo_units_list  
ds_wo_units_list = create uo_ds_top

//get list of RWIP unit items that have asset_id = null
ds_wo_units_list_sqls = "SELECT UNITIZE_ELIG_UNITS_TEMP.work_order_ID,  " + &
" UNITIZE_ELIG_UNITS_TEMP.gl_account_id,  " /*2*/ + &  
" MAJOR_LOCATION.major_location_id, " /*3*/ + &
" UNITIZE_ELIG_UNITS_TEMP.utility_account_id,  " /*4*/  + &
" UNITIZE_ELIG_UNITS_TEMP.bus_segment_id, " /*5*/  + &
" UNITIZE_ELIG_UNITS_TEMP.sub_account_id,  " /*6*/  + &
" 0, " /*7*/  + &
" DECODE(UNITIZE_WO_LIST_TEMP.CLOSING_OPTION_ID, 2, to_number(to_char(UNITIZE_WO_LIST_TEMP.IN_SERVICE_DATE, 'yyyy')), " + &
"                        4, to_number(to_char(UNITIZE_WO_LIST_TEMP.IN_SERVICE_DATE, 'yyyy')), " + gl_year_string + " ) vintage, "  /*8*/  + &
" UNITIZE_ELIG_UNITS_TEMP.asset_location_id, "  /*9*/ + &
" MAJOR_LOCATION.location_type_id,  "  /*10*/ + &
" RETIREMENT_UNIT.retirement_unit_id,  " /*11*/  + &
" RETIREMENT_UNIT.property_unit_id, " /*12*/  + &
" UNITIZE_ELIG_UNITS_TEMP.unit_item_id,  " + &
" UPPER(TRIM(UNITIZE_ELIG_UNITS_TEMP.ACTIVITY_CODE))  " + &
" FROM UNITIZE_ELIG_UNITS_TEMP, UNITIZE_WO_LIST_TEMP, ASSET_LOCATION, MAJOR_LOCATION, RETIREMENT_UNIT " + &
" WHERE UNITIZE_ELIG_UNITS_TEMP.WORK_ORDER_ID = UNITIZE_WO_LIST_TEMP.WORK_ORDER_ID   " + &
" AND UNITIZE_ELIG_UNITS_TEMP.ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID    " + &
" AND ASSET_LOCATION.MAJOR_LOCATION_ID = MAJOR_LOCATION.MAJOR_LOCATION_ID " + &
" AND UNITIZE_ELIG_UNITS_TEMP.RETIREMENT_UNIT_ID = RETIREMENT_UNIT.RETIREMENT_UNIT_ID " + &
" AND UNITIZE_WO_LIST_TEMP.company_id = " + string(i_company_id) + &
" AND UNITIZE_WO_LIST_TEMP.error_msg is null  " + &
" AND UNITIZE_ELIG_UNITS_TEMP.expenditure_type_id = 2 " + &
" AND TRIM(UPPER(UNITIZE_ELIG_UNITS_TEMP.ACTIVITY_CODE)) IN ('URET', 'MRET', 'URGL', 'SAGL', 'SALE') 	 " + & 
" AND UNITIZE_ELIG_UNITS_TEMP.ldg_asset_id is null " + &  
" AND (UNITIZE_ELIG_UNITS_TEMP.utility_account_id, UNITIZE_ELIG_UNITS_TEMP.bus_segment_id) in  " + &
" 						( " + &
" 						SELECT b.utility_account_id, b.bus_segment_id  " + &
" 						FROM depr_group a, depr_group_control b " + &
" 						where a.depr_group_id = b.depr_group_id " + &
" 						and a.company_id = " + string(i_company_id) + &
" 						and a.subledger_type_id in (select subledger_type_id from subledger_control where depreciation_indicator = 3) " + &
" 						) "
							

rtn_string = f_create_dynamic_ds(ds_wo_units_list, 'grid', ds_wo_units_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_wo_units_list for rwip asset_id check: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_wo_units = ds_wo_units_list.RowCount()

//currently there is no way to add class codes to RWIP unit items

//subledger type is always 0 and vintage is unknown

//loop over each of the standard basis options
for i = 1 to num_wo_units

	gl_id2 = ds_wo_units_list.getitemnumber(i, 2)  
	maj_loc3 = ds_wo_units_list.getitemnumber(i, 3)  
	ut4 = ds_wo_units_list.getitemnumber(i, 4)  
	bs5 = ds_wo_units_list.getitemnumber(i, 5)  
	sa6 = ds_wo_units_list.getitemnumber(i, 6)    
	sl7 = 0
	vintage8 = ds_wo_units_list.getitemnumber(i, 8)    
	al9 = ds_wo_units_list.getitemnumber(i, 9)  
	lt10 = ds_wo_units_list.getitemnumber(i, 10)  
	ru11 = ds_wo_units_list.getitemnumber(i, 11)  
	pu12 = ds_wo_units_list.getitemnumber(i, 12)  
	cc13 = 0
	ccv14 = ''
	
	//maint 44954: check allocated amount prior to calling f_find_depr_group
	wo_id = ds_wo_units_list.getitemnumber(i, 1)  
	unit_item_id = ds_wo_units_list.getitemnumber(i, 13) 
	unit_activity_code = ds_wo_units_list.getitemstring(i, 14) 
	
	//Maint 38474:  see if this unit item has any dollars allocated for RWIP
	check_allocation_amt = 0
	select sum(amount) into :check_allocation_amt
	from CHARGE_GROUP_CONTROL
	where work_order_id = :wo_id
	and unit_item_id = :unit_item_id 
	and batch_unit_item_id is null;
	
	if isnull(check_allocation_amt) then check_allocation_amt = 0
	
	if check_allocation_amt = 0 then
		if unit_activity_code = 'URET' or unit_activity_code = 'MRET' then
			//nothing to check if allocation amount = 0 and quantity is not involved for URET/MRET that is RWIP unitization only, go to the next Retirement Unit Item
			continue
		end if 
	end if
	
	depr_group_id = 0
	SELECT pp_depr_pkg.f_find_depr_group
		 (:i_company_id, :gl_id2, :maj_loc3, :ut4, :bs5, :sa6, :sl7, :vintage8, :al9,  :lt10,  :ru11, :pu12,  :cc13, :ccv14)
	into :depr_group_id		  
	FROM dual;  
	
	//reset msg
	msg = ''
		
	//oracle function return negative for invalid or missing depr group
	if depr_group_id <= 0 then
		msg = "Unitization 185: Cannot create pending transactions for COR/Salvage ... " + &
				  "Cannot find a valid depr group mapping for this " + &
				  "Bus. Segment/Util. Account/Sub Account/GL Account " + &
				  "Company/Location combination. " + &
				  "company_id = " + string(i_company_id) + &
				  ", gl_account_id = " + string(gl_id2) + &
				  ", utility_account_id = " + string(ut4) + &
				  ", sub_account_id = " + string(sa6) + &
				  ", bus_segment_id = " + string(bs5) + &
				  ", vintage = " + string(vintage8) + &
				  ", asset_location_id = " + string(al9) + &
				  ", retirement_unit_id = " + string(ru11) + &
				  ", property_unit_id = " + string(pu12)+ &
				  ", unit_item_id = " + string(unit_item_id) + " *Add the missing Depr Group Control or change the account attributes for Unitization. Reset Unitization and Try Again.*"
		msg = mid(msg, 1, 2000)
	end if 
	
	//verify if depr_group_id found is sublegder_type_id = 0
	depr_indicator = 0 
	select depreciation_indicator into :depr_indicator
	from subledger_control, depr_group
	where depr_group.subledger_type_id = subledger_control.subledger_type_id
	and depr_group_id = :depr_group_id;
	
	if depr_indicator = 1 or depr_indicator = 3 then 
		msg = "Unitization 185d: Cannot create pending transactions for COR/Salvage " + &
				  "for an individually depreciated depr group with no ASSET_ID. The ASSET_ID is required for accounts that are setup for individual asset depreciation. " + &
				  "Bus. Segment/Util. Account/Sub Account/GL Account " + &
				  "Company/Location combination. " + &
					"company_id = " + string(i_company_id) + &
				  ", gl_account_id = " + string(gl_id2) + &
				  ", utility_account_id = " + string(ut4) + &
				  ", sub_account_id = " + string(sa6) + &
				  ", bus_segment_id = " + string(bs5) + &
				  ", vintage = " + string(vintage8) + &
				  ", asset_location_id = " + string(al9) + &
				  ", retirement_unit_id = " + string(ru11) + &
				  ", property_unit_id = " + string(pu12) + &
				  ", unit_item_id = " + string(unit_item_id) + " *Add the missing Asset ID or change the account attributes for Unitization. Reset Unitization and Try Again.*"
		msg = mid(msg, 1, 2000)
	end if 
				
	if isnull(msg) or msg = '' then 
		//ok
	else
		
		update UNITIZE_WO_STG_UNITS_TEMP 
		set error_msg = :msg
		where company_id = :i_company_id and work_order_id = :wo_id and new_unit_item_id = :unit_item_id;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP update rwip asset_id: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			destroy ds_wo_units_list
			return -1
		end if		
		num_nrows = sqlca.sqlnrows     
		
		if num_nrows = 0 then   
			//the unit item was not staged in this run, it is an prior run made unit item.  need to insert to record error.
			insert into UNITIZE_WO_STG_UNITS_TEMP (company_id, work_order_id, new_unit_item_id, error_msg)
			values (:i_company_id, :wo_id, :unit_item_id, :msg);
			
			if sqlca.SQLCode < 0 then 
				i_msg[] = i_null_arrary[] 
				i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert rwip asset_id: " + sqlca.SQLErrText
				of_log() 
				rollback;		 
				destroy ds_wo_units_list
				return -1
			end if		
			num_nrows = sqlca.sqlnrows     
		end if 

	end if 
next

destroy ds_wo_units_list

//Handle errors identified in the default functions above.  
of_rtn = of_errors('UNITS')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

return 1
end function

public function integer of_insert_into_pend_transaction ();longlong num_nrows, num_buckets, k, num_nrows_pend, check_count
string sqls

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": inserting pend trans data..."
of_log() 

//insert from global temp table.  use hardcoded 1 for posting_status.
insert into PEND_TRANSACTION
(PEND_TRANS_ID, COMPANY_ID, GL_POSTING_MO_YR, WORK_ORDER_NUMBER,  LDG_ASSET_ID, LDG_ACTIVITY_ID, LDG_DEPR_GROUP_ID, IN_SERVICE_YEAR,
GL_ACCOUNT_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID, FUNC_CLASS_ID, RETIREMENT_UNIT_ID, PROPERTY_GROUP_ID, ASSET_LOCATION_ID, 
GL_JE_CODE, SERIAL_NUMBER, FERC_ACTIVITY_CODE, SUBLEDGER_INDICATOR, DESCRIPTION, LONG_DESCRIPTION, 
RETIRE_METHOD_ID, BOOKS_SCHEMA_ID, POSTING_STATUS, USER_ID1, 
ACTIVITY_CODE, POSTING_QUANTITY, POSTING_AMOUNT, 
REPLACEMENT_AMOUNT, RESERVE_CREDITS, SALVAGE_RETURNS, SALVAGE_CASH, COST_OF_REMOVAL)
select
PEND_TRANS_ID, COMPANY_ID, GL_POSTING_MO_YR, WORK_ORDER_NUMBER,  LDG_ASSET_ID, LDG_ACTIVITY_ID, LDG_DEPR_GROUP_ID, IN_SERVICE_YEAR,
GL_ACCOUNT_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID, FUNC_CLASS_ID, RETIREMENT_UNIT_ID, PROPERTY_GROUP_ID, ASSET_LOCATION_ID, 
GL_JE_CODE, SERIAL_NUMBER, FERC_ACTIVITY_CODE, SUBLEDGER_INDICATOR, DESCRIPTION, LONG_DESCRIPTION, 
RETIRE_METHOD_ID, BOOKS_SCHEMA_ID, 1, USER_ID1, 
ACTIVITY_CODE, POSTING_QUANTITY, POSTING_AMOUNT, 
REPLACEMENT_AMOUNT, RESERVE_CREDITS, SALVAGE_RETURNS, SALVAGE_CASH, COST_OF_REMOVAL
from UNITIZE_PEND_TRANS_TEMP
where company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into PEND_TRANSACTION: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows_pend = sqlca.sqlnrows   

if i_single_wo then
	if num_nrows_pend > 0 then
		i_single_wo_pending_made = true
	else
		i_single_wo_pending_made= false
	end if 
end if 

insert into PEND_BASIS 
(PEND_TRANS_ID,
BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10,
BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20,
BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30,
BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40,
BASIS_41, BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60,
BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70)
select
PEND_TRANS_ID,
BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10,
BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20,
BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30,
BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40,
BASIS_41, BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60,
BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70
from UNITIZE_PEND_TRANS_TEMP
where company_id = :i_company_id
and ldg_activity_id is null;  /*non-ocr; unitization only records*/

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into PEND_BASIS: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows    

////insert pend basis for proportional retirements processed in unitization with asset_id = not null and accum_cost <> 0.
////only the a.activity_code in ('URGL', 'SAGL') will be in this situation.
insert into  pend_basis (pend_trans_id, 
basis_1, basis_2,basis_3, basis_4, basis_5, basis_6, basis_7, basis_8, basis_9, basis_10,
basis_11, basis_12, basis_13, basis_14,basis_15, basis_16, basis_17, basis_18, basis_19, basis_20,
basis_21, basis_22, basis_23, basis_24, basis_25, basis_26, basis_27, basis_28, basis_29, basis_30,
basis_31, basis_32, basis_33, basis_34,basis_35, basis_36, basis_37, basis_38, basis_39, basis_40,
basis_41, basis_42, basis_43, basis_44,basis_45, basis_46, basis_47, basis_48, basis_49, basis_50,
basis_51, basis_52, basis_53, basis_54,basis_55, basis_56, basis_57, basis_58, basis_59, basis_60,
basis_61, basis_62, basis_63, basis_64,basis_65, basis_66, basis_67, basis_68, basis_69, basis_70 ) 	
select pend_trans_id, 
round(c.basis_1*(a.posting_amount/b.accum_cost),2), round(c.basis_2*(a.posting_amount/b.accum_cost),2), round(c.basis_3*(a.posting_amount/b.accum_cost),2), round(c.basis_4*(a.posting_amount/b.accum_cost),2), round(c.basis_5*(a.posting_amount/b.accum_cost),2), round(c.basis_6*(a.posting_amount/b.accum_cost),2), round(c.basis_7*(a.posting_amount/b.accum_cost),2), round(c.basis_8*(a.posting_amount/b.accum_cost),2), round(c.basis_9*(a.posting_amount/b.accum_cost),2), round(c.basis_10*(a.posting_amount/b.accum_cost),2),
round(c.basis_11*(a.posting_amount/b.accum_cost),2), round(c.basis_12*(a.posting_amount/b.accum_cost),2), round(c.basis_13*(a.posting_amount/b.accum_cost),2), round(c.basis_14*(a.posting_amount/b.accum_cost),2), round(c.basis_15*(a.posting_amount/b.accum_cost),2), round(c.basis_16*(a.posting_amount/b.accum_cost),2), round(c.basis_17*(a.posting_amount/b.accum_cost),2), round(c.basis_18*(a.posting_amount/b.accum_cost),2), round(c.basis_19*(a.posting_amount/b.accum_cost),2), round(c.basis_20*(a.posting_amount/b.accum_cost),2),
round(c.basis_21*(a.posting_amount/b.accum_cost),2), round(c.basis_22*(a.posting_amount/b.accum_cost),2), round(c.basis_23*(a.posting_amount/b.accum_cost),2), round(c.basis_24*(a.posting_amount/b.accum_cost),2), round(c.basis_25*(a.posting_amount/b.accum_cost),2), round(c.basis_26*(a.posting_amount/b.accum_cost),2), round(c.basis_27*(a.posting_amount/b.accum_cost),2), round(c.basis_28*(a.posting_amount/b.accum_cost),2), round(c.basis_29*(a.posting_amount/b.accum_cost),2), round(c.basis_30*(a.posting_amount/b.accum_cost),2),
round(c.basis_31*(a.posting_amount/b.accum_cost),2), round(c.basis_32*(a.posting_amount/b.accum_cost),2), round(c.basis_33*(a.posting_amount/b.accum_cost),2), round(c.basis_34*(a.posting_amount/b.accum_cost),2), round(c.basis_35*(a.posting_amount/b.accum_cost),2), round(c.basis_36*(a.posting_amount/b.accum_cost),2), round(c.basis_37*(a.posting_amount/b.accum_cost),2), round(c.basis_38*(a.posting_amount/b.accum_cost),2), round(c.basis_39*(a.posting_amount/b.accum_cost),2), round(c.basis_40*(a.posting_amount/b.accum_cost),2),
round(c.basis_41*(a.posting_amount/b.accum_cost),2), round(c.basis_42*(a.posting_amount/b.accum_cost),2), round(c.basis_43*(a.posting_amount/b.accum_cost),2), round(c.basis_44*(a.posting_amount/b.accum_cost),2), round(c.basis_45*(a.posting_amount/b.accum_cost),2), round(c.basis_46*(a.posting_amount/b.accum_cost),2), round(c.basis_47*(a.posting_amount/b.accum_cost),2), round(c.basis_48*(a.posting_amount/b.accum_cost),2), round(c.basis_49*(a.posting_amount/b.accum_cost),2), round(c.basis_50*(a.posting_amount/b.accum_cost),2),
round(c.basis_51*(a.posting_amount/b.accum_cost),2), round(c.basis_52*(a.posting_amount/b.accum_cost),2), round(c.basis_53*(a.posting_amount/b.accum_cost),2), round(c.basis_54*(a.posting_amount/b.accum_cost),2), round(c.basis_55*(a.posting_amount/b.accum_cost),2), round(c.basis_56*(a.posting_amount/b.accum_cost),2), round(c.basis_57*(a.posting_amount/b.accum_cost),2), round(c.basis_58*(a.posting_amount/b.accum_cost),2), round(c.basis_59*(a.posting_amount/b.accum_cost),2), round(c.basis_60*(a.posting_amount/b.accum_cost),2),
round(c.basis_61*(a.posting_amount/b.accum_cost),2), round(c.basis_62*(a.posting_amount/b.accum_cost),2), round(c.basis_63*(a.posting_amount/b.accum_cost),2), round(c.basis_64*(a.posting_amount/b.accum_cost),2), round(c.basis_65*(a.posting_amount/b.accum_cost),2), round(c.basis_66*(a.posting_amount/b.accum_cost),2), round(c.basis_67*(a.posting_amount/b.accum_cost),2), round(c.basis_68*(a.posting_amount/b.accum_cost),2), round(c.basis_69*(a.posting_amount/b.accum_cost),2), round(c.basis_70*(a.posting_amount/b.accum_cost),2)
from UNITIZE_PEND_TRANS_TEMP a, cpr_ledger b, cpr_ldg_basis c
where a.ldg_asset_id = b.asset_id
and b.asset_id = c.asset_id
and a.ferc_activity_code = 2 
and a.ldg_activity_id is not null  /*CHARGE_GROUP_ID of OCR*/
and a.ldg_asset_id is not null
and a.company_id = :i_company_id 
and a.posting_amount <> 0
and b.accum_cost <> 0 
and not exists (select 1 from pend_basis d where a.pend_trans_id = d.pend_trans_id);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into PEND_BASIS, asset accum_cost <> 0: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//insert pend basis for FULL retirements processed in unitization with asset_id with asset_id = not null and (accum_cost = 0  or posting_amount = 0)
//when an OCR retirement has posting_amount = 0, need to do full retirement because an asset could be activity with accum_cost = 0.
////only the a.activity_code in ('URGL', 'SAGL') will be in this situation.
insert into  pend_basis (pend_trans_id, 
basis_1, basis_2,basis_3, basis_4, basis_5, basis_6, basis_7, basis_8, basis_9, basis_10,
basis_11, basis_12, basis_13, basis_14,basis_15, basis_16, basis_17, basis_18, basis_19, basis_20,
basis_21, basis_22, basis_23, basis_24, basis_25, basis_26, basis_27, basis_28, basis_29, basis_30,
basis_31, basis_32, basis_33, basis_34,basis_35, basis_36, basis_37, basis_38, basis_39, basis_40,
basis_41, basis_42, basis_43, basis_44,basis_45, basis_46, basis_47, basis_48, basis_49, basis_50,
basis_51, basis_52, basis_53, basis_54,basis_55, basis_56, basis_57, basis_58, basis_59, basis_60,
basis_61, basis_62, basis_63, basis_64,basis_65, basis_66, basis_67, basis_68, basis_69, basis_70 ) 	
select pend_trans_id, 
	c.basis_1*-1, c.basis_2*-1,c.basis_3*-1, c.basis_4*-1, c.basis_5*-1, c.basis_6*-1, c.basis_7*-1, c.basis_8*-1, c.basis_9*-1, c.basis_10*-1,
	c.basis_11*-1, c.basis_12*-1, c.basis_13*-1, c.basis_14*-1,c.basis_15*-1, c.basis_16*-1, c.basis_17*-1, c.basis_18*-1, c.basis_19*-1, c.basis_20*-1,
	c.basis_21*-1, c.basis_22*-1, c.basis_23*-1, c.basis_24*-1, c.basis_25*-1, c.basis_26*-1, c.basis_27*-1, c.basis_28*-1, c.basis_29*-1, c.basis_30*-1,
	c.basis_31*-1, c.basis_32*-1, c.basis_33*-1, c.basis_34*-1,c.basis_35*-1, c.basis_36*-1, c.basis_37*-1, c.basis_38*-1, c.basis_39*-1, c.basis_40*-1,
	c.basis_41*-1, c.basis_42*-1, c.basis_43*-1, c.basis_44*-1,c.basis_45*-1, c.basis_46*-1, c.basis_47*-1, c.basis_48*-1, c.basis_49*-1, c.basis_50*-1,
	c.basis_51*-1, c.basis_52*-1, c.basis_53*-1, c.basis_54*-1,c.basis_55*-1, c.basis_56*-1, c.basis_57*-1, c.basis_58*-1, c.basis_59*-1, c.basis_60*-1,
	c.basis_61*-1, c.basis_62*-1, c.basis_63*-1, c.basis_64*-1,c.basis_65*-1, c.basis_66*-1, c.basis_67*-1, c.basis_68*-1, c.basis_69*-1, c.basis_70*-1 
from UNITIZE_PEND_TRANS_TEMP a, cpr_ledger b, cpr_ldg_basis c
where a.ldg_asset_id = b.asset_id
and b.asset_id = c.asset_id
and a.ferc_activity_code = 2  
and a.ldg_activity_id is not null   /*CHARGE_GROUP_ID of OCR*/
and a.ldg_asset_id is not null
and a.company_id = :i_company_id  
and (b.accum_cost = 0 or a.posting_amount = 0)
and not exists (select 1 from pend_basis d where a.pend_trans_id = d.pend_trans_id);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into PEND_BASIS, asset accum_cost = 0: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//plug rounding differences into basis_1 for set_of_books_id = 1.  the other set of books don't need a round plug, because the posting_amount is derived after basis amounts are determined.  
select max(book_summary_id) into :num_buckets from book_summary;

sqls = ''
sqls = 'update pend_basis c set c.basis_1 = nvl(c.basis_1,0) - (select sum( '
for k = 1 to num_buckets
	if k = 1 then
		sqls+= 'nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	else
		sqls+= ' + nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	end if
next

sqls += ') - sum(d.posting_amount) '
sqls += ' from pend_basis a, set_of_books b, pend_transaction d '
sqls += ' where c.pend_trans_id =  a.pend_trans_id '
sqls += ' and a.pend_trans_id = d.pend_trans_id '
sqls += ' and d.ldg_asset_id is not null '
sqls += ' and d.ldg_activity_id is not null '
sqls += ' and d.company_id = ' + string(i_company_id)
sqls += ' and d.posting_status = 1 '
sqls += ' and d.ferc_activity_code = 2 '
sqls += ' and d.ldg_depr_group_id < 0 '
sqls += ' and b.set_of_books_id = 1 '
sqls += ' ) ' 
sqls += ' where exists (select 1 from pend_transaction dd where dd.pend_trans_id = c.pend_trans_id ' 
sqls += ' and dd.ldg_asset_id is not null '
sqls += ' and dd.ldg_activity_id is not null '
sqls += ' and dd.ldg_depr_group_id < 0 '
sqls += ' and dd.ferc_activity_code = 2 '
sqls += ' and dd.company_id = ' + string(i_company_id)
sqls += ' and posting_status = 1 )'
	
execute immediate :sqls;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating basis_1 for ocr rounding plug: ' + sqlca.sqlerrtext
	i_msg[2] =  ' SQL: ' + sqls 
	of_log() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows


insert into CLASS_CODE_PENDING_TRANS
(PEND_TRANS_ID, CLASS_CODE_ID, VALUE)
select b.pend_trans_id, a.class_code_id, a.value
from UNIT_ITEM_CLASS_CODE a, UNITIZE_PEND_TRANS_TEMP b
where a.work_order_id = b.work_order_id
and a.unit_item_id = b.unit_item_id
and b.company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into CLASS_CODE_PENDING_TRANS: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//insert into pend_related_asset for related asset data if the ldg_asset_id is negative.  use abs().
//Note:  related asset logic uses negative asset_id, which will not work for clients who have real negative asset_ids in the CPR_LEDGER (created by quick conversion effort).

insert into pend_related_asset (pend_related_asset_id, pend_trans_id, related_asset_id, related_asset_type_id, co_retire)
select  pwrplant1.nextval, b.pend_trans_id, abs(b.ldg_asset_id), a.related_type_id, nvl(a.co_retire,0)
from related_asset_type a, UNITIZE_PEND_TRANS_TEMP b
where b.company_id = :i_company_id
and a.related_type_id = 3
and b. ldg_asset_id < 0; 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into pend_related_asset: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if num_nrows > 0 then		
	//reset the negative ldg_asset_id to a null value 
	update PEND_TRANSACTION a
	set a.ldg_asset_id = null, a.retire_method_id = null
	where a.ldg_asset_id < 0 
	and a.company_id = :i_company_id
	and a.ldg_depr_group_id < 0
	and exists (select 1 from UNITIZE_PEND_TRANS_TEMP b
					where a.pend_trans_id = b.pend_trans_id
					);

	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: update for negative ldg_asset_id: " + sqlca.SQLErrText
		of_log() 
		rollback;		
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  	
end if 

if i_equip_ledger_processed then
	insert into pend_equip (pend_trans_id, equip_id, quantity)
	select distinct b.pend_trans_id, a.equip_id, 0
	from cpr_equip_estimate_relate a, UNITIZE_PEND_TRANS_TEMP b
	where a.work_order_id = b.work_order_id
	and a.unit_item_id = b.unit_item_id;

	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into pend_equip: " + sqlca.SQLErrText
		of_log() 
		rollback;		
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  	
end if 

if num_nrows_pend > 0 and i_single_wo then
	//check if any work order left in the process
	check_count = 0
	select count(1) into :check_count
	from UNITIZE_WO_LIST_TEMP where company_id = :i_company_id;
	
	if check_count > 0 then 
		f_wo_status_box("", "Pending Transactions Created for the Work Order.")
	end if 
end if

return 1
end function

public function integer of_prepare_pend_transaction ();longlong bk_schema, num_nrows, ocr_charge_type_id, check_count
string user_id1, gl_je_code

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": preparing pend trans data..."
of_log() 

user_id1 = s_user_info.user_id 

setnull(gl_je_code)
select gl_je_code into :gl_je_code
from standard_journal_entries, gl_je_control
where standard_journal_entries.je_id = gl_je_control.je_id and upper(ltrim(rtrim(process_id))) = 'AUTOMATIC 101';

if isnull(gl_je_code) or gl_je_code = '' then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to determine gl_je_code for  'AUTOMATIC 101'"
	of_log() 
	return -1
end if 


//////////////////////////////////////////////////INFO for PEND_TRANSACTION prep:///////////////////////////////////////////////////////////////////////////////////////////////
//RETIRE_METHOD_ID: 1 if the ldg_asset_id is not null; otherwise it must be null for post.exe.
//SUBLEDGER_INDICATOR:  always 0.
//BOOKS_SCHEMA_ID:  always 1. (once upon a time, there were multiple books schemas, but not anymore) 
//LDG_DEPR_GROUP_ID: the negtaive of closing_option_id
//FUNC_CLASS_ID:  the FUNC_CLASS_ID from UTILITY_ACCOUNT 
//REPLACEMENT_AMOUNT:  UNITIZED_WORK_ORDER.REPLACEMENT_AMOUNT if Additons only (do not NVL()); else 0
//
////CLOSING OPTIONS included for Auto101 from dw_wo_to_unitize_101:
//
//      "WORK_ORDER_ACCOUNT"."CLOSING_OPTION_ID" in (2,4) 
//AND "WORK_ORDER_CONTROL"."WO_STATUS_ID" in (4,5) 
//AND "WORK_ORDER_CONTROL"."COMPLETION_DATE" is not null   
//AND add_months(to_date(to_char(completion_date,'mm/yyyy'),'mm/yyyy'), nvl(work_order_control.late_chg_wait_period,0)) <= :month
//or
//      "WORK_ORDER_ACCOUNT"."CLOSING_OPTION_ID" in (6,7,8)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
			
//first clear out the global temp table
delete from UNITIZE_PEND_TRANS_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_PEND_TRANS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//now insert into global temp table.
//IN_SERVICE_YEAR:  if i_closing_option_id = 2 or 4 then WORK_ORDER_CONTROL.INSERVICE_DATE; else i_month_yr in this insert.  an update SQL syntax later will handle OCR retirements.
//ACTIVITY_CODE:  UNITIZED_WORK_ORDER.ACTIVITY_CODE; if MRET, change to URET
//POSTING_QUANTITY:  If additions, then use UNIT_ITEM's Quantity; if SAGL/URGL, use quantity; otherwise 0.
//POSTING_AMOUNT:  set it to 0 in this insert.  an update SQL syntax later will handle URGL/SAGLfor OCR retirements and another SQL will foot the posting_amount.
//LDG_ACIVITY_ID:  null in this insert.  an update SQL syntax later will handle URGL/SAGL for OCR retirements.
insert into UNITIZE_PEND_TRANS_TEMP
(COMPANY_ID, GL_POSTING_MO_YR, WORK_ORDER_ID, WORK_ORDER_NUMBER, UNIT_ITEM_ID,  LDG_ASSET_ID, LDG_DEPR_GROUP_ID, IN_SERVICE_YEAR,
GL_ACCOUNT_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID, FUNC_CLASS_ID, RETIREMENT_UNIT_ID, PROPERTY_GROUP_ID, ASSET_LOCATION_ID, 
GL_JE_CODE, SERIAL_NUMBER, FERC_ACTIVITY_CODE, SUBLEDGER_INDICATOR, DESCRIPTION, LONG_DESCRIPTION, 
RETIRE_METHOD_ID, BOOKS_SCHEMA_ID, USER_ID1, 

ACTIVITY_CODE, 
POSTING_QUANTITY, 
POSTING_AMOUNT, 
REPLACEMENT_AMOUNT,

BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10,
BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20,
BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30,
BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40,
BASIS_41, BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60,
BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70,
RESERVE_CREDITS, SALVAGE_RETURNS, SALVAGE_CASH, COST_OF_REMOVAL
)
select b.COMPANY_ID, :i_month_yr, b.WORK_ORDER_ID, a.WORK_ORDER_NUMBER, b.UNIT_ITEM_ID,  b.LDG_ASSET_ID, a.CLOSING_OPTION_ID*-1,
DECODE(a.CLOSING_OPTION_ID, 2, a.IN_SERVICE_DATE, 4, a.IN_SERVICE_DATE,  :i_month_yr) IN_SERVICE_YEAR,
B.GL_ACCOUNT_ID, b.BUS_SEGMENT_ID, b.UTILITY_ACCOUNT_ID, b.SUB_ACCOUNT_ID, e.FUNC_CLASS_ID, b.RETIREMENT_UNIT_ID, b.PROPERTY_GROUP_ID, b.ASSET_LOCATION_ID, 
:gl_je_code, trim(b.SERIAL_NUMBER), c.expenditure_type_id FERC_ACTIVITY_CODE, 0 SUBLEDGER_INDICATOR, nvl(trim(b.SHORT_DESCRIPTION),'Work Order Addition'), b.DESCRIPTION, 
decode(b.ldg_asset_id, null, null, 1) RETIRE_METHOD_ID, 1 BOOKS_SCHEMA_ID, :user_id1,  

DECODE(TRIM(b.ACTIVITY_CODE), 'MRET', 'URET', b.ACTIVITY_CODE) ACTIVITY_CODE, 
DECODE(c.expenditure_type_id, 1, nvl(b.QUANTITY,0), 0) POSTING_QUANTITY, 
0 POSTING_AMOUNT, 
DECODE(c.expenditure_type_id, 1, b.REPLACEMENT_AMOUNT, 0) REPLACEMENT_AMOUNT,

SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 1, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 2, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 3, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 4, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 5, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 6, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 7, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 8, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 9, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 10, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 11, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 12, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 13, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 14, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 15, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 16, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 17, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 18, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 19, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 20, c.AMOUNT, 0), 0)), 
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 21, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 22, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 23, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 24, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 25, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 26, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 27, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 28, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 29, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 30, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 31, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 32, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 33, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 34, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 35, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 36, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 37, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 38, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 39, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 40, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 41, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 42, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 43, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 44, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 45, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 46, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 47, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 48, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 49, c.AMOUNT, 0), 0)), 
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 50, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 51, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 52, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 53, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 54, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 55, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 56, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 57, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 58, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 59, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 60, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 61, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 62, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 63, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 64, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 65, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 66, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 67, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 68, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 69, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, DECODE(d.BOOK_SUMMARY_ID, 70, c.AMOUNT, 0), 0)),
SUM(DECODE(c.expenditure_type_id, 1, 0, DECODE(d.PROCESSING_TYPE_ID, 8, c.AMOUNT, 0)))*-1,  /*RESERVE_CREDITS*/
SUM(DECODE(c.expenditure_type_id, 1, 0, DECODE(d.PROCESSING_TYPE_ID, 7, c.AMOUNT, 0)))*-1,  /*SALVAGE_RETURNS*/ 
SUM(DECODE(c.expenditure_type_id, 1, 0, DECODE(d.PROCESSING_TYPE_ID, 2, c.AMOUNT, 0)))*-1,  /*SALVAGE_CASH*/
SUM(DECODE(c.expenditure_type_id, 1, 0, DECODE(d.PROCESSING_TYPE_ID, 1, 0, 8, 0, 7, 0, 2, 0, c.AMOUNT)))*-1  /*COST_OF_REMOVAL*/
from UNITIZE_WO_LIST_TEMP a, UNITIZED_WORK_ORDER b, CHARGE_GROUP_CONTROL c, CHARGE_TYPE d, UTILITY_ACCOUNT e
where a.work_order_id = b.work_order_id
and a.company_id = :i_company_id
and a.error_msg is null
and b.work_order_id = c.work_order_id
and b.unit_item_id = c.unit_item_id
and c.charge_type_id = d.charge_type_id
and e.utility_account_id = b.utility_account_id
and e.bus_segment_id = b.bus_segment_id
and b.unit_item_id > 0 
and c.expenditure_type_id in (1,2)  /*do not filter out OCR processing_type_id.  the decodes() above will exclude dollars for OCR*/ 
and c.batch_unit_item_id is null
and nvl(c.pend_transaction,0) = 0 
and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS f
					where f.work_order_id = b.work_order_id
					and f.unit_item_id = b.unit_item_id
					)
group by b.COMPANY_ID, b.WORK_ORDER_ID, a.WORK_ORDER_NUMBER, b.UNIT_ITEM_ID, b.LDG_ASSET_ID, a.CLOSING_OPTION_ID*-1, 
DECODE(a.CLOSING_OPTION_ID, 2, a.IN_SERVICE_DATE, 4, a.IN_SERVICE_DATE, :i_month_yr),
B.GL_ACCOUNT_ID, b.BUS_SEGMENT_ID, b.UTILITY_ACCOUNT_ID, b.SUB_ACCOUNT_ID, e.FUNC_CLASS_ID, b.RETIREMENT_UNIT_ID, b.PROPERTY_GROUP_ID, b.ASSET_LOCATION_ID, 
trim(b.SERIAL_NUMBER), c.expenditure_type_id, nvl(trim(b.SHORT_DESCRIPTION),'Work Order Addition'), b.DESCRIPTION, 
DECODE(TRIM(b.ACTIVITY_CODE), 'MRET', 'URET', b.ACTIVITY_CODE), 
DECODE(c.expenditure_type_id, 1, nvl(b.QUANTITY,0), 0),
DECODE(c.expenditure_type_id, 1, b.REPLACEMENT_AMOUNT, 0) 
;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_PEND_TRANS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows    

if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": preparing pend basis data..."
	of_log() 
	
	//Now insert Unit Items that have $0 allocated dollars to include quantity data. 
	//Need to create pend transactions for these because they might have non-zero quantity.
	//Even in the case of 0 quantity and 0 allocated cost, we want to get them maked as "used" for unitization so they don't hang around for next time.
	//Maint 37939: need to be careful to avoid doing this where there are no addition charges eligible to unitize in this run.
	insert into UNITIZE_PEND_TRANS_TEMP
	(COMPANY_ID, GL_POSTING_MO_YR, WORK_ORDER_ID, WORK_ORDER_NUMBER, UNIT_ITEM_ID,  LDG_ASSET_ID, LDG_DEPR_GROUP_ID, IN_SERVICE_YEAR,
	GL_ACCOUNT_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID, FUNC_CLASS_ID, RETIREMENT_UNIT_ID, PROPERTY_GROUP_ID, ASSET_LOCATION_ID, 
	GL_JE_CODE, SERIAL_NUMBER, FERC_ACTIVITY_CODE, SUBLEDGER_INDICATOR, DESCRIPTION, LONG_DESCRIPTION, 
	RETIRE_METHOD_ID, BOOKS_SCHEMA_ID, USER_ID1, 
	ACTIVITY_CODE, 
	POSTING_QUANTITY, 
	POSTING_AMOUNT, 
	REPLACEMENT_AMOUNT,
	BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10,
	BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20,
	BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30,
	BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40,
	BASIS_41, BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
	BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60,
	BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70,
	RESERVE_CREDITS, SALVAGE_RETURNS, SALVAGE_CASH, COST_OF_REMOVAL
	)
	select b.COMPANY_ID, :i_month_yr, b.WORK_ORDER_ID, a.WORK_ORDER_NUMBER, b.UNIT_ITEM_ID,  b.LDG_ASSET_ID, a.CLOSING_OPTION_ID*-1,
	DECODE(a.CLOSING_OPTION_ID, 2, a.IN_SERVICE_DATE, 4, a.IN_SERVICE_DATE,  :i_month_yr) IN_SERVICE_YEAR,
	B.GL_ACCOUNT_ID, b.BUS_SEGMENT_ID, b.UTILITY_ACCOUNT_ID, b.SUB_ACCOUNT_ID, e.FUNC_CLASS_ID, b.RETIREMENT_UNIT_ID, b.PROPERTY_GROUP_ID, b.ASSET_LOCATION_ID, 
	:gl_je_code, trim(b.SERIAL_NUMBER), 1 FERC_ACTIVITY_CODE, 0 SUBLEDGER_INDICATOR, nvl(trim(b.SHORT_DESCRIPTION),'Work Order Addition'), b.DESCRIPTION, 
	decode(b.ldg_asset_id, null, null, 1) RETIRE_METHOD_ID, 1 BOOKS_SCHEMA_ID, :user_id1, 
	UPPER(TRIM(b.ACTIVITY_CODE)) ACTIVITY_CODE, 
	nvl(b.QUANTITY,0) POSTING_QUANTITY, 
	0 POSTING_AMOUNT, 
	nvl(b.REPLACEMENT_AMOUNT,0) REPLACEMENT_AMOUNT,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0
	from UNITIZE_WO_LIST_TEMP a, UNITIZED_WORK_ORDER b, UTILITY_ACCOUNT e
	where a.work_order_id = b.work_order_id
	and a.company_id = :i_company_id
	and a.has_elig_adds = 1
	and a.error_msg is null 
	and b.unit_item_id > 0 
	and e.utility_account_id = b.utility_account_id
	and e.bus_segment_id = b.bus_segment_id
	and UPPER(TRIM(b.ACTIVITY_CODE)) in ( 'MADD', 'UADD')
	and exists (select 1 from charge_group_control c
					where b.work_order_id = c.work_order_id
					and c.expenditure_type_id = 1 /*Maint 37939*/
					and c.batch_unit_item_id is null
					and nvl(c.pend_transaction,0) = 0
					)
	and not exists (select 1 from wo_unit_item_pend_trans d
					where b.work_order_id = d.work_order_id
					and b.unit_item_id = d.unit_item_id 
					)
	and not exists (select 1 from UNITIZE_PEND_TRANS_TEMP f
					where b.work_order_id = f.work_order_id
					and b.unit_item_id = f.unit_item_id 
					)
	group by b.COMPANY_ID, b.WORK_ORDER_ID, a.WORK_ORDER_NUMBER, b.UNIT_ITEM_ID, b.LDG_ASSET_ID, a.CLOSING_OPTION_ID*-1, 
	DECODE(a.CLOSING_OPTION_ID, 2, a.IN_SERVICE_DATE, 4, a.IN_SERVICE_DATE, :i_month_yr),
	B.GL_ACCOUNT_ID, b.BUS_SEGMENT_ID, b.UTILITY_ACCOUNT_ID, b.SUB_ACCOUNT_ID, e.FUNC_CLASS_ID, b.RETIREMENT_UNIT_ID, b.PROPERTY_GROUP_ID, b.ASSET_LOCATION_ID, 
	trim(b.SERIAL_NUMBER), 1, nvl(trim(b.SHORT_DESCRIPTION),'Work Order Addition'), b.DESCRIPTION, 
	decode(b.ldg_asset_id, null, null, 1), 
	UPPER(TRIM(b.ACTIVITY_CODE)),
	nvl(b.QUANTITY,0), 
	nvl(b.REPLACEMENT_AMOUNT,0) 
	;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_PEND_TRANS_TEMP (extra): " + sqlca.SQLErrText
		of_log() 
		rollback;		
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    
end if //if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then (Late Close does not need this because all quantity is 0)

if i_elig_ocr > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": preparing pend RWIP data..."
	of_log() 

	//backfill pend data for OCR unit items that are URGL/SAGL.  LDG_ACIVITY_ID = CHARGE_GROUP_ID
	//the BASIS columns will be handled in the insert into PEND_BASIS.
	UPDATE UNITIZE_PEND_TRANS_TEMP a
	set (a.posting_amount, a.posting_quantity, a.LDG_ACTIVITY_ID) = 
				(select  
				c.amount*-1, c.quantity*-1, c.charge_group_id 
				from UNITIZE_WO_LIST_TEMP x, unitized_work_order b, charge_group_control c
				where a.work_order_id = x.work_order_id
				and a.work_order_id = b.work_order_id
				and a.unit_item_id = b.unit_item_id
				and b.unit_item_id = c.unit_item_id
				and b.work_order_id = c.unit_item_id
				and x.has_gain_loss_ocr = 1
				and c.charge_type_id = :i_ocr_charge_type_id /*OCR charge_type_id*/
				and trim(b.activity_code) in ('URGL', 'SAGL')
				and c.expenditure_type_id = 2
				and c.unit_item_id > 0
				and c.batch_unit_item_id is null
				and nvl(c.pend_transaction, 0) = 0
				)
	where a.company_id = :i_company_id
	and a.ferc_activity_code = 2
	and trim(a.activity_code) in ('URGL', 'SAGL')
	and exists (select 1 
					from UNITIZE_WO_LIST_TEMP x, unitized_work_order b, charge_group_control c
					where a.work_order_id = x.work_order_id
					and a.work_order_id = b.work_order_id
					and a.unit_item_id = b.unit_item_id
					and b.unit_item_id = c.unit_item_id
					and b.work_order_id = c.unit_item_id
					and x.has_gain_loss_ocr = 1
					and c.charge_type_id = :i_ocr_charge_type_id /*OCR charge_type_id*/
					and trim(b.activity_code) in ('URGL', 'SAGL')
					and c.expenditure_type_id = 2
					and c.unit_item_id > 0
					and c.batch_unit_item_id is null
					and nvl(c.pend_transaction, 0) = 0
					);
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: update UNITIZE_PEND_TRANS_TEMP (URGL quantity/cost): " + sqlca.SQLErrText
		of_log() 
		rollback;		
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    
end if  //if i_elig_ocr > 0

//maint-43648:  remove IN_SERVICE_DATE update using PAYMENT_DATE from CWIP_CHARGE on OCR units.  SQL was extremely slow.
//               this logic is not right anyway in the rare case where PAYMENT_DATE might even be used.  see ARCPP-895.

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": finalizing pend data..."
of_log() 

//first, backfill PEND_TRANS_ID = pwrplant1.nextval
//this update will not group the PEND_TRANS_IDs to be sequential by WORK_ORDER_NUMBER.
update UNITIZE_PEND_TRANS_TEMP
set pend_trans_id = pwrplant1.nextval 
where company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update UNITIZE_PEND_TRANS_TEMP.pend_trans_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		
	return -1
end if		
num_nrows = sqlca.sqlnrows    


///////////////////////////////////////////////THIS PORTION OF THE CODE IS NEEDED/////////////////////////////////////////////////////////////
//////////////////////////////////BASE DB TRIGGERS WILL UPDATE THE POSTING AMOUNT FOR UPDATES not INSERTS/////////////////////////
//Update Pend Transaction with Posting Amount
longlong num_buckets,max_bk_id, k
string sqls

select count(*), max(book_summary_id) into :num_buckets, :max_bk_id
from book_summary;

if num_buckets <> max_bk_id then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: max(book_summary_id) = " + string(max_bk_id) + " vs count(*) = " + string(num_buckets) 
	i_msg[2] = ":     book_summary_id numbers should not be skipped!" 
	of_log() 
	return -1
end if 

sqls = ''
sqls = 'update UNITIZE_PEND_TRANS_TEMP c set c.posting_amount = (select sum( '
for k = 1 to num_buckets
	if k = 1 then
		sqls+= 'nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	else
		sqls+= ' + nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	end if
next

sqls += ') from UNITIZE_PEND_TRANS_TEMP a, set_of_books b  '
sqls += ' where c.pend_trans_id =  a.pend_trans_id '
sqls += ' and a.ldg_activity_id is null '
sqls += ' and b.set_of_books_id = 1 '
sqls += ') ' // end of sum select
sqls += ' where c.company_id = ' + string(i_company_id) + ' and c.ldg_activity_id is null'
	
execute immediate :sqls;

if sqlca.sqlcode <> 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  'Error: updating posting_amount: ' + sqlca.sqlerrtext
	i_msg[2] =  ' SQL: ' + sqls 
	of_log() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows

return 1
end function

public function string of_wip_comp_closing101_wo (longlong a_company_id, longlong a_month_number, string a_closing_type, longlong a_wo_id);////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////NOT BEING USED:  continue using the existing code in uo_wip_computation.uf_wip_comp_closing()
////copied from uo_wip_computation.uf_wip_comp_closing and adapted for the new unitization code.
////arg 1:  a_company_id
////arg 2:  a_month_number
////arg 3:  a_closing_type  <-- will always be "unitization" here
////arg 4:  a_wo_id
////return type is a string variable for any errors encountered here.  if no error, the return value of '' will be used for the wo.
////
////this function uses data already saved in the PENDING tables.
////there are external functions that may also rely on data already saved in the PENDING table.
////data in all the PENDING tables (including class_code_pending_trans) for the non-WIP Comp closings is required prior to starting the code in this function!!!
////this function will add new records to PEND_TRANSACTION tables for WIP Comp closings.
////this function should have no commits or rollbacks, the calling function controls both.
////errors encountered for a wo in this function needs to cause non-WIP Comp data already staged for pending to be deleted.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//
//string DESCRIPTION,UNITIZATION_BASIS, SQLs, error_message  
//longlong num_nrows, ext_rtn
//longlong wip_comps[], i, unit_basis[], j, k, num_buckets 
//any results[], reset_array[]
//
////i_msg[] = i_null_arrary[] 
////i_msg[1] =  ": unitizing WIP Comp charges..."
////of_log()   
////
//
//error_message = ''
//
////Call Extension Function
//ext_rtn = 0
//ext_rtn = f_wip_comp_extension(-1, a_month_number, 'closing_start', a_wo_id, a_company_id)
//if ext_rtn = -1 then 
//	error_message = "Error Occured in f_wip_comp_extension for wip computation closing start" 
//	return error_message
//end if 
//
//delete from wip_comp_temp_wo;
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: delete from wip_comp_temp_wo: " + sqlca.SQLErrText
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows     
//
//insert into wip_comp_temp_wo (work_order_id) values (:a_wo_id);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: insert into wip_comp_temp_wo: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows     
// 
////Call Extensions:  ---- good to update not eligible flag or insert additional work order ids
//ext_rtn = 0
//ext_rtn = f_wip_comp_dynamic_ext(-1,a_month_number, 'closing-after-eligibility', a_wo_id, a_company_id)
//if ext_rtn = -1 then 
//	error_message = "Error Occured in f_wip_comp_dynamic_ext for wip computation = " + description
//	return error_message
//end if   
//
//
////1.	Compute Charges to be closed based on the type - WIP_COMP_UNIT_CALC 
//delete from wip_comp_unit_calc a 
//where a.UNIT_TYPE = :a_closing_type
//and exists (select 1 from wip_comp_temp_wo b
//						where a.work_order_id = b.work_order_id
//						)
//and nvl(pend_transaction, 0) = 0;
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: Delete previous runs from WIP COMP UNIT CALC: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows     
// 
// // maint 7883 6/29/2011
//update wip_comp_charges a
//set wip_comp_summary_id = decode(lower(:a_closing_type),'non-unitized', decode(nvl(a.non_unitized_status,0) ,0,null),wip_comp_summary_id), 
//wip_comp_sum_unitize_id = decode(lower(:a_closing_type),'unitization',decode(nvl(a.unitized_status,0) ,0,null),wip_comp_sum_unitize_id)
//where exists (select 1 from wip_comp_temp_wo b where a.work_order_id = b.work_order_id);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: Update wip_comp_charges.wip_comp_summary_id: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows    
//
//insert into wip_comp_unit_calc (
//WIP_COMP_SUMMARY_ID,
//WIP_COMPUTATION_ID,
//WORK_ORDER_ID,
//MONTH_NUMBER,
//TOTAL_AMOUNT,
//BOOK_SUMMARY_ID,
//UNIT_TYPE,
//PROCESS_SESSION)
//(select pwrplant1.nextval, a.* 
//		from (select a.wip_computation_id, a.work_order_id, :a_month_number, sum(a.amount), b.book_summary_id,:a_closing_type,userenv('sessionid')
//				from wip_comp_charges a, wip_comp_temp_wo c,  charge_type b, cost_element e
//				where a.work_order_id = c.work_order_id
//				and a.cost_element_id = e.cost_element_id
//				and e.charge_Type_id = b.charge_type_id
//				and a.wip_comp_sum_unitize_id is null
//				and nvl(a.unitized_status,0) =  0
//				and nvl(a.non_unitized_status,0) <> 100 
//				and a.month_number <= :a_month_number 
//				and exists (select 1 from wip_computation j
//								where a.wip_computation_id = j.wip_computation_id
//								and nvl(j.close_to_asset,1) = 1
//								and j.expenditure_type_id = 5)
//				group by a.wip_computation_id, a.work_order_id, :a_month_number, b.book_summary_id,:a_closing_type,userenv('sessionid')
//				) a
//				
//	);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: Insert WIP COMP UNIT CALC Unitized: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows    
//
////2.	Mark charges with summary id
//update wip_comp_charges b
//set wip_comp_sum_unitize_id = (select wip_comp_summary_id
//										from wip_comp_unit_calc a,  charge_type d, cost_element e
//										where a.wip_computation_id = b.wip_computation_id
//										and a.work_order_id = b.work_order_id
//										and a.month_number = :a_month_number
//										and a.book_summary_id = d.book_summary_id
//										 and b.cost_element_id = e.cost_element_id
//										and e.charge_Type_id = d.charge_type_id
//										and a.UNIT_TYPE = :a_closing_type // CWB 20100709 in case they're processing more than one type (ex. unitization & non-untiization) during the same month 
//										and b.wip_computation_id = a.wip_computation_id //cwb 20100317 added wip_computation_id to the join
//										/* ### 8116: JAK: 20110719:  If there are multiple closing of the same WIP comp, need to pickup only the latest run */
//										and nvl(a.pend_transaction,0) = 0 
//										)
//where exists (select 1 from wip_comp_temp_wo c
//					where b.work_order_id = c.work_order_id
//					)
//and b.wip_comp_sum_unitize_id is null
//and b.month_number <= :a_month_number 
//and nvl(b.unitized_status,0) =  0
//and nvl(b.non_unitized_status,0) <> 100
//and exists (select 1 from wip_computation j
//								where b.wip_computation_id = j.wip_computation_id
//								and nvl(j.close_to_asset,1) = 1
//								and j.expenditure_type_id = 5)
//;
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: Insert WIP COMP CHARGES Update Unitized: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows      
//	
////3.	Get list of current pending transactions from into WIP_COMP_PEND_TRANS table
//
//// Delete on unused or last run of wip_comp_pending_trans records where pend transactions no longer exist
//delete from wip_comp_pending_trans b
//where exists (select 1 from wip_comp_temp_wo c
//						where b.work_order_id = c.work_order_id
//						)
//and nvl(pend_transaction,0) = 0;
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: Delete wip_comp_pending_trans where pend_transaction = 0: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows    
//
//
////Maint 8241: for "unitized (101)" closings, add the wip_comp101_rollup_id
//insert into wip_comp_pending_trans
//(WIP_COMP_SUMMARY_ID,
//ORIG_PEND_TRANS_ID,
//WIP_COMPUTATION_ID,
//WORK_ORDER_ID,
//MONTH_NUMBER,
//POSTING_AMOUNT,
//PEND_BASIS_AMOUNT,
//NEW_PEND_TRANS_ID,
//AMOUNT,
//BOOK_SUMMARY_ID,
//PEND_TRANSACTION,
//PROCESS_SESSION, 
//wip_comp101_rollup_id)
//(select a.wip_comp_summary_id, b.pend_trans_id, a.wip_computation_id, a.work_order_id, :a_month_number, b.posting_amount, 0, 0, 0, a.book_summary_id,
//0, userenv('sessionid'), wc.wip_comp101_rollup_id
//from wip_comp_unit_calc a, pend_transaction b, work_order_control woc, gl_account gl, wip_computation wc
//where woc.work_order_number = b.work_order_number
//and wc.wip_computation_id = a.wip_computation_id
//and woc.company_id = b.company_id
//and woc.funding_wo_indicator = 0  
//and b.gl_account_id = gl.gl_account_id
//and (gl.account_type_id <> 12 and b.retirement_unit_id not between 1 and 5)
//and a.work_order_id = woc.work_order_id
//and trim(upper(b.activity_code)) in ('UADD','MADD')
//and b.ldg_depr_group_id < 0    /*##sjh 7/11/2011  Maint 7691*/
//and nvl(a.pend_transaction,0) = 0 
//and to_number(to_char(b.gl_posting_mo_yr,'yyyymm')) = :a_month_number
//and A.UNIT_TYPE = :a_closing_type  /*##sjh 7/11/2011  Maint 7997*/
//and exists (select 1 from wip_comp_temp_wo c
//					where a.work_order_id = c.work_order_id)
//);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: Insert wip_comp_pending_trans from pend trans unitized: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows    
// 
//
//// - Update Pend Basis Amount
////ADD HERE Need to add for non-timed unitization or 106 processing must pull from pend_trans_archive
//sqls = 'select distinct a.WIP_COMPUTATION_ID from wip_comp_pending_trans a, wip_computation b where (a.WIP_COMPUTATION_ID = b.WIP_COMPUTATION_ID and unitization_basis_chk = 1) '
//results = reset_array
//f_get_column(results,sqls)
//wip_comps = results
//
//if upperbound(wip_comps) > 0 then
//	for i = 1 to upperbound(wip_comps)
//		setnull(unitization_basis)
//		select unitization_basis
//		into :unitization_basis
//		from wip_computation
//		where wip_computation_id = :wip_comps[i]
//		;
//		if isnull(unitization_basis) then
//			error_message = "Error the Unitization Basis and Basis Check fields not in sync for wip_computation_id = " + string(wip_comps[i]) 
//			return error_message
//		end if
//		f_parsestringintonumarray (unitization_basis,',',unit_basis)
//		
//		sqls = 'update wip_comp_pending_trans a set PEND_BASIS_AMOUNT = (select '
//		for j = 1 to upperbound(unit_basis)
//			if j = 1 then
//				sqls+= 'BASIS_' + string(unit_basis[j])			
//			else
//				sqls+= ' + BASIS_' + string(unit_basis[j])
//			end if
//		next
//		sqls+= ' from pend_basis b '
//		sqls+= ' where a.ORIG_PEND_TRANS_ID = b.pend_trans_id '
//		sqls+= ' and wip_computation_id = ' + string(wip_comps[i])
//		sqls+= ' and nvl(pend_transaction,0) = 0 '
//		sqls+= ' and exists (select 1 from pend_basis c where a.ORIG_PEND_TRANS_ID = c.pend_trans_id) '
//		sqls+= '  and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) ) ' //// JRH 20090224 --> Added Missing Parenthesis
//		
//		execute immediate :sqls;
//
//		if sqlca.SQLCode < 0 then 
//			error_message =  ": ERROR: update wip_comp_pending_trans pend basis amount from pend basis unitized: " + sqlca.SQLErrText 
//			return error_message
//		end if		
//		num_nrows = sqlca.sqlnrows    
//	next
//end if
//
////If they are not using specific basis amounts update to posting amount
//update wip_comp_pending_trans a set PEND_BASIS_AMOUNT = POSTING_AMOUNT
//where wip_computation_id in (select wip_computation_id from wip_computation where nvl(unitization_basis_chk,0) = 0)
//and  nvl(pend_transaction,0) = 0 
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) ;
//
//if sqlca.SQLCode < 0 then  
//	error_message = ": ERROR: update wip_comp_pending_trans pend basis amount from posting amount: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows
//
////if pend_basis_amount is sum zero for a work order id then update to posting amount
//update wip_comp_pending_trans a set PEND_BASIS_AMOUNT = POSTING_AMOUNT, pend_basis_override = 'WO BASIS ZERO USE POSTING AMT'
//where wip_computation_id in (select wip_computation_id from wip_computation where nvl(unitization_basis_chk,0) = 1)
//and  nvl(pend_transaction,0) = 0 
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) 
//and work_order_id in (select work_order_id 
//							from wip_comp_pending_trans
//							where nvl(pend_transaction,0) = 0 
//							and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) 
//							group by work_order_id
//							having sum(pend_basis_amount) = 0);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: update wip_comp_pending_trans pend basis amount from posting amount where wo = 0: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows 
//
////don't want to fail so set pend_basis_amount = 1
//update wip_comp_pending_trans a set PEND_BASIS_AMOUNT = 1, pend_basis_override = 'WO ZERO POSTING AMT DFLT to 1'
//where nvl(pend_transaction,0) = 0 
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) 
//and work_order_id in (select work_order_id 
//							from wip_comp_pending_trans
//							where nvl(pend_transaction,0) = 0 
//							and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) 
//							group by work_order_id
//							having sum(pend_basis_amount) = 0);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: update wip_comp_pending_trans pend basis amount from posting amount where wo = 0 for posting amount: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows  
//
////4.	Allocate UNIT_CALC over the PEND_TRANS
////ratio_to_report(<amt>) over(Partion by <field>)
//update wip_comp_pending_trans a
//set amount = (select sum(allocated_amt) allocated_amt
//			from (
//			select wip_comp_summary_id, orig_pend_trans_id, allocated_amt
//			from (
//			select a.wip_comp_summary_id,a.ORIG_PEND_TRANS_ID,
//			ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) alloc_pct,
//			b.total_amount,
//			round(ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) * b.total_amount,2) allocated_amt
//			from wip_comp_pending_trans a, wip_comp_unit_calc b
//			where a.wip_comp_summary_id = b.wip_comp_summary_id
//			and nvl(a.pend_transaction,0) = 0
//			and nvl(b.pend_transaction,0) = 0
//			and B.UNIT_TYPE = :a_closing_type  /*##sjh 7/11/2011  Maint 7997*/
//			)
//			union all /*maint 38389 needs to be "union all"*/
//			select a.wip_comp_summary_id, 
//			 d.orig_pend_trans_id,
//			/*sum(allocated_amt) - b.total_amount allocated_amt   <-- this calc is backwards ###sjh 7/5/2011  Maint 7958*/
//			 b.total_amount - sum(allocated_amt) allocated_amt
//			from (select a.wip_comp_summary_id,a.ORIG_PEND_TRANS_ID,
//			ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) alloc_pct,
//			b.total_amount,
//			round(ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) * b.total_amount,2) allocated_amt
//			from wip_comp_pending_trans a, wip_comp_unit_calc b
//			where a.wip_comp_summary_id = b.wip_comp_summary_id
//			and nvl(a.pend_transaction,0) = 0
//			and nvl(b.pend_transaction,0) = 0
//			and B.UNIT_TYPE = :a_closing_type  /*##sjh 7/11/2011  Maint 7997*/
//			) a, wip_comp_unit_calc b, 
//			(select min(orig_pend_trans_id) orig_pend_trans_id,d.wip_comp_summary_id from wip_comp_pending_trans d where nvl(d.pend_transaction,0) = 0 group by d.wip_comp_summary_id) d
//												
//			where a.wip_comp_summary_id = b.wip_comp_summary_id
//			and d.wip_comp_summary_id = a.wip_comp_summary_id
//			group by a.wip_comp_summary_id, b.total_amount,d.orig_pend_trans_id
//			) aa
//			where aa.wip_comp_summary_id = a.wip_comp_summary_id
//			and aa.orig_pend_trans_id = a.orig_pend_trans_id
//)
//where  nvl(a.pend_transaction,0) = 0
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	);
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: update wip_comp_pending_trans set amount from allocation: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows  
//	
////5.	Insert Pending Transactions
////a.	Keep all attributes except gl_account and amounts
////ADD HERE Need to add for non-timed unitization or 106 processing must pull from pend_trans_archive
////Logic for Minor Add that doesn't use the GL Account from WIP Computation
//update wip_comp_pending_trans a set gl_account_id = 
//(select decode(nvl(b.post_minor_add,0),1,c.gl_account_id,decode(:a_closing_type, 'non-unitized',non_unit_gl_account,'unitization',unitized_gl_account))
//	from wip_computation b, pend_transaction c
//	where a.wip_computation_id = b.wip_computation_id
//		and c.pend_trans_id = a.orig_pend_Trans_id
//	)
//where  nvl(a.pend_transaction,0) = 0
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	);
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: update wip_comp_pending_trans set gl account: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows   
//
////b.	Retirement Unit option, allows for a specific retirement unit
//update wip_comp_pending_trans a set retirement_unit_id = 
//(select decode(:a_closing_type, 'non-unitized',1,decode(nvl(b.post_minor_add,0),1,c.retirement_unit_id,nvl(b.retirement_unit_id, c.retirement_unit_id)))
//	from wip_computation b, pend_transaction c
//	where a.wip_computation_id = b.wip_computation_id
//	and c.pend_trans_id = a.orig_pend_Trans_id
//	)
//where  nvl(a.pend_transaction,0) = 0
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	);
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: update wip_comp_pending_trans set retirement unit: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows    
//	
////c.	If option is to use Minor Adds,
//update wip_comp_pending_trans a set minor_pend_tran_id = orig_pend_trans_id
//where  nvl(a.pend_transaction,0) = 0
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
//and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(post_minor_add,0) = 1);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: update wip_comp_pending_trans set minor_pend_tran_id: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows     
//		
////d.	Flag as regulatory entries -  pend_transaction.wip_comp_transaction = 1
////e.	Assign New Pend Trans Id
//delete from wip_comp_pend_trans_temp;
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: wip_comp_pend_trans_temp delete: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows    
//
////Maint 8241: for "unitized (101)" closings, save the nvl(wip_comp101_rollup_id,0) in the wip_computation_id field
////                  for "non-unitized (106)" closings, the wip_computation_id value is saved as is 	
//INSERT INTO wip_comp_pend_trans_temp 
//				(orig_pend_trans_id, 
//				 work_order_id, 
//				 month_number, 
//				 gl_account_id, 
//				 retirement_unit_id, 
//				 minor_pend_tran_id, 
//				 wip_computation_id,
//				 wip_comp101_rollup_id) 
//(SELECT DISTINCT aa.orig_pend_trans_id, 
//					  aa.work_order_id, 
//					  aa.month_number, 
//					  aa.gl_account_id, 
//					  aa.retirement_unit_id, 
//					  Nvl(aa.minor_pend_tran_id, 0), 
//					  Decode(Lower(:a_closing_type), 'non-unitized', aa.wip_computation_id, /*NULL*/ 0) ,
//					  Decode(Lower(:a_closing_type), 'non-unitized', null, bb.wip_comp101_rollup_id) 
// FROM   wip_comp_pending_trans aa, wip_computation bb
// WHERE  aa.wip_computation_id = bb.wip_computation_id and Nvl(aa.pend_transaction, 0) = 0 
//		  AND EXISTS (SELECT 1 
//						  FROM   wip_comp_temp_wo c 
//						  WHERE  aa.work_order_id = c.work_order_id));  
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: update wip_comp_pend_trans_temp insert: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows     
//
//update wip_comp_pend_trans_temp
//set new_pend_trans_id = pwrplant1.nextval;
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: update wip_comp_pend_trans_temp.new_pend_trans_id: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows     
//
//UPDATE wip_comp_pending_trans a 
//SET    new_pend_trans_id = (SELECT new_pend_trans_id 
//									 FROM   wip_comp_pend_trans_temp b 
//									 WHERE  a.orig_pend_trans_id = b.orig_pend_trans_id 
//									  AND a.work_order_id = b.work_order_id 
//									  AND a.month_number = b.month_number 
//									  AND a.gl_account_id = b.gl_account_id 
//									  AND a.retirement_unit_id = 
//											b.retirement_unit_id 
//									  AND Nvl(a.minor_pend_tran_id, 0) = 
//											b.minor_pend_tran_id 
//									  AND ( a.wip_computation_id = b.wip_computation_id 
//												OR Lower(:a_closing_type) <> 'non-unitized' ) 
//									  AND nvl(a.wip_comp101_rollup_id,0) = nvl(b.wip_comp101_rollup_id ,0)
//												/*Maint 8241: for "unitized (101)" closings, save the nvl(wip_comp101_rollup_id,0) in the wip_computation_id field*/ 
//											 ) 
//WHERE  Nvl(a.pend_transaction, 0) = 0 
//		 AND EXISTS (SELECT 1 
//						 FROM   wip_comp_temp_wo c 
//						 WHERE  a.work_order_id = c.work_order_id);  
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: update wip_comp_pending_trans set new_pend_trans_id: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows      										
//									
////7.	Insert Related Pend Trans Records
////ADD HERE Need to add for non-timed unitization or 106 processing must pull from pend_trans_archive
//insert into pend_transaction
//(PEND_TRANS_ID,
//LDG_ASSET_ID,
//LDG_DEPR_GROUP_ID,
//BOOKS_SCHEMA_ID,
//RETIREMENT_UNIT_ID,
//UTILITY_ACCOUNT_ID,
//BUS_SEGMENT_ID,
//FUNC_CLASS_ID,
//SUB_ACCOUNT_ID,
//ASSET_LOCATION_ID,
//GL_ACCOUNT_ID,
//COMPANY_ID,
//GL_POSTING_MO_YR,
//SUBLEDGER_INDICATOR,
//ACTIVITY_CODE,
//GL_JE_CODE,
//WORK_ORDER_NUMBER,
//POSTING_QUANTITY,
//USER_ID1,
//POSTING_AMOUNT,
//IN_SERVICE_YEAR,
//DESCRIPTION,
//LONG_DESCRIPTION,
//PROPERTY_GROUP_ID,
//RETIRE_METHOD_ID,
//POSTING_ERROR,
//POSTING_STATUS,
//COST_OF_REMOVAL,
//SALVAGE_CASH,
//SALVAGE_RETURNS,
//FERC_ACTIVITY_CODE,
//SERIAL_NUMBER,
//RESERVE_CREDITS,
//minor_pend_Tran_id,wip_comp_transaction,wip_computation_id)
//(select distinct b.NEW_PEND_TRANS_ID,
//decode(b.minor_pend_tran_id,null,null,LDG_ASSET_ID),
//a.LDG_DEPR_GROUP_ID,
//a.BOOKS_SCHEMA_ID,
//b.RETIREMENT_UNIT_ID,
//a.UTILITY_ACCOUNT_ID,
//a.BUS_SEGMENT_ID,
//a.FUNC_CLASS_ID,
//a.SUB_ACCOUNT_ID,
//a.ASSET_LOCATION_ID,
//b.GL_ACCOUNT_ID,
//a.COMPANY_ID,
//a.GL_POSTING_MO_YR,
//a.SUBLEDGER_INDICATOR,
//a.ACTIVITY_CODE,
//a.GL_JE_CODE,
//a.WORK_ORDER_NUMBER,
//0 POSTING_QUANTITY,
//a.USER_ID1,
//0 POSTING_AMOUNT,
//a.IN_SERVICE_YEAR,
//a.DESCRIPTION,
//a.LONG_DESCRIPTION,
//a.PROPERTY_GROUP_ID,
//pu.RETIRE_METHOD_ID,
//null POSTING_ERROR,
//1 POSTING_STATUS,
//0 COST_OF_REMOVAL,
//0 SALVAGE_CASH,
//0 SALVAGE_RETURNS,
//a.FERC_ACTIVITY_CODE,
//a.SERIAL_NUMBER,
//0 RESERVE_CREDITS,
//b.minor_pend_Tran_id, 1 wip_comp_transaction,
//decode(b.minor_pend_tran_id,null,decode(lower(:a_closing_type), 'unitization', decode(w.wip_comp101_rollup_id, null, null, w.wip_comp101_rollup_id), b.wip_computation_id),null) wip_computation_id /*<--if minor add, wip_computation_id = null */ 
///*Maint 8241: for "unitized (101)" closings, save the wip_comp101_rollup_id in the wip_computation_id field*/ 
//from pend_transaction a, wip_comp_pending_trans b, property_unit pu, retirement_Unit ru, wip_computation w
//where b.orig_pend_trans_Id = a.pend_trans_id
//and nvl(b.pend_transaction,0) = 0
//and w.wip_computation_id = b.wip_computation_id 
//and b.retirement_Unit_id = ru.retirement_unit_id
//and ru.property_unit_id = pu.property_unit_id
//and exists (select 1 from wip_comp_temp_wo c where b.work_order_id = c.work_order_id	)
//)	;
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": insert pending transactions for wip comp close: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows      		 
//
////-- Pend Basis
//
////Get Max Buckets from Book Summary
//select count(*) into :num_buckets from book_summary; 
//
//sqls = ''
//sqls = 'insert into pend_basis (pend_trans_id '
//for k = 1 to num_buckets
//sqls+= ', basis_' + string(k)
//next
//sqls += ' ) '
//sqls += ' ( select b.new_pend_trans_id '
//
//for k = 1 to num_buckets
//sqls+= ', sum(decode(b.book_summary_id, ' + string(k) + ', amount, 0))'
//next
//
//sqls += ' from wip_comp_pending_trans b, pend_transaction c where b.new_pend_trans_id = c.pend_trans_id  '
//sqls+= ' and nvl(b.pend_transaction,0) = 0 '
//sqls+= '  and exists (select 1 from wip_comp_temp_wo c where b.work_order_id = c.work_order_id	) '
//sqls+= ' group by b.new_pend_trans_id ) '
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": insert into pend_basis for wip comp close: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows      		 
//
/////////////////////////////////////////////////THIS PORTION OF THE CODE IS NEEDED/////////////////////////////////////////////////////////////
////////////////////////////////////BASE DB TRIGGERS WILL UPDATE THE POSTING AMOUNT FOR UPDATES not INSERTS/////////////////////////
////Update Pend Transaction with Posting Amount  
//sqls = ''
//sqls = 'update pend_transaction c set posting_amount = (select sum( '
//for k = 1 to num_buckets
//	if k = 1 then
//		sqls+= 'nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
//	else
//		sqls+= ' + nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
//	end if
//next
//sqls += ') from pend_basis a, set_of_books b  '
//sqls += ' where c.pend_trans_id =  a.pend_trans_id '
//sqls += ' and b.set_of_books_id = 1 '
//sqls += ') ' 
//sqls += ' where c.pend_trans_id in (select new_pend_trans_id from wip_comp_pending_trans bb  '
//sqls += ' 									where bb.new_pend_trans_id = c.pend_trans_id '
//sqls+= ' 										and nvl(bb.pend_transaction,0) = 0 '
//sqls+= '  								and exists (select 1 from wip_comp_temp_wo cc where bb.work_order_id = cc.work_order_id	) ) '
//	
//execute immediate :sqls; 
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": update pend_transaction.posting_amount wip comp close: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows
//
//insert into class_code_pending_trans
//(class_code_Id, pend_Trans_id, value)
//(select distinct class_code_Id, new_pend_Trans_id, value
//from  class_code_pending_trans a, wip_comp_pending_trans b
//	where b.orig_pend_trans_Id = a.pend_trans_id
//	and nvl(b.pend_transaction,0) = 0
//		and exists (select 1 from wip_comp_temp_wo c where b.work_order_id = c.work_order_id	)
//);
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: insert into pend class code table for wip comp close: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows       
//	
//	
////a.	Option for related assets
//
//insert into pend_related_asset
//(pend_related_asset_id, pend_trans_id, related_pend_trans_id, related_asset_type_id, co_retire)
//(select distinct new_pend_trans_id, orig_pend_trans_id,new_pend_trans_id,c.related_asset_option, b.co_retire
//from wip_comp_pending_trans a, related_asset_type b, wip_computation c
//where a.wip_computation_id = c.wip_computation_id
//and c.related_asset_option = b.related_type_id
//and nvl(c.related_asset_option,0) <> 0
//	and nvl(a.pend_transaction,0) = 0
//	and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
//);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: insert into pend related asset for wip comp close: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows      
//	
////8.	Mark Records as Pending Status
//
//update wip_comp_pending_trans a
//set pend_transaction = 100
//where nvl(a.pend_transaction,0) = 0
//	and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
//	and exists (select 1 from pend_transaction b where b.pend_trans_id = a.new_pend_trans_id);
//
//if sqlca.SQLCode < 0 then 
//	error_message =  ": ERROR: update wip_comp_pend_trans.pend_transaction: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows      
//
//update wip_comp_unit_calc aa
//set pend_transaction = 100
//where nvl(aa.pend_transaction,0) = 0
//	and exists (select 1 from wip_comp_temp_wo c where aa.work_order_id = c.work_order_id	)
//	and exists (select 1 from wip_comp_pending_trans a, pend_transaction b where b.pend_trans_id = a.new_pend_trans_id
//					and aa.wip_comp_summary_id = a.wip_comp_summary_id);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: update wip_comp_unit_calc.pend_transaction: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows       
//	
//
//update wip_comp_charges aa
//set unitized_status = 100
//where nvl(aa.unitized_status,0) = 0
//and exists (select 1 from wip_comp_temp_wo c where aa.work_order_id = c.work_order_id	)
//and exists (select 1 from wip_comp_pending_trans a, pend_transaction b where b.pend_trans_id = a.new_pend_trans_id
//				and aa.wip_comp_sum_unitize_id = a.wip_comp_summary_id);
//
//if sqlca.SQLCode < 0 then 
//	error_message = ": ERROR: update wip_comp_charges.unitized_status: " + sqlca.SQLErrText 
//	return error_message
//end if		
//num_nrows = sqlca.sqlnrows  
//
//		
////Call Extension Function
//ext_rtn = f_wip_comp_extension(-1,a_month_number, 'closing_end',a_wo_id,a_company_id)
//	
//if ext_rtn = -1 then
//	error_message = "Error Occured in f_wip_comp_extension for wip computation closing start" 
//	return error_message
//end if
//	
////Call Extensions:  ---- good to update not eligible flag or insert additional work order ids
//ext_rtn = 0
//ext_rtn = f_wip_comp_dynamic_ext(-1,a_month_number, 'closing-after-pending',a_wo_id,a_company_id)
//
//if ext_rtn = -1 then
//	error_message =  "Error Occured in f_wip_comp_dynamic_ext for wip computation = " + description 
//	return error_message
//end if 
//	
return ''
end function

public function integer of_wip_comp_closing101_check ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This function must be performed in a wo loop.
//1. determine if the wo has WIP Comp charges eligible for 101 closing.
//2. call the WIP Comp 101 closing code for the wo.
//3. if error returns, delete the non-WIP Comp pending 101 data for the wo.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
longlong num_nrows, i, wo_id, num_wos, of_rtn
string wo_list_sqls, of_wip_rtn_string, rtn_string
uo_ds_top ds_wos_list

ds_wos_list = create uo_ds_top

wo_list_sqls = "select distinct b.work_order_id " + &
" from wip_comp_charges a, UNITIZE_WO_LIST_TEMP b " + &
" where a.work_order_id = b.work_order_id  " + &
" and b.company_id = " + string(i_company_id)  + & 
" and b.error_msg is null " + & 
" and nvl(a.unitized_status,0) =  0 " + &
" and nvl(a.non_unitized_status,0) <> 100  " + &
" and a.month_number <= " + string(i_month_number)  + &
" and exists (select 1 from wip_computation j " + &
" 				where a.wip_computation_id = j.wip_computation_id " + &
" 				and nvl(j.close_to_asset,1) = 1 " + &
" 				and j.expenditure_type_id = 5)"
				
rtn_string = f_create_dynamic_ds(ds_wos_list, 'grid', wo_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_wos_list for wip comp close: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_wos = ds_wos_list.RowCount()

if num_wos > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": unitizing WIP Comp charges..."
	of_log()   
else
	destroy ds_wos_list 
	return 1
end if 

uo_wip_computations uo_wip
uo_wip = CREATE uo_wip_computations 

for i = 1 to num_wos

	wo_id = ds_wos_list.getitemnumber(i, 1)
	of_rtn = uo_wip.uf_wip_comp_closing(i_company_id , i_month_number ,'unitization', wo_id)
	
//	of_wip_rtn_string = of_wip_comp_closing101_wo(i_company_id, i_month_number, 'unitization', wo_id)	
//	if of_wip_rtn_string = '' or isnull(of_wip_rtn_string) then
//		//no error for the wo in the WIP Comp function
//	end if

	if of_rtn = -1 then
		update UNITIZE_WO_LIST_TEMP a
		set a.rtn_code = -1, a.error_msg = substrb('Unitization 189: Error processing WIP Comp Closing: '||:of_wip_rtn_string, 1, 2000)
		where a.company_id = :i_company_id
		and a.work_order_id = :wo_id
		and a.error_msg is null;  
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_TEMP for Unitization 189: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			destroy ds_wos_list  
			destroy uo_wip  
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
	end if 
	
next 

destroy ds_wos_list 
destroy uo_wip 
		
//Handle errors identified above
of_rtn = of_errors('WIPCOMP')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

return 1
end function

public function integer of_eligible_wos ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//generate the initial list of eligible work orders and insert into the UNITIZE_WO_LIST_TEMP global temp table.
//use the SQL filters found in the dw_wo_to_unitize_101 and dw_wo_to_unitize_101_late datawindows tradionally used.
//concurrency management is handled in of_prepare_wo_list() at the bottom of this function.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
string sqls_select_main, sqls_dw, late_month_number_str, final_select_sql,  insert_sqls, insert_error_msg
string sqls_select_where, sqls_single_wo, sqls_blankets, sqls_arc
longlong pos1, check_arc, num_nrows, of_rtn, month_mm

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": gathering eligible work order(s)..."
of_log() 

sqls_blankets = ''

//First clear out global temp table, which is specific to this session.
delete from UNITIZE_WO_LIST_TEMP;
	
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to delete from UNITIZE_WO_LIST_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

if i_single_wo = false then 
	
	if i_unitization_type = 'AUTO' then
		//add BLANKET or NON-BLANKET filters as needed
		if upper(i_closing_type) = 'BLANKETS' then
			sqls_blankets =  ' and work_order_account.closing_option_id > 5 ' + "~r~n"  	
		elseif upper(i_closing_type) = 'NON-BLANKETS' then
			sqls_blankets = ' and work_order_account.closing_option_id < 5 ' + "~r~n" 	
		else
			//no additional filter needed when the option is "All WOs"
			sqls_blankets = ''
		end if 
	end if 
	
	//add the ARC (auto review) sql for all work orders for the company if needed.
	//the main WHERE filters already has COMPANY_ID filter in the SQL.
	select count(*) into :check_arc from wo_arc_results, work_order_control
	where wo_arc_results.work_order_id = work_order_control.work_order_id and company_id = :i_company_id;
	 
	if check_arc > 0 then	
		sqls_arc = " AND NOT EXISTS( " + &
				"	SELECT 1 " + &
				"		FROM wo_arc_results waro " + &
				"		WHERE waro.arc_run_id = NVL(( " + &
				"		SELECT MAX (arc_run_id) " + &
				"		FROM wo_arc_results wari " + &
				"		WHERE waro.work_order_id = wari.work_order_id " + &
				"		AND waro.arc_rule_id = wari.arc_rule_id " + &
				"			) " + &
				"		,0) " + &
				"		AND (waro.pass_fail = 1 " + &
				"		AND NVL (waro.override, 0) = 0) " + &
				"		AND waro.work_order_id = work_order_control.work_order_id " + &
				"		AND EXISTS ( " + &
				"		SELECT wac.wo_status_id " + &
				"		, wac.arc_class_id " + &
				"		, warc.arc_rule_id " + &
				"		, wacco.closing_option_id " + &
				"		, wacwot.work_order_type_id " + &
				"		FROM wo_arc_class wac " + &
				"		JOIN wo_arc_rule_class warc " + &
				"		ON wac.arc_class_id = warc.arc_class_id " + &
				"		JOIN wo_arc_class_cl_opt wacco " + &
				"		ON wac.arc_class_id = wacco.arc_class_id " + &
				"		JOIN wo_arc_class_wot wacwot " + &
				"		ON wac.arc_class_id = wacwot.arc_class_id " + &
				"		WHERE wac.unitization = 1 " + &
				"		AND work_order_account.closing_option_id = wacco.closing_option_id " + &
				"		AND work_order_control.wo_status_id = wac.wo_status_id " + &
				"		AND work_order_control.work_order_type_id = wacwot.work_order_type_id " + &
				"		AND waro.arc_rule_id = warc.arc_rule_id)) "		
	end if
	
else 
	//single wo is being unitized.
	
	//add the WORK_ORDER_ID for single wo unitization 
	sqls_single_wo =  ' and work_order_control.work_order_id = ' + string(i_single_wo_id) + " ~r~n"   
	
	//add the ARC (auto review) sql if needed
	select count(*) into :check_arc from wo_arc_results, work_order_control
	where wo_arc_results.work_order_id = work_order_control.work_order_id 
	and work_order_control.company_id = :i_company_id 
	and work_order_control.work_order_id = :i_single_wo_id;
	 
	if check_arc > 0 then		 
		sqls_arc = " and NOT EXISTS( Select work_order_id, arc_run_id from wo_arc_results big " + "~r~n" + &
		" where arc_run_id = (select nvl(max(small.arc_run_id),0) from wo_arc_results small,  " + "~r~n" + &
		" wo_arc_run_control small2 where small.work_order_id = big.work_order_id " + "~r~n" + &
		" and small.arc_run_id = small2.arc_run_id and small.arc_class_id is not null) " + "~r~n" + &
		" and work_order_id = work_order_control.work_order_id " + "~r~n" + &
		" and exists (select 1 from wo_arc_results small, wo_arc_class class " + "~r~n" + &
						" where small.work_order_id = big.work_order_id " + "~r~n" + &
						" and small.arc_run_id = big.arc_run_id " + "~r~n" + &
						" and small.work_order_id = " + string(i_single_wo_id) + "~r~n" + &
						" and small.arc_class_id = class.arc_class_id " + "~r~n" + &
						" and class.unitization = 1 " + "~r~n" + &
						" and (small.pass_fail = 1 and nvl(small.override,0) = 0)) " + "~r~n" + &
		" group by work_order_id,arc_run_id ) " 
	end if

end if 

sqls_select_main = "SELECT " + "~r~n" + &
"   WORK_ORDER_CONTROL.WORK_ORDER_ID, " + "~r~n" + &
"   WORK_ORDER_CONTROL.WORK_ORDER_NUMBER, " + "~r~n" + &
"   WORK_ORDER_CONTROL.COMPANY_ID, " + "~r~n" + &
"   WORK_ORDER_CONTROL.BUS_SEGMENT_ID, " + "~r~n" + &
"   WORK_ORDER_CONTROL.MAJOR_LOCATION_ID, " + "~r~n" + &
"   WORK_ORDER_CONTROL.ASSET_LOCATION_ID, " + "~r~n" + &
"   WORK_ORDER_CONTROL.WO_STATUS_ID, " + "~r~n" + &
"   WORK_ORDER_CONTROL.IN_SERVICE_DATE, " + "~r~n" + &  
"   WORK_ORDER_CONTROL.COMPLETION_DATE, " + "~r~n" + & 
"   WORK_ORDER_CONTROL.LATE_CHG_WAIT_PERIOD, " + "~r~n" + &
"   WORK_ORDER_CONTROL.FUNDING_WO_ID, " + "~r~n" + &
"   nvl(WORK_ORDER_CONTROL.DESCRIPTION, 'NONE') description, " + "~r~n" + &
"   nvl(substrb(WORK_ORDER_CONTROL.LONG_DESCRIPTION,1,254), 'NONE')  long_description, " + "~r~n" + &
"   WORK_ORDER_ACCOUNT.CLOSING_OPTION_ID, " + "~r~n" + &
"   WORK_ORDER_ACCOUNT.CWIP_GL_ACCOUNT, " + "~r~n" + &
"   WORK_ORDER_ACCOUNT.NON_UNITIZED_GL_ACCOUNT,  " + "~r~n" + &
"   WORK_ORDER_ACCOUNT.UNITIZED_GL_ACCOUNT, " + "~r~n" + &   
"   WORK_ORDER_ACCOUNT.ALLOC_METHOD_TYPE_ID, " + "~r~n" + &
"   decode( nvl( WORK_ORDER_ACCOUNT.RWIP_TYPE_ID, WORK_ORDER_TYPE.RWIP_TYPE_ID), null, 'yes', -1, 'yes', 'no')  rwip_yes_no, " + "~r~n" + &
"   nvl(WORK_ORDER_ACCOUNT.EST_UNIT_ITEM_OPTION,0) est_unit_item_option, " + "~r~n" + &
"   nvl(WORK_ORDER_ACCOUNT.UNIT_ITEM_FROM_ESTIMATE,0) unit_item_from_estimate, " + "~r~n" + &
"   nvl(WORK_ORDER_ACCOUNT.ACCRUAL_TYPE_ID,1), " + "~r~n" + &
"   WORK_ORDER_ACCOUNT.TOLERANCE_ID, " + "~r~n" + &
"   nvl(WORK_ORDER_ACCOUNT.UNITIZE_BY_ACCOUNT,0), " + "~r~n" + &
"   WORK_ORDER_ACCOUNT.FUNC_CLASS_ID, " + "~r~n" + &
"   nvl(WORK_ORDER_TYPE.REPLACEMENT_PERCENTAGE,0), " + "~r~n" + &
"   '" + i_unitization_type + "' ~r~n" + &
" FROM WORK_ORDER_ACCOUNT,  " + "~r~n" + &
"   WORK_ORDER_CONTROL, " + "~r~n" + &
"   WORK_ORDER_TYPE "  


//Need to get the where clause filters from dw_wo_to_unitize_101 or dw_wo_to_unitize_101_late
uo_ds_top ds_wo_to_unitize_101
ds_wo_to_unitize_101 = CREATE uo_ds_top
choose case i_unitization_type 
	case 'AUTO', 'AUTO WO'
		ds_wo_to_unitize_101.DataObject = "dw_wo_to_unitize_101"
	case 'AUTO LATE', 'AUTO LATE WO'
		ds_wo_to_unitize_101.DataObject = "dw_wo_to_unitize_101_late" 
	case else
		//error
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: invalid unitization type: " + i_unitization_type
		i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
		of_log() 
		rollback;		 
		return -1
end choose 
ds_wo_to_unitize_101.SetTransObject(sqlca) 	
sqls_dw = upper(ds_wo_to_unitize_101.getsqlselect())  

DESTROY ds_wo_to_unitize_101

pos1 = pos(sqls_dw, 'WHERE ')	
sqls_select_where = mid(sqls_dw, pos1)

//replace dw input arguements
sqls_select_where = f_replace_string(sqls_select_where, ':COMPANY_ID', string(i_company_id), 'all')

if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then
	sqls_select_where = f_replace_string(sqls_select_where, ':MONTH', " '"+ i_month_yr_string + "' ", 'all')
else 
	//late close is the only "else" that can happen in this "if" section
	late_month_number_str = string(i_late_month_number)	
	sqls_select_where = f_replace_string(sqls_select_where, ':LATE_MO_NUM', " "+ late_month_number_str + " ", 'all')
end if 

//combined each SQL string into the final select statement
if isnull(sqls_blankets) then sqls_blankets = ''
if isnull(sqls_arc) then sqls_arc = ''
if isnull(sqls_single_wo) then sqls_single_wo = ''

final_select_sql = trim(sqls_select_main + sqls_select_where + sqls_blankets + sqls_arc + sqls_single_wo)


////Info:  CLOSING OPTIONS included for regular Auto Unitization:  dw_wo_to_unitize_101
//
//      "WORK_ORDER_ACCOUNT"."CLOSING_OPTION_ID" in (2,4) 
//AND "WORK_ORDER_CONTROL"."WO_STATUS_ID" in (4,5) 
//AND "WORK_ORDER_CONTROL"."COMPLETION_DATE" is not null   
//AND add_months(to_date(to_char(completion_date,'mm/yyyy'),'mm/yyyy'), nvl(work_order_control.late_chg_wait_period,0)) <= :month
//or
//      "WORK_ORDER_ACCOUNT"."CLOSING_OPTION_ID" in (6,7,8) 
////Info:  CLOSING OPTIONS included for regular Auto Unitization:  dw_wo_to_unitize_101

////////////TESTING: manual debug for specific wo_ids ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////final_select_sql = final_select_sql + ' and work_order_control.work_order_id in ( 409796759 ,  576279823)'

if final_select_sql = '' or isnull(final_select_sql) then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: Unable to construct wo eligibility requirements (SQL=null)."
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
	of_log() 
	rollback;
	return -1
end if 

//Save the list of eligible work orders for the company
insert_sqls = "INSERT INTO UNITIZE_WO_LIST_TEMP " + "~r~n" + &
" (WORK_ORDER_ID, WORK_ORDER_NUMBER, COMPANY_ID, BUS_SEGMENT_ID, MAJOR_LOCATION_ID, ASSET_LOCATION_ID, " + "~r~n" + &
" WO_STATUS_ID, IN_SERVICE_DATE, COMPLETION_DATE, LATE_CHG_WAIT_PERIOD, FUNDING_WO_ID, DESCRIPTION, LONG_DESCRIPTION, CLOSING_OPTION_ID, " + "~r~n" + &
" CWIP_GL_ACCOUNT, NON_UNITIZED_GL_ACCOUNT, UNITIZED_GL_ACCOUNT, ALLOC_METHOD_TYPE_ID, RWIP_YES_NO, " + "~r~n" + &
" EST_UNIT_ITEM_OPTION, UNIT_ITEM_FROM_ESTIMATE, ACCRUAL_TYPE_ID, TOLERANCE_ID, UNITIZE_BY_ACCOUNT, WOA_FUNC_CLASS_ID, REPLACEMENT_PERCENTAGE, UNITIZATION_TYPE " + "~r~n" + &
" ) " + final_select_sql
  
execute immediate :insert_sqls;
		
if sqlca.SQLCode < 0 then 
	insert_error_msg = sqlca.SQLErrText	
	rollback;	
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_WO_LIST_TEMP insert: " + insert_error_msg
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id)
	of_log() 
	
	if ib_called_from_w_wo_control and i_single_wo = false then
		//able to display full insert SQL syntax in f_wo_status_box, so show full insert syntax
		f_wo_status_box(i_sbox_title, "")
		f_wo_status_box(i_sbox_title, ": ERROR: UNITIZE_WO_LIST_TEMP insert: " + insert_error_msg) 
		f_wo_status_box(i_sbox_title, "")
		f_wo_status_box(i_sbox_title, "FAILED SQL insert string = " + insert_sqls ) 
		f_wo_status_box(i_sbox_title, "")
	end if  	
	
	//the insert into PP_PROCESSES_MESSAGE in of_log() will truncate the msg string to 2000. 
	//the full insert syntax is way over 2000.   the where clause will likely be the cause of the SQL error, so save the where clause for Online Logs.	 
	if i_process_id > 0 then
		f_pp_msgs("")
		f_pp_msgs( mid("FAILED SQL where string = " +  sqls_select_where + sqls_blankets + sqls_arc + sqls_single_wo, 1, 2000) )
	end if 
		 	
	return -1
end if		
num_nrows = sqlca.sqlnrows

if num_nrows = 0 then
	return 0 
end if 

//remove ineligible work orders for the company base on quarterly/annual close options
month_mm = month(date(i_month_yr))
choose case month_mm
	case 1, 2, 4, 5, 7, 8, 10, 11
		//need to remove quarterly and annual close work orders in these non-quarter and non-annual ending months
		delete from UNITIZE_WO_LIST_TEMP
		where company_id = :i_company_id
		and closing_option_id in (7,8);
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] = ": ERROR: UNITIZE_WO_LIST_TEMP delete for closing options 7 and 8: " + sqlca.SQLErrText
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
	case 3,6,9
		//need to remove annual close work orders in the quarter ending months
		delete from UNITIZE_WO_LIST_TEMP
		where company_id = :i_company_id
		and closing_option_id = 8;		
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] = ": ERROR: UNITIZE_WO_LIST_TEMP delete for closing option 8: " + sqlca.SQLErrText
			i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 	
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
	case else
		//keep all work orders for Decemeber
		
end choose

//Handle delete/inserts needed to avoid concurrent Unitization of the same work order(s)
of_rtn = of_prepare_wo_list()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

//Save to real table for troubleshooting
of_rtn = of_archive_wo_list_temp()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 


return 1 
end function

public function integer of_validate_correct_unitization_config (longlong a_process_id, boolean a_visual);///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This is a system level configuration validation function that needs to be called prior to starting unitization for any company or companies.
//These validation checks should NOT be run for each company in a company process loop.
//This function should be called PRIOR to calling of__main() and of__main_late().
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
string auto_unitization_is_on_sc, allo_methods_sqls, invalid_alloc_type, invalid_alloc_desc, rtn_string
longlong alloc_method_type_id, num_methods, i, default_alloc_id, max_priority, check_cnt, min_priority, max_priority_book, max_priority_tax, num_nrows, invalid_alloc_id
boolean insert_done
uo_ds_top ds_alloc_methods

i_visual = a_visual

//Check the process_id for Online Logs for Unitization of more than 1 work order
if i_single_wo = false then
	if isnull(a_process_id) or a_process_id = 0 then
		i_msg[] = i_null_arrary[]
		i_msg[1] = ": ERROR: There is no process_id defined for Automatic Unitization."  
		i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization."  
		of_log()
		return -1
	end if 
end if 

i_process_id = a_process_id

i_msg[] = i_null_arrary[] 
i_msg[1] = ": Checking Unitization Configuration..."
of_log() 


//make sure the gl_je_code is configured
select standard_journal_entries.gl_je_code, standard_journal_entries.je_id
into :i_gl_je_code, :i_je_id
from standard_journal_entries, gl_je_control
where standard_journal_entries.je_id = gl_je_control.je_id  
and upper(ltrim(rtrim(gl_je_control.process_id))) = 'AUTOMATIC 101';

if i_gl_je_code = "" or isnull(i_gl_je_code) then 
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: There is no gl_je_code defined in gl_je_control for Automatic 101."  
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization."  
	of_log()
	return -1
end if 

//make sure this system control is set to Yes.  This is not a non-company specific control.
auto_unitization_is_on_sc =  f_pp_system_control_company('AUTOMATIC UNITIZATION IS TURNED ON', -1)

if auto_unitization_is_on_sc = '' or lower(auto_unitization_is_on_sc) = "no" then
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: Unable to starting Auto Unitization: AUTOMATIC UNITIZATION IS TURNED ON system control is not set to Yes."  
	of_log()
	return -1
end if


//Verify at least one Unit Allocation Basis setup
default_alloc_id = 0
select min(alloc_id) into :default_alloc_id from unit_allocation;

if default_alloc_id = 0 or isnull(default_alloc_id) then
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: At least one Unit Allocation Basis must be setup."  
	i_msg[2] = ":            Please go to the Allocation Maint config window and setup an Allocation Basis."  
	of_log()
	return -1
end if 

//Verify all alloc_types are the 3 base options and none other setup by mistake.
invalid_alloc_id = 0
select min(alloc_id) into :invalid_alloc_id
from UNIT_ALLOCATION
where trim(lower(alloc_type)) not in ('estimates' , 'actuals', 'standards', 'none');

if invalid_alloc_id > 0 then	
	select description, alloc_type 
	into :invalid_alloc_desc, :invalid_alloc_type
	from UNIT_ALLOCATION where alloc_id = :invalid_alloc_id;

	i_msg[] = i_null_arrary[]
	i_msg[1] = ': ERROR: Invalid Alloc Type = "' + invalid_alloc_type + '" found '
	i_msg[2] = ':            for Allocation Basis = "' + invalid_alloc_desc + '"!'  
	i_msg[3] = ':            Please go to the Allocation Maint config window and correct the Allocation Basis setup.'  
	of_log()
	return -1
end if 

//Verify all priorities are greater than 0 and not blank. 
max_priority = 0
select min(nvl(priority,0)), max(nvl(priority,0)) into :min_priority, :max_priority
from unit_alloc_meth_control
where priority is not null;

if isnull(min_priority) then
	min_priority = 0
end if 

if isnull(max_priority) then
	max_priority = 0
end if 

if min_priority < 0 or max_priority < 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =   ": ERROR: Allocation Priorities cannot be negative."  
	i_msg[2] =   ":            Please go to the Allocation Maint config window and update the Priority column."  
	of_log()
	return -1
end if 

if min_priority = 0 or max_priority = 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] =   ": ERROR: Allocation Priorities cannot be 0."  
	i_msg[2] =   ":            Please go to the Allocation Maint config window and update the Priority column."  
	of_log()
	return -1
end if 

////This verification is not needed in new code.  New code will do round plugs by charge_group_id, not priority.
////Verify the tax only CHARGE_TYPE_IDs have different PRIORITY. 
//shared_priority = 0
//select count(1) into :shared_priority
//from (
//select priority
//from unit_alloc_meth_control
//where charge_type_id in (select charge_type_id from charge_type where processing_type_id not in (1,5))
//intersect 
//select priority
//from unit_alloc_meth_control
//where charge_type_id in (select charge_type_id from charge_type where processing_type_id = 5)
//);
//
//if shared_priority > 0 then 
//	i_msg[] = i_null_arrary[]
//	i_msg[1] =   ": ERROR: Allocation Priorities need to be different for Tax Charge Types vs. Book Charge Types."  
//	i_msg[2] =   ":            Please go to the Allocation Maint config window and update the Priority column."  
//	of_log()
//	return -1
//end if

//Verify all CHARGE_TYPE_IDs are configured in the Unitization Allocation setup for each Alloc Method.  
//If missing CHARGE_TYPE_IDs are identified, automatically insert the missing configuration data for each Alloc Method assigned to distinctly different PRIORITY.

//First get the right priorities to use for book charge types vs tax charge types
max_priority_book = 0
select max(nvl(priority,0)) into :max_priority_book
from unit_alloc_meth_control
where charge_type_id in (select charge_type_id from charge_type where processing_type_id not in (1,5))
and priority is not null;

max_priority_tax = 0
select max(nvl(priority,0)) into :max_priority_tax
from unit_alloc_meth_control
where charge_type_id in (select charge_type_id from charge_type where processing_type_id = 5)
and priority is not null;

ds_alloc_methods = create uo_ds_top
allo_methods_sqls = "select alloc_method_type_id from unit_alloc_method"
rtn_string = f_create_dynamic_ds(ds_alloc_methods, 'grid', allo_methods_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_alloc_methods for unitization config check: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

insert_done = false
num_methods = ds_alloc_methods.RowCount()

for i = 1 to num_methods
	alloc_method_type_id = ds_alloc_methods.getitemnumber(i, 1)	
	
	//insert any missing book charge types
	insert into unit_alloc_meth_control (alloc_method_type_id, charge_type_id, alloc_id, limit_for_auto_allo, priority)
	select :alloc_method_type_id, charge_type_id, null, 0, :max_priority_book
	from (
		select charge_type_id
		from charge_type where processing_type_id not in (1, 5) 
		minus
		select charge_type_id
		from unit_alloc_meth_control 
		where alloc_method_type_id = :alloc_method_type_id
		);

	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: unit_alloc_meth_control insert for book: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		destroy ds_alloc_methods
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  
	
	if num_nrows > 0 then
		insert_done = true
	end if 
	
	//insert any missing tax charge types
	insert into unit_alloc_meth_control (alloc_method_type_id, charge_type_id, alloc_id, limit_for_auto_allo, priority)
	select :alloc_method_type_id, charge_type_id, null, 0, :max_priority_tax
	from (
		select charge_type_id
		from charge_type where processing_type_id = 5 
		minus
		select charge_type_id
		from unit_alloc_meth_control 
		where alloc_method_type_id = :alloc_method_type_id
		);

	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: unit_alloc_meth_control insert for tax: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		destroy ds_alloc_methods
		return -1
	end if		
	num_nrows = sqlca.sqlnrows   
	
	if num_nrows > 0 then
		insert_done = true
	end if 

next 

//maint 43357:  save the inserted data, so that it is easy to see which missing data was added by the alloc_id column, which will show blank on the window display
if insert_done then
	commit;
end if

if num_methods = 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: At least one Unit Allocation Method must be setup."
	i_msg[2] = ":            Please go to the Allocation Maint config window and setup an Allocation Method."    
	of_log()
	destroy ds_alloc_methods
	return -1
end if 

//ds_alloc_methods is no longer needed
destroy ds_alloc_methods

//Force the user to select the Alloc Id for charge types that are missing this assignment
//The inserts above will automatically add the missing charge types, but the user should determine the Alloc Basis for those charge types.
select count(1) into :check_cnt
from unit_alloc_meth_control 
where alloc_id is null
and charge_type_id in (select charge_type_id from charge_type where processing_type_id <> 1);

if check_cnt > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: Allocation configuration for Charge Types have missing Alloc Basis assignments.  Please update the missing information."
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//Default the PRIORITIES that are blank
update unit_alloc_meth_control
set priority = :max_priority_book
where priority is null 
and charge_type_id in (select charge_type_id from charge_type where processing_type_id not in (1,5));

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: unit_alloc_meth_control update priority book: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

update unit_alloc_meth_control
set priority = :max_priority_tax
where priority is null 
and charge_type_id in (select charge_type_id from charge_type where processing_type_id = 5);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: unit_alloc_meth_control update priority tax: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1
end function

public function integer of_validate_pending_back_to_cwip ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Add all the basis dollars saved in the UNITIZE_PEND_TRANS_TEMP and compare to all AMOUNT in CWIP_CHARGE for additions.
//Add the specific RWIP columns to compare to CWIP_CHARGE for RWIP.
//Use the UNITIZE_ALLOC_ERRORS_TEMP table to identify balancing errors.
//Need to be forgiving for pennies on RWIP balancing, because old code did not handle rounding.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn, check_count

if lower(i_pend_balance) = 'yes' then

	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": balancing unitized Additions..."
	of_log() 
	
	//Balance Addition pending dollars.
	if i_elig_add_wos > 0 then
		//for this validation, need to use the BASIS columns.  do not use POSTING_AMOUNT, because POSTING_AMOUNT is manipulated by SET_OF_BOOKS indicators.
		insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
		select distinct a.company_id, a.work_order_id,  
		substrb('Unitization 128: 101 Pend Trans Basis Amount '||to_char(unitized_amount)||' does not balance with 107/106 Charge Amount '||to_char(charge_amount)||' *Reset Unitization and Try Again*', 1, 2000)
		from UNITIZE_WO_LIST_TEMP a, ( 
											select charges.work_order_id, charge_amount, unitized_amount
											from 
												(select a.work_order_id, sum(a.amount) charge_amount 
												from cwip_charge a, UNITIZE_WO_LIST_TEMP b, charge_group_control c, work_order_charge_group d, work_order_account e
												where a.work_order_id = b.work_order_id
												and b.work_order_id = e.work_order_id
												and b.company_id = :i_company_id
												and a.expenditure_type_id = 1   
												and b.has_elig_adds = 1
												and a.month_number <= :i_month_number
												and a.unit_closed_month_number is null  
												and d.charge_id = a.charge_id 
												and d.work_order_id = c.work_order_id
												and d.charge_group_id = c.charge_group_id
												and c.expenditure_type_id = a.expenditure_type_id  /*validate the expenditure types match*/
												and c.charge_type_id = a.charge_type_id  /*validate the charge types match*/
												and c.batch_unit_item_id is null 
												and 
													  (e.CWIP_GL_ACCOUNT = a.GL_ACCOUNT_ID OR
														e.NON_UNITIZED_GL_ACCOUNT = a.GL_ACCOUNT_ID OR 
														a.GL_ACCOUNT_ID in (1,2) 
													  )
							  
												group by a.work_order_id
												) charges,
												
												(select work_order_id, sum(nvl(basis_1,0) + nvl(basis_2,0) + nvl(basis_3,0) + nvl(basis_4,0) +  nvl(basis_5,0) + 
												nvl(basis_6,0) + nvl(basis_7,0) + nvl(basis_8,0)  +  nvl(basis_9,0) + nvl(basis_10,0)  + 
												nvl(basis_11,0) + nvl(basis_12,0) + nvl(basis_13,0) + nvl(basis_14,0)  +  nvl(basis_15,0) + 
												nvl(basis_16,0) + nvl(basis_17,0) + nvl(basis_18,0)  + nvl(basis_19,0) + nvl(basis_20,0)  + 
												nvl(basis_21,0) + nvl(basis_22,0) + nvl(basis_23, 0) + nvl(basis_24,0)  +  nvl(basis_25, 0) + 
												nvl(basis_26,0) + nvl(basis_27,0) + nvl(basis_28,0)  + nvl(basis_29,0) + nvl(basis_30,0)  + 
												nvl(basis_31,0) + nvl(basis_32,0)  +  nvl(basis_33, 0) + nvl(basis_34,0) + nvl(basis_35, 0) + 
												nvl(basis_36,0) + nvl(basis_37,0) + nvl(basis_38,0)  +   nvl(basis_39,0) + nvl(basis_40,0)  + 
												nvl(basis_41,0) + nvl(basis_42,0)  +  nvl(basis_43, 0) + nvl(basis_44,0) + nvl(basis_45, 0) + 
												nvl(basis_46,0) + nvl(basis_47,0) + nvl(basis_48,0)  +   nvl(basis_49,0) + nvl(basis_50,0)  + 
												nvl(basis_51,0) + nvl(basis_52,0)  +  nvl(basis_53, 0) + nvl(basis_54,0) + nvl(basis_55, 0) + 
												nvl(basis_56,0) + nvl(basis_57,0) + nvl(basis_58,0)  +   nvl(basis_59,0) + nvl(basis_60,0) +
												nvl(basis_61,0) + nvl(basis_62,0)  +  nvl(basis_63, 0) + nvl(basis_64,0) + nvl(basis_65, 0) + 
												nvl(basis_66,0) + nvl(basis_67,0) + nvl(basis_68,0)  +   nvl(basis_69,0) + nvl(basis_70,0)) unitized_amount
												from UNITIZE_PEND_TRANS_TEMP
												where ferc_activity_code = 1
												group by work_order_id 
												) pend
											
											where charges.work_order_id = pend.work_order_id
											and charges.charge_amount <> pend.unitized_amount 
											) balance
		where a.work_order_id = balance.work_order_id 
		and a.company_id = :i_company_id;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP 107/106 bal check: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
		
		if num_nrows > 0 then
			//Handle errors identified above.  
			of_rtn = of_errors('ALLO')
			if of_rtn <> 1 then
				//errors are handled inside function call
				return -1
			end if  
		end if
	end if //if i_elig_add_wos > 0

	//Balance RWIP pending dollars.
	if i_elig_rwip_wos > 0 then
		//Note: old 101 code did not validate balancing and did not plug for rounding diffs, if there is old unitized RWIP data going through new 101 code now, allocation issues will be caught now, 
		//but the issue is likely caused by the old 101 code, and the wo needs to have RESET UNITIZATION done so that it goes through the new 101 code from start to finish.
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": balancing unitized RWIP..."
		of_log() 
		
		//cor
		insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
		select distinct a.company_id, a.work_order_id,  
		substrb('Unitization 129:  COR Pend Trans Amount '||to_char(unitized_amount)||' does not balance with COR Charge Amount '||to_char(charge_amount)||' *Reset Unitization and Try Again*' , 1, 2000)
		from UNITIZE_WO_LIST_TEMP a, ( 
											select charges.work_order_id, charge_amount, unitized_amount
											from 
												(select a.work_order_id, sum(a.amount)*-1 charge_amount 
												from cwip_charge a, UNITIZE_WO_LIST_TEMP b, charge_group_control c, work_order_charge_group d, work_order_account e
												where a.work_order_id = b.work_order_id
												and b.work_order_id = e.work_order_id
												and b.company_id = :i_company_id
												and b.rwip_yes_no = 'yes'
												and a.expenditure_type_id = 2   
												and b.has_elig_rwip = 1
												and a.month_number <= :i_month_number
												and a.unit_closed_month_number is null  
												and d.charge_id = a.charge_id 
												and d.work_order_id = c.work_order_id
												and d.charge_group_id = c.charge_group_id
												and c.expenditure_type_id = a.expenditure_type_id  /*validate the expenditure types match*/
												and c.charge_type_id = a.charge_type_id /*validate the charge types match*/
												and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id not in (1,2,7,8))
												and c.batch_unit_item_id is null 
												and e.REMOVAL_GL_ACCOUNT = a.GL_ACCOUNT_ID  
												group by a.work_order_id
												) charges,
												
												(select work_order_id, sum(nvl(cost_of_removal,0)) unitized_amount
												from UNITIZE_PEND_TRANS_TEMP
												where ferc_activity_code = 2
												group by work_order_id 
												) pend
											
											where charges.work_order_id = pend.work_order_id
											and charges.charge_amount <> pend.unitized_amount /*Maint 38310: old code did not validate balancing and did not plug for rounding diffs*/
											) balance
		where a.work_order_id = balance.work_order_id 
		and a.company_id = :i_company_id;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP cor bal check: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
		
		if num_nrows > 0 then
			//Handle errors identified above.  
			of_rtn = of_errors('ALLO')
			if of_rtn <> 1 then
				//errors are handled inside function call
				return -1
			end if  
		end if
		
		//salvage_cash 
		insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id,	error_msg)
		select distinct a.company_id, a.work_order_id, 
		substrb('Unitization 130:  Salvage Cash Pend Trans Amount '||to_char(unitized_amount)||' does not balance with Salvage Cash Charge Amount '||to_char(charge_amount)||' *Reset Unitization and Try Again*' , 1, 2000)
		from UNITIZE_WO_LIST_TEMP a, ( 
											select charges.work_order_id, charge_amount, unitized_amount
											from 
												(select a.work_order_id, sum(a.amount)*-1 charge_amount 
												from cwip_charge a, UNITIZE_WO_LIST_TEMP b, charge_group_control c, work_order_charge_group d, work_order_account e
												where a.work_order_id = b.work_order_id
												and b.work_order_id = e.work_order_id
												and b.company_id = :i_company_id
												and b.rwip_yes_no = 'yes'
												and a.expenditure_type_id = 2   
												and b.has_elig_rwip = 1
												and a.month_number <= :i_month_number
												and a.unit_closed_month_number is null  
												and d.charge_id = a.charge_id 
												and d.work_order_id = c.work_order_id
												and d.charge_group_id = c.charge_group_id
												and c.expenditure_type_id = a.expenditure_type_id  /*validate the expenditure types match*/
												and c.charge_type_id = a.charge_type_id /*validate the charge types match*/
												and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id = 2)
												and c.batch_unit_item_id is null 
												and e.SALVAGE_GL_ACCOUNT = a.GL_ACCOUNT_ID  
												group by a.work_order_id
												) charges,
												
												(select work_order_id, sum(nvl(salvage_cash,0)) unitized_amount
												from UNITIZE_PEND_TRANS_TEMP
												where ferc_activity_code = 2
												group by work_order_id 
												) pend
											
											where charges.work_order_id = pend.work_order_id
											and charges.charge_amount <> pend.unitized_amount /*Maint 38310: old code did not validate balancing and did not plug for rounding diffs*/
											) balance
		where a.work_order_id = balance.work_order_id 
		and a.company_id = :i_company_id;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP salvage_cash bal check: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
		
		if num_nrows > 0 then
			//Handle errors identified above.  
			of_rtn = of_errors('ALLO')
			if of_rtn <> 1 then
				//errors are handled inside function call
				return -1
			end if  
		end if
		
		//salvage_returns 
		insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
		select distinct a.company_id, a.work_order_id, 
		substrb('Unitization 131:  Salvage Returns Pend Trans Amount '||to_char(unitized_amount)||' does not balance with Salvage Returns Charge Amount '||to_char(charge_amount)||' *Reset Unitization and Try Again*' , 1, 2000)
		from UNITIZE_WO_LIST_TEMP a, ( 
											select charges.work_order_id, charge_amount, unitized_amount
											from 
												(select a.work_order_id, sum(a.amount)*-1 charge_amount 
												from cwip_charge a, UNITIZE_WO_LIST_TEMP b, charge_group_control c, work_order_charge_group d, work_order_account e
												where a.work_order_id = b.work_order_id
												and b.work_order_id = e.work_order_id
												and b.company_id = :i_company_id
												and b.rwip_yes_no = 'yes'
												and a.expenditure_type_id = 2   
												and b.has_elig_rwip = 1
												and a.month_number <= :i_month_number
												and a.unit_closed_month_number is null  
												and d.charge_id = a.charge_id 
												and d.work_order_id = c.work_order_id
												and d.charge_group_id = c.charge_group_id
												and c.expenditure_type_id = a.expenditure_type_id  /*validate the expenditure types match*/
												and c.charge_type_id = a.charge_type_id /*validate the charge types match*/
												and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id = 7)
												and c.batch_unit_item_id is null 
												and e.SALVAGE_GL_ACCOUNT = a.GL_ACCOUNT_ID  
												group by a.work_order_id
												) charges,
												
												(select work_order_id, sum(nvl(salvage_returns,0)) unitized_amount
												from UNITIZE_PEND_TRANS_TEMP
												where ferc_activity_code = 2
												group by work_order_id 
												) pend
											
											where charges.work_order_id = pend.work_order_id
											and charges.charge_amount <> pend.unitized_amount /*Maint 38310: old code did not validate balancing and did not plug for rounding diffs*/
											) balance
		where a.work_order_id = balance.work_order_id 
		and a.company_id = :i_company_id;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP salvage_returns bal check: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows 
		
		if num_nrows > 0 then
			//Handle errors identified above.  
			of_rtn = of_errors('ALLO')
			if of_rtn <> 1 then
				//errors are handled inside function call
				return -1
			end if  
		end if
		
		//reserve_credits 
		insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
		select distinct a.company_id, a.work_order_id,  
		substrb('Unitization 132:  Reserve Credits Pend Trans Amount '||to_char(unitized_amount)||' does not balance with Reserve Credits Charge Amount '||to_char(charge_amount)||' *Reset Unitization and Try Again*' , 1, 2000)
		from UNITIZE_WO_LIST_TEMP a, ( 
											select charges.work_order_id, charge_amount, unitized_amount
											from 
												(select a.work_order_id, sum(a.amount)*-1 charge_amount 
												from cwip_charge a, UNITIZE_WO_LIST_TEMP b, charge_group_control c, work_order_charge_group d, work_order_account e
												where a.work_order_id = b.work_order_id
												and b.work_order_id = e.work_order_id
												and b.company_id = :i_company_id
												and b.rwip_yes_no = 'yes'
												and a.expenditure_type_id = 2   
												and b.has_elig_rwip = 1
												and a.month_number <= :i_month_number
												and a.unit_closed_month_number is null  
												and d.charge_id = a.charge_id 
												and d.work_order_id = c.work_order_id
												and d.charge_group_id = c.charge_group_id
												and c.expenditure_type_id = a.expenditure_type_id  /*validate the expenditure types match*/
												and c.charge_type_id = a.charge_type_id /*validate the charge types match*/
												and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id = 8)
												and c.batch_unit_item_id is null 
												and 
													  (e.REMOVAL_GL_ACCOUNT = a.GL_ACCOUNT_ID OR
														e.SALVAGE_GL_ACCOUNT = a.GL_ACCOUNT_ID  
													  )
												group by a.work_order_id
												) charges,
												
												(select work_order_id, sum(nvl(reserve_credits,0)) unitized_amount
												from UNITIZE_PEND_TRANS_TEMP
												where ferc_activity_code = 2
												group by work_order_id 
												) pend
											
											where charges.work_order_id = pend.work_order_id
											and charges.charge_amount <> pend.unitized_amount /*Maint 38310: old code did not validate balancing and did not plug for rounding diffs*/
											) balance
		where a.work_order_id = balance.work_order_id 
		and a.company_id = :i_company_id;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP reserve_credits bal check: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows
		
		if num_nrows > 0 then
			//Handle errors identified above.  
			of_rtn = of_errors('ALLO')
			if of_rtn <> 1 then
				//errors are handled inside function call
				return -1
			end if  
		end if
	end if // if i_elig_rwip_wos > 0
	
end if //i_pend_balance = yes

return 1
end function

public function integer of_validate_cgc_back_to_cwip ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Validate the CHARGE_TYPE_ID for the charge groups that were made in a prior unitization run and not yet finished unitization.
//
//Before new charges are grouped and prepared for the Unitization processing, the existing charge groups are validated in the of_validate_cgc_back_to_cwip() function in order to avoid errors later in the closing.  
//Specifically, it has been known to occur where SQL scripts are used to "zap" the CWIP_CHARGE table to change the CHARGE_TYPE_ID of charges as a correction effort, 
//     which is a bad thing to do if those same charges have already been included in the Unitization processing of a prior Unitization run.  
//Work orders are allowed to go through the Unitization processing as many times as needed prior to final close to the CPR, 
//     which is why there could be prior made charge group already on the work order.  
//To catch this timing error, a data validation was created to verify the CHARGE_TYPE_ID on the CHARGE_GROUP_CONTROL table matches the CHARGE_TYPE_ID on the CWIP_CHARGE table of 
//      charge groups that have been included for Unitization and not yet sent to Pending and subsequently closed to the CPR.  
//Work orders that are identified as having this mismatch in the charge data will get flagged with an error message and deleted from the UNITIZE_WO_LIST_TEMP and UNITIZE_WO_LIST tables.
//
//NOTE:  This validation is time consuming.  So if the system control, Auto 101 -  Balance Pending Trans, is set to Yes, then this validation will not occur inside the of_validate_cgc_back_to_cwip() function.  
//           Instead, the unitized dollar validation that gets called after Unitization pending transactions are successfully created will detect the same types of data issues between 
//               CWIP_CHARGE and CHARGE_GROUP_CONTROL, if any, inside the of_validate_pending_back_to_cwip() function.  
//           If the control value is set to No, then the data validation will occur inside the of_validate_cgc_back_to_cwip() function.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

if lower(i_pend_balance) = 'yes' then
	//save time by not validating the charge to charge group data here.
	//later after pending transactions are made, the of_validate_pending_back_to_cwip() function will 
	//     use "i_pend_balance" = "yes" to perform the dollar balancing to catch bad charge data.
	//this validation is slow to run, so avoid double balancing since the dollar balancing is going to get done later.
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": validating charge types..."
of_log() 

/////Use different comparison.  This one will only catch differences with the CHARGE_TYPE_ID in the event that someone zaps the CWIP_CHARGE or CHARGE_GROUP_CONTROL table's CHARGE_TYPE_ID columns.
//UPDATE UNITIZE_WO_LIST_TEMP a
//SET a.rtn_code = -1, a.error_msg = 'Unitization 120: Charge Types on Charge_Group_Control do not agree to CWIP_Charge. *Reset Unitization and Try Again*'
//where a.company_id = :i_company_id
//and a.error_msg is null
//and exists (select 1 
//				from work_order_charge_group b, cwip_charge c, charge_group_control d
//				where a.work_order_id = b.work_order_id
//				and b.work_order_id = c.work_order_id
//				and b.charge_id = c.charge_id
//				and b.work_order_id = d.work_order_id
//				and b.charge_group_id = d.charge_group_id 
//				and c.charge_type_id <> d.charge_type_id
//				and d.batch_unit_item_id is null 
//				);

//This comparison includes a data comparison of the CWIP to CGC data via amount, unit_closed_month_number, and gl_account_id values.
UPDATE UNITIZE_WO_LIST_TEMP a
SET a.rtn_code = -1, a.error_msg = 'Unitization 120: The Amount by Charge Types and GL Acct do not agree between CWIP_Charge and Charge_Group_Control. *Reset Unitization and Try Again*'
where a.company_id = :i_company_id
and a.error_msg is null
and a.work_order_id in (select work_order_id
								from (
									select c.work_order_id, c.charge_type_id, c.charge_group_id, c.expenditure_type_id, sum(c.amount), 2
									from charge_group_control c, UNITIZE_WO_LIST_TEMP x
									where c.work_order_id = x.work_order_id 
									and x.company_id = :i_company_id
									and x.error_msg is null
									and c.expenditure_type_id in (1,2)
									and c.charge_type_id <> :i_ocr_charge_type_id /*OCR*/
									and c.batch_unit_item_id is null 
									and exists (select 1 from work_order_charge_group b
													where b.work_order_id = c.work_order_id  
													and b.charge_group_id = c.charge_group_id 
													)
									group by c.work_order_id, c.charge_type_id, c.charge_group_id, c.expenditure_type_id
									
									minus
									
									select c.work_order_id, c.charge_type_id, b.charge_group_id, c.expenditure_type_id, sum(c.amount), 2
									from cwip_charge c, work_order_charge_group b, work_order_account e, UNITIZE_WO_LIST_TEMP x
									where c.work_order_id = x.work_order_id
									and x.company_id = :i_company_id
									and x.error_msg is null
									and x.work_order_id = e.work_order_id
									and c.charge_id = b.charge_id
									and c.work_order_id = b.work_order_id    
									and c.expenditure_type_id in (1,2)  
									and c.charge_type_id <> :i_ocr_charge_type_id /*OCR*/
									and c.unit_closed_month_number is null 
									and 
										  (e.CWIP_GL_ACCOUNT = c.GL_ACCOUNT_ID OR
											e.NON_UNITIZED_GL_ACCOUNT = c.GL_ACCOUNT_ID OR 
											e.REMOVAL_GL_ACCOUNT = c.GL_ACCOUNT_ID OR 
											e.SALVAGE_GL_ACCOUNT = c.GL_ACCOUNT_ID OR 
											c.GL_ACCOUNT_ID in (1,2) 
										  )
																					  
									group by c.work_order_id, c.charge_type_id, b.charge_group_id, c.expenditure_type_id
									)
								);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP cgc charge_type_id check: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

if num_nrows > 0 then
	//Handle errors identified in the tolerance checks above.  
	of_rtn = of_errors('WO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if 

return 1
end function

public function integer of_allocation_limits ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This function should only be called for Regular Close Unitization.  Late Close does not use this.
//limit_for_auto_allo:  table doc description of column is below.
//Dollar limit used in the determination of automatic allocation.  
//The user can restrict the total dollars that can be automatically allocated for particular charge types.  
//For example, labor may have no limit, but only invoices under a certain dollar amount would be allocated.  
//The limit is applied after the unitization automatic summaries are performed.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   checking allocation limits..."
of_log() 

insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select a.company_id, b.work_order_id, 
substrb(
		'Unitization 138: Cannot allocate charge type, '||d.description||', amount = '||to_char(b.amount)||', and limit for allo = '||to_char(limit_for_auto_allo)||' ('||decode(b.expenditure_type_id, 1, 'Additions', 'RWIP')||')'
		, 1, 2000)
from UNITIZE_WO_LIST_TEMP a, CHARGE_GROUP_CONTROL b, unit_alloc_meth_control c, charge_type d
where a.work_order_id = b.work_order_id
and a.alloc_method_type_id = c.alloc_method_type_id
and b.charge_type_id = c.charge_type_id
and d.charge_type_id = b.charge_type_id
and a.company_id = :i_company_id 
and b.expenditure_type_id in (1,2)
and b.batch_unit_item_id is null
and b.unit_item_id is null /*original charge available for allocation*/
and a.error_msg is null  
and c.limit_for_auto_allo <> 0
and b.amount >= c.limit_for_auto_allo;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_ERRORS_TEMP (limits): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if 
end if

return 1
end function

public function integer of_start_msg ();////////////////////////////////////////////////////////////////////////////////////
//messaging for starting the Unitization processing.
///////////////////////////////////////////////////////////////////////////////////
longlong max_id, num_nrows
string msg, insert_error_msg

if i_single_wo then
	return 1
end if 
 
//  "id" is the PK ... Get the max + 1 ...
max_id = 0
select max(id) into :max_id from wo_auto101_control
where company_id = :i_company_id and accounting_month = :i_month_yr;

if max_id = 0 or isnull(max_id) then 
	max_id = 1
else
	max_id++
end if

msg = "Beginning at: " + string(today()) + ", " + string(now()) 

insert into wo_auto101_control  (id, work_order_id, accounting_month, return_code, message, run, company_id, closing_option_id)
values ( :max_id, 0, :i_month_yr, 1, :msg, :i_run, :i_company_id, null);
		
if sqlca.SQLCode < 0 then 	
	
	insert_error_msg = sqlca.SQLErrText	
	rollback;	
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into wo_auto101_control(start): " + insert_error_msg
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id)
	of_log() 
	
	if pos(lower(insert_error_msg), 'unique constraint') > 0 or pos(lower(insert_error_msg), 'deadlock ') > 0 then
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: Unable to start unitization for run " + string(i_run) + ".  A concurrent processing of unitizing the same work order(s) may be causing this error.  Please verify work orders for the same company are not being Unitized (Auto or Manual) at the same time and Try Again."
		of_log() 	
	end if		
		 	
	return -1	
	
end if		
num_nrows = sqlca.sqlnrows

//commit here to save the first entry into wo_auto101_control table for new run
commit;

return 1
end function

public function integer of_insert_into_charge_group_control_allo (longlong a_priority_number);longlong num_nrows
string wo_list_sqls, rtn_string
longlong num_wos, i
longlong wo_id, max_charge_gp_id
uo_ds_top ds_wos_list 
ds_wos_list = create uo_ds_top


i_msg[] = i_null_arrary[] 
if a_priority_number = 0 then
	i_msg[1] =  ": preparing allocated charges..."
else
	i_msg[1] =  ":   preparing allocated charges..."
end if 
of_log() 

//remove amount = 0 where allocation results are all zeros
delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP a
where a.company_id = :i_company_id
and a.allocation_priority = :a_priority_number
and a.amount = 0
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP for amount = 0 failed: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//set a unique "ID" for each charge group staged (this table is deleted for the company for each new priority)
update UNITIZE_ALLOC_CGC_INSERTS_TEMP a
set a.ID = rownum
where a.company_id = :i_company_id
and a.allocation_priority = :a_priority_number
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_CGC_INSERTS_TEMP.ID update failed: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//maint 43357:  replace row_number() oracle function and loop by one work order at a time.
//                   large volumn of charges causes this update for new_charge_group_id to never finish.
//                   tested for looping by work order with  row_number() oracle function, and the update was still slow for the large volumn work orders.
//                   the old code is commented out to show what it was and why it is not being used.

wo_list_sqls = "select a.work_order_id, a.max_charge_group_id from UNITIZE_WO_LIST_TEMP a where error_msg is null and a.company_id = " + string(i_company_id) + &
	" and exists (select 1 from UNITIZE_ALLOC_CGC_INSERTS_TEMP b where a.work_order_id = b.work_order_id and b.allocation_priority = " + string(a_priority_number) + &
	" and b.company_id = " + string(i_company_id) + ")" 
rtn_string = f_create_dynamic_ds(ds_wos_list, 'grid', wo_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_wos_list for new_charge_group_id: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_wos = ds_wos_list.RowCount()

for i = 1 to num_wos
	
	wo_id = ds_wos_list.getitemnumber(i, 1)
	max_charge_gp_id = ds_wos_list.getitemnumber(i, 2)
	
//	//testing: show more info
//	i_msg[] = i_null_arrary[] 
//	i_msg[1] =   ":   wo_id " + string(wo_id) + " for max_charge_gp_id " + string(max_charge_gp_id)
//	of_log()  	
		
	//using the unqiue ID update the NEW_CHARGE_GROUP_ID for each work order
	update UNITIZE_ALLOC_CGC_INSERTS_TEMP a
	set a.new_charge_group_id =  :max_charge_gp_id + rownum  									 
	where a.work_order_id = :wo_id
	and a.allocation_priority = :a_priority_number; 	
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_ALLOC_CGC_INSERTS_TEMP new charge_group_id for wo_id = " + string(wo_id) +": " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		destroy ds_wos_list
		return -1
	end if		
	num_nrows = sqlca.sqlnrows   
	
//	//testing: show more info
//	i_msg[] = i_null_arrary[] 
//	i_msg[1] =   ":   wo_id " + string(wo_id) + " updated " + string(num_nrows) + " rows"
//	of_log()  	 
	
next
 
destroy ds_wos_list 


i_msg[] = i_null_arrary[] 
if a_priority_number = 0 then
	i_msg[1] =  ": creating allocated charges..."
else
	i_msg[1] =  ":   creating allocated charges..."
end if 
of_log() 

//INSERT allocation results (alloc_id = null; group_indicator = null; unit_target_id = null; allocation_priority= 0)
//maint-43648: get retirement_unit_id, utility_account_id, bus_segment_id, sub_account_id, property_group_id, asset_location_id, serial_number from UNITIZE_ELIG_UNITS_TEMP
insert into charge_group_control
	(charge_group_id, work_order_id, alloc_id, unit_item_id, utility_account_id, bus_segment_id, sub_account_id, charge_type_id, expenditure_type_id, 
	retirement_unit_id, property_group_id, unit_target_id, description, group_indicator, allocation_priority, unitization_summary, amount, quantity, book_summary_name, 
	asset_location_id, serial_number, orig_charge_group_id)
select a.new_charge_group_id, a.work_order_id, null, a.unit_item_id, c.utility_account_id, c.bus_segment_id, c.sub_account_id, a.charge_type_id, a.expenditure_type_id, 
	c.retirement_unit_id, c.property_group_id, null, a.description, null, 0, a.unitization_summary, a.amount, a.quantity, a.book_summary_name, 
	c.asset_location_id, trim(c.serial_number), a.orig_charge_group_id
from UNITIZE_ALLOC_CGC_INSERTS_TEMP a, UNITIZE_WO_LIST_TEMP b, UNITIZE_ELIG_UNITS_TEMP c
where a.work_order_id = b.work_order_id
and c.work_order_id = a.work_order_id
and c.unit_item_id = a.unit_item_id
and b.error_msg is null
and a.company_id = :i_company_id
and a.allocation_priority = :a_priority_number;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into charge_group_control allo: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//Create a Unit_item_id = 0 for work orders that have split charge allocation results
insert into unitized_work_order (work_order_id, unit_item_id)
select distinct work_order_id, 0
from UNITIZE_WO_LIST_TEMP a
where a.company_id = :i_company_id
and a.error_msg is null 
and not exists (select 1 from UNITIZED_WORK_ORDER b
					where a.work_order_id = b.work_order_id
					and b.unit_item_id = 0
					);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into unitized_work_order unit_item_id = 0: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//Update the original charge groups that link back to CWIP_CHARGE 
//a_priority_number = 0 is for Late Close since Late Close does not allocate by Priority numbers
update charge_group_control a
set a.unit_item_id = 0 
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and a.unit_item_id is null 
and a.allocation_priority = decode(:a_priority_number, 0, a.allocation_priority, :a_priority_number)
and exists (select 1
				from UNITIZE_ALLOC_CGC_INSERTS_TEMP b, UNITIZE_WO_LIST_TEMP c
				where a.work_order_id = b.work_order_id
				and c.work_order_id = b.work_order_id
				and c.error_msg is null
				and a.charge_group_id = b.orig_charge_group_id
				and c.company_id = :i_company_id
				and b.allocation_priority = :a_priority_number
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR:update charge_group_control unit_item_id = 0: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//update UNIT_ITEM_ID = 0 for charge_groups where the amount = 0 and the record did not get allocated.
//a_priority_number = 0 is for Late Close since Late Close does not allocate by Priority numbers
update CHARGE_GROUP_CONTROL a
set a.unit_item_id = 0
where a.unit_item_id is null
and a.amount = 0
and a.batch_unit_item_id is null
and nvl(a.pend_transaction,0) = 0
and a.expenditure_type_id in (1,2)
and a.allocation_priority = decode(:a_priority_number, 0, a.allocation_priority, :a_priority_number)
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/
and exists (select 1 from UNITIZE_WO_LIST_TEMP b
			where b.company_id = :i_company_id
			and a.work_order_id = b.work_order_id
			and b.error_msg is null
			);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR:update charge_group_control unit_item_id = 0 for amount = 0: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

return 1
end function

public function integer of_allocation_validate_wo_est (longlong a_priority_number);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////unit items could be made from Actuals when i_unit_item_from_estimate = 0.
////one for one will require ESTIMATE_ID to UNIT_ESTIMATE_ID join but will fail later if none are found in UWO.
////detect this specific problem and provide better messaging for 101 errors.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
string msg
longlong of_rtn, num_nrows

msg = "Unitization 280: " + &
	"Unable to Allocate charges. The 'Unit Item Est Level' = 'One for One', but none of the Unit Items are created from Estimates. " + &
	"Change 'UI from Est' option to 'Yes.' * Reset Unitization and try again*"
							
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct a.company_id, a.work_order_id, substrb(:msg, 1, 2000) 
from UNITIZE_WO_LIST_TEMP a, CHARGE_GROUP_CONTROL b, UNIT_ALLOCATION c
where a.work_order_id = b.work_order_id
and b.alloc_id = c.alloc_id
and a.company_id = :i_company_id
and b.allocation_priority = :a_priority_number
and b.expenditure_type_id in (1,2)
AND a.EST_UNIT_ITEM_OPTION = 1 /*1 for 1*/
AND a.UNIT_ITEM_FROM_ESTIMATE = 0  /* potential for creating unit items from Actuals*/
and b.batch_unit_item_id is null
and b.unit_item_id is null
and b.alloc_id is not null 
and TRIM(LOWER(c.alloc_type)) = 'estimates'
and a.error_msg is null
and not exists (select 1 from UNITIZE_ELIG_UNITS_TEMP e
					where b.work_order_id = e.work_order_id
					and e.status in (11, 10)
					and e.unit_estimate_id is not null 
					);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_ERRORS_TEMP (280): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

if num_nrows > 0 then
//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if

return 1
end function

public function integer of_allocation_tolerance_check (longlong a_priority_number);//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Estimates based allocation must check negative vs positive estimates data
//the system control, TOLERANCE FOR ESTIMATES, is used to determine if the tolerance for negative vs positive value Estimates data is within the configured tolerance percent.  
//If the control value is not determined, a default value of 0.1 is used (10% percent).  
//This is the percentage (0.10) that negative wo estimates can be of positive estimates for a particular work order and unit allocation (alloc_id).  
//Note: if this percentage is allowed to approach 100% (1.00), a denominator in the calculation approaches zero and results in distorted allocation results.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
string tolerance_str
longlong num_nrows, of_rtn
decimal {2} tolerance

tolerance_str = f_pp_system_control_company('TOLERANCE FOR ESTIMATES', i_company_id)

if tolerance_str = '' or isnull(tolerance_str) then
	tolerance = 0.1  /*default*/
else
	tolerance = dec(tolerance_str)
end if

tolerance_str = string(tolerance*100)

insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select inside.company_id, inside.work_order_id, 
substrb(
'Unitization 137: Negative estimates exist that are greater than the tolerance of '||:tolerance_str||'% percent of the positive estimates. Current negative pct is '||to_char(round(abs(inside.negative)/inside.positive, 4)*100)||'%.'
, 1, 2000) 
from (
		select a.company_id, a.work_order_id, b.expenditure_type_id, b.alloc_id, 
		sum(decode(sign(b.alloc_statistic), -1, b.alloc_statistic, 0)) negative, sum(decode(sign(b.alloc_statistic), 1, b.alloc_statistic, 0)) positive
		from UNITIZE_WO_LIST_TEMP a, UNITIZE_RATIO_UNITS_TEMP b
		where a.work_order_id = b.work_order_id
		AND a.company_id = :i_company_id      
		AND b.alloc_priority = :a_priority_number 
		and b.alloc_type = 'estimates'
		and a.error_msg is null 
		group by a.company_id, a.work_order_id, b.expenditure_type_id, b.alloc_id 
		having sum(decode(sign(b.alloc_statistic), 1, b.alloc_statistic, 0)) <> 0
		) inside
where abs(inside.negative)/inside.positive > :tolerance;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_ERRORS_TEMP (est tolerance): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

if num_nrows > 0 then 
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if 

return 1

end function

public function integer of_allocation_statistic_estimates_all ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Maint 38041   
//Purpose:Insert data into the UNITIZE_ALLOC_EST_TEMP globabl temp table as a side table of WO_ESTIMATE, which is a table that can get very large over time.
//          Estimate based allocation statistics do not vary by priorities like Actuals based allocations, 
//          but multiple configurations of the ALLOC_ID (unit allocations) could be different across different priority numbers, meaning different combinations of ALLOC_IDs for each priority number.
//          Setting up the allocation statistics for estimates needs to be done for all allocation basis using Estimates type for unitization allocation configuration during the first priority. 
//          Then the priority specific processing can pull the data already setup in the master estimates table, UNITIZE_ALLOC_EST_TEMP.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, check_count, of_rtn	


//determine if the work order(s) being unitized right now have any "estimates" based Allocations.
check_count = 0
select count(1) into :check_count
from UNITIZE_ALLOC_LIST_TEMP
where company_id = :i_company_id
and alloc_type = 'estimates';

if check_count > 0 then 
	//1 for 1		
	//old datawindow: dw_unit_allo_calc_est_ui
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   gathering eligible estimate data(1to1)..."
	of_log() 
	
	///////////////////////////////////
	//For One for One method	
	////////////////////////////////////
	
	//no need to insert into UNITIZE_ALLOC_EST_STG_TEMP for 1 for 1 estimates.  Insert directly into UNITIZE_ALLOC_EST_TEMP.
	insert into UNITIZE_ALLOC_EST_TEMP 
	(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
	ALLOC_ID, ALLOC_TYPE, ALLOC_BASIS, ALLOC_STATISTIC)
	SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
		a.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
		d.ALLOC_ID, lower(trim(e.alloc_type)), lower(trim(e.alloc_basis)),
		decode(lower(trim(e.alloc_basis)), 'quantity', nvl(a.quantity,0), a.AMOUNT)  					
	 FROM WO_ESTIMATE a,   
			UNITIZE_ELIG_UNITS_TEMP b,
			ESTIMATE_CHARGE_TYPE c,   
			UNIT_ALLOC_BASIS d,
			UNIT_ALLOCATION e,
			UNITIZE_WO_LIST_TEMP x
	WHERE b.WORK_ORDER_ID = x.WORK_ORDER_ID    
		AND a.WORK_ORDER_ID =  x.WORK_ORDER_ID    
		AND a.WORK_ORDER_ID =  b.WORK_ORDER_ID    
		AND d.ALLOC_ID = e.ALLOC_ID  
		AND b.EXCLUDE_FROM_ALLOCATIONS = 0
		AND x.COMPANY_ID = :i_company_id     
		AND x.EST_UNIT_ITEM_OPTION = 1 /*1 for 1*/
		AND x.ERROR_MSG is null 
		AND a.REVISION =  x.MAX_REVISION
		AND a.BATCH_UNIT_ITEM_ID is null    
		AND e.ALLOC_ID in (select f.alloc_id 
									from UNITIZE_ALLOC_LIST_TEMP f
									where f.company_id = :i_company_id
									and f.alloc_type = 'estimates'
									)
		AND a.EST_CHG_TYPE_ID = c.EST_CHG_TYPE_ID    
		AND d.EST_CHG_TYPE_ID = c.EST_CHG_TYPE_ID    
		AND a.ESTIMATE_ID = b.UNIT_ESTIMATE_ID   /*1 for 1*/  
		AND a.expenditure_type_id in (1,2)  
		AND a.EXPENDITURE_TYPE_ID = b.EXPENDITURE_TYPE_ID;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_EST_TEMP(estimates 1for1): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows     
	
	//////////////////////////////////////////////////////////////////////////////////////
	//For Summarize method insert into UNITIZE_ALLOC_EST_STG_TEMP
	//////////////////////////////////////////////////////////////////////////////////////	
	
	//first insert into UNITIZE_ALLOC_EST_STG_TEMP to prep data for UNITIZE_ALLOC_EST_TEMP 
	//insert for wos with replace_amt_as_unit = 0 for the addition estimates 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   gathering eligible estimate data(adds)..."
	of_log() 
	
	insert into UNITIZE_ALLOC_EST_STG_TEMP	( WORK_ORDER_ID, EXPENDITURE_TYPE_ID, REVISION, EST_CHG_TYPE_ID,
	UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID, 
	SERIAL_NUMBER, 
	ASSET_ID,  
	AMOUNT, QUANTITY, REPLACEMENT_AMOUNT)
	SELECT a.WORK_ORDER_ID, a.EXPENDITURE_TYPE_ID, a.REVISION, a.EST_CHG_TYPE_ID,
	a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID, 
	trim(a.SERIAL_NUMBER), 
	/*wo_est_trans_type_id: 4 = Partial Replacement  3 = Replacement: do not keep the asset_id for ldg_asset_id for additions*/
	decode(a.wo_est_trans_type_id, 3, null, 4, null, a.asset_id), 
	SUM(NVL(a.AMOUNT,0)), SUM(NVL(a.QUANTITY,0)), SUM(nvl(a.REPLACEMENT_AMOUNT,0))
	FROM WO_ESTIMATE a, UNITIZE_WO_LIST_TEMP b
	WHERE a.WORK_ORDER_ID = b.WORK_ORDER_ID   
	AND b.COMPANY_ID = :i_company_id     
	AND nvl(b.replace_amt_as_unit,0) = 0 /*replacement amount is not a unit attribute*/
	AND b.ERROR_MSG is null 
	AND a.REVISION =  b.MAX_REVISION
	AND a.BATCH_UNIT_ITEM_ID is null   
	AND a.expenditure_type_id = 1 /*additions*/
	and a.est_chg_type_id in (select c.est_chg_type_id 
									from UNIT_ALLOC_BASIS c, UNIT_ALLOCATION d, UNITIZE_ALLOC_LIST_TEMP e
									where  c.alloc_id = d.alloc_id
									and e.company_id = :i_company_id
									and e.alloc_type = 'estimates'
									)
	GROUP BY a.REVISION, a.WORK_ORDER_ID, a.EXPENDITURE_TYPE_ID, a.REVISION, a.EST_CHG_TYPE_ID,
	a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID, 
	trim(a.SERIAL_NUMBER), decode(a.wo_est_trans_type_id, 3, null, 4, null, a.asset_id);
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_EST_STG_TEMP(sum adds): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  	
	
	//call of_replacement_amt_as_pct() to determine the "i_has_replace_pct_estimates" boolean
	of_replacement_amt_as_pct()	
	if i_has_replace_pct_estimates then		
		//next, insert into UNITIZE_ALLOC_EST_STG_TEMP to prep data for UNITIZE_ALLOC_EST_TEMP 
		//insert for wos with replace_amt_as_unit = 1 for the addition estimates 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ":   gathering eligible estimate data(pct)..."
		of_log() 
		
		insert into UNITIZE_ALLOC_EST_STG_TEMP	( WORK_ORDER_ID, EXPENDITURE_TYPE_ID, REVISION, EST_CHG_TYPE_ID,
		UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID, 
		SERIAL_NUMBER, 
		ASSET_ID,  
		AMOUNT, QUANTITY, REPLACEMENT_AMOUNT)
		SELECT a.WORK_ORDER_ID, a.EXPENDITURE_TYPE_ID, a.REVISION, a.EST_CHG_TYPE_ID,
		a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID, 
		trim(a.SERIAL_NUMBER), 
		/*wo_est_trans_type_id: 4 = Partial Replacement  3 = Replacement: do not keep the asset_id for ldg_asset_id for additions*/
		decode(a.wo_est_trans_type_id, 3, null, 4, null, a.asset_id), 
		SUM(NVL(a.AMOUNT,0)), SUM(NVL(a.QUANTITY,0)), nvl(a.REPLACEMENT_AMOUNT,0)
		FROM WO_ESTIMATE a, UNITIZE_WO_LIST_TEMP b
		WHERE a.WORK_ORDER_ID = b.WORK_ORDER_ID   
		AND b.COMPANY_ID = :i_company_id     
		AND b.replace_amt_as_unit = 1 /*replacement amount is a unit attribute*/
		AND b.ERROR_MSG is null 
		AND a.REVISION =  b.MAX_REVISION
		AND a.BATCH_UNIT_ITEM_ID is null   
		AND a.expenditure_type_id  = 1 /*additions*/
		and a.est_chg_type_id in (select c.est_chg_type_id 
										from UNIT_ALLOC_BASIS c, UNIT_ALLOCATION d, UNITIZE_ALLOC_LIST_TEMP e
										where  c.alloc_id = d.alloc_id
										and e.company_id = :i_company_id
										and e.alloc_type = 'estimates'
										)
		GROUP BY a.REVISION, a.WORK_ORDER_ID, a.EXPENDITURE_TYPE_ID, a.REVISION, a.EST_CHG_TYPE_ID,
		a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID, 
		trim(a.SERIAL_NUMBER),  
		decode(a.wo_est_trans_type_id, 3, null, 4, null, a.asset_id), 
		nvl(a.REPLACEMENT_AMOUNT,0);
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_EST_STG_TEMP(sum adds pct): " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		
		num_nrows = sqlca.sqlnrows  
	end if //i_has_replace_pct_estimates
	
	//first insert into UNITIZE_ALLOC_EST_STG_TEMP to prep data for UNITIZE_ALLOC_EST_TEMP insert for wos with replace_amt_as_unit = 0
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   gathering eligible estimate data(rets)..."
	of_log() 
	
	////////////////////////////////////////////////////////////////////////////
	//retirement estimates: replacement_amount is always 0
	/////////////////////////////////////////////////////////////////////////////
	insert into UNITIZE_ALLOC_EST_STG_TEMP	( WORK_ORDER_ID, EXPENDITURE_TYPE_ID, REVISION, EST_CHG_TYPE_ID,
	UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID, 
	SERIAL_NUMBER, ASSET_ID,  AMOUNT, QUANTITY, REPLACEMENT_AMOUNT)
	SELECT a.WORK_ORDER_ID, a.EXPENDITURE_TYPE_ID, a.REVISION, a.EST_CHG_TYPE_ID,
	a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID, 
	trim(a.SERIAL_NUMBER), a.ASSET_ID, SUM(NVL(a.AMOUNT,0)), SUM(NVL(a.QUANTITY,0)), 0 
	FROM WO_ESTIMATE a, UNITIZE_WO_LIST_TEMP b
	WHERE a.WORK_ORDER_ID = b.WORK_ORDER_ID   
	AND b.COMPANY_ID = :i_company_id   
	AND b.ERROR_MSG is null 
	AND a.REVISION =  b.MAX_REVISION
	AND a.BATCH_UNIT_ITEM_ID is null   
	AND a.expenditure_type_id = 2 /*retirements*/
	and a.est_chg_type_id in (select c.est_chg_type_id 
									from UNIT_ALLOC_BASIS c, UNIT_ALLOCATION d, UNITIZE_ALLOC_LIST_TEMP e
									where  c.alloc_id = d.alloc_id
									and e.company_id = :i_company_id
									and e.alloc_type = 'estimates'
									)
	GROUP BY a.REVISION, a.WORK_ORDER_ID, a.EXPENDITURE_TYPE_ID, a.REVISION, a.EST_CHG_TYPE_ID,
	a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID, 
	trim(a.SERIAL_NUMBER), a.ASSET_ID;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_EST_STG_TEMP(sum ret): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  	
	
	/////////////////////////////////////////////////////////////////////////////
	//Summarize method insert into UNITIZE_ALLOC_EST_TEMP
	/////////////////////////////////////////////////////////////////////////////
		
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   gathering eligible estimate data(all)..."
	of_log() 
	
	//old datawindow: dw_unit_allo_calc_est  
	insert into UNITIZE_ALLOC_EST_TEMP 
	(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, 
	BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
	ALLOC_ID, ALLOC_TYPE, ALLOC_BASIS, ALLOC_STATISTIC)
	SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
		a.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
		d.ALLOC_ID, lower(trim(e.alloc_type)), lower(trim(e.alloc_basis)), 
		sum(decode(lower(trim(e.alloc_basis)), 'quantity', a.QUANTITY, a.AMOUNT)) 					
	 FROM UNITIZE_ALLOC_EST_STG_TEMP a,   
			UNITIZE_ELIG_UNITS_TEMP b,
			ESTIMATE_CHARGE_TYPE c,   
			UNIT_ALLOC_BASIS d,
			UNIT_ALLOCATION e,
			UNITIZE_WO_LIST_TEMP x  
	WHERE b.WORK_ORDER_ID = x.WORK_ORDER_ID   
		AND a.WORK_ORDER_ID =  x.WORK_ORDER_ID    
		AND a.WORK_ORDER_ID =  b.WORK_ORDER_ID    
		AND d.ALLOC_ID = e.ALLOC_ID  
		AND b.EXCLUDE_FROM_ALLOCATIONS = 0
		AND x.COMPANY_ID = :i_company_id     
		AND x.EST_UNIT_ITEM_OPTION = 0 /*summarize*/
		AND x.ERROR_MSG is null   
		AND a.EST_CHG_TYPE_ID = c.EST_CHG_TYPE_ID    
		AND d.EST_CHG_TYPE_ID = c.EST_CHG_TYPE_ID    
		AND a.PROPERTY_GROUP_ID = b.PROPERTY_GROUP_ID   
		AND a.ASSET_LOCATION_ID = b.ASSET_LOCATION_ID    
		AND a.BUS_SEGMENT_ID = b.BUS_SEGMENT_ID     
		AND a.UTILITY_ACCOUNT_ID = b.UTILITY_ACCOUNT_ID    
		AND a.SUB_ACCOUNT_ID = b.SUB_ACCOUNT_ID
		AND a.RETIREMENT_UNIT_ID = b.RETIREMENT_UNIT_ID      		
		AND a.EXPENDITURE_TYPE_ID =  b.EXPENDITURE_TYPE_ID	
		AND   nvl(trim(a.serial_number) ,'NULLSERIAL_NUMBER') =  nvl(trim(b.serial_number) ,'NULLSERIAL_NUMBER')	
		AND   nvl(a.asset_id,0) =  nvl(b.ldg_asset_id,0)
		AND   nvl(a.replacement_amount,0) =  nvl(b.replacement_amount,0)
				
	GROUP BY   x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
		a.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
		d.ALLOC_ID, lower(trim(e.alloc_type)), lower(trim(e.alloc_basis));
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_EST_TEMP(estimates sum all): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows     
	
end if //if check_count > 0  

return 1
end function

public function integer of_wo_estimate_defaults_backfill ();/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Backfill null fields on the WO_ESTIMATE table with the default logic for each default-able attribute.
/////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn
string rtn_string

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": updating estimate nulls..."
of_log() 

//bus segment:  default comes from WORK_ORDER_CONTROL.BUS_SEGMENT_ID
//this backfill is more straighforward and uses UNITIZE_WO_LIST_TEMP table, which has only bus segment per work order.
update WO_ESTIMATE a
set a.bus_segment_id = (select b.bus_segment_id from UNITIZE_WO_LIST_TEMP b
								where a.work_order_id = b.work_order_id
								and a.revision = b.max_revision 
								and b.company_id = :i_company_id 
								and b.bus_segment_id is not null 
								and b.error_msg is null
								)
where a.bus_segment_id is null
and a.batch_unit_item_id is null
and a.asset_id is null
and a.expenditure_type_id in (1, 2) 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id
				and x.bus_segment_id is not null 
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.bus_segment_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//asset location: 1st, the default comes from WORK_ORDER_CONTROL.ASSET_LOCATION_ID							
update WO_ESTIMATE a
set a.asset_location_id = (select c.asset_location_id from UNITIZE_WO_LIST_TEMP c, ASSET_LOCATION b
								where a.work_order_id = c.work_order_id
								and a.revision = c.max_revision
								and c.asset_location_id = b.asset_location_id
								and nvl(b.status_code_id,1) = 1  /*active*/
								and c.company_id = :i_company_id
								and c.error_msg is null
								and c.asset_location_id is not null 
								)	 
where a.asset_location_id is null
and a.batch_unit_item_id is null
and a.asset_id is null
and a.expenditure_type_id in (1, 2) 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id 
				and x.error_msg is null
				and x.asset_location_id is not null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.asset_location_id backfill1: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if
num_nrows = sqlca.sqlnrows     
								

//asset location:  2nd, use MIN(ASSET_LOCATION_ID) FROM WORK_ORDER_CONTROL.MAJOR_LOCATION_ID 
update WO_ESTIMATE a
set a.asset_location_id = (select min(b.asset_location_id) from UNITIZE_WO_LIST_TEMP c, ASSET_LOCATION b
								where a.work_order_id = c.work_order_id
								and a.revision = c.max_revision
								and c.major_location_id = b.major_location_id
								and nvl(b.status_code_id,1) = 1  /*active*/
								and c.company_id = :i_company_id
								and c.error_msg is null
								and c.asset_location_id is null 
								group by b.major_location_id
								)	 
where a.asset_location_id is null
and a.batch_unit_item_id is null
and a.asset_id is null
and a.expenditure_type_id in (1, 2) 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id 
				and x.error_msg is null
				and x.asset_location_id is null 
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.asset_location_id backfill1: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     


//property group:  default comes from PROP_GROUP_PROP_UNIT & FUNC_CLASS_PROP_GRP
update WO_ESTIMATE a
set a.property_group_id = (select min(b.property_group_id) 
									from prop_group_prop_unit b, func_class_prop_grp c, retirement_unit d, utility_account e
									where a.utility_account_id = e.utility_account_id
									and a.bus_segment_id = e.bus_segment_id
									and a.retirement_unit_id = d.retirement_unit_id 
									and e.func_class_id = c.func_class_id
									and b.property_unit_id = d.property_unit_id
									and c.property_group_id = b.property_group_id
								)
where a.property_group_id is null
and a.batch_unit_item_id is null
and a.asset_id is null
and a.expenditure_type_id in (1, 2) 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.property_group_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//sub account:  default comes from SUB_ACCOUNT table based on the BS_ID and UT_ID used in the joins.
update WO_ESTIMATE a
set a.sub_account_id = (select min(b.sub_account_id) from sub_account b 
								where a.utility_account_id = b.utility_account_id
								and a.bus_segment_id = b.bus_segment_id
								and nvl(b.status_code_id,1) = 1  /*active*/
								)
								
where a.sub_account_id is null
and a.batch_unit_item_id is null
and a.asset_id is null
and a.expenditure_type_id in (1, 2) 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.sub_account_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

////////////////////////////////////////////////////////////////////////////////////////////////
//Maint 38140:  batch_unit_item_id:  0 is not a valid value.  in some very old custom code, the batch_unit_item_id would be updated to 0 to be "re-usable" forever.
//to keep the code consistent, any 0 wo estimates will be update to null to be used as expected in the Unitization processing logic instead of modifying all 
//of the code to use NVL(0).
////////////////////////////////////////////////////////////////////////////////////////////////
update WO_ESTIMATE a
set a.batch_unit_item_id = null
where a.batch_unit_item_id = 0
and a.expenditure_type_id in (1, 2) 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.batch_unit_item_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//no need to run validations to verify all of the nulls got filled in on WO_ESTIMATE.  

//maint 44314: backfill missing sub account where asset_id is found in cpr_ledger, but include bus segment and utility account due to foreign key constraint
update wo_estimate a
set (a.utility_account_id, a.bus_segment_id, a.sub_account_id) = 
							(select b.utility_account_id, b.bus_segment_id, b.sub_account_id 
								from cpr_ledger b
								where a.asset_id = b.asset_id
								)
where a.asset_id > 0
and a.sub_account_id is null
and a.batch_unit_item_id is null
and a.expenditure_type_id in (1,2)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.sub_account for asset_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//maint 44314: backfill missing property_group_id where asset_id is found in cpr_ledger
update wo_estimate a
set a.property_group_id = (select b.property_group_id from cpr_ledger b
								where a.asset_id = b.asset_id
								)
where a.asset_id > 0
and a.property_group_id is null
and a.batch_unit_item_id is null
and a.expenditure_type_id in (1,2)
and exists (select 1 from UNITIZE_WO_LIST_TEMP x 
				where a.work_order_id = x.work_order_id
				and a.revision = x.max_revision 
				and x.company_id = :i_company_id
				and x.error_msg is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: WO_ESTIMATE.property_group for asset_id backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  


//sync serial_number for estimates and units already made to avoid allocation errors
of_rtn = of_sync_wo_est_serial_number()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

return 1
end function

public function integer of_wo_estimate_get_max_revision ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Obtain the max(revision) to use for work order using Estimates to create Unit Items or Estimates for Allocations
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows 

update UNITIZE_WO_LIST_TEMP z
set z.max_revision = (select max(a.revision) from WO_ESTIMATE a
							where a.work_order_id = z.work_order_id
							group by a.work_order_id 
							)
where z.company_id = :i_company_id;
					
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =   ": ERROR: getting max revision for UNITIZE_WO_LIST_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

//save this information into the Archive table
update UNITIZE_WO_LIST_ARC a
set a.max_revision = 
	(select b.max_revision
	from UNITIZE_WO_LIST_TEMP b
	where a.work_order_id = b.work_order_id
	)
where a.company_id = :i_company_id
and a.db_session_id = :i_this_session_id 
and a.unit_closed_month_number = :i_month_number
and a.run_time_string = :i_run_time_string
and a.run = :i_run
and a.unitization_type = :i_unitization_type;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_ARC for max_revision data: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1
end function

public function integer of_initialize_variables ();////////////////////////////////////////////////////////////////////////////////////////////////////////
//Initialize variables and tables used in the Unitization processing.
/////////////////////////////////////////////////////////////////////////////////////////////////////// 
longlong check_count 
string err_message

//determine CHARGE_TYPE_ID for Accrual if Accrual Calc is done.
i_wo_accr_calc_sc = f_pp_system_control_company("Work Order Accrual Calculation",i_company_id)
if isnull(i_wo_accr_calc_sc) or i_wo_accr_calc_sc = "" then i_wo_accr_calc_sc = 'no'
if lower(i_wo_accr_calc_sc) = 'yes' then
	//get the system assigned CHARGE_TYPE_ID for Accrual
	i_wo_accr_ct = longlong(f_pp_system_control_company("Work Order Accrual Charge Type", i_company_id))
end if 

//format the month variables
select to_date(:i_month_number, 'yyyymm') into :i_month_yr from dual;

//verify the select statement is successful and returns 1 row
//return code for a select statement is -1 for error and 100 for no rows
if sqlca.SQLCode <> 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: initializing i_month_yr variable: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if 

i_month_yr_string_display = string(i_month_yr, 'mmm-yyyy') 
i_month_yr_string = string(i_month_yr, 'dd-mmm-yyyy') /*this date format works when used as the input arg value in the SQL of dw_wo_to_unitize_101*/

//get "i_ocr_charge_type_id"
check_count = 0
select count(1), min(charge_type_id) into :check_count, :i_ocr_charge_type_id
from charge_type
where processing_type_id = 1;

choose case sqlca.SQLCode 
	case -1, 100
		err_message = "ERROR: Unable to find OCR charge type. " + sqlca.SQLErrText   
	case else
		if check_count = 0 then 
			err_message = "ERROR: Unable to find OCR charge type. "
		elseif check_count = 1 then
			if isnull(i_ocr_charge_type_id) then 
				err_message = "ERROR: OCR charge_type_id on CHARGE_TYPE table is null. " 
			end if 
		else	
			err_message = "ERROR: There is more than one OCR charge type. " 
		end if 
end choose 

if err_message = '' or isnull(err_message) then
	//ok
else
	i_msg[] = i_null_arrary[] 
	i_msg[1] = err_message
	of_log() 
	rollback;		
	return -1 
end if 

//get "i_pend_balance"
//careful:  this system control name originally has extra spaces.  check if there is a system control with proper spacing in the control name.
select count(1) into:check_count
from pp_system_control_company 
where lower(control_name) = 'auto 101 - balance pending trans';

if check_count > 0 then
	i_pend_balance = lower(f_pp_system_control_company("Auto 101 - Balance Pending Trans", i_company_id))
else
	//the database has the original system control name with extra spacing
	i_pend_balance = lower(f_pp_system_control_company("Auto 101 -  Balance Pending Trans", i_company_id))
end if

//make the default be "yes" if no system control found
if isnull(i_pend_balance) or i_pend_balance = '' then 
	i_pend_balance = 'yes'  
end if 

//check for equipement ledger
i_cpr_equip_type_num = 0
select count(1) into :i_cpr_equip_type_num from cpr_equip_type;

//get  "i_run" 
//i_run is the number of times auto 101 has run for the given month and company 
i_run = 0
select max(run) into :i_run
  from wo_auto101_control
 where company_id = :i_company_id
 and accounting_month = :i_month_yr;

if i_run = 0 or isnull(i_run) then
   i_run = 1
else
   i_run++
end if

if i_single_wo = false then 
	i_closing_type = i_wos_to_run
else
	i_closing_type = 'SINGLE WO'
end if 

if i_single_wo = true then
	i_late_month_number = i_wo_close_late_month_number
end if 

i_equip_ledger_processed = false
i_net_zero_inserting = false

//get the unique start time
SELECT TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS') into :i_run_time_string FROM DUAL;

if i_run_time_string = '' or isnull(i_run_time_string) then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: initializing i_run_time_string variable: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if

//get the month_number to use to delete from the ARC tables
select to_char(add_months(:i_month_yr, -3), 'yyyymm') into :i_delete_month_number from dual;

if i_delete_month_number = 0 or isnull(i_delete_month_number) then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: initializing i_delete_month_number variable: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if


return 1


end function

public function integer of_prepare_wo_list ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Need UNITIZE_WO_LIST table to manage the WORK_ORDER_IDs to avoid concurrent run issues.  
//Need UNITIZE_WO_LIST_TEMP global temp table to handle the processing logic to only process for the work orders identified in this run.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, rtn, wo_count
string insert_error_msg

//avoid concurrent run of unitization for work orders already being unitized by company when a single wo unitization (manually or single auto) has already started.
//no need to error tag these work order.  silently and automatically exclude these work orders from this "run" of Unitization.
delete from UNITIZE_WO_LIST_TEMP a
where a.company_id = :i_company_id
and exists (select 1 from UNITIZE_WO_LIST b
				where b.company_id = :i_company_id
				and b.work_order_id = a.work_order_id);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_WO_LIST_TEMP delete for WOs in already being Unitized: " + sqlca.SQLErrText
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 	
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Add the work order(s) being unitized from the Global TEMP table that will be used in this code for Unitization processing for this specific "run" into the regular table.
//The UNITIZE_WO_LIST table can be referenced across all databases where multiple sessions of Unitization could occur at the same time. 
INSERT INTO UNITIZE_WO_LIST 
(WORK_ORDER_ID, WORK_ORDER_NUMBER, COMPANY_ID, UNITIZATION_TYPE, DB_SESSION_ID)
SELECT WORK_ORDER_ID, WORK_ORDER_NUMBER, COMPANY_ID, :i_unitization_type, :i_this_session_id 
FROM UNITIZE_WO_LIST_TEMP;

if sqlca.SQLCode < 0 then 	
	insert_error_msg = sqlca.SQLErrText	
	rollback;	
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_WO_LIST insert: " + insert_error_msg
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id)
	of_log() 
	
	//able to display full insert SQL syntax in f_wo_status_box, so show full insert syntax
	f_wo_status_box(i_sbox_title, "")
	f_wo_status_box(i_sbox_title, ": ERROR: UNITIZE_WO_LIST insert: " + insert_error_msg) 
	f_wo_status_box(i_sbox_title, "") 
	
	if pos(lower(insert_error_msg), 'unique constraint') > 0 or pos(lower(insert_error_msg), 'deadlock ') > 0 then
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: Unable to get list of eligible wos for run " + string(i_run) + ".  A concurrent processing of unitizing the same work order(s) may be causing this error.  Please verify work orders for the same company are not being Unitized (Auto or Manual) at the same time and Try Again."
		of_log() 
	end if	
		 	
	return -1	
end if		
num_nrows = sqlca.sqlnrows 

//need to commit here to keep track of which work orders are being unitized to avoid duplicate unitization by another session.
commit;


if i_single_wo = false then 	
	//get latest count of wos in this processing run
	wo_count = 0
	select count(1) into :wo_count
	from UNITIZE_WO_LIST_TEMP;

	//show number of wos in the same style of msg display as the old auto101 code.
	if i_visual = false then
		f_wo_status_box("",LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),16) + ' ' + i_unitization_type + ' Unitizing '+ string(num_nrows) + ' work orders... ' )	
	end if
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ":  " + i_unitization_type + " Unitizing "+ string(num_nrows) + " work orders... "
	of_log() 
end if 

return 1
end function

public function integer of_validate_wos_extension ();//////////////////////////////////////////////////////////////////////////////////
//The of_validate_wos_extension function will loop by work order:
//  1. f_unitization_audit
//  2. f_wo_audit_tax
//////////////////////////////////////////////////////////////////////////////////
string wo_list_sqls, error_message_tax, msg, rtn_string
string args[]
longlong i, num_wos, wo_id, rtn, rtn2, num_nrows, ret
uo_ds_top ds_wos_list

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": checking extensions..."
of_log() 

ds_wos_list = create uo_ds_top

wo_list_sqls = "select work_order_id from UNITIZE_WO_LIST_TEMP where error_msg is null and company_id = " + string(i_company_id)  
rtn_string = f_create_dynamic_ds(ds_wos_list, 'grid', wo_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_wos_list for wos extension check: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_wos = ds_wos_list.RowCount()

for i = 1 to num_wos

	wo_id = ds_wos_list.getitemnumber(i, 1)
	rtn = 0 //reset for each new work order
	rtn2 = 0
	
	//Call the f_unitization_audit function that can contain customized audits and defaults.
	//both AUTO and AUTO LATE need to use "AUTO"
	rtn = f_unitization_audit("AUTO", wo_id)
	
	if rtn <> 1 then
		msg = "Unitization 153: " + i_unitization_type + ": Error in the f_unitization_audit function."
		update UNITIZE_WO_LIST_TEMP 
		set rtn_code = :rtn, error_msg = :msg
		where company_id = :i_company_id
		and work_order_id = :wo_id
		and error_msg is null;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP f_unitization_audit of wo_id="+ string(wo_id) + ": " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			destroy ds_wos_list
			return -1
		end if		
		num_nrows = sqlca.sqlnrows    
		continue
	end if
   
	// Call Dynamic Validation option
	args[1] = string(wo_id)
	args[2] = string(i_company_id)
	
	ret = f_wo_validation_control(1056,args)
	
	if ret = -1 then
		f_pp_msgs("Work Order Control Automatic Unitization (Dynamic Validations) Failed for Work Order ID: " + string(wo_id) + ", Company ID:" + string(i_company_id))
		continue
	end if
	
	// Successful Validation
	commit;
	
	//Call the tax f_wo_audit_tax function that can contain customized audits and defaults.
	rtn2 = f_wo_audit_tax("AUTO101",  wo_id, error_message_tax )

	if rtn2 = -1 then
		if isnull(error_message_tax) then error_message_tax = ''
		msg = "Unitization 154: " + i_unitization_type + ": Error in the f_wo_audit_tax function. " + error_message_tax 
		update UNITIZE_WO_LIST_TEMP 
		set rtn_code = :rtn2, error_msg = :msg
		where company_id = :i_company_id
		and work_order_id = :wo_id
		and error_msg is null;
		
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP f_wo_audit_tax of wo_id="+ string(wo_id) + ": " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			destroy ds_wos_list
			return -1
		end if		
		num_nrows = sqlca.sqlnrows    
		continue
	end if 

next

destroy ds_wos_list
return 1
end function

public function integer of_clear_wo_list_curr_session ();/////////////////////////////////////////////////////////////////////////////////////////
//Clear out the UNITIZE_WO_LIST tables for next run.
//This function is called at the very end of unitization processing.
/////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows
string error_msg

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": clearing out wo list stage tables..."
of_log()

if i_single_wo then	
	delete from UNITIZE_WO_LIST 
	where company_id = :i_company_id
	and work_order_id = :i_single_wo_id
	and unitization_type = :i_unitization_type
	and db_session_id = :i_this_session_id;
	
	if sqlca.SQLCode < 0 then 	
		error_msg = ": ERROR: UNITIZE_WO_LIST reset for single wo: " + sqlca.SQLErrText
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  error_msg
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows     
	
else	
	delete from UNITIZE_WO_LIST a
	where a.company_id = :i_company_id
	and a.db_session_id = :i_this_session_id 
	and a.unitization_type = :i_unitization_type
	and exists (select 1 from UNITIZE_WO_LIST_TEMP b
					where a.work_order_id = b.work_order_id
					);

	if sqlca.SQLCode < 0 then 	
		error_msg = ": ERROR: unable to delete from UNITIZE_WO_LIST at completion: " + sqlca.SQLErrText
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  error_msg
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows     

end if

//clear out global temp table, which is specific to this session.
delete from UNITIZE_WO_LIST_TEMP;
	
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to delete from UNITIZE_WO_LIST_TEMP at completion: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

commit;
return 1
end function

public function integer of_clear_wo_list_prior_sessions ();longlong num_nrows
string clear_msg
boolean clear_result

//First remove stranded work orders from the UNITIZED_WO_LIST table for old connections that are not in any session or were killed sessions. 
clear_result =  f_pp_reset_disconnected('UNITIZE_WO_LIST', i_company_id, clear_msg)
if clear_result = false then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: Unable to clear out stranded work orders from prior Unitization process runs: " + clear_msg
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 	
	of_log() 
	rollback;		 
	return -1
end if 

SELECT userenv('sessionid') into :i_this_session_id FROM DUAL;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: Unable to determine database session_id: " + sqlca.SQLErrText
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 	
	of_log() 
	rollback;		 
	return -1
end if 
num_nrows = sqlca.sqlnrows	

if i_this_session_id <= 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: Invalid database session_id = : " + string(i_this_session_id) 
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization for company_id = " + string(i_company_id) 	
	of_log() 
	rollback;		 
	return -1
end if 

//a commit in the f_clear_wos_from_prior_sessions() function is good to have.
commit;

return 1
end function

public function integer of_equipment_ledger (longlong a_wo_id, longlong a_revision, longlong a_est_unit_item_option);//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//Updates for equipment ledger tables. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, check_count

//table_name: cpr_equip_estimate_relate
//table description: The CPR Equipment Estimate Relate table stores the relationship of Work Order Unit Estimates to Equipment Records.  
//This relationship is utilized while the Equipment Records are in a pending status.

 
//check if equipment ledger estimates for the revision exists.
check_count = 0
select count(1) into :check_count 
from cpr_equip_estimate_relate a
where a.work_order_id = :a_wo_id
and a.revision = :a_revision
and exists (select 1 from WO_ESTIMATE c
				where a.estimate_id = c.estimate_id
				and a.work_order_id = c.work_order_id
				and a.revision = c.revision
				and c.batch_unit_item_id is null
				);
					
if check_count = 0 then
	return 1
end if  

if a_est_unit_item_option = 1 then
	update cpr_equip_estimate_relate a
	set a.unit_item_id = (select b.unit_item_id 
								from unitized_work_order b
								where a.work_order_id = b.work_order_id
								and a.estimate_id = b.unit_estimate_id
								)
	where a.work_order_id = :a_wo_id
	and a.revision = :a_revision
	and exists (select 1 from WO_ESTIMATE c
					where a.estimate_id = c.estimate_id
					and a.work_order_id = c.work_order_id
					and a.revision = c.revision
					and c.batch_unit_item_id is null
					);
				
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[]
		i_msg[1]  =   ": ERROR: update cpr_equip_estimate_relate (1 for 1): " + sqlca.SQLErrText
		of_log()
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
else
	
	//summarized option not allowed to be used for Equip Ledger
	UPDATE UNITIZE_WO_LIST_TEMP a
	SET a.rtn_code = -1, a.error_msg = 'Unitization 288b: Work orders with Equip Ledger estimates must use One for One Unit Item Est Option. *Change Unit Item Est Level Option and Reset Unitization.*'
	where a.company_id = :i_company_id
	and a.error_msg is null
	and a.work_order_id = :a_wo_id;
				
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[]
		i_msg[1]  =   ": ERROR: update UNITIZE_WO_LIST_TEMP (288b): " + sqlca.SQLErrText
		of_log()
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
	
end if

i_equip_ledger_processed = true

return 1
end function

public function integer of_wo_checks_and_updates ();//////////////////////////////////////////////////////////////////////////////////////////////////////////
//loop over work orders being unitized to process the following one work order at a time
//  1.  required class codes
//  2.  equipment ledger
//////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong check_cc, revision, rtn1, rtn2, est_unit_item_option, has_elig_adds
string msg_txt, wo_list_sqls, rtn_string
longlong wo_id, i, num_wos, num_nrows, of_rtn
uo_ds_top ds_wos_list 

//check for required class codes
check_cc = 0
select count(*) into :check_cc  
from 
	(select  class_code_id from class_code where cpr_indicator = 1 and  required  = 1
	union
	select class_code_id from class_code_display where required = 1); 

if i_cpr_equip_type_num > 0 then
	//check if any of the work orders being unitized right now have equipment ledger estimates.
	//re-initialize i_cpr_equip_type_num based on this new check that is specific for the work orders.
	i_cpr_equip_type_num = 0
	select count(1) into :i_cpr_equip_type_num
	from UNITIZE_WO_LIST_TEMP a
	where a.company_id = :i_company_id
	and a.error_msg is null
	and exists (select 1
				from cpr_equip_estimate_relate b, wo_estimate c
				where a.work_order_id = b.work_order_id 
				and b.estimate_id = c.estimate_id
				and b.revision = c.revision
				and b.work_order_id = c.work_order_id
				and a.max_revision = c.revision
				and c.batch_unit_item_id is null
				);
	//if there are no work orders being unitized right now with equipment ledger estimates, then "cpr_equip_type_num" variable will be 0 and not trigger the equip ledger code
end if  
			
if check_cc > 0 and i_cpr_equip_type_num > 0 then
	msg_txt = ": checking required class codes and eq ledger..."
elseif check_cc > 0 and i_cpr_equip_type_num = 0 then
	msg_txt = ": checking required class codes..."
elseif check_cc = 0 and i_cpr_equip_type_num > 0 then
	msg_txt = ": checking equip ledger..."
else
	//nothing to do
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  msg_txt
of_log() 

if check_cc > 0 then
	i_ds_dw_required_cc = CREATE uo_ds_top
	i_ds_dw_required_cc.DataObject = "dw_required_class_codes"
	i_ds_dw_required_cc.SetTransObject(sqlca)
end if 

ds_wos_list = create uo_ds_top

wo_list_sqls = "select work_order_id, max_revision, est_unit_item_option, has_elig_adds from UNITIZE_WO_LIST_TEMP where error_msg is null and company_id = " + string(i_company_id)  
rtn_string = f_create_dynamic_ds(ds_wos_list, 'grid', wo_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_wos_list for checks and updates: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_wos = ds_wos_list.RowCount()

for i = 1 to num_wos

	wo_id = ds_wos_list.getitemnumber(i, 1)
	has_elig_adds = ds_wos_list.getitemnumber(i, 4)
	
	if check_cc > 0 and has_elig_adds = 1 then
		rtn1 = of_unit_item_class_codes_required(wo_id)
	
		if rtn1 <> 1 then
			//errors are handled inside function call
			destroy ds_wos_list			
			destroy i_ds_dw_required_cc			
			return -1
		end if 
	end if 
	
	if i_cpr_equip_type_num > 0 then
		revision =  ds_wos_list.getitemnumber(i, 2)
		est_unit_item_option =  ds_wos_list.getitemnumber(i, 3)
		
		rtn2 = of_equipment_ledger(wo_id, revision, est_unit_item_option)
	
		if rtn2 <> 1 then
			//errors are handled inside function call
			destroy ds_wos_list
			
			if check_cc > 0 then 
				destroy i_ds_dw_required_cc
			end if 
			
			return -1
		end if 
	end if 
	
next 
	
destroy ds_wos_list

if check_cc > 0 then 
	destroy i_ds_dw_required_cc
end if 

//Handle errors identified in the default functions above.  
of_rtn = of_errors('WO')
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

return 1

end function

public function integer of_validate_wos_pending ();longlong num_nrows 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": checking pend transactions..."
of_log() 

//check for addition pending transactions
UPDATE UNITIZE_WO_LIST_TEMP a
SET a.rtn_code = -1, a.error_msg = 'Unitization 150a: '||to_char((select count(1) from PEND_TRANSACTION b
																where a.work_order_number = b.work_order_number
																and a.company_id = b.company_id
																and upper(b.activity_code) like '%ADD%' 
																group by b.work_order_number
																))
												||' addition pending transaction(s) found. *Post or delete prior made pending transactions.*'
where a.company_id = :i_company_id
and a.error_msg is null
and exists (select 1 from PEND_TRANSACTION b
				where a.work_order_number = b.work_order_number
				and a.company_id = b.company_id
				and upper(b.activity_code) like '%ADD%'  
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP pend trans check: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//check for unitization pending transactions
UPDATE UNITIZE_WO_LIST_TEMP a
SET a.rtn_code = -1, a.error_msg = 'Unitization 150b: '||to_char((select count(1) from PEND_TRANSACTION b
																where a.work_order_number = b.work_order_number
																and a.company_id = b.company_id
																and b.ldg_depr_group_id < 0 
																group by b.work_order_number
																))
												||' unitization pending transaction(s) found. *Post or delete prior made pending transactions.*'
where a.company_id = :i_company_id
and a.error_msg is null
and exists (select 1 from PEND_TRANSACTION b
				where a.work_order_number = b.work_order_number
				and a.company_id = b.company_id
				and b.ldg_depr_group_id < 0  
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP pend trans check2: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    


return 1
end function

public function integer of_unit_item_ocr_gainloss ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Original Cost Retirement records have a corresponding Unit Item that exist in the UNITIZE_ELIG_UNITS_TEMP table that impact Unitization of RWIP charges.
//Make sure the OCR units have proper data validation to avoid Unitization errors with the RWIP unitization.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn
string error_message

if i_elig_ocr = 0 then
	return 1
end if

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": validating gain loss retires..."
of_log() 

///////////////////Unit level validations//////////////////////////


//Detect badly interfaced OCR Unit Items with a GL (gain loss) activity code that is missing asset_id data.
error_message = 'Unitization 282: Gain Loss Retirements must have an Asset_ID provided for unit_item_id = '

insert into UNITIZE_WO_STG_UNITS_TEMP (company_id, work_order_id, new_unit_item_id, error_msg)
select a.company_id, b.work_order_id, b.unit_item_id, substrb(:error_message||to_char(b.unit_item_id)||' *Delete and re-make the Gain Loss Retirement as needed.*', 1, 2000)
from UNITIZE_WO_LIST_TEMP a, UNITIZE_ELIG_UNITS_TEMP b, CHARGE_GROUP_CONTROL c 
where a.work_order_id = b.work_order_id 
and a.company_id = :i_company_id  
and a.has_gain_loss_ocr = 1
and b.work_order_id = c.work_order_id
and b.unit_item_id = c.unit_item_id
and nvl(c.pend_transaction,0) = 0
and c.batch_unit_item_id is null 
and c.expenditure_type_id = 2
and c.charge_type_id = :i_ocr_charge_type_id  /*OCR*/
and trim(b.activity_code) in ('URGL', 'SAGL')
and b.ldg_asset_id is null 
and not exists (select 1 from UNITIZE_WO_STG_UNITS_TEMP c
					where b.work_order_id = c.work_order_id
					and b.unit_item_id = c.new_unit_item_id
					);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert gain loss no asset_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		  
	return -1
end if		
num_nrows = sqlca.sqlnrows     

////comment out below validation.  even the manual fix for bad OCR is very difficult.  just ignore this bad data condition and avoid "subquery returns more than one row" by using min().
/*
////OCR records are supposed to have unique records by the following:
////1.  charge_id
////2.  charge_group_id
////3.  unit_item_id
////However, the expected rules of OCR records are not always true due to bad data SQL updates, OCR interfaces or other code issues.
////For example, a work order's single OCR record should only have UNIT_ITEM_ID = 18 as a single row in the CHARGE_GROUP_CONTROL table for the OCR CHARGE_TYPE_ID.
////When OCR data is bad, the UNIT_ITEM_ID = 18 will have multiple rows in the CHARGE_GROUP_CONTROL table with the OCR CHARGE_TYPE_ID, 
////        when there should be separate UNIT_ITEM_IDS for each of the OCR CHARGE_GROUP_IDs.
////Note:  it is okay for UNIT_ITEM_ID = 18 to have multiple rows in the CHARGE_GROUP_CONTROL table for non-OCR CHARGE_TYPE_IDs (these would be allocated RWIP charges).
////Need to detect these OCR data errors in order to avoid the oralce error, subquery returns more than one row, for OCR related SQLs when bad OCR data exists.
//
////the OCR unit items are not staged in unitization.  they are prior made unit items.
//error_message = 'Unitization 281: Error: Original Cost Retires cannot have more than 1 charge_group_id. Data correction required for unit_item_id = '
//insert into UNITIZE_WO_STG_UNITS_TEMP (company_id, work_order_id, new_unit_item_id, error_msg)
//select a.company_id, b.work_order_id, b.unit_item_id, substrb(:error_message||to_char(b.unit_item_id)||' *Delete and re-make the Retirements as needed.*', 1, 2000)
//from UNITIZE_WO_LIST_TEMP a, UNITIZED_WORK_ORDER b, CHARGE_GROUP_CONTROL c 
//where a.work_order_id = b.work_order_id 
//and a.company_id = :i_company_id
//and b.work_order_id = c.work_order_id
//and b.unit_item_id = c.unit_item_id
//and c.unit_item_id > 0 
//and c.batch_unit_item_id is null
//and c.expenditure_type_id = 2
//and c.charge_type_id in (select charge_type_id from charge_type where processing_type_id = 1) /*OCR*/  
//group by a.company_id, b.work_order_id, b.unit_item_id
//having count(1) > 1;
//
//if sqlca.SQLCode < 0 then 
//	i_msg[] = i_null_arrary[] 
//	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert bad OCR counts: " + sqlca.SQLErrText
//	of_log() 
//	rollback;		  
//	return -1
//end if		
//num_nrows = sqlca.sqlnrows     
*/

if num_nrows > 0 then
	//Handle errors identified in the validate function above.  
	of_rtn = of_errors('UNITS')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if 

return 1
end function

public function integer of_allocation_refresh_staged_data ();longlong num_nrows, of_rtn
		
i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   resetting alloc data..."
of_log() 

if i_first_priority then
	//Get Max(charge_group_id) from CHARGE_GROUP_CONTROL for each work order
	update UNITIZE_WO_LIST_TEMP a
	set a.max_charge_group_id = (select max(b.charge_group_id)
											from CHARGE_GROUP_CONTROL b
											where a.work_order_id = b.work_order_id
											and b.charge_group_id >= 0
											group by b.work_order_id
									)
	where a.company_id = :i_company_id
	and a.error_msg is null;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update max_charge_group_id: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    
	
	//update for nulls
	update UNITIZE_WO_LIST_TEMP a
	set a.max_charge_group_id = 0
	where a.company_id = :i_company_id
	and a.error_msg is null
	and a.max_charge_group_id is null;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update max_charge_group_id(null): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows   

else

	//Update the new Max(charge_group_id) from UNITIZE_ALLOC_CGC_INSERTS_TEMP for each work order 
	update UNITIZE_WO_LIST_TEMP a
	set a.max_charge_group_id = (select max(b.new_charge_group_id)
											from UNITIZE_ALLOC_CGC_INSERTS_TEMP b
											where a.work_order_id = b.work_order_id
											group by b.work_order_id
									)
	where a.company_id = :i_company_id
	and a.error_msg is null
	and exists (select 1 from UNITIZE_ALLOC_CGC_INSERTS_TEMP b
					where a.work_order_id = b.work_order_id 
					);
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update new max_charge_group_id: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    
	
	//update for nulls
	update UNITIZE_WO_LIST_TEMP a
	set a.max_charge_group_id = 0
	where a.company_id = :i_company_id
	and a.error_msg is null
	and a.max_charge_group_id is null;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update new max_charge_group_id(null): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows   

end if 

delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_CGC_INSERTS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

delete from UNITIZE_ALLOC_ERRORS_TEMP;
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_ERRORS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

return 1
end function

public function integer of_allocate_charges_by_acct (longlong a_priority_number);//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//This function creates the allocation data that will be inserted into CHARGE_GROUP_CONTROL.
//The data saved in the UNITIZE_ALLOC_CGC_INSERTS_TEMP table will alter be saved into the  UNITIZE_ALLOC_CGC_INSERTS_ARC table. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   allocating by account..."
of_log() 

//the old auto101 code does not join for BUS_SEGMENT_ID. there is an expectation for the UTILITY_ACCOUNT_ID to be unique, even though the
//   datamodel does not actually have UTILITY_ACCOUNT_ID as a unique identifier.

//alloc_ratio_by_sub
insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
	(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id,  
	description, ratio_used, quantity, allocation_priority, 
	unitization_summary, book_summary_name, unit_item_id,
	amount  )
select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id,
	substrb(decode(a.expenditure_type_id, 2, 'Allocated Removal/Salvage', 'Allocated '||a.book_summary_name), 1, 35), 'alloc_ratio_by_sub', 0 quantity, a.allocation_priority, 
	a.unitization_summary, a.book_summary_name, b.unit_item_id,
	
		round(a.amount * b.alloc_ratio_by_sub, 2) - 
		decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
		sum(round(a.amount * b.alloc_ratio_by_sub,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
	
from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
where a.work_order_id = b.work_order_id  
and c.work_order_id = b.work_order_id
and c.company_id = :i_company_id
and c.error_msg is null
and a.allocation_priority = :a_priority_number 
and a.alloc_id = b.alloc_id 
and a.allocation_priority = b.alloc_priority
and a.expenditure_type_id = b.expenditure_type_id
and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id <> 1)
and a.unit_item_id is null
and a.batch_unit_item_id is null 
and a.utility_account_id = b.utility_account_id  /*required*/
and a.sub_account_id = b.sub_account_id  /*required*/
and b.unitize_by_account = 1
and not exists (select 1
					from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
					where x.work_order_id = a.work_order_id
					and a.allocation_priority = x.allocation_priority 
					and a.alloc_id = x.alloc_id 
					and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
					and x.company_id = :i_company_id
					); 
	
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP for alloc_ratio_by_sub: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//alloc_ratio_by_ut
insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
	(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id,  
	description, ratio_used, quantity, allocation_priority, 
	unitization_summary, book_summary_name, unit_item_id,
	amount  )
select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id, 
	substrb(decode(a.expenditure_type_id, 2, 'Allocated Removal/Salvage', 'Allocated '||a.book_summary_name), 1, 35), 'alloc_ratio_by_ut', 0 quantity, a.allocation_priority, 
	a.unitization_summary, a.book_summary_name, b.unit_item_id,
	
		round(a.amount * b.alloc_ratio_by_ut, 2) - 
		decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
		sum(round(a.amount * b.alloc_ratio_by_ut,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
	
from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
where a.work_order_id = b.work_order_id  
and c.work_order_id = b.work_order_id
and c.company_id = :i_company_id
and c.error_msg is null
and a.allocation_priority = :a_priority_number 
and a.alloc_id = b.alloc_id 
and a.allocation_priority = b.alloc_priority
and a.expenditure_type_id = b.expenditure_type_id
and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id <> 1)
and a.unit_item_id is null
and a.batch_unit_item_id is null 
and a.utility_account_id = b.utility_account_id  /*required*/
and a.sub_account_id is null
and b.unitize_by_account = 1
and not exists (select 1
					from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
					where x.work_order_id = a.work_order_id
					and a.allocation_priority = x.allocation_priority 
					and a.alloc_id = x.alloc_id 
					and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
					and x.company_id = :i_company_id
					); 
	
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP for alloc_ratio_by_ut: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Check for inability to allocate for Unitize by Account = Yes.  
//It is very important to perform the error validations immediately after allocation.
//If this error validation is not done right now, the Allocate Remaining functionaly will perform Charge Group allocation across un-matching Sub/UT accounts. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//1.  It is an allocation error for failing to find a matching Unit Item for a particular UTILITY_ACCOUNT_ID with SUB_ACCOUNT_ID = not null when the wo has "Unitize by Account" = "Yes".
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id, substrb(decode(a.expenditure_type_id, 1, 
'Unitization 135: Cannot allocate Additions for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'" with Unitize by Account = Yes.  Alloc Basis for included Charge Type does not match or No Unit Item found for utility_account_id = '||to_char(a.utility_account_id)||' and sub_account_id = '||to_char(a.sub_account_id)||'. *Change Unitize by Account to "No" or Review UT on Units and Alloc Basis*',
'Unitization 135R: Cannot allocate RWIP for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'" with Unitize by Account = Yes.  Alloc Basis for included Charge Type does not match or No Unit Item found for utility_account_id = '||to_char(a.utility_account_id)||' and sub_account_id = '||to_char(a.sub_account_id)||'. *Change Unitize by Account to "No" or Review UT on Units and Alloc Basis*' 
), 1, 2000)
from CHARGE_GROUP_CONTROL a, UNITIZE_WO_LIST_TEMP b, CHARGE_TYPE c, UNIT_ALLOCATION d
where a.work_order_id = b.work_order_id
and b.company_id = :i_company_id
and a.allocation_priority = :a_priority_number   
and a.charge_type_id = c.charge_type_id
and a.alloc_id = d.alloc_id
and a.expenditure_type_id in (1,2)
and c.processing_type_id <> 1
and b.unitize_by_account = 1
and a.amount <> 0 
and a.batch_unit_item_id is null
and a.unit_item_id is null
and a.utility_account_id is not null  
and a.sub_account_id is not null  
and b.error_msg is null
and not exists (select 1
					from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
					where x.work_order_id = a.work_order_id
					and a.allocation_priority = x.allocation_priority 
					and a.alloc_id = x.alloc_id 
					and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
					and x.company_id = :i_company_id
					); 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (135.1): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if
 
//2.  It is an allocation error for failing to find a matching Unit Item for a particular UTILITY_ACCOUNT_ID with SUB_ACCOUNT_ID = null when the wo has "Unitize by Account" = "Yes".
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id,   
substrb(decode(a.expenditure_type_id, 1, 
'Unitization 135: Cannot allocate Additions for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'" with Unitize by Account = Yes.  Alloc Basis for included Charge Type does not match or No Unit Item found for utility_account_id = '||to_char(a.utility_account_id)||'. *Change Unitize by Account to "No" or Review UT on Units and Alloc Basis*',
'Unitization 135R: Cannot allocate RWIP for Charge Type = "'||c.description||'", Priority = '||to_char(a.allocation_priority)||', Alloc Desc = "'||d.description||'" with Unitize by Account = Yes.  Alloc Basis for included Charge Type does not match or No Unit Item found for utility_account_id = '||to_char(a.utility_account_id)||'. *Change Unitize by Account to "No" or Review UT on Units and Alloc Basis*' 
), 1, 2000)
from CHARGE_GROUP_CONTROL a, UNITIZE_WO_LIST_TEMP b, CHARGE_TYPE c, UNIT_ALLOCATION d
where a.work_order_id = b.work_order_id
and b.company_id = :i_company_id
and a.allocation_priority = :a_priority_number   
and a.charge_type_id = c.charge_type_id
and a.alloc_id = d.alloc_id
and a.expenditure_type_id in (1,2)
and c.processing_type_id <> 1
and b.unitize_by_account = 1
and a.amount <> 0 
and a.batch_unit_item_id is null
and a.unit_item_id is null
and a.utility_account_id is not null  
and a.sub_account_id is null  
and b.error_msg is null
and NOT EXISTS (select 1 from UNITIZE_ALLOC_CGC_INSERTS_TEMP e
					 where a.work_order_id = e.work_order_id  
					 and a.expenditure_type_id = e.expenditure_type_id
				 	 and a.allocation_priority = e.allocation_priority 
				 	 and a.alloc_id = e.alloc_id 
					 and e.orig_charge_group_id = a.charge_group_id  /*already allocated charges*/ 
					 and e.company_id = :i_company_id 
					 );

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (135.2): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if

return 1
end function

public function integer of_unitization_final_updates ();//final updates
longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": updating unitization tables..."
of_log() 

//Unitized and Sent to Pending
update CHARGE_GROUP_CONTROL a
set a.pend_transaction = 100
where a.expenditure_type_id in (1,2)
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and nvl(a.pend_transaction,0) = 0
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
				where a.work_order_id = x.work_order_id
				and x.company_id = :i_company_id
				and x.error_msg is null
				); 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating CHARGE_GROUP_CONTROL.pend_transaction = 100: " + sqlca.SQLErrText
	of_log() 
	rollback;		  
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Gain Loss OCR processed in Unitization
if i_elig_ocr > 0 then
	update CHARGE_GROUP_CONTROL a
	set a.pend_transaction = 100
	where a.expenditure_type_id = 2
	and a.charge_type_id = :i_ocr_charge_type_id  /*OCR*/ 
	and nvl(a.pend_transaction,0) = 0
	and exists (select 1 from UNITIZE_PEND_TRANS_TEMP  b
					where a.work_order_id = b.work_order_id
					and b.company_id = :i_company_id
					and b.unit_item_id = a.unit_item_id
					and b.ldg_activity_id = a.charge_group_id
					)
	and exists (select 1 from UNITIZE_WO_LIST_TEMP x
				where a.work_order_id = x.work_order_id
				and x.company_id = :i_company_id
				and x.has_gain_loss_ocr = 1
				and x.error_msg is null);
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating CHARGE_GROUP_CONTROL.pend_transaction = 100 OCR: " + sqlca.SQLErrText
		of_log() 
		rollback;		  
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
end if //if i_elig_ocr > 0 then

//update the Max(unit_item_id) from each work order
update UNITIZE_WO_LIST_TEMP a
set a.max_unit_item_id = (select max(b.unit_item_id)
										from UNITIZE_PEND_TRANS_TEMP b
										where a.work_order_id = b.work_order_id
										group by b.work_order_id
								)
where a.company_id = :i_company_id
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP update max_unit_item_id (last): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    
		
update CHARGE_GROUP_CONTROL a
set a.batch_unit_item_id = (select b.max_unit_item_id
										from UNITIZE_WO_LIST_TEMP b
										where a.work_order_id = b.work_order_id 
										and b.company_id = :i_company_id
										and b.error_msg is null 
								)
where a.expenditure_type_id in (1,2)
and a.batch_unit_item_id is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
				where a.work_order_id = x.work_order_id
				and x.company_id = :i_company_id
				and x.error_msg is null
				); 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating CHARGE_GROUP_CONTROL.batch_unit_item_id: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

insert into WO_UNIT_ITEM_PEND_TRANS (work_order_id, unit_item_id, pend_trans_id)
select work_order_id, unit_item_id, pend_trans_id
from UNITIZE_PEND_TRANS_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into wo_unit_item_pend_trans: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then	
	//Regular Close
		
	update WO_ESTIMATE a
	set a.batch_unit_item_id = (select b.max_unit_item_id
											from UNITIZE_WO_LIST_TEMP b
											where a.work_order_id = b.work_order_id 
											and b.company_id = :i_company_id
											and b.error_msg is null 
									)
	where a.expenditure_type_id in (1,2)
	and a.batch_unit_item_id is null
	and exists (select 1 from UNITIZE_WO_LIST_TEMP x
					where a.work_order_id = x.work_order_id
					and x.company_id = :i_company_id
					and x.error_msg is null
					); 
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating WO_ESTIMATE.batch_unit_item_id: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  
	
	//update wo_status_id	
	update work_order_control a 
	set a.wo_status_id = 6
	where a.company_id = :i_company_id
	and a.funding_wo_indicator = 0 
	and a.wo_status_id <> 7
	and exists (select 1 
				from UNITIZE_WO_LIST_TEMP b
				where b.work_order_id = a.work_order_id 
				and b.error_msg is null
				and b.closing_option_id in (2,4)
				and exists (select 1 from UNITIZE_PEND_TRANS_TEMP c where b.work_order_id = c.work_order_id)
				) ;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: work_order_control.wo_status_id: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    
end if 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////comment out:  Do not perform these updates.  this bad data condition needs to be addressed on a case by case.
////Maint 38140:  batch_unit_item_id:  0 is not a valid value for CHARGE_GROUP_CONTROL
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
////update to null where there is no matching unit item records in WO_UNIT_ITEM_PEND_TRANS table, indicating not yet unitized.
//update charge_group_control a
//set a.batch_unit_item_id = null
//where a.expenditure_type_id in (1,2)
//and a.batch_unit_item_id = 0
//and nvl(a.pend_transaction, 0) = 0
//and exists (select 1 from UNITIZE_WO_LIST_TEMP b
//				where a.work_order_id = b.work_order_id
//				and b.company_id = :i_company_id
//				and b.error_msg is null
//				)
//and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS c
//					where a.work_order_id = c.work_order_id
//					and a.unit_item_id = c.unit_item_id
//					); 
//
//if sqlca.SQLCode < 0 then 
//	i_msg[] = i_null_arrary[] 
//	i_msg[1] =  ": ERROR: update charge_group_control.batch_unit_item_id = null: " + sqlca.SQLErrText
//	of_log() 
//	rollback;		 
//	return -1
//end if		
//num_nrows = sqlca.sqlnrows 
//
////update to -1 where there is matching unit item records in WO_UNIT_ITEM_PEND_TRANS table, indicating already unitized.
//update charge_group_control a
//set a.batch_unit_item_id = -1
//where a.expenditure_type_id in (1,2)
//and a.batch_unit_item_id = 0
//and exists (select 1 from UNITIZE_WO_LIST_TEMP b
//				where a.work_order_id = b.work_order_id
//				and b.company_id = :i_company_id
//				and b.error_msg is null
//				)
//and exists (select 1 from WO_UNIT_ITEM_PEND_TRANS c
//					where a.work_order_id = c.work_order_id
//					and a.unit_item_id = c.unit_item_id
//					); 
//
//if sqlca.SQLCode < 0 then 
//	i_msg[] = i_null_arrary[] 
//	i_msg[1] =  ": ERROR: update charge_group_control.batch_unit_item_id = -1: " + sqlca.SQLErrText
//	of_log() 
//	rollback;		 
//	return -1
//end if		
//num_nrows = sqlca.sqlnrows 

return 1
end function

public function integer of_unit_item_late_rwip_reset ();//Maint 35663:  need to remove old and unposted RWIP unitization data created from old code
longlong num_nrows, of_rtn

if i_elig_rwip_wos = 0 then
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": resetting old unit items (rwip)..."
of_log() 

//delete unposted allocated charges groups created from allocation
delete from CHARGE_GROUP_CONTROL a
where a.expenditure_type_id = 2 
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and a.batch_unit_item_id is null
and a.unit_item_id > 0
and a.alloc_id is null
and a.unit_target_id is null
and a.description = 'Allocated Removal/Salvage' 			
and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
					where a.work_order_id = b.work_order_id
					 and a.unit_item_id = b.unit_item_id
					 )					 
and exists (select 1 from UNITIZE_WO_LIST_TEMP c
				where a.work_order_id = c.work_order_id
				and c.company_id = :i_company_id
				and c.has_elig_rwip = 1
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: CHARGE_GROUP_CONTROL rwip reset(1): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//maint 44146:  delete unposted allocated charges groups created from targets
delete from CHARGE_GROUP_CONTROL a
where a.expenditure_type_id = 2 
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and a.batch_unit_item_id is null
and a.unit_item_id > 0
and a.alloc_id is null
and a.unit_target_id is not null 	
and a.description like '(%' 
and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
					where a.work_order_id = b.work_order_id
					 and a.unit_item_id = b.unit_item_id
					 )					 
and exists (select 1 from UNITIZE_WO_LIST_TEMP c
				where a.work_order_id = c.work_order_id
				and c.company_id = :i_company_id
				and c.has_elig_rwip = 1
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: CHARGE_GROUP_CONTROL rwip reset(1): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    
			 
//reset the charge groups that link back to cwip_charge (do not include a filter for unit_target_id:  using targets is like both direct assign and allocation)
update CHARGE_GROUP_CONTROL a
set a.unit_item_id = null 
where a.expenditure_type_id = 2 
and a.unit_item_id is not null  /*incude 0*/ 
and a.alloc_id is not null  /*the ones that link to cwip_charge*/
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and a.batch_unit_item_id is null	
and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
				where a.work_order_id = b.work_order_id
				 and a.unit_item_id = b.unit_item_id
				 )			 
and exists (select 1 from UNITIZE_WO_LIST_TEMP c
			where a.work_order_id = c.work_order_id
			and c.company_id = :i_company_id
			and c.has_elig_rwip = 1
			);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: CHARGE_GROUP_CONTROL rwip reset(2): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

//maint 44146:  delete from child table of UNITIZED_WORK_ORDER first
delete from UNITIZATION_TARGET t
where (t.work_order_id, t.unit_item_id) in (
	select a.work_order_id, a.unit_item_id
	 from UNITIZED_WORK_ORDER a
	where a.company_id = :i_company_id
	and a.unit_item_id > 0  /*exclude 0*/
	and trim(a.activity_code) in ('URET','MRET','URGL','SALE','SAGL') 
	and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
					where a.work_order_id = b.work_order_id
					 and a.unit_item_id = b.unit_item_id
					 )
	and not exists (select 1 from CHARGE_GROUP_CONTROL c
					where a.work_order_id = c.work_order_id
					 and a.unit_item_id = c.unit_item_id
					 )			 
	and exists (select 1 from UNITIZE_WO_LIST_TEMP d
				where a.work_order_id = d.work_order_id
				and d.company_id = :i_company_id
				and d.has_elig_rwip = 1
				)
	);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZATION_TARGET: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

delete from UNIT_ITEM_CLASS_CODE t
where (t.work_order_id, t.unit_item_id) in (
	select a.work_order_id, a.unit_item_id
	 from UNITIZED_WORK_ORDER a
	where a.company_id = :i_company_id
	and a.unit_item_id > 0  /*exclude 0*/
	and trim(a.activity_code) in ('URET','MRET','URGL','SALE','SAGL') 
	and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
					where a.work_order_id = b.work_order_id
					 and a.unit_item_id = b.unit_item_id
					 )
	and not exists (select 1 from CHARGE_GROUP_CONTROL c
					where a.work_order_id = c.work_order_id
					 and a.unit_item_id = c.unit_item_id
					 )			 
	and exists (select 1 from UNITIZE_WO_LIST_TEMP d
				where a.work_order_id = d.work_order_id
				and d.company_id = :i_company_id
				and d.has_elig_rwip = 1
				)
	);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNIT_ITEM_CLASS_CODE rwip reset: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

delete from unitized_work_order_sl_basis t
where (t.work_order_id, t.unit_item_id) in (
	select a.work_order_id, a.unit_item_id
	 from UNITIZED_WORK_ORDER a
	where a.company_id = :i_company_id
	and a.unit_item_id > 0  /*exclude 0*/
	and trim(a.activity_code) in ('URET','MRET','URGL','SALE','SAGL') 
	and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
					where a.work_order_id = b.work_order_id
					 and a.unit_item_id = b.unit_item_id
					 )
	and not exists (select 1 from CHARGE_GROUP_CONTROL c
					where a.work_order_id = c.work_order_id
					 and a.unit_item_id = c.unit_item_id
					 )			 
	and exists (select 1 from UNITIZE_WO_LIST_TEMP d
				where a.work_order_id = d.work_order_id
				and d.company_id = :i_company_id
				and d.has_elig_rwip = 1
				)
	);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from unitized_work_order_sl_basis rwip reset: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

delete from unitized_work_order_sl t
where (t.work_order_id, t.unit_item_id) in (
	select a.work_order_id, a.unit_item_id
	 from UNITIZED_WORK_ORDER a
	where a.company_id = :i_company_id
	and a.unit_item_id > 0  /*exclude 0*/
	and trim(a.activity_code) in ('URET','MRET','URGL','SALE','SAGL') 
	and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
					where a.work_order_id = b.work_order_id
					 and a.unit_item_id = b.unit_item_id
					 )
	and not exists (select 1 from CHARGE_GROUP_CONTROL c
					where a.work_order_id = c.work_order_id
					 and a.unit_item_id = c.unit_item_id
					 )			 
	and exists (select 1 from UNITIZE_WO_LIST_TEMP d
				where a.work_order_id = d.work_order_id
				and d.company_id = :i_company_id
				and d.has_elig_rwip = 1
				)
	);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from unitized_work_order_sl rwip reset: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

delete from unitized_work_order_memo t
where (t.work_order_id, t.unit_item_id) in (
	select a.work_order_id, a.unit_item_id
	 from UNITIZED_WORK_ORDER a
	where a.company_id = :i_company_id
	and a.unit_item_id > 0  /*exclude 0*/
	and trim(a.activity_code) in ('URET','MRET','URGL','SALE','SAGL') 
	and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
					where a.work_order_id = b.work_order_id
					 and a.unit_item_id = b.unit_item_id
					 )
	and not exists (select 1 from CHARGE_GROUP_CONTROL c
					where a.work_order_id = c.work_order_id
					 and a.unit_item_id = c.unit_item_id
					 )			 
	and exists (select 1 from UNITIZE_WO_LIST_TEMP d
				where a.work_order_id = d.work_order_id
				and d.company_id = :i_company_id
				and d.has_elig_rwip = 1
				)
	);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from unitized_work_order_memo rwip reset: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   
				 
delete from UNITIZED_WORK_ORDER a
where a.company_id = :i_company_id
and a.unit_item_id > 0  /*exclude 0*/
and trim(a.activity_code) in ('URET','MRET','URGL','SALE','SAGL') 
and not exists (select 1 from WO_UNIT_ITEM_PEND_TRANS b
				where a.work_order_id = b.work_order_id
				 and a.unit_item_id = b.unit_item_id
				 )
and not exists (select 1 from CHARGE_GROUP_CONTROL c
				where a.work_order_id = c.work_order_id
				 and a.unit_item_id = c.unit_item_id
				 )			 
and exists (select 1 from UNITIZE_WO_LIST_TEMP d
			where a.work_order_id = d.work_order_id
			and d.company_id = :i_company_id
			and d.has_elig_rwip = 1
			);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZED_WORK_ORDER rwip reset(3): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//get the earliest batch_unit_item_id for RWIP Unit Items for the work orders being unitized
update UNITIZE_WO_LIST_TEMP c
set c.MIN_RWIP_BATCH_UNIT_ID = (select min(batch_unit_item_id)  
												from CHARGE_GROUP_CONTROL a, UNITIZED_WORK_ORDER b
												where c.work_order_id = a.work_order_id 
												and a.work_order_id = b.work_order_id
												and a.unit_item_id = b.unit_item_id
												and a.batch_unit_item_id > 0  /*do not include fake or net $0 unitized records which are typcially negative*/
												and a.expenditure_type_id = 2
												and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
												and trim(b.activity_code) in ('URET','MRET','URGL','SALE','SAGL')
												group by a.work_order_id
												)
where c.company_id = :i_company_id
and c.has_elig_rwip = 1;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update UNITIZE_WO_LIST_TEMP.MIN_RWIP_BATCH_UNIT_ID: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//make sure their is a prior RWIP unitization history for late RWIP charges
update UNITIZE_WO_LIST_TEMP  
set error_msg = 'Unitization 173: Unable to create RWIP Unit Items from prior unitization postings. *Manual Unitization Recommended*'
where company_id = :i_company_id
and error_msg is null
and MIN_RWIP_BATCH_UNIT_ID is null
and has_elig_rwip = 1;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update UNITIZE_WO_LIST_TEMP.MIN_RWIP_BATCH_UNIT_ID: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

if num_nrows > 0 then
	//Handle errors identified in the tolerance checks above.  
	of_rtn = of_errors('WO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if 

//save this information into the Archive table
update UNITIZE_WO_LIST_ARC a
set a.MIN_RWIP_BATCH_UNIT_ID = 
									(select b.MIN_RWIP_BATCH_UNIT_ID
									from UNITIZE_WO_LIST_TEMP b
									where a.work_order_id = b.work_order_id
									and b.MIN_RWIP_BATCH_UNIT_ID is not null
									)
where a.company_id = :i_company_id
and a.db_session_id = :i_this_session_id 
and a.unit_closed_month_number = :i_month_number
and a.run_time_string = :i_run_time_string
and a.run = :i_run
and a.unitization_type = :i_unitization_type
and exists ( select 1
				from UNITIZE_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id
				and b.MIN_RWIP_BATCH_UNIT_ID is not null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_ARC for MIN_RWIP_BATCH_UNIT_ID data: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1
end function

public function integer of_unit_item_staging_late_adds ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Stages new Unit Items to be made for Late Additions during Late Charge Unitization.
//Use CPR tables to create new Unit Items for Late Additions.
//Only insert non-matching ASSET_ID minor unit items as new staged Units.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
longlong num_nrows
string control_value, error_text

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": evaluating new unit items (adds)..."
of_log() 

if i_elig_add_wos = 0 then
	return 1
end if 

//Use CPR tables to create Addition Minor Add unit items
//for Late Close Addition Unit Items, it is easy to avoid creating duplicate Unit Items by joining on the LDG_ASSET_ID.
//note:  if RU with asset_acct_meth_id = 5 incorrectly configured as non-memo would default to UADD
insert into UNITIZE_WO_STG_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_SOURCE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
ASSET_LOCATION_ID, SERIAL_NUMBER, GL_ACCOUNT_ID, QUANTITY, AMOUNT, 
PROPERTY_GROUP_ID, ACTIVITY_CODE, LDG_ASSET_ID, DESCRIPTION, SHORT_DESCRIPTION
) 
select distinct a.company_id, a.work_order_id, 'LATE', d.retirement_unit_id, d.utility_account_id, d.bus_segment_id, d.sub_account_id, 
d.asset_location_id, trim(d.serial_number), d.gl_account_id, 0 as quantity, 0 as amount, 
d.property_group_id, decode(p.asset_acct_meth_id,2,'MADD','UADD') activity_code, d.asset_id, substrb(d.long_description, 1, 254), 'Late Charge Unitization'
from UNITIZE_WO_LIST_TEMP a, cpr_activity c, cpr_ledger d, retirement_unit r, property_unit p 
where c.asset_id = d.asset_id
and a.work_order_number = c.work_order_number
and a.company_id = d.company_id 
and a.company_id = :i_company_id 
and a.has_elig_adds = 1
and d.retirement_unit_id > 5
and nvl(r.memo_only,0) = 0  /*do not use memo only retirement units*/
and trim(c.activity_code) in ('UADD','MADD','CFNU')
and r.retirement_unit_Id = d.retirement_unit_id
and r.property_unit_id = p.property_unit_id
and a.unitized_gl_account = d.gl_account_id  /*this join avoids creating minor additions for wip comp assets*/
and not exists (select 1 from unitized_work_order w
					where w.work_order_id = a.work_order_id
					and w.ldg_asset_id = d.asset_id
					and not exists (select 1 from wo_unit_item_pend_trans pt
										where pt.work_order_id = w.work_order_id
										and pt.unit_item_id = w.unit_item_id)
					)
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert from CPR Minor Adds: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1
end function

public function integer of_unit_item_staging_late_rwip ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Stages new Unit Items to be made for Late RWIP during Late Charge Unitization.
//Use CHARGE_GROUP_CONTROL & UNITIZED_WORK_ORDER tables to create new Unit Items for Late RWIP. 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong of_rtn,  num_nrows

if i_elig_rwip_wos = 0 then
	return 1
end if 

//first reset old unposted RWIP unitization data to remove old unused RWIP unit items
//of_errors('WO') called inside this function
of_rtn = of_unit_item_late_rwip_reset() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": evaluating new unit items (rwip)..."
of_log() 

//create new RWIP unit items 
//no need to worry about creating duplicate RWIP unit items, because a reset has occured above.
//the STATUS column must be updated with the original UNIT_ITEM_ID to link the new Unit Items to the original Unit Items, which is used later in Allocations.
insert into UNITIZE_WO_STG_UNITS_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_SOURCE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
ASSET_LOCATION_ID, SERIAL_NUMBER, GL_ACCOUNT_ID, QUANTITY, AMOUNT, 
PROPERTY_GROUP_ID, ACTIVITY_CODE, LDG_ASSET_ID, DESCRIPTION, SHORT_DESCRIPTION, STATUS, MIN_RWIP_BATCH_UNIT_ID
) 
select a.company_id, b.work_order_id, 'LATE', b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, 
b.asset_location_id, trim(b.serial_number), b.gl_account_id, 0 as quantity, 0 as amount, 
b.property_group_id, trim(b.activity_code), b.ldg_asset_id, b.description, 'Late Charge Unitization', b.unit_item_id, A.MIN_RWIP_BATCH_UNIT_ID
from UNITIZE_WO_LIST_TEMP a, UNITIZED_WORK_ORDER b
where a.work_order_id = b.work_order_id
and a.company_id = :i_company_id 
and a.error_msg is null
and a.has_elig_rwip = 1
and b.retirement_unit_id > 5
and trim(b.activity_code) in ('URET','MRET','URGL','SALE','SAGL') 
and exists (select 1 from CHARGE_GROUP_CONTROL w
				where w.work_order_id = b.work_order_id  
				 and w.unit_item_id = b.unit_item_id
				 and w.expenditure_type_id = 2
				 and w.charge_type_id <> :i_ocr_charge_type_id 
				 and w.batch_unit_item_id = a.min_rwip_batch_unit_id
				)   
;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP insert for RWIP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1
end function

public function integer of_unit_item_source_updates ();///////////////////////////////////////////////////////////////////////////////////////////////////
//Update the unit_item_source column on the UNITIZE_WO_LIST_TEMP global temp table
//////////////////////////////////////////////////////////////////////////////////////////////////

longlong num_nrows, of_rtn 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": updating unit item source..."
of_log() 

if i_unitization_type = 'AUTO LATE' or  i_unitization_type= 'AUTO LATE WO' then	
	
	//backfill the UNIT_ITEM_SOURCE as LATE for all late close work orders
	update UNITIZE_WO_LIST_TEMP a
	set a.unit_item_source = 'LATE'
	where a.company_id = :i_company_id;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP.UNIT_ITEM_SOURCE = LATE backfill: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows   
	
else
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//For Regular Close Units should only be created from Actuals or Estimates, not both.
	//////////////////////////////////////////////////////////////////////////////////////////////////

	//identify the wos with staged unit item from both sources
	update UNITIZE_WO_LIST_TEMP x
	set x.rtn_code = -1, x.error_msg = substrb('Unitization 283: unit item source cannot be both actuals and estimate. *Review actuals and estimates.*', 1, 2000)
	where x.company_id = :i_company_id 
	and x.error_msg is null
	and exists (select 1
					from UNITIZE_WO_STG_UNITS_TEMP a
					where a.work_order_id = x.work_order_id
					and a.company_id = :i_company_id 
					and a.unit_item_source = 'ESTIMATES'
					)		
	and exists (select 1 
					from UNITIZE_WO_STG_UNITS_TEMP b
					where b.work_order_id = x.work_order_id
					and b.company_id = :i_company_id 
					and b.unit_item_source = 'ACTUALS'
					)
	;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: updating source errorr msg on UNITIZE_WO_LIST_TEMP: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
	
	if num_nrows > 0 then
		//Handle errors identified in the check above.  
		of_rtn = of_errors('WO')
		if of_rtn <> 1 then
			//errors are handled inside function call
			return -1
		end if 
	end if 
	
	//need to backfill the REPLACEMENT_AMOUNT to null if it is zero
	update UNITIZE_WO_STG_UNITS_TEMP
	set replacement_amount = null
	where replacement_amount = 0
	and company_id = :i_company_id 
	and UNIT_ITEM_SOURCE  = 'ESTIMATES';
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: update UNITIZE_WO_STG_UNITS_TEMP replacement_amount = null: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    

	//backfill the UNIT_ITEM_SOURCE field based on the inserts above.  the work order's Unit Item Source will be used later for other validations.
	//using min() or max() has no impact as the source should be the same for all records per work order, but it allows the update succeed when there are multiple rows in the "STG" table per work order..
	update UNITIZE_WO_LIST_TEMP a
	set a.unit_item_source = (select max(b.unit_item_source) from UNITIZE_WO_STG_UNITS_TEMP b
										where a.work_order_id = b.work_order_id
										and b.company_id = :i_company_id
										group by b.work_order_id 
										)
	where a.company_id = :i_company_id;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP.UNIT_ITEM_SOURCE backfill 1: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    

	//backfill the UNIT_ITEM_SOURCE field for work orders that did not get any new Units made in this run for Actuals
	update UNITIZE_WO_LIST_TEMP a
	set a.unit_item_source = 'ACTUALS' 
	where a.company_id = :i_company_id
	and a.unit_item_source is null
	and exists (select 1 from unitized_work_order b
				where a.work_order_id = b.work_order_id
				and b.unit_item_id > 0
				and b.status is null
				and not exists (select 1 from wo_unit_item_pend_trans c
									where b.work_order_id = c.work_order_id
									and b.unit_item_id = c.unit_item_id
									)
					);
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP.UNIT_ITEM_SOURCE backfill 2: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    

	//backfill the UNIT_ITEM_SOURCE field for work orders that did not get any new Units made in this run for Estimates
	update UNITIZE_WO_LIST_TEMP a
	set a.unit_item_source = 'ESTIMATES' 
	where a.company_id = :i_company_id
	and a.unit_item_source is null
	and exists (select 1 from unitized_work_order b
				where a.work_order_id = b.work_order_id
				and b.unit_item_id > 0
				and b.status is not null 
				and not exists (select 1 from wo_unit_item_pend_trans c
									where b.work_order_id = c.work_order_id
									and b.unit_item_id = c.unit_item_id
									)
					);
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP.UNIT_ITEM_SOURCE backfill 3: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows    
end if 

//Save the data to the Archive table
update UNITIZE_WO_LIST_ARC a
set (a.unit_item_source, a.replace_amt_as_unit)= 
	(select b.unit_item_source, b.replace_amt_as_unit
	from UNITIZE_WO_LIST_TEMP b
	where a.work_order_id = b.work_order_id
	)
where a.company_id = :i_company_id
and a.db_session_id = :i_this_session_id 
and a.unit_closed_month_number = :i_month_number
and a.run_time_string = :i_run_time_string
and a.run = :i_run
and a.unitization_type = :i_unitization_type;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_ARC for unit_item_source data: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 


return 1
end function

public function integer of_unit_item_staging_reset ();//Reset the global temp table 
longlong num_nrows

delete from UNITIZE_WO_STG_UNITS_TEMP;
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_STG_UNITS_TEMP reset: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows   

return 1
end function

public function integer of_allocation_prepare_regular ();//the i_unitization_type should be regular close.  Late Close does not use Allocation Method configuation.
longlong num_nrows, of_rtn

if i_unitization_type = 'AUTO LATE' or i_unitization_type = 'AUTO LATE WO' then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: Non-Late updates not correct for Regular Auto Unitization."
	of_log() 
	rollback;		 
	return -1
end if 

delete from UNITIZE_ALLOC_EST_STG_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_EST_STG_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

delete from UNITIZE_ALLOC_EST_TEMP;
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_EST_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

delete from UNITIZE_ALLOC_STND_TEMP;
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_STND_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//reset the table first.
delete from UNITIZE_ALLOC_LIST_TEMP;
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_LIST_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//now insert.  get the distinct combination of allocation configurations associated late close 
insert into UNITIZE_ALLOC_LIST_TEMP (company_id, alloc_id, alloc_type, alloc_basis, alloc_priority)
select distinct a.company_id, b.alloc_id, lower(trim(c.alloc_type)), lower(trim(c.alloc_basis)), b.allocation_priority 
from UNITIZE_WO_LIST_TEMP a, charge_group_control b, unit_allocation c
where a.work_order_id = b.work_order_id
and b.alloc_id = c.alloc_id
and a.company_id = :i_company_id
and b.batch_unit_item_id is null
and b.expenditure_type_id in (1,2)
and b.unit_item_id is null
and a.error_msg is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_LIST_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows	 

//Save to real table for troubleshooting
of_rtn = of_archive_alloc_list_temp()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if 

//check amount on the charge groups for exceeding charge_type_id allocation limits 
//of_errors('ALLO') called in this function
of_rtn = of_allocation_limits()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if 

//Gather estimate data from WO_ESTIMATE
of_rtn  = of_allocation_statistic_estimates_all()
//error handling done in function call
if of_rtn < 0 then 
	return -1
end if 

//get standard allocation precision information
of_rtn  = of_allocation_std_info()
//error handling done in function call
if of_rtn < 0 then 
	return -1
end if 

return 1
end function

public function integer of_allocation_prepare_late ();//the i_unitization_type should be late close
longlong of_rtn, num_nrows
string cv

if i_unitization_type = 'AUTO' or i_unitization_type = 'AUTO WO' then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: Late updates not correct for Regular Auto Unitization."
	of_log() 
	rollback;		 
	return -1
end if 

//reset the "late" tables
delete from UNITIZE_RATIO_LATE_ADD_TEMP;
	
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_RATIO_LATE_ADD_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

delete from UNITIZE_RATIO_LATE_RWIP_TEMP;
	
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_RATIO_LATE_RWIP_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//Overwrite the "Unitize by Account" from WORK_ORDER_ACCOUNT with System Control Config Rules:
//If wo = Yes and control = No: override wo attribute with system control value to make all work orders allocate to different UT Accounts during Late Close.  
//If wo = No and control = Yes: do NOT overwrite wo attribute with system control value, simply obey wo attribute during Late Close.  
//System control description: 
//                                    "Yes" will indicate late charge unitization will obey  Unitize by Account, which is a work order attribute on Work Order Account.
//                                    "No" will override the Unitize by Account = Yes to force the Unitize by Account = No for late charge unitization.  
//                                    This control will only override the Unitize by Account = Yes, and it will NOT override the Unitize by Account = No.
//Default system control value:  No.  
cv = lower(f_pp_system_control_company('AUTO101 - LATE CHARGE OBEY ACCOUNT', i_company_id))

if cv = '' or isnull(cv) then 
	cv = 'no'
end if 

if cv = 'no' then
	//update the Unitize_By_Account attribute to match the system control
	update UNITIZE_WO_LIST_TEMP a
	set a.UNITIZE_BY_ACCOUNT = 0
	where a.company_id = :i_company_id
	and a.error_msg is null
	and a.unitize_by_account = 1;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: update late UNITIZE_WO_LIST_TEMP.UNITIZE_BY_ACCOUNT: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  
end if 

return 1
end function

public function integer of_allocation_late_stats_rwip ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get the RWIP allocation statistics from CHARGE_GROUP_CONTROL for original RWIP Unitization dollars.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, check_count, of_rtn
string error_msg

if i_elig_rwip_wos = 0 then
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": processing late RWIP..."
of_log() 

//reset this global temp table for the late RWIP charges
delete from UNITIZE_ALLOC_LATE_PT_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_LATE_PT_TEMP (rwip): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//need the distinct list of processing_type_ids for RWIP by work order
insert into UNITIZE_ALLOC_LATE_PT_TEMP (company_id, work_order_id, processing_type_id)
select distinct x.COMPANY_ID,  a.WORK_ORDER_ID, b.PROCESSING_TYPE_ID
from CHARGE_GROUP_CONTROL a, CHARGE_TYPE b, UNITIZE_WO_LIST_TEMP x
where a.work_order_id = x.work_order_id
and a.charge_type_id = b.charge_type_id
and a.expenditure_type_id = 2
AND x.company_id = :i_company_id
AND x.error_msg is null  
AND x.has_elig_rwip = 1
and a.batch_unit_item_id is null
and a.unit_item_id is null 
and b.processing_type_id <> 1  /*non-OCR*/
;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_LATE_PT_TEMP (rwip): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//Save to real table for troubleshooting
of_rtn = of_archive_alloc_late_pt_temp()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get best match Allocation Statistics come from the CHARGE_GROUP_CONTROL table for Units that match by UNIT_ITEM_ID and PROCESSING_TYPE_ID
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

insert into UNITIZE_RATIO_LATE_RWIP_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, 
EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, 
BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
PROCESSING_TYPE_ID,
ALLOC_STATISTIC, COST_SOURCE, MIN_RWIP_BATCH_UNIT_ID
)
SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
	b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
	d.PROCESSING_TYPE_ID,
	sum(c.amount) ALLOC_STATISTIC	, 'MATCHED'	, x.MIN_RWIP_BATCH_UNIT_ID
 FROM UNITIZE_ALLOC_LATE_PT_TEMP a,
 		UNITIZE_ELIG_UNITS_TEMP b,
		CHARGE_GROUP_CONTROL c,
		CHARGE_TYPE d, 
		UNITIZE_WO_LIST_TEMP x		
WHERE a.work_order_id = x.work_order_id
	AND b.WORK_ORDER_ID = x.WORK_ORDER_ID 
	AND b.WORK_ORDER_ID = c.WORK_ORDER_ID 
	AND c.charge_type_id = d.charge_type_id
	AND b.EXCLUDE_FROM_ALLOCATIONS = 0
	AND d.PROCESSING_TYPE_ID <> 1  /*non-OCR*/
	AND a.processing_type_id = d.processing_type_id 
	AND x.COMPANY_ID = :i_company_id
	AND x.ERROR_MSG is null  
	AND x.has_elig_rwip = 1
	and c.expenditure_type_id = b.expenditure_type_id
	AND c.expenditure_type_id = 2
	AND b.expenditure_type_id = 2
	AND c.UNIT_ITEM_ID = b.STATUS /*join by the new UNIT_ITEM_ID = original UNIT_ITEM_ID*/
	AND c.BATCH_UNIT_ITEM_ID = x.MIN_RWIP_BATCH_UNIT_ID /*include only the original RWIP unitization data*/
GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
	b.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID,
	d.PROCESSING_TYPE_ID , x.MIN_RWIP_BATCH_UNIT_ID
HAVING sum(c.amount) <> 0
;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_RATIO_LATE_RWIP_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get Allocation Statistics from the CHARGE_GROUP_CONTROL table for Units that match by UNIT_ITEM_ID (no processing_type_id costs)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//delete from UNITIZE_ALLOC_LATE_BP_TEMP the processing types that were matched in the insert above
delete from UNITIZE_ALLOC_LATE_PT_TEMP a 
where a.company_id = :i_company_id
and exists (select 1 from UNITIZE_RATIO_LATE_RWIP_TEMP b
				where a.work_order_id = b.work_order_id
				and a.processing_type_id = b.processing_type_id
				and b.company_id = :i_company_id
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_LATE_PT_TEMP before all costs (rwip): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//use PROCESSING_TYPE_ID = 0 and group the dollars for all processing types for this insert
insert into UNITIZE_RATIO_LATE_RWIP_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, 
EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, 
BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
PROCESSING_TYPE_ID,
ALLOC_STATISTIC, COST_SOURCE, MIN_RWIP_BATCH_UNIT_ID
)

SELECT v.company_id, v.work_order_id, v.unit_item_id, v.expenditure_type_id, v.utility_account_id, v.unitize_by_account,
v.bus_segment_id, v.sub_account_id, t.processing_type_id, v.alloc_statistic, 'ALL', v.MIN_RWIP_BATCH_UNIT_ID

FROM UNITIZE_ALLOC_LATE_PT_TEMP t, /*cartesian join this table to the view*/

	(SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, 
	sum(abs(c.amount))  ALLOC_STATISTIC, x.MIN_RWIP_BATCH_UNIT_ID
					
	 FROM 
			UNITIZE_ELIG_UNITS_TEMP b,
			CHARGE_GROUP_CONTROL c,
			CHARGE_TYPE d, 
			UNITIZE_WO_LIST_TEMP x
			
	WHERE b.WORK_ORDER_ID = x.WORK_ORDER_ID 
		AND b.WORK_ORDER_ID = c.WORK_ORDER_ID 
		AND c.charge_type_id = d.charge_type_id
		AND b.EXCLUDE_FROM_ALLOCATIONS = 0
		AND d.PROCESSING_TYPE_ID <> 1  /*non-OCR*/
		AND x.COMPANY_ID = :i_company_id
		AND x.ERROR_MSG is null  
		AND x.has_elig_rwip = 1
		and c.expenditure_type_id = b.expenditure_type_id
		AND c.expenditure_type_id = 2
		AND b.expenditure_type_id = 2
		AND c.amount <> 0
		AND c.UNIT_ITEM_ID = b.STATUS /*join by the new UNIT_ITEM_ID = original UNIT_ITEM_ID*/
		AND c.BATCH_UNIT_ITEM_ID = x.MIN_RWIP_BATCH_UNIT_ID /*include only the original RWIP unitization data*/
		and exists (select 1 from UNITIZE_ALLOC_LATE_PT_TEMP c
							where x.work_order_id = c.work_order_id
							)  /*wo has late charges with mismatched processing_type_ids*/
	GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, x.MIN_RWIP_BATCH_UNIT_ID
	) v

WHERE v.work_order_id = t.work_order_id /*cartesian join the costs to the mismatched processing_type_ids*/
;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_RATIO_LATE_RWIP_TEMP (all): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

/////////////////////////////////////////////////////////////////////////////////////////////
//determine if "catch all" did in fact catch all
/////////////////////////////////////////////////////////////////////////////////////////////

//delete from UNITIZE_ALLOC_LATE_BP_TEMP the book summaries that were matched in the insert above
delete from UNITIZE_ALLOC_LATE_PT_TEMP a 
where a.company_id = :i_company_id
and exists (select 1 from UNITIZE_RATIO_LATE_RWIP_TEMP b
				where a.work_order_id = b.work_order_id
				and a.processing_type_id = b.processing_type_id
				and b.company_id = :i_company_id
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_LATE_BP_TEMP after all costs (rwip): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//if all eligible late book summaries have found allocation statistics for allocation basis, then this table should be empty
check_count = 0
select count(1) into :check_count
from UNITIZE_ALLOC_LATE_PT_TEMP a 
where a.company_id = :i_company_id
and a.processing_type_id is not null;

if check_count> 0 then
	error_msg = "Unitization 284R: No basis for allocation. Unable to find prior unitized RWIP " 
	 
	insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
	select a.company_id, a.work_order_id, :error_msg||' for '||decode(processing_type_id, 8, 'Reserve Credits', 7, 'Salvage Credits', 2, 'Salvage Cash', 'Cost of Removal')||' *Manual Unitization Recommended*'
	from UNITIZE_WO_LIST_TEMP a, UNITIZE_ALLOC_LATE_PT_TEMP b 
	where a.work_order_id = b.work_order_id
	and a.company_id = :i_company_id   
	and a.error_msg is null
	and a.has_elig_rwip = 1;

	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: update UNITIZE_WO_LIST_TEMP for 284R: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  

	if num_nrows > 0 then
		//Handle errors identified above.
		of_rtn = of_errors('ALLO')
		if of_rtn <> 1 then
			//errors are handled inside function call
			return -1
		end if  
	end if 
end if 

return 1
end function

public function integer of_allocation_late_by_precision ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Late Close does not Allocate the same way as Regular Close.
//The Unitization Allocation configuration for the work order's Alloc Method Type is not used for Late Close.
//Late Close has two levels of precision for Allocation of the late charges:
//  1.  best match by book summary dollars for Additions and best match by processing type dollars for RWIP
//  2.  total cost for Additions and RWIP
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong of_rtn

//prepare the next CHARGE_GROUP_ID.  there is no allocation by priority for Late Close, so i_first_priority = true.
i_first_priority = true 
of_rtn = of_allocation_refresh_staged_data()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	

/////////////
//ADDS
/////////////

//get the best match allocation statistics used for late Addition type charges (match to basis)
//of_errors('ALLO') called inside this function
of_rtn = of_allocation_late_stats_adds()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	

//calculate the Addition allocation ratios
of_rtn= of_allocation_late_stats_adds_ratio()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	

//Save to real table for troubleshooting
of_rtn = of_archive_ratio_late_add_temp()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

/////////////
//RWIP
/////////////

//get the allocation statistics used for late RWIP type charges
//of_errors('ALLO') called inside this function
of_rtn = of_allocation_late_stats_rwip()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	

//calculate the RWIP allocation ratios
of_rtn= of_allocation_late_stats_rwip_ratio()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	

//Save to real table for troubleshooting
of_rtn = of_archive_ratio_late_rwip_temp()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

/////////////
//ALLOCATE
/////////////

//allocate the Additon late charge by Account = Yes
of_rtn = of_allocate_late_charges_by_acct()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	

//allocate the remaining Additon late charges
of_rtn = of_allocate_late_charges_remaining()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	 

//insert into CHARGE_GROUP_CONTROL the split charges created from allocation
of_rtn = of_insert_into_charge_group_control_allo(0)
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	

//Save to real table for troubleshooting. maint 43357: archive after insert
of_rtn = of_archive_alloc_cgc_inserts_temp()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

/////maint-43648:  this is no longer needed.  unit attributes are now part of of_insert_into_charge_group_control_allo()
////update the CGC columns to match to the UWO columns for Unitized work orders.
//of_rtn = of_update_charge_groups_unitized('ALLO')
//if of_rtn <> 1 then
//	//errors are handled inside function call
//	return -1 
//end if

commit;

return 1
end function

public function integer of__main ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//Main function for Regular Unitization: There are many commits inside this function.
//										            Work orders are tagged with errors and excluded from processing if validations fail.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
integer of_rtn
longlong check_count
string fast_101

if i_single_wo = false then	
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ": Closing Type = " + i_closing_type
	i_msg[2] =  ": Run = " + string(i_run)  
	i_msg[3] =  ": Month = " + i_month_yr_string_display
end if 
of_log()

//Get eligible work order(s) saved into the wo list tables
//a commit is executed in of_prepare_wo_list() which is called inside of_eligible_wo() to save unitizing work order in the UNITIZE_WO_LIST table.
of_rtn = of_eligible_wos()
if of_rtn <> 1 then
	//errors are handled inside function call
	if of_rtn = 0 then
		//nothing to do
		if i_single_wo = false then
			//a commit inside this function
			of_finished_msg()
		end if 
		return 1
	else
		return -1
	end if 
end if 

//get max revision
of_rtn = of_wo_estimate_get_max_revision()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if		
commit;

//run work order level validations
//of_errors('WO') function called at the end this function
of_rtn = of_validate_wos()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

commit;

//check UNITIZE_WO_LIST_TEMP has at least one work order left to process
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST_TEMP
where rownum = 1;

if check_count = 0 then 
	return 1
end if

/////////////////////////
//Charge grouping
/////////////////////////

////validate existing charge groups for charge_type_id
////of_errors('WO') function called at the end this function
//of_rtn = of_validate_cgc_back_to_cwip()
//if of_rtn <> 1 then
//	//errors are handled inside function call
//	return -1 
//end if   
//commit;

//create new charge groups from charges that have not yet been picked up for unitization
of_rtn = of_insert_into_charge_group_control()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

////tagging charge groups for net $0 unitization should not be part of Allocations
////this function loops by work orders
//of_rtn = of_net_zero_unitization()
//if of_rtn <> 1 then
//	//errors are handled inside function call
//	return -1 
//end if   
//commit;

//determine types of charges eligible for Unitization
of_rtn = of_identify_types_of_elig_charges() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;

//Identify work orders with no charges available to unitize and silently remove them from the UNITIZE_WO_LIST_TEMP global table
//No eligible charge available to unitize is not an error, which is why these work orders are silently removed from the unitization list.
of_rtn = of_remove_wos_without_charges() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;

////////////////////////////////////////////////////////////////////
//Validate Existing Unit Items on UNITIZED_WORK_ORDER
////////////////////////////////////////////////////////////////////

//Validate important data combinations for prior made unit items
// of_errors('WO') called at the end of this function
of_rtn = of_unit_item_inserted_validations()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//OCR validations that impact RWIP unitization
//of_errors('UNITS') called at the end of this function
of_rtn = of_unit_item_ocr_gainloss()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//refresh unit attributes on existing UNITIZED_WORK_ORDER table for work orders using "One for One" estimates option
of_rtn = of_unit_item_refresh_for_one4one()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if		
commit;

//check UNITIZE_WO_LIST_TEMP has at least one work order left to process
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST_TEMP
where rownum = 1;

if check_count = 0 then 
	return 1
end if

//////////////////////////////////////////////////////////////
//WO Estimates
//////////////////////////////////////////////////////////////

//backfill missing columns on WO_ESTIMATE that have defaulting capability
of_rtn = of_wo_estimate_defaults_backfill()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if		
commit;

///////////////////////////////
//Create New Unit Items
///////////////////////////////

//clear out the units staging table
of_rtn= of_unit_item_staging_reset() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

//generate full list of available unit items for potential new unit items from Actuals and Estimates
of_rtn = of_unit_item_staging()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if 
commit;

//validate the unit items source
//of_errors('WO') called at the end of this function
of_rtn = of_unit_item_source_updates()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if 
commit;

//get defaulted unit attributes for ones that are null (units made from Actuals don't get backfilled on CWIP_CHARGE)
//of_errors('UNITS') function called at the end this function
of_rtn = of_unit_item_defaults()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if
commit;

//need to do this after of_unit_item_staging() in order to have unit_item_source data.
//of_errors('WO') called at the end of this function
of_rtn = of_unitization_tolerance() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if	
commit;

//compare and remove newly staged Unit Item for unique combinations that are already in UNITIZED_WORK_ORDER table
of_rtn = of_unit_item_compare_to_uwo()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if    
commit;

//verify staged unit item attributes, ie acct/loc combinations
//of_errors('UNITS') called at the end of this function
of_rtn = of_unit_item_stg_validations()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if
commit;

//backfill asset descriptions and gl_account_id for the minor unit items 
of_rtn = of_unit_item_descriptions()  
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if		
commit; 

//Prepare for insert into UNITIZED_WORK_ORDER table 
of_rtn = of_unit_item_insert_prepare() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if    
commit;

//Insert new Unit Items into UNITIZED_WORK_ORDER table from the UNITIZE_WO_STG_UNITS_TEMP table
of_rtn = of_insert_into_unitized_work_order() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if    
commit;

//Gather only the eligible Units from UNITIZED_WORK_ORDER table, exclude already used Units.
of_rtn = of_unitization_elig_unit_items() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;

//////////////////////////////////////////////////////////////////////////
//Direct Assign for Regular Close
//////////////////////////////////////////////////////////////////////////
//direct assign
of_rtn = of_direct_assign()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//special direct assign for net $0
//of_errors('WO') called at the end of this function
//need to put this function call AFTER regular Direct Assign and BEFORE Allocations because we don't want regular Direct Assigment to use 
//    new units created from the f_unitization_net_zero() function.  
//The Direct Assigment logic does not use the EXCLUDE_FROM_ALLOCATIONS option on UNITIZED_WORK_ORDER.
//The Allocation does use  EXCLUDE_FROM_ALLOCATIONS option on UNITIZED_WORK_ORDER.
of_rtn = of_unitization_net_zero() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if	
commit;

//////////////////////////////////////////////////////////////////////////
//Class Codes for Regular Close
//////////////////////////////////////////////////////////////////////////
//add class codes to unit items
of_rtn = of_unit_item_class_codes()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//Identify work orders with no unit items available to unitize in UNITIZE_ELIG_UNITS_TEMP table.
//Must perform this validation after both Direct Assigns are finished.
//This check needs to happen for all sources:  actuals, estimates, cpr, etc.
//of_errors('WO') called at the end of this function
of_rtn = of_remove_wos_without_unit_items() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  

//check UNITIZE_WO_LIST_TEMP has at least one work order left to process
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST_TEMP
where rownum = 1;

if check_count = 0 then 
	return 1
end if
	
/////////////////////////
//Allocate
/////////////////////////

//prepare allocation data
of_rtn = of_allocation_prepare()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;
	
//must perform allocation in order by priority with smallest priority number first. for example, priority 1 is process before priority 2.
//the allocation processing MUST be done in a priority loop, because the actuals methodology relies on already unitized charge_group_controls to determine 
//             the allocation stastic used for charge groups configured to use actuals.  
//a smaller number priority based on estimate would impact the bigger number prioriy based on actuals.
of_rtn = of_allocation_by_priorities()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//verify the charge_group_id split allocation results balance to the original charge_group_id amount
//of_errors('ALLO') function is called at the end of this function 
of_rtn = of_allocation_reconcile()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//verify the work orders have all charge groups are unitized (updated with a UNIT_ITEM_ID)
//of_errors('WO') called at the end of this function
of_rtn = of_unitization_completion_check()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//update the AMOUNT column on UNITIZED_WORK_ORDER_COLUMN after unitization is complete.
of_rtn = of_update_unitized_wo_amount()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if
commit;

//check the depr group mapping for RWIP Unit Items
//of_errors('UNITS') called at the end of this function
of_rtn = of_unit_item_rwip_asset_id()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if
commit;

//last function prior to creating pending transactions done in a WO loop:
//complicated validations and updates performed in a WO loop when the wo is ready for pending:  required class codes and equip ledger
//of_errors('WO') called at the end of this function
of_rtn = of_wo_checks_and_updates()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if
commit;

//check UNITIZE_WO_LIST_TEMP has at least one work order left to process
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST_TEMP
where rownum = 1;

if check_count = 0 then 
	return 1
end if 		

//////////////////////////////////////////////////
//Prepare pending transactions
//////////////////////////////////////////////////

//prepare for insert into PEND tables.
of_rtn = of_prepare_pend_transaction()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit; 

//Balance the Addition basis and RWIP dollars to CWIP_CHARGE for each wo
//of_errors('ALLO') function is called at the end of this function 
of_rtn= of_validate_pending_back_to_cwip()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if
commit;

//////////////////////////////////////////////////
//Create pending transactions
//////////////////////////////////////////////////
//insert into PEND tables.
of_rtn = of_insert_into_pend_transaction()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   

//Call WIP Comp closing after insert into real pending tables.
of_rtn = of_wip_comp_closing101_check()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   

//Update the BATCH_UNIT_ITEM_ID on WO_ESTIMATE & CHARGE_GROUP_CONTROL
//Insert into WO_UNIT_ITEM_PEND_TRANS
//Update wo_status_id
of_rtn = of_unitization_final_updates()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if  
commit;

return 1
end function

public function integer of__main_late ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//Main function for Late Close Unitization: There are many commits inside this function.
//								              		Work orders are tagged with errors and excluded from processing if validations fail.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
integer of_rtn
longlong prior_late_months, check_count
string late_close_option

late_close_option = UPPER(f_pp_system_control_company('AUTO101 - LATE CHARGE UNITIZATION', i_company_id))
if late_close_option = 'NO' or late_close_option = '' or isnull(late_close_option) then
	//nothing to do here, return to calling function
	return 0
end if 

// SEB: i_late_charge_mos should be set by:
//  -- w_wo_close when i_single_wo is true
//  -- w_wo_control/nvo_wo_control when i_single_wo is false
prior_late_months = i_late_charge_mos

if isnull(prior_late_months) then 
	prior_late_months = 0 
else
	prior_late_months = prior_late_months*-1
end if

//get the late month variables to use for Auto Late unitization.
select add_months(:i_month_yr, :prior_late_months), to_number(to_char(add_months(:i_month_yr, :prior_late_months) ,'yyyymm'))
into :i_late_month_yr, :i_late_month_number
from dual;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to determine late month number: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		

i_late_month_yr_string_display =  string(i_late_month_yr, 'mmm-yyyy')
 /*the date format used in the SQL of dw_wo_to_unitize_101_auto is yyyymm, which is i_late_month_number*/	 

if i_single_wo = false then	
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ": Closing Type = " + i_unitization_type
	i_msg[2] =  ": Run = " + string(i_run)  
	i_msg[3] =  ": Month = " + i_month_yr_string_display
	i_msg[4] =  ": Late Month = " + i_late_month_yr_string_display
end if 
of_log()

//Get eligible work order(s) saved into the wo list tables
//a commit is executed in of_prepare_wo_list() which is called inside of_eligible_wo() to save unitizing work order in the UNITIZE_WO_LIST table.
of_rtn = of_eligible_wos()
if of_rtn <> 1 then
	//errors are handled inside function call
	if of_rtn = 0 then
		//nothing to do
		if i_single_wo = false then
			//a commit inside this function
			of_finished_msg()
		end if 
		return 1
	else
		return -1
	end if 
end if 

//Run work order level validations
//of_errors('WO') function called at the end this function
of_rtn = of_validate_wos()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;

/////////////////////////
//Charge grouping
/////////////////////////

////validate existing charge groups for charge_type_id
////of_errors('WO') function called at the end this function
//of_rtn = of_validate_cgc_back_to_cwip()
//if of_rtn <> 1 then
//	//errors are handled inside function call
//	return -1 
//end if   
//commit;

//create new charge groups from charges that have not yet been picked up for unitization
of_rtn = of_insert_into_charge_group_control()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

////tagging charge groups for net $0 unitization should not be part of Allocations
////this function loops by work orders
//of_rtn = of_net_zero_unitization()
//if of_rtn <> 1 then
//	//errors are handled inside function call
//	return -1 
//end if   
//commit;

//determine types of charges eligible for Unitization
of_rtn = of_identify_types_of_elig_charges() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;

//NEW:  Identify work orders with no charges available to unitize and silently remove them from the UNITIZE_WO_LIST_TEMP global table
//No eligible charge available to unitize is not an error, which is why these work orders are silently removed from the unitization list.
of_rtn = of_remove_wos_without_charges() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;

/////////////////////////
//Create Unit Items
/////////////////////////
	
//clear out the units staging table
of_rtn= of_unit_item_staging_reset() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

//create new minor addition unit items 
of_rtn = of_unit_item_staging_late_adds() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

//create new RWIP unit items  
of_rtn = of_unit_item_staging_late_rwip() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

//update source on staging table
of_rtn = of_unit_item_source_updates()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if 
commit;

//of_errors('WO') called at the end of this function
of_rtn = of_unitization_tolerance_late()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if
commit;
	
//prepare for insert into UNITIZED_WORK_ORDER table 
of_rtn = of_unit_item_insert_prepare() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if    
commit;

//insert new Unit Items into UNITIZED_WORK_ORDER table from the UNITIZE_WO_STG_UNITS_TEMP table
of_rtn = of_insert_into_unitized_work_order() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if    
commit;

//Gather only the eligible Units from UNITIZED_WORK_ORDER table, exclude already used Units.
of_rtn = of_unitization_elig_unit_items() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;


//////////////////////////////////////////////////////////////////////////
//No Class Codes and No regular Direct Assign for Late Close
//////////////////////////////////////////////////////////////////////////

//special direct assign for net $0
//of_errors('WO') called at the end of this function
//need to put this function call AFTER regular Direct Assign and BEFORE Allocations because we don't want regular Direct Assigment to use 
//    new units created from the f_unitization_net_zero() function.  
//The Direct Assigment logic does not use the EXCLUDE_FROM_ALLOCATIONS option on UNITIZED_WORK_ORDER.
//The Allocation does use  EXCLUDE_FROM_ALLOCATIONS option on UNITIZED_WORK_ORDER.
of_rtn = of_unitization_net_zero() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if	
commit;

//Identify work orders with no unit items available to unitize in UNITIZED_WORK_ORDER table.
//Need to check the UNITIZED_WORK_ORDER table to include any prior made Unit Items in addition to new Unit Items made in this run.
//This check needs to happen for all sources:  actuals, estimates, cpr, etc.
//of_errors('WO') called at the end of this function
of_rtn = of_remove_wos_without_unit_items() 
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if  
commit;

//check UNITIZE_WO_LIST_TEMP has at least one work order left to process
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST_TEMP
where rownum = 1;

if check_count = 0 then 
	return 1
end if

/////////////////////////
//Allocate
/////////////////////////

//prepare allocation data
of_rtn = of_allocation_prepare()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//perform allocation
of_rtn = of_allocation_late_by_precision()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//verify the charge_group_id split allocation results balance to the original charge_group_id amount
//of_errors('ALLO') function is called at the end of this function 
of_rtn = of_allocation_reconcile()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//verify the work orders have all charge groups are unitized (updated with a UNIT_ITEM_ID)
//of_errors('WO') called at the end of this function
of_rtn = of_unitization_completion_check()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if	
commit;

//update the AMOUNT column on UNITIZED_WORK_ORDER_COLUMN after unitization is complete.
of_rtn = of_update_unitized_wo_amount()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if
commit;

//For Late Close, of_wo_checks_and_updates(), does not need to be run.  Class Codes and Equip Ledger are not part of Late Close.


//check UNITIZE_WO_LIST_TEMP has at least one work order left to process
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST_TEMP
where rownum = 1;

if check_count = 0 then 
	return 1
end if 	

//////////////////////////////////////////////////
//Prepare pending transactions
//////////////////////////////////////////////////

//prepare for insert into PEND tables.
of_rtn = of_prepare_pend_transaction()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   
commit;

//Balance the Addition basis and RWIP dollars to CWIP_CHARGE for each wo
//of_errors('ALLO') function is called at the end of this function 
of_rtn= of_validate_pending_back_to_cwip()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if
commit;

//////////////////////////////////////////////////
//Create pending transactions
//////////////////////////////////////////////////

//insert into PEND tables.
of_rtn = of_insert_into_pend_transaction()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   

//Call WIP Comp closing after insert into real pending tables.
of_rtn = of_wip_comp_closing101_check()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if   

//Update the BATCH_UNIT_ITEM_ID on WO_ESTIMATE & CHARGE_GROUP_CONTROL
//Insert into WO_UNIT_ITEM_PEND_TRANS
//Update wo_status_id
of_rtn = of_unitization_final_updates()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1 
end if  
commit; 
 
return 1
end function

public function integer of_start_company (longlong a_company_id, longlong a_month_number, longlong a_process_id, string a_unitize_regular, string a_unitize_late, boolean a_visual);///////////////////////////////////////////////////////////////////////////////////////////////////////////
//a_company_id:  company being unitized
//a_month_number:  gl month of unitization
//a_process_id:  the process_id for Online Logs
//a_unitize_regular:  YES or NO
//a_unitize_late:  YES or NO
//a_visual:  true = show more message outputs to the w_wo_status_box; false = less messages
///////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong rtn, rtn_code_main, check_count, of_rtn
string msg, fast_101, late_close_option

//Start Auto Unitization.
//Set the main variables that will drive all of the unitization processing.

//System level validations in "of_validate_correct_unitization_config()" should be called outside of the company loop as it should only be
//called one time prior to starting Auto Unitization.  It is inefficient and redundant to call this function for every company.

//"types" of unitization:  AUTO CO, AUTO LATE CO, AUTO WO, AUTO LATE WO, MANUAL
//in v10.4.2.0 this new code will only be used for Regular Unitization.  Late Close still goes through old101 code.
//in v10.4.3.0 this new code is used for both Regular and Late Close.

i_company_id = a_company_id
i_month_number = a_month_number
i_process_id = a_process_id
i_visual = a_visual

fast_101 = UPPER(f_pp_system_control_company('AUTO101 - FAST NO LOOP', i_company_id))
if fast_101 = 'NO' or fast_101 = '' or isnull(fast_101) then
	//nothing to do here, return to calling function
	return 0
end if 

//Check the process_id for Online Logs for Unitization at the company level.
if isnull(a_process_id) or a_process_id = 0 then
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: There is no process_id defined for Automatic Unitization."  
	i_msg[2] = ": ERROR: Unable to run " + i_unitization_type + " Unitization."  
	of_log()
	return -1
end if  

//clear out work orders unitized in prior disconnected sessions. a commit is inside this function.
rtn = of_clear_wo_list_prior_sessions()
if rtn <> 1 then
	//errors handled inside this function already
	return -1
end if 	

//check for concurrent unitization for this company
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST
where company_id = :i_company_id
and unitization_type in ('AUTO', 'AUTO LATE');

if check_count > 0 then
	//the company is running Auto Unitization right now	
	i_msg[] = i_null_arrary[]
	i_msg[1] = ": ERROR: Auto Unitization is already running for Company.  Please do not duplicate unitization processing for this company."
	of_log()
	return -1
end if 

//Initialize other variables needed for processing
of_rtn = of_initialize_variables()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 
 
//Get the company description
select description into :i_co_desc
from company where company_id = :i_company_id;

//a commit is executed in of_start_msg() to increment the "run" for WO_AUTO101_CONTROL table.
of_rtn = of_start_msg()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if

//Run company level validations
of_rtn = of_validate_company()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

//Start regular auto unitization of company
if UPPER(a_unitize_regular) = 'YES' then
	i_unitization_type = 'AUTO'
	rtn_code_main = of__main()
	
	//Since the main processing is finished, clear out the UNITIZE_WO_LIST table for wos unitized in this run. a commit is inside this function.
	of_rtn = of_clear_wo_list_curr_session()
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1 
	end if   
	
	if rtn_code_main = 1 then
		i_msg[] = i_null_arrary[]
		i_msg[1] =   ": Finished Regular Auto Unitization for " + i_co_desc + "."  
		of_log()
	end if 
end if


//fatal error in regular unitization should stop unitization and avoid starting late unitization.
if rtn_code_main < 0 then
	return rtn_code_main
end if 

//Start late auto unitization of company
if UPPER(a_unitize_late) = 'YES' then
	i_unitization_type = 'AUTO LATE'
	
	i_msg[] = i_null_arrary[]
	i_msg[1] =   ": Starting Late Auto Unitization for " + i_co_desc + "."  
	of_log()
	
	rtn_code_main = of__main_late()

	//Since the main processing is finished, clear out the UNITIZE_WO_LIST table for wos unitized in this run. a commit is inside this function.
	of_rtn = of_clear_wo_list_curr_session()
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1 
	end if   
	
	if rtn_code_main = 1 then
		i_msg[] = i_null_arrary[]
		i_msg[1] =   ": Finished Late Auto Unitization for " + i_co_desc + "."  
		of_log()
	end if 
end if

//a commit is inside this function.
of_rtn = of_finished_msg()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if

return rtn_code_main

end function

public function integer of_allocation_late_stats_rwip_ratio ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Important fact about the SQLs below:  
//     If the "alloc_ratio" values are all 0 based on poor configuration, no divide by zero error will occur using RATIO_TO_REPORT,
//     because this oracle function does not divide in the tradional sense of numerator/denominator.  
//     The logic that determins the "alloc_ratio" must have additional logic to address situations where the
//     allocation records all have "alloc_ratio" = 0.
//     Without specific logic to catch scenarios where all records in the allocation
//     pool have "alloc_ratio" = 0, later functions that allocate the amounts will allocation 100% to the first
//     allocation record using the rounding syntax that compares amount to allocate vs.
//     total allocated amount.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

if i_elig_rwip_wos = 0 then
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   calculating ratios..."
of_log() 

//update the "alloc_ratio_by_sub" alloc_ratio to be used when CGC utility_account_id = not null and sub_account_id is not null and wo is configured FOR "unitize_by_account" = "YES."
update UNITIZE_RATIO_LATE_RWIP_TEMP a
set a.alloc_ratio_by_sub = (select ratio
								from 
									(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.processing_type_id, b.utility_account_id, b.sub_account_id) ratio,
									b.company_id, b.work_order_id, b.expenditure_type_id, b.processing_type_id, b.utility_account_id, b.sub_account_id, b.unit_item_id
									from UNITIZE_RATIO_LATE_RWIP_TEMP b, UNITIZE_WO_LIST_TEMP x
									where b.work_order_id = x.work_order_id
									and x.error_msg is null
									and x.company_id = :i_company_id 
									and x.has_elig_rwip = 1
									and b.utility_account_id is not null
									and b.sub_account_id is not null
									and b.unitize_by_account = 1 
									) c
								where a.company_id = c.company_id
								and a.work_order_id = c.work_order_id
								and a.expenditure_type_id = c.expenditure_type_id
								and a.processing_type_id = c.processing_type_id  
								and a.utility_account_id = c.utility_account_id
								and a.sub_account_id = c.sub_account_id
								and a.unit_item_id = c.unit_item_id
								)
where a.company_id = :i_company_id 
and a.utility_account_id is not null
and a.sub_account_id is not null 
and a.unitize_by_account = 1
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_rwip = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_RWIP_TEMP.alloc_ratio_by_sub: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the "alloc_ratio_by_ut" alloc_ratio to be used when CGC utility_account_id = not null and sub_account_id is null and wo is configured FOR "unitize_by_account" = "YES."
//do not include sub_account_id in the filtering to determine "alloc_ratio_by_ut."
update UNITIZE_RATIO_LATE_RWIP_TEMP a
set a.alloc_ratio_by_ut =(select ratio
								from 
									(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.processing_type_id, b.utility_account_id) ratio,
									b.company_id, b.work_order_id, b.expenditure_type_id, b.processing_type_id, b.utility_account_id, b.unit_item_id
									from UNITIZE_RATIO_LATE_RWIP_TEMP b, UNITIZE_WO_LIST_TEMP x
									where b.work_order_id = x.work_order_id
									and x.error_msg is null
									and x.company_id = :i_company_id 
									and x.has_elig_rwip = 1
									and b.utility_account_id is not null
									and b.unitize_by_account = 1 
									) c
								where a.company_id = c.company_id
								and a.work_order_id = c.work_order_id
								and a.expenditure_type_id = c.expenditure_type_id
								and a.processing_type_id = c.processing_type_id  
								and a.utility_account_id = c.utility_account_id
								and a.unit_item_id = c.unit_item_id
								)
where a.company_id = :i_company_id 
and a.utility_account_id is not null
and a.unitize_by_account = 1
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_rwip = 1);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_RWIP_TEMP.alloc_ratio_by_ut: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the general alloc_ratio, which is used by the "Allocation Remaining" functionality and for all CGC data (regardless of the UTILITY_ACCOUNT_ID and SUB_ACCOUNT_ID data).
//do not include utility_account_id nor sub_account_id in the filtering to determine "alloc_ratio."
update UNITIZE_RATIO_LATE_RWIP_TEMP a
set a.alloc_ratio = (select ratio
						from 
							(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.processing_type_id) ratio,
							b.company_id, b.work_order_id, b.expenditure_type_id, b.processing_type_id, b.unit_item_id
							from UNITIZE_RATIO_LATE_RWIP_TEMP b, UNITIZE_WO_LIST_TEMP x
							where b.work_order_id = x.work_order_id
							and x.error_msg is null
							and x.company_id = :i_company_id 
							and x.has_elig_rwip = 1
							) c
						where a.company_id = c.company_id
						and a.work_order_id = c.work_order_id
						and a.expenditure_type_id = c.expenditure_type_id
						and a.processing_type_id = c.processing_type_id 
						and a.unit_item_id = c.unit_item_id
						)
where a.company_id = :i_company_id 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_rwip = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_RWIP_TEMP.alloc_ratio: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Take care of nulls
update UNITIZE_RATIO_LATE_RWIP_TEMP a
set a.alloc_ratio = 0
where a.company_id = :i_company_id 
and a.alloc_ratio is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_rwip = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_RWIP_TEMP.alloc_ratio(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

update UNITIZE_RATIO_LATE_RWIP_TEMP a
set a.alloc_ratio_by_sub = 0
where a.company_id = :i_company_id 
and a.alloc_ratio_by_sub is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_rwip = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_RWIP_TEMP.alloc_ratio_by_sub(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

update UNITIZE_RATIO_LATE_RWIP_TEMP a
set a.alloc_ratio_by_ut = 0
where a.company_id = :i_company_id 
and a.alloc_ratio_by_ut is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_rwip = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_RWIP_TEMP.alloc_ratio_by_ut(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows
 
return 1
end function

public function integer of_allocation_late_stats_adds ();/////////////////////////////////////////////////////////////////////////////////////// 
//Get the Addition allocation statistics from the CPR for Basis and Total Costs
/////////////////////////////////////////////////////////////////////////////////////// 
longlong num_nrows, check_count, of_rtn
string error_msg

if i_elig_add_wos = 0 then
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": processing late Additions..."
of_log()

//reset this global temp table for the late addition charges
delete from UNITIZE_ALLOC_LATE_BK_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_LATE_BK_TEMP (adds): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//need the distinct list of book_summary_ids for Additions by work order
insert into UNITIZE_ALLOC_LATE_BK_TEMP (company_id, work_order_id, book_summary_id)
select distinct x.COMPANY_ID,  a.WORK_ORDER_ID, b.BOOK_SUMMARY_ID
from CHARGE_GROUP_CONTROL a, CHARGE_TYPE b, UNITIZE_WO_LIST_TEMP x
where a.work_order_id = x.work_order_id
and a.charge_type_id = b.charge_type_id
and a.expenditure_type_id = 1
AND x.company_id = :i_company_id
AND x.error_msg is null  
AND x.has_elig_adds = 1
and a.batch_unit_item_id is null
and a.unit_item_id is null 
;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_LATE_BK_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//Save to real table for troubleshooting
of_rtn = of_archive_alloc_late_bk_temp()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get the best match Allocation Statistics come from the CPR Basis tables for Units that match by ASSET_ID and BOOK_SUMMARY_ID
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//get the allocation statistics from the CPR tables.
//the basis_n column joins to the book_summary_id of the late charges for each work order 
//the CPR data statistic for the book_summary_id will NOT insert into UNITIZE_RATIO_LATE_ADD_TEMP table if the basis $ amount is not greater than $0.
//there are two SQL filters to enforce the > than $0 for the basis $ amount:  (1) where clause > 0 and (2) having clause <> 0.
//if the work order has only negative dollar basis from prior Unitization close to match to new late charges for the same basis, this function won't work.
//the work order that has prior unitization where the assets in the CPR have negative dollars must be manually unitized.
//the next function will need to try and find positive asset costs that are at the asset total cost level vs specific basis level.
insert into UNITIZE_RATIO_LATE_ADD_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, 
BUS_SEGMENT_ID, SUB_ACCOUNT_ID, BOOK_SUMMARY_ID, COST_SOURCE,
ALLOC_STATISTIC
)
/*Use CPR_LDG_BASIS for specific accounting method*/
SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, c.book_summary_id, 'CPR_LDG_BASIS',
	sum(decode(c.book_summary_id,1,basis_1,2,basis_2,3,basis_3,4,basis_4,5,basis_5,6,basis_6,
			7,basis_7,8,basis_8,9,basis_9,10,basis_10,11,basis_11,12,basis_12,13,basis_13,14,basis_14,
			15,basis_15,16,basis_16,17,basis_17,18,basis_18,19,basis_19,20,basis_20,21,basis_21,22,basis_22,
			23,basis_23,24,basis_24,25,basis_25,26,basis_26,27,basis_27,28,basis_28,
			29,basis_29,30,basis_30,31,basis_31,32,basis_32,33,basis_33,
			34,basis_34,35,basis_35,36,basis_36,37,basis_37,38,basis_38,
			39,basis_39,40,basis_40,41,basis_41,42,basis_42,43,basis_43,
			44,basis_44,45,basis_45,46,basis_46,47,basis_47,48,basis_48,
			49,basis_49,50,basis_50,51,basis_51,52,basis_52,53,basis_53,
			54,basis_54,55,basis_55,56,basis_56,57,basis_57,58,basis_58,
			59,basis_59,60,basis_60,61,basis_61,62,basis_62,63,basis_63,
			64,basis_64,65,basis_65,66,basis_66,67,basis_67,68,basis_68,
			69,basis_69,70,basis_70 
		))  ALLOC_STATISTIC
				
 FROM CPR_LDG_BASIS a,
 		UNITIZE_ELIG_UNITS_TEMP b,
		UNITIZE_ALLOC_LATE_BK_TEMP c,
		RETIREMENT_UNIT r,
		PROPERTY_UNIT p,
		UNITIZE_WO_LIST_TEMP x
		
WHERE a.asset_id = b.ldg_asset_id
	AND b.WORK_ORDER_ID = x.WORK_ORDER_ID 
	AND b.WORK_ORDER_ID = c.WORK_ORDER_ID 
	AND b.EXCLUDE_FROM_ALLOCATIONS = 0
	AND x.COMPANY_ID = :i_company_id
	AND x.ERROR_MSG is null  
	AND x.has_elig_adds = 1 
	AND b.expenditure_type_id = 1 
	AND b.retirement_unit_id = r.retirement_unit_id
	AND r.property_unit_id = p.property_unit_id    
	AND p.asset_acct_meth_id = 1
	and decode(c.book_summary_id,1,basis_1,2,basis_2,3,basis_3,4,basis_4,5,basis_5,6,basis_6,
		7,basis_7,8,basis_8,9,basis_9,10,basis_10,11,basis_11,12,basis_12,13,basis_13,14,basis_14,
		15,basis_15,16,basis_16,17,basis_17,18,basis_18,19,basis_19,20,basis_20,21,basis_21,22,basis_22,
		23,basis_23,24,basis_24,25,basis_25,26,basis_26,27,basis_27,28,basis_28,
			29,basis_29,30,basis_30,31,basis_31,32,basis_32,33,basis_33,
			34,basis_34,35,basis_35,36,basis_36,37,basis_37,38,basis_38,
			39,basis_39,40,basis_40,41,basis_41,42,basis_42,43,basis_43,
			44,basis_44,45,basis_45,46,basis_46,47,basis_47,48,basis_48,
			49,basis_49,50,basis_50,51,basis_51,52,basis_52,53,basis_53,
			54,basis_54,55,basis_55,56,basis_56,57,basis_57,58,basis_58,
			59,basis_59,60,basis_60,61,basis_61,62,basis_62,63,basis_63,
			64,basis_64,65,basis_65,66,basis_66,67,basis_67,68,basis_68,
			69,basis_69,70,basis_70 
			 ) > 0 /*basis_n is a positive amount*/
GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, c.book_summary_id
HAVING sum( decode(c.book_summary_id,1,basis_1,2,basis_2,3,basis_3,4,basis_4,5,basis_5,6,basis_6,
		7,basis_7,8,basis_8,9,basis_9,10,basis_10,11,basis_11,12,basis_12,13,basis_13,14,basis_14,
	15,basis_15,16,basis_16,17,basis_17,18,basis_18,19,basis_19,20,basis_20,21,basis_21,22,basis_22,
	23,basis_23,24,basis_24,25,basis_25,26,basis_26,27,basis_27,28,basis_28,
		29,basis_29,30,basis_30,31,basis_31,32,basis_32,33,basis_33,
		34,basis_34,35,basis_35,36,basis_36,37,basis_37,38,basis_38,
		39,basis_39,40,basis_40,41,basis_41,42,basis_42,43,basis_43,
		44,basis_44,45,basis_45,46,basis_46,47,basis_47,48,basis_48,
		49,basis_49,50,basis_50,51,basis_51,52,basis_52,53,basis_53,
		54,basis_54,55,basis_55,56,basis_56,57,basis_57,58,basis_58,
		59,basis_59,60,basis_60,61,basis_61,62,basis_62,63,basis_63,
		64,basis_64,65,basis_65,66,basis_66,67,basis_67,68,basis_68,
		69,basis_69,70,basis_70 )) <> 0  
	
UNION ALL

/*Use CPR_ACT_BASIS for mass accounting method*/
SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, c.book_summary_id, 'CPR_ACT_BASIS',
	sum(decode(c.book_summary_id,1,a.basis_1,2,a.basis_2,3,a.basis_3,4,a.basis_4,5,a.basis_5,6,a.basis_6,
				7,a.basis_7,8,a.basis_8,9,a.basis_9,10,a.basis_10,11,a.basis_11,12,a.basis_12,13,a.basis_13,14,a.basis_14,
				15,a.basis_15,16,a.basis_16,17,a.basis_17,18,a.basis_18,19,a.basis_19,20,a.basis_20,21,a.basis_21,22,a.basis_22,
				23,a.basis_23,24,a.basis_24,25,a.basis_25,26,a.basis_26,27,a.basis_27,28,a.basis_28,
				29,a.basis_29,30,a.basis_30,31,a.basis_31,32,a.basis_32,33,a.basis_33,
				34,a.basis_34,35,a.basis_35,36,a.basis_36,37,a.basis_37,38,a.basis_38,
				39,a.basis_39,40,a.basis_40,41,a.basis_41,42,a.basis_42,43,a.basis_43,
				44,a.basis_44,45,a.basis_45,46,a.basis_46,47,a.basis_47,48,a.basis_48,
				49,a.basis_49,50,a.basis_50,51,a.basis_51,52,a.basis_52,53,a.basis_53,
				54,a.basis_54,55,a.basis_55,56,a.basis_56,57,a.basis_57,58,a.basis_58,
				59,a.basis_59,60,a.basis_60,61,a.basis_61,62,a.basis_62,63,a.basis_63,
				64,a.basis_64,65,a.basis_65,66,a.basis_66,67,a.basis_67,68,a.basis_68,
				69,a.basis_69,70,a.basis_70 
			))  ALLOC_STATISTIC
				
 FROM CPR_ACT_BASIS a,
 		UNITIZE_ELIG_UNITS_TEMP b,
		UNITIZE_ALLOC_LATE_BK_TEMP c, 
		CPR_ACTIVITY e,
		RETIREMENT_UNIT r,
		PROPERTY_UNIT p,
		UNITIZE_WO_LIST_TEMP x 
		
WHERE e.asset_id = b.ldg_asset_id
	AND a.asset_id = e.asset_id
	AND a.asset_activity_id = e.asset_activity_id
	AND x.work_order_number = e.work_order_number /*wo number join needed for mass property*/
	AND b.WORK_ORDER_ID = x.WORK_ORDER_ID 
	AND b.WORK_ORDER_ID = c.WORK_ORDER_ID  
	AND b.EXCLUDE_FROM_ALLOCATIONS = 0
	AND x.COMPANY_ID = :i_company_id
	AND x.ERROR_MSG is null  
	AND x.has_elig_adds = 1 
	AND b.expenditure_type_id = 1 
	AND b.retirement_unit_id = r.retirement_unit_id
	AND r.property_unit_id = p.property_unit_id    
	AND p.asset_acct_meth_id = 2
	and decode(c.book_summary_id,1,a.basis_1,2,a.basis_2,3,a.basis_3,4,a.basis_4,5,a.basis_5,6,a.basis_6,
				7,a.basis_7,8,a.basis_8,9,a.basis_9,10,a.basis_10,11,a.basis_11,12,a.basis_12,13,a.basis_13,14,a.basis_14,
				15,a.basis_15,16,a.basis_16,17,a.basis_17,18,a.basis_18,19,a.basis_19,20,a.basis_20,21,a.basis_21,22,a.basis_22,
				23,a.basis_23,24,a.basis_24,25,a.basis_25,26,a.basis_26,27,a.basis_27,28,a.basis_28,
				29,a.basis_29,30,a.basis_30,31,a.basis_31,32,a.basis_32,33,a.basis_33,
				34,a.basis_34,35,a.basis_35,36,a.basis_36,37,a.basis_37,38,a.basis_38,
				39,a.basis_39,40,a.basis_40,41,a.basis_41,42,a.basis_42,43,a.basis_43,
				44,a.basis_44,45,a.basis_45,46,a.basis_46,47,a.basis_47,48,a.basis_48,
				49,a.basis_49,50,a.basis_50,51,a.basis_51,52,a.basis_52,53,a.basis_53,
				54,a.basis_54,55,a.basis_55,56,a.basis_56,57,a.basis_57,58,a.basis_58,
				59,a.basis_59,60,a.basis_60,61,a.basis_61,62,a.basis_62,63,a.basis_63,
				64,a.basis_64,65,a.basis_65,66,a.basis_66,67,a.basis_67,68,a.basis_68,
				69,a.basis_69,70,a.basis_70 
			 ) <> 0 /*basis_n is a non-zero amount*/
	and exists (select 1 from CPR_LDG_BASIS z
				where z.asset_id = a.asset_id
				and decode(c.book_summary_id,1, 
				z.basis_1,2,z.basis_2,3,z.basis_3,4,z.basis_4,5,z.basis_5,6,z.basis_6,
				7,z.basis_7,8,z.basis_8,9,z.basis_9,10,z.basis_10,11,z.basis_11,12,z.basis_12,13,z.basis_13,14,z.basis_14,
				15,z.basis_15,16,z.basis_16,17,z.basis_17,18,z.basis_18,19,z.basis_19,20,z.basis_20,21,z.basis_21,22,z.basis_22,
				23,z.basis_23,24,z.basis_24,25,z.basis_25,26,z.basis_26,27,z.basis_27,28,z.basis_28,
				29,z.basis_29,30,z.basis_30,31,z.basis_31,32,z.basis_32,33,z.basis_33,
				34,z.basis_34,35,z.basis_35,36,z.basis_36,37,z.basis_37,38,z.basis_38,
				39,z.basis_39,40,z.basis_40,41,z.basis_41,42,z.basis_42,43,z.basis_43,
				44,z.basis_44,45,z.basis_45,46,z.basis_46,47,z.basis_47,48,z.basis_48,
				49,z.basis_49,50,z.basis_50,51,z.basis_51,52,z.basis_52,53,z.basis_53,
				54,z.basis_54,55,z.basis_55,56,z.basis_56,57,z.basis_57,58,z.basis_58,
				59,z.basis_59,60,z.basis_60,61,z.basis_61,62,z.basis_62,63,z.basis_63,
				64,z.basis_64,65,z.basis_65,66,z.basis_66,67,z.basis_67,68,z.basis_68,
				69,z.basis_69,70,z.basis_70 ) > 0  /*the asset cumulative basis n amount must be positive amount*/
				)
GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, c.book_summary_id
HAVING	sum(decode(c.book_summary_id,1,a.basis_1,2,a.basis_2,3,a.basis_3,4,a.basis_4,5,a.basis_5,6,a.basis_6,
				7,a.basis_7,8,a.basis_8,9,a.basis_9,10,a.basis_10,11,a.basis_11,12,a.basis_12,13,a.basis_13,14,a.basis_14,
				15,a.basis_15,16,a.basis_16,17,a.basis_17,18,a.basis_18,19,a.basis_19,20,a.basis_20,21,a.basis_21,22,a.basis_22,
				23,a.basis_23,24,a.basis_24,25,a.basis_25,26,a.basis_26,27,a.basis_27,28,a.basis_28,
				29,a.basis_29,30,a.basis_30,31,a.basis_31,32,a.basis_32,33,a.basis_33,
				34,a.basis_34,35,a.basis_35,36,a.basis_36,37,a.basis_37,38,a.basis_38,
				39,a.basis_39,40,a.basis_40,41,a.basis_41,42,a.basis_42,43,a.basis_43,
				44,a.basis_44,45,a.basis_45,46,a.basis_46,47,a.basis_47,48,a.basis_48,
				49,a.basis_49,50,a.basis_50,51,a.basis_51,52,a.basis_52,53,a.basis_53,
				54,a.basis_54,55,a.basis_55,56,a.basis_56,57,a.basis_57,58,a.basis_58,
				59,a.basis_59,60,a.basis_60,61,a.basis_61,62,a.basis_62,63,a.basis_63,
				64,a.basis_64,65,a.basis_65,66,a.basis_66,67,a.basis_67,68,a.basis_68,
				69,a.basis_69,70,a.basis_70)) <> 0 ;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_RATIO_LATE_ADD_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get the "catch all" Allocation Statistics come from the CPR total costs that match by ASSET_ID (no Basis costs)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//delete from UNITIZE_ALLOC_LATE_BK_TEMP the book summaries that were matched in the insert above
delete from UNITIZE_ALLOC_LATE_BK_TEMP a 
where a.company_id = :i_company_id
and exists (select 1 from UNITIZE_RATIO_LATE_ADD_TEMP b
				where a.work_order_id = b.work_order_id
				and a.book_summary_id = b.book_summary_id
				and b.company_id = :i_company_id
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_LATE_BK_TEMP before all costs: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//get the Total Cost allocation statistics from the CPR tables. 
insert into UNITIZE_RATIO_LATE_ADD_TEMP 
(COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, 
BUS_SEGMENT_ID, SUB_ACCOUNT_ID, ALLOC_STATISTIC, BOOK_SUMMARY_ID, COST_SOURCE 
)

/*Use CPR_LEDGER.ACCUM_COST for specific accounting method*/
SELECT v.company_id, v.work_order_id, v.unit_item_id, v.expenditure_type_id, v.utility_account_id, v.unitize_by_account,
v.bus_segment_id, v.sub_account_id, v.alloc_statistic, t.book_summary_id, v.cost_source

FROM UNITIZE_ALLOC_LATE_BK_TEMP t, /*cartesian join this table to the view*/

	(SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
	b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, 'CPR_LEDGER' cost_source,
	sum(a.accum_cost)  ALLOC_STATISTIC 				
	 FROM CPR_LEDGER a,
			UNITIZE_ELIG_UNITS_TEMP b,  
			RETIREMENT_UNIT r,
			PROPERTY_UNIT p,
			UNITIZE_WO_LIST_TEMP x 		
	WHERE a.asset_id = b.ldg_asset_id
		AND b.WORK_ORDER_ID = x.WORK_ORDER_ID  
		AND b.EXCLUDE_FROM_ALLOCATIONS = 0
		AND x.COMPANY_ID = :i_company_id
		AND x.ERROR_MSG is null  
		AND x.has_elig_adds = 1 
		AND b.expenditure_type_id = 1 
		AND b.retirement_unit_id = r.retirement_unit_id
		AND r.property_unit_id = p.property_unit_id    
		AND p.asset_acct_meth_id = 1
		and a.accum_cost > 0 /*accum_cost is a positive amount*/
		and exists (select 1 from UNITIZE_ALLOC_LATE_BK_TEMP c
						where x.work_order_id = c.work_order_id
						)  /*only insert for the wo that have late charges with mismatching book_summary_ids*/
	GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, b.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID 
	HAVING sum(a.accum_cost) <> 0 
	) v
	
WHERE v.work_order_id = t.work_order_id /*cartesian join the costs to the mismatched book_summary_ids*/

UNION ALL 

/*Use CPR_ACTIVITY.ACTIVITY_COST for mass accounting method*/
SELECT v.company_id, v.work_order_id, v.unit_item_id, v.expenditure_type_id, v.utility_account_id, v.unitize_by_account,
 v.bus_segment_id, v.sub_account_id, v.alloc_statistic, t.book_summary_id, v.cost_source

FROM UNITIZE_ALLOC_LATE_BK_TEMP t,  /*cartesian join this table to the view*/

	(SELECT x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
		b.EXPENDITURE_TYPE_ID, b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID, 'CPR_ACTIVITY' cost_source,
		sum(a.activity_cost)  ALLOC_STATISTIC					
	 FROM CPR_ACTIVITY a,
			UNITIZE_ELIG_UNITS_TEMP b, 
			RETIREMENT_UNIT r,
			PROPERTY_UNIT p,
			UNITIZE_WO_LIST_TEMP x			
	WHERE a.asset_id = b.ldg_asset_id  
		AND a.work_order_number = x.work_order_number /*wo number join needed for mass property*/ 
		AND b.WORK_ORDER_ID = x.WORK_ORDER_ID  
		AND b.EXCLUDE_FROM_ALLOCATIONS = 0
		AND x.COMPANY_ID = :i_company_id
		AND x.ERROR_MSG is null  
		AND x.has_elig_adds = 1 
		AND b.expenditure_type_id = 1 
		AND b.retirement_unit_id = r.retirement_unit_id
		AND r.property_unit_id = p.property_unit_id    
		AND p.asset_acct_meth_id = 2
		and a.activity_cost <> 0 /*activity_cost is a non-zero amount*/
		and exists (select 1 from UNITIZE_ALLOC_LATE_BK_TEMP c
						where x.work_order_id = c.work_order_id
						)  /*only insert for the wo that have late charges with mismatching book_summary_ids*/
		and exists (select 1 from CPR_LEDGER z
					where z.asset_id = a.asset_id
					and z.accum_cost > 0 /*total asset cost must be positive*/
					)
	GROUP BY x.COMPANY_ID, x.WORK_ORDER_ID, b.UNIT_ITEM_ID, 
		b.EXPENDITURE_TYPE_ID,  b.UTILITY_ACCOUNT_ID, x.UNITIZE_BY_ACCOUNT, 
		b.BUS_SEGMENT_ID, b.SUB_ACCOUNT_ID 
	HAVING sum(a.activity_cost) <> 0  
	)  v

WHERE v.work_order_id = t.work_order_id /*cartesian join the costs to the mismatched book_summary_ids*/
;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_RATIO_LATE_ADD_TEMP (all): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

/////////////////////////////////////////////////////////////////////////////////////////////
//determine if "catch all" did in fact catch all
/////////////////////////////////////////////////////////////////////////////////////////////

//delete from UNITIZE_ALLOC_LATE_BK_TEMP the book summaries that were matched in the insert above
delete from UNITIZE_ALLOC_LATE_BK_TEMP a 
where a.company_id = :i_company_id
and exists (select 1 from UNITIZE_RATIO_LATE_ADD_TEMP b
				where a.work_order_id = b.work_order_id
				and a.book_summary_id = b.book_summary_id
				and b.company_id = :i_company_id
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: delete from UNITIZE_ALLOC_LATE_BK_TEMP after all costs: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//if all eligible late book summaries have found allocation statistics for allocation basis, then this table should be empty
check_count = 0
select count(1) into :check_count
from UNITIZE_ALLOC_LATE_BK_TEMP a 
where a.company_id = :i_company_id
and a.book_summary_id is not null;

if check_count > 0 then
	error_msg = "Unitization 284: No basis for allocation. The CPR asset costs are not greater than $0"
	 
	insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
	select a.company_id, a.work_order_id, :error_msg||' for '||c.summary_name||' *Manual Unitization Recommended*'
	from UNITIZE_WO_LIST_TEMP a, UNITIZE_ALLOC_LATE_BK_TEMP b, BOOK_SUMMARY c
	where a.work_order_id = b.work_order_id
	and c.book_summary_id = b.book_summary_id
	and a.company_id = :i_company_id   
	and a.error_msg is null
	and a.has_elig_adds = 1;

	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: update UNITIZE_WO_LIST_TEMP for 284: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows  

	if num_nrows > 0 then
		//Handle errors identified above.
		of_rtn = of_errors('ALLO')
		if of_rtn <> 1 then
			//errors are handled inside function call
			return -1
		end if  
	end if 
end if 

return 1
end function

public function integer of_allocation_late_stats_adds_ratio ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Important fact about the SQLs below:  
//     If the "alloc_ratio" values are all 0 based on poor configuration, no divide by zero error will occur using RATIO_TO_REPORT,
//     because this oracle function does not divide in the tradional sense of numerator/denominator.  
//     The logic that determins the "alloc_ratio" must have additional logic to address situations where the
//     allocation records all have "alloc_ratio" = 0.
//     Without specific logic to catch scenarios where all records in the allocation
//     pool have "alloc_ratio" = 0, later functions that allocate the amounts will allocation 100% to the first
//     allocation record using the rounding syntax that compares amount to allocate vs.
//     total allocated amount.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

if i_elig_add_wos = 0 then
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   calculating ratios..."
of_log() 

//update the "alloc_ratio_by_sub" alloc_ratio to be used when CGC utility_account_id = not null and sub_account_id is not null and wo is configured FOR "unitize_by_account" = "YES."
update UNITIZE_RATIO_LATE_ADD_TEMP a
set a.alloc_ratio_by_sub = (select ratio
								from 
									(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.book_summary_id, b.utility_account_id, b.sub_account_id) ratio,
									b.company_id, b.work_order_id, b.expenditure_type_id, b.book_summary_id, b.utility_account_id, b.sub_account_id, b.unit_item_id
									from UNITIZE_RATIO_LATE_ADD_TEMP b, UNITIZE_WO_LIST_TEMP x
									where b.work_order_id = x.work_order_id
									and x.error_msg is null
									and x.company_id = :i_company_id 
									and x.has_elig_adds = 1
									and b.utility_account_id is not null
									and b.sub_account_id is not null
									and b.unitize_by_account = 1 
									) c
								where a.company_id = c.company_id
								and a.work_order_id = c.work_order_id
								and a.expenditure_type_id = c.expenditure_type_id
								and a.book_summary_id = c.book_summary_id  
								and a.utility_account_id = c.utility_account_id
								and a.sub_account_id = c.sub_account_id
								and a.unit_item_id = c.unit_item_id
								)
where a.company_id = :i_company_id 
and a.utility_account_id is not null
and a.sub_account_id is not null 
and a.unitize_by_account = 1
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_adds = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_ADD_TEMP.alloc_ratio_by_sub: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the "alloc_ratio_by_ut" alloc_ratio to be used when CGC utility_account_id = not null and sub_account_id is null and wo is configured FOR "unitize_by_account" = "YES."
//do not include sub_account_id in the filtering to determine "alloc_ratio_by_ut."
update UNITIZE_RATIO_LATE_ADD_TEMP a
set a.alloc_ratio_by_ut =(select ratio
								from 
									(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.book_summary_id, b.utility_account_id) ratio,
									b.company_id, b.work_order_id, b.expenditure_type_id, b.book_summary_id, b.utility_account_id, b.unit_item_id
									from UNITIZE_RATIO_LATE_ADD_TEMP b, UNITIZE_WO_LIST_TEMP x
									where b.work_order_id = x.work_order_id
									and x.error_msg is null
									and x.company_id = :i_company_id 
									and x.has_elig_adds = 1
									and b.utility_account_id is not null
									and b.unitize_by_account = 1 
									) c
								where a.company_id = c.company_id
								and a.work_order_id = c.work_order_id
								and a.expenditure_type_id = c.expenditure_type_id
								and a.book_summary_id = c.book_summary_id  
								and a.utility_account_id = c.utility_account_id
								and a.unit_item_id = c.unit_item_id
								)
where a.company_id = :i_company_id 
and a.utility_account_id is not null
and a.unitize_by_account = 1
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_adds = 1);


if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_ADD_TEMP.alloc_ratio_by_ut: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//update the general alloc_ratio, which is used by the "Allocation Remaining" functionality and for all CGC data (regardless of the UTILITY_ACCOUNT_ID and SUB_ACCOUNT_ID data).
//do not include utility_account_id nor sub_account_id in the filtering to determine "alloc_ratio."
update UNITIZE_RATIO_LATE_ADD_TEMP a
set a.alloc_ratio = (select ratio
						from 
							(select ratio_to_report(b.alloc_statistic) over (partition by b.work_order_id, b.expenditure_type_id, b.book_summary_id) ratio,
							b.company_id, b.work_order_id, b.expenditure_type_id, b.book_summary_id, b.unit_item_id
							from UNITIZE_RATIO_LATE_ADD_TEMP b, UNITIZE_WO_LIST_TEMP x
							where b.work_order_id = x.work_order_id
							and x.error_msg is null
							and x.company_id = :i_company_id 
							and x.has_elig_adds = 1
							) c
						where a.company_id = c.company_id
						and a.work_order_id = c.work_order_id
						and a.expenditure_type_id = c.expenditure_type_id
						and a.book_summary_id = c.book_summary_id 
						and a.unit_item_id = c.unit_item_id
						)
where a.company_id = :i_company_id 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_adds = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_ADD_TEMP.alloc_ratio: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Take care of nulls
update UNITIZE_RATIO_LATE_ADD_TEMP a
set a.alloc_ratio = 0
where a.company_id = :i_company_id 
and a.alloc_ratio is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_adds = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_ADD_TEMP.alloc_ratio(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

update UNITIZE_RATIO_LATE_ADD_TEMP a
set a.alloc_ratio_by_sub = 0
where a.company_id = :i_company_id 
and a.alloc_ratio_by_sub is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_adds = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_ADD_TEMP.alloc_ratio_by_sub(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

update UNITIZE_RATIO_LATE_ADD_TEMP a
set a.alloc_ratio_by_ut = 0
where a.company_id = :i_company_id 
and a.alloc_ratio_by_ut is null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
			where a.work_order_id = x.work_order_id
			and x.company_id = :i_company_id
			and x.error_msg is null
			and x.has_elig_adds = 1);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: UNITIZE_RATIO_LATE_ADD_TEMP.alloc_ratio_by_ut(null): " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows
 
return 1
end function

public function integer of_allocate_late_charges_by_acct ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//This function creates the allocation data that will be inserted into CHARGE_GROUP_CONTROL.
//The data saved in the UNITIZE_ALLOC_CGC_INSERTS_TEMP table will alter be saved into the  UNITIZE_ALLOC_CGC_INSERTS_ARC table. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": allocating by account..."
of_log() 

//For Late Close, use allocation_priority = 0 for the inserts into UNITIZE_ALLOC_CGC_INSERTS_TEMP.


//late Addition charges
if i_elig_add_wos > 0 then
	//alloc_ratio_by_sub
	insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
		(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id,  
		description, ratio_used, quantity, allocation_priority, 
		unitization_summary, book_summary_name, unit_item_id,
		amount  )
	select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id,
		substrb('Allocated '||a.book_summary_name, 1, 35), 'alloc_ratio_by_sub', 0 quantity, 0 allocation_priority, 
		a.unitization_summary, a.book_summary_name, b.unit_item_id,
		
			round(a.amount * b.alloc_ratio_by_sub, 2) - 
			decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
			sum(round(a.amount * b.alloc_ratio_by_sub,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
		
	from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_LATE_ADD_TEMP b, UNITIZE_WO_LIST_TEMP c, CHARGE_TYPE d
	where a.work_order_id = b.work_order_id  
	and c.work_order_id = b.work_order_id
	and c.company_id = :i_company_id
	and c.error_msg is null
	and a.charge_type_id = d.charge_type_id
	and d.book_summary_id = b.book_summary_id  
	and a.expenditure_type_id = b.expenditure_type_id
	and a.expenditure_type_id = 1 
	and a.unit_item_id is null
	and a.batch_unit_item_id is null 
	and a.utility_account_id = b.utility_account_id  /*required*/
	and a.sub_account_id = b.sub_account_id  /*required*/
	and b.unitize_by_account = 1
	and c.has_elig_adds = 1
	and not exists (select 1
						from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
						where x.work_order_id = a.work_order_id
						and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
						and x.company_id = :i_company_id
						)
						; 
		
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP for alloc_ratio_by_sub (late add): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
	
	//alloc_ratio_by_ut
	insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
		(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id,  
		description, ratio_used, quantity, allocation_priority, 
		unitization_summary, book_summary_name, unit_item_id,
		amount  )
	select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id, 
		substrb('Allocated '||a.book_summary_name, 1, 35), 'alloc_ratio_by_ut', 0 quantity, 0 allocation_priority, 
		a.unitization_summary, a.book_summary_name, b.unit_item_id,
		
			round(a.amount * b.alloc_ratio_by_ut, 2) - 
			decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
			sum(round(a.amount * b.alloc_ratio_by_ut,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
		
	from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_LATE_ADD_TEMP b, UNITIZE_WO_LIST_TEMP c, CHARGE_TYPE d
	where a.work_order_id = b.work_order_id  
	and c.work_order_id = b.work_order_id
	and c.company_id = :i_company_id
	and c.error_msg is null
	and a.charge_type_id = d.charge_type_id
	and d.book_summary_id = b.book_summary_id  
	and a.expenditure_type_id = b.expenditure_type_id
	and a.expenditure_type_id = 1 
	and a.unit_item_id is null
	and a.batch_unit_item_id is null 
	and a.utility_account_id = b.utility_account_id  /*required*/
	and a.sub_account_id is null
	and b.unitize_by_account = 1
	and c.has_elig_adds = 1
	and not exists (select 1
						from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
						where x.work_order_id = a.work_order_id 
						and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
						and x.company_id = :i_company_id
						)
						; 
		
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP for alloc_ratio_by_ut (late add): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 

//Late RWIP charges
if i_elig_rwip_wos > 0 then
	//alloc_ratio_by_sub
	insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
		(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id,  
		description, ratio_used, quantity, allocation_priority, 
		unitization_summary, book_summary_name, unit_item_id,
		amount  )
	select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id,
		substrb('Allocated '||a.book_summary_name, 1, 35), 'alloc_ratio_by_sub', 0 quantity, 0 allocation_priority, 
		a.unitization_summary, a.book_summary_name, b.unit_item_id,
		
			round(a.amount * b.alloc_ratio_by_sub, 2) - 
			decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
			sum(round(a.amount * b.alloc_ratio_by_sub,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
		
	from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_LATE_RWIP_TEMP b, UNITIZE_WO_LIST_TEMP c, CHARGE_TYPE d
	where a.work_order_id = b.work_order_id  
	and c.work_order_id = b.work_order_id
	and c.company_id = :i_company_id
	and c.error_msg is null
	and a.charge_type_id = d.charge_type_id
	and d.processing_type_id = b.processing_type_id  
	and a.expenditure_type_id = b.expenditure_type_id
	and a.expenditure_type_id = 2 
	and d.processing_type_id <> 1 /*non-OCR*/ 
	and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
	and a.unit_item_id is null
	and a.batch_unit_item_id is null 
	and a.utility_account_id = b.utility_account_id  /*required*/
	and a.sub_account_id = b.sub_account_id  /*required*/
	and b.unitize_by_account = 1
	and c.has_elig_rwip = 1
	and not exists (select 1
						from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
						where x.work_order_id = a.work_order_id
						and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
						and x.company_id = :i_company_id
						)
						; 
		
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP for alloc_ratio_by_sub (late rwip): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
	
	//alloc_ratio_by_ut
	insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
		(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id,  
		description, ratio_used, quantity, allocation_priority, 
		unitization_summary, book_summary_name, unit_item_id,
		amount  )
	select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id, 
		substrb('Allocated '||a.book_summary_name, 1, 35), 'alloc_ratio_by_ut', 0 quantity, 0 allocation_priority, 
		a.unitization_summary, a.book_summary_name, b.unit_item_id,
		
			round(a.amount * b.alloc_ratio_by_ut, 2) - 
			decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
			sum(round(a.amount * b.alloc_ratio_by_ut,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
		
	from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_LATE_RWIP_TEMP b, UNITIZE_WO_LIST_TEMP c, CHARGE_TYPE d
	where a.work_order_id = b.work_order_id  
	and c.work_order_id = b.work_order_id
	and c.company_id = :i_company_id
	and c.error_msg is null
	and a.charge_type_id = d.charge_type_id
	and d.processing_type_id = b.processing_type_id  
	and a.expenditure_type_id = b.expenditure_type_id
	and a.expenditure_type_id = 2
	and d.processing_type_id <> 1 /*non-OCR*/ 
	and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
	and a.unit_item_id is null
	and a.batch_unit_item_id is null 
	and a.utility_account_id = b.utility_account_id  /*required*/
	and a.sub_account_id is null
	and b.unitize_by_account = 1
	and c.has_elig_rwip = 1
	and not exists (select 1
						from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
						where x.work_order_id = a.work_order_id 
						and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
						and x.company_id = :i_company_id
						)
						; 
		
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP for alloc_ratio_by_ut (late rwip): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Check for inability to allocate for Unitize by Account = Yes.  
//It is very important to perform the error validations immediately after allocation.
//If this error validation is not done right now, the Allocate Remaining functionaly will perform Charge Group allocation across un-matching Sub/UT accounts. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//1.  It is an allocation error for failing to find a matching Unit Item for a particular UTILITY_ACCOUNT_ID with SUB_ACCOUNT_ID = not null when the wo has "Unitize by Account" = "Yes".
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id,  
substrb(decode(a.expenditure_type_id, 1, 
'Unitization 135: Cannot allocate Additions for Charge Type = "'||c.description||'" with Unitize by Account = Yes.  No Unit Items found for utility_account_id = '||to_char(a.utility_account_id)||' and sub_account_id = '||to_char(a.sub_account_id)||'. *Change Unitize by Account to "No" or Manually Unitize late charges*',
'Unitization 135R: Cannot allocate RWIP for Charge Type = "'||c.description||'" with Unitize by Account = Yes.  No Unit Items found for utility_account_id = '||to_char(a.utility_account_id)||' and sub_account_id = '||to_char(a.sub_account_id)||'. *Change Unitize by Account to "No" or Manually Unitize late charges*'  
), 1, 2000)
from CHARGE_GROUP_CONTROL a, UNITIZE_WO_LIST_TEMP b, CHARGE_TYPE c 
where a.work_order_id = b.work_order_id
and b.company_id = :i_company_id 
and a.charge_type_id = c.charge_type_id 
and a.expenditure_type_id in (1,2)
and c.processing_type_id <> 1 /*non-OCR*/ 
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and b.unitize_by_account = 1
and a.amount <> 0 
and a.batch_unit_item_id is null
and a.unit_item_id is null
and a.utility_account_id is not null  
and a.sub_account_id is not null  
and b.error_msg is null
and NOT EXISTS (select 1 from UNITIZE_ALLOC_CGC_INSERTS_TEMP e
					 where a.work_order_id = e.work_order_id  
					 and a.expenditure_type_id = e.expenditure_type_id 
					 and e.orig_charge_group_id = a.charge_group_id  /*already allocated charges*/ 
					 and e.company_id = :i_company_id 
					 );

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (135.1 late): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if
 
//2.  It is an allocation error for failing to find a matching Unit Item for a particular UTILITY_ACCOUNT_ID with SUB_ACCOUNT_ID = null when the wo has "Unitize by Account" = "Yes".
insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct b.company_id, b.work_order_id, 
substrb(decode(a.expenditure_type_id, 1, 
'Unitization 135: Cannot allocate late Additions for Charge Type = "'||c.description||'" with Unitize by Account = Yes.  No Unit Items found for utility_account_id = '||to_char(a.utility_account_id)||'. *Change Unitize by Account to No or Manually Unitize late charges*',
'Unitization 135R: Cannot allocate late RWIP for Charge Type = "'||c.description||'" with Unitize by Account = Yes.  No Unit Items found for utility_account_id = '||to_char(a.utility_account_id)||'. *Change Unitize by Account to No or Manually Unitize late charges*' 
), 1, 2000)
from CHARGE_GROUP_CONTROL a, UNITIZE_WO_LIST_TEMP b, CHARGE_TYPE c 
where a.work_order_id = b.work_order_id
and b.company_id = :i_company_id 
and a.charge_type_id = c.charge_type_id 
and a.expenditure_type_id in (1,2)
and c.processing_type_id <> 1 /*non-OCR*/ 
and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
and b.unitize_by_account = 1
and a.amount <> 0 
and a.batch_unit_item_id is null
and a.unit_item_id is null
and a.utility_account_id is not null  
and a.sub_account_id is null  
and b.error_msg is null
and NOT EXISTS (select 1 from UNITIZE_ALLOC_CGC_INSERTS_TEMP e
					 where a.work_order_id = e.work_order_id  
					 and a.expenditure_type_id = e.expenditure_type_id 
					 and e.orig_charge_group_id = a.charge_group_id  /*already allocated charges*/ 
					 and e.company_id = :i_company_id 
					 );

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_ALLOC_ERRORS_TEMP insert (135.2 late): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if

return 1
end function

public function integer of_allocate_late_charges_remaining ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//This function creates the allocation data that will be inserted into CHARGE_GROUP_CONTROL.
//The data saved in the UNITIZE_ALLOC_CGC_INSERTS_TEMP table will alter be saved into the  UNITIZE_ALLOC_CGC_INSERTS_ARC table. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": allocating remaining charges..."
of_log() 

////last step in allocation of charges to unit items
////this function will occur after allocation by utility account and sub account
////do not filter for unitize_by_account = 0 in this function.  
////this function should apply to all remaining charges not yet allocated.

//For Late Close, use allocation_priority = 0 for the insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP

//late Addition charges
if i_elig_add_wos > 0 then
	insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
		(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id, 
		description, ratio_used, quantity, allocation_priority, 
		unitization_summary, book_summary_name, unit_item_id,
		amount  )
	select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id, 
		substrb('Allocated '||a.book_summary_name, 1, 35), 'alloc_ratio', 0 quantity, 0 allocation_priority, 
		a.unitization_summary, a.book_summary_name, b.unit_item_id,
		
		round(a.amount * b.alloc_ratio, 2) - 
			decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
			sum(round(a.amount * b.alloc_ratio,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
		
	from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_LATE_ADD_TEMP b, UNITIZE_WO_LIST_TEMP c, CHARGE_TYPE d
	where a.work_order_id = b.work_order_id  
	and c.work_order_id = b.work_order_id
	and c.company_id = :i_company_id
	and d.charge_type_id = a.charge_type_id
	and c.error_msg is null
	and a.expenditure_type_id = b.expenditure_type_id 
	and a.expenditure_type_id = 1
	and c.has_elig_adds = 1
	and b.book_summary_id = d.book_summary_id  
	and a.unit_item_id is null
	and a.batch_unit_item_id is null 
	and not exists (select 1
						from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
						where x.work_order_id = a.work_order_id 
						and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
						and x.company_id = :i_company_id
						)
						; 
		
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP remaining: " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 

if i_elig_rwip_wos > 0 then
	//late RWIP charges
	insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP
		(company_id, orig_charge_group_id, work_order_id, alloc_id, charge_type_id, expenditure_type_id, 
		description, ratio_used, quantity, allocation_priority, 
		unitization_summary, book_summary_name, unit_item_id,
		amount  )
	select b.company_id, a.charge_group_id, a.work_order_id, a.alloc_id, a.charge_type_id, a.expenditure_type_id, 
		'Allocated Removal/Salvage', 'alloc_ratio', 0 quantity, 0 allocation_priority, 
		a.unitization_summary, a.book_summary_name, b.unit_item_id,
		
		round(a.amount * b.alloc_ratio, 2) - 
			decode(row_number() over (partition by a.work_order_id, a.charge_group_id order by abs(b.alloc_statistic) desc, b.unit_item_id),1,  
			sum(round(a.amount * b.alloc_ratio,2)) over (partition by a.work_order_id, a.charge_group_id) - a.amount,0) SPREAD_WITH_ROUNDING_WITH_PLUG
		
	from CHARGE_GROUP_CONTROL a, UNITIZE_RATIO_LATE_RWIP_TEMP b, UNITIZE_WO_LIST_TEMP c, CHARGE_TYPE d
	where a.work_order_id = b.work_order_id  
	and c.work_order_id = b.work_order_id
	and c.company_id = :i_company_id
	and d.charge_type_id = a.charge_type_id
	and c.error_msg is null
	and a.expenditure_type_id = b.expenditure_type_id 
	and a.expenditure_type_id = 2
	and b.processing_type_id = d.processing_type_id  
	and d.processing_type_id <> 1  /*non-OCR*/
	and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
	and c.has_elig_rwip = 1
	and a.unit_item_id is null
	and a.batch_unit_item_id is null 
	and not exists (select 1
						from UNITIZE_ALLOC_CGC_INSERTS_TEMP x
						where x.work_order_id = a.work_order_id 
						and x.orig_charge_group_id = a.charge_group_id  /*do not allocate already allocated charges*/
						and x.company_id = :i_company_id
						)
						; 
		
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_TEMP remaining (late): " + sqlca.SQLErrText
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows 
end if 

return 1
end function

public function integer of_allocation_reconcile ();//compare the sum(amount) of the split and allocated charge_group_id to the amount of the original charge_group_id
longlong count, of_rtn, num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": balancing allocation results..."
of_log() 

insert into UNITIZE_ALLOC_ERRORS_TEMP (company_id, work_order_id, error_msg)
select distinct a.company_id, b.work_order_id,  
substrb(
		'Unitization 199: Allocated results out of balance for charge_group_id = '||to_char(b.charge_group_id)||', charge_type_id = '||to_char(b.charge_type_id)||', amount = '||to_char(b.amount)||' vs total_amount = '||to_char(allocated_total.tot_amount)
		, 1, 2000)
from UNITIZE_WO_LIST_TEMP a, CHARGE_GROUP_CONTROL b, UNIT_ALLOCATION c, 
		(select sum(d.amount) tot_amount, d.orig_charge_group_id, d.work_order_id
		from UNITIZE_ALLOC_CGC_INSERTS_TEMP d, UNITIZE_WO_LIST_TEMP e
		where d.work_order_id = e.work_order_id
		and e.error_msg is null
		and e.company_id = :i_company_id
		group by d.orig_charge_group_id, d.work_order_id
		) allocated_total
where a.work_order_id = b.work_order_id
and b.alloc_id = c.alloc_id
and a.company_id = :i_company_id
and b.expenditure_type_id in (1,2)
and b.batch_unit_item_id is null
and b.unit_item_id is not null
and b.alloc_id is not null 
and a.error_msg is null
and b.work_order_id = allocated_total.work_order_id 
and b.charge_group_id = allocated_total.orig_charge_group_id  /*easy join of original charge_group_id to the split and allocated new charge_group_ids*/
and b.amount <> allocated_total.tot_amount;
 
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ALLOC_ERRORS_TEMP (reconcile): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows       

if num_nrows > 0 then
	//Handle errors identified above.  
	of_rtn = of_errors('ALLO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if

return 1
end function

public function integer of_identify_types_of_elig_charges ();/////////////////////////////////////////////////////////////////////////////////////////////////////////
//updates HAS_ELIG_ADDS, HAS_ELIG_RWIP, HAS_GAIN_LOSS_OCR for the wo list.
////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong add_rows, rwip_rows, ocr_rows, num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": evaluating elig charges..."
of_log() 

//eligible Addition charges to Unitize
update UNITIZE_WO_LIST_TEMP a
set a.HAS_ELIG_ADDS = 1
where a.company_id = :i_company_id
and exists (select 1 from CHARGE_GROUP_CONTROL b
				where a.work_order_id = b.work_order_id
				and b.expenditure_type_id = 1 
				and b.batch_unit_item_id is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update UNITIZE_WO_LIST_TEMP.HAS_ELIG_ADDS: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
add_rows = sqlca.sqlnrows   
i_elig_add_wos = add_rows

if i_elig_add_wos > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   Addition charges..."
	of_log()  
end if


//eligible RWIP charges to Unitize
update UNITIZE_WO_LIST_TEMP a
set a.HAS_ELIG_RWIP = 1
where a.company_id = :i_company_id
and exists (select 1 from CHARGE_GROUP_CONTROL b
				where a.work_order_id = b.work_order_id
				and b.expenditure_type_id = 2
				and b.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/
				and b.batch_unit_item_id is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update UNITIZE_WO_LIST_TEMP.HAS_ELIG_RWIP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
rwip_rows = sqlca.sqlnrows    
i_elig_rwip_wos = rwip_rows

if i_elig_rwip_wos > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   RWIP charges..."
	of_log() 
end if

//eligible OCR units with gain loss
update UNITIZE_WO_LIST_TEMP a
set a.HAS_GAIN_LOSS_OCR = 1
where a.company_id = :i_company_id
and exists (select 1 from CHARGE_GROUP_CONTROL b, UNITIZED_WORK_ORDER c
				where a.work_order_id = b.work_order_id
				and b.work_order_id = c.work_order_id
				and b.unit_item_id = c.unit_item_id
				and trim(c.activity_code) in ('URGL', 'SAGL')
				and b.expenditure_type_id = 2
				and b.charge_type_id = :i_ocr_charge_type_id  /*OCR*/
				and b.batch_unit_item_id is null
				and nvl(b.pend_transaction,0) = 0
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update UNITIZE_WO_LIST_TEMP.HAS_GAIN_LOSS_OCR: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
ocr_rows = sqlca.sqlnrows   
i_elig_ocr = ocr_rows

if i_elig_ocr > 0 then
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ":   OCR gain/loss..."
	of_log() 
end if

//save this information into the Archive table
update UNITIZE_WO_LIST_ARC a
set (a.HAS_ELIG_ADDS, a.HAS_ELIG_RWIP, a.HAS_GAIN_LOSS_OCR) = (select b.HAS_ELIG_ADDS, b.HAS_ELIG_RWIP, b.HAS_GAIN_LOSS_OCR
																					from UNITIZE_WO_LIST_TEMP b
																					where a.work_order_id = b.work_order_id
																					)
where a.company_id = :i_company_id
and a.db_session_id = :i_this_session_id 
and a.unit_closed_month_number = :i_month_number
and a.run_time_string = :i_run_time_string
and a.run = :i_run
and a.unitization_type = :i_unitization_type;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_ARC for has_ data: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

return 1
end function

public function integer of_start_single_wo (longlong a_wo_id, longlong a_month_number, string a_unitize_regular, string a_unitize_late, boolean a_visual);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//this function is called from the Completion window:  Auto Unitize button for a single wo
//
//a_company_id:  company being unitized
//a_month_number:  gl month of unitization
//a_process_id:  the process_id for Online Logs is not used for single wo, no online logs made for single wo.
//a_type:  there 2 "types" of unitization for a single work order:  AUTO WO, AUTO LATE WO
//a_visual:  true = show more message outputs to the w_wo_status_box; false = less messages
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//need to make sure the single wo being unitized is not already in an Auto Unitization List, meaning it is being unitized already.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong co_id, of_rtn, check_count, rtn_code_main, check_pend_count, of_rtn_auto101_errors
string msg, fast_101

i_single_wo = true
i_single_wo_id = a_wo_id

i_month_number = a_month_number
i_process_id = 0   //always use 0 for process id for single wo auto unitization to avoid creating Online Logs 
i_visual = a_visual 

select company_id, work_order_number into :i_company_id, :i_wo_number
from work_order_control where work_order_id = :a_wo_id;

fast_101 = lower(f_pp_system_control_company('AUTO101 - FAST NO LOOP', i_company_id))
if fast_101 = 'no' or fast_101 = '' or isnull(fast_101) then
	//nothing to do here, return to calling function
	return 0
end if  

//clear out work orders unitized in prior disconnected sessions. a commit is inside this function.
of_rtn = of_clear_wo_list_prior_sessions()
if of_rtn <> 1 then
	//errors handled inside this function already
	return -1
end if 	

//check for concurrent unitization for this wo
check_count = 0
select count(1) into :check_count
from UNITIZE_WO_LIST
where work_order_id = :a_wo_id;

if check_count > 0 then
	//the work order is arleady unitizing 
	f_wo_status_box("",  "ERROR: This work order is already being unitized.  Please do not duplicate unitization processing for this work order.  The company may be Auto Unitizing that includes this work order." )
	f_wo_status_box("",  "" ) 
	return -1
end if 

//Initialize other variables needed for processing
of_rtn = of_initialize_variables()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 
 
//Get the company description
select description into :i_co_desc
from company where company_id = :i_company_id;

//a commit is executed in of_start_msg() to increment the "run" for WO_AUTO101_CONTROL table.
of_rtn = of_start_msg()
if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if

of_rtn = of_single_wo_auto101_control('DELETE')
if of_rtn < 0 then
	//errors are handled inside function call
	return -1
end if

//Start regular auto unitization of company
if UPPER(a_unitize_regular) = 'YES' then
	i_unitization_type = 'AUTO'
	rtn_code_main = of__main()

	//Since the main processing is finished, clear out the UNITIZE_WO_LIST table for wos unitized in this run. a commit is inside this function.
	of_rtn = of_clear_wo_list_curr_session()
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1 
	end if   	 
	
	//check if an error has been encountered during regular unitization.  keep in mine the old errors for the single wo were cleared out prior to running unitization.
	//if an error occurs, no need to try Late Close.
	of_rtn_auto101_errors = of_single_wo_auto101_control('CHECK')
	if of_rtn_auto101_errors < 0 then
		//errors are handled inside function call
		return rtn_code_main
	end if 
	
	//check and see if regular close unitization pending transaction were successfully made for this work order.
	//if pending were made, running late close for the work order will not create more pending transactions.  no need to try Late Close.
	check_pend_count = 0
	select count(1) into :check_pend_count 
	from pend_transaction 
	where ldg_depr_group_id < 0
	and company_id = :i_company_id 
	and work_order_number = :i_wo_number;
	
	if isnull(check_pend_count) then check_pend_count = 0

end if

if check_pend_count > 0 then
	//nothing more needed for this work order
else
	//No pending and no errors created from regualr unitization, so start late unitization of the work order.
	if UPPER(a_unitize_late) = 'YES' then
		i_unitization_type = 'AUTO LATE'
		rtn_code_main = of__main_late()			

		//Since the main processing is finished, clear out the UNITIZE_WO_LIST table for wos unitized in this run. a commit is inside this function.
		of_rtn = of_clear_wo_list_curr_session()
		if of_rtn <> 1 then
			//errors are handled inside function call
			return -1 
		end if   
		
		//check if an error has been encountered during late close unitization.  keep in mine the old errors for the single wo were cleared out prior to running unitization.
		//if an error occurs, no need to insert "finished" unitization message for this single wo.
		of_rtn_auto101_errors = of_single_wo_auto101_control('CHECK')
		if of_rtn_auto101_errors < 0 then
			//errors are handled inside function call
			return rtn_code_main
		end if

		//check and see if late close unitization pending transaction were successfully made for this work order. 
		check_pend_count = 0
		select count(1) into :check_pend_count 
		from pend_transaction 
		where ldg_depr_group_id < 0
		and company_id = :i_company_id 
		and work_order_number = :i_wo_number;
		
		if isnull(check_pend_count) then check_pend_count = 0
	
	end if	
end if 

//fatal error in regular unitization should stop unitization and avoid starting late unitization.
if rtn_code_main < 0 then
	return rtn_code_main
end if 

//check if the work order qualified for either regular or late close.  if no qualify, there would be no pending and no errors.
if check_pend_count = 0 and of_rtn_auto101_errors = 0 then
	if ib_called_from_w_wo_close then
		f_wo_status_box("Automatic Unitization", "INFO:  This work order is not eligible for Auto Unitization at this time.")
	end if 
else
	
	i_msg[] = i_null_arrary[]
	i_msg[1] =   ": Finished " + i_unitization_type + " Unitization for " + i_co_desc + "."  
	of_log()
	
	//a commit is inside this function.
	of_rtn = of_finished_msg()
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if
end if

return rtn_code_main

end function

public function integer of_single_wo_auto101_control (string a_type_of_call);//a_type_of_call = DELETE, CHECK
//returns 0 for success in this of_single_wo_auto101_control() function.

longlong check_error_count,num_nrows
string wo_unitization_error_msg

choose case a_type_of_call 
	case 'DELETE'  
		delete from wo_auto101_control where work_order_id = :i_single_wo_id;
					
		if sqlca.SQLCode < 0 then 
			i_msg[] = i_null_arrary[] 
			i_msg[1] =   ": ERROR: delete from wo_auto101_control for single wo: " + sqlca.SQLErrText
			of_log() 
			rollback;		 
			return -1
		end if		 
		num_nrows = sqlca.sqlnrows    

	case 'CHECK'

		check_error_count = 0
		select count(1), max(message)  into :check_error_count, :wo_unitization_error_msg
		from wo_auto101_control
		where work_order_id = :i_single_wo_id;
		
		if isnull(check_error_count) then check_error_count = 0	
		
		if check_error_count > 0 then 
			//no need to try for Late Close for this work order. Display error for the Completion window.
			f_wo_status_box("Automatic Unitization", "")  
			f_wo_status_box("Automatic Unitization", "ERROR: " + wo_unitization_error_msg)
			
			check_error_count = check_error_count*-1
			return check_error_count
		end if 
end choose

return 0
end function

public function integer of_unitization_elig_unit_items ();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Insert data into the UNITIZE_ELIG_UNITS_TEMP global temp table as a side table of UNITIZED_WORK_ORDER, which is a table that can get very large over time.
//The data inside UNITIZE_ELIG_UNITS_TEMP is only available unit items, excluding already used unit items.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, of_rtn

//create a side global temp table of UNITIZED_WORK_ORDER table data.
i_msg[] = i_null_arrary[] 
i_msg[1] =  ": gathering eligible unit items..."
of_log()    

//first insert missing data into wo_unit_item_pend_trans
//discovered there can be OCR units with BATCH_UNIT_ITEM_ID > 0 and no rows in WO_UNIT_ITEM_PEND_TRANS table.
insert into wo_unit_item_pend_trans (work_order_id, unit_item_id, pend_trans_id)
select distinct a.work_order_id, a.unit_item_id, 0
from charge_group_control a, UNITIZE_WO_LIST_TEMP b
where a.work_order_id = b.work_order_id
and b.company_id = :i_company_id
and b.error_msg is null
and a.expenditure_type_id = 2 
and a.charge_type_id = :i_ocr_charge_type_id /*OCR*/
and a.batch_unit_item_id > 0  /*do not include negative batch_unit_item_ids*/  
and a.unit_item_id > 0
and not exists (select 1 from wo_unit_item_pend_trans b
					where a.work_order_id = b.work_order_id
					and a.unit_item_id = b.unit_item_id
					);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into wo_unit_item_pend_trans (stage avail units): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

//clear out the global temp table
delete from UNITIZE_ELIG_UNITS_TEMP;
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ": ERROR: delete from UNITIZE_ELIG_UNITS_TEMP: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

insert into UNITIZE_ELIG_UNITS_TEMP ( WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, ACTIVITY_CODE, GL_ACCOUNT_ID,
UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID, 
SERIAL_NUMBER, LDG_ASSET_ID, UNIT_ESTIMATE_ID, QUANTITY, STATUS, EXCLUDE_FROM_ALLOCATIONS, MIN_RWIP_BATCH_UNIT_ID, REPLACEMENT_AMOUNT)
SELECT a.WORK_ORDER_ID, a.UNIT_ITEM_ID, decode(upper(trim(a.activity_code)), 'UADD', 1, 'MADD', 1, 2) ,  upper(trim(a.activity_code)), a.GL_ACCOUNT_ID,
a.UTILITY_ACCOUNT_ID, a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID, a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID, 
trim(a.SERIAL_NUMBER), a.LDG_ASSET_ID, a.UNIT_ESTIMATE_ID, NVL(a.QUANTITY,0), a.STATUS, nvl(a.EXCLUDE_FROM_ALLOCATIONS,0), b.MIN_RWIP_BATCH_UNIT_ID, a.REPLACEMENT_AMOUNT
FROM UNITIZED_WORK_ORDER a, UNITIZE_WO_LIST_TEMP b
WHERE a.WORK_ORDER_ID = b.WORK_ORDER_ID   
AND b.COMPANY_ID = :i_company_id     
AND b.ERROR_MSG is null 
AND a.UNIT_ITEM_ID > 0 
AND NOT EXISTS (select 1 from WO_UNIT_ITEM_PEND_TRANS c 
					 where a.unit_item_id = c.unit_item_id    
					 and a.work_order_id = c.work_order_id 
						)    
AND upper(trim(a.activity_code)) in ('UADD', 'MADD', 'URET', 'MRET', 'URGL', 'SAGL', 'SALE');

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: insert into UNITIZE_ELIG_UNITS_TEMP(stage units): " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

//Save to real table for troubleshooting
of_rtn = of_archive_elig_units_temp()

if of_rtn <> 1 then
	//errors are handled inside function call
	return -1
end if 


return 1
end function

public function integer of_unitization_net_zero ();//************************************************************************************************
//
//  Function             :  of_unitization_net_zero  *NEW method*
//
//  Description          :  Identify charge groups that net $0 by expenditure_type_id and charge_type_id to Direct Assign to a Unit Item.
//
//  Timing                :  to be called after normal Direct Assign in the Unitization procesing of Auto and Manual Unitization.  
//
//************************************************************************************************
string net_zero_control_value
longlong num_nrows, check_count, of_rtn, num_nrows1, num_nrows2

net_zero_control_value = lower(f_pp_system_control_company('UNITIZATION - Net Zero Check', i_company_id))

if net_zero_control_value <> 'yes' then 
	//nothing to do
	return 1
end if 

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": tagging net $0 charges..."
of_log() 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//update the CHARGE_GROUP_CONTROL table for NET_ZERO column
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//called from Fast Auto Unitization (use UNITIZE_WO_LIST_TEMP global temp table) 
update charge_group_control a
set a.net_zero = (
					select b.charge_type_id
					from charge_group_control b, UNITIZE_WO_LIST_TEMP c
					where a.work_order_id = b.work_order_id
					and b.work_order_id = c.work_order_id 
					and c.company_id = :i_company_id
					and c.error_msg is null
					and a.expenditure_type_id = b.expenditure_type_id
					and a.charge_type_id = b.charge_type_id
					and b.expenditure_type_id in (1,2)
					and b.charge_type_id <> :i_ocr_charge_type_id
					and b.batch_unit_item_id is null
					and nvl(b.pend_transaction, 0) = 0
					and b.unit_item_id is null
					group by b.work_order_id, b.expenditure_type_id, b.charge_type_id
					having sum(b.amount) = 0
					)
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and a.unit_item_id is null 
and a.batch_unit_item_id is null
and nvl(a.pend_transaction, 0) = 0
and a.expenditure_type_id in (1,2)
and a.charge_type_id <> :i_ocr_charge_type_id
and a.net_zero is null
and exists (select 1
				from charge_group_control b, UNITIZE_WO_LIST_TEMP c
				where a.work_order_id = b.work_order_id
				and b.work_order_id = c.work_order_id 
				and c.company_id = :i_company_id
				and c.error_msg is null
				and a.expenditure_type_id = b.expenditure_type_id
				and a.charge_type_id = b.charge_type_id
				and b.expenditure_type_id in (1,2)
				and b.charge_type_id <> :i_ocr_charge_type_id
				and b.batch_unit_item_id is null
				and nvl(b.pend_transaction, 0) = 0
				and b.unit_item_id is null
				group by b.work_order_id, b.expenditure_type_id, b.charge_type_id
				having sum(b.amount) = 0
			);
			
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update NET_ZERO: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

if num_nrows = 0 then
	//check if net_zero had UNIT_ITEM_ID = null, which means the data processing below needs to be done:
	check_count = 0
	select count(1) into :check_count
	from charge_group_control a
	where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)   
	and a.unit_item_id is null
	and a.batch_unit_item_id is null
	and nvl(a.pend_transaction, 0) = 0
	and a.expenditure_type_id in (1,2)
	and a.charge_type_id <> :i_ocr_charge_type_id
	and a.net_zero is not null;
	
	if check_count = 0 then			
		//nothing to do
		return 1
	end if  
end if 

/////////////////////////////////////////////////////////////////////////////////////////
//identify the work order that have net $0 charges 
/////////////////////////////////////////////////////////////////////////////////////////
//set has_net_zero_adds
update UNITIZE_WO_LIST_TEMP a
set a.HAS_NET_ZERO_ADDS = 1
where a.company_id = :i_company_id
and a.error_msg is null
and a.has_elig_adds = 1	
and exists (select 1 from CHARGE_GROUP_CONTROL c
				where a.work_order_id = c.work_order_id
				and c.expenditure_type_id = 1
				and c.batch_unit_item_id is null
				and c.unit_item_id is null
				and c.net_zero is not null
				);
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update has_net_zero_adds: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//set has_net_zero_rwip
update UNITIZE_WO_LIST_TEMP a
set a.HAS_NET_ZERO_RWIP = 1
where a.company_id = :i_company_id
and a.error_msg is null
and a.has_elig_rwip = 1	
and exists (select 1 from CHARGE_GROUP_CONTROL c
				where a.work_order_id = c.work_order_id
				and c.expenditure_type_id = 2
				and c.batch_unit_item_id is null
				and c.unit_item_id is null
				and c.net_zero is not null
				);
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update has_net_zero_rwip: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows
 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//update the CHARGE_GROUP_CONTROL table for UNIT_ITEM_ID column (special Direct Assign)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
update charge_group_control a
set a.unit_item_id = (select max(b.unit_item_id) from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
							where a.work_order_id = b.work_order_id 
							and b.work_order_id = c.work_order_id
							and c.company_id = :i_company_id
							and c.error_msg is null
							and nvl(c.HAS_NET_ZERO_ADDS,0) + nvl(c.HAS_NET_ZERO_RWIP,0) > 0 
							and a.expenditure_type_id = b.expenditure_type_id
							),
	a.orig_charge_group_id = a.charge_group_id 
where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/
and a.batch_unit_item_id is null
and nvl(a.pend_transaction, 0) = 0
and a.unit_item_id is null
and a.expenditure_type_id in (1,2)
and a.charge_type_id <> :i_ocr_charge_type_id
and a.net_zero is not null
and exists (select 1 from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
			where a.work_order_id = b.work_order_id 
			and b.work_order_id = c.work_order_id
			and c.company_id = :i_company_id
			and c.error_msg is null
			and nvl(c.HAS_NET_ZERO_ADDS,0) + nvl(c.HAS_NET_ZERO_RWIP,0) > 0 
			and a.expenditure_type_id = b.expenditure_type_id
			);
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update UNIT_ITEM_ID: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if			 
num_nrows = sqlca.sqlnrows     

//maint-43648: only call of_update_charge_groups_unitized() if Net Zero happened
if num_nrows > 0 then
	//update the CGC columns to match to the UWO columns for Unitized charge groups.
	of_rtn = of_update_charge_groups_unitized('NETZERO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1 
	end if 
end if  

commit;

/////////////////////////////////////////////////////////////////////////////////////////
//identify the work order that have net $0 charges and no Unit Items
/////////////////////////////////////////////////////////////////////////////////////////

//determine if addotopm net $0 charges have no Unit Item 
update UNITIZE_WO_LIST_TEMP a 
set a.CREATE_NET_ZERO_ADD = 1
where a.company_id = :i_company_id
and a.error_msg is null
and a.HAS_NET_ZERO_ADDS = 1	 	
and not exists (select 1 from UNITIZE_ELIG_UNITS_TEMP b
					where a.work_order_id = b.work_order_id
					and b.expenditure_type_id = 1
					);
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update CREATE_NET_ZERO_ADD: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

if num_nrows > 0 then 
	//tag the ADD charges for fake unitization if non_unitized_status is the same.
	of_rtn = of_unitization_net_zero_adds()
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if   
	commit;
end if

//determine if rwip net $0 charges have no Unit Item 
update UNITIZE_WO_LIST_TEMP a 
set a.CREATE_NET_ZERO_RWIP = 1
where a.company_id = :i_company_id
and a.error_msg is null
and a.HAS_NET_ZERO_RWIP = 1	 	
and not exists (select 1 from UNITIZE_ELIG_UNITS_TEMP b
					where a.work_order_id = b.work_order_id
					and b.expenditure_type_id = 2
					);
				
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update CREATE_NET_ZERO_RWIP: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows  

if num_nrows > 0 then 
	//tag the RWIP charges for fake unitization. 
	of_rtn = of_unitization_net_zero_rwip()
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if   
	commit;
end if  

////////////////////////////////////////////////////////////////
//save this information into the Archive table
////////////////////////////////////////////////////////////////
update UNITIZE_WO_LIST_ARC a
set (a.has_net_zero_adds, a.has_net_zero_rwip, a.create_net_zero_add, a.create_net_zero_rwip, a.HAS_ELIG_ADDS, a.HAS_ELIG_RWIP ) = 
	(select b.has_net_zero_adds, b.has_net_zero_rwip, b.create_net_zero_add, b.create_net_zero_rwip, b.HAS_ELIG_ADDS, b.HAS_ELIG_RWIP 
	from UNITIZE_WO_LIST_TEMP b
	where a.work_order_id = b.work_order_id
	and nvl(b.HAS_NET_ZERO_ADDS,0) + nvl(b.HAS_NET_ZERO_RWIP,0) > 0 
	)
where a.company_id = :i_company_id
and a.db_session_id = :i_this_session_id 
and a.unit_closed_month_number = :i_month_number
and a.run_time_string = :i_run_time_string
and a.run = :i_run
and a.unitization_type = :i_unitization_type
and exists (select 1 from UNITIZE_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id
				and nvl(b.HAS_NET_ZERO_ADDS,0) + nvl(b.HAS_NET_ZERO_RWIP,0) > 0 
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: updating UNITIZE_WO_LIST_ARC for net_zero data: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

commit;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//make sure the UNIT_ITEM_ID on the CHARGE_GROUP_CONTROL table is direct assigned for net zero charge groups
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
update UNITIZE_WO_LIST_TEMP a
set a.rtn_code = -1, a.error_msg = 'Unitization 286: No Unit Item made from Estimates or Actuals required for Addition charges.*Add missing info and try again*'
where a.company_id = :i_company_id
and a.error_msg is null
and a.HAS_NET_ZERO_ADDS = 1
and exists (select 1 from charge_group_control b
				where a.work_order_id = b.work_order_id 
				and b.batch_unit_item_id is null
				and b.unit_item_id is null
				and b.expenditure_type_id = 1
				and b.net_zero is not null
			)
;
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update error_msg 286: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if			 
num_nrows1 = sqlca.sqlnrows   

update UNITIZE_WO_LIST_TEMP a
set a.rtn_code = -1, a.error_msg = 'Unitization 286R: No Unit Item made from Estimates or Actuals required for RWIP charges.*Add missing info and try again*'
where a.company_id = :i_company_id
and a.error_msg is null
and a.HAS_NET_ZERO_RWIP = 1
and exists (select 1 from charge_group_control b
				where a.work_order_id = b.work_order_id 
				and b.batch_unit_item_id is null
				and b.unit_item_id is null
				and b.expenditure_type_id = 2
				and b.charge_type_id <> :i_ocr_charge_type_id
				and b.net_zero is not null
			)
;
		
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update error_msg 286R: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if			 
num_nrows2 = sqlca.sqlnrows   

if num_nrows1 > 0 or num_nrows2 > 0 then
	//Handle errors identified in the validation check above.  
	of_rtn = of_errors('WO')
	if of_rtn <> 1 then
		//errors are handled inside function call
		return -1
	end if  
end if 

return 1
end function

public function integer of_sync_wo_est_serial_number ();//******************************************************************************************
//
//  Function     :  of_sync_wo_est_serial_number
//
//  Description  :   this function will update (1) WO_ESTIMATE for the wo/revision and (2) UNITIZED_WORK_ORDER to sync to the CPR_LEDGER table.
//                      the sql joins the ASSET_ID.  the related assets will be negative ASSET_IDs on the tables, and we do not want to update those records. 
//
//******************************************************************************************
longlong num_nrows 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//update the WO_ESTIMATE table
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
update wo_estimate a
set a.serial_number = (select b.serial_number
							from cpr_ledger b
							where a.asset_id = b.asset_id
							and nvl(trim(a.serial_number),'nullserialnumber') <> nvl(trim(b.serial_number), 'nullserialnumber')
							)
where a.batch_unit_item_id is null
and a.expenditure_type_id in (1,2)
and a.asset_id > 0
and exists (select 1
				from cpr_ledger b
				where a.asset_id = b.asset_id
				and nvl(trim(a.serial_number),'nullserialnumber') <> nvl(trim(b.serial_number), 'nullserialnumber')
				)
and exists (select 1 
				from UNITIZE_WO_LIST_TEMP c
				where a.work_order_id = c.work_order_id
				and a.revision = c.max_revision
				and c.error_msg is null
				);
 
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update wo_estimate.serial_number: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows       

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//update the UNITIZED_WORK_ORDER table 
//need to do this for Unit Item creation and Allocation joins to work.
//without this update, if Reset Unitization is not done, the Unit Item creation will create another Unit with the correct serial_number
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
update unitized_work_order a
set a.serial_number = (select b.serial_number
							from cpr_ledger b
							where a.ldg_asset_id = b.asset_id
							and nvl(trim(a.serial_number),'nullserialnumber') <> nvl(trim(b.serial_number), 'nullserialnumber')
							)
where a.ldg_asset_id > 0
and not exists (select 1 
					from wo_unit_item_pend_trans pt
					where pt.work_order_id = a.work_order_id
					and pt.unit_item_id = a.unit_item_id
					)	
and exists (select 1
				from cpr_ledger b
				where a.ldg_asset_id = b.asset_id
				and nvl(trim(a.serial_number),'nullserialnumber') <> nvl(trim(b.serial_number), 'nullserialnumber')
				)
and exists (select 1 
				from UNITIZE_WO_LIST_TEMP c
				where a.work_order_id = c.work_order_id 
				and c.error_msg is null
				); 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update unitized_work_order.serial_number: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows     

return 1
end function

public function integer of_update_charge_groups_unitized (string a_type_cgc);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//this function backfills the unit attributes for the CHARGE_GROUP_CONTROL records where UNIT_ITEM_ID got assigned.   
//this update must be performed after UNIT_ITEM_ID is updated on CGC for Direct Assign, Allocation, and Special Direct Assign in Net $0 Unitization.  
//There is an expectation for the CGC fields to match UNITIZED_WORK_ORDER fields when UNIT_ITEM_ID is not null.
//called after Direct Assign or Allocation:  a_type_cgc is  'DIRECT' or 'ALLO'
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong num_nrows
string sqls

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   updating (" + lower(a_type_cgc) + ") chg groups..." 
of_log() 

//maint-43648: Use merge statement here for better performance.

choose case a_type_cgc
	case 'NETZERO' 
		//called after Net Zero tagging
		
		//CAT: 4/7/15: Use merge here instead.
//		update CHARGE_GROUP_CONTROL a
//		set (a.retirement_unit_id, a.utility_account_id, a.bus_segment_id, a.sub_account_id, a.property_group_id, a.asset_location_id, a.serial_number) =
//			(select b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, b.property_group_id, b.asset_location_id, b.serial_number 
//			from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
//			where a.unit_item_id = b.unit_item_id
//			and a.work_order_id = b.work_order_id
//			and b.work_order_id = c.work_order_id
//			and c.company_id = :i_company_id	
//			and c.error_msg is null 
//			)
//		where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/ 
//		and a.charge_group_id = a.orig_charge_group_id /*Direct Assigned*/
//		and a.alloc_id is not null /*links to CWIP_CHARGE*/ 
//		and a.net_zero is not null
//		and a.batch_unit_item_id is null 
//		and a.expenditure_type_id in (1, 2)
//		and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
//		and a.unit_item_id > 0
//		and exists (select 1
//						from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
//						where a.unit_item_id = b.unit_item_id
//						and a.work_order_id = b.work_order_id
//						and b.work_order_id = c.work_order_id
//						and c.company_id = :i_company_id	
//						and c.error_msg is null 
//					);  
			
			sqls = ' merge into ( select * '
			sqls +=	'				from charge_group_control '
			sqls +=	'					where charge_group_id = orig_charge_group_id '
			sqls +=	'					and alloc_id is not null '
			sqls +=	'					and net_zero is not null '
			sqls +=	'					and batch_unit_item_id is null  '
			sqls +=	'					and expenditure_type_id in (1, 2) '
			sqls +=	'					and charge_type_id <> ' + string(i_ocr_charge_type_id) + ' '
			sqls +=	'					and unit_item_id > 0 '
			sqls +=	'				) a '
			sqls +=	'using ( select b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, b.property_group_id, b.asset_location_id, b.serial_number, b.unit_item_id, b.work_order_id  '
			sqls +=	'			from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c '
			sqls +=	'			where b.work_order_id = c.work_order_id '
			sqls +=	'			and c.company_id = ' + string(i_company_id) + ' '
			sqls +=	'			and c.error_msg is null  '
			sqls +=	'		 ) d '
			sqls +=	'on (a.work_order_id = d.work_order_id '
			sqls +=	'		and a.unit_item_id = d.unit_item_id '
			sqls +=	'	  ) '
			sqls +=	'when matched then '
			sqls +=	'	update set a.retirement_unit_id = d.retirement_unit_id,  '
			sqls +=	'	a.utility_account_id = d.utility_account_id,  '
			sqls +=	'	a.bus_segment_id = d.bus_segment_id,  '
			sqls +=	'	a.sub_account_id = d.sub_account_id,  '
			sqls +=	'	a.property_group_id = d.property_group_id,  '
			sqls +=	'	a.asset_location_id = d.asset_location_id,  '
			sqls +=	'	a.serial_number = d.serial_number '
			
			execute immediate :sqls;

		
	case 'DIRECT'  
		//called after Direct Assign 
		
		//CAT: 4/7/15: Change this to a merge.  See if helps performance.
//		update CHARGE_GROUP_CONTROL a
//		set (a.retirement_unit_id, a.utility_account_id, a.bus_segment_id, a.sub_account_id, a.property_group_id, a.asset_location_id, a.serial_number) =
//			(select b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, b.property_group_id, b.asset_location_id, b.serial_number 
//			from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
//			where a.unit_item_id = b.unit_item_id
//			and a.work_order_id = b.work_order_id
//			and b.work_order_id = c.work_order_id
//			and c.company_id = :i_company_id	
//			and c.error_msg is null 
//			)
//		where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/ 
//		and a.charge_group_id = a.orig_charge_group_id /*Direct Assigned*/
//		and a.alloc_id is not null /*links to CWIP_CHARGE*/ 
//		and a.batch_unit_item_id is null 
//		and a.expenditure_type_id in (1, 2)
//		and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
//		and a.unit_item_id > 0
//		and exists (select 1
//						from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
//						where a.unit_item_id = b.unit_item_id
//						and a.work_order_id = b.work_order_id
//						and b.work_order_id = c.work_order_id
//						and c.company_id = :i_company_id	
//						and c.error_msg is null 
//					);

		sqls = 'merge into ( select * '
		sqls +=	'  					from charge_group_control '
		sqls +=	' 					where charge_group_id = orig_charge_group_id '
		sqls +=	'					and net_zero is null '
		sqls +=	' 					and alloc_id is not null '
		sqls +=	' 					and batch_unit_item_id is null '
		sqls +=	' 					and expenditure_type_id in (1,2) '
		sqls +=	' 					and charge_type_id <> ' + string(i_ocr_charge_type_id) + ' '
		sqls +=	' 					and unit_item_id > 0 '
		sqls +=	' 				) a '
		sqls +=	' using (	select b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, b.property_group_id, b.asset_location_id, b.serial_number, b.work_order_id, b.unit_item_id  '
		sqls +=	' 			from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c '
		sqls +=	' 			where b.work_order_id = c.work_order_id '
		sqls +=	' 			and c.company_id = ' + string(i_company_id) + '	'
		sqls +=	' 			and c.error_msg is null  '
		sqls +=	' 		 ) d '
		sqls +=	' on (a.work_order_id = d.work_order_id '
		sqls +=	' 		and a.unit_item_id = d.unit_item_id) '
		sqls +=	' when matched then '
		sqls +=	' 	update set a.retirement_unit_id = d.retirement_unit_id, '
		sqls +=	' 	a.utility_account_id = d.utility_account_id, '
		sqls +=	' 	a.bus_segment_id = d.bus_segment_id, '
		sqls +=	' 	a.sub_account_id = d.sub_account_id, '
		sqls +=	' 	a.property_group_id = d.property_group_id, '
		sqls +=	' 	a.asset_location_id = d.asset_location_id, '
		sqls +=	' 	a.serial_number = d.serial_number '
		
		execute immediate :sqls;
		
		//CAT: 4/7/15: End change.

	
case 'ALLO'
	
	//should NOT be called after Allocations 
	
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update charge_group_control attributes backfill not supposed to be called!"
	of_log() 
	rollback;		 
	return -1 


//	//CAT: 4/8/15: Use merge here instead.
	
//	update CHARGE_GROUP_CONTROL a
//	set (a.retirement_unit_id, a.utility_account_id, a.bus_segment_id, a.sub_account_id, a.property_group_id, a.asset_location_id, a.serial_number) =
//		(select b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, b.property_group_id, b.asset_location_id, b.serial_number 
//		from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
//		where a.unit_item_id = b.unit_item_id
//		and a.work_order_id = b.work_order_id
//		and b.work_order_id = c.work_order_id
//		and c.company_id = :i_company_id	
//		and c.error_msg is null 
//		)
//	where exists (select 1 from UNITIZE_WO_LIST_TEMP c where a.work_order_id = c.work_order_id and c.company_id = :i_company_id and c.error_msg is null)  /*adding this makes the sql faster*/ 
//	and a.alloc_id is null /*created from Allocations*/ 
//	and a.batch_unit_item_id is null 
//	and a.expenditure_type_id in (1, 2)
//	and a.charge_type_id <> :i_ocr_charge_type_id  /*non-OCR*/ 
//	and a.unit_item_id > 0
//	and exists (select 1
//					from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c
//					where a.unit_item_id = b.unit_item_id
//					and a.work_order_id = b.work_order_id
//					and b.work_order_id = c.work_order_id
//					and c.company_id = :i_company_id	
//					and c.error_msg is null 
//				);
//
//	sqls = ' merge into ( select * '
//	sqls += '					from charge_group_control '
//	sqls += '						where alloc_id is null  '
//	sqls += '						and batch_unit_item_id is null  '
//	sqls += '						and expenditure_type_id in (1, 2) '
//	sqls += '						and charge_type_id <> ' + string(i_ocr_charge_type_id) + ' '
//	sqls += '						and unit_item_id > 0 '
//	sqls += '					) a '
//	sqls += '	using (	select b.retirement_unit_id, b.utility_account_id, b.bus_segment_id, b.sub_account_id, b.property_group_id, b.asset_location_id, b.serial_number, b.unit_item_id, b.work_order_id  '
//	sqls += '				from UNITIZE_ELIG_UNITS_TEMP b, UNITIZE_WO_LIST_TEMP c '
//	sqls += '				where b.work_order_id = c.work_order_id '
//	sqls += '				and c.company_id = ' + string(i_company_id) + ' '	
//	sqls += '				and c.error_msg is null  '
//	sqls += '			) d '
//	sqls += '	on (a.unit_item_id = d.unit_item_id '
//	sqls += '			and a.work_order_id = d.work_order_id '
//	sqls += '		 ) '
//	sqls += '	when matched then '
//	sqls += '		update set a.retirement_unit_id = d.retirement_unit_id,  '
//	sqls += '		a.utility_account_id = d.utility_account_id,  '
//	sqls += '		a.bus_segment_id = d.bus_segment_id,  '
//	sqls += '		a.sub_account_id = d.sub_account_id,  '
//	sqls += '		a.property_group_id = d.property_group_id,  '
//	sqls += '		a.asset_location_id = d.asset_location_id,  '
//	sqls += '		a.serial_number = d.serial_number '
//	
//	execute immediate :sqls;
//	

end choose

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: update charge_group_control attributes backfill: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows       

return 1
end function

public function integer of_old_net_zero_unitization ();///*OLD method*/
//string control_value, wo_list_sqls, rtn_string
//longlong wo_id, i, num_wos, num_nrows
//uo_ds_top ds_wos_list
//
////JRD - 10/27/07 - Before allocation starts, mark any charge groups
//// that sum to zero based on any attribute that can be used for allocation methods
//// this includes for example charge type because even though a bucket may result in
//// zero, the allocation method could make it so that one unit item does not get a 
//// basis of zero for that bucket.
//
//// ### RJO - Request #2083 - 6/27/2008 - Added checks to:
////     (1) Prevent updates to unit items whith charges in more than one non-unitized status.
////         For example, you do not want to exclude a charge group from unitization if it has 
////         $100 in 106 and $-100 in 107, even though it nets zero.
////     (2) Do not update original cost retirement items.  Mass retirements will have amount=0 on
////         charge_group_control and were being picked up by this statement.  This was preventing 
////         them from ever posting.
//
////SJH - 10/21/2008 ADDED " AND cc.expenditure_type_id = z.expenditure_type_id" TO ALLOW SAVL/COR RETIREMENT CHARGES TO QUALIFY FOR NET $0
////SJH - 11/12/2008 ADDED " AND Z.WORK_ORDER_ID = WOCG.WORK_ORDER_ID 
////SJH - 1/21/2009 REPLACE CHARGE_GROUP_ID JOIN WITH CHARGE_TYPE_ID JOIN IN THE COUNT(DISTCINT SQL
////SJH - 1/23/2009 ADDED EXTRA SQL AFTER HAVING SUM(amount) = 0 TO EXCLUDE ALLOCATED/TARGET ITEMS
////SJH - 10/20/2010 ADDED EXTRA SQL in the outer update for NVL (z.pend_transaction, 0) = 0 AND z.batch_unit_item_id IS NULL
////SJH - 10/20/2010 ADDED EXTRA SQL in the subselect for count(distinct...) to include unit_closed_month_number
////SJH-  12/09/2010 modify the sql ito include this line of sql in the inner and outter WHERE clauses in the sql to only include and update 
////           charge_group_control data where unit_item_id is null, which identifies the unitization data as new data that has not yet been involved in any allocation or direct assignments:
//
//
//// ### RJO - 3789 - Added system control to turn off this check if the client desires
//control_value = lower(f_pp_system_control_company('UNITIZATION - Net Zero Check', i_company_id))
//
//if control_value <> 'yes' then 
//	//nothing to do
//	return 1
//end if 
//
//i_msg[] = i_null_arrary[] 
//i_msg[1] =  ": OLD tagging net $0 charges..."
//of_log() 
//
//ds_wos_list = create uo_ds_top
//
//wo_list_sqls = "select work_order_id from UNITIZE_WO_LIST_TEMP where error_msg is null and company_id = " + string(i_company_id)  
//rtn_string = f_create_dynamic_ds(ds_wos_list, 'grid', wo_list_sqls, sqlca, true)
//
//if upper(rtn_string) <> 'OK' then 
//	i_msg[] = i_null_arrary[] 
//	i_msg[1] =  ": ERROR: unable to create ds_wos_list for net $0: " + rtn_string
//	of_log() 
//	rollback;		  
//	return -1 
//end if 
//
//num_wos = ds_wos_list.RowCount()
//
//for i = 1 to num_wos
//
//	wo_id = ds_wos_list.getitemnumber(i, 1)
//	
//	UPDATE charge_group_control z
//		SET z.pend_transaction = 1,
//			 z.batch_unit_item_id = -13,
//			 z.alloc_id = NULL
//	 WHERE z.work_order_id = :wo_id
//		AND 1 >= // ensure all charges assigned to this unit item have the same non-unitized status
//									 (SELECT COUNT (DISTINCT nvl(non_unitized_status,0))
//													 FROM cwip_charge cc, work_order_charge_group wocg
//													WHERE cc.work_order_id = wocg.work_order_id
//													  AND cc.charge_id = wocg.charge_id  AND Z.WORK_ORDER_ID = WOCG.WORK_ORDER_ID 
//													  AND cc.charge_type_id = z.charge_type_id AND cc.unit_closed_month_number is null 
//													  AND cc.expenditure_type_id = z.expenditure_type_id)
//			AND (z.work_order_id,
//			  NVL (z.alloc_id, 0),
//			  NVL (z.utility_account_id, 0),
//			  NVL (z.bus_segment_id, 0),
//			  NVL (z.sub_account_id, 0),
//			  z.charge_type_id,
//			  z.expenditure_type_id,
//			  z.book_summary_name,
//			  NVL(z.asset_location_id, 0),
//			  NVL(z.serial_number, 'Z1')
//			 ) IN (
//				 SELECT  cgc.work_order_id, NVL(cgc.alloc_id, 0),
//							 NVL(cgc.utility_account_id, 0), NVL(cgc.bus_segment_id, 0),
//							 NVL(cgc.sub_account_id, 0), cgc.charge_type_id,
//							 cgc.expenditure_type_id, cgc.book_summary_name,
//							 NVL(cgc.asset_location_id, 0), NVL(cgc.serial_number, 'Z1')
//					  FROM charge_group_control cgc, charge_type ct
//					 WHERE cgc.work_order_id = :wo_id
//						AND NVL(cgc.pend_transaction, 0) = 0
//						AND cgc.batch_unit_item_id IS NULL
//						AND cgc.unit_item_id IS NULL
//						AND cgc.charge_type_id = ct.charge_type_id
//						AND ct.processing_type_id <> 1 // exclude Original Cost Retirements
//				 GROUP BY cgc.work_order_id,
//							 NVL(cgc.alloc_id, 0),
//							 NVL(cgc.utility_account_id, 0),
//							 NVL(cgc.bus_segment_id, 0),
//							 NVL(cgc.sub_account_id, 0),
//							 cgc.charge_type_id,
//							 cgc.expenditure_type_id,
//							 cgc.book_summary_name,
//							 NVL(cgc.asset_location_id, 0),
//							 NVL(cgc.serial_number, 'Z1')
//					HAVING SUM (cgc.amount) = 0
//					)
//	and exists (select 1 from work_order_charge_group v
//					where v.charge_group_id = z.charge_group_id
//					and v.work_order_id = z.work_order_id
//					)
//	and nvl(z.pend_transaction,0) = 0
//	and z.batch_unit_item_id is null
//	and z.unit_item_id IS NULL;
//				
//	if sqlca.SQLCode < 0 then 
//		i_msg[] = i_null_arrary[] 
//		i_msg[1] =  ": ERROR: net $0 unitization: " + sqlca.SQLErrText
//		of_log() 
//		rollback;	
//		destroy ds_wos_list	 
//		return -1
//	end if		
//	num_nrows = sqlca.sqlnrows    
//	
//next 
//	
//destroy ds_wos_list
return 1
end function

public function integer of_validate_wos_equip_ledger ();longlong num_nrows

if i_cpr_equip_type_num = 0 then
	return 1
end if

i_msg[] = i_null_arrary[] 
i_msg[1] =  ": checking equip ledger..."
of_log() 

//the cpr_equip_estimate_relate might not have the work_order_id filled in at the time of unitization.    
update cpr_equip_estimate_relate a
set a.work_order_id = (select b.work_order_id
							from wo_estimate b, UNITIZE_WO_LIST_TEMP c 
							where a.estimate_id = b.estimate_id
							and a.revision = b.revision
							and b.work_order_id = c.work_order_id
							and b.revision = c.max_revision
							and c.company_id = :i_company_id
							and c.error_msg is null
							)
where a.work_order_id is null;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1]  =   ": ERROR: update cpr_equip_estimate_relate.work_order_id: " + sqlca.SQLErrText
	of_log()
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows 

//required to be setup for "one for one" est option
UPDATE UNITIZE_WO_LIST_TEMP a
SET a.rtn_code = -1, a.error_msg = 'Unitization 288: Work orders with Equip Ledger estimates must use One for One Unit Item Est Option. *Change Unit Item Est Level Option and Reset Unitization.*'
where a.company_id = :i_company_id
and a.error_msg is null
and a.est_unit_item_option = 0  /*summarize*/
and exists (select 1
				from cpr_equip_estimate_relate b, wo_estimate c
				where a.work_order_id = b.work_order_id 
				and b.estimate_id = c.estimate_id
				and b.work_order_id = c.work_order_id
				and b.revision = c.revision
				and a.max_revision = c.revision
				and c.batch_unit_item_id is null
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: UNITIZE_WO_LIST_TEMP eq ledger one for one check: " + sqlca.SQLErrText
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows    

return 1

end function

public function integer of_archive_wo_list_temp ();longlong num_nrows

delete from UNITIZE_WO_LIST_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_WO_LIST_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//Maint 41295:  RTN_CODE is no longer hijacked for i_this_session_id
//                   ERROR_MSG is no longer hijacked for i_closing_type

insert into UNITIZE_WO_LIST_ARC (UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, WORK_ORDER_ID , WORK_ORDER_NUMBER, COMPANY_ID,
	BUS_SEGMENT_ID , MAJOR_LOCATION_ID, ASSET_LOCATION_ID, WO_STATUS_ID, IN_SERVICE_DATE, COMPLETION_DATE , LATE_CHG_WAIT_PERIOD,
	FUNDING_WO_ID, DESCRIPTION, LONG_DESCRIPTION , CLOSING_OPTION_ID, CWIP_GL_ACCOUNT , NON_UNITIZED_GL_ACCOUNT, UNITIZED_GL_ACCOUNT,
	ALLOC_METHOD_TYPE_ID , ACCRUAL_TYPE_ID , RWIP_YES_NO , EST_UNIT_ITEM_OPTION   , UNIT_ITEM_FROM_ESTIMATE , UNIT_ITEM_SOURCE, UNITIZE_BY_ACCOUNT, 
	TOLERANCE_ID,  TOLERANCE_REVISION, TOLERANCE_THRESHOLD, TOLERANCE_PCT, TOLERANCE_ESTIMATE_AMT, TOLERANCE_ACTUAL_AMT, TOLERANCE_EST_ACT_DIFF,
	WOA_FUNC_CLASS_ID, MAX_CHARGE_GROUP_ID, MAX_REVISION, MAX_UNIT_ITEM_ID, UNITIZATION_TYPE, LATE_MONTH_NUMBER, DB_SESSION_ID, CLOSING_TYPE, REPLACEMENT_PERCENTAGE) 
select :i_month_number, :i_run_time_string, :i_run, WORK_ORDER_ID , WORK_ORDER_NUMBER, COMPANY_ID,
	BUS_SEGMENT_ID , MAJOR_LOCATION_ID, ASSET_LOCATION_ID, WO_STATUS_ID, IN_SERVICE_DATE, COMPLETION_DATE , LATE_CHG_WAIT_PERIOD,
	FUNDING_WO_ID, DESCRIPTION, LONG_DESCRIPTION , CLOSING_OPTION_ID, CWIP_GL_ACCOUNT , NON_UNITIZED_GL_ACCOUNT, UNITIZED_GL_ACCOUNT,
	ALLOC_METHOD_TYPE_ID , ACCRUAL_TYPE_ID , RWIP_YES_NO , EST_UNIT_ITEM_OPTION   , UNIT_ITEM_FROM_ESTIMATE , UNIT_ITEM_SOURCE, UNITIZE_BY_ACCOUNT, 
	TOLERANCE_ID,  TOLERANCE_REVISION, TOLERANCE_THRESHOLD, TOLERANCE_PCT, TOLERANCE_ESTIMATE_AMT, TOLERANCE_ACTUAL_AMT, TOLERANCE_EST_ACT_DIFF,
	WOA_FUNC_CLASS_ID, MAX_CHARGE_GROUP_ID, MAX_REVISION, MAX_UNIT_ITEM_ID, UNITIZATION_TYPE, :i_late_month_number, :i_this_session_id, :i_closing_type, replacement_percentage 
	from UNITIZE_WO_LIST_TEMP; 

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_WO_LIST_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_archive_alloc_list_temp ();longlong num_nrows

delete from UNITIZE_ALLOC_LIST_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_ALLOC_LIST_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into UNITIZE_ALLOC_LIST_ARC ( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID, ALLOC_ID, ALLOC_PRIORITY, ALLOC_TYPE, ALLOC_BASIS)
	select  :i_month_number, :i_run_time_string, :i_run, COMPANY_ID, ALLOC_ID, ALLOC_PRIORITY, ALLOC_TYPE, ALLOC_BASIS
from UNITIZE_ALLOC_LIST_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_ALLOC_LIST_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_archive_elig_units_temp ();longlong num_nrows

if i_net_zero_inserting then
	
	insert into UNITIZE_ELIG_UNITS_ARC
		( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID, WORK_ORDER_ID , UNIT_ITEM_ID , EXPENDITURE_TYPE_ID, ACTIVITY_CODE, GL_ACCOUNT_ID, UTILITY_ACCOUNT_ID,
		 BUS_SEGMENT_ID, SUB_ACCOUNT_ID , RETIREMENT_UNIT_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID , SERIAL_NUMBER , LDG_ASSET_ID, UNIT_ESTIMATE_ID ,
		 STATUS,  QUANTITY , EXCLUDE_FROM_ALLOCATIONS)
	select :i_month_number, :i_run_time_string, :i_run, b.company_id, a.WORK_ORDER_ID , a.new_unit_item_id , decode(upper(trim(a.activity_code)), 'UADD', 1, 'MADD', 1, 2) , a.ACTIVITY_CODE, a.GL_ACCOUNT_ID, a.UTILITY_ACCOUNT_ID,
		 a.BUS_SEGMENT_ID, a.SUB_ACCOUNT_ID , a.RETIREMENT_UNIT_ID, a.ASSET_LOCATION_ID, a.PROPERTY_GROUP_ID , a.SERIAL_NUMBER , a.LDG_ASSET_ID, a.UNIT_ESTIMATE_ID ,
		 a.STATUS,  a.QUANTITY , 1
	from UNITIZE_WO_STG_UNITS_TEMP a, UNITIZE_WO_LIST_TEMP b
	where a.work_order_id = b.work_order_id
	and b.company_id = :i_company_id
	and nvl(b.CREATE_NET_ZERO_ADD,0) + nvl(b.CREATE_NET_ZERO_RWIP,0) > 0
	and a.UNIT_ITEM_SOURCE = 'NET_ZERO'
	and b.error_msg is null
	and a.error_msg is null;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: insert into UNITIZE_ELIG_UNITS_ARC (net zero): " + sqlca.SQLErrText 
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
	
else
	
	delete from UNITIZE_ELIG_UNITS_ARC
	where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
	and company_id = :i_company_id;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: delete from UNITIZE_ELIG_UNITS_ARC: " + sqlca.SQLErrText 
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
	
	insert into UNITIZE_ELIG_UNITS_ARC
		( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID, WORK_ORDER_ID , UNIT_ITEM_ID , EXPENDITURE_TYPE_ID, ACTIVITY_CODE, GL_ACCOUNT_ID, UTILITY_ACCOUNT_ID,
		 BUS_SEGMENT_ID, SUB_ACCOUNT_ID , RETIREMENT_UNIT_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID , SERIAL_NUMBER , LDG_ASSET_ID, UNIT_ESTIMATE_ID ,
		 STATUS, EXCLUDE_FROM_ALLOCATIONS, QUANTITY, REPLACEMENT_AMOUNT )
	select :i_month_number, :i_run_time_string, :i_run, :i_company_id, WORK_ORDER_ID , UNIT_ITEM_ID , EXPENDITURE_TYPE_ID, ACTIVITY_CODE, GL_ACCOUNT_ID, UTILITY_ACCOUNT_ID,
		 BUS_SEGMENT_ID, SUB_ACCOUNT_ID , RETIREMENT_UNIT_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID , SERIAL_NUMBER , LDG_ASSET_ID, UNIT_ESTIMATE_ID ,
		 STATUS, EXCLUDE_FROM_ALLOCATIONS, QUANTITY, REPLACEMENT_AMOUNT
	from UNITIZE_ELIG_UNITS_TEMP;
	
	if sqlca.SQLCode < 0 then 
		i_msg[] = i_null_arrary[] 
		i_msg[1] = ": ERROR: insert into UNITIZE_ELIG_UNITS_ARC: " + sqlca.SQLErrText 
		of_log() 
		rollback;		 
		return -1
	end if		
	num_nrows = sqlca.sqlnrows
	
end if 

return 1
end function

public function integer of_archive_ratio_units_temp ();longlong num_nrows

delete from UNITIZE_RATIO_UNITS_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_RATIO_UNITS_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into UNITIZE_RATIO_UNITS_ARC
	( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID , WORK_ORDER_ID , EXPENDITURE_TYPE_ID , ALLOC_ID, ALLOC_PRIORITY,
	 UNIT_ITEM_ID , ALLOC_TYPE , ALLOC_BASIS , UNITIZE_BY_ACCOUNT, UTILITY_ACCOUNT_ID , BUS_SEGMENT_ID, SUB_ACCOUNT_ID  ,
	 ALLOC_STATISTIC , ALLOC_RATIO  , ALLOC_RATIO_BY_UT  , ALLOC_RATIO_BY_SUB  )
 select   :i_month_number, :i_run_time_string, :i_run, COMPANY_ID , WORK_ORDER_ID , EXPENDITURE_TYPE_ID , ALLOC_ID, ALLOC_PRIORITY,
	 UNIT_ITEM_ID , ALLOC_TYPE , ALLOC_BASIS , UNITIZE_BY_ACCOUNT, UTILITY_ACCOUNT_ID , BUS_SEGMENT_ID, SUB_ACCOUNT_ID  ,
	 ALLOC_STATISTIC , ALLOC_RATIO  , ALLOC_RATIO_BY_UT  , ALLOC_RATIO_BY_SUB   
 from UNITIZE_RATIO_UNITS_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_RATIO_UNITS_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_archive_ratio_late_add_temp ();longlong num_nrows

delete from UNITIZE_RATIO_LATE_ADD_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_RATIO_LATE_ADD_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into UNITIZE_RATIO_LATE_ADD_ARC ( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, 
	UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, BOOK_SUMMARY_ID, COST_SOURCE, ALLOC_STATISTIC,
	ALLOC_RATIO, ALLOC_RATIO_BY_UT, ALLOC_RATIO_BY_SUB)
select  :i_month_number, :i_run_time_string, :i_run, COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, 
	UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, BOOK_SUMMARY_ID, COST_SOURCE, ALLOC_STATISTIC,
	ALLOC_RATIO, ALLOC_RATIO_BY_UT, ALLOC_RATIO_BY_SUB
from UNITIZE_RATIO_LATE_ADD_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_RATIO_LATE_ADD_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_archive_ratio_late_rwip_temp ();longlong num_nrows

delete from UNITIZE_RATIO_LATE_RWIP_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_RATIO_LATE_RWIP_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into UNITIZE_RATIO_LATE_RWIP_ARC ( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, 
	UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, PROCESSING_TYPE_ID, COST_SOURCE, ALLOC_STATISTIC,
	ALLOC_RATIO, ALLOC_RATIO_BY_UT, ALLOC_RATIO_BY_SUB)
select  :i_month_number, :i_run_time_string, :i_run, COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, 
	UTILITY_ACCOUNT_ID, UNITIZE_BY_ACCOUNT, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, PROCESSING_TYPE_ID, COST_SOURCE, ALLOC_STATISTIC,
	ALLOC_RATIO, ALLOC_RATIO_BY_UT, ALLOC_RATIO_BY_SUB
from UNITIZE_RATIO_LATE_RWIP_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_RATIO_LATE_RWIP_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_archive_alloc_late_bk_temp ();longlong num_nrows

delete from UNITIZE_ALLOC_LATE_BK_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_ALLOC_LATE_BK_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into UNITIZE_ALLOC_LATE_BK_ARC ( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID, WORK_ORDER_ID, BOOK_SUMMARY_ID) 
	select  :i_month_number, :i_run_time_string, :i_run, COMPANY_ID, WORK_ORDER_ID, BOOK_SUMMARY_ID 
from UNITIZE_ALLOC_LATE_BK_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_ALLOC_LATE_BK_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_archive_alloc_late_pt_temp ();longlong num_nrows

delete from UNITIZE_ALLOC_LATE_PT_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_ALLOC_LATE_PT_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into UNITIZE_ALLOC_LATE_PT_ARC ( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, COMPANY_ID, WORK_ORDER_ID, PROCESSING_TYPE_ID) 
	select  :i_month_number, :i_run_time_string, :i_run, COMPANY_ID, WORK_ORDER_ID, PROCESSING_TYPE_ID 
from UNITIZE_ALLOC_LATE_PT_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_ALLOC_LATE_PT_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

return 1
end function

public function integer of_archive_alloc_cgc_inserts_temp ();longlong num_nrows

delete from UNITIZE_ALLOC_CGC_INSERTS_ARC
where UNIT_CLOSED_MONTH_NUMBER < :i_delete_month_number
and company_id = :i_company_id;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: delete from UNITIZE_ALLOC_CGC_INSERTS_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

insert into UNITIZE_ALLOC_CGC_INSERTS_ARC ( UNIT_CLOSED_MONTH_NUMBER, RUN_TIME_STRING, RUN, ID,
	company_id, orig_charge_group_id, new_charge_group_id, work_order_id, alloc_id, unit_item_id, utility_account_id, bus_segment_id, sub_account_id, charge_type_id, 
	expenditure_type_id, retirement_unit_id, property_group_id, asset_location_id, serial_number, description, group_indicator, allocation_priority, unitization_summary, 
	book_summary_name, amount, quantity, ratio_used ) 
	select  :i_month_number, :i_run_time_string, :i_run, id, 
	company_id, orig_charge_group_id, new_charge_group_id, work_order_id, alloc_id, unit_item_id, utility_account_id, bus_segment_id, sub_account_id, charge_type_id, 	
	expenditure_type_id, retirement_unit_id, property_group_id, asset_location_id, serial_number, description, group_indicator, allocation_priority, unitization_summary, 
	book_summary_name, amount, quantity, ratio_used 
from UNITIZE_ALLOC_CGC_INSERTS_TEMP;

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] = ": ERROR: insert into UNITIZE_ALLOC_CGC_INSERTS_ARC: " + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if		
num_nrows = sqlca.sqlnrows

//of_allocation_refresh_staged_data() table is called for each priority in regular Auto101

return 1
end function

public subroutine of_setinstancevariables (boolean ab_called_from_w_wo_close, boolean ab_called_from_w_wo_control, longlong a_late_charge_mos, string a_wos_to_run, longlong a_wo_close_late_month_number);/************************************************************************************************************************************************************
**
**	of_setInstanceVariables()
**	
**	This function is called by w_wo_close or nvo_wo_control in ssp_wo_auto_unitization to set instance variables that were previously looked up in various
** functions by referencing instance variables on w_wo_close or w_wo_control.  Added this function to allow for syncing of this object between the 
**	main repository and interfaces repository.
**	
**	Parameters	:	boolean  : (ab_called_from_w_wo_close): TRUE: Called from w_wo_close; FALSE: Called from w/nvo_wo_control
**						boolean  : (ab_called_from_w_wo_control): TRUE: Called from w/nvo_wo_control; FALSE: Called from w_wo_close
**						longlong	: (a_late_charge_months) Corresponds to i_late_charge_mos on either w_wo_close or w/nvo_wo_control
**						string	: (a_wos_to_run) Corresponds to i_wos_to_run on w/nvo_wo_control
**						longlong	: (a_wo_close_late_month_number) Corresponds to i_late_month_number on w_wo_close
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
ib_called_from_w_wo_close   = ab_called_from_w_wo_close
ib_called_from_w_wo_control = ab_called_from_w_wo_control


i_late_charge_mos 			  = a_late_charge_mos
i_wos_to_run					  = a_wos_to_run
i_wo_close_late_month_number = a_wo_close_late_month_number

end subroutine

public function integer of_replacement_amt_as_pct ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//this function sets the i_has_replace_pct_estimates.
//i_has_replace_pct_estimates is useful for work orders where REPLACEMENT_PERCENTAGE = 1 on WORK_ORDER_TYPE, but there are no addition estimates with REPLACEMENT_AMOUNT <> 0
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

longlong check_count 

//if there are any work orders using the REPLACEMENT_AMOUNT as a Percentage, check if there are any estimates with REPLACEMENT_AMOUNT <> 0.
if i_has_replace_pct_wos then
	check_count = 0
	select count(1) into :check_count
	from UNITIZE_WO_LIST_TEMP a
	where a.company_id = :i_company_id
	and a.replacement_percentage = 1 
	and a.replace_amt_as_unit = 1  
	and a.est_unit_item_option = 0 
	and a.unit_item_source = 'ESTIMATES'
	and a.error_msg is null
	and rownum = 1;
					
	if check_count > 0 then
		i_has_replace_pct_estimates = true
	else
		i_has_replace_pct_estimates = false
	end if 
else
	i_has_replace_pct_estimates = false
end if 

return 1
end function

public function integer of_unitization_net_zero_rwip ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//as of v2015.2, do NOT create new Unit Item(s) for work orders that have net$0 charges. See Maint 44954.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   no unit items(2 of 2)..." 
of_log() 

//for RWIP charges, fake unitize the rows that were not Direct Assigned for net $0 because of missing Unit Items.
//no need to worry about the non_unitized_status on CWIP_CHARGE since RWIP charges cannot close to 106.
update cwip_charge a
set a.gl_account_id = (select d.retirement_gl_account from work_order_account d
							where a.work_order_id = d.work_order_id
							),
a.closed_month_number = :i_month_number,
a.unit_closed_month_number = :i_month_number 
where a.expenditure_type_id = 2
and a.charge_type_id <> :i_ocr_charge_type_id 
and a.unit_closed_month_number is null 
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
				where a.work_order_id = x.work_order_id
				and x.CREATE_NET_ZERO_RWIP = 1
				and x.HAS_NET_ZERO_RWIP = 1
				)
and exists (select 1 
			from work_order_charge_group b, charge_group_control c, UNITIZE_WO_LIST_TEMP z
			where z.work_order_id = c.work_order_id
			and z.CREATE_NET_ZERO_RWIP = 1
			and z.HAS_NET_ZERO_RWIP = 1
			and a.charge_id = b.charge_id
			and b.work_order_id = c.work_order_id
			and b.charge_group_id = c.charge_group_id
			and a.charge_type_id = c.charge_type_id 
			and c.expenditure_type_id = 2
			and c.charge_type_id <> :i_ocr_charge_type_id
			and c.batch_unit_item_id is null
			and c.unit_item_id is null
			and c.net_zero = c.charge_type_id
			and c.net_zero is not null 
			);
 
if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update cwip_charge for rwip net $0: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1
end if	 
num_nrows = sqlca.sqlnrows 

//update the CGC columns to exclude from unitization
update charge_group_control a
set a.batch_unit_item_id = -13, a.pend_transaction = 1, a.alloc_id = null
where a.expenditure_type_id = 2
and a.charge_type_id <> :i_ocr_charge_type_id
and a.batch_unit_item_id is null 
and a.unit_item_id is null
and a.net_zero = a.charge_type_id
and a.net_zero is not null
and exists (select 1 from UNITIZE_WO_LIST_TEMP x
				where a.work_order_id = x.work_order_id
				and x.CREATE_NET_ZERO_RWIP = 1
				and x.HAS_NET_ZERO_RWIP = 1
				);

if sqlca.SQLCode < 0 then 
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update cgc -13 for rwip net $0: ' + sqlca.SQLErrText 
	of_log() 
	rollback;		 
	return -1 
end if 
num_nrows = sqlca.sqlnrows 

//if all the rwip charges have been fake unitized, then there might not be any more eligible charges that need to go through the rest of unitization
update UNITIZE_WO_LIST_TEMP a
set a.HAS_ELIG_RWIP = null
where a.company_id = :i_company_id
and a.CREATE_NET_ZERO_RWIP = 1
and a.HAS_NET_ZERO_RWIP = 1
and a.HAS_ELIG_RWIP = 1
and not exists (select 1 from CHARGE_GROUP_CONTROL b
				where a.work_order_id = b.work_order_id
				and b.expenditure_type_id = 2 
				and b.charge_type_id <> :i_ocr_charge_type_id
				and b.alloc_id is not null 
				and b.batch_unit_item_id is null
				);
			
if sqlca.SQLCode < 0 then  
	i_msg[] = i_null_arrary[]
	i_msg[1] =  ': ERROR: Unable to update UNITIZE_WO_LIST_TEMP.HAS_ELIG_RWIP = null for net $0 fake unitization: ' + sqlca.SQLErrText 
	of_log()  
	rollback;		 
	return -1  
end if		
num_nrows = sqlca.sqlnrows 


return 1
end function

public function integer of_unitization_net_zero_adds ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//as of v2015.2, do NOT create new Unit Item(s) for work orders that have net$0 charges. See Maint 44954.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong num_nrows, check_distinct_count, adds_not_assigned, rwip_not_assigned, ocr_charge_type_id, min_gl_acct_id, max_gl_acct_id, wo_id, i, num_wo_list
string rtn_string, wo_list_sqls
boolean fake_unitization_done
uo_ds_top ds_wo_list_net0_adds
ds_wo_list_net0_adds = create uo_ds_top

i_msg[] = i_null_arrary[] 
i_msg[1] =  ":   no unit items(1 of 2)..." 
of_log() 
 
//for Addition charge groups require more effort.  Find list of work orders missing Unit Items to see if we can do fake unitization.
//Loop over each work order
wo_list_sqls = "select work_order_id from UNITIZE_WO_LIST_TEMP where CREATE_NET_ZERO_ADD = 1 and HAS_NET_ZERO_ADDS = 1 "
rtn_string = f_create_dynamic_ds(ds_wo_list_net0_adds, 'grid', wo_list_sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	i_msg[] = i_null_arrary[] 
	i_msg[1] =  ": ERROR: unable to create ds_wo_list_net0_adds for priority allocation: " + rtn_string
	of_log() 
	rollback;		  
	return -1 
end if 

num_wo_list = ds_wo_list_net0_adds.RowCount()

for i = 1 to num_wo_list
	
	wo_id = ds_wo_list_net0_adds.getitemnumber(i, 1)  
	fake_unitization_done = false 
	
	//verify the non_unitized_status on CWIP_CHARGE is the same.  if different, the charge groups do NOT qualify for net $0 for fake unitization and need to be untagged.
	check_distinct_count = 0
	select count(distinct nvl(a.non_unitized_status,0)), min(a.gl_account_id), max(a.gl_account_id) 
	into :check_distinct_count, :min_gl_acct_id, :max_gl_acct_id
	from cwip_charge a
	where a.work_order_id = :wo_id 
	and a.expenditure_type_id = 1
	and a.unit_closed_month_number is null 
	and exists (select 1 
				from work_order_charge_group b, charge_group_control c
				where a.charge_id = b.charge_id
				and b.work_order_id = c.work_order_id
				and b.charge_group_id = c.charge_group_id
				and a.charge_type_id = c.charge_type_id
				and c.work_order_id = :wo_id 
				and c.expenditure_type_id = 1
				and nvl(c.pend_transaction, 0) = 0
				and c.batch_unit_item_id is null
				and c.unit_item_id is null
				and c.net_zero = c.charge_type_id
				and c.net_zero is not null
				);
	
	if check_distinct_count > 1 then
		//need to untag the additions that net $0 for having different non_unitized_status.
		update charge_group_control a
		set a.net_zero = null 
		where a.work_order_id = :wo_id 
		and a.batch_unit_item_id is null
		and nvl(a.pend_transaction, 0) = 0
		and a.unit_item_id is null
		and a.expenditure_type_id = 1 
		and a.net_zero = a.charge_type_id
		and a.net_zero is not null
		and a.charge_type_id in (select c.charge_type_id
										from charge_group_control d, work_order_charge_group b, cwip_charge c
										where d.work_order_id = b.work_order_id
										and d.charge_group_id = b.charge_group_id
										and d.work_order_id = :wo_id
										and c.work_order_id = :wo_id
										and d.batch_unit_item_id is null
										and nvl(d.pend_transaction, 0) = 0
										and d.unit_item_id is null
										and d.net_zero = d.charge_type_id
										and d.net_zero is not null
										and b.charge_id = c.charge_id
										and d.expenditure_type_id = 1
										and c.expenditure_type_id = 1
										and d.charge_type_id = c.charge_type_id
										and c.unit_closed_month_number is null
										group by c.charge_type_id
										having count(distinct nvl(c.non_unitized_status,0)) > 1);
		
		if sqlca.SQLCode < 0 then  
			i_msg[] = i_null_arrary[]
			i_msg[1] =  ': ERROR: Unable to untag for net $0 for additions: ' + sqlca.SQLErrText 
			of_log() 
			rollback;		 
			destroy ds_wo_list_net0_adds
			return -1  
		end if		
		num_nrows = sqlca.sqlnrows 
	end if 
	
	//the non_unitized_status on the grouped charges are the same. the charges qualify for net $0.
	//1. update CWIP_CHARGE for gl_account_id and closed months
	//2. update CGC for fake unitization to exclude them from Unitization process. 
	
	if min_gl_acct_id < 3 then 
		//tax gl accounts 
		update cwip_charge a
		set a.gl_account_id = 3, 
		a.unit_closed_month_number = :i_month_number, 
		a.closed_month_number = decode(a.closed_month_number, null, :i_month_number, a.closed_month_number)
		where a.work_order_id = :wo_id
		and a.gl_account_id in (1,2)
		and a.expenditure_type_id = 1
		and a.unit_closed_month_number is null 
		and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id = 5)
		and exists (select 1 
					from work_order_charge_group b, charge_group_control c
					where a.charge_id = b.charge_id
					and b.work_order_id = c.work_order_id
					and b.charge_group_id = c.charge_group_id
					and a.charge_type_id = c.charge_type_id
					and c.work_order_id = :wo_id
					and c.charge_type_id in (select charge_type_id from charge_type where processing_type_id = 5)
					and c.expenditure_type_id = 1
					and nvl(c.pend_transaction, 0) = 0
					and c.batch_unit_item_id is null
					and c.unit_item_id is null
					and c.net_zero = c.charge_type_id
					and c.net_zero is not null
					);
				
		if sqlca.SQLCode < 0 then  
			i_msg[] = i_null_arrary[]
			i_msg[1] =  ': ERROR: Unable to update cwip_charge for tax adds net $0: ' + sqlca.SQLErrText 
			of_log() 
			destroy ds_wo_list_net0_adds
			rollback;		 
			return -1  
		end if		
		num_nrows = sqlca.sqlnrows 
		
		fake_unitization_done = true

	end if  //tax gl accounts 
	
	if max_gl_acct_id > 3 then 
		//regular gl accounts 
		update cwip_charge a
		set a.gl_account_id = (select d.unitized_gl_account from work_order_account d
									where d.work_order_id = a.work_order_id
									),
		a.unit_closed_month_number = :i_month_number, 
		a.closed_month_number = decode(a.closed_month_number, null, :i_month_number, a.closed_month_number)
		where a.work_order_id = :wo_id
		and a.gl_account_id > 3
		and a.expenditure_type_id = 1
		and a.unit_closed_month_number is null 
		and a.charge_type_id in (select charge_type_id from charge_type where processing_type_id <> 5)
		and exists (select 1 
					from work_order_charge_group b, charge_group_control c
					where a.charge_id = b.charge_id
					and b.work_order_id = c.work_order_id
					and b.charge_group_id = c.charge_group_id
					and a.charge_type_id = c.charge_type_id
					and c.work_order_id = :wo_id
					and c.charge_type_id in (select charge_type_id from charge_type where processing_type_id <> 5)
					and c.expenditure_type_id = 1
					and nvl(c.pend_transaction, 0) = 0
					and c.batch_unit_item_id is null
					and c.unit_item_id is null
					and c.net_zero = c.charge_type_id
					and c.net_zero is not null
					);
				
		if sqlca.SQLCode < 0 then   
			i_msg[] = i_null_arrary[]
			i_msg[1] =  ': ERROR: Unable to update cwip_charge for book adds net $0: ' + sqlca.SQLErrText 
			of_log() 
			destroy ds_wo_list_net0_adds
			rollback;		 
			return -1 
		end if		
		num_nrows = sqlca.sqlnrows    	 
		
		fake_unitization_done = true

	end if  //regular gl accounts  
	
	if fake_unitization_done then
		//update the CGC columns to exclude from unitization
		update charge_group_control a
		set a.batch_unit_item_id = -13, a.pend_transaction = 1, a.alloc_id = null
		where a.work_order_id = :wo_id
		and a.expenditure_type_id = 1
		and nvl(a.pend_transaction, 0) = 0
		and a.batch_unit_item_id is null 
		and a.unit_item_id is null
		and a.net_zero = a.charge_type_id
		and a.net_zero is not null;
				
		if sqlca.SQLCode < 0 then  
			i_msg[] = i_null_arrary[]
			i_msg[1] =  ': ERROR: Unable to update cgc -13 for adds net $0: ' + sqlca.SQLErrText 
			of_log() 
			destroy ds_wo_list_net0_adds
			rollback;		 
			return -1  
		end if		
		num_nrows = sqlca.sqlnrows 
		
		//if all the addition charges have been fake unitized, then there might not be any more eligible charges that need to go through the rest of unitization
		update UNITIZE_WO_LIST_TEMP a
		set a.HAS_ELIG_ADDS = null
		where a.work_order_id = :wo_id
		and a.CREATE_NET_ZERO_ADD = 1
		and a.HAS_NET_ZERO_ADDS = 1
		and a.HAS_ELIG_ADDS = 1
		and not exists (select 1 from CHARGE_GROUP_CONTROL b
						where a.work_order_id = b.work_order_id
						and b.work_order_id = :wo_id
						and b.expenditure_type_id = 1
						and b.alloc_id is not null 
						and b.batch_unit_item_id is null
						);
					
		if sqlca.SQLCode < 0 then  
			i_msg[] = i_null_arrary[]
			i_msg[1] =  ': ERROR: Unable to update UNITIZE_WO_LIST_TEMP.HAS_ELIG_ADDS = null for net $0 fake unitization: ' + sqlca.SQLErrText 
			of_log() 
			destroy ds_wo_list_net0_adds
			rollback;		 
			return -1  
		end if		
		num_nrows = sqlca.sqlnrows 
	end if 
	
next  

destroy ds_wo_list_net0_adds

return 1
end function

on uo_wo_unitization.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wo_unitization.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

