HA$PBExportHeader$uo_job_task_interface.sru
$PBExportComments$v10.3 maint: 4015 - task changes from SoCo
forward
global type uo_job_task_interface from nonvisualobject
end type
end forward

global type uo_job_task_interface from nonvisualobject
end type
global uo_job_task_interface uo_job_task_interface

type variables
boolean i_msg = true
boolean i_status_box =true
boolean i_write_success = true
longlong i_process_id
boolean i_visual = false
boolean no_updates = false
end variables

forward prototypes
public subroutine wf_ddl ()
public subroutine uf_msgs (string a_msg)
public function longlong uf_check_sql ()
public function longlong uf_create_cc (longlong a_company)
public function longlong uf_update_monthly_ests (longlong a_company)
public function longlong uf_load_monthly_ests (longlong a_company)
public function integer uf_create_job_task (longlong a_company, string a_batch)
public function longlong uf_update_job_task (longlong a_company, string a_batch)
public function integer uf_create_job_task (longlong a_company)
public function integer uf_create_job_task (string a_batch)
end prototypes

public subroutine wf_ddl ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   wf_ddl()
//
//   Purpose  :   Displays SQL text for both job_task_interface_staging, and job_task_interface_staging_arc.
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------     				 --------     					-----------   		--------------------------------------------- 
//  07-23-2008     			PowerPlan           			Version 1.0   			Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

//CREATE TABLE PWRPLANT."JOB_TASK_INTERFACE_STAGING" ("JOB_TASK_ID" VARCHAR2(35) NOT NULL, "WORK_ORDER_ID" NUMBER(22,0) NOT NULL, 
//"WORK_ORDER_NUMBER" VARCHAR2(35), "TIME_STAMP" DATE, "USER_ID" VARCHAR2(18), "DESCRIPTION" VARCHAR2(35) NOT NULL, "LONG_DESCRIPTION" VARCHAR2(254), 
//"EST_START_DATE" DATE, "EST_COMPLETE_DATE" DATE, "COMPLETION_DATE" DATE, "STATUS_CODE_ID" NUMBER(22,0), 
//"PERCENT_COMPLETE" NUMBER(22,8), "PARENT_JOB_TASK" VARCHAR2(35), "CHARGEABLE" NUMBER(22,0), "FORECAST" NUMBER(22,0), 
//"ESTIMATE" NUMBER(22,0), "LEVEL_NUMBER" NUMBER(22,0), "WORK_EFFORT" NUMBER(22,2), "FIELD1" VARCHAR2(35), 
//"FIELD2" VARCHAR2(35), "FIELD3" VARCHAR2(35), "FIELD4" VARCHAR2(35), "FIELD5" VARCHAR2(35), "PRIORITY_CODE_ID" NUMBER(22,0), 
//"EXTERNAL_JOB_TASK" VARCHAR2(35), "JOB_TASK_STATUS_ID" NUMBER(22,0), "SYSTEM_ID" NUMBER(22,0), "GROUP_ID" NUMBER(22,0), 
//"INITIATOR" VARCHAR2(18), "TASK_OWNER" VARCHAR2(18), "BUDGET_ANALYST" VARCHAR2(18), "PROJECT_MANAGER" VARCHAR2(18), 
//"ENGINEER" VARCHAR2(18), "OTHER_CONTACT" VARCHAR2(18), "NOTES" VARCHAR2(4000), "CLASS_CODE1" VARCHAR2(254), 
//"CLASS_VALUE1" VARCHAR2(254), "CLASS_CODE2" VARCHAR2(254), "CLASS_VALUE2" VARCHAR2(254),
//"CLASS_CODE3" VARCHAR2(254), "CLASS_VALUE3" VARCHAR2(254),"CLASS_CODE4" VARCHAR2(254), "CLASS_VALUE4" VARCHAR2(254),
//"CLASS_CODE5" VARCHAR2(254), "CLASS_VALUE5" VARCHAR2(254),"CLASS_CODE6" VARCHAR2(254), "CLASS_VALUE6" VARCHAR2(254),
//"CLASS_CODE7" VARCHAR2(254), "CLASS_VALUE7" VARCHAR2(254),"CLASS_CODE8" VARCHAR2(254), "CLASS_VALUE8" VARCHAR2(254),
//"CLASS_CODE9" VARCHAR2(254), "CLASS_VALUE9" VARCHAR2(254),"CLASS_CODE10" VARCHAR2(254), "CLASS_VALUE10" VARCHAR2(254),
//"CLASS_CODE11" VARCHAR2(254), "CLASS_VALUE11" VARCHAR2(254),"CLASS_CODE12" VARCHAR2(254), "CLASS_VALUE12" VARCHAR2(254),
//"CLASS_CODE13" VARCHAR2(254), "CLASS_VALUE13" VARCHAR2(254),"CLASS_CODE14" VARCHAR2(254), "CLASS_VALUE14" VARCHAR2(254),
//"CLASS_CODE15" VARCHAR2(254), "CLASS_VALUE15" VARCHAR2(254),"CLASS_CODE16" VARCHAR2(254), "CLASS_VALUE16" VARCHAR2(254),
//"CLASS_CODE17" VARCHAR2(254), "CLASS_VALUE17" VARCHAR2(254),"CLASS_CODE18" VARCHAR2(254), "CLASS_VALUE18" VARCHAR2(254),
//"CLASS_CODE19" VARCHAR2(254), "CLASS_VALUE19" VARCHAR2(254),"CLASS_CODE20" VARCHAR2(254), "CLASS_VALUE20" VARCHAR2(254),
//"SETUP_TEMPLATE" varchar2(35), "SETUP_TEMPLATE_ID" number(22,0), "STATUS" number(22,0), "ACTION" varchar2(1),
//"ROW_ID" number(22,0), "PRIORITY_CODE_DESCR" varchar2(35), "SYSTEM_DESCR" varchar2(35), "GROUP_DESCR" varchar2(35),
//"JOB_TASK_STATUS_DESCR" varchar2(35), "COMPANY_ID" NUMBER(22, 0), "FUNDING_TASK_INDICATOR" NUMBER(22, 0)) ;
//
//create or replace public synonym job_task_interface_staging for pwrplant.job_task_interface_staging;
//GRANT ALL PRIVILEGES ON PWRPLANT.job_task_interface_staging TO pwrplant_role_dev;

//CREATE TABLE PWRPLANT."JOB_TASK_INTERFACE_STAGING_ARC" ("JOB_TASK_ID" VARCHAR2(35) NOT NULL, "WORK_ORDER_ID" NUMBER(22,0) NOT NULL, 
//"WORK_ORDER_NUMBER" VARCHAR2(35), "TIME_STAMP" DATE, "USER_ID" VARCHAR2(18), "DESCRIPTION" VARCHAR2(35) NOT NULL, "LONG_DESCRIPTION" VARCHAR2(254), 
//"EST_START_DATE" DATE, "EST_COMPLETE_DATE" DATE, "COMPLETION_DATE" DATE, "STATUS_CODE_ID" NUMBER(22,0), 
//"PERCENT_COMPLETE" NUMBER(22,8), "PARENT_JOB_TASK" VARCHAR2(35), "CHARGEABLE" NUMBER(22,0), "FORECAST" NUMBER(22,0), 
//"ESTIMATE" NUMBER(22,0), "LEVEL_NUMBER" NUMBER(22,0), "WORK_EFFORT" NUMBER(22,2), "FIELD1" VARCHAR2(35), 
//"FIELD2" VARCHAR2(35), "FIELD3" VARCHAR2(35), "FIELD4" VARCHAR2(35), "FIELD5" VARCHAR2(35), "PRIORITY_CODE_ID" NUMBER(22,0), 
//"EXTERNAL_JOB_TASK" VARCHAR2(35), "JOB_TASK_STATUS_ID" NUMBER(22,0), "SYSTEM_ID" NUMBER(22,0), "GROUP_ID" NUMBER(22,0), 
//"INITIATOR" VARCHAR2(18), "TASK_OWNER" VARCHAR2(18), "BUDGET_ANALYST" VARCHAR2(18), "PROJECT_MANAGER" VARCHAR2(18), 
//"ENGINEER" VARCHAR2(18), "OTHER_CONTACT" VARCHAR2(18), "NOTES" VARCHAR2(4000), "CLASS_CODE1" VARCHAR2(254), 
//"CLASS_VALUE1" VARCHAR2(254), "CLASS_CODE2" VARCHAR2(254), "CLASS_VALUE2" VARCHAR2(254),
//"CLASS_CODE3" VARCHAR2(254), "CLASS_VALUE3" VARCHAR2(254),"CLASS_CODE4" VARCHAR2(254), "CLASS_VALUE4" VARCHAR2(254),
//"CLASS_CODE5" VARCHAR2(254), "CLASS_VALUE5" VARCHAR2(254),"CLASS_CODE6" VARCHAR2(254), "CLASS_VALUE6" VARCHAR2(254),
//"CLASS_CODE7" VARCHAR2(254), "CLASS_VALUE7" VARCHAR2(254),"CLASS_CODE8" VARCHAR2(254), "CLASS_VALUE8" VARCHAR2(254),
//"CLASS_CODE9" VARCHAR2(254), "CLASS_VALUE9" VARCHAR2(254),"CLASS_CODE10" VARCHAR2(254), "CLASS_VALUE10" VARCHAR2(254),
//"CLASS_CODE11" VARCHAR2(254), "CLASS_VALUE11" VARCHAR2(254),"CLASS_CODE12" VARCHAR2(254), "CLASS_VALUE12" VARCHAR2(254),
//"CLASS_CODE13" VARCHAR2(254), "CLASS_VALUE13" VARCHAR2(254),"CLASS_CODE14" VARCHAR2(254), "CLASS_VALUE14" VARCHAR2(254),
//"CLASS_CODE15" VARCHAR2(254), "CLASS_VALUE15" VARCHAR2(254),"CLASS_CODE16" VARCHAR2(254), "CLASS_VALUE16" VARCHAR2(254),
//"CLASS_CODE17" VARCHAR2(254), "CLASS_VALUE17" VARCHAR2(254),"CLASS_CODE18" VARCHAR2(254), "CLASS_VALUE18" VARCHAR2(254),
//"CLASS_CODE19" VARCHAR2(254), "CLASS_VALUE19" VARCHAR2(254),"CLASS_CODE20" VARCHAR2(254), "CLASS_VALUE20" VARCHAR2(254),
//"SETUP_TEMPLATE" varchar2(35), "SETUP_TEMPLATE_ID" number(22,0), "STATUS" number(22,0), "ACTION" varchar2(1),
//"ROW_ID" number(22,0), "PRIORITY_CODE_DESCR" varchar2(35), "SYSTEM_DESCR" varchar2(35), "GROUP_DESCR" varchar2(35),
//"JOB_TASK_STATUS_DESCR" varchar2(35), "COMPANY_ID" NUMBER(22, 0), "FUNDING_TASK_INDICATOR" NUMBER(22, 0), "RUN_DATE" DATE) ;
//
//create or replace public synonym job_task_interface_staging_arc for pwrplant.job_task_interface_staging;
//GRANT ALL PRIVILEGES ON PWRPLANT.job_task_interface_staging_arc TO pwrplant_role_dev;
end subroutine

public subroutine uf_msgs (string a_msg);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :  uo_job_task_interface.uf_msgs()
//
//   Purpose  :   Send message to display box for end user
//                    
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      				--------    					 -----------   			--------------------------------------------- 
//  07-23-2008      			PowerPlan          		 Version 1.0   				Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

if i_msg then
	f_pp_msgs(string(now()) + ": " + a_msg)
end if

if i_status_box and i_visual then
	f_status_box('',a_msg)
end if

end subroutine

public function longlong uf_check_sql ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_job_task_interface.uf_check_sql
//
//   Purpose  :   Checks to make sure the current SQL statment is valid.
//                     If the statement is valid, then the fuction displays the Rows Affected. otherwise the function  returns -1.
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
if sqlca.sqlcode < 0 then 
	uf_msgs("SQLERROR: "+ sqlca.sqlerrtext)
	return -1
end if

if i_write_success then
	uf_msgs("  Rows Affected: " + string(sqlca.sqlnrows))
end if

return 1

end function

public function longlong uf_create_cc (longlong a_company);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_job_task_interface.uf_create_cc
//
//   Purpose  :   Creates Class Codes from pre-staged data in the table wo_interface_staging.
//					Data is added to both CLASS_CODE_ID and CLASS_CODE.
//                    
//                    This functions is automatically called through the function uf_create_job_task.
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

longlong max_id

select max(class_code_id)
into :max_id
from class_code;

uf_msgs( " Insert into Class Code " )
insert into class_code (CLASS_CODE_ID,
TIME_STAMP,
USER_ID,
DESCRIPTION,
CWIP_INDICATOR,
TAX_INDICATOR,
CPR_INDICATOR,
BUDGET_INDICATOR,
LONG_DESCRIPTION,
CLASS_CODE_TYPE,
JOB_TASK_INDICATOR)
select :max_id + rownum, sysdate, user, substr(description,1,35), 0, 0, 0, 0, 
description, 'standard', 1
from 
(select distinct trim(class_code1) description
from job_task_interface_staging
where  (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code2)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code3)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code4)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code5)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code6)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code7)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code8)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code9)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code10)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code11)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code12)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code13)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code14)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code15)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code16)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code17)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code18)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code19)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct trim(class_code20)
from job_task_interface_staging
where (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
minus
select trim(description)
from 	 class_code
minus
select null
from dual);
if sqlca.sqlcode < 0 then 
	uf_msgs("SQLERROR: " +    sqlca.sqlerrtext)
	
	return -1
end if
uf_msgs("Rows Affected: " + string(sqlca.sqlnrows))

uf_msgs( " Insert into Class Code Values " )
insert into class_code_values (class_code_id, value)
(select distinct cc.class_code_id, w.class_value1
from class_code cc, job_task_interface_staging w
where trim(w.class_code1) = trim(cc.description)
and w.class_value1 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value2
from class_code cc, job_task_interface_staging w
where trim(w.class_code2) = trim(cc.description)
and w.class_value2 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value3
from class_code cc, job_task_interface_staging w
where trim(w.class_code3) = trim(cc.description)
and w.class_value3 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value4
from class_code cc, job_task_interface_staging w
where trim(w.class_code4) = trim(cc.description)
and w.class_value4 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value5
from class_code cc, job_task_interface_staging w
where trim(w.class_code5) = trim(cc.description)
and w.class_value5 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value6
from class_code cc, job_task_interface_staging w
where trim(w.class_code6) = trim(cc.description)
and w.class_value6 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value7
from class_code cc, job_task_interface_staging w
where trim(w.class_code7) = trim(cc.description)
and w.class_value7 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value8
from class_code cc, job_task_interface_staging w
where trim(w.class_code8) = trim(cc.description)
and w.class_value8 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value9
from class_code cc, job_task_interface_staging w
where trim(w.class_code9) = trim(cc.description)
and w.class_value9 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value10
from class_code cc, job_task_interface_staging w
where trim(w.class_code10) = trim(cc.description)
and w.class_value10 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value11
from class_code cc, job_task_interface_staging w
where trim(w.class_code11) = trim(cc.description)
and w.class_value11 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value12
from class_code cc, job_task_interface_staging w
where trim(w.class_code12) = trim(cc.description)
and w.class_value12 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value13
from class_code cc, job_task_interface_staging w
where trim(w.class_code13) = trim(cc.description)
and w.class_value13 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value14
from class_code cc, job_task_interface_staging w
where trim(w.class_code14) = trim(cc.description)
and w.class_value14 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value15
from class_code cc, job_task_interface_staging w
where trim(w.class_code15) = trim(cc.description)
and w.class_value15 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value16
from class_code cc, job_task_interface_staging w
where trim(w.class_code16) = trim(cc.description)
and w.class_value16 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value17
from class_code cc, job_task_interface_staging w
where trim(w.class_code17) = trim(cc.description)
and w.class_value17 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value18
from class_code cc, job_task_interface_staging w
where trim(w.class_code18) = trim(cc.description)
and w.class_value18 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value19
from class_code cc, job_task_interface_staging w
where trim(w.class_code19) = trim(cc.description)
and w.class_value19 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
union
select distinct cc.class_code_id, w.class_value20
from class_code cc, job_task_interface_staging w
where trim(w.class_code20) = trim(cc.description)
and w.class_value20 is not null
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and status = 1
minus
select class_code_id, value
from class_code_values);
if sqlca.sqlcode < 0 then 
	uf_msgs("SQLERROR: " +    sqlca.sqlerrtext)
	
	return -1
end if
uf_msgs("Rows Affected: " + string(sqlca.sqlnrows))

return 1
end function

public function longlong uf_update_monthly_ests (longlong a_company);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_job_task_interface.uf_update_monthly_ests
//
//   Purpose  :   This function will move job task estimates around based on date changes
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//Issues 
//   1. What if the job task dates exceede the work order/funding project dates
return 1
end function

public function longlong uf_load_monthly_ests (longlong a_company);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_job_task_interface.uf_load_monthly_ests
//
//   Purpose  :   This function will load estimate dollars for a particular job task
//                    
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//Issues 
//   1. What if the job task dates exceede the work order/funding project dates

return 1
end function

public function integer uf_create_job_task (longlong a_company, string a_batch);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_job_task_interface.uf_create_job_task
//
//   Purpose  :   Creates Class Codes from pre-staged data in the table job_task_interface_staging.
//					- either work order id or company id must be filled in
//                      - the status code field should be left null but can not be set to 2
//                    
//                    All rows that are successfully processed are updated to status of 2.
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      				--------     					-----------   		--------------------------------------------- 
//  07-23-2008      			PowerPlan           			Version 1.0   		Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
longlong rtn, counts, max_id,  cc_count, i, update_counts, num_batches, ii
string sqls_cc

if isnull(a_batch) then a_batch = 'null'
if isnull(a_company) then a_company = -1

uf_msgs("------------------------------- ")
uf_msgs("------------------------------- ")
uf_msgs("Begin Job Task Loading - Company Id = "+string(a_company))


//--------------------------------------------------------------------------------
//
// PROCESSING STATUS
//
//--------------------------------------------------------------------------------
uf_msgs("Setting Status")
// PP-41914
update job_task_interface_staging a
set status = 1,
	funding_task_indicator = nvl(a.funding_task_indicator, 0),
	error_message = null,
	work_order_number = upper(a.work_order_number)
where nvl(batch_id,'null') = :a_batch
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and nvl(status,1) <> 2
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// COMPANY
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Mapping Company")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on gl_company_no
update job_task_interface_staging a
set a.company_id = (
	select p.company_id
	from company p
	where p.gl_company_no = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Map on company description
update job_task_interface_staging a
set a.company_id = (
	select p.company_id
	from company p
	where p.description = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Map on company_id
update job_task_interface_staging a
set a.company_id = (
	select p.company_id
	from company p
	where to_char(p.company_id) = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Flag records where ext_company could not be mapped
update job_task_interface_staging a
set a.error_message = a.error_message || 'Task - cannot derive company_id; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

////
////	No external value was provided
////

// Flag records missing company_id as errors
update job_task_interface_staging a
set a.error_message = a.error_message || 'Task - company_id is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// WORK ORDER NUMBER
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Work Order Number")

// Flag records missing work_order_number as errors
update job_task_interface_staging a
set a.error_message = a.error_message || 'Task - work_order_number is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and a.work_order_number is null
;
if uf_check_sql() = -1 then return -1
// Field is not null...this should never find anything


//--------------------------------------------------------------------------------
//
// JOB TASK ID
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Job Task Id")

// Flag records missing job_task_id as errors
update job_task_interface_staging a
set a.error_message = a.error_message || 'Task - job_task_id is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and a.job_task_id is null
;
if uf_check_sql() = -1 then return -1
// Field is not null...this should never find anything


//--------------------------------------------------------------------------------
//
// VALIDATION
//   - must have a valid COMPANY_ID and WORK_ORDER_NUMBER and JOB_TASK_ID
//
//--------------------------------------------------------------------------------
uf_msgs("Flagging Errors (missing Company, Work Order Number, or Job Task)")

// Flag records that already have errors as unable to continue processing (either invalid company_id or work_order_number)
update job_task_interface_staging a
set a.status = -11
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and a.error_message is not null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// DETERMINE THE NUMBER OF BATCHES
//   - Loop over batches of work orders
//
//--------------------------------------------------------------------------------
uf_msgs("Identifying Process Batches")

// Determine the number of loops needed to process all work orders in sequence
select max(batches)
into :num_batches
from (
	select work_order_number, funding_task_indicator, company_id, job_task_id, count(distinct nvl(seq_id,0)) batches
	from job_task_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	group by work_order_number, funding_task_indicator, company_id, job_task_id
	)
;
if uf_check_sql() = -1 then return -1

uf_msgs("Updating status to Pending")

// Update records that have not yet been processed to status = 3 (pending
update job_task_interface_staging a
set a.status = 3 // pending
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// LOOP OVER THE BATCHES
//
//--------------------------------------------------------------------------------
for ii = 1 to num_batches
	uf_msgs("***** Batch "+string(ii)+" of "+string(num_batches)+" *****")
	uf_msgs(" ")
	
	uf_msgs("Updating status to In-Process")
	update job_task_interface_staging a
	set a.status = 1 // in-process
	where a.status = 3
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and nvl(seq_id,0) in (
		select min(nvl(seq_id,0))
		from job_task_interface_staging b
		where b.status = 3
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and b.work_order_number = a.work_order_number
		and b.funding_task_indicator = a.funding_task_indicator
		and b.company_id = a.company_id
		and b.job_task_id = a.job_task_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// WORK ORDER ID
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Work Order Id")
	
	// Map on work_order_number
	update job_task_interface_staging a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.work_order_number = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_task_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on external_wo_number
	update job_task_interface_staging a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.external_wo_number = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_task_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update job_task_interface_staging a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.description = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_task_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on work_order_id
	update job_task_interface_staging a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where to_char(p.work_order_id) = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_task_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing work_order_id as errors
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - cannot derive work_order_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// DETERMINE ACTION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Setting Action")
	
	// Reset action code
	update job_task_interface_staging a
	set a.action = null
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	;
	if uf_check_sql() = -1 then return -1
	
	// Identify "update" records
	update job_task_interface_staging a 
	set a.action = 'U'
	where exists (
		select 1 from job_task b
		where b.work_order_id = a.work_order_id
		and b.job_task_id = a.job_task_id
		)
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	;
	if uf_check_sql() = -1 then return -1
		
	// Identify "insert" records
	update job_task_interface_staging a 
	set a.action = 'I'
	where nvl(action,'I') <> 'U'
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	;
	if uf_check_sql() = -1 then return -1
	
	
//	//--------------------------------------------------------------------------------
//	//
//	// SETTING OLD RECORDS AS PROCESSED BASED ON SEQUENCE (SEQ_ID)
//	//   If a newer record for the same job task already exists, mark the
//	//   old one as processed, and continue processing the latest one
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Checking Sequence")
//	update job_task_interface_staging a
//	set a.status = 2
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and nvl(a.seq_id,0) < (
//		select nvl(max(b.seq_id),0)
//		from job_task_interface_staging b
//		where b.status = 1
//		and nvl(b.batch_id,'null') = :a_batch
//		and (b.company_id = :a_company or :a_company = -1)
//		and b.work_order_id = a.work_order_id
//		and b.job_task_id = a.job_task_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
	
	// Flag duplicate records where the same job task for the same work order exists more than once
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - duplicate records exist for this job task with identical/blank seq_ids or there was a lower sequence id that had failed and therefore this record cannot be processed until the prior record is fixed.; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and (a.work_order_id, a.job_task_id) in (
		select c.work_order_id, c.job_task_id
		from job_task_interface_staging c
		where c.status = 1
		and nvl(c.batch_id,'null') = :a_batch
		and (c.company_id = :a_company or :a_company = -1)
		group by c.work_order_id, c.job_task_id
		having count(*) > 1
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	//--------------------------------------------------------------------------------
	//
	// COUNT RECORDS TO PROCESS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Checking for records to process")
	
	// Look for any records to process whether Insert or Update
	select count(*) into :counts
	from job_task_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	;
		
	if counts = 0 then
		uf_msgs("0 records to process")
		uf_msgs("-------------------------------")
		uf_msgs("-------------------------------")
		return 1
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// DESCRIPTION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Validating Job Task Description")
	
	// Map on description from job_task_list - ONLY FOR INSERTS
	update job_task_interface_staging a 
	set a.description = (
		select description
		from job_task_list b
		where b.job_task_id = a.job_task_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.description is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing description as errors - ONLY FOR INSERTS
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - description is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.description is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// PARENT JOB TASK
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Parent Job Task")
	
	// Flag records where parent_job_task does not exist in either the base job_task table or the staging table to be processed in this batch
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - parent_job_task does not exist for these job tasks; '
	where (work_order_id, parent_job_task) in (
		select work_order_id, parent_job_task
		from job_task_interface_staging
		where parent_job_task is not null
		and status = 1
		and nvl(batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		MINUS
		select work_order_id, job_task_id
		from job_task
		MINUS
		select work_order_id, job_task_id
		from job_task_interface_staging
		where status = 1
		and nvl(batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and action = 'I'
		)
	and status = 1
	and nvl(batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.parent_job_task is not null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// SETUP TEMPLATE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Setup Template")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external setup template
	update job_task_interface_staging a 
	set a.setup_template_id = (
		select max(setup_template_id)
		from job_task_setup_template b
		where b.external_setup_template = a.setup_template
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.setup_template_id is null						
	and a.setup_template is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on setup template description
	update job_task_interface_staging a 
	set a.setup_template_id = (
		select max(setup_template_id)
		from job_task_setup_template b
		where b.description = a.setup_template
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.setup_template_id is null						
	and a.setup_template is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where setup_template could not be mapped
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - cannot derive setup_template_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.setup_template_id is null
	and a.setup_template is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// TASK STATUS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Task Status")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_task_status
	update job_task_interface_staging a 
	set a.job_task_status_id = (
		select max(job_task_status_id)
		from job_task_status b
		where b.external_task_status = a.job_task_status_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.job_task_status_id is null
	and a.job_task_status_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on task status description
	update job_task_interface_staging a 
	set a.job_task_status_id = (
		select max(job_task_status_id)
		from job_task_status b
		where b.description = a.job_task_status_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.job_task_status_id is null
	and a.job_task_status_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where job_task_status_descr could not be mapped
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - cannot derive job_task_status_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.job_task_status_id is null
	and a.job_task_status_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// PRIORITY CODE
	//  - add them if they don't exist
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Priority Code")
	
	// Create New Priority Codes
	insert into job_task_priority_code (priority_code_id, description, notes, external_priority_code, active)
	select pwrplant1.nextval, substr(priority_code_descr, 1, 35), null, substr(priority_code_descr, 1, 35), 1
	from (
		select trim(priority_code_descr) priority_code_descr
		from job_task_interface_staging
		where status = 1
		and nvl(batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		MINUS
		select description
		from job_task_priority_code
		MINUS
		select external_priority_code
		from job_task_priority_code
		)
	where trim(priority_code_descr) is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external priority code
	update job_task_interface_staging a 
	set a.priority_code_id = (
		select max(priority_code_id)
		from job_task_priority_code b
		where b.external_priority_code = a.priority_code_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.priority_code_id is null
	and a.priority_code_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on priority code description
	update job_task_interface_staging a 
	set a.priority_code_id = (
		select max(priority_code_id)
		from job_task_priority_code b
		where b.description = a.priority_code_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.priority_code_id is null
	and a.priority_code_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where priority_code_descr could not be mapped
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - cannot derive priority_code_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.priority_code_id is null
	and a.priority_code_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// SYSTEM
	//  - add them if they don't exist
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping System")
	
	// Create New Systems
	insert into job_task_system (system_id, description, notes, external_system, active)
	select pwrplant1.nextval, substr(system_descr, 1, 35), null, substr(system_descr, 1, 35), 1
	from (
		select trim(system_descr) system_descr
		from job_task_interface_staging
		where status = 1
		and nvl(batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		MINUS
		select gg.description
		from job_task_system gg
		MINUS
		select ff.external_system
		from job_task_system ff
		)
	where trim(system_descr) is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_system
	update job_task_interface_staging a 
	set a.system_id = (
		select max(system_id)
		from job_task_system b
		where b.external_system = a.system_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.system_id is null
	and a.system_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on system description
	update job_task_interface_staging a 
	set a.system_id = (
		select max(system_id)
		from job_task_system b
		where b.description = a.system_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.system_id is null
	and a.system_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where system_descr could not be mapped
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - cannot derive system_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.system_id is null
	and a.system_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// GROUP
	//  - add them if they don't exist
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Group")
	
	// Create New Groups
	insert into job_task_group (group_id, description, notes, external_group_id, active) 
	select pwrplant1.nextval, substr(group_descr, 1, 35), null, substr(group_descr, 1, 35), 1
	from (
		select trim(group_descr) group_descr
		from job_task_interface_staging
		where status = 1
		and nvl(batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		MINUS
		select aa.description
		from job_task_group aa
		MINUS
		select bb.external_group_id
		from job_task_group bb
		)
	where trim(group_descr) is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_group_id
	update job_task_interface_staging a 
	set a.group_id = (
		select max(group_id)
		from job_task_group b
		where  b.external_group_id = a.group_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.group_id is null
	and a.group_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on group description
	update job_task_interface_staging a 
	set a.group_id = (
		select max(group_id)
		from job_task_group b
		where b.description = a.group_descr
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.group_id is null
	and a.group_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where group_descr could not be mapped
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - cannot derive group_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.group_id is null
	and a.group_descr is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// JOB_TASK_LIST MAPPINGS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Other Mappings from Job_Task_List")
	
	// Map other attributes on job_task_list - ONLY FOR INSERTS
	update job_task_interface_staging a 
	set (a.long_description, a.external_job_task, a.priority_code_id,
		a.job_task_status_id, a.system_id, a.group_id,
		a.initiator, a.task_owner, a.budget_analyst,
		a.project_manager, a.engineer, a.other_contact,
		a.notes, a.setup_template_id) = (
			select nvl(a.long_description, b.long_description), nvl(a.external_job_task, b.external_job_task), nvl(a.priority_code_id, b.priority_code_id),
				nvl(a.job_task_status_id, b.job_task_status_id), nvl(a.system_id, b.system_id), nvl(a.group_id, b.group_id),
				nvl(a.initiator, b.initiator), nvl(a.task_owner, b.task_owner), nvl(a.budget_analyst, b.budget_analyst),
				nvl(a.project_manager, b.project_manager), nvl(a.engineer, b.engineer), nvl(a.other_contact, b.other_contact),
				nvl(a.notes, b.notes), nvl(a.setup_template_id, b.setup_template_id)
			from job_task_list b
			where b.job_task_id = a.job_task_id
			)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	and a.job_task_id in (
		select job_task_id
		from job_task_list
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// INITIAL TASK STATUS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Initial Status")
	
	// Map on job_task_setup_template - ONLY FOR INSERTS
	update job_task_interface_staging a 
	set a.job_task_status_id = (
		select nvl(b.initial_status, a.job_task_status_id)
		from job_task_setup_template b
		where b.setup_template_id = a.setup_template_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	//and a.job_task_status_id is null --this is a new record, always use the initial_status from job_task_setup_template if it exists
	and a.setup_template_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// CHARGEABLE, ESTIMATE, FORECAST
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Cost Flags")
	
	// Map on job_task_status (always update status from template if not already filled in) - ONLY FOR INSERTS
	update job_task_interface_staging a 
	set (a.CHARGEABLE, a.ESTIMATE, a.FORECAST) = (
		select nvl(a.CHARGEABLE, b.CHARGEABLE), nvl(a.ESTIMATE, b.ESTIMATE), nvl(a.FORECAST, b.FORECAST)
		from job_task_status b
		where b.job_task_status_id = a.job_task_status_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	and job_task_status_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map from job_task_list (only on initial creation of job task if not already filled in) - ONLY FOR INSERTS
	update job_task_interface_staging a
	set (a.CHARGEABLE, a.ESTIMATE, a.FORECAST) = (
		select nvl(a.CHARGEABLE, b.CHARGEABLE), nvl(a.ESTIMATE, b.ESTIMATE), nvl(a.FORECAST, b.FORECAST)
		from job_task_list b
		where b.job_task_id = a.job_task_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	and a.job_task_id in (
		select job_task_id
		from job_task_list
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Default to 1 (only on initial creation of job task if not already filled in) - ONLY FOR INSERTS
	update job_task_interface_staging a 
	set a.CHARGEABLE = nvl(a.CHARGEABLE,1),
		a.ESTIMATE = nvl(a.ESTIMATE,1),
		a.FORECAST = nvl(a.FORECAST,1)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// STATUS CODE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Status Code")
	
	// Map on job_task_status_id - ONLY FOR INSERTS
	update job_task_interface_staging  a
	set a.status_code_id = (
		select nvl(a.status_code_id, b.job_task_status_code)
		from job_task_status b
		where b.job_task_status_id = a.job_task_status_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	and a.job_task_status_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Default status_code_id to 1 (Active) - ONLY FOR INSERTS
	update job_task_interface_staging a 
	set a.status_code_id = nvl(a.status_code_id, 1)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// LEVEL NUMBER
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Level Number")
	
	// Clear out the level numbers
	update job_task_interface_staging
	set level_number = null
	where status = 1
	and nvl(batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////Fill in the Level ID
	////
	
	// The program should always override the passed in level id.  The logic for level id should be the following
	// a.	If Parent job task is null level number = 1
	// b.	Otherwise the level number should be level number + 1 of the parent job task.
	// c.	Keep in mind this can be tricky if the both the parent job task and the child job task are both in the same batch
	//		(or the grandparent, parent, and child and so on $$HEX1$$2620$$ENDHEX$$)  This logic needs to work for N levels
	
	// Update the level number to 1 where parent_job_task is null
	update job_task_interface_staging a 
	set a.level_number = 1
	where status = 1
	and nvl(batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and parent_job_task is null
	;
	if uf_check_sql() = -1 then return -1
	
	//Then update the job task with parents already in the job_task table
	update job_task_interface_staging a 
	set a.level_number = (
		select b.level_number + 1
		from job_task b
		where b.job_task_id = a.parent_job_task
		and b.work_order_id = a.work_order_id
		)
	where status = 1
	and nvl(batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and (parent_job_task, work_order_id) in (
		select job_task_id, work_order_id
		from job_task
		)
	and level_number is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Loop through the table to find levels\
	i = 0
	do
		update job_task_interface_staging a 
		set a.level_number = (
			select max(b.level_number) + 1
			from job_task_interface_staging b
			where b.job_task_id = a.parent_job_task
			and b.work_order_id = a.work_order_id
			and b.status = 1
			and nvl(b.batch_id,'null') = :a_batch
			and (b.company_id = :a_company or :a_company = -1)
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.level_number is null
		;
		if uf_check_sql() = -1 then return -1
		
		// Determine if there are still null levels
		// PP-41846
		select count(*) into :counts
		from job_task_interface_staging
		where status = 1
		and nvl(batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and level_number is null
		and (work_order_id, parent_job_task) in (
			select work_order_id, job_task_id
			from job_task_interface_staging
			where status = 1
			and nvl(batch_id,'null') = :a_batch
			and (company_id = :a_company or :a_company = -1)
			and action = 'I')
		;
		
		i++
		// PP-41846
	loop while counts > 0 and i < 20
	
	// PP-41846
	// Flag records where level_number could not be derived
	update job_task_interface_staging a
	set a.error_message = a.error_message || 'Task - cannot derive level_number; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.level_number is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERTS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting New Job Tasks")
	
	// Check for any new funding projects to process
	select count(*) into :counts
	from job_task_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and action = 'I'
	;
	
	if isnull(counts) or counts = 0 then
		uf_msgs("No New Job Tasks to Process")
		goto updates
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT JOB_TASK
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Job Task")
	
	// Insert into job_task
	// PP-42417
	INSERT INTO JOB_TASK (
		BUDGET_ANALYST, CHARGEABLE, COMPLETION_DATE, DESCRIPTION,
		ENGINEER, ESTIMATE, EST_COMPLETE_DATE, EST_START_DATE,
		EXTERNAL_JOB_TASK, FIELD1, FIELD2, FIELD3,
		FIELD4, FIELD5, FORECAST, GROUP_ID,
		INITIATOR, JOB_TASK_ID, JOB_TASK_STATUS_ID, LEVEL_NUMBER,
		LONG_DESCRIPTION, NOTES, OTHER_CONTACT, PARENT_JOB_TASK,
		PERCENT_COMPLETE, PRIORITY_CODE_ID, PROJECT_MANAGER, SETUP_TEMPLATE_ID,
		STATUS_CODE_ID, SYSTEM_ID, TASK_OWNER, WORK_EFFORT,
		WORK_ORDER_ID, initiation_date, reason_for_work, background, future_projects, alternatives, financial_analysis
		)
	SELECT BUDGET_ANALYST, CHARGEABLE, COMPLETION_DATE, DESCRIPTION,
		ENGINEER, ESTIMATE, nvl(EST_COMPLETE_DATE, (select b.est_complete_date from work_order_control b where a.work_order_id = b.work_order_id)), 
		nvl(EST_START_DATE, (select b.est_start_date from work_order_control b where a.work_order_id = b.work_order_id)),
		EXTERNAL_JOB_TASK, FIELD1, FIELD2, FIELD3,
		FIELD4, FIELD5, FORECAST, GROUP_ID,
		INITIATOR, JOB_TASK_ID, JOB_TASK_STATUS_ID, LEVEL_NUMBER,
		LONG_DESCRIPTION, NOTES, OTHER_CONTACT, PARENT_JOB_TASK,
		PERCENT_COMPLETE, PRIORITY_CODE_ID, PROJECT_MANAGER, SETUP_TEMPLATE_ID,
		STATUS_CODE_ID, SYSTEM_ID, TASK_OWNER, WORK_EFFORT,
		WORK_ORDER_ID, sysdate, reason_for_work, background, future_projects, alternatives, financial_analysis
	FROM JOB_TASK_INTERFACE_STAGING a
	WHERE STATUS = 1
	and nvl(batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and action = 'I'
	and error_message is null
	;
	if uf_check_sql() = -1 then return -1
		
	
	//--------------------------------------------------------------------------------
	//
	// CLASS CODES
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Class Codes")
	
	// Create new class codes
	//rtn = uf_create_cc(a_company) -- I think we should set these up if we know we are going to need them
	//if rtn = -1 then
	//	return -1
	//end if
	
	Select count(*) into :cc_count
	from all_tab_columns
	where table_name = 'JOB_TASK_INTERFACE_STAGING' 
	and column_name like 'CLASS_CODE%'
	;
	if uf_check_sql() = -1 then return -1
		
	for i = 1 to cc_count
		uf_msgs('Class Code Insert '+string(i))
		sqls_cc = &
			'insert into job_task_class_code (job_task_id, work_order_id, class_code_id, value) '+&
			'select a.job_task_id, a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
			'from job_task_interface_staging a, class_code cc '+&
			'where status = 1 '+&
			"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
			'and (a.company_id =  '+string(a_company)+' or '+string(a_company)+' = -1) '+&
			"and action = 'I' "+&
			"and error_message is null "+&
			'and a.class_code'+string(i)+' is not null '+&
			'and class_value'+string(i)+' is not null '+&
			'and trim(cc.description) = trim(a.class_code'+string(i)+') '+&
			'and cc.job_task_indicator = 1 '
		execute immediate :sqls_cc;
		if uf_check_sql() = -1 then return -1
	next
	
	
	//--------------------------------------------------------------------------------
	//
	// MARK RECORDS AS PROCESSED
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Interface Status Update")
	
	// Update status to indicate it has been processed
	update job_task_interface_staging 
	set status = 2
	where status = 1
	and nvl(batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and action = 'I'
	and error_message is null;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// UPDATES
	//
	//--------------------------------------------------------------------------------
	updates:
	
	uf_msgs("------------------------------- ")
	uf_msgs("Updating Existing Job Tasks")
	
	select count(*)
	into :update_counts
	from job_task_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and action = 'U'
	and error_message is null
	;
	
	if isnull(update_counts) then update_counts = 0
	if update_counts = 0 then
		uf_msgs("No Existing Job Tasks to Process")
	end if
	
	if no_updates or update_counts = 0 then
		// Do not run updates
	else
		rtn = uf_update_job_task(a_company, a_batch)
		if rtn = -1 then
			return -1
		end if
	end if
	
next


//--------------------------------------------------------------------------------
//
// CLASS_CODE_VALUES
//
//--------------------------------------------------------------------------------
uf_msgs("Backfilling Class Code Values")
insert into class_code_values (class_code_id, value)
select class_code_id, value
from job_task_class_code
minus
select class_code_id, value
from class_code_values
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// ARCHIVE RECORDS
//
//--------------------------------------------------------------------------------
uf_msgs("Archiving Records")

// Insert into archive table
insert into job_task_interface_staging_arc (
	NOTES, CLASS_CODE1, CLASS_VALUE1, CLASS_CODE2, CLASS_VALUE2,
	CLASS_CODE3, CLASS_VALUE3, CLASS_CODE4, CLASS_VALUE4, CLASS_CODE5,
	CLASS_VALUE5, CLASS_CODE6, CLASS_VALUE6, CLASS_CODE7, CLASS_VALUE7,
	CLASS_CODE8, CLASS_VALUE8, CLASS_CODE9, CLASS_VALUE9, CLASS_CODE10,
	CLASS_VALUE10, CLASS_CODE11, CLASS_VALUE11, CLASS_CODE12, CLASS_VALUE12,
	CLASS_CODE13, CLASS_VALUE13, CLASS_CODE14, CLASS_VALUE14, CLASS_CODE15,
	CLASS_VALUE15, CLASS_CODE16, CLASS_VALUE16, CLASS_CODE17, CLASS_VALUE17,
	CLASS_CODE18, CLASS_VALUE18, CLASS_CODE19, CLASS_VALUE19, CLASS_CODE20,
	CLASS_VALUE20, SETUP_TEMPLATE, SETUP_TEMPLATE_ID, STATUS, ACTION,
	ROW_ID, PRIORITY_CODE_DESCR, SYSTEM_DESCR, GROUP_DESCR, JOB_TASK_STATUS_DESCR,
	COMPANY_ID, JOB_TASK_ID, WORK_ORDER_ID, WORK_ORDER_NUMBER, DESCRIPTION,
	LONG_DESCRIPTION, EST_START_DATE, EST_COMPLETE_DATE, COMPLETION_DATE, STATUS_CODE_ID,
	PERCENT_COMPLETE, PARENT_JOB_TASK, CHARGEABLE, FORECAST, ESTIMATE,
	LEVEL_NUMBER, WORK_EFFORT, FIELD1, FIELD2, FIELD3,
	FIELD4, FIELD5, PRIORITY_CODE_ID, EXTERNAL_JOB_TASK, JOB_TASK_STATUS_ID,
	SYSTEM_ID, GROUP_ID, INITIATOR, TASK_OWNER, BUDGET_ANALYST,
	PROJECT_MANAGER, ENGINEER, OTHER_CONTACT, RUN_DATE, batch_id,
	ext_company, seq_id, user_id, time_stamp, reason_for_work, background, future_projects, alternatives, financial_analysis, funding_task_indicator
	)
select NOTES, CLASS_CODE1, CLASS_VALUE1, CLASS_CODE2, CLASS_VALUE2,
	CLASS_CODE3, CLASS_VALUE3, CLASS_CODE4, CLASS_VALUE4, CLASS_CODE5,
	CLASS_VALUE5, CLASS_CODE6, CLASS_VALUE6, CLASS_CODE7, CLASS_VALUE7,
	CLASS_CODE8, CLASS_VALUE8, CLASS_CODE9, CLASS_VALUE9, CLASS_CODE10,
	CLASS_VALUE10, CLASS_CODE11, CLASS_VALUE11, CLASS_CODE12, CLASS_VALUE12,
	CLASS_CODE13, CLASS_VALUE13, CLASS_CODE14, CLASS_VALUE14, CLASS_CODE15,
	CLASS_VALUE15, CLASS_CODE16, CLASS_VALUE16, CLASS_CODE17, CLASS_VALUE17,
	CLASS_CODE18, CLASS_VALUE18, CLASS_CODE19, CLASS_VALUE19, CLASS_CODE20,
	CLASS_VALUE20, SETUP_TEMPLATE, SETUP_TEMPLATE_ID, STATUS, ACTION,
	ROW_ID, PRIORITY_CODE_DESCR, SYSTEM_DESCR, GROUP_DESCR, JOB_TASK_STATUS_DESCR,
	COMPANY_ID, JOB_TASK_ID, WORK_ORDER_ID, WORK_ORDER_NUMBER, DESCRIPTION,
	LONG_DESCRIPTION, EST_START_DATE, EST_COMPLETE_DATE, COMPLETION_DATE, STATUS_CODE_ID,
	PERCENT_COMPLETE, PARENT_JOB_TASK, CHARGEABLE, FORECAST, ESTIMATE,
	LEVEL_NUMBER, WORK_EFFORT, FIELD1, FIELD2, FIELD3,
	FIELD4, FIELD5, PRIORITY_CODE_ID, EXTERNAL_JOB_TASK, JOB_TASK_STATUS_ID,
	SYSTEM_ID, GROUP_ID, INITIATOR, TASK_OWNER, BUDGET_ANALYST,
	PROJECT_MANAGER, ENGINEER, OTHER_CONTACT, sysdate, batch_id,
	ext_company, seq_id, user_id, time_stamp, reason_for_work, background, future_projects, alternatives, financial_analysis, funding_task_indicator
from job_task_interface_staging
where nvl(status,0) = 2
and nvl(batch_id,'null') = :a_batch
and (company_id = :a_company or :a_company = -1)
and error_message is null
;
if uf_check_sql() = -1 then return -1

// Delete Archived Data
delete from job_task_interface_staging 
where nvl(status,0) = 2
and nvl(batch_id,'null') = :a_batch
and (company_id = :a_company or :a_company = -1)
and error_message is null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// COMMIT
//
//--------------------------------------------------------------------------------
uf_msgs("Committing records")

// Commit here
commit;

uf_msgs("Finished Job Task Loading - Company Id = "+string(a_company))
uf_msgs("------------------------------- ")
uf_msgs("------------------------------- ")

return 1
end function

public function longlong uf_update_job_task (longlong a_company, string a_batch);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_job_task_interface.uf_update_job_task()
//
//   Purpose  :   UPDATES  rows from the job_task_interface_staging
//					
//                    
//                    This functions is automatically called through the function uf_create_job_task()
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
longlong rtn, i, cc_count
string sqls_cc, sqls_update_cc

if isnull(a_batch) then a_batch = 'null'

//--------------------------------------------------------------------------------
//
// JOB TASK
//
//--------------------------------------------------------------------------------
uf_msgs("Updating Job Task")
update JOB_TASK z
set (BUDGET_ANALYST, CHARGEABLE, COMPLETION_DATE, DESCRIPTION,
	ENGINEER, ESTIMATE, EST_COMPLETE_DATE, EST_START_DATE,
	EXTERNAL_JOB_TASK, FIELD1, FIELD2, FIELD3,
	FIELD4, FIELD5, FORECAST, GROUP_ID,
	INITIATOR, JOB_TASK_ID, JOB_TASK_STATUS_ID, LEVEL_NUMBER,
	LONG_DESCRIPTION, NOTES, OTHER_CONTACT, PARENT_JOB_TASK,
	PERCENT_COMPLETE, PRIORITY_CODE_ID, PROJECT_MANAGER, SETUP_TEMPLATE_ID,
	STATUS_CODE_ID, SYSTEM_ID, TASK_OWNER, WORK_EFFORT,
	WORK_ORDER_ID, reason_for_work, background, future_projects, alternatives, financial_analysis
	) = (
	SELECT nvl(a.BUDGET_ANALYST, z.BUDGET_ANALYST), nvl(a.CHARGEABLE, z.CHARGEABLE), nvl(a.COMPLETION_DATE, z.COMPLETION_DATE), nvl(a.DESCRIPTION, z.DESCRIPTION),
		nvl(a.ENGINEER, z.ENGINEER), nvl(a.ESTIMATE, z.ESTIMATE), nvl(a.EST_COMPLETE_DATE, z.EST_COMPLETE_DATE), nvl(a.EST_START_DATE, z.EST_START_DATE),
		nvl(a.EXTERNAL_JOB_TASK, z.EXTERNAL_JOB_TASK), nvl(a.FIELD1, z.FIELD1), nvl(a.FIELD2, z.FIELD2), nvl(a.FIELD3, z.FIELD3),
		nvl(a.FIELD4, z.FIELD4), nvl(a.FIELD5, z.FIELD5), nvl(a.FORECAST, z.FORECAST), nvl(a.GROUP_ID, z.GROUP_ID),
		nvl(a.INITIATOR, z.INITIATOR), nvl(a.JOB_TASK_ID, z.JOB_TASK_ID), nvl(a.JOB_TASK_STATUS_ID, z.JOB_TASK_STATUS_ID), nvl(a.LEVEL_NUMBER, z.LEVEL_NUMBER),
		nvl(a.LONG_DESCRIPTION, z.LONG_DESCRIPTION), nvl(a.NOTES, z.NOTES), nvl(a.OTHER_CONTACT, z.OTHER_CONTACT), nvl(a.PARENT_JOB_TASK, z.PARENT_JOB_TASK),
		nvl(a.PERCENT_COMPLETE, z.PERCENT_COMPLETE), nvl(a.PRIORITY_CODE_ID, z.PRIORITY_CODE_ID), nvl(a.PROJECT_MANAGER, z.PROJECT_MANAGER), nvl(a.SETUP_TEMPLATE_ID, z.SETUP_TEMPLATE_ID),
		nvl(a.STATUS_CODE_ID, z.STATUS_CODE_ID), nvl(a.SYSTEM_ID, z.SYSTEM_ID), nvl(a.TASK_OWNER, z.TASK_OWNER), nvl(a.WORK_EFFORT, z.WORK_EFFORT), 
		nvl(a.WORK_ORDER_ID, z.WORK_ORDER_ID), nvl(a.reason_for_work, z.reason_for_work), nvl(a.background, z.background), nvl(a.future_projects, z.future_projects), nvl(a.alternatives, z.alternatives), nvl(a.financial_analysis, z.financial_analysis)
	FROM JOB_TASK_INTERFACE_STAGING a
	WHERE a.STATUS = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.action = 'U'
	and a.job_task_id = z.job_task_id
	and a.work_order_id = z.work_order_id
	)
where (z.work_order_id, z.job_task_id) in (
	select b.work_order_id, b.job_task_id
	from job_task_interface_staging b
	where b.status = 1
	and nvl(b.batch_id,'null') = :a_batch
	and (b.company_id = :a_company or :a_company = -1)
	and b.action = 'U'
	)
;	
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// CLASS CODES
//
//--------------------------------------------------------------------------------
uf_msgs("Updating Class Codes")

Select count(*) into :cc_count
from all_tab_columns
where table_name = 'JOB_TASK_INTERFACE_STAGING' 
and column_name like 'CLASS_CODE%'
;
if uf_check_sql() = -1 then return -1


//PP-44419
for i = 1 to cc_count
	uf_msgs( " Class Code Insert "+string(i))
	sqls_cc = &
		'insert into job_task_class_code (job_task_id, work_order_id, class_code_id, value) '+&
		'select a.job_task_id, a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
		'from job_task_interface_staging a, class_code cc '+&
		"where action = 'U' "+&
		'and status = 1 '+&
		"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
		'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
		'and a.class_code'+string(i)+' is not null '+&
		'and class_value'+string(i)+' is not null '+&
		'and trim(cc.description) = trim(a.class_code'+string(i)+') '+&
		'and cc.job_task_indicator = 1 '+&
		'and not exists ( '+&
		'	select 1 from job_task_class_code c '+&
		'	where c.job_task_id = a.job_task_id '+&
		'	and c.class_code_id = cc.class_code_id '+&
		'	) '	
		
	execute immediate :sqls_cc;
	if uf_check_sql() = -1 then return -1

//PP-44419
//PP-45913
		sqls_update_cc = &
		"update job_task_class_code z "+&
		"set value =  ( "+&
		"	select a.class_value"+string(i)+' '+&
		"	from job_task_interface_staging a, class_code b "+&
		"	where a.status = 1 "+&
		"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
		'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
		"	and a.action = 'U' "+&
		"	and a.class_code"+string(i)+" is not null "+&
		"	and a.class_value"+string(i)+" is not null "+&
		"	and z.work_order_id = a.work_order_id "+&
		"	and z.job_task_id = a.job_task_id "+&
		"	and z.class_code_id = b.class_code_id "+&
		"	and b.description = a.class_code"+string(i)+" "+&
		"	and b.job_task_indicator = 1 "+&
		"	and z.value <> a.class_value"+string(i)+" "+&
		"	) "+&
		"where (job_task_id, work_order_id, class_code_id) in ( "+&
		"	select c.job_task_id, c.work_order_id, e.class_code_id "+&
		"	from job_task_interface_staging c, class_code e "+&
		"	where c.status = 1 "+&
		"	and nvl(c.batch_id,'null') = '"+a_batch+"' "+&
		'	and (c.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
		"	and c.action = 'U' "+&
		"	and c.class_code"+string(i)+" is not null "+&
		"	and c.class_value"+string(i)+" is not null "+&
		"	and c.class_code"+string(i)+" = e.description "+&
		"	and e.job_task_indicator = 1 "+&
		"	and z.value <> c.class_value"+string(i)+" "+&
		"	) "
		
	execute immediate :sqls_update_cc;
	if uf_check_sql() = -1 then return -1
next


	
//////
//////Add Logic to update children Tasks for certain fields when these fields are changed on their parents
/////For now just update chargeable, estimate, forecast, status_code
/////This code only touches those tasks that have been updated by this interface
//uf_msgs('Update Children when parents have changed')
//update job_task a
//set (chargeable,estimate,forecast, status_code_id) = (
//     select nvl(b.chargeable,a.chargeable), nvl(b.estimate,a.estimate), nvl(b.forecast,a.forecast), 
//	  nvl(b.status_code_id,a.status_code_id)
//	 from job_task_interface_staging b
//	 where b.parent_job_task = a.job_task_id
//	     and a.parent_job_task is not null
//		and b.status > 0)/*Status > 0 because the I's have a status of 1 and the U's have a status of 2 at this point*/
//where parent_job_task is not null
//     and parent_job_task in (
//	      select job_task_id
//	      from job_task_interface_staging
//	      where status > 0)
//;
//if uf_check_sql() = -1 then
//	return -1
//end if


///Possibly add code to update parents %complete based on changes in children


//--------------------------------------------------------------------------------
//
// MARK RECORDS AS PROCESSED
//
//--------------------------------------------------------------------------------
uf_msgs("Interface Status Update")

// Update status to indicate it has been processed
update job_task_interface_staging a
set status = 2
where status = 1
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and action = 'U'
;
if uf_check_sql() = -1 then return -1


return 1
end function

public function integer uf_create_job_task (longlong a_company);longlong rtn
string null_str
setnull(null_str)

rtn = uf_create_job_task(a_company, null_str)

return rtn

end function

public function integer uf_create_job_task (string a_batch);longlong rtn, neg_one = -1

rtn = uf_create_job_task(neg_one, a_batch)

return rtn

end function

on uo_job_task_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_job_task_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

