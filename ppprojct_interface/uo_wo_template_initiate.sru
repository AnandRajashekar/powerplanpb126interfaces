HA$PBExportHeader$uo_wo_template_initiate.sru
forward
global type uo_wo_template_initiate from nonvisualobject
end type
end forward

global type uo_wo_template_initiate from nonvisualobject
end type
global uo_wo_template_initiate uo_wo_template_initiate

forward prototypes
public function integer uf_wo_est_template_initiate ()
public function integer uf_wo_dept_template_initiate ()
end prototypes

public function integer uf_wo_est_template_initiate ();longlong check, wo_est_template_id, cnt
uo_ds_top ds_count
string rets, sqls

select count(*) into :check
from wo_estimate
where work_order_id in (select work_order_id from wo_est_processing_temp);

// JAW 20070523 - run this for all- don't need funding wo check
if check > 0 then
	goto job_task
end if

cnt = 0

select count(*) into :cnt from wo_est_template_wo_type  
where work_order_type_id in (select revision from wo_est_processing_temp) ;


if  cnt > 0 then

insert into wo_estimate
  (estimate_id,
   revision,
   work_order_id,
   expenditure_type_id,
   est_chg_type_id,
   utility_account_id,
   retirement_unit_id,
   sub_account_id,
   stck_keep_unit_id,
   job_task_id,
   bus_segment_id,
   department_id,
   hours,
   quantity,
   amount,
   property_group_Id,
   asset_location_Id,
   replacement_amount,
   field_1,
   field_2,
   field_3,
   field_4,
   field_5,
   notes,
   percent,
   wo_est_trans_type_id)
  (select pwrplant1.nextval,
          1,
          c.work_order_id,
          expenditure_type_id,
          est_chg_type_id,
          utility_account_id,
          retirement_unit_id,
          sub_account_id,
          stck_keep_unit_id,
          job_task_id,
          a.bus_segment_id,
          department_id,
          hours,
          quantity,
          amount,
          property_group_Id,
          asset_location_Id,
          replacement_amount,
          field_1,
          field_2,
          field_3,
          field_4,
          field_5,
          notes,
          percent,
          decode(expenditure_type_id, 1, 1, 2, 2, 6)
     from wo_est_template         a,
          wo_est_template_wo_type b,
          wo_est_processing_temp  c
    where a.wo_est_template_id = b.wo_est_template_id
      and b.work_order_type_id = c.revision);
	
end if
		
// PP-41865
if sqlca.sqlcode <> 0 then
	if g_main_application then
		messagebox("Update (f_wo_est_template_initiate)", "Insert failed into wo_estimate." + sqlca.sqlerrtext, Exclamation!)
	else
		f_pp_msgs("f_wo_est_template_initiate - Insert failed into wo_estimate." + sqlca.sqlerrtext)
	end if
	return -1
end if

job_task:

// PP-41865
select count(*) into :check
from job_task
where work_order_id in (select work_order_id from wo_est_processing_temp);

if check > 0 then
	goto monthly_est
end if

insert into job_task (
	work_order_id,
	job_task_id,
	description,
	long_description,
	status_code_id,
	est_start_date,
	est_complete_date,
	parent_job_task,
	chargeable,
	forecast,
	estimate,
	level_number )
select wept.work_order_id,
	jtl.job_task_id,
	jtl.description,
	jtl.long_description,
	1 status_code_id,
	woc.est_start_date,
	woc.est_complete_date,
	jtl.parent_job_task,
	jtl.chargeable,
	jtl.forecast,
	jtl.estimate,
	jtl.level_number
from job_task_default jtd, job_task_list jtl, work_order_control woc, wo_est_processing_temp wept
where jtd.work_order_type_id = wept.revision
and jtd.job_task_id = jtl.job_task_id
and woc.work_order_id = wept.work_order_id;

// PP-41865
if sqlca.sqlcode <> 0 then
	if g_main_application then
		messagebox("Update (f_wo_est_template_initiate)", "Insert failed into job_task." + sqlca.sqlerrtext, Exclamation!)
	else
		f_pp_msgs("f_wo_est_template_initiate - Insert failed into job_task." + sqlca.sqlerrtext)
	end if
	return -1
end if

monthly_est:

// PP-41865
select count(*) into :check
from wo_est_monthly
where work_order_id in (select work_order_id from wo_est_processing_temp);

if check > 0 then
	goto f_end
end if

// Default estimate lines based on work order type
insert into wo_est_monthly
(est_monthly_id, work_order_id, revision, expenditure_type_id, 
	est_chg_type_id, department_id, wo_work_order_id, job_task_id, utility_account_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june, july, august, 
	september, october, november, december, total, year
)
(select pwrplant1.nextval, work_order_id, revision, expenditure_type_id,
	est_chg_type_id, department_id, wo_work_order_id, job_task_id, utility_account_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	0,0,0,0,0,0,0,0,0,0,0,0,0,year 
	from		
	(select d.work_order_id, 1 revision, a.expenditure_type_id,
			a.est_chg_type_id, a.department_id, a.wo_work_order_id, a.job_task_id, a.utility_account_id, a.budget_id,
			a.attribute01_id, a.attribute02_id, a.attribute03_id, a.attribute04_id, a.attribute05_id, a.attribute06_id, a.attribute07_id, a.attribute08_id, a.attribute09_id, a.attribute10_id
		from wo_est_monthly_default a, work_order_control b, work_order_type c, wo_est_processing_temp d
		where a.wem_default_id = c.wem_default_id and
			c.work_order_type_id = b.work_order_type_id and
			b.work_order_id = d.work_order_id and b.est_start_date is not null and
			b.est_complete_date is not null
	) combos,
	(select min(year) year
		from work_order_control a, 
			(select year from pp_table_years) b,
			wo_est_processing_temp c
		where a.work_order_id = c.work_order_id and 
			year between to_char(a.est_start_date, 'yyyy') and
					to_char(a.est_complete_date, 'yyyy')
	) years
);

if sqlca.sqlcode < 0 then
	// ### CDM - Maint 8189 - remove rollbacks and f_setpointers
	//ROLLBACK using SQLCA ;
	if g_main_application then
		messagebox("Update (f_wo_est_template_initiate)", "Insert failed into wo_est_monthly. "  + sqlca.sqlerrtext, Exclamation!)
	else
		f_pp_msgs("f_wo_est_template_initiate - Insert failed into wo_est_monthly. "  + sqlca.sqlerrtext)
	end if
	//f_setpointer("arrow", "Ready")
	return -1
end if
          


// Insert years other than start year
insert into wo_est_monthly
(est_monthly_id, work_order_id, revision, expenditure_type_id, 
	est_chg_type_id, department_id, wo_work_order_id, job_task_id, utility_account_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june, july, august, 
	september, october, november, december, total, year
)
(select est_monthly_id, a.work_order_id, a.revision, expenditure_type_id,
	est_chg_type_id, department_id, wo_work_order_id, job_task_id, utility_account_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	0,0,0,0,0,0,0,0,0,0,0,0,0, years.year
	from wo_est_monthly a,	
		(select year  year      //BMB - Need to take out the MIN, it only inserted estimates for start_year + 1
			from work_order_control a, 
				(select year from pp_table_years) b,
				wo_est_processing_temp c
			where a.work_order_id = c.work_order_id and 
				year between to_char(a.est_start_date, 'yyyy') + 1 and
						to_char(a.est_complete_date, 'yyyy')
		) years,
		wo_est_processing_temp c
	where a.work_order_id = c.work_order_id
	and years.year is not null //JRD = need to add this for single year project
);
 
if sqlca.sqlcode < 0 then
	// ### CDM - Maint 8189 - remove rollbacks and f_setpointers
	//ROLLBACK using SQLCA ;
	if g_main_application then
		messagebox("Update (f_wo_est_template_initiate)", "Insert failed into wo_est_monthly." + sqlca.sqlerrtext, Exclamation!)
	else
		f_pp_msgs("f_wo_est_template_initiate - Insert failed into wo_est_monthly." + sqlca.sqlerrtext)
	end if
	//f_setpointer("arrow", "Ready")
	return -1
end if


f_end:
/****************************************************************************************************************************/
check = 0
check = uf_wo_dept_template_initiate()
if check < 1 then
	// ### CDM - Maint 8189 - remove rollbacks and f_setpointers
	//ROLLBACK using SQLCA ;
	//f_setpointer("arrow", "Ready")
	return -1
end if
/****************************************************************************************************************************/

return 1
end function

public function integer uf_wo_dept_template_initiate ();longlong rtn
boolean success = true

/*Check to see if there is any default if not don't waste time get out of here*/
select count(1) into :rtn from wo_type_dept_template, wo_est_processing_temp  where work_order_type_id = revision;
if rtn < 1 then 
	goto inherit_fp
end if

/*Insert into work_order_department from templates*/
insert into work_order_department (work_order_id, department_id)
select 
	work_order_id,
	department_id
from 
(
	select distinct department.department_id, work_order_id 
	from wo_type_dept_template, dept_template_assign, department, wo_est_processing_temp
	where wo_type_dept_template.dept_template_id = dept_template_assign.dept_template_id
	and dept_template_assign.department_id = department.department_id
	and department.status_code_id = 1
	and wo_type_dept_template.work_order_type_id = wo_est_processing_temp.revision
)
minus
select a.work_order_id, department_id from work_order_department a, wo_est_processing_temp b
where a.work_order_id = b.work_order_id
;
if sqlca.sqlcode < 0 then
	// ### CDM - Maint 8189 - only use messagebox in visual app
	if g_main_application then
		messagebox("Error occurred in f_wo_dept_template_initiate()", "Insert failed into work_order_department (Templates).~r~n "  + sqlca.sqlerrtext, Exclamation!)
	else
		f_pp_msgs("Error occurred in f_wo_dept_template_initiate() - Insert failed into work_order_department (Templates).~r~n "  + sqlca.sqlerrtext)
	end if
	success = false
	goto TheEnd
end if

inherit_fp:
/*Insert into work_order_department from Funding Project*/
// PP-41916
insert into work_order_department (work_order_id, department_id)
select wept.work_order_id, d.department_id 
from department d, work_order_department wod, work_order_control woc, wo_est_processing_temp wept
where wept.work_order_id = woc.work_order_id
and woc.funding_wo_id = wod.work_order_id
and woc.funding_wo_indicator = 0
and wod.department_id = d.department_id
and d.status_code_id = 1
and exists (
	select 1
	from pp_system_control_companies pp
	where pp.company_id = woc.company_id
	and lower(trim(control_name)) = lower('woinit - inherit fp dept tab')
	and lower(trim(control_value)) = lower('yes'))		
minus
select wod.work_order_id, wod.department_id
from work_order_department wod, wo_est_processing_temp wept
where wept.work_order_id = wod.work_order_id
;
if sqlca.sqlcode < 0 then
	// ### CDM - Maint 8189 - only use messagebox in visual app
	if g_main_application then
		messagebox("Error occurred in f_wo_dept_template_initiate()", "Insert failed into work_order_department (Inherit from FP).~r~n"  + sqlca.sqlerrtext, Exclamation!)
	else
		f_pp_msgs("Error occurred in f_wo_dept_template_initiate() - Insert failed into work_order_department (Inherit from FP).~r~n"  + sqlca.sqlerrtext)
	end if
	success = false
	goto TheEnd
end if

theEnd:
if success then
	return 1
else 
	return -1
end if
end function

on uo_wo_template_initiate.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wo_template_initiate.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

