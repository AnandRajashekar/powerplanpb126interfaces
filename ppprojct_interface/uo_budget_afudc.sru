HA$PBExportHeader$uo_budget_afudc.sru
$PBExportComments$7767-Update budget_amounts after processing budget items
forward
global type uo_budget_afudc from nonvisualobject
end type
end forward

global type uo_budget_afudc from nonvisualobject
end type
global uo_budget_afudc uo_budget_afudc

type variables

string i_table_name
longlong i_actuals_month_number
longlong i_start_month_number
longlong i_end_month_number
longlong i_tax_cwip_est_chg_type
longlong i_book_cwip_est_chg_type
longlong i_tax_closings_est_chg_type
longlong i_book_closings_est_chg_type
longlong i_status_position
longlong i_cwip_start_month
longlong i_budget_version = 0
longlong i_funding_wo_indicator = 1
boolean i_status_box = false
boolean i_progress_bar = true
boolean i_pp_msgs = false
boolean i_messagebox_onerror = true
boolean i_statusbox_onerror = true
boolean i_add_time_to_messages = true
boolean i_display_rowcount = true 
boolean i_display_msg_on_sql_success = true
boolean i_return_cpi
boolean i_cap_interest = false
boolean i_stage_tax = false, i_stage_cpi = true, i_restrict = true
boolean i_dont_calc_afc = false///some clients want to user the AFC proces to calculate closings but just accept the AFUDC passed in
boolean i_stage_wip = false
end variables

forward prototypes
public function longlong uf_calc ()
public function longlong uf_delete_afudc ()
public function longlong uf_return_results ()
public function longlong uf_transpose ()
public subroutine uf_msg (string a_msg, string a_error)
public function longlong uf_check_sql (string a_msg)
public function longlong uf_read (string a_table_name, longlong a_actuals_month_number)
public function longlong uf_start_month ()
public function longlong uf_cap_interest_setup ()
public function longlong uf_cwip_balances ()
public function longlong uf_closings_by_wo ()
public function longlong uf_pre_process (string a_table_name, longlong a_actuals_month_number)
public function longlong uf_post_process ()
public function longlong uf_closings_to_deprfcst (longlong a_actuals_month_number, string a_table_name, longlong a_fcst_depr_version)
public function longlong uf_detailed_closings (longlong a_actuals_month_number, string a_table_name, longlong a_fcst_depr_version)
end prototypes

public function longlong uf_calc ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_calc() 
//
//	  Purpose  :	This function contains the loop over the months to calculate AFUDC and CPI.  This function also calculates closings
//		             
//
//   Arguments:		
//	
//   Returns :   1 if successfuly -1 if a failure occurs
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
longlong i, prev_month_number, count, count_total, increment_amount, test_end_year, rtn

uf_msg('Starting AFUDC Calc','I')

count = 1


select max(month_number)
into :i_end_month_number
from budget_afudc_calc_temp
;
if uf_check_sql('Selecting last month to process') <> 1 then return -1

if  i_budget_version <> 0 then
	///if processing at the budget item level lest only run through the end of the budget version
	select end_year*100+12
	into :test_end_year
	from budget_version
	where budget_version_id = :i_budget_version
	;
	if uf_check_sql('Select Budget Version end year') <> 1 then return -1
	
	if not isnull(test_end_year) then
		if test_end_year < i_end_month_number and test_end_year > i_actuals_month_number then
			i_end_month_number = test_end_year
		end if
	end if
	
end if

prev_month_number = i_cwip_start_month - 1

count_total = i_end_month_number - i_start_month_number
if count_total <> 0 then
	increment_amount = 60/count_total
end if

for i = i_cwip_start_month to i_end_month_number
	i_status_position = i_status_position + increment_amount
	if long(mid(string(i),5,2)) > 12 then
		i = (long(mid(string(i),1,4)) + 1)*100 + 1
	end if
	if i <> i_cwip_start_month then
		/* 20150929 CJS Changing for performance gains */
		update budget_afudc_calc_temp a
		set (uncompounded_afudc, uncompounded_cpi, beg_cwip, beg_cwip_afudc, beg_cwip_cpi, cpi_balance, tax_expense_amount_bal) = 
    			 (select
				 (nvl(uncompounded_afudc,0)  + nvl(afudc_debt,0) + nvl(afudc_equity,0) - nvl(compounded_afudc,0)) - 
							  ((nvl(uncompounded_afudc,0)  + nvl(afudc_debt,0) + nvl(afudc_equity,0) - nvl(compounded_afudc,0)) * closings_pct) uncompounded_afudc,
					  /*20100105 CJR the above change made to reduce the uncompounded AFUDC by the closing percent*/
				 (nvl(uncompounded_cpi,0) + nvl(cpi,0) - nvl(compounded_cpi,0)) - 
							 ((nvl(uncompounded_cpi,0) + nvl(cpi,0) - nvl(compounded_cpi,0)) * closings_pct) uncompouded_cpi,
					  /*20100105 CJR the above change made to reduce the uncompounded CPI by the closing percent*/
				 nvl(end_cwip,0) beg_cwip,
				 nvl(end_cwip_afudc,0) beg_cwip_afudc,
				 nvl(end_cwip_cpi,0) beg_cwip_cpi,
				 nvl(cpi_balance,0) +nvl(cpi,0) - nvl(cpi_closings,0) cpi_balance,
				 nvl(tax_expense_amount_bal,0) + nvl(tax_expense_amount,0) - nvl(tax_expense_amount_closed,0) tax_expense_amount_bal
			 from budget_afudc_calc_temp b
				  where a.work_order_id = b.work_order_id
					and a.expenditure_type_id = b.expenditure_type_id
					and a.wo_work_order_id = b.wo_work_order_id
					and b.month_number = :prev_month_number)
   		where a.month_number = :i
		;
		if uf_check_sql('Rolling forward last months results for month_number = '+string(i)) <> 1 then return -1

		update budget_afudc_calc_temp a 
		set 
		uncompounded_afudc = decode(uncompounded_afudc, null, 0, uncompounded_afudc),
		uncompounded_cpi = decode(uncompounded_cpi, null, 0, uncompounded_cpi),
		beg_cwip = decode(beg_cwip, null, 0, beg_cwip),
		beg_cwip_afudc = decode(beg_cwip_afudc, null, 0, beg_cwip_afudc),
		beg_cwip_cpi = decode(beg_cwip_cpi, null, 0, beg_cwip_cpi),
		cpi_balance = decode(cpi_balance, null, 0, cpi_balance),
		tax_expense_amount_bal = decode(tax_expense_amount_bal, null, 0, tax_expense_amount_bal)
		where month_number = :i
		and 
		(uncompounded_afudc is null or
		uncompounded_cpi is null or
		beg_cwip is null or
		beg_cwip_afudc is null or
		beg_cwip_cpi is null or
		cpi_balance is null or
		tax_expense_amount_bal is null)
		;
		if uf_check_sql('Updating empty rollforwards for month_number = '+string(i)) <> 1 then return -1
	end if

	update budget_afudc_calc_temp
	set compounded_afudc = nvl(uncompounded_afudc,0) * nvl(afudc_compound_ind,0),
	     compounded_cpi = nvl(uncompounded_cpi,0) * nvl(cpi_compound_ind,0)
	where month_number = :i
	;
	if uf_check_sql('Compounding AFUDC based on indicators for month = '+string(i)) <> 1 then return -1


	
	
	///###CJR Maint 5845 old, none, whole and day options were backwards.  
	update budget_afudc_calc_temp a
	set  afudc_base = nvl(beg_cwip_afudc,0) + nvl(current_month_charges_afudc ,0) + nvl(compounded_afudc,0) + (
				(nvl(beg_cwip_afudc,0) + nvl(current_month_charges_afudc ,0) + nvl(compounded_afudc,0)) * closings_pct *  (
			select 	decode(lower(nvl(b.in_service_option,'half')),
						'old',0,
						'none',1,
						'whole',0,
						'half',.5,
						'day',1 - ((to_char(a.in_service_date,'dd') - 1) / to_char(last_day(a.in_service_date),'dd')),
						.5)
			from afudc_control b
			where a.afudc_type_id = b.afudc_type_id) * -1),
		 cpi_base= nvl(beg_cwip_cpi,0) + (nvl(current_month_charges_cpi,0)*(1 - nvl(fp_percent_expense,0))) + nvl(compounded_cpi,0) + (
		 	(nvl(beg_cwip_cpi,0) + (nvl(current_month_charges_cpi,0)*(1 - nvl(fp_percent_expense,0))) + nvl(compounded_cpi,0)) * closings_pct * (
			select 	decode(lower(nvl(b.in_service_option,'half')),
						'old',0,
						'none',1,
						'whole',0,
						'half',.5,
						'day',1 - ((to_char(a.in_service_date,'dd') - 1) / to_char(last_day(a.in_service_date),'dd')),
						.5)
			from afudc_control b
			where a.afudc_type_id = b.afudc_type_id) * -1)
	where month_number = :i
	;
	if uf_check_sql('Calculating AFUDC Base for month =  '+string(i)) <> 1 then return -1
	
	if i_cap_interest then
			
//			update budget_afudc_calc_temp a
//			set a.cap_interest_adj_ratio = (
//				select y.percent
//				from (
//					select bac.work_order_id, bac.expenditure_type_id, decode(sum(bac.afudc_debt),0,0,-(sum(bac.afudc_debt) - (sum(nvl(b.internal_interest_expense,0) + nvl(b.external_interest_expense,0)))) /sum(bac.afudc_debt)) percent,
//							bac.month_number
//					from budget_cap_interest_calc b, work_order_control woc, budget_afudc_calc_temp bac
//					where bac.work_order_id = woc.work_order_id
//						and woc.company_id = b.company_id
//						and bac.month_number = b.month_number
//						and bac.month_number = :i
//						and bac.expenditure_type_id = 1
//					group by bac.work_order_id, bac.month_number, bac.expenditure_type_id) y
//				where a.work_order_id = y.work_order_id
//				  and a.month_number = y.month_number
//				  and a.expenditure_type_id = y.expenditure_type_id
//				)
//			where exists (
//				select 1
//				from (
//					select bac.work_order_id, bac.expenditure_type_id, bac.month_number
//					from budget_cap_interest_calc b, work_order_control woc, budget_afudc_calc_temp bac
//					where bac.work_order_id = woc.work_order_id
//						and woc.company_id = b.company_id
//						and bac.month_number = b.month_number
//						and bac.month_number = :i
//						and bac.expenditure_type_id = 1
//					group by bac.work_order_id, bac.month_number, bac.expenditure_type_id) t
//				where a.work_order_id =t.work_order_id
//					and a.month_number = t.month_number
//					and a.expenditure_type_id = t.expenditure_type_id
//			)
//			;
//			if uf_check_sql('Building Cap Intrest Adjustment Factors '+string(i)) <> 1 then return -1
			
			update budget_afudc_calc_temp a
			set cap_interest_adjustment = nvl(afudc_base,0) * nvl(cap_interest_adj_ratio,0)
			where month_number = :i
			  and exists (select 1 from budget_cap_interest_wo b where a.work_order_id = b.work_order_id)
			;
			if uf_check_sql('Adjusting Cap Interest Amount Child '+string(i)) <> 1 then return -1
			
			update budget_afudc_calc_temp a
			set cap_interest_adjustment = (
				select c.adj_amt
				from(
					select b.expenditure_type_id, b.month_number, c.parent_wo, -1*sum(b.cap_interest_adjustment) adj_amt, b.wo_work_order_id
					from budget_afudc_calc_temp b, budget_cap_interest_wo c
					where c.work_order_id = b.work_order_id
						and b.expenditure_type_id = 1
						and b.month_number = :i
					group by b.expenditure_type_id, b.month_number, c.parent_wo, b.wo_work_order_id
					) c
				where a.work_order_id = c.parent_wo
					and a.expenditure_type_id = c.expenditure_type_id
					and a.month_number = c.month_number
					and a.wo_work_order_id = c.wo_work_order_id
			)
			where a.month_number = :i
			    and (a.work_order_id, a.expenditure_type_id, a.month_number, a.wo_work_order_id) in (
				select c.parent_wo, b.expenditure_type_id, b.month_number, b.wo_work_order_id
					from budget_afudc_calc_temp b, budget_cap_interest_wo c
					where c.work_order_id = b.work_order_id
						and b.expenditure_type_id = 1
						and b.month_number = :i
				group by b.expenditure_type_id, b.month_number, c.parent_wo, b.wo_work_order_id)
			;
			if uf_check_sql('Adjusting Cap Interest Amount Parent '+string(i)) <> 1 then return -1
			
//			update budget_afudc_calc_temp
//			set afudc_base = nvl(afudc_base,0) + nvl(cap_interest_adjustment,0)
//			where month_number = :i
//			;
//			if uf_check_sql('Adjusting AFUDC base '+string(i)) <> 1 then return -1
				
		
	end if
	
	
	if i > i_actuals_month_number then
		update budget_afudc_calc_temp
		set afudc_debt = (nvl(afudc_base,0)+nvl(cap_interest_adjustment,0)) * nvl(afudc_debt_rate,0) * nvl(eligible_for_afudc,0) * nvl(input_afudc_ratio,1),
			afudc_equity = nvl(afudc_base,0) * nvl(afudc_equity_rate,0) * nvl(eligible_for_afudc,0) * nvl(input_afudc_ratio,1),
			cpi = nvl(cpi_base,0) * nvl(cpi_rate,0) * nvl(eligible_for_cpi,0) * nvl(input_cpi_ratio,1)
		where month_number = :i
		;
		if uf_check_sql('Calculating AFUDC  month =  '+string(i)) <> 1 then return -1
	
		///###7970 CJR add code to zero out negative afudc based on afudc control flag.  cap interest calc ignores this flag
		if not i_cap_interest then
			update budget_afudc_calc_temp
			set afudc_debt = 0, afudc_equity = 0
			where nvl(afudc_base,0) < 0 
			and afudc_type_id in (select afudc_type_id from afudc_control where nvl(allow_neg_afudc,0) = 0)
			and month_number = :i
			;
			if uf_check_sql('Calculating AFUDC (reverse negatives) month =  '+string(i)) <> 1 then return -1
			
			update budget_afudc_calc_temp
			set cpi = 0
			where nvl(cpi_base,0) < 0 
			and afudc_type_id in (select afudc_type_id from afudc_control where nvl(allow_neg_cpi,0) = 0)
			and month_number = :i
			;
			if uf_check_sql('Calculating AFUDC (reverse negatives) month =  '+string(i)) <> 1 then return -1
		end if
	
	
		//	update budget_afudc_calc_temp a
		//	set afudc_debt = nvl(afudc_debt,0) * (
		//		select 	decode(lower(nvl(b.in_service_option,'half')),
		//					'old',1,
		//					'none',0,
		//					'whole',1,
		//					'half',.5,
		//					'day',to_char(a.in_service_date,'dd') / to_char(last_day(a.in_service_date),'dd'),
		//					.5)
		//		from afudc_control b
		//		where a.afudc_type_id = b.afudc_type_id),		
		//		afudc_equity = nvl(afudc_equity,0) * (
		//		select 	decode(lower(nvl(b.in_service_option,'half')),
		//					'old',1,
		//					'none',0,
		//					'whole',1,
		//					'half',.5,
		//					'day',to_char(a.in_service_date,'dd') / to_char(last_day(a.in_service_date),'dd'),
		//					.5)
		//		from afudc_control b
		//		where a.afudc_type_id = b.afudc_type_id),
		//		cpi = nvl(cpi,0) * (
		//		select 	decode(lower(nvl(b.in_service_option,'half')),
		//					'old',1,
		//					'none',0,
		//					'whole',1,
		//					'half',.5,
		//					'day',to_char(a.in_service_date,'dd') / to_char(last_day(a.in_service_date),'dd'),
		//					.5)
		//		from afudc_control b
		//		where a.afudc_type_id = b.afudc_type_id) 
		//	where to_char(a.in_service_date,'YYYYMM') = a.month_number
		//		and month_number = :i
		//	;
		//	if uf_check_sql('Factoring AFUDC for in-service month month =  '+string(i)) <> 1 then return -1
		
//		//	alter table budget_afudc_calc add tax_status_id number(22,0);
//		//alter table budget_afudc_calc add fp_percent_expense number(22,8);
//		//alter table budget_afudc_calc add tax_expense_amount number(22,2);
//		//alter table budget_afudc_calc add tax_expense_amount_bal number(22,2);
//		//alter table budget_afudc_calc add tax_expense_amount_closed number(22,2);
//		///CJR the table wo_est_closings amount contains closings dollar amounts a status = 2 means they have not been used to build a closing pattern and they should be picked up here
//		///CJR the table wo_est_closings amount contains closings dollar amounts a status = 2 means they have not been used to build a closing pattern and they should be picked up here
//		update budget_afudc_calc_temp a
//		set (current_month_closings)= (
//			select sum(w.WO_EST_CLOSINGS_AMOUNT)
//			from WO_EST_CLOSINGS_WOS w
//			where w.funding_wo_id = a.work_order_id
//			and w.month_number = :i
//			and w.revision = a.revision
//			and decode(w.expenditure_type_id,-1,a.expenditure_type_id, w.expenditure_type_id) = a.expenditure_type_id
//			and nvl(w.status,0) = 2)
//		where exists (
//			select 1
//			from WO_EST_CLOSINGS_WOS w
//			where w.funding_wo_id = a.work_order_id
//			and w.month_number = :i
//			and w.revision = a.revision
//			and decode(w.expenditure_type_id,-1,a.expenditure_type_id, w.expenditure_type_id) = a.expenditure_type_id
//			and nvl(w.status,0) = 2)
//		and closing_pattern_id is null
//		and month_number = :i
//		;
//		if uf_check_sql('Adding in closing amounts for month_number =  '+string(i)) <> 1 then return -1
//	
//	
//		update budget_afudc_calc_temp
//		set closings_pct = decode(closings_pct,1,1,decode( (nvl(beg_cwip,0) +nvl(amount,0)+nvl(afudc_debt,0) + nvl(afudc_equity,0)),0,0,
//		decode(sign((nvl(current_month_closings,0) -  (nvl(beg_cwip,0) +nvl(amount,0)+nvl(afudc_debt,0) + nvl(afudc_equity,0)))),1,1,0,1,nvl(current_month_closings,0)/(nvl(beg_cwip,0) +nvl(amount,0)+nvl(afudc_debt,0) + nvl(afudc_equity,0)))))
//		where nvl(current_month_closings,0) <> 0
//		and closing_pattern_id is null
//		and month_number = :i
//		;
//		if uf_check_sql('Setting Closings Pct for month_number =  '+string(i)) <> 1 then return -1
//
	
		update budget_afudc_calc_temp
		set current_month_closings = (nvl(beg_cwip,0) +nvl(amount,0)+nvl(afudc_debt,0) + nvl(afudc_equity,0)) * nvl(closings_pct,0),
			 current_month_closings_afudc = 	(nvl(beg_cwip_afudc,0) + nvl(compounded_afudc,0) + nvl(amount_afudc,0) + nvl(cap_interest_adjustment,0)) * nvl(closings_pct,0),
			 current_month_closings_cpi = (nvl(beg_cwip_cpi,0) + nvl(amount_cpi,0) +  nvl(compounded_cpi,0)- nvl(tax_expense_amount,0)) * nvl(closings_pct,0) ,
			 cpi_closings = (nvl(cpi_balance,0) + nvl(cpi,0)) * nvl(closings_pct,0),
			 tax_expense_amount_closed = nvl(tax_expense_amount_bal,0) * nvl(closings_pct,0)
		where month_number = :i
		;
		if uf_check_sql('Calculating Closings for month =  '+string(i)) <> 1 then return -1
	end if
	
	update budget_afudc_calc_temp
	set end_cwip = nvl(beg_cwip,0) + nvl(amount,0) - nvl(current_month_closings,0) + nvl(afudc_debt,0) + nvl(afudc_equity,0) ,
		 end_cwip_afudc = nvl(beg_cwip_afudc,0) + nvl(amount_afudc,0) - nvl(current_month_closings_afudc,0)  + nvl(compounded_afudc,0) + nvl(cap_interest_adjustment,0),
		 end_cwip_cpi = nvl(beg_cwip_cpi,0) + nvl(amount_cpi,0) - nvl(current_month_closings_cpi,0)  + nvl(compounded_cpi,0)
	where month_number = :i
	;
	if uf_check_sql('Calculating Eng CWIP for month =  '+string(i)) <> 1 then return -1
	

	prev_month_number = i
	
	count ++
next

rtn = f_budget_afudc_custom(2,i_table_name, i_actuals_month_number)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_afudc' ) = 1 then
	rtn = sqlca.f_client_budget_afudc(2,i_table_name, i_actuals_month_number)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = '2'
args[2] = i_table_name
args[3] = string(i_actuals_month_number)
rtn = f_wo_validation_control(81,args)
if rtn < 0 then
	rollback;
	return -1
end if

return 1
end function

public function longlong uf_delete_afudc ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_delete_afudc() 
//
//	  Purpose  :	This function deletes all previously calculated AFUDC Debt and Equity and CPI for the work orders id's and revisions loaded in wo_est_processing_temp 
//		             
//
//   Arguments:	 none
//	
//   Returns :   1 if successfuly -1 if failed (SQL Error)
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 	

string sqls, revision, spread_table, spread_field, sqls2

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	spread_table = 'BUDGET_MONTHLY_SPREAD'
	spread_field = 'BUDGET_MONTHLY_ID'
	revision = 'BUDGET_VERSION_ID'
else 
	spread_table = 'WO_EST_MONTHLY_SPREAD'
	spread_field = 'EST_MONTHLY_ID'
	revision = 'REVISION'
end if
///CJR this delete is slow and it doesn't hurt anything to strand records in this table.
//uf_msg('Deleting Spread factors','I')
//sqls = " "
//sqls += " delete from "+spread_table+ " c "
//sqls += " where exists ( "
//sqls += " select 1 "
//sqls += " from estimate_charge_type e, wo_est_processing_temp w, "+i_table_name+" a "
//sqls += " where e.est_chg_type_id = a.est_chg_type_id "
//sqls += " and nvl(e.afudc_flag,'Q') in ('E','D','P') "
//if i_table_name <> 'BUDGET_MONTHLY_DATA' then
//	sqls += "   and a.work_order_id= w.work_order_id"
//else
//	sqls += "   and a.budget_id= w.work_order_id"
//end if
//sqls += "   and w.revision = a."+revision
//sqls += "   and a.est_chg_type_id = e.est_chg_type_id "
//sqls += "   and a."+spread_field+" = c."+spread_field
//sqls +=" ) "
//execute immediate :sqls;
//if uf_check_sql("Deleting Previously Calc Spread id's") <> 1 then return -1
uf_msg('Deleted '+string(sqlca.sqlnrows),'I')
uf_msg('Deleting Previously calculated AFUDC','I')
sqls2 = " "
sqls2 += " delete from "+i_table_name+" a "
sqls2 += " where exists  ( "
sqls2 += "	select 1 "
sqls2 += "	from wo_est_processing_temp w "
sqls2 += "	where  "
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls2 += "    a.work_order_id= w.work_order_id"
else
	sqls2 += "    a.budget_id= w.work_order_id"
end if
sqls2 += "   and w.revision = a."+revision
sqls2 += " ) "
sqls2 += " and a.est_chg_type_id in (select est_chg_type_id from estimate_charge_type where nvl(afudc_flag,'Q') in ('D','E','P')) "
execute immediate :sqls2;
if uf_check_sql('Deleting Previously Calc AFUDC Est') <> 1 then return -1
uf_msg('Deleted '+string(sqlca.sqlnrows),'I')

i_status_position =90

return 1
end function

public function longlong uf_return_results ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_return_results() 
//
//	  Purpose  :	This function takes the calculated results and returns them to the table this process was being run for (wo_est_monthly, budget_monthly_data, wo_est_monthly_subs_det)
//		             
//
//   Arguments:		
//	
//   Returns :   1 if successfuly -1 if a failure occurs
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string sqls_debt, sqls_equity, sqls_cpi,spread_table, spread_field, revision, utility_account_col, sqls2, start_date_col, end_date_col, date_table, pk_col_name, wo_col, id_col
string sqls3, return_cpi_cv, sqls
longlong cpi_est_check, debt_est_check, equity_est_check, rtn
boolean est_error = false

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	spread_table = 'BUDGET_MONTHLY_SPREAD'
	spread_field = 'BUDGET_MONTHLY_ID'
	revision = 'BUDGET_VERSION_ID'
	utility_account_col = 'budget_plant_class_id'
	start_date_col = 'start_date'
	end_date_col = 'close_date'
	date_table = "budget_amounts"
	pk_col_name = " budget_monthly_id "
	wo_col = 'null'
	id_col = 'budget_id'
else 
	spread_table = 'WO_EST_MONTHLY_SPREAD'
	spread_field = 'EST_MONTHLY_ID'
	revision = 'REVISION'
	utility_account_col = 'utility_account_id'
	start_date_col = 'est_start_date'
	end_date_col = 'est_complete_date'
	date_table = 'work_order_approval'
	pk_col_name = " est_monthly_id "
	wo_col = 'work_order_id'
	id_col = 'work_order_id'
end if

// pp_system_control_company(control_id, control_name, control_value,
//description, long_description, company_id)
//values(pwrplant1.nextval, 'Budget - Return CPI', 'Yes','dw_yes_no;1',
//'if set to yes the calculated cpi results will be put back on wo_est_monthly if no they will only be in budget_afudc_calc',-1)
//;
//commit;

//select lower(trim(control_value)) into :return_cpi_cv
//		from pp_system_control_company
//		where company_id = -1
//		and upper(trim(control_name)) = upper(trim('Budget - Return CPI'));

return_cpi_cv = f_pp_system_control('Budget - Return CPI')
		
if isnull(return_cpi_cv) then return_cpi_cv = 'no'

if return_cpi_cv = 'yes' then 
	i_return_cpi = true
else
	i_return_cpi = false
end if

///###9265 this is no longer needed because wo est monthly no longer needs to have est monthly id's by estimate attributes
update budget_afudc_calc_temp
set est_monthly_id_debt = pwrplant1.nextval
;
if uf_check_sql('Setting Debt Est Monthly id') <> 1 then return -1
update budget_afudc_calc_temp
set est_monthly_id_equity = pwrplant1.nextval
;
if uf_check_sql('Setting Equity Est Monthly id') <> 1 then return -1
update budget_afudc_calc_temp
set est_monthly_id_cpi = pwrplant1.nextval
;
if uf_check_sql('Setting CPI Est Monthly id') <> 1 then return -1
//
//
//// ### CDM - Maint 5814 - add index for this and ability to add hint to force use of index
//sqls = &
//"update budget_afudc_calc_temp a "+&
//"set (est_monthly_id_debt,est_monthly_id_equity,est_monthly_id_cpi)= ( "+&
//" select min(est_monthly_id_debt), min(est_monthly_id_equity), min(est_monthly_id_cpi) "+&
//"  from budget_afudc_calc_temp b "+&
//"  where a.work_order_id = b.work_order_id "+&
//"  and a.revision = b.revision "+&
//"  and a.expenditure_type_id = b.expenditure_type_id) "
//sqls = f_sql_add_hint(sqls,"uo_afudc.uf_return_results10")
//execute immediate :sqls;
//
//### CJR Maint 5847
if i_funding_wo_indicator = 2 then
	//if Budget Level PRocessing get min est chg type by AFUDC_FLAG
	update budget_afudc_calc_temp a
	set debt_est_chg_type_id = (
			select min(est_chg_type_Id) from estimate_charge_type
				where afudc_flag in ('D')
				and funding_charge_indicator = decode(:i_funding_wo_indicator,1,1,2,1,0)),
		equity_est_chg_type_id = (
			select min(est_chg_type_Id) from estimate_charge_type
				where afudc_flag in ('E')
				and funding_charge_indicator = decode(:i_funding_wo_indicator,1,1,2,1,0)),
		cpi_est_chg_type_id = (
			select min(est_chg_type_Id) from estimate_charge_type
				where afudc_flag in ('P')
				and funding_charge_indicator = decode(:i_funding_wo_indicator,1,1,2,1,0))
		
	; 
	if uf_check_sql('Setting Est Charge Type (BI)') <> 1 then return -1
else
	update budget_afudc_calc_temp a
	set debt_est_chg_type_id = (
 		select decode(a.funding_wo_indicator,0,e.est_chg_type_id,e.funding_chg_type)
		from afudc_control ac, wo_est_hierarchy_map ct, estimate_charge_type e, work_order_account woa
		where ac.debt_cost_element = ct.cost_element_id
		    and ct.est_chg_type_id = e.est_chg_type_id
		    and a.afudc_type_id = ac.afudc_type_id
			and a.work_order_id = woa.work_order_id(+)
			and nvl(woa.wo_est_hierarchy_id,1) = ct.wo_est_hierarchy_id),
	equity_est_chg_type_id = (
 		select decode(a.funding_wo_indicator,0,e.est_chg_type_id,e.funding_chg_type)
		from afudc_control ac, wo_est_hierarchy_map ct, estimate_charge_type e, work_order_account woa
		where ac.equity_cost_element = ct.cost_element_id
		   and ct.est_chg_type_id = e.est_chg_type_id
		   and a.afudc_type_id = ac.afudc_type_id
			and a.work_order_id = woa.work_order_id(+)
			and NVL(woa.wo_est_hierarchy_id,1) = ct.wo_est_hierarchy_id),
	cpi_est_chg_type_id = (
 		select decode(a.funding_wo_indicator,0,e.est_chg_type_id,e.funding_chg_type)
		from afudc_control ac, wo_est_hierarchy_map ct, estimate_charge_type e, work_order_account woa
		where ac.cpi_cost_element = ct.cost_element_id
		   and ct.est_chg_type_id = e.est_chg_type_id
		   and a.afudc_type_id = ac.afudc_type_id
			and a.work_order_id = woa.work_order_id(+)
			and NVL(woa.wo_est_hierarchy_id,1) = ct.wo_est_hierarchy_id)
	
	; 
	if uf_check_sql('Setting Est Charge Type (FP\WO)') <> 1 then return -1

end if


select count(*)
into :cpi_est_check
from budget_afudc_calc_temp
where cpi_est_chg_type_id is null and nvl(cpi,0) <> 0
;


select count(*)
into :debt_est_check
from budget_afudc_calc_temp
where debt_est_chg_type_id is null and nvl(afudc_debt,0) <> 0
;

select count(*)
into :equity_est_check
from budget_afudc_calc_temp
where equity_est_chg_type_id is null and nvl(afudc_equity,0) <> 0
;

if isnull(cpi_est_check) then cpi_est_check = 0
if isnull(debt_est_check) then debt_est_check = 0
if isnull(equity_est_check) then equity_est_check = 0

if cpi_est_check > 0 then
	uf_msg('The CPI Cost Element is not mapped to an estimate charge type please updating your mappings and recalculate','E')
	est_error = true
end if

if debt_est_check > 0 then
	uf_msg('The AFUDC Debt Cost Element is not mapped to an estimate charge type please updating your mappings and recalculate','E')
	est_error = true
end if

if equity_est_check > 0 then
	uf_msg('The AFUDC Equity Cost Element is not mapped to an estimate charge type please updating your mappings and recalculate','E')
	est_error = true
end if

if est_error then
	return -1
end if








	//AFUDC DEBT
sqls_debt = ' '
sqls_debt += ' insert into '+i_table_name
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_debt += ' (work_order_id, '+pk_col_name+', '+revision+', '
else
	sqls_debt += ' (budget_id, '+pk_col_name+', '+revision+', '
end if
sqls_debt += ' year , '
sqls_debt += ' expenditure_type_id , '
sqls_debt += ' est_chg_type_id , '
sqls_debt += ' january , '
sqls_debt += ' february , '
sqls_debt += ' march, '
sqls_debt += ' april , '
sqls_debt += ' may, '
sqls_debt += ' june, '
sqls_debt += ' july , '
sqls_debt += ' august , '
sqls_debt += ' september , '
sqls_debt += ' october , '
sqls_debt += ' november, '
sqls_debt += ' december, '
sqls_debt += ' total , '
sqls_debt += ' future_dollars , '
sqls_debt += ' hist_actuals  '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_debt += ' ,wo_work_order_id '
end if
sqls_debt += ' )  '
sqls_debt += '  (select work_order_id,min(est_monthly_id_debt),revision, '
sqls_debt += ' substr(to_char(trans.month_number),1,4) year , '
sqls_debt += ' expenditure_type_id , '
sqls_debt += ' debt_est_chg_type_id , '
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'01',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'02',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'03',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'04',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'05',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'06',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'07',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'08',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'09',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'10',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'11',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'12',decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt),0)),0), "
sqls_debt += " nvl(sum(decode(debt_est_chg_type_id,equity_est_chg_type_id,afudc_debt + afudc_equity, afudc_debt)),0), "
sqls_debt += ' 0 future_dollars, '
sqls_debt += ' 0 hist_actuals '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_debt += ',decode(wo_work_order_id,-99,null,wo_work_order_id) '
end if
sqls_debt += ' from budget_afudc_calc_temp trans '
sqls_debt += ' where expenditure_type_id = 1 '
sqls_debt += '  and exists (select 1 from budget_afudc_calc_temp b where b.work_order_id = trans.work_order_id and b.revision = trans.revision and (b.afudc_debt <> 0 or (b.equity_est_chg_type_id = b.debt_est_chg_type_id and b.afudc_equity <> 0)) ) '
/*cjr 20100107 added above so that if no AFUDC calculated zero dollar rows wouldn't be returned have to do it this way so that all years will be return if any month of the project is non-zero
For example if the project has AFUDC in 2009 but not in 2010 we need 2010 row so that WO_EST_MONTHLY will be whole but if no dollars in either year we don't need the results*/
sqls_debt += ' group by work_order_id, revision,  '
sqls_debt += ' substr(to_char(trans.month_number),1,4) , '
sqls_debt += ' expenditure_type_id , '
sqls_debt += ' debt_est_chg_type_id  '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_debt += ',decode(wo_work_order_id,-99,null,wo_work_order_id) '
end if
sqls_debt += ' ) '
execute  immediate :sqls_debt;
if uf_check_sql(' Inserting AFUDC Debt Results') <> 1 then return -1


//AFUDC equity
sqls_equity = ' '
sqls_equity += ' insert into '+i_table_name
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_equity += ' (work_order_id, '+pk_col_name+', '+revision+', '
else
	sqls_equity += ' (budget_id, '+pk_col_name+', '+revision+', '
end if
sqls_equity += ' year , '
sqls_equity += ' expenditure_type_id , '
sqls_equity += ' est_chg_type_id , '
sqls_equity += ' january , '
sqls_equity += ' february , '
sqls_equity += ' march, '
sqls_equity += ' april , '
sqls_equity += ' may, '
sqls_equity += ' june, '
sqls_equity+= ' july , '
sqls_equity += ' august , '
sqls_equity += ' september , '
sqls_equity += ' october , '
sqls_equity += ' november, '
sqls_equity += ' december, '
sqls_equity += ' total , '
sqls_equity += ' future_dollars , '
sqls_equity += ' hist_actuals  '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_equity += ' ,wo_work_order_id '
end if
sqls_equity += ' )  '
sqls_equity += '  (select work_order_id ,min(est_monthly_id_equity),revision, '
sqls_equity += ' substr(to_char(trans.month_number),1,4) year , '
sqls_equity += ' expenditure_type_id , '
sqls_equity += ' equity_est_chg_type_id , '
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'01',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'02',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'03',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'04',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'05',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'06',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'07',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'08',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'09',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'10',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'11',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'12',decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity),0)),0), "
sqls_equity += " nvl(sum(decode(debt_est_chg_type_id,equity_est_chg_type_id,0, afudc_equity)),0), "
sqls_equity += ' 0 future_dollars, '
sqls_equity += ' 0 hist_actuals '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_equity += ',decode(wo_work_order_id,-99,null,wo_work_order_id) '
end if
sqls_equity += ' from budget_afudc_calc_temp trans '
sqls_equity += ' where expenditure_type_id = 1 '
sqls_equity += '  and debt_est_chg_type_id <> equity_est_chg_type_id '
sqls_equity += '  and exists (select 1 from budget_afudc_calc_temp b where b.work_order_id = trans.work_order_id and b.revision = trans.revision and b.afudc_equity <> 0  ) '
/*cjr 20100107 added above so that if no AFUDC calculated zero dollar rows wouldn't be returned have to do it this way so that all years will be return if any month of the project is non-zero
For example if the project has AFUDC in 2009 but not in 2010 we need 2010 row so that WO_EST_MONTHLY will be whole but if no dollars in either year we don't need the results*/
sqls_equity += ' group by work_order_id, revision,  '
sqls_equity += ' substr(to_char(trans.month_number),1,4) , '
sqls_equity += ' expenditure_type_id , '
sqls_equity += ' equity_est_chg_type_id  '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_equity += ',decode(wo_work_order_id,-99,null,wo_work_order_id) '
end if
sqls_equity += ' ) '
execute  immediate :sqls_equity;
if uf_check_sql(' Inserting AFUDC Equity Results') <> 1 then return -1

if i_return_cpi then
		//CPI
	sqls_cpi = ' '
	sqls_cpi += ' insert into '+i_table_name
	if i_table_name <> 'BUDGET_MONTHLY_DATA' then
		sqls_cpi += ' (work_order_id, '+pk_col_name+', '+revision+', '
	else
		sqls_cpi += ' (budget_id, '+pk_col_name+', '+revision+', '
	end if
	sqls_cpi += ' year , '
	sqls_cpi += ' expenditure_type_id , '
	sqls_cpi += ' est_chg_type_id , '
	sqls_cpi += ' january , '
	sqls_cpi += ' february , '
	sqls_cpi += ' march, '
	sqls_cpi += ' april , '
	sqls_cpi += ' may, '
	sqls_cpi += ' june, '
	sqls_cpi+= ' july , '
	sqls_cpi += ' august , '
	sqls_cpi += ' september , '
	sqls_cpi += ' october , '
	sqls_cpi += ' november, '
	sqls_cpi += ' december, '
	sqls_cpi += ' total , '
	sqls_cpi += ' future_dollars , '
	sqls_cpi += ' hist_actuals  '
	if i_table_name <> 'BUDGET_MONTHLY_DATA' then
		sqls_cpi += ' ,wo_work_order_id '
	end if
	sqls_cpi += ' )  '
	sqls_cpi += '  (select work_order_id ,min(est_monthly_id_cpi),revision, '
	sqls_cpi += ' substr(to_char(trans.month_number),1,4) year , '
	sqls_cpi += ' expenditure_type_id , '
	sqls_cpi += ' cpi_est_chg_type_id , '
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'01',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'02',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'03',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'04',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'05',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'06',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'07',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'08',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'09',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'10',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'11',cpi,0)),0), "
	sqls_cpi += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'12',cpi,0)),0), "
	sqls_cpi += " nvl(sum(cpi),0), "
	sqls_cpi += ' 0 future_dollars, '
	sqls_cpi += ' 0 hist_actuals '
	if i_table_name <> 'BUDGET_MONTHLY_DATA' then
		sqls_cpi += ',decode(wo_work_order_id,-99,null,wo_work_order_id) '
	end if
	sqls_cpi += ' from budget_afudc_calc_temp trans '
	sqls_cpi +='  where expenditure_type_id = 1 '
	sqls_cpi +='  and exists (select 1 from budget_afudc_calc_temp b where b.work_order_id = trans.work_order_id and b.revision = trans.revision and b.cpi <> 0  ) '
	sqls_cpi += ' group by work_order_id, revision, '
	sqls_cpi += ' substr(to_char(trans.month_number),1,4) , '
	sqls_cpi += ' expenditure_type_id , '
	sqls_cpi += ' cpi_est_chg_type_id  '
	if i_table_name <> 'BUDGET_MONTHLY_DATA' then
		sqls_cpi += ',decode(wo_work_order_id,-99,null,wo_work_order_id) '
	end if
	sqls_cpi += ' ) '
	execute  immediate :sqls_cpi;
	if uf_check_sql(' Inserting CPI Results') <> 1 then return -1
end if

if i_table_name <> 'WO_EST_MONTHLY_SUBS_DET' then
//	sqls2 = " "
//	sqls2 += ' insert into '+i_table_name
//	sqls2 += " (work_order_id, "+pk_col_name+", "+revision+", year, expenditure_type_id, est_chg_type_id, "
//	sqls2 += "	department_id, "+utility_account_col+", job_task_id,  january,february, march,april, "
//	sqls2 += "	may,june,july, august,september, october,november, december, total, long_description, "
//	sqls2 += "	hist_actuals, future_dollars "
//	if i_table_name = 'BUDGET_MONTLY_DATA' then
//		sqls2+= ' ,budget_id '
//	end if
//	sqls2 += " ) "
//	sqls2 += " select a.work_order_id, "+pk_col_name+", a."+revision+", b.year, min(a.expenditure_type_id), "
//	sqls2 += "	min(a.est_chg_type_id), min(a.department_id), min(a."+utility_account_col+"), min(a.job_task_id), 0, 0, "
//	sqls2 += "	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, min(a.long_description), 0, 0 "
//	if i_table_name = 'BUDGET_MONTLY_DATA' then
//		sqls2+= ' ,a.budget_id '
//	end if
//	sqls2 += " from "+i_table_name+" a, pp_table_years b "
//	sqls2 += " where b.year >= ( "
//	sqls2 += "	select to_number(to_char(c."+start_date_col+",'yyyy')) "
//	sqls2 += "	from "+date_table+" c "
//	sqls2 += "	where c."+id_col+" = a."+id_col
//	sqls2 += "     and c."+revision+" = a."+revision
//	sqls2 += "	) "
//	sqls2 += " and b.year <= ( "
//	sqls2 += "	select to_number(to_char(d."+end_date_col+",'yyyy')) "
//	sqls2 += "	from "+date_table+" d "
//	sqls2 += "	where d."+id_col+" = a."+id_col
//	sqls2 += "       and d."+revision+" = a."+revision
//	sqls2 += "	) "
//	sqls2 += " and exists ( "
//	sqls2 += "	select 1 "
//	sqls2 += "	from wo_est_processing_temp q "
//	sqls2 += "	where q.work_order_id = a."+id_col
//	sqls2 += "		and q.revision = a."+revision
//	sqls2 += "	) "
//	sqls2 += " and not exists ( "
//	sqls2 += "	select 1 from "+i_table_name+" e "
//	sqls2 += "	where a."+id_col+" = e."+id_col
//	sqls2 += "	and a."+revision+" = e."+revision
//	sqls2 += "	and a."+pk_col_name+" = e."+pk_col_name
//	sqls2 += "	and b.year = e.year "
//	sqls2 +="	) "
//	sqls2 +=" group by "+id_col+", "+pk_col_name+", "+revision+", b.year, a.work_order_id "
//	execute immediate :sqls2;
//	if uf_check_sql('Inserting Missing Combinations') <> 1 then return -1
//	
//	
//
///###9265 no longer needed
//sqls3 = ' '
//sqls3 += ' insert into '+spread_table 
//sqls3 += '	( '+spread_field+' ) '
//sqls3 += ' select est_monthly_id_debt '
//sqls3 += '	from budget_afudc_calc_temp a '
//sqls3 += '  where afudc_debt <> 0 '
//sqls3 +='  Union '
//sqls3 += ' select est_monthly_id_equity '
//sqls3 += '	from budget_afudc_calc_temp a '
//sqls3 += '  where afudc_equity <> 0 '
//sqls3 += '      and debt_est_chg_type_id <> equity_est_chg_type_id '
//if i_return_cpi then
//	sqls3 +='  Union '
//	sqls3 += ' select est_monthly_id_cpi '
//	sqls3 += '	from budget_afudc_calc_temp a '
//	sqls3 += '  where cpi <> 0 '
//end if
//execute immediate :sqls3;
//if uf_check_sql('Inserting into spread factor table for new entries ') <> 1 then return -1
	
end if

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	update budget_monthly_data
	set jan_local = nvl(january,0),
		feb_local = nvl(february,0),
		mar_local = nvL(march,0),
		apr_local = nvl(april,0),
		may_local = nvl(may,0),
		jun_local = nvl(june,0),
		jul_local = nvl(july,0),
		aug_local = nvl(august,0),
		sep_local = nvl(september,0),
		oct_local = nvl(october,0),
		nov_local = nvL(november,0),
		dec_local = nvl(december,0),
		tot_local = nvl(total,0)
	where (budget_monthly_id) in (
		select est_monthly_id_debt
		from budget_afudc_calc_temp
		where afudc_debt <> 0
			union
		select est_monthly_id_equity
		from budget_afudc_calc_temp
		where debt_est_chg_type_id <> equity_est_chg_type_id
		   and afudc_debt <> 0
			union
		select est_monthly_id_cpi
		from budget_afudc_calc_temp
		where cpi <> 0
	)
	;
	if sqlca.sqlcode < 0 then
		uf_msg('Could not insert into '+i_table_name+'_SPREAD for new monthly entries. '+sqlca.sqlerrtext,'E')
		rollback;
		return -1
	end if	
	
end if

i_status_position =95

return 1
end function

public function longlong uf_transpose ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_transpose() 
//
//	  Purpose  :	This function pulls the base data from the table being processed for work roders in wo_est_processing_temp.  The insert also returns any actual AFUDC that was calculated
//		             
//
//   Arguments:		
//	
//   Returns :   1 if successfuly -1 if a failure occurs
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 


delete from budget_afudc_calc_temp
;
if uf_check_sql('Cleaning out budget afudc calc temp') < 0 then
	return -1
end if



uf_msg('Pulling in Base for AFUDC base','I')

choose case i_table_name
	case 'WO_EST_MONTHLY' 
		///The inservice date field gets filled in with the complete date for Removal and Salvage.  This date essentially marks the last date this type of cost should close
		///So this field is the inservice date additiions and complete date for removal and salvage.  Note closing option also is considered when talking about closings
		///from this SQL I took out the reference to late charge waiting period when using the estimate complete date
		///For funding projects that have a status of complete or better we use the actual complete date and then add the late charge waiting period to it.  
		///20100712 - changed the between below to always use the est complete date even if an actual complete date exists.  
//***************************************************************************************** 

		insert into budget_afudc_calc_temp (work_order_id, revision, expenditure_type_id, month_number, amount, amount_afudc, amount_cpi, afudc_type_id, closing_pattern_id, 
			 closing_option_id,eligible_for_afudc, eligible_for_cpi, afudc_stop_date, afudc_start_date,in_service_date,afudc_debt, afudc_equity, cpi,funding_wo_indicator, retirements, ciac,
			 orig_eligible_for_afudc, orig_eligible_for_cpi, actuals_mn_proj, actuals_mn_bv, budget_version_id, orig_est_start_date, orig_est_in_service_date, orig_est_complete_date,
			 cwip_start_month, wo_work_order_id)
		select a.work_order_id, b.revision, decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id) expenditure_type_id, 
			b.year*100 + p.month_num month_number, 
			sum(	decode(e.processing_type_id,1,0,5,0,decode(nvl(e.afudc_flag,'Q'),'E',0,'D',0,'P',0,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december))) * nvl(e.percent_book_basis,1)) amount,  
			sum(		decode(e.processing_type_id,1,0,5,0,decode(nvl(e.afudc_flag,'Q'),'E',0,'D',0,'P',0,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december))) * nvl(e.percent_book_basis,1) * nvl(e.afudc_eligible_indicator,1)) amount_afudc, 
			sum(		decode(e.processing_type_id,1,0,decode(nvl(e.afudc_flag,'Q'),'E',0,'D',0,'P',0,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december))) *nvl( e.percent_tax_basis,1) * nvl(e.afudc_eligible_indicator,1)) amount_cpi, 
		c.afudc_type_id, d.closing_pattern_id, c.closing_option_id,/*CJR changed this code to pull closing pattern from work order approval*/
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,c.eligible_for_afudc,0), 
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,c.eligible_for_cpi,0),
		c.afudc_stop_date, c.afudc_start_date,
			case 
			when a.wo_status_id < 4 then
					decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(d.est_in_service_date,d.est_complete_date),d.est_complete_date)	
			when a.wo_status_id = 4 then
					decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(nvl(a.in_service_date,d.est_in_service_date),d.est_complete_date),d.est_complete_date)
			 when a.wo_status_id > 4 then
					decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(nvl(a.in_service_date,d.est_in_service_date),d.est_complete_date),add_months(nvl(a.completion_date,d.est_complete_date),nvl(a.late_chg_wait_period,0)))
				end,
sum(	decode(nvl(e.afudc_flag,'Q'),'D',decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0) * e.percent_book_basis) afudc_debt,  
			sum(		decode(nvl(e.afudc_flag,'Q'),'E',decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0) * e.percent_book_basis ) afudc_equity, 
			sum(		decode(nvl(e.afudc_flag,'Q'),'P',decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0) * e.percent_tax_basis ) amount_cpi, a.funding_wo_indicator,
			sum(		decode(b.expenditure_type_id,2,decode(e.processing_type_id,1,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0),0)) retirements,
			sum(		decode(e.processing_type_id,3,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0)) ciac,
				c.eligible_for_afudc, c.eligible_for_cpi, d.actuals_month_number, (select actuals_month from budget_version where budget_version_id = :i_budget_version), :i_budget_version,
				d.est_start_date, nvl(d.est_in_service_date,d.est_complete_date), d.est_complete_date,
				:i_cwip_start_month, nvl(b.wo_work_order_id,-99)
			
			from pp_table_months p, work_order_control a, wo_est_monthly b, work_order_account c, estimate_charge_type e, work_order_approval d, wo_est_processing_temp t
			where t.work_order_id = a.work_order_id
				and t.work_order_id = b.work_order_id
				and t.revision = b.revision
				and t.work_order_id = c.work_order_id
				and t.work_order_id = d.work_order_id
				and t.revision= d.revision
				and b.est_chg_type_id = e.est_chg_type_id
				and nvl(e.afudc_flag,'Q') not in ('X','W','C','R','V')
				and e.processing_type_id not in  (/*1,*/9,10)
				and b.expenditure_type_id in (1,2)
				and to_date(to_char(b.year*100 + p.month_num),'YYYYMM') between trunc(d.est_start_date,'month') and trunc(add_months(d.est_complete_date,nvl(a.late_chg_wait_period,0)),'month')
			group by a.work_order_id, b.revision, decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),
			b.year*100 + p.month_num,p.month_num,d.est_start_date, d.est_complete_date,
			case 
			when a.wo_status_id < 4 then
					decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(d.est_in_service_date,d.est_complete_date),d.est_complete_date)	
			when a.wo_status_id = 4 then
					decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(nvl(a.in_service_date,d.est_in_service_date),d.est_complete_date),d.est_complete_date)
			 when a.wo_status_id > 4 then
					decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(nvl(a.in_service_date,d.est_in_service_date),d.est_complete_date),add_months(nvl(a.completion_date,d.est_complete_date),nvl(a.late_chg_wait_period,0)))
			end,
			c.afudc_type_id, d.closing_pattern_id, c.closing_option_id,
			decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,c.eligible_for_afudc,0), 
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,c.eligible_for_cpi,0), c.afudc_stop_date, c.afudc_start_date, a.funding_wo_indicator,
		c.eligible_for_afudc, c.eligible_for_cpi, d.actuals_month_number,
		d.est_start_date, nvl(d.est_in_service_date,d.est_complete_date), d.est_complete_date, b.wo_work_order_id
				;
				if uf_check_sql('Transposing wo est monthly') <> 1 then return -1
				if i_cap_interest then
					insert into budget_afudc_calc_temp(work_order_id, revision, expenditure_type_id, month_number, amount, amount_afudc, amount_cpi, 
					afudc_type_id, closing_pattern_id, closing_option_id,eligible_for_afudc, eligible_for_cpi, afudc_stop_date, afudc_start_date,in_service_date,afudc_debt, afudc_equity, cpi,funding_wo_indicator, retirements, ciac,
					orig_eligible_for_afudc, orig_eligible_for_cpi, actuals_mn_proj, actuals_mn_bv, budget_version_id, orig_est_start_date, orig_est_in_service_date, orig_est_complete_date,
					cwip_start_month, wo_work_order_id)
					select temp.work_order_id, temp.revision, 1, b.month_number, 0, 0, 0, woa.afudc_type_id, woaa.closing_pattern_id, woa.closing_option_id, 
					woa.eligible_for_afudc,	woa.eligible_for_cpi, woa.afudc_stop_date, woa.afudc_start_date, nvl(woaa.est_in_service_date, woaa.est_complete_date), 0, 0, 0, woc.funding_wo_indicator, 0, 0,
					woa.eligible_for_afudc, woa.eligible_for_cpi, woaa.actuals_month_number, (select actuals_month from budget_version where budget_version_id = :i_budget_version), :i_budget_version,
					woaa.est_start_date, nvl(woaa.est_in_service_date,woaa.est_complete_date), woaa.est_complete_date, :i_cwip_start_month, b.wo_work_order_id
					from work_order_control woc, wo_est_processing_temp temp, work_order_account woa, work_order_approval woaa, 
						( select ciw.parent_wo work_order_id, b.month_number, nvl(b.wo_work_order_id,-99) wo_work_order_id
						  from budget_afudc_calc_temp b, budget_cap_interest_wo ciw
						  where b.work_order_id = ciw.work_order_id
							and b.expenditure_type_id = 1
						  minus
						 select work_order_id, month_number, nvl(wo_work_order_id,-99) from budget_afudc_calc_temp
						 where expenditure_type_id = 1
						) b
					where woc.work_order_id = temp.work_order_id
					  and woa.work_order_id = temp.work_order_id
						and woaa.work_order_id = temp.work_order_id
					  and woaa.revision = temp.revision
					  and b.work_order_id = temp.work_order_id
					;
					if uf_check_sql('Transposing wo est monthly (cap interest)') <> 1 then return -1
				end if
				
	update budget_afudc_calc_temp a
	set (eligible_for_afudc, eligible_for_cpi, afudc_stop_date, afudc_start_date, in_service_date, afudc_type_id, closing_option_id, closing_pattern_id) = (
		select woa.eligible_for_afudc, woa.eligible_for_cpi,woa.afudc_stop_date,woa.afudc_start_date, least(nvl(nvl(woc.in_service_date, woc.est_in_service_date), a.in_service_date), a.in_service_date),
			woa.afudc_type_id, woa.closing_option_id, null
		from work_order_account woa, work_order_control woc
		where a.wo_work_order_id = woa.work_order_id
		and woc.work_order_id = woa.work_order_id)
	where exists (
		select 1
		from work_order_account woa
		where woa.work_order_id = a.wo_work_order_id)
	;
	if uf_check_sql('backfilling work order id attributes') <> 1 then return -1
	case 'BUDGET_MONTHLY_DATA'
		///See note above for now I'm leaving in the reference to late charge waiting period here?  I'm torn I may come back and take it out here as well and have it follow the note above.
		insert into budget_afudc_calc_temp (work_order_id, revision, expenditure_type_id,  month_number, amount, amount_afudc, amount_cpi, afudc_type_id, closing_pattern_id, 
			 closing_option_id,eligible_for_afudc, eligible_for_cpi, afudc_stop_date, afudc_start_date,in_service_date,afudc_debt, afudc_equity, cpi,funding_wo_indicator,retirements, ciac,
			 orig_eligible_for_afudc, orig_eligible_for_cpi, actuals_mn_proj, actuals_mn_bv, budget_version_id, orig_est_start_date, orig_est_in_service_date, orig_est_complete_date,
			 cwip_start_month, wo_work_order_id )
		select a.budget_id, b.budget_version_id, decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id) expenditure_type_id, b.year*100 + p.month_num month_number, 
			sum(	decode(e.processing_type_id,1,0,5,0,decode(nvl(e.afudc_flag,'Q'),'E',0,'D',0,'P',0,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december))) * nvl(e.percent_book_basis,1)) amount,  
			sum(		decode(e.processing_type_id,1,0,5,0,decode(nvl(e.afudc_flag,'Q'),'E',0,'D',0,'P',0,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december))) * nvl(e.percent_book_basis,1) * nvl(e.afudc_eligible_indicator,1)) amount_afudc, 
			sum(		decode(e.processing_type_id,1,0,decode(nvl(e.afudc_flag,'Q'),'E',0,'D',0,'P',0,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december))) *nvl( e.percent_tax_basis,1) * nvl(e.afudc_eligible_indicator,1)) amount_cpi, 
		a.afudc_type_id, c.closing_pattern_id, c.closing_option_id,
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,a.eligible_for_afudc,0), 
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,a.eligible_for_cpi,0),null, null,
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(c.in_service_date,c.close_date),add_months(c.close_date,nvl(c.late_chg_wait_period,0))),
sum(	decode(nvl(e.afudc_flag,'Q'),'D',decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0) * e.percent_book_basis) afudc_debt,  
			sum(		decode(nvl(e.afudc_flag,'Q'),'E',decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0) * e.percent_book_basis ) afudc_equity, 
			sum(		decode(nvl(e.afudc_flag,'Q'),'P',decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0) * e.percent_tax_basis ) amount_cpi, 2,
			sum(		decode(b.expenditure_type_id,2,decode(e.processing_type_id,1,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0),0)) retirements,
			sum(		decode(e.processing_type_id,3,decode(p.month_num,1,b.JANUARY , 
									  2,b.february, 
									  3,b.march, 
									  4,b.april, 
									  5,b.may, 
									  6,b.june, 
									  7,b.july, 
									  8,b.august, 
									  9,b.september, 
									  10,b.october, 
									  11,b.november, 
									  12,b.december),0)) ciac,
				a.eligible_for_afudc, a.eligible_for_cpi, bv.actuals_month, (select actuals_month from budget_version where budget_version_id = :i_budget_version), :i_budget_version,
				c.start_date, nvl(c.in_service_date,c.close_date), c.close_date,
				:i_cwip_start_month, -99
			
			from pp_table_months p, budget a, budget_monthly_data b, budget_amounts c, estimate_charge_type e, wo_est_processing_temp t, budget_version bv
			where t.work_order_id = a.budget_id
 				and t.work_order_id = b.budget_id
				and t.revision = b.budget_version_id
				and t.work_order_id = c.budget_id
				and t.revision = c.budget_version_id
				and to_date(to_char(b.year*100 + p.month_num),'YYYYMM') between trunc(c.start_date,'month') and trunc(add_months(c.close_date,nvl(c.late_chg_wait_period,0)),'month')
				and b.est_chg_type_id = e.est_chg_type_id
				and e.processing_type_id not in  (/*1,*/9,10)
				and nvl(e.afudc_flag,'Q') not in ('X','W','C','R','V')
				and b.expenditure_type_id in (1,2)
				and t.revision = bv.budget_version_id
			group by a.budget_id, b.budget_version_id, decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id) ,  
			b.year*100 + p.month_num , 
			a.afudc_type_id, c.closing_pattern_id, c.closing_option_id,
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,a.eligible_for_afudc,0), 
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,a.eligible_for_cpi,0),
		decode(decode(e.processing_type_id,2,6,7,6,b.expenditure_type_id),1,nvl(c.in_service_date,c.close_date),add_months(c.close_date,nvl(c.late_chg_wait_period,0))),
		a.eligible_for_afudc, a.eligible_for_cpi, bv.actuals_month,
		c.start_date, nvl(c.in_service_date,c.close_date), c.close_date
			
				;
				if uf_check_sql('Transposing budget_monthly_data ') <> 1 then return -1
				
			if i_cap_interest then
					insert into budget_afudc_calc_temp(work_order_id, revision, expenditure_type_id, month_number, amount, amount_afudc, amount_cpi, 
					afudc_type_id, closing_pattern_id, closing_option_id,eligible_for_afudc, eligible_for_cpi, afudc_stop_date, afudc_start_date,in_service_date,afudc_debt, afudc_equity, cpi,funding_wo_indicator, retirements, ciac,
					orig_eligible_for_afudc, orig_eligible_for_cpi, actuals_mn_proj, actuals_mn_bv, budget_version_id, orig_est_start_date, orig_est_in_service_date, orig_est_complete_date,
					cwip_start_month)
					select temp.work_order_id, temp.revision, 1, b.month_number, 0, 0, 0, bud.afudc_type_id, ba.closing_pattern_id, ba.closing_option_id, 
					bud.eligible_for_afudc,	bud.eligible_for_cpi, null afudc_stop_date,  null afudc_start_date, nvl(ba.in_service_date, ba.close_date), 0, 0, 0, 2, 0, 0,
					bud.eligible_for_afudc, bud.eligible_for_cpi, bv.actuals_month, (select actuals_month from budget_version where budget_version_id = :i_budget_version), :i_budget_version,
					ba.start_date, nvl(ba.in_service_date,ba.close_date), ba.close_date,
					:i_cwip_start_month
					from budget bud, wo_est_processing_temp temp, budget_amounts ba, budget_version bv,
						( select ciw.parent_wo work_order_id, b.month_number
						  from budget_afudc_calc_temp b, budget_cap_interest_wo ciw
						  where b.work_order_id = ciw.work_order_id
							and b.expenditure_type_id = 1
						  minus
						 select work_order_id, month_number from budget_afudc_calc_temp
						) b
					where bud.budget_id = temp.work_order_id
					  and ba.budget_id = temp.work_order_id
					  and ba.budget_version_id = temp.revision
					  and b.work_order_id = temp.work_order_id
					  and bv.budget_version_id = temp.revision
					;
					if uf_check_sql('Transposing budget_monthly_data (cap interest)') <> 1 then return -1
			end if
	case 'WO_EST_MONTHLY_SUBS_DET'
end choose

sqlca.analyze_table('BUDGET_AFUDC_CALC_TEMP')
if uf_check_sql('Analyze Table Budget AFUDC CALC TEMP (Pre-backfill)') <> 1 then return -1

insert into budget_afudc_calc_temp (work_order_id, revision, expenditure_type_id, month_number, amount, amount_afudc, amount_cpi, afudc_type_id, closing_pattern_id, 
closing_option_id,eligible_for_afudc, eligible_for_cpi, afudc_stop_date, afudc_start_date,in_service_date,afudc_debt, afudc_equity, cpi,funding_wo_indicator, retirements, ciac,
orig_eligible_for_afudc, orig_eligible_for_cpi, actuals_mn_proj, actuals_mn_bv, budget_version_id, orig_est_start_date, orig_est_in_service_date, orig_est_complete_date,
cwip_start_month, wo_work_order_id)
SELECT
t.work_order_id, t.revision, expenditure_type_id, p.month_number, 0 amount, 0 amount_afudc, 0 amount_cpi, Max(afudc_type_id), Max(t.closing_pattern_id), Max(closing_option_id), Max(eligible_for_afudc),
Max(eligible_for_cpi), Max(afudc_stop_date), Max(afudc_start_date), Max(t.in_service_date), 0 afudc_debt, 0 afudc_equity, 0 cpi, Max(t.funding_wo_indicator), 0 retirements, 0 ciac,
Max(orig_eligible_for_afudc), Max(orig_eligible_for_cpi), Max(actuals_mn_proj), Max(actuals_mn_bv), Max(t.budget_version_id), Max(orig_est_start_date), Max(orig_est_in_service_date),
Max(orig_est_complete_date), Max(cwip_start_month), wo_work_order_id
FROM budget_afudc_calc_temp t, pp_calendar p, work_order_approval woa
WHERE t.work_order_id = woa.work_order_id
AND t.revision = woa.revision
AND to_date(p.month_number,'yyyymm') BETWEEN trunc(woa.est_start_date,'month') AND trunc(woa.est_complete_date,'month')
AND not exists (
SELECT 1
FROM budget_afudc_calc_temp temp
WHERE temp.work_order_id = t.work_order_id
AND temp.revision = t.revision
AND temp.expenditure_type_id = t.expenditure_type_id
AND temp.wo_work_order_id = t.wo_work_order_id
AND p.month_number = temp.month_number)
GROUP BY t.work_order_id, t.revision, expenditure_type_id, p.month_number, wo_work_order_id
;

if uf_check_sql('Backfill missing months') <> 1 then return -1

sqlca.analyze_table('BUDGET_AFUDC_CALC_TEMP')
if uf_check_sql('Analyze Table Budget AFUDC CALC TEMP') <> 1 then return -1
	

return 1
end function

public subroutine uf_msg (string a_msg, string a_error);

//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_msg() 
//
//	  Purpose  :	returns messages to the user based on boolean variables 
//		             
//
//   Arguments:	a_msg  : string message to return to the user
//					a_error : string that tells what type of message
//								E = Error Message
//								W = Warning Message
//								I = Information Message
//	
//   Returns :   none
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_title, time_string

if i_add_time_to_messages then
	time_string = string(now())
	a_msg = time_string + ' ' + a_msg
end if

message_title = 'Budget AFUDC Calculation'

if i_status_box then
	f_status_box(message_title,a_msg)
end if


if i_pp_msgs then
	f_pp_msgs(a_msg)
end if

if i_progress_bar  then
	f_progressbar(message_title,a_msg,100,i_status_position)
end if

if i_statusbox_onerror and a_error = 'E' then
	f_status_box(message_title,a_msg)
end if

if i_messagebox_onerror and a_error = 'E' then
	messagebox(message_title,a_msg)
end if




end subroutine

public function longlong uf_check_sql (string a_msg);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_check_sql() 
//
//	  Purpose  :	This function checks the sqlca.sqlcode if less then one it displays an error message if greater then or equal to one and success messageing is turned on it displays a sucess message
//		             
//
//   Arguments:		a_msg - message to be displayed along with error or success
//	
//   Returns :   1 if no sql error -1 if there is an sql error
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_string

message_string = a_msg
if i_add_time_to_messages then
	message_string = message_string+string(now())
end if

if sqlca.sqlcode < 0 then
	uf_msg('SQL ERROR: '+message_string,'E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
else
	if  i_display_msg_on_sql_success then
		if i_display_rowcount then
			message_string = message_string+' : processed '+string(sqlca.sqlnrows)+' rows '
		end if

		uf_msg(message_string,'I')
	end if		
	return 1
end if
end function

public function longlong uf_read (string a_table_name, longlong a_actuals_month_number);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_read() 
//
//	  Purpose  :	Driving function for preforming Budget AFUDC & CPI Calcs  the process does the following
///							1. Transpose data from i_table_name for work orders/budget items revisions/budget_versions in the wo_est_processing_temp table into the temp table budget_afudc_calc_temp- uf_transpose
///							2. Updates some intial variables and begining balances
///							3. Loops over the months and calculates Closings (tax and book) and AFUDC Debt, Equity and CPI
///							4. Deletes existing results stored in i_table for Afudc Debt, Afudc equity, and CPI
///							5. Returns the results of this calculation to i_table_name
///							6. Inserts the calculated results into the permanent table budget_afudc_calc
///
///					There are other functions contained in this user object to return closings and cwip balance but that are to be called individually
//		             
//
//   Arguments:		a_table_name: string table these overheads are ran against (WO_EST_MONTHLY, BUDGET_MONTHLY_DATA, WO_EST_MONTHLY_SUBS_DE
//						a_actuals_month_number: YYYYMM format that contains the starting point for the AFUDC calc
//	
//   Returns :   1 if successfuly -1 if a failure occurs
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
longlong rtn
uo_budget_revision uo_revisions

rtn = uf_pre_process(a_table_name, a_actuals_month_number)
if rtn < 0 then
	rollback;
	return -1
end if

rtn = uf_calc()
if rtn < 0 then
	rollback;
	return -1
end if

rtn = uf_delete_afudc()
if rtn < 0 then
	rollback;
	return -1
end if

rtn = uf_return_results()
if rtn < 0 then
	rollback;
	return -1
end if

rtn = uf_post_process()
if rtn < 0 then
	rollback;
	return -1
end if

commit;

return 1

end function

public function longlong uf_start_month ();longlong start_month

longlong woa_start_month, woa_act_month, bv_start_month, bv_act_month, check
longlong woa_end_month, bv_end_month, last_closed_month

setnull(woa_start_month)
setnull(woa_act_month)
setnull(bv_start_month)
setnull(bv_act_month)
///First lets get the actuals month if it wasn't passed in it will be null at this point if it wasn't passed in
///I'm intentionally not using the i_budget_Version variable because this is set when calculating AFUDC for an entire budget version
///When this is done an actuals month is always set. so none of this logic will fire
/*- Funding Projects
		- max work order approval actauls month number
		- if above is null then max actuals month number on any budget version the funding project is hooked to
		- if above is null then the min estimate start date - 1 month 
 - Budget Items
		- min actuals month number on any budget version that the funding project revisions are in
		- min Dec of year prior to start year on any budget versions that the funding project revisons are in
 - Work Orders
 		- max w
 		- min est_start_month - 1
*/

if i_funding_wo_indicator <> 2 then///For funding projects and work orders get the woa 
	select min(to_number(to_char(est_start_date, 'yyyymm'))),
		decode(max(nvl(woa.actuals_month_number,0)),0,min(nvl(to_number(to_char(add_months(est_start_date,-1), 'yyyymm')),0)),max(nvl(woa.actuals_month_number,0)))
	into :woa_start_month, :woa_act_month
	from work_order_approval woa, wo_est_processing_temp t
	where woa.work_order_id = t.work_order_id
	and woa.revision =t.revision
	;
	if uf_check_sql('Selecting actuals month from Work Order Approval') <> 1 then return -1
else //for budget items get the data from budget_amounts
	select min(to_number(to_char(nvl(start_date,sysdate), 'yyyymm'))),
			0
	into :woa_start_month, :woa_act_month
	from budget_amounts woa, wo_est_processing_temp t
	where woa.budget_id = t.work_order_id
	and woa.budget_version_id =t.revision
	;
	if uf_check_sql('Selecting actuals month from Work Order Approval') <> 1 then return -1	
end if

if i_budget_version <> 0 then
	select min(start_year*100+1),
			max(actuals_month)
	into :bv_start_month, :bv_act_month
	from budget_version bv
	where bv.budget_version_id = :i_budget_version
	 ;
	 if uf_check_sql('Selecting actuals month from Budget version') <> 1 then return -1
else
	if i_funding_wo_indicator = 1  then	
		select min(start_year*100+1),
				max(actuals_month)
		into :bv_start_month, :bv_act_month
		from budget_version bv
		where exists (
			select 1
			from budget_version_fund_proj bvfp, wo_est_processing_temp w
			where bvfp.budget_version_id = bv.budget_version_id
				and bvfp.work_order_id = w.work_order_id
				and bvfp.revision = w.revision
				and bvfp.active = 1)
		 ;
		 if uf_check_sql('Selecting actuals month from Budget version') <> 1 then return -1
	elseif i_funding_wo_indicator = 0 then
		bv_start_month = 0
		bv_act_month = 0
	else 
		select min(start_year*100+1),
				max(actuals_month)
		into :bv_start_month, :bv_act_month
		from budget_version bv, wo_est_processing_temp t
		where bv.budget_version_id = t.revision
		 ;
		 if uf_check_sql('Selecting actuals month from Budget version') <> 1 then return -1
	end if
end if



if isnull(woa_start_month) then woa_start_month = 0
if isnull(woa_act_month) then woa_act_month = 0
if isnull(bv_start_month) then bv_start_month = 0
if isnull(bv_act_month) then bv_act_month = 0

choose case i_funding_wo_indicator
	case 1/*Funding Projects*/
		///For actuals pick the greater of the budget version and work order approval actuals months
		if  woa_act_month > bv_act_month then
			i_actuals_month_number = woa_act_month
		else
			i_actuals_month_number = bv_act_month
		end if	
		
		if isnull(i_actuals_month_number) then
			i_actuals_month_number = 0
		end if
		
		///Cwip start month should come from the budget version otherwise just set it to the actuals month
		 if bv_start_month > 190001 then
			i_cwip_start_month = bv_start_month
		else
			i_cwip_start_month = i_actuals_month_number
		end if					
		
		///if the cwip start_month > actuals month set it to the actuals month
		if i_cwip_start_month > i_actuals_month_number then
			i_cwip_start_month = i_actuals_month_number
		end if
		
	case 0 /*Work Orders*/
		///For work orders the actuals month and start month should be the same
		if woa_act_month > 190001 then
			i_cwip_start_month = woa_act_month
			i_actuals_month_number = woa_act_month
		else
			i_cwip_start_month = woa_start_month
			i_actuals_month_number = woa_start_month
		end if
					
	case 2 /*Budget Items*/
		i_actuals_month_number = bv_act_month
		i_cwip_start_month = bv_start_month
		
		if i_cwip_start_month > i_actuals_month_number then
			i_cwip_start_month = i_actuals_month_number
		end if
		
end choose
		
	
		
if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then 
	uf_msg('ERROR finding actuals month for i_funding_wo_indicator = '+string(i_funding_wo_indicator),'E')
	return -1
end if

if isnull(i_cwip_start_month) then
	i_cwip_start_month = i_actuals_month_number
end if

///Now the calc start month should be just one month after actuals month
if mid(string(i_actuals_month_number),5,2) = '12' then
	i_start_month_number = i_actuals_month_number + 89
else
	i_start_month_number = i_actuals_month_number + 1
end if


//###CJR this function was modified based on maint 5798
///This code makes sure that the cwip start month isn't greater then the last closed month.
///The new AFUDC code for performance reasons builds an archive table of begining balances so it doesn't have to go to cwip charge each time
///If this archive table is empty the calc will automatically build it for the cwip start month
/// but if that cwip start month is greater then last closed month the archive table will get built incorrectly 
///so lets check and if that is the case then back the cwip start month back to the min max closed month for all companies
//represented in this run of the AFUDC calculation

//KSY: Maint 7852 Only get date for active companies (status_code_id = 1)

if i_funding_wo_indicator = 1 or i_funding_wo_indicator = 0 then
	select min(month)
	into :last_closed_month
	from wo_est_processing_temp w, work_order_control woc, 
		(select w.company_id, to_number(to_char(max(w.accounting_month),'YYYYMM')) month
		from wo_process_control w, company 
		where w.powerplant_closed is not null
			and w.company_id = company.company_id
			and company.status_code_id = 1
		group by w.company_id) wp
	where w.work_order_id = woc.work_order_id
	and woc.company_id = wp.company_id
	and wp.month < :i_cwip_start_month
	;
	if uf_check_sql('Selecting max closed month from wo_process_control (FP\WO)') <> 1 then return -1
else
	select min(month)
	into :last_closed_month
	from wo_est_processing_temp w, budget woc, 
		(select w.company_id, to_number(to_char(max(w.accounting_month),'YYYYMM')) month
		from wo_process_control w, company 
		where w.powerplant_closed is not null
			and w.company_id = company.company_id
			and company.status_code_id = 1
		group by w.company_id) wp
	where w.work_order_id = woc.budget_id
	and woc.company_id = wp.company_id
	and wp.month < :i_cwip_start_month
	;
	if uf_check_sql('Selecting max closed month from wo_process_control (BI)') <> 1 then return -1
end if

if last_closed_month < i_cwip_start_month  then
	uf_msg('Warning the start year of this process '+string(i_cwip_start_month)+' is greater then the current closed month in the system', 'I')
	uf_msg('Defaulting start month to last closed month in system','I')
	i_cwip_start_month = last_closed_month
end if
		

//if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then
//	Choose case i_funding_wo_indicator
//	case 1
//		select min(actuals_month), min(start_year * 100 + 1)
//		into :i_actuals_month_number, :i_cwip_start_month
//		from budget_version bv
//		where exists (
//			select 1
//			from budget_version_fund_proj bvfp, wo_est_processing_temp w
//			where bvfp.budget_version_id = bv.budget_version_id
//				and bvfp.work_order_id = w.work_order_id
//				and bvfp.revision = w.revision
//				and bvfp.active = 1)
//		 ;
//		if uf_check_sql('Selecting actuals month from budget version') <> 1 then return -1
//		if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then
//			select ((min(start_year) - 1) * 100)+12
//			into :i_actuals_month_number
//			from budget_version bv
//			where exists (
//				select 1
//				from budget_version_fund_proj bvfp, wo_est_processing_temp w
//				where bvfp.budget_version_id = bv.budget_version_id
//					and bvfp.work_order_id = w.work_order_id
//					and bvfp.revision = w.revision
//					and bvfp.active = 1)
//			 ;
//			 if uf_check_sql('Selecting start month from budget version') <> 1 then return -1
//		end if
//	case 2
//		select min(actuals_month), start_year * 100 + 1
//		into :i_actuals_month_number, :i_cwip_start_month
//		from budget_version bv
//		where exists (
//			select 1
//			from wo_est_processing_temp w
//			where w.revision = bv.budget_version_id)
//		 ;
//		if uf_check_sql('Selecting actuals month from budget version') <> 1 then return -1
//		if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then
//			select ((min(start_year) - 1) * 100)+12
//			into :i_actuals_month_number
//			from budget_version bv
//			where exists (
//				select 1
//				from wo_est_processing_temp w
//				where w.revision = bv.budget_version_id)
//			;
//			 if uf_check_sql('Selecting start month from budget version') <> 1 then return -1
//		end if
//	end choose
//	
//	if i_funding_wo_indicator <> 2 then
//		if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then 
//			select add_months(min(est_start_date),-1)
//			into :i_actuals_month_number
//			from work_order_approval woa
//			where exists (
//					select 1
//					from  wo_est_processing_temp w
//					where woa.work_order_id = w.work_order_id
//						and woa.revision = w.revision)
//			;
//		end if
//	end if
//end if//if actuals month is null or < 190001



	

///Now lets find the CWIP start month.  THis is how far back we will calculate CWIP balances prior to the actuals month.  
//
//if i_budget_version <> 0 then 
//	select start_year * 100 + 1
//	into :i_cwip_start_month
//	from budget_version
//	where budget_version_id = :i_budget_version
//	;
//end if

///CJR moved above
//if  isnull(start_month) and i_funding_wo_indicator <> 0 then 
//	if i_table_name = 'WO_EST_MONTHLY'  then/*FUnding Projects*/
//		select min(start_year * 100 + 1)
//		into :start_month
//		from budget_version b
//		where exists (
//			select 1
//			from budget_version_fund_proj bvfp, wo_est_processing_temp w
//			where bvfp.budget_version_id = b.budget_version_id
//				and bvfp.work_order_id = w.work_order_id
//				and bvfp.revision = w.revision)
//		;
//	else/*Budget ITems*/
//		select min(start_year * 100 + 1)
//		into :start_month
//		from budget_version b
//		where exists (
//			select 1
//			from  wo_est_processing_temp w
//			where w.revision = b.budget_version_id)
//		;
//	end if
//end if
	


//if isnull(i_cwip_start_month) or i_cwip_start_month > i_actuals_month_number or i_cwip_start_month < 190001 then 
//	start_month = i_actuals_month_number
//end if


return 1
end function

public function longlong uf_cap_interest_setup ();boolean new_parents = false, new_children = false
longlong cap_count
//
//
//
//
//
//
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	
	insert into wo_est_processing_temp(work_order_id, revision, substitution_id)
	select b.parent_wo, a.revision, -1
	from wo_est_processing_temp a, BUDGET_CAP_INTEREST_WO b
	where a.work_order_id = b.work_order_id
	minus
	select work_order_id, revision, -1
	from wo_est_processing_temp
	;
	if uf_check_sql('Adding Missing Parent Work Orders '+i_table_name) <> 1 then
		return -1
	elseif sqlca.sqlnrows > 0 then
		new_parents = true
		uf_msg('!!!!!!Warning!!!!!!','I')
		uf_msg(' Added Cap Interest Parent Budget Items because child budget items were being processed','I')
		uf_msg(' Parent Budget Items have been added to this budget version ','I')
	end if

	
elseif i_table_name = 'WO_EST_MONTHLY' then
	
	insert into wo_est_processing_temp(work_order_id, revision, substitution_id)
	select b.parent_wo, bvfp.budget_version_id, -1
	from wo_est_processing_temp a, BUDGET_CAP_INTEREST_WO b, budget_version_fund_proj bvfp, budget_version_fund_proj child_bvfp
	where a.work_order_id = b.work_order_id
	and b.parent_wo = bvfp.work_order_id 
	and :i_budget_version <> 0
	minus
	select work_order_id, revision, -1
	from wo_est_processing_temp
	;
	if uf_check_sql('Adding Missing Parent Work Orders '+i_table_name) <> 1 then
		return -1
	elseif sqlca.sqlnrows > 0 then
		new_parents = true
		uf_msg('!!!!!!Warning!!!!!!','I')
		uf_msg(' Added Cap Interest Parent Funding Projects because child work orders were being processed','I')
		uf_msg(' Parent Funding Projects have been added to this budget version ','I')
	end if
	
	
end if

//
//if new_parents then
//	if i_table_name = 'WO_EST_MONTHLY' then
//		insert into work_order_approval (
//			work_order_id, revision, approval_type_id,budget_version_id, rejected,	notes, actuals_month_number, first_approver,
//			revision_description, revision_long_description, budget_review_type_id,
//			est_start_date, est_complete_date, est_in_service_date,
//			approval_status_id, review_status )
//		select b.work_order_id, nvl((select max(revision)+1 from work_order_approval woa where woa.work_order_id = b.work_order_id),1), a.approval_type_id,
//			null budget_version_id, 2 rejected,
//			null notes, a.actuals_month_number, null,
//			null revision_description, null revision_long_description, a.budget_review_type_id,
//			a.est_start_date, a.est_complete_date, a.est_in_service_date,
//			1 approval_status_id, null review_status
//		from work_order_approval a, wo_est_processing_temp b, budget_cap_interest_wo cap
//		where a.work_order_id = cap.work_order_id
//		and cap.parent_wo = b.work_order_id
//		and b.revision = -99999
//		;
//		if uf_check_sql('Adding Missing Parent Work Orders to work order approval'+i_table_name) <> 1 then
//			return -1
//		end if
//		
//		update wo_est_processing_temp a
//		set revision = (select bvfp.revision from budget_version_fund_proj bvfp where bvfp.budget_version_id = :i_budget_version_id and bvfp.work_order_id = a.work_order_id)
//		where
//	elseif i_table_name = 'BUDGET_MONTHLY_DATA' then
//		insert into budget_amounts(budget_id, budget_version_id, start_date, close_date, budget_total_dollars, curr_year_budget_dollars, closing_pattern_id, in_service_date, closing_option_id, late_chg_wait_period,
//		base_year, escalation_id)
//		select b.work_order_id, b.revision, a.start_date, a.close_date, 0, 0, a.closing_pattern_id, a.in_service_date, a.closing_option_id, a.late_chg_wait_period, a.base_year, a.escalation_id
//		from budget_amounts a, wo_est_processing_temp b, budget_cap_interest_wo cap
//			where a.budget_id = cap.work_order_id
//			and cap.parent_wo = b.work_order_id
//			and (b.work_order_id, b.revision) in (select b.work_order_id, b.revision from wo_est_processing_temp
//															minus
//															select budget_id, budget_version_id from budget_amounts)
//		;
//		if uf_check_sql('Adding Missing Parent Budget Items to Budget Amounts'+i_table_name) <> 1 then
//			return -1
//		end if
//	end if
//end if
	
		
		


return 1


end function

public function longlong uf_cwip_balances ();/*******************************************************************
 * uf_cwip_balances()
 *
 * Update the beg_cwip, closed_current_month, end_cwip from start_month to actuals month
 *
 * Author: Bryan Barnes
 * 
 * Initial Version 1.0 Written 2009-09-01
 *
 * Logic: Update the first cwip_bal for the project, revision for start_month
 *        Update the closed_current_month for all months between start and actuals
 *        Foot the end_balance and beg_balance
 *******************************************************************/

string rollup_column, sqls, cv, ret
longlong i, wo_work_order_id_check, check_by_wo

//###CJR this function was modified based on maint 5798
choose case i_funding_wo_indicator
	case 0 
		rollup_column = 'WORK_ORDER_ID'
	case 1
		rollup_column = 'FUNDING_WO_ID'
	case 2
		rollup_column = 'BUDGET_ID'
end choose


cv = lower(trim(f_pp_system_control_company('Budget AFUDC Calc Fast',-1)))

if isnull(cv) or cv = "" then cv = 'slow'

//###CJR this function was modified based on maint 5798
if cv = 'fast' or cv = 'fast closings' then
	update wo_est_processing_temp a
	set substitution_id = 1
	where exists (
		select 1
		from (
			select work_order_id, month_number, expenditure_type_id, wo_work_order_id
			from budget_afudc_calc_temp 
			where month_number = :i_cwip_start_month
			minus
			select work_order_id, month_number, expenditure_type_id, wo_work_order_id
			from budget_afudc_calc_beg_bal 
			) b
		where a.work_order_id = b.work_order_id
	)
	;
	if uf_check_sql('Checking for missing balance records '+string(i_cwip_start_month)) <> 1 then return -1
		
	if sqlca.sqlnrows > 0 then
		select count(*)
		into :check_by_wo
		from budget_afudc_calc_temp bac, wo_est_processing_temp w
		where bac.work_order_id = w.work_order_id
		and bac.wo_work_order_id <> -99
		and w.substitution_id = 1
		;
		if uf_check_sql('Checking for by wo') <> 1 then return -1
		if check_by_wo > 0 then
			check_by_wo = 1
		else
			check_by_wo = 0
		end if
		ret = f_budget_build_cwip(i_funding_wo_indicator,i_cwip_start_month,i_cwip_start_month,1, check_by_wo)
		if ret <> 'OK' then
			uf_msg(ret,'E')
			return -1
		end if
	end if
		
	if i_funding_wo_indicator = 1 then
			//Update everything in budget afudc calc temp that has a real wo_work_order_id in it
		update budget_afudc_calc_temp a
		set (beg_cwip,   beg_cwip_afudc, beg_cwip_cpi, cpi_balance) = (
			select beg_cwip,   beg_cwip_afudc, beg_cwip_cpi, cpi_balance
			from budget_afudc_calc_beg_bal b
			where a.work_order_id = b.work_order_id
				and a.month_number = b.month_number
				and a.expenditure_type_id = b.expenditure_type_id
				and a.wo_work_order_id = b.wo_work_order_id
				and a.month_number = :i_cwip_start_month
				and b.wo_work_order_id <> -99)
		where month_number = :i_cwip_start_month
		and a.wo_work_order_id <> -99
		;
		if uf_check_sql('Updating Beg balances wo_work_order_id (FAST) for start month of '+string(i_cwip_start_month)) <> 1 then return -1
		
		///Now update everything that doesn't have  a wo work order id.  The trick is that the table budget_afudc_calc_beg_bal could be by work order or it could have -99
		///need to sum up everything that has work order order in budget_afudc_calc_beg_bal but wasn't estimated by that work order therefore wo_work_order_id
		///wouldn't be filled in on budget_afudc_calc_temp
		update budget_afudc_calc_temp a
		set (beg_cwip,   beg_cwip_afudc, beg_cwip_cpi, cpi_balance) = (
			select sum(beg_cwip),   sum(beg_cwip_afudc), sum(beg_cwip_cpi), sum(cpi_balance)
			from budget_afudc_calc_beg_bal b
			where a.work_order_id = b.work_order_id
				and a.month_number = b.month_number
				and a.expenditure_type_id = b.expenditure_type_id
				and  not exists (select 1 from budget_afudc_calc_temp c where b.wo_work_order_id = decode(c.wo_work_order_id,-99,-999,c.wo_work_order_id))
				///so the above looks wierd but I don't want to pull any rows from budget_afudc_calc_beg_bal that were updated in the first statement.  The problem is that 
				//budget_afudc_calc_beg_bal could also have -99 rows but I do want those rows because they would not have gone through the first update
				and a.month_number = :i_cwip_start_month )
		where month_number = :i_cwip_start_month
		and a.wo_work_order_id = -99
		;
		if uf_check_sql('Updating Beg balances non wo_work_order_id (FAST) for start month of '+string(i_cwip_start_month)) <> 1 then return -1
	else
		update budget_afudc_calc_temp a
		set (beg_cwip,   beg_cwip_afudc, beg_cwip_cpi, cpi_balance) = (
			select beg_cwip,   beg_cwip_afudc, beg_cwip_cpi, cpi_balance
			from budget_afudc_calc_beg_bal b
			where a.work_order_id = b.work_order_id
				and a.month_number = b.month_number
				and a.expenditure_type_id = b.expenditure_type_id
				and a.wo_work_order_id = b.wo_work_order_id
				and a.month_number = :i_cwip_start_month )
		where month_number = :i_cwip_start_month
		;
		if uf_check_sql('Updating Beg balances non wo_work_order_id (FAST) for start month of '+string(i_cwip_start_month)) <> 1 then return -1
	end if
	

else
	
	if i_funding_wo_indicator = 1 then
		select count(*)
		into :wo_work_order_id_check
		from budget_afudc_calc_temp
		where wo_work_order_id <> -99
		;
		if uf_check_sql('Checking for wo work order ids') <> 1 then return -1
	end if
	
	if wo_work_order_id_check <> 0 then
		uf_msg('AFUDC by work order id must use the fast begining balance process please change the system control Budget AFUDC Calc Fast to FAST','E')
		return -1
	end if
		
	///Update Budget AFUDC Calc set the beg cwip balance for the i_cwip_start_month row
	/*### - MDZ - 33999 - 20131111*/
	/*Added links to work_order_account and afudc_control and the decode for cost element in the afudc eligible indicator.  For ACTUALS the cost element
		should be set as not eligible for AFUDC, but budget uses them and expects them to be eligible for AFUDC.  Therefore, the decode forces them to be included.  
		If a client does not compound it should appropriately reverse it out after the fact in the calc.*/
	sqls = &
	"UPDATE budget_afudc_calc_temp upd SET (beg_cwip,   beg_cwip_afudc, beg_cwip_cpi, cpi_balance) =	 " + &
	 "(   " + & 
	"	SELECT SUM(decode(a.processing_type_id,5,0,a.amount) * a.effective_percent_basis) as begin_cwip,   " + & 
	"			    SUM(decode(a.afudc_elig_indicator,1,decode(a.processing_type_id,5,0,a.amount),0) * a.effective_percent_basis) as beg_cwip_afudc,   " + & 
	"			    SUM(decode(a.afudc_elig_indicator,1,amount,0) * a.effective_percent_tax_basis) as beg_cwip_cpi,  " + & 
	"			    SUM(decode(a.cpi_indicator,1,amount,0)) as cpi_balance "+&
	"	FROM   " + & 
	"	(   " + & 
		"SELECT  WOC." + rollup_column + "  ,    " + & 
			"cwip.amount,   " + & 
			"1 as effective_percent_basis,   " + & 
			"1 as effective_percent_tax_basis,  " + & 
			" decode(cwip.cost_element_id,ac.equity_cost_element,1,ac.debt_cost_element,1,ctd.afudc_elig_indicator) afudc_elig_indicator,  " + & 
			"decode(ct.processing_type_id,2,6,7,6,cwip.expenditure_type_id) expenditure_type_id,  " + & 
			"ct.processing_type_id , " + & 
		  " decode(nvl((select count(*) from afudc_control ac where cwip.cost_element_id = ac.cpi_cost_element),0),0,0,1) cpi_indicator "+&
		 "FROM cwip_charge cwip, charge_type_data ctd, (select charge_type_id, max(effective_date) effective_date from charge_type_data group by charge_type_id) ctdd, charge_type ct,   " + & 
			"wo_est_processing_temp temp, work_order_control woc   " + & 
			" ,work_order_account woa, afudc_control ac " + &
		 "WHERE cwip.work_order_id = woc.work_order_id   " + & 
			"AND temp.work_order_id = WOC." + rollup_column + "     " + & 
			"AND cwip.month_number < " + string(i_cwip_start_month) + "   " + & 
			"AND cwip.charge_type_id = ct.charge_type_id   " + & 
			"AND ctd.charge_type_id =  ct.charge_type_id  " + & 
			"AND ctd.effective_date = ctdd.effective_date   " + & 
	"	 and ctdd.charge_type_id = ctd.charge_type_id " + & 
	 "	 AND cwip.expenditure_type_id in (1,2)   " + & 
	 "	 AND ct.processing_type_id not in (1,9,10)  " + & 
	"	 AND nvl(cwip.closed_month_number,999999) >=  " + string(i_cwip_start_month) + " " + & 
		" and woc.work_order_id = woa.work_order_id " + &
		" and woa.afudc_type_id = ac.afudc_type_id " + &
	"	) a   " + & 
	"	WHERE a." + rollup_column + "   = upd.work_order_id   " + & 
			  "and a.expenditure_type_id = upd.expenditure_type_id  " + & 
	"	GROUP BY a."+rollup_column+", a.expenditure_type_id     " + & 
	") 	 " + & 
	 "WHERE month_number = " + string(i_cwip_start_month) + "  " 
 
	 sqls = f_sql_add_hint(sqls,'uo_budget_afudc.uf_cwip_balances_begbal'+string(i_funding_wo_indicator))
	
	EXECUTE IMMEDIATE :sqls;
	if uf_check_sql('Updating Beg balances for start month of '+string(i_cwip_start_month)) <> 1 then return -1
end if


update budget_afudc_calc_temp
set beg_cwip = nvl(beg_cwip,0),
	beg_cwip_afudc = nvl(beg_cwip_afudc,0),
	beg_cwip_cpi = nvl(beg_cwip_cpi,0),
	cpi_balance = nvl(cpi_balance,0)
where month_number = :i_cwip_start_month
;
if uf_check_sql('Updating Beg balances (nulls to zero) for start month of '+string(i_cwip_start_month)) <> 1 then return -1
//###CJR this function was modified based on maint 5798
if cv = 'fast closings' then
	///### Maint 9265 CJR added loop around this logic so it will do one month at a time.  
	for i = i_cwip_start_month to i_actuals_month_number
		if long(mid(string(i),5,2)) > 12 then
			i = (long(mid(string(i),1,4)) + 1)*100 + 1
		end if
		update wo_est_processing_temp
		set substitution_id = 0
		where substitution_id = 1
		;
		if uf_check_sql('Restting substitution id ') <> 1 then return -1
		
		update wo_est_processing_temp a
		set substitution_id = 1
		where exists (
			select 1
			from (
				select work_order_id, month_number, expenditure_type_id
				from budget_afudc_calc_temp 
				where month_number = :i
				minus
				select work_order_id, month_number, expenditure_type_id
				from budget_afudc_calc_closing
				) b
			where a.work_order_id = b.work_order_id
		)
		;
		if uf_check_sql('Checking for missing closings records '+string(i)) <> 1 then return -1
			
		if sqlca.sqlnrows > 0 then
			select count(*)
			into :check_by_wo
			from budget_afudc_calc_temp bac, wo_est_processing_temp w
			where bac.work_order_id = w.work_order_id
			and bac.wo_work_order_id <> -99
			and w.substitution_id = 1
			;
			if uf_check_sql('Checking for by wo') <> 1 then return -1
			if check_by_wo >0 then
				check_by_wo = 1
			else 
				check_by_wo = 0
			end if
			ret = f_budget_build_cwip(i_funding_wo_indicator,i,i,2,check_by_wo)
			if ret <> 'OK' then
				uf_msg(ret,'E')
				return -1
			end if
		end if
			

	next
	
	if i_funding_wo_indicator = 1 then
			//Update everything in budget afudc calc temp that has a real wo_work_order_id in it
		update budget_afudc_calc_temp a
		set (current_month_closings,   current_month_closings_afudc, current_month_closings_cpi, cpi_closings) = (
			select current_month_closings,   current_month_closings_afudc, current_month_closings_cpi, cpi_closings
			from budget_afudc_calc_closing b
			where a.work_order_id = b.work_order_id
				and a.month_number = b.month_number
				and a.expenditure_type_id = b.expenditure_type_id
				and a.wo_work_order_id = b.wo_work_order_id
				and a.month_number between :i_cwip_start_month and :i_actuals_month_number
				and b.wo_work_order_id <> -99)
		where month_number between :i_cwip_start_month and :i_actuals_month_number
		and a.wo_work_order_id <> -99
		;
		if uf_check_sql('Updating Beg balances wo_work_order_id (FAST) for start month of '+string(i_cwip_start_month)) <> 1 then return -1
		
		///Now update everything that doesn't have  a wo work order id.  The trick is that the table budget_afudc_calc_beg_bal could be by work order or it could have -99
		///need to sum up everything that has work order order in budget_afudc_calc_beg_bal but wasn't estimated by that work order therefore wo_work_order_id
		///wouldn't be filled in on budget_afudc_calc_temp
		update budget_afudc_calc_temp a
		set (current_month_closings,   current_month_closings_afudc, current_month_closings_cpi, cpi_closings) = (
			select sum(current_month_closings),   sum(current_month_closings_afudc), sum(current_month_closings_cpi), sum(cpi_closings)
			from budget_afudc_calc_closing b
			where a.work_order_id = b.work_order_id
				and a.month_number = b.month_number
				and a.expenditure_type_id = b.expenditure_type_id
				and  not exists (select 1 from budget_afudc_calc_temp c where b.wo_work_order_id = decode(c.wo_work_order_id,-99,-999,c.wo_work_order_id))
				///so the above looks wierd but I don't want to pull any rows from budget_afudc_calc_beg_bal that were updated in the first statement.  The problem is that 
				//budget_afudc_calc_beg_bal could also have -99 rows but I do want those rows because they would not have gone through the first update
				and a.month_number between :i_cwip_start_month and :i_actuals_month_number )
		where  a.month_number between :i_cwip_start_month and :i_actuals_month_number
		and a.wo_work_order_id = -99
		;
		if uf_check_sql('Updating Beg balances non wo_work_order_id (FAST) for start month of '+string(i_cwip_start_month)) <> 1 then return -1
	else
		update budget_afudc_calc_temp a
		set (current_month_closings,   current_month_closings_afudc, current_month_closings_cpi, cpi_closings) = (
			select current_month_closings,   current_month_closings_afudc, current_month_closings_cpi, cpi_closings
			from budget_afudc_calc_closing b
			where a.work_order_id = b.work_order_id
				and a.month_number = b.month_number
				and a.expenditure_type_id = b.expenditure_type_id
				and a.wo_work_order_id = b.wo_work_order_id
				and a.month_number between :i_cwip_start_month and :i_actuals_month_number)
		where month_number between :i_cwip_start_month and :i_actuals_month_number
		;
		if uf_check_sql('Updating Beg balances non wo_work_order_id (FAST) for start month of '+string(i_cwip_start_month)) <> 1 then return -1
	end if
		
else
	///Don't need to do the wo_work_order_check again beacuse if it passed up there it would pass down here if it failed above then it exited the code
	
	//Update the closed_current_month
	sqls = &
	 "UPDATE budget_afudc_calc_temp upd SET (current_month_closings,   current_month_closings_afudc, current_month_closings_cpi, cpi_closings)=        " + & 
	  "(       " + & 
	"	 SELECT SUM(decode(a.processing_type_id,5,0,a.amount) * a.effective_percent_basis) as current_month_closings,       " + & 
	"			     SUM(decode(a.afudc_elig_indicator,1,decode(a.processing_type_id,5,0,a.amount),0) * a.effective_percent_basis) as current_month_closings_afudc,     " + & 
	"			     SUM(decode(a.afudc_elig_indicator,1,amount,0) * a.effective_percent_tax_basis) as current_month_closings_cpi ,    " + & 
	"				SUM(decode(a.cpi_indicator,1,amount,0)) as cpi_closings "+&
	"	 FROM       " + & 
	"	 (       " + & 
		 "SELECT WOC." + rollup_column + ",        " + & 
			 "cwip.amount,       " + & 
			 "1 as effective_percent_basis,       " + & 
			 "1 as effective_percent_tax_basis,     " + & 
	"	     ctd.afudc_elig_indicator,     " + & 
			 "decode(ct.processing_type_id,2,6,7,6,cwip.expenditure_type_id) expenditure_type_id,     " + & 
			 "ct.processing_type_id,     " + & 
			 "cwip.closed_month_number month_number,     " + & 
		  " decode(nvl((select count(*) from afudc_control ac where cwip.cost_element_id = ac.cpi_cost_element),0),0,0,1) cpi_indicator "+&
		  "FROM cwip_charge cwip, charge_type_data ctd, (select charge_type_id, max(effective_date) effective_date from charge_type_data group by charge_type_id) ctdd, charge_type ct,       " + & 
			 "wo_est_processing_temp temp, work_order_control woc       " + & 
		  "WHERE cwip.work_order_id = woc.work_order_id       " + & 
				 "AND temp.work_order_id = woc." + rollup_column + "       " + & 
				 "AND cwip.month_number <= "+string(i_actuals_month_number)+" " + & 
				 "AND cwip.charge_type_id = ct.charge_type_id       " + & 
				 "AND ctd.charge_type_id =  ct.charge_type_id     " + & 
				 "AND ctd.effective_date = ctdd.effective_date      " + & 
	"		and ctd.charge_type_id = ctdd.charge_type_id  " + & 
	"	       AND cwip.expenditure_type_id in (1,2)       " + & 
	"	       AND ct.processing_type_id not in (1,9,10)    " + & 
	"	     AND nvl(cwip.closed_month_number,999999) between "+ string(i_cwip_start_month) + " and "+string(i_actuals_month_number)+" " + & 
	"	 ) a       " + & 
	"	 WHERE a." + rollup_column + " = upd.work_order_id       " + & 
				"and a.expenditure_type_id = upd.expenditure_type_id     " + & 
	"		 and a.month_number = upd.month_number  " + & 
	"	 GROUP BY a." + rollup_column + " , a.month_number, a.expenditure_type_id " + & 
	 ") 	    " + & 
	  "WHERE month_number between "+ string(i_cwip_start_month) + " and "+string(i_actuals_month_number)
	  
	sqls =   f_sql_add_hint(sqls,'uo_budget_afudc.uf_cwip_balances_closings'+string(i_funding_wo_indicator))
	EXECUTE IMMEDIATE :sqls;
	if uf_check_sql('Updating Closing Amounts between start mont '+string(i_cwip_start_month)+' and '+string(i_actuals_month_number)) <> 1 then return -1
end if

update budget_afudc_calc_temp
set current_month_closings = nvl(current_month_closings,0),
	current_month_closings_afudc = nvl(current_month_closings_afudc,0),
	current_month_closings_cpi = nvl(current_month_closings_cpi,0),
	cpi_closings = nvl(cpi_closings,0)
where month_number between :i_cwip_start_month and :i_actuals_month_number
;
if uf_check_sql('Updating Closing Amounts (nulls to zero) between start mont '+string(i_cwip_start_month)+' and '+string(i_actuals_month_number)) <> 1 then return -1




///The process assumes that you have updated with actuals through the actuals month number and so 
////We don't need to do anything special to get the AFUDC, CPI, and other amounts
////Update the AFUDC, CPI amounts
//sqls = &
//"UPDATE budget_afudc_calc_temp upd SET (afudc_debt,   afudc_equity, cpi, amount, amount_afudc, amount_cpi)=  " + &
//"NVL(( " + &
//"	SELECT SUM(a.afudc_debt) as afudc_debt" + &
//"			    SUM(a.afudc_equity) as afudc_equity "+&
//"			    SUM(a.cpi) as cpi "+&
//"	FROM " + & 
//"	( " + &
//"   SELECT DISTINCT " + rollup_column + ", " + & 
//"      decode(cwip.cost_element,ac.debt_cost_element,cwip.amount,0) afudc_debt" + &
//"      decode(cwip.cost_element,ac.equity_cost_element,cwip.amount,0) afudc_equity " + &
//"      decode(cwip.cost_element,ac.cpi_cost_element,cwip.amount,0) cpi " + &
//"      decode(cwip.cost_element,ac.debt_cost_element,cwip.amount,0) afudc_debt" + &
//"      decode(cwip.cost_element,ac.equity_cost_element,cwip.amount,0) afudc_equity " + &
//"      decode(cwip.cost_element,ac.cpi_cost_element,cwip.amount,0) cpi " + &
//"		cwip.month_number "+&
//"    FROM cwip_charge cwip, wo_est_processing_temp temp, work_order_control woc,work_order_account woa, afudc_control ac " + &
//"    WHERE cwip.work_order_id = woc.work_order_id " + &
//"      AND temp.work_order_id = woc." + rollup_column + " " + &
//"      AND cwip.month_number < " + string(i_start_month_number) + " " + &
//" 	   AND cwip.expenditure_type_id =1 " + &
//"	) a " + &
//"	WHERE a." + rollup_column + " = upd.work_order_id " + &
//"        and 1 = upd.expenditure_type_id "+&
//"		and a.month_number = upd.month_number "+&
//"	GROUP BY a." + rollup_column + " " + &
//"), 0) 	"+&
//" WHERE month_number = " + string(i_cwip_start_month) + " "
//
//EXECUTE IMMEDIATE :sqls;
//IF sqlca.sqlcode < 0 THEN
//	MessageBox("Error", "SQL Error(uf_cwip_balances): " + sqlca.sqlerrtext + " for SQL: " + sqls)
//	RETURN -1
//END IF
///Update the beginning and ending balances for the range of dates
/*

UPDATE budget_afudc_calc_temp upd set (end_cwip, beg_cwip) = 
(
	SELECT FIRST_VALUE(a.beg_cwip) OVER(PARTITION BY a.work_order_id, a.revision ORDER BY a.month_number) +
  SUM(a.amount + a.afudc_debt + a.afudc_equity) OVER(PARTITION BY a.work_order_id, a.revision ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
  SUM(a.current_month_closings) OVER(PARTITION BY a.work_order_id, a.revision ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as end_balance,
  
  (
	  FIRST_VALUE(a.beg_cwip) OVER(PARTITION BY a.work_order_id, a.revision ORDER BY a.month_number) +
  SUM(a.amount) OVER(PARTITION BY a.work_order_id, a.revision ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
  SUM(a.current_month_closings) OVER(PARTITION BY a.work_order_id, a.revision ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
  )  - (a.amount - a.current_month_closings  + a.afudc_debt + a.afudc_equity) as beg_cwip	  	  
	FROM budget_afudc_calc_temp a
	WHERE EXISTS
	(
	
	  SELECT 1
	  FROM wo_est_processing_temp tmp
	  WHERE a.work_order_id = tmp.work_order_id
			AND a.revision = tmp.revision
	)
	AND a.month_number BETWEEN :i_cwip_start_month AND :i_actuals_month_number
	AND a.work_order_id = upd.work_order_id
	AND a.revision = upd.revision
	AND a.month_number = upd.month_number
) WHERE EXISTS
(
	SELECT 1
	FROM wo_est_processing_temp tmp
	WHERE upd.work_order_id = tmp.work_order_id
		AND upd.revision = tmp.revision
) AND month_number BETWEEN :i_cwip_start_month AND :i_actuals_month_number
;

*/

//sqlca.truncate_table('budget_afudc_foot_temp')
//if uf_check_sql('Clearing Temp Footing table ') <> 1 then return -1
//
//
//	INSERT INTO budget_afudc_foot_temp (work_order_id, revision, month_number, expenditure_type_id, end_cwip, beg_cwip,
// end_cwip_afudc, 
//beg_cwip_afudc, 
//end_cwip_cpi, beg_cwip_cpi,
//cpi_balance
//)
//	SELECT a.work_order_id, a.revision, a.month_number, a.expenditure_type_id,
//
// FIRST_VALUE(a.beg_cwip) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number) +
//  SUM(a.amount + a.afudc_debt + a.afudc_equity) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
//  SUM(a.current_month_closings) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as end_balance,
//  
//  (
//	  FIRST_VALUE(a.beg_cwip) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number) +
//	  SUM(a.amount) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
//	  SUM(a.current_month_closings) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
//	  SUM(a.afudc_debt + a.afudc_equity) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
//  )  - (a.amount - a.current_month_closings + a.afudc_debt + a.afudc_equity) as beg_cwip,
//
// FIRST_VALUE(a.beg_cwip_afudc) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number) +
//  SUM(a.amount_afudc + a.afudc_debt + a.afudc_equity) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
//  SUM(a.current_month_closings_afudc) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as end_balance_afudc,
//  
//  (
//	  FIRST_VALUE(a.beg_cwip_afudc) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number) +
//	  SUM(a.amount_afudc  ) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
//	  SUM(a.current_month_closings_afudc) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
//	  SUM(a.afudc_debt + a.afudc_equity) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
//  )  - (a.amount_afudc - a.current_month_closings + a.afudc_debt + a.afudc_equity) as beg_cwip_afudc,
//
// FIRST_VALUE(a.beg_cwip_cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number) +
//  SUM(a.amount_cpi + a.cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
//  SUM(a.current_month_closings_cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as end_balance_cpi,
//  
//  (
//	  FIRST_VALUE(a.beg_cwip_cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number) +
//	  SUM(a.amount_cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
//	  SUM(a.current_month_closings_cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
//	   SUM(a.cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
//  )  - (a.amount_cpi - a.current_month_closings_cpi + a.cpi) as beg_cwip_cpi,
//  
//  
//( FIRST_VALUE(a.cpi_balance) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number) +
//  SUM(a.cpi) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
//  SUM(a.cpi_closings) OVER(PARTITION BY a.work_order_id, a.revision, a.expenditure_type_id ORDER BY a.month_number RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
// ) - (a.cpi - a.cpi_closings) as cpi_balance
//  
//	FROM budget_afudc_calc_temp a
//	WHERE a.month_number BETWEEN :i_cwip_start_month AND :i_actuals_month_number
//;
//
//if uf_check_sql('Building begining and ending balances ') <> 1 then return -1
//
//
//
//
//UPDATE budget_afudc_calc_temp upd SET (end_cwip, beg_cwip, end_cwip_afudc, beg_cwip_afudc, end_cwip_cpi, beg_cwip_cpi, cpi_balance) =
//(
//	SELECT end_cwip, beg_cwip, end_cwip_afudc, beg_cwip_afudc, end_cwip_cpi, beg_cwip_cpi, cpi_balance
//	FROM budget_afudc_foot_temp a
//	WHERE a.work_order_id = upd.work_order_id
//		AND a.revision = upd.revision
//		AND a.month_number = upd.month_number
//		and a.expenditure_type_id = upd.expenditure_type_id
//)
//WHERE  upd.month_number BETWEEN :i_cwip_start_month AND :i_actuals_month_number;
//if uf_check_sql('Setting begining and ending balances ') <> 1 then return -1



 
RETURN 0
end function

public function longlong uf_closings_by_wo ();string sqls

uf_msg('Deleting existing closings by work order','I')
delete from wo_est_closings_wos wos
where exists (
	select 1 from wo_est_processing_temp temp
	where wos.funding_wo_id = temp.work_order_id
	and wos.revision = temp.revision
	);

if uf_check_sql('Deleting from wo_est_closings_wos') <> 1 then return -1


uf_msg('Inserting new closings by work order','I')
sqls = &
"insert into wo_est_closings_wos (~r~n"+&
"	funding_wo_id, revision, work_order_id, expenditure_type_id, month_number, wo_est_closings_amount, status )~r~n"+&
"select wemv.work_order_id,~r~n"+&
"	wemv.revision,~r~n"+&
"	wemv.wo_work_order_id,~r~n"+&
"	decode(ect.processing_type_id,2,6,7,6,wemv.expenditure_type_id) expenditure_type_id,~r~n"+&
"	nvl((~r~n"+&
"		case when wo.wo_status_id < 4 and wemv.expenditure_type_id = 1 and wemv.month_number <= nvl(to_number(to_char(nvl(wo.est_in_service_date, wo.est_complete_date),'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(nvl(wo.est_in_service_date, wo.est_complete_date),'yyyymm'))~r~n"+&
"		when wo.wo_status_id >= 4 and wemv.expenditure_type_id = 1 and wemv.month_number <= nvl(to_number(to_char(nvl(nvl(wo.in_service_date, wo.est_in_service_date), wo.est_complete_date),'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(nvl(nvl(wo.in_service_date, wo.est_in_service_date), wo.est_complete_date),'yyyymm'))~r~n"+&
"		when wo.wo_status_id <= 4 and wemv.expenditure_type_id = 2 and wemv.month_number <= nvl(to_number(to_char(wo.est_complete_date,'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(wo.est_complete_date,'yyyymm'))~r~n"+&
"		when wo.wo_status_id > 4 and wemv.expenditure_type_id = 2 and wemv.month_number <= nvl(to_number(to_char(add_months(nvl(wo.completion_date, wo.est_complete_date),nvl(wo.late_chg_wait_period,0)),'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(add_months(nvl(wo.completion_date, wo.est_complete_date),nvl(wo.late_chg_wait_period,0)),'yyyymm'))~r~n"+&
"		else~r~n"+&
"			wemv.month_number~r~n"+&
"		end~r~n"+&
"		),0) month_number,~r~n"+&
"	sum(wemv.amount) amount,~r~n"+&
"	2 status~r~n"+&
"from wo_est_processing_temp temp, wo_est_monthly_view wemv, estimate_charge_type ect, work_order_control wo~r~n"+&
"where temp.work_order_id = wemv.work_order_id~r~n"+&
"and temp.revision = wemv.revision~r~n"+&
"and wemv.wo_work_order_id = wo.work_order_id~r~n"+&
"and wemv.expenditure_type_id in (1,2)~r~n"+&
"and wemv.est_chg_type_id = ect.est_chg_type_id~r~n"+&
"and wemv.est_chg_type_id = ect.est_chg_type_id~r~n"+&
"and ect.processing_type_id not in (1,5,9,10)~r~n"+&
"group by wemv.work_order_id,~r~n"+&
"	wemv.revision,~r~n"+&
"	wemv.wo_work_order_id,~r~n"+&
"	decode(ect.processing_type_id,2,6,7,6,wemv.expenditure_type_id),~r~n"+&
"	nvl((~r~n"+&
"		case when wo.wo_status_id < 4 and wemv.expenditure_type_id = 1 and wemv.month_number <= nvl(to_number(to_char(nvl(wo.est_in_service_date, wo.est_complete_date),'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(nvl(wo.est_in_service_date, wo.est_complete_date),'yyyymm'))~r~n"+&
"		when wo.wo_status_id >= 4 and wemv.expenditure_type_id = 1 and wemv.month_number <= nvl(to_number(to_char(nvl(nvl(wo.in_service_date, wo.est_in_service_date), wo.est_complete_date),'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(nvl(nvl(wo.in_service_date, wo.est_in_service_date), wo.est_complete_date),'yyyymm'))~r~n"+&
"		when wo.wo_status_id <= 4 and wemv.expenditure_type_id = 2 and wemv.month_number <= nvl(to_number(to_char(wo.est_complete_date,'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(wo.est_complete_date,'yyyymm'))~r~n"+&
"		when wo.wo_status_id > 4 and wemv.expenditure_type_id = 2 and wemv.month_number <= nvl(to_number(to_char(add_months(nvl(wo.completion_date, wo.est_complete_date),nvl(wo.late_chg_wait_period,0)),'yyyymm')),999999) then~r~n"+&
"			to_number(to_char(add_months(nvl(wo.completion_date, wo.est_complete_date),nvl(wo.late_chg_wait_period,0)),'yyyymm'))~r~n"+&
"		else~r~n"+&
"			wemv.month_number~r~n"+&
"		end~r~n"+&
"		),0)~r~n"

sqls = f_sql_add_hint(sqls, 'uf_closings_by_wo_10')

execute immediate :sqls;

if uf_check_sql('Inserting into wo_est_closings_wos') <> 1 then return -1


uf_msg('Inserting summary closings records','I')
insert into wo_est_closings_wos (
	funding_wo_id, revision, work_order_id, month_number, expenditure_type_id, wo_est_closings_amount, status
	)
select wos.funding_wo_id,
	wos.revision,
	wos.work_order_id,
	wos.month_number,
	-1 expenditure_type_id,
	sum(wos.wo_est_closings_amount) wo_est_closings_amount,
	0 status
from wo_est_closings_wos wos, wo_est_processing_temp temp
where wos.funding_wo_id = temp.work_order_id
and wos.revision = temp.revision
and nvl(wos.wo_est_closings_amount,0) <> 0
group by wos.funding_wo_id, wos.revision,
	wos.work_order_id, wos.month_number;

if uf_check_sql('Inserting summary closings records') <> 1 then return -1


return 1
end function

public function longlong uf_pre_process (string a_table_name, longlong a_actuals_month_number);longlong rtn, cap_interest_count, check, wo_check
string ret

////NOTES
///current month closings are not reflected in current month afudc.  

///TO DO
////Update beg cwip code to pull in tax expensing amounts
///add system control to determine OCR sign



i_status_position = 0
i_table_name = upper(a_table_name)

/*20100105 CJR if a null or low actuals month is passed in set the actuals month to be 
	- if funding project/budget item min actuals month from budget version
	- if funding project/budget item and the above fails to find anything Dec of year prior to min start year of budget versions on the funding project revisions
	- if both of the above fail or processing work order then min est_start_date - 1 month 
if isnull(a_actuals_month_number) or a_actuals_month_number < 190001 then
	uf_msg('The actuals month number passed in is invalid','E')
	return -1
end if
*/
///CJR we are ignoring the actuals month number passed in and deriving it ourselves see uf_Start month code to see the logic
//i_actuals_month_number = a_actuals_month_number
//
//if i_actuals_month_number < 190001 then
//	setnull(i_actuals_month_number)
//end if

///This code is now in uf_start_month()
//if mid(string(i_actuals_month_number),5,2) = '12' then
//	i_start_month_number = i_actuals_month_number + 89
//else
//	i_start_month_number = i_actuals_month_number + 1
//end if
//###CJR this function was modified based on maint 5798
sqlca.analyze_table('WO_EST_PROCESSING_TEMP')

rtn = f_budget_afudc_custom(-1,i_table_name, i_actuals_month_number)
if rtn < 0 then return -1
if sqlca.f_client_extension_exists( 'f_client_budget_afudc' ) = 1 then
	rtn = sqlca.f_client_budget_afudc(-1,i_table_name, i_actuals_month_number)
	if rtn < 0 then return -1
end if
string args[]
args[1] = '-1'
args[2] = i_table_name
args[3] = string(i_actuals_month_number)
rtn = f_wo_validation_control(81,args)
if rtn < 0 then return -1



if i_table_name = 'BUDGET_MONTHLY_DATA' then
	i_funding_wo_indicator = 2
else
	select count(*)
	into :wo_check
	from work_order_control woc, wo_est_processing_temp t
	where woc.funding_wo_indicator = 0
	  and t.work_order_id = woc.work_order_id
	;
	
	if isnull(wo_check) then wo_check = 0
	if wo_check > 0 then
		i_funding_wo_indicator = 0
	end if
end if




ret = f_wo_est_processing_protected_revs(i_funding_wo_indicator) 
if ret <> 'OK' then
	uf_msg('ERROR Checking for locked revisions','E')
	uf_msg(ret,'E')
	return -1
end if


select count(*)
into :check
from wo_est_processing_temp
;

if isnull(check) then check = 0

if check = 0 then
	uf_msg('No projects to process all project revisions are either approved or locked ','I')
	return 1
end if

/*20100105 this function call has been moved to after i_funding_wo_indicator is set this function will now set cwip_start_month, calc_start_month, and actuals_month*/
rtn = uf_start_month()
if rtn < 0 then
	return -1
end if

uf_msg('Starting AFUDC calc for Table Name: '+i_table_name+' and actuals month number: '+string(i_actuals_month_number),'I')
select count(*)
into :cap_interest_count
from wo_est_processing_temp b
where exists (select 1 from budget_cap_interest_wo a where a.work_order_id = b.work_order_id)
;

if isnull(cap_interest_count) then cap_interest_count = 0

if cap_interest_count > 0 then
	i_cap_interest = true
else
	i_cap_interest = false
end if

if i_cap_interest then
	select count(*)
	into :check
	from (
		select parent_wo 
		from budget_cap_interest_wo a
		where exists (select 1 from wo_est_processing_temp w where w.work_order_id = a.work_order_id)
		minus
		select work_order_id
		from wo_est_processing_temp)
	;
	if uf_check_sql('Checking for Missing Parent Work Orders') <> 1 then return -1
	if isnull(check) then check = 0
	if check > 0 then
		uf_msg('Warning there are parent projects missing please add these projects to the budget version','E')
		return -1
	end if
end if

///Pull dollars from appropriate table (wo_est_monthly, budget_monthly_data, wo_est_monthly_subs_det)
rtn = uf_transpose()
if rtn < 0 then
	return -1
end if
i_status_position = 10
rtn = f_budget_afudc_custom(1,i_table_name, i_actuals_month_number)
if rtn < 0 then return -1
if sqlca.f_client_extension_exists( 'f_client_budget_afudc' ) = 1 then
	rtn = sqlca.f_client_budget_afudc(1,i_table_name, i_actuals_month_number)
	if rtn < 0 then return -1
end if
args[1] = '1'
args[2] = i_table_name
args[3] = string(i_actuals_month_number)
rtn = f_wo_validation_control(81,args)
if rtn < 0 then return -1

//if not i_dont_calc_afc then
//	///Update AFUDC from months greater then actuals month
	update budget_afudc_calc_temp
	set afudc_debt = nvl(afudc_debt,0), afudc_equity = nvl(afudc_equity,0), cpi = nvl(cpi,0)
	where month_number > :i_actuals_month_number
	;
	if uf_check_sql('Resetting AFUDC & CPI values for months after actuals month number') <> 1 then return -1
//end if
i_status_position = 11
update budget_afudc_calc_temp a
set beg_cwip =0,
	beg_cwip_afudc =0,
	beg_cwip_cpi =0,
     current_month_closings = 0,
     current_month_closings_afudc = 0,
     current_month_closings_cpi = 0,
     uncompounded_afudc = 0,
     uncompounded_cpi = 0,
     compounded_afudc = 0,
     compounded_cpi = 0,
     end_cwip = 0,
 	end_cwip_afudc = 0,
     end_cwip_cpi = 0
;
if uf_check_sql('Resetting Calc Variables') <> 1 then return -1
i_status_position = 12

///
///Eligibility flags
///

///Use the eligibility flags as a way to turn AFUDC on and off on a month by month basis

////Start and Stop Dates
update budget_afudc_calc_temp
set eligible_for_afudc= 0, eligible_for_cpi = 0
where (to_date(to_char(month_number),'YYYYMM') < nvl(trunc(afudc_start_date,'month'),to_date(to_char(month_number),'YYYYMM'))
	or to_date(to_char(month_number),'YYYYMM') > nvl(trunc(afudc_stop_date,'month'),to_date(to_char(month_number),'YYYYMM')))
;
if uf_check_sql('Setting Eligible for AFUDC: Afudc Stop and Start Date') <> 1 then return -1
i_status_position = 13
///Inservice date
update budget_afudc_calc_temp
set eligible_for_afudc = 0, eligible_for_cpi = 0
where month_number > to_char(in_service_date,'YYYYMM')
;
if uf_check_sql('Setting Eligible for AFUDC: greater then in-service date') <> 1 then return -1
i_status_position = 14

///Clearing and External are not eligible for AFUDC by default
update budget_afudc_calc_temp
set eligible_for_afudc = 0, eligible_for_cpi = 0
where closing_option_id in (5,9)
;
if uf_check_sql('Setting Eligible for AFUDC: closing option') <> 1 then return -1
///CJR this isn't needed the calc starts after the actuals month
//i_status_position = 15
//
//update budget_afudc_calc_temp
//set eligible_for_afudc = 0, eligible_for_cpi = 0
//where month_number <= :i_actuals_month_number
//;
//if uf_check_sql('Setting Eligible for AFUDC: for months less then actuals month') <> 1 then return -1
i_status_position =16
///Blankets are not eligible for AFUDC by default
update budget_afudc_calc_temp
set eligible_for_afudc = 0 , eligible_for_cpi = 0
where closing_option_id in (6,7,8,10,11)
  and afudc_type_id in (
   		select afudc_type_id from afudc_control
		minus
		select afudc_type_id from afudc_control
		where nvl(current_month_only,0) = 1	)
;
if uf_check_sql('Setting Eligible for AFUDC: Blanket Projects') <> 1 then return -1
i_status_position = 17
////Need to add logic for the following
	////min est $$ AFUDC
	////Min est $$ CPI
	////Min est  months  AFUDC
	////Min est months  CPI
	////Triggers??
	////Idle???
	///CPI deminmis ?????
///
///Rates & Compounding Indicators
///


update budget_afudc_calc_temp a set 	
	afudc_debt_rate = (
		select debt_rate
		   from afudc_data b
		 where a.afudc_type_id = b.afudc_type_id
			and b.effective_date =(select max(c.effective_date) from afudc_data c
											where a.afudc_type_id = c.afudc_type_id
												and c.effective_date <= to_date(to_char(a.month_number),'YYYYMM'))),		
	afudc_equity_rate = (
		select equity_rate
		 from afudc_data b
		 where a.afudc_type_id = b.afudc_type_id
			and b.effective_date =(select max(c.effective_date) from afudc_data c
											where a.afudc_type_id = c.afudc_type_id
												and c.effective_date <= to_date(to_char(a.month_number),'YYYYMM'))),	
	cpi_rate = (
		select cpi_rate
		 from afudc_data b
		 where a.afudc_type_id = b.afudc_type_id
			and b.effective_date =(select max(c.effective_date) from afudc_data c
											where a.afudc_type_id = c.afudc_type_id
												and c.effective_date <= to_date(to_char(a.month_number),'YYYYMM'))),	
	afudc_compound_ind = (
		select decode(substr(to_char(a.month_number),5,2),
				'01',afudc_jan_com_ind, '02',afudc_feb_com_ind, '03',afudc_mar_com_ind,
				'04',afudc_apr_com_ind, '05',afudc_may_com_ind, '06',afudc_jun_com_ind,
				'07',afudc_jul_com_ind, '08',afudc_aug_com_ind, '09',afudc_sep_com_ind,
				'10',afudc_oct_com_ind, '11',afudc_nov_com_ind, '12',afudc_dec_com_ind, 0)
		  from afudc_data b
		 where a.afudc_type_id = b.afudc_type_id
			and b.effective_date =(select max(c.effective_date) from afudc_data c
											where a.afudc_type_id = c.afudc_type_id
												and c.effective_date <= to_date(to_char(a.month_number),'YYYYMM'))),	
	cpi_compound_ind = (
		select decode(substr(to_char(a.month_number),5,2),
				'01',cpi_jan_com_ind, '02',cpi_feb_com_ind, '03',cpi_mar_com_ind,
				'04',cpi_apr_com_ind, '05',cpi_may_com_ind, '06',cpi_jun_com_ind,
				'07',cpi_jul_com_ind, '08',cpi_aug_com_ind, '09',cpi_sep_com_ind,
				'10',cpi_oct_com_ind, '11',cpi_nov_com_ind, '12',cpi_dec_com_ind, 0)
		 from afudc_data b
		 where a.afudc_type_id = b.afudc_type_id
			and b.effective_date =(select max(c.effective_date) from afudc_data c
											where a.afudc_type_id = c.afudc_type_id
												and c.effective_date <= to_date(to_char(a.month_number),'YYYYMM')))
	;
	if uf_check_sql('Setting Rates and Compound indicators') <> 1 then return -1
	i_status_position = 18

//### Added input_ratio for debt, equity and CPI
// On the actuals side, it's set to -999 if not between -1 and 1, however it gets set to 1 and gives an AFUDC status message
//saying that the ratio was ignored. Since there is no AFUDC status on budget, just setting it to 1

update budget_afudc_calc_temp a set 	
	input_afudc_ratio = nvl((
		select  case when nvl(bair.input_afudc_ratio,1) between -1 and 1 then nvl(bair.input_afudc_ratio,1) else 1 end air
		   from budget_afudc_input_ratio bair
		 where a.work_order_id = bair.work_order_id
			and bair.effective_date =(select max(c.effective_date) from budget_afudc_input_ratio c
											where a.work_order_id = c.work_order_id
												and c.effective_date <= to_date(to_char(a.month_number),'YYYYMM'))),1),	
	input_cpi_ratio	=nvl((
		select case when nvl(bair.input_cpi_ratio,1) between -1 and 1 then nvl(bair.input_cpi_ratio,1) else 1 end
		   from budget_afudc_input_ratio bair
		 where a.work_order_id = bair.work_order_id
			and bair.effective_date =(select max(c.effective_date) from budget_afudc_input_ratio c
											where a.work_order_id = c.work_order_id
												and c.effective_date <= to_date(to_char(a.month_number),'YYYYMM'))),1);
												
///
///Current Month Charges percentage to be used in the calc
///

update budget_afudc_calc_temp a
set current_month_charges_afudc = amount_afudc * nvl((
 		select b.ignore_current_month //decode(lower(nvl(b.ignore_current_month,'no')),'no',.5,'yes',0,1)
		from afudc_control b
		where a.afudc_type_id = b.afudc_type_id),.5),
	current_month_charges_cpi = amount_cpi * nvl((
		select b.ignore_current_month_cpi //decode(lower(nvl(b.ignore_current_month,'no')),'no',.5,'yes',0,1)
		from afudc_control b
		where a.afudc_type_id = b.afudc_type_id),.5)
;
if uf_check_sql('Setting current month charges %') <> 1 then return -1
i_status_position = 19




///
///Closing Percentage the insert sets this value to 0 so flip it to the right percentage where is shouldn't be 1
///

update budget_afudc_calc_temp
set closings_pct = null
;
if uf_check_sql('Setting Closing %: Setting everything to 0') <> 1 then return -1

/////Closing Pattern based
update budget_afudc_calc_temp a
set closings_pct = (select max(percent_close)
						from closing_pattern_data b
						where a.closing_pattern_id = b.closing_pattern_id
							and a.month_number = b.month_number
							and a.expenditure_type_id = b.closing_type	)
where closing_pattern_id is not null
  and exists (
		select 1
		from closing_pattern_data c
		where a.closing_pattern_id = c.closing_pattern_id
			and a.month_number = c.month_number
			and a.expenditure_type_id = c.closing_type)
;
if uf_check_sql('Setting Closing %: Closing Pattern') <> 1 then return -1

update budget_afudc_calc_temp a
set closings_pct = (select max(percent_close)
						from closing_pattern_data b
						where a.closing_pattern_id = b.closing_pattern_id
							and a.month_number = b.month_number
							and -1 = b.closing_type
						)
where closing_pattern_id is not null
  and closings_pct is null
  and exists (
		select 1
		from closing_pattern_data c
		where a.closing_pattern_id = c.closing_pattern_id
			and a.month_number = c.month_number
			and -1 = c.closing_type
)
;
if uf_check_sql('Setting Closing %: Closing Pattern') <> 1 then return -1
i_status_position =20
///CJR
///So there is a slight hole here if a funding project is using either standard auto no-non untized or standard no non-unitized 
///The in-service date field contains the est in-service date not the est complete date.  Which means we are going to close the dollars too soon
///Suggest now that on the budget side that you set est in-service date and est complete date to be equal maybe use the custom function if needed.  although this could potentially make your AFUDC run longer.  
///You could even leave the est in-service date blank then is uses the est complete date (again it would make your AFUDC run longer if a earlier in-service date was desired)
///if this becomes a problem we can try to come up with a work around (maybe add a new field to budget_afudc_calc_temp)
///now I don't know of anyone using these two closing options in the budget so ignore for now
update budget_afudc_calc_temp a
set closings_pct = 1
where closing_option_id in (1,2,3,4)
  and month_number >= to_number(to_char(in_service_date,'YYYYMM'))
;
if uf_check_sql('Setting Closing %: In-service date') <> 1 then return -1
i_status_position =21
//
/////Monthly close or manual blankets for now treat manual blanket no-nonunitize as the same as the normal manual blanket
update budget_afudc_calc_temp
set closings_pct = 1
where closing_option_id in (6,10,11)
;
if uf_check_sql('Setting Closing %: Blankets(monthly,manual)') <> 1 then return -1
i_status_position =22
/////Annual and Quarterly close
update budget_afudc_calc_temp
set closings_pct = 1
where closing_option_id in (7,8)
	and expenditure_type_id = 1
;
if uf_check_sql('Setting Closing %: Blankets (Annual, Quarterly Additions)') <> 1 then return -1
i_status_position =23
update budget_afudc_calc_temp a
set closings_pct = 1
where closing_option_id = 8
and expenditure_type_id in (2,6)
and exists (
		select 1
		from pp_table_months b
		where to_number(substr(a.month_number,5,2)) = b.month_num
			and b.fiscal_period = 12)
;
if uf_check_sql('Setting Closing %: Blankets (Annual Removal/Salvage)') <> 1 then return -1
i_status_position =24
update budget_afudc_calc_temp a
set closings_pct = 1
where closing_option_id = 7
and expenditure_type_id in (2,6)
and to_number(substr(a.month_number,5,2)) in (
	select max(p.month_num)
	from pp_table_months p
	group by quarter_num)
;
if uf_check_sql('Setting Closing %: Blankets ( Quarterly Removal/Salvage)') <> 1 then return -1
i_status_position =25

update budget_afudc_calc_temp
set closings_pct = nvl(closings_pct,0)
;
if uf_check_sql('Setting Closing %: Nulls to 0') <> 1 then return -1
i_status_position =25

rtn = uf_cwip_balances()
if rtn < 0 then
	return -1
end if

///At some point should come back to this statment and pull the open AFUDC and CPI balances from actual to start this out
///For now we are assuming they have updated with actuals
//###CJR Maint 5987
///Changed this logic to calculate the uncompounded AFUDC amount for the cwip start month
///Then in the rollforward to get to the actuals month ending balance use the compounding indicators 
///To determine when this amount should roll into the base.
update budget_afudc_calc_temp a
set uncompounded_afudc =nvl( (select sum(b.afudc_debt + b.afudc_equity)
										from budget_afudc_calc_temp b
										where a.work_order_id = b.work_order_id
											and a.expenditure_type_id = b.expenditure_type_id
											and b.month_number < :i_cwip_start_month
											and a.wo_work_order_id = b.wo_work_order_id
											and b.month_number > nvl( (select max(month_number)
																				from budget_afudc_calc_temp c
																				where c.work_order_id = b.work_order_id
																					and c.expenditure_type_id = b.expenditure_type_id
																					and a.wo_work_order_id = b.wo_work_order_id
																					and c.afudc_compound_ind = 1
																					and c.month_number < :i_cwip_start_month),0)),0),
	uncompounded_cpi =nvl( (select sum(b.cpi)
									from budget_afudc_calc_temp b
									where a.work_order_id = b.work_order_id
										and a.expenditure_type_id = b.expenditure_type_id
										and b.month_number < :i_cwip_start_month
										and a.wo_work_order_id = b.wo_work_order_id
										and b.month_number > nvl( (select max(month_number)
																			from budget_afudc_calc_temp c
																			where c.work_order_id = b.work_order_id
																				and c.expenditure_type_id = b.expenditure_type_id
																				and a.wo_work_order_id = b.wo_work_order_id
																				and c.cpi_compound_ind = 1
																				and c.month_number < :i_cwip_start_month),0)),0)
where month_number = :i_cwip_start_month
;
if uf_check_sql('Setting begining Uncompounded AFUDC & CPI') <> 1 then return -1


////###CJR Maint 5987
////No longer needed moved the compounding roll forward into the calc function
////###CJR this function was modified based on maint 5798
//###CJR modifed based on 8514 (un commented and changed to beg cwip 
update budget_afudc_calc_temp
set beg_cwip_afudc = beg_cwip_afudc - uncompounded_afudc,
	 beg_cwip_cpi = beg_cwip_cpi - uncompounded_cpi
where month_number = :i_cwip_start_month
;
if uf_check_sql('Rolling Back Uncompounded AFUDC from AFUDC basis') <> 1 then return -1

i_status_position =26
//update budget_afudc_calc_temp a
//set uncompounded_afudc = (select nvl(uncompounded_afudc,0)  + nvl(afudc_debt,0) + nvl(afudc_equity,0)
//									  from budget_afudc_calc_temp b
//									  where a.work_order_id = b.work_order_id
//										and a.expenditure_type_id = b.expenditure_type_id
//										and a.expenditure_type_id = 1
//										and b.expenditure_type_id = 1
//										and b.month_number = :i_actuals_month_number),
//	uncompounded_cpi = (select nvl(uncompounded_cpi,0)  + nvl(cpi,0)
//									  from budget_afudc_calc_temp b
//									  where a.work_order_id = b.work_order_id
//										and a.expenditure_type_id = b.expenditure_type_id
//										and a.expenditure_type_id = 1
//										and b.expenditure_type_id = 1
//										and b.month_number = :i_actuals_month_number)
//where month_number = :i_start_month_number
//  and expenditure_type_id = 1
//;

if i_cap_interest then
	update budget_afudc_calc_temp a
	set cap_interest_adj_ratio = -1
	where exists (select 1 from budget_cap_interest_wo b where a.work_order_id = b.work_order_id)
	;
	if uf_check_sql('Building Cap Intrest Adjustment Factors ') <> 1 then return -1
	
	update budget_afudc_calc_temp a
	set cap_interest_adj_ratio = 1
	where exists (select 1 from budget_cap_interest_wo b where a.work_order_id = b.parent_wo)
	;
	if uf_check_sql('Building Cap Intrest Adjustment Factors ') <> 1 then return -1
end if

//////Tax expensing
//CDM make this an update to the temp table
///Come back and touch this up based on wo_work_order_id change ignoring from now and this will just pull from funding project level
update budget_afudc_calc_temp a
set (tax_status_id, fp_percent_expense) = (
	select b.tax_status_id, b.fp_percent_expense
	from WORK_ORDER_TAX_STATUS b, (select work_order_id, max(effective_accounting_month) effective_accounting_month from WORK_ORDER_TAX_STATUS group by work_order_id) c
	where a.work_order_id = b.work_order_id
	and b.effective_accounting_month = c.effective_accounting_month
	and b.work_order_id = c.work_order_id) //CDM add this where clause
;
if uf_check_sql('Set Tax expensing status ') <> 1 then return -1

update budget_afudc_calc_temp
set tax_expense_amount = nvl(fp_percent_expense,0) * nvl(amount_cpi,0)
;
if uf_check_sql('Calculating Tax Expense Amount ') <> 1 then return -1

rtn = f_budget_afudc_custom(0,i_table_name, i_actuals_month_number)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_afudc' ) = 1 then
	rtn = sqlca.f_client_budget_afudc(0,i_table_name, i_actuals_month_number)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
args[1] = '0'
args[2] = i_table_name
args[3] = string(i_actuals_month_number)
rtn = min(rtn, f_wo_validation_control(81,args))
if rtn < 0 then
	rollback;
	return -1
end if

return 1
end function

public function longlong uf_post_process ();longlong rtn
uo_budget_revision uo_revisions

// ### CDM - Maint 7767 - Update budget_amounts after processing budget items
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	uf_msg('Updating Budget Amounts','I')
	uo_revisions = create uo_budget_revision
	
	uo_revisions.i_messagebox_onerror = i_messagebox_onerror
	uo_revisions.i_status_box = i_status_box
	uo_revisions.i_pp_msgs = i_pp_msgs
	uo_revisions.i_add_time_to_messages = i_add_time_to_messages
	uo_revisions.i_statusbox_onerror = i_statusbox_onerror
	uo_revisions.i_display_msg_on_sql_success = i_display_msg_on_sql_success
	uo_revisions.i_display_rowcount = i_display_rowcount
	uo_revisions.i_progress_bar = i_progress_bar
	
	rtn = uo_revisions.uf_update_budget_amounts()
	destroy uo_revisions
	
	if rtn = -1 then
		rollback;
		return -1
	end if
end if

///Now move the results from the temp calc table to a permanent table.  
delete from budget_afudc_calc a
where exists (
select 1
from wo_est_processing_temp b
where a.work_order_id = b.work_order_id
  and a.revision = b.revision)
 ;
 if uf_check_sql('Deleting Previous AFUDC results') <> 1 then return -1

insert into budget_afudc_calc (
WORK_ORDER_ID,REVISION,EXPENDITURE_TYPE_ID,MONTH_NUMBER,AMOUNT,AMOUNT_AFUDC,AMOUNT_CPI,IN_SERVICE_DATE,AFUDC_TYPE_ID,CLOSING_PATTERN_ID,
CLOSING_OPTION_ID,ELIGIBLE_FOR_AFUDC,ELIGIBLE_FOR_CPI,AFUDC_STOP_DATE,AFUDC_START_DATE,AFUDC_DEBT_RATE,AFUDC_EQUITY_RATE,CPI_RATE,AFUDC_COMPOUND_IND,
CPI_COMPOUND_IND,CLOSINGS_PCT,FUNDING_WO_INDICATOR,BEG_CWIP,BEG_CWIP_AFUDC,BEG_CWIP_CPI,CURRENT_MONTH_CHARGES_AFUDC,CURRENT_MONTH_CHARGES_CPI,
CURRENT_MONTH_CLOSINGS,CURRENT_MONTH_CLOSINGS_AFUDC,CURRENT_MONTH_CLOSINGS_CPI,UNCOMPOUNDED_AFUDC,UNCOMPOUNDED_CPI,COMPOUNDED_AFUDC,
COMPOUNDED_CPI,AFUDC_BASE,CPI_BASE,AFUDC_DEBT,AFUDC_EQUITY,CPI,END_CWIP,END_CWIP_AFUDC,END_CWIP_CPI,EST_MONTHLY_ID_DEBT,EST_MONTHLY_ID_EQUITY,
EST_MONTHLY_ID_CPI,EST_MONTHLY_ID_CWIPBAL_CWIP,EST_MONTHLY_ID_CWIPBAL_TAX,EST_MONTHLY_ID_CWIPBAL_REMOVAL,EST_MONTHLY_ID_CWIPBAL_SALVAGE,EST_MONTHLY_ID_CLOSINGS_CWIP,
EST_MONTHLY_ID_CLOSINGS_TAX,EST_MONTHLY_ID_CLOSINGS_RMVL,EST_MONTHLY_ID_CLOSINGS_SLVG,DEBT_EST_CHG_TYPE_ID,EQUITY_EST_CHG_TYPE_ID,CPI_EST_CHG_TYPE_ID, cap_interest_adjustment,
cap_interest_adj_ratio, cpi_balance, cpi_closings,tax_status_id, fp_percent_expense, tax_expense_amount, tax_expense_amount_bal, tax_expense_amount_closed, retirements, ciac,
orig_eligible_for_afudc, orig_eligible_for_cpi, actuals_mn_proj, actuals_mn_bv, budget_version_id, orig_est_start_date, orig_est_in_service_date, orig_est_complete_date,
cwip_start_month, wo_work_order_id , input_afudc_ratio, input_cpi_ratio)
select  WORK_ORDER_ID,REVISION,EXPENDITURE_TYPE_ID,MONTH_NUMBER,AMOUNT,AMOUNT_AFUDC,AMOUNT_CPI,IN_SERVICE_DATE,AFUDC_TYPE_ID,CLOSING_PATTERN_ID,
CLOSING_OPTION_ID,ELIGIBLE_FOR_AFUDC,ELIGIBLE_FOR_CPI,AFUDC_STOP_DATE,AFUDC_START_DATE,AFUDC_DEBT_RATE,AFUDC_EQUITY_RATE,CPI_RATE,AFUDC_COMPOUND_IND,
CPI_COMPOUND_IND,CLOSINGS_PCT,FUNDING_WO_INDICATOR,BEG_CWIP,BEG_CWIP_AFUDC,BEG_CWIP_CPI,CURRENT_MONTH_CHARGES_AFUDC,CURRENT_MONTH_CHARGES_CPI,
CURRENT_MONTH_CLOSINGS,CURRENT_MONTH_CLOSINGS_AFUDC,CURRENT_MONTH_CLOSINGS_CPI,UNCOMPOUNDED_AFUDC,UNCOMPOUNDED_CPI,COMPOUNDED_AFUDC,
COMPOUNDED_CPI,AFUDC_BASE,CPI_BASE,AFUDC_DEBT,AFUDC_EQUITY,CPI,END_CWIP,END_CWIP_AFUDC,END_CWIP_CPI,EST_MONTHLY_ID_DEBT,EST_MONTHLY_ID_EQUITY,
EST_MONTHLY_ID_CPI,EST_MONTHLY_ID_CWIPBAL_CWIP,EST_MONTHLY_ID_CWIPBAL_TAX,EST_MONTHLY_ID_CWIPBAL_REMOVAL,EST_MONTHLY_ID_CWIPBAL_SALVAGE,EST_MONTHLY_ID_CLOSINGS_CWIP,
EST_MONTHLY_ID_CLOSINGS_TAX,EST_MONTHLY_ID_CLOSINGS_RMVL,EST_MONTHLY_ID_CLOSINGS_SLVG,DEBT_EST_CHG_TYPE_ID,EQUITY_EST_CHG_TYPE_ID,CPI_EST_CHG_TYPE_ID, cap_interest_adjustment,
cap_interest_adj_ratio, cpi_balance, cpi_closings, tax_status_id, fp_percent_expense, tax_expense_amount, tax_expense_amount_bal, tax_expense_amount_closed, retirements, ciac,
orig_eligible_for_afudc, orig_eligible_for_cpi, actuals_mn_proj, actuals_mn_bv, budget_version_id, orig_est_start_date, orig_est_in_service_date, orig_est_complete_date,
cwip_start_month, wo_work_order_id , input_afudc_ratio, input_cpi_ratio
  from budget_afudc_calc_temp
 ;
 if uf_check_sql('Inserting into BUDGET AFUDC CALC') <> 1 then return -1



rtn = f_budget_afudc_custom(3,i_table_name, i_actuals_month_number)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_afudc' ) = 1 then
	rtn = sqlca.f_client_budget_afudc(3,i_table_name, i_actuals_month_number)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = '3'
args[2] = i_table_name
args[3] = string(i_actuals_month_number)
rtn = f_wo_validation_control(81,args)
if rtn < 0 then
	rollback;
	return -1
end if
i_status_position =100
if isvalid(w_progressbar) then
	close(w_progressbar)
end if

return 1
end function

public function longlong uf_closings_to_deprfcst (longlong a_actuals_month_number, string a_table_name, longlong a_fcst_depr_version);
longlong use_bpc, use_jt, rtn,i, cpi_reconcile_item, depr_endmonth, cpi_book_summary_id, l_budget_version_id
string sqls, amount_field, closing_type, expenditure_type_string, closing_name, sqls_ret, id_field, rev_field, closing_table, exp_type_select
boolean process_version_level

if i_budget_version = 0 then
	process_version_level = false
else
	process_version_level = true
end if

select nvl(to_char(max(fdv.end_date),'YYYYMM'),'999999'), max(budget_version_id)
into :depr_endmonth, :l_budget_version_id
from fcst_depr_version fdv
where fdv.fcst_depr_version_id = :a_fcst_depr_version
;

if i_stage_cpi then
	select max(reconcile_item_id), max(book_summary_id)
	into :cpi_reconcile_item, :cpi_book_summary_id
	from estimate_charge_type 
	where funding_charge_indicator = 1
	  and nvl(afudc_flag,'Q') = 'P'
	;
	if uf_check_sql(' Selecting CPI Reconcile item id') <> 1 then return -1
end if


///Stage Closings into monthly columns
///Build pct table
///Join the two together to insert into depr fcst

if process_version_level then
	delete from budget_closings_stage
	where fcst_depr_version_id = :a_fcst_depr_version;
else
	delete from budget_closings_stage
	where fcst_depr_version_id = :a_fcst_depr_version
	and exists (
		select 1 from wo_est_processing_temp w
		where w.work_order_id = budget_closings_stage.work_order_id
		and w.revision = budget_closings_stage.revision
		);
end if
if uf_check_sql(' Deleting previous results from closing stage') <> 1 then return -1


if process_version_level then
	delete from budget_closings_pct
	where fcst_depr_version_id = :a_fcst_depr_version;
else
	delete from budget_closings_pct
	where fcst_depr_version_id = :a_fcst_depr_version
	and exists (
		select 1 from wo_est_processing_temp w
		where w.work_order_id = budget_closings_pct.work_order_id
		and w.revision = budget_closings_pct.revision
		);
end if
if uf_check_sql(' Deleting previous results from closing pct') <> 1 then return -1

for i = 1 to 4
	choose case i
		case 1
			amount_field = 'current_month_closings'
			closing_type = "decode(trans.expenditure_type_id,1,'C',2,'R',6,'V')"
			expenditure_type_string = ' and trans.expenditure_type_id in (1,2,6) '
			closing_name = 'Book Closings'
			closing_table = 'BUDGET_AFUDC_CALC'
			exp_type_select = 'trans.expenditure_type_id'
		case 2 
			if i_stage_tax then
				amount_field = 'current_month_closings_cpi'
				closing_type = " nvl('X','X') "
				expenditure_type_string = ' and trans.expenditure_type_id = 1 '
				closing_name = 'Tax Closings'
				closing_table = 'BUDGET_AFUDC_CALC'
				exp_type_select = 'trans.expenditure_type_id'
			else
				continue
			end if
		case 3
			if i_stage_cpi then
				amount_field = 'cpi_closings'
				closing_type = " nvl('Q','Q') "
				expenditure_type_string = ' and trans.expenditure_type_id = 1 '
				closing_name = 'CPI Closings'
				closing_table = 'BUDGET_AFUDC_CALC'
				exp_type_select = 'trans.expenditure_type_id'
			else
				continue
			end if
		case 4
			if i_stage_wip then
				amount_field = 'wip_calc_closings'
				closing_type = " nvl('W','W') "
				expenditure_type_string = ' and 1 = 1 '
				closing_name = 'WIP Closings'
				closing_table = 'budget_wip_comp_calc'
				exp_type_select = '1'
			else
				continue
			end if
	end choose
sqls = " "
sqls = ' insert into budget_closings_stage (revision, work_order_id, expenditure_type_id, year,  '+&
	     ' january ,  february ,   march ,   april ,   may ,  june , july ,  august ,  september ,  october ,  november ,  december , '+&
		 ' closing_type, fcst_depr_version_id)  '+&
		' select trans.revision, trans.work_order_id, '+exp_type_select+',  substr(trans.month_number,1,4) year,   '+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'01',trans."+amount_field+",0)),0) jan,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'02',trans."+amount_field+",0)),0) feb,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'03',trans."+amount_field+",0)),0) mar,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'04',trans."+amount_field+",0)),0) apr,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'05',trans."+amount_field+",0)),0) may,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'06',trans."+amount_field+",0)),0) jun,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'07',trans."+amount_field+",0)),0) jul,  "+&
		" nvl(sum(decode(substr(to_char(trans.month_number),-2),'08',trans."+amount_field+",0)),0) aug,  "+&
		" nvl(sum(decode(substr(to_char(trans.month_number),-2),'09',trans."+amount_field+",0)),0) sep,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'10',trans."+amount_field+",0)),0) oct,  "+&
		"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'11',trans."+amount_field+",0)),0) nov,  "+&
		" nvl(sum(decode(substr(to_char(trans.month_number),-2),'12',trans."+amount_field+",0)),0) dcm,  "+&
		"  "+closing_type+", "+string(a_fcst_depr_version)+'  '+&
		 " from "+closing_table+" trans, "
		 if a_table_name = 'WO_EST_MONTHLY' then
			sqls += ' budget_version_fund_proj bvfp '
			if i_restrict then
				///if restricting by business segment and company we need to join to work order control to get this extra information
				sqls +=' ,work_order_control woc '
			end if
			sqls +=' where bvfp.work_order_id = trans.work_order_id '
			sqls +='  and bvfp.budget_version_id = '+string(l_budget_version_id)
			sqls +='  and exists(select 1 from wo_est_processing_temp wept where bvfp.work_order_id = wept.work_order_id and bvfp.revision = wept.revision) '
			sqls +='  and bvfp.active = 1 '
			sqls +=' and trans.month_number > '+string(a_actuals_month_number)
			sqls +=' and bvfp.revision = trans.revision '
			if i_restrict then
				sqls +=' and bvfp.work_order_id = woc.work_order_id '
				sqls +=' and exists ( '
				sqls +=' 	select 1 from fcst_depr_group_version fdgv '
				sqls +=' 	where fdgv.company_id = woc.company_id '
				sqls +=' 		and fdgv.bus_segment_id = woc.bus_segment_id '
				sqls +='		and fdgv.fcst_depr_version_id = '+string(a_fcst_depr_version) +' ) '
			end if
			
		else
			sqls += ' budget_amounts bvfp '
			if i_restrict then
				///if restricting by business segment and company we need to join to budget to get this extra information
				sqls +=' ,budget woc '
			end if
			sqls +='  where bvfp.budget_id = trans.work_order_id '
			sqls +='       and bvfp.budget_version_id = '+string(l_budget_version_id)
			sqls +='  and exists(select 1 from wo_est_processing_temp wept where bvfp.budget_id = wept.work_order_id and bvfp.budget_version_id = wept.revision) '
			sqls += ' and trans.month_number > '+string(a_actuals_month_number)
			sqls +='  and bvfp.budget_version_id = trans.revision '
			if i_restrict then
				sqls += ' and woc.budget_id = bvfp.budget_id '
				sqls +=' and exists ( '
				sqls +=' 		select 1 from fcst_depr_group_version fdgv '
				sqls +=' 		where fdgv.company_id = woc.company_id '
				sqls +='    	and fdgv.bus_segment_id = woc.bus_segment_id '
				sqls +=' 		and fdgv.fcst_depr_version_id = '+string(a_fcst_depr_version)+') '
			end if
		end if
		sqls +=' '+expenditure_type_string
		sqls += ' and trans.month_number <= '+string(depr_endmonth)
		sqls +=' group by trans.work_order_id, trans.revision,  '+exp_type_select+',substr(trans.month_number,1,4), '+closing_type
		sqls +=' having '
		sqls += "   nvl(sum(decode(substr(to_char(trans.month_number),-2),'01',trans."+amount_field+",0)),0) <> 0 or "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'02',trans."+amount_field+",0)),0) <> 0 or  "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'03',trans."+amount_field+",0)),0) <> 0 or  "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'04',trans."+amount_field+",0)),0) <> 0 or  "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'05',trans."+amount_field+",0)),0) <> 0 or  "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'06',trans."+amount_field+",0)),0) <> 0 or  "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'07',trans."+amount_field+",0)),0) <> 0 or  "+&
				" nvl(sum(decode(substr(to_char(trans.month_number),-2),'08',trans."+amount_field+",0)),0) <> 0 or  "+&
				" nvl(sum(decode(substr(to_char(trans.month_number),-2),'09',trans."+amount_field+",0)),0) <> 0 or  "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'10',trans."+amount_field+",0)),0) <> 0 or  "+&
				"  nvl(sum(decode(substr(to_char(trans.month_number),-2),'11',trans."+amount_field+",0)),0) <> 0 or  "+&
				" nvl(sum(decode(substr(to_char(trans.month_number),-2),'12',trans."+amount_field+",0)),0) <> 0   "
				
	execute immediate :sqls;
	if uf_check_sql(' Staging Closings - '+a_table_name+' '+closing_name) <> 1 then return -1

next

/////NOW PROCESS OCR's if they are estimated on a funding project
closing_name = 'Book Retirements'
if a_table_name = 'WO_EST_MONTHLY' then
	id_field = 'w.work_order_id'
	rev_field = 'w.revision'
else
	id_field = 'w.budget_id'
	rev_field = 'w.budget_version_id'
end if
///###CJR Maint 5466 (changed closing type to O from C below)
sqls_ret = '  insert into budget_closings_stage (revision, work_order_id, expenditure_type_id, year,   '+&
	           ' january ,  february ,   march ,   april ,   may ,  june , july ,  august ,  september ,  october ,  november ,  december ,  '+&
		      ' closing_type, fcst_depr_version_id)  '+&
			  ' select '+rev_field+', '+id_field+', expenditure_type_id, year,  '+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '01'),-1,january,0),january)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '02'),-1,february,0),february)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '03'),-1,march,0),march)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '04'),-1,april,0),april)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '05'),-1,may,0),may)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '06'),-1,june,0),june)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '07'),-1,july,0),july)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '08'),-1,august,0),august)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '09'),-1,september,0),september)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '10'),-1,october,0),october)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '11'),-1,november,0),november)), "+&
			  " -sum(decode(sign(year - substr("+string(a_actuals_month_number)+",1,4)),0,decode(sign(substr("+string(a_actuals_month_number)+",5,2) - '12'),-1,december,0),december)), "+&
			  " 'O', "+string(a_fcst_depr_version)+" "
			  if a_table_name = 'WO_EST_MONTHLY' then
				sqls_ret += ' from wo_est_monthly w, budget_version_fund_proj bvfp '
				sqls_ret += ' where w.work_order_id = bvfp.work_order_id '
				sqls_ret += ' 	and w.revision = bvfp.revision '
				sqls_ret += ' 	and bvfp.active = 1 '
				sqls_ret +='     and bvfp.budget_version_id = '+string(l_budget_version_id)
				sqls_ret += '  and exists(select 1 from wo_est_processing_temp wept where bvfp.work_order_id = wept.work_order_id and bvfp.revision = wept.revision) '
				sqls_ret += '	and exists (select 1 from estimate_charge_type e where e.est_chg_type_id = w.est_chg_type_id and e.processing_type_id = 1) '
				sqls_ret += '	and w.expenditure_type_id = 2 '
				sqls_ret += '	and year >= substr('+string(a_actuals_month_number)+' ,1,4) '
			else //BUDGET MONTHLY DATA
				sqls_ret += ' from budget_monthly_data w '
				sqls_ret += ' where  exists (select 1 from estimate_charge_type e where e.est_chg_type_id = w.est_chg_type_id and e.processing_type_id = 1) '
				sqls_ret += '	and w.expenditure_type_id = 2 '
				sqls_ret += '	and w.year >= substr('+string(a_actuals_month_number)+' ,1,4) '
				sqls_ret +='     and w.budget_version_id = '+string(l_budget_version_id)
				sqls_ret += '  and exists(select 1 from wo_est_processing_temp wept where w.budget_id = wept.work_order_id and w.budget_version_id = wept.revision) '
			end if
			sqls_ret += ' and year <= '+mid(string(depr_endmonth),1,4)
			sqls_ret += ' group by '+rev_field+', '+id_field+", expenditure_type_id, year ,  'C', "+string(a_fcst_depr_version)
			
			execute immediate :sqls_ret;
			if uf_check_sql(' Staging Closings - '+a_table_name+' '+closing_name) <> 1 then return -1


	


///This is a custom function that builds the budget_closings_pct table.  The result should fill in the amount column and the following update will fill in the percent
///Future development will be to make this table driven.
uf_msg('Building Budget Closings Pct Table','I')
rtn = f_budget_closings_pct(a_table_name, a_actuals_month_number,a_fcst_depr_version,process_version_level)
if rtn < 0 then 
	return -1
end if

if i_stage_wip then
	///This is a custom function that builds the budget_closings_pct table.  The result should fill in the amount column and the following update will fill in the percent
	///Future development will be to make this table driven.
	uf_msg('Building Budget Closings Pct Table(WIP)','I')
	rtn = f_budget_closings_pct('BUDGET_WIP_COMP_CALC', a_actuals_month_number,a_fcst_depr_version,process_version_level)
	if rtn < 0 then 
		return -1
	end if
end if

///These are implicit commit;
sqlca.analyze_table('budget_closings_stage')
if  uf_check_sql('Analzye table budget_closings_stg') <> 1 then return -1
sqlca.analyze_table('budget_closings_pct')
if  uf_check_sql('Analzye table budget_closings_pct') <> 1 then return -1
//###CJR MAINT 5424
//update budget_closings_pct z
//set percent = (
//	select y.percentage
//	from ( 
//			select id, round(ratio_to_report(amount) over( Partition by work_order_id, revision, nvl(year,9999), nvl(closing_type,'C')), 8) percentage
//			from budget_closings_pct 
//			where fcst_depr_version_id = :a_fcst_depr_version
//			) y
//	where y.id = z.id
//		)
//where z.fcst_depr_version_id = :a_fcst_depr_version
//;
//if uf_check_sql(' Updating Percent on Budget Closings PCT') <> 1 then return -1
sqlca.truncate_table('budget_closings_pct_temp')
if uf_check_sql(' Truncating table budget_closings_pct_temp') <> 1 then return -1
insert into budget_closings_pct_temp (id, percentage)
select y.id, nvl(round(ratio_to_report(amount) 
	over( Partition by work_order_id, revision, nvl(year,9999), nvl(closing_type,'C')), 8),0) percentage
from budget_closings_pct y
where fcst_depr_version_id = :a_fcst_depr_version
and exists (
	select 1 from wo_est_processing_temp w
	where w.work_order_id = y.work_order_id
	and w.revision = y.revision
	);
if  uf_check_sql('Populating Budget Closings Pct table') <> 1 then return -1

update budget_closings_pct y
set percent = (
	select percentage from budget_closings_pct_temp z
	where z.id = y.id)
where fcst_depr_version_id = :a_fcst_depr_version
and exists (
	select 1 from wo_est_processing_temp w
	where w.work_order_id = y.work_order_id
	and w.revision = y.revision
	);
if uf_check_sql(' Updating Percent on Budget Closings PCT') <> 1 then return -1

update budget_closings_pct z
set percent = percent -  (
	select y.diff
	from (
			select work_order_id, revision, nvl(year,9999) year, nvl(closing_type,'C') closing_type, round(sum(percent) - 1,8) diff, max(id), fcst_depr_version_id
			from budget_closings_pct
			where fcst_depr_version_id = :a_fcst_depr_version
			and exists (
				select 1 from wo_est_processing_temp w
				where w.work_order_id = budget_closings_pct.work_order_id
				and w.revision = budget_closings_pct.revision
				)
			having round(sum(percent) - 1,8) <> 0
			group by work_order_id, revision, fcst_depr_version_id, nvl(year,9999), nvl(closing_type,'C')
			) y
	where y.work_order_id = z.work_order_id
		and y.revision = z.revision
		and y.closing_type = nvl(z.closing_type,'C') 
		and y.year = nvl(z.year,9999))
where (z.work_order_id, z.revision, z.id, z.fcst_depr_version_id, nvl(z.closing_type,'C'), nvl(z.year,9999) ) in (
	select work_order_id, revision, max(id), fcst_depr_version_id,  nvl(closing_type,'C'), nvl(year,9999)
	from budget_closings_pct
	where fcst_depr_version_id = :a_fcst_depr_version
	and exists (
		select 1 from wo_est_processing_temp w
		where w.work_order_id = budget_closings_pct.work_order_id
		and w.revision = budget_closings_pct.revision
		)
	having round(sum(percent) - 1,8) <> 0
	group by work_order_id, revision, fcst_depr_version_id, nvl(year,9999), nvl(closing_type,'C')
  )
 ;
 if uf_check_sql(' Updating Percent on Budget Closings PCT') <> 1 then return -1
 
update budget_closings_pct
set percent = 0
where percent is null
;
if uf_check_sql(' Updating Percent on Budget Closings PCT (Nulls)') <> 1 then return -1

///Add code here to verify that each work order/funding project only has 100% per closing type

///Add code here to verify that each funding project in this budget version with closings (based on the restrict above) has closings pct
///Book summary was missing from this insert statement book summary was added as part of 10.2.2.5
insert into fcst_budget_load(load_id, time_stamp,user_id, fcst_depr_version_id, budget_version_id, company_id, major_location_id, budget_plant_class_id,
year, expenditure_type_id, closing_type,  january, february, march, april, may, june, july, august, september,
october, november, december, total, budget_id, work_order_id,revision,job_task_id,long_description, bus_segment_id, reconcile_item_id, book_summary_id)
select pwrplant1.nextval, sysdate, user, :a_fcst_depr_version, :l_budget_version_id,a.*
from (
  select bcp.company_id, bcp.major_location_id, bcp.budget_plant_class_id,bcs.year, decode(bcs.expenditure_type_id,6,2,bcs.expenditure_type_id),
 decode(bcs.closing_type, 'W','C','O','C',bcs.closing_type) closing_type,
sum(bcs.january * bcp.percent ), sum(bcs.february * bcp.percent),sum( bcs.march * bcp.percent),sum(bcs.april * bcp.percent), 
sum(bcs.may * bcp.percent), sum(bcs.june * bcp.percent),sum(bcs.july * bcp.percent), sum(bcs.august * bcp.percent),
sum(bcs.september * bcp.percent), sum(bcs.october * bcp.percent), sum(bcs.november * bcp.percent), sum(bcs.december * bcp.percent),
sum(bcs.january * bcp.percent)+ sum(bcs.february * bcp.percent)+ sum(bcs.march * bcp.percent)+sum(bcs.april * bcp.percent)+
sum( bcs.may * bcp.percent)+ sum(bcs.june * bcp.percent)+sum(bcs.july * bcp.percent)+ sum(bcs.august * bcp.percent)+sum( bcs.september * bcp.percent)+
sum(bcs.october * bcp.percent)+ sum(bcs.november * bcp.percent)+sum( bcs.december * bcp.percent) total,
decode(upper(:a_table_name),'BUDGET_MONTHLY_DATA',bcp.work_order_id,bcp.budget_id), bcp.work_order_id, bcp.revision,
bcp.job_task_id, 'Staged Closings', bcp.bus_segment_id, decode(bcs.closing_type,'Q',:cpi_reconcile_item, bcp.reconcile_item_id), decode(bcs.closing_type,'Q',:cpi_book_summary_id, bcp.book_summary_id)
from budget_closings_pct bcp, budget_closings_stage bcs
where bcp.work_order_id = bcs.work_order_id
	and bcp.revision = bcs.revision
	and bcp.fcst_depr_version_id = bcs.fcst_depr_version_id
	and bcp.fcst_depr_version_id = :a_fcst_depr_version
	and nvl(bcp.closing_type,decode(bcs.closing_type,'V','R','Q','C',bcs.closing_type)) = decode(bcs.closing_type,'V','R','Q','C',bcs.closing_type)/*Salvage should follow the removal closes*/
	and nvl(bcp.year,bcs.year) = bcs.year /*year and closing type are optional fields*/
	and exists (
		select 1 from wo_est_processing_temp wept
		where wept.work_order_id = bcp.work_order_id
		and wept.revision = bcp.revision
		)
group by bcp.company_id, bcp.major_location_id, bcp.budget_plant_class_id,bcs.year, decode(bcs.expenditure_type_id,6,2,bcs.expenditure_type_id),
  decode(upper(:a_table_name),'BUDGET_MONTHLY_DATA',bcp.work_order_id,bcp.budget_id), bcp.work_order_id, bcp.revision,
  bcp.job_task_id, bcp.bus_segment_id, decode(bcs.closing_type,'Q',:cpi_reconcile_item, bcp.reconcile_item_id), bcs.closing_type, decode(bcs.closing_type,'Q',:cpi_book_summary_id, bcp.book_summary_id)
) a
;
if uf_check_sql(' Load Closings') <> 1 then return -1


return 1
end function

public function longlong uf_detailed_closings (longlong a_actuals_month_number, string a_table_name, longlong a_fcst_depr_version);



///Data model for reference


//create global temporary table budget_closings_temp (
//	id number(22,0),
//	work_order_id number(22,0),
//	revision number(22,0),
//	month_number number(22,0),
//	expenditure_type_id number(22,0),
//	actuals_month_number number(22,0),
//	company_id number(22,0),
//	bus_segment_id number(22,0),
//	major_location_id number(22,0),
//	job_task_id varchar2(35),
//	book_summary_id number(22,0),
//	vintage number(22,0),	
//	beg_cwip number(22,2),
//	current_month_amount number(22,2),
//	current_month_closings number(22,2),
//	end_cwip number(22,2))
//on commit preserve rows
//;
//alter table budget_closings_temp add (constraint pk_budget_closings_temp primary key (id) );
//CREATE UNIQUE INDEX ix_budget_closings_temp
//  ON budget_closings_temp (work_order_id, revision, month_number, expenditure_type_id, company_id, bus_segment_id, major_location_id,
//								 job_task_id,book_summary_id,vintage) ;
//create or replace public synonym budget_closings_temp for pwrplant.budget_closings_temp;
//grant all on budget_closings_temp to pwrplant_role_dev;
//
//create table budget_closings (
//	id number(22,0),
//	work_order_id number(22,0),
//	revision number(22,0),
//	month_number number(22,0),
//	expenditure_type_id number(22,0),
//	actuals_month_number number(22,0),
//	company_id number(22,0),
//	bus_segment_id number(22,0),
//	major_location_id number(22,0),
//	job_task_id varchar2(35),
//	book_summary_id number(22,0),
//	vintage number(22,0),	
//	beg_cwip number(22,2),
//	current_month_amount number(22,2),
//	current_month_closings number(22,2),
//	end_cwip number(22,2))
//;
//alter table budget_closings add (constraint pk_budget_closings primary key (id) using index tablespace pwrplant_idx);
//CREATE UNIQUE INDEX ix_budget_closings
//  ON budget_closings (work_order_id, revision, month_number, expenditure_type_id, company_id, bus_segment_id, major_location_id,
//								 job_task_id,book_summary_id,vintage) tablespace pwrplant_idx ;
//create or replace public synonym budget_closings for pwrplant.budget_closings;
//grant all on budget_closings to pwrplant_role_dev;
//
//create table budget_closings_values(
//	"VALUE" varchar2(35),
//	sql varchar2(4000),
//	time_stamp date,
//	user_id varchar2(18))
//;
//
//alter table budget_closings_values add (constraint pk_budget_closings_values primary key ("VALUE") using index tablespace pwrplant_idx);
//grant all on budget_closings_values to pwrplant_role_dev;
//create or replace public synonym budget_closings_values for pwrplant.budget_closings_values;
//
//select * from budget_closings_values;
//
//insert into budget_closings_values("VALUE",sql)
//values('company_id','work_order_control.company_id')
//;
//
//insert into budget_closings_values("VALUE",sql)
//values('bus_segment_id','work_order_control.bus_segment_id')
//;
//	
//insert into budget_closings_values("VALUE",sql)
//values('major_location_id','work_order_control.major_location_id')
//;
//
//insert into budget_closings_values("VALUE",sql)
//values('job_task_id','wo_est_monthly.job_task_id')
//;
//
//insert into budget_closings_values("VALUE",sql)
//values('vintage','null')
//;
//
//insert into budget_closings_values("VALUE",sql)
//values('book_summary_id','estimate_charge_type.book_summary_id')
//;


longlong rtn, i, j, end_year, prev_month_number , check,budget_version_id
string ret, val_sql, val
string budget_company, budget_major_location, budget_bus_segment, budget_job_task, budget_book_summary,budget_vintage, actual_sql, budget_budget_plant_class
string actual_company, actual_major_location, actual_bus_segment, actual_job_task, actual_book_summary,actual_vintage, budget_sql, actual_budget_plant_class
boolean process_version_level
uo_ds_top ds_closings_values
ds_closings_values = create uo_ds_top



if i_budget_version = 0 then
	process_version_level = false
else
	process_version_level = true
end if

select budget_version_id
into :budget_version_id
from fcst_depr_version
where fcst_depr_version_id = :a_fcst_depr_version;

i_table_name = a_table_name
//i_budget_version = a_budget_version

i_actuals_month_number = a_actuals_month_number
i_cwip_start_month = a_actuals_month_number

if i_actuals_month_number < i_cwip_start_month then
	i_cwip_start_month = i_actuals_month_number
end if


if process_version_level then
	delete from budget_closings_pct
	where fcst_depr_version_id = :a_fcst_depr_version;
else
	delete from budget_closings_pct
	where fcst_depr_version_id = :a_fcst_depr_version
	and exists (
		select 1 from wo_est_processing_temp w
		where w.work_order_id = budget_closings_pct.work_order_id
		and w.revision = budget_closings_pct.revision
		);
end if

sqlca.truncate_table('budget_closings_temp')
if uf_check_sql(' Truncating table budget_closings_temp') <> 1 then return -1

if a_table_name = 'BUDGET_MONTHLY_DATA' then  
	i_funding_wo_indicator = 2 
else
	i_funding_wo_indicator = 1
end if

//get the actuals month start month and beg cwip month
//rtn = uf_start_month()
//if rtn < 0 then
//	return -1
//end if

i_actuals_month_number = a_actuals_month_number
i_cwip_start_month = a_actuals_month_number


if i_actuals_month_number < i_cwip_start_month then
	i_cwip_start_month= i_actuals_month_number
end if

//if isvalid(w_budget2fcst) then
	//i_restrict = w_budget2fcst.i_restrict_co_bs
	//i_stage_tax = w_budget2fcst.i_stage_tax
	//i_stage_cpi = w_budget2fcst.i_stage_cpi
	//i_stage_wip = w_budget2fcst.i_stage_wip
//end if


delete from budget_closings_pct
where fcst_depr_version_id = :a_fcst_depr_version
;
if uf_check_sql("Clearing Budget Closing %'s")< 0 then
	return -1
end if

delete from budget_closings_temp
;
if uf_check_sql("Clearing Budget Closing Temp")< 0 then
	return -1
end if

val_sql = "Select VALUE, sql from budget_closings_values "
///NOW INSERT ACTUALS AMOUNTS 
ret = f_create_dynamic_ds(ds_closings_values,'grid',val_sql,sqlca,true)
if ret <> 'OK' then
	uf_msg('ERROR Building Actual Insert Datastore ','E')
	uf_msg(ret,'E')
end if

for i = 1 to ds_closings_values.rowcount()
	setnull(val_sql)
	setnull(val)
	val = ds_closings_values.getitemstring(i,1)
	val_sql = ds_closings_values.getitemstring(i,2)
	choose case lower(val)
		case 'actual company_id'  
			actual_company = val_sql
		case 'actual bus_segment_id'  
			actual_bus_segment = val_sql
		case 'actual major_location_id'  
			actual_major_location = val_sql
		case 'actual job_task_id'  
			actual_job_task = val_sql
		case 'actual book_summary_id'  
			actual_book_summary = val_sql
		case 'actual vintage'  
			actual_vintage = val_sql
		case 'actual budget_plant_class_id'
			actual_budget_plant_class = val_sql
		case 'budget company_id'  
			budget_company = val_sql
		case 'budget bus_segment_id'  
			budget_bus_segment = val_sql
		case 'budget major_location_id'  
			budget_major_location = val_sql
		case 'budget job_task_id'  
			budget_job_task = val_sql
		case 'budget book_summary_id'  
			budget_book_summary = val_sql
		case 'budget vintage'  
			budget_vintage = val_sql
		case 'budget budget_plant_class_id'
			budget_budget_plant_class = val_sql
	end choose
	
next


// ### DJL - Maint 9948 - 04.16.2012
// Prevent the budget from bringing over closings that occurred prior to the actual month.
// I don't think I care about the in service date, closing option, and closing pattern for actuals.
// If we want to avoid bringing over closings we need to make sure closed month number is null
// when we gather the begin cwip balances and when we gather the current month amount.
// Since the last union pulls closings from the actuals month, I decided to comment it out.
actual_sql = 'insert into budget_closings_temp(id, fcst_depr_version_id, work_order_id,revision, month_number, expenditure_type_id, actuals_month_number, '
actual_sql += '	company_id, bus_segment_id, major_location_id, job_task_id, book_summary_id, vintage, budget_plant_class_id, beg_cwip, '
actual_sql += '	current_month_amount, current_month_closings, end_cwip, closing_type, in_service_date, closing_pattern_id, closing_option_id) '
actual_sql += ' select pwrplant1.nextval, ' + string(a_fcst_depr_version) +', vw.* '
actual_sql += ' from ( '
actual_sql += '	select work_order_id, '
actual_sql += '			revision, '
actual_sql += '			month_number, '
actual_sql += '			expenditure_type_id, '
actual_sql += '			actuals_month_number, '
actual_sql += '			actual_company, '
actual_sql += '			actual_bus_segment, '
actual_sql += '			 actual_major_location, '
actual_sql += '			actual_job_task, '
actual_sql += '			actual_book_summary, '
actual_sql += '			actual_vintage, '
actual_sql += ' 			actual_budget_plant_class, '
actual_sql += '			sum(beg_cwip) beg_cwip, '
actual_sql += '			sum(current_month_amount) current_month_amount, '
actual_sql += '			sum(curernt_month_closings) curernt_month_closings, '
actual_sql += '			sum(end_cwip) end_cwip, '
actual_sql += '				closing_type, in_service_date, closing_pattern_id, closing_option_id '
actual_sql += '		from ( '
actual_sql += '			select budget_version_fund_proj.work_order_id, '
actual_sql += '					budget_version_fund_proj.revision, '
actual_sql += '					'+string(i_cwip_start_month)+' month_number, '
actual_sql += '					decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id) expenditure_type_id, '
actual_sql += '					'+string(i_actuals_month_number)+' actuals_month_number, '
actual_sql += '					'+actual_company+' actual_company , '
actual_sql += '					'+actual_bus_segment+' actual_bus_segment, '
actual_sql += '					'+actual_major_location+' actual_major_location, '
actual_sql += '					'+actual_job_task+' actual_job_task ,'
actual_sql += '					'+actual_book_summary+' actual_book_summary, '
actual_sql += '					'+actual_vintage+' actual_vintage, '
actual_sql += '					'+actual_budget_plant_class+' actual_budget_plant_class, '
actual_sql += '					sum(decode(sign(nvl(closed_month_number,999999) - ' + string(i_cwip_start_month) + '),-1,0,cwip_charge.amount)) beg_cwip, '
actual_sql += '					0 current_month_amount, '
actual_sql += '					0 curernt_month_closings, '
actual_sql += '					0 end_cwip, '
actual_sql += "					decode(charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(cwip_charge.expenditure_type_id,1,'C','R')) closing_type, "
actual_sql += "		case "
actual_sql += "	  	when work_order_control.wo_status_id < 4 then "
actual_sql += "	      	 decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(work_order_approval.est_in_service_date,work_order_approval.est_complete_date),work_order_approval.est_complete_date)  "
actual_sql += "	  	when work_order_control.wo_status_id = 4 then "
actual_sql += "	     	  decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),work_order_approval.est_complete_date) "
actual_sql += "	  	when work_order_control.wo_status_id > 4 then "
actual_sql += "	   	    decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),add_months(nvl(work_order_control.completion_date,work_order_approval.est_complete_date),nvl(work_order_control.late_chg_wait_period,0))) "
actual_sql += "		end in_service_date, "
actual_sql += "		 work_order_approval.closing_pattern_id, work_order_account.closing_option_Id "
actual_sql += '			from cwip_charge, work_order_control, work_order_control wo, charge_type,budget_version_fund_proj, work_order_account, work_order_approval  '
actual_sql += '			where budget_version_fund_proj.work_order_id = work_order_control.work_order_id '
actual_sql += '			and budget_version_fund_proj.budget_version_id =  '+string(budget_version_id)
actual_sql += '			and budget_version_fund_proj.active = 1 '
actual_sql += '			and work_order_control.work_order_id = wo.funding_wo_id '
actual_sql += '			and wo.work_order_id = cwip_charge.work_order_id '
actual_sql += '	         and work_order_control.work_order_id = work_order_account.work_order_id '
actual_sql += '			and work_order_approval.work_order_id =  budget_version_fund_proj.work_order_id  '
actual_sql += '			and work_order_approval.revision =  budget_version_fund_proj.revision '
actual_sql += '          and cwip_charge.charge_type_id = charge_type.charge_type_id '
actual_sql += '		    and charge_type.processing_type_id in (2,3,4,5,7) '///This excludes retirements, revenue, commitments, reserve credits, other credits but includes tax processing types by design
actual_sql += '		    and cwip_charge.month_number < '+string(i_cwip_start_month)
//CWB PC-1486 Per Chris Mardis, take Exelon's pppatchl and add in wo_est_processing_temp references
actual_sql += '		    and exists (select 1 from wo_est_processing_temp wept where wept.work_order_id = budget_version_fund_proj.work_order_id and wept.revision = budget_version_fund_proj.revision) '
//DJL - Maint 9948
actual_sql += '		    and nvl(cwip_charge.closed_month_number,999999) > ' + string(i_actuals_month_number)
actual_sql += '			group by budget_version_fund_proj.work_order_id, '
actual_sql += '					budget_version_fund_proj.revision, '
actual_sql += '					'+string(i_cwip_start_month)+' , '
actual_sql += '					decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id) , '
actual_sql += '					'+string(i_actuals_month_number)+' , '
actual_sql += '					'+actual_company+'  , '
actual_sql += '					'+actual_bus_segment+' , '
actual_sql += '					'+actual_major_location+' , '
actual_sql += '					'+actual_job_task+'  ,'
actual_sql += '					'+actual_book_summary+' , '
actual_sql += '					'+actual_vintage+' , '
actual_sql += '					'+actual_budget_plant_class+' , '
actual_sql += "					decode(charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(cwip_charge.expenditure_type_id,1,'C','R')), "
actual_sql += "		case "
actual_sql += "	  	when work_order_control.wo_status_id < 4 then "
actual_sql += "	      	 decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(work_order_approval.est_in_service_date,work_order_approval.est_complete_date),work_order_approval.est_complete_date)  "
actual_sql += "	  	when work_order_control.wo_status_id = 4 then "
actual_sql += "	     	  decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),work_order_approval.est_complete_date) "
actual_sql += "	  	when work_order_control.wo_status_id > 4 then "
actual_sql += "	   	    decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),add_months(nvl(work_order_control.completion_date,work_order_approval.est_complete_date),nvl(work_order_control.late_chg_wait_period,0))) "
actual_sql += "		end , "
actual_sql += "		 work_order_approval.closing_pattern_id, work_order_account.closing_option_Id "
actual_sql += '		union all '
actual_sql += '			select budget_version_fund_proj.work_order_id, '
actual_sql += '					budget_version_fund_proj.revision, '
actual_sql += '					cwip_charge.month_number month_number, '
actual_sql += '					decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id) expenditure_type_id, '
actual_sql += '					'+string(i_actuals_month_number)+' actuals_month_number, '
actual_sql += '					'+actual_company+' actual_company , '
actual_sql += '					'+actual_bus_segment+' actual_bus_segment, '
actual_sql += '					'+actual_major_location+' actual_major_location, '
actual_sql += '					'+actual_job_task+' actual_job_task ,'
actual_sql += '					'+actual_book_summary+' actual_book_summary, '
actual_sql += '					'+actual_vintage+' actual_vintage, '
actual_sql += '					'+actual_budget_plant_class+' actual_budget_plant_class, '
actual_sql += '					0 beg_cwip, '
actual_sql += '					sum(amount) current_month_amount, '
actual_sql += '					0 curernt_month_closings, '
actual_sql += '					0 end_cwip, '
actual_sql += "					decode(charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(cwip_charge.expenditure_type_id,1,'C','R')), "
actual_sql += "		case "
actual_sql += "	  	when work_order_control.wo_status_id < 4 then "
actual_sql += "	      	 decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(work_order_approval.est_in_service_date,work_order_approval.est_complete_date),work_order_approval.est_complete_date)  "
actual_sql += "	  	when work_order_control.wo_status_id = 4 then "
actual_sql += "	     	  decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),work_order_approval.est_complete_date) "
actual_sql += "	  	when work_order_control.wo_status_id > 4 then "
actual_sql += "	   	    decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),add_months(nvl(work_order_control.completion_date,work_order_approval.est_complete_date),nvl(work_order_control.late_chg_wait_period,0))) "
actual_sql += "		end in_service_date, "
actual_sql += "		 work_order_approval.closing_pattern_id, work_order_account.closing_option_Id "
actual_sql += '			from cwip_charge, work_order_control, work_order_control wo, charge_type,budget_version_fund_proj, work_order_account, work_order_approval  '
actual_sql += '			where budget_version_fund_proj.work_order_id = work_order_control.work_order_id '
actual_sql += '			and budget_version_fund_proj.budget_version_id = '+string(budget_version_id)
actual_sql += '			and budget_version_fund_proj.active = 1 '
actual_sql += '			and work_order_control.work_order_id = wo.funding_wo_id '
actual_sql += '			and wo.work_order_id = cwip_charge.work_order_id '
actual_sql += '	         and work_order_control.work_order_id = work_order_account.work_order_id '
actual_sql += '			and work_order_approval.work_order_id =  budget_version_fund_proj.work_order_id  '
actual_sql += '			and work_order_approval.revision =  budget_version_fund_proj.revision '
actual_sql += '          and cwip_charge.charge_type_id = charge_type.charge_type_id '
actual_sql += '		    and charge_type.processing_type_id in (2,3,4,5,7) '///This excludes retirements, revenue, commitments, reserve credits, other credits but includes tax processing types by design
actual_sql += '		    and cwip_charge.month_number between '+string(i_cwip_start_month)+' and '+string(i_actuals_month_number)+'  '
//CWB PC-1486 Per Chris Mardis, take Exelon's pppatchl and add in wo_est_processing_temp references
actual_sql += '		    and exists (select 1 from wo_est_processing_temp wept where wept.work_order_id = budget_version_fund_proj.work_order_id and wept.revision = budget_version_fund_proj.revision) '
//DJL - Maint 9948
actual_sql += '		    and nvl(cwip_charge.closed_month_number,999999) > ' + string(i_actuals_month_number)
actual_sql += '			group by budget_version_fund_proj.work_order_id, '
actual_sql += '					budget_version_fund_proj.revision, '
actual_sql += '					cwip_charge.month_number, '
actual_sql += '					decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id) , '
actual_sql += '					'+string(i_actuals_month_number)+' , '
actual_sql += '					'+actual_company+'  , '
actual_sql += '					'+actual_bus_segment+' , '
actual_sql += '					'+actual_major_location+' , '
actual_sql += '					'+actual_job_task+'  ,'
actual_sql += '					'+actual_book_summary+' , '
actual_sql += '					'+actual_vintage+' , '
actual_sql += '					'+actual_budget_plant_class+' , '
actual_sql += "					decode(charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(cwip_charge.expenditure_type_id,1,'C','R')), "
actual_sql += "		case "
actual_sql += "	  	when work_order_control.wo_status_id < 4 then "
actual_sql += "	      	 decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(work_order_approval.est_in_service_date,work_order_approval.est_complete_date),work_order_approval.est_complete_date)  "
actual_sql += "	  	when work_order_control.wo_status_id = 4 then "
actual_sql += "	     	  decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),work_order_approval.est_complete_date) "
actual_sql += "	  	when work_order_control.wo_status_id > 4 then "
actual_sql += "	   	    decode(decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),add_months(nvl(work_order_control.completion_date,work_order_approval.est_complete_date),nvl(work_order_control.late_chg_wait_period,0))) "
actual_sql += "		end , "
actual_sql += "		 work_order_approval.closing_pattern_id, work_order_account.closing_option_Id "
//DJL - Maint 9948
//actual_sql += '		union all '
//actual_sql += '			select budget_version_fund_proj.work_order_id, '
//actual_sql += '					budget_version_fund_proj.revision, '
//actual_sql += '					cwip_charge.closed_month_number month_number, '
//actual_sql += '					decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id) expenditure_type_id, '
//actual_sql += '					'+string(i_actuals_month_number)+' actuals_month_number, '
//actual_sql += '					'+actual_company+' actual_company , '
//actual_sql += '					'+actual_bus_segment+' actual_bus_segment, '
//actual_sql += '					'+actual_major_location+' actual_major_location, '
//actual_sql += '					'+actual_job_task+' actual_job_task ,'
//actual_sql += '					'+actual_book_summary+' actual_book_summary, '
//actual_sql += '					'+actual_vintage+' actual_vintage, '
//actual_sql += '					'+actual_budget_plant_class+' actual_budget_plant_class, '
//actual_sql += '					0 beg_cwip, '
//actual_sql += '					0 current_month_amount, '
//actual_sql += '					sum(amount) curernt_month_closings, '
//actual_sql += '					0 end_cwip, '
//actual_sql += "					decode(charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(cwip_charge.expenditure_type_id,1,'C','R')) "
//actual_sql += '			from cwip_charge, work_order_control, work_order_control wo, charge_type,budget_version_fund_proj, work_order_account  '
//actual_sql += '			where budget_version_fund_proj.work_order_id = work_order_control.work_order_id '
//actual_sql += '			and budget_version_fund_proj.budget_version_id =  '+string(i_budget_version)
//actual_sql += '			and budget_version_fund_proj.active = 1 '
//actual_sql += '			and work_order_control.work_order_id = wo.funding_wo_id '
//actual_sql += '			and wo.work_order_id = cwip_charge.work_order_id '
//actual_sql += '	         and work_order_control.work_order_id = work_order_account.work_order_id '
//actual_sql += '          and cwip_charge.charge_type_id = charge_type.charge_type_id '
//actual_sql += '		    and charge_type.processing_type_id in (2,3,4,5,7) '///This excludes retirements, revenue, commitments, reserve credits, other credits but includes tax processing types by design
//actual_sql += '		    and nvl(cwip_charge.closed_month_number,999999) between '+string(i_cwip_start_month)+' and '+string(i_actuals_month_number)+'  '
//actual_sql += '			group by budget_version_fund_proj.work_order_id, '
//actual_sql += '					budget_version_fund_proj.revision, '
//actual_sql += '					cwip_charge.closed_month_number, '
//actual_sql += '					decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id) , '
//actual_sql += '					'+string(i_actuals_month_number)+' , '
//actual_sql += '					'+actual_company+'  , '
//actual_sql += '					'+actual_bus_segment+' , '
//actual_sql += '					'+actual_major_location+' , '
//actual_sql += '					'+actual_job_task+'  ,'
//actual_sql += '					'+actual_book_summary+' , '
//actual_sql += '					'+actual_vintage+' , '
//actual_sql += '					'+actual_budget_plant_class+' , '
//actual_sql += "					decode(charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(cwip_charge.expenditure_type_id,1,'C','R')) "
actual_sql +=' ) '
actual_sql +=' group by  work_order_id,  '
actual_sql +=' 			revision,  '
actual_sql +=' 			month_number, '
actual_sql +=' 			expenditure_type_id, '
actual_sql +=' 			actuals_month_number, '
actual_sql +=' 			actual_company, '
actual_sql +=' 			actual_bus_segment, '
actual_sql +=' 			 actual_major_location, '
actual_sql +=' 			actual_job_task, '
actual_sql +=' 			actual_book_summary, '
actual_sql +=' 			actual_vintage, closing_type, actual_budget_plant_class, in_service_date, closing_pattern_id, closing_option_id) vw '
if i_restrict then
	actual_sql += ' where exists ( '
	actual_sql += '     select 1 from fcst_depr_group_version '
	actual_sql += '     where fcst_depr_group_version.fcst_depr_version_id = '+string(a_fcst_depr_version)
	actual_sql += '          and fcst_depr_group_version.company_id = vw.actual_company'
	actual_sql += '          and fcst_depr_group_version.bus_segment_id = vw.actual_bus_segment ) '
end if

execute immediate :actual_sql;
if uf_check_sql('Inserting Actual Amounts') < 0 then
	return -1
end if


//S TEAGUE 8-26-2010: Received update to the budget_sql from Cliff Robinson
//S TEAGUE 9-1-2010: Fixed join between budget_version_fund_proj and wo_est_monthly

budget_sql = 'insert into budget_closings_temp(id, fcst_depr_version_id, work_order_id,revision, month_number, expenditure_type_id, actuals_month_number, '
budget_sql += '	company_id, bus_segment_id, major_location_id, job_task_id, book_summary_id, vintage, budget_plant_class_id, beg_cwip, '
budget_sql += '	current_month_amount, current_month_closings, end_cwip, closing_type, in_service_date, closing_pattern_id, closing_option_id) '
budget_sql +=  'select pwrplant1.nextval, ' + string(a_fcst_depr_version) +', vw.* '
budget_sql += ' from ( '
budget_sql += '	select budget_version_fund_proj.work_order_id, '
budget_sql += '			budget_version_fund_proj.revision, '
budget_sql += '			wo_est_monthly.year*100+pp_table_months.month_num month_number, '
budget_sql += '			decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id) expendiure_type_id, '
budget_sql += '			'+string(i_actuals_month_number)+' , '
budget_sql += '			'+budget_company+' company_id, '
budget_sql += ' 			'+budget_bus_segment+' bus_segment_id, '
budget_sql += '			'+budget_major_location+' major_location_id, '
budget_sql += '			'+budget_job_task+' job_task_id, '
budget_sql += '			'+budget_book_summary+' book_summary_id, '
budget_sql += '			' +budget_vintage+' vintage, '
budget_sql += '         '+budget_budget_plant_class +' budget_plant_class_id, '
budget_sql += '			0 beg_cwip, '
budget_sql += '			sum(decode(pp_table_months.month_num,1,january,2,february,3,march,4,april,5,may,6,june, '
budget_sql += '  				7,july,8,august,9,september,10,october,11,november,12,december)) current_month_amount,	'
budget_sql += '			0 current_month_closings, '
budget_sql += '			0 end_cwip, '
budget_sql += "         decode(estimate_charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(wo_est_monthly.expenditure_type_id,1,'C','R')), "
budget_sql += "         case "
budget_sql += "           when work_order_control.wo_status_id < 4 then "
budget_sql += "               decode(decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id),1,nvl(work_order_approval.est_in_service_date,work_order_approval.est_complete_date),work_order_approval.est_complete_date)  "
budget_sql += "           when work_order_control.wo_status_id = 4 then "
budget_sql += "               decode(decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),work_order_approval.est_complete_date) "
budget_sql += "           when work_order_control.wo_status_id > 4 then "
budget_sql += "               decode(decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),add_months(nvl(work_order_control.completion_date,work_order_approval.est_complete_date),nvl(work_order_control.late_chg_wait_period,0))) "
budget_sql += "         end in_service_date,  "
budget_sql += "    work_order_approval.closing_pattern_id, work_order_account.closing_option_Id "
budget_sql += '	from budget_version_fund_proj, work_order_control, work_order_account, wo_est_monthly, estimate_charge_type, pp_table_months, '
budget_sql += '			work_order_approval '
budget_sql += '	where budget_version_fund_proj.work_order_id = wo_est_monthly.work_order_id '
budget_sql += '	and budget_version_fund_proj.revision = wo_est_monthly.revision '
budget_sql += '	and budget_version_fund_proj.work_order_id = work_order_control.work_order_id '
budget_sql += '	and budget_version_fund_proj.work_order_id = work_order_account.work_order_id '
budget_sql += '	and work_order_approval.work_order_id = work_order_account.work_order_id '
budget_sql += '	and work_order_approval.revision = budget_version_fund_proj.revision '
budget_sql += '	and wo_est_monthly.est_chg_type_id = estimate_charge_type.est_chg_type_id '
budget_sql += '	and budget_version_fund_proj.active = 1 '
budget_sql += '	and budget_version_fund_proj.budget_version_id =  '+string(budget_version_id)
budget_sql += '	and wo_est_monthly.year*100+pp_table_months.month_num > '+string(i_actuals_month_number)
//CWB PC-1486 Per Chris Mardis, take Exelon's pppatchl and add in wo_est_processing_temp references
budget_sql += '	and exists (select 1 from wo_est_processing_temp wept where wept.work_order_id = budget_version_fund_proj.work_order_id and wept.revision = budget_version_fund_proj.revision) '
budget_sql += '	Group By budget_version_fund_proj.work_order_id, '
budget_sql += '			budget_version_fund_proj.revision, '
budget_sql += '			wo_est_monthly.year*100+pp_table_months.month_num , '
budget_sql += '			decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id) , '
budget_sql += '			'+string(i_actuals_month_number)+' , '
budget_sql += '			'+budget_company+' , '
budget_sql += ' 			'+budget_bus_segment+' , '
budget_sql += '			'+budget_major_location+' , '
budget_sql += '			'+budget_job_task+' , '
budget_sql += '			'+budget_book_summary+' , '
budget_sql += '			' +budget_vintage+' , '
budget_sql += '              '+budget_budget_plant_class +' , '
budget_sql += '			0 , '
budget_sql += '			0 , '
budget_sql += '			0 ,  '
budget_sql += "         decode(estimate_charge_type.processing_type_id,2,'V',7,'V',5,'X',1,'C',decode(wo_est_monthly.expenditure_type_id,1,'C','R')), "
budget_sql += "   case "
budget_sql += "     when work_order_control.wo_status_id < 4 then "
budget_sql += "         decode(decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id),1,nvl(work_order_approval.est_in_service_date,work_order_approval.est_complete_date),work_order_approval.est_complete_date)  "
budget_sql += "     when work_order_control.wo_status_id = 4 then "
budget_sql += "         decode(decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),work_order_approval.est_complete_date) "
budget_sql += "     when work_order_control.wo_status_id > 4 then "
budget_sql += "         decode(decode(estimate_charge_type.processing_type_id,2,6,7,6,wo_est_monthly.expenditure_type_id),1,nvl(nvl(work_order_control.in_service_date,work_order_approval.est_in_service_date),work_order_approval.est_complete_date),add_months(nvl(work_order_control.completion_date,work_order_approval.est_complete_date),nvl(work_order_control.late_chg_wait_period,0))) "
budget_sql += "   end,  "
budget_sql += "   work_order_approval.closing_pattern_id, work_order_account.closing_option_Id "
budget_sql += ') vw '
if i_restrict then
	budget_sql += ' where exists ( '
	budget_sql += '     select 1 from fcst_depr_group_version '
	budget_sql += '     where fcst_depr_group_version.fcst_depr_version_id = '+string(a_fcst_depr_version)
	budget_sql += '          and fcst_depr_group_version.company_id = vw.company_id '
	budget_sql += '          and fcst_depr_group_version.bus_segment_id = vw.bus_segment_id ) '
end if


execute immediate :budget_sql;
if uf_check_sql('Inserting Budget Amounts')< 0 then
	return -1
end if

///Insert any missing combinations
insert into budget_closings_temp( work_order_id, revision, expenditure_type_id, company_id, bus_segment_id, major_location_id, job_task_id, book_summary_id, actuals_month_number,
		vintage, budget_plant_class_id, month_number, closing_type, id, beg_cwip, current_month_amount, current_month_closings,end_cwip, fcst_depr_version_id
		,in_service_date, closing_pattern_id, closing_option_id)
select work_order_id, revision, expenditure_type_id, company_id, bus_segment_id, major_location_id, job_task_id, book_summary_id, actuals_month_number,
		vintage, budget_plant_class_id, month_number,closing_type, pwrplant1.nextval, 0, 0, 0,0, fcst_depr_version_id, in_service_date, closing_pattern_id, closing_option_id
from (
	select work_order_id, revision, expenditure_type_id, B.company_id, bus_segment_id, major_location_id, job_task_id, book_summary_id, actuals_month_number,
			vintage, budget_plant_class_id, year*100+months.month_num month_number, closing_type, fcst_depr_version_id, in_service_date, closing_pattern_id, closing_option_id
	from budget_closings_temp b, pp_table_months months, pp_table_yearS year, budget_version bv
	where bv.budget_version_id = :budget_version_id
		and year.year between substr(:i_cwip_start_month,1,4) and bv.end_year
	minus
	select work_order_id, revision, expenditure_type_id, company_id, bus_segment_id, major_location_id, job_task_id, book_summary_id, actuals_month_number,
			vintage, budget_plant_class_id, month_number, closing_type, fcst_depr_version_id, in_service_date, closing_pattern_id, closing_option_id
	from budget_closings_temp)
;
if uf_check_sql('Inserting missing Amounts') < 0 then
	return -1
end if

/////update closings pct from AFUDC table (shortcut for now should probably calculate closings here so doesn't have to rely on AFUDC)
//update budget_closings_temp a
//set closings_pct = (
//			select closings_pct
//			from budget_afudc_calc b
//			where a.work_order_id = b.work_order_id
//				and a.revision = b.revision
//				and a.expenditure_type_id = b.expenditure_type_id
//				and a.month_number = b.month_number)
//;
//if uf_check_sql('Updating Closings Temp closing pct ') < 0 then
//	return -1
//end if
//update budget_closings_temp a
//set closings_pct = 0
//where closings_pct is null
//;
//if uf_check_sql('Updating Closings Temp closing pct to 0') < 0 then
//	return -1
//end if
/////retirements always close right away
//update budget_closings_temp a
//set closings_pct = 1
//where closing_type = 'C'
//and expenditure_type_id = 2
//;
//if uf_check_sql('Updating Closings Temp closing pct to 1 for retirements') < 0 then
//	return -1
//end if




// ### DJL - Maint 9946 - 04.20.2012
// Allow run of depreciation forecast process without having to run the AFUDC process.

///
///Closing Percentage the insert sets this value to 0 so flip it to the right percentage where is shouldn't be 1
///
update budget_closings_temp
set closings_pct = null
;
if uf_check_sql('Setting Closing %: Setting everything to 0') <> 1 then return -1

/////Closing Pattern based
// Ref: a.expenditure_type_id - decode(charge_type.processing_type_id,2,6,7,6,cwip_charge.expenditure_type_id)
update budget_closings a
set closings_pct = (select max(percent_close)
						from closing_pattern_data b
						where a.closing_pattern_id = b.closing_pattern_id
							and a.month_number = b.month_number
							and a.expenditure_type_id = b.closing_type	)
where closing_pattern_id is not null
  and exists (
		select 1
		from closing_pattern_data c
		where a.closing_pattern_id = c.closing_pattern_id
			and a.month_number = c.month_number
			and a.expenditure_type_id = c.closing_type)
;
if uf_check_sql('Setting Closing %: Closing Pattern') <> 1 then return -1

update budget_closings a
set closings_pct = (select max(percent_close)
						from closing_pattern_data b
						where a.closing_pattern_id = b.closing_pattern_id
							and a.month_number = b.month_number
							and -1 = b.closing_type
						)
where closing_pattern_id is not null
  and closings_pct is null
  and exists (
		select 1
		from closing_pattern_data c
		where a.closing_pattern_id = c.closing_pattern_id
			and a.month_number = c.month_number
			and -1 = c.closing_type
)
;
if uf_check_sql('Setting Closing %: Closing Pattern') <> 1 then return -1

///CJR
///So there is a slight hole here if a funding project is using either standard auto no-non untized or standard no non-unitized 
///The in-service date field contains the est in-service date not the est complete date.  Which means we are going to close the dollars too soon
///Suggest now that on the budget side that you set est in-service date and est complete date to be equal maybe use the custom function if needed.  although this could potentially make your AFUDC run longer.  
///You could even leave the est in-service date blank then is uses the est complete date (again it would make your AFUDC run longer if a earlier in-service date was desired)
///if this becomes a problem we can try to come up with a work around (maybe add a new field to budget_afudc_calc_temp)
///now I don't know of anyone using these two closing options in the budget so ignore for now
update budget_closings_temp a
set closings_pct = 1
where closing_option_id in (1,2,3,4)
  and month_number >= to_number(to_char(in_service_date,'YYYYMM'))



;
if uf_check_sql('Setting Closing %: In-service date') <> 1 then return -1

//
/////Monthly close or manual blankets for now treat manual blanket no-nonunitize as the same as the normal manual blanket
update budget_closings_temp
set closings_pct = 1
where closing_option_id in (6,10,11)
;
if uf_check_sql('Setting Closing %: Blankets(monthly,manual)') <> 1 then return -1

/////Annual and Quarterly close
update budget_closings_temp
set closings_pct = 1
where closing_option_id in (7,8)
	and expenditure_type_id = 1
;
if uf_check_sql('Setting Closing %: Blankets (Annual, Quarterly Additions)') <> 1 then return -1

update budget_closings_temp a
set closings_pct = 1
where closing_option_id = 8
and expenditure_type_id in (2,6)
and exists (
		select 1
		from pp_table_months b
		where to_number(substr(a.month_number,5,2)) = b.month_num
			and b.fiscal_period = 12)
;
if uf_check_sql('Setting Closing %: Blankets (Annual Removal/Salvage)') <> 1 then return -1


update budget_closings_temp a
set closings_pct = 1
where closing_option_id = 7
and expenditure_type_id in (2,6)
and to_number(substr(a.month_number,5,2)) in (
	select max(p.month_num)
	from pp_table_months p
	group by quarter_num)
;
if uf_check_sql('Setting Closing %: Blankets ( Quarterly Removal/Salvage)') <> 1 then return -1


update budget_closings_temp
set closings_pct = nvl(closings_pct,0)
;
if uf_check_sql('Setting Closing %: Nulls to 0') <> 1 then return -1






select end_year*100+12
into :end_year
from budget_version
where budget_version_id = :budget_version_id
;
if uf_check_sql('Selecting End Year') < 0 then
	return -1
end if
prev_month_number = i_cwip_start_month

///now loop over and calculate the closings
for j = i_cwip_start_month to end_year
	if long(mid(string(j),5,2)) > 12 then
		j = (long(mid(string(j),1,4)) + 1)*100 + 1
	end if
	
	if j <> i_cwip_start_month then
		update budget_closings_temp a
		set beg_cwip =nvl( (select b.end_cwip 
							from budget_closings_temp b
							where b.month_number = :prev_month_number
								and a.work_order_id = b.work_order_id
								and a.revision = b.revision
								and a.expenditure_type_id = b.expenditure_type_id
								and a.company_id = b.company_id
								and nvl(a.job_task_id, '-9879' ) = nvl(b.job_task_id,'-9879')
								and a.bus_segment_id = b.bus_segment_id
								and nvl(a.major_location_id,-9999) = nvL(b.major_location_id,-9999)
								and nvl(a.budget_plant_class_id,-9999) = nvl(b.budget_plant_class_id,-9999)
								and nvl(a.book_summary_id,-9999) =nvl( b.book_summary_id,-9999)
								and nvl(a.vintage,0) = nvl(b.vintage,0)
								and nvl(a.closing_type, 'XX') = nvl(b.closing_type, 'XX'))
								,0)
		where month_number = :j
		;
		if uf_check_sql('Updating Begining CWIP') < 0 then
			return -1
		end if
	end if
	if j > i_actuals_month_number then
		update budget_closings_temp
		set current_month_closings = (beg_cwip + current_month_amount) * closings_pct,
			end_cwip = (beg_cwip + current_month_amount ) - ((beg_cwip + current_month_amount) * closings_pct)
		where month_number = :j
		;
		if uf_check_sql('Current Month Closings and Ending CWIP') < 0 then
			return -1
		end if
	else
		//actuals month already have the closings amounts calculated so just calculate the ending cwip balance
		update budget_closings_temp
		set end_cwip = beg_cwip + current_month_amount  -current_month_closings
		where month_number = :j
		;
		if uf_check_sql('Current Month Closings and Ending CWIP (actuals_month)') < 0 then
			return -1
		end if
	end if

	prev_month_number = j
next



///If doing detail closings this gives you the oportunity to allocate based on budget plant class
uf_msg('Building Budget Closings Pct Table','I')
rtn = f_budget_closings_pct(a_table_name, i_actuals_month_number,a_fcst_depr_version,process_version_level)
if rtn < 0 then 
	return -1
end if

select count(*)
into :check
from budget_closings_pct
where fcst_depr_version_id = :a_fcst_depr_version
and exists (
	select 1 from wo_est_processing_temp wept
	where wept.work_order_id = budget_closings_pct.work_order_id
	and wept.revision = budget_closings_pct.revision
	)
;

if check > 1 then
	update budget_closings_pct z
	set percent = (
		select y.percentage
		from ( 
				select id, round(ratio_to_report(amount) over( Partition by work_order_id, revision,  nvl(closing_type,'C')), 8) percentage
				from budget_closings_pct 
				where fcst_depr_version_id = :a_fcst_depr_version
				and exists (
					select 1 from wo_est_processing_temp wept
					where wept.work_order_id = budget_closings_pct.work_order_id
					and wept.revision = budget_closings_pct.revision
					)
				) y
		where y.id = z.id
			)
	where z.fcst_depr_version_id = :a_fcst_depr_version
	and exists (
		select 1 from wo_est_processing_temp wept
		where wept.work_order_id = z.work_order_id
		and wept.revision = z.revision
		)
	;
	if uf_check_sql(' Updating Percent on Budget Closings PCT') <> 1 then return -1
	
	update budget_closings_pct z
	set percent = percent -  (
		select y.diff
		from (
				select work_order_id, revision,  nvl(closing_type,'C') closing_type, round(sum(percent) - 1,8) diff, max(id), fcst_depr_version_id
				from budget_closings_pct
				where fcst_depr_version_id = :a_fcst_depr_version
				and exists (
					select 1 from wo_est_processing_temp wept
					where wept.work_order_id = budget_closings_pct.work_order_id
					and wept.revision = budget_closings_pct.revision
					)
				having round(sum(percent) - 1,8) <> 0
				group by work_order_id, revision, fcst_depr_version_id, nvl(year,9999), nvl(closing_type,'C')
				) y
		where y.work_order_id = z.work_order_id
			and y.revision = z.revision
			and y.closing_type = nvl(z.closing_type,'C') )
	where (z.work_order_id, z.revision, z.id, z.fcst_depr_version_id, nvl(z.closing_type,'C') ) in (
		select work_order_id, revision, max(id), fcst_depr_version_id,  nvl(closing_type,'C')
		from budget_closings_pct
		where fcst_depr_version_id = :a_fcst_depr_version
		and exists (
			select 1 from wo_est_processing_temp wept
			where wept.work_order_id = budget_closings_pct.work_order_id
			and wept.revision = budget_closings_pct.revision
			)
		having round(sum(percent) - 1,8) <> 0
		group by work_order_id, revision, fcst_depr_version_id, nvl(closing_type,'C')
	  )
	 ;
	 if uf_check_sql(' Updating Percent on Budget Closings PCT') <> 1 then return -1
	 
	update budget_closings_pct
	set percent = 0
	where percent is null
	and exists (
		select 1 from wo_est_processing_temp wept
		where wept.work_order_id = budget_closings_pct.work_order_id
		and wept.revision = budget_closings_pct.revision
		)
	;
	if uf_check_sql(' Updating Percent on Budget Closings PCT (Nulls)') <> 1 then return -1
	insert into fcst_budget_load(load_id, time_stamp,user_id, budget_version_id, fcst_depr_version_id, company_id, major_location_id, budget_plant_class_id,
	year, expenditure_type_id, closing_type,  january, february, march, april, may, june, july, august, september,
	october, november, december, total, budget_id, work_order_id,job_task_id,long_description, bus_segment_id, reconcile_item_id, book_summary_id, revision)
	select pwrplant1.nextval, sysdate, user, :budget_version_id,:a_fcst_depr_version, a.*
	from (
	  select bcs.company_id, bcs.major_location_id, bcp.budget_plant_class_id,substr(bcs.month_number,1,4),
	  decode(bcs.expenditure_type_id,6,2,bcs.expenditure_type_id),
	 bcs.closing_type,
	 sum(decode(substr(bcs.month_number,5,2),'01',current_month_closings,0) * bcp.percent) january,
	 sum(decode(substr(bcs.month_number,5,2),'02',current_month_closings,0) * bcp.percent) february,
	  sum(decode(substr(bcs.month_number,5,2),'03',current_month_closings,0) * bcp.percent) march,
	   sum(decode(substr(bcs.month_number,5,2),'04',current_month_closings,0) * bcp.percent) april,
		 sum(decode(substr(bcs.month_number,5,2),'05',current_month_closings,0) * bcp.percent) may,
		  sum(decode(substr(bcs.month_number,5,2),'06',current_month_closings,0) * bcp.percent) june,
		   sum(decode(substr(bcs.month_number,5,2),'07',current_month_closings,0) * bcp.percent) july,
			 sum(decode(substr(bcs.month_number,5,2),'08',current_month_closings,0) * bcp.percent) august,
			  sum(decode(substr(bcs.month_number,5,2),'09',current_month_closings,0) * bcp.percent) september,
			   sum(decode(substr(bcs.month_number,5,2),'10',current_month_closings,0) * bcp.percent) october,
				 sum(decode(substr(bcs.month_number,5,2),'11',current_month_closings,0) * bcp.percent) november,
				  sum(decode(substr(bcs.month_number,5,2),'12',current_month_closings,0) * bcp.percent) december,
				  	sum(bcs.current_month_closings * bcp.percent) total,				  
			decode(upper(:a_table_name),'BUDGET_MONTHLY_DATA',bcs.work_order_id,(select budget_id from work_order_control where work_order_id = bcs.work_order_id)), 
			decode(upper(:a_table_name),'BUDGET_MONTHLY_DATA',null,bcs.work_order_id), 
	bcs.job_task_id, 'Staged Detail Closings', bcs.bus_segment_id, 
		(select reconcile_item_id from book_summary bs where bs.book_summary_id = bcs.book_summary_id), bcs.book_summary_id, bcp.revision
	from budget_closings_pct bcp, budget_closings_temp bcs
	where bcp.work_order_id = bcs.work_order_id
	  and bcp.revision = bcs.revision
	  and bcp.fcst_depr_version_id =:a_fcst_depr_version
	  and nvl(bcp.closing_type,decode(bcs.closing_type,'V','R',bcs.closing_type)) = decode(bcs.closing_type,'V','R',bcs.closing_type)/*Salvage should follow the removal closes*/
	  group by   bcs.company_id, bcs.major_location_id, bcp.budget_plant_class_id,substr(bcs.month_number,1,4), decode(bcs.expenditure_type_id,6,2,bcs.expenditure_type_id),
	 bcs.closing_type,bcs.work_order_id,
	bcs.job_task_id, 'Staged Detail Closings', bcs.bus_segment_id, 
		bcs.book_summary_id, bcp.revision
	having  sum(decode(substr(bcs.month_number,5,2),'01',current_month_closings,0) * bcp.percent) <> 0 or
	 sum(decode(substr(bcs.month_number,5,2),'02',current_month_closings,0) * bcp.percent)  <> 0 or
	  sum(decode(substr(bcs.month_number,5,2),'03',current_month_closings,0) * bcp.percent)  <> 0 or
	   sum(decode(substr(bcs.month_number,5,2),'04',current_month_closings,0) * bcp.percent)  <> 0 or
		 sum(decode(substr(bcs.month_number,5,2),'05',current_month_closings,0) * bcp.percent)  <> 0 or
		  sum(decode(substr(bcs.month_number,5,2),'06',current_month_closings,0) * bcp.percent)  <> 0 or
		   sum(decode(substr(bcs.month_number,5,2),'07',current_month_closings,0) * bcp.percent)  <> 0 or
			 sum(decode(substr(bcs.month_number,5,2),'08',current_month_closings,0) * bcp.percent)  <> 0 or
			  sum(decode(substr(bcs.month_number,5,2),'09',current_month_closings,0) * bcp.percent)  <> 0 or
			   sum(decode(substr(bcs.month_number,5,2),'10',current_month_closings,0) * bcp.percent)  <> 0 or
				 sum(decode(substr(bcs.month_number,5,2),'11',current_month_closings,0) * bcp.percent)  <> 0 or
				  sum(decode(substr(bcs.month_number,5,2),'12',current_month_closings,0) * bcp.percent)  <> 0 
	) a
	;
if uf_check_sql(' Load Closings') <> 1 then return -1
else
		insert into fcst_budget_load(load_id, time_stamp,user_id, budget_version_id, fcst_depr_version_id, company_id, major_location_id, budget_plant_class_id,
	year, expenditure_type_id, closing_type,  january, february, march, april, may, june, july, august, september,
	october, november, december, total, budget_id, work_order_id,job_task_id,long_description, bus_segment_id, reconcile_item_id, book_summary_id, revision)
	select pwrplant1.nextval, sysdate, user, :budget_version_id,:a_fcst_depr_version, a.*
	from (
	  select bcs.company_id, bcs.major_location_id, bcs.budget_plant_class_id,substr(bcs.month_number,1,4), decode(bcs.expenditure_type_id,6,2,bcs.expenditure_type_id),
	 bcs.closing_type,
	 sum(decode(substr(bcs.month_number,5,2),'01',current_month_closings,0) ) january,
	 sum(decode(substr(bcs.month_number,5,2),'02',current_month_closings,0) ) february,
	  sum(decode(substr(bcs.month_number,5,2),'03',current_month_closings,0) ) march,
	   sum(decode(substr(bcs.month_number,5,2),'04',current_month_closings,0) ) april,
		 sum(decode(substr(bcs.month_number,5,2),'05',current_month_closings,0) ) may,
		  sum(decode(substr(bcs.month_number,5,2),'06',current_month_closings,0) ) june,
		   sum(decode(substr(bcs.month_number,5,2),'07',current_month_closings,0) ) july,
			 sum(decode(substr(bcs.month_number,5,2),'08',current_month_closings,0) ) august,
			  sum(decode(substr(bcs.month_number,5,2),'09',current_month_closings,0) ) september,
			   sum(decode(substr(bcs.month_number,5,2),'10',current_month_closings,0) ) october,
				 sum(decode(substr(bcs.month_number,5,2),'11',current_month_closings,0) ) november,
				  sum(decode(substr(bcs.month_number,5,2),'12',current_month_closings,0) ) december,
				  	sum(bcs.current_month_closings ) total,				  
			decode(upper(:a_table_name),'BUDGET_MONTHLY_DATA',bcs.work_order_id,(select budget_id from work_order_control where work_order_id = bcs.work_order_id)), 
			decode(upper(:a_table_name),'BUDGET_MONTHLY_DATA',null,bcs.work_order_id), 
	bcs.job_task_id, 'Staged Detail Closings', bcs.bus_segment_id, 
		(select reconcile_item_id from book_summary bs where bs.book_summary_id = bcs.book_summary_id), bcs.book_summary_id, bcs.revision
	from  budget_closings_temp bcs
	where month_number > :a_actuals_month_number
	  group by   bcs.company_id, bcs.major_location_id, bcs.budget_plant_class_id,substr(bcs.month_number,1,4), decode(bcs.expenditure_type_id,6,2,bcs.expenditure_type_id),
	 bcs.closing_type,bcs.work_order_id,
	bcs.job_task_id, 'Staged Detail Closings', bcs.bus_segment_id, 
		bcs.book_summary_id, bcs.revision
	having sum(decode(substr(bcs.month_number,5,2),'01',current_month_closings,0) ) <> 0 or
	 sum(decode(substr(bcs.month_number,5,2),'02',current_month_closings,0) ) <> 0 or
	  sum(decode(substr(bcs.month_number,5,2),'03',current_month_closings,0) ) <> 0 or
	   sum(decode(substr(bcs.month_number,5,2),'04',current_month_closings,0) ) <> 0 or
		 sum(decode(substr(bcs.month_number,5,2),'05',current_month_closings,0) ) <> 0 or
		  sum(decode(substr(bcs.month_number,5,2),'06',current_month_closings,0) ) <> 0 or
		   sum(decode(substr(bcs.month_number,5,2),'07',current_month_closings,0) ) <> 0 or
			 sum(decode(substr(bcs.month_number,5,2),'08',current_month_closings,0) ) <> 0 or
			  sum(decode(substr(bcs.month_number,5,2),'09',current_month_closings,0) ) <> 0 or
			   sum(decode(substr(bcs.month_number,5,2),'10',current_month_closings,0) ) <> 0 or
				 sum(decode(substr(bcs.month_number,5,2),'11',current_month_closings,0) ) <> 0 or
				  sum(decode(substr(bcs.month_number,5,2),'12',current_month_closings,0) ) <> 0 
	
	) a
	;
	if uf_check_sql(' Load Closings') <> 1 then return -1

end if



// ### DJL - Maint 9947: 04.20.2012
// Don?t wipe the table that stores the details behind how the closings (107 -> 101) are calculated. 
if not i_restrict then
	delete from budget_closings a
	where exists (
		select 1
		from budget_version_fund_proj bvfp
		where bvfp.work_order_id = a.work_order_id
		and bvfp.revision = a.revision
		and bvfp.budget_Version_id = :budget_version_id)
	and fcst_depr_version_id = :a_fcst_depr_version 
	;
	if uf_check_sql(' Delete from budget_closings') <> 1 then return -1
else
	delete from budget_closings a
	where exists (
		select 1
		from budget_version_fund_proj bvfp, work_order_control woc
		where bvfp.work_order_id = a.work_order_id
		and bvfp.revision = a.revision
		and bvfp.budget_Version_id = :budget_version_id
		and bvfp.work_order_id = woc.work_order_id
		and exists (select 1 from fcst_depr_group_version fdgv 
						where fdgv.fcst_depr_version_id =  :a_fcst_depr_version 
							and  fdgv.bus_segment_id = woc.bus_segment_id
							and  fdgv.company_id = woc.company_id)
			)
	and fcst_depr_version_id = :a_fcst_depr_version
	;
	if uf_check_sql(' Delete from budget_closings') <> 1 then return -1
end if

insert into budget_closings
select * from budget_closings_temp
;

if uf_check_sql(' insert into budget_closings') <> 1 then return -1





return 1
end function

on uo_budget_afudc.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_budget_afudc.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

