HA$PBExportHeader$uo_wo_initiate.sru
$PBExportComments$8186-fix insert into work_order_initiator
forward
global type uo_wo_initiate from nonvisualobject
end type
end forward

global type uo_wo_initiate from nonvisualobject
end type
global uo_wo_initiate uo_wo_initiate

type variables
boolean i_messagebox_onerror = true
boolean i_statusbox_onerror = false
boolean i_status_box = false
boolean i_pp_msgs = false
boolean i_add_time_to_messages = false
boolean i_display_msg_on_sql_success = false
boolean i_display_rowcount = false

// Variables used in uf_wo_initiate
longlong company_id, budget_id, funding_wo_id, temp_co, cnt_proj, funding_wo_indicator, &
	bus_segment_id, reason_cd_id, major_location_id, asset_location_id, late_chg_wait_period, &
	department_id, agreemnt_id, work_order_grp_id, estimating_option_id, closing_option_id, &
	alloc_method_type_id, eligible_for_afudc, afudc_type_id, eligible_for_cpi, cwip_gl_account, &
	expense_gl_account, non_unitized_gl_account, unitized_gl_account, jobbing_gl_account, &
	retirement_gl_account, salvage_gl_account, removal_gl_account, func_class_id, reimbursable_type_id, &
	unitize_by_account, wo_est_hierarchy_id, est_unit_item_option, work_type_id, approval_type_id, &
	tolerance_id, closing_pattern_id, accrual_type_id, repair_exclude, rwip_type_id, bs_counter, &
	wo_id, tax_expense_test_id, repair_location_method_id, budget_review_type_id, copy_from_wo_id, unit_item_from_estimate
decimal wo_type_id
string descr, long_descr, wo_number, inherit, cr_deriver_type_detail, cr_deriver_type_estimate, &
	workflow, null_str, require_asset_loc, update_bud_sum, update_bud_org, workflow_budg_review, &
	sqls, column_list, notes, initiator
datetime est_start_date, est_complete_date, est_in_service_date, afudc_start_date, afudc_stop_date
any results[]

end variables

forward prototypes
public function longlong uf_check_sql (string a_msg)
public subroutine uf_msg (string a_msg, string a_error)
public subroutine uf_reset_vars ()
public function longlong uf_wo_initiate (ref s_wo_initiate a_wos[])
public function longlong uf_get_vars (ref s_wo_initiate a_wos)
public function longlong uf_delete (longlong a_wo_id, string a_delete_type)
end prototypes

public function longlong uf_check_sql (string a_msg);string message_string

message_string = a_msg

if sqlca.sqlcode < 0 then
	uf_msg('SQL ERROR: '+message_string+' - '+sqlca.sqlerrtext,'E')
	rollback;
	return -1
else
	if i_display_msg_on_sql_success then
		if i_display_rowcount then
			message_string = message_string+' : processed '+string(sqlca.sqlnrows)+' rows '
		end if
		
		uf_msg(message_string,'I')
	end if		
	return 1
end if

end function

public subroutine uf_msg (string a_msg, string a_error);string message_title, time_string

message_title = 'Initiation Processing'

if i_add_time_to_messages then
	time_string = string(now())
	a_msg = time_string+' '+a_msg
end if

if g_main_application then
	if a_error = 'E' then
		if i_statusbox_onerror then
			f_status_box(message_title,a_msg)
		end if
		if i_messagebox_onerror then
			messagebox(message_title,a_msg)
		end if
		rollback;
	else
		if i_status_box then
			f_status_box(message_title,a_msg)
		end if
	end if
end if

if i_pp_msgs or (not g_main_application) then
	f_pp_msgs(a_msg)
end if

end subroutine

public subroutine uf_reset_vars ();any null_any_array[]

setnull(company_id)
setnull(wo_type_id)
setnull(budget_id)
setnull(funding_wo_id)
setnull(temp_co)
setnull(cnt_proj)
setnull(funding_wo_indicator)
setnull(bus_segment_id)
setnull(reason_cd_id)
setnull(major_location_id)
setnull(asset_location_id)
setnull(late_chg_wait_period)
setnull(department_id)
setnull(agreemnt_id)
setnull(work_order_grp_id)
setnull(estimating_option_id)
setnull(closing_option_id)
setnull(alloc_method_type_id)
setnull(eligible_for_afudc)
setnull(afudc_type_id)
setnull(eligible_for_cpi)
setnull(cwip_gl_account)
setnull(expense_gl_account)
setnull(non_unitized_gl_account)
setnull(unitized_gl_account)
setnull(jobbing_gl_account)
setnull(retirement_gl_account)
setnull(salvage_gl_account)
setnull(removal_gl_account)
setnull(func_class_id)
setnull(reimbursable_type_id)
setnull(unitize_by_account)
setnull(wo_est_hierarchy_id)
setnull(est_unit_item_option)
setnull(work_type_id)
setnull(approval_type_id)
setnull(tolerance_id)
setnull(closing_pattern_id)
setnull(accrual_type_id)
setnull(repair_exclude)
setnull(rwip_type_id)
setnull(bs_counter)
setnull(wo_id)
setnull(descr)
setnull(long_descr)
setnull(wo_number)
setnull(inherit)
setnull(cr_deriver_type_detail)
setnull(cr_deriver_type_estimate)
setnull(workflow)
setnull(null_str)
setnull(est_start_date)
setnull(est_complete_date)
setnull(est_in_service_date)
setnull(afudc_start_date)
setnull(afudc_stop_date)
setnull(require_asset_loc)
setnull(update_bud_sum)
setnull(update_bud_org)
setnull(workflow_budg_review)
setnull(budget_review_type_id)
setnull(copy_from_wo_id)
setnull(sqls)
setnull(column_list)
setnull(notes)
setnull(initiator)
setnull(tax_expense_test_id)
results = null_any_array

end subroutine

public function longlong uf_wo_initiate (ref s_wo_initiate a_wos[]);//******************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//		Function:				uf_wo_initiate
//
//		Purpose:				Initiate a Funding Project and optionally its corresponding Budget Item
//								OR
//								Initiate a Work Order
//
//		Arguments:			s_wo_initiate:		Please open S_WO_INITIATE to see a description of each parameter and under
//														what circumstances each is required to be filled in.
//
//		Assumptions:		Regarding Company
//								- If Initiating WO, Company always comes from parent FP
//								- If Initiating FP, Company comes first from the FP Type
//								- If FP Type Company is blank, Company must be passed in as an argument
//
//								Regarding Owning Department, Locations, and Estimated Dates
//								- Variables passed in take precendence
//								- If any variables are null, then if copying from another WO/FP, get the variables from the one being copied
//								- If any variables are still null, try to get the values from the WOT or Inheritance
//
//								INHERITING for WO Initiation
//								- If inheriting there are a limited number of fields that are inherited from the FP versus coming from the WOT
//								- Some fields always come from the parent FP
//								- Some fields always come from the WOT
//
//		Return Codes:		-2: invalid company
//								-3: invalid funding project
//								-4: invalid company / bus segment relationship
//								-5: invalid major location
//								-6: invalid department
//								-7: invalid description
//								-8: invalid dates - only if validates are turned on
//								-9: invalid work order type
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*******************************************************************************************
longlong i, j, rtn
uo_budget_revision uo_rev, uo_rev_null
uo_rev_null = create uo_budget_revision
uo_workflow_tools uo_tools, uo_tools_review, uo_tools_null
uo_tools_null = create uo_workflow_tools

for i = 1 to upperbound(a_wos)
	uo_rev = uo_rev_null
	uo_tools = uo_tools_null
	uo_tools_review = uo_tools_null
	
	rtn = uf_get_vars(a_wos[i])
	if rtn <> 1 then
		return rtn
	end if
	
	//
	// Data Validations
	//
	if isnull(major_location_id) or major_location_id = 0 then
		return -5
	end if
	
	if isnull(department_id) or department_id = 0 then
		return -6
	end if
	
	if isnull(descr) or trim(descr) = "" then
		return -7
	end if
	
	//
	// Check if we have a valid company / bus segment relationship
	//
	select count(*) into :bs_counter
	from company_bus_segment_control 
	where company_id = :company_id
	and bus_segment_id = :bus_segment_id;
	
	if bs_counter <> 1 then
		return -4
	end if
	
	//
	// Check if we have valid dates
	//
	if isnull(est_start_date) then est_start_date = datetime(today())
	if isnull(est_in_service_date) then est_in_service_date = datetime(today())
	if isnull(est_complete_date) then est_complete_date = datetime(today())
	
	if isnull(est_start_date) or isnull(est_complete_date) or isnull(est_in_service_date) &
	or not ( est_start_date <= est_in_service_date and est_in_service_date <= est_complete_date ) then
		return -8
	end if
	
	//
	// Budet item is auto-created - update Budget Summary and Budget Organization based on system controls
	//
	if isnull(funding_wo_id) or funding_wo_id = 0 then
		if isnull(budget_id) or budget_id = 0 then
			select pwrplant1.nextval
			into :budget_id
			from dual;
			
			insert into budget
				(budget_id, budget_number, budget_status_id, major_location_id, long_description, 
				 company_id, description, resp_person, department_id, bus_segment_id, afudc_type_id)
			select
				:budget_id, :wo_number, 1 /*budget_status_id*/, :major_location_id, :long_descr,
				:company_id, :descr, user, :department_id, :bus_segment_id, :afudc_type_id
			from dual;
			
			if uf_check_sql('Inserting into budget') = -1 then return -1
			
			update_bud_sum = f_pp_system_control_company('CAPBUD - WOT to Budget Summary',-1)
			if lower(update_bud_sum) = 'yes' then
				update budget set budget_summary_id = (
					select min(budget_summary_id)
					from work_order_type_budget_summary
					where work_order_type_id = :wo_type_id
					)
				where budget_id = :budget_id;
				
				if uf_check_sql('Updating budget.budget_summary_id') = -1 then return -1
			end if
			
			update_bud_org = f_pp_system_control_company('CAPBUD - WOG to Budget Org',-1)
			if lower(update_bud_org) = 'yes' then
				update budget set budget_organization_id = (
					select min(budget_organization_id)
					from work_order_group_budget_org
					where work_order_grp_id = :work_order_grp_id
					)
				where budget_id = :budget_id;
				if uf_check_sql('Updating budget.budget_organization_id') = -1 then return -1
			end if
		end if
	end if
	
	
	//
	// Insert into base tables
	//
	select pwrplant1.nextval
	into :wo_id
	from dual;
	
	a_wos[i].wo_id = wo_id
	
	
	// WORK_ORDER_CONTROL
	insert into work_order_control (
		work_order_id, work_order_number, company_id, bus_segment_id, budget_id, wo_status_id, work_order_type_id,
		reason_cd_id, funding_wo_id, major_location_id, asset_location_id, current_revision,
		description, long_description, funding_wo_indicator, est_start_date, est_complete_date, est_in_service_date,
		late_chg_wait_period, department_id )
	select :wo_id, :wo_number, :company_id, :bus_segment_id, :budget_id, 1 wo_status_id, :wo_type_id,
		:reason_cd_id, :funding_wo_id, :major_location_id, :asset_location_id, 0 current_revision,
		:descr, :long_descr, :funding_wo_indicator, :est_start_date, :est_complete_date, :est_in_service_date,
		:late_chg_wait_period, :department_id
	from dual;
	
	if uf_check_sql('Inserting into work_order_control') = -1 then return -1
	
	
	//###PATCH(11206) Removed repair_exclude
	// WORK_ORDER_ACCOUNT
	insert into work_order_account (
		work_order_id, agreemnt_id, tolerance_id, work_order_grp_id, estimating_option_id,
		closing_option_id, closing_pattern_id, alloc_method_type_id,
		eligible_for_afudc, afudc_type_id, afudc_start_date, afudc_stop_date, eligible_for_cpi,
		cwip_gl_account, expense_gl_account, non_unitized_gl_account, unitized_gl_account,
		jobbing_gl_account, retirement_gl_account, salvage_gl_account, removal_gl_account,
		func_class_id, reimbursable_type_id, unitize_by_account, cr_deriver_type_detail, cr_deriver_type_estimate,
		est_unit_item_option, work_type_id, accrual_type_id, rwip_type_id, wo_est_hierarchy_id, unit_item_from_estimate )
	select 
		:wo_id, :agreemnt_id, :tolerance_id, :work_order_grp_id, :estimating_option_id,
		:closing_option_id, :closing_pattern_id, :alloc_method_type_id,
		:eligible_for_afudc, :afudc_type_id, :afudc_start_date, :afudc_stop_date, :eligible_for_cpi,
		:cwip_gl_account, :expense_gl_account, :non_unitized_gl_account, :unitized_gl_account,
		:jobbing_gl_account, :retirement_gl_account, :salvage_gl_account, :removal_gl_account,
		:func_class_id, :reimbursable_type_id, :unitize_by_account, :cr_deriver_type_detail, :cr_deriver_type_estimate,
		:est_unit_item_option, :work_type_id, :accrual_type_id, :rwip_type_id, :wo_est_hierarchy_id, :unit_item_from_estimate
	from dual;
	
	if uf_check_sql('Inserting into work_order_account') = -1 then return -1
	

	//WORK_ORDER_TAX_REPAIRS
	insert into work_order_tax_repairs (
		work_order_id, tax_expense_test_id, repair_location_method_id 	)
	select
		:wo_id, :tax_expense_test_id, :repair_location_method_id
	from dual;

	if uf_check_sql('Inserting into work_order_tax_repairs') = -1 then return -1
	
	
	// WORK_ORDER_INITIATOR
	insert into work_order_initiator (
		work_order_id, users, initiation_date, responsibility_type )
	select
		:wo_id, nvl(:initiator, lower(user)), sysdate, 'prepare'
	from dual;
	
	if uf_check_sql('Inserting into work_order_initiator') = -1 then return -1
	
	
	// WORK_ORDER_APPROVAL
	delete from wo_est_processing_temp;
	insert into wo_est_processing_temp (work_order_id, revision)
	values (:wo_id, 0);
	
	uo_rev.i_messagebox_onerror = i_messagebox_onerror
	uo_rev.i_statusbox_onerror = i_statusbox_onerror
	uo_rev.i_status_box = i_status_box
	uo_rev.i_pp_msgs = i_pp_msgs
	uo_rev.i_add_time_to_messages = i_add_time_to_messages
	uo_rev.i_display_msg_on_sql_success = i_display_msg_on_sql_success
	uo_rev.i_display_rowcount = i_display_rowcount
	
	if funding_wo_indicator = 1 then
		rtn = uo_rev.uf_new_revision('fp')
	else
		rtn = uo_rev.uf_new_revision('wo')
	end if
	
	if rtn = -1 then return -1
	
	// Approval Setup/Defaults
	workflow = lower(trim(f_pp_system_control_company('Workflow User Object', company_id)))
	
	if workflow = 'yes' then
		uo_tools.i_messagebox_onerror = i_messagebox_onerror
		uo_tools.i_statusbox_onerror = i_statusbox_onerror
		uo_tools.i_status_box = i_status_box
		uo_tools.i_add_time_to_messages = i_add_time_to_messages
		uo_tools.i_pp_msgs = i_pp_msgs
		
		if funding_wo_indicator = 1 then
			uo_tools.uf_get_workflow('fp_approval', company_id, string(wo_id), string(1), null_str, null_str, null_str)
		else
			uo_tools.uf_get_workflow('wo_approval', company_id, string(wo_id), string(1), null_str, null_str, null_str)
		end if
		rtn = uo_tools.uf_new_workflow(approval_type_id)
		
		if rtn = -1 then return -1
	end if
	
	// Budget Review Setup/Defaults
	if funding_wo_indicator = 1 then
		workflow_budg_review = lower(trim(f_pp_system_control_company('Workflow User Object-Budg Review', company_id)))
		
		if workflow_budg_review = 'yes' then
			uo_tools_review.i_messagebox_onerror = i_messagebox_onerror
			uo_tools_review.i_statusbox_onerror = i_statusbox_onerror
			uo_tools_review.i_status_box = i_status_box
			uo_tools_review.i_add_time_to_messages = i_add_time_to_messages
			uo_tools_review.i_pp_msgs = i_pp_msgs
			
			uo_tools_review.uf_get_workflow('budg_review', company_id, string(wo_id), string(1), null_str, null_str, null_str)
			
			rtn = uo_tools_review.uf_new_workflow(budget_review_type_id)
			
			if rtn = -1 then return -1
		end if
		
	end if
	
	
	// FUNDING_JUSTIFICATION
	insert into funding_justification (work_order_id)
	select :wo_id
	from dual;
	
	if uf_check_sql('Inserting into funding_justification') = -1 then return -1
	
	
	// WORK_ORDER_CLASS_CODE DEFAULTS
	insert into work_order_class_code (
		work_order_id, class_code_id, value )
	select
		:wo_id, class_code_id, value
	from wo_type_class_code_default
	where work_order_type_id = :wo_type_id
	and trim(value) is not null;
	
	if uf_check_sql('Inserting defaults into work_order_class_code') = -1 then return -1
	
	if funding_wo_indicator = 0 then
		delete from work_order_class_code
		where work_order_id = :wo_id
		and class_code_id in (
			select class_code_id
			from work_order_class_code
			where work_order_id = :funding_wo_id
			and class_code_id in (
				select class_code_id
				from class_code
				where inherit_from_fp = 1
				and cwip_indicator = 1
				)
			and trim(value) is not null
			);
		
		if uf_check_sql('deleting defaults from work_order_class_code') = -1 then return -1
		
		insert into work_order_class_code (
			work_order_id, class_code_id, value )
		select :wo_id, class_code_id, value
		from work_order_class_code
		where work_order_id = :funding_wo_id
		and class_code_id in (
			select class_code_id
			from class_code
			where inherit_from_fp = 1
			and cwip_indicator = 1
			)
		and trim(value) is not null;
		
		if uf_check_sql('inserting inheritances into work_order_class_code') = -1 then return -1
	end if
	
	
	// JOB_TASK DEFAULTS
	insert into job_task (
		work_order_id, job_task_id, description, parent_job_task,
		chargeable, forecast, estimate, level_number, status_code_id,
		est_start_date, est_complete_date )
	select :wo_id, jtl.job_task_id, jtl.description, parent_job_task,
		chargeable, forecast, estimate, level_number, 1 status_code_id,
		:est_start_date, :est_complete_date
	from job_task_list jtl, job_task_default jtd
	where jtl.job_task_id = jtd.job_task_id
	and jtd.work_order_type_id = :wo_type_id;
	
	if uf_check_sql('Inserting into job_task') = -1 then return -1
	
	
	// ESTIMATES DEFAULTS
	f_wo_est_template_initiate(wo_id, wo_type_id)
	
	
	//
	// COPY VARIABLES FROM EXISTING WO/FP
	//
	if copy_from_wo_id > 0 then
		
		// COPY JUSTIFICATIONS
		if a_wos[i].copy_vars.copy_just then
			update funding_justification
			set (REASON_FOR_WORK, BACKGROUND, FUTURE_PROJECTS, ALTERNATIVES, FINANCIAL_ANALYSIS) = (         
				select REASON_FOR_WORK, BACKGROUND, FUTURE_PROJECTS, ALTERNATIVES, FINANCIAL_ANALYSIS
				from funding_justification
				where work_order_id = :copy_from_wo_id
				)
			where work_order_id = :wo_id;
			
			if uf_check_sql('Copying funding_justification') = -1 then return -1
			
			insert into wo_documentation (work_order_id, document_id, document_notes, document_stage_id, document_status_id, est_revision, tab_indicator)
			select :wo_id, document_id, document_notes, document_stage_id, document_status_id, decode(t.doc_tied_to_revision, 1, 1, 0), outside.tab_indicator
			from wo_documentation outside, wo_doc_justification_tabs t
			where outside.tab_indicator = t.tab_indicator
			and not exists (
				select 1 from wo_documentation a
				where outside.document_id = a.document_id
				and outside.tab_indicator = a.tab_indicator
				and a.work_order_id = :wo_id)
			and work_order_id = :copy_from_wo_id;
			
			if uf_check_sql('Copying wo_documentation') = -1 then return -1
			
			insert into wo_doc_justification (work_order_id,justification_id, document_id, justification_detail, orig_input_date,orig_input_user,not_changed)
			select :wo_id, justification_id, document_id, justification_detail, orig_input_date, orig_input_user, not_changed
			from wo_doc_justification oustide
			where not exists (
				select 1 from wo_doc_justification a
				where oustide.document_id = a.document_id
				and oustide.justification_id = a.justification_id
				and a.work_order_id = :wo_id
				)
			and work_order_id = :copy_from_wo_id;
			
			if uf_check_sql('Copying wo_doc_justification') = -1 then return -1
		end if
		
		// COPY CONTACTS
		if a_wos[i].copy_vars.copy_contact then
			update work_order_initiator
			set (proj_mgr, plant_accountant, contract_adm, engineer, other,
				notify_proj_mgr, notify_plant_accountant, notify_contract_adm, notify_engineer, notify_other
				) = (
				select proj_mgr, plant_accountant, contract_adm, engineer, other, 
					notify_proj_mgr, notify_plant_accountant, notify_contract_adm, notify_engineer, notify_other
				from work_order_initiator
				where work_order_id = :copy_from_wo_id
				)
			where work_order_id = :wo_id;
			
			if uf_check_sql('Copying work_order_initiator') = -1 then return -1
		end if
		
		// COPY APPROVALS
		if a_wos[i].copy_vars.copy_approval then
			update work_order_approval
			set (approval_type_id, authorizer1, authorizer2, authorizer3, authorizer4, authorizer5, authorizer6, authorizer7, authorizer8, authorizer9, authorizer10) = (
				select approval_type_id, authorizer1, authorizer2, authorizer3, authorizer4, authorizer5, authorizer6, authorizer7, authorizer8, authorizer9, authorizer10
				from work_order_approval
				where work_order_id = :copy_from_wo_id
				and revision = (
					select max(revision)
					from work_order_approval
					where work_order_id = :copy_from_wo_id
					)
				)
			where work_order_id = :wo_id
			and revision = 1;
			
			if uf_check_sql('Copying work_order_approval') = -1 then return -1
		end if
		
		// COPY TASKS
		if a_wos[i].copy_vars.copy_task then
			delete from job_task
			where work_order_id = :wo_id;
			
			if uf_check_sql('Copying job_task (delete)') = -1 then return -1
			
			sqls = 'insert into JOB_TASK '
			rtn = f_get_column(results, "select upper(column_name) from sys.all_tab_columns where owner = 'PWRPLANT' and table_name = 'JOB_TASK' order by column_id")
			column_list = f_parsestringarrayintostring(results)
			sqls += '(' + column_list + ') '
			sqls += 'select ' + f_replace_string(column_list, 'WORK_ORDER_ID', string(wo_id), 'all') + ' '
			sqls += 'from JOB_TASK '
			sqls += 'where work_order_id = ' + string(copy_from_wo_id)
			
			execute immediate :sqls;
			
			if uf_check_sql('Copying job_task (insert)') = -1 then return -1
			
			insert into job_task_class_code (work_order_id, job_task_id, class_code_id, value)
			select :wo_id, job_task_id, class_code_id, value
			from job_task_class_code
			where work_order_id = :copy_from_wo_id;
			
			if uf_check_sql('Copying job_task_class_code') = -1 then return -1
		end if
		
		// COPY DEPARTMENTS
		if a_wos[i].copy_vars.copy_dept then
			sqls = 'insert into WORK_ORDER_DEPARTMENT '
			rtn = f_get_column(results, "select upper(column_name) from sys.all_tab_columns where owner = 'PWRPLANT' and table_name = 'WORK_ORDER_DEPARTMENT' order by column_id")
			column_list = f_parsestringarrayintostring(results)
			sqls += '(' + column_list + ') '
			sqls += 'select ' + f_replace_string(column_list, 'WORK_ORDER_ID', string(wo_id), 'all') + ' '
			sqls += 'from WORK_ORDER_DEPARTMENT '
			sqls += 'where work_order_id = ' + string(copy_from_wo_id)
			
			execute immediate :sqls;
			
			if uf_check_sql('Copying work_order_department') = -1 then return -1
		end if
		
		// COPY UNIT ESTIMATES
		if a_wos[i].copy_vars.copy_unit then
			delete from wo_estimate
			where work_order_id = :wo_id
			and revision <> 1000;
			
			sqls = 'insert into WO_ESTIMATE '
			rtn = f_get_column(results, "select upper(column_name) from sys.all_tab_columns where owner = 'PWRPLANT' and table_name = 'WO_ESTIMATE' order by column_id")
			column_list = f_parsestringarrayintostring(results)
			sqls += '(' + column_list + ') '
			// overwrite the work_order_id, estimate_id, and batch_unit_item_id
			column_list = f_replace_string(column_list, 'BATCH_UNIT_ITEM_ID', 'null', 'all')
			column_list = f_replace_string(column_list, 'WORK_ORDER_ID', string(wo_id), 'all')
			column_list = f_replace_string(column_list, 'ESTIMATE_ID', 'pwrplant1.nextval', 'all')
			column_list = f_replace_string(column_list, 'EST_MONTHLY_ID', 'null', 'all')
			column_list = f_replace_string(column_list, 'REVISION', '1', 'all')
			sqls += 'select ' + column_list + ' '
			sqls += 'from WO_ESTIMATE '
			sqls += 'where revision < 1000 '
			sqls += 'and work_order_id = ' + string(copy_from_wo_id) + ' '
			sqls += 'and revision = ( '
			sqls += '	select max(revision) from work_order_approval '
			sqls += '	where work_order_id = ' + string(copy_from_wo_id) + ' '
			sqls += '	) '
			
			execute immediate :sqls;
			
			if uf_check_sql('Copying wo_estimate (unit est)') = -1 then return -1
		end if
		
		// COPY AS-BUILT
		if a_wos[i].copy_vars.copy_as_built then
			delete from wo_estimate
			where work_order_id = :wo_id
			and revision = 1000;
			
			sqls = 'insert into WO_ESTIMATE '
			rtn = f_get_column(results, "select upper(column_name) from sys.all_tab_columns where owner = 'PWRPLANT' and table_name = 'WO_ESTIMATE' order by column_id")
			column_list = f_parsestringarrayintostring(results)
			sqls += '(' + column_list + ') '
			// overwrite the work_order_id, estimate_id, and batch_unit_item_id
			column_list = f_replace_string(column_list, 'BATCH_UNIT_ITEM_ID', 'null', 'all')
			column_list = f_replace_string(column_list, 'WORK_ORDER_ID', string(wo_id), 'all')
			column_list = f_replace_string(column_list, 'ESTIMATE_ID', 'pwrplant1.nextval', 'all')
			sqls += 'select ' + column_list + ' '
			sqls += 'from WO_ESTIMATE '
			sqls += 'where work_order_id = ' + string(copy_from_wo_id) + ' '
			sqls += 'and revision = 1000 '
			
			execute immediate :sqls;
			
			if uf_check_sql('Copying wo_estimate (as-built)') = -1 then return -1
		end if
		
		// COPY CLASS CODES
		if a_wos[i].copy_vars.copy_class_code then
			sqls = 'insert into WORK_ORDER_CLASS_CODE '
			rtn = f_get_column(results, "select upper(column_name) from sys.all_tab_columns where owner = 'PWRPLANT' and table_name = 'WORK_ORDER_CLASS_CODE' order by column_id")
			column_list = f_parsestringarrayintostring(results)
			sqls += '(' + column_list + ') '
			// overwrite the work_order_id
			sqls += 'select ' + f_replace_string(column_list, 'WORK_ORDER_ID', string(wo_id), 'all') + ' '
			sqls += 'from WORK_ORDER_CLASS_CODE wo '
			sqls += 'where work_order_id = ' + string(copy_from_wo_id)
			
			sqls += 'and not exists ( '
			sqls += '	select 1 from work_order_class_code c '
			sqls += '	where c.work_order_id = ' + string(wo_id) + ' '
			sqls += '	and c.class_code_id = wo.class_code_id '
			sqls += '	) '
			
			execute immediate :sqls;
			
			if uf_check_sql('Copying work_order_class_code') = -1 then return -1
		end if
		
		// COPY OVERHEADS
		if a_wos[i].copy_vars.copy_oh then
			sqls = 'insert into WO_CLEAR_OVER '
			rtn = f_get_column(results, "select upper(column_name) from sys.all_tab_columns where owner = 'PWRPLANT' and table_name = 'WO_CLEAR_OVER' order by column_id")
			column_list = f_parsestringarrayintostring(results)
			sqls += '(' + column_list + ') '
			// overwrite the work_order_id
			sqls += 'select ' + f_replace_string(column_list, 'WORK_ORDER_ID', string(wo_id), 'all') + ' '
			sqls += 'from WO_CLEAR_OVER '
			sqls += 'where work_order_id = ' + string(copy_from_wo_id)
			
			execute immediate :sqls;
			
			if uf_check_sql('Copying wo_clear_over') = -1 then return -1
		end if
		
	end if
	
	
	//
	// INITIATE JOINT WORK ORDERS, IF ANY
	//
	for j = 1 to upperbound(a_wos[i].joint_wos)
		rtn = f_wo_initiate_no_commit( &
			a_wos[i].joint_wos[j].wo_type_id, &
			a_wos[i].joint_wos[j].budget_id, &
			a_wos[i].joint_wos[j].funding_wo_id, &
			false /*inherit - NOT USED*/, &
			descr, &
			long_descr, &
			false /*funding_wo - NOT USED*/, &
			a_wos[i].joint_wos[j].wo_number, &
			0 /*budget_version_id - NOT USED*/ &
			)
		
		if rtn < 0 then
			return rtn
		end if
		
		update work_order_account
		set (agreemnt_id, tolerance_id) = (
			select agreemnt_id, tolerance_id
			from work_order_account
			where work_order_id = :wo_id
		)
		where work_order_id = :rtn;
		if uf_check_sql('Updating joint work order work_order_account') = -1 then return -1
		
		update work_order_control
		set (major_location_id, asset_location_id, wo_status_id, current_revision,
				est_start_date, est_in_service_date, est_complete_date) = (
			select major_location_id, asset_location_id, wo_status_id, current_revision,
				est_start_date, est_in_service_date, est_complete_date
			from work_order_control
			where work_order_id = :wo_id
		)
		where work_order_id = :rtn;
		if uf_check_sql('Updating joint work order work_order_control') = -1 then return -1
		
		update work_order_approval
		set (est_start_date, est_complete_date, est_in_service_date) = (
			select est_start_date, est_complete_date, est_in_service_date
			from work_order_control
			where work_order_id = :wo_id
		)
		where work_order_id = :rtn;
		if uf_check_sql('Updating joint work order work_order_approval') = -1 then return -1
		
		insert into co_tenancy_wo (work_order_id, co_tenant_wo)
		values (:wo_id, :rtn);
		if uf_check_sql('Inserting joing work order co_tenancy_wo') = -1 then return -1
		
	next
	
next

destroy uo_rev_null
destroy uo_tools_null

return 1

end function

public function longlong uf_get_vars (ref s_wo_initiate a_wos);uf_reset_vars()

company_id = a_wos.company_id
wo_type_id = a_wos.wo_type_id
budget_id = a_wos.budget_id
funding_wo_id = a_wos.funding_wo_id
descr = left(a_wos.descr,35)
long_descr = left(a_wos.long_descr,254)
wo_number = left(a_wos.wo_number,35)
department_id = a_wos.department_id
major_location_id = a_wos.major_location_id
asset_location_id = a_wos.asset_location_id
est_start_date = a_wos.est_start_date
est_complete_date = a_wos.est_complete_date
est_in_service_date = a_wos.est_in_service_date
initiator = a_wos.initiator

if est_start_date = datetime(date(1900, 1, 1)) then setnull(est_start_date)
if est_complete_date = datetime(date(1900, 1, 1)) then setnull(est_complete_date)
if est_in_service_date = datetime(date(1900, 1, 1)) then setnull(est_in_service_date)

if department_id = 0 then setnull(department_id)
if major_location_id = 0 then setnull(major_location_id)
if asset_location_id = 0 then setnull(asset_location_id)

if trim(initiator) = '' then setnull(initiator)

//
// COPY VARIABLES FROM EXISTING WO/FP
//
if a_wos.copy_vars.copy_from_wo_id > 0 then
	copy_from_wo_id = a_wos.copy_vars.copy_from_wo_id
	
	// ALWAYS COPY THESE ATTRIBUTES
	select nvl(:long_descr,long_description),
		notes,
		reason_cd_id
	into :long_descr, :notes, :reason_cd_id
	from work_order_control
	where work_order_id = :copy_from_wo_id;
	
	// COPY ESTIMATED DATES
	if a_wos.copy_vars.copy_est_dates then
		select nvl(:est_start_date,woc.est_start_date),
			nvl(:est_complete_date,woc.est_complete_date),
			nvl(:est_in_service_date,woc.est_in_service_date)
		into :est_start_date, :est_complete_date, :est_in_service_date
		from work_order_control woc
		where work_order_id = :copy_from_wo_id;
	end if
	
	// COPY LOCATIONS
	if a_wos.copy_vars.copy_location then
		select nvl(:major_location_id,major_location_id),
			nvl(:asset_location_id,asset_location_id)
		into :major_location_id, :asset_location_id
		from work_order_control
		where work_order_id = :copy_from_wo_id;
	end if
	
end if


//
// Validate a wo type was passed in
//
if isnull(wo_type_id) or wo_type_id = 0 then
	return -9
end if

//
// INITIATE A FUNDING PROJECT
//
if isnull(funding_wo_id) or funding_wo_id = 0 then
	
	funding_wo_indicator = 1
	setnull(funding_wo_id)
	
	//
	// Get the company_id
	//
	select company_id into :temp_co
	from work_order_type
	where work_order_type_id = :wo_type_id;
	
	if isnull(temp_co) or temp_co = 0 then
		company_id = company_id
	else
		company_id = temp_co
	end if
	
	if isnull(company_id) or company_id = 0 then
		return -2
	end if
	
	// Check that the department is valid for the company
	// First, try the department passed in as an argument
	// Then, try the department of the work order being copied (if applicable)
	select nvl(a.department_id,b.department_id)
	into :department_id
	from
		(	select max(d.department_id) department_id
			from department d, division div
			where d.division_id = div.division_id
			and nvl(div.company_id,:company_id) = :company_id
			and d.department_id = :department_id
		) a,
		(	// ALWAYS COPY THE OWNING DEPARTMENT
			select max(d.department_id) department_id
			from department d, division div, work_order_control woc
			where d.division_id = div.division_id
			and nvl(div.company_id,:company_id) = :company_id
			and d.department_id = woc.department_id
			and woc.work_order_id = :copy_from_wo_id
		) b;
	
	//
	// Check if we are inheriting from the FP Type (versus from the budget item by default when present)
	//
	inherit = lower(trim(f_pp_system_control_company("FPINIT-INHERIT FP TYPE",company_id)))
	if inherit = '' or isnull(inherit) or trim(inherit) = 'no' then
		inherit = 'no'
	else
		inherit = 'yes'
	end if
	
	//
	// Get the attributes
	//
	select
		//decode(trim(:descr),null,b.description,:descr),
		nvl(nvl(:department_id,b.department_id),(select department_id from pp_security_users where upper(trim(users)) = upper(trim(user)))),
		nvl(:major_location_id,b.major_location_id),
		decode(:inherit,'yes',nvl(wot.bus_segment_id,b.bus_segment_id),nvl(b.bus_segment_id,wot.bus_segment_id)),
		wot.agreemnt_id, wot.tolerance_id,
		(select max(wgwt.work_order_grp_id) from wo_grp_wo_type wgwt where wgwt.work_order_type_id = wot.work_order_type_id),
		wot.estimating_option_id, wot.closing_option_id, wot.closing_pattern_id, wot.alloc_method_type_id,
		wot.afc_elig_indicator, wot.default_afc_type_id, wot.cpi_elig_indicator,
		wot.cwip_gl_account, wot.expense_gl_account, wot.non_unitized_gl_account, wot.unitized_gl_account,
		wot.jobbing_gl_account, wot.retirement_gl_account, wot.salvage_gl_account, wot.removal_gl_account,
		wot.func_class_id, wot.approval_type_id, wot.reimbursable_type_id, wot.unitize_by_account,
		wot.cr_deriver_type_detail, wot.cr_deriver_type_estimate, wot.est_unit_item_option, wot.work_type_id,
		wot.accrual_type_id, wot.repair_exclude, wot.wo_est_hierarchy_id, wot.tax_expense_test_id, wot.budget_review_type_id,
		wot.repair_location_method_id
	into
		//:descr,
		:department_id,
		:major_location_id,
		:bus_segment_id,
		:agreemnt_id, :tolerance_id,
		:work_order_grp_id,
		:estimating_option_id, :closing_option_id, :closing_pattern_id, :alloc_method_type_id,
		:eligible_for_afudc, :afudc_type_id, :eligible_for_cpi,
		:cwip_gl_account, :expense_gl_account, :non_unitized_gl_account, :unitized_gl_account,
		:jobbing_gl_account, :retirement_gl_account, :salvage_gl_account, :removal_gl_account,
		:func_class_id, :approval_type_id, :reimbursable_type_id, :unitize_by_account,
		:cr_deriver_type_detail, :cr_deriver_type_estimate, :est_unit_item_option, :work_type_id,
		:accrual_type_id, :repair_exclude, :wo_est_hierarchy_id, :tax_expense_test_id, :budget_review_type_id,
		:repair_location_method_id
	from work_order_type wot left outer join budget b on b.budget_id = nvl(:budget_id,0)
	where wot.work_order_type_id = :wo_type_id;
	
	
//
//  INITIATE A WORK ORDER
//
else
	
	funding_wo_indicator = 0
	
	//
	// Check if the funding project exists
	// Get the company_id
	//
	select count(*), max(company_id)
	into :cnt_proj, :company_id
	from work_order_control
	where work_order_id = :funding_wo_id;
	
	if cnt_proj <> 1 then
		return -3
	end if
	
	if isnull(company_id) or company_id = 0 then
		return -2
	end if
	
	// Check that the department is valid for the company
	// First, try the department passed in as an argument
	// Then, try the department of the work order being copied (if applicable)
	select nvl(a.department_id,b.department_id)
	into :department_id
	from
		(	select max(d.department_id) department_id
			from department d, division div
			where d.division_id = div.division_id
			and nvl(div.company_id,:company_id) = :company_id
			and d.department_id = :department_id
		) a,
		(	// ALWAYS COPY THE OWNING DEPARTMENT
			select max(d.department_id) department_id
			from department d, division div, work_order_control woc
			where d.division_id = div.division_id
			and nvl(div.company_id,:company_id) = :company_id
			and d.department_id = woc.department_id
			and woc.work_order_id = :copy_from_wo_id
		) b;
	
	//
	// Check if we are inheriting
	//
	inherit = upper(trim(f_pp_system_control_company('Work Order Initiation', company_id)))
	if inherit = '' or isnull(inherit) or inherit = 'DO NOT INHERIT' then
		inherit = ''
	else
		inherit = 'INHERIT'
	end if
	
	//
	// Get the attributes
	//
	select
		/* INHERITING - THESE FIELDS OBEY THE SYSTEM CONTROL TO INHERIT FROM THE FUNDING PROJECT - OTHERWISE THEY COME FROM THE WORK ORDER TYPE */
		decode(:inherit,'INHERIT',nvl(woc.bus_segment_id,wot.bus_segment_id),nvl(wot.bus_segment_id,woc.bus_segment_id)),
		decode(:inherit,'INHERIT',nvl(woc.late_chg_wait_period,wot.late_chg_wait_period),nvl(wot.late_chg_wait_period,woc.late_chg_wait_period)),
		decode(:inherit,'INHERIT',nvl(woa.agreemnt_id,wot.agreemnt_id),nvl(wot.agreemnt_id,woa.agreemnt_id)),
		decode(:inherit,'INHERIT',nvl(woa.work_order_grp_id,(select max(wgwt.work_order_grp_id) from wo_grp_wo_type wgwt where wgwt.work_order_type_id = wot.work_order_type_id)),nvl((select max(wgwt.work_order_grp_id) from wo_grp_wo_type wgwt where wgwt.work_order_type_id = wot.work_order_type_id),woa.work_order_grp_id)),
		decode(:inherit,'INHERIT',nvl(woa.estimating_option_id,wot.estimating_option_id),nvl(wot.estimating_option_id,woa.estimating_option_id)),
		decode(:inherit,'INHERIT',nvl(woa.closing_option_id,wot.closing_option_id),nvl(wot.closing_option_id,woa.closing_option_id)),
		decode(:inherit,'INHERIT',nvl(woa.alloc_method_type_id,wot.alloc_method_type_id),nvl(wot.alloc_method_type_id,woa.alloc_method_type_id)),
		decode(:inherit,'INHERIT',nvl(woa.eligible_for_afudc,wot.afc_elig_indicator),nvl(wot.afc_elig_indicator,woa.eligible_for_afudc)),
		decode(:inherit,'INHERIT',nvl(woa.afudc_type_id,wot.default_afc_type_id),nvl(wot.default_afc_type_id,woa.afudc_type_id)),
		decode(:inherit,'INHERIT',nvl(woa.eligible_for_cpi,wot.cpi_elig_indicator),nvl(wot.cpi_elig_indicator,woa.eligible_for_cpi)),
		decode(:inherit,'INHERIT',nvl(woa.cwip_gl_account,wot.cwip_gl_account),nvl(wot.cwip_gl_account,woa.cwip_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.expense_gl_account,wot.expense_gl_account),nvl(wot.expense_gl_account,woa.expense_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.non_unitized_gl_account,wot.non_unitized_gl_account),nvl(wot.non_unitized_gl_account,woa.non_unitized_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.unitized_gl_account,wot.unitized_gl_account),nvl(wot.unitized_gl_account,woa.unitized_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.jobbing_gl_account,wot.jobbing_gl_account),nvl(wot.jobbing_gl_account,woa.jobbing_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.retirement_gl_account,wot.retirement_gl_account),nvl(wot.retirement_gl_account,woa.retirement_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.salvage_gl_account,wot.salvage_gl_account),nvl(wot.salvage_gl_account,woa.salvage_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.removal_gl_account,wot.removal_gl_account),nvl(wot.removal_gl_account,woa.removal_gl_account)),
		decode(:inherit,'INHERIT',nvl(woa.func_class_id,wot.func_class_id),nvl(wot.func_class_id,woa.func_class_id)),
		decode(:inherit,'INHERIT',nvl(woa.reimbursable_type_id,wot.reimbursable_type_id),nvl(wot.reimbursable_type_id,woa.reimbursable_type_id)),
		decode(:inherit,'INHERIT',nvl(woa.unitize_by_account,wot.unitize_by_account),nvl(wot.unitize_by_account,woa.unitize_by_account)),
		decode(:inherit,'INHERIT',nvl(woa.wo_est_hierarchy_id,wot.wo_est_hierarchy_id),nvl(wot.wo_est_hierarchy_id,woa.wo_est_hierarchy_id)),
		decode(:inherit,'INHERIT',nvl(woa.est_unit_item_option,wot.est_unit_item_option),nvl(wot.est_unit_item_option,woa.est_unit_item_option)),
		decode(:inherit,'INHERIT',nvl(woa.work_type_id,wot.work_type_id),nvl(wot.work_type_id,woa.work_type_id)),
		decode(:inherit,'INHERIT',nvl(a.approval_type_id,wot.approval_type_id),nvl(wot.approval_type_id,a.approval_type_id)),
		decode(:inherit,'INHERIT',nvl(wotr.tax_expense_test_id,wot.tax_expense_test_id),nvl(wot.tax_expense_test_id,wotr.tax_expense_test_id)),
		decode(:inherit,'INHERIT',nvl(wotr.repair_location_method_id,wot.repair_location_method_id),nvl(wot.repair_location_method_id,wotr.repair_location_method_id)),	
		decode(:inherit,'INHERIT',nvl(woa.unit_item_from_estimate,wot.unit_item_from_estimate),nvl(wot.unit_item_from_estimate,woa.unit_item_from_estimate)),
		
		/* THESE FIELDS CAN BE DEFAULTED FROM THE PARENT FUNDING PROJECT IF NOT PASSED IN */
		woc.budget_id, nvl(:reason_cd_id,woc.reason_cd_id), nvl(:major_location_id,woc.major_location_id), nvl(:asset_location_id,woc.asset_location_id),
		//decode(trim(:descr),null,woc.description,:descr),
		//decode(trim(:long_descr),null,woc.long_description,:long_descr),
		nvl(:est_start_date,woc.est_start_date), nvl(:est_complete_date,woc.est_complete_date), nvl(:est_in_service_date,woc.est_in_service_date), nvl(:department_id,woc.department_id),
		woa.afudc_start_date, woa.afudc_stop_date,
		/* THESE FIELDS ALWAYS FROM FROM THE WORK ORDER TYPE */
		wot.tolerance_id, wot.closing_pattern_id, wot.cr_deriver_type_detail, wot.cr_deriver_type_estimate,
		wot.accrual_type_id, wot.repair_exclude, wot.rwip_type_id
	/* THIS SQL IS THE SAME FROM THIS POINT DOWN WHETHER INHERITING OR NOT */
	into :bus_segment_id, :late_chg_wait_period, :agreemnt_id,
		:work_order_grp_id,
		:estimating_option_id, :closing_option_id,
		:alloc_method_type_id, :eligible_for_afudc, :afudc_type_id, :eligible_for_cpi,
		:cwip_gl_account, :expense_gl_account, :non_unitized_gl_account, :unitized_gl_account,
		:jobbing_gl_account, :retirement_gl_account, :salvage_gl_account, :removal_gl_account,
		:func_class_id, :reimbursable_type_id, :unitize_by_account, :wo_est_hierarchy_id,
		:est_unit_item_option, :work_type_id,
		:approval_type_id,
		:tax_expense_test_id,
		:repair_location_method_id,
		:unit_item_from_estimate,
		:budget_id, :reason_cd_id, :major_location_id, :asset_location_id,
		//:descr,
		//:long_descr,
		:est_start_date, :est_complete_date, :est_in_service_date, :department_id,
		:afudc_start_date, :afudc_stop_date,
		:tolerance_id, :closing_pattern_id, :cr_deriver_type_detail, :cr_deriver_type_estimate,
		:accrual_type_id, :repair_exclude, :rwip_type_id
	from work_order_control woc, work_order_account woa,
		(	select * from work_order_approval a
			where a.work_order_id = :funding_wo_id
			and a.revision = (
				select max(b.revision) from work_order_approval b
				where b.work_order_id = a.work_order_id
				)
		) a,
		(	select * from work_order_type wot
			where wot.work_order_type_id = :wo_type_id
		) wot,
		work_order_tax_repairs wotr		
	where woc.work_order_id = :funding_wo_id
	and woc.work_order_id = woa.work_order_id
	and woc.work_order_id = a.work_order_id (+)
	and woc.work_order_id = wotr.work_order_id (+)	;
	
end if // if isnull(funding_wo_id) or funding_wo_id = 0 then

return 1

end function

public function longlong uf_delete (longlong a_wo_id, string a_delete_type);//******************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//		Function:				uf_wo_delete
//
//		Purpose:				To delete work orders or funding projects. Function will perform validations/checks in selected order
//
//		Arguments:			a_wo_id:				wo_id - wo_order_id of the record being deleted
//								a_delete_type:		wo - Work Order/Funding Project delete
//														bi - Budget Item delete
//
//		Return Codes:		1: delete successful
//								-1: delete unsuccessful
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*******************************************************************************************

longlong fund_wo_ind, rtn, i, j, exception_flag, exception[], fp_id, bud_id, cnt, rtn2
string sqls2, delete_type, delete_tables[], table_name, where_clause, exception_message, rets, sort_string
any count[], empty_array[]

uo_ds_top ds_delete
ds_delete = create uo_ds_top

//Grab arguments
wo_id = a_wo_id
delete_type = lower(a_delete_type)


//Set Tables/Get Funding Wo Indicator
if delete_type = 'wo' then
	
	select funding_wo_indicator
	into :fund_wo_ind
	from work_order_control woc
	where work_order_id = :wo_id
	;
	if uf_check_sql('Checking funding_wo_indicator') = -1 then return -1
	
	//Store funding_wo_id (for further processing at the end)
	select funding_wo_id
	into :fp_id
	from work_order_control
	where work_order_id = :wo_id
	and funding_wo_indicator = 0
	;
	if uf_check_sql('Storing FP ID') = -1 then return -1
	
	//Store budget_id (for further processing at the end)
	select budget_id
	into :bud_id
	from work_order_control
	where work_order_id = :wo_id
	;
	if uf_check_sql('Storing Budget ID') = -1 then return -1
	
	delete_tables[1] = 'wo_delete_custom'
	delete_tables[2] = 'wo_delete'
	
elseif delete_type = 'bi' then
	fund_wo_ind = 2
	
	delete_tables[1] = 'budget_delete_custom'
	delete_tables[2] = 'budget_delete'
else
	messagebox('Invalid delete type', "Invalid delete type argument. 'wo' or 'budget' are the only valid options.")
	return -1
end if

//Loop through each table once
for i = 1 to UpperBound(delete_tables)
	
	//Create statement for DS
	sqls = 'select id, table_name, where_clause, exception_flag, exception_message, sort_order from '+delete_tables[i]
	if delete_type = 'wo' then
		sqls+= ' where funding_wo_indicator in (-1, ' + string(fund_wo_ind) + ')'
	end if
	
	
	//Create/Sort DS
	rets = f_create_dynamic_ds(ds_delete,"grid",sqls,sqlca,true)
	if rets <> 'OK' then
		uf_msg('Error creating datastore: '+rets,'E')
		rollback;
		return -1
	end if
	sort_string = 'sort_order'
	ds_delete.setsort(sort_string)
	ds_delete.sort()
	
	//Loop through DS rows
	for j = 1 to ds_delete.rowcount()
		
		//Clear/Recreate variables
		setnull(table_name)
		setnull(where_clause)
		setnull(exception_flag)
		setnull(exception_message)
		table_name = ds_delete.getitemstring(j, 'table_name')
		where_clause = ds_delete.getitemstring(j, 'where_clause')
		exception_flag = ds_delete.getitemnumber(j, 'exception_flag')
		exception_message = ds_delete.getitemstring(j, 'exception_message')
		
		//Create syntax statement
		sqls2 = table_name+' '+where_clause
		sqls2 = f_replace_string(sqls2, '<<wo_id>>', string(wo_id), 'all')
		
		//update sqls2 with delete or validation count
		if exception_flag = 0 then
			sqls2 = 'delete from ' + sqls2
			execute immediate :sqls2;
			if uf_check_sql('Deleting Record ') = -1 then return -1
		else
			sqls2 = 'select count(*) from ' + sqls2
			rtn = f_get_column(count[],sqls2)
			exception[] = count[]
			count[] = empty_array[]
			
			//If exception thrown then
			if exception[1] > 0 then
				if exception_flag = 1 then
					//Do Not Perform Anything
					rollback;
					messagebox('Process Ended', ' Record has not been deleted. ' + exception_message)
					return -1
				elseif exception_flag = 2 then
					//Check First
					rtn = messagebox("Delete Check", exception_message+" Are you sure you want to delete?", question!, yesno!)
					if rtn <> 1 then //If response = No
						rollback;
						messagebox('Process Ended', 'Record has not been deleted. ' + exception_message)
						return -1
					else //replace the count with a delete if response = Yes
						sqls2 = f_replace_string(sqls2, 'select count(*) from ', 'delete from ', 'first')
						execute immediate :sqls2;
						if uf_check_sql('Validating Record ') = -1 then return -1
					end if
				end if //if exception_flag = 1 then
			end if //if exception[1] > 0 then
		end if //if exception_flag = 0 then
		
	next //for j = 1 to ds_delete.rowcount()

next //for i = 1 to UpperBound(delete_tables)


commit;
messagebox("Success","Record Successfully Deleted.")

//If this was the only work order, see if user wants to also delete Funding Project (as longlong as no other work orders are associated to the funding project)
//If this was the only Funding Project, see if user wants to also delete Budget Item (as longlong as no other funding projects are associated to the budget item)
if fund_wo_ind = 0 then 
	select count(*)
	into :cnt
	from work_order_control
	where funding_wo_id = :fp_id
	;
	if uf_check_sql('Storing cnt argument') = -1 then return -1
	
	if cnt = 0 then
		rtn = messagebox("Delete FP", " Would you like to also delete the Funding Project?", question!, yesno!)
		if rtn = 1 then
			rtn2 = uf_delete(fp_id, 'wo')
			if rtn2 <> 1 then
				rollback;
				messagebox("Delete FP Error", "Unable to delete Funding Project")
			end if //rtn2 <> 1 then
		end if //rtn = 1 then
	end if //cnt = 0 then
	
elseif fund_wo_ind = 1 then
	
	select count(*)
	into :cnt
	from work_order_control
	where budget_id = :bud_id
	;
	if uf_check_sql('Storing cnt argument') = -1 then return -1
	
	if cnt = 0 then
		rtn = messagebox("Delete BI", " Would you like to also delete the Budget Item?", question!, yesno!)
		if rtn = 1 then
			rtn2 = uf_delete(bud_id, 'bi')
			if rtn2 <> 1 then
				rollback;
				messagebox("Delete BI Error", "Unable to delete Budget Item")
			end if //rtn2 <> 1 then
		end if //if rtn = 1 then
	end if //if cnt = 0 then
	
end if

return 1


end function

on uo_wo_initiate.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wo_initiate.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

