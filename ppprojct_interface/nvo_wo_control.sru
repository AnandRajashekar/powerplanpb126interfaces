HA$PBExportHeader$nvo_wo_control.sru
forward
global type nvo_wo_control from nonvisualobject
end type
end forward

global type nvo_wo_control from nonvisualobject autoinstantiate
end type

type variables
// Specific to nvo_wo_control
longlong i_company_idx[]
string i_company_descr[]
longlong i_array_position_of_failed_companies[]
longlong i_num_failed_companies
uo_ds_top i_ds_pp_interface_dates_check
uo_ds_top i_ds_interface_dates_all
uo_ds_top i_ds_wo_process_afudc_ovh
uo_ds_top i_ds_wo_control //this replaces i_dw

// From w_wo_control
uo_ds_top dw_wo_charges_summ
uo_ds_top i_ds_charges
uo_ds_top i_ds_wo_units
uo_ds_top i_ds_cpr_act_month

boolean i_bal_depreciation
boolean i_bal_summary
boolean i_bal_act_actbasis
boolean i_bal_ledger_ldgbasis
boolean i_bal_ledger_act
boolean i_bal_ledger_subl	
boolean i_bal_quantities
boolean i_bal_subl
boolean i_balanced_cancelled
boolean i_automatic, i_drag, i_need_to_untarget
boolean i_targeting, i_allocating, i_unitize_by_account
boolean i_oh_error
boolean i_split_oh_afudc
boolean i_calc, i_approve, i_afudc_error, i_no_summary
boolean i_late_cancel
boolean auto_106_after_101
boolean i_late_only, i_fast101_has_run

string i_dw_title
string i_dw_object
string i_where_cpr_company
string i_work_order_number
string i_filename_101
string i_filename_101_grp
string i_late_months_str
string i_filename_106
string i_wo_number
string i_late_opt
string i_wos_to_run
string i_late_charge_options
string i_yes_no_late

int i_col_num

longlong i_row
longlong i_company
longlong i_color
longlong i_unit_item_id, i_charge_ids_to_unitize[]
longlong i_wo_id
longlong i_month_number
longlong i_owned
longlong i_lb_company_selected
longlong i_run
longlong i_late_month_number
longlong i_process_id
longlong i_ovh_type 
longlong i_late_charge_mos
longlong i_num_basis, i_fast101_run_id

datetime i_month
datetime i_months[4]

decimal{2} i_amount, i_quantity, i_allo_amount

string i_ProcessString
end variables

forward prototypes
public function boolean of_checksystemcontrol (integer a_button_number)
public function integer of_selectedcompanies ()
public function integer of_updatedw ()
public subroutine of_constructor ()
public function boolean of_cleanup (longlong a_button_number, string a_verify_descr, string a_msg)
public function longlong of_getdescriptionsfromids (longlong a_company_ids[])
public function integer of_retrievecompany ()
public function integer of_updatedwnocommit ()
public function integer of_currentmonth (datetime a_current_month)
public function integer of_wo_summaries ()
public function integer of_retrievecompanyloop (longlong a_company_id, datetime a_month)
public function integer of_companychanged (integer a_index, datetime a_month)
public function longlong of_setupfromcompaniesandmonth (longlong a_company_ids[], date a_month)
public function string of_buildcheckstring (string a_exe_name, string a_processstring)
public function string of_buildprocessstring (string a_exe_name, longlong a_company_id[], longlong a_month_number)
public function boolean of_lockprocess (string a_exe_name, longlong a_company_id[], longlong a_month_number, ref string a_msg)
public function boolean of_releaseprocess (ref string a_msg)
public subroutine of_log_failed_companies (string a_log_header)
public subroutine of_add_to_failed_company_list (longlong a_company_index)
end prototypes

public function boolean of_checksystemcontrol (integer a_button_number);/************************************************************************************************************************************************************
**
**	of_checkSystemControl()
**	
**	This function corresponds to w_wo_control.wf_check_system_control(a_button_number).  This determines whether emails should be sent based on the
**	selected companies.
**	
**	Parameters	:	longlong	:	(a_button_number) Corresponds to the various buttons on w_wo_control
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
// If a_system_control is turned on for at least one selected company, then return TRUE.
// If at least one company is not set, but "all companies" is turned on, then return TRUE.
// Otherwise return FALSE

integer	num_companies, c
string	control_value
boolean	hasNull

longlong	start_pos, end_pos

start_pos = 1
end_pos	= 1
hasNull = FALSE
num_companies = upperbound(i_company_idx)
for c=1 to num_companies
   //   it's selected, update the rest of the window
   of_companyChanged(c, i_month)
   control_value = upper(f_pp_system_control_company("WO MONTHLY CLOSE EMAILS", i_company))
   if upper(trim(control_value)) = 'NO' then setnull(control_value)
   // There is an entry for the given company
	if isnull( control_value ) = false then
		// emails enabled for this process and this company
		
		do while end_pos > 0
			end_pos = pos(control_value, ",", start_pos)
			if end_pos > 0 then
				if Integer(mid(control_value, start_pos, end_pos - start_pos)) = a_button_number then return TRUE
			elseif end_pos = 0 then
				if Integer(mid(control_value, start_pos, len(control_value) - start_pos)) = a_button_number then return TRUE
			else
				return FALSE
			end if
			start_pos = end_pos + 1
//			if pos(control_value, String( a_button_number )) > 0 then
//				return TRUE
//			end if
		loop
	else
		hasNULL = TRUE
	end if
next

// If for all the selected companies had emails disabled
if hasNULL = false then 
	return FALSE
else
	//Some companies had no value (neither disabled nor enabled), check if it's enabled for all the companies
   control_value = upper(f_pp_system_control_company("WO MONTHLY CLOSE EMAILS", -1))
   if upper(trim(control_value)) = 'NO' then setnull(control_value)
   if pos(control_value, String( a_button_number )) > 0 then
      return TRUE
   end if

end if


//All the values are set to "NO" or the system control is not set
return FALSE
end function

public function integer of_selectedcompanies ();/************************************************************************************************************************************************************
**
**	of_selectedCompanies()
**	
**	This function corresponds to w_wo_control.wf_selected_companies() and has been modified to 
**	determine whether the companies in i_company_idx[] can be processed together.
**	
**	Parameters	:	(none)
**	
**	Returns		:	integer	: Success: The number of companies selected; Failure: -1 
**
************************************************************************************************************************************************************/
// returns number of companies selected, or -1 if the open/closed monthsdo not line up 

longlong num_companies, c, total_selected, company_id, owned, k, p
string text
longlong max_open_mo, max_closed_mo, open_mo, closed_mo
boolean split_afudc

k = 0
total_selected = 0
p = 0


num_companies = upperbound(i_company_idx)

for c=1 to num_companies
	k = k + 1
	
	total_selected = total_selected + 1
	
	//LLL: Trimming here
	text = trim(i_company_descr[c])

	select company_id, owned into :company_id, :owned 
	from company where ltrim(rtrim(description)) = :text;
	  
	select max(to_char(accounting_month, 'yyyymm')) into :max_open_mo
	from wo_process_control 
	where company_id = :company_id and powerplant_closed is null;
	
	select max(to_char(accounting_month, 'yyyymm')) into :max_closed_mo
	from wo_process_control 
	where company_id = :company_id and powerplant_closed is not null;
	
	select count(*) into :p from afudc_oh_process_control
	where company_id = :company_id;
	
	if k = 1 then
		
		open_mo = max_open_mo
		closed_mo = max_closed_mo
		if p = 1 then
			i_split_oh_afudc = true
		else
			i_split_oh_afudc = false
		end if
		
	else
		
		if open_mo <> max_open_mo  or closed_mo <>  max_closed_mo then
			
			return -1
		end if
		
		if i_split_oh_afudc and p <> 1 then
			return -1
		elseif not i_split_oh_afudc and p <> 0 then
			return -1 
		end if
		
	end if	
next 
	

return total_selected
end function

public function integer of_updatedw ();/************************************************************************************************************************************************************
**
**	of_updateDW()
**	
**	This function corresponds to w_wo_control.wf_update_dw(). Updates wo_control.
**	
**	Parameters	:	(none)
**	
**	Returns		:	integer	: Success: 1; Failure: -1
**
************************************************************************************************************************************************************/
int rtn


rtn = i_ds_wo_control.update()

if rtn = 1 then

  commit using sqlca;
  return 1

else

   rollback using sqlca;

   f_pp_msgs("Cannot update the WO Control table.")
   return -1

end if 
end function

public subroutine of_constructor ();/************************************************************************************************************************************************************
**
**	of_constructor()
**	
**	This function sets up the instance datastores.
**	
**	Parameters	:	(none)
**	
**	Returns		:	(none)
**
************************************************************************************************************************************************************/
// Set up all of the datastores
i_ds_wo_control = CREATE uo_ds_top
i_ds_wo_control.dataObject = 'dw_wo_process_control'
i_ds_wo_control.setTransObject(SQLCA)

i_ds_cpr_act_month = CREATE uo_ds_top
i_ds_cpr_act_month.dataObject = 'dw_cpr_act_month'
i_ds_cpr_act_month.setTransObject(SQLCA)

i_ds_interface_dates_all = CREATE uo_ds_top
i_ds_interface_dates_all.dataObject = 'dw_wo_interface_dates_all'
i_ds_interface_dates_all.setTransObject(SQLCA)

i_ds_wo_process_afudc_ovh = CREATE uo_ds_top
i_ds_wo_process_afudc_ovh.dataObject = 'dw_wo_process_afudc_ovh'
i_ds_wo_process_afudc_ovh.setTransObject(SQLCA)

i_ds_pp_interface_dates_check = CREATE uo_ds_top
i_ds_pp_interface_dates_check.dataObject = 'dw_pp_interface_dates_check'
i_ds_pp_interface_dates_check.setTransObject(SQLCA)
end subroutine

public function boolean of_cleanup (longlong a_button_number, string a_verify_descr, string a_msg);/************************************************************************************************************************************************************
**
**	of_cleanup()
**	
**	This function will send emails to notify the appropriate users that a process has completed.
**	
**	Parameters	:	longlong	:	(a_button_number) Corresponds to the various buttons on w_wo_control
**						string   :  (a_verify_descr) The description from pp_verify
**						string   :  (a_msg) The process name that has finished
**	
**	Returns		:	integer	: Success: The number of companies selected; Failure: -1 
**
************************************************************************************************************************************************************/
longlong i
string	find_str, user_id, sqls
boolean	vfy_users
uo_ds_top ds_users

if of_checkSystemControl( a_button_number ) = true then
	ds_users = create uo_ds_top
	vfy_users = false
	select nvl(user_sql, '') into :sqls from pp_verify where lower(description) = :a_verify_descr;
	if sqls <> "" then
		if f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true) = "OK" then
			if ds_users.RowCount() > 0 then
				vfy_users = true
			end if
		end if
	end if

	if vfy_users = true then
		for i=1 to ds_users.RowCount()
			user_id = ds_users.GetItemString( i, 1)
			f_send_mail( user_id, a_msg + " completed", &
			a_msg + " completed   on " + String(Today(), "mm/dd/yyyy") + &
			" at " + String(Today(), "hh:mm:ss"), user_id)
		next
	else
		f_send_mail( s_user_info.user_id, a_msg + " completed", &
		a_msg + " completed   on " + String(Today(), "mm/dd/yyyy") + &
		" at " + String(Today(), "hh:mm:ss"), s_user_info.user_id)
	end if
end if

return true
end function

public function longlong of_getdescriptionsfromids (longlong a_company_ids[]);/************************************************************************************************************************************************************
**
**	of_getDescriptionsFromIDs()
**	
**	This function populates the i_company_descr array based on the company ids passed in
**	
**	Parameters	:	longlong	:	(a_company_ids[]) The selected companies_ids
**	
**	Returns		:	integer	: Success: 1; Failure: -1 
**
************************************************************************************************************************************************************/
longlong index

for index = 1 to upperbound(a_company_ids)
	select description
	into :i_company_descr[index]
	from company
	where company_id = :a_company_ids[index];
	
	if sqlca.sqlcode = 100 then
		f_pp_msgs("ERROR: Could not find company_id " + string(a_company_ids[index]) + " in company view.")
		return -1
	elseif sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: SQL Error getting company description from company table for company_id " + string(a_company_ids[index]) + ". SQL Error: " + sqlca.sqlerrtext)
		return -1
	end if
next

return 1
end function

public function integer of_retrievecompany ();/************************************************************************************************************************************************************
**
**	of_retrieveCompany()
**	
**	This function corresponds to w_wo_control.wf_retrieve_company(). Performs some validations and sets some instance variables for the 
**	current company.
**	
**	Parameters	:	(none)
**	
**	Returns		:	integer	: Success: 1; Failure: -1
**
************************************************************************************************************************************************************/
string   sqls, where_clause
integer  i, rows, ret
longlong color, rtn, nrows, interface_id, check_count 
datetime month, max_wo_month, max_pp_month, min_wo_month

////////////////
//compare max months in pp_interface_dates and wo_process_control and fix pp_interface_dates as needed 
setnull(max_wo_month)
check_count = 0
select max(accounting_month), count(*) into :max_wo_month , :check_count
from wo_process_control where company_id = :i_company;

if isnull(max_wo_month) then
	f_pp_msgs('Call PPC. All Months are missing in the Wo Process Control table.')
	rows = 0
	goto bad_data_found 
end if

if check_count < 4 then
	f_pp_msgs('Call PPC. Some Months are missing in the Wo Process Control table.')
	rows = 0
	goto bad_data_found
end if

nrows = i_ds_pp_interface_dates_check.retrieve(i_company, 'Project Management')  
			  
select add_months (:max_wo_month,   -3) into :min_wo_month from dual; 
	
if nrows = 0 then 
	//no pp_interface_dates were found
	
	check_count = 0
	select count(*) into :check_count from pp_interface
	where subsystem = 'Project Management' and needed_for_closing = 1;
	
	if check_count = 0 then
		 // insert dummy row in pp_interface;
		insert into pp_interface (company_id, interface_id, subsystem, window, description,
		needed_for_closing)
		values (:i_company, 998, 'Project Management', 'w_close_interface', 'Not in Use', 1); 
		
		 if sqlca.sqlcode < 0 then
			f_pp_msgs( "Cannot insert missing record into pp_interface." + sqlca.sqlerrtext)
			rows = 0
			goto bad_data_found
		 end if 
	end if 

	//  insert new rows in pp_interface_dates to sync with wo_process_control 

	insert into pp_interface_dates(company_id, interface_id,accounting_month) 
	( select a.company_id, a.interface_id, b.accounting_month
	from pp_interface a, wo_process_control b
	where a.company_id = b.company_id
	and a.subsystem = 'Project Management' 
	and b.company_id = :i_company and needed_for_closing = 1 
	and b.accounting_month >=  :min_wo_month 
	minus
	select company_id, interface_id, accounting_month
	from pp_interface_dates
	where interface_id in (select interface_id from pp_interface
		where subsystem = 'Project Management' and company_id = :i_company
		and needed_for_closing = 1 ) 
	and company_id = :i_company 
	and accounting_month >=   :min_wo_month  );

	if sqlca.sqlcode < 0 then
		f_pp_msgs( "Cannot insert missing record into pp_interface_dates." + sqlca.sqlerrtext)
		rows = 0
		goto bad_data_found
	end if 
 

else

	for i = 1 to nrows
		
		max_pp_month = i_ds_pp_interface_dates_check.getitemdatetime(i, 'max_month')
		interface_id = i_ds_pp_interface_dates_check.getitemnumber(i, 'interface_id')
		check_count =  i_ds_pp_interface_dates_check.getitemnumber(i, 'count_check')
		
		if max_wo_month = max_pp_month then
			if check_count < 4 then
				f_pp_msgs('Call PPC. Some Months are missing in the Interface Dates table for interface id ' + string(interface_id))
				rows = 0
				goto bad_data_found 
			end if
			continue
		end if 
	
		if max_wo_month < max_pp_month then
			//very bad - need to fix by hand  
			f_pp_msgs('Call PPC. Some Interface Dates for interface id ' + string(interface_id) + &
				' are missing in the Wo Process Control table')
			goto retrieve_data
		end if 
	
		if max_wo_month > max_pp_month then 
			  //   insert new rows in pp_interface_dates only to sync with wo_process_control 
				
				insert into pp_interface_dates(company_id, interface_id,accounting_month) 
				(select a.company_id, a.interface_id, b.accounting_month
				from pp_interface a, wo_process_control b
				where a.company_id = b.company_id
				and a.subsystem = 'Project Management' 
				and b.company_id = :i_company and a.interface_id = :interface_id
				and needed_for_closing = 1 
				and b.accounting_month >=   :min_wo_month 
				minus
				select company_id, interface_id, accounting_month
				from pp_interface_dates
				where interface_id in (select interface_id from pp_interface
					where subsystem = 'Project Management' 
					and company_id = :i_company
					and interface_id = :interface_id
					and needed_for_closing = 1 ) 
				and company_id = :i_company 
				and accounting_month >=  :min_wo_month);
				
				if sqlca.sqlcode < 0 then
					f_pp_msgs("Cannot insert missing records into  pp_interface_dates." + sqlca.sqlerrtext) 
					rows = 0
					goto bad_data_found
				end if 
		
		 end if
	  
	next 
	 
end if

//////////////
retrieve_data:

i_ds_interface_dates_all.retrieve(i_company)

rows = i_ds_interface_dates_all.rowcount()

bad_data_found:

if rows = 0 then 
	//bad_data_found:
	return -1
end if

if rows > 4 then rows = 4


for i = 1 to rows
    month = i_ds_interface_dates_all.getitemdatetime(i,'accounting_month')
    i_months[i] = month
next

i_month = i_months[1]
i_ds_wo_control.retrieve(i_company, i_month)
return 1

end function

public function integer of_updatedwnocommit ();/************************************************************************************************************************************************************
**
**	of_updateDWNoCommit()
**	
**	This function corresponds to w_wo_control.wf_update_dw_no_commit(). Updates wo_control.
**	
**	Parameters	:	(none)
**	
**	Returns		:	integer	: Success: 1; Failure: -1
**
************************************************************************************************************************************************************/
int rtn
 
rtn = i_ds_wo_control.update()

if rtn <> 1 then
   
	f_pp_msgs("ERROR: i_ds_wo_control.update()~n~n" + sqlca.SQLErrText)
   rollback using sqlca;
   return -1
end if 

return 1
end function

public function integer of_currentmonth (datetime a_current_month);string sqls, where_clause
int i, rtn
longlong rows, row
string user_id
longlong session_id

select userenv('sessionid') into :session_id from dual;

where_clause = " upper(cpr_act_month.user_id) = '" + upper(s_user_info.user_id) + "'" + &
               " and cpr_act_month.batch_report_id = 0 and " + &
					" cpr_act_month.session_id = " + string(session_id)

sqls = "delete from cpr_act_month where " +  where_clause
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs( "ERROR: in of_currentmonth: deleting from cpr_act_month~n~n" + &
		sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if

i_ds_cpr_act_month.insertrow(0)

user_id = s_user_info.user_id

i_ds_cpr_act_month.setitem(1,'user_id',user_id)
i_ds_cpr_act_month.setitem(1,'session_id',session_id)
i_ds_cpr_act_month.setitem(1,'batch_report_id',0)
i_ds_cpr_act_month.setitem(1,'month',a_current_month)

rtn = i_ds_cpr_act_month.Update()

if rtn = 1 then
	// maint 7324
   //commit using sqlca;
   i_ds_cpr_act_month.reset()
   i_ds_cpr_act_month.resetupdate()
else
	f_pp_msgs("of_currentmonth Error - " + &
		"ERROR: i_ds_cpr_act_month.Update():~n~n" + sqlca.SQLErrText)
   rollback using sqlca;
   return -1
end if

return 1
end function

public function integer of_wo_summaries ();datetime gl_posting_mo_yr 
longlong rowcount, row, ret
longlong i, rows, clear_wo_id, m
longlong 	  rtn, max_open_month, month_number   
datetime 	approved_date, null_date 
longlong	c, num_companies 
string company_descr

f_pp_msgs( "Updating wo_summary "+ ' ' + string(now()))

month_number = year(date(i_month)) * 100 + month(date(i_month))
ret = f_update_wo_summary_month(i_company, month_number)
if ret = -1 then
		f_pp_msgs("ERROR: updating wo_summary after AFUDC calculation " )
	rollback;
	return -1
else
	commit;
end if

max_open_month = 0
select to_number(to_char(max(accounting_month),'yyyymm')) into :max_open_month
	from wo_process_control where company_id = :i_company;	
if isnull(max_open_month) or max_open_month = 0 or max_open_month < month_number then
	max_open_month = month_number
end if

for m = month_number to max_open_month		
	choose case right(string(m), 2)
		case "01", "02", "03", "04", "05", "06", &
			  "07", "08", "09", "10", "11", "12"
		case else
			//  If the loop crosses years, the m variable could contain 200013, 200014, etc.
			continue
	end choose
next
	
return 1
end function

public function integer of_retrievecompanyloop (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
**
**	of_retrieveCompanyLoop()
**	
**	This function corresponds to w_wo_control.wf_retrieve_company_loop(). Sets some instance variables for the 
**	current company.
**	
**	Parameters	:	(none)
**	
**	Returns		:	integer	: Success: 1; Failure: -1
**
************************************************************************************************************************************************************/
string   sqls, where_clause
integer  i, rows, ret
longlong color, rtn
datetime month

i_ds_interface_dates_all.retrieve(a_company_id)

rows = i_ds_interface_dates_all.rowcount()

if rows = 0 then 
	return -1
end if

if rows > 4 then rows = 4

for i = 1 to rows
    month = i_ds_interface_dates_all.getitemdatetime(i,'accounting_month')  

    i_months[i] = month
next

i_month = a_month
i_month_number = year(date(a_month))*100 + month(date(a_month))
i_ds_wo_control.retrieve(a_company_id, a_month)
return 1
end function

public function integer of_companychanged (integer a_index, datetime a_month);/************************************************************************************************************************************************************
**
**	of_companyChanged()
**	
**	This function corresponds to w_wo_control.wf_company_changed(). Gets the company id and sets some instance variables for the 
**	current company.
**	
**	Parameters	:	integer  : (a_index) Corresponds to the index of the company in i_company_idx
**	
**	Returns		:	integer	: Success: 1; Failure: -1
**
************************************************************************************************************************************************************/
string text, wo_process_ans
longlong company_id, owned, i, rtn



//Company Has Been Altered -- Determine Chosen

//LLL: Trimming here
text = trim(i_company_descr[a_index])

//SQL BEGIN
select company_id, owned into :company_id, :owned 
  from company where ltrim(rtrim(description)) = :text;
//SQL END

i_owned = owned

i_company = company_id

of_retrieveCompanyLoop(company_id, a_month)

return 1
end function

public function longlong of_setupfromcompaniesandmonth (longlong a_company_ids[], date a_month);
//function to put the nvo_wo_control object in the correct state for processing based on an array of company IDs and a selected month 

longlong i

//set our instance variables
this.i_company_idx = a_company_ids
this.i_month = datetime(a_month)
this.i_month_number = year(a_month)*100 + month(a_month)

//populate the i_company_descr array
for i = 1 to upperbound(this.i_company_idx)
	SELECT description
	INTO :i_company_descr[i]
	FROM company
	WHERE company_id = :i_company_idx[i];
	
	if sqlca.sqlcode < 0 then 
		f_pp_msgs("ERROR: Could not get description from the Company view for company_id " + string(i_company_idx[i]) + ". SQL Error: " + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 then 
		f_pp_msgs("ERROR: Could not find a record in the Company view for company_id " + string(i_company_idx[i]) + &
			". Make sure the user " + s_user_info.user_id + " has the correct permissions and security settings to access this view and company.")
		return -1
	end if
next

//initialize i_ds_interface_dates_all



return 1
end function

public function string of_buildcheckstring (string a_exe_name, string a_processstring);/************************************************************************************************************************************************************
**
**	of_buildCheckstring()
**	
**	This function will build the check string that will be used to check for related processes for this exe.
** If a process such as a calculation process is passed in, then we also want to pass back the correlating approval processes to check
**		
**	Parameters	:	string a_exe_name: The exe name for the process currently running, e.g. ssp_wo_calc_oh_afudc_wip.exe
**						string a_processString
**
**	Returns		:	string	: Returns the check string
**
************************************************************************************************************************************************************/
string checkString, processKeyWord

checkString = a_processstring

CHOOSE CASE a_exe_name

CASE "ssp_wo_new_month.exe"
		//WO_NEW_MONTH
CASE "ssp_wo_close_charge_collection.exe"
		// WO_CLOSE_CHG
CASE "ssp_wo_calc_oh_afudc_wip.exe"
		//WO_CALC_AFUDC
		//// the following should not be allowed to run concurrently for the same company and month:
		// Calc Overheads and AFUDC and Approve Overheads and AFUDC
		// Calc Overheads and AFUDC and Automatic Non-Unitized
		// Calc Overheads and AFUDC and Automatic Unitization
		// Calc Overheads and AFUDC and Non-Unitized Failed Unitization		
		checkString = checkString + f_replace_string(checkString, 'WO_CALC_AFUDC', 'WO_APPR_AFUDC','all')
		checkString = checkString + f_replace_string(checkString, 'WO_APPR_AFUDC', 'WO_NON_UNIT','all')
		checkString = checkString + f_replace_string(checkString, 'WO_NON_UNIT', 'WO_AUTO_UNIT','all')		
CASE "ssp_wo_appr_oh_afudc_wip.exe"
		//WO_APPR_AFUDC
CASE "ssp_wo_calculate_accruals.exe"
		//WO_CALC_ACC
		//// the following should not be allowed to run concurrently for the same company and month:
		// Calculate Accruals and Approve Accruals
		// Calculate Accruals and Automatic Non-Unitized
		// Calculate Accruals and Automatic Unitization
		// Calculate Accruals and Non-Unitized Failed Unitization
		checkString = checkString + f_replace_string(checkString, 'WO_CALC_ACC', 'WO_APPR_ACC','all')		
		checkString = checkString + f_replace_string(checkString, 'WO_APPR_ACC', 'WO_NON_UNIT','all')		
		checkString = checkString + f_replace_string(checkString, 'WO_NON_UNIT', 'WO_AUTO_UNIT','all')		
CASE "ssp_wo_approve_accruals.exe"
		//WO_APPR_ACC
CASE "ssp_wo_retirements.exe"
		//WO_RETIRE
CASE "ssp_wo_auto_nonunitize.exe non"
		//// the following should not be allowed to run concurrently for the same company and month
		//Automatic Non-Unitized and Non-Unitized Failed Unitization
		checkString = checkString + f_replace_string(checkString, 'WO_NON_UNIT', 'WO_FAILED_UNIT','all')			
CASE "ssp_wo_auto_unitization.exe"
		//// the following should not be allowed to run concurrently for the same company and month
		// Automatic Unitization and Non-Unitized Failed Unitization
		checkString = checkString + f_replace_string(checkString, 'WO_AUTO_UNIT', 'WO_NON_UNIT','all')			
CASE "ssp_wo_auto_nonunitize.exe failed"
		//WO_FAILED_UNIT
CASE "ssp_release_je_wo.exe"
		//WO_RJE
CASE "ssp_wo_gl_reconciliation.exe"
		//WO_RECON
CASE "ssp_wo_close_month.exe"
		//WO_CLOSE_MONTH
CASE "ssp_preview_je.exe"
		//PREVIEW_JE
CASE ELSE
		 return "ERROR"
END CHOOSE

return checkString
end function

public function string of_buildprocessstring (string a_exe_name, longlong a_company_id[], longlong a_month_number);/************************************************************************************************************************************************************
**
**	of_buildProcessString()
**	
**	This function should build the process string that will be used to lock the given process.
**		
**	Parameters	:	string a_exe_name: The exe name for the process currently running, e.g. ssp_wo_new_month.exe
**						longlong a_company_id[]: The array of company ids to process
**						longlong a_month_number: The month being processed in YYYYMM format
**
**	Returns		:	string	: Returns the process string
**
************************************************************************************************************************************************************/
string processkeyword, processstring
longlong num_Companies, c, i

//Determine the process key word based on a_exe_name
CHOOSE CASE a_exe_name
	CASE "ssp_wo_new_month.exe"
		processKeyWord = "WO_NEW_MONTH"
	CASE "ssp_wo_close_charge_collection.exe"
		processKeyWord = "WO_CLOSE_CHG"
	CASE "ssp_wo_calc_oh_afudc_wip.exe"
		processKeyWord = "WO_CALC_AFUDC"
	CASE "ssp_wo_appr_oh_afudc_wip.exe"
		processKeyWord = "WO_APPR_AFUDC"
	CASE "ssp_wo_calculate_accruals.exe"
		processKeyWord = "WO_CALC_ACC"
	CASE "ssp_wo_approve_accruals.exe"
		processKeyWord = "WO_APPR_ACC"
	CASE "ssp_wo_retirements.exe"
		processKeyWord = "WO_RETIRE"
	CASE "ssp_wo_auto_nonunitize.exe non"
		processKeyWord = "WO_NON_UNIT"
	CASE "ssp_wo_auto_unitization.exe"
		processKeyWord = "WO_AUTO_UNIT"
	CASE "ssp_wo_auto_nonunitize.exe failed"
		processKeyWord = "WO_FAILED_UNIT"
	CASE "ssp_release_je_wo.exe"
		processKeyWord = "WO_RJE"
	CASE "ssp_wo_gl_reconciliation.exe"
		processKeyWord = "WO_RECON"
	CASE "ssp_wo_close_month.exe"
		processKeyWord = "WO_CLOSE_MONTH"
	CASE "ssp_preview_je.exe"
		processKeyWord = "PREVIEW_JE"
	CASE ELSE
		return "ERROR"
END CHOOSE

//Loop over the array of company ids to build the format string
num_companies = upperbound(a_company_id)
for c=1 to num_companies
   processString =  processString + processKeyWord + "|" + string(a_company_id[c]) + "|" + string(a_month_number) + ";"
next

return processString
end function

public function boolean of_lockprocess (string a_exe_name, longlong a_company_id[], longlong a_month_number, ref string a_msg); /************************************************************************************************************************************************************
**
**	of_lockprocess()
**	
**	This function will lock the process for the executable name being passed in.
**		
**	Parameters	:	string a_exe_name: The exe name for the process currently running, e.g. ssp_wo_new_month.exe
**						longlong a_company_id[]: The array of company ids to process
**						longlong a_month_number: The month being processed in YYYYMM format
**						ref string a_msg: Passes any error messages back to the caller
**
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
//Variables Declaration
string lockString, checkString
boolean rtn

//Call of_buildProcessString() to build the process string or lock string
lockString = of_buildProcessString(a_exe_name, a_company_id[], a_month_number)
//check processString for error
if lockString = "ERROR" then
	f_pp_msgs("Error occurred in building the lock string.")
	f_pp_msgs("Executable name: " + a_exe_name)
	return false
end if

//Based on the a_exe_name, determine whether anything else needs to be added to the check string
//e.g. if a calc is being processed, need to add the corresponding approval to the check string
checkString = of_buildCheckString(a_exe_name, lockString)
if checkString = "ERROR" then
	f_pp_msgs("Error occurred in building the check string.")
	f_pp_msgs("Executable name: " + a_exe_name)
	return false
end if

// make sure process is not currently running
rtn = f_pp_check_process(checkString, a_msg)
if rtn = true then
	f_pp_msgs("The check for any concurrent process is successful.")
	f_pp_msgs("No other process is running, preparing to lock the process.")
else
	f_pp_msgs("Error occurred in f_pp_check_process.")
	f_pp_msgs("Detected process with executable name: " + a_exe_name + " is currently running")
	f_pp_msgs("Error Message: " + a_msg)
	return false
end if

// lock the process
rtn = f_pp_set_process(lockString, checkString, a_msg)
// Other uses
// f_pp_set_process('afc_1','afc_1;afc_-1'); //Lock process afc_1, but first check to see if afc_1 or afc_-1 is already running. If so, fail. Returns OK.
// f_pp_set_process('afc_2','afc_2;afc_-1'); //Lock process afc_2, but first check to see if afc_2 or afc_-1 is already running. If so, fail. Returns OK.
// f_pp_set_process('afc_-1','afc_%'); //Lock process afc_-1, but first check to see if any afc process is already running. Returns Error.
// f_pp_set_process('afc_1',' '); //Remove the lock on process afc_1. You can also use f_pp_release_process() for this.
// f_pp_set_process('afc_2',' '); //Remove the lock on process afc_2. You can also use f_pp_release_process() for this.
// f_pp_set_process('afc_-1','afc_%'); //Lock process afc_-1, but first check to see if any afc process is already running. Returns OK this time.
//	f_pp_set_process('afc_-1',' '); //Remove the lock on process afc_-1. You can also use f_pp_release_process() for this.


//If successful, set i_processString = process string
if rtn = true then
	i_processString = lockString
else
     f_pp_msgs("The process is currently being used in another session")	
	return false
end if

end function

public function boolean of_releaseprocess (ref string a_msg); /************************************************************************************************************************************************************
**
**	of_releaseProcess()
**	
**	This function will release the process for the executable name being passed in.
**		
**	Parameters	:	ref string a_msg: Passes any error messages back to the caller
**
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/

boolean rtn

rtn = f_pp_release_process(i_processString, a_msg)

return rtn
end function

public subroutine of_log_failed_companies (string a_log_header);//**********************************************************************************************************************************************************
//
// of_log_failed_companies()
// 
// log the list of companies that failed at whatever they were doing in their company loop
// 
// Parameters : String: a_log_header - log entry header, like "CALCULATE ACCRUALS"
// 
// Returns : Nothing
//
//**********************************************************************************************************************************************************

int num_failed_companies, c, company_array_position

num_failed_companies = upperbound(i_array_position_of_failed_companies)
for c=1 to num_failed_companies
	company_array_position = i_array_position_of_failed_companies[c]
	f_pp_msgs("FAILED TO " + a_log_header + " FOR : Company ID: " + String(i_company_idx[company_array_position]) + ", Company Name: " + i_company_descr[company_array_position] )
next
end subroutine

public subroutine of_add_to_failed_company_list (longlong a_company_index);//**********************************************************************************************************************************************************
//
// of_add_to_failed_company_list()
// 
// add a company to the list of failed companies so they can be logged at the end of the process
// 
// Parameters : LongLong: a_company_index - the i_company_idx array position of the failed compmay
// 
// Returns : Nothing
//
//**********************************************************************************************************************************************************

// keep track of all the companies that failed to unitize so they can be logged later
i_num_failed_companies = upperbound(i_array_position_of_failed_companies) + 1
i_array_position_of_failed_companies[i_num_failed_companies] = a_company_index
end subroutine

on nvo_wo_control.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_wo_control.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;
DESTROY i_ds_interface_dates_all
DESTROY i_ds_wo_control
end event

