HA$PBExportHeader$uo_budget_wip_computations.sru
$PBExportComments$7767-update budget_amounts after processing budget items
forward
global type uo_budget_wip_computations from nonvisualobject
end type
end forward

global type uo_budget_wip_computations from nonvisualobject
end type
global uo_budget_wip_computations uo_budget_wip_computations

type variables
longlong i_wip_comp
longlong i_funding_wo_indicator = 1 /*only works on funding projects at this point may be enhanced later*/
longlong i_actuals_month_number
longlong i_start_month_number
longlong i_cwip_start_month
longlong i_end_month
longlong i_budget_version = 0
boolean i_status_box = false
boolean i_progress_bar = true
boolean i_pp_msgs = false
boolean i_messagebox_onerror = true
boolean i_statusbox_onerror = true
boolean i_add_time_to_messages = true
boolean i_display_rowcount = true 
boolean i_display_msg_on_sql_success = true
longlong i_status_position
string i_table_name
boolean i_error = false
longlong i_priority = -10

longlong i_wip_compuation_id[], i_tot_computations


/*WIP COMP VARIABLES USED IN THE WIP COMP CALC LOOP*/
string i_DESCRIPTION
longlong i_company_id_from
string i_budget_sql_eligibility 









end variables

forward prototypes
public function longlong uf_start_month ()
public function longlong uf_rate ()
public function longlong uf_wip_comp_calc (string a_table_name)
public subroutine uf_msg (string a_msg)
public function longlong uf_sqlca (string a_msg, boolean a_rows)
public function longlong uf_wip_comp_calc_month ()
public function longlong uf_delete_wip ()
public function longlong uf_return_wip ()
public function longlong uf_budget_wip_comp_ext (longlong a_wip_comp_id, longlong a_month_number, string a_caller)
end prototypes

public function longlong uf_start_month ();
longlong start_month, end_month, woa_end_month, bv_end_month

longlong woa_start_month, woa_act_month, bv_start_month, bv_act_month

setnull(woa_start_month)
setnull(woa_act_month)
setnull(bv_start_month)
setnull(bv_act_month)
setnull(bv_end_month)
setnull(woa_end_month)

///First lets get the actuals month if it wasn't passed in it will be null at this point if it wasn't passed in
///I'm intentionally not using the i_budget_Version variable because this is set when calculating AFUDC for an entire budget version
///When this is done an actuals month is always set. so none of this logic will fire
/*- Funding Projects
		- max work order approval actauls month number
		- if above is null then max actuals month number on any budget version the funding project is hooked to
		- if above is null then the min estimate start date - 1 month 
 - Budget Items
		- min actuals month number on any budget version that the funding project revisions are in
		- min Dec of year prior to start year on any budget versions that the funding project revisons are in
 - Work Orders
 		- max w
 		- min est_start_month - 1
*/

if i_funding_wo_indicator <> 2 then///For funding projects and work orders get the woa 
	select min(to_number(to_char(est_start_date, 'yyyymm'))),
	decode(max(nvl(woa.actuals_month_number,0)),0,min(nvl(to_number(to_char(add_months(est_start_date,-1), 'yyyymm')),0)),max(nvl(woa.actuals_month_number,0))),
	max(to_number(to_char(est_complete_date,'yyyymm')))
	into :woa_start_month, :woa_act_month, :woa_end_month
	from work_order_approval woa, wo_est_processing_temp t
	where woa.work_order_id = t.work_order_id
	and woa.revision =t.revision
	;
	if uf_sqlca('Selecting actuals month from Work Order Approval',false) <> 1 then return -1
	
else //for budget items get the data from budget_amounts
	select min(to_number(to_char(nvl(start_date,sysdate), 'yyyymm'))),
			0
	into :woa_start_month, :woa_act_month
	from budget_amounts woa, wo_est_processing_temp t
	where woa.budget_id = t.work_order_id
	and woa.budget_version_id =t.revision
	;
	if uf_sqlca('Selecting actuals month from Work Order Approval',false) <> 1 then return -1	
end if

uf_msg('funding wo indicator : '+string(i_funding_wo_indicator))
uf_msg('woa start month : '+string(woa_start_month))
uf_msg('woa act month : '+string(woa_act_month))
uf_msg('woa end month : '+string(woa_end_month))
	
if i_budget_version <> 0 then
	select min(start_year*100+1),
			max(actuals_month),
			min(end_year*100+12)
	into :bv_start_month, :bv_act_month, :bv_end_month
	from budget_version bv
	where bv.budget_version_id = :i_budget_version
	 ;
	 if uf_sqlca('Selecting actuals month from Budget version',false) <> 1 then return -1
else
	if i_funding_wo_indicator = 1  then	
		select min(start_year*100+1),
				max(actuals_month)
		into :bv_start_month, :bv_act_month
		from budget_version bv
		where exists (
			select 1
			from budget_version_fund_proj bvfp, wo_est_processing_temp w
			where bvfp.budget_version_id = bv.budget_version_id
				and bvfp.work_order_id = w.work_order_id
				and bvfp.revision = w.revision
				and bvfp.active = 1)
		 ;
		 if uf_sqlca('Selecting actuals month from Budget version',false) <> 1 then return -1
	elseif i_funding_wo_indicator = 0 then
		bv_start_month = 0
		bv_act_month = 0
	else 
		select min(start_year*100+1),
				max(actuals_month)
		into :bv_start_month, :bv_act_month
		from budget_version bv, wo_est_processing_temp t
		where bv.budget_version_id = t.revision
		 ;
		 if uf_sqlca('Selecting actuals month from Budget version',false) <> 1 then return -1
	end if
end if

if isnull(woa_start_month) then woa_start_month = 0
if isnull(woa_act_month) then woa_act_month = 0
if isnull(bv_start_month) then bv_start_month = 0
if isnull(bv_act_month) then bv_act_month = 0

uf_msg('i_budget_version : '+string(i_budget_version))
uf_msg('bv_start_month : '+string(bv_start_month))
uf_msg('bv_act_month : '+string(bv_act_month))
uf_msg('bv_end_month : '+string(bv_end_month))

choose case i_funding_wo_indicator
	case 1/*Funding Projects*/
		///For actuals pick the greater of the budget version and work order approval actuals months
		if  woa_act_month > bv_act_month then
			i_actuals_month_number = woa_act_month
		else
			i_actuals_month_number = bv_act_month
		end if	
		
		if isnull(i_actuals_month_number) then
			i_actuals_month_number = 0
		end if

		uf_msg('i_actuals_month_number : '+string(i_actuals_month_number))
		
		select min(to_number(to_char(est_start_date, 'yyyymm')))
		into :i_cwip_start_month
		from work_order_approval woa
		where exists (select 1
							from wo_est_processing_temp w
							where w.work_order_id = woa.work_order_id
								and w.revision = woa.revision);
		
		uf_msg('i_cwip_start_month : '+string(i_cwip_start_month))
		
		///if the cwip start_month > actuals month set it to the actuals month
		if i_cwip_start_month > i_actuals_month_number then
			i_cwip_start_month = i_actuals_month_number
			uf_msg('i_cwip_start_month (iter2): '+string(i_cwip_start_month))
		end if
		
		if isnull(bv_end_month) then
			end_month = woa_end_month
			uf_msg('end_month (woa_end_month): '+string(end_month))
		else
			end_month = bv_end_month
			uf_msg('end_month (bv_end_month): '+string(end_month))
		end if
		
	case 0 /*Work Orders*/
		///For work orders the actuals month and start month should be the same
		if woa_act_month > 190001 then
			i_cwip_start_month = woa_act_month
			i_actuals_month_number = woa_act_month
		else
			i_cwip_start_month = woa_start_month
			i_actuals_month_number = woa_start_month
		end if
					
	case 2 /*Budget Items*/
		i_actuals_month_number = bv_act_month
		i_cwip_start_month = bv_start_month
		
		if i_cwip_start_month > i_actuals_month_number then
			i_cwip_start_month = i_actuals_month_number
		end if
		
end choose
		
if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then 
	uf_msg('ERROR finding actuals month for i_funding_wo_indicator = '+string(i_funding_wo_indicator))
	return -1
end if

if isnull(i_cwip_start_month) then
	i_cwip_start_month = i_actuals_month_number
end if

///Now the calc start month should be just one month after actuals month
if mid(string(i_actuals_month_number),5,2) = '12' then
	i_start_month_number = i_actuals_month_number + 89
else
	i_start_month_number = i_actuals_month_number + 1
end if

i_end_month = end_month

uf_msg('i_cwip_start_month (iter3): '+string(i_cwip_start_month))

return 1
end function

public function longlong uf_rate ();	longlong bv_rates_check, rate_bv_id
	
	////This function updates the rates for all months for a particular wip computation the table budget_wip_comp_calc
	////It updates the rate field, the compound rate field and the rate_calc_date field. The rate calc date allows the calculation to calculation amounts but back them out until the calc date
	
	
	///First check to see if they are storing rates by budget version
	///If so then the pulls below should be by budget version if not then pull where budget version = -1
	select count(*)
	into :bv_rates_check
	from budget_wip_comp_rate
	where budget_version_id = :i_budget_version
	and wip_computation_id = :i_wip_comp
	;
	if uf_sqlca('Retrieve Wip Computation Rates by BV check',false) = -1 then return -1
	if isnull(bv_rates_check) then bv_rates_check = 0
	if bv_rates_check > 0 then
		rate_bv_id = i_budget_version
	else
		rate_bv_id = -1
	end if
	
	///
	///Rate
	///
	
		///First pull rate rule = 0 this means the user entered a rate in the rate field on budget_wip_comp_rate
		update budget_wip_comp_calc_temp a
		set (rate , rate_calc_date, cwip_in_base_to_alloc) =
			(select b.rate,nvl(b.calculation_date,effective_date), nvl(cwip_in_base_alloc,0)  from budget_wip_comp_rate b
			where wip_computation_id = :i_wip_comp 
				and effective_date = (select max(effective_date) from budget_wip_comp_rate
											where wip_computation_id = :i_wip_comp
											and b.budget_version_id = :rate_bv_id
											and to_number(to_char(effective_date,'yyyymm')) <= a.month_number
											)
				and a.wip_computation_id = b.wip_computation_id
				and nvl(b.rate_rule, 0) = 0
				and b.budget_version_id = :rate_bv_id
			)
		where exists	 (select 1 from budget_wip_comp_rate c
								where c.wip_computation_id = :i_wip_comp 
								and c.effective_date = (select max(effective_date) from budget_wip_comp_rate
														where wip_computation_id = :i_wip_comp
														and c.budget_version_id = :rate_bv_id
														and to_number(to_char(effective_date,'yyyymm')) <= a.month_number
														)
								and a.wip_computation_id = c.wip_computation_id
								and nvl(c.rate_rule, 0) = 0
								and c.budget_version_id = :rate_bv_id
							)
		and nvl(not_eligible_flag,0) = 0
		;
		if uf_sqlca('Updating the rate where the rate_rule = 0 (BV = '+string(rate_bv_id)+') ',true) = -1 then return -1
		
		
		///Next pull the rate rules 1-3 from the afudc data table
		///1= AFUDC Debt Rate
		///2 = AFUDC Equity
		///3 = AFUDC Debt + AFUDC Equity
		
		//update the rate where rate_rule in (1,2,3) using budget version
		update budget_wip_comp_calc_temp a
		  set (rate , rate_calc_date,cwip_in_base_to_alloc)=
				(select decode(d.rate_rule,1,b.debt_rate,2,b.equity_rate,3,b.debt_rate + b.equity_rate), nvl(d.calculation_date,d.effective_date), nvl(cwip_in_base_alloc,0) 
				from afudc_data b,
						  work_order_account c,
						  budget_wip_comp_rate d               
				where c.work_order_id = a.work_order_id
					 and c.afudc_type_id = b.afudc_type_id
					 and d.rate_rule in (1,2,3)
					and d.effective_date = (select max(effective_date) 
													from budget_wip_comp_rate dd 
													where d.wip_computation_id = dd.wip_computation_id 
													and dd.effective_date < to_date(a.month_number,'yyyymm')
													and dd.budget_version_id = :rate_bv_id
												)
					and b.effective_date = (select max(bb.effective_date)
																from afudc_data bb                                               
										where b.afudc_type_id = bb.afudc_type_id
										and bb.effective_date < to_date(a.month_number,'yyyymm')
																  )
					 and wip_computation_id = :i_wip_comp
					 and nvl(not_eligible_flag,0) = 0
					 and d.budget_version_id = :rate_bv_id )
		  where exists (select 1
						from budget_wip_comp_rate d               
						where wip_computation_id = :i_wip_comp
						and d.effective_date = (select max(effective_date) 
													from budget_wip_comp_rate dd 
													where d.wip_computation_id = dd.wip_computation_id 
													and dd.effective_date < to_date(a.month_number,'yyyymm')
													and dd.budget_version_id = :rate_bv_id
													)
						and d.rate_rule in (1,2,3)
						and d.budget_version_id = :rate_bv_id 
						and d.wip_computation_id = a.wip_computation_id
					)
		  and nvl(a.not_eligible_flag,0) = 0
		;
	
		if uf_sqlca('Updating the rate where the rate_rule in (1,2,3) BV ',true) = -1 then return -1
	
	///
	///Compound Rate
	///
		
		//update the rate where rate_rule = 0 using budget version
		update budget_wip_comp_calc_temp a
		set compound_rate =
			(select b.compound_rate from budget_wip_comp_rate b
			where wip_computation_id = :i_wip_comp 
				and effective_date = (select max(effective_date) from budget_wip_comp_rate
											where wip_computation_id = :i_wip_comp
											and b.budget_version_id = :rate_bv_id
											and to_number(to_char(effective_date,'yyyymm')) <= a.month_number
											)
				and a.wip_computation_id = b.wip_computation_id
				and nvl(b.comp_rate_rule, 0) = 0
				and b.budget_version_id = :rate_bv_id
			)
		where exists	 (select compound_rate from budget_wip_comp_rate b
								where wip_computation_id = :i_wip_comp 
								and effective_date = (select max(effective_date) from budget_wip_comp_rate
														where wip_computation_id = :i_wip_comp
														and b.budget_version_id = :rate_bv_id
														and to_number(to_char(effective_date,'yyyymm')) <= a.month_number
														)
								and a.wip_computation_id = b.wip_computation_id
								and nvl(b.comp_rate_rule, 0) = 0
								and b.budget_version_id = :rate_bv_id
							)
		and nvl(not_eligible_flag,0) = 0
		;
		if uf_sqlca('Updating the rate where the rate_rule = 0 (BV) ',true) = -1 then return -1
				
		///1= AFUDC Debt Rate
		///2 = AFUDC Equity
		///3 = AFUDC Debt + AFUDC Equity
		//update the rate where rate_rule in (1,2,3) using budget version
		update budget_wip_comp_calc_temp a
		  set compound_rate =
				(select decode(d.comp_rate_rule,1,b.debt_rate,2,b.equity_rate,3,b.debt_rate + b.equity_rate)
				from afudc_data b,
						  work_order_account c,
						  budget_wip_comp_rate d               
				where c.work_order_id = a.work_order_id
					 and c.afudc_type_id = b.afudc_type_id
					 and d.comp_rate_rule in (1,2,3)
					and d.effective_date = (select max(effective_date) 
													from budget_wip_comp_rate dd 
													where d.wip_computation_id = dd.wip_computation_id 
													and dd.effective_date < to_date(a.month_number,'yyyymm')
													and dd.budget_version_id = :rate_bv_id
												)
					and b.effective_date = (select max(bb.effective_date)
																from afudc_data bb                                               
										where b.afudc_type_id = bb.afudc_type_id
										and bb.effective_date < to_date(a.month_number,'yyyymm')
																  )
					 and wip_computation_id = :i_wip_comp
					 and nvl(not_eligible_flag,0) = 0
					 and d.budget_version_id = :rate_bv_id )
		  where exists (select 1
						from budget_wip_comp_rate d               
						where wip_computation_id = :i_wip_comp
						and d.effective_date = (select max(effective_date) 
													from budget_wip_comp_rate dd 
													where d.wip_computation_id = dd.wip_computation_id 
													and dd.effective_date < to_date(a.month_number,'yyyymm')
													and dd.budget_version_id = :rate_bv_id
													)
						and d.comp_rate_rule in (1,2,3)
						and d.budget_version_id = :rate_bv_id 
						and d.wip_computation_id  = a.wip_computation_id
					)
		  and nvl(a.not_eligible_flag,0) = 0
		;	
		if uf_sqlca('Updating the rate where the rate_rule in (1,2,3) BV ',true) = -1 then return -1
		
	return 1
end function

public function longlong uf_wip_comp_calc (string a_table_name);

dec{2} cwip_in_base_to_alloc, total_base, total_allocated
longlong compound_month_number,cmpd, check2, check3, ext_rtn
longlong end_month_number, bv_rates_check, rate_bv_id, rtn
string sqls
uo_budget_revision uo_revisions

//*************************************************************************************************************************
// Notes
// All Commits and Rollbacks are Handled in Calling Functions
//
// a_comp_timing is defined as follows:
// 1 = Before any Overhead or AFUDC Calculations
// 2 = After the "Before AFUDC" Overhead Calculations and Before AFUDC calculation
// 3 = After AFUDC calculation Before "After AFUDC" Overhead Calculation
// 4 = After all AFUDC and Overhead calculations
// 0 = Inactive // should not be called
//*************************************************************************************************************************



uf_msg('Starting Budget WIP Computations for Company_id ')
any results[]
longlong i
i_table_name = upper(a_table_name)

uf_msg(' Clear Prior Calculation Runs ')
sqlca.truncate_table('budget_wip_comp_calc_temp')
if uf_sqlca('truncating budget_wip_comp_calc_temp',false) = -1 then return -1

uf_msg(' Set start and end month numbers ')
rtn = uf_start_month()


//Retrieve the List of wip_computation where valid for budget in (1,2)
///Valid for budget = 1 valid for both budget and actuals
//Valid for budget = 2 valid for just budget
///Valid for budget = 0 valid for just actuals
i_tot_computations = f_get_column(results,'select wip_computation_id from wip_computation where nvl(valid_for_budget,0) in (1,2)  order by priority, wip_computation_id ') //CWB - add comp timing

if i_tot_computations < 0 then
	uf_msg('  Error Getting Computations')
	uf_sqlca('Erorr Details',true)
	return -1
elseif i_tot_computations = 0 then 
	uf_msg('  No WIP Computations ')
	return 1
else
	uf_msg('  Total WIP Computations ' + string(i_tot_computations))
end if

i_wip_compuation_id = results




	
 
////Now loop over Wip Computations and do the processing



for i = 1 to i_tot_computations
	////FIRST Null out all the variables so we know that we have fresh copies of everything
	setnull(i_description)
	setnull(i_budget_sql_eligibility) 
	setnull(i_company_id_from)

	///CJR the goal is to do as much processing in this part of the loop before we start the monthly loop
	///Now select the variables back in for this wip comp
	i_wip_comp = i_wip_compuation_id[i]
	select DESCRIPTION,	budget_sql_eligibility, company_id_from
	into :i_DESCRIPTION,:i_budget_sql_eligibility, :i_company_id_from
	from wip_computation
	where wip_computation_id = :i_wip_comp
	;	
	if uf_sqlca('Retrieve Wip Computation Details',false) = -1 then return -1
		
	uf_msg('Computation ' + string(i) + ' of ' + string(i_tot_computations) + ' : ' + i_description)
	//Call Extension Function
	ext_rtn = f_wip_comp_extension(i_wip_comp,-1, 'budget calc',-1,i_COMPANY_ID_FROM)
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_extension for wip computation = " + i_description)
		return -1
	end if
	
	///Now lets insert eligible work orders for this wip computation

	uf_msg(' Determine Eligible Work Orders ')
	
	sqls =  "insert into budget_wip_comp_calc_temp " + & 
	"(wip_computation_id, work_order_id, revision, month_number, company_id, isd_impact, calc_in_service_date, half_month_option, stop_option,  " + & 
	" rate, afudc_base, jur_cwip_in_base, base_amount, compound_base, compound_calc, uncompound_base, compound_rate, calc_amount, juris_pct, " +&
	" afudc_type_id, closing_option_id, comp_type, juris_cwip_base_opt, jurisdiction_id, charge_option_id, expenditure_type_id,oh_basis_bdg_id,  "+&
	" jur_allo_book_id,  est_chg_type_id, compound_indicator) "+&
	" select wip.wip_computation_id, w.work_order_id,  w.revision, ((year.year*100)+month.month_num), work_order_control.company_id, " + & 
	" decode(lower(nvl(wip.isd_compound_option,'half')),'half',.5,'none',1,'skip',0,'day', " + & 
	"	1 - to_number(to_char(nvl(work_order_control.in_service_date,nvl(woa.est_in_service_date,woa.est_complete_date)),'dd'))/to_number(to_char(last_day(nvl(work_order_control.in_service_date,nvl(woa.est_in_service_date,woa.est_complete_date))),'dd')), " + & 
	" 'full',0), " + & 
	" nvl(work_order_control.in_service_date,nvl(woa.est_in_service_date,woa.est_complete_date)), " + & 
	" nvl(wip.half_month_option,1), " + & 
	" nvl(stop_option,'3'), "+&
	" 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, woaa.afudc_type_id, woaa.closing_option_id, "+&
	" wip.comp_type, wip.juris_cwip_base_opt, wip.jurisdiction_id, wip.charge_option_id, wip.expenditure_type_id,wip.oh_basis_bdg_id, "+&
	" wip.jur_allo_book_id,  wip.est_chg_type_id, "+&
	" decode(month.month_num,1,nvl(cmpd_jan,0), 2, nvl(cmpd_feb,0), 3, nvl(cmpd_mar,0), 4, nvl(cmpd_apr,0), 5, nvl(cmpd_may,0), 6, nvl(cmpd_jun,0), "+&
	"									 7, nvl(cmpd_jul,0), 8, nvl(cmpd_aug,0), 9, nvl(cmpd_sep,0), 10,nvl(cmpd_oct,0), 11, nvl(cmpd_nov,0), 12, nvl(cmpd_dec,0),0) compound_indicator "+&
	" from work_order_control , wip_computation wip, wo_est_processing_temp w, " + & 
	"		pp_table_years year, pp_table_months month, work_order_account woaa, work_order_approval woa" + & 
	" where  wip.wip_computation_id = "+string(i_wip_comp)+&
	" and woaa.work_order_id = work_order_control.work_order_id "+&
	" and work_order_control.work_order_id = w.work_order_id "+&
	" and woa.work_order_id = w.work_order_id "+&
	" and woa.revision = w.revision "+&
	" and  ( exists (select 1 " + & 
	"					from wip_comp_wo_type a  " + & 
	"					where a.work_order_type_id = work_order_control.work_order_type_id  " + & 
	"						and a.wip_computation_id = wip.wip_computation_id " + & 
	"						and a.wip_computation_id = "+string(i_wip_comp)+" ) "+&
	"		or exists (select 1  " + & 
	"					from wip_comp_wo_override a  " + & 
	"					where a.work_order_id = work_order_control.work_order_id  " + & 
	"					and a.wip_computation_id = wip.wip_computation_id " + & 
	"					and a.wip_computation_id = "+string(i_wip_comp)+&
	"					and include_exclude = 1)  " + & 
	" ) " +&
	" and not exists (select 1   " + & 
	"					from wip_comp_wo_override a  " + & 
	"					where a.work_order_id = work_order_control.work_order_id  " + & 
	"					and a.wip_computation_id = wip.wip_computation_id " + & 
	"					and a.wip_computation_id = "+string(i_wip_comp)+&
	"					and include_exclude = 0) " + & 
	" and to_date(to_char(year.year*100 + month.month_num),'YYYYMM') between to_date(to_char("+string(i_cwip_start_month)+"),'YYYYMM') and to_date(to_char("+string(i_end_month)+" ),'YYYYMM') "

	
	if not isnull(i_budget_sql_eligibility) then
		sqls = sqls + i_budget_sql_eligibility
	end if
	execute immediate :sqls;
	if uf_sqlca('Retrieve Wip Comp Calc Eligibility',true) = -1 then 
		uf_msg(sqls)
		return -1
	end if

	///
	///Eligiblity Update
	///
		
			
			
		//Update Records based on Stop Options:
		///1= stop at in-service
		///2 = start at in-service
		///3 = always
		//Not Eligble Flags
		// 1 = Not Eligible
		// 2 = Eligible for ISD Adjustment
		// 0 = Eligible
		
		update budget_wip_comp_calc_temp a
		set not_eligible_flag = 2,
			 calculation_notes = trim(a.calculation_notes||' '||'Stop at In-Service Non-Blanket - Calculated ISD Adjustments;')
		where to_number(to_char(calc_in_service_date,'yyyymm')) < month_number
		and  closing_option_id in (1,2,3,4) 
		and stop_option = '1'		
		and wip_computation_id = :i_wip_comp
		;	
		if uf_sqlca('Wip Comp Calc Stop Option 1 - Non Blankets ',true) = -1 then return -1
		
		update budget_wip_comp_calc_temp a
		set not_eligible_flag = 1,
			 calculation_notes = trim(a.calculation_notes||' '||'Stop at In-Service Blanket - No Calculation;')
		where   closing_option_id > 4
		and stop_option = '1'
		and wip_computation_id = :i_wip_comp
		;
		
		if uf_sqlca('Wip Comp Calc Stop Option 1 Blankets',true) = -1 then return -1
		
		update budget_wip_comp_calc_temp a
		set not_eligible_flag = 1,
			 calculation_notes = trim(a.calculation_notes||' '||'Start at In-Service Non-Blanket - No ISD Date - No Calc;')
		where to_number(to_char(calc_in_service_date,'yyyymm')) > month_number
		and  closing_option_id in (1,2,3,4)
		and stop_option = '2'
		and wip_computation_id = :i_wip_comp
		;
		
		if uf_sqlca('Wip Comp Calc Stop Option 2 - Non Blankets ',true) = -1 then return -1
		
		///If its null it means it is eligible
		update budget_wip_comp_calc_temp
		set not_eligible_flag = 0
		where not_eligible_flag is null
		and wip_computation_id = :i_wip_comp
		;
		if uf_sqlca('Wip Comp Calc Stop Option- Eligible ',true) = -1 then return -1
				
		//Call Extensions:  ---- good to update not eligible flag or insert additional work order ids
		ext_rtn = 0
		ext_rtn = uf_budget_wip_comp_ext(i_wip_comp,-1, 'calc-after-eligibility')
		
		if ext_rtn = -1 then
			uf_msg("Error Occured in f_wip_comp_bdg_dynamic_ext for wip computation = " + i_description)
			return -1
		end if
		
	///
	///Rates
	///
		
		///update the rates for this wip comp
		rtn = uf_rate()
		if rtn < 0 then
			return -1
		end if
		
		//Call Extensions:  ---- good to update not eligible flag or insert additional work order ids
		ext_rtn = 0
		ext_rtn = uf_budget_wip_comp_ext(i_wip_comp,-1, 'calc-after-eligibility')
		
		if ext_rtn = -1 then
			uf_msg("Error Occured in f_wip_comp_bdg_dynamic_ext for wip computation = " + i_description)
			return -1
		end if
		
next


	
///
///Base Amounts
///
	

		
	//Now lets pull in the base.  
	//Charge Option ID
		//1 - Current Month
		//2 - Balance
	
	//Half Month Option - half_month_option
		//1 = Yes
		//0 = No
	///CJR note this update statement assumes that the projects have been updated with actuals.  Is does not go to the cwip charge table to get Begining balances
	update budget_wip_comp_calc_temp a 
	set (BASE_AMOUNT,CURRENT_MONTH_BASE_AMOUNT) =  //jrd 2-25-09 add current month data
	(select sum(decode(charge_option_id,2,
			////Charge option #2 life to date balance pull all costs from wo_est_monthly up through the prior month
			decode(sign(b.year - substr(a.month_number,1,4)),1,0,-1,b.total,
				decode(substr(a.month_number,5,2),'12',january+february+march+april+may+june+july+august+september+october+november,
																'11',january+february+march+april+may+june+july+august+september+october,
																'10',january+february+march+april+may+june+july+august+september,
																'09',january+february+march+april+may+june+july+august,
																'08',january+february+march+april+may+june+july,
																'07',january+february+march+april+may+june,
																'06',january+february+march+april+may,
																'05',january+february+march+april,
																'04',january+february+march,
																'03',january+february,
																'02',january,0)),
			///Charge option #1 current month only so this field = 0
					0)///end decode(i_charge_option_id
				) base_amount,
	sum(decode(sign(b.year - substr(a.month_number,1,4)),1,0,-1,0,
				decode(substr(a.month_number,5,2),'12',december * decode(half_month_option,1,.5,1),
																'11',november* decode(half_month_option,1,.5,1),
																'10',october* decode(half_month_option,1,.5,1),
																'09',september* decode(half_month_option,1,.5,1),
																'08',august* decode(half_month_option,1,.5,1),
																'07',july* decode(half_month_option,1,.5,1),
																'06',june* decode(half_month_option,1,.5,1),
																'05',may* decode(half_month_option,1,.5,1),
																'04',april* decode(half_month_option,1,.5,1),
																'03',march* decode(half_month_option,1,.5,1),
																'02',february* decode(half_month_option,1,.5,1),
																'01',january* decode(half_month_option,1,.5,1),0)))CURRENT_MONTH_BASE_AMOUNT
		from wo_est_monthly b, oh_alloc_basis_bdg c
		where c.overhead_basis_id = a.oh_basis_bdg_id
		and a.work_order_id = b.work_order_id
		and a.revision = b.revision
		and b.expenditure_type_id = c.expenditure_type_id
		and b.est_chg_type_id = c.est_chg_type_id
		and nvl(nvl(c.utility_account_id,b.utility_account_id),-998833) = nvl(b.utility_account_id,-998833)
		and nvl(nvl(c.department_id,b.department_id),-998833)= nvl(b.department_id,-998833)
	)
	where nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc Base Amount Set',true) = -1 then return -1
	
	
	///Pull the AFUDC base from budget_afudc_calc for this work order/revision expenditure type = 1
	update budget_wip_comp_calc_temp a 
	set (AFUDC_BASE,closing_pct) = 
	(select b.afudc_base, nvl(b.closings_pct,0)
		from budget_afudc_calc b
		where a.work_order_id = b.work_order_id
		and b.month_number = a.month_number
		and b.expenditure_type_id = 1
		and a.revision = b.revision
	)
	where nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc AFUDC Base Set',true) = -1 then return -1
	

	//need to set juris_pct
	update budget_wip_comp_calc_temp a
	set 	JURIS_PCT = (select jur_allo_book_pct
							from jur_allo_book
							where jur_allo_book_id = a.jur_allo_book_id
							and jurisdiction_id = a.jurisdiction_id
							and effective_date = (select max(effective_date) from jur_allo_book
									where jur_allo_book_id = a.jur_allo_book_id
									and to_number(to_char(effective_date,'yyyymm')) <= a.month_number
									)
							)
	where  nvl(not_eligible_flag,0) = 0
	and jur_allo_book_id is not null;
	
	if uf_sqlca('Wip Comp Calc JUR PCT 1',true) = -1 then return -1

	update budget_wip_comp_calc_temp 
	set 	JURIS_PCT = 1
	where nvl(not_eligible_flag,0) = 0
		and jur_allo_book_id is null;
	
	if uf_sqlca('Wip Comp Calc JUR PCT 2',true) = -1 then return -1
	
///	
///CWIP IN BASE ALLOCATION
///

	update budget_wip_comp_calc_temp a
	set alloc_cwip_in_base =
		(select round((ratio_to_report(nvl(base_amount,0) + nvl(current_month_base_amount,0)) over( Partition by wip_computation_id, month_number) * cwip_in_base_to_alloc), 8) amt
		from budget_wip_comp_calc_temp temp
		where temp.work_order_id = a.work_order_id
			and temp.revision = a.revision
			and temp.wip_computation_id = a.wip_computation_id
			and temp.month_number = a.month_number)
	where  nvl(not_eligible_flag,0) = 0
	and cwip_in_base_to_alloc <> 0
	;
	if uf_sqlca('Wip Comp Calc alloc_cwip_in_base ',true) = -1 then return -1
	
	
	update budget_wip_comp_calc_temp 
	set 	alloc_cwip_in_base = NVL(alloc_cwip_in_base,0)
	WHERE nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc alloc_cwip_in_base 2',true) = -1 then return -1
	
	
	//Call Extensions --- update base amounts or fill in the other_base_adjustment for adjustments - add to calculation notes
	ext_rtn = 0
	ext_rtn = uf_budget_wip_comp_ext(-1,i, 'calc-after-base-amount')
	
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_bdg_dynamic_ext for wip computation = " + i_description)
		return -1
	end if
	
	///This process for now assumes that the budget and actual wip computations are the same.  May want to add a field on the wip computation table at somepoint to map
	///A budget only wip computation to an actual only wip computation but for now budget only wip computations will have no begining balance
	///Another note the begining balances for compounded are determined after this statement without going back to the cwip_charge table.  There could be an issue if a funding project
	//has actual balances in months prior to the estimated start date of this budget version.  This statment will only add the total amounts to months between the estimated start date of this revision
	///and the actuals month
	update budget_wip_comp_calc_temp a
	set total_amount = (select sum(amount)
							  from cwip_charge cc, work_order_control woc
							  where cc.status = a.wip_computation_id
								and cc.work_order_id = woc.work_order_id
								and woc.funding_wo_id = a.work_order_id
								and a.month_number = cc.month_number
								and cc.month_number <= :i_actuals_month_number)
	where month_number <= :i_actuals_month_number
	;
	if uf_sqlca('Wip Comp Calc Updating Actuals Months',true) = -1 then return -1
	
	///Calculate the ending wip calc balance (un closed) to rolled forward to the next month.  See below I'm backing out the total amount thats because the total amount gets 
	///added to the wip_calc_balance when rolling forward
	///The reason I'm summing the total including the actuals month and then backing out the activity from the actuals month is that if the actuals month activity closed
	///in the same month it wouldn't get picked up correctly so I'm calculating the real ending balance for the actuals month and then backing into the beging balance
	//wip_calc_balance is really the begining balance.  
	update budget_wip_comp_calc_temp a
	set wip_calc_balance =nvl( (select sum(cc.amount)
										from cwip_charge cc, work_order_control woc
									  where cc.status = a.wip_computation_id
										and cc.work_order_id = woc.work_order_id
										and woc.funding_wo_id = a.work_order_id
										and a.month_number = :i_actuals_month_number
										and cc.month_number <= :i_actuals_month_number
										and nvl(cc.closed_month_number,999999) > :i_actuals_month_number),0)	- nvl(total_amount,0)
	where month_number = :i_actuals_month_number
	;
	
	///Now lets set the compounded and uncompounded amounts
	update budget_wip_comp_calc_temp a
	set uncompound_base =nvl( (select sum(b.total_amount)
											from budget_wip_comp_calc_temp b
											where a.work_order_id = b.work_order_id
												and a.wip_computation_id = b.wip_computation_id
												and b.month_number < :i_actuals_month_number
												and b.month_number >= nvl( (select max(month_number)
																					from budget_wip_comp_calc_temp c
																					where c.work_order_id = b.work_order_id
																						and c.wip_computation_id = b.wip_computation_id
																						and nvl(c.compound_indicator,0) = 1
																						and c.month_number <= :i_actuals_month_number),0)),0),
		compound_base =nvl( (select sum(b.total_amount)
											from budget_wip_comp_calc_temp b
											where a.work_order_id = b.work_order_id
												and a.wip_computation_id = b.wip_computation_id
												and b.month_number < :i_actuals_month_number
												and b.month_number < nvl( (select max(month_number)
																					from budget_wip_comp_calc_temp c
																					where c.work_order_id = b.work_order_id
																						and c.wip_computation_id = b.wip_computation_id
																						and nvl(c.compound_indicator,0) = 1
																						and c.month_number <= :i_actuals_month_number),0)),0)	
	where month_number = :i_actuals_month_number
	;
	if uf_sqlca('Wip Comp Calc Updating compound/uncompound amounts ',true) = -1 then return -1
	
	update budget_wip_comp_calc_temp
	set RATE = NVL(RATE,0),
	AFUDC_BASE = NVL(AFUDC_BASE,0),
	JUR_CWIP_IN_BASE = NVL(JUR_CWIP_IN_BASE,0),
	BASE_AMOUNT = NVL(BASE_AMOUNT,0),
	CURRENT_MONTH_BASE_AMOUNT = NVL(CURRENT_MONTH_BASE_AMOUNT,0),
	COMPOUND_BASE = NVL(COMPOUND_BASE,0),
	COMPOUND_CALC = NVL(COMPOUND_CALC,0),
	UNCOMPOUND_BASE = NVL(UNCOMPOUND_BASE,0),
	COMPOUND_RATE = NVL(COMPOUND_RATE,0),
	CALC_AMOUNT = NVL(CALC_AMOUNT,0),
	JURIS_PCT = NVL(JURIS_PCT,0),
	alloc_cwip_in_base = nvl(alloc_cwip_in_base,0),
	total_amount = nvl(total_amount,0),
	input_amount = nvl(input_amount,0),
	other_base_adjustment = nvl(other_base_adjustment,0)
	;
	
	if uf_sqlca('Wip Comp Calc NVLs',true) = -1 then return -1
	///Up till now everything has been done in the temp table so no harm in committing jsut doing it here for performance
	commit;
	rtn = uf_wip_comp_calc_month()
	if rtn < 0 then
		return -1
	end if
	
	
	///Delete wip results from wo_est_monthly
	rtn = uf_delete_wip()
	if rtn< 0 then 
		return -1
	end if
	
	///Insert wip results into wo_est_monthly
	rtn = uf_return_wip()
	if rtn < 0 then 
		return -1
	end if
	
	// ### CDM - Maint 7767 - Update budget_amounts after processing budget items
	if i_table_name = 'BUDGET_MONTHLY_DATA' then
		uo_revisions = create uo_budget_revision
		
		uo_revisions.i_messagebox_onerror = i_messagebox_onerror
		uo_revisions.i_status_box = i_status_box
		uo_revisions.i_pp_msgs = i_pp_msgs
		uo_revisions.i_add_time_to_messages = i_add_time_to_messages
		uo_revisions.i_statusbox_onerror = i_statusbox_onerror
		uo_revisions.i_display_msg_on_sql_success = i_display_msg_on_sql_success
		uo_revisions.i_display_rowcount = i_display_rowcount
		uo_revisions.i_progress_bar = i_progress_bar
		
		rtn = uo_revisions.uf_update_budget_amounts()
		destroy uo_revisions
		
		if rtn = -1 then
			rollback;
			return -1
		end if
	end if
	
	///Now return results to wip comp calc table from temp table
	delete from budget_wip_comp_calc a
	where exists (
		select 1
		from wo_est_processing_temp w
		where w.work_order_id = a.work_order_id
		and w.revision = a.revision)
	;
	if uf_sqlca('Delete WIP Results',true) = -1 then return -1
	
	if i_budget_version <> 0 then
		insert into budget_wip_comp_calc
		select * 
		from budget_wip_comp_calc_temp
		where month_number >= (select to_number(to_char(start_year) || '01')
											from budget_version
											where budget_version_id = :i_budget_version)
		;
	else
		insert into budget_wip_comp_calc
		select * 
		from budget_wip_comp_calc_temp
		;
		
	end if
	
	if uf_sqlca('Return wip results',true) = -1 then return -1
	
	commit;
	

return 1
end function

public subroutine uf_msg (string a_msg);

//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_msg() 
//
//	  Purpose  :	returns messages to the user based on boolean variables 
//		             
//
//   Arguments:	a_msg  : string message to return to the user
//					a_error : string that tells what type of message
//								E = Error Message
//								W = Warning Message
//								I = Information Message
//	
//   Returns :   none
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_title, time_string

if i_add_time_to_messages then
	time_string = string(now())
	a_msg = a_msg + ' '+time_string
end if

message_title = 'Budget WIP Comp Calculation'

if i_status_box then
	f_status_box(message_title,a_msg)
end if


if i_pp_msgs then
	f_pp_msgs(a_msg)
end if

if i_progress_bar  then
	f_progressbar(message_title,a_msg,100,i_status_position)
end if

if i_statusbox_onerror and i_error then
	f_status_box(message_title,a_msg)
end if

if i_messagebox_onerror and i_error then
	messagebox(message_title,a_msg)
end if




end subroutine

public function longlong uf_sqlca (string a_msg, boolean a_rows);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_check_sql() 
//
//	  Purpose  :	This function checks the sqlca.sqlcode if less then one it displays an error message if greater then or equal to one and success messageing is turned on it displays a sucess message
//		             
//
//   Arguments:		a_msg - message to be displayed along with error or success
//	
//   Returns :   1 if no sql error -1 if there is an sql error
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_string

message_string = a_msg
if i_add_time_to_messages then
	message_string = message_string+string(now())
end if

if sqlca.sqlcode < 0 then
	i_error = true
	uf_msg('SQL ERROR: '+message_string)
	uf_msg(sqlca.sqlerrtext)
	return -1
else
	if  a_rows then
		message_string = message_string+' : processed '+string(sqlca.sqlnrows)+' rows '
		uf_msg(message_string)
	end if		
	return 1
end if





end function

public function longlong uf_wip_comp_calc_month ();longlong i,end_month_number, ext_rtn,post_cwip, prev_month_number, bv_id, check, priorities , priority[], j, current_priority,input_bv
string stop_option, isd_compound_option, description
any results[]

select max(month_number)
into :end_month_number
from budget_wip_comp_calc_temp
;
if uf_sqlca('Selecting Max end month',true) = -1 then return -1

select count(*)
into :check
from budget_cwip_in_rate_base
where budget_version_id = :i_budget_version
;

if isnull(check) then check = 0
if check > 0 then
	bv_id = i_budget_version
else
	check = 0
	select count(*)
	into :check
	from budget_cwip_in_rate_base
	;
	if isnull(check) then check = 0
	if check > 0 then 
		bv_id = -1
	else 
		bv_id = -100
	end if
end if

select count(*)
into :check
from budget_wip_comp_input
where budget_version_id = :i_budget_version
;

if isnull(check) then check = 0
if check > 0 then
	input_bv = i_budget_version
else
	check = 0
	select count(*)
	into :check
	from budget_wip_comp_input
	;
	if isnull(check) then check = 0
	if check > 0 then 
		input_bv = -1
	else 
		input_bv = -100
	end if
end if

priorities = f_get_column(results,'select distinct nvl(priority,0) from wip_computation where nvl(valid_for_budget,0) in (1,2)  order by nvl(priority,0) ') 

priority = results

for j = 1 to priorities
	setnull(i_priority)
	i_priority = priority[j]
	if isnull(i_priority) then i_priority = 0

			
	for i = i_start_month_number to end_month_number
		
	
		
		if long(mid(string(i),5,2)) > 12 then
			i = (long(mid(string(i),1,4)) + 1)*100 + 1
		end if
		
		if  long(mid(string(i),5,2)) = 1 then
			prev_month_number = i - 89
		else
			prev_month_number = i - 1
		end if
		
		ext_rtn = 0
		ext_rtn = uf_budget_wip_comp_ext(-1,i,'calc-start-month-calc')
		if ext_rtn = -1 then
			uf_msg("Error Occured in f_wip_comp_dynamic_ext for wip computation = " + i_description)
			return -1
		end if	
	
	
		
		///Rollforward the previous months compounding amount and keep the running wip comp amount so that closings can be calculated
		update budget_wip_comp_calc_temp a
		set uncompound_base  = nvl((
				select nvl(b.uncompound_base,0) + nvl(b.total_amount,0)
				from budget_wip_comp_calc_temp b
				where a.wip_computation_id = b.wip_computation_id
				and a.work_order_id = b.work_order_id
				and a.month_number = :i
				and b.month_number = :prev_month_number),0),
			compound_base  =nvl( (
				select nvl(b.compound_base,0)
				from budget_wip_comp_calc_temp b
				where a.wip_computation_id = b.wip_computation_id
				and a.work_order_id = b.work_order_id
				and a.month_number = :i
				and b.month_number = :prev_month_number),0),
			wip_calc_balance = nvl((
				select nvl(b.wip_calc_balance,0) + nvl(b.total_amount,0) - nvl(b.wip_calc_closings,0)
				from budget_wip_comp_calc_temp b
				where a.wip_computation_id = b.wip_computation_id
				and a.work_order_id = b.work_order_id
				and a.month_number = :i
				and b.month_number = :prev_month_number),0)		
		where month_number = :i
		and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority)
		;
		if uf_sqlca('Wip Comp Calc Rollfoward uncompound and wip balance amounts for month '+string(i),true) = -1 then return -1
		
		if bv_id <> -100 then
			////Pull the cwip in base amount from the budget cwip in base table
			///This table will be built before the calc starts either by importing amounts/Coping from another budget version or SQL based
			////Moving this inside the monthly loop to allow for extension functions to touch this table based on previous months calc
			update budget_wip_comp_calc_temp a 
			set (JUR_CWIP_IN_BASE) = 
			nvl((select nvl(cwip_in_base,0)
				from budget_cwip_in_rate_base b
				where a.work_order_id = b.work_order_id
				and b.effective_date = to_date(:i,'yyyymm')
				and b.jurisdiction_id = a.JURISDICTION_ID
				and b.budget_version_id = :bv_id
			),0)
			where  nvl(not_eligible_flag,0) = 0
			and a.month_number = :i
			and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority);	
			if uf_sqlca('Wip Comp Calc JUR CWIP in Base Set',true) = -1 then return -1
		end if
		
		///Calculate this month amount
		update budget_wip_comp_calc_temp a
		set calc_amount = 
				case a.comp_type
				when 'straight_rate' then ((base_amount + current_month_base_amount + other_base_adjustment ) * rate)
				when 'post_in_service_cost' then ((base_amount + current_month_base_amount + other_base_adjustment ) * rate)
				when 'jur_afudc' then (decode(AFUDC_BASE,0,0,(base_amount + current_month_base_amount + other_base_adjustment ) * rate * juris_pct * (JUR_CWIP_IN_BASE/AFUDC_BASE)))
				when 'jur_afudc_offset' then (decode(AFUDC_BASE,0,0,(base_amount + current_month_base_amount + other_base_adjustment ) * rate * juris_pct * (1 - (JUR_CWIP_IN_BASE/AFUDC_BASE))))
				 else 0
				end
		where month_number = :i
		and nvl(not_eligible_flag,0) = 0
		and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority);
		if uf_sqlca('Wip Comp Calc Calc Amount  Set for month '+string(i),true) = -1 then return -1
	
		
		update budget_wip_comp_calc_temp  a
			set 	input_amount = (select sum(amount)
									from budget_wip_comp_input b
									where a.wip_computation_id = b.wip_computation_id
									and b.month_number = :i
									and a.work_order_id = b.work_order_id
									and b.budget_version_id = :input_bv
									)
			where month_number = :i
			and exists (
				select 1
				from budget_wip_comp_input c
				where a.wip_computation_id = c.wip_computation_id
				and c.month_number = :i
				and a.work_order_id = c.work_order_id
				and c.budget_version_id = :input_bv)
			and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority)
			;
			
			if uf_sqlca('Wip Comp Calc Input Amount',true) = -1 then return -1
	
		update budget_wip_comp_calc_temp a 
		set (COMPOUND_BASE) = nvl(compound_base,0) + nvl(UNCOMPOUND_BASE,0) * nvl(compound_indicator,1),
			uncompound_base = decode(nvl(compound_indicator,1),0,nvl(uncompound_base,0),0)
		where  month_number = :i
		and nvl(not_eligible_flag,0) = 0
		and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority);
		
		if uf_sqlca('Wip Comp Calc Compound Base Amount  Set for month '+string(i),true) = -1 then return -1
		
		//compound calc = compound base * compound rate
		update budget_wip_comp_calc_temp
		set 	COMPOUND_CALC = COMPOUND_base * COMPOUND_rate
		where month_number = :i
		and nvl(not_eligible_flag,0) = 0
		and wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority);
		
		if uf_sqlca('Wip Comp Calc compound calc for month '+string(i),true) = -1 then return -1
	
	
		////Adjust the calc amount if this month is the in-service month based on in-service month indicator
		update budget_wip_comp_calc_temp
		set isd_adjustment = nvl(isd_impact,0) * (nvl(COMPOUND_CALC ,0) + nvl(CALC_AMOUNT,0)) * -1
		where month_number = :i
		and to_number(to_char(calc_in_service_date,'YYYYMM')) = :i //cwb 20100305 fix to calculated isd correctly
		and nvl(not_eligible_flag,0) = 0
		and wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority)
		;			
		if uf_sqlca('Wip Comp Calc ISD Adjustment - Current Month for month '+string(i),true) = -1 then return -1
	
		//Call Extensions ---- update other_computed_adj with any special adjustments
		ext_rtn = 0
		ext_rtn = uf_budget_wip_comp_ext(-1,i,'calc-after-calc-amount')
		if ext_rtn = -1 then
			uf_msg("Error Occured in f_wip_comp_dynamic_ext for wip computation = " + i_description)
			return -1
		end if	
		
		///in the budget we added the case for Xcel to allow them to have an effective date and a calc date.  
		///This is to handle cases when they have a file date that is differant from the approved date.  the file for something as of January for example but it doesn't get approved until April
		///the calc will calc the amounts in Jan - april and then reverse it then when the apply date hits it will apply all the reversed amounts
		
		///adjust any months out that are prior to the calc date
		update budget_wip_comp_calc_temp
		set rate_calc_date_adj = (nvl(COMPOUND_CALC,0) + nvl(CALC_AMOUNT,0) + nvl(INPUT_AMOUNT,0) + nvl(isd_adjustment,0)+ nvl(other_computed_adj,0)) * -1
		where nvl(to_number(to_char(rate_calc_date,'YYYYMM')),0) > month_number
		and month_number = :i
		and wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority)
		;
		if uf_sqlca('WIP comp calc rate_calc_date_adj for  '+string(i),true) = -1 then return -1
		
		///adjust any months up that are after the calc date
		update budget_wip_comp_calc_temp a
		set rate_calc_date_adj = nvl((
			select -1*sum(rate_calc_date_adj)
			from budget_wip_comp_calc_temp b
			where a.wip_computation_id = b.wip_computation_id
				and b.month_number < :i
				and a.work_order_id = b.work_order_id),0)
		where nvl(to_number(to_char(rate_calc_date,'YYYYMM')),0) = month_number
		and a.month_number = :i
		and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority)
		;
		if uf_sqlca('WIP comp calc rate_calc_date_adj for  '+string(i),true) = -1 then return -1
		
		update budget_wip_comp_calc_temp
		set 	TOTAL_AMOUNT = nvl(COMPOUND_CALC,0) + nvl(CALC_AMOUNT,0) + nvl(INPUT_AMOUNT,0) + nvl(isd_adjustment,0)+ nvl(other_computed_adj,0) + nvl(rate_calc_date_adj,0)
		where month_number = :i
		and wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority)
		;
		if uf_sqlca('Wip Comp Calc total amount for month '+string(i),true) = -1 then return -1
		
		////Calculate WIP closings
		update budget_wip_comp_calc_temp
		set wip_calc_closings = (nvl(wip_calc_balance,0)+nvl(total_amount,0)) * nvl(closing_pct,0)
		where month_number = :i
		and wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = :i_priority)
		;
		if uf_sqlca('Wip Comp Calc total amount for month '+string(i),true) = -1 then return -1
	
		
	next 
next

return 1
end function

public function longlong uf_delete_wip ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_wip_computations.uf_delete_wip() 
//
//	  Purpose  :	This function deletes all previously calculated WIP Computation Amounts for the work orders id's and revisions loaded in wo_est_processing_temp 
//		             
//
//   Arguments:	 none
//	
//   Returns :   1 if successfuly -1 if failed (SQL Error)
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 	

string sqls, revision, spread_table, spread_field, sqls2
///No code exists to run this processs against budget_monthly_data this code is just here for future development
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	spread_table = 'BUDGET_MONTHLY_SPREAD'
	spread_field = 'BUDGET_MONTHLY_ID'
	revision = 'BUDGET_VERSION_ID'
else 
	spread_table = 'WO_EST_MONTHLY_SPREAD'
	spread_field = 'EST_MONTHLY_ID'
	revision = 'REVISION'
end if

uf_msg('Deleting Previously calculated WIP Results')
sqls2 = " "
sqls2 += " delete from "+i_table_name+" a "
sqls2 += " where exists  ( "
sqls2 += "	select 1 "
sqls2 += "	from wo_est_processing_temp w "
sqls2 += "	where  "
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls2 += "    a.work_order_id= w.work_order_id"
else
	sqls2 += "    a.budget_id= w.work_order_id"
end if
sqls2 += "   and w.revision = a."+revision
sqls2 += " ) "
sqls2 += " and a.est_chg_type_id in (select nvl(est_chg_type_id,0) from wip_computation where valid_for_budget in (1,2) ) "
execute immediate :sqls2;
if uf_sqlca('Deleting Previously Calc AFUDC Est',true) <> 1 then return -1
uf_msg('Deleted '+string(sqlca.sqlnrows))



return 1
end function

public function longlong uf_return_wip ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_return_results() 
//
//	  Purpose  :	This function takes the calculated results and returns them to the table this process was being run for (wo_est_monthly, budget_monthly_data, wo_est_monthly_subs_det)
//		             
//
//   Arguments:		
//	
//   Returns :   1 if successfuly -1 if a failure occurs
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string sqls_wip, sqls_equity, sqls_cpi,spread_table, spread_field, revision, utility_account_col, sqls2, start_date_col, end_date_col, date_table, pk_col_name, wo_col, id_col
string sqls3

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	spread_table = 'BUDGET_MONTHLY_SPREAD'
	spread_field = 'BUDGET_MONTHLY_ID'
	revision = 'BUDGET_VERSION_ID'
	utility_account_col = 'budget_plant_class_id'
	start_date_col = 'start_date'
	end_date_col = 'close_date'
	date_table = "budget_amounts"
	pk_col_name = " budget_monthly_id "
	wo_col = 'null'
	id_col = 'budget_id'
else 
	spread_table = 'WO_EST_MONTHLY_SPREAD'
	spread_field = 'EST_MONTHLY_ID'
	revision = 'REVISION'
	utility_account_col = 'utility_account_id'
	start_date_col = 'est_start_date'
	end_date_col = 'est_complete_date'
	date_table = 'work_order_approval'
	pk_col_name = " est_monthly_id "
	wo_col = 'work_order_id'
	id_col = 'work_order_id'
end if


//need to ensure that all estimates fall within est start ane end dates
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	DELETE FROM budget_wip_comp_calc_temp a
  	WHERE NOT EXISTS (
    		SELECT 1 FROM work_order_approval woa 
   		WHERE a.work_order_id = woa.work_order_id
    		AND a.revision = woa.revision
    		AND month_number BETWEEN To_Char(woa.est_start_date, 'yyyymm') AND To_Char(woa.est_complete_date, 'yyyymm')
	);
	  
	  if uf_sqlca('Delete based on Start/End Dates',true) = -1 then return -1
end if

sqls_wip = ' '
sqls_wip += ' insert into '+i_table_name
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls_wip += ' ( '+pk_col_name+' ,work_order_id, '+revision+', '
else
	sqls_wip += ' ( '+pk_col_name+',budget_id, '+revision+', '
end if
sqls_wip += ' year , '
sqls_wip += ' expenditure_type_id , '
sqls_wip += ' est_chg_type_id , '
sqls_wip += ' january , '
sqls_wip += ' february , '
sqls_wip += ' march, '
sqls_wip += ' april , '
sqls_wip += ' may, '
sqls_wip += ' june, '
sqls_wip += ' july , '
sqls_wip += ' august , '
sqls_wip += ' september , '
sqls_wip += ' october , '
sqls_wip += ' november, '
sqls_wip += ' december, '
sqls_wip += ' total , '
sqls_wip += ' future_dollars , '
sqls_wip += ' hist_actuals  '
sqls_wip += ' )  '
sqls_wip +=' select pwrplant1.nextval, a.* from '
sqls_wip += '  (select work_order_id,revision, '
sqls_wip += ' substr(to_char(trans.month_number),1,4) year , '
sqls_wip += ' expenditure_type_id , '
sqls_wip += ' est_chg_type_id , '
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'01',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'02',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'03',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'04',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'05',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'06',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'07',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'08',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'09',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'10',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'11',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'12',nvl(total_amount,0),0)),0), "
sqls_wip += " nvl(sum(nvl(total_amount,0)),0), "
sqls_wip += ' 0 future_dollars, '
sqls_wip += ' 0 hist_actuals '
sqls_wip += ' from budget_wip_comp_calc_temp trans '
sqls_wip += ' where exists (select 1 from budget_wip_comp_calc_temp b where b.work_order_id = trans.work_order_id and b.revision = trans.revision and b.total_amount <> 0 ) '
sqls_wip += '   and exists (select 1 from wip_computation wip where wip.wip_computation_id = trans.wip_computation_id and nvl(wip.post_to_budget ,0) = 1) '
/*cjr 20100107 added above so that if no WIP calculated zero dollar rows wouldn't be returned have to do it this way so that all years will be return if any month of the project is non-zero
For example if the project has WIP in 2009 but not in 2010 we need 2010 row so that WO_EST_MONTHLY will be whole but if no dollars in either year we don't need the results*/
sqls_wip += ' group by work_order_id, revision, est_monthly_id, '
sqls_wip += ' substr(to_char(trans.month_number),1,4) , '
sqls_wip += ' expenditure_type_id , '
sqls_wip += ' est_chg_type_id  '
sqls_wip += ' ) a '
execute  immediate :sqls_wip;
if uf_sqlca(' Inserting AFUDC Debt Results',true) <> 1 then return -1

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	update budget_monthly_data
	set jan_local = nvl(january,0),
		feb_local = nvl(february,0),
		mar_local = nvL(march,0),
		apr_local = nvl(april,0),
		may_local = nvl(may,0),
		jun_local = nvl(june,0),
		jul_local = nvl(july,0),
		aug_local = nvl(august,0),
		sep_local = nvl(september,0),
		oct_local = nvl(october,0),
		nov_local = nvL(november,0),
		dec_local = nvl(december,0),
		tot_local = nvl(total,0)
	where (budget_monthly_id) in (
		select est_monthly_id
		from budget_wip_comp_calc
		where total_amount <> 0
	)
	;
	if sqlca.sqlcode < 0 then
		uf_msg('Could not insert into '+i_table_name+'_SPREAD for new monthly entries. '+sqlca.sqlerrtext)
		rollback;
		return -1
	end if	
end if

return 1

end function

public function longlong uf_budget_wip_comp_ext (longlong a_wip_comp_id, longlong a_month_number, string a_caller);//******************************************************************************************
//
//  Function  :  f_wip_comp_dynamic_ext
//
//  Description  :  A base function that gets called from the following places in the WIP Computation
//				 Loops over rows in the wip_comp_extensions
//				
//				replaces keywords <company_id>, <month_number>, <work_order_id>, <wip_computation_id> with values
//
//  
//					callers:  calc-after-eligibility
//								calc-after-base-amount
//								calc-after-calc-amount
//								closing-after-eligibility		
//								closing-after-pending
//								
//
////
//******************************************************************************************
boolean		lb_success = true
longlong tot_calls, i, wip_comp_id, call_order
string sqls, ds_sql, ret, wip_comp_sql, sqls1, sqls2, sqls3
uo_ds_top ds_extensions
ds_extensions = create uo_ds_top



if a_wip_comp_id <> -1   then
	wip_comp_sql = ' wip_computation_id = '+string(a_wip_comp_id)
elseif i_priority <> -10 then
	wip_comp_sql = ' wip_computation_id in (select wip_computation_id from wip_computation where nvl(priority,0) = '+string(i_priority)+') '
else
	wip_comp_sql = ' 1=1 '
end if



ds_sql = 'select wip_computation_id, nvl(call_order,0) '+&
			' from budget_wip_comp_extensions '+&
			" where lower(call_location) = '"+lower(a_caller)+"' "+&
			" and "+wip_comp_sql+' '+&
			' order by nvl(call_order,0) '



ret = f_create_dynamic_ds(ds_extensions,'grid',ds_sql,sqlca,true)
if ret <> 'OK' then
	return -1
end if

tot_calls = ds_extensions.rowcount()

if  tot_calls = 0 then return 1



for i = 1 to tot_calls
	setnull(wip_comp_id)
	setnull(call_order)
	setnull(sqls)
	
	wip_comp_id = ds_extensions.getitemnumber(i,1)
	call_order = ds_extensions.getitemnumber(i,2)
	
	
	///###CJR 7403 
	select nvl(sql1,' '),nvl(sql2,' '), nvl(sql3,' ')
	into :sqls1, :sqls2, :sqls3
	from budget_wip_comp_extensions
	where wip_computation_id = :wip_comp_id
	and lower(call_location) = lower(:a_caller)
	and call_order = :call_order
	;	
	if sqlca.sqlcode < 0 then
		uf_msg( "Wip Computation Extensions error Getting SQL from Table: " + sqlca.sqlerrtext)
		return -1
	end if
	
	sqls = sqls1+sqls2+sqls3
	
	if isnull(sqls) then 
		uf_msg( "Wip Computation Extensions SQL is null the process will continue but this extension will be skipped ")
		uf_msg( "WIP Computation id = "+string(wip_comp_id))
		uf_msg( "Caller = "+string(a_caller))
		uf_msg( "Call Order = "+string(call_order))
		continue
	end if
	
	//Replace key words
	sqls = f_replace_string(sqls,"<month_number>", string(a_month_number),'all')
	sqls = f_replace_string(sqls,"<wip_computation_id>", string(a_wip_comp_id),'all')
	sqls = f_replace_string(sqls,"<budget_version_id>",string(i_budget_version),'all')
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		uf_msg("Error Executing SQL: " + sqlca.sqlerrtext + " ; " + sqls)
		uf_msg( "WIP Computation id = "+string(wip_comp_id))
		uf_msg( "Caller = "+string(a_caller))
		uf_msg( "Call Order = "+string(call_order))
		continue
		return -1
	end if
	
next


if lb_success then
	return 1
else
	return -1
end if
end function

on uo_budget_wip_computations.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_budget_wip_computations.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

