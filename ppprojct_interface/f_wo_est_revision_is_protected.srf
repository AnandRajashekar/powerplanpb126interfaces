HA$PBExportHeader$f_wo_est_revision_is_protected.srf
$PBExportComments$155 / 5325 - fiscal year functionality
global type f_wo_est_revision_is_protected from function_object
end type

forward prototypes
global function string f_wo_est_revision_is_protected (longlong a_wo_id, string a_revision, longlong a_value)
end prototypes

global function string f_wo_est_revision_is_protected (longlong a_wo_id, string a_revision, longlong a_value);//----------------------------------------------------------------------------CDM 8.14.2008
//----------------------------------------------------------------------------
//
//  FUNCTION:		f_wo_est_revision_is_protected
//
//  Arguments:		longlong - a_wo_id:			array of work_order_ids
//
//						string  - a_revision:		"max"			Use max revision (<> 1000)
//															"current"	Use last approved / current revision
//															"selected"  Specific revision passed in last argument
//															"budg_ver"	Derive revision from the budget version
//
//						longlong	  - a_value:			revision number    (a_revision = "selected")
//															budget_version_id  (a_revision = "budg_ver")
//
//  Return value:	Returns reason for revision being protected
//
//										month is not protected - ""
//
//										month is protected - 
//													"Budget Version is Not Open For Entry"
//													"Budget Version Requires Substitutions"
//													"Revision is Pending Approval"
//													"Revision is Approved"
//													"Revision is Inactive"
//													"Revision is Budget Reviewed"
//													"Revision is Pending Budget Review"
//
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

longlong is_fp, wo_id, revision, not_open_for_entry, subs_only, &
	app_stat, i, co_id, rev_stat
string rev_lock, edit_approved


not_open_for_entry = 0
subs_only = 0

wo_id = a_wo_id

//
// Get the revision and Funding Indicator
//
if lower(trim(a_revision)) = "max" then
	//Use max revision
	select max(woa.revision), max(woc.funding_wo_indicator), max(woc.company_id)
	into :revision, :is_fp, :co_id
	from work_order_approval woa, work_order_control woc
	where woa.work_order_id = :wo_id
	and woa.revision < 1000
	and woa.work_order_id = woc.work_order_id;
elseif lower(trim(a_revision)) = "current" then
	//Use last approved / current revision
	select decode(nvl(current_revision,0),0,1,current_revision), funding_wo_indicator, company_id
	into :revision, :is_fp, :co_id
	from work_order_control
	where work_order_id = :wo_id;
elseif lower(trim(a_revision)) = "selected" then
	//Particular revision passed into function
	revision = a_value
	select funding_wo_indicator, company_id
	into :is_fp, :co_id
	from work_order_control
	where work_order_id = :wo_id;
elseif lower(trim(a_revision)) = "budg_ver" then
	//Use budget version to derive revision
	select max(bvfp.revision), max(woc.funding_wo_indicator), max(woc.company_id)
	into :revision, :is_fp, :co_id
	from budget_version_fund_proj bvfp, work_order_control woc
	where bvfp.active = 1
	and bvfp.work_order_id = :wo_id
	and bvfp.budget_version_id = :a_value
	and bvfp.work_order_id = woc.work_order_id;
elseif lower(trim(a_revision)) = "budget_item" then
	revision = a_value
	is_fp = 2
	select max(company_id)
	into :co_id
	from budget
	where budget_id = :wo_id;
end if


if is_fp <> 2 then
	app_stat = 0
	rev_stat = -1
	//
	// Check budget version options for funding projects
	// Check approval status of the revision
	// Check review status of the revision
	//
	select not_open_for_entry, subs_only,
		approval_status_id, review_status_id
	into :not_open_for_entry, :subs_only,
		:app_stat, :rev_stat
	from
		(	select decode(count(*),0,0,1) not_open_for_entry
			from budget_version_fund_proj fp, budget_version bv
			where revision = :revision
			and bv.budget_version_id = fp.budget_version_id
			and work_order_id = :wo_id
			and fp.active = 1
			and open_for_entry = 0
		),
		(	select decode(count(*),0,0,1) subs_only
			from budget_version_fund_proj fp, budget_version bv
			where revision = :revision
			and bv.budget_version_id = fp.budget_version_id
			and work_order_id = :wo_id
			and fp.active = 1
			and bring_in_subs = 1
		),
		(	select approval_status_id, review_status review_status_id
			from work_order_approval
			where work_order_id = :wo_id
			and revision = :revision
		)//,
		//(	select decode(max(status),3,3,0,0,-1) review_status_id
		//	from budget_review
		//	where work_order_id = :wo_id
		//	and revision = :revision
		//)
		;
else
	rev_stat = -1
	subs_only = 0
	//
	// Check budget version options for funding projects
	//
	select decode(bv.open_for_entry,0,1,0) not_open_for_entry,
		(	select decode(count(*),0,0,9)
			from budget_approval bal
			where bal.budget_id = ba.budget_id
			and bal.budget_version_id = ba.budget_version_id
			and bal.approval_number = (
				select max(bapp.approval_number) from budget_approval bapp
				where bapp.budget_id = bal.budget_id
				)
			and bal.approval_status_id = 2
		) app_stat
	into :not_open_for_entry,
		:app_stat
	from budget_amounts ba, budget_version bv
	where ba.budget_id = :wo_id
	and ba.budget_version_id = :revision
	and ba.budget_version_id = bv.budget_version_id;
end if


if isNull(not_open_for_entry) then not_open_for_entry = 0
if isNull(subs_only) then subs_only = 0
if isnull(app_stat) then app_stat = 0
if isnull(rev_stat) then rev_stat = -1


//
// Check if Review Status Locks the revision
//
rev_lock = lower(trim(f_pp_system_control_company('FP EST - Budget Review Locks Ests',co_id)))
if isnull(rev_lock) or trim(rev_lock) = '' then
	rev_lock = 'yes'
end if


//
// Check if Approval Status Locks the revision - FP's only
//
edit_approved = lower(trim(f_pp_system_control_company('FUNDPROJ - Edit Approved FP',co_id)))
if isnull(edit_approved) or trim(edit_approved) = '' then
	edit_approved = 'no'
end if

if is_fp = 1 and edit_approved = 'yes' then
	app_stat = 1
end if


//
// Return values
//
if not_open_for_entry = 1 then
	return "Budget Version is Not Open For Entry"
end if
if subs_only = 1 then
	return "Budget Version Requires Substitutions"
end if
if app_stat = 2 then
	return "Revision is Pending Approval"
end if
if app_stat = 3 then
	return "Revision is Approved"
end if
if app_stat = 5 then
	return "Revision is Inactive"
end if
if app_stat = 9 then
	return "Budget Item/Version is Pending Approval"
end if
//if rev_stat = 0 and rev_lock = 'yes' then
if rev_stat = 3 and rev_lock = 'yes' then
	return "Revision is Budget Reviewed"
end if
//if rev_stat = 3 and rev_lock = 'yes' then
if rev_stat = 2 and rev_lock = 'yes' then
	return "Revision is Pending Budget Review"
end if

return ""

end function

