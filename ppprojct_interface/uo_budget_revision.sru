HA$PBExportHeader$uo_budget_revision.sru
$PBExportComments$8402-Add WO_WO_ID Est attr; 8347-Load Budget Dollars fixes; 7678-centralize updating budget_amounts; 8569-include future_dollars in copy; 8481-add allow_edit, orig_authority_limit, is_custom_limit to copy; 8471-specify workflow subsystem when copying;
forward
global type uo_budget_revision from nonvisualobject
end type
end forward

global type uo_budget_revision from nonvisualobject
end type
global uo_budget_revision uo_budget_revision

type variables
//Variables for messaging
boolean i_messagebox_onerror = true
boolean i_status_box = false
boolean i_pp_msgs = false
boolean i_add_time_to_messages = false
boolean i_statusbox_onerror = false
boolean i_display_msg_on_sql_success = false
boolean i_display_rowcount = false
boolean i_progress_bar = false

longlong i_status_position = 0


//Variables for slide revisions

//Variables for new revisions
longlong i_new_bv_ids[]
boolean i_called_from_send_for_appr = false

//Variables for cotenant spread
longlong i_cotenant_bv_id

//Variables for update with actuals
boolean i_only_cur_year = false

//Variables for fy_temp table
boolean i_analyze = true
end variables

forward prototypes
public function longlong uf_slide_update ()
public function longlong uf_wo_to_wem ()
public function longlong uf_transpose (string dollar_vs_unit)
public function longlong uf_wem_to_wo_ovh ()
public function longlong uf_wem_to_wo_afc ()
public subroutine uf_msg (string a_msg, string a_error)
public function longlong uf_check_sql (string a_msg)
public function longlong uf_slide_revision (datetime a_new_start, datetime a_new_end, datetime a_new_est_in_serv)
public function longlong uf_update_with_act (string a_wo_fp, longlong a_actuals_month)
public function longlong uf_new_revision (string a_wo_fp)
public function longlong uf_date_change (string a_wo_fp, datetime a_new_start, datetime a_new_end, datetime a_new_est_in_serv)
public function longlong uf_cotenant_spread (string a_wo_fp)
public function longlong uf_justification_new (longlong a_tab, longlong a_doc_stage, longlong a_funding_wo_ind)
public function longlong uf_delete_revision ()
public function longlong uf_fy_temp_to_wem (longlong funding_wo_ind, boolean fiscal_year)
public function longlong uf_slide (datetime a_old_start, datetime a_old_end, datetime a_new_start, datetime a_new_end, datetime a_new_est_start, datetime a_new_est_complete, datetime a_new_est_in_serv)
public function longlong uf_slide_revision_dollars (datetime a_new_start, datetime a_new_end, datetime a_new_est_start, datetime a_new_est_complete, datetime a_new_est_in_serv)
public function longlong uf_wem_to_fy_temp (longlong funding_wo_ind, boolean fiscal_year, longlong estimate_mode_id)
public function longlong uf_delete_bi_version ()
public function longlong uf_new_bi_version (longlong a_new_version)
public function longlong uf_update_budget_amounts ()
public function longlong uf_load_fp_to_bi ()
public function longlong uf_justification_new_detail (longlong a_tab, longlong a_doc_stage, longlong a_funding_wo_ind)
public function longlong uf_derivation_spread (string a_wo_fp)
public function longlong uf_load_wo_to_fp ()
public function longlong uf_fy_temp_backfill_year (longlong a_year)
public function longlong uf_scale_revision (double a_new_amount, string a_table_name, longlong a_year)
public function longlong uf_scale_revision (double a_new_amount, string a_table_name)
end prototypes

public function longlong uf_slide_update ();//------------------------------------------------------------------------
//
// FUNCTION:		uf_slide_commit
//
// DESCRIPTION:	Save changes from the calculation done by the 
//						Stretch/Slide logic
//
//	ASSUMPTIONS:	1 - Must be called following a call to uf_slide_revision
//						2 - Records are assumed to still reside in the table
//								wo_est_slide_results_temp
//
//	ARGUMENTS:		none
//
//------------------------------------------------------------------------
datetime est_start_date, est_complete_date, est_in_service_date
longlong counter, rtn, wo_id, rev


//
// Check to make sure records still exist in slide results table
//
select count(*)
into :counter
from wo_est_slide_results_temp;

if counter = 0 then
	uf_msg('No slide results exist to be saved.','E')
	return -1
end if


//
// Delete existing estimates - supplemental estimates
//
delete from wo_est_supplemental_data c
where exists (
	select 1 from wo_est_monthly a
	where a.est_monthly_id = c.est_monthly_id
	and a.year = c.year
	and exists (
		select 1 from wo_est_slide_results_temp b
		where b.est_monthly_id = a.est_monthly_id
		)
	);

if uf_check_sql('Deleting from wo_est_supplemental_data') <> 1 then return -1


//
// Delete any stale/existing escalations
//
delete from wo_est_monthly_escalation a
where exists (
	select 1
	from wo_est_monthly c
	where c.est_monthly_id = a.est_monthly_id
	and c.year = a.year
	and exists (
		select 1 from wo_est_slide_results_temp b
		where b.est_monthly_id = c.est_monthly_id
		)
	);

if uf_check_sql('Deleting from wo_est_monthly_escalation') <> 1 then return -1


//
// Delete existing estimates - monthly estimates
//
delete from wo_est_monthly a
where exists (
	select 1 from wo_est_slide_results_temp b
	where b.est_monthly_id = a.est_monthly_id
	);

if uf_check_sql('Deleting from wo_est_monthly') <> 1 then return -1


//
// Insert updated estimates into wo_est_monthly
//    Don't need to insert into wo_est_monthly_spread as those records will not change.
//
insert into wo_est_monthly (
	est_monthly_id, work_order_id, revision, year,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, substitution_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	hist_actuals, future_dollars,
	january, february, march, april, may, june, july, august, september, october, november, december, total,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total )
select est_monthly_id, work_order_id, revision, year,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, substitution_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	hist_actuals, future_dollars,
	january, february, march, april, may, june, july, august, september, october, november, december, total,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total
from wo_est_slide_results_temp;

if uf_check_sql('Inserting into wo_est_monthly') <> 1 then return -1


//
// Update estimated dates
//
select distinct work_order_id, revision, est_start_date, est_complete_date, est_in_service_date
into :wo_id, :rev, :est_start_date, :est_complete_date, :est_in_service_date
from wo_est_slide_results_temp;

update work_order_approval
set est_start_date = :est_start_date, est_complete_date = :est_complete_date, est_in_service_date = :est_in_service_date
where work_order_id = :wo_id
and revision = :rev;

if uf_check_sql('Updating work_order_approval dates') <> 1 then return -1

rtn = f_wo_date_change(wo_id, rev, est_start_date, est_complete_date, est_in_service_date)
if rtn = -1 then return -1

return 1

end function

public function longlong uf_wo_to_wem ();//------------------------------------------------------------------------
//
// FUNCTION:		uf_wo_to_wem
//
// DESCRIPTION:	Sends estimates from wo_estimate to wo_est_monthly
//
//	ASSUMPTIONS:	Cannot send As-Builts
//
//	ARGUMENTS:		none
//
//------------------------------------------------------------------------
longlong cnt_as_built, rtn, counter
string ret


//
// Check to ensure we're not working with As-Builts
//
select count(*) into :cnt_as_built
from wo_est_processing_temp
where revision = 1000;

if cnt_as_built > 0 then
	uf_msg('Cannot send As-Builts to the Dollar Estimates.','E')
	return -1
end if


//
// Removing any revisions that are locked for editing
//
ret = f_wo_est_processing_protected_revs(1) 
if ret <> 'OK' then
	uf_msg(ret,'E')
	return -1
end if


//
// Ensure there remains something to process
//
select count(*) into :counter
from wo_est_processing_temp;
if counter = 0 then
	uf_msg('No revisions remain or are eligible to be processed.','E')
	return -1
end if


//
// Insert estimates into transpose table
//
rtn = uf_transpose('unit')

if rtn = -1 then
	return -1
end if


//
// Delete any existing estimates
//
delete from wo_est_monthly_escalation c
where exists (
	select 1 from wo_est_monthly a
	where exists (
		select 1 from wo_est_processing_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		)
	and a.est_monthly_id = c.est_monthly_id
	and a.year = c.year
	);

if uf_check_sql('Deleting from wo_est_monthly_escalation') <> 1 then return -1

delete from wo_est_supplemental_data c
where exists (
	select 1 from wo_est_monthly a
	where exists (
		select 1 from wo_est_processing_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		)
	and a.est_monthly_id = c.est_monthly_id
	and a.year = c.year
	);

if uf_check_sql('Deleting from wo_est_supplemental_data') <> 1 then return -1

delete from wo_est_monthly_spread c
where exists (
	select 1 from wo_est_monthly a
	where exists (
		select 1 from wo_est_processing_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		)
	and a.est_monthly_id = c.est_monthly_id
	);

if uf_check_sql('Deleting from wo_est_monthly_spread') <> 1 then return -1

delete from wo_est_monthly a
where exists (
	select 1 from wo_est_processing_temp b
	where b.work_order_id = a.work_order_id
	and b.revision = a.revision
	);

if uf_check_sql('Deleting from wo_est_monthly') <> 1 then return -1


//
// Insert into wo_est_monthly
//
insert into wo_est_monthly (
	est_monthly_id, work_order_id, revision, year,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
	substitution_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june, july, august, september, october, november, december,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec
	)
select est_monthly_id, work_order_id, revision, to_number(substr(month_number,1,4)) year,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, null wo_work_order_id, job_task_id, long_description, // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
	substitution_id, null budget_id,
	null attribute01_id, null attribute02_id, null attribute03_id, null attribute04_id, null attribute05_id, null attribute06_id, null attribute07_id, null attribute08_id, null attribute09_id, null attribute10_id,
	round(sum(decode(substr(month_number,5,2),'01',amount,0)),2) january,
	round(sum(decode(substr(month_number,5,2),'02',amount,0)),2) february,
	round(sum(decode(substr(month_number,5,2),'03',amount,0)),2) march,
	round(sum(decode(substr(month_number,5,2),'04',amount,0)),2) april,
	round(sum(decode(substr(month_number,5,2),'05',amount,0)),2) may,
	round(sum(decode(substr(month_number,5,2),'06',amount,0)),2) june,
	round(sum(decode(substr(month_number,5,2),'07',amount,0)),2) july,
	round(sum(decode(substr(month_number,5,2),'08',amount,0)),2) august,
	round(sum(decode(substr(month_number,5,2),'09',amount,0)),2) september,
	round(sum(decode(substr(month_number,5,2),'10',amount,0)),2) october,
	round(sum(decode(substr(month_number,5,2),'11',amount,0)),2) november,
	round(sum(decode(substr(month_number,5,2),'12',amount,0)),2) december,
	round(sum(decode(substr(month_number,5,2),'01',hours,0)),4) hrs_jan,
	round(sum(decode(substr(month_number,5,2),'02',hours,0)),4) hrs_feb,
	round(sum(decode(substr(month_number,5,2),'03',hours,0)),4) hrs_mar,
	round(sum(decode(substr(month_number,5,2),'04',hours,0)),4) hrs_apr,
	round(sum(decode(substr(month_number,5,2),'05',hours,0)),4) hrs_may,
	round(sum(decode(substr(month_number,5,2),'06',hours,0)),4) hrs_jun,
	round(sum(decode(substr(month_number,5,2),'07',hours,0)),4) hrs_jul,
	round(sum(decode(substr(month_number,5,2),'08',hours,0)),4) hrs_aug,
	round(sum(decode(substr(month_number,5,2),'09',hours,0)),4) hrs_sep,
	round(sum(decode(substr(month_number,5,2),'10',hours,0)),4) hrs_oct,
	round(sum(decode(substr(month_number,5,2),'11',hours,0)),4) hrs_nov,
	round(sum(decode(substr(month_number,5,2),'12',hours,0)),4) hrs_dec,
	round(sum(decode(substr(month_number,5,2),'01',quantity,0)),4) qty_jan,
	round(sum(decode(substr(month_number,5,2),'02',quantity,0)),4) qty_feb,
	round(sum(decode(substr(month_number,5,2),'03',quantity,0)),4) qty_mar,
	round(sum(decode(substr(month_number,5,2),'04',quantity,0)),4) qty_apr,
	round(sum(decode(substr(month_number,5,2),'05',quantity,0)),4) qty_may,
	round(sum(decode(substr(month_number,5,2),'06',quantity,0)),4) qty_jun,
	round(sum(decode(substr(month_number,5,2),'07',quantity,0)),4) qty_jul,
	round(sum(decode(substr(month_number,5,2),'08',quantity,0)),4) qty_aug,
	round(sum(decode(substr(month_number,5,2),'09',quantity,0)),4) qty_sep,
	round(sum(decode(substr(month_number,5,2),'10',quantity,0)),4) qty_oct,
	round(sum(decode(substr(month_number,5,2),'11',quantity,0)),4) qty_nov,
	round(sum(decode(substr(month_number,5,2),'12',quantity,0)),4) qty_dec
from wo_est_transpose_temp a
group by est_monthly_id, work_order_id, revision, to_number(substr(month_number,1,4)),
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, long_description,
	factor_id, curve_id, rate_type_id, substitution_id;

if uf_check_sql('Inserting into wo_est_monthly') <> 1 then return -1

update wo_est_monthly a
set total = january + february + march + april + may + june + july + august + september + october + november + december,
	hrs_total = hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec,
	qty_total = qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec
where exists (
	select 1 from wo_est_transpose_temp b
	where b.est_monthly_id = a.est_monthly_id
	);

if uf_check_sql('Updating totals on wo_est_monthly') <> 1 then return -1

update work_order_approval a
set actuals_month_number = null
where exists (
	select 1 from wo_est_processing_temp b
	where b.work_order_id = a.work_order_id
	and b.revision = a.revision
	);

if uf_check_sql('Clearing out actuals month number on work_order_approval') <> 1 then return -1

return 1

end function

public function longlong uf_transpose (string dollar_vs_unit);//------------------------------------------------------------------------
//
// FUNCTION:      uf_transpose
//
// DESCRIPTION:   Transposes estimates from wo_est_monthly into the 
//                  global temp table wo_est_transpose_temp
//
//   ASSUMPTIONS:   1 - Estimates are generated from the view
//                        wo_est_monthly_deescalation_vw, which is
//                        wo_est_monthly dollars minus any escalations
//
//   ARGUMENTS:      dollar_vs_unit:   "dollar"   - estimates come from wo_est_monthly
//                                    "unit"   - estimates come from wo_estimate
//
//------------------------------------------------------------------------

delete from wo_est_transpose_temp;

if lower(trim(dollar_vs_unit)) = 'dollar' then

	insert into wo_est_transpose_temp (
		est_monthly_id, work_order_id, revision, month_number,
		expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
		factor_id, curve_id, rate_type_id, substitution_id, hist_actuals, future_dollars, amount, hours, quantity )
	with b as (
			select vw.work_order_id, vw.revision, est_monthly_id, year,
				vw.expenditure_type_id, vw.est_chg_type_id, vw.department_id, vw.utility_account_id, vw.wo_work_order_id, vw.job_task_id, vw.long_description, vw.substitution_id, vw.budget_id,
				vw.attribute01_id, vw.attribute02_id, vw.attribute03_id, vw.attribute04_id, vw.attribute05_id, vw.attribute06_id, vw.attribute07_id, vw.attribute08_id, vw.attribute09_id, vw.attribute10_id,
				vw.hist_actuals, vw.future_dollars,
				vw.january, vw.february, vw.march, vw.april, vw.may, vw.june, vw.july, vw.august, vw.september, vw.october, vw.november, vw.december,
				vw.hrs_jan, vw.hrs_feb, vw.hrs_mar, vw.hrs_apr, vw.hrs_may, vw.hrs_jun, vw.hrs_jul, vw.hrs_aug, vw.hrs_sep, vw.hrs_oct, vw.hrs_nov, vw.hrs_dec,
				vw.qty_jan, vw.qty_feb, vw.qty_mar, vw.qty_apr, vw.qty_may, vw.qty_jun, vw.qty_jul, vw.qty_aug, vw.qty_sep, vw.qty_oct, vw.qty_nov, vw.qty_dec
			from wo_est_monthly_deescalation_vw vw, wo_est_processing_temp temp where vw.work_order_id = temp.work_order_id and vw.revision = temp.revision
			),
		d as (
			select woa.work_order_id, woa.revision, woa.est_start_date, woa.est_complete_date
			from work_order_approval woa, wo_est_processing_temp temp where woa.work_order_id = temp.work_order_id and woa.revision = temp.revision
			)
	select b.est_monthly_id, b.work_order_id, b.revision, b.year*100 + p.month_num month_number, 
		b.expenditure_type_id, b.est_chg_type_id, b.department_id, b.utility_account_id, b.wo_work_order_id, b.job_task_id, b.long_description, b.budget_id,
		b.attribute01_id, b.attribute02_id, b.attribute03_id, b.attribute04_id, b.attribute05_id, b.attribute06_id, b.attribute07_id, b.attribute08_id, b.attribute09_id, b.attribute10_id,
		c.factor_id, c.curve_id, c.rate_type_id, b.substitution_id,
		b.hist_actuals, b.future_dollars,
		sum(nvl(decode(p.month_num,
								  1,b.january, 
								  2,b.february, 
								  3,b.march, 
								  4,b.april, 
								  5,b.may, 
								  6,b.june, 
								  7,b.july, 
								  8,b.august, 
								  9,b.september, 
								  10,b.october, 
								  11,b.november, 
								  12,b.december),0)) amount,  
		sum(nvl(decode(p.month_num,
								  1,b.hrs_jan,
								  2,b.hrs_feb, 
								  3,b.hrs_mar, 
								  4,b.hrs_apr, 
								  5,b.hrs_may, 
								  6,b.hrs_jun, 
								  7,b.hrs_jul, 
								  8,b.hrs_aug, 
								  9,b.hrs_sep, 
								  10,b.hrs_oct, 
								  11,b.hrs_nov, 
								  12,b.hrs_dec),0)) hours, 
		sum(nvl(decode(p.month_num,
								  1,b.qty_jan,
								  2,b.qty_feb, 
								  3,b.qty_mar, 
								  4,b.qty_apr, 
								  5,b.qty_may, 
								  6,b.qty_jun, 
								  7,b.qty_jul, 
								  8,b.qty_aug, 
								  9,b.qty_sep, 
								  10,b.qty_oct, 
								  11,b.qty_nov, 
								  12,b.qty_dec),0)) quantity
	from pp_table_months p, /*wo_est_monthly_deescalation_vw*/ b, wo_est_monthly_spread c, /*work_order_approval*/ d /*, wo_est_processing_temp t*/
	where b.est_monthly_id = c.est_monthly_id (+)
	and b.work_order_id = d.work_order_id
	and b.revision = d.revision
	and to_date(to_char(b.year*100 + p.month_num),'YYYYMM') between trunc(d.est_start_date,'month') and trunc(d.est_complete_date,'month')
	group by b.est_monthly_id, b.work_order_id, b.revision, b.year*100 + p.month_num, 
		b.expenditure_type_id, b.est_chg_type_id, b.department_id, b.utility_account_id, b.wo_work_order_id, b.job_task_id, b.long_description, b.budget_id,
		b.attribute01_id, b.attribute02_id, b.attribute03_id, b.attribute04_id, b.attribute05_id, b.attribute06_id, b.attribute07_id, b.attribute08_id, b.attribute09_id, b.attribute10_id,
		c.factor_id, c.curve_id, c.rate_type_id, b.substitution_id,
		b.hist_actuals, b.future_dollars
	order by month_number
	;
	
	// PP-41915
	if uf_check_sql('Inserting into transpose table') <> 1 then return -1
	
	sqlca.analyze_table('wo_est_transpose_temp')
	
elseif lower(trim(dollar_vs_unit)) = 'unit' then
	
	update wo_estimate a
	set a.est_monthly_id = pwrplant1.nextval
	where exists (
		select 1 from wo_est_processing_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		);
	
	// PP-41915
	if uf_check_sql('Updating wo_estimate.est_monthly_id') <> 1 then return -1
	
	insert into wo_est_transpose_temp (
		est_monthly_id, work_order_id, revision, month_number,
		expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
		factor_id, curve_id, rate_type_id, substitution_id, hist_actuals, future_dollars,
		amount, hours, quantity )
	select a.est_monthly_id, a.work_order_id, a.revision, c.month_number,
		a.expenditure_type_id, a.est_chg_type_id, a.department_id, a.utility_account_id, null wo_work_order_id, a.job_task_id, a.notes, null budget_id,
		null attribute01_id, null attribute02_id, null attribute03_id, null attribute04_id, null attribute05_id, null attribute06_id, null attribute07_id, null attribute08_id, null attribute09_id, null attribute10_id,
		null factor_id, null curve_id, null rate_type_id, null substitution_id, null hist_actuals, null future_dollars,
		a.amount/c.span, a.hours/c.span, a.quantity/c.span
	from wo_estimate a, wo_est_processing_temp b,
		(   select woa.*, years.year * 100 + months.month_num month_number,
				months_between(trunc(woa.est_complete_date,'month'),trunc(woa.est_start_date,'month')) + 1 span
			from work_order_approval woa, pp_table_months months, pp_table_years years
			where to_char(years.year * 100 + months.month_num) between to_char(woa.est_start_date,'yyyymm') and to_char(woa.est_complete_date,'yyyymm')
		) c
	where a.work_order_id = b.work_order_id
	and a.revision = b.revision
	and a.work_order_id = c.work_order_id
	and a.revision = c.revision;
	
	// PP-41915
	if uf_check_sql('Inserting into wo_est_transpose_temp') <> 1 then return -1
	
	sqlca.analyze_table('wo_est_transpose_temp')
	
	update wo_est_transpose_temp a
	set (amount, hours, quantity) = (
		select a.amount + (old.amount - new.amount),
			a.hours + (old.hours - new.hours),
			a.quantity + (old.quantity - new.quantity)
		from wo_estimate old,
			(   select work_order_id, revision, est_monthly_id, sum(amount) amount, sum(hours) hours, sum(quantity) quantity
				from wo_est_transpose_temp
				group by work_order_id, revision, est_monthly_id
			) new
		where old.est_monthly_id = new.est_monthly_id
		and old.work_order_id = new.work_order_id
		and old.revision = new.revision
		and old.est_monthly_id = a.est_monthly_id
		and old.work_order_id = a.work_order_id
		and old.revision = a.revision
		)
	where exists (
		select est_monthly_id, month_number from (
			select est_monthly_id, max(month_number) month_number
			from wo_est_transpose_temp b
			group by est_monthly_id
			) c
		where c.est_monthly_id = a.est_monthly_id
		and c.month_number = a.month_number
		);
	
	// PP-41915
	if uf_check_sql('Updating wo_est_transpose_temp') <> 1 then return -1
	
	update wo_estimate a
	set a.est_monthly_id = null
	where exists (
		select 1 from wo_est_processing_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		);
	
	// PP-41915
	if uf_check_sql('Updating wo_estimate.est_monthly_id') <> 1 then return -1
	
end if

return 1

end function

public function longlong uf_wem_to_wo_ovh ();//------------------------------------------------------------------------
//
// FUNCTION:		uf_wem_to_wo_ovh
//
// DESCRIPTION:	Sends overhead estimates from wo_est_monthly to wo_estimate
//
//	ASSUMPTIONS:	Cannot send As-Builts
//
//	ARGUMENTS:		none
//
//------------------------------------------------------------------------
longlong cnt_as_built, rtn


//
// Insert estimates into transpose table
//
rtn = uf_transpose('dollar')

if rtn = -1 then
	return -1
end if


//
// Delete any existing estimates
//
delete from wo_estimate a
where exists (
	select 1 from wo_est_processing_temp b
	where b.work_order_id = a.work_order_id
	and b.revision = a.revision
	)
and exists (
	select 1 from estimate_charge_type c
	where c.est_chg_type_id = a.est_chg_type_id
	and nvl(c.afudc_flag,'Q') = 'O'
	);

if uf_check_sql('Deleting from wo_estimate') <> 1 then return -1


//
// Insert into wo_est_monthly
//
insert into wo_estimate (
	estimate_id, work_order_id, revision,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, notes,
	amount, hours, quantity
	)
select est_monthly_id, work_order_id, revision,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, long_description,
	sum(amount), sum(hours), sum(quantity)
from wo_est_transpose_temp a
where exists (
	select 1 from estimate_charge_type c
	where c.est_chg_type_id = a.est_chg_type_id
	and nvl(c.afudc_flag,'Q') = 'O'
	)
group by est_monthly_id, work_order_id, revision,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, long_description,
	factor_id, curve_id, rate_type_id, substitution_id;

if uf_check_sql('Inserting into wo_estimate') <> 1 then return -1


return 1

end function

public function longlong uf_wem_to_wo_afc ();//------------------------------------------------------------------------
//
// FUNCTION:		uf_wem_to_wo_afc
//
// DESCRIPTION:	Sends AFUDC estimates from wo_est_monthly to wo_estimate
//
//	ASSUMPTIONS:	Cannot send As-Builts
//
//	ARGUMENTS:		none
//
//------------------------------------------------------------------------
longlong cnt_as_built, rtn


//
// Insert estimates into transpose table
//
rtn = uf_transpose('dollar')

if rtn = -1 then
	return -1
end if


//
// Delete any existing estimates
//
delete from wo_estimate a
where exists (
	select 1 from wo_est_processing_temp b
	where b.work_order_id = a.work_order_id
	and b.revision = a.revision
	)
and exists (
	select 1 from estimate_charge_type c
	where c.est_chg_type_id = a.est_chg_type_id
	and nvl(c.afudc_flag,'Q') in ('E','D','P')
	);

if uf_check_sql('Deleting from wo_estimate') <> 1 then return -1


//
// Insert into wo_est_monthly
//
insert into wo_estimate (
	estimate_id, work_order_id, revision,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, notes,
	amount, hours, quantity
	)
select est_monthly_id, work_order_id, revision,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, long_description,
	sum(amount), sum(hours), sum(quantity)
from wo_est_transpose_temp a
where exists (
	select 1 from estimate_charge_type c
	where c.est_chg_type_id = a.est_chg_type_id
	and nvl(c.afudc_flag,'Q') in ('E','D','P')
	)
group by est_monthly_id, work_order_id, revision,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, long_description,
	factor_id, curve_id, rate_type_id, substitution_id;

if uf_check_sql('Inserting into wo_estimate') <> 1 then return -1


return 1

end function

public subroutine uf_msg (string a_msg, string a_error);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_revision.uf_msg()
//
//	  Purpose  :	returns messages to the user based on boolean variables 
//		             
//
//   Arguments:	a_msg  : string message to return to the user
//					a_error : string that tells what type of message
//								E = Error Message
//								W = Warning Message
//								I = Information Message
//	
//   Returns :   none
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_title, time_string

message_title = 'Revision Processing'

if i_add_time_to_messages then
	time_string = string(now())
	a_msg = time_string+' '+a_msg
end if

if g_main_application then
	if upper(a_error) = 'E' then
		if i_messagebox_onerror then
			messagebox(message_title, a_msg)
		end if
		if i_statusbox_onerror then
			f_status_box(message_title, a_msg)
		end if
	else
		if i_status_box then
			f_status_box(message_title, a_msg)
		end if
		if i_progress_bar and i_status_position > 0 then
			f_progressbar(message_title,a_msg,100,i_status_position)
		end if
	end if
end if

if i_pp_msgs or (not g_main_application) then
	f_pp_msgs(a_msg)
end if

end subroutine

public function longlong uf_check_sql (string a_msg);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_afudc.uf_check_sql() 
//
//	  Purpose  :	This function checks the sqlca.sqlcode if less then one it displays an error message if greater then or equal to one and success messageing is turned on it displays a sucess message
//		             
//
//   Arguments:		a_msg - message to be displayed along with error or success
//	
//   Returns :   1 if no sql error -1 if there is an sql error
//
//
//     DATE		     NAME				REVISION                         CHANGES
//   ---------    		--------   			-----------    ----------------------------------------------- 
//    12-29-08    Cliff Robinson		1.0				 Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_string

message_string = a_msg

if sqlca.sqlcode < 0 then
	uf_msg('SQL ERROR: '+message_string+' - '+sqlca.sqlerrtext,'E')
	rollback;
	i_status_position = 0
	f_progressbar('Title', 'close(w_progressbar)', 100, -1)
	//if isvalid(w_progressbar) then
	//	close(w_progressbar)
	//end if
	return -1
else
	if i_display_msg_on_sql_success then
		if i_display_rowcount then
			message_string = message_string+' : processed '+string(sqlca.sqlnrows)+' rows '
		end if
		
		uf_msg(message_string,'I')
	end if		
	return 1
end if

end function

public function longlong uf_slide_revision (datetime a_new_start, datetime a_new_end, datetime a_new_est_in_serv);//------------------------------------------------------------------------
//
// FUNCTION:		uf_slide_revision
//
// DESCRIPTION:	Stretch/Slide estimated DATES, and adjust 
//						dollars/hrs/qty accordingly
//
//	ASSUMPTIONS:	1 - ONLY ONE REVISION CAN SLIDE AT A TIME
//						2 - work_order_id/revision must be inserted into
//								wo_est_processing_temp
//						3 - uf_slide_commit must be called separately to save
//								changes to wo_est_monthly
//
//	ARGUMENTS:	a_new_start -			datetime -	New estimate start date
//						a_new_end -			datetime -	New estimate complete date
//						a_new_est_in_serv -	datetime -	New estimate in service date
//
// RETURN CODES:	1 - SUCCESS
//						2 - SUCCESS - DATES CHANGED TO ACCOUNT FOR ACTUALS MONTHS
//					  -1 - ERROR
//
//------------------------------------------------------------------------
datetime old_start_date, old_end_date
longlong rtn


//
// Use the estimated dates as the old start and end
//
select min(a.est_start_date), max(a.est_complete_date)
into :old_start_date, :old_end_date
from work_order_approval a, work_order_control b
where (a.work_order_id, a.revision) in (
	select work_order_id, revision 
	from wo_est_processing_temp
	)
and a.work_order_id = b.work_order_id;

rtn = uf_slide(old_start_date, old_end_date, a_new_start, a_new_end, a_new_start, a_new_end, a_new_est_in_serv)

return rtn

end function

public function longlong uf_update_with_act (string a_wo_fp, longlong a_actuals_month);longlong rtn, cst_rtn, cst_rtn2, cst_rtn3, long_arr[], hrs_qty_update, years[], year, cnt, i, cnt_hierarchy
string sqls, ret, func_name, str_arr[], cv, args[]
boolean new_ests = false
any results[]

cv = lower(trim(f_pp_system_control_company('Update with Act-Curr Year Only',-1)))

if cv = 'yes' then
	i_only_cur_year = true
else
	i_only_cur_year = false
end if

func_name = 'uf_update_with_act'
long_arr[1] = a_actuals_month
if i_only_cur_year then
	str_arr[1] = 'i_only_cur_year'
else
	str_arr[1] = ' '
end if

a_wo_fp = lower(trim(a_wo_fp))

sqlca.truncate_table('wo_est_update_with_act_temp')
sqlca.truncate_table('wo_est_actuals_temp')

sqlca.analyze_table('wo_est_processing_temp')
sqlca.analyze_table('wo_est_monthly')
sqlca.analyze_table('wo_est_monthly_spread')


//
// Call to custom function before any processing
//
cst_rtn = f_budget_revisions_custom(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '1'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
args[4] = string(long_arr[1])
args[5] = string(str_arr[1])
cst_rtn3 = f_wo_validation_control(83,args)
if cst_rtn3 < 0 then 
   return -1
end if


//
// Check to ensure actuals_month is in proper format and a valid month
//
if isnull(a_actuals_month) or a_actuals_month = 0 then
	uf_msg('No actuals month was selected.  Cannot update with actuals.','E')
	return -1
end if

if a_actuals_month < 200000 then
	uf_msg('Error:Actuals Month Number not in the month number format, check the actuals month number field.','E')
	return -1
end if


//
// Remove any revisions that are locked or uneditable
//
uf_msg('Checking for locked revisions','I')
ret = f_wo_est_processing_protected_revs(1) 
if ret <> 'OK' then
	uf_msg('ERROR Checking for locked revisions - '+ret,'E')
	return -1
end if


//
// Put all revisions into update with actuals temp table
//
delete from wo_est_update_with_act_temp;

uf_msg('Inserting revisions into temp table','I')
insert into wo_est_update_with_act_temp (
	work_order_id, revision )
select work_order_id, revision
from wo_est_processing_temp;

if uf_check_sql('Inserting into wo_est_update_with_act_temp') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')

// Analyze table after insert
sqlca.analyze_table('wo_est_update_with_act_temp')


//
// Check if there are any wos/fps that do not have an act/est hierarchy assigned
// ### CDM - Maint 5733
//
select count(*), sum(decode(b.wo_est_hierarchy_id,null,0,1))
into :cnt, :cnt_hierarchy
from wo_est_update_with_act_temp a, work_order_account b
where a.work_order_id = b.work_order_id;

if cnt > 0 and cnt_hierarchy = 0 then
	if cnt = 1 then
		if a_wo_fp = 'fp' then
			uf_msg('This funding project is not assigned an Act Est Hierarchy.  Cannot update with actuals.', 'E')
		else
			uf_msg('This work order is not assigned an Act Est Hierarchy.  Cannot update with actuals.', 'E')
		end if
	else
		if a_wo_fp = 'fp' then
			uf_msg('No funding projects are assigned an Act Est Hierarchy.  Cannot update with actuals.', 'E')
		else
			uf_msg('No work orders are not assigned an Act Est Hierarchy.  Cannot update with actuals.', 'E')
		end if
	end if
	// ### CDM - Maint 6996 - do not continue if no projects with act est hierarchy
	return -1
elseif cnt > 0 and cnt_hierarchy < cnt then
	if i_messagebox_onerror then
		if a_wo_fp = 'fp' then
			rtn = messagebox('Update with Actuals', 'WARNING - Some funding projects are missing an Act Est Hierarchy.~r~n~r~nContinue?', question!, yesno!)
		else
			rtn = messagebox('Update with Actuals', 'WARNING - Some work orders are missing an Act Est Hierarchy.~r~n~r~nContinue?', question!, yesno!)
		end if
		if rtn <> 1 then return -1
	else
		// Can't prompt the user, so just continue with processing but provide a warning.
		if a_wo_fp = 'fp' then
			uf_msg('WARNING - Some funding projects are missing an Act Est Hierarchy.','I')
		else
			uf_msg('WARNING - Some work orders are missing an Act Est Hierarchy.','I')
		end if
	end if
elseif cnt > 0 then
	//This is okay...everything has an Act Est Hierarchy
else
	uf_msg('Nothing to process.  Cannot update with actuals.', 'E')
end if


//
// Capture Hierarchy Levels of detail
//
uf_msg('Updating temp table with level of detail','I')
update wo_est_update_with_act_temp z
set (include_wo, include_jt, include_ua, include_dept,
	hrs_qty_update, expand_dates_bak, expand_dates_fwd,
	include_attr01, include_attr02, include_attr03, include_attr04, include_attr05,
	include_attr06, include_attr07, include_attr08, include_attr09, include_attr10) = (
		select nvl(max(b.include_wo),0), nvl(max(b.include_jt),0), nvl(max(b.include_ua),0), nvl(max(b.include_dept),0),
			nvl(max(b.hrs_qty_update),0), nvl(max(b.expand_dates_bak),1), nvl(max(b.expand_dates_fwd),1),
			nvl(max(include_attr01),0), nvl(max(include_attr02),0), nvl(max(include_attr03),0), nvl(max(include_attr04),0), nvl(max(include_attr05),0),
			nvl(max(include_attr06),0), nvl(max(include_attr07),0), nvl(max(include_attr08),0), nvl(max(include_attr09),0), nvl(max(include_attr10),0)
		from work_order_account a, wo_est_hierarchy b
		where a.wo_est_hierarchy_id = b.wo_est_hierarchy_id
		and a.work_order_id = z.work_order_id
		);

if uf_check_sql('Backfilling levels of detail on wo_est_update_with_act_temp') <> 1 then return -1


//
// Capture "job task rollup" system control - ONLY APPLIES TO FUNDING PROJECTS
//
uf_msg('Updating temp table for job task rollup','I')
update wo_est_update_with_act_temp z
set jt_rollup = (
	select nvl(max(to_number(b.control_value)),0)
	from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('FP EST - job task rollup'))
	and a.work_order_id = z.work_order_id
	)
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('FP EST - job task rollup'))
	and a.work_order_id = z.work_order_id
	and upper(trim(b.control_value)) in (
		select to_char(class_code_id) from class_code
		)
	and :a_wo_fp = 'fp'
	)
and include_jt = 1;

if uf_check_sql('Updating wo_est_update_with_act_temp.jt_rollup') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Capture "job task summary rollup" system control - ONLY APPLIES TO FUNDING PROJECTS
//
uf_msg('Updating temp table for job task summary rollup','I')
update wo_est_update_with_act_temp z
set jt_rollup_summary = 1, jt_rollup = 0
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('Update w Act-JobTask Summ Rollup'))
	and a.work_order_id = z.work_order_id
	and upper(trim(b.control_value)) = 'YES'
	and :a_wo_fp = 'fp'
	)
and include_jt = 1;

if uf_check_sql('Updating wo_est_update_with_act_temp.jt_rollup_summary') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Capture "plant class" system control - ONLY APPLIES TO FUNDING PROJECTS
//
uf_msg('Updating temp table for plant class translation','I')
update wo_est_update_with_act_temp z
set ua_plant_class = 1
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('Utility Account - Plant Class'))
	and a.work_order_id = z.work_order_id
	and a.funding_wo_indicator = 1
	and substr(upper(trim(b.control_value)),1,1) = 'Y'
	and :a_wo_fp = 'fp'
	)
and include_ua = 1;

if uf_check_sql('Updating wo_est_update_with_act_temp.ua_plant_class') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Capture "date change" system control
//
uf_msg('Updating temp table for expanding dates','I')
update wo_est_update_with_act_temp z
set always_update_dates = 1
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim(decode(:a_wo_fp,'fp','WOEST - FP DATE CHANGE','WOEST - WO DATE CHANGE')))
	and a.work_order_id = z.work_order_id
	and instr(upper(trim(b.control_value)),upper(trim('always update'))) > 0
	);

if uf_check_sql('Updating wo_est_update_with_act_temp.always_update_dates') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


sqlca.analyze_table('wo_est_update_with_act_temp')


//
// Get the min month with actuals
//
delete from wo_est_actuals_temp;

if uf_check_sql('Deleting from wo_est_actuals_temp') <> 1 then return -1

uf_msg('Inserting actuals range into temp table','I')
if a_wo_fp = 'fp' then
	insert into wo_est_actuals_temp (work_order_id, min_month, max_month)
	select temp.work_order_id, min(month_number) min_date, max(month_number) max_date
	from cwip_charge a, work_order_control b, wo_est_update_with_act_temp temp
	where a.work_order_id =  b.work_order_id
	and b.funding_wo_id = temp.work_order_id
	group by temp.work_order_id;
else
	insert into wo_est_actuals_temp (work_order_id, min_month, max_month)
	select temp.work_order_id, min(month_number) min_date, max(month_number) max_date
	from cwip_charge a, work_order_control b, wo_est_update_with_act_temp temp
	where a.work_order_id =  b.work_order_id
	and b.work_order_id = temp.work_order_id
	group by temp.work_order_id;
end if

if uf_check_sql('Inserting actuals date range into wo_est_actuals_temp') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')

// Analyze the table after insert
sqlca.analyze_table('wo_est_actuals_temp')


//
// Expand dates where applicable - start date
//
uf_msg('Expanding start date for early charges','I')
update work_order_approval a
set a.est_start_date = (
	select to_date(min_month,'yyyymm')
	from wo_est_actuals_temp temp
	where a.work_order_id = temp.work_order_id
	)
where (a.work_order_id, a.revision) in (
	select b.work_order_id, b.revision
	from wo_est_actuals_temp temp, wo_est_update_with_act_temp b
	where temp.work_order_id = b.work_order_id
	and b.expand_dates_bak = 1
	and nvl(to_char(a.est_start_date,'yyyymm'),'999999') > temp.min_month
	);

if uf_check_sql('Expanding dates on work_order_approval (start date)') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')

//
// Expand dates where applicable - complete date
//
uf_msg('Expanding complete date for late charges','I')
update work_order_approval a
set a.est_complete_date = (
	select to_date(max_month,'yyyymm')
	from wo_est_actuals_temp temp
	where a.work_order_id = temp.work_order_id
	)
where (a.work_order_id, a.revision) in (
	select b.work_order_id, b.revision
	from wo_est_actuals_temp temp, wo_est_update_with_act_temp b
	where temp.work_order_id = b.work_order_id
	and b.expand_dates_fwd = 1
	and nvl(to_char(a.est_complete_date,'yyyymm'),'000000') < temp.max_month
	);

if uf_check_sql('Expanding dates on work_order_approval (complete date)') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')

//
// Propagate the updated dates to work order control
//
uf_msg('Propagating dates back to headers','I')
update work_order_control woc
set est_start_date = (
	select woa.est_start_date from work_order_approval woa, wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	and woa.work_order_id = woc.work_order_id
	),
est_complete_date = (
	select woa.est_complete_date from work_order_approval woa, wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	and woa.work_order_id = woc.work_order_id
	)
where exists (
	select 1 from work_order_approval woa, wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	and temp.work_order_id = woc.work_order_id
	and temp.always_update_dates = 1
	and	(	woc.est_start_date <> woa.est_start_date
				or
				woc.est_complete_date <> woa.est_complete_date
			)
	);

if uf_check_sql('Updating dates back to work_order_control') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')

//
// Call to custom function before building actuals table
//
cst_rtn = f_budget_revisions_custom(func_name,2,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,2,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '2'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
args[4] = string(long_arr[1])
args[5] = string(str_arr[1])
cst_rtn3 = f_wo_validation_control(83,args)
if cst_rtn3 < 0 then 
   return -1
end if


//
// Build view of actuals based on fields populated in wo_est_update_with_act_temp
//
delete from wo_est_actuals_temp;

if uf_check_sql('Deleting from wo_est_actuals_temp') <> 1 then return -1

uf_msg('Building temp table of actuals','I')
if a_wo_fp = 'fp' then
	sqls = &
	"insert into wo_est_actuals_temp ( "+&
	"   work_order_id, "+&
	"   revision, "+&
	"   expenditure_type_id, "+&
	"   est_chg_type_id, "+&
	"   wo_work_order_id, "+ /*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"   job_task_id, "+&
	"   utility_account_id, "+&
	"   department_id, "+&
	"   attribute01_id, "+&
	"   attribute02_id, "+&
	"   attribute03_id, "+&
	"   attribute04_id, "+&
	"   attribute05_id, "+&
	"   attribute06_id, "+&
	"   attribute07_id, "+&
	"   attribute08_id, "+&
	"   attribute09_id, "+&
	"   attribute10_id, "+&
	"   amount, "+&
	"   hours, "+&
	"   quantity, "+&
	"   month_number, "+&
	"   include_wo, "+&
	"   include_jt, "+&
	"   include_ua, "+&
	"   include_dept, "+&
	"   hrs_qty_update ) "+&
	"select "+&
	"   temp.work_order_id work_order_id, "+&
	"   temp.revision revision, "+&
	"   cwip.expenditure_type_id, "+&
	"   ect.funding_chg_type est_chg_type_id, "+&
	"   decode(temp.include_wo,1,cwip.work_order_id,null) wo_work_order_id, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"   decode(temp.include_jt,1,decode(temp.jt_rollup_summary,1,jtl.job_task_summary,decode(nvl(temp.jt_rollup,0),0,cwip.job_task_id,jt_wocc.value)),null) job_task_id, "+&
	"   decode(temp.include_ua,1,decode(temp.ua_plant_class,1,bpc.budget_plant_class_id,cwip.utility_account_id),null) utility_account_id, "+&
	"   decode(temp.include_dept,1,cwip.department_id,null) department_id, "+&
	"   decode(temp.include_attr01,1,attribute01_id,null) attribute01_id, "+&
	"   decode(temp.include_attr02,1,attribute02_id,null) attribute02_id, "+&
	"   decode(temp.include_attr03,1,attribute03_id,null) attribute03_id, "+&
	"   decode(temp.include_attr04,1,attribute04_id,null) attribute04_id, "+&
	"   decode(temp.include_attr05,1,attribute05_id,null) attribute05_id, "+&
	"   decode(temp.include_attr06,1,attribute06_id,null) attribute06_id, "+&
	"   decode(temp.include_attr07,1,attribute07_id,null) attribute07_id, "+&
	"   decode(temp.include_attr08,1,attribute08_id,null) attribute08_id, "+&
	"   decode(temp.include_attr09,1,attribute09_id,null) attribute09_id, "+&
	"   decode(temp.include_attr10,1,attribute10_id,null) attribute10_id, "+&
	"   sum(cwip.amount) amount, "+&
	"   sum(cwip.hours) hours, "+&
	"   sum(cwip.quantity) quantity, "+&
	"   cwip.month_number month_number, "+&
	"   temp.include_wo, "+&
	"   temp.include_jt, "+&
	"   temp.include_ua, "+&
	"   temp.include_dept, "+&
	"   temp.hrs_qty_update "+&
	"from wo_est_update_with_act_temp temp, "+&
	"   work_order_approval woa, "+&
	"   (   select woc.*, t.jt_rollup "+&
	"      from work_order_control woc, wo_est_update_with_act_temp t "+&
	"      where woc.funding_wo_id = t.work_order_id "+&
	"   ) woc, "+&
	"   work_order_account acct, "+&
	"   cwip_charge cwip, "+&
	"   wo_est_hierarchy_map weh, "+&
	"   estimate_charge_type ect, "+&
	"   work_order_class_code jt_wocc, "+&
	"   job_task_list jtl, "+&
	"   (   select budget_plant_class_id, utility_account_id "+&
	"      from budget_plant_class bpc, utility_account ua "+&
	"      where bpc.external_account_code = ua.external_account_code "+&
	"   ) bpc "+&
	"where temp.work_order_id = woa.work_order_id "+&
	"and temp.revision = woa.revision "+&
	"and temp.work_order_id = woc.funding_wo_id "+&
	"and temp.work_order_id = acct.work_order_id "+&
	"and acct.wo_est_hierarchy_id = weh.wo_est_hierarchy_id "+&
	"and woc.work_order_id = cwip.work_order_id "
	if i_only_cur_year then
		sqls += &
		"and cwip.month_number <= "+string(a_actuals_month)+" "+&
		"and substr(cwip.month_number,1,4) = '"+left(string(a_actuals_month),4)+"' "
	else
		sqls += &
		"and cwip.month_number <= "+string(a_actuals_month)+" "
	end if
	sqls += &
	"and cwip.month_number between to_number(to_char(woa.est_start_date,'YYYYMM')) and to_number(to_char(woa.est_complete_date,'YYYYMM')) "+&
	"and cwip.cost_element_id = weh.cost_element_id "+&
	"and weh.est_chg_type_id = ect.est_chg_type_id "+&
	"and ect.funding_chg_type is not null "+&
	"and woc.work_order_id = jt_wocc.work_order_id (+) "+&
	"and woc.jt_rollup = jt_wocc.class_code_id (+) "+&
	"and cwip.job_task_id = jtl.job_task_id (+) "+&
	"and cwip.utility_account_id = bpc.utility_account_id (+) "+&
	"and not exists ( "+&
	"   select 1 from update_with_actuals_exclusion f "+&
	"   where f.cost_element_id = cwip.cost_element_id "+&
	'   and f."LEVEL" = 1 '+&
	"   ) "+&
	"and not exists ( "+&
	"   select 1 from update_with_actuals_excl_et g "+&
	"   where g.expenditure_type_id = cwip.expenditure_type_id "+&
	'   and g."LEVEL" = 1 '+&
	"   ) "+&
	"group by temp.work_order_id, "+&
	"   temp.revision, "+&
	"   cwip.expenditure_type_id, "+&
	"   ect.funding_chg_type, "+&
	"   decode(temp.include_wo,1,cwip.work_order_id,null), "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"   decode(temp.include_jt,1,decode(temp.jt_rollup_summary,1,jtl.job_task_summary,decode(nvl(temp.jt_rollup,0),0,cwip.job_task_id,jt_wocc.value)),null), "+&
	"   decode(temp.include_ua,1,decode(temp.ua_plant_class,1,bpc.budget_plant_class_id,cwip.utility_account_id),null), "+&
	"   decode(temp.include_dept,1,cwip.department_id,null), "+&
	"   decode(temp.include_attr01,1,attribute01_id,null), "+&
	"   decode(temp.include_attr02,1,attribute02_id,null), "+&
	"   decode(temp.include_attr03,1,attribute03_id,null), "+&
	"   decode(temp.include_attr04,1,attribute04_id,null), "+&
	"   decode(temp.include_attr05,1,attribute05_id,null), "+&
	"   decode(temp.include_attr06,1,attribute06_id,null), "+&
	"   decode(temp.include_attr07,1,attribute07_id,null), "+&
	"   decode(temp.include_attr08,1,attribute08_id,null), "+&
	"   decode(temp.include_attr09,1,attribute09_id,null), "+&
	"   decode(temp.include_attr10,1,attribute10_id,null), "+&
	"   cwip.month_number, "+&
	"   temp.include_wo, "+&
	"   temp.include_jt, "+&
	"   temp.include_ua, "+&
	"   temp.include_dept, "+&
	"   temp.hrs_qty_update "
else
	sqls = &
	"insert into wo_est_actuals_temp ( "+&
	"   work_order_id, "+&
	"   revision, "+&
	"   expenditure_type_id, "+&
	"   est_chg_type_id, "+&
	"   wo_work_order_id, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"   job_task_id, "+&
	"   utility_account_id, "+&
	"   department_id, "+&
	"   attribute01_id, "+&
	"   attribute02_id, "+&
	"   attribute03_id, "+&
	"   attribute04_id, "+&
	"   attribute05_id, "+&
	"   attribute06_id, "+&
	"   attribute07_id, "+&
	"   attribute08_id, "+&
	"   attribute09_id, "+&
	"   attribute10_id, "+&
	"   amount, "+&
	"   hours, "+&
	"   quantity, "+&
	"   month_number, "+&
	"   include_wo, "+&
	"   include_jt, "+&
	"   include_ua, "+&
	"   include_dept, "+&
	"   hrs_qty_update ) "+&
	"select "+&
	"   temp.work_order_id work_order_id, "+&
	"   temp.revision revision, "+&
	"   cwip.expenditure_type_id, "+&
	"   ect.est_chg_type_id est_chg_type_id, "+&
	"   decode(temp.include_wo,1,cwip.work_order_id,null) wo_work_order_id, "+&
	"   decode(temp.include_jt,1,decode(temp.jt_rollup_summary,1,jtl.job_task_summary,decode(nvl(temp.jt_rollup,0),0,cwip.job_task_id,jt_wocc.value)),null) job_task_id, "+&
	"   decode(temp.include_ua,1,decode(temp.ua_plant_class,1,bpc.budget_plant_class_id,cwip.utility_account_id),null) utility_account_id, "+&
	"   decode(temp.include_dept,1,cwip.department_id,null) department_id, "+&
	"   decode(temp.include_attr01,1,attribute01_id,null) attribute01_id, "+&
	"   decode(temp.include_attr02,1,attribute02_id,null) attribute02_id, "+&
	"   decode(temp.include_attr03,1,attribute03_id,null) attribute03_id, "+&
	"   decode(temp.include_attr04,1,attribute04_id,null) attribute04_id, "+&
	"   decode(temp.include_attr05,1,attribute05_id,null) attribute05_id, "+&
	"   decode(temp.include_attr06,1,attribute06_id,null) attribute06_id, "+&
	"   decode(temp.include_attr07,1,attribute07_id,null) attribute07_id, "+&
	"   decode(temp.include_attr08,1,attribute08_id,null) attribute08_id, "+&
	"   decode(temp.include_attr09,1,attribute09_id,null) attribute09_id, "+&
	"   decode(temp.include_attr10,1,attribute10_id,null) attribute10_id, "+&
	"   sum(cwip.amount) amount, "+&
	"   sum(cwip.hours) hours, "+&
	"   sum(cwip.quantity) quantity, "+&
	"   cwip.month_number month_number, "+&
	"   temp.include_wo, "+&
	"   temp.include_jt, "+&
	"   temp.include_ua, "+&
	"   temp.include_dept, "+&
	"   temp.hrs_qty_update "+&
	"from wo_est_update_with_act_temp temp, "+&
	"   work_order_approval woa, "+&
	"   (   select woc.*, t.jt_rollup "+&
	"      from work_order_control woc, wo_est_update_with_act_temp t "+&
	"      where woc.work_order_id = t.work_order_id "+&
	"   ) woc, "+&
	"   work_order_account acct, "+&
	"   cwip_charge cwip, "+&
	"   wo_est_hierarchy_map weh, "+&
	"   estimate_charge_type ect, "+&
	"   work_order_class_code jt_wocc, "+&
	"   job_task_list jtl, "+&
	"   (   select budget_plant_class_id, utility_account_id "+&
	"      from budget_plant_class bpc, utility_account ua "+&
	"      where bpc.external_account_code = ua.external_account_code "+&
	"   ) bpc "+&
	"where temp.work_order_id = woa.work_order_id "+&
	"and temp.revision = woa.revision "+&
	"and temp.work_order_id = woc.work_order_id "+&
	"and temp.work_order_id = acct.work_order_id "+&
	"and acct.wo_est_hierarchy_id = weh.wo_est_hierarchy_id "+&
	"and woc.work_order_id = cwip.work_order_id "
	if i_only_cur_year then
		sqls += &
		"and cwip.month_number <= "+string(a_actuals_month)+" "+&
		"and substr(cwip.month_number,1,4) = '"+left(string(a_actuals_month),4)+"' "
	else
		sqls += &
		"and cwip.month_number <= "+string(a_actuals_month)+" "
	end if
	sqls += &
	"and cwip.month_number between to_number(to_char(woa.est_start_date,'YYYYMM')) and to_number(to_char(woa.est_complete_date,'YYYYMM')) "+&
	"and cwip.cost_element_id = weh.cost_element_id "+&
	"and weh.est_chg_type_id = ect.est_chg_type_id "+&
	"and 99 is not null "+&
	"and woc.work_order_id = jt_wocc.work_order_id (+) "+&
	"and woc.jt_rollup = jt_wocc.class_code_id (+) "+&
	"and cwip.job_task_id = jtl.job_task_id (+) "+&
	"and cwip.utility_account_id = bpc.utility_account_id (+) "+&
	"and not exists ( "+&
	"   select 1 from update_with_actuals_exclusion f "+&
	"   where f.cost_element_id = cwip.cost_element_id "+&
	'   and f."LEVEL" = 0 '+&
	"   ) "+&
	"and not exists ( "+&
	"   select 1 from update_with_actuals_excl_et g "+&
	"   where g.expenditure_type_id = cwip.expenditure_type_id "+&
	'   and g."LEVEL" = 0 '+&
	"   ) "+&
	"group by temp.work_order_id, "+&
	"   temp.revision, "+&
	"   cwip.expenditure_type_id, "+&
	"   ect.est_chg_type_id, "+&
	"   decode(temp.include_wo,1,cwip.work_order_id,null), "+&
	"   decode(temp.include_jt,1,decode(temp.jt_rollup_summary,1,jtl.job_task_summary,decode(nvl(temp.jt_rollup,0),0,cwip.job_task_id,jt_wocc.value)),null), "+&
	"   decode(temp.include_ua,1,decode(temp.ua_plant_class,1,bpc.budget_plant_class_id,cwip.utility_account_id),null), "+&
	"   decode(temp.include_dept,1,cwip.department_id,null), "+&
	"   decode(temp.include_attr01,1,attribute01_id,null), "+&
	"   decode(temp.include_attr02,1,attribute02_id,null), "+&
	"   decode(temp.include_attr03,1,attribute03_id,null), "+&
	"   decode(temp.include_attr04,1,attribute04_id,null), "+&
	"   decode(temp.include_attr05,1,attribute05_id,null), "+&
	"   decode(temp.include_attr06,1,attribute06_id,null), "+&
	"   decode(temp.include_attr07,1,attribute07_id,null), "+&
	"   decode(temp.include_attr08,1,attribute08_id,null), "+&
	"   decode(temp.include_attr09,1,attribute09_id,null), "+&
	"   decode(temp.include_attr10,1,attribute10_id,null), "+&
	"   cwip.month_number, "+&
	"   temp.include_wo, "+&
	"   temp.include_jt, "+&
	"   temp.include_ua, "+&
	"   temp.include_dept, "+&
	"   temp.hrs_qty_update "
end if

sqls = f_sql_add_hint(sqls, 'uf_update_with_act_10')

execute immediate :sqls;

if uf_check_sql('Inserting actuals into wo_est_actuals_temp') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')


//
// Call to custom function after building actuals table
//
cst_rtn = f_budget_revisions_custom(func_name,3,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,3,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '3'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
args[4] = string(long_arr[1])
args[5] = string(str_arr[1])
cst_rtn3 = f_wo_validation_control(83,args)
if cst_rtn3 < 0 then 
   return -1
end if


//
// Analyze table after insert
//
sqlca.analyze_table('wo_est_actuals_temp')


//
// Insert any missing combinations of estimate attributes based on existing actuals
//
uf_msg('Inserting missing estimate attribute combinations','I')
sqls = &
"insert into wo_est_monthly ( "+&
"	est_monthly_id, work_order_id, revision, year, "+&
"	expenditure_type_id, est_chg_type_id, wo_work_order_id, job_task_id, utility_account_id, department_id, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
"	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, "+&
"	january, february, march, april, may, june, july, august, september, october, november, december, total, "+&
"	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total, "+&
"	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total ) "+&
"with actuals as ( "+&
"	select distinct b.work_order_id, b.revision, "+&
"		a.expenditure_type_id, a.est_chg_type_id, a.wo_work_order_id, a.job_task_id, a.utility_account_id, a.department_id, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
"		a.attribute01_id, a.attribute02_id, a.attribute03_id, a.attribute04_id, a.attribute05_id, a.attribute06_id, a.attribute07_id, a.attribute08_id, a.attribute09_id, a.attribute10_id, "+&
"		b.include_wo, b.include_jt, b.include_ua, b.include_dept, "+&
"		b.include_attr01, b.include_attr02, b.include_attr03, b.include_attr04, b.include_attr05, b.include_attr06, b.include_attr07, b.include_attr08, b.include_attr09, b.include_attr10, "+&
"		to_number(substr(to_char(a.month_number),1,4)) year "+&
"	from wo_est_actuals_temp a, wo_est_update_with_act_temp b "+&
"	where a.work_order_id = b.work_order_id "+&
"	) "+&
"select pwrplant1.nextval, actuals.work_order_id, actuals.revision, actuals.year, "+&
"	actuals.expenditure_type_id, actuals.est_chg_type_id, actuals.wo_work_order_id, actuals.job_task_id, actuals.utility_account_id, actuals.department_id, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
"	actuals.attribute01_id, actuals.attribute02_id, actuals.attribute03_id, actuals.attribute04_id, actuals.attribute05_id, actuals.attribute06_id, actuals.attribute07_id, actuals.attribute08_id, actuals.attribute09_id, actuals.attribute10_id, "+&
"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "+&
"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "+&
"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 "+&
"from actuals "+&
"where not exists ( "+&
"	select 1 from wo_est_monthly d "+&
"	where actuals.work_order_id = d.work_order_id "+&
"	and actuals.revision = d.revision "+&
"	and actuals.year = d.year "+&
"	and actuals.expenditure_type_id = d.expenditure_type_id "+&
"	and actuals.est_chg_type_id = d.est_chg_type_id "+&
"	and nvl(actuals.wo_work_order_id, 0) = nvl(decode(actuals.include_wo,1,d.wo_work_order_id,actuals.wo_work_order_id), 0) "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
"	and nvl(actuals.job_task_id, '*') = nvl(decode(actuals.include_jt,1,d.job_task_id,actuals.job_task_id), '*') "+&
"	and nvl(actuals.utility_account_id, 0) = nvl(decode(actuals.include_ua,1,d.utility_account_id,actuals.utility_account_id), 0) "+&
"	and nvl(actuals.department_id, 0) = nvl(decode(actuals.include_dept,1,d.department_id,actuals.department_id), 0) "+&
"	and nvl(actuals.attribute01_id, 0) = nvl(decode(actuals.include_attr01,1,d.attribute01_id,actuals.attribute01_id), 0) "+&
"	and nvl(actuals.attribute02_id, 0) = nvl(decode(actuals.include_attr02,1,d.attribute02_id,actuals.attribute02_id), 0) "+&
"	and nvl(actuals.attribute03_id, 0) = nvl(decode(actuals.include_attr03,1,d.attribute03_id,actuals.attribute03_id), 0) "+&
"	and nvl(actuals.attribute04_id, 0) = nvl(decode(actuals.include_attr04,1,d.attribute04_id,actuals.attribute04_id), 0) "+&
"	and nvl(actuals.attribute05_id, 0) = nvl(decode(actuals.include_attr05,1,d.attribute05_id,actuals.attribute05_id), 0) "+&
"	and nvl(actuals.attribute06_id, 0) = nvl(decode(actuals.include_attr06,1,d.attribute06_id,actuals.attribute06_id), 0) "+&
"	and nvl(actuals.attribute07_id, 0) = nvl(decode(actuals.include_attr07,1,d.attribute07_id,actuals.attribute07_id), 0) "+&
"	and nvl(actuals.attribute08_id, 0) = nvl(decode(actuals.include_attr08,1,d.attribute08_id,actuals.attribute08_id), 0) "+&
"	and nvl(actuals.attribute09_id, 0) = nvl(decode(actuals.include_attr09,1,d.attribute09_id,actuals.attribute09_id), 0) "+&
"	and nvl(actuals.attribute10_id, 0) = nvl(decode(actuals.include_attr10,1,d.attribute10_id,actuals.attribute10_id), 0) "+&
"	) "

sqls = f_sql_add_hint(sqls, 'uf_update_with_act_20')

execute immediate :sqls;

if uf_check_sql('Inserting missing estimate attribute combinations into wo_est_monthly') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')

if sqlca.sqlnrows > 0 then
	new_ests = true
else
	new_ests = false
end if


////
//// Insert any missing years after dates have changed
////
//uf_msg('Inserting missing years from estimated range','I')
//sqls = &
//"insert into wo_est_monthly "+&
//"(work_order_id, est_monthly_id, revision, year, "+&
//"	expenditure_type_id,est_chg_type_id,department_id,utility_account_id,wo_work_order_id,job_task_id,long_description, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
//"	january,february,march,april,may,june,july,august,september,october,november,december,total, "+&
//"	hrs_jan,hrs_feb,hrs_mar,hrs_apr,hrs_may,hrs_jun,hrs_jul,hrs_aug,hrs_sep,hrs_oct,hrs_nov,hrs_dec,hrs_total, "+&
//"	qty_jan,qty_feb,qty_mar,qty_apr,qty_may,qty_jun,qty_jul,qty_aug,qty_sep,qty_oct,qty_nov,qty_dec,qty_total ) "+&
//"select a.work_order_id, a.est_monthly_id, a.revision, b.year, "+&
//"	min(expenditure_type_id), min(est_chg_type_id), min(department_id), min(utility_account_id), min(wo_work_order_id), min(job_task_id), min(long_description), "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
//"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "+&
//"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "+&
//"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 "+&
//"from wo_est_monthly a, pp_table_years b, work_order_approval c, wo_est_update_with_act_temp temp "+&
//"where a.work_order_id = c.work_order_id "+&
//"and a.revision = c.revision "+&
//"and c.work_order_id = temp.work_order_id "+&
//"and c.revision = temp.revision "+&
//"and b.year between to_char(c.est_start_date,'yyyy') and to_char(c.est_complete_date,'yyyy') "+&
//"and not exists "+&
//"	(	select 1 from wo_est_monthly c  "+&
//"		where c.work_order_id = a.work_order_id "+&
//"		and c.revision = a.revision "+&
//"		and c.est_monthly_id = a.est_monthly_id "+&
//"		and c.year = b.year "+&
//"	) "+&
//"group by a.work_order_id, a.est_monthly_id, a.revision, b.year "
//
//sqls = f_sql_add_hint(sqls, 'uf_update_with_act_30')
//
//execute immediate :sqls;
//
//if uf_check_sql('Inserting missing years into wo_est_monthly') <> 1 then return -1
//
//uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')


sqlca.analyze_table('wo_est_monthly')


//
// Clear out actuals months - dollars
//
uf_msg("Updating actuals months to zero",'I')
sqls = &
"update wo_est_monthly a set ( "+&
"	january, february, march, april, may, june, july, august, september, october, november, december, "+&
"	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, "+&
"	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec "+&
"	) = ( "+&
"	select decode(sign(a.year*100 + 1 - "+string(a_actuals_month)+"), 1, a.january, 0), "+&
"			decode(sign(a.year*100 + 2 - "+string(a_actuals_month)+"), 1, a.february, 0), "+&
"			decode(sign(a.year*100 + 3 - "+string(a_actuals_month)+"), 1, a.march, 0), "+&
"			decode(sign(a.year*100 + 4 - "+string(a_actuals_month)+"), 1, a.april, 0), "+&
"			decode(sign(a.year*100 + 5 - "+string(a_actuals_month)+"), 1, a.may, 0), "+&
"			decode(sign(a.year*100 + 6 - "+string(a_actuals_month)+"), 1, a.june, 0), "+&
"			decode(sign(a.year*100 + 7 - "+string(a_actuals_month)+"), 1, a.july, 0), "+&
"			decode(sign(a.year*100 + 8 - "+string(a_actuals_month)+"), 1, a.august, 0), "+&
"			decode(sign(a.year*100 + 9 - "+string(a_actuals_month)+"), 1, a.september, 0), "+&
"			decode(sign(a.year*100 + 10 - "+string(a_actuals_month)+"), 1, a.october, 0), "+&
"			decode(sign(a.year*100 + 11 - "+string(a_actuals_month)+"), 1, a.november, 0), "+&
"			decode(sign(a.year*100 + 12 - "+string(a_actuals_month)+"), 1, a.december, 0), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 1 - "+string(a_actuals_month)+"), 1, a.hrs_jan, 0),a.hrs_jan), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 2 - "+string(a_actuals_month)+"), 1, a.hrs_feb, 0),a.hrs_feb), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 3 - "+string(a_actuals_month)+"), 1, a.hrs_mar, 0),a.hrs_mar), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 4 - "+string(a_actuals_month)+"), 1, a.hrs_apr, 0),a.hrs_apr), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 5 - "+string(a_actuals_month)+"), 1, a.hrs_may, 0),a.hrs_may), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 6 - "+string(a_actuals_month)+"), 1, a.hrs_jun, 0),a.hrs_jun), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 7 - "+string(a_actuals_month)+"), 1, a.hrs_jul, 0),a.hrs_jul), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 8 - "+string(a_actuals_month)+"), 1, a.hrs_aug, 0),a.hrs_aug), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 9 - "+string(a_actuals_month)+"), 1, a.hrs_sep, 0),a.hrs_sep), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 10 - "+string(a_actuals_month)+"), 1, a.hrs_oct, 0),a.hrs_oct), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 11 - "+string(a_actuals_month)+"), 1, a.hrs_nov, 0),a.hrs_nov), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 12 - "+string(a_actuals_month)+"), 1, a.hrs_dec, 0),a.hrs_dec), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 1 - "+string(a_actuals_month)+"), 1, a.qty_jan, 0),a.qty_jan), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 2 - "+string(a_actuals_month)+"), 1, a.qty_feb, 0),a.qty_feb), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 3 - "+string(a_actuals_month)+"), 1, a.qty_mar, 0),a.qty_mar), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 4 - "+string(a_actuals_month)+"), 1, a.qty_apr, 0),a.qty_apr), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 5 - "+string(a_actuals_month)+"), 1, a.qty_may, 0),a.qty_may), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 6 - "+string(a_actuals_month)+"), 1, a.qty_jun, 0),a.qty_jun), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 7 - "+string(a_actuals_month)+"), 1, a.qty_jul, 0),a.qty_jul), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 8 - "+string(a_actuals_month)+"), 1, a.qty_aug, 0),a.qty_aug), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 9 - "+string(a_actuals_month)+"), 1, a.qty_sep, 0),a.qty_sep), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 10 - "+string(a_actuals_month)+"), 1, a.qty_oct, 0),a.qty_oct), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 11 - "+string(a_actuals_month)+"), 1, a.qty_nov, 0),a.qty_nov), "+&
"			decode(c.hrs_qty_update,1,decode(sign(a.year*100 + 12 - "+string(a_actuals_month)+"), 1, a.qty_dec, 0),a.qty_dec) "+&
"	from wo_est_update_with_act_temp c "+&
"	where c.work_order_id = a.work_order_id "+&
"	and c.revision = a.revision "+&
"	) "+&
"where exists ( "+&
"	select 1 from wo_est_update_with_act_temp b "+&
"	where b.work_order_id = a.work_order_id "+&
"	and b.revision = a.revision "+&
"	) "
if i_only_cur_year then
	sqls += &
	"and a.year = "+left(string(a_actuals_month),4)+" "
else
	sqls += &
	"and a.year <= "+left(string(a_actuals_month),4)+" "
end if

sqls = f_sql_add_hint(sqls, 'uf_update_with_act_40')

execute immediate :sqls;

if uf_check_sql('Clearing out actuals months on wo_est_monthly') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


uf_msg("Updating inclusion indicators",'I')
update wo_est_monthly a
set (a.uwa_include_wo, a.uwa_include_jt, a.uwa_include_ua, a.uwa_include_dept) = (
	select b.include_wo, b.include_jt, b.include_ua, b.include_dept
	from wo_est_update_with_act_temp b
	where b.work_order_id = a.work_order_id
	and b.revision = a.revision
	)
where exists (
	select 1 from wo_est_update_with_act_temp b
	where b.work_order_id = a.work_order_id
	and b.revision = a.revision
	);
if uf_check_sql('Updating inclusion indicators') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Check if we are updating hours and quantities
//
select count(*)
into :hrs_qty_update
from wo_est_update_with_act_temp
where hrs_qty_update = 1;


//
// Get the distinct list of years
//
cnt = f_get_column(results,"select distinct to_number(substr(month_number,1,4)) year from wo_est_actuals_temp order by 1 desc ")
years = results

delete from wo_est_actuals_temp2;

uf_msg("Saving off actuals records",'I')
insert into wo_est_actuals_temp2 (
	work_order_id, revision, expenditure_type_id, est_chg_type_id, wo_work_order_id, job_task_id, utility_account_id, department_id, // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	amount, hours, quantity, month_number,
	include_wo, include_jt, include_ua, include_dept, hrs_qty_update,
	include_attr01, include_attr02, include_attr03, include_attr04, include_attr05, include_attr06, include_attr07, include_attr08, include_attr09, include_attr10
	)
select work_order_id, revision, expenditure_type_id, est_chg_type_id, wo_work_order_id, job_task_id, utility_account_id, department_id, // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	amount, hours, quantity, month_number,
	include_wo, include_jt, include_ua, include_dept, hrs_qty_update,
	include_attr01, include_attr02, include_attr03, include_attr04, include_attr05, include_attr06, include_attr07, include_attr08, include_attr09, include_attr10
from wo_est_actuals_temp;

if uf_check_sql('Inserting actuals into wo_est_actuals_temp2') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')

//
// Loop over the distinct list of years
//
for i = 1 to cnt
	year = years[i]
	
	sqlca.truncate_table('wo_est_actuals_temp')
	delete from wo_est_actuals_temp;
	
	uf_msg("Processing year "+string(year),'I')
	sqls = &
	"insert into wo_est_actuals_temp ( "+&
	"   work_order_id, revision, expenditure_type_id, est_chg_type_id, wo_work_order_id, job_task_id, utility_account_id, department_id, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, "+&
	"   amount, hours, quantity, month_number, "+&
	"   include_wo, include_jt, include_ua, include_dept, hrs_qty_update, "+&
	"	include_attr01, include_attr02, include_attr03, include_attr04, include_attr05, include_attr06, include_attr07, include_attr08, include_attr09, include_attr10 "+&
	"   ) "+&
	"select work_order_id, revision, expenditure_type_id, est_chg_type_id, wo_work_order_id, job_task_id, utility_account_id, department_id, "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, "+&
	"   amount, hours, quantity, month_number, "+&
	"   include_wo, include_jt, include_ua, include_dept, hrs_qty_update, "+&
	"	include_attr01, include_attr02, include_attr03, include_attr04, include_attr05, include_attr06, include_attr07, include_attr08, include_attr09, include_attr10 "+&
	"from wo_est_actuals_temp2 "+&
	"where substr(month_number,1,4) = "+string(year)+" "
	
	execute immediate :sqls;
	
	if uf_check_sql('Inserting year '+string(year)+' into wo_est_actuals_temp') <> 1 then return -1
	
	uf_msg('   '+string(sqlca.sqlnrows)+' actuals records being processed','I')
	
	// Analyze table
	sqlca.analyze_table('wo_est_actuals_temp')
	
	
	//
	// Update actuals months - dollars - back to wo_est_transpose_temp for existing estimates only
	//
	uf_msg("Updating months with actuals",'I')
	sqls = &
	"update wo_est_monthly a set ( "+&
	"	january, february, march, april, may, june, july, august, september, october, november, december "
	if hrs_qty_update > 0 then
		sqls += &
		"	, "+&
		"	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, "+&
		"	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec "
	end if
	sqls += &
	"	) = ( "+&
	"	select "
	if string(year) = left(string(a_actuals_month),4) then
		sqls += &
		"			decode(sign(a.year||'01' - "+string(a_actuals_month)+"), 1, a.january, sum(decode(substr(b.month_number, 5, 2), '01', b.amount, 0))) january, "+&
		"			decode(sign(a.year||'02' - "+string(a_actuals_month)+"), 1, a.february, sum(decode(substr(b.month_number, 5, 2), '02', b.amount, 0))) february, "+&
		"			decode(sign(a.year||'03' - "+string(a_actuals_month)+"), 1, a.march, sum(decode(substr(b.month_number, 5, 2), '03', b.amount, 0))) march, "+&
		"			decode(sign(a.year||'04' - "+string(a_actuals_month)+"), 1, a.april, sum(decode(substr(b.month_number, 5, 2), '04', b.amount, 0))) april, "+&
		"			decode(sign(a.year||'05' - "+string(a_actuals_month)+"), 1, a.may, sum(decode(substr(b.month_number, 5, 2), '05', b.amount, 0))) may, "+&
		"			decode(sign(a.year||'06' - "+string(a_actuals_month)+"), 1, a.june, sum(decode(substr(b.month_number, 5, 2), '06', b.amount, 0))) june, "+&
		"			decode(sign(a.year||'07' - "+string(a_actuals_month)+"), 1, a.july, sum(decode(substr(b.month_number, 5, 2), '07', b.amount, 0))) july, "+&
		"			decode(sign(a.year||'08' - "+string(a_actuals_month)+"), 1, a.august, sum(decode(substr(b.month_number, 5, 2), '08', b.amount, 0))) august, "+&
		"			decode(sign(a.year||'09' - "+string(a_actuals_month)+"), 1, a.september, sum(decode(substr(b.month_number, 5, 2), '09', b.amount, 0))) september, "+&
		"			decode(sign(a.year||'10' - "+string(a_actuals_month)+"), 1, a.october, sum(decode(substr(b.month_number, 5, 2), '10', b.amount, 0))) october, "+&
		"			decode(sign(a.year||'11' - "+string(a_actuals_month)+"), 1, a.november, sum(decode(substr(b.month_number, 5, 2), '11', b.amount, 0))) november, "+&
		"			decode(sign(a.year||'12' - "+string(a_actuals_month)+"), 1, a.december, sum(decode(substr(b.month_number, 5, 2), '12', b.amount, 0))) december "
	else
		sqls += &
		"			sum(decode(substr(b.month_number, 5, 2), '01', b.amount, 0)) january, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '02', b.amount, 0)) february, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '03', b.amount, 0)) march, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '04', b.amount, 0)) april, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '05', b.amount, 0)) may, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '06', b.amount, 0)) june, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '07', b.amount, 0)) july, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '08', b.amount, 0)) august, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '09', b.amount, 0)) september, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '10', b.amount, 0)) october, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '11', b.amount, 0)) november, "+&
		"			sum(decode(substr(b.month_number, 5, 2), '12', b.amount, 0)) december "
	end if
	if hrs_qty_update > 0 then
		sqls += &
		"			, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'01' - "+string(a_actuals_month)+"), 1, a.hrs_jan, sum(decode(substr(b.month_number, 5, 2), '01', b.hours, 0))), a.hrs_jan) hrs_jan, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'02' - "+string(a_actuals_month)+"), 1, a.hrs_feb, sum(decode(substr(b.month_number, 5, 2), '02', b.hours, 0))), a.hrs_feb) hrs_feb, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'03' - "+string(a_actuals_month)+"), 1, a.hrs_mar, sum(decode(substr(b.month_number, 5, 2), '03', b.hours, 0))), a.hrs_mar) hrs_mar, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'04' - "+string(a_actuals_month)+"), 1, a.hrs_apr, sum(decode(substr(b.month_number, 5, 2), '04', b.hours, 0))), a.hrs_apr) hrs_apr, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'05' - "+string(a_actuals_month)+"), 1, a.hrs_may, sum(decode(substr(b.month_number, 5, 2), '05', b.hours, 0))), a.hrs_may) hrs_may, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'06' - "+string(a_actuals_month)+"), 1, a.hrs_jun, sum(decode(substr(b.month_number, 5, 2), '06', b.hours, 0))), a.hrs_jun) hrs_jun, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'07' - "+string(a_actuals_month)+"), 1, a.hrs_jul, sum(decode(substr(b.month_number, 5, 2), '07', b.hours, 0))), a.hrs_jul) hrs_jul, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'08' - "+string(a_actuals_month)+"), 1, a.hrs_aug, sum(decode(substr(b.month_number, 5, 2), '08', b.hours, 0))), a.hrs_aug) hrs_aug, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'09' - "+string(a_actuals_month)+"), 1, a.hrs_sep, sum(decode(substr(b.month_number, 5, 2), '09', b.hours, 0))), a.hrs_sep) hrs_sep, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'10' - "+string(a_actuals_month)+"), 1, a.hrs_oct, sum(decode(substr(b.month_number, 5, 2), '10', b.hours, 0))), a.hrs_oct) hrs_oct, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'11' - "+string(a_actuals_month)+"), 1, a.hrs_nov, sum(decode(substr(b.month_number, 5, 2), '11', b.hours, 0))), a.hrs_nov) hrs_nov, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'12' - "+string(a_actuals_month)+"), 1, a.hrs_dec, sum(decode(substr(b.month_number, 5, 2), '12', b.hours, 0))), a.hrs_dec) hrs_dec, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'01' - "+string(a_actuals_month)+"), 1, a.qty_jan, sum(decode(substr(b.month_number, 5, 2), '01', b.quantity, 0))), a.hrs_jan) qty_jan, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'02' - "+string(a_actuals_month)+"), 1, a.qty_feb, sum(decode(substr(b.month_number, 5, 2), '02', b.quantity, 0))), a.hrs_feb) qty_feb, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'03' - "+string(a_actuals_month)+"), 1, a.qty_mar, sum(decode(substr(b.month_number, 5, 2), '03', b.quantity, 0))), a.hrs_mar) qty_mar, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'04' - "+string(a_actuals_month)+"), 1, a.qty_apr, sum(decode(substr(b.month_number, 5, 2), '04', b.quantity, 0))), a.hrs_apr) qty_apr, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'05' - "+string(a_actuals_month)+"), 1, a.qty_may, sum(decode(substr(b.month_number, 5, 2), '05', b.quantity, 0))), a.hrs_may) qty_may, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'06' - "+string(a_actuals_month)+"), 1, a.qty_jun, sum(decode(substr(b.month_number, 5, 2), '06', b.quantity, 0))), a.hrs_jun) qty_jun, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'07' - "+string(a_actuals_month)+"), 1, a.qty_jul, sum(decode(substr(b.month_number, 5, 2), '07', b.quantity, 0))), a.hrs_jul) qty_jul, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'08' - "+string(a_actuals_month)+"), 1, a.qty_aug, sum(decode(substr(b.month_number, 5, 2), '08', b.quantity, 0))), a.hrs_aug) qty_aug, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'09' - "+string(a_actuals_month)+"), 1, a.qty_sep, sum(decode(substr(b.month_number, 5, 2), '09', b.quantity, 0))), a.hrs_sep) qty_sep, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'10' - "+string(a_actuals_month)+"), 1, a.qty_oct, sum(decode(substr(b.month_number, 5, 2), '10', b.quantity, 0))), a.hrs_oct) qty_oct, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'11' - "+string(a_actuals_month)+"), 1, a.qty_nov, sum(decode(substr(b.month_number, 5, 2), '11', b.quantity, 0))), a.hrs_nov) qty_nov, "+&
		"			decode(b.hrs_qty_update,1,decode(sign(a.year||'12' - "+string(a_actuals_month)+"), 1, a.qty_dec, sum(decode(substr(b.month_number, 5, 2), '12', b.quantity, 0))), a.hrs_dec) qty_dec "
	end if
	sqls += &
	"	from wo_est_actuals_temp b "+&
	"	where b.work_order_id = a.work_order_id "+&
	"	and substr(b.month_number, 1, 4) = a.year "+&
	"	and b.expenditure_type_id = a.expenditure_type_id "+&
	"	and b.est_chg_type_id = a.est_chg_type_id "+&
	"	and decode(b.include_wo,1,nvl(b.wo_work_order_id, 0),1) = decode(a.uwa_include_wo,1,nvl(a.wo_work_order_id, 0),1) "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"	and decode(b.include_jt,1,nvl(b.job_task_id, '*'),'*') = decode(a.uwa_include_jt,1,nvl(a.job_task_id, '*'),'*') "+&
	"	and decode(b.include_ua,1,nvl(b.utility_account_id, 0),1) = decode(a.uwa_include_ua,1,nvl(a.utility_account_id, 0),1) "+&
	"	and decode(b.include_dept,1,nvl(b.department_id, 0),1) = decode(a.uwa_include_dept,1,nvl(a.department_id, 0),1) "+&
	"	and decode(b.include_attr01,1,nvl(b.attribute01_id, 0),1) = decode(a.uwa_include_attr01,1,nvl(a.attribute01_id, 0),1) "+&
	"	and decode(b.include_attr02,1,nvl(b.attribute02_id, 0),1) = decode(a.uwa_include_attr02,1,nvl(a.attribute02_id, 0),1) "+&
	"	and decode(b.include_attr03,1,nvl(b.attribute03_id, 0),1) = decode(a.uwa_include_attr03,1,nvl(a.attribute03_id, 0),1) "+&
	"	and decode(b.include_attr04,1,nvl(b.attribute04_id, 0),1) = decode(a.uwa_include_attr04,1,nvl(a.attribute04_id, 0),1) "+&
	"	and decode(b.include_attr05,1,nvl(b.attribute05_id, 0),1) = decode(a.uwa_include_attr05,1,nvl(a.attribute05_id, 0),1) "+&
	"	and decode(b.include_attr06,1,nvl(b.attribute06_id, 0),1) = decode(a.uwa_include_attr06,1,nvl(a.attribute06_id, 0),1) "+&
	"	and decode(b.include_attr07,1,nvl(b.attribute07_id, 0),1) = decode(a.uwa_include_attr07,1,nvl(a.attribute07_id, 0),1) "+&
	"	and decode(b.include_attr08,1,nvl(b.attribute08_id, 0),1) = decode(a.uwa_include_attr08,1,nvl(a.attribute08_id, 0),1) "+&
	"	and decode(b.include_attr09,1,nvl(b.attribute09_id, 0),1) = decode(a.uwa_include_attr09,1,nvl(a.attribute09_id, 0),1) "+&
	"	and decode(b.include_attr10,1,nvl(b.attribute10_id, 0),1) = decode(a.uwa_include_attr10,1,nvl(a.attribute10_id, 0),1) "
	if hrs_qty_update > 0 then
		sqls += &
		"	group by b.hrs_qty_update "
	end if
	sqls += &
	"	) "+&
	"where (a.work_order_id, a.revision) in ( "+&
	"	select b.work_order_id, b.revision from wo_est_update_with_act_temp b "+&
	"	) "+&
	"and exists ( "+&
	"	select 1 from wo_est_monthly b "+&
	"	where b.work_order_id = a.work_order_id "+&
	"	and b.revision = a.revision "+&
	"	and b.year = a.year "+&
	"	and b.expenditure_type_id = a.expenditure_type_id "+&
	"	and b.est_chg_type_id = a.est_chg_type_id "+&
	"	and decode(b.uwa_include_wo,1,nvl(b.wo_work_order_id, 0),1) = decode(a.uwa_include_wo,1,nvl(a.wo_work_order_id, 0),1) "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"	and decode(b.uwa_include_jt,1,nvl(b.job_task_id, '*'),'*') = decode(a.uwa_include_jt,1,nvl(a.job_task_id, '*'),'*') "+&
	"	and decode(b.uwa_include_ua,1,nvl(b.utility_account_id, 0),1) = decode(a.uwa_include_ua,1,nvl(a.utility_account_id, 0),1) "+&
	"	and decode(b.uwa_include_dept,1,nvl(b.department_id, 0),1) = decode(a.uwa_include_dept,1,nvl(a.department_id, 0),1) "+&
	"	and decode(b.uwa_include_attr01,1,nvl(b.attribute01_id, 0),1) = decode(a.uwa_include_attr01,1,nvl(a.attribute01_id, 0),1) "+&
	"	and decode(b.uwa_include_attr02,1,nvl(b.attribute02_id, 0),1) = decode(a.uwa_include_attr02,1,nvl(a.attribute02_id, 0),1) "+&
	"	and decode(b.uwa_include_attr03,1,nvl(b.attribute03_id, 0),1) = decode(a.uwa_include_attr03,1,nvl(a.attribute03_id, 0),1) "+&
	"	and decode(b.uwa_include_attr04,1,nvl(b.attribute04_id, 0),1) = decode(a.uwa_include_attr04,1,nvl(a.attribute04_id, 0),1) "+&
	"	and decode(b.uwa_include_attr05,1,nvl(b.attribute05_id, 0),1) = decode(a.uwa_include_attr05,1,nvl(a.attribute05_id, 0),1) "+&
	"	and decode(b.uwa_include_attr06,1,nvl(b.attribute06_id, 0),1) = decode(a.uwa_include_attr06,1,nvl(a.attribute06_id, 0),1) "+&
	"	and decode(b.uwa_include_attr07,1,nvl(b.attribute07_id, 0),1) = decode(a.uwa_include_attr07,1,nvl(a.attribute07_id, 0),1) "+&
	"	and decode(b.uwa_include_attr08,1,nvl(b.attribute08_id, 0),1) = decode(a.uwa_include_attr08,1,nvl(a.attribute08_id, 0),1) "+&
	"	and decode(b.uwa_include_attr09,1,nvl(b.attribute09_id, 0),1) = decode(a.uwa_include_attr09,1,nvl(a.attribute09_id, 0),1) "+&
	"	and decode(b.uwa_include_attr10,1,nvl(b.attribute10_id, 0),1) = decode(a.uwa_include_attr10,1,nvl(a.attribute10_id, 0),1) "+&
	"	having min(b.est_monthly_id) = a.est_monthly_id "+&
	"	) "+&
	"and a.year = "+string(year)+" "+&
	"and exists ( "+&
	"	select 1 from wo_est_actuals_temp b "+&
	"	where b.work_order_id = a.work_order_id "+&
	"	and b.revision = a.revision "+&
	"	and substr(b.month_number, 1, 4) = a.year "+&
	"	and b.expenditure_type_id = a.expenditure_type_id "+&
	"	and b.est_chg_type_id = a.est_chg_type_id "+&
	"	and decode(b.include_wo,1,nvl(b.wo_work_order_id, 0),1) = decode(a.uwa_include_wo,1,nvl(a.wo_work_order_id, 0),1) "+/*### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates*/&
	"	and decode(b.include_jt,1,nvl(b.job_task_id, '*'),'*') = decode(a.uwa_include_jt,1,nvl(a.job_task_id, '*'),'*') "+&
	"	and decode(b.include_ua,1,nvl(b.utility_account_id, 0),1) = decode(a.uwa_include_ua,1,nvl(a.utility_account_id, 0),1) "+&
	"	and decode(b.include_dept,1,nvl(b.department_id, 0),1) = decode(a.uwa_include_dept,1,nvl(a.department_id, 0),1) "+&
	"	and decode(b.include_attr01,1,nvl(b.attribute01_id, 0),1) = decode(a.uwa_include_attr01,1,nvl(a.attribute01_id, 0),1) "+&
	"	and decode(b.include_attr02,1,nvl(b.attribute02_id, 0),1) = decode(a.uwa_include_attr02,1,nvl(a.attribute02_id, 0),1) "+&
	"	and decode(b.include_attr03,1,nvl(b.attribute03_id, 0),1) = decode(a.uwa_include_attr03,1,nvl(a.attribute03_id, 0),1) "+&
	"	and decode(b.include_attr04,1,nvl(b.attribute04_id, 0),1) = decode(a.uwa_include_attr04,1,nvl(a.attribute04_id, 0),1) "+&
	"	and decode(b.include_attr05,1,nvl(b.attribute05_id, 0),1) = decode(a.uwa_include_attr05,1,nvl(a.attribute05_id, 0),1) "+&
	"	and decode(b.include_attr06,1,nvl(b.attribute06_id, 0),1) = decode(a.uwa_include_attr06,1,nvl(a.attribute06_id, 0),1) "+&
	"	and decode(b.include_attr07,1,nvl(b.attribute07_id, 0),1) = decode(a.uwa_include_attr07,1,nvl(a.attribute07_id, 0),1) "+&
	"	and decode(b.include_attr08,1,nvl(b.attribute08_id, 0),1) = decode(a.uwa_include_attr08,1,nvl(a.attribute08_id, 0),1) "+&
	"	and decode(b.include_attr09,1,nvl(b.attribute09_id, 0),1) = decode(a.uwa_include_attr09,1,nvl(a.attribute09_id, 0),1) "+&
	"	and decode(b.include_attr10,1,nvl(b.attribute10_id, 0),1) = decode(a.uwa_include_attr10,1,nvl(a.attribute10_id, 0),1) "+&
	"	) "
	
	sqls = f_sql_add_hint(sqls, 'uf_update_with_act_50')
	
	execute immediate :sqls;
	
	if uf_check_sql('Updating actuals months in wo_est_monthly') <> 1 then return -1
	
	uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')
	
next


//
// Update totals to sum of months and null months to zero
//
uf_msg("Updating totals and null months",'I')
sqls = &
"update wo_est_monthly a set "+&
"	january = nvl(january, 0), "+&
"	february = nvl(february, 0), "+&
"	march = nvl(march, 0), "+&
"	april = nvl(april, 0), "+&
"	may = nvl(may, 0), "+&
"	june = nvl(june, 0), "+&
"	july = nvl(july, 0), "+&
"	august = nvl(august, 0), "+&
"	september = nvl(september, 0), "+&
"	october = nvl(october, 0), "+&
"	november = nvl(november, 0), "+&
"	december = nvl(december, 0), "+&
"	total = nvl(january, 0) + nvl(february, 0) + nvl(march, 0) + nvl(april, 0) + nvl(may, 0) + nvl(june, 0) + nvl(july, 0) + nvl(august, 0) + nvl(september, 0) + nvl(october, 0) + nvl(november, 0) + nvl(december, 0) "
if hrs_qty_update > 0 then
	sqls += &
	"	, "+&
	"	hrs_jan = nvl(hrs_jan, 0), "+&
	"	hrs_feb = nvl(hrs_feb, 0), "+&
	"	hrs_mar = nvl(hrs_mar, 0), "+&
	"	hrs_apr = nvl(hrs_apr, 0), "+&
	"	hrs_may = nvl(hrs_may, 0), "+&
	"	hrs_jun = nvl(hrs_jun, 0), "+&
	"	hrs_jul = nvl(hrs_jul, 0), "+&
	"	hrs_aug = nvl(hrs_aug, 0), "+&
	"	hrs_sep = nvl(hrs_sep, 0), "+&
	"	hrs_oct = nvl(hrs_oct, 0), "+&
	"	hrs_nov = nvl(hrs_nov, 0), "+&
	"	hrs_dec = nvl(hrs_dec, 0), "+&
	"	hrs_total = nvl(hrs_jan, 0) + nvl(hrs_feb, 0) + nvl(hrs_mar, 0) + nvl(hrs_apr, 0) + nvl(hrs_may, 0) + nvl(hrs_jun, 0) + nvl(hrs_jul, 0) + nvl(hrs_aug, 0) + nvl(hrs_sep, 0) + nvl(hrs_oct, 0) + nvl(hrs_nov, 0) + nvl(hrs_dec, 0), "+&
	"	qty_jan = nvl(qty_jan, 0), "+&
	"	qty_feb = nvl(qty_feb, 0), "+&
	"	qty_mar = nvl(qty_mar, 0), "+&
	"	qty_apr = nvl(qty_apr, 0), "+&
	"	qty_may = nvl(qty_may, 0), "+&
	"	qty_jun = nvl(qty_jun, 0), "+&
	"	qty_jul = nvl(qty_jul, 0), "+&
	"	qty_aug = nvl(qty_aug, 0), "+&
	"	qty_sep = nvl(qty_sep, 0), "+&
	"	qty_oct = nvl(qty_oct, 0), "+&
	"	qty_nov = nvl(qty_nov, 0), "+&
	"	qty_dec = nvl(qty_dec, 0), "+&
	"	qty_total = nvl(qty_jan, 0) + nvl(qty_feb, 0) + nvl(qty_mar, 0) + nvl(qty_apr, 0) + nvl(qty_may, 0) + nvl(qty_jun, 0) + nvl(qty_jul, 0) + nvl(qty_aug, 0) + nvl(qty_sep, 0) + nvl(qty_oct, 0) + nvl(qty_nov, 0) + nvl(qty_dec, 0) "
end if
sqls += &
"where exists ( "+&
"	select 1 from wo_est_update_with_act_temp b "+&
"	where b.work_order_id = a.work_order_id "+&
"	and b.revision = a.revision "+&
"	) "+&
"and a.year*100 < "+string(a_actuals_month)+" "

sqls = f_sql_add_hint(sqls, 'uf_update_with_act_60')

execute immediate :sqls;

if uf_check_sql('Updating actuals months in wo_est_monthly') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Zero out escalations for actuals months
//
uf_msg("Updating escalations to zero for actuals months",'I')
sqls = &
"update wo_est_monthly_escalation esc "+&
"set january = decode(sign(to_number(year||'01') - "+string(a_actuals_month)+"),1,nvl(january,0),0), "+&
"	february = decode(sign(to_number(year||'02') - "+string(a_actuals_month)+"),1,nvl(february,0),0), "+&
"	march = decode(sign(to_number(year||'03') - "+string(a_actuals_month)+"),1,nvl(march,0),0), "+&
"	april = decode(sign(to_number(year||'04') - "+string(a_actuals_month)+"),1,nvl(april,0),0), "+&
"	may = decode(sign(to_number(year||'05') - "+string(a_actuals_month)+"),1,nvl(may,0),0), "+&
"	june = decode(sign(to_number(year||'06') - "+string(a_actuals_month)+"),1,nvl(june,0),0), "+&
"	july = decode(sign(to_number(year||'07') - "+string(a_actuals_month)+"),1,nvl(july,0),0), "+&
"	august = decode(sign(to_number(year||'08') - "+string(a_actuals_month)+"),1,nvl(august,0),0), "+&
"	september = decode(sign(to_number(year||'09') - "+string(a_actuals_month)+"),1,nvl(september,0),0), "+&
"	october = decode(sign(to_number(year||'10') - "+string(a_actuals_month)+"),1,nvl(october,0),0), "+&
"	november = decode(sign(to_number(year||'11') - "+string(a_actuals_month)+"),1,nvl(november,0),0), "+&
"	december = decode(sign(to_number(year||'12') - "+string(a_actuals_month)+"),1,nvl(december,0),0), "+&
"	total = "+&
"		decode(sign(to_number(year||'01') - "+string(a_actuals_month)+"),1,nvl(january,0),0)+ "+&
"		decode(sign(to_number(year||'02') - "+string(a_actuals_month)+"),1,nvl(february,0),0)+  "+&
"		decode(sign(to_number(year||'03') - "+string(a_actuals_month)+"),1,nvl(march,0),0)+ "+&
"		decode(sign(to_number(year||'04') - "+string(a_actuals_month)+"),1,nvl(april,0),0)+ "+&
"		decode(sign(to_number(year||'05') - "+string(a_actuals_month)+"),1,nvl(may,0),0)+ "+&
"		decode(sign(to_number(year||'06') - "+string(a_actuals_month)+"),1,nvl(june,0),0)+ "+&
"		decode(sign(to_number(year||'07') - "+string(a_actuals_month)+"),1,nvl(july,0),0)+ "+&
"		decode(sign(to_number(year||'08') - "+string(a_actuals_month)+"),1,nvl(august,0),0)+ "+&
"		decode(sign(to_number(year||'09') - "+string(a_actuals_month)+"),1,nvl(september,0),0)+ "+&
"		decode(sign(to_number(year||'10') - "+string(a_actuals_month)+"),1,nvl(october,0),0)+ "+&
"		decode(sign(to_number(year||'11') - "+string(a_actuals_month)+"),1,nvl(november,0),0)+ "+&
"		decode(sign(to_number(year||'12') - "+string(a_actuals_month)+"),1,nvl(december,0),0) "+&
"where exists ( "+&
"	select 1 from wo_est_monthly wem, wo_est_update_with_act_temp temp "+&
"	where wem.est_monthly_id = esc.est_monthly_id "+&
"	and wem.year = esc.year "+&
"	and wem.work_order_id = temp.work_order_id "+&
"	and wem.revision = temp.revision "+&
"	and wem.year <= to_number(substr(to_char("+string(a_actuals_month)+"),1,4)) "+&
"	) "+&
"and esc.year <= to_number(substr(to_char("+string(a_actuals_month)+"),1,4)) "

sqls = f_sql_add_hint(sqls, 'uf_update_with_act_70')

execute immediate :sqls;

if uf_check_sql('Zeroing out escalations for actuals months') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


////
//// Insert spread records
////
//if new_ests then
//	uf_msg("Inserting missing monthly ids into spread table",'I')
//	sqls = &
//	"insert into wo_est_monthly_spread (est_monthly_id) "+&
//	"select est_monthly_id from wo_est_monthly a "+&
//	"where exists ( "+&
//	"	select 1 from wo_est_update_with_act_temp b "+&
//	"	where b.work_order_id = a.work_order_id "+&
//	"	and b.revision = a.revision "+&
//	"	) "+&
//	"minus "+&
//	"select est_monthly_id from wo_est_monthly_spread "
//	
//	sqls = f_sql_add_hint(sqls, 'uf_update_with_act_80')
//	
//	execute immediate :sqls;
//	
//	if uf_check_sql('Inserting records into wo_est_monthly_spread') <> 1 then return -1
//end if


//
// Update has_respread = 0 for funding projects that have not yet been updated with actuals through selected month
//
// ### CDM - Maint 8445 - add logic to update has_respread
if a_wo_fp = 'fp' then
	uf_msg("Updating has_respread on budget_version_fund_proj",'I')
	update budget_version_fund_proj bvfp
	set has_respread = 0
	where exists (
		select 1 from wo_est_update_with_act_temp temp, work_order_approval woa
		where temp.work_order_id = woa.work_order_id
		and temp.revision = woa.revision
		and woa.work_order_id = bvfp.work_order_id
		and woa.revision = bvfp.revision
		and nvl(woa.actuals_month_number,0) < :a_actuals_month
		);
	
	if uf_check_sql('Updating has_respread on budget_version_fund_proj') <> 1 then return -1
	
	uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')
end if


//
// Update actuals month number on work_order_approval
//
uf_msg("Updating actuals month number on revision",'I')
update work_order_approval woa
set woa.actuals_month_number = :a_actuals_month
where exists (
	select 1 from wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	);

if uf_check_sql('Updating actuals_month_number on work_order_approval') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Call to custom function after all processing is complete
//
cst_rtn = f_budget_revisions_custom(func_name,4,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,4,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '4'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
args[4] = string(long_arr[1])
args[5] = string(str_arr[1])
cst_rtn3 = f_wo_validation_control(83,args)
if cst_rtn3 < 0 then 
   return -1
end if


uf_msg("Done Updating with Actuals",'I')

return 1

end function

public function longlong uf_new_revision (string a_wo_fp);//------------------------------------------------------------------------
//
// FUNCTION:		uf_new_revision
//
// DESCRIPTION:	Copy existing revisions or create new revisions
//						for work orders or funding projects
//
//	ASSUMPTIONS:	1 - work_order_id/revision must be inserted into
//								wo_est_processing_temp
//						2 - When Copying revisions, must have a valid revision in the
//								revision column of wo_est_processing_temp
//						3 - When creating New revisions, must have zero (0) in the
//								revision column of wo_est_processing_temp
//						4 - Defaulting authorizers and multiple approvers only applies
//								to New revisions, not Copied revisions
//						5 - Defaulting budget versions on fp revisions only applies
//								to New revisions, and only when i_new_bv_ids variable
//								has no elements in the array
//
//	ARGUMENTS:		a_wo_fp -				string -		'wo' for work orders
//																	'fp' for funding projects
//
// VARIABLES:		i_new_bv_ids -		longlong array -	array of budget_version_ids to
//																	associate to new fp revisions
//
// RETURN CODES:	1 - SUCCESS
//					  -1 - ERROR
//
//------------------------------------------------------------------------
string bv_ids_str, sqls, func_name, str_arr[], s_just_field, ret, args[]
longlong long_arr[], cst_rtn, cst_rtn2, cst_rtn3, i, tab_ind, stage_id, called_from_send_for_appr
uo_ds_top ds_new_just

a_wo_fp = lower(trim(a_wo_fp))
func_name = 'uf_new_revision'

//
// Call to custom function before any processing
//
cst_rtn = f_budget_revisions_custom(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
long_arr[1] = 0
str_arr[1] = ' '
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '1'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
cst_rtn3 = f_wo_validation_control(85,args)
if cst_rtn3 < 0 then 
   return -1
end if

//
// Stage revisions along with new revision numbers
//
delete from wo_est_copy_revision_temp;

insert into wo_est_copy_revision_temp (
	work_order_id, old_revision, new_revision )
select a.work_order_id, a.revision, greatest(nvl(max(b.revision),0),nvl(max(c.revision),0)) + 1 revision
from wo_est_processing_temp a, 
	(	select work_order_id, nvl(max(revision),0) revision
		from work_order_approval
		where revision < 1000
		group by work_order_id
	) b,
	(	select work_order_id, nvl(max(revision),0) revision
		from work_order_approval_arch
		group by work_order_id
	) c
where a.work_order_id = b.work_order_id (+)
and a.work_order_id = c.work_order_id (+)
group by a.work_order_id, a.revision;

if uf_check_sql('Inserting into wo_est_copy_revision_temp') <> 1 then return -1

//
// Mark revisions as auto-approved (copy revisions) for the insert into work_order_approval
//
update wo_est_copy_revision_temp z
set auto_approved = 1
where 
	(	exists (
			select 1 from work_order_approval a, approval b, work_order_control c,
				(	select company.company_id, pp.control_value from company, pp_system_control_companies pp
					where lower(trim(pp.control_name (+))) = lower(trim('Workflow User Object'))
					and pp.company_id (+) = company.company_id
				) pp
			where a.approval_type_id = b.approval_type_id
			and b.auth_level1 is null and b.auth_level2 is null
			and b.auth_level3 is null and b.auth_level4 is null
			and b.auth_level5 is null and b.auth_level6 is null
			and b.auth_level7 is null and b.auth_level8 is null
			and b.auth_level9 is null and b.auth_level10 is null
			and a.work_order_id = z.work_order_id
			and a.revision = z.old_revision
			and a.work_order_id = c.work_order_id
			and c.company_id = pp.company_id
			and 'yes' <> lower(trim(nvl(pp.control_value,'no')))
			)
		or
		exists (
			select 1 from work_order_approval a, workflow_type b, work_order_control c,
				(	select wt.workflow_type_id, count(distinct wtr.workflow_rule_id) cnt
					from workflow_type wt, workflow_type_rule wtr
					where wt.workflow_type_id = wtr.workflow_type_id (+)
					group by wt.workflow_type_id
				) d,
				(	select company.company_id, pp.control_value from company, pp_system_control_companies pp
					where lower(trim(pp.control_name (+))) = lower(trim('Workflow User Object'))
					and pp.company_id (+) = company.company_id
				) pp
			where a.approval_type_id = b.workflow_type_id
			and b.workflow_type_id = d.workflow_type_id
			and d.cnt = 0
			and a.work_order_id = z.work_order_id
			and a.revision = z.old_revision
			and a.work_order_id = c.work_order_id
			and c.company_id = pp.company_id
			and 'yes' = lower(trim(nvl(pp.control_value,'no')))
			)
	)
and old_revision <> 0;

if uf_check_sql('Updating auto approved revisions (copy revisions)') <> 1 then return -1

//
// Mark revisions as auto-approved (new_revisions) for the insert into work_order_approval
//
update wo_est_copy_revision_temp z
set auto_approved = 1
where 
	(	exists (
			select 1 from work_order_type a, approval b, work_order_control c,
				(	select company.company_id, pp.control_value from company, pp_system_control_companies pp
					where lower(trim(pp.control_name (+))) = lower(trim('Workflow User Object'))
					and pp.company_id (+) = company.company_id
				) pp
			where a.approval_type_id = b.approval_type_id
			and b.auth_level1 is null and b.auth_level2 is null
			and b.auth_level3 is null and b.auth_level4 is null
			and b.auth_level5 is null and b.auth_level6 is null
			and b.auth_level7 is null and b.auth_level8 is null
			and b.auth_level9 is null and b.auth_level10 is null
			and a.work_order_type_id = c.work_order_type_id
			and c.work_order_id = z.work_order_id
			and c.company_id = pp.company_id
			and 'yes' <> lower(trim(nvl(pp.control_value,'no')))
			)
		or
		exists (
			select 1 from work_order_type a, workflow_type b, work_order_control c,
				(	select wt.workflow_type_id, count(distinct wtr.workflow_rule_id) cnt
					from workflow_type wt, workflow_type_rule wtr
					where wt.workflow_type_id = wtr.workflow_type_id (+)
					group by wt.workflow_type_id
				) d,
				(	select company.company_id, pp.control_value from company, pp_system_control_companies pp
					where lower(trim(pp.control_name (+))) = lower(trim('Workflow User Object'))
					and pp.company_id (+) = company.company_id
				) pp
			where a.approval_type_id = b.workflow_type_id
			and b.workflow_type_id = d.workflow_type_id
			and d.cnt = 0
			and a.work_order_type_id = c.work_order_type_id
			and c.work_order_id = z.work_order_id
			and c.company_id = pp.company_id
			and 'yes' = lower(trim(nvl(pp.control_value,'no')))
			)
	)
and old_revision = 0;

if uf_check_sql('Updating auto approved revisions (new revisions)') <> 1 then return -1

//
// Mark revisions to clear user text fields for the insert into work_order_approval
//
update wo_est_copy_revision_temp z
set clear_user_texts = 1
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('BUDGET - Clear Reason Codes'))
	and a.work_order_id = z.work_order_id
	and upper(trim(b.control_value)) = 'YES'
	)
and old_revision <> 0;

if uf_check_sql('Clearing user texts on work_order_approval') <> 1 then return -1

//
// Mark revisions to clear user date fields for the insert into work_order_approval
//
update wo_est_copy_revision_temp z
set clear_user_dates = 1
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('BUDGET - Clear User Dates'))
	and a.work_order_id = z.work_order_id
	and upper(trim(b.control_value)) = 'YES'
	)
and old_revision <> 0;

if uf_check_sql('Clearing user dates on work_order_approval') <> 1 then return -1

//
// Call to custom function after setup table has been built
//
cst_rtn = f_budget_revisions_custom(func_name,2,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
long_arr[1] = 0
str_arr[1] = ' '
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,2,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '2'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
cst_rtn3 = f_wo_validation_control(85,args)
if cst_rtn3 < 0 then 
   return -1
end if

//
// Insert into work_order_approval (copy revisions)
//
insert into work_order_approval (
	work_order_id, revision, approval_type_id,
	authorizer1, authorizer2,
	authorizer3, authorizer4,
	authorizer5, authorizer6,
	authorizer7, authorizer8,
	authorizer9, authorizer10,
	authorized_date1, authorized_date2, authorized_date3, authorized_date4, authorized_date5,
	authorized_date6, authorized_date7, authorized_date8, authorized_date9, authorized_date10,
	budget_version_id, rejected,
	notes, actuals_month_number, first_approver,
	revision_description, revision_long_description, budget_review_type_id,
	est_start_date, est_complete_date, est_in_service_date,
	user_text1, user_text2, user_text3, user_text4, user_text5,
	user_text6, user_text7, user_text8, user_text9, user_text10,
	user_text11, user_text12, user_text13, user_text14, user_text15,
	user_date1, user_date2, user_date3, user_date4, user_date5,
	user_date6, user_date7, user_date8, user_date9, user_date10,
	user_date11, user_date12, user_date13, user_date14, user_date15,
	approval_status_id, review_status,
	closing_pattern_id, base_year )
select a.work_order_id, b.new_revision, a.approval_type_id,
	decode(nvl(b.auto_approved,0),1,null,a.authorizer1) authorizer1, decode(nvl(b.auto_approved,0),1,null,a.authorizer2) authorizer2,
	decode(nvl(b.auto_approved,0),1,null,a.authorizer3) authorizer3, decode(nvl(b.auto_approved,0),1,null,a.authorizer4) authorizer4,
	decode(nvl(b.auto_approved,0),1,null,a.authorizer5) authorizer5, decode(nvl(b.auto_approved,0),1,null,a.authorizer6) authorizer6,
	decode(nvl(b.auto_approved,0),1,null,a.authorizer7) authorizer7, decode(nvl(b.auto_approved,0),1,null,a.authorizer8) authorizer8,
	decode(nvl(b.auto_approved,0),1,null,a.authorizer9) authorizer9, decode(nvl(b.auto_approved,0),1,null,a.authorizer10) authorizer10,
	null authorized_date1, null authorized_date2, null authorized_date3, null authorized_date4, null authorized_date5,
	null authorized_date6, null authorized_date7, null authorized_date8, null authorized_date9, null authorized_date10,
	null budget_version_id, decode(nvl(b.auto_approved,0),1,null,2) rejected,
	null notes, a.actuals_month_number, a.first_approver,
	null revision_description, null revision_long_description, a.budget_review_type_id,
	a.est_start_date, a.est_complete_date, a.est_in_service_date,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text1) user_text1,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text2) user_text2,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text3) user_text3,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text4) user_text4,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text5) user_text5,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text6) user_text6,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text7) user_text7,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text8) user_text8,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text9) user_text9,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text10) user_text10,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text11) user_text11,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text12) user_text12,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text13) user_text13,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text14) user_text14,
	decode(nvl(b.clear_user_texts,0),1,null,a.user_text15) user_text15,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date1) user_date1,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date2) user_date2,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date3) user_date3,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date4) user_date4,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date5) user_date5,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date6) user_date6,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date7) user_date7,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date8) user_date8,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date9) user_date9,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date10) user_date10,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date11) user_date11,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date12) user_date12,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date13) user_date13,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date14) user_date14,
	decode(nvl(b.clear_user_dates,0),1,null,a.user_date15) user_date15,
	decode(nvl(b.auto_approved,0),1,6,1) approval_status_id, 1 review_status,
	a.closing_pattern_id, a.base_year
from work_order_approval a, wo_est_copy_revision_temp b
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and b.old_revision <> 0;

if uf_check_sql('Copying revisions into work_order_approval') <> 1 then return -1

//
// Copy wo_approval_multiple records
//
insert into wo_approval_multiple 
(work_order_id, revision, approval_type_id, auth_level,
	users, category_id, required, requiring_user, auth_level_id)
select a.work_order_id, b.new_revision, a.approval_type_id, a.auth_level,
	a.users, a.category_id, a.required, a.requiring_user, a.auth_level_id
from wo_approval_multiple a, wo_est_copy_revision_temp b
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and b.old_revision <> 0
and nvl(b.auto_approved,0) <> 1;

if uf_check_sql('Copying records into wo_approval_multiple') <> 1 then return -1

//
// Insert into work_order_approval (new revisions) - default in any approvers
//
insert into work_order_approval (
	work_order_id, revision, approval_type_id,
	authorizer1, authorizer2, authorizer3, authorizer4, authorizer5,
	authorizer6, authorizer7, authorizer8, authorizer9, authorizer10,
	authorized_date1, authorized_date2, authorized_date3, authorized_date4, authorized_date5,
	authorized_date6, authorized_date7, authorized_date8, authorized_date9, authorized_date10,
	budget_version_id, rejected,
	notes, actuals_month_number, first_approver,
	revision_description, revision_long_description, budget_review_type_id,
	est_start_date, est_complete_date, est_in_service_date,
	user_text1, user_text2, user_text3, user_text4, user_text5,
	user_text6, user_text7, user_text8, user_text9, user_text10,
	user_text11, user_text12, user_text13, user_text14, user_text15,
	user_date1, user_date2, user_date3, user_date4, user_date5,
	user_date6, user_date7, user_date8, user_date9, user_date10,
	user_date11, user_date12, user_date13, user_date14, user_date15,
	approval_status_id, review_status,
	closing_pattern_id, base_year )
select c.work_order_id, b.new_revision, a.approval_type_id,
	decode(nvl(b.auto_approved,0),1,null,d.auth_level1) authorizer1, decode(nvl(b.auto_approved,0),1,null,d.auth_level2) authorizer2,
	decode(nvl(b.auto_approved,0),1,null,d.auth_level3) authorizer3, decode(nvl(b.auto_approved,0),1,null,d.auth_level4) authorizer4,
	decode(nvl(b.auto_approved,0),1,null,d.auth_level5) authorizer5, decode(nvl(b.auto_approved,0),1,null,d.auth_level6) authorizer6,
	decode(nvl(b.auto_approved,0),1,null,d.auth_level7) authorizer7, decode(nvl(b.auto_approved,0),1,null,d.auth_level8) authorizer8,
	decode(nvl(b.auto_approved,0),1,null,d.auth_level9) authorizer9, decode(nvl(b.auto_approved,0),1,null,d.auth_level10) authorizer10,
	null authorized_date1, null authorized_date2, null authorized_date3, null authorized_date4, null authorized_date5,
	null authorized_date6, null authorized_date7, null authorized_date8, null authorized_date9, null authorized_date10,
	null budget_version_id, decode(nvl(b.auto_approved,0),1,null,2) rejected,
	null notes, null actuals_month_number, null first_approver,
	null revision_description, null revision_long_description, a.budget_review_type_id,
	c.est_start_date, c.est_complete_date, c.est_in_service_date,
	null user_text1, null user_text2, null user_text3, null user_text4, null user_text5,
	null user_text6, null user_text7, null user_text8, null user_text9, null user_text10,
	null user_text11, null user_text12, null user_text13, null user_text14, null user_text15,
	null user_date1, null user_date2, null user_date3, null user_date4, null user_date5,
	null user_date6, null user_date7, null user_date8, null user_date9, null user_date10,
	null user_date11, null user_date12, null user_date13, null user_date14, null user_date15,
	decode(nvl(b.auto_approved,0),1,6,1) approval_status_id, 1 review_status,
	e.closing_pattern_id, e.base_year
from work_order_type a, wo_est_copy_revision_temp b, work_order_control c, approval_defaults d, work_order_account e
where c.work_order_id = b.work_order_id
and c.work_order_type_id = a.work_order_type_id
and c.work_order_type_id = d.work_order_type_id (+)
and c.work_order_id = e.work_order_id
and b.old_revision = 0;

if uf_check_sql('Creating revisions in work_order_approval') <> 1 then return -1

//
// Default in any Multiple approvers
//
insert into wo_approval_multiple 
(work_order_id, revision, approval_type_id, auth_level,
	users, category_id, required, requiring_user, auth_level_id)
select a.work_order_id, b.new_revision, a.approval_type_id, c.auth_level,
	c.users, c.category_id, c.required, user, d.auth_level_id
from work_order_approval a, wo_est_copy_revision_temp b, wo_approval_multiple_default c,
	(	select approval_type_id, 1 auth_level_id, auth_level1 auth_level from approval union all
		select approval_type_id, 2 auth_level_id, auth_level2 auth_level from approval union all
		select approval_type_id, 3 auth_level_id, auth_level3 auth_level from approval union all
		select approval_type_id, 4 auth_level_id, auth_level4 auth_level from approval union all
		select approval_type_id, 5 auth_level_id, auth_level5 auth_level from approval union all
		select approval_type_id, 6 auth_level_id, auth_level6 auth_level from approval union all
		select approval_type_id, 7 auth_level_id, auth_level7 auth_level from approval union all
		select approval_type_id, 8 auth_level_id, auth_level8 auth_level from approval union all
		select approval_type_id, 9 auth_level_id, auth_level9 auth_level from approval union all
		select approval_type_id, 10 auth_level_id, auth_level10 auth_level from approval
	) d
where a.work_order_id = b.work_order_id
and a.revision = b.new_revision
and a.approval_type_id = c.approval_type_id
and c.approval_type_id = d.approval_type_id
and lower(trim(c.auth_level)) = lower(trim(d.auth_level))
and b.old_revision = 0
and nvl(b.auto_approved,0) <> 1;

if uf_check_sql('Creating records in wo_approval_multiple') <> 1 then return -1

//
// For Auto-Approved Revisions update current_revision on work_order_control
//
update work_order_control a
set a.current_revision = (
	select b.new_revision
	from wo_est_copy_revision_temp b
	where b.work_order_id = a.work_order_id
	),
a.wo_status_id = decode(a.wo_status_id, 1, 2, a.wo_status_id)
where exists (
	select 1 from wo_est_copy_revision_temp b
	where b.work_order_id = a.work_order_Id
	and nvl(b.auto_approved,0) = 1
	);

if uf_check_sql('Updating current revision on work_order_control (auto approved)') <> 1 then return -1

//
// Copy budget review records
//
insert into budget_review (
	work_order_id, revision, review_date, status, level_id )
select a.work_order_id, b.new_revision, null, 2, a.level_id
from budget_review a, wo_est_copy_revision_temp b
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and b.old_revision <> 0;

if uf_check_sql('Copying records into budget_review') <> 1 then return -1

//
// Copy budget review detail records
//
insert into budget_review_detail (
	work_order_id, level_id, revision, status, users, require_flag, review_date )
select a.work_order_id, a.level_id, b.new_revision, 2, a.users, a.require_flag, null
	from budget_review_detail a, wo_est_copy_revision_temp b
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and b.old_revision <> 0;

if uf_check_sql('Copying records into budget_review_detail') <> 1 then return -1

//
// Translate old dollar est_monthly_ids to new ones
//
delete from wo_estimate_class_code_temp;

insert into wo_estimate_class_code_temp (estimate_id, new_estimate_id)
select est_monthly_id, pwrplant1.nextval
from (
	select distinct a.est_monthly_id
	from wo_est_monthly a, wo_est_copy_revision_temp b
	where a.work_order_id = b.work_order_id
	and a.revision = b.old_revision
	and b.old_revision <> 0
	);

if uf_check_sql('Inserting into wo_estimate_class_code_temp') <> 1 then return -1

//
// Copy monthly estimate records
//
insert into wo_est_monthly (
	est_monthly_id, work_order_id, revision, year,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june,
	july, august, september, october, november, december, total,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun,
	hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun,
	qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total,
	long_description, future_dollars, hist_actuals, substitution_id )
select c.new_estimate_id, a.work_order_id, b.new_revision, a.year,
	a.expenditure_type_id, a.est_chg_type_id, a.department_id, a.utility_account_id, a.wo_work_order_id, a.job_task_id, a.budget_id,
	a.attribute01_id, a.attribute02_id, a.attribute03_id, a.attribute04_id, a.attribute05_id, a.attribute06_id, a.attribute07_id, a.attribute08_id, a.attribute09_id, a.attribute10_id,
	a.january, a.february, a.march, a.april, a.may, a.june,
	a.july, a.august, a.september, a.october, a.november, a.december, a.total,
	a.hrs_jan, a.hrs_feb, a.hrs_mar, a.hrs_apr, a.hrs_may, a.hrs_jun,
	a.hrs_jul, a.hrs_aug, a.hrs_sep, a.hrs_oct, a.hrs_nov, a.hrs_dec, a.hrs_total,
	a.qty_jan, a.qty_feb, a.qty_mar, a.qty_apr, a.qty_may, a.qty_jun,
	a.qty_jul, a.qty_aug, a.qty_sep, a.qty_oct, a.qty_nov, a.qty_dec, a.qty_total,
	// ### CDM - Maint 8569 - include future_dollars in copy
	a.long_description, nvl(future_dollars,0) future_dollars, nvl(hist_actuals,0) hist_actuals, null substitution_id
from wo_est_monthly a, wo_est_copy_revision_temp b, wo_estimate_class_code_temp c
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and a.est_monthly_id = c.estimate_id
and b.old_revision <> 0;

if uf_check_sql('Copying estimates into wo_est_monthly') <> 1 then return -1

//
// Copy wo_est_monthly_spread records
//
insert into wo_est_monthly_spread (
	est_monthly_id, factor_id, curve_id, rate_type_id )
select b.new_estimate_id, a.factor_id, a.curve_id, a.rate_type_id
from wo_est_monthly_spread a, wo_estimate_class_code_temp b
where a.est_monthly_id = b.estimate_id;

if uf_check_sql('Copying records into wo_est_monthly_spread') <> 1 then return -1

//
// Copy supplemental estimate records
//
insert into wo_est_supplemental_data (
	est_monthly_id, year, supplemental_id, work_order_id, revision,
	supplemental_type_id, supplemental_data,
	january, february, march, april, may, june,
	july, august, september, october, november, december, total,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun,
	hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun,
	qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total )
select c.new_estimate_id, a.year, a.supplemental_id, a.work_order_id, b.new_revision,
	a.supplemental_type_id, a.supplemental_data,
	a.january, a.february, a.march, a.april, a.may, a.june,
	a.july, a.august, a.september, a.october, a.november, a.december, a.total,
	a.hrs_jan, a.hrs_feb, a.hrs_mar, a.hrs_apr, a.hrs_may, a.hrs_jun,
	a.hrs_jul, a.hrs_aug, a.hrs_sep, a.hrs_oct, a.hrs_nov, a.hrs_dec, a.hrs_total,
	a.qty_jan, a.qty_feb, a.qty_mar, a.qty_apr, a.qty_may, a.qty_jun,
	a.qty_jul, a.qty_aug, a.qty_sep, a.qty_oct, a.qty_nov, a.qty_dec, a.qty_total
from wo_est_supplemental_data a, wo_est_copy_revision_temp b, wo_estimate_class_code_temp c
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and a.est_monthly_id = c.estimate_id
and b.old_revision <> 0;

if uf_check_sql('Copying supplemental records into wo_est_supplemental_data') <> 1 then return -1

//
// Copy monthly escalations
//
insert into wo_est_monthly_escalation (
	est_monthly_id, year,
	january, february, march, april, may, june,
	july, august, september, october, november, december, total,
	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate,
	jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate,
	base_year, rate_type_id )
select b.new_estimate_id, a.year,
	a.january, a.february, a.march, a.april, a.may, a.june,
	a.july, a.august, a.september, a.october, a.november, a.december, a.total,
	a.jan_rate, a.feb_rate, a.mar_rate, a.apr_rate, a.may_rate, a.jun_rate,
	a.jul_rate, a.aug_rate, a.sep_rate, a.oct_rate, a.nov_rate, a.dec_rate,
	a.base_year, a.rate_type_id
from wo_est_monthly_escalation a, wo_estimate_class_code_temp b
where a.est_monthly_id = b.estimate_id;

if uf_check_sql('Copying escalation data into wo_est_monthly_escalation') <> 1 then return -1

//
// Translate old unit estimate_ids to new ones
//
delete from wo_estimate_class_code_temp;

insert into wo_estimate_class_code_temp (estimate_id, new_estimate_id)
select estimate_id, pwrplant1.nextval
from (
	select distinct a.estimate_id
	from wo_estimate a, wo_est_copy_revision_temp b
	where a.work_order_id = b.work_order_id
	and a.revision = b.old_revision
	and b.old_revision <> 0
	);

if uf_check_sql('Inserting into wo_estimate_class_code_temp') <> 1 then return -1

//
// Copy unit estimates
//
//###SRM - 11394 - new estimates in wo_estimate table set to have batch_unit_item_id = null
insert into wo_estimate (
	estimate_id, revision, work_order_id,
	expenditure_type_id, est_chg_type_id, job_task_id, utility_account_id, department_id,
	sub_account_id, stck_keep_unit_id, retirement_unit_id, bus_segment_id, asset_id,
	quantity, hours, amount, notes,
	batch_unit_item_id, property_group_id, asset_location_id, replacement_amount,
	serial_number, wo_est_trans_type_id, est_in_service_date, unit_desc,
	unit_long_desc, wo_est_unit, est_monthly_id, percent,
	field_1, field_2, field_3, field_4, field_5, retire_vintage, ocr_api )
select c.new_estimate_id, b.new_revision, a.work_order_id,
	a.expenditure_type_id, a.est_chg_type_id, a.job_task_id, a.utility_account_id, a.department_id,
	a.sub_account_id, a.stck_keep_unit_id, a.retirement_unit_id, a.bus_segment_id, a.asset_id,
	a.quantity, a.hours, a.amount, a.notes,
	null, a.property_group_id, a.asset_location_id, a.replacement_amount,
	a.serial_number, a.wo_est_trans_type_id, a.est_in_service_date, a.unit_desc,
	a.unit_long_desc, a.wo_est_unit, null est_monthly_id, a.percent,
	a.field_1, a.field_2, a.field_3, a.field_4, a.field_5, a.retire_vintage, a.ocr_api
from wo_estimate a, wo_est_copy_revision_temp b, wo_estimate_class_code_temp c
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and a.estimate_id = c.estimate_id
and b.old_revision <> 0;

if uf_check_sql('Copying estimates into wo_estimate') <> 1 then return -1

//
// Copy unit estimates class codes
//
insert into wo_estimate_class_code(estimate_id, class_code_id, value)
select c.new_estimate_id, a.class_code_id, a.value
from wo_estimate_class_code a, wo_estimate_class_code_temp c
where c.estimate_id = a.estimate_id;
	
if uf_check_sql('Copying estimate class codes into wo_estimate_class_code') <> 1 then return -1

//
// Copy afudc calculation results
//
insert into budget_afudc_calc (
	work_order_id, revision, expenditure_type_id, month_number,
	amount, amount_afudc, amount_cpi, in_service_date, afudc_type_id,
	closing_pattern_id, closing_option_id, eligible_for_afudc, eligible_for_cpi,
	afudc_stop_date, afudc_start_date, afudc_debt_rate, afudc_equity_rate,
	cpi_rate, afudc_compound_ind, cpi_compound_ind, closings_pct,
	funding_wo_indicator, beg_cwip, beg_cwip_afudc, beg_cwip_cpi,
	current_month_charges_afudc, current_month_charges_cpi, current_month_closings,
	current_month_closings_afudc, current_month_closings_cpi,
	uncompounded_afudc, uncompounded_cpi, compounded_afudc, compounded_cpi,
	afudc_base, cpi_base, afudc_debt, afudc_equity, cpi,
	end_cwip, end_cwip_afudc, end_cwip_cpi,
	est_monthly_id_debt, est_monthly_id_equity, est_monthly_id_cpi,
	est_monthly_id_cwipbal_cwip, est_monthly_id_cwipbal_tax, est_monthly_id_cwipbal_removal,
	est_monthly_id_cwipbal_salvage, est_monthly_id_closings_cwip, est_monthly_id_closings_tax,
	est_monthly_id_closings_rmvl, est_monthly_id_closings_slvg,
	debt_est_chg_type_id, equity_est_chg_type_id, cpi_est_chg_type_id, wo_work_order_id )
select a.work_order_id, b.new_revision, a.expenditure_type_id, a.month_number,
	a.amount, a.amount_afudc, a.amount_cpi, a.in_service_date, a.afudc_type_id,
	a.closing_pattern_id, a.closing_option_id, a.eligible_for_afudc, a.eligible_for_cpi,
	a.afudc_stop_date, a.afudc_start_date, a.afudc_debt_rate, a.afudc_equity_rate,
	a.cpi_rate, a.afudc_compound_ind, a.cpi_compound_ind, a.closings_pct,
	a.funding_wo_indicator, a.beg_cwip, a.beg_cwip_afudc, a.beg_cwip_cpi,
	a.current_month_charges_afudc, a.current_month_charges_cpi, a.current_month_closings,
	a.current_month_closings_afudc, a.current_month_closings_cpi,
	a.uncompounded_afudc, a.uncompounded_cpi, a.compounded_afudc, a.compounded_cpi,
	a.afudc_base, a.cpi_base, a.afudc_debt, a.afudc_equity, a.cpi,
	a.end_cwip, a.end_cwip_afudc, a.end_cwip_cpi,
	null est_monthly_id_debt, null est_monthly_id_equity, null est_monthly_id_cpi,
	null est_monthly_id_cwipbal_cwip, null est_monthly_id_cwipbal_tax, null est_monthly_id_cwipbal_removal,
	null est_monthly_id_cwipbal_salvage, null est_monthly_id_closings_cwip, null est_monthly_id_closings_tax,
	null est_monthly_id_closings_rmvl, null est_monthly_id_closings_slvg,
	a.debt_est_chg_type_id, a.equity_est_chg_type_id, a.cpi_est_chg_type_id, a.wo_work_order_id
from budget_afudc_calc a, wo_est_copy_revision_temp b
where a.work_order_id = b.work_order_id
and a.revision = b.old_revision
and b.old_revision <> 0;

if uf_check_sql('Copying afudc results into budget_afudc_calc') <> 1 then return -1

if a_wo_fp = 'fp' then
	//
	// For funding projects - associate to budget version(s)
	//
	if upperbound(i_new_bv_ids) > 0 then
		//
		// Use designated budget version id's if they exist
		//
		bv_ids_str = f_parsenumarrayintostring(i_new_bv_ids)
		
		//
		// If the user was prompted and intentionally didn't select a budget version, this will be "0"
		//
		if bv_ids_str <> "0" then
			//
			// Mark old revisions as inactive for designated budget version(s)
			//
			sqls = &
				"update budget_version_fund_proj a " +&
				"set a.active = 0 " +&
				"where a.budget_version_id in ("+bv_ids_str+") " +&
				"and exists ( " +&
				"	select 1 from wo_est_copy_revision_temp b " +&
				"	where b.work_order_id = a.work_order_id " +&
				"	) "
			execute immediate :sqls;
			
			if uf_check_sql('Updating old revisions in budget version(s) to inactive (FP)') <> 1 then return -1
			
			//
			// Insert new revisions as active for designated budget version(s)
			//
			sqls = &
				"insert into budget_version_fund_proj ( " +&
				"	work_order_id, revision, budget_version_id, active, " +&
				"	person_added, date_active, date_added ) " +&
				"select a.work_order_id, a.new_revision, b.budget_version_id, 1 active, " +&
				"	user, sysdate, sysdate " +&
				"from wo_est_copy_revision_temp a, budget_version b " +&
				"where b.budget_version_id in ("+bv_ids_str+") "
			execute immediate :sqls;
			
			if uf_check_sql('Associating new revisions to budget version(s) as active (FP)') <> 1 then return -1
		end if
		
	else
		//
		// Default budget version_id's if none are designated in budget version id array
		//   Only for new revisions...does not apply to revisions being copied
		//
		
		//
		// ### CDM - Maint 6500
		// System control to only default the budget version based on the current version flag (i.e. ignore the budget version assigned to the budget item)
		//
		update wo_est_copy_revision_temp z
		set z.dflt_curr_ver = 1
		where exists (
			select 1 from work_order_control a, pp_system_control_companies p
			where z.work_order_id = a.work_order_id
			and a.company_id = p.company_id
			and lower(trim(p.control_name)) = lower(trim('FP Est - Dflt Curr Version Only'))
			and lower(trim(p.control_value)) = 'yes'
			)
		and z.old_revision = 0;
		
		if uf_check_sql('Updating dflt_curr_ver on wo_est_copy_revision_temp based on system control "FP Est - Dflt Curr Version Only"') <> 1 then return -1
		
		//
		// First try to default budget_version_id from the budget_id...only if we are not exclusively defaulting based on the current version
		//
		update wo_est_copy_revision_temp z
		set z.new_budget_version_id = (
			select min(b.budget_version_id)
			from work_order_control a, budget_amounts b, budget_version c
			where a.budget_id = b.budget_id
			and a.work_order_id = z.work_order_id
			and b.budget_version_id = c.budget_version_id
			and c.open_for_entry = 1
			)
		where z.old_revision = 0
		and nvl(z.dflt_curr_ver,0) <> 1;
		
		if uf_check_sql('Updating new_budget_version_id on wo_est_copy_revision_temp based on budget_id') <> 1 then return -1
		
		//
		// Then try to default budget_version_id from the current_version
		//
		//select lower(trim(control_value)) into :cv_by_co
		//from pp_system_control_company
		//where company_id = -1
		//and upper(trim(control_name)) = upper(trim('BV Current Version by Company'));
		
		//if cv_by_co = 'yes' then
			update wo_est_copy_revision_temp z
			set z.new_budget_version_id = (
				select a.budget_version_id
				from budget_version a, work_order_control b
				where a.current_version = 1
				and a.open_for_entry = 1
				and a.company_id = b.company_id
				and b.work_order_id = z.work_order_id
				)
			where z.old_revision = 0
			and z.new_budget_version_id is null
			and exists (
				select 1 from work_order_control woc, pp_system_control_companies pp
				where woc.work_order_id = z.work_order_id
				and woc.company_id = pp.company_id
				and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
				and upper(trim(pp.control_value)) = upper(trim('yes'))
				);
		// ### CDM - Maint 6218 - current_version by company_summary
		//elseif cv_by_co = 'summary' then
			update wo_est_copy_revision_temp z
			set z.new_budget_version_id = (
				select a.budget_version_id
				from budget_version a, company b, company c, work_order_control d
				where a.current_version = 1
				and a.open_for_entry = 1
				and a.company_id = b.company_id
				and b.company_summary_id = c.company_summary_id
				and c.company_id = d.company_id
				and d.work_order_id = z.work_order_id
				)
			where z.old_revision = 0
			and z.new_budget_version_id is null
			and exists (
				select 1 from work_order_control woc, pp_system_control_companies pp
				where woc.work_order_id = z.work_order_id
				and woc.company_id = pp.company_id
				and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
				and upper(trim(pp.control_value)) = upper(trim('summary'))
				);
		//else
			update wo_est_copy_revision_temp z
			set z.new_budget_version_id = (
				select budget_version_id
				from budget_version a
				where current_version = 1
				and open_for_entry = 1
				)
			where z.old_revision = 0
			and z.new_budget_version_id is null
			and not exists (
				select 1 from work_order_control woc, pp_system_control_companies pp
				where woc.work_order_id = z.work_order_id
				and woc.company_id = pp.company_id
				and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
				and upper(trim(pp.control_value)) in (upper(trim('yes')), upper(trim('summary')))
				);
		//end if
		
		if uf_check_sql('Updating new_budget_version_id on wo_est_copy_revision_temp based on current_version') <> 1 then return -1
		
		//
		// Mark old revisions as inactive for designated budget version(s)
		//
		update budget_version_fund_proj z
		set z.active = 0
		where exists (
			select 1 from wo_est_copy_revision_temp a
			where a.work_order_id = z.work_order_id
			and a.new_budget_version_id = z.budget_version_id
			and a.old_revision = 0
			)
		and z.active = 1;
		
		if uf_check_sql('Updating old revisions in budget version(s) to inactive (FP)') <> 1 then return -1
		
		//
		// Insert new revisions as active for designated budget version(s)
		//
		insert into budget_version_fund_proj (
			work_order_id, revision, budget_version_id, active,
			person_added, date_active, date_added )
		select a.work_order_id, a.new_revision, a.new_budget_version_id, 1 active,
			user, sysdate, sysdate
		from wo_est_copy_revision_temp a
		where a.work_order_id is not null
		and a.new_budget_version_id is not null
		and a.old_revision = 0;
		
		if uf_check_sql('Associating new revisions to budget version(s) as active (FP)') <> 1 then return -1
	end if
else
	//
	// For work orders - mark previous revisions as inactive
	//
	update work_order_approval a
	set rejected = 1,
		notes = substr('REJECTED DUE TO NEW REVISION - ' || notes,1,2000),
		approval_status_id = 5
	where exists (
		select 1 from wo_est_copy_revision_temp b, work_order_control c
		where b.work_order_id = c.work_order_id
		and c.funding_wo_indicator = 0
		and b.work_order_id = a.work_order_id
		and b.new_revision > a.revision
		)
	//and nvl(a.rejected,0) <> 1
	// ### CDM - Maint 6177 - only mark revisions as inactive that are not pending approval or approved.
	and approval_status_id not in (2,3);
	
	if uf_check_sql('Updating old revisions to inactive (WO)') <> 1 then return -1
	
	// ### CDM - Maint 6177 - only mark revisions as inactive that are not pending approval or approved.
	update workflow w
	set approval_status_id = 5
	where exists (
		select 1 from wo_est_copy_revision_temp b, work_order_control c, work_order_approval woa
		where b.work_order_id = c.work_order_id
		and c.funding_wo_indicator = 0
		and b.work_order_id = woa.work_order_id
		and woa.approval_status_id = 5
		and to_char(woa.work_order_id) = w.id_field1
		and to_char(woa.revision) = w.id_field2
		)
	and w.subsystem = 'wo_approval'
	and w.approval_status_id <> 5;
	
	if uf_check_sql('Updating old workflows to inactive (WO)') <> 1 then return -1
	
end if

//
// Clear out workflows if they exist for some reason
//
delete from workflow_detail
where workflow_id in (
	select workflow_id
	from workflow w
	where exists (
		select 1 from wo_est_copy_revision_temp a
		where to_char(a.work_order_id) = w.id_field1
		and to_char(a.new_revision) = w.id_field2
		)
	and w.subsystem in ('fp_approval','wo_approval','budg_review') // ### CDM - Maint 5728 - add subsystem to sql
	);

if uf_check_sql('Removing stale workflow details') <> 1 then return -1

delete from workflow w
where exists (
	select 1 from wo_est_copy_revision_temp a
	where to_char(a.work_order_id) = w.id_field1
	and to_char(a.new_revision) = w.id_field2
	)
and w.subsystem in ('fp_approval','wo_approval','budg_review'); // ### CDM - Maint 5728 - add subsystem to sql

if uf_check_sql('Removing stale workflows') <> 1 then return -1

//
// Copy approval workflows
//
insert into workflow (
	workflow_id,
	workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
	approval_amount, approval_status_id, use_limits,
	sql_approval_amount )
select
	pwrplant1.nextval workflow_id, //(select nvl(max(workflow_id),0) from workflow) + rownum workflow_id, // ### Maint - 5728 - add nvl() around max()
	workflow_type_id, workflow_type_desc, subsystem,
	id_field1, to_char(b.new_revision) id_field2,
	approval_amount, decode(nvl(b.auto_approved,0),1,6,1) approval_status_id, use_limits,
	sql_approval_amount
from workflow a, wo_est_copy_revision_temp b
where a.id_field1 = to_char(b.work_order_id)
and a.id_field2 = to_char(b.old_revision)
and a.subsystem in ('fp_approval','wo_approval')
and b.old_revision <> 0;

if uf_check_sql('Copying approval workflows') <> 1 then return -1

//
// Copy approval workflow details
//
// ### CDM - Maint 8481 - add allow_edit, orig_authority_limit, is_custom_limit to copy
insert into workflow_detail (
	workflow_id,
	id, workflow_rule_id, workflow_rule_desc, rule_order,
	users, date_approved, user_approved, approval_status_id,
	required, notes, authority_limit, num_approvers, num_required,
	sql, allow_edit, orig_authority_limit, is_custom_limit )
select 
	// ### CDM - Maint 8471 - need to specify subsystem
	(select workflow_id from workflow where id_field1 = to_char(b.work_order_id) and id_field2 = to_char(b.new_revision) and subsystem in ('fp_approval','wo_approval')) workflow_id,
	wd.id, wd.workflow_rule_id, wd.workflow_rule_desc, wd.rule_order,
	wd.users, null date_approved, null user_approved, 1 approval_status_id,
	wd.required, null notes, wd.authority_limit, wd.num_approvers, wd.num_required,
	wd.sql, wd.allow_edit, wd.orig_authority_limit, wd.is_custom_limit
from workflow w, workflow_detail wd, wo_est_copy_revision_temp b
where to_char(b.work_order_id) = w.id_field1
and to_char(b.old_revision) = w.id_field2
and w.workflow_id = wd.workflow_id
and w.subsystem in ('fp_approval','wo_approval')
and b.old_revision <> 0;

if uf_check_sql('Copying approval workflow details') <> 1 then return -1

//
// Insert new approval workflows
//
insert into workflow (
	workflow_id,
	workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
	approval_amount, approval_status_id, use_limits,
	sql_approval_amount )
select
	pwrplant1.nextval workflow_id, //(select nvl(max(workflow_id),0) from workflow) + rownum workflow_id, // ### Maint - 5728 - add nvl() around max()
	c.approval_type_id workflow_type_id, d.description workflow_type_desc, /*d.subsystem,*/ decode(:a_wo_fp,'fp','fp_approval','wo_approval') subsystem, // ### CDM - Maint 5728 - add subsystem to sql
	to_char(b.work_order_id) id_field1, to_char(b.new_revision) id_field2,
	0 approval_amount, decode(nvl(b.auto_approved,0),1,6,1) approval_status_id, d.use_limits,
	d.sql_approval_amount
from work_order_control a, wo_est_copy_revision_temp b, work_order_type c, workflow_type d
where a.work_order_id = b.work_order_id
and a.work_order_type_id = c.work_order_type_id
and c.approval_type_id = d.workflow_type_id
and b.old_revision = 0;

if uf_check_sql('Inserting approval workflows') <> 1 then return -1

//
// Insert new approval workflow details
//
/*
No need to insert details for new workflows...they will be populated / defaulted when the individual workflows are redrawn
*/

if a_wo_fp = 'fp' then
	//
	// Copy review workflows
	//
	insert into workflow (
		workflow_id,
		workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
		approval_amount, approval_status_id, use_limits,
		sql_approval_amount )
	select
		pwrplant1.nextval workflow_id,
		workflow_type_id, workflow_type_desc, subsystem,
		id_field1, to_char(b.new_revision) id_field2,
		approval_amount, 1 approval_status_id, use_limits,
		sql_approval_amount
	from workflow a, wo_est_copy_revision_temp b
	where a.id_field1 = to_char(b.work_order_id)
	and a.id_field2 = to_char(b.old_revision)
	and a.subsystem in ('budg_review')
	and b.old_revision <> 0;
	
	if uf_check_sql('Copying review workflows') <> 1 then return -1
	
	//
	// Copy review workflow details
	//
	// ### CDM - Maint 8481 - add allow_edit, orig_authority_limit, is_custom_limit to copy
	insert into workflow_detail (
		workflow_id,
		id, workflow_rule_id, workflow_rule_desc, rule_order,
		users, date_approved, user_approved, approval_status_id,
		required, notes, authority_limit, num_approvers, num_required,
		sql, allow_edit, orig_authority_limit, is_custom_limit )
	select 
		// ### CDM - Maint 8471 - need to specify subsystem
		(select workflow_id from workflow where id_field1 = to_char(b.work_order_id) and id_field2 = to_char(b.new_revision) and subsystem in ('budg_review')) workflow_id,
		wd.id, wd.workflow_rule_id, wd.workflow_rule_desc, wd.rule_order,
		wd.users, null date_approved, null user_approved, 1 approval_status_id,
		wd.required, null notes, wd.authority_limit, wd.num_approvers, wd.num_required,
		wd.sql, wd.allow_edit, wd.orig_authority_limit, wd.is_custom_limit
	from workflow w, workflow_detail wd, wo_est_copy_revision_temp b
	where to_char(b.work_order_id) = w.id_field1
	and to_char(b.old_revision) = w.id_field2
	and w.workflow_id = wd.workflow_id
	and w.subsystem in ('budg_review')
	and b.old_revision <> 0;
	
	if uf_check_sql('Copying review workflow details') <> 1 then return -1
	
	//
	// Insert new review workflows
	//
	insert into workflow (
		workflow_id,
		workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
		approval_amount, approval_status_id, use_limits,
		sql_approval_amount )
	select
		pwrplant1.nextval workflow_id,
		c.approval_type_id workflow_type_id, d.description workflow_type_desc, 'budg_review' subsystem,
		to_char(b.work_order_id) id_field1, to_char(b.new_revision) id_field2,
		0 approval_amount, 1 approval_status_id, d.use_limits,
		d.sql_approval_amount
	from work_order_control a, wo_est_copy_revision_temp b, work_order_type c, workflow_type d
	where a.work_order_id = b.work_order_id
	and a.work_order_type_id = c.work_order_type_id
	and c.approval_type_id = d.workflow_type_id
	and b.old_revision = 0;
	
	if uf_check_sql('Inserting review workflows') <> 1 then return -1
	
	//
	// Insert new review workflow details
	//
	/*
	No need to insert details for new workflows...they will be populated / defaulted when the individual workflows are redrawn
	*/
end if

//
// Copy monthly estimate derivation percentages
//
insert into wo_est_derivation_pct (work_order_id, revision, expenditure_type_id, cr_derivation_rollup, pct)
select pct.work_order_id, temp.new_revision, pct.expenditure_type_id, pct.cr_derivation_rollup, pct.pct
from wo_est_derivation_pct pct, wo_est_copy_revision_temp temp
where temp.work_order_id = pct.work_order_id
and temp.old_revision = pct.revision
and temp.old_revision <> 0;

if uf_check_sql('Copying monthly estimate derivation percentages') <> 1 then return -1

//
// Insert new justifications
//
ds_new_just = create uo_ds_top
if a_wo_fp = 'fp' then
	s_just_field = "fp_new_rev_stage"
else
	s_just_field = "wo_new_rev_stage"
end if
sqls = &
	"select tab_indicator, "+s_just_field+" new_stage " +&
	"from wo_doc_justification_tabs " +&
	"where "+s_just_field+" is not null "
ret = f_create_dynamic_ds(ds_new_just, 'grid', sqls, sqlca, true)
if ret <> 'OK' then
	uf_msg("Error creating datastore:~r~n"+sqls,'E')
end if

if ds_new_just.rowcount() > 0 then
	if i_called_from_send_for_appr then
		called_from_send_for_appr = 1
	else
		called_from_send_for_appr = 0
	end if
	
	// Delete and Re-insert into wo_est_processing_temp according to the system controls below
	delete from wo_est_processing_temp;
	
	insert into wo_est_processing_temp (work_order_id, revision)
	select work_order_id, 0
	from wo_est_copy_revision_temp temp
	where exists (
		select 1 from work_order_control woc, pp_system_control_companies pp
		where woc.work_order_id = temp.work_order_id
		and woc.company_id = pp.company_id
		and lower(trim(pp.control_name)) = lower(trim(decode(:a_wo_fp,'fp','Funding Just-New Doc on New Rev-FP','Funding Just-New Doc on New Rev-WO')))
		and (
			(lower(trim(pp.control_value)) = 'always')
			OR
			(lower(trim(pp.control_value)) = 'on send' and :called_from_send_for_appr = 1)
			)
		);
	
	if uf_check_sql('Re-inserting into wo_est_processing_temp') <> 1 then return -1
	
	// Create new justification documents for records in wo_est_processing_temp
	for i = 1 to ds_new_just.rowcount()
		setnull(tab_ind)
		setnull(stage_id)
		tab_ind = ds_new_just.getitemnumber(i,"tab_indicator")
		stage_id = ds_new_just.getitemnumber(i,"new_stage")
		
		if tab_ind > 0 and stage_id > 0 then
			uf_justification_new(tab_ind, stage_id, 1 /*same for wos and fps*/)
		end if
	next
	
	// Restore wo_est_processing_temp
	delete from wo_est_processing_temp;
	
	insert into wo_est_processing_temp (work_order_id, revision)
	select work_order_id, old_revision
	from wo_est_copy_revision_temp;
	
	if uf_check_sql('Restoring records in wo_est_processing_temp') <> 1 then return -1
	
end if
destroy ds_new_just

//
// Call to custom function at end of processing
//
cst_rtn = f_budget_revisions_custom(func_name,3,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
long_arr[1] = 0
str_arr[1] = ' '
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,3,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '3'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
cst_rtn3 = f_wo_validation_control(85,args)
if cst_rtn3 < 0 then 
   return -1
end if

return 1

end function

public function longlong uf_date_change (string a_wo_fp, datetime a_new_start, datetime a_new_end, datetime a_new_est_in_serv);longlong month_no, month_no_end, year_no, year_no_end, &
	count_of_bad, rtn, long_arr[], cst_rtn, cst_rtn2, cst_rtn3
boolean bad_estimates = false
string func_name, str_arr[], args[]

func_name = 'uf_date_change'
str_arr[1] = string(a_new_start)
str_arr[2] = string(a_new_end)
str_arr[3] = string(a_new_est_in_serv)

month_no = 		month(date(a_new_start))
month_no_end = month(date(a_new_end))
year_no = 		year(date(a_new_start))
year_no_end =  year(date(a_new_end))

//
// Custom function call to check to stop processing if needed
//
cst_rtn = f_budget_revisions_custom(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
long_arr[1] = 0
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '1'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
args[4] = string(str_arr[1])
args[5] = string(str_arr[2])
args[6] = string(str_arr[3])
cst_rtn3 = f_wo_validation_control(86,args)
if cst_rtn3 < 0 then 
   return -1
end if

//
// Check for existence of estimates outside of new estimated range
//
select count(*) into :count_of_bad
from wo_est_monthly wem, wo_est_processing_temp t
where wem.work_order_id = t.work_order_id
and wem.revision = t.revision
and (
	(wem.year < :year_no and abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
										abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
										abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)) <> 0)
	or
	(wem.year > :year_no_end and abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
												abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
												abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)) <> 0)
	or
	(wem.year = :year_no and
		decode(:month_no,
			12,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0)),
			11,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0)),
			10,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0)),
			9,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0)),
			8,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0)),
			7,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0)),
			6,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0)),
			5,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0)),
			4,abs(nvl(january,0))+abs(nvl(february,0))+abs(nvl(march,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0)),
			3,abs(nvl(january,0))+abs(nvl(february,0))+
				abs(nvl(hrs_jan,0))+abs(nvl(hrs_feb,0))+
				abs(nvl(qty_jan,0))+abs(nvl(qty_feb,0)),
			2,abs(nvl(january,0))+
				abs(nvl(hrs_jan,0))+
				abs(nvl(qty_jan,0))
			) <> 0)
	or
	(wem.year = :year_no_end and
		decode(:month_no_end,
			1,abs(nvl(february,0))+abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_feb,0))+abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_feb,0))+abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			2,abs(nvl(march,0))+abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_mar,0))+abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_mar,0))+abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			3,abs(nvl(april,0))+abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_apr,0))+abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_apr,0))+abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			4,abs(nvl(may,0))+abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_may,0))+abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_may,0))+abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			5,abs(nvl(june,0))+abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_jun,0))+abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_jun,0))+abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			6,abs(nvl(july,0))+abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_jul,0))+abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_jul,0))+abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			7,abs(nvl(august,0))+abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_aug,0))+abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_aug,0))+abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			8,abs(nvl(september,0))+abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_sep,0))+abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_sep,0))+abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			9,abs(nvl(october,0))+abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_oct,0))+abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_oct,0))+abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			10,abs(nvl(november,0))+abs(nvl(december,0))+
				abs(nvl(hrs_nov,0))+abs(nvl(hrs_dec,0))+
				abs(nvl(qty_nov,0))+abs(nvl(qty_dec,0)),
			11,abs(nvl(december,0))+
				abs(nvl(hrs_dec,0))+
				abs(nvl(qty_dec,0))
			) <> 0)
	);
										
if count_of_bad > 0 then bad_estimates = true

//
// Insert any missing est_monthly_id/year combinations within the new estimated range
//
insert into wo_est_monthly (
	work_order_id, est_monthly_id, revision, year, expenditure_type_id, est_chg_type_id, 
	department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january,february, march, april, may, june, july, august, september, october, november, december, total,
	hist_actuals, future_dollars,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total
)
select a.work_order_id, est_monthly_id, a.revision, b.year, min(expenditure_type_id), min(est_chg_type_id),
	min(department_id), min(utility_account_id), min(wo_work_order_id), min(job_task_id), min(long_description), min(budget_id),
	min(attribute01_id), min(attribute02_id), min(attribute03_id), min(attribute04_id), min(attribute05_id), min(attribute06_id), min(attribute07_id), min(attribute08_id), min(attribute09_id), min(attribute10_id),
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
from wo_est_monthly a, wo_est_processing_temp t, pp_table_years b
where a.work_order_id = t.work_order_id
and a.revision = t.revision
and b.year between :year_no and :year_no_end
and not exists (
	select 1 from wo_est_monthly c 
	where a.work_order_id = c.work_order_id
	and a.revision = c.revision
	and a.est_monthly_id = c.est_monthly_id
	and b.year = c.year
	)
group by a.work_order_id, est_monthly_id, a.revision, b.year;

if sqlca.sqlcode < 0 then
	MessageBox('ERROR','Could not insert missing years within new range. ' +sqlca.sqlerrtext)
	rollback;
	return -1
end if

if bad_estimates and i_messagebox_onerror then
	rtn = MessageBox('Warning', &
		'There are estimates outside the start and completion dates with non-zero amounts.  Do you want to delete these amounts?', &
		StopSign!,YesNo!)
	if rtn = 2 then 
		MessageBox('Date Change','Cannot change dates.  Please ensure all years/months not in the new date range have zero amounts.')
		return -1
	end if
end if

//
// Delete any years outside of estimated range
//
delete from wo_est_monthly wem
where (year < :year_no or year > :year_no_end)
and (work_order_id, revision) in (
	select work_order_id, revision
	from wo_est_processing_temp t
	);

if sqlca.sqlcode < 0 then
	MessageBox('ERROR','Could not delete years outside new range. ' +sqlca.sqlerrtext)
	rollback;
	return -1
end if	

//
// Zero out months prior to start month
//
update wo_est_monthly wem
set january = decode(sign(1 - :month_no),-1,0,january),			hrs_jan = decode(sign(1 - :month_no),-1,0,hrs_jan),		qty_jan = decode(sign(1 - :month_no),-1,0,qty_jan),
	february = decode(sign(2 - :month_no),-2,0,february),			hrs_feb = decode(sign(2 - :month_no),-1,0,hrs_feb),		qty_feb = decode(sign(2 - :month_no),-1,0,qty_feb),
	march = decode(sign(3 - :month_no),-1,0,march),				hrs_mar = decode(sign(3 - :month_no),-1,0,hrs_mar),	qty_mar = decode(sign(3 - :month_no),-1,0,qty_mar),
	april = decode(sign(4 - :month_no),-1,0,april),						hrs_apr = decode(sign(4 - :month_no),-1,0,hrs_apr),	qty_apr = decode(sign(4 - :month_no),-1,0,qty_apr),
	may = decode(sign(5 - :month_no),-1,0,may),						hrs_may = decode(sign(5 - :month_no),-1,0,hrs_may),	qty_may = decode(sign(5 - :month_no),-1,0,qty_may),
	june = decode(sign(6 - :month_no),-1,0,june),						hrs_jun = decode(sign(6 - :month_no),-1,0,hrs_jun),		qty_jun = decode(sign(6 - :month_no),-1,0,qty_jun),
	july = decode(sign(7 - :month_no),-1,0,july),						hrs_jul = decode(sign(7 - :month_no),-1,0,hrs_jul),		qty_jul = decode(sign(7 - :month_no),-1,0,qty_jul),
	august = decode(sign(8 - :month_no),-1,0,august),				hrs_aug = decode(sign(8 - :month_no),-1,0,hrs_aug),	qty_aug = decode(sign(8 - :month_no),-1,0,qty_aug),
	september = decode(sign(9 - :month_no),-1,0,september),		hrs_sep = decode(sign(9 - :month_no),-1,0,hrs_sep),	qty_sep = decode(sign(9 - :month_no),-1,0,qty_sep),
	october = decode(sign(10 - :month_no),-1,0,october),			hrs_oct = decode(sign(10 - :month_no),-1,0,hrs_oct),	qty_oct = decode(sign(10 - :month_no),-1,0,qty_oct),
	november = decode(sign(11 - :month_no),-1,0,november),		hrs_nov = decode(sign(11 - :month_no),-1,0,hrs_nov),	qty_nov = decode(sign(11 - :month_no),-1,0,qty_nov)
where year = :year_no
and (work_order_id, revision) in (
	select work_order_id, revision
	from wo_est_processing_temp
	);

if sqlca.sqlcode < 0 then
	MessageBox('ERROR','Could not update months prior to new start month to zero. ' +sqlca.sqlerrtext)
	rollback;
	return -1
end if	

//
// Zero out months after end month
//
update wo_est_monthly wem
set february = decode(sign(:month_no_end - 2),-1,0,february),		hrs_feb = decode(sign(:month_no_end - 2),-1,0,hrs_feb),		qty_feb = decode(sign(:month_no_end - 2),-1,0,qty_feb),
	march = decode(sign(:month_no_end - 3),-1,0,march),				hrs_mar = decode(sign(:month_no_end - 3),-1,0,hrs_mar),	qty_mar = decode(sign(:month_no_end - 3),-1,0,qty_mar),
	april = decode(sign(:month_no_end - 4),-1,0,april),					hrs_apr = decode(sign(:month_no_end - 4),-1,0,hrs_apr),		qty_apr = decode(sign(:month_no_end - 4),-1,0,qty_apr),
	may = decode(sign(:month_no_end - 5),-1,0,may),					hrs_may = decode(sign(:month_no_end - 5),-1,0,hrs_may),	qty_may = decode(sign(:month_no_end - 5),-1,0,qty_may),
	june = decode(sign(:month_no_end - 6),-1,0,june),					hrs_jun = decode(sign(:month_no_end - 6),-1,0,hrs_jun),		qty_jun = decode(sign(:month_no_end - 6),-1,0,qty_jun),
	july = decode(sign(:month_no_end - 7),-1,0,july),						hrs_jul = decode(sign(:month_no_end - 7),-1,0,hrs_jul),		qty_jul = decode(sign(:month_no_end - 7),-1,0,qty_jul),
	august = decode(sign(:month_no_end - 8),-1,0,august),				hrs_aug = decode(sign(:month_no_end - 8),-1,0,hrs_aug),		qty_aug = decode(sign(:month_no_end - 8),-1,0,qty_aug),
	september = decode(sign(:month_no_end - 9),-1,0,september),	hrs_sep = decode(sign(:month_no_end - 9),-1,0,hrs_sep),		qty_sep = decode(sign(:month_no_end - 9),-1,0,qty_sep),
	october = decode(sign(:month_no_end - 10),-1,0,october),			hrs_oct = decode(sign(:month_no_end - 10),-1,0,hrs_oct),		qty_oct = decode(sign(:month_no_end - 10),-1,0,qty_oct),
	november = decode(sign(:month_no_end - 11),-1,0,november),	hrs_nov = decode(sign(:month_no_end - 11),-1,0,hrs_nov),	qty_nov = decode(sign(:month_no_end - 11),-1,0,qty_nov),
	december = decode(sign(:month_no_end - 12),-1,0,december),	hrs_dec = decode(sign(:month_no_end - 12),-1,0,hrs_dec),	qty_dec = decode(sign(:month_no_end - 12),-1,0,qty_dec)
where year = :year_no_end
and (work_order_id, revision) in (
	select work_order_id, revision
	from wo_est_processing_temp
	);

if sqlca.sqlcode < 0 then
	MessageBox('ERROR','Could not update months after new end month to zero. ' +sqlca.sqlerrtext)
	rollback;
	return -1
end if	

//
// Update totals and null values
//
update wo_est_monthly
set total = nvl(january,0) + nvl(february,0) + nvl(march,0) + nvl(april,0) + nvl(may,0) + nvl(june,0) + nvl(july,0) + nvl(august,0) + nvl(september,0) + nvl(october,0) + nvl(november,0) + nvl(december,0),
	 hrs_total = nvl(hrs_jan,0) + nvl(hrs_feb,0) + nvl(hrs_mar,0) + nvl(hrs_apr,0) + nvl(hrs_may,0) + nvl(hrs_jun,0) + nvl(hrs_jul,0) + nvl(hrs_aug,0) + nvl(hrs_sep,0) + nvl(hrs_oct,0) + nvl(hrs_nov,0) + nvl(hrs_dec,0),
	 qty_total = nvl(qty_jan,0) + nvl(qty_feb,0) + nvl(qty_mar,0) + nvl(qty_apr,0) + nvl(qty_may,0) + nvl(qty_jun,0) + nvl(qty_jul,0) + nvl(qty_aug,0) + nvl(qty_sep,0) + nvl(qty_oct,0) + nvl(qty_nov,0) + nvl(qty_dec,0),
	 january = nvl(january,0),				hrs_jan = nvl(hrs_jan,0),		qty_jan = nvl(qty_jan,0),
	 february = nvl(february,0),				hrs_feb = nvl(hrs_feb,0),		qty_feb = nvl(qty_feb,0),
	 march = nvl(march,0),					hrs_mar = nvl(hrs_mar,0),		qty_mar = nvl(qty_mar,0),
	 april = nvl(april,0),						hrs_apr = nvl(hrs_apr,0),		qty_apr = nvl(qty_apr,0),
	 may = nvl(may,0),						hrs_may = nvl(hrs_may,0),		qty_may = nvl(qty_may,0),
	 june = nvl(june,0),						hrs_jun = nvl(hrs_jun,0),		qty_jun = nvl(qty_jun,0),
	 july = nvl(july,0),							hrs_jul = nvl(hrs_jul,0),		qty_jul = nvl(qty_jul,0),
	 august = nvl(august,0),					hrs_aug = nvl(hrs_aug,0),		qty_aug = nvl(qty_aug,0),
	 september = nvl(september,0),		hrs_sep = nvl(hrs_sep,0),		qty_sep = nvl(qty_sep,0),
	 october = nvl(october,0),				hrs_oct = nvl(hrs_oct,0),			qty_oct = nvl(qty_oct,0),
	 november = nvl(november,0),			hrs_nov = nvl(hrs_nov,0),		qty_nov = nvl(qty_nov,0),
	 december = nvl(december,0),			hrs_dec = nvl(hrs_dec,0),		qty_dec = nvl(qty_dec,0)
where (work_order_id, revision) in (
	select work_order_id, revision
	from wo_est_processing_temp
	);

if sqlca.sqlcode < 0 then
	MessageBox('ERROR','Could not update total for months within new range. ' +sqlca.sqlerrtext)
	rollback;
	return -1
end if

//
// Propagate dates to headers
//
update work_order_control a
set est_start_date = :a_new_start,
	est_complete_date = :a_new_end,
	est_in_service_date = :a_new_est_in_serv
where exists (
	select 1 from wo_est_processing_temp t, work_order_control woc, pp_system_control_companies pp
	where t.work_order_id = woc.work_order_id
	and woc.company_id = pp.company_id
	and upper(trim(pp.control_name)) = decode(woc.funding_wo_indicator,1,'WOEST - FP DATE CHANGE','WOEST - WO DATE CHANGE')
	and lower(trim(pp.control_value)) = 'always update'
	and woc.work_order_id = a.work_order_id
	);

if sqlca.sqlcode < 0 then
	MessageBox('ERROR','Could not update work_order_control dates. ' +sqlca.sqlerrtext)
	rollback;
	return -1
end if

//
// Update joint child work orders, if any
//
update work_order_control a
set (est_start_date, est_complete_date, est_in_service_date) = (
	select b.est_start_date, b.est_complete_date , b.est_in_service_date
	from work_order_control b, wo_est_processing_temp t, co_tenancy_wo c
	where b.work_order_id = t.work_order_id
	and t.work_order_id = c.work_order_id
	and c.co_tenant_wo = a.work_order_id
	)
where a.work_order_id in (
	select c.co_tenant_wo
	from wo_est_processing_temp t, co_tenancy_wo c
	where t.work_order_id = c.work_order_id
	);

if sqlca.SQLCode < 0 then
	messagebox("ERROR", "Could not update joint child dates. " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

//
// Custom function at end of processing
//
cst_rtn = f_budget_revisions_custom(func_name,2,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
long_arr[1] = 0
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,2,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '2'
args[2] = a_wo_fp
args[3] = string(cst_rtn3)
args[4] = string(str_arr[1])
args[5] = string(str_arr[2])
args[6] = string(str_arr[3])
cst_rtn3 = f_wo_validation_control(86,args)
if cst_rtn3 < 0 then 
   return -1
end if

// ### CDM - Maint 8188 - Remove reference to this window
//if isvalid(w_wo_est_monthly_custom) then
//	w_wo_est_monthly_custom.i_dates_changed = true
//end if

return 1

end function

public function longlong uf_cotenant_spread (string a_wo_fp);string sqls, ret, str_error, old_bud_num, bud_num, bud_desc, co_num, co_desc
longlong i, null_long_arr[], rtn, long_arr[], cnt
any results[]
uo_ds_top ds_a

if a_wo_fp = 'fp' then
	
	// DON'T RELY ON THE BUDGET ITEM 
	////
	//// Checking to ensure projects exist for all companies in the co tenant agreement(s) specified on the budget items
	////
	//i_status_position = 16
	//uf_msg('Looking for cotenant projects missing from budget items','I')
	//
	//cnt = 0
	//select count(*) into :cnt from (
	//	select b.budget_id, co.company_id
	//	from wo_est_processing_temp t, work_order_control woc, budget b, co_tenancy_partners co
	//	where t.work_order_id = woc.work_order_id
	//	and woc.budget_id = b.budget_id
	//	and b.agreemnt_id = co.agreemnt_id
	//	minus
	//	select woc.budget_id, woc.company_id
	//	from work_order_control woc
	//	where funding_wo_indicator = 1
	//	and budget_id in (
	//		select b.budget_id
	//		from wo_est_processing_temp t, work_order_control woc, budget b
	//		where t.work_order_id = woc.work_order_id
	//		and woc.budget_id = b.budget_id
	//		and nvl(b.agreemnt_id,0) <> 0
	//		)
	//	);
	//
	//if cnt > 0 then
	//	sqls = &
	//		"select b.budget_number, b.description budget, c.gl_company_no, c.description company " +&
	//		"from budget b, " +&
	//		"	company_setup c, " +&
	//		"	(	select b.budget_id, co.company_id " +&
	//		"		from wo_est_processing_temp t, work_order_control woc, budget b, co_tenancy_partners co " +&
	//		"		where t.work_order_id = woc.work_order_id " +&
	//		"		and woc.budget_id = b.budget_id " +&
	//		"		and b.agreemnt_id = co.agreemnt_id " +&
	//		"		minus " +&
	//		"		select woc.budget_id, woc.company_id " +&
	//		"		from work_order_control woc " +&
	//		"		where funding_wo_indicator = 1 " +&
	//		"		and budget_id in ( " +&
	//		"			select b.budget_id " +&
	//		"			from wo_est_processing_temp t, work_order_control woc, budget b " +&
	//		"			where t.work_order_id = woc.work_order_id " +&
	//		"			and woc.budget_id = b.budget_id " +&
	//		"			and nvl(b.agreemnt_id,0) <> 0 " +&
	//		"			) " +&
	//		"	) a " +&
	//		"where a.budget_id = b.budget_id " +&
	//		"and a.company_id = c.company_id "
	//	ds_a = create uo_ds_top
	//	ret = f_create_dynamic_ds(ds_a,'grid',sqls,sqlca,true)
	//	if ret <> 'OK' then
	//		i_status_position = 0
	//		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
	//		//if isvalid(w_progressbar) then
	//		//	close(w_progressbar)
	//		//end if
	//		uf_msg('Error creating datastore for cotenant projects missing from budget items','E')
	//		return -1
	//	end if
	//	ds_a.setsort('budget_number, gl_company_no')
	//	ds_a.sort()
	//	if ds_a.rowcount() > 0 then
	//		str_error = 'Cotenant Agreements exist for the following budget items, but no corresponding funding projects exist for the budget items & companies listed below:~r~n'
	//		old_bud_num = " "
	//		for i = 1 to ds_a.rowcount()
	//			bud_num = ds_a.getitemstring(i,'budget_number')
	//			bud_desc = ds_a.getitemstring(i,"budget")
	//			co_num = ds_a.getitemstring(i,"gl_company_no")
	//			co_desc = ds_a.getitemstring(i,"company")
	//			if bud_num <> old_bud_num then
	//				str_error += bud_num+' ('+bud_desc+')~r~n'
	//				old_bud_num = bud_num
	//			end if
	//			str_error += '   '+co_num+' - '+co_desc+'~r~n'
	//		next
	//		i_status_position = 0
	//		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
	//		//if isvalid(w_progressbar) then
	//		//	close(w_progressbar)
	//		//end if
	//		uf_msg(str_error,'E')
	//		return -1
	//	end if
	//	destroy ds_a
	//end if
	//
	////
	//// Checking to ensure only one project exists for each company in the co tenant agreement(s) specified on the budget items
	////
	//i_status_position = 16
	//uf_msg('Looking for extra projects associated to the budget items','I')
	//
	//cnt = 0
	//select count(*) into :cnt from (
	//	select woc.budget_id, woc.company_id, rank() over(partition by woc.budget_id, woc.company_id order by woc.budget_id, woc.company_id, woc.work_order_id) cnt
	//	from work_order_control woc
	//	where funding_wo_indicator = 1
	//	and budget_id in (
	//		select b.budget_id
	//		from wo_est_processing_temp t, work_order_control woc, budget b
	//		where t.work_order_id = woc.work_order_id
	//		and woc.budget_id = b.budget_id
	//		and nvl(b.agreemnt_id,0) <> 0
	//		)
	//	minus
	//	select b.budget_id, co.company_id, 1 cnt
	//	from wo_est_processing_temp t, work_order_control woc, budget b, co_tenancy_partners co
	//	where t.work_order_id = woc.work_order_id
	//	and woc.budget_id = b.budget_id
	//	and b.agreemnt_id = co.agreemnt_id
	//	);
	//
	//if cnt > 0 then
	//	sqls = &
	//		"select b.budget_number, b.description budget, c.gl_company_no, c.description company, a.cnt " +&
	//		"from budget b, " +&
	//		"	company_setup c, " +&
	//		"	(	select woc.budget_id, woc.company_id, rank() over(partition by woc.budget_id, woc.company_id order by woc.budget_id, woc.company_id, woc.work_order_id) cnt " +&
	//		"		from work_order_control woc " +&
	//		"		where funding_wo_indicator = 1 " +&
	//		"		and budget_id in ( " +&
	//		"			select b.budget_id " +&
	//		"			from wo_est_processing_temp t, work_order_control woc, budget b " +&
	//		"			where t.work_order_id = woc.work_order_id " +&
	//		"			and woc.budget_id = b.budget_id " +&
	//		"			and nvl(b.agreemnt_id,0) <> 0 " +&
	//		"			) " +&
	//		"		minus " +&
	//		"		select b.budget_id, co.company_id, 1 cnt " +&
	//		"		from wo_est_processing_temp t, work_order_control woc, budget b, co_tenancy_partners co " +&
	//		"		where t.work_order_id = woc.work_order_id " +&
	//		"		and woc.budget_id = b.budget_id " +&
	//		"		and b.agreemnt_id = co.agreemnt_id " +&
	//		"	) a " +&
	//		"where a.budget_id = b.budget_id " +&
	//		"and a.company_id = c.company_id "
	//	ds_a = create uo_ds_top
	//	ret = f_create_dynamic_ds(ds_a,'grid',sqls,sqlca,true)
	//	if ret <> 'OK' then
	//		i_status_position = 0
	//		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
	//		//if isvalid(w_progressbar) then
	//		//	close(w_progressbar)
	//		//end if
	//		uf_msg('Error creating datastore for cotenant projects missing from budget items','E')
	//		return -1
	//	end if
	//	
	//	ds_a.setfilter("cnt = 1")
	//	ds_a.filter()
	//	ds_a.setsort('budget_number, gl_company_no')
	//	ds_a.sort()
	//	if ds_a.rowcount() > 0 then
	//		str_error = 'Cotenant Agreements exist for the following budget items, but funding projects exist for the companies listed below that are not in the agreement:~r~n'
	//		old_bud_num = " "
	//		for i = 1 to ds_a.rowcount()
	//			bud_num = ds_a.getitemstring(i,'budget_number')
	//			bud_desc = ds_a.getitemstring(i,"budget")
	//			co_num = ds_a.getitemstring(i,"gl_company_no")
	//			co_desc = ds_a.getitemstring(i,"company")
	//			if bud_num <> old_bud_num then
	//				str_error += bud_num+' ('+bud_desc+')~r~n'
	//				old_bud_num = bud_num
	//			end if
	//			str_error += '   '+co_num+' - '+co_desc+'~r~n'
	//		next
	//		i_status_position = 0
	//		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
	//		//if isvalid(w_progressbar) then
	//		//	close(w_progressbar)
	//		//end if
	//		uf_msg(str_error,'E')
	//		return -1
	//	end if
	//	
	//	ds_a.setfilter("cnt = 2")
	//	ds_a.filter()
	//	ds_a.setsort('budget_number, gl_company_no')
	//	ds_a.sort()
	//	if ds_a.rowcount() > 0 then
	//		str_error = 'Cotenant Agreements exist for the following budget items, but more than one funding project exists for the budget items & companies listed below:~r~n'
	//		old_bud_num = " "
	//		for i = 1 to ds_a.rowcount()
	//			bud_num = ds_a.getitemstring(i,'budget_number')
	//			bud_desc = ds_a.getitemstring(i,"budget")
	//			co_num = ds_a.getitemstring(i,"gl_company_no")
	//			co_desc = ds_a.getitemstring(i,"company")
	//			if bud_num <> old_bud_num then
	//				str_error += bud_num+' ('+bud_desc+')~r~n'
	//				old_bud_num = bud_num
	//			end if
	//			str_error += '   '+co_num+' - '+co_desc+'~r~n'
	//		next
	//		i_status_position = 0
	//		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
	//		//if isvalid(w_progressbar) then
	//		//	close(w_progressbar)
	//		//end if
	//		uf_msg(str_error,'E')
	//		return -1
	//	end if
	//
	//	destroy ds_a
	//end if
	
	
	//
	// Pull missing projects into the processing for the applicable agreement / budget version
	//
	i_status_position = 10
	uf_msg('Pulling in missing projects for cotenant spread processing','I')
	
	insert into wo_est_processing_temp (work_order_id, revision)
	select a.co_tenant_wo, nvl(bvfp.revision,0) revision
	from budget_version_fund_proj bvfp,
		(  with ids as (
				select c.work_order_id id, c.work_order_id co_tenant_wo, woc.company_id, woa.agreemnt_id
				from co_tenancy_wo c, work_order_control woc, work_order_account woa
				where c.work_order_id = woc.work_order_id
				and woc.funding_wo_indicator = 1
				and c.work_order_id = woa.work_order_id
				union
				select c.work_order_id id, c.co_tenant_wo, woc.company_id, woa.agreemnt_id
				from co_tenancy_wo c, work_order_control woc, work_order_account woa
				where c.co_tenant_wo = woc.work_order_id
				and woc.funding_wo_indicator = 1
				and c.work_order_id = woa.work_order_id
				)
			select co_tenant_wo
			from ids
			where id in (
				select ids.id
				from ids, wo_est_processing_temp t
				where ids.co_tenant_wo = t.work_order_id
				)
			minus
			select work_order_id
			from wo_est_processing_temp
		) a
	where bvfp.work_order_id (+) = a.co_tenant_wo
	and bvfp.budget_version_id (+) = :i_cotenant_bv_id
	and bvfp.active (+) = 1;
	
	if uf_check_sql('Inserting into wo_est_processing_temp') <> 1 then return -1
	
	//
	// Remove projects from processing that are not in an agreement / not applicable
	//
	i_status_position = 20
	uf_msg('Removing projects that are not in an agreement from cotenant spread processing','I')
	
	delete from wo_est_processing_temp
	where work_order_id in
		(  with ids as (
				select c.work_order_id id, c.work_order_id co_tenant_wo, woc.company_id, woa.agreemnt_id
				from co_tenancy_wo c, work_order_control woc, work_order_account woa
				where c.work_order_id = woc.work_order_id
				and woc.funding_wo_indicator = 1
				and c.work_order_id = woa.work_order_id
				union
				select c.work_order_id id, c.co_tenant_wo, woc.company_id, woa.agreemnt_id
				from co_tenancy_wo c, work_order_control woc, work_order_account woa
				where c.co_tenant_wo = woc.work_order_id
				and woc.funding_wo_indicator = 1
				and c.work_order_id = woa.work_order_id
				)
			select work_order_id
			from wo_est_processing_temp
			minus
			select co_tenant_wo
			from ids
			where id in (
				select ids.id
				from ids, wo_est_processing_temp t
				where ids.co_tenant_wo = t.work_order_id
				)
		);
	
	if uf_check_sql('Deleting from wo_est_processing_temp') <> 1 then return -1
	
	//
	// Inserting projects into temp table
	//
	delete from wo_est_cotenant_spread_temp;
	
	if uf_check_sql('Deleting from wo_est_cotenant_spread_temp') <> 1 then return -1
	
	//insert into wo_est_cotenant_spread_temp (work_order_id, agreemnt_id, budget_id)
	//select distinct list.work_order_id, b.agreemnt_id, b.budget_id
	//from wo_est_processing_temp t, work_order_control woc, budget b, work_order_control list, co_tenancy_partners co
	//where t.work_order_id = woc.work_order_id
	//and woc.budget_id = b.budget_id
	//and b.agreemnt_id = co.agreemnt_id
	//and b.budget_id = list.budget_id
	//and co.company_id = list.company_id
	//and list.funding_wo_indicator = 1;
	
	insert into wo_est_cotenant_spread_temp (work_order_id, agreemnt_id, id)
	select co_tenant_wo, agreemnt_id, id
	from (
		select c.work_order_id id, c.work_order_id co_tenant_wo, woc.company_id, woa.agreemnt_id
		from co_tenancy_wo c, work_order_control woc, work_order_account woa
		where c.work_order_id = woc.work_order_id
		and woc.funding_wo_indicator = 1
		and c.work_order_id = woa.work_order_id
		union
		select c.work_order_id id, c.co_tenant_wo, woc.company_id, woa.agreemnt_id
		from co_tenancy_wo c, work_order_control woc, work_order_account woa
		where c.co_tenant_wo = woc.work_order_id
		and woc.funding_wo_indicator = 1
		and c.work_order_id = woa.work_order_id
		);
	
	if uf_check_sql('Inserting into wo_est_cotenant_spread_temp') <> 1 then return -1
	
	
	delete from temp_work_order;
	
	if uf_check_sql('Deleting from temp_work_order') <> 1 then return -1
	
	insert into temp_work_order (user_id, session_id, batch_report_id, work_order_id)
	select user, userenv('sessionid'), 0, work_order_id
	from wo_est_cotenant_spread_temp;
	
	if uf_check_sql('Inserting into temp_work_order') <> 1 then return -1
	
	
	//
	// Create new revisions for projects not in the selected budget version
	//
	i_status_position = 30
	uf_msg('Creating new revisions for budget version where necessary','I')
	
	delete from wo_est_processing_temp;
	
	insert into wo_est_processing_temp (work_order_id, revision)
	select t.work_order_id, nvl(bvfp.revision,0) revision
	from wo_est_cotenant_spread_temp t, budget_version_fund_proj bvfp
	where t.work_order_id = bvfp.work_order_id (+)
	and bvfp.budget_version_id (+) = :i_cotenant_bv_id
	and bvfp.active (+) = 1
	minus
	select t.work_order_id, bvfp.revision
	from wo_est_cotenant_spread_temp t, budget_version_fund_proj bvfp,
		(select work_order_id, revision, nvl(max(revision_edit),1) revision_edit from wo_est_month_is_editable group by work_order_id, revision) editable
	where t.work_order_id = bvfp.work_order_id
	and bvfp.budget_version_id = :i_cotenant_bv_id
	and bvfp.active = 1
	and bvfp.work_order_id = editable.work_order_id
	and bvfp.revision = editable.revision
	and editable.revision_edit = 1;
	
	if uf_check_sql('Inserting into wo_est_processing_temp') <> 1 then return -1
	
	if sqlca.sqlnrows > 0 then
		i_new_bv_ids = null_long_arr
		i_new_bv_ids[1] = i_cotenant_bv_id
		
		rtn = uf_new_revision('fp')
		if rtn <> 1 then
			return -1
		end if
	end if
	
	//
	// Fill in the revision numbers for the projects
	//
	update wo_est_cotenant_spread_temp a
	set a.revision = (
		select bvfp.revision
		from budget_version_fund_proj bvfp
		where bvfp.work_order_id = a.work_order_id
		and bvfp.budget_version_id = :i_cotenant_bv_id
		and bvfp.active = 1
		);
	
	if uf_check_sql('Updating revision numbers on wo_est_cotenant_spread_temp') <> 1 then return -1
	
	//
	// Fill in partner percentages
	//
	update wo_est_cotenant_spread_temp a
	set a.co_tenancy_pct = (
		select p.co_tenancy_pct
		from work_order_control woc, co_tenancy_partners p
		where woc.work_order_id = a.work_order_id
		and p.company_id = woc.company_id
		and p.agreemnt_id = a.agreemnt_id
		);
	
	if uf_check_sql('Updating partner percentages on wo_est_cotenant_spread_temp') <> 1 then return -1
	
	//
	// Save off results after summing estimates and splitting according to percentages
	//
	i_status_position = 44
	uf_msg('Calculating cotenant estimate results','I')
	
	delete from wo_est_slide_results_temp;
	
	if uf_check_sql('Cleaning out previous results') <> 1 then return -1
	
	// ### CDM - Maint 8962 - Need to account for actuals months in budget co-tenant spread
	insert into wo_est_slide_results_temp (
		work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
		/*factor_id, curve_id,*/ rate_type_id,
		january, february, march, april, may, june,
		july, august, september, october, november, december,
		hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun,
		hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
		qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun,
		qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec )
	WITH estimates as (
		select t.work_order_id, t.revision, wem.year, wem.expenditure_type_id, wem.est_chg_type_id, wem.department_id, wem.utility_account_id, wem.wo_work_order_id, wem.job_task_id, wem.long_description, wem.budget_id,
			wem.attribute01_id, wem.attribute02_id, wem.attribute03_id, wem.attribute04_id, wem.attribute05_id, wem.attribute06_id, wem.attribute07_id, wem.attribute08_id, wem.attribute09_id, wem.attribute10_id,
			/*wems.factor_id, wems.curve_id,*/ wems.rate_type_id,
			wem.january, wem.february, wem.march, wem.april, wem.may, wem.june, wem.july, wem.august, wem.september, wem.october, wem.november, wem.december,
			wem.hrs_jan, wem.hrs_feb, wem.hrs_mar, wem.hrs_apr, wem.hrs_may, wem.hrs_jun, wem.hrs_jul, wem.hrs_aug, wem.hrs_sep, wem.hrs_oct, wem.hrs_nov, wem.hrs_dec,
			wem.qty_jan, wem.qty_feb, wem.qty_mar, wem.qty_apr, wem.qty_may, wem.qty_jun, wem.qty_jul, wem.qty_aug, wem.qty_sep, wem.qty_oct, wem.qty_nov, wem.qty_dec,
			to_number(substr(nvl(woa.actuals_month_number,0),1,4)) actuals_year,
			nvl(woa.actuals_month_number,0) actuals_month,
			t.co_tenancy_pct,
			t.id //woc.budget_id
		from 	wo_est_cotenant_spread_temp t,
			work_order_control woc,
			wo_est_monthly wem,
			wo_est_monthly_spread wems,
			work_order_approval woa
		where t.work_order_id = wem.work_order_id
		and t.revision = wem.revision
		and wem.est_monthly_id = wems.est_monthly_id (+)
		and t.work_order_id = woc.work_order_id
		and t.work_order_id = woa.work_order_id
		and t.revision = woa.revision
		)
	// Actuals Months
	select est.work_order_id, est.revision, est.year, est.expenditure_type_id, est.est_chg_type_id, est.department_id, est.utility_account_id, est.wo_work_order_id, est.job_task_id, est.long_description, est.budget_id,
		est.attribute01_id, est.attribute02_id, est.attribute03_id, est.attribute04_id, est.attribute05_id, est.attribute06_id, est.attribute07_id, est.attribute08_id, est.attribute09_id, est.attribute10_id,
		/*est.factor_id, est.curve_id,*/ est.rate_type_id,
		case when est.year*100+1 <= est.actuals_month then nvl(est.january,0) else 0 end jan,
		case when est.year*100+2 <= est.actuals_month then nvl(est.february,0) else 0 end feb,
		case when est.year*100+3 <= est.actuals_month then nvl(est.march,0) else 0 end mar,
		case when est.year*100+4 <= est.actuals_month then nvl(est.april,0) else 0 end apr,
		case when est.year*100+5 <= est.actuals_month then nvl(est.may,0) else 0 end may,
		case when est.year*100+6 <= est.actuals_month then nvl(est.june,0) else 0 end jun,
		case when est.year*100+7 <= est.actuals_month then nvl(est.july,0) else 0 end jul,
		case when est.year*100+8 <= est.actuals_month then nvl(est.august,0) else 0 end aug,
		case when est.year*100+9 <= est.actuals_month then nvl(est.september,0) else 0 end sep,
		case when est.year*100+10 <= est.actuals_month then nvl(est.october,0) else 0 end oct,
		case when est.year*100+11 <= est.actuals_month then nvl(est.november,0) else 0 end nov,
		case when est.year*100+12 <= est.actuals_month then nvl(est.december,0) else 0 end dec,
		case when est.year*100+1 <= est.actuals_month then nvl(est.hrs_jan,0) else 0 end hrs_jan,
		case when est.year*100+2 <= est.actuals_month then nvl(est.hrs_feb,0) else 0 end hrs_feb,
		case when est.year*100+3 <= est.actuals_month then nvl(est.hrs_mar,0) else 0 end hrs_mar,
		case when est.year*100+4 <= est.actuals_month then nvl(est.hrs_apr,0) else 0 end hrs_apr,
		case when est.year*100+5 <= est.actuals_month then nvl(est.hrs_may,0) else 0 end hrs_may,
		case when est.year*100+6 <= est.actuals_month then nvl(est.hrs_jun,0) else 0 end hrs_jun,
		case when est.year*100+7 <= est.actuals_month then nvl(est.hrs_jul,0) else 0 end hrs_jul,
		case when est.year*100+8 <= est.actuals_month then nvl(est.hrs_aug,0) else 0 end hrs_aug,
		case when est.year*100+9 <= est.actuals_month then nvl(est.hrs_sep,0) else 0 end hrs_sep,
		case when est.year*100+10 <= est.actuals_month then nvl(est.hrs_oct,0) else 0 end hrs_oct,
		case when est.year*100+11 <= est.actuals_month then nvl(est.hrs_nov,0) else 0 end hrs_nov,
		case when est.year*100+12 <= est.actuals_month then nvl(est.hrs_dec,0) else 0 end hrs_dec,
		case when est.year*100+1 <= est.actuals_month then nvl(est.qty_jan,0) else 0 end qty_jan,
		case when est.year*100+2 <= est.actuals_month then nvl(est.qty_feb,0) else 0 end qty_feb,
		case when est.year*100+3 <= est.actuals_month then nvl(est.qty_mar,0) else 0 end qty_mar,
		case when est.year*100+4 <= est.actuals_month then nvl(est.qty_apr,0) else 0 end qty_apr,
		case when est.year*100+5 <= est.actuals_month then nvl(est.qty_may,0) else 0 end qty_may,
		case when est.year*100+6 <= est.actuals_month then nvl(est.qty_jun,0) else 0 end qty_jun,
		case when est.year*100+7 <= est.actuals_month then nvl(est.qty_jul,0) else 0 end qty_jul,
		case when est.year*100+8 <= est.actuals_month then nvl(est.qty_aug,0) else 0 end qty_aug,
		case when est.year*100+9 <= est.actuals_month then nvl(est.qty_sep,0) else 0 end qty_sep,
		case when est.year*100+10 <= est.actuals_month then nvl(est.qty_oct,0) else 0 end qty_oct,
		case when est.year*100+11 <= est.actuals_month then nvl(est.qty_nov,0) else 0 end qty_nov,
		case when est.year*100+12 <= est.actuals_month then nvl(est.qty_dec,0) else 0 end qty_dec
	from estimates est
	where est.year <= est.actuals_year
	UNION ALL
	// Forecasted Months
	select split.work_order_id, split.revision, est.year, est.expenditure_type_id, est.est_chg_type_id, est.department_id, est.utility_account_id, est.wo_work_order_id, est.job_task_id, est.long_description, est.budget_id,
		est.attribute01_id, est.attribute02_id, est.attribute03_id, est.attribute04_id, est.attribute05_id, est.attribute06_id, est.attribute07_id, est.attribute08_id, est.attribute09_id, est.attribute10_id,
		/*est.factor_id, est.curve_id,*/ est.rate_type_id,
		sum(case when est.year*100+1 > est.actuals_month then split.co_tenancy_pct*nvl(est.january,0) else 0 end) jan,
		sum(case when est.year*100+2 > est.actuals_month then split.co_tenancy_pct*nvl(est.february,0) else 0 end) feb,
		sum(case when est.year*100+3 > est.actuals_month then split.co_tenancy_pct*nvl(est.march,0) else 0 end) mar,
		sum(case when est.year*100+4 > est.actuals_month then split.co_tenancy_pct*nvl(est.april,0) else 0 end) apr,
		sum(case when est.year*100+5 > est.actuals_month then split.co_tenancy_pct*nvl(est.may,0) else 0 end) may,
		sum(case when est.year*100+6 > est.actuals_month then split.co_tenancy_pct*nvl(est.june,0) else 0 end) jun,
		sum(case when est.year*100+7 > est.actuals_month then split.co_tenancy_pct*nvl(est.july,0) else 0 end) jul,
		sum(case when est.year*100+8 > est.actuals_month then split.co_tenancy_pct*nvl(est.august,0) else 0 end) aug,
		sum(case when est.year*100+9 > est.actuals_month then split.co_tenancy_pct*nvl(est.september,0) else 0 end) sep,
		sum(case when est.year*100+10 > est.actuals_month then split.co_tenancy_pct*nvl(est.october,0) else 0 end) oct,
		sum(case when est.year*100+11 > est.actuals_month then split.co_tenancy_pct*nvl(est.november,0) else 0 end) nov,
		sum(case when est.year*100+12 > est.actuals_month then split.co_tenancy_pct*nvl(est.december,0) else 0 end) dec,
		sum(case when est.year*100+1 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_jan,0) else 0 end) hrs_jan,
		sum(case when est.year*100+2 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_feb,0) else 0 end) hrs_feb,
		sum(case when est.year*100+3 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_mar,0) else 0 end) hrs_mar,
		sum(case when est.year*100+4 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_apr,0) else 0 end) hrs_apr,
		sum(case when est.year*100+5 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_may,0) else 0 end) hrs_may,
		sum(case when est.year*100+6 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_jun,0) else 0 end) hrs_jun,
		sum(case when est.year*100+7 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_jul,0) else 0 end) hrs_jul,
		sum(case when est.year*100+8 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_aug,0) else 0 end) hrs_aug,
		sum(case when est.year*100+9 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_sep,0) else 0 end) hrs_sep,
		sum(case when est.year*100+10 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_oct,0) else 0 end) hrs_oct,
		sum(case when est.year*100+11 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_nov,0) else 0 end) hrs_nov,
		sum(case when est.year*100+12 > est.actuals_month then split.co_tenancy_pct*nvl(est.hrs_dec,0) else 0 end) hrs_dec,
		sum(case when est.year*100+1 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_jan,0) else 0 end) qty_jan,
		sum(case when est.year*100+2 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_feb,0) else 0 end) qty_feb,
		sum(case when est.year*100+3 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_mar,0) else 0 end) qty_mar,
		sum(case when est.year*100+4 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_apr,0) else 0 end) qty_apr,
		sum(case when est.year*100+5 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_may,0) else 0 end) qty_may,
		sum(case when est.year*100+6 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_jun,0) else 0 end) qty_jun,
		sum(case when est.year*100+7 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_jul,0) else 0 end) qty_jul,
		sum(case when est.year*100+8 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_aug,0) else 0 end) qty_aug,
		sum(case when est.year*100+9 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_sep,0) else 0 end) qty_sep,
		sum(case when est.year*100+10 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_oct,0) else 0 end) qty_oct,
		sum(case when est.year*100+11 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_nov,0) else 0 end) qty_nov,
		sum(case when est.year*100+12 > est.actuals_month then split.co_tenancy_pct*nvl(est.qty_dec,0) else 0 end) qty_dec
	from 	estimates est,
		wo_est_cotenant_spread_temp split
	where split.id = est.id
	and est.year >= est.actuals_year
	group by split.work_order_id, split.revision, est.year, est.expenditure_type_id, est.est_chg_type_id, est.department_id, est.utility_account_id, est.wo_work_order_id, est.job_task_id, est.long_description, est.budget_id,
		est.attribute01_id, est.attribute02_id, est.attribute03_id, est.attribute04_id, est.attribute05_id, est.attribute06_id, est.attribute07_id, est.attribute08_id, est.attribute09_id, est.attribute10_id,
		/*est.factor_id, est.curve_id,*/ est.rate_type_id;
	
	if uf_check_sql('Saving off co tenancy estimate results') <> 1 then return -1
	
	//
	// Backfill the est_monthly_id's for unique estimates
	//
	i_status_position = 58
	uf_msg('Backfilling estimate ids','I')
	
	update wo_est_slide_results_temp
	set est_monthly_id = pwrplant1.nextval;
	
	if uf_check_sql('Backfilling est_monthly_ids on estimate results') <> 1 then return -1
	
	update wo_est_slide_results_temp a
	set a.est_monthly_id = (
		select min(b.est_monthly_id)
		from wo_est_slide_results_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		and b.expenditure_type_id = a.expenditure_type_id
		and b.est_chg_type_id = a.est_chg_type_id
		and nvl(b.department_id,0) = nvl(a.department_id,0)
		and nvl(b.utility_account_id,0) = nvl(a.utility_account_id,0)
		and nvl(b.wo_work_order_id,0) = nvl(a.wo_work_order_id,0) // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
		and nvl(b.job_task_id,'*') = nvl(a.job_task_id,'*')
		and nvl(b.long_description,'*') = nvl(a.long_description,'*')
		and nvl(b.budget_id,0) = nvl(a.budget_id,0)
		and nvl(b.attribute01_id,'*') = nvl(a.attribute01_id,'*')
		and nvl(b.attribute02_id,'*') = nvl(a.attribute02_id,'*')
		and nvl(b.attribute03_id,'*') = nvl(a.attribute03_id,'*')
		and nvl(b.attribute04_id,'*') = nvl(a.attribute04_id,'*')
		and nvl(b.attribute05_id,'*') = nvl(a.attribute05_id,'*')
		and nvl(b.attribute06_id,'*') = nvl(a.attribute06_id,'*')
		and nvl(b.attribute07_id,'*') = nvl(a.attribute07_id,'*')
		and nvl(b.attribute08_id,'*') = nvl(a.attribute08_id,'*')
		and nvl(b.attribute09_id,'*') = nvl(a.attribute09_id,'*')
		and nvl(b.attribute10_id,'*') = nvl(a.attribute10_id,'*')
		and nvl(b.factor_id,0) = nvl(a.factor_id,0)
		and nvl(b.curve_id,0) = nvl(a.curve_id,0)
		and nvl(b.rate_type_id,0) = nvl(a.rate_type_id,0)
		);
	
	if uf_check_sql('Updating unique est_monthly_ids on estimate results') <> 1 then return -1
	
	//
	// NEED TO UPDATE ROUNDING ERRORS
	//
	i_status_position = 72
	uf_msg('Updating rounding errors','I')
	
	update wo_est_slide_results_temp a
	set (
		a.january, a.february, a.march, a.april, a.may, a.june,
		a.july, a.august, a.september, a.october, a.november, a.december,
		a.hrs_jan, a.hrs_feb, a.hrs_mar, a.hrs_apr, a.hrs_may, a.hrs_jun,
		a.hrs_jul, a.hrs_aug, a.hrs_sep, a.hrs_oct, a.hrs_nov, a.hrs_dec,
		a.qty_jan, a.qty_feb, a.qty_mar, a.qty_apr, a.qty_may, a.qty_jun,
		a.qty_jul, a.qty_aug, a.qty_sep, a.qty_oct, a.qty_nov, a.qty_dec ) =
		(
		select nvl(a.january,0) + d.jan jan, nvl(a.february,0) + d.feb feb, nvl(a.march,0) + d.mar mar, nvl(a.april,0) + d.apr apr, nvl(a.may,0) + d.may may, nvl(a.june,0) + d.jun jun,
			nvl(a.july,0) + d.jul jul, nvl(a.august,0) + d.aug aug, nvl(a.september,0) + d.sep sep, nvl(a.october,0) + d.oct oct, nvl(a.november,0) + d.nov nov, nvl(a.december,0) + d.dec dec,
			nvl(a.hrs_jan,0) + d.hrs_jan hrs_jan, nvl(a.hrs_feb,0) + d.hrs_feb hrs_feb, nvl(a.hrs_mar,0) + d.hrs_mar hrs_mar, nvl(a.hrs_apr,0) + d.hrs_apr hrs_apr, nvl(a.hrs_may,0) + d.hrs_may hrs_may, nvl(a.hrs_jun,0) + d.hrs_jun hrs_jun,
			nvl(a.hrs_jul,0) + d.hrs_jul hrs_jul, nvl(a.hrs_aug,0) + d.hrs_aug hrs_aug, nvl(a.hrs_sep,0) + d.hrs_sep hrs_sep, nvl(a.hrs_oct,0) + d.hrs_oct hrs_oct, nvl(a.hrs_nov,0) + d.hrs_nov hrs_nov, nvl(a.hrs_dec,0) + d.hrs_dec hrs_dec,
			nvl(a.qty_jan,0) + d.qty_jan qty_jan, nvl(a.qty_feb,0) + d.qty_feb qty_feb, nvl(a.qty_mar,0) + d.qty_mar qty_mar, nvl(a.qty_apr,0) + d.qty_apr qty_apr, nvl(a.qty_may,0) + d.qty_may qty_may, nvl(a.qty_jun,0) + d.qty_jun qty_jun,
			nvl(a.qty_jul,0) + d.qty_jul qty_jul, nvl(a.qty_aug,0) + d.qty_aug qty_aug, nvl(a.qty_sep,0) + d.qty_sep qty_sep, nvl(a.qty_oct,0) + d.qty_oct qty_oct, nvl(a.qty_nov,0) + d.qty_nov qty_nov, nvl(a.qty_dec,0) + d.qty_dec qty_dec
		from (
			select c.year, c.expenditure_type_id, c.est_chg_type_id, c.department_id, c.utility_account_id, c.wo_work_order_id, c.job_task_id, c.long_description, c.budget_id,
				c.attribute01_id, c.attribute02_id, c.attribute03_id, c.attribute04_id, c.attribute05_id, c.attribute06_id, c.attribute07_id, c.attribute08_id, c.attribute09_id, c.attribute10_id,
				/*c.factor_id, c.curve_id,*/ c.rate_type_id,
				sum(nvl(c.jan,0)) jan, sum(nvl(c.feb,0)) feb, sum(nvl(c.mar,0)) mar, sum(nvl(c.apr,0)) apr, sum(nvl(c.may,0)) may, sum(nvl(c.jun,0)) jun,
				sum(nvl(c.jul,0)) jul, sum(nvl(c.aug,0)) aug, sum(nvl(c.sep,0)) sep, sum(nvl(c.oct,0)) oct, sum(nvl(c.nov,0)) nov, sum(nvl(c.dec,0)) dec, 
				sum(nvl(c.hrs_jan,0)) hrs_jan, sum(nvl(c.hrs_feb,0)) hrs_feb, sum(nvl(c.hrs_mar,0)) hrs_mar, sum(nvl(c.hrs_apr,0)) hrs_apr, sum(nvl(c.hrs_may,0)) hrs_may, sum(nvl(c.hrs_jun,0)) hrs_jun,
				sum(nvl(c.hrs_jul,0)) hrs_jul, sum(nvl(c.hrs_aug,0)) hrs_aug, sum(nvl(c.hrs_sep,0)) hrs_sep, sum(nvl(c.hrs_oct,0)) hrs_oct, sum(nvl(c.hrs_nov,0)) hrs_nov, sum(nvl(c.hrs_dec,0)) hrs_dec,
				sum(nvl(c.qty_jan,0)) qty_jan, sum(nvl(c.qty_feb,0)) qty_feb, sum(nvl(c.qty_mar,0)) qty_mar, sum(nvl(c.qty_apr,0)) qty_apr, sum(nvl(c.qty_may,0)) qty_may, sum(nvl(c.qty_jun,0)) qty_jun,
				sum(nvl(c.qty_jul,0)) qty_jul, sum(nvl(c.qty_aug,0)) qty_aug, sum(nvl(c.qty_sep,0)) qty_sep, sum(nvl(c.qty_oct,0)) qty_oct, sum(nvl(c.qty_nov,0)) qty_nov, sum(nvl(c.qty_dec,0)) qty_dec
			from (
				select wem.year, wem.expenditure_type_id, wem.est_chg_type_id, wem.department_id, wem.utility_account_id, wem.wo_work_order_id, wem.job_task_id, wem.long_description, wem.budget_id,
					wem.attribute01_id, wem.attribute02_id, wem.attribute03_id, wem.attribute04_id, wem.attribute05_id, wem.attribute06_id, wem.attribute07_id, wem.attribute08_id, wem.attribute09_id, wem.attribute10_id,
					/*wems.factor_id, wems.curve_id,*/ wems.rate_type_id,
					sum(nvl(wem.january,0)) jan, sum(nvl(wem.february,0)) feb, sum(nvl(wem.march,0)) mar, sum(nvl(wem.april,0)) apr, sum(nvl(wem.may,0)) may, sum(nvl(wem.june,0)) jun,
					sum(nvl(wem.july,0)) jul, sum(nvl(wem.august,0)) aug, sum(nvl(wem.september,0)) sep, sum(nvl(wem.october,0)) oct, sum(nvl(wem.november,0)) nov, sum(nvl(wem.december,0)) dec, 
					sum(nvl(wem.hrs_jan,0)) hrs_jan, sum(nvl(wem.hrs_feb,0)) hrs_feb, sum(nvl(wem.hrs_mar,0)) hrs_mar, sum(nvl(wem.hrs_apr,0)) hrs_apr, sum(nvl(wem.hrs_may,0)) hrs_may, sum(nvl(wem.hrs_jun,0)) hrs_jun,
					sum(nvl(wem.hrs_jul,0)) hrs_jul, sum(nvl(wem.hrs_aug,0)) hrs_aug, sum(nvl(wem.hrs_sep,0)) hrs_sep, sum(nvl(wem.hrs_oct,0)) hrs_oct, sum(nvl(wem.hrs_nov,0)) hrs_nov, sum(nvl(wem.hrs_dec,0)) hrs_dec,
					sum(nvl(wem.qty_jan,0)) qty_jan, sum(nvl(wem.qty_feb,0)) qty_feb, sum(nvl(wem.qty_mar,0)) qty_mar, sum(nvl(wem.qty_apr,0)) qty_apr, sum(nvl(wem.qty_may,0)) qty_may, sum(nvl(wem.qty_jun,0)) qty_jun,
					sum(nvl(wem.qty_jul,0)) qty_jul, sum(nvl(wem.qty_aug,0)) qty_aug, sum(nvl(wem.qty_sep,0)) qty_sep, sum(nvl(wem.qty_oct,0)) qty_oct, sum(nvl(wem.qty_nov,0)) qty_nov, sum(nvl(wem.qty_dec,0)) qty_dec
				from 	wo_est_cotenant_spread_temp t,
					wo_est_monthly wem,
					wo_est_monthly_spread wems
				where t.work_order_id = wem.work_order_id
				and t.revision = wem.revision
				and wem.est_monthly_id = wems.est_monthly_id (+)
				group by wem.year, wem.expenditure_type_id, wem.est_chg_type_id, wem.department_id, wem.utility_account_id, wem.wo_work_order_id, wem.job_task_id, wem.long_description, wem.budget_id,
					wem.attribute01_id, wem.attribute02_id, wem.attribute03_id, wem.attribute04_id, wem.attribute05_id, wem.attribute06_id, wem.attribute07_id, wem.attribute08_id, wem.attribute09_id, wem.attribute10_id,
					/*wems.factor_id, wems.curve_id,*/ wems.rate_type_id
				union all
				select wes.year, wes.expenditure_type_id, wes.est_chg_type_id, wes.department_id, wes.utility_account_id, wes.wo_work_order_id, wes.job_task_id, wes.long_description, wes.budget_id,
					wes.attribute01_id, wes.attribute02_id, wes.attribute03_id, wes.attribute04_id, wes.attribute05_id, wes.attribute06_id, wes.attribute07_id, wes.attribute08_id, wes.attribute09_id, wes.attribute10_id,
					/*wes.factor_id, wes.curve_id,*/ wes.rate_type_id,
					-1*sum(nvl(wes.january,0)) jan, -1*sum(nvl(wes.february,0)) feb, -1*sum(nvl(wes.march,0)) mar, -1*sum(nvl(wes.april,0)) apr, -1*sum(nvl(wes.may,0)) may, -1*sum(nvl(wes.june,0)) jun,
					-1*sum(nvl(wes.july,0)) jul, -1*sum(nvl(wes.august,0)) aug, -1*sum(nvl(wes.september,0)) sep, -1*sum(nvl(wes.october,0)) oct, -1*sum(nvl(wes.november,0)) nov, -1*sum(nvl(wes.december,0)) dec, 
					-1*sum(nvl(wes.hrs_jan,0)) hrs_jan, -1*sum(nvl(wes.hrs_feb,0)) hrs_feb, -1*sum(nvl(wes.hrs_mar,0)) hrs_mar, -1*sum(nvl(wes.hrs_apr,0)) hrs_apr, -1*sum(nvl(wes.hrs_may,0)) hrs_may, -1*sum(nvl(wes.hrs_jun,0)) hrs_jun,
					-1*sum(nvl(wes.hrs_jul,0)) hrs_jul, -1*sum(nvl(wes.hrs_aug,0)) hrs_aug, -1*sum(nvl(wes.hrs_sep,0)) hrs_sep, -1*sum(nvl(wes.hrs_oct,0)) hrs_oct, -1*sum(nvl(wes.hrs_nov,0)) hrs_nov, -1*sum(nvl(wes.hrs_dec,0)) hrs_dec,
					-1*sum(nvl(wes.qty_jan,0)) qty_jan, -1*sum(nvl(wes.qty_feb,0)) qty_feb, -1*sum(nvl(wes.qty_mar,0)) qty_mar, -1*sum(nvl(wes.qty_apr,0)) qty_apr, -1*sum(nvl(wes.qty_may,0)) qty_may, -1*sum(nvl(wes.qty_jun,0)) qty_jun,
					-1*sum(nvl(wes.qty_jul,0)) qty_jul, -1*sum(nvl(wes.qty_aug,0)) qty_aug, -1*sum(nvl(wes.qty_sep,0)) qty_sep, -1*sum(nvl(wes.qty_oct,0)) qty_oct, -1*sum(nvl(wes.qty_nov,0)) qty_nov, -1*sum(nvl(wes.qty_dec,0)) qty_dec
				from wo_est_slide_results_temp wes
				group by wes.year, wes.expenditure_type_id, wes.est_chg_type_id, wes.department_id, wes.utility_account_id, wes.wo_work_order_id, wes.job_task_id, wes.long_description, wes.budget_id,
					wes.attribute01_id, wes.attribute02_id, wes.attribute03_id, wes.attribute04_id, wes.attribute05_id, wes.attribute06_id, wes.attribute07_id, wes.attribute08_id, wes.attribute09_id, wes.attribute10_id,
					/*wes.factor_id, wes.curve_id,*/ wes.rate_type_id
				) c
			group by c.year, c.expenditure_type_id, c.est_chg_type_id, c.department_id, c.utility_account_id, c.wo_work_order_id, c.job_task_id, c.long_description, c.budget_id,
				c.attribute01_id, c.attribute02_id, c.attribute03_id, c.attribute04_id, c.attribute05_id, c.attribute06_id, c.attribute07_id, c.attribute08_id, c.attribute09_id, c.attribute10_id,
				/*c.factor_id, c.curve_id,*/ c.rate_type_id
			) d
		where d.year = a.year
		and d.expenditure_type_id = a.expenditure_type_id
		and d.est_chg_type_id = a.est_chg_type_id
		and nvl(d.department_id,0) = nvl(a.department_id,0)
		and nvl(d.utility_account_id,0) = nvl(a.utility_account_id,0)
		and nvl(d.wo_work_order_id,0) = nvl(a.wo_work_order_id,0) // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
		and nvl(d.job_task_id,'*') = nvl(a.job_task_id,'*')
		and nvl(d.long_description,'*') = nvl(a.long_description,'*')
		and nvl(d.budget_id,0) = nvl(a.budget_id,0)
		and nvl(d.attribute01_id,'*') = nvl(a.attribute01_id,'*')
		and nvl(d.attribute02_id,'*') = nvl(a.attribute02_id,'*')
		and nvl(d.attribute03_id,'*') = nvl(a.attribute03_id,'*')
		and nvl(d.attribute04_id,'*') = nvl(a.attribute04_id,'*')
		and nvl(d.attribute05_id,'*') = nvl(a.attribute05_id,'*')
		and nvl(d.attribute06_id,'*') = nvl(a.attribute06_id,'*')
		and nvl(d.attribute07_id,'*') = nvl(a.attribute07_id,'*')
		and nvl(d.attribute08_id,'*') = nvl(a.attribute08_id,'*')
		and nvl(d.attribute09_id,'*') = nvl(a.attribute09_id,'*')
		and nvl(d.attribute10_id,'*') = nvl(a.attribute10_id,'*')
		//and nvl(d.factor_id,0) = nvl(a.factor_id,0)
		//and nvl(d.curve_id,0) = nvl(a.curve_id,0)
		and nvl(d.rate_type_id,0) = nvl(a.rate_type_id,0)
		)
	where a.work_order_id in (
		select max(b.work_order_id) work_order_id
		from wo_est_cotenant_spread_temp b
		where b.co_tenancy_pct = (
			select max(z.co_tenancy_pct) from wo_est_cotenant_spread_temp z
			where z.agreemnt_id = b.agreemnt_id
			and z.id = b.id
			)
		group by b.agreemnt_id, b.id
		);
	
	if uf_check_sql('Updating rounding on errors on estimates') <> 1 then return -1
	
	//
	// Delete estimates from projects
	//
	i_status_position = 86
	uf_msg('Replacing estimates with cotenant calculations','I')
	
	delete from wo_est_monthly_spread wems
	where exists (
		select 1 from wo_est_monthly wem
		where wem.est_monthly_id = wems.est_monthly_id
		and exists (
			select 1 from wo_est_cotenant_spread_temp t
			where t.work_order_id = wem.work_order_id
			and t.revision = wem.revision
			)
		);
	
	if uf_check_sql('Deleting prior est monthly ids') <> 1 then return -1
	
	delete from wo_est_monthly wem
	where exists (
		select 1 from wo_est_cotenant_spread_temp t
		where t.work_order_id = wem.work_order_id
		and t.revision = wem.revision
		);
	
	if uf_check_sql('Deleting prior estimates') <> 1 then return -1
	
	//
	// Replace with new estimates on projects
	//
	insert into wo_est_monthly (
		est_monthly_id, work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
		january, february, march, april, may, june,
		july, august, september, october, november, december,
		total,
		hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun,
		hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
		hrs_total,
		qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun,
		qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec,
		qty_total )
	select est_monthly_id, work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
		sum(january), sum(february), sum(march), sum(april), sum(may), sum(june),
		sum(july), sum(august), sum(september), sum(october), sum(november), sum(december),
		sum(january + february + march + april + may + june + july + august + september + october + november + december),
		sum(hrs_jan), sum(hrs_feb), sum(hrs_mar), sum(hrs_apr), sum(hrs_may), sum(hrs_jun),
		sum(hrs_jul), sum(hrs_aug), sum(hrs_sep), sum(hrs_oct), sum(hrs_nov), sum(hrs_dec),
		sum(hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec),
		sum(qty_jan), sum(qty_feb), sum(qty_mar), sum(qty_apr), sum(qty_may), sum(qty_jun),
		sum(qty_jul), sum(qty_aug), sum(qty_sep), sum(qty_oct), sum(qty_nov), sum(qty_dec),
		sum(qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec)
	from wo_est_slide_results_temp
	group by est_monthly_id, work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id;
	
	if uf_check_sql('Inserting new estimates') <> 1 then return -1
	
	insert into wo_est_monthly_spread (
		est_monthly_id, factor_id, curve_id, rate_type_id )
	select distinct est_monthly_id, factor_id, curve_id, rate_type_id
	from wo_est_slide_results_temp;
	
	if uf_check_sql('Inserting new est monthly ids') <> 1 then return -1
	
end if

i_status_position = 100
uf_msg('Done!','I')
f_progressbar('Title', 'close(w_progressbar)', 100, -1)
//if isvalid(w_progressbar) then
//	close(w_progressbar)
//end if

return 1

end function

public function longlong uf_justification_new (longlong a_tab, longlong a_doc_stage, longlong a_funding_wo_ind);string sqls, func_name, wo_fp, str_arr[], args[]
longlong cst_rtn, cst_rtn2, cst_rtn3, long_arr[], rtn

func_name = "uf_justification_new"

if a_funding_wo_ind = 2 then
	wo_fp = 'bi'
elseif a_funding_wo_ind = 1 then
	wo_fp = 'fp'
elseif a_funding_wo_ind = 0 then
	wo_fp = 'wo'
end if

long_arr[1] = a_tab
long_arr[2] = a_doc_stage

uf_msg('Inserting new justification header','I')

sqls = &
"insert into wo_documentation (work_order_id, document_id, document_stage_id, " +&
"	document_status_id, document_notes, est_revision, tab_indicator) " +&
"select temp.work_order_id, a.next_doc_id, "+string(a_doc_stage)+", " +&
"	1, '<No notes exist for this document>', decode(tabs.doc_tied_to_revision, 1, b.max_revision, 0), "+string(a_tab)+" " +&
"from wo_est_processing_temp temp, "
if a_funding_wo_ind <> 2 then
	sqls += &
	"	( " +&
	"		select t.work_order_id, woc.company_id " +&
	"		from wo_est_processing_temp t, work_order_control woc " +&
	"		where t.work_order_id = woc.work_order_id " +&
	"	) woc, "
else
	sqls += &
	"	( " +&
	"		select t.work_order_id, woc.company_id " +&
	"		from wo_est_processing_temp t, budget woc " +&
	"		where t.work_order_id = woc.budget_id " +&
	"	) woc, "
end if
sqls += &
"	wo_doc_justification_tabs tabs, " +&
"	( " +&
"		select t.work_order_id, nvl(max(w.document_id), 0) + 1 next_doc_id " +&
"		from wo_est_processing_temp t, wo_documentation w " +&
"		where t.work_order_id = w.work_order_id (+) " +&
"		and "+string(a_tab)+" = w.tab_indicator (+) " +&
"		group by t.work_order_id " +&
"	) a, ( "
if a_funding_wo_ind <> 2 then
	sqls += &
	"	select t.work_order_id, nvl(max(w.revision),0) max_revision " +&
	"	from wo_est_processing_temp t, wo_est_initial_revision w " +&
	"	where t.work_order_id = w.work_order_id (+) " +&
	"	group by t.work_order_id "
else
	sqls += &
	"	select t.work_order_id, nvl(max(w.budget_version_id),0) max_revision " +&
	"	from wo_est_processing_temp t, bi_est_initial_version w " +&
	"	where t.work_order_id = w.budget_id " +&
	"	group by t.work_order_id "
end if
sqls += &
"	) b " +&
"where temp.work_order_id = woc.work_order_id " +&
"and tabs.tab_indicator = "+string(a_tab)+" " +&
"and temp.work_order_id = a.work_order_id " +&
"and temp.work_order_id = b.work_order_id "

execute immediate :sqls;

if uf_check_sql('Inserting into wo_documentation') <> 1 then return -1

// Insert new justification detail
rtn = uf_justification_new_detail(a_tab, a_doc_stage, a_funding_wo_ind)

if rtn = -1 then
	return -1
end if

//
// Call to custom function after processing new justifications
//
cst_rtn = f_budget_revisions_custom(func_name,1,wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
str_arr[1] = ' '
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,1,wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '1'
args[2] = wo_fp
args[3] = string(cst_rtn3)
args[4] = string(long_arr[1])
args[5] = string(long_arr[2])
cst_rtn3 = f_wo_validation_control(87,args)
if cst_rtn3 < 0 then 
   return -1
end if

return 1
 
end function

public function longlong uf_delete_revision ();delete from wo_estimate_class_code c
where exists(
	select 1 from wo_estimate e, wo_est_processing_temp t
	where e.estimate_id = c.estimate_id
	and e.work_order_id = t.work_order_id
	and e.revision = t.revision
	);

if uf_check_sql('Deleting estimate class codes') <> 1 then return -1

delete from wo_estimate w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting unit estimates') <> 1 then return -1

delete from wo_est_monthly_spread c
where exists (
	select 1 from wo_est_monthly  w, wo_est_processing_temp t
	where w.est_monthly_id = c.est_monthly_id
	and w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting monthly spreads') <> 1 then return -1

delete from wo_est_monthly_escalation esc
where exists (
	select 1 from wo_est_monthly wem, wo_est_processing_temp t
	where wem.work_order_id = t.work_order_id
	and wem.revision = t.revision
	and wem.est_monthly_id = esc.est_monthly_id
	and wem.year = esc.year
	);

if uf_check_sql('Deleting monthly escalations') <> 1 then return -1

delete from wo_est_monthly w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting monthly estimates') <> 1 then return -1

delete from wo_est_supplemental_data w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting monthly supplemental estimates') <> 1 then return -1

delete from wo_approval_multiple w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting multiple approvers') <> 1 then return -1

delete from work_order_approval w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting revision') <> 1 then return -1

delete from budget_review w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting budget reviews') <> 1 then return -1

delete from budget_review_detail w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting budget review details') <> 1 then return -1

delete from budget_version_fund_proj w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting budget version relationships') <> 1 then return -1

delete from budget_substitutions w
where exists (
	select 1 from wo_est_processing_temp t
	where w.to_work_order_id = t.work_order_id
	and w.to_revision = t.revision
	);

if uf_check_sql('Deleting budget substitutions (1)') <> 1 then return -1

delete from budget_substitutions w
where exists (
	select 1 from wo_est_processing_temp t
	where w.from_work_order_id = t.work_order_id
	and w.from_revision = t.revision
	);

if uf_check_sql('Deleting budget substitutions (2)') <> 1 then return -1

delete from budget_afudc_calc w
where exists (
	select 1 from wo_est_processing_temp t
	where w.work_order_id = t.work_order_id
	and w.revision = t.revision
	);

if uf_check_sql('Deleting budget afudc calculations') <> 1 then return -1

update wo_documentation d
set d.est_revision = 0
where exists (
	select 1 from wo_est_processing_temp t
	where t.work_order_id = d.work_order_id
	and t.revision = d.est_revision
	);

if uf_check_sql('Disassociating justification documents') <> 1 then return -1

delete from wo_est_derivation_pct p
where exists (
	select 1 from wo_est_processing_temp t
	where t.work_order_id = p.work_order_id
	and t.revision = p.revision
	);

if uf_check_sql('Deleting monthly estimate derivation percentages') <> 1 then return -1

return 1
end function

public function longlong uf_fy_temp_to_wem (longlong funding_wo_ind, boolean fiscal_year);string sqls
string table_name, est_mo_id, wo_id, rev, util, wo_approval, start_date, end_date, year

if funding_wo_ind = 2 then
	if i_analyze then
		sqlca.analyze_table('budget_amounts')
	end if
	
	table_name = 'budget_monthly_data'
	est_mo_id = 'budget_monthly_id'
	wo_id = 'budget_id'
	rev = 'budget_version_id'
	util = 'budget_plant_class_id'
	wo_approval = 'budget_amounts'
	start_date = 'start_date'
	end_date = 'close_date'
else
	if i_analyze then
		sqlca.analyze_table('work_order_approval')
	end if
	
	table_name = 'wo_est_monthly'
	est_mo_id = 'est_monthly_id'
	wo_id = 'work_order_id'
	rev = 'revision'
	util = 'utility_account_id'
	wo_approval = 'work_order_approval'
	start_date = 'est_start_date'
	end_date = 'est_complete_date'
end if
if fiscal_year then
	year = 'p.year'
else
	year = 't.year'
end if
if i_analyze then
	sqlca.analyze_table('pp_calendar')
	sqlca.analyze_table('wo_est_monthly_fy_temp')
end if

//
// Delete records being replaced
//
sqls = &
"delete from "+table_name+" w " +&
"where exists ( " +&
"	select 1 from wo_est_monthly_fy_temp t " +&
"	where t.est_monthly_id = w."+est_mo_id+" " +&
"	and t.updated = 1 " +&
"	) "

sqls = f_sql_add_hint(sqls, 'uf_fy_temp_to_wem_20')

execute immediate :sqls;

if uf_check_sql('Deleting from wo_est_monthly (uf_fy_temp_to_wem)') <> 1 then return -1


//
// Insert records being replaced
//
sqls = &
"insert into "+table_name+" ( " +&
"	"+est_mo_id+", "+wo_id+", "+rev+", year, " +&
"	expenditure_type_id, est_chg_type_id, department_id, "+util+", job_task_id, long_description, " +&
"	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, "
if funding_wo_ind = 2 then
	sqls += &
	"work_order_id, "
else
	sqls += &
	"wo_work_order_id, substitution_id, budget_id, " // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
end if
sqls += &
"	future_dollars, hist_actuals, " +&
"	january, february, march, april, may, june, july, august, september, october, november, december, total "
if funding_wo_ind <> 2 then
	sqls += &
	"	, hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total, " +&
	"	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total "
end if
sqls += &
"	) " +&
"select t.est_monthly_id, t.work_order_id, t.revision, "+year+", " +&
"	t.expenditure_type_id, t.est_chg_type_id, t.department_id, t.utility_account_id, t.job_task_id, t.long_description, " +&
"	t.attribute01_id, t.attribute02_id, t.attribute03_id, t.attribute04_id, t.attribute05_id, t.attribute06_id, t.attribute07_id, t.attribute08_id, t.attribute09_id, t.attribute10_id, "
if funding_wo_ind = 2 then
	sqls += &
	"t.wo_work_order_id, "
else
	sqls += &
	"t.wo_work_order_id, max(t.substitution_id), t.budget_id, " // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
end if
sqls += &
"	nvl(t.future_dollars,0), nvl(t.hist_actuals,0), "
if fiscal_year then
	sqls += &
	"		sum(nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) january, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) february, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) march, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) april, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) may, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) june, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) july, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) august, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) september, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) october, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) november, " +&
	"		sum(nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0)) december, " +&
	"		sum( " +&
	"			nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) + " +&
	"			nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,t.january,2,t.february,3,t.march,4,t.april,5,t.may,6,t.june,7,t.july,8,t.august,9,t.september,10,t.october,11,t.november,12,t.december),0),0) " +&
	"			) total "
	if funding_wo_ind <> 2 then
		sqls += &
		"		, sum(nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_jan, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_feb, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_mar, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_apr, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_may, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_jun, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_jul, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_aug, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_sep, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_oct, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_nov, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0)) hrs_dec, " +&
		"		sum( " +&
		"			nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,t.hrs_jan,2,t.hrs_feb,3,t.hrs_mar,4,t.hrs_apr,5,t.hrs_may,6,t.hrs_jun,7,t.hrs_jul,8,t.hrs_aug,9,t.hrs_sep,10,t.hrs_oct,11,t.hrs_nov,12,t.hrs_dec),0),0) " +&
		"			) hrs_total, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_jan, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_feb, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_mar, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_apr, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_may, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_jun, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_jul, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_aug, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_sep, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_oct, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_nov, " +&
		"		sum(nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0)) qty_dec, " +&
		"		sum( " +&
		"			nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) + " +&
		"			nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,t.qty_jan,2,t.qty_feb,3,t.qty_mar,4,t.qty_apr,5,t.qty_may,6,t.qty_jun,7,t.qty_jul,8,t.qty_aug,9,t.qty_sep,10,t.qty_oct,11,t.qty_nov,12,t.qty_dec),0),0) " +&
		"			) qty_total "
	end if
else
	sqls += &
	"		sum(nvl(t.january,0)) january, " +&
	"		sum(nvl(t.february,0)) february, " +&
	"		sum(nvl(t.march,0)) march, " +&
	"		sum(nvl(t.april,0)) april, " +&
	"		sum(nvl(t.may,0)) may, " +&
	"		sum(nvl(t.june,0)) june, " +&
	"		sum(nvl(t.july,0)) july, " +&
	"		sum(nvl(t.august,0)) august, " +&
	"		sum(nvl(t.september,0)) september, " +&
	"		sum(nvl(t.october,0)) october, " +&
	"		sum(nvl(t.november,0)) november, " +&
	"		sum(nvl(t.december,0)) december, " +&
	"		sum( " +&
	"			nvl(t.january,0) + " +&
	"			nvl(t.february,0) + " +&
	"			nvl(t.march,0) + " +&
	"			nvl(t.april,0) + " +&
	"			nvl(t.may,0) + " +&
	"			nvl(t.june,0) + " +&
	"			nvl(t.july,0) + " +&
	"			nvl(t.august,0) + " +&
	"			nvl(t.september,0) + " +&
	"			nvl(t.october,0) + " +&
	"			nvl(t.november,0) + " +&
	"			nvl(t.december,0) " +&
	"			) total "
	if funding_wo_ind <> 2 then
		sqls += &
		"		, sum(nvl(t.hrs_jan,0)) hrs_jan, " +&
		"		sum(nvl(t.hrs_feb,0)) hrs_feb, " +&
		"		sum(nvl(t.hrs_mar,0)) hrs_mar, " +&
		"		sum(nvl(t.hrs_apr,0)) hrs_apr, " +&
		"		sum(nvl(t.hrs_may,0)) hrs_may, " +&
		"		sum(nvl(t.hrs_jun,0)) hrs_jun, " +&
		"		sum(nvl(t.hrs_jul,0)) hrs_jul, " +&
		"		sum(nvl(t.hrs_aug,0)) hrs_aug, " +&
		"		sum(nvl(t.hrs_sep,0)) hrs_sep, " +&
		"		sum(nvl(t.hrs_oct,0)) hrs_oct, " +&
		"		sum(nvl(t.hrs_nov,0)) hrs_nov, " +&
		"		sum(nvl(t.hrs_dec,0)) hrs_dec, " +&
		"		sum( " +&
		"			nvl(t.hrs_jan,0) + " +&
		"			nvl(t.hrs_feb,0) + " +&
		"			nvl(t.hrs_mar,0) + " +&
		"			nvl(t.hrs_apr,0) + " +&
		"			nvl(t.hrs_may,0) + " +&
		"			nvl(t.hrs_jun,0) + " +&
		"			nvl(t.hrs_jul,0) + " +&
		"			nvl(t.hrs_aug,0) + " +&
		"			nvl(t.hrs_sep,0) + " +&
		"			nvl(t.hrs_oct,0) + " +&
		"			nvl(t.hrs_nov,0) + " +&
		"			nvl(t.hrs_dec,0) " +&
		"			) hrs_total, " +&
		"		sum(nvl(t.qty_jan,0)) qty_jan, " +&
		"		sum(nvl(t.qty_feb,0)) qty_feb, " +&
		"		sum(nvl(t.qty_mar,0)) qty_mar, " +&
		"		sum(nvl(t.qty_apr,0)) qty_apr, " +&
		"		sum(nvl(t.qty_may,0)) qty_may, " +&
		"		sum(nvl(t.qty_jun,0)) qty_jun, " +&
		"		sum(nvl(t.qty_jul,0)) qty_jul, " +&
		"		sum(nvl(t.qty_aug,0)) qty_aug, " +&
		"		sum(nvl(t.qty_sep,0)) qty_sep, " +&
		"		sum(nvl(t.qty_oct,0)) qty_oct, " +&
		"		sum(nvl(t.qty_nov,0)) qty_nov, " +&
		"		sum(nvl(t.qty_dec,0)) qty_dec, " +&
		"		sum( " +&
		"			nvl(t.qty_jan,0) + " +&
		"			nvl(t.qty_feb,0) + " +&
		"			nvl(t.qty_mar,0) + " +&
		"			nvl(t.qty_apr,0) + " +&
		"			nvl(t.qty_may,0) + " +&
		"			nvl(t.qty_jun,0) + " +&
		"			nvl(t.qty_jul,0) + " +&
		"			nvl(t.qty_aug,0) + " +&
		"			nvl(t.qty_sep,0) + " +&
		"			nvl(t.qty_oct,0) + " +&
		"			nvl(t.qty_nov,0) + " +&
		"			nvl(t.qty_dec,0) " +&
		"			) qty_total "
	end if
end if
if fiscal_year then
	sqls += &
	"from pp_calendar p, wo_est_monthly_fy_temp t, "+wo_approval+" a " +&
	"where p.fiscal_year = t.year " +&
	"and t.work_order_id = a."+wo_id+" " +&
	"and t.revision = a."+rev+" "
else
	sqls += &
	"from wo_est_monthly_fy_temp t, "+wo_approval+" a " +&
	"where t.work_order_id = a."+wo_id+" " +&
	"and t.revision = a."+rev+" "
end if
sqls += &
"and to_char("+year+") between nvl(to_char(a."+start_date+",'yyyy'),'0000') and nvl(to_char(a."+end_date+",'yyyy'),'9999') " +&
"and exists ( " +&
"	select 1 from wo_est_monthly_fy_temp t2 " +&
"	where t2.est_monthly_id = t.est_monthly_id " +&
"	and t2.updated = 1 " +&
"	) " +&
"group by t.est_monthly_id, t.work_order_id, t.revision, "+year+", " +&
"	t.expenditure_type_id, t.est_chg_type_id, t.department_id, t.utility_account_id, t.job_task_id, t.long_description, " +&
"	t.attribute01_id, t.attribute02_id, t.attribute03_id, t.attribute04_id, t.attribute05_id, t.attribute06_id, t.attribute07_id, t.attribute08_id, t.attribute09_id, t.attribute10_id, " +&
"	nvl(t.future_dollars,0), nvl(t.hist_actuals,0), "
if funding_wo_ind = 2 then
	sqls += &
	"t.wo_work_order_id "
else
	sqls += &
	"t.wo_work_order_id, t.budget_id " // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
end if

sqls = f_sql_add_hint(sqls, 'uf_fy_temp_to_wem_30')

execute immediate :sqls;

if uf_check_sql('Inserting into wo_est_monthly (uf_fy_temp_to_wem)') <> 1 then return -1


//
// Reset updated flag to null
//
update wo_est_monthly_fy_temp
set updated = null;

if uf_check_sql('Updating "updated" flag to null (uf_fy_temp_to_wem)') <> 1 then return -1


return 1
end function

public function longlong uf_slide (datetime a_old_start, datetime a_old_end, datetime a_new_start, datetime a_new_end, datetime a_new_est_start, datetime a_new_est_complete, datetime a_new_est_in_serv);//------------------------------------------------------------------------
//
// FUNCTION:		uf_slide
//
// DESCRIPTION:	Stretch/Slide estimated dates, and adjust 
//						dollars/hrs/qty accordingly
//
//	ASSUMPTIONS:	1 - ONLY ONE REVISION CAN SLIDE AT A TIME
//						2 - work_order_id/revision must be inserted into
//								wo_est_processing_temp
//						3 - uf_slide_commit must be called separately to save
//								changes to wo_est_monthly
//
//	ARGUMENTS:	a_old_start -				datetime -	Old start of the span of time to slide
//						a_old_end -					datetime -	Old end of the span of time to slide
//						a_new_start -				datetime -	New start of the span of time to slide
//						a_new_end -				datetime -	New end of the span of time to slide
//						a_new_est_start -			datetime -	New estimate start date
//						a_new_est_complete -	datetime -	New estimate complete date
//						a_new_est_in_serv -		datetime -	New estimate in service date
//
// RETURN CODES:	1 - SUCCESS
//						2 - SUCCESS - DATES CHANGED TO ACCOUNT FOR ACTUALS MONTHS
//					  -1 - ERROR
//
//------------------------------------------------------------------------
longlong old_span, check_revs, total_segments, new_start_mn, new_end_mn, &
	rtn, check_act, act_month_start, act_month_end, actuals_month, funding_wo_ind, &
	slide_start, slide_end, old_start_mn, old_end_mn, wo_id, rev, cnt
string ret, ret_app


//
// Set variables
//
old_start_mn = long(string(a_old_start, 'yyyymm'))
old_end_mn = long(string(a_old_end, 'yyyymm'))
new_start_mn = long(string(a_new_start, 'yyyymm'))
new_end_mn = long(string(a_new_end, 'yyyymm'))


//
// Ensure estimated dates are valid months
//
if new_start_mn < 190001 or new_start_mn > 299912 &
or new_end_mn < 190001 or new_end_mn > 299912 &
or long(string(a_new_est_start, 'yyyymm')) > 299912 or long(string(a_new_est_start, 'yyyymm')) > 299912 &
or long(string(a_new_est_complete, 'yyyymm')) > 299912 or long(string(a_new_est_complete, 'yyyymm')) > 299912 &
or long(string(a_new_est_in_serv, 'yyyymm')) > 299912 or long(string(a_new_est_in_serv, 'yyyymm')) > 299912 &
then
	uf_msg('Invalid date range. Please ensure the Est Start Date and Est Complete Date '+&
		'have valid months and years.','E')
	rollback;
	return -1
end if


//
// Ensure the date ranges make sense
//
if a_new_est_start <= a_new_est_in_serv and a_new_est_in_serv <= a_new_est_complete then
	// This is okay
else
	uf_msg('Invalid date range. Please ensure the Est Complete Date is on or after the '+&
		'Est Start Date and that the Est In Service Date falls in between the two.','E')
	rollback;
	return -1
end if


//
// Ensure only 1 revision is sliding
//
check_revs = 0
select count(*)
into :check_revs
from wo_est_processing_temp;

if check_revs > 1 then
	uf_msg('Too many revisions. Only one revision can slide at a time.','E')
	rollback;
	return -1
end if


//
// Ensure the work order and revision are valid
//
check_revs = 0
actuals_month = 0
funding_wo_ind = 0
select count(*), nvl(max(a.actuals_month_number),0), nvl(max(b.funding_wo_indicator),0)
into :check_revs, :actuals_month, :funding_wo_ind
from work_order_approval a, work_order_control b
where (a.work_order_id, a.revision) in (
	select work_order_id, revision 
	from wo_est_processing_temp
	)
and a.work_order_id = b.work_order_id;

if check_revs <> 1 then
	uf_msg('Invalid from/to revisions. Please ensure revisions exist prior to sliding.','E')
	rollback;
	return -1
end if


//
// Check to make sure revision is editable
//
select work_order_id, revision
into :wo_id, :rev
from wo_est_processing_temp;
ret_app = f_wo_est_revision_is_protected(wo_id, "selected", rev)

if ret_app <> "" then
	uf_msg('Cannot slide the project because '+ret_app+'.','E')
	rollback;
	return -1
end if


//
// Set the months that are eligible to slide (start month through end month)
//
slide_start = new_start_mn
slide_end = new_end_mn


//
// Transpose the estimates
//
rtn = uf_transpose('dollar')

if rtn = -1 then
	return -1
end if

//
// Check for non-zero actuals amounts
//   If none exist then actuals month is meaningless
//
select count(*) into :cnt
from wo_est_transpose_temp
where month_number <= :actuals_month
and amount <> 0;

if cnt = 0 then
	actuals_month = 190001
end if


//
// We need to account for actuals months
//   Actuals month < Start month
//
if actuals_month > 190000 and actuals_month < old_start_mn then
	
	//
	// Clear out the actuals_month_number. It is meaningless if is less than the est start month
	//
	update work_order_approval a
	set actuals_month_number = null
	where exists (
		select 1 from wo_est_processing_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		);
	
	if uf_check_sql('Clearing out actuals month number on work_order_approval') <> 1 then return -1
	
	commit;
	
	actuals_month = 0
	
	//
	// Set the slide months to the New months that are eligible to slide (non-actuals months)
	//
	slide_start = new_start_mn
	slide_end = new_end_mn
	
	
//
// We need to account for actuals months
//   Actuals month between Start month and End month (not including End month)
//
elseif actuals_month > 190000 and actuals_month >= old_start_mn and actuals_month < old_end_mn then
	
	//
	// Ensure the new estimated start date encapsulates all actuals months
	//   New start month cannot fall after any actuals months, so we don't lose that data
	//
	if new_start_mn > old_start_mn then
		uf_msg('New Start Month falls after months with actuals. Cannot proceed.','E')
		rollback;
		return -1
	end if
	
	//
	// Ensure the new estimated complete date encapsulates all actuals months
	//   New end month cannot fall on or before Actuals month, so we don't lose that data.
	//
	if new_end_mn <= actuals_month then
		uf_msg('New Complete Month falls on or before months with actuals. Cannot proceed.','E')
		rollback;
		return -1
	end if
	
	//
	// Set the slide months to the New months that are eligible to slide (non-actuals months)
	//
	if right(string(actuals_month),2) = '12' then
		slide_start = actuals_month + 89
	else
		slide_start = actuals_month + 1
	end if
	slide_end = new_end_mn
	
	
//
// We need to account for actuals months
//   Actuals month >= End month
//
elseif actuals_month > 190000 and actuals_month >= old_end_mn then
	
	//
	// Set the actuals month number to the old estimated complete date, so if we expand the
	//   complete date out further in the logic below, then we want those new zero-dollar
	//   months to be reflected as estimates, not actuals.
	//
	if actuals_month > old_end_mn then
		update work_order_approval a
		set actuals_month_number = to_number(to_char(est_complete_date,'yyyymm'))
		where exists (
			select 1 from wo_est_processing_temp b
			where b.work_order_id = a.work_order_id
			and b.revision = a.revision
			);
		
		if uf_check_sql('Updating actuals month number on work_order_approval') <> 1 then return -1
		
		commit;
		
		actuals_month = old_end_mn
	end if
	
	//
	// All Old months are actuals, so ensure the New start month encapsulates them
	//
	if new_start_mn > old_start_mn then
		uf_msg('New Start Month falls after months with actuals. Cannot proceed.','E')
		rollback;
		return -1
	end if
	
	//
	// All Old months are actuals, so ensure the New end month encapsulates them
	//
	if new_end_mn < old_end_mn then
		uf_msg('New Complete Month falls before months with actuals. Cannot proceed.','E')
		rollback;
		return -1
	end if
	
	//
	// Set the slide months to the New months that are eligible to slide (non-actuals months)
	//
	if right(string(actuals_month),2) = '12' then
		slide_start = actuals_month + 89
		slide_end = actuals_month + 89
	else
		slide_start = actuals_month + 1
		slide_end = actuals_month + 1
	end if
	
end if


// CDM - move this up so we can use it to check the actuals month
////
//// Transpose the estimates
////
//rtn = uf_transpose('dollar')
//
//if rtn = -1 then
//	return -1
//end if

//
// If we are sliding just the dollar range (not the entire date range) then remove any estimates (presumably zeros) outside of this range
//
delete from wo_est_transpose_temp
where month_number > :old_end_mn or month_number < :old_start_mn;

if uf_check_sql('Deleting unnecessary months from wo_est_transpose_temp') <> 1 then return -1


//
// Get the span of time of the old estimated dates for the calc below
//
old_span = 0
//
// Actuals month may be valid or it may be zero (null). Use the greater of the 
//   "Est Start Date" and the "Actuals Month + 1" as the beginning of the span
//
select
	months_between(
		trunc(:a_old_end,'month'),
		greatest(
			trunc(:a_old_start,'month'),
			add_months(to_date(to_char(decode(nvl(actuals_month_number,0),0,180001,actuals_month_number)),'yyyymm'),1)
			)
		) + 1
into :old_span
from work_order_approval
where (work_order_id,revision) in (
	select work_order_id, revision 
	from wo_est_processing_temp
	);

if isnull(old_span) or old_span < 0 then
	uf_msg('Invalid date range. Please ensure the original Start Month and Complete Month are valid.','E')
	rollback;
	return -1
elseif old_span > 1200 /*100 years*/ then
	uf_msg('Invalid date range. Please ensure the original Start Month and Complete Month are valid.','E')
	rollback;
	return -1
end if


//
// Slide the estimates
//   Only slide estimates that are after the actuals month (beginning with month slide_start)
//   Months on or before actuals month (less than slide_start) should be copied as-is
//
delete from wo_est_slide_results_temp;

insert into wo_est_slide_results_temp (
	est_monthly_id, work_order_id, revision, year,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	factor_id, curve_id, rate_type_id, substitution_id,
	hist_actuals, future_dollars,
	january, february, march, april, may, june, july, august, september, october, november, december,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec,
	est_start_date,
	est_complete_date,
	est_in_service_date
	)
select est_monthly_id, work_order_id, revision, to_number(substr(month_number,1,4)) year,
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	factor_id, curve_id, rate_type_id, substitution_id,
	hist_actuals, future_dollars,
	round(sum(decode(substr(month_number,5,2),'01',amount,0)),2) january,
	round(sum(decode(substr(month_number,5,2),'02',amount,0)),2) february,
	round(sum(decode(substr(month_number,5,2),'03',amount,0)),2) march,
	round(sum(decode(substr(month_number,5,2),'04',amount,0)),2) april,
	round(sum(decode(substr(month_number,5,2),'05',amount,0)),2) may,
	round(sum(decode(substr(month_number,5,2),'06',amount,0)),2) june,
	round(sum(decode(substr(month_number,5,2),'07',amount,0)),2) july,
	round(sum(decode(substr(month_number,5,2),'08',amount,0)),2) august,
	round(sum(decode(substr(month_number,5,2),'09',amount,0)),2) september,
	round(sum(decode(substr(month_number,5,2),'10',amount,0)),2) october,
	round(sum(decode(substr(month_number,5,2),'11',amount,0)),2) november,
	round(sum(decode(substr(month_number,5,2),'12',amount,0)),2) december,
	round(sum(decode(substr(month_number,5,2),'01',hours,0)),4) hrs_jan,
	round(sum(decode(substr(month_number,5,2),'02',hours,0)),4) hrs_feb,
	round(sum(decode(substr(month_number,5,2),'03',hours,0)),4) hrs_mar,
	round(sum(decode(substr(month_number,5,2),'04',hours,0)),4) hrs_apr,
	round(sum(decode(substr(month_number,5,2),'05',hours,0)),4) hrs_may,
	round(sum(decode(substr(month_number,5,2),'06',hours,0)),4) hrs_jun,
	round(sum(decode(substr(month_number,5,2),'07',hours,0)),4) hrs_jul,
	round(sum(decode(substr(month_number,5,2),'08',hours,0)),4) hrs_aug,
	round(sum(decode(substr(month_number,5,2),'09',hours,0)),4) hrs_sep,
	round(sum(decode(substr(month_number,5,2),'10',hours,0)),4) hrs_oct,
	round(sum(decode(substr(month_number,5,2),'11',hours,0)),4) hrs_nov,
	round(sum(decode(substr(month_number,5,2),'12',hours,0)),4) hrs_dec,
	round(sum(decode(substr(month_number,5,2),'01',quantity,0)),4) qty_jan,
	round(sum(decode(substr(month_number,5,2),'02',quantity,0)),4) qty_feb,
	round(sum(decode(substr(month_number,5,2),'03',quantity,0)),4) qty_mar,
	round(sum(decode(substr(month_number,5,2),'04',quantity,0)),4) qty_apr,
	round(sum(decode(substr(month_number,5,2),'05',quantity,0)),4) qty_may,
	round(sum(decode(substr(month_number,5,2),'06',quantity,0)),4) qty_jun,
	round(sum(decode(substr(month_number,5,2),'07',quantity,0)),4) qty_jul,
	round(sum(decode(substr(month_number,5,2),'08',quantity,0)),4) qty_aug,
	round(sum(decode(substr(month_number,5,2),'09',quantity,0)),4) qty_sep,
	round(sum(decode(substr(month_number,5,2),'10',quantity,0)),4) qty_oct,
	round(sum(decode(substr(month_number,5,2),'11',quantity,0)),4) qty_nov,
	round(sum(decode(substr(month_number,5,2),'12',quantity,0)),4) qty_dec,
	:a_new_est_start,
	:a_new_est_complete,
	:a_new_est_in_serv
from (
	select est_monthly_id, work_order_id, revision, month_number,
			expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
			attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
			factor_id, curve_id, rate_type_id, substitution_id,
			hist_actuals, future_dollars,
			amount, hours, quantity,
			0 partition
	from wo_est_transpose_temp
	where month_number <=/*<*/ :actuals_month /*slide_start*/
	union all
	select est_monthly_id, work_order_id, revision, month_number,
			expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
			attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
			factor_id, curve_id, rate_type_id, substitution_id,
			hist_actuals, future_dollars,
			sum(amount) amount, sum(hours) hours, sum(quantity) quantity,
			partition
	from (
		select est_monthly_id, work_order_id, revision, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
				expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
				attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
				factor_id, curve_id, rate_type_id, substitution_id,
				hist_actuals, future_dollars,
				amount, hours, quantity,
				trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
		from (
			select est_monthly_id, work_order_id, revision, month_number, 
				expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
				attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
				factor_id, curve_id, rate_type_id, substitution_id,
				hist_actuals, future_dollars,
				amount/multiplier amount, hours/multiplier hours, quantity/multiplier quantity
			from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
				(	select max(rownum) over() multiplier
					from (
						select rownum
						from pp_table_months, pp_table_years
						where year * 100 + month_num between :slide_start and :slide_end
						)
				)
			order by est_monthly_id, month_number
			) a
		) a
	group by est_monthly_id, work_order_id, revision, month_number,
		expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
		factor_id, curve_id, rate_type_id, substitution_id,
		hist_actuals, future_dollars,
		partition
	//order by est_monthly_id, partition, month_number
	union all
	select est_monthly_id, work_order_id, revision, b.month_number,
			expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
			attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
			factor_id, curve_id, rate_type_id, substitution_id,
			hist_actuals, future_dollars,
			0 amount, 0 hours, 0 quantity,
			0 partition
	from
		(	select distinct est_monthly_id, work_order_id, revision, /*month_number,*/ // ### CDM - Maint 7132 - do not include month_number in distinct
				expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
				attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
				factor_id, curve_id, rate_type_id, substitution_id,
				hist_actuals, future_dollars
			from wo_est_transpose_temp
		) a,
		(	select year * 100 + month_num month_number
			from pp_table_months, pp_table_years
			where year * 100 + month_num between :new_start_mn and :new_end_mn
		) b
	)
group by est_monthly_id, work_order_id, revision, to_number(substr(month_number,1,4)),
	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	factor_id, curve_id, rate_type_id, substitution_id,
	hist_actuals, future_dollars
;

if uf_check_sql('Inserting into wo_est_slide_results_temp') <> 1 then return -1


//
// Backfill totals to use in the rounding sql below
//
update wo_est_slide_results_temp a
set total = january + february + march + april + may + june + july + august + september + october + november + december,
	hrs_total = hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec,
	qty_total = qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec;

if uf_check_sql('Updating totals (1) wo_est_slide_results_temp') <> 1 then return -1


//
// Rounding (DOLLARS) - throw differences in the last month with non-zero values for each estimate
//
delete from wo_est_slide_adjust_temp;

insert into wo_est_slide_adjust_temp (est_monthly_id, last_month, adjustment, hrs_adj, qty_adj)
select adj.est_monthly_id, month.last_month, adj.adjustment, adj.hrs_adj, adj.qty_adj
from
	(	select new.est_monthly_id, old_total - new_total adjustment,
			old_hrs_total - new_hrs_total hrs_adj, old_qty_total - new_qty_total qty_adj
		from
			(	select est_monthly_id, sum(old.total) old_total,
					sum(old.hrs_total) old_hrs_total, sum(old.qty_total) old_qty_total
				from wo_est_monthly_deescalation_vw old
				where exists (
					select 1 from wo_est_slide_results_temp a where a.est_monthly_id = old.est_monthly_id)
				group by est_monthly_id
			) old,
			(	select est_monthly_id, sum(new.total) new_total,
					sum(new.hrs_total) new_hrs_total, sum(new.qty_total) new_qty_total
				from wo_est_slide_results_temp new
				group by new.est_monthly_id
			) new
		where old.est_monthly_id = new.est_monthly_id
	) adj,
	(	select est_monthly_id, max(month_number) last_month from (
			select est_monthly_id, month_number, amount, 0 partition
			from wo_est_transpose_temp
			where month_number <=/*<*/ :actuals_month /*slide_start*/
			union all
			select est_monthly_id, month_number, sum(amount) amount, partition from (
				select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
						amount, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
				from (
					select est_monthly_id, month_number, amount/multiplier amount
					from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
						(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
					order by est_monthly_id, month_number
					) a
				) a
			group by est_monthly_id, month_number, partition
			order by est_monthly_id, partition, month_number
			)
		where amount <> 0 // max month where amount is not zero
		group by est_monthly_id
	) month
where adj.est_monthly_id = month.est_monthly_id;

if uf_check_sql('Inserting rounding differences (dollars) in wo_est_slide_adjust_temp') <> 1 then return -1


update wo_est_slide_results_temp t
set (january,february,march,april,may,june,july,august,september,october,november,december) = (
	select decode(to_char(month.last_month), t.year||'01', t.january + month.adjustment, t.january) jan,
			decode(to_char(month.last_month), t.year||'02', t.february + month.adjustment, t.february) feb,
			decode(to_char(month.last_month), t.year||'03', t.march + month.adjustment, t.march) mar,
			decode(to_char(month.last_month), t.year||'04', t.april + month.adjustment, t.april) apr,
			decode(to_char(month.last_month), t.year||'05', t.may + month.adjustment, t.may) may,
			decode(to_char(month.last_month), t.year||'06', t.june + month.adjustment, t.june) jun,
			decode(to_char(month.last_month), t.year||'07', t.july + month.adjustment, t.july) jul,
			decode(to_char(month.last_month), t.year||'08', t.august + month.adjustment, t.august) aug,
			decode(to_char(month.last_month), t.year||'09', t.september + month.adjustment, t.september) sep,
			decode(to_char(month.last_month), t.year||'10', t.october + month.adjustment, t.october) oct,
			decode(to_char(month.last_month), t.year||'11', t.november + month.adjustment, t.november) nov,
			decode(to_char(month.last_month), t.year||'12', t.december + month.adjustment, t.december) dec
	from wo_est_slide_adjust_temp month
	where month.est_monthly_id = t.est_monthly_id
	and to_number(substr(to_char(month.last_month),1,4)) = t.year
	)
where (t.est_monthly_id, t.year) in (
	select est_monthly_id, to_number(substr(to_char(last_month),1,4))
	from wo_est_slide_adjust_temp
	);

if uf_check_sql('Updating rounding differences (dollars) in wo_est_slide_results_temp') <> 1 then return -1


//update wo_est_slide_results_temp a
//set (january,february,march,april,may,june,july,august,september,october,november,december) = (
//					select decode(to_char(month.last_month), t.year||'01', t.january + adjustment, t.january) jan,
//							decode(to_char(month.last_month), t.year||'02', t.february + adjustment, t.february) feb,
//							decode(to_char(month.last_month), t.year||'03', t.march + adjustment, t.march) mar,
//							decode(to_char(month.last_month), t.year||'04', t.april + adjustment, t.april) apr,
//							decode(to_char(month.last_month), t.year||'05', t.may + adjustment, t.may) may,
//							decode(to_char(month.last_month), t.year||'06', t.june + adjustment, t.june) jun,
//							decode(to_char(month.last_month), t.year||'07', t.july + adjustment, t.july) jul,
//							decode(to_char(month.last_month), t.year||'08', t.august + adjustment, t.august) aug,
//							decode(to_char(month.last_month), t.year||'09', t.september + adjustment, t.september) sep,
//							decode(to_char(month.last_month), t.year||'10', t.october + adjustment, t.october) oct,
//							decode(to_char(month.last_month), t.year||'11', t.november + adjustment, t.november) nov,
//							decode(to_char(month.last_month), t.year||'12', t.december + adjustment, t.december) dec
//					from wo_est_slide_results_temp t,
//						(	select new.est_monthly_id, old_total - new_total adjustment,
//								old_hrs_total - new_hrs_total hrs_adj, old_qty_total - new_qty_total qty_adj
//							from
//								(	select est_monthly_id, sum(old.total) old_total,
//										sum(old.hrs_total) old_hrs_total, sum(old.qty_total) old_qty_total
//									from wo_est_monthly_deescalation_vw old
//									where exists (
//										select 1 from wo_est_slide_results_temp a where a.est_monthly_id = old.est_monthly_id)
//									group by est_monthly_id
//								) old,
//								(	select est_monthly_id, sum(new.total) new_total,
//										sum(new.hrs_total) new_hrs_total, sum(new.qty_total) new_qty_total
//									from wo_est_slide_results_temp new
//									group by new.est_monthly_id
//								) new
//							where old.est_monthly_id = new.est_monthly_id
//						) adj,
//						(	select est_monthly_id, max(month_number) last_month from (
//								select est_monthly_id, month_number, amount, 0 partition
//								from wo_est_transpose_temp
//								where month_number <=/*<*/ :actuals_month /*slide_start*/
//								union all
//								select est_monthly_id, month_number, sum(amount) amount, partition from (
//									select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
//											amount, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
//									from (
//										select est_monthly_id, month_number, amount/multiplier amount
//										from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
//											(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
//										order by est_monthly_id, month_number
//										) a
//									) a
//								group by est_monthly_id, month_number, partition
//								order by est_monthly_id, partition, month_number
//								)
//							where amount <> 0 // max month where amount is not zero
//							group by est_monthly_id
//						) month
//					where t.est_monthly_id = adj.est_monthly_id
//					and t.est_monthly_id = month.est_monthly_id
//					and t.est_monthly_id = a.est_monthly_id
//					and t.year = a.year
//					)
//where exists (
//	select 1 from 
//		(	select est_monthly_id, max(month_number) last_month from (
//				select est_monthly_id, month_number, amount, 0 partition
//				from wo_est_transpose_temp
//				where month_number <=/*<*/ :actuals_month /*slide_start*/
//				union all
//				select est_monthly_id, month_number, sum(amount) amount, partition from (
//					select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
//							amount, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
//					from (
//						select est_monthly_id, month_number, amount/multiplier amount
//						from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
//							(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
//						order by est_monthly_id, month_number
//						) a
//					) a
//				group by est_monthly_id, month_number, partition
//				order by est_monthly_id, partition, month_number
//				)
//			where amount <> 0 // max month where amount is not zero
//			group by est_monthly_id
//		) month
//	where a.est_monthly_id = month.est_monthly_id
//	);
//
//if uf_check_sql('Updating rounding differences (dollars) in wo_est_slide_results_temp') <> 1 then return -1


//
// Rounding (HOURS) - throw differences in the last month with non-zero values for each estimate
//
delete from wo_est_slide_adjust_temp;

insert into wo_est_slide_adjust_temp (est_monthly_id, last_month, adjustment, hrs_adj, qty_adj)
select adj.est_monthly_id, month.last_month, adj.adjustment, adj.hrs_adj, adj.qty_adj
from
	(	select new.est_monthly_id, old_total - new_total adjustment,
			old_hrs_total - new_hrs_total hrs_adj, old_qty_total - new_qty_total qty_adj
		from
			(	select est_monthly_id, sum(old.total) old_total,
					sum(old.hrs_total) old_hrs_total, sum(old.qty_total) old_qty_total
				from wo_est_monthly_deescalation_vw old
				where exists (
					select 1 from wo_est_slide_results_temp a where a.est_monthly_id = old.est_monthly_id)
				group by est_monthly_id
			) old,
			(	select est_monthly_id, sum(new.total) new_total,
					sum(new.hrs_total) new_hrs_total, sum(new.qty_total) new_qty_total
				from wo_est_slide_results_temp new
				group by new.est_monthly_id
			) new
		where old.est_monthly_id = new.est_monthly_id
	) adj,
	(	select est_monthly_id, max(month_number) last_month from (
			select est_monthly_id, month_number, hours, 0 partition
			from wo_est_transpose_temp
			where month_number <=/*<*/ :actuals_month /*slide_start*/
			union all
			select est_monthly_id, month_number, sum(hours) hours, partition from (
				select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
						hours, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
				from (
					select est_monthly_id, month_number, hours/multiplier hours
					from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
						(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
					order by est_monthly_id, month_number
					) a
				) a
			group by est_monthly_id, month_number, partition
			order by est_monthly_id, partition, month_number
			)
		where hours <> 0 // max month where hours is not zero
		group by est_monthly_id
	) month
where adj.est_monthly_id = month.est_monthly_id;

if uf_check_sql('Inserting rounding differences (hours) in wo_est_slide_adjust_temp') <> 1 then return -1


update wo_est_slide_results_temp t
set (hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec) = (
	select decode(to_char(month.last_month), t.year||'01', t.hrs_jan + month.hrs_adj, t.hrs_jan) jan,
			decode(to_char(month.last_month), t.year||'02', t.hrs_feb + month.hrs_adj, t.hrs_feb) feb,
			decode(to_char(month.last_month), t.year||'03', t.hrs_mar + month.hrs_adj, t.hrs_mar) mar,
			decode(to_char(month.last_month), t.year||'04', t.hrs_apr + month.hrs_adj, t.hrs_apr) apr,
			decode(to_char(month.last_month), t.year||'05', t.hrs_may + month.hrs_adj, t.hrs_may) may,
			decode(to_char(month.last_month), t.year||'06', t.hrs_jun + month.hrs_adj, t.hrs_jun) jun,
			decode(to_char(month.last_month), t.year||'07', t.hrs_jul + month.hrs_adj, t.hrs_jul) jul,
			decode(to_char(month.last_month), t.year||'08', t.hrs_aug + month.hrs_adj, t.hrs_aug) aug,
			decode(to_char(month.last_month), t.year||'09', t.hrs_sep + month.hrs_adj, t.hrs_sep) sep,
			decode(to_char(month.last_month), t.year||'10', t.hrs_oct + month.hrs_adj, t.hrs_oct) oct,
			decode(to_char(month.last_month), t.year||'11', t.hrs_nov + month.hrs_adj, t.hrs_nov) nov,
			decode(to_char(month.last_month), t.year||'12', t.hrs_dec + month.hrs_adj, t.hrs_dec) dec
	from wo_est_slide_adjust_temp month
	where month.est_monthly_id = t.est_monthly_id
	and to_number(substr(to_char(month.last_month),1,4)) = t.year
	)
where (t.est_monthly_id, t.year) in (
	select est_monthly_id, to_number(substr(to_char(last_month),1,4))
	from wo_est_slide_adjust_temp
	);

if uf_check_sql('Updating rounding differences (hours) in wo_est_slide_results_temp') <> 1 then return -1


//update wo_est_slide_results_temp a
//set (hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec) = (
//					select decode(to_char(month.last_month), t.year||'01', t.hrs_jan + hrs_adj, t.hrs_jan) hrs_jan,
//							decode(to_char(month.last_month), t.year||'02', t.hrs_feb + hrs_adj, t.hrs_feb) hrs_feb,
//							decode(to_char(month.last_month), t.year||'03', t.hrs_mar + hrs_adj, t.hrs_mar) hrs_mar,
//							decode(to_char(month.last_month), t.year||'04', t.hrs_apr + hrs_adj, t.hrs_apr) hrs_apr,
//							decode(to_char(month.last_month), t.year||'05', t.hrs_may + hrs_adj, t.hrs_may) hrs_may,
//							decode(to_char(month.last_month), t.year||'06', t.hrs_jun + hrs_adj, t.hrs_jun) hrs_jun,
//							decode(to_char(month.last_month), t.year||'07', t.hrs_jul + hrs_adj, t.hrs_jul) hrs_jul,
//							decode(to_char(month.last_month), t.year||'08', t.hrs_aug + hrs_adj, t.hrs_aug) hrs_aug,
//							decode(to_char(month.last_month), t.year||'09', t.hrs_sep + hrs_adj, t.hrs_sep) hrs_sep,
//							decode(to_char(month.last_month), t.year||'10', t.hrs_oct + hrs_adj, t.hrs_oct) hrs_oct,
//							decode(to_char(month.last_month), t.year||'11', t.hrs_nov + hrs_adj, t.hrs_nov) hrs_nov,
//							decode(to_char(month.last_month), t.year||'12', t.hrs_dec + hrs_adj, t.hrs_dec) hrs_dec
//					from wo_est_slide_results_temp t,
//						(	select new.est_monthly_id, old_total - new_total adjustment,
//								old_hrs_total - new_hrs_total hrs_adj, old_qty_total - new_qty_total qty_adj
//							from
//								(	select est_monthly_id, sum(old.total) old_total,
//										sum(old.hrs_total) old_hrs_total, sum(old.qty_total) old_qty_total
//									from wo_est_monthly_deescalation_vw old
//									where exists (
//										select 1 from wo_est_slide_results_temp a where a.est_monthly_id = old.est_monthly_id)
//									group by est_monthly_id
//								) old,
//								(	select est_monthly_id, sum(new.total) new_total,
//										sum(new.hrs_total) new_hrs_total, sum(new.qty_total) new_qty_total
//									from wo_est_slide_results_temp new
//									group by new.est_monthly_id
//								) new
//							where old.est_monthly_id = new.est_monthly_id
//						) adj,
//						(	select est_monthly_id, max(month_number) last_month from (
//								select est_monthly_id, month_number, hours, 0 partition
//								from wo_est_transpose_temp
//								where month_number <=/*<*/ :actuals_month /*slide_start*/
//								union all
//								select est_monthly_id, month_number, sum(hours) hours, partition from (
//									select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
//											hours, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
//									from (
//										select est_monthly_id, month_number, hours/multiplier hours
//										from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
//											(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
//										order by est_monthly_id, month_number
//										) a
//									) a
//								group by est_monthly_id, month_number, partition
//								order by est_monthly_id, partition, month_number
//								)
//							where hours <> 0 // max month where hours is not zero
//							group by est_monthly_id
//						) month
//					where t.est_monthly_id = adj.est_monthly_id
//					and t.est_monthly_id = month.est_monthly_id
//					and t.est_monthly_id = a.est_monthly_id
//					and t.year = a.year
//					)
//where exists (
//	select 1 from 
//		(	select est_monthly_id, max(month_number) last_month from (
//				select est_monthly_id, month_number, hours, 0 partition
//				from wo_est_transpose_temp
//				where month_number <=/*<*/ :actuals_month /*slide_start*/
//				union all
//				select est_monthly_id, month_number, sum(hours) hours, partition from (
//					select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
//							hours, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
//					from (
//						select est_monthly_id, month_number, hours/multiplier hours
//						from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
//							(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
//						order by est_monthly_id, month_number
//						) a
//					) a
//				group by est_monthly_id, month_number, partition
//				order by est_monthly_id, partition, month_number
//				)
//			where hours <> 0 // max month where hours is not zero
//			group by est_monthly_id
//		) month
//	where a.est_monthly_id = month.est_monthly_id
//	);
//
//if uf_check_sql('Updating rounding differences (hours) in wo_est_slide_results_temp') <> 1 then return -1


//
// Rounding (QUANTITY) - throw differences in the last month with non-zero values for each estimate
//
delete from wo_est_slide_adjust_temp;

insert into wo_est_slide_adjust_temp (est_monthly_id, last_month, adjustment, hrs_adj, qty_adj)
select adj.est_monthly_id, month.last_month, adj.adjustment, adj.hrs_adj, adj.qty_adj
from
	(	select new.est_monthly_id, old_total - new_total adjustment,
			old_hrs_total - new_hrs_total hrs_adj, old_qty_total - new_qty_total qty_adj
		from
			(	select est_monthly_id, sum(old.total) old_total,
					sum(old.hrs_total) old_hrs_total, sum(old.qty_total) old_qty_total
				from wo_est_monthly_deescalation_vw old
				where exists (
					select 1 from wo_est_slide_results_temp a where a.est_monthly_id = old.est_monthly_id)
				group by est_monthly_id
			) old,
			(	select est_monthly_id, sum(new.total) new_total,
					sum(new.hrs_total) new_hrs_total, sum(new.qty_total) new_qty_total
				from wo_est_slide_results_temp new
				group by new.est_monthly_id
			) new
		where old.est_monthly_id = new.est_monthly_id
	) adj,
	(	select est_monthly_id, max(month_number) last_month from (
			select est_monthly_id, month_number, quantity, 0 partition
			from wo_est_transpose_temp
			where month_number <=/*<*/ :actuals_month /*slide_start*/
			union all
			select est_monthly_id, month_number, sum(quantity) quantity, partition from (
				select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
						quantity, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
				from (
					select est_monthly_id, month_number, quantity/multiplier quantity
					from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
						(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
					order by est_monthly_id, month_number
					) a
				) a
			group by est_monthly_id, month_number, partition
			order by est_monthly_id, partition, month_number
			)
		where quantity <> 0 // max month where quantity is not zero
		group by est_monthly_id
	) month
where adj.est_monthly_id = month.est_monthly_id;

if uf_check_sql('Inserting rounding differences (quantity) in wo_est_slide_adjust_temp') <> 1 then return -1


update wo_est_slide_results_temp t
set (qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec) = (
	select decode(to_char(month.last_month), t.year||'01', t.qty_jan + month.qty_adj, t.qty_jan) jan,
			decode(to_char(month.last_month), t.year||'02', t.qty_feb + month.qty_adj, t.qty_feb) feb,
			decode(to_char(month.last_month), t.year||'03', t.qty_mar + month.qty_adj, t.qty_mar) mar,
			decode(to_char(month.last_month), t.year||'04', t.qty_apr + month.qty_adj, t.qty_apr) apr,
			decode(to_char(month.last_month), t.year||'05', t.qty_may + month.qty_adj, t.qty_may) may,
			decode(to_char(month.last_month), t.year||'06', t.qty_jun + month.qty_adj, t.qty_jun) jun,
			decode(to_char(month.last_month), t.year||'07', t.qty_jul + month.qty_adj, t.qty_jul) jul,
			decode(to_char(month.last_month), t.year||'08', t.qty_aug + month.qty_adj, t.qty_aug) aug,
			decode(to_char(month.last_month), t.year||'09', t.qty_sep + month.qty_adj, t.qty_sep) sep,
			decode(to_char(month.last_month), t.year||'10', t.qty_oct + month.qty_adj, t.qty_oct) oct,
			decode(to_char(month.last_month), t.year||'11', t.qty_nov + month.qty_adj, t.qty_nov) nov,
			decode(to_char(month.last_month), t.year||'12', t.qty_dec + month.qty_adj, t.qty_dec) dec
	from wo_est_slide_adjust_temp month
	where month.est_monthly_id = t.est_monthly_id
	and to_number(substr(to_char(month.last_month),1,4)) = t.year
	)
where (t.est_monthly_id, t.year) in (
	select est_monthly_id, to_number(substr(to_char(last_month),1,4))
	from wo_est_slide_adjust_temp
	);

if uf_check_sql('Updating rounding differences (quantity) in wo_est_slide_results_temp') <> 1 then return -1


//update wo_est_slide_results_temp a
//set (qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec) = (
//					select decode(to_char(month.last_month), t.year||'01', t.qty_jan + qty_adj, t.qty_jan) qty_jan,
//							decode(to_char(month.last_month), t.year||'02', t.qty_feb + qty_adj, t.qty_feb) qty_feb,
//							decode(to_char(month.last_month), t.year||'03', t.qty_mar + qty_adj, t.qty_mar) qty_mar,
//							decode(to_char(month.last_month), t.year||'04', t.qty_apr + qty_adj, t.qty_apr) qty_apr,
//							decode(to_char(month.last_month), t.year||'05', t.qty_may + qty_adj, t.qty_may) qty_may,
//							decode(to_char(month.last_month), t.year||'06', t.qty_jun + qty_adj, t.qty_jun) qty_jun,
//							decode(to_char(month.last_month), t.year||'07', t.qty_jul + qty_adj, t.qty_jul) qty_jul,
//							decode(to_char(month.last_month), t.year||'08', t.qty_aug + qty_adj, t.qty_aug) qty_aug,
//							decode(to_char(month.last_month), t.year||'09', t.qty_sep + qty_adj, t.qty_sep) qty_sep,
//							decode(to_char(month.last_month), t.year||'10', t.qty_oct + qty_adj, t.qty_oct) qty_oct,
//							decode(to_char(month.last_month), t.year||'11', t.qty_nov + qty_adj, t.qty_nov) qty_nov,
//							decode(to_char(month.last_month), t.year||'12', t.qty_dec + qty_adj, t.qty_dec) qty_dec
//					from wo_est_slide_results_temp t,
//						(	select new.est_monthly_id, old_total - new_total adjustment,
//								old_hrs_total - new_hrs_total hrs_adj, old_qty_total - new_qty_total qty_adj
//							from
//								(	select est_monthly_id, sum(old.total) old_total,
//										sum(old.hrs_total) old_hrs_total, sum(old.qty_total) old_qty_total
//									from wo_est_monthly_deescalation_vw old
//									where exists (
//										select 1 from wo_est_slide_results_temp a where a.est_monthly_id = old.est_monthly_id)
//									group by est_monthly_id
//								) old,
//								(	select est_monthly_id, sum(new.total) new_total,
//										sum(new.hrs_total) new_hrs_total, sum(new.qty_total) new_qty_total
//									from wo_est_slide_results_temp new
//									group by new.est_monthly_id
//								) new
//							where old.est_monthly_id = new.est_monthly_id
//						) adj,
//						(	select est_monthly_id, max(month_number) last_month from (
//								select est_monthly_id, month_number, quantity, 0 partition
//								from wo_est_transpose_temp
//								where month_number <=/*<*/ :actuals_month /*slide_start*/
//								union all
//								select est_monthly_id, month_number, sum(quantity) quantity, partition from (
//									select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
//											quantity, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
//									from (
//										select est_monthly_id, month_number, quantity/multiplier quantity
//										from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
//											(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
//										order by est_monthly_id, month_number
//										) a
//									) a
//								group by est_monthly_id, month_number, partition
//								order by est_monthly_id, partition, month_number
//								)
//							where quantity <> 0 // max month where quantity is not zero
//							group by est_monthly_id
//						) month
//					where t.est_monthly_id = adj.est_monthly_id
//					and t.est_monthly_id = month.est_monthly_id
//					and t.est_monthly_id = a.est_monthly_id
//					and t.year = a.year
//					)
//where exists (
//	select 1 from 
//		(	select est_monthly_id, max(month_number) last_month from (
//				select est_monthly_id, month_number, quantity, 0 partition
//				from wo_est_transpose_temp
//				where month_number <=/*<*/ :actuals_month /*slide_start*/
//				union all
//				select est_monthly_id, month_number, sum(quantity) quantity, partition from (
//					select est_monthly_id, to_number(to_char(add_months(to_date(to_char(:slide_start),'yyyymm'),trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span)),'yyyymm')) month_number,
//							quantity, trunc((rank() over(partition by est_monthly_id order by rownum, est_monthly_id, month_number) - 1)/:old_span) partition
//					from (
//						select est_monthly_id, month_number, quantity/multiplier quantity
//						from (select * from wo_est_transpose_temp where month_number >/*>=*/ :actuals_month /*slide_start*/),
//							(select max(rownum) over() multiplier from (select rownum from pp_table_months, pp_table_years where year * 100 + month_num between :slide_start and :slide_end))
//						order by est_monthly_id, month_number
//						) a
//					) a
//				group by est_monthly_id, month_number, partition
//				order by est_monthly_id, partition, month_number
//				)
//			where quantity <> 0 // max month where quantity is not zero
//			group by est_monthly_id
//		) month
//	where a.est_monthly_id = month.est_monthly_id
//	);
//
//if uf_check_sql('Updating rounding differences (quantity) in wo_est_slide_results_temp') <> 1 then return -1



////
//// Update values in case they were nulled out (this should not happen, but just in case)
////
//update wo_est_slide_results_temp a
//set (january,february,march,april,may,june,july,august,september,october,november,december,
//	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
//	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec) = (
//		select nvl(january,0), nvl(february,0), nvl(march,0), nvl(april,0), nvl(may,0), nvl(june,0), nvl(july,0), nvl(august,0), nvl(september,0), nvl(october,0), nvl(november,0), nvl(december,0),
//			nvl(hrs_jan,0), nvl(hrs_feb,0), nvl(hrs_mar,0), nvl(hrs_apr,0), nvl(hrs_may,0), nvl(hrs_jun,0), nvl(hrs_jul,0), nvl(hrs_aug,0), nvl(hrs_sep,0), nvl(hrs_oct,0), nvl(hrs_nov,0), nvl(hrs_dec,0),
//			nvl(qty_jan,0), nvl(qty_feb,0), nvl(qty_mar,0), nvl(qty_apr,0), nvl(qty_may,0), nvl(qty_jun,0), nvl(qty_jul,0), nvl(qty_aug,0), nvl(qty_sep,0), nvl(qty_oct,0), nvl(qty_nov,0), nvl(qty_dec,0)
//		from wo_est_slide_results_temp b
//		where b.est_monthly_id = a.est_monthly_id
//		and b.year = a.year
//		);
//
//if uf_check_sql('Updating nulls to zero in wo_est_slide_results_temp') <> 1 then return -1


//
// Backfill totals in case any rounding above changed the values in the last months with 
//
update wo_est_slide_results_temp a
set total = january + february + march + april + may + june + july + august + september + october + november + december,
	hrs_total = hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec,
	qty_total = qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec
where total <> january + february + march + april + may + june + july + august + september + october + november + december
 or hrs_total <> hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec
 or qty_total <> qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec;

if uf_check_sql('Updating totals (2) wo_est_slide_results_temp') <> 1 then return -1


return 1

end function

public function longlong uf_slide_revision_dollars (datetime a_new_start, datetime a_new_end, datetime a_new_est_start, datetime a_new_est_complete, datetime a_new_est_in_serv);//------------------------------------------------------------------------
//
// FUNCTION:		uf_slide_revision_dollars
//
// DESCRIPTION:	Stretch/Slide estimated DOLLARS, and adjust 
//						dollars/hrs/qty accordingly
//
//	ASSUMPTIONS:	1 - ONLY ONE REVISION CAN SLIDE AT A TIME
//						2 - work_order_id/revision must be inserted into
//								wo_est_processing_temp
//						3 - uf_slide_commit must be called separately to save
//								changes to wo_est_monthly
//
//	ARGUMENTS:	a_new_start -				datetime -	New start of the span of non-zero estimates
//						a_new_end -				datetime -	New end of the span of non-zero estimates
//						a_new_est_start -			datetime -	New estimate start date
//						a_new_est_complete -	datetime -	New estimate complete date
//						a_new_est_in_serv -		datetime -	New estimate in service date
//
// RETURN CODES:	1 - SUCCESS
//						2 - SUCCESS - DATES CHANGED TO ACCOUNT FOR ACTUALS MONTHS
//					  -1 - ERROR
//
//------------------------------------------------------------------------
datetime old_start_date, old_end_date
longlong rtn


//
// Use the first and last months with non-zero dollars as the old start and end
//
select nvl(to_date(to_char(min(wem.month_number)),'yyyymm'),min(woa.est_start_date)), nvl(to_date(to_char(max(wem.month_number)),'yyyymm'),min(woa.est_complete_date))
into :old_start_date, :old_end_date
from work_order_approval woa,
	(
	select a.work_order_id, a.revision,
		to_number(to_char(wem.year)*100+p.month_num) month_number,
		sum(decode(p.month_num,
			1,nvl(wem.january,0),
			2,nvl(wem.february,0),
			3,nvl(wem.march,0),
			4,nvl(wem.april,0),
			5,nvl(wem.may,0),
			6,nvl(wem.june,0),
			7,nvl(wem.july,0),
			8,nvl(wem.august,0),
			9,nvl(wem.september,0),
			10,nvl(wem.october,0),
			11,nvl(wem.november,0),
			12,nvl(wem.december,0),
			0)) amount
	from work_order_approval a, wo_est_monthly wem, pp_table_months p
	where (a.work_order_id, a.revision) in (
		select work_order_id, revision 
		from wo_est_processing_temp
		)
	and a.work_order_id = wem.work_order_id
	and a.revision = wem.revision
	group by a.work_order_id, a.revision, wem.year, p.month_num
	) wem
where (woa.work_order_id, woa.revision) in (
	select work_order_id, revision 
	from wo_est_processing_temp
	)
and woa.work_order_id = wem.work_order_id
and woa.revision = wem.revision
and wem.amount <> 0;

rtn = uf_slide(old_start_date, old_end_date, a_new_start, a_new_end, a_new_est_start, a_new_est_complete, a_new_est_in_serv)

return rtn

end function

public function longlong uf_wem_to_fy_temp (longlong funding_wo_ind, boolean fiscal_year, longlong estimate_mode_id);string sqls
string est_mo_id, wo_id, rev, table_name, util, year, ect_filter

// ### CDM - Maint 6807 - new estimate modes
if isnull(estimate_mode_id) or estimate_mode_id = 0 then
	ect_filter = ""
else
	select est_chg_type_filter into :ect_filter
	from monthly_estimate_mode
	where estimate_mode_id = :estimate_mode_id;
	
	if isnull(ect_filter) or trim(ect_filter) = "" then
		ect_filter = ""
	end if
end if

if funding_wo_ind = 2 then
	if i_analyze then
		sqlca.analyze_table('budget_monthly_data')
	end if
	
	est_mo_id = 'budget_monthly_id'
	wo_id = 'budget_id'
	rev = 'budget_version_id'
	util = 'budget_plant_class_id'
	table_name = 'budget_monthly_data'
else
	if i_analyze then
		sqlca.analyze_table('wo_est_monthly')
	end if
	
	est_mo_id = 'est_monthly_id'
	wo_id = 'work_order_id'
	rev = 'revision'
	util = 'utility_account_id'
	table_name = 'wo_est_monthly'
end if
if fiscal_year then
	year = 'p.fiscal_year'
else
	year = 'w.year'
end if
if i_analyze then
	sqlca.analyze_table('pp_calendar')
	sqlca.analyze_table('wo_est_processing_temp')
end if

sqlca.truncate_table('wo_est_monthly_fy_temp')

delete from wo_est_monthly_fy_temp;

if uf_check_sql('Deleting from wo_est_monthly_fy_temp') <> 1 then return -1

sqls = &
"insert into wo_est_monthly_fy_temp ( " +&
"	est_monthly_id, work_order_id, revision, year, " +&
"	expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, long_description, " +&
"	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, "
if funding_wo_ind = 2 then
	sqls += &
	"wo_work_order_id, "
else
	sqls += &
	"wo_work_order_id, substitution_id, budget_id, " // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
end if
sqls += &
"	future_dollars, hist_actuals, " +&
"	january, february, march, april, may, june, july, august, september, october, november, december, total "
if funding_wo_ind <> 2 then
	sqls += &
	"	, hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total, " +&
	"	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total "
end if
sqls += &
"	) " +&
"select w."+est_mo_id+" est_monthly_id, w."+wo_id+" work_order_id, w."+rev+" revision, "+year+" year, " +&
"	w.expenditure_type_id, w.est_chg_type_id, w.department_id, 	w."+util+" utility_account_id, w.job_task_id, w.long_description, " +&
"	w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id, "
if funding_wo_ind = 2 then
	sqls += &
	"w.work_order_id, "
else
	sqls += &
	"w.wo_work_order_id, max(w.substitution_id), w.budget_id, " // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
end if
sqls += &
"	nvl(w.future_dollars,0), nvl(w.hist_actuals,0), "
if fiscal_year then
	sqls += &
	"	sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) january, " +&
	"	sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) february, " +&
	"	sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) march, " +&
	"	sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) april, " +&
	"	sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) may, " +&
	"	sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) june, " +&
	"	sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) july, " +&
	"	sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) august, " +&
	"	sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) september, " +&
	"	sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) october, " +&
	"	sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) november, " +&
	"	sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) december, " +&
	"	sum( " +&
	"		nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) + " +&
	"		nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) " +&
	"		) total "
	if funding_wo_ind <> 2 then
		sqls += &
		"	, sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jan, " +&
		"	sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_feb, " +&
		"	sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_mar, " +&
		"	sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_apr, " +&
		"	sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_may, " +&
		"	sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jun, " +&
		"	sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jul, " +&
		"	sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_aug, " +&
		"	sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_sep, " +&
		"	sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_oct, " +&
		"	sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_nov, " +&
		"	sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_dec, " +&
		"	sum( " +&
		"		nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) " +&
		"		) hrs_total, " +&
		"	sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jan, " +&
		"	sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_feb, " +&
		"	sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_mar, " +&
		"	sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_apr, " +&
		"	sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_may, " +&
		"	sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jun, " +&
		"	sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jul, " +&
		"	sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_aug, " +&
		"	sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_sep, " +&
		"	sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_oct, " +&
		"	sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_nov, " +&
		"	sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_dec, " +&
		"	sum( " +&
		"		nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) + " +&
		"		nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) " +&
		"		) qty_total "
	end if
else
	sqls += &
	"	sum(nvl(w.january,0)) january, " +&
	"	sum(nvl(w.february,0)) february, " +&
	"	sum(nvl(w.march,0)) march, " +&
	"	sum(nvl(w.april,0)) april, " +&
	"	sum(nvl(w.may,0)) may, " +&
	"	sum(nvl(w.june,0)) june, " +&
	"	sum(nvl(w.july,0)) july, " +&
	"	sum(nvl(w.august,0)) august, " +&
	"	sum(nvl(w.september,0)) september, " +&
	"	sum(nvl(w.october,0)) october, " +&
	"	sum(nvl(w.november,0)) november, " +&
	"	sum(nvl(w.december,0)) december, " +&
	"	sum( " +&
	"		nvl(w.january,0) + " +&
	"		nvl(w.february,0) + " +&
	"		nvl(w.march,0) + " +&
	"		nvl(w.april,0) + " +&
	"		nvl(w.may,0) + " +&
	"		nvl(w.june,0) + " +&
	"		nvl(w.july,0) + " +&
	"		nvl(w.august,0) + " +&
	"		nvl(w.september,0) + " +&
	"		nvl(w.october,0) + " +&
	"		nvl(w.november,0) + " +&
	"		nvl(w.december,0) " +&
	"		) total "
	if funding_wo_ind <> 2 then
		sqls += &
		"	, sum(nvl(w.hrs_jan,0)) hrs_jan, " +&
		"	sum(nvl(w.hrs_feb,0)) hrs_feb, " +&
		"	sum(nvl(w.hrs_mar,0)) hrs_mar, " +&
		"	sum(nvl(w.hrs_apr,0)) hrs_apr, " +&
		"	sum(nvl(w.hrs_may,0)) hrs_may, " +&
		"	sum(nvl(w.hrs_jun,0)) hrs_jun, " +&
		"	sum(nvl(w.hrs_jul,0)) hrs_jul, " +&
		"	sum(nvl(w.hrs_aug,0)) hrs_aug, " +&
		"	sum(nvl(w.hrs_sep,0)) hrs_sep, " +&
		"	sum(nvl(w.hrs_oct,0)) hrs_oct, " +&
		"	sum(nvl(w.hrs_nov,0)) hrs_nov, " +&
		"	sum(nvl(w.hrs_dec,0)) hrs_dec, " +&
		"	sum( " +&
		"		nvl(w.hrs_jan,0) + " +&
		"		nvl(w.hrs_feb,0) + " +&
		"		nvl(w.hrs_mar,0) + " +&
		"		nvl(w.hrs_apr,0) + " +&
		"		nvl(w.hrs_may,0) + " +&
		"		nvl(w.hrs_jun,0) + " +&
		"		nvl(w.hrs_jul,0) + " +&
		"		nvl(w.hrs_aug,0) + " +&
		"		nvl(w.hrs_sep,0) + " +&
		"		nvl(w.hrs_oct,0) + " +&
		"		nvl(w.hrs_nov,0) + " +&
		"		nvl(w.hrs_dec,0) " +&
		"		) hrs_total, " +&
		"	sum(nvl(w.qty_jan,0)) qty_jan, " +&
		"	sum(nvl(w.qty_feb,0)) qty_feb, " +&
		"	sum(nvl(w.qty_mar,0)) qty_mar, " +&
		"	sum(nvl(w.qty_apr,0)) qty_apr, " +&
		"	sum(nvl(w.qty_may,0)) qty_may, " +&
		"	sum(nvl(w.qty_jun,0)) qty_jun, " +&
		"	sum(nvl(w.qty_jul,0)) qty_jul, " +&
		"	sum(nvl(w.qty_aug,0)) qty_aug, " +&
		"	sum(nvl(w.qty_sep,0)) qty_sep, " +&
		"	sum(nvl(w.qty_oct,0)) qty_oct, " +&
		"	sum(nvl(w.qty_nov,0)) qty_nov, " +&
		"	sum(nvl(w.qty_dec,0)) qty_dec, " +&
		"	sum( " +&
		"		nvl(w.qty_jan,0) + " +&
		"		nvl(w.qty_feb,0) + " +&
		"		nvl(w.qty_mar,0) + " +&
		"		nvl(w.qty_apr,0) + " +&
		"		nvl(w.qty_may,0) + " +&
		"		nvl(w.qty_jun,0) + " +&
		"		nvl(w.qty_jul,0) + " +&
		"		nvl(w.qty_aug,0) + " +&
		"		nvl(w.qty_sep,0) + " +&
		"		nvl(w.qty_oct,0) + " +&
		"		nvl(w.qty_nov,0) + " +&
		"		nvl(w.qty_dec,0) " +&
		"		) qty_total "
	end if
end if
if fiscal_year then
	sqls += &
	"from pp_calendar p, "+table_name+" w, wo_est_processing_temp t " +&
	"where t.work_order_id = w."+wo_id+" " +&
	"and t.revision = w."+rev+" " +&
	"and p.year = w.year "
else
	sqls += &
	"from "+table_name+" w, wo_est_processing_temp t " +&
	"where t.work_order_id = w."+wo_id+" " +&
	"and t.revision = w."+rev+" "
end if
// ### CDM - Maint 6807 - new estimate modes
if ect_filter <> "" then
	sqls += &
	"and w.est_chg_type_id in ("+ect_filter+")"
end if
sqls += &
"group by w."+est_mo_id+", w."+wo_id+", w."+rev+", "+year+", " +&
"	w.expenditure_type_id, w.est_chg_type_id, w.department_id, " +&
"	w."+util+", w.long_description, w.job_task_id, " +&
"	w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id, " +&
"	nvl(w.future_dollars,0), nvl(w.hist_actuals,0), "
if funding_wo_ind = 2 then
	sqls += &
	"w.work_order_id "
else
	sqls += &
	"w.wo_work_order_id, w.budget_id " // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
end if

if funding_wo_ind <> 2 then
	sqls = f_sql_add_hint(sqls, 'uf_wem_to_fy_10')
else
	sqls = f_sql_add_hint(sqls, 'uf_wem_to_fy_20')
end if

execute immediate :sqls;

if uf_check_sql('Inserting into wo_est_monthly_fy_temp') <> 1 then return -1

return 1
end function

public function longlong uf_delete_bi_version ();// ### CDM - Maint 6695 - centralize code to insert into budget_amounts
//
// Delete afudc calculation results
//
delete from budget_afudc_calc bac
where exists (
	select 1 from wo_est_processing_temp a
	where a.work_order_id = bac.work_order_id
	and a.revision = bac.revision
	);

if uf_check_sql('Deleting afudc results from budget_afudc_calc') <> 1 then return -1

//
// Delete monthly escalations
//
delete from budget_monthly_data_escalation z
where exists (
	select 1 from wo_est_processing_temp a, budget_monthly_data b
	where a.work_order_id = b.budget_id
	and a.revision = b.budget_version_id
	and b.budget_monthly_id = z.budget_monthly_id
	and b.year = z.year
	);

if uf_check_sql('Deleting escalation data from budget_monthly_data_escalation') <> 1 then return -1

//
// Delete budget_class_code
//
delete from budget_class_code bcc
where exists (
	select 1 from wo_est_processing_temp a
	where a.work_order_id = bcc.budget_id
	and a.revision = bcc.budget_version_id
	);

if uf_check_sql('Deleting records from budget_class_code') <> 1 then return -1

//
// Delete budget_monthly_spread records
//
delete from budget_monthly_spread z
where exists (
	select 1 from wo_est_processing_temp a, budget_monthly_data b
	where a.work_order_id = b.budget_id
	and a.revision = b.budget_version_id
	and b.budget_monthly_id = z.budget_monthly_id
	);

if uf_check_sql('Deleting records from budget_monthly_spread') <> 1 then return -1

//
// Delete monthly estimate records
//
delete from budget_monthly_data bmd
where exists (
	select 1 from wo_est_processing_temp a
	where a.work_order_id = bmd.budget_id
	and a.revision = bmd.budget_version_id
	);

if uf_check_sql('Deleting estimates from budget_monthly_data') <> 1 then return -1

//
// Delete budget_amounts
//
delete from budget_amounts ba
where exists (
	select 1 from wo_est_processing_temp a
	where a.work_order_id = ba.budget_id
	and a.revision = ba.budget_version_id
	);

if uf_check_sql('Deleting budget items from budget_amounts') <> 1 then return -1

//
// Delete budget_amounts
//
update wo_documentation d
set d.est_revision = 0
where exists (
	select 1 from wo_est_processing_temp t
	where t.work_order_id = d.work_order_id
	and t.revision = d.est_revision
	);

if uf_check_sql('Disassociating justification documents') <> 1 then return -1

return 1
end function

public function longlong uf_new_bi_version (longlong a_new_version);//------------------------------------------------------------------------
//
// FUNCTION:		uf_new_revision
//
// DESCRIPTION:	For budget items, copy budget_version information to another budget_version
//
//	ASSUMPTIONS:	1 - budget_id/budget_version_id must be inserted into
//								wo_est_processing_temp
//						2 - When copying versions, must have a valid version in the
//								revision column of wo_est_processing_temp
//						3 - When adding a budget item to a budget version from scratch, must have zero (0) in the
//								revision column of wo_est_processing_temp
//
// RETURN CODES:	1 - SUCCESS
//					  -1 - ERROR
//
//------------------------------------------------------------------------
// ### CDM - Maint 6695 - centralize code to insert into budget_amounts
string sqls, func_name, str_arr[], ret, args[]
longlong long_arr[], cst_rtn, cst_rtn2, cst_rtn3, i, tab_ind, stage_id
uo_ds_top ds_new_just

func_name = 'uf_new_bi_version'


//
// Call to custom function before any processing
//
cst_rtn = f_budget_revisions_custom(func_name,1,'bi',long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
long_arr[1] = 0
str_arr[1] = ' '
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,1,'bi',long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '1'
args[2] = string(cst_rtn3)
cst_rtn3 = f_wo_validation_control(88,args)
if cst_rtn3 < 0 then 
   return -1
end if


//
// Copy budget_amounts
//
insert into budget_amounts (
	budget_id, budget_version_id,
	start_date, close_date, in_service_date,
	budget_total_dollars, curr_year_budget_dollars, hist_amount,
	closing_pattern_id, closing_option_id, late_chg_wait_period,
	base_year, escalation_id,
	budget_total_local, curr_year_budget_local, hist_amount_local,
	user_text1, user_text2, user_text3, user_text4 )
select ba.budget_id, :a_new_version,
	ba.start_date, ba.close_date, ba.in_service_date,
	ba.budget_total_dollars, ba.curr_year_budget_dollars, ba.hist_amount,
	ba.closing_pattern_id, ba.closing_option_id, ba.late_chg_wait_period,
	ba.base_year, ba.escalation_id,
	ba.budget_total_local, ba.curr_year_budget_local, ba.hist_amount_local,
	ba.user_text1, ba.user_text2, ba.user_text3, ba.user_text4
from budget_amounts ba, wo_est_processing_temp w
where ba.budget_id = w.work_order_id
and ba.budget_version_id = w.revision
and w.revision <> 0;

if uf_check_sql('Copying budget items into budget_amounts') <> 1 then return -1


//
// New budget_amounts
//
insert into budget_amounts (
	budget_id, budget_version_id,
	start_date, close_date, in_service_date,
	budget_total_dollars, curr_year_budget_dollars, hist_amount,
	closing_pattern_id, closing_option_id, late_chg_wait_period,
	base_year, escalation_id,
	budget_total_local, curr_year_budget_local, hist_amount_local,
	user_text1, user_text2, user_text3, user_text4 )
select w.work_order_id, :a_new_version,
	trunc(sysdate,'DDD') start_date, trunc(sysdate,'DDD') close_date, trunc(sysdate,'DDD') in_service_date,
	0 budget_total_dollars, 0 curr_year_budget_dollars, 0 hist_amount,
	null closing_pattern_id, null closing_option_id, null late_chg_wait_period,
	null base_year, null escalation_id,
	0 budget_total_local, 0 curr_year_budget_local, 0 hist_amount_local,
	null user_text1, null user_text2, null user_text3, null user_text4
from wo_est_processing_temp w
where w.revision = 0;

if uf_check_sql('Inserting budget items into budget_amounts') <> 1 then return -1


//
// Translate old dollar est_monthly_ids to new ones
//
delete from wo_estimate_class_code_temp;

insert into wo_estimate_class_code_temp (estimate_id, new_estimate_id)
select budget_monthly_id, pwrplant1.nextval
from (
	select distinct a.budget_monthly_id
	from budget_monthly_data a, wo_est_processing_temp b
	where a.budget_id = b.work_order_id
	and a.budget_version_id = b.revision
	and b.revision <> 0
	);

if uf_check_sql('Inserting into wo_estimate_class_code_temp') <> 1 then return -1


//
// Copy monthly estimate records
//
insert into budget_monthly_data (
	budget_monthly_id, budget_id, budget_version_id, year,
	expenditure_type_id, est_chg_type_id, department_id, budget_plant_class_id, job_task_id,
	work_order_id, long_description, hist_actuals, future_dollars, use_local_curr,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june, july, august, september, october, november, december, total,
	jan_local, feb_local, mar_local,apr_local, may_local, jun_local, jul_local, aug_local, sep_local, oct_local, nov_local, dec_local, tot_local )
select c.new_estimate_id, bmd.budget_id, :a_new_version, bmd.year,
	bmd.expenditure_type_id, bmd.est_chg_type_id, bmd.department_id, bmd.budget_plant_class_id, bmd.job_task_id,
	// ### CDM - Maint 8569 - include future_dollars in copy
	bmd.work_order_id, bmd.long_description, nvl(hist_actuals,0) hist_actuals, nvl(future_dollars,0) future_dollars, bmd.use_local_curr,
	bmd.attribute01_id, bmd.attribute02_id, bmd.attribute03_id, bmd.attribute04_id, bmd.attribute05_id, bmd.attribute06_id, bmd.attribute07_id, bmd.attribute08_id, bmd.attribute09_id, bmd.attribute10_id,
	bmd.january, bmd.february, bmd.march, bmd.april, bmd.may, bmd.june, bmd.july, bmd.august, bmd.september, bmd.october, bmd.november, bmd.december, bmd.total,
	bmd.jan_local, bmd.feb_local, bmd.mar_local, bmd.apr_local, bmd.may_local, bmd.jun_local, bmd.jul_local, bmd.aug_local, bmd.sep_local, bmd.oct_local, bmd.nov_local, bmd.dec_local, bmd.tot_local
from budget_monthly_data bmd, wo_est_processing_temp w, wo_estimate_class_code_temp c
where bmd.budget_id = w.work_order_id
and bmd.budget_version_id = w.revision
and bmd.budget_monthly_id = c.estimate_id
and w.revision <> 0;

if uf_check_sql('Copying estimates into budget_monthly_data') <> 1 then return -1


//
// Copy budget_monthly_spread records
//
insert into budget_monthly_spread (
	budget_monthly_id, factor_id, curve_id, rate_type_id )
select b.new_estimate_id, a.factor_id, a.curve_id, a.rate_type_id
from budget_monthly_spread a, wo_estimate_class_code_temp b
where a.budget_monthly_id = b.estimate_id;

if uf_check_sql('Copying records into wo_est_monthly_spread') <> 1 then return -1


//
// Copy budget_class_code
//
insert into budget_class_code (
	budget_id, budget_version_id, class_code_id, value )
select b.budget_id, :a_new_version, b.class_code_id, b.value
from budget_class_code b, wo_est_processing_temp w
where b.budget_id = w.work_order_id
and b.budget_version_id = w.revision;

if uf_check_sql('Copying records into wo_est_monthly_spread') <> 1 then return -1


//
// Copy monthly escalations
//
insert into budget_monthly_data_escalation (
	budget_monthly_id, year,
	january, february, march, april, may, june,
	july, august, september, october, november, december, total,
	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate,
	jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate,
	base_year, rate_type_id )
select b.new_estimate_id, a.year,
	a.january, a.february, a.march, a.april, a.may, a.june,
	a.july, a.august, a.september, a.october, a.november, a.december, a.total,
	a.jan_rate, a.feb_rate, a.mar_rate, a.apr_rate, a.may_rate, a.jun_rate,
	a.jul_rate, a.aug_rate, a.sep_rate, a.oct_rate, a.nov_rate, a.dec_rate,
	a.base_year, a.rate_type_id
from budget_monthly_data_escalation a, wo_estimate_class_code_temp b
where a.budget_monthly_id = b.estimate_id;

if uf_check_sql('Copying escalation data into budget_monthly_data_escalation') <> 1 then return -1


//
// Copy afudc calculation results
//
insert into budget_afudc_calc (
	work_order_id, revision, expenditure_type_id, month_number,
	amount, amount_afudc, amount_cpi, in_service_date, afudc_type_id,
	closing_pattern_id, closing_option_id, eligible_for_afudc, eligible_for_cpi,
	afudc_stop_date, afudc_start_date, afudc_debt_rate, afudc_equity_rate,
	cpi_rate, afudc_compound_ind, cpi_compound_ind, closings_pct,
	funding_wo_indicator, beg_cwip, beg_cwip_afudc, beg_cwip_cpi,
	current_month_charges_afudc, current_month_charges_cpi, current_month_closings,
	current_month_closings_afudc, current_month_closings_cpi,
	uncompounded_afudc, uncompounded_cpi, compounded_afudc, compounded_cpi,
	afudc_base, cpi_base, afudc_debt, afudc_equity, cpi,
	end_cwip, end_cwip_afudc, end_cwip_cpi,
	est_monthly_id_debt, est_monthly_id_equity, est_monthly_id_cpi,
	est_monthly_id_cwipbal_cwip, est_monthly_id_cwipbal_tax, est_monthly_id_cwipbal_removal,
	est_monthly_id_cwipbal_salvage, est_monthly_id_closings_cwip, est_monthly_id_closings_tax,
	est_monthly_id_closings_rmvl, est_monthly_id_closings_slvg,
	debt_est_chg_type_id, equity_est_chg_type_id, cpi_est_chg_type_id, wo_work_order_id )
select a.work_order_id, :a_new_version, a.expenditure_type_id, a.month_number,
	a.amount, a.amount_afudc, a.amount_cpi, a.in_service_date, a.afudc_type_id,
	a.closing_pattern_id, a.closing_option_id, a.eligible_for_afudc, a.eligible_for_cpi,
	a.afudc_stop_date, a.afudc_start_date, a.afudc_debt_rate, a.afudc_equity_rate,
	a.cpi_rate, a.afudc_compound_ind, a.cpi_compound_ind, a.closings_pct,
	a.funding_wo_indicator, a.beg_cwip, a.beg_cwip_afudc, a.beg_cwip_cpi,
	a.current_month_charges_afudc, a.current_month_charges_cpi, a.current_month_closings,
	a.current_month_closings_afudc, a.current_month_closings_cpi,
	a.uncompounded_afudc, a.uncompounded_cpi, a.compounded_afudc, a.compounded_cpi,
	a.afudc_base, a.cpi_base, a.afudc_debt, a.afudc_equity, a.cpi,
	a.end_cwip, a.end_cwip_afudc, a.end_cwip_cpi,
	null est_monthly_id_debt, null est_monthly_id_equity, null est_monthly_id_cpi,
	null est_monthly_id_cwipbal_cwip, null est_monthly_id_cwipbal_tax, null est_monthly_id_cwipbal_removal,
	null est_monthly_id_cwipbal_salvage, null est_monthly_id_closings_cwip, null est_monthly_id_closings_tax,
	null est_monthly_id_closings_rmvl, null est_monthly_id_closings_slvg,
	a.debt_est_chg_type_id, a.equity_est_chg_type_id, a.cpi_est_chg_type_id, a.wo_work_order_id
from budget_afudc_calc a, wo_est_processing_temp b
where a.work_order_id = b.work_order_id
and a.revision = b.revision
and b.revision <> 0;

if uf_check_sql('Copying afudc results into budget_afudc_calc') <> 1 then return -1


////
//// Insert new justifications
////
//ds_new_just = create uo_ds_top
//sqls = &
//	"select tab_indicator, bi_new_rev_stage new_stage " +&
//	"from wo_doc_justification_tabs " +&
//	"where bi_new_rev_stage is not null "
//ret = f_create_dynamic_ds(ds_new_just, 'grid', sqls, sqlca, true)
//if ret <> 'OK' then
//	uf_msg("Error creating datastore:~r~n"+sqls,'E')
//end if
//
//for i = 1 to ds_new_just.rowcount()
//	setnull(tab_ind)
//	setnull(stage_id)
//	tab_ind = ds_new_just.getitemnumber(i,"tab_indicator")
//	stage_id = ds_new_just.getitemnumber(i,"new_stage")
//	
//	if tab_ind > 0 and stage_id > 0 then
//		uf_justification_new(tab_ind, stage_id, 2)
//	end if
//next
//destroy ds_new_just


//
// Call to custom function at end of processing
//
cst_rtn = f_budget_revisions_custom(func_name,3,'bi',long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
   return -1
end if
long_arr[1] = 0
str_arr[1] = ' '
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,3,'bi',long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '3'
args[2] = string(cst_rtn3)
cst_rtn3 = f_wo_validation_control(88,args)
if cst_rtn3 < 0 then 
   return -1
end if

return 1
end function

public function longlong uf_update_budget_amounts ();//------------------------------------------------------------------------
//
// FUNCTION:		uf_update_budget_amounts
//
// DESCRIPTION:	For budget items, update budget_amounts table based on budget_monthly_data
//
//	ASSUMPTIONS:	1 - budget_id/budget_version_id must be inserted into
//								wo_est_processing_temp
//
// RETURN CODES:	1 - SUCCESS
//					  -1 - ERROR
//
//------------------------------------------------------------------------
// ### CDM - Maint 7678 - centralize code for updating budget_amounts
update budget_amounts a
set (budget_total_dollars, curr_year_budget_dollars, budget_total_local, curr_year_budget_local) = (
	select nvl(sum(bmd.total),0),
		nvl(sum(decode(bmd.year,bv.current_year,bmd.total,0)),0),
		nvl(sum(bmd.tot_local),0),
		nvl(sum(decode(bmd.year,bv.current_year,bmd.tot_local,0)),0)
	from budget_monthly_data_fy bmd, budget_version bv
	where bmd.budget_id = a.budget_id
	and bmd.budget_version_id = a.budget_version_id
	and bmd.est_chg_type_id in (
		select est_chg_type_id
		from estimate_charge_type
		where nvl(afudc_flag,'Q') not in ('X','W','C','R','V','T','Y','Z') and nvl(processing_type_id,4) not in (1,5,9,10)
		)
	and bmd.budget_version_id = bv.budget_version_id
	)
where exists (
	select 1 from wo_est_processing_temp t
	where t.work_order_id = a.budget_id
	and t.revision = a.budget_version_id
	);

if uf_check_sql('Updating budget_amount (total dollars and current year dollars)') = -1 then return -1

return 1

end function

public function longlong uf_load_fp_to_bi ();longlong long_arr[], cst_rtn, cst_rtn2, cst_rtn3
string str_arr[], func_name, a_wo_fp, args[]

func_name = 'uf_load_fp_to_bi'
a_wo_fp = 'bi'


delete from temp_budget;
if uf_check_sql("Deleting from temp_budget") = -1 then return -1

insert into temp_budget (user_id, session_id, budget_id, batch_report_id, budget_version_id)
select lower(user), userenv('sessionid'), work_order_id, 0, revision
from wo_est_processing_temp;
if uf_check_sql("Inserting into temp_budget") = -1 then return -1


delete from wo_est_load_budgets_temp;
if uf_check_sql("Deleting from wo_est_load_budgets_temp") = -1 then return -1

insert into wo_est_load_budgets_temp (budget_id, budget_version_id, obey_review, review_any_revision, update_header_from_fp)
select t.work_order_id, t.revision, nvl(bv.level_id,0), decode(lower(trim(nvl(pp.control_value,'no'))),'yes',1,0), decode(lower(trim(nvl(pp2.control_value,'yes'))),'no',0,1)
from wo_est_processing_temp t, budget_version bv, budget b,
	(	select * from pp_system_control_companies pp
		where lower(trim(pp.control_name)) = lower(trim('BUDGET - Budget Review Any Revision'))
	) pp,
	(	select * from pp_system_control_companies pp
		where lower(trim(pp.control_name)) = lower(trim('BUDGET - Update Header with FP'))
	) pp2
where t.revision = bv.budget_version_id
and t.work_order_id = b.budget_id
and b.company_id = pp.company_id (+)
and b.company_id = pp2.company_id (+)
and not exists ( // ### CDM - Maint 8347 - Do not overwrite pending amounts at the budget item level
	select 1 from bi_est_month_is_editable edit
	where edit.work_order_id = t.work_order_id
	and edit.revision = t.revision
	and revision_edit = 0
	);
if uf_check_sql("Inserting into wo_est_load_budgets_temp") = -1 then return -1


cst_rtn = f_budget_revisions_custom(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn)
if cst_rtn < 0 then 
	return -1
end if
long_arr[1] = 0
str_arr[1] = ' '
if sqlca.f_client_extension_exists( 'f_client_budget_revision' ) = 1 then
	cst_rtn2 = sqlca.f_client_budget_revision(func_name,1,a_wo_fp,long_arr,str_arr,cst_rtn2)
	if cst_rtn2 < 0 then 
		return -1
	end if
end if
args[1] = '1'
args[2] = string(cst_rtn3)
cst_rtn3 = f_wo_validation_control(89,args)
if cst_rtn3 < 0 then 
	return -1
end if


sqlca.analyze_table('wo_est_load_budgets_temp')
sqlca.analyze_table('budget_monthly_data')


uf_msg("Deleting from BUDGET_CLASS_CODE.",'I')
DELETE FROM "BUDGET_CLASS_CODE" z
WHERE exists (
	select 1 from wo_est_load_budgets_temp t
	where t.budget_id = z.budget_id
	and t.budget_version_id = z.budget_version_id
	)
and exists (
	select 1 from budget b, work_order_control woc
	where b.budget_id = z.budget_id
	and b.budget_id = woc.budget_id
	and b.budget_number = woc.work_order_number
	and woc.funding_wo_indicator = 1
	);
if uf_check_sql("Deleting from budget_class_code") = -1 then return -1


uf_msg("Deleting from BUDGET_AMOUNTS",'I')
DELETE FROM "BUDGET_AMOUNTS" z
WHERE exists (
	select 1 from wo_est_load_budgets_temp t
	where t.budget_id = z.budget_id
	and t.budget_version_id = z.budget_version_id
	);
if uf_check_sql("Deleting from budget_amounts") = -1 then return -1


uf_msg("Deleting from BUDGET_MONTHLY_DATA_ESCALATION",'I')
DELETE FROM "BUDGET_MONTHLY_DATA_ESCALATION" esc
WHERE exists (
	select 1 from budget_monthly_data bmd, wo_est_load_budgets_temp t
	where t.budget_id = bmd.budget_id
	and t.budget_version_id = bmd.budget_version_id
	and bmd.budget_monthly_id = esc.budget_monthly_id
	and bmd.year = esc.year
	);
if uf_check_sql("Deleting from budget_monthly_data_escalation") = -1 then return -1


uf_msg("Deleting from BUDGET_MONTHLY_SPREAD",'I')
DELETE FROM "BUDGET_MONTHLY_SPREAD" spread
WHERE exists (
	select 1 from budget_monthly_data bmd, wo_est_load_budgets_temp t
	where t.budget_id = bmd.budget_id
	and t.budget_version_id = bmd.budget_version_id
	and bmd.budget_monthly_id = spread.budget_monthly_id
	);
if uf_check_sql("Deleting from budget_monthly_spread") = -1 then return -1


uf_msg("Deleting from BUDGET_MONTHLY_DATA",'I')
DELETE FROM "BUDGET_MONTHLY_DATA" z
WHERE exists (
	select 1 from wo_est_load_budgets_temp t
	where t.budget_id = z.budget_id
	and t.budget_version_id = z.budget_version_id
	);
if uf_check_sql("Deleting from budget_monthly_data") = -1 then return -1


//
// Insert into budget_monthly_data from wo_est_monthly
//	- Check if budget version obeys review
//	- Check if system control for obeying review allows any revision to be reviewed or just the revision in the version
//
uf_msg("Getting list of EST_MONTHLY_IDs",'I')
delete from wo_est_processing_temp;
if uf_check_sql("Deleting from wo_est_processing_temp") = -1 then return -1

insert into wo_est_processing_temp (work_order_id, revision, substitution_id)
select bvfp.work_order_id, bvfp.revision, t.budget_version_id
from wo_est_load_budgets_temp t, work_order_control woc, budget_version_fund_proj bvfp
where t.budget_id = woc.budget_id
and woc.funding_wo_indicator = 1
and woc.work_order_id = bvfp.work_order_id
and t.budget_version_id = bvfp.budget_version_id
and bvfp.active = 1
and exists (
	select 1 from work_order_approval a
	where a.work_order_id = woc.work_order_id
	and decode(t.obey_review,0,3,nvl(a.review_status,1)) = 3
	and decode(t.review_any_revision, 1, bvfp.revision, a.revision) = bvfp.revision
	);
if uf_check_sql("Inserting into wo_est_processing_temp") = -1 then return -1


sqlca.analyze_table('wo_est_processing_temp')


uf_msg("Inserting into BUDGET_MONTHLY_DATA",'I')
insert into budget_monthly_data (
	work_order_id, budget_monthly_id, budget_id, budget_version_id, year,
	expenditure_type_id, est_chg_type_id, department_id, long_description, job_task_id, budget_plant_class_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june, july, august, september, october, november, december, total,
	future_dollars, hist_actuals)
select wem.work_order_id, wem.est_monthly_id, woc.budget_id, t.substitution_id, wem.year,
	wem.expenditure_type_id, wem.est_chg_type_id, wem.department_id, wem.long_description, wem.job_task_id, wem.utility_account_id,
	wem.attribute01_id, wem.attribute02_id, wem.attribute03_id, wem.attribute04_id, wem.attribute05_id, wem.attribute06_id, wem.attribute07_id, wem.attribute08_id, wem.attribute09_id, wem.attribute10_id,
	wem.january, wem.february, wem.march, wem.april, wem.may, wem.june, wem.july, wem.august, wem.september, wem.october, wem.november, wem.december, wem.total,
	wem.future_dollars, wem.hist_actuals
from wo_est_processing_temp t, wo_est_monthly wem, work_order_control woc
where t.work_order_id = wem.work_order_id
and t.revision = wem.revision
and t.work_order_id = woc.work_order_id;
if uf_check_sql("Inserting into budget_monthly_data") = -1 then return -1


uf_msg("Inserting into BUDGET_MONTHLY_SPREAD",'I')
insert into budget_monthly_spread (budget_monthly_id, factor_id, curve_id)
select est_monthly_id, factor_id, curve_id
from wo_est_monthly_spread
where est_monthly_id in (
	select est_monthly_id
	from wo_est_monthly wem, wo_est_processing_temp t
	where wem.work_order_id = t.work_order_id
	and wem.revision = t.revision
	minus
	select budget_monthly_id
	from budget_monthly_spread
	);
if uf_check_sql("Inserting into budget_monthly_spread") = -1 then return -1


uf_msg("Inserting into BUDGET_MONTHLY_DATA_ESCALATION",'I')
insert into budget_monthly_data_escalation (
	budget_monthly_id, year,
	january, february, march, april, may, june, july, august, september, october, november, december, total,
	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate,
	base_year, rate_type_id)
select esc.est_monthly_id, esc.year,
	esc.january, esc.february, esc.march, esc.april, esc.may, esc.june, esc.july, esc.august, esc.september, esc.october, esc.november, esc.december, esc.total,
	esc.jan_rate, esc.feb_rate, esc.mar_rate, esc.apr_rate, esc.may_rate, esc.jun_rate, esc.jul_rate, esc.aug_rate, esc.sep_rate, esc.oct_rate, esc.nov_rate, esc.dec_rate,
	esc.base_year, esc.rate_type_id
from wo_est_monthly_escalation esc, wo_est_monthly wem, wo_est_processing_temp t
where esc.est_monthly_id = wem.est_monthly_id
and esc.year = wem.year
and wem.work_order_id = t.work_order_id
and wem.revision = t.revision;
if uf_check_sql("Inserting into budget_monthly_data_escalation") = -1 then return -1


uf_msg("Inserting into BUDGET_AMOUNTS",'I')
insert into budget_amounts (
	budget_id, budget_version_id, start_date, close_date,
	budget_total_dollars, curr_year_budget_dollars,
	in_service_date, closing_option_id, late_chg_wait_period,
	closing_pattern_id )
select woc.budget_id, t.substitution_id, min(woa.est_start_date), max(woa.est_complete_date),
	sum(wem.total), sum(decode(wem.year,bv.current_year,wem.total,0)),
	max(woa.est_in_service_date), min(woacct.closing_option_id), min(woc.late_chg_wait_period),
	min(woa.closing_pattern_id)
from wo_est_processing_temp t, work_order_control woc, work_order_account woacct, work_order_approval woa, wo_est_monthly_fy wem, budget_version bv
where t.work_order_id = woc.work_order_id
and woc.work_order_id = woacct.work_order_id
and t.work_order_id = woa.work_order_id
and t.revision = woa.revision
and t.work_order_id = wem.work_order_id
and t.revision = wem.revision
and t.substitution_id = bv.budget_version_id
group by woc.budget_id, t.substitution_id;

if uf_check_sql("Inserting into budget_amounts") = -1 then return -1	


uf_msg("Updating budget_summary_id",'I')
update budget b
set budget_summary_id = (
	select min(wotbs.budget_summary_id)
	from work_order_control woc, work_order_type_budget_summary wotbs
	where woc.budget_id = b.budget_id
	and woc.work_order_type_id = wotbs.work_order_type_id
	and woc.funding_wo_indicator = 1
	)
where exists (
	select 1 from work_order_control woc, wo_est_load_budgets_temp t
	where t.budget_id = b.budget_id
	and t.budget_id = woc.budget_id
	and woc.work_order_number = b.budget_number
	and woc.funding_wo_indicator = 1
	)
and exists (
	select 1 from pp_system_control_companies pp
	where lower(trim(pp.control_name)) = lower(trim('CAPBUD - WOT TO BUDGET SUMMARY'))
	and lower(trim(pp.control_value)) = lower(trim('yes'))
	and pp.company_id = b.company_id
	);
if uf_check_sql("Updating budget_summary_id") = -1 then return -1



uf_msg("Updating budget_organization_id",'I')
update budget b
set budget_organization_id = (
	select min(wogbo.budget_organization_id)
	from work_order_control woc, work_order_account woa, work_order_group_budget_org wogbo
	where woc.budget_id = b.budget_id
	and woc.work_order_id = woa.work_order_id
	and woa.work_order_grp_id = wogbo.work_order_grp_id
	and woc.funding_wo_indicator = 1
	)
where exists (
	select 1 from work_order_control woc, wo_est_load_budgets_temp t
	where t.budget_id = b.budget_id
	and t.budget_id = woc.budget_id
	and woc.work_order_number = b.budget_number
	and woc.funding_wo_indicator = 1
	)
and exists (
	select 1 from pp_system_control_companies pp
	where lower(trim(pp.control_name)) = lower(trim('CAPBUD - WOG TO BUDGET ORG'))
	and lower(trim(pp.control_value)) = lower(trim('yes'))
	and pp.company_id = b.company_id
	);
if uf_check_sql("Updating budget_organization_id") = -1 then return -1


//
// From this point forward we check the update_header_from_fp flag
//
uf_msg("Updating budget from funding projects",'I')
update budget b
set (description, long_description, department_id, major_location_id, bus_segment_id, resp_person,
	agreemnt_id, afudc_type_id, eligible_for_afudc, eligible_for_cpi, reimbursable_type_id
	) = (
	select min(woc.description), min(woc.long_description), min(woc.department_id), min(woc.major_location_id), min(woc.bus_segment_id), min(woi.users),
		min(woa.agreemnt_id), min(woa.afudc_type_id), max(woa.eligible_for_afudc), max(woa.eligible_for_cpi), min(woa.reimbursable_type_id)
	from work_order_control woc, work_order_initiator woi, work_order_account woa
	where woc.budget_id = b.budget_id
	and woc.work_order_id = woi.work_order_id
	and woc.work_order_id = woa.work_order_id
	and woc.funding_wo_indicator = 1
	)
where exists (
	select 1 from work_order_control woc, wo_est_load_budgets_temp t
	where t.budget_id = b.budget_id
	and t.budget_id = woc.budget_id
	and woc.work_order_number = b.budget_number
	and woc.funding_wo_indicator = 1
	and t.update_header_from_fp = 1
	);
if uf_check_sql("Updating budget from work_order_control") = -1 then return -1


uf_msg("Updating budget company from funding projects",'I')
update budget b
set company_id = (
	select min(woc.company_id)
	from work_order_control woc
	where woc.budget_id = b.budget_id
	and woc.funding_wo_indicator = 1
	)
where exists (
	select 1 from wo_est_load_budgets_temp t
	where t.budget_id = b.budget_id
	and t.update_header_from_fp = 1
	)
and not exists (
	select 1 from budget c, budget d
	where b.budget_id = c.budget_id
	and c.budget_number = d.budget_number
	and c.budget_id <> d.budget_id
	and d.company_id = (
		select min(woc.company_id)
		from work_order_control woc
		where woc.budget_id = c.budget_id
		and woc.funding_wo_indicator = 1
		)
	)
and not exists ( /* ### CDM - Maint 9070 - only update the company if it does not have one of the companies from the underlying FPs */
	select 1 from work_order_control
	where work_order_control.budget_id = b.budget_id
	and work_order_control.funding_wo_indicator = 1
	and work_order_control.company_id = b.company_id
	);
if uf_check_sql("Updating budget.company_id from work_order_control") = -1 then return -1


uf_msg("Updating budget amounts from funding projects",'I')
update budget_amounts ba
set (base_year, escalation_id) = (
	select nvl(min(woa.base_year),min(woacct.base_year)), min(woacct.escalation_id)
	from wo_est_load_budgets_temp t, work_order_control woc, work_order_account woacct, work_order_approval woa, budget_version_fund_proj bvfp
	where t.budget_id = ba.budget_id
	and t.budget_version_id = ba.budget_version_id
	and t.budget_id = woc.budget_id
	and woc.funding_wo_indicator = 1
	and woc.work_order_id = bvfp.work_order_id
	and t.budget_version_id = bvfp.budget_version_id
	and bvfp.active = 1
	and woc.work_order_id = woacct.work_order_id
	and bvfp.work_order_id = woa.work_order_id
	and bvfp.revision = woa.revision
	)
where exists (
	select 1 from wo_est_load_budgets_temp t
	where t.budget_id = ba.budget_id
	and t.budget_version_id = ba.budget_version_id
	and t.update_header_from_fp = 1
	);
if uf_check_sql("Updating budget_amounts") = -1 then return -1


uf_msg("Inserting into budget_class_code",'I')
insert into budget_class_code (budget_id, budget_version_id, class_code_id, "VALUE")
select woc.budget_id, t.substitution_id, wocc.class_code_id, min(wocc."VALUE")
from wo_est_processing_temp t, work_order_control woc, work_order_class_code wocc, class_code cc
where t.work_order_id = woc.work_order_id
and woc.work_order_id = wocc.work_order_id
and wocc.class_code_id = cc.class_code_id
and cc.fp_indicator = 1
and cc.budget_indicator = 1
and exists (
	select 1 from budget b, wo_est_load_budgets_temp t
	where t.budget_id = b.budget_id
	and b.budget_id = woc.budget_id
	and b.budget_number = woc.work_order_number
	and t.update_header_from_fp = 1
	)
group by woc.budget_id, t.substitution_id, wocc.class_code_id;
if uf_check_sql("Inserting into budget_class_code") = -1 then return -1


uf_msg("Updating budget_monthly_data.department_id where NULL",'I')
update budget_monthly_data bmd
set department_id = (
	select b.department_id
	from budget b
	where b.budget_id = bmd.budget_id
	)
where exists (
	select 1 from wo_est_load_budgets_temp t
	where t.budget_id = bmd.budget_id
	and t.budget_version_id = bmd.budget_version_id
	and t.update_header_from_fp = 1
	)
and bmd.department_id is null;
if uf_check_sql("Updating budget_monthly_data.department_id") = -1 then return -1


return 1
end function

public function longlong uf_justification_new_detail (longlong a_tab, longlong a_doc_stage, longlong a_funding_wo_ind);string sqls, func_name, wo_fp, str_arr[]
longlong cst_rtn, long_arr[]

func_name = "uf_justification_new_detail"

if a_funding_wo_ind = 2 then
	wo_fp = 'bi'
elseif a_funding_wo_ind = 1 then
	wo_fp = 'fp'
elseif a_funding_wo_ind = 0 then
	wo_fp = 'wo'
end if

long_arr[1] = a_tab
long_arr[2] = a_doc_stage

uf_msg('Inserting new justification detail','I')

sqls = &
"insert into wo_doc_justification (work_order_id, justification_id, document_id, " +&
"	justification_detail, " +&
"	orig_input_date, " +&
"	orig_input_user, " +&
"	not_changed, " +&
"	calc_locked) " +&
"select temp.work_order_id, v.justification_id, v.document_id, " +&
"	decode((select decode(count(*),0,0,1) from wo_doc_justification wdj where ~"x~".copy_to_new_stage = 1 and wdj.work_order_id = v.work_order_id and wdj.justification_id = v.justification_id and wdj.document_id = (select max(wd.document_id) from wo_documentation wd, wo_doc_justification a where wd.work_order_id = a.work_order_id and wd.document_id = a.document_id and wd.tab_indicator = "+string(a_tab)+" and a.work_order_id = wdj.work_order_id and a.justification_id = wdj.justification_id and a.document_id < v.document_id and wd.document_id = case when temp.substitution_id > 0 then temp.substitution_id else wd.document_id end and wd.est_revision = decode(temp.revision,0,wd.est_revision,temp.revision))), " +&
"		1, (select wdj.justification_detail from wo_doc_justification wdj where wdj.work_order_id = v.work_order_id and wdj.justification_id = v.justification_id and wdj.document_id = (select max(wd.document_id) from wo_documentation wd, wo_doc_justification a where wd.work_order_id = a.work_order_id and wd.document_id = a.document_id and wd.tab_indicator = "+string(a_tab)+" and a.work_order_id = wdj.work_order_id and a.justification_id = wdj.justification_id and a.document_id < v.document_id and wd.document_id = case when temp.substitution_id > 0 then temp.substitution_id else wd.document_id end and wd.est_revision = decode(temp.revision,0,wd.est_revision,temp.revision))), " +&
"		decode(nvl(~"x~".dropdown,0), 1, nvl(~"x~".default_value,'<Select a value>'), 0, decode(lower(~"x~".type_of_data), 'workflow', nvl(~"x~".default_value,'<Select a value>'), 'char', nvl(~"x~".default_value,'<Enter data here>'), 'date', nvl(~"x~".default_value, to_char(sysdate, 'dd-Mon-yy')), '0'))) justification_detail, " +&
"	sysdate orig_input_date, " +&
"	user orig_input_user, " +&
"	decode(lower(trim(pp.control_value)), 'no', 0, ~"x~".allow_edit) not_changed, " +&
"	(select nvl(max(wdj.calc_locked),0) from wo_doc_justification wdj where wdj.work_order_id = v.work_order_id and wdj.justification_id = v.justification_id and wdj.document_id = (select max(wd.document_id) from wo_documentation wd where wd.work_order_id = wdj.work_order_id and wd.tab_indicator = "+string(a_tab)+" and wd.document_id < v.document_id and wd.document_id = case when temp.substitution_id > 0 then temp.substitution_id else wd.document_id end and wd.est_revision = decode(temp.revision,0,wd.est_revision,temp.revision))) calc_locked " +&
"from wo_est_processing_temp temp, " +&
"	( "
if a_funding_wo_ind <> 2 then
	sqls += &
	"		select t.work_order_id, woc.company_id " +&
	"		from wo_est_processing_temp t, work_order_control woc " +&
	"		where t.work_order_id = woc.work_order_id "
else
	sqls += &
	"		select t.work_order_id, woc.company_id " +&
	"		from wo_est_processing_temp t, budget woc " +&
	"		where t.work_order_id = woc.budget_id "
end if
sqls += &
"	) woc, " +&
"	pp_system_control_companies pp, " +&
"	pp_system_control_companies pp2, " +&
"	wo_doc_justification_rules ~"x~",  " +&
"	( " +&
"		select v.work_order_id, v.justification_id, w.document_id " +&
"		from wo_documentation w, " +&
"			( " +&
"				select t.work_order_id, w.justification_id " +&
"				from wo_est_processing_temp t, wo_doc_justification_rules w " +&
"				where w.for_all_wo_types = 1 " +&
"				and w.tab_indicator = "+string(a_tab)+" " +&
"				union "
if a_funding_wo_ind <> 2 then
	sqls += &
	"				select t.work_order_id, a.justification_id justification_id " +&
	"				from wo_est_processing_temp t, work_order_control b, wo_doc_justification_wo_type a, wo_doc_justification_rules c " +&
	"				where t.work_order_id = b.work_order_id " +&
	"				and b.work_order_type_id = a.work_order_type_id " +&
	"				and a.justification_id = c.justification_id " +&
	"				and c.tab_indicator = "+string(a_tab)+" "
else
	sqls += &
	"				select t.work_order_id, a.justification_id justification_id " +&
	"				from wo_est_processing_temp t, budget b, wo_doc_justification_bud_sum a, wo_doc_justification_rules c " +&
	"				where t.work_order_id = b.budget_id " +&
	"				and b.budget_summary_id = a.budget_summary_id " +&
	"				and a.justification_id = c.justification_id " +&
	"				and c.tab_indicator = "+string(a_tab)+" "
end if
sqls += &
"			) v " +&
"		where w.work_order_id = v.work_order_id " +&
"		and w.tab_indicator = "+string(a_tab)+" " +&
"		minus " +&
"		select d.work_order_id, d.justification_id, d.document_id " +&
"		from wo_est_processing_temp t, wo_documentation w, wo_doc_justification d " +&
"		where t.work_order_id = w.work_order_id " +&
"		and w.tab_indicator = "+string(a_tab)+" " +&
"		and w.work_order_id = d.work_order_id " +&
"		and w.document_id = d.document_id " +&
"	) v, ( " +&
"		select   t.work_order_id, max(document_id) max_doc_id " +&
"		from wo_est_processing_temp t, wo_documentation w " +&
"		where t.work_order_id = w.work_order_id " +&
"		and "+string(a_tab)+" = w.tab_indicator " +&
"		group by t.work_order_id " +&
"	) w " +&
"where temp.work_order_id = woc.work_order_id " +&
"and woc.company_id = pp.company_id (+) " +&
"and lower(trim('Funding Just - Use Hide Function')) = lower(trim(pp.control_name (+))) " +&
"and temp.work_order_id = v.work_order_id " +&
"and temp.work_order_id = w.work_order_id " +&
"and v.justification_id = ~"x~".justification_id " +&
"and ~"x~".tab_indicator = "+string(a_tab)+" " +&
"and nvl(~"x~".work_flow_flag,0) in (0,1) " +&
"and woc.company_id = pp2.company_id (+) " +&
"and lower(trim('funding just-protect previous docs')) = lower(trim(pp2.control_name (+))) " +&
"and v.document_id = decode(lower(trim(pp2.control_value)), 'no', v.document_id, w.max_doc_id) "

execute immediate :sqls;

if uf_check_sql('Inserting into wo_doc_justification') <> 1 then return -1

return 1

end function

public function longlong uf_derivation_spread (string a_wo_fp);string sqls, ret, str_error, old_bud_num, bud_num, bud_desc, co_num, co_desc, projects, old_wo_num, wo_num, exp_type, deriv_rollup
longlong i, null_long_arr[], rtn, long_arr[], cnt, check
decimal pct
any results[]
uo_ds_top ds_a


if a_wo_fp = 'fp' then
	projects = 'Funding Projects'
	ret = f_wo_est_processing_protected_revs(1)
else
	projects = 'Work Orders'
	ret = f_wo_est_processing_protected_revs(0)
end if

if ret <> 'OK' then
	uf_msg('ERROR Checking for locked revisions: '+ret,'E')
	return -1
end if

select count(*)
into :check
from wo_est_processing_temp;

if isnull(check) or check = 0 then
	if a_wo_fp = 'fp' then
		uf_msg('No funding projects to process. All funding project revisions are either approved or locked.','E')
	else
		uf_msg('No work orders to process. All work order revisions are either approved or locked.','E')
	end if
	return -1
end if


//
// Checking to ensure all Exp Type / Derivation Rollups being estimated exist in the percentage table
//
i_status_position = 16
uf_msg('Looking for missing derivation spread percentages','I')

select count(*) into :cnt
from (
	select wem.work_order_id, wem.revision, wem.expenditure_type_id, et.description expenditure_type, ect.cr_derivation_rollup, woc.work_order_number
	from wo_est_processing_temp temp, wo_est_monthly wem, expenditure_type et, estimate_charge_type ect, work_order_control woc
	where temp.work_order_id = wem.work_order_id
	and temp.revision = wem.revision
	and wem.expenditure_type_id = et.expenditure_type_id
	and wem.est_chg_type_id = ect.est_chg_type_id
	and temp.work_order_id = woc.work_order_id
	minus
	select pct.work_order_id, pct.revision, pct.expenditure_type_id, et.description expenditure_type, pct.cr_derivation_rollup, woc.work_order_number
	from wo_est_processing_temp temp, wo_est_derivation_pct pct, expenditure_type et, work_order_control woc
	where temp.work_order_id = pct.work_order_id
	and temp.revision = pct.revision
	and pct.expenditure_type_id = et.expenditure_type_id
	and temp.work_order_id = woc.work_order_id
	);

if cnt > 0 then
	sqls = &
		"select work_order_number, expenditure_type, cr_derivation_rollup~r~n"+&
		"from (~r~n"+&
		"	select wem.work_order_id, wem.revision, wem.expenditure_type_id, et.description expenditure_type, ect.cr_derivation_rollup, woc.work_order_number~r~n"+&
		"	from wo_est_processing_temp temp, wo_est_monthly wem, expenditure_type et, estimate_charge_type ect, work_order_control woc~r~n"+&
		"	where temp.work_order_id = wem.work_order_id~r~n"+&
		"	and temp.revision = wem.revision~r~n"+&
		"	and wem.expenditure_type_id = et.expenditure_type_id~r~n"+&
		"	and wem.est_chg_type_id = ect.est_chg_type_id~r~n"+&
		"	and temp.work_order_id = woc.work_order_id~r~n"+&
		"	minus~r~n"+&
		"	select pct.work_order_id, pct.revision, pct.expenditure_type_id, et.description expenditure_type, pct.cr_derivation_rollup, woc.work_order_number~r~n"+&
		"	from wo_est_processing_temp temp, wo_est_derivation_pct pct, expenditure_type et, work_order_control woc~r~n"+&
		"	where temp.work_order_id = pct.work_order_id~r~n"+&
		"	and temp.revision = pct.revision~r~n"+&
		"	and pct.expenditure_type_id = et.expenditure_type_id~r~n"+&
		"	and temp.work_order_id = woc.work_order_id~r~n"+&
		"	)"
	
	ds_a = create uo_ds_top
	ret = f_create_dynamic_ds(ds_a,'grid',sqls,sqlca,true)
	if ret <> 'OK' then
		i_status_position = 0
		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
		//if isvalid(w_progressbar) then
		//	close(w_progressbar)
		//end if
		uf_msg('Error creating datastore for derivation percentages missing from '+lower(projects)+'.','E')
		return -1
	end if
	ds_a.setsort('work_order_number, expenditure_type, cr_derivation_rollup')
	ds_a.sort()
	if ds_a.rowcount() > 0 then
		str_error = 'Derivation Percentages do not exist for the following combinations of Expenditure Types & Derivation Rollups:~r~n'
		old_wo_num = " "
		for i = 1 to ds_a.rowcount()
			wo_num = ds_a.getitemstring(i,'work_order_number')
			exp_type = ds_a.getitemstring(i,"expenditure_type")
			deriv_rollup = ds_a.getitemstring(i,"cr_derivation_rollup")
			if wo_num <> old_wo_num then
				str_error += wo_num+'~r~n'
				old_wo_num = wo_num
			end if
			str_error += '   '+exp_type+' - '+deriv_rollup+'~r~n'
		next
		i_status_position = 0
		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
		//if isvalid(w_progressbar) then
		//	close(w_progressbar)
		//end if
		uf_msg(str_error,'E')
		return -1
	end if
	destroy ds_a
end if


//
// Checking to ensure estimate percentages sum to 100% for each work order / revision / derivation rollup
//
i_status_position = 16
uf_msg('Looking for derivation spread percentages that do not sum to 100%','I')

select count(*) into :cnt
from (
	select pct.work_order_id, pct.revision, nvl(sum(pct.pct),0) pct, woc.work_order_number, pct.cr_derivation_rollup
	from wo_est_processing_temp temp, wo_est_derivation_pct pct, work_order_control woc
	where temp.work_order_id = pct.work_order_id (+)
	and temp.revision = pct.revision (+)
	and temp.work_order_id = woc.work_order_id
	group by pct.work_order_id, pct.revision, woc.work_order_number, pct.cr_derivation_rollup
	having nvl(sum(pct.pct),0) <> 1
	);

if cnt > 0 then
	sqls = &
		"select work_order_number, cr_derivation_rollup, pct~r~n"+&
		"from (~r~n"+&
		"	select pct.work_order_id, pct.revision, nvl(sum(pct.pct),0) pct, woc.work_order_number, pct.cr_derivation_rollup~r~n"+&
		"	from wo_est_processing_temp temp, wo_est_derivation_pct pct, work_order_control woc~r~n"+&
		"	where temp.work_order_id = pct.work_order_id (+)~r~n"+&
		"	and temp.revision = pct.revision (+)~r~n"+&
		"	and temp.work_order_id = woc.work_order_id~r~n"+&
		"	group by pct.work_order_id, pct.revision, woc.work_order_number, pct.cr_derivation_rollup~r~n"+&
		"	having nvl(sum(pct.pct),0) <> 1~r~n"+&
		"	)"
	
	ds_a = create uo_ds_top
	ret = f_create_dynamic_ds(ds_a,'grid',sqls,sqlca,true)
	if ret <> 'OK' then
		i_status_position = 0
		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
		//if isvalid(w_progressbar) then
		//	close(w_progressbar)
		//end if
		uf_msg('Error creating datastore for derivation percentages that do not sum to 100%.','E')
		return -1
	end if
	ds_a.setsort('work_order_number')
	ds_a.sort()
	if ds_a.rowcount() > 0 then
		str_error = 'Derivation Percentages do not sum to 100% for the following '+lower(projects)+' / Derivation Rollups:~r~n'
		for i = 1 to ds_a.rowcount()
			wo_num = ds_a.getitemstring(i,'work_order_number')
			deriv_rollup = ds_a.getitemstring(i, 'cr_derivation_rollup')
			pct = ds_a.getitemdecimal(i,"pct")
			str_error += '   '+wo_num+' / '+deriv_rollup+' - '+string(pct,"0.00##%")+'~r~n'
		next
		i_status_position = 0
		f_progressbar('Title', 'close(w_progressbar)', 100, -1)
		//if isvalid(w_progressbar) then
		//	close(w_progressbar)
		//end if
		uf_msg(str_error,'E')
		return -1
	end if
	destroy ds_a
end if


//
// Save off results after summing estimates and splitting according to percentages
//
i_status_position = 44
uf_msg('Calculating derivation estimate results','I')

delete from wo_est_slide_results_temp;

if uf_check_sql('Cleaning out previous results') <> 1 then return -1

insert into wo_est_slide_results_temp (
	work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	/*factor_id, curve_id,*/ rate_type_id,
	january, february, march, april, may, june,
	july, august, september, october, november, december,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun,
	hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun,
	qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec )
WITH estimates as (
   select t.work_order_id, t.revision, wem.year, wem.expenditure_type_id, wem.est_chg_type_id, wem.department_id, wem.utility_account_id, wem.wo_work_order_id, wem.job_task_id, wem.long_description, wem.budget_id,
	wem.attribute01_id, wem.attribute02_id, wem.attribute03_id, wem.attribute04_id, wem.attribute05_id, wem.attribute06_id, wem.attribute07_id, wem.attribute08_id, wem.attribute09_id, wem.attribute10_id,
      /*wems.factor_id, wems.curve_id,*/ wems.rate_type_id,
      nvl(wem.january,0) january, nvl(wem.february,0) february, nvl(wem.march,0) march, nvl(wem.april,0) april, nvl(wem.may,0) may, nvl(wem.june,0) june, nvl(wem.july,0) july, nvl(wem.august,0) august, nvl(wem.september,0) september, nvl(wem.october,0) october, nvl(wem.november,0) november, nvl(wem.december,0) december,
      nvl(wem.hrs_jan,0) hrs_jan, nvl(wem.hrs_feb,0) hrs_feb, nvl(wem.hrs_mar,0) hrs_mar, nvl(wem.hrs_apr,0) hrs_apr, nvl(wem.hrs_may,0) hrs_may, nvl(wem.hrs_jun,0) hrs_jun, nvl(wem.hrs_jul,0) hrs_jul, nvl(wem.hrs_aug,0) hrs_aug, nvl(wem.hrs_sep,0) hrs_sep, nvl(wem.hrs_oct,0) hrs_oct, nvl(wem.hrs_nov,0) hrs_nov, nvl(wem.hrs_dec,0) hrs_dec,
      nvl(wem.qty_jan,0) qty_jan, nvl(wem.qty_feb,0) qty_feb, nvl(wem.qty_mar,0) qty_mar, nvl(wem.qty_apr,0) qty_apr, nvl(wem.qty_may,0) qty_may, nvl(wem.qty_jun,0) qty_jun, nvl(wem.qty_jul,0) qty_jul, nvl(wem.qty_aug,0) qty_aug, nvl(wem.qty_sep,0) qty_sep, nvl(wem.qty_oct,0) qty_oct, nvl(wem.qty_nov,0) qty_nov, nvl(wem.qty_dec,0) qty_dec,
      to_number(substr(nvl(woa.actuals_month_number,0),1,4)) actuals_year,
      nvl(woa.actuals_month_number,0) actuals_month,
      ect.cr_derivation_rollup
   from wo_est_processing_temp t,
      work_order_control woc,
      wo_est_monthly wem,
      wo_est_monthly_spread wems,
      work_order_approval woa,
      estimate_charge_type ect
   where t.work_order_id = wem.work_order_id
   and t.revision = wem.revision
   and wem.est_monthly_id = wems.est_monthly_id (+)
   and t.work_order_id = woc.work_order_id
   and t.work_order_id = woa.work_order_id
   and t.revision = woa.revision
   and wem.est_chg_type_id = ect.est_chg_type_id
	),
sum_estimates as (
	select work_order_id, revision, year, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
      attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
      /*factor_id, curve_id,*/ rate_type_id,
      january, february, march, april, may, june, july, august, september, october, november, december,
      hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
      qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec,
      actuals_year,
      actuals_month,
      cr_derivation_rollup,
		decode((sum(january) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(january) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(january / sum(january) over(partition by work_order_id, revision, year, cr_derivation_rollup))) jan_pct,
		decode((sum(february) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(february) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(february / sum(february) over(partition by work_order_id, revision, year, cr_derivation_rollup))) feb_pct,
		decode((sum(march) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(march) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(march / sum(march) over(partition by work_order_id, revision, year, cr_derivation_rollup))) mar_pct,
		decode((sum(april) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(april) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(april / sum(april) over(partition by work_order_id, revision, year, cr_derivation_rollup))) apr_pct,
		decode((sum(may) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(may) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(may / sum(may) over(partition by work_order_id, revision, year, cr_derivation_rollup))) may_pct,
		decode((sum(june) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(june) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(june / sum(june) over(partition by work_order_id, revision, year, cr_derivation_rollup))) jun_pct,
		decode((sum(july) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(july) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(july / sum(july) over(partition by work_order_id, revision, year, cr_derivation_rollup))) jul_pct,
		decode((sum(august) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(august) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(august / sum(august) over(partition by work_order_id, revision, year, cr_derivation_rollup))) aug_pct,
		decode((sum(september) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(september) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(september / sum(september) over(partition by work_order_id, revision, year, cr_derivation_rollup))) sep_pct,
		decode((sum(october) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(october) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(october / sum(october) over(partition by work_order_id, revision, year, cr_derivation_rollup))) oct_pct,
		decode((sum(november) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(november) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(november / sum(november) over(partition by work_order_id, revision, year, cr_derivation_rollup))) nov_pct,
		decode((sum(december) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(december) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(december / sum(december) over(partition by work_order_id, revision, year, cr_derivation_rollup))) dec_pct,
		decode((sum(hrs_jan) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_jan) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_jan / sum(hrs_jan) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_jan_pct,
		decode((sum(hrs_feb) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_feb) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_feb / sum(hrs_feb) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_feb_pct,
		decode((sum(hrs_mar) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_mar) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_mar / sum(hrs_mar) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_mar_pct,
		decode((sum(hrs_apr) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_apr) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_apr / sum(hrs_apr) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_apr_pct,
		decode((sum(hrs_may) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_may) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_may / sum(hrs_may) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_may_pct,
		decode((sum(hrs_jun) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_jun) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_jun / sum(hrs_jun) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_jun_pct,
		decode((sum(hrs_jul) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_jul) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_jul / sum(hrs_jul) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_jul_pct,
		decode((sum(hrs_aug) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_aug) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_aug / sum(hrs_aug) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_aug_pct,
		decode((sum(hrs_sep) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_sep) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_sep / sum(hrs_sep) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_sep_pct,
		decode((sum(hrs_oct) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_oct) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_oct / sum(hrs_oct) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_oct_pct,
		decode((sum(hrs_nov) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_nov) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_nov / sum(hrs_nov) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_nov_pct,
		decode((sum(hrs_dec) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(hrs_dec) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(hrs_dec / sum(hrs_dec) over(partition by work_order_id, revision, year, cr_derivation_rollup))) hrs_dec_pct,
		decode((sum(qty_jan) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_jan) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_jan / sum(qty_jan) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_jan_pct,
		decode((sum(qty_feb) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_feb) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_feb / sum(qty_feb) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_feb_pct,
		decode((sum(qty_mar) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_mar) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_mar / sum(qty_mar) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_mar_pct,
		decode((sum(qty_apr) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_apr) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_apr / sum(qty_apr) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_apr_pct,
		decode((sum(qty_may) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_may) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_may / sum(qty_may) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_may_pct,
		decode((sum(qty_jun) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_jun) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_jun / sum(qty_jun) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_jun_pct,
		decode((sum(qty_jul) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_jul) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_jul / sum(qty_jul) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_jul_pct,
		decode((sum(qty_aug) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_aug) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_aug / sum(qty_aug) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_aug_pct,
		decode((sum(qty_sep) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_sep) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_sep / sum(qty_sep) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_sep_pct,
		decode((sum(qty_oct) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_oct) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_oct / sum(qty_oct) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_oct_pct,
		decode((sum(qty_nov) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_nov) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_nov / sum(qty_nov) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_nov_pct,
		decode((sum(qty_dec) over(partition by work_order_id, revision, year, cr_derivation_rollup)),0,(1/(count(qty_dec) over(partition by work_order_id, revision, year, cr_derivation_rollup))),(qty_dec / sum(qty_dec) over(partition by work_order_id, revision, year, cr_derivation_rollup))) qty_dec_pct
	from (
      select work_order_id, revision, year, /*expenditure_type_id, */est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
         attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
         /*factor_id, curve_id,*/ rate_type_id,
         sum(nvl(january,0)) january, sum(nvl(february,0)) february, sum(nvl(march,0)) march, sum(nvl(april,0)) april, sum(nvl(may,0)) may, sum(nvl(june,0)) june, sum(nvl(july,0)) july, sum(nvl(august,0)) august, sum(nvl(september,0)) september, sum(nvl(october,0)) october, sum(nvl(november,0)) november, sum(nvl(december,0)) december,
         sum(nvl(hrs_jan,0)) hrs_jan, sum(nvl(hrs_feb,0)) hrs_feb, sum(nvl(hrs_mar,0)) hrs_mar, sum(nvl(hrs_apr,0)) hrs_apr, sum(nvl(hrs_may,0)) hrs_may, sum(nvl(hrs_jun,0)) hrs_jun, sum(nvl(hrs_jul,0)) hrs_jul, sum(nvl(hrs_aug,0)) hrs_aug, sum(nvl(hrs_sep,0)) hrs_sep, sum(nvl(hrs_oct,0)) hrs_oct, sum(nvl(hrs_nov,0)) hrs_nov, sum(nvl(hrs_dec,0)) hrs_dec,
         sum(nvl(qty_jan,0)) qty_jan, sum(nvl(qty_feb,0)) qty_feb, sum(nvl(qty_mar,0)) qty_mar, sum(nvl(qty_apr,0)) qty_apr, sum(nvl(qty_may,0)) qty_may, sum(nvl(qty_jun,0)) qty_jun, sum(nvl(qty_jul,0)) qty_jul, sum(nvl(qty_aug,0)) qty_aug, sum(nvl(qty_sep,0)) qty_sep, sum(nvl(qty_oct,0)) qty_oct, sum(nvl(qty_nov,0)) qty_nov, sum(nvl(qty_dec,0)) qty_dec,
         actuals_year,
         actuals_month,
         cr_derivation_rollup
      from estimates
		group by work_order_id, revision, year, /*expenditure_type_id, */est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
         attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
         /*factor_id, curve_id,*/ rate_type_id,
			actuals_year,
         actuals_month,
         cr_derivation_rollup
		)
	)
select est.work_order_id, est.revision, est.year, est.expenditure_type_id, est.est_chg_type_id, est.department_id, est.utility_account_id, est.wo_work_order_id, est.job_task_id, est.long_description, est.budget_id,
	est.attribute01_id, est.attribute02_id, est.attribute03_id, est.attribute04_id, est.attribute05_id, est.attribute06_id, est.attribute07_id, est.attribute08_id, est.attribute09_id, est.attribute10_id,
	/*est.factor_id, est.curve_id,*/ est.rate_type_id,
	case when est.year*100+1 <= est.actuals_month then nvl(est.january,0) else 0 end jan,
	case when est.year*100+2 <= est.actuals_month then nvl(est.february,0) else 0 end feb,
	case when est.year*100+3 <= est.actuals_month then nvl(est.march,0) else 0 end mar,
	case when est.year*100+4 <= est.actuals_month then nvl(est.april,0) else 0 end apr,
	case when est.year*100+5 <= est.actuals_month then nvl(est.may,0) else 0 end may,
	case when est.year*100+6 <= est.actuals_month then nvl(est.june,0) else 0 end jun,
	case when est.year*100+7 <= est.actuals_month then nvl(est.july,0) else 0 end jul,
	case when est.year*100+8 <= est.actuals_month then nvl(est.august,0) else 0 end aug,
	case when est.year*100+9 <= est.actuals_month then nvl(est.september,0) else 0 end sep,
	case when est.year*100+10 <= est.actuals_month then nvl(est.october,0) else 0 end oct,
	case when est.year*100+11 <= est.actuals_month then nvl(est.november,0) else 0 end nov,
	case when est.year*100+12 <= est.actuals_month then nvl(est.december,0) else 0 end dec,
	case when est.year*100+1 <= est.actuals_month then nvl(est.hrs_jan,0) else 0 end hrs_jan,
	case when est.year*100+2 <= est.actuals_month then nvl(est.hrs_feb,0) else 0 end hrs_feb,
	case when est.year*100+3 <= est.actuals_month then nvl(est.hrs_mar,0) else 0 end hrs_mar,
	case when est.year*100+4 <= est.actuals_month then nvl(est.hrs_apr,0) else 0 end hrs_apr,
	case when est.year*100+5 <= est.actuals_month then nvl(est.hrs_may,0) else 0 end hrs_may,
	case when est.year*100+6 <= est.actuals_month then nvl(est.hrs_jun,0) else 0 end hrs_jun,
	case when est.year*100+7 <= est.actuals_month then nvl(est.hrs_jul,0) else 0 end hrs_jul,
	case when est.year*100+8 <= est.actuals_month then nvl(est.hrs_aug,0) else 0 end hrs_aug,
	case when est.year*100+9 <= est.actuals_month then nvl(est.hrs_sep,0) else 0 end hrs_sep,
	case when est.year*100+10 <= est.actuals_month then nvl(est.hrs_oct,0) else 0 end hrs_oct,
	case when est.year*100+11 <= est.actuals_month then nvl(est.hrs_nov,0) else 0 end hrs_nov,
	case when est.year*100+12 <= est.actuals_month then nvl(est.hrs_dec,0) else 0 end hrs_dec,
	case when est.year*100+1 <= est.actuals_month then nvl(est.qty_jan,0) else 0 end qty_jan,
	case when est.year*100+2 <= est.actuals_month then nvl(est.qty_feb,0) else 0 end qty_feb,
	case when est.year*100+3 <= est.actuals_month then nvl(est.qty_mar,0) else 0 end qty_mar,
	case when est.year*100+4 <= est.actuals_month then nvl(est.qty_apr,0) else 0 end qty_apr,
	case when est.year*100+5 <= est.actuals_month then nvl(est.qty_may,0) else 0 end qty_may,
	case when est.year*100+6 <= est.actuals_month then nvl(est.qty_jun,0) else 0 end qty_jun,
	case when est.year*100+7 <= est.actuals_month then nvl(est.qty_jul,0) else 0 end qty_jul,
	case when est.year*100+8 <= est.actuals_month then nvl(est.qty_aug,0) else 0 end qty_aug,
	case when est.year*100+9 <= est.actuals_month then nvl(est.qty_sep,0) else 0 end qty_sep,
	case when est.year*100+10 <= est.actuals_month then nvl(est.qty_oct,0) else 0 end qty_oct,
	case when est.year*100+11 <= est.actuals_month then nvl(est.qty_nov,0) else 0 end qty_nov,
	case when est.year*100+12 <= est.actuals_month then nvl(est.qty_dec,0) else 0 end qty_dec
from estimates est
where est.year <= est.actuals_year
UNION ALL
select est.work_order_id, est.revision, est.year, est.expenditure_type_id, est.est_chg_type_id, est.department_id, est.utility_account_id, est.wo_work_order_id, est.job_task_id, est.long_description, est.budget_id,
	est.attribute01_id, est.attribute02_id, est.attribute03_id, est.attribute04_id, est.attribute05_id, est.attribute06_id, est.attribute07_id, est.attribute08_id, est.attribute09_id, est.attribute10_id,
	/*est.factor_id, est.curve_id,*/ est.rate_type_id,
	sum(case when est.year*100+1 > est.actuals_month then est.jan_pct*est_total.pct*nvl(est_total.january,0) else 0 end) jan,
	sum(case when est.year*100+2 > est.actuals_month then est.feb_pct*est_total.pct*nvl(est_total.february,0) else 0 end) feb,
	sum(case when est.year*100+3 > est.actuals_month then est.mar_pct*est_total.pct*nvl(est_total.march,0) else 0 end) mar,
	sum(case when est.year*100+4 > est.actuals_month then est.apr_pct*est_total.pct*nvl(est_total.april,0) else 0 end) apr,
	sum(case when est.year*100+5 > est.actuals_month then est.may_pct*est_total.pct*nvl(est_total.may,0) else 0 end) may,
	sum(case when est.year*100+6 > est.actuals_month then est.jun_pct*est_total.pct*nvl(est_total.june,0) else 0 end) jun,
	sum(case when est.year*100+7 > est.actuals_month then est.jul_pct*est_total.pct*nvl(est_total.july,0) else 0 end) jul,
	sum(case when est.year*100+8 > est.actuals_month then est.aug_pct*est_total.pct*nvl(est_total.august,0) else 0 end) aug,
	sum(case when est.year*100+9 > est.actuals_month then est.sep_pct*est_total.pct*nvl(est_total.september,0) else 0 end) sep,
	sum(case when est.year*100+10 > est.actuals_month then est.oct_pct*est_total.pct*nvl(est_total.october,0) else 0 end) oct,
	sum(case when est.year*100+11 > est.actuals_month then est.nov_pct*est_total.pct*nvl(est_total.november,0) else 0 end) nov,
	sum(case when est.year*100+12 > est.actuals_month then est.dec_pct*est_total.pct*nvl(est_total.december,0) else 0 end) dec,
	sum(case when est.year*100+1 > est.actuals_month then est.hrs_jan_pct*est_total.pct*nvl(est_total.hrs_jan,0) else 0 end) hrs_jan,
	sum(case when est.year*100+2 > est.actuals_month then est.hrs_feb_pct*est_total.pct*nvl(est_total.hrs_feb,0) else 0 end) hrs_feb,
	sum(case when est.year*100+3 > est.actuals_month then est.hrs_mar_pct*est_total.pct*nvl(est_total.hrs_mar,0) else 0 end) hrs_mar,
	sum(case when est.year*100+4 > est.actuals_month then est.hrs_apr_pct*est_total.pct*nvl(est_total.hrs_apr,0) else 0 end) hrs_apr,
	sum(case when est.year*100+5 > est.actuals_month then est.hrs_may_pct*est_total.pct*nvl(est_total.hrs_may,0) else 0 end) hrs_may,
	sum(case when est.year*100+6 > est.actuals_month then est.hrs_jun_pct*est_total.pct*nvl(est_total.hrs_jun,0) else 0 end) hrs_jun,
	sum(case when est.year*100+7 > est.actuals_month then est.hrs_jul_pct*est_total.pct*nvl(est_total.hrs_jul,0) else 0 end) hrs_jul,
	sum(case when est.year*100+8 > est.actuals_month then est.hrs_aug_pct*est_total.pct*nvl(est_total.hrs_aug,0) else 0 end) hrs_aug,
	sum(case when est.year*100+9 > est.actuals_month then est.hrs_sep_pct*est_total.pct*nvl(est_total.hrs_sep,0) else 0 end) hrs_sep,
	sum(case when est.year*100+10 > est.actuals_month then est.hrs_oct_pct*est_total.pct*nvl(est_total.hrs_oct,0) else 0 end) hrs_oct,
	sum(case when est.year*100+11 > est.actuals_month then est.hrs_nov_pct*est_total.pct*nvl(est_total.hrs_nov,0) else 0 end) hrs_nov,
	sum(case when est.year*100+12 > est.actuals_month then est.hrs_dec_pct*est_total.pct*nvl(est_total.hrs_dec,0) else 0 end) hrs_dec,
	sum(case when est.year*100+1 > est.actuals_month then est.qty_jan_pct*est_total.pct*nvl(est_total.qty_jan,0) else 0 end) qty_jan,
	sum(case when est.year*100+2 > est.actuals_month then est.qty_feb_pct*est_total.pct*nvl(est_total.qty_feb,0) else 0 end) qty_feb,
	sum(case when est.year*100+3 > est.actuals_month then est.qty_mar_pct*est_total.pct*nvl(est_total.qty_mar,0) else 0 end) qty_mar,
	sum(case when est.year*100+4 > est.actuals_month then est.qty_apr_pct*est_total.pct*nvl(est_total.qty_apr,0) else 0 end) qty_apr,
	sum(case when est.year*100+5 > est.actuals_month then est.qty_may_pct*est_total.pct*nvl(est_total.qty_may,0) else 0 end) qty_may,
	sum(case when est.year*100+6 > est.actuals_month then est.qty_jun_pct*est_total.pct*nvl(est_total.qty_jun,0) else 0 end) qty_jun,
	sum(case when est.year*100+7 > est.actuals_month then est.qty_jul_pct*est_total.pct*nvl(est_total.qty_jul,0) else 0 end) qty_jul,
	sum(case when est.year*100+8 > est.actuals_month then est.qty_aug_pct*est_total.pct*nvl(est_total.qty_aug,0) else 0 end) qty_aug,
	sum(case when est.year*100+9 > est.actuals_month then est.qty_sep_pct*est_total.pct*nvl(est_total.qty_sep,0) else 0 end) qty_sep,
	sum(case when est.year*100+10 > est.actuals_month then est.qty_oct_pct*est_total.pct*nvl(est_total.qty_oct,0) else 0 end) qty_oct,
	sum(case when est.year*100+11 > est.actuals_month then est.qty_nov_pct*est_total.pct*nvl(est_total.qty_nov,0) else 0 end) qty_nov,
	sum(case when est.year*100+12 > est.actuals_month then est.qty_dec_pct*est_total.pct*nvl(est_total.qty_dec,0) else 0 end) qty_dec
from
	(	
		select est.work_order_id, est.revision, year, pct.expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
			attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
			rate_type_id,
         actuals_year,
         actuals_month,
         est.cr_derivation_rollup,
         jan_pct, feb_pct, mar_pct, apr_pct, may_pct, jun_pct, jul_pct, aug_pct, sep_pct, oct_pct, nov_pct, dec_pct,
         hrs_jan_pct, hrs_feb_pct, hrs_mar_pct, hrs_apr_pct, hrs_may_pct, hrs_jun_pct, hrs_jul_pct, hrs_aug_pct, hrs_sep_pct, hrs_oct_pct, hrs_nov_pct, hrs_dec_pct,
         qty_jan_pct, qty_feb_pct, qty_mar_pct, qty_apr_pct, qty_may_pct, qty_jun_pct, qty_jul_pct, qty_aug_pct, qty_sep_pct, qty_oct_pct, qty_nov_pct, qty_dec_pct
		from sum_estimates est, wo_est_derivation_pct pct
		where est.work_order_id = pct.work_order_id
		and est.revision = pct.revision
		and est.cr_derivation_rollup = pct.cr_derivation_rollup
	) est,
	(	select est.work_order_id, est.revision, est.year, pct.expenditure_type_id, pct.cr_derivation_rollup, nvl(pct.pct,0) pct,
			sum(nvl(est.january,0)) january, sum(nvl(est.february,0)) february, sum(nvl(est.march,0)) march, sum(nvl(est.april,0)) april, sum(nvl(est.may,0)) may, sum(nvl(est.june,0)) june,
			sum(nvl(est.july,0)) july, sum(nvl(est.august,0)) august, sum(nvl(est.september,0)) september, sum(nvl(est.october,0)) october, sum(nvl(est.november,0)) november, sum(nvl(est.december,0)) december,
			sum(nvl(est.hrs_jan,0)) hrs_jan, sum(nvl(est.hrs_feb,0)) hrs_feb, sum(nvl(est.hrs_mar,0)) hrs_mar, sum(nvl(est.hrs_apr,0)) hrs_apr, sum(nvl(est.hrs_may,0)) hrs_may, sum(nvl(est.hrs_jun,0)) hrs_jun,
			sum(nvl(est.hrs_jul,0)) hrs_jul, sum(nvl(est.hrs_aug,0)) hrs_aug, sum(nvl(est.hrs_sep,0)) hrs_sep, sum(nvl(est.hrs_oct,0)) hrs_oct, sum(nvl(est.hrs_nov,0)) hrs_nov, sum(nvl(est.hrs_dec,0)) hrs_dec,
			sum(nvl(est.qty_jan,0)) qty_jan, sum(nvl(est.qty_feb,0)) qty_feb, sum(nvl(est.qty_mar,0)) qty_mar, sum(nvl(est.qty_apr,0)) qty_apr, sum(nvl(est.qty_may,0)) qty_may, sum(nvl(est.qty_jun,0)) qty_jun,
			sum(nvl(est.qty_jul,0)) qty_jul, sum(nvl(est.qty_aug,0)) qty_aug, sum(nvl(est.qty_sep,0)) qty_sep, sum(nvl(est.qty_oct,0)) qty_oct, sum(nvl(est.qty_nov,0)) qty_nov, sum(nvl(est.qty_dec,0)) qty_dec
		from sum_estimates est, wo_est_derivation_pct pct
		where est.work_order_id = pct.work_order_id
		and est.revision = pct.revision
		and est.cr_derivation_rollup = pct.cr_derivation_rollup
		group by est.work_order_id, est.revision, est.year, pct.expenditure_type_id, pct.cr_derivation_rollup, pct.pct
	) est_total
where est_total.work_order_id = est.work_order_id
and est_total.revision = est.revision
and est_total.year = est.year
and est_total.expenditure_type_id = est.expenditure_type_id
and est_total.cr_derivation_rollup = est.cr_derivation_rollup
and est.year >= est.actuals_year
group by est.work_order_id, est.revision, est.year, est.expenditure_type_id, est.est_chg_type_id, est.department_id, est.utility_account_id, est.wo_work_order_id, est.job_task_id, est.long_description, est.budget_id,
	est.attribute01_id, est.attribute02_id, est.attribute03_id, est.attribute04_id, est.attribute05_id, est.attribute06_id, est.attribute07_id, est.attribute08_id, est.attribute09_id, est.attribute10_id,
	/*est.factor_id, est.curve_id,*/ est.rate_type_id;

if uf_check_sql('Saving off estimate derivation results') <> 1 then return -1

//
// Backfill the est_monthly_id's for unique estimates
//
i_status_position = 58
uf_msg('Backfilling estimate ids','I')

update wo_est_slide_results_temp
set est_monthly_id = pwrplant1.nextval;

if uf_check_sql('Backfilling est_monthly_ids on estimate results') <> 1 then return -1

update wo_est_slide_results_temp a
set a.est_monthly_id = (
	select min(b.est_monthly_id)
	from wo_est_slide_results_temp b
	where b.work_order_id = a.work_order_id
	and b.revision = a.revision
	and b.expenditure_type_id = a.expenditure_type_id
	and b.est_chg_type_id = a.est_chg_type_id
	and nvl(b.department_id,0) = nvl(a.department_id,0)
	and nvl(b.utility_account_id,0) = nvl(a.utility_account_id,0)
	and nvl(b.wo_work_order_id,0) = nvl(a.wo_work_order_id,0) // ### CDM - Maint 8402 - Add WO as an Estimate Attribute on FP Estimates
	and nvl(b.job_task_id,'*') = nvl(a.job_task_id,'*')
	and nvl(b.long_description,'*') = nvl(a.long_description,'*')
	and nvl(b.budget_id,0) = nvl(a.budget_id,0)
	and nvl(b.attribute01_id,'*') = nvl(a.attribute01_id,'*')
	and nvl(b.attribute02_id,'*') = nvl(a.attribute02_id,'*')
	and nvl(b.attribute03_id,'*') = nvl(a.attribute03_id,'*')
	and nvl(b.attribute04_id,'*') = nvl(a.attribute04_id,'*')
	and nvl(b.attribute05_id,'*') = nvl(a.attribute05_id,'*')
	and nvl(b.attribute06_id,'*') = nvl(a.attribute06_id,'*')
	and nvl(b.attribute07_id,'*') = nvl(a.attribute07_id,'*')
	and nvl(b.attribute08_id,'*') = nvl(a.attribute08_id,'*')
	and nvl(b.attribute09_id,'*') = nvl(a.attribute09_id,'*')
	and nvl(b.attribute10_id,'*') = nvl(a.attribute10_id,'*')
	and nvl(b.factor_id,0) = nvl(a.factor_id,0)
	and nvl(b.curve_id,0) = nvl(a.curve_id,0)
	and nvl(b.rate_type_id,0) = nvl(a.rate_type_id,0)
	);

if uf_check_sql('Updating unique est_monthly_ids on estimate results') <> 1 then return -1

//
// NEED TO UPDATE ROUNDING ERRORS
//
i_status_position = 72
uf_msg('Updating rounding errors','I')

update wo_est_slide_results_temp a
set (
	a.january, a.february, a.march, a.april, a.may, a.june,
	a.july, a.august, a.september, a.october, a.november, a.december,
	a.hrs_jan, a.hrs_feb, a.hrs_mar, a.hrs_apr, a.hrs_may, a.hrs_jun,
	a.hrs_jul, a.hrs_aug, a.hrs_sep, a.hrs_oct, a.hrs_nov, a.hrs_dec,
	a.qty_jan, a.qty_feb, a.qty_mar, a.qty_apr, a.qty_may, a.qty_jun,
	a.qty_jul, a.qty_aug, a.qty_sep, a.qty_oct, a.qty_nov, a.qty_dec ) =
	(
	select nvl(a.january,0) + d.jan jan, nvl(a.february,0) + d.feb feb, nvl(a.march,0) + d.mar mar, nvl(a.april,0) + d.apr apr, nvl(a.may,0) + d.may may, nvl(a.june,0) + d.jun jun,
		nvl(a.july,0) + d.jul jul, nvl(a.august,0) + d.aug aug, nvl(a.september,0) + d.sep sep, nvl(a.october,0) + d.oct oct, nvl(a.november,0) + d.nov nov, nvl(a.december,0) + d.dec dec,
		nvl(a.hrs_jan,0) + d.hrs_jan hrs_jan, nvl(a.hrs_feb,0) + d.hrs_feb hrs_feb, nvl(a.hrs_mar,0) + d.hrs_mar hrs_mar, nvl(a.hrs_apr,0) + d.hrs_apr hrs_apr, nvl(a.hrs_may,0) + d.hrs_may hrs_may, nvl(a.hrs_jun,0) + d.hrs_jun hrs_jun,
		nvl(a.hrs_jul,0) + d.hrs_jul hrs_jul, nvl(a.hrs_aug,0) + d.hrs_aug hrs_aug, nvl(a.hrs_sep,0) + d.hrs_sep hrs_sep, nvl(a.hrs_oct,0) + d.hrs_oct hrs_oct, nvl(a.hrs_nov,0) + d.hrs_nov hrs_nov, nvl(a.hrs_dec,0) + d.hrs_dec hrs_dec,
		nvl(a.qty_jan,0) + d.qty_jan qty_jan, nvl(a.qty_feb,0) + d.qty_feb qty_feb, nvl(a.qty_mar,0) + d.qty_mar qty_mar, nvl(a.qty_apr,0) + d.qty_apr qty_apr, nvl(a.qty_may,0) + d.qty_may qty_may, nvl(a.qty_jun,0) + d.qty_jun qty_jun,
		nvl(a.qty_jul,0) + d.qty_jul qty_jul, nvl(a.qty_aug,0) + d.qty_aug qty_aug, nvl(a.qty_sep,0) + d.qty_sep qty_sep, nvl(a.qty_oct,0) + d.qty_oct qty_oct, nvl(a.qty_nov,0) + d.qty_nov qty_nov, nvl(a.qty_dec,0) + d.qty_dec qty_dec
	from (
		select c.work_order_id, c.revision, c.year, c.cr_derivation_rollup,
			sum(nvl(c.jan,0)) jan, sum(nvl(c.feb,0)) feb, sum(nvl(c.mar,0)) mar, sum(nvl(c.apr,0)) apr, sum(nvl(c.may,0)) may, sum(nvl(c.jun,0)) jun,
			sum(nvl(c.jul,0)) jul, sum(nvl(c.aug,0)) aug, sum(nvl(c.sep,0)) sep, sum(nvl(c.oct,0)) oct, sum(nvl(c.nov,0)) nov, sum(nvl(c.dec,0)) dec, 
			sum(nvl(c.hrs_jan,0)) hrs_jan, sum(nvl(c.hrs_feb,0)) hrs_feb, sum(nvl(c.hrs_mar,0)) hrs_mar, sum(nvl(c.hrs_apr,0)) hrs_apr, sum(nvl(c.hrs_may,0)) hrs_may, sum(nvl(c.hrs_jun,0)) hrs_jun,
			sum(nvl(c.hrs_jul,0)) hrs_jul, sum(nvl(c.hrs_aug,0)) hrs_aug, sum(nvl(c.hrs_sep,0)) hrs_sep, sum(nvl(c.hrs_oct,0)) hrs_oct, sum(nvl(c.hrs_nov,0)) hrs_nov, sum(nvl(c.hrs_dec,0)) hrs_dec,
			sum(nvl(c.qty_jan,0)) qty_jan, sum(nvl(c.qty_feb,0)) qty_feb, sum(nvl(c.qty_mar,0)) qty_mar, sum(nvl(c.qty_apr,0)) qty_apr, sum(nvl(c.qty_may,0)) qty_may, sum(nvl(c.qty_jun,0)) qty_jun,
			sum(nvl(c.qty_jul,0)) qty_jul, sum(nvl(c.qty_aug,0)) qty_aug, sum(nvl(c.qty_sep,0)) qty_sep, sum(nvl(c.qty_oct,0)) qty_oct, sum(nvl(c.qty_nov,0)) qty_nov, sum(nvl(c.qty_dec,0)) qty_dec
		from (
			select wem.work_order_id, wem.revision, wem.year, ect.cr_derivation_rollup,
				sum(nvl(wem.january,0)) jan, sum(nvl(wem.february,0)) feb, sum(nvl(wem.march,0)) mar, sum(nvl(wem.april,0)) apr, sum(nvl(wem.may,0)) may, sum(nvl(wem.june,0)) jun,
				sum(nvl(wem.july,0)) jul, sum(nvl(wem.august,0)) aug, sum(nvl(wem.september,0)) sep, sum(nvl(wem.october,0)) oct, sum(nvl(wem.november,0)) nov, sum(nvl(wem.december,0)) dec,
				sum(nvl(wem.hrs_jan,0)) hrs_jan, sum(nvl(wem.hrs_feb,0)) hrs_feb, sum(nvl(wem.hrs_mar,0)) hrs_mar, sum(nvl(wem.hrs_apr,0)) hrs_apr, sum(nvl(wem.hrs_may,0)) hrs_may, sum(nvl(wem.hrs_jun,0)) hrs_jun,
				sum(nvl(wem.hrs_jul,0)) hrs_jul, sum(nvl(wem.hrs_aug,0)) hrs_aug, sum(nvl(wem.hrs_sep,0)) hrs_sep, sum(nvl(wem.hrs_oct,0)) hrs_oct, sum(nvl(wem.hrs_nov,0)) hrs_nov, sum(nvl(wem.hrs_dec,0)) hrs_dec,
				sum(nvl(wem.qty_jan,0)) qty_jan, sum(nvl(wem.qty_feb,0)) qty_feb, sum(nvl(wem.qty_mar,0)) qty_mar, sum(nvl(wem.qty_apr,0)) qty_apr, sum(nvl(wem.qty_may,0)) qty_may, sum(nvl(wem.qty_jun,0)) qty_jun,
				sum(nvl(wem.qty_jul,0)) qty_jul, sum(nvl(wem.qty_aug,0)) qty_aug, sum(nvl(wem.qty_sep,0)) qty_sep, sum(nvl(wem.qty_oct,0)) qty_oct, sum(nvl(wem.qty_nov,0)) qty_nov, sum(nvl(wem.qty_dec,0)) qty_dec
			from 	wo_est_processing_temp t,
				wo_est_monthly wem,
				estimate_charge_type ect
			where t.work_order_id = wem.work_order_id
			and t.revision = wem.revision
			and wem.est_chg_type_id = ect.est_chg_type_id
			group by wem.work_order_id, wem.revision, wem.year, ect.cr_derivation_rollup
			union all
			select wes.work_order_id, wes.revision, wes.year, ect.cr_derivation_rollup,
				-1*sum(nvl(wes.january,0)) jan, -1*sum(nvl(wes.february,0)) feb, -1*sum(nvl(wes.march,0)) mar, -1*sum(nvl(wes.april,0)) apr, -1*sum(nvl(wes.may,0)) may, -1*sum(nvl(wes.june,0)) jun,
				-1*sum(nvl(wes.july,0)) jul, -1*sum(nvl(wes.august,0)) aug, -1*sum(nvl(wes.september,0)) sep, -1*sum(nvl(wes.october,0)) oct, -1*sum(nvl(wes.november,0)) nov, -1*sum(nvl(wes.december,0)) dec, 
				-1*sum(nvl(wes.hrs_jan,0)) hrs_jan, -1*sum(nvl(wes.hrs_feb,0)) hrs_feb, -1*sum(nvl(wes.hrs_mar,0)) hrs_mar, -1*sum(nvl(wes.hrs_apr,0)) hrs_apr, -1*sum(nvl(wes.hrs_may,0)) hrs_may, -1*sum(nvl(wes.hrs_jun,0)) hrs_jun,
				-1*sum(nvl(wes.hrs_jul,0)) hrs_jul, -1*sum(nvl(wes.hrs_aug,0)) hrs_aug, -1*sum(nvl(wes.hrs_sep,0)) hrs_sep, -1*sum(nvl(wes.hrs_oct,0)) hrs_oct, -1*sum(nvl(wes.hrs_nov,0)) hrs_nov, -1*sum(nvl(wes.hrs_dec,0)) hrs_dec,
				-1*sum(nvl(wes.qty_jan,0)) qty_jan, -1*sum(nvl(wes.qty_feb,0)) qty_feb, -1*sum(nvl(wes.qty_mar,0)) qty_mar, -1*sum(nvl(wes.qty_apr,0)) qty_apr, -1*sum(nvl(wes.qty_may,0)) qty_may, -1*sum(nvl(wes.qty_jun,0)) qty_jun,
				-1*sum(nvl(wes.qty_jul,0)) qty_jul, -1*sum(nvl(wes.qty_aug,0)) qty_aug, -1*sum(nvl(wes.qty_sep,0)) qty_sep, -1*sum(nvl(wes.qty_oct,0)) qty_oct, -1*sum(nvl(wes.qty_nov,0)) qty_nov, -1*sum(nvl(wes.qty_dec,0)) qty_dec
			from wo_est_slide_results_temp wes, estimate_charge_type ect
			where wes.est_chg_type_id = ect.est_chg_type_id
			group by wes.work_order_id, wes.revision, wes.year, wes.year, ect.cr_derivation_rollup
			) c
		group by c.work_order_id, c.revision, c.year, c.cr_derivation_rollup
		) d,
		estimate_charge_type ect
	where ect.est_chg_type_id = a.est_chg_type_id
	and d.work_order_id = a.work_order_id
	and d.revision = a.revision
	and d.year = a.year
	and d.cr_derivation_rollup = ect.cr_derivation_rollup
	)
where a.est_monthly_id in (
	select min(wes.est_monthly_id)
	from wo_est_slide_results_temp wes, estimate_charge_type ect, estimate_charge_type ect_outside
	where wes.est_chg_type_id = ect.est_chg_type_id
	and wes.work_order_id = a.work_order_id
	and wes.revision = a.revision
	and wes.year = a.year
	and ect.cr_derivation_rollup = ect_outside.cr_derivation_rollup
	and ect_outside.est_chg_type_id = a.est_chg_type_id
	);

if uf_check_sql('Updating rounding on errors on estimates') <> 1 then return -1

//
// Delete estimates from projects
//
i_status_position = 86
uf_msg('Replacing estimates with derivation results','I')

delete from wo_est_monthly_spread wems
where exists (
	select 1 from wo_est_monthly wem
	where wem.est_monthly_id = wems.est_monthly_id
	and exists (
		select 1 from wo_est_processing_temp t
		where t.work_order_id = wem.work_order_id
		and t.revision = wem.revision
		)
	);

if uf_check_sql('Deleting prior est monthly ids') <> 1 then return -1

delete from wo_est_monthly wem
where exists (
	select 1 from wo_est_processing_temp t
	where t.work_order_id = wem.work_order_id
	and t.revision = wem.revision
	);

if uf_check_sql('Deleting prior estimates') <> 1 then return -1

//
// Replace with new estimates on projects
//
insert into wo_est_monthly (
	est_monthly_id, work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june,
	july, august, september, october, november, december,
	total,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun,
	hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,
	hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun,
	qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec,
	qty_total )
select est_monthly_id, work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	sum(january), sum(february), sum(march), sum(april), sum(may), sum(june),
	sum(july), sum(august), sum(september), sum(october), sum(november), sum(december),
	sum(january + february + march + april + may + june + july + august + september + october + november + december),
	sum(hrs_jan), sum(hrs_feb), sum(hrs_mar), sum(hrs_apr), sum(hrs_may), sum(hrs_jun),
	sum(hrs_jul), sum(hrs_aug), sum(hrs_sep), sum(hrs_oct), sum(hrs_nov), sum(hrs_dec),
	sum(hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec),
	sum(qty_jan), sum(qty_feb), sum(qty_mar), sum(qty_apr), sum(qty_may), sum(qty_jun),
	sum(qty_jul), sum(qty_aug), sum(qty_sep), sum(qty_oct), sum(qty_nov), sum(qty_dec),
	sum(qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec)
from wo_est_slide_results_temp
group by est_monthly_id, work_order_id, revision, year, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, wo_work_order_id, job_task_id, long_description, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id;

if uf_check_sql('Inserting new estimates') <> 1 then return -1

insert into wo_est_monthly_spread (
	est_monthly_id, factor_id, curve_id, rate_type_id )
select distinct est_monthly_id, factor_id, curve_id, rate_type_id
from wo_est_slide_results_temp;

if uf_check_sql('Inserting new est monthly ids') <> 1 then return -1

i_status_position = 100
uf_msg('Done!','I')
f_progressbar('Title', 'close(w_progressbar)', 100, -1)
//if isvalid(w_progressbar) then
//	close(w_progressbar)
//end if

return 1

end function

public function longlong uf_load_wo_to_fp ();longlong long_arr[], cst_rtn
string str_arr[], func_name, a_wo_fp, ret, sqls

func_name = 'uf_load_wo_to_fp'
a_wo_fp = 'fp'


//
// Remove any revisions that are locked or uneditable
//
uf_msg('Checking for locked revisions','I')
ret = f_wo_est_processing_protected_revs(1) 
if ret <> 'OK' then
	uf_msg('ERROR Checking for locked revisions - '+ret,'E')
	return -1
end if


//
// Put all revisions into the temp table
//
delete from wo_est_update_with_act_temp;

uf_msg('Inserting revisions into temp table','I')

// Use the include_wo/substituion_id field as the actuals_month_number
insert into wo_est_update_with_act_temp (work_order_id, revision, include_wo)
select work_order_id, revision, substitution_id
from wo_est_processing_temp;

if uf_check_sql('Inserting into wo_est_update_with_act_temp') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')

// Analyze table after insert
sqlca.analyze_table('wo_est_update_with_act_temp')


//
// Capture "job task rollup" system control
//
uf_msg('Updating temp table for job task rollup','I')
update wo_est_update_with_act_temp z
set jt_rollup = (
	select nvl(max(to_number(b.control_value)),0)
	from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('FP EST - job task rollup'))
	and a.work_order_id = z.work_order_id
	)
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('FP EST - job task rollup'))
	and a.work_order_id = z.work_order_id
	and upper(trim(b.control_value)) in (
		select to_char(class_code_id) from class_code
		)
	)
;

if uf_check_sql('Updating wo_est_update_with_act_temp.jt_rollup') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Capture "job task summary rollup" system control
//
uf_msg('Updating temp table for job task summary rollup','I')
update wo_est_update_with_act_temp z
set jt_rollup_summary = 1, jt_rollup = 0
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('Update w Act-JobTask Summ Rollup'))
	and a.work_order_id = z.work_order_id
	and upper(trim(b.control_value)) = 'YES'
	)
;

if uf_check_sql('Updating wo_est_update_with_act_temp.jt_rollup_summary') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Capture "plant class" system control
//
uf_msg('Updating temp table for plant class translation','I')
update wo_est_update_with_act_temp z
set ua_plant_class = 1
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('Utility Account - Plant Class'))
	and a.work_order_id = z.work_order_id
	and a.funding_wo_indicator = 1
	and substr(upper(trim(b.control_value)),1,1) = 'Y'
	)
;

if uf_check_sql('Updating wo_est_update_with_act_temp.ua_plant_class') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Capture "date change" system control
//
uf_msg('Updating temp table for expanding dates','I')
update wo_est_update_with_act_temp z
set always_update_dates = 1
where exists (
	select 1 from work_order_control a, pp_system_control_companies b
	where a.company_id = b.company_id
	and upper(trim(b.control_name)) = upper(trim('WOEST - FP DATE CHANGE'))
	and a.work_order_id = z.work_order_id
	and instr(upper(trim(b.control_value)),upper(trim('always update'))) > 0
	);

if uf_check_sql('Updating wo_est_update_with_act_temp.always_update_dates') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


sqlca.analyze_table('wo_est_update_with_act_temp')


//
// Get the min start month and max complete month
//
delete from wo_est_actuals_temp;

if uf_check_sql('Deleting from wo_est_actuals_temp') <> 1 then return -1

uf_msg('Inserting actuals range into temp table','I')
insert into wo_est_actuals_temp (work_order_id, min_month, max_month)
select temp.work_order_id, to_char(min(woa.est_start_date),'yyyymm') min_date, to_char(max(woa.est_complete_date),'yyyymm') max_date
from work_order_approval woa, work_order_control woc, wo_est_update_with_act_temp temp
where temp.work_order_id = woc.funding_wo_id
and woc.funding_wo_indicator = 0
and woc.work_order_id = woa.work_order_id
and woa.revision = (
	select max(a.revision) from work_order_approval a
	where a.work_order_id = woc.work_order_id
	)
and woc.wo_status_id not in (8)
group by temp.work_order_id;

if uf_check_sql('Inserting actuals date range into wo_est_actuals_temp') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')

// Analyze the table after insert
sqlca.analyze_table('wo_est_actuals_temp')


//
// Expand dates where applicable - start date
//
uf_msg('Expanding start date for early charges','I')
update work_order_approval a
set a.est_start_date = (
	select to_date(min_month,'yyyymm')
	from wo_est_actuals_temp temp
	where a.work_order_id = temp.work_order_id
	)
where (a.work_order_id, a.revision) in (
	select b.work_order_id, b.revision
	from wo_est_actuals_temp temp, wo_est_update_with_act_temp b
	where temp.work_order_id = b.work_order_id
	and nvl(to_char(a.est_start_date,'yyyymm'),'999999') > temp.min_month
	);

if uf_check_sql('Expanding dates on work_order_approval (start date)') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')

//
// Expand dates where applicable - complete date
//
uf_msg('Expanding complete date for late charges','I')
update work_order_approval a
set a.est_complete_date = (
	select to_date(max_month,'yyyymm')
	from wo_est_actuals_temp temp
	where a.work_order_id = temp.work_order_id
	)
where (a.work_order_id, a.revision) in (
	select b.work_order_id, b.revision
	from wo_est_actuals_temp temp, wo_est_update_with_act_temp b
	where temp.work_order_id = b.work_order_id
	and nvl(to_char(a.est_complete_date,'yyyymm'),'000000') < temp.max_month
	);

if uf_check_sql('Expanding dates on work_order_approval (complete date)') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')

//
// Propagate the updated dates to work order control
//
uf_msg('Propagating dates back to headers','I')
update work_order_control woc
set est_start_date = (
	select woa.est_start_date from work_order_approval woa, wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	and woa.work_order_id = woc.work_order_id
	),
est_complete_date = (
	select woa.est_complete_date from work_order_approval woa, wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	and woa.work_order_id = woc.work_order_id
	)
where exists (
	select 1 from work_order_approval woa, wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	and temp.work_order_id = woc.work_order_id
	and temp.always_update_dates = 1
	and	(	woc.est_start_date <> woa.est_start_date
				or
				woc.est_complete_date <> woa.est_complete_date
			)
	);

if uf_check_sql('Updating dates back to work_order_control') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Summarize estimates from work orders
//
delete from wo_est_slide_results_temp;

uf_msg('Summarizing estimates from work orders','I')
sqls = &
	"insert into wo_est_slide_results_temp ( "+&
	"   est_monthly_id, "+&
	"   work_order_id, "+&
	"   revision, "+&
	"   year, "+&
	"   expenditure_type_id, "+&
	"   est_chg_type_id, "+&
	"   wo_work_order_id, "+&
	"   job_task_id, "+&
	"   utility_account_id, "+&
	"   department_id, "+&
	"   long_description, "+&
	"	budget_id, "+&
	"	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, "+&
	"   factor_id, "+&
	"   curve_id, "+&
	"   rate_type_id, "+&
	"   substitution_id, "+&
	"   january, february, march, april, may, june, july, august, september, october, november, december, total, "+&
	"   hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total, "+&
	"   qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total "+&
	"   ) "+&
	"select "+&
	"   wem.est_monthly_id work_order_id, "+&
	"   temp.work_order_id work_order_id, "+&
	"   temp.revision revision, "+&
	"   wem.year work_order_id, "+&
	"   wem.expenditure_type_id, "+&
	"   ect.funding_chg_type est_chg_type_id, "+&
	"   wem.work_order_id wo_work_order_id, "+&
	"   decode(temp.jt_rollup_summary,1,jtl.job_task_summary,decode(nvl(temp.jt_rollup,0),0,wem.job_task_id,jt_wocc.value)) job_task_id, "+&
	"   decode(temp.ua_plant_class,1,bpc.budget_plant_class_id,wem.utility_account_id) utility_account_id, "+&
	"   wem.department_id department_id, "+&
	"   wem.long_description, "+&
	"	wem.budget_id, "+&
	"	wem.attribute01_id, wem.attribute02_id, wem.attribute03_id, wem.attribute04_id, wem.attribute05_id, wem.attribute06_id, wem.attribute07_id, wem.attribute08_id, wem.attribute09_id, wem.attribute10_id, "+&
	"   wems.factor_id, "+&
	"   wems.curve_id, "+&
	"   wems.rate_type_id, "+&
	"   wem.substitution_id, "+&
	"   wem.january, wem.february, wem.march, wem.april, wem.may, wem.june, wem.july, wem.august, wem.september, wem.october, wem.november, wem.december, wem.total, "+&
	"   wem.hrs_jan, wem.hrs_feb, wem.hrs_mar, wem.hrs_apr, wem.hrs_may, wem.hrs_jun, wem.hrs_jul, wem.hrs_aug, wem.hrs_sep, wem.hrs_oct, wem.hrs_nov, wem.hrs_dec, wem.hrs_total, "+&
	"   wem.qty_jan, wem.qty_feb, wem.qty_mar, wem.qty_apr, wem.qty_may, wem.qty_jun, wem.qty_jul, wem.qty_aug, wem.qty_sep, wem.qty_oct, wem.qty_nov, wem.qty_dec, wem.qty_total "+&
	"from wo_est_update_with_act_temp temp, "+&
	"   (   select woc.*, t.jt_rollup "+&
	"      from work_order_control woc, wo_est_update_with_act_temp t "+&
	"      where woc.funding_wo_id = t.work_order_id "+&
	"   ) woc, "+&
	"   wo_est_monthly wem, "+&
	"   wo_est_monthly_spread wems, "+&
	"   estimate_charge_type ect, "+&
	"   work_order_class_code jt_wocc, "+&
	"   job_task_list jtl, "+&
	"   (   select budget_plant_class_id, utility_account_id "+&
	"      from budget_plant_class bpc, utility_account ua "+&
	"      where bpc.external_account_code = ua.external_account_code "+&
	"   ) bpc "+&
	"where temp.work_order_id = woc.funding_wo_id "+&
	"and woc.work_order_id = wem.work_order_id "+&
	"and wem.revision = ( "+&
	"   select max(a.revision) from work_order_approval a "+&
	"   where a.work_order_id = woc.work_order_id "+&
	"   ) "+&
	"and wem.est_monthly_id = wems.est_monthly_id (+) "+&
	"and wem.est_chg_type_id = ect.est_chg_type_id "+&
	"and ect.funding_chg_type is not null "+&
	"and woc.work_order_id = jt_wocc.work_order_id (+) "+&
	"and woc.jt_rollup = jt_wocc.class_code_id (+) "+&
	"and wem.job_task_id = jtl.job_task_id (+) "+&
	"and wem.utility_account_id = bpc.utility_account_id (+) "

sqls = f_sql_add_hint(sqls, 'uf_load_wo_to_fp_10')

execute immediate :sqls;

if uf_check_sql('Summarizing estimates from work orders') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')


//
// Delete existing estimate spread records
//
uf_msg('Deleting existing estimate spread records','I')

delete from wo_est_monthly_spread
where est_monthly_id in (
	select est_monthly_id
	from wo_est_monthly
	where (work_order_id, revision) in (
		select work_order_id, revision
		from wo_est_update_with_act_temp
		)
	);

if uf_check_sql('Deleting existing estimate spread records') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records deleted','I')


//
// Delete existing estimate records
//
uf_msg('Deleting existing estimate records','I')

delete from wo_est_monthly
where (work_order_id, revision) in (
	select work_order_id, revision
	from wo_est_update_with_act_temp
	);

if uf_check_sql('Deleting existing estimate records') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records deleted','I')


//
// Translate est_monthly_ids
//
delete from wo_estimate_class_code_temp;

insert into wo_estimate_class_code_temp (estimate_id, new_estimate_id)
select est_monthly_id, pwrplant1.nextval
from (
	select distinct est_monthly_id
	from wo_est_slide_results_temp
	);

if uf_check_sql('Generating new est_monthly_ids') <> 1 then return -1


//
// Insert new estimate records
//
uf_msg('Inserting new estimate records','I')

insert into wo_est_monthly (
	est_monthly_id, work_order_id, revision, year,
	expenditure_type_id, est_chg_type_id, wo_work_order_id, job_task_id, utility_account_id, department_id, long_description, substitution_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june, july, august, september, october, november, december, total,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total
	)
select temp.new_estimate_id, work_order_id, revision, year,
	expenditure_type_id, est_chg_type_id, wo_work_order_id, job_task_id, utility_account_id, department_id, long_description, substitution_id, budget_id,
	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,
	january, february, march, april, may, june, july, august, september, october, november, december, total,
	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total,
	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec, qty_total
from wo_est_slide_results_temp wem, wo_estimate_class_code_temp temp
where wem.est_monthly_id = temp.estimate_id;

if uf_check_sql('Inserting new estimate records') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')


//
// Insert new estimate spread records
//
uf_msg('Inserting new estimate spread records','I')

insert into wo_est_monthly_spread (est_monthly_id, factor_id, curve_id, rate_type_id)
select distinct temp.new_estimate_id, factor_id, curve_id, rate_type_id
from wo_est_slide_results_temp wem, wo_estimate_class_code_temp temp
where wem.est_monthly_id = temp.estimate_id;

if uf_check_sql('Inserting new estimate spread records') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records inserted','I')


//
// Null out closing_pattern_id on existing revisions
//
uf_msg('Removing closing patterns from revisions','I')

update work_order_approval woa
set closing_pattern_id = null
where exists (
	select 1 from wo_est_update_with_act_temp temp
	where temp.work_order_id = woa.work_order_id
	and temp.revision = woa.revision
	);

if uf_check_sql('Removing closing patterns from revisions') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Null out closing_pattern_id on headers
//
uf_msg('Removing closing patterns from headers','I')

update work_order_account woacct
set closing_pattern_id = null
where exists (
	select 1 from wo_est_update_with_act_temp temp
	where temp.work_order_id = woacct.work_order_id
	);

if uf_check_sql('Removing closing patterns from headers') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records updated','I')


//
// Delete previous closing pattern data
//
uf_msg('Deleting previous closing pattern data','I')

delete from closing_pattern_data
where closing_pattern_id in (
	select cp.closing_pattern_id
	from wo_est_update_with_act_temp temp, work_order_control woc, closing_pattern cp
	where temp.work_order_id = woc.work_order_id
	and cp.description = woc.work_order_number||'-'||temp.revision
	and cp.active = woc.work_order_id
	);

if uf_check_sql('Deleting previous closing pattern data') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records deleted','I')


//
// Delete previous closing pattern
//
uf_msg('Deleting previous closing pattern','I')

delete from closing_pattern
where closing_pattern_id in (
	select cp.closing_pattern_id
	from wo_est_update_with_act_temp temp, work_order_control woc, closing_pattern cp
	where temp.work_order_id = woc.work_order_id
	and cp.description = woc.work_order_number||'-'||temp.revision
	and cp.active = woc.work_order_id
	);

if uf_check_sql('Deleting previous closing pattern') <> 1 then return -1

uf_msg('   '+string(sqlca.sqlnrows)+' records deleted','I')


return 1
end function

public function longlong uf_fy_temp_backfill_year (longlong a_year);string sqls

sqls = &
"insert into wo_est_monthly_fy_temp (~r~n"+&
"	updated, est_monthly_id, year,~r~n"+&
"	work_order_id, revision, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, wo_work_order_id, long_description, budget_id,~r~n"+&
"	attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,~r~n"+&
"	january, february, march, april, may, june, july, august, september, october, november, december,~r~n"+&
"	hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun, hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec,~r~n"+&
"	qty_jan, qty_feb, qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov, qty_dec,~r~n"+&
"	hist_actuals, future_dollars~r~n"+&
"	)~r~n"+&
"select 0 updated, pwrplant1.nextval est_monthly_id, "+string(a_year)+",~r~n"+&
"	temp.*,~r~n"+&
"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,~r~n"+&
"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,~r~n"+&
"	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,~r~n"+&
"	0, 0~r~n"+&
"from (~r~n"+&
"	select work_order_id, revision, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, wo_work_order_id, long_description, budget_id,~r~n"+&
"		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id~r~n"+&
"	from wo_est_monthly_fy_temp~r~n"+&
"	minus~r~n"+&
"	select work_order_id, revision, expenditure_type_id, est_chg_type_id, department_id, utility_account_id, job_task_id, wo_work_order_id, long_description, budget_id,~r~n"+&
"		attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id~r~n"+&
"	from wo_est_monthly_fy_temp~r~n"+&
"	where year = "+string(a_year)+"~r~n"+&
"	) temp"

sqls = f_sql_add_hint(sqls, 'uf_fy_temp_backfill_year_10')

execute immediate :sqls;

if uf_check_sql('Inserting into wo_est_monthly_fy_temp') <> 1 then return -1

return 1
end function

public function longlong uf_scale_revision (double a_new_amount, string a_table_name, longlong a_year);//------------------------------------------------------------------------
//
// FUNCTION:		uf_scale_revision
//
// DESCRIPTION:	Inflate/Deflate estimated dollars/hrs/qty accordingly
//
//	ASSUMPTIONS:	1 - ONLY ONE REVISION CAN SCALE AT A TIME
//						2 - work_order_id/revision must be inserted into
//								wo_est_processing_temp
//						3 - uf_scale_commit must be called separately to save
//								changes to wo_est_monthly
//
//	ARGUMENTS:		a_new_amount -			double -	New total estimate amount
//
// RETURN CODES:	1 - SUCCESS
//					  -1 - ERROR
//
//------------------------------------------------------------------------
longlong check_revs, rtn, check_act, actuals_month, funding_wo_ind, &
	start_mn, end_mn, wo_id, rev
string ret, ret_app, sqls
double actuals_amount, old_amount, new_amount, old_estimate_amount, new_estimate_amount, &
	scale_ratio
uo_ds_top ds_amounts


//
// Set variables
//
new_amount = a_new_amount


//
// Ensure only 1 revision is scaling
//
check_revs = 0
select count(*)
into :check_revs
from wo_est_processing_temp;

if check_revs > 1 then
	uf_msg('Too many revisions. Only one revision can slide at a time.','E')
	rollback;
	return -1
end if


//
// Ensure the work order and revision are valid
//
check_revs = 0
actuals_month = 0
funding_wo_ind = 0
select count(*), nvl(max(a.actuals_month_number),0), nvl(max(b.funding_wo_indicator),0),
	to_number(to_char(max(a.est_start_date),'yyyymm')), to_number(to_char(max(a.est_complete_date),'yyyymm'))
into :check_revs, :actuals_month, :funding_wo_ind,
	:start_mn, :end_mn
from work_order_approval a, work_order_control b
where (a.work_order_id, a.revision) in (
	select work_order_id, revision 
	from wo_est_processing_temp
	)
and a.work_order_id = b.work_order_id;

if check_revs <> 1 then
	uf_msg('Invalid from/to revisions. Please ensure revisions exist prior to sliding.','E')
	rollback;
	return -1
end if


//
// Check to make sure revision is editable
//
select work_order_id, revision
into :wo_id, :rev
from wo_est_processing_temp;
ret_app = f_wo_est_revision_is_protected(wo_id, "selected", rev)

if ret_app <> "" then
	uf_msg('Cannot scale the project because '+ret_app+'.','E')
	rollback;
	return -1
end if


//
// We need to account for actuals months
//   Actuals month < Start month
//
if actuals_month > 190000 and actuals_month < start_mn then
	
	//
	// Clear out the actuals_month_number. It is meaningless if is less than the est start month
	//
	update work_order_approval a
	set actuals_month_number = null
	where exists (
		select 1 from wo_est_processing_temp b
		where b.work_order_id = a.work_order_id
		and b.revision = a.revision
		);
	
	if uf_check_sql('Clearing out actuals month number on work_order_approval') <> 1 then return -1
	
	commit;
	
	actuals_month = 0
	
	
//
// We need to account for actuals months
//   Actuals month between Start month and End month (not including End month)
//
elseif actuals_month > 190000 and actuals_month >= start_mn and actuals_month < end_mn then
	
	//
	// This is okay; scale only estimated months
	//
	actuals_month = actuals_month
	
	
//
// We need to account for actuals months
//   Actuals month >= End month
//
elseif actuals_month > 190000 and actuals_month >= end_mn then
	
	//
	// Cannot scale; all months are actuals
	//
	if actuals_month > end_mn then
		update work_order_approval a
		set actuals_month_number = to_number(to_char(est_complete_date,'yyyymm'))
		where exists (
			select 1 from wo_est_processing_temp b
			where b.work_order_id = a.work_order_id
			and b.revision = a.revision
			);
		
		if uf_check_sql('Updating actuals month number on work_order_approval') <> 1 then return -1
		
		commit;
		
		actuals_month = end_mn
	end if
	
	uf_msg('Cannot scale the project because all estimated months are actuals.','E')
	rollback;
	return -1
	
end if


//
// If we are sliding/scaling a single year, ensure that there is are editable months that are able to be forecasted
//
if not isnull(a_year) then
	if (a_year * 100 + 12) <= actuals_month then
		uf_msg('Cannot scale the project because all estimated months in the given year are actuals.','E')
		rollback;
		return -1
	end if
end if


//
// Get the scaling ratio based on the new amount in proportion to the old amount
//
sqls = &
"select sum(decode(sign(year||'01' - "+string(actuals_month)+"),1,0,nvl(january,0))) + " +&
"	sum(decode(sign(year||'02' - "+string(actuals_month)+"),1,0,nvl(february,0))) + " +&
"	sum(decode(sign(year||'03' - "+string(actuals_month)+"),1,0,nvl(march,0))) + " +&
"	sum(decode(sign(year||'04' - "+string(actuals_month)+"),1,0,nvl(april,0))) + " +&
"	sum(decode(sign(year||'05' - "+string(actuals_month)+"),1,0,nvl(may,0))) + " +&
"	sum(decode(sign(year||'06' - "+string(actuals_month)+"),1,0,nvl(june,0))) + " +&
"	sum(decode(sign(year||'07' - "+string(actuals_month)+"),1,0,nvl(july,0))) + " +&
"	sum(decode(sign(year||'08' - "+string(actuals_month)+"),1,0,nvl(august,0))) + " +&
"	sum(decode(sign(year||'09' - "+string(actuals_month)+"),1,0,nvl(september,0))) + " +&
"	sum(decode(sign(year||'10' - "+string(actuals_month)+"),1,0,nvl(october,0))) + " +&
"	sum(decode(sign(year||'11' - "+string(actuals_month)+"),1,0,nvl(november,0))) + " +&
"	sum(decode(sign(year||'12' - "+string(actuals_month)+"),1,0,nvl(december,0))) actuals_amount, " +&
"	sum(nvl(january,0) + nvl(february,0) + nvl(march,0) + nvl(april,0) + nvl(may,0) + nvl(june,0) + " +&
"		nvl(july,0) + nvl(august,0) + nvl(september,0) + nvl(october,0) + nvl(november,0) + nvl(december,0)) old_amount " +&
"from "+a_table_name+" " +&
"where (work_order_id, revision) in ( " +&
"	select work_order_id, revision " +&
"	from wo_est_processing_temp " +&
"	) "
if not isnull(a_year) then
	sqls += &
	"and year = "+string(a_year)+" "
end if

ds_amounts = create uo_ds_top
ret = f_create_dynamic_ds(ds_amounts,'grid',sqls,sqlca,true)

if ret <> 'OK' then
	uf_msg('Error creating datastore','E')
end if

actuals_amount = ds_amounts.getitemdecimal(1,"actuals_amount")
old_amount = ds_amounts.getitemdecimal(1,"old_amount")

destroy ds_amounts

old_estimate_amount = old_amount - actuals_amount
new_estimate_amount = new_amount - actuals_amount

scale_ratio = new_estimate_amount / old_estimate_amount


//
// Transpose the estimates...this is only used in determining in which estimate & month to put the rounding errors
//
//rtn = uf_transpose('dollar')
//
//if rtn = -1 then
//	return -1
//end if
delete from wo_est_transpose_temp;

sqls = &
"insert into wo_est_transpose_temp ( " +&
"	est_monthly_id, work_order_id, revision, month_number, amount ) " +&
"select b.est_monthly_id, b.work_order_id, b.revision, b.year*100 + p.month_num month_number, sum(nvl(decode(p.month_num,1,b.january,2,b.february,3,b.march,4,b.april,5,b.may,6,b.june,7,b.july,8,b.august,9,b.september,10,b.october,11,b.november,12,b.december),0)) amount " +&
"from pp_table_months p, "+a_table_name+" b, work_order_approval d, wo_est_processing_temp t " +&
"where b.work_order_id = t.work_order_id " +&
"and b.revision = t.revision " +&
"and b.work_order_id = d.work_order_id " +&
"and b.revision = d.revision " +&
"and to_date(to_char(b.year*100 + p.month_num),'YYYYMM') between trunc(d.est_start_date,'month') and trunc(d.est_complete_date,'month') "
if not isnull(a_year) then
	sqls += &
	"and b.year = "+string(a_year)+" "
end if
sqls += &
"group by b.est_monthly_id, b.work_order_id, b.revision, b.year*100 + p.month_num "

execute immediate :sqls;

if uf_check_sql('Transposing amounts') <> 1 then return -1


//
// Scale the estimates
//   Only scale estimates that are after the actuals month (beginning with months greater than actuals month)
//   Months on or before actuals month should be copied as-is
//
sqls = &
"update "+a_table_name+" set " +&
"	january = round(decode(sign(year||'01' - "+string(actuals_month)+"),1,nvl(january*"+string(scale_ratio)+",0),nvl(january,0)),2), " +&
"	february = round(decode(sign(year||'02' - "+string(actuals_month)+"),1,nvl(february*"+string(scale_ratio)+",0),nvl(february,0)),2), " +&
"	march = round(decode(sign(year||'03' - "+string(actuals_month)+"),1,nvl(march*"+string(scale_ratio)+",0),nvl(march,0)),2), " +&
"	april = round(decode(sign(year||'04' - "+string(actuals_month)+"),1,nvl(april*"+string(scale_ratio)+",0),nvl(april,0)),2), " +&
"	may = round(decode(sign(year||'05' - "+string(actuals_month)+"),1,nvl(may*"+string(scale_ratio)+",0),nvl(may,0)),2), " +&
"	june = round(decode(sign(year||'06' - "+string(actuals_month)+"),1,nvl(june*"+string(scale_ratio)+",0),nvl(june,0)),2), " +&
"	july = round(decode(sign(year||'07' - "+string(actuals_month)+"),1,nvl(july*"+string(scale_ratio)+",0),nvl(july,0)),2), " +&
"	august = round(decode(sign(year||'08' - "+string(actuals_month)+"),1,nvl(august*"+string(scale_ratio)+",0),nvl(august,0)),2), " +&
"	september = round(decode(sign(year||'09' - "+string(actuals_month)+"),1,nvl(september*"+string(scale_ratio)+",0),nvl(september,0)),2), " +&
"	october = round(decode(sign(year||'10' - "+string(actuals_month)+"),1,nvl(october*"+string(scale_ratio)+",0),nvl(october,0)),2), " +&
"	november = round(decode(sign(year||'11' - "+string(actuals_month)+"),1,nvl(november*"+string(scale_ratio)+",0),nvl(november,0)),2), " +&
"	december = round(decode(sign(year||'12' - "+string(actuals_month)+"),1,nvl(december*"+string(scale_ratio)+",0),nvl(december,0)),2), " +&
"	hrs_jan = round(decode(sign(year||'01' - "+string(actuals_month)+"),1,nvl(hrs_jan*"+string(scale_ratio)+",0),nvl(hrs_jan,0)),4), " +&
"	hrs_feb = round(decode(sign(year||'02' - "+string(actuals_month)+"),1,nvl(hrs_feb*"+string(scale_ratio)+",0),nvl(hrs_feb,0)),4), " +&
"	hrs_mar = round(decode(sign(year||'03' - "+string(actuals_month)+"),1,nvl(hrs_mar*"+string(scale_ratio)+",0),nvl(hrs_mar,0)),4), " +&
"	hrs_apr = round(decode(sign(year||'04' - "+string(actuals_month)+"),1,nvl(hrs_apr*"+string(scale_ratio)+",0),nvl(hrs_apr,0)),4), " +&
"	hrs_may = round(decode(sign(year||'05' - "+string(actuals_month)+"),1,nvl(hrs_may*"+string(scale_ratio)+",0),nvl(hrs_may,0)),4), " +&
"	hrs_jun = round(decode(sign(year||'06' - "+string(actuals_month)+"),1,nvl(hrs_jun*"+string(scale_ratio)+",0),nvl(hrs_jun,0)),4), " +&
"	hrs_jul = round(decode(sign(year||'07' - "+string(actuals_month)+"),1,nvl(hrs_jul*"+string(scale_ratio)+",0),nvl(hrs_jul,0)),4), " +&
"	hrs_aug = round(decode(sign(year||'08' - "+string(actuals_month)+"),1,nvl(hrs_aug*"+string(scale_ratio)+",0),nvl(hrs_aug,0)),4), " +&
"	hrs_sep = round(decode(sign(year||'09' - "+string(actuals_month)+"),1,nvl(hrs_sep*"+string(scale_ratio)+",0),nvl(hrs_sep,0)),4), " +&
"	hrs_oct = round(decode(sign(year||'10' - "+string(actuals_month)+"),1,nvl(hrs_oct*"+string(scale_ratio)+",0),nvl(hrs_oct,0)),4), " +&
"	hrs_nov = round(decode(sign(year||'11' - "+string(actuals_month)+"),1,nvl(hrs_nov*"+string(scale_ratio)+",0),nvl(hrs_nov,0)),4), " +&
"	hrs_dec = round(decode(sign(year||'12' - "+string(actuals_month)+"),1,nvl(hrs_dec*"+string(scale_ratio)+",0),nvl(hrs_dec,0)),4), " +&
"	qty_jan = round(decode(sign(year||'01' - "+string(actuals_month)+"),1,nvl(qty_jan*"+string(scale_ratio)+",0),nvl(qty_jan,0)),4), " +&
"	qty_feb = round(decode(sign(year||'02' - "+string(actuals_month)+"),1,nvl(qty_feb*"+string(scale_ratio)+",0),nvl(qty_feb,0)),4), " +&
"	qty_mar = round(decode(sign(year||'03' - "+string(actuals_month)+"),1,nvl(qty_mar*"+string(scale_ratio)+",0),nvl(qty_mar,0)),4), " +&
"	qty_apr = round(decode(sign(year||'04' - "+string(actuals_month)+"),1,nvl(qty_apr*"+string(scale_ratio)+",0),nvl(qty_apr,0)),4), " +&
"	qty_may = round(decode(sign(year||'05' - "+string(actuals_month)+"),1,nvl(qty_may*"+string(scale_ratio)+",0),nvl(qty_may,0)),4), " +&
"	qty_jun = round(decode(sign(year||'06' - "+string(actuals_month)+"),1,nvl(qty_jun*"+string(scale_ratio)+",0),nvl(qty_jun,0)),4), " +&
"	qty_jul = round(decode(sign(year||'07' - "+string(actuals_month)+"),1,nvl(qty_jul*"+string(scale_ratio)+",0),nvl(qty_jul,0)),4), " +&
"	qty_aug = round(decode(sign(year||'08' - "+string(actuals_month)+"),1,nvl(qty_aug*"+string(scale_ratio)+",0),nvl(qty_aug,0)),4), " +&
"	qty_sep = round(decode(sign(year||'09' - "+string(actuals_month)+"),1,nvl(qty_sep*"+string(scale_ratio)+",0),nvl(qty_sep,0)),4), " +&
"	qty_oct = round(decode(sign(year||'10' - "+string(actuals_month)+"),1,nvl(qty_oct*"+string(scale_ratio)+",0),nvl(qty_oct,0)),4), " +&
"	qty_nov = round(decode(sign(year||'11' - "+string(actuals_month)+"),1,nvl(qty_nov*"+string(scale_ratio)+",0),nvl(qty_nov,0)),4), " +&
"	qty_dec = round(decode(sign(year||'12' - "+string(actuals_month)+"),1,nvl(qty_dec*"+string(scale_ratio)+",0),nvl(qty_dec,0)),4) " +&
"where (work_order_id, revision) in ( " +&
"	select work_order_id, revision  " +&
"	from wo_est_processing_temp " +&
"	) "
if not isnull(a_year) then
	sqls += &
	"and year = "+string(a_year)+" "
end if

execute immediate :sqls;

if uf_check_sql('Updating scaled amounts') <> 1 then return -1


//
// Backfill totals to use in the rounding sql below
//
sqls = &
"update "+a_table_name+" " +&
"set total = january + february + march + april + may + june + july + august + september + october + november + december, " +&
"	hrs_total = hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec, " +&
"	qty_total = qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec " +&
"where (work_order_id, revision) in ( " +&
"	select work_order_id, revision " +&
"	from wo_est_processing_temp " +&
"	) "
if not isnull(a_year) then
	sqls += &
	"and year = "+string(a_year)+" "
end if

execute immediate :sqls;

if uf_check_sql('Updating totals (1)') <> 1 then return -1


//
// Rounding (DOLLARS) - throw differences in the last month with non-zero values for each estimate
//
sqls = &
"update "+a_table_name+" wem~r~n" +&
"set (january,february,march,april,may,june,july,august,september,october,november,december) = (~r~n" +&
"	select decode(mon,1,january + "+string(new_amount)+" - cur_amount,january) january,~r~n" +&
"		decode(mon,2,february + "+string(new_amount)+" - cur_amount,february) february,~r~n" +&
"		decode(mon,3,march + "+string(new_amount)+" - cur_amount,march) march,~r~n" +&
"		decode(mon,4,april + "+string(new_amount)+" - cur_amount,april) april,~r~n" +&
"		decode(mon,5,may + "+string(new_amount)+" - cur_amount,may) may,~r~n" +&
"		decode(mon,6,june + "+string(new_amount)+" - cur_amount,june) june,~r~n" +&
"		decode(mon,7,july + "+string(new_amount)+" - cur_amount,july) july,~r~n" +&
"		decode(mon,8,august + "+string(new_amount)+" - cur_amount,august) august,~r~n" +&
"		decode(mon,9,september + "+string(new_amount)+" - cur_amount,september) september,~r~n" +&
"		decode(mon,10,october + "+string(new_amount)+" - cur_amount,october) october,~r~n" +&
"		decode(mon,11,november + "+string(new_amount)+" - cur_amount,november) november,~r~n" +&
"		decode(mon,12,december + "+string(new_amount)+" - cur_amount,december) december~r~n" +&
"	from "+a_table_name+" temp,~r~n" +&
"		(	select sum(total) cur_amount~r~n" +&
"			from "+a_table_name+" t~r~n" +&
"			where (work_order_id, revision) in (~r~n" +&
"				select work_order_id, revision~r~n" +&
"				from wo_est_processing_temp~r~n" +&
"				)~r~n"
if not isnull(a_year) then
	sqls += &
	"			and year = "+string(a_year)+"~r~n"
end if
sqls += &
"		) b,~r~n" +&
"		(	select est_monthly_id, to_number(substr(to_char(month_number),5,2)) mon~r~n" +&
"			from (~r~n" +&
"				select est_monthly_id, month_number~r~n" +&
"				from wo_est_transpose_temp t~r~n" +&
"				where t.amount <> 0~r~n" +&
"				and t.month_number > "+string(actuals_month)+"~r~n" +&
"				and t.month_number = (~r~n" +&
"					select max(a.month_number)~r~n" +&
"					from wo_est_transpose_temp a~r~n" +&
"					where a.est_monthly_id = t.est_monthly_id~r~n" +&
"					and a.amount <> 0~r~n" +&
"					and a.month_number > "+string(actuals_month)+"~r~n" +&
"					)~r~n" +&
"				order by month_number desc, est_monthly_id desc~r~n" +&
"				)~r~n" +&
"			where rownum = 1~r~n" +&
"		) a~r~n" +&
"	where temp.est_monthly_id = wem.est_monthly_id~r~n" +&
"	and temp.year = wem.year~r~n" +&
"	)~r~n" +&
"where (est_monthly_id, year) = (~r~n" +&
"	select est_monthly_id, to_number(substr(to_char(month_number),1,4))~r~n" +&
"	from (~r~n" +&
"		select est_monthly_id, month_number~r~n" +&
"		from wo_est_transpose_temp t~r~n" +&
"		where t.amount <> 0~r~n" +&
"		and t.month_number > "+string(actuals_month)+"~r~n" +&
"		and t.month_number = (~r~n" +&
"			select max(a.month_number)~r~n" +&
"			from wo_est_transpose_temp a~r~n" +&
"			where a.est_monthly_id = t.est_monthly_id~r~n" +&
"			and a.amount <> 0~r~n" +&
"			and a.month_number > "+string(actuals_month)+"~r~n" +&
"			)~r~n" +&
"		order by month_number desc, est_monthly_id desc~r~n" +&
"		)~r~n" +&
"	where rownum = 1~r~n" +&
"	)~r~n"
if not isnull(a_year) then
	sqls += &
	"and year = "+string(a_year)+"~r~n"
end if

execute immediate :sqls;

if uf_check_sql('Updating rounding differences (dollars)') <> 1 then return -1


//
// Backfill totals in case any rounding above changed the values in the last months with 
//
sqls = &
"update "+a_table_name+" a " +&
"set total = january + february + march + april + may + june + july + august + september + october + november + december, " +&
"	hrs_total = hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec, " +&
"	qty_total = qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec " +&
"where (total <> january + february + march + april + may + june + july + august + september + october + november + december " +&
"	or hrs_total <> hrs_jan + hrs_feb + hrs_mar + hrs_apr + hrs_may + hrs_jun + hrs_jul + hrs_aug + hrs_sep + hrs_oct + hrs_nov + hrs_dec " +&
"	or qty_total <> qty_jan + qty_feb + qty_mar + qty_apr + qty_may + qty_jun + qty_jul + qty_aug + qty_sep + qty_oct + qty_nov + qty_dec " +&
"	) " +&
"and (work_order_id, revision) in ( " +&
"	select work_order_id, revision " +&
"	from wo_est_processing_temp " +&
"	) "
if not isnull(a_year) then
	sqls += &
	"and year = "+string(a_year)+" "
end if

execute immediate :sqls;

if uf_check_sql('Updating totals (2)') <> 1 then return -1


return 1

end function

public function longlong uf_scale_revision (double a_new_amount, string a_table_name);longlong rtn, year

setnull(year)

rtn = uf_scale_revision(a_new_amount, a_table_name, year)

return rtn

end function

on uo_budget_revision.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_budget_revision.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

