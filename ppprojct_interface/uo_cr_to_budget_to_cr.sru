HA$PBExportHeader$uo_cr_to_budget_to_cr.sru
$PBExportComments$7768-make new revisions for locked revisions in this budget version; 7678-need to update budget_amounts after processing budget items
forward
global type uo_cr_to_budget_to_cr from nonvisualobject
end type
end forward

global type uo_cr_to_budget_to_cr from nonvisualobject
end type
global uo_cr_to_budget_to_cr uo_cr_to_budget_to_cr

type variables

string i_company_sqls
string i_cr_company
string i_projects_company
string i_budget_version
longlong i_where_clause, i_insert_where_clause, i_delete_where_clause
longlong i_id
string i_projects_table
string i_posting_description
string i_cr_table = 'none'

string i_use_new_structures_table

///CJR added this variable so that we could pass in things like only send one project etc.
///This variable needs to start with and
string i_additional_where_clause = " "

///CJR added this variable so that we could pass in things like only send one project etc.
///This variable needs to start with and
string i_additional_delete_where_clause = " "


boolean i_use_status_box = false

///CJR this boolean variable is in place to see if when pulling in data from the CR if we only want to pull over data that is already in this budget version
// or pull all data based on the where clause and setup new revisions if new projects are included
boolean i_only_in_bv = true


///When called from capital budget entry screens we know what values we know the projects and revisions we are dealing with so we don't need to go through a budget version
boolean i_wo_est_proc_temp = false
boolean i_alloc

// SEK 042710: added this instance variable for the labor cap dollars function - this tells
// 	whether the funding project is being mapped; also added the bv id variables for use in the labor cap function
boolean i_map_fp
longlong i_cr_bv_id, i_bv_id

// ### - SEK - 5978 - 113010
longlong i_map_quantity

// ### - SEK - 5978 - 122110
string i_cr_budget_version, i_cap_budget_version
end variables

forward prototypes
public subroutine uf_msg (string a_msg)
public function longlong uf_to_projects ()
public function longlong uf_to_cr ()
public function string uf_build_where_clause (longlong a_where_clause_id)
public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field)
public function longlong uf_parse (string a_stringtoparse, string a_separator, string a_array_to_hold[])
public function string uf_clean_string (string a_string)
public function longlong uf_read (longlong a_id, longlong a_insert_where_clause, longlong a_delete_where_clause, string a_budget_version)
public function integer uf_labor_cap_dollars ()
public function longlong uf_to_cr_entry ()
public function longlong uf_to_cr_afudc_oh ()
public function longlong uf_to_cr_depr_fcst ()
end prototypes

public subroutine uf_msg (string a_msg);

//This function will either write to a f_pp_msgs or f_status_box. 

if i_use_status_box then
	f_status_box('CR to Budget to CR', a_msg+' '+string(now()))
else
	f_pp_msgs(a_msg)
end if
end subroutine

public function longlong uf_to_projects ();//*****************************************************************************************
//
//   Function   : uf_to_projects
//
//	 Description : This function will be called from uf_read to send CR data (CR_BUDGET_DATA) 
//						to either wo_est_monthly or budget_monthly_data.  This function should 
//						always be called from uf_read as uf_read determines which type of posting
//						is running as well as doing some initial setup work including intializing 
//						instance variables that this function will need
//
//  Arguments: None 
//
//*****************************************************************************************
string insert_sqls, select_sqls, sqls, field, ack_list, ack_default_value, ack_select_list, &
		 where_clause, staging_table, sqls_ds, to_column_name, from_column, projects_table, &
		 work_order_id_sqls, insert_where_clause, delete_where_clause, col_select_list, &
		 group_select_list, s_date
longlong num_rows, num_elements, ack_pos, col_order, rtn, i, source_id, bv_id, from_table_column, &
	  max_id, new_revs = 0, ii
date ddate
time ttime
datetime started_at, finished_at
any results[]
uo_budget_revision  uo_revisions

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
started_at = datetime(ddate, ttime)

if i_cr_table = 'none' then
	i_cr_table = 'CR_BUDGET_DATA'
end if


if upper(i_projects_table) = 'MONTHLY,SUPPLEMENTAL' then
	staging_table = 'wo_est_monthly_cr'
	staging_table = '"' + upper(staging_table) + '"'
	projects_table = '"WO_EST_MONTHLY"'
else
	staging_table = i_projects_table + '_cr'
	staging_table = '"' + upper(staging_table) + '"'
	projects_table = '"' + upper(i_projects_table) + '"'
end if

// Truncate the staging table
sqlca.truncate_table(staging_table);
	
if sqlca.SQLCode < 0 then
	uf_msg("ERROR: Truncating " + staging_table + " : " + &
			 sqlca.SQLErrText )
	rollback;
	return -1
end if

// Build the posting columns ds
sqls_ds = " select replace(replace(replace(to_column_name,' ','_'),'-','_'),'/','_') to_column_name, " + &
	       " from_column from_column, " + &
		 	 " nvl(from_table_column, 0) from_table_column, " + &
			 ' "TYPE" col_type, "WIDTH" col_width, "DECIMAL" col_decimal, '+&
			 ' "ORDER" col_order ' + &
			 " from cr_to_budget_to_cr_columns" + &
			 " where id = " + string(i_id) + " order by " + '"ORDER"'
		 
uo_ds_top ds_columns
ds_columns = CREATE uo_ds_top

f_create_dynamic_ds(ds_columns,"grid",sqls_ds,sqlca,true)
num_rows = ds_columns.rowcount()

if num_rows < 1 then
	uf_msg('ERROR: Unable to find Budget Columns with posting id ' + string(i_id))
	return -1
end if

// Build the insert statement to insert into cr_budget_data_projects
if staging_table = '"WO_EST_MONTHLY_CR"' then
	insert_sqls += ' "EST_MONTHLY_ID", "REVISION", '
elseif staging_table = '"BUDGET_MONTHLY_DATA_CR"' then
	insert_sqls += ' "BUDGET_MONTHLY_ID", "BUDGET_VERSION_ID", '
end if

// The user mapped fields should come before the year, amounts, and quantities
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))

	Choose case staging_table
		case '"WO_EST_MONTHLY_CR"'
			Choose case to_column_name
				case "EST_MONTHLY_ID", "REVISION", "YEAR", "JANUARY", "FEBRUARY", "MARCH", &
					  "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", &
					  "NOVEMBER", "DECEMBER", "TOTAL", "TIME_STAMP", "USER_ID", &
					  "HRS_JAN", "HRS_FEB", "HRS_MAR", "HRS_APR", "HRS_MAY", "HRS_JUN", "HRS_JUL", &
					  "HRS_AUG", "HRS_SEP", "HRS_OCT", "HRS_NOV", "HRS_DEC", "HRS_TOTAL"
					// these fields are not available to the user to map into in the 
					// posting control window
					continue
				case else
					
			end Choose
		case '"BUDGET_MONTHLY_DATA_CR"'
			Choose case to_column_name
				case "BUDGET_MONTHLY_ID", "BUDGET_VERSION_ID", "YEAR", "JANUARY", "FEBRUARY", &
					  "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", &
					  "NOVEMBER", "DECEMBER", "TOTAL", "TIME_STAMP", "USER_ID", &
					   "JAN_LOCAL", "FEB_LOCAL", "MAR_LOCAL", "APR_LOCAL", "MAY_LOCAL", "JUN_LOCAL", "JUL_LOCAL","AUG_LOCAL",&
					  "SEP_LOCAL","OCT_LOCAL","NOV_LOCAL","DEC_LOCAL","TOT_LOCAL"
					  // ### - CJR - 7446 - 20110427
//					  "HRS_JAN", "HRS_FEB", "HRS_MAR", "HRS_APR", "HRS_MAY", "HRS_JUN", "HRS_JUL", &
//					  "HRS_AUG", "HRS_SEP", "HRS_OCT", "HRS_NOV", "HRS_DEC", "HRS_TOTAL"
					// these fields are not available to the user to map into in the 
					// posting control window
					continue
				case else
					
			end Choose
		case else
			uf_msg("ERROR: unidentifiable staging table")
			rollback;
			return -1
	end Choose
	
	insert_sqls = insert_sqls + '"' + to_column_name + '", '
	
next

insert_sqls += ' "YEAR", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", ' + &
					' "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER", "TOTAL", '
// ### - CJR - 7446 - 20110427
if staging_table = '"WO_EST_MONTHLY_CR"' then
	insert_sqls +=  ' "HRS_JAN", "HRS_FEB", "HRS_MAR", "HRS_APR", "HRS_MAY", "HRS_JUN", "HRS_JUL", ' + &
					' "HRS_AUG", "HRS_SEP", "HRS_OCT", "HRS_NOV", "HRS_DEC", "HRS_TOTAL", '
else
	insert_sqls += ' "JAN_LOCAL", "FEB_LOCAL", "MAR_LOCAL", "APR_LOCAL", "MAY_LOCAL", "JUN_LOCAL", "JUL_LOCAL","AUG_LOCAL", '+&
					 '  "SEP_LOCAL","OCT_LOCAL","NOV_LOCAL","DEC_LOCAL","TOT_LOCAL", '	
end if
	insert_sqls += ' "TIME_STAMP", "USER_ID" '


uf_msg("Building Select SQLS")


// Build the select statement -- fields that the user mapped in the control window
// NOTE: In order to allow for subselects to be entered in the mappings, I chose to build
//			the sql so that everything the user enters goes into it's own view, including the
// 		the amount and month number - which the user can't map, so that the sums will work
//			properly without requiring the user to be clever about the way they enter their
//			subselects.  
//			Below three things are built: 
//					select_sqls - contains the SQL going into the view mentioned above
//					col_select_list - the list of every column in the view (minus amount, 
//						month number, and work order id)
//					group_select_list - the list of every column in the view that is not
//						being summed (minus month number and work order id)

for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
		
	Choose case staging_table
		case '"WO_EST_MONTHLY_CR"'
			Choose case to_column_name
				case "EST_MONTHLY_ID", "REVISION", "YEAR", "JANUARY", "FEBRUARY", "MARCH", &
					  "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", &
					  "NOVEMBER", "DECEMBER", "TOTAL", "TIME_STAMP", "USER_ID", &
					  "HRS_JAN", "HRS_FEB", "HRS_MAR", "HRS_APR", "HRS_MAY", "HRS_JUN", "HRS_JUL", &
					  "HRS_AUG", "HRS_SEP", "HRS_OCT", "HRS_NOV", "HRS_DEC", "HRS_TOTAL"
					// these fields are not available to the user to map into in the 
					// posting control window
					continue
				case else
					
			end Choose
		case '"BUDGET_MONTHLY_DATA_CR"'
			Choose case to_column_name
				case "BUDGET_MONTHLY_ID", "BUDGET_VERSION_ID", "YEAR", "JANUARY", "FEBRUARY", &
					  "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", &
					  "NOVEMBER", "DECEMBER", "TOTAL", "TIME_STAMP", "USER_ID", &				
					  "JAN_LOCAL", "FEB_LOCAL", "MAR_LOCAL", "APR_LOCAL", "MAY_LOCAL", "JUN_LOCAL", "JUL_LOCAL","AUG_LOCAL",&
					  "SEP_LOCAL","OCT_LOCAL","NOV_LOCAL","DEC_LOCAL","TOT_LOCAL"
					   // ### - CJR - 7446 - 20110427
						// "HRS_JAN", "HRS_FEB", "HRS_MAR", "HRS_APR", "HRS_MAY", "HRS_JUN", "HRS_JUL", &
						// "HRS_AUG", "HRS_SEP", "HRS_OCT", "HRS_NOV", "HRS_DEC", "HRS_TOTAL"
					// these fields are not available to the user to map into in the 
					// posting control window
					continue
				case else
					
			end Choose
		case else
			uf_msg("ERROR: unidentifiable staging table")
			rollback;
			return -1
	end Choose
	
	from_column = ds_columns.GetItemString(i, "from_column")
	from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if from_column = "'_'" then from_column = "' '"
	
	if from_table_column = 1 then
		from_column = uf_clean_string(from_column)
		from_column = '"' + upper(from_column) + '"'
	else
		// Need to support sum -- i.e. the user can map into the quantity field
		if left(upper(from_column),4) = 'SUM(' then
			// trim off 'SUM(' from the front and ')' from the end
			from_column = left(from_column, len(from_column) - 1)
			from_column = right(from_column, len(from_column) - 4)
			
			select_sqls = select_sqls + from_column + " s" + string(col_order) + ", "
			col_select_list = col_select_list + " sum(y.s" + string(col_order) + "), "
			continue
		end if
	end if
	
	if to_column_name = "WORK_ORDER_ID" then
		select_sqls = select_sqls + from_column + " work_order_id, "
		col_select_list = col_select_list + "y.work_order_id, "
		group_select_list = group_select_list + "y.work_order_id, "
	elseif to_column_name = "BUDGET_ID" then
		select_sqls = select_sqls + from_column + " budget_id, "
		col_select_list = col_select_list + "y.budget_id, "
		group_select_list = group_select_list + "y.budget_id, "
	else
		select_sqls = select_sqls + from_column + " c" + string(col_order) + ", "
		col_select_list = col_select_list + "y.c" + string(col_order) + ", "
		group_select_list = group_select_list + "y.c" + string(col_order) + ", "
	end if
	
	if to_column_name = "WORK_ORDER_ID" then
		work_order_id_sqls = "c" + string(col_order)
	end if
next

// Look up the budget_version_id corresponding to i_budget_version
// ### - SEK - 5978 - 122110
select budget_version_id into :bv_id
  from budget_version
 where description = :i_cap_budget_version;
// where description = :i_budget_version;
  
if isnull(bv_id) or bv_id = 0 then
	uf_msg("ERROR: Unable to find the budget_version_id corresponding to '" + &
			 i_budget_version + "'!")
	rollback;
	return -1
end if

// Get the insert where clause
if i_insert_where_clause = 0 or isnull(i_insert_where_clause) then
	// User did not pick a where clause so skip this part
else
	uf_msg("Building Insert Where Clause")
	//This should return a where clause that starts with and
	insert_where_clause = uf_build_where_clause(i_insert_where_clause)
end if
if isnull(insert_where_clause) then insert_where_clause = ""

if insert_where_clause = "ERROR" then
	uf_msg("ERROR: Building the insert where clause.")
	rollback;
	return -1
end if

if isnull(i_additional_where_clause) then
	///CJR by default this variable is set to a single space if it is null something outside of the user object was trying to set it and it ended up with a null
	///This is bad and so lets exist and make them fix that process
	///As a note the i_additional_where_clause variable needs to start with and.  
	uf_msg("ERROR i additional insert where clause is null")
	rollback;
	return -1
end if

uf_msg("Inserting into " + staging_table)

// Build sqls
sqls  = "insert into " + staging_table + " ( "
sqls += insert_sqls + ") "
sqls += " select pwrplant1.nextval, "
if staging_table = '"WO_EST_MONTHLY_CR"' then
	sqls += " rev.revision, "
elseif staging_table = '"BUDGET_MONTHLY_DATA_CR"' then
	sqls += " " + string(bv_id) + ", "
end if
sqls += " x.*, sysdate, user "
sqls += "from ( "
sqls += "	select " + col_select_list + " "
sqls += "			 y.year, " 
sqls += "			 sum(y.jan), "
sqls += "			 sum(y.feb), "
sqls += "			 sum(y.mar), "
sqls += "			 sum(y.apr), "
sqls += "			 sum(y.may), "
sqls += "			 sum(y.jun), "
sqls += "			 sum(y.jul), "
sqls += "			 sum(y.aug), "
sqls += "			 sum(y.sep), "
sqls += "			 sum(y.oct), "
sqls += "			 sum(y.nov), "
sqls += "			 sum(y.dec), "
sqls += "			 sum(y.amount), "
// ### - CJR - 7446 - 20110427
if staging_table = '"WO_EST_MONTHLY_CR"' then
	sqls += "			 sum(y.hrs_jan), "
	sqls += "			 sum(y.hrs_feb), "
	sqls += "			 sum(y.hrs_mar), "
	sqls += "			 sum(y.hrs_apr), "
	sqls += "			 sum(y.hrs_may), "
	sqls += "			 sum(y.hrs_jun), "
	sqls += "			 sum(y.hrs_jul), "
	sqls += "			 sum(y.hrs_aug), "
	sqls += "			 sum(y.hrs_sep), "
	sqls += "			 sum(y.hrs_oct), "
	sqls += "			 sum(y.hrs_nov), "
	sqls += "			 sum(y.hrs_dec), "
	sqls += "			 sum(y.quantity) "
else
	sqls += "			 sum(y.jan) jan_local, "
	sqls += "			 sum(y.feb) feb_local, "
	sqls += "			 sum(y.mar) mar_local, "
	sqls += "			 sum(y.apr) apr_local, "
	sqls += "			 sum(y.may) may_local, "
	sqls += "			 sum(y.jun) jun_local, "
	sqls += "			 sum(y.jul) jul_local, "
	sqls += "			 sum(y.aug) aug_local, "
	sqls += "			 sum(y.sep) sep_local, "
	sqls += "			 sum(y.oct) oct_local, "
	sqls += "			 sum(y.nov) nov_local, "
	sqls += "			 sum(y.dec) dec_local, "
	sqls += "			 sum(y.amount) tot_local "
end if
sqls += "	  from ( " 
sqls += "			select " + select_sqls + " "
sqls += "					 to_number(substr(to_char(month_number), 1, 4)) year, "
sqls += "					 decode(substr(to_char(month_number), 5, 2),'01', amount, 0) jan, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '02', amount, 0) feb, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '03', amount, 0) mar, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '04', amount, 0) apr, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '05', amount, 0) may, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '06', amount, 0) jun, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '07', amount, 0) jul, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '08', amount, 0) aug, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '09', amount, 0) sep, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '10', amount, 0) oct, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '11', amount, 0) nov, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '12', amount, 0) dec, "
sqls += "					 amount, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '01', quantity, 0) hrs_jan, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '02', quantity, 0) hrs_feb, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '03', quantity, 0) hrs_mar, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '04', quantity, 0) hrs_apr, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '05', quantity, 0) hrs_may, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '06', quantity, 0) hrs_jun, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '07', quantity, 0) hrs_jul, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '08', quantity, 0) hrs_aug, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '09', quantity, 0) hrs_sep, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '10', quantity, 0) hrs_oct, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '11', quantity, 0) hrs_nov, "
sqls += "					 decode(substr(to_char(month_number), 5, 2), '12', quantity, 0) hrs_dec, "
sqls += "					 quantity "
sqls += "			  from "+i_cr_table+" cr " 
// ### - SEK - 5978 - 122110
//sqls += "			 where cr.budget_version = '" + i_budget_version + "' "
sqls += "			 where cr.budget_version = '" + i_cr_budget_version + "' "
sqls += insert_where_clause+i_additional_where_clause
sqls += "			) y " 
sqls += "	 group by " + group_select_list + " y.year " 
sqls += "	) x "
if upper(i_projects_table) = 'WO_EST_MONTHLY' then
	if not i_wo_est_proc_temp then
		sqls += " ,  ( "  
		sqls += " select revision, work_order_id from budget_version_fund_proj " 
		sqls += " where active = 1 "
		sqls += " and budget_version_id = " + string(bv_id)
		sqls += "	) rev " 
		////Check flag passed in to see if we should outer join here
		if i_only_in_bv then
			sqls += " where rev.work_order_id = x.work_order_id "
		else
			sqls += " where rev.work_order_id (+) = x.work_order_id "
		end if
	else
		sqls += " ,  ( select revision revision, work_order_id from wo_est_processing_temp	) rev " 
		sqls +=' where rev.work_order_id = x.work_order_id '
	end if
else
	sqls += " ,  budget_amounts rev "
	if i_only_in_bv then
		sqls += " where rev.budget_id = x.budget_id "
		sqls +=' and rev.budget_version_id = ' + string(bv_id)
	else
		sqls += " where rev.budget_id (+) = x.budget_id "
		sqls +=' and rev.budget_version_id (+) = ' + string(bv_id)
	end if	
end if
			
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	uf_msg("ERROR: Inserting into " + staging_table + ": " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if		

//***********************************************
// CALL TO CUSTOM FUNCTION FOR PROCESSING OF DATA
//***********************************************
uf_msg("Calling custom function")
rtn = f_cr_to_budget_to_cr_custom(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_projects',i_cr_budget_version,this)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_to_cr' ) = 1 then
	rtn = sqlca.f_client_budget_to_cr(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_projects',i_cr_budget_version)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = string(i_id)
args[2] = insert_where_clause
args[3] = delete_where_clause
args[4] = i_cap_budget_version
args[5] = 'uf_to_projects'
args[6] = i_cr_budget_version
rtn = f_wo_validation_control(84,args)
if rtn < 0 then
	rollback;
	return -1
end if

if upper(i_projects_table) = 'WO_EST_MONTHLY' then
	delete from wo_est_processing_temp;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Deleting from wo_est_processing_temp : " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	if not i_only_in_bv then
		// Create new revisions for projects not in the budget version
		// Only applies if i_only_in_bv = false
		uf_msg("Checking for projects not in the budget version.")
		insert into wo_est_processing_temp
			(work_order_id, revision)
		select distinct a.work_order_id, 0
		from wo_est_monthly_cr a
		where revision is null
		and work_order_id is not null
		and est_chg_type_id is not null
		and year is not null
		and est_monthly_id is not null;
		
		if sqlca.sqlcode < 0 then 
			uf_msg("ERROR: Inserting into wo_est_processing_temp : " + sqlca.SQLErrText)
			rollback;
			return -1
		end if
		
		// ### CDM - Maint 7768 - make new revisions for locked revisions in this budget version
		new_revs += sqlca.sqlnrows
	end if
	
	// Create new revisions for projects in the budget version where the revisions are locked.
	uf_msg("Checking for projects with locked revisions in the budget version.")
	delete from temp_work_order;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Deleting from temp_work_order : " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	insert into temp_work_order (user_id, session_id, batch_report_id, work_order_id)
	select distinct 'pwrplant', userenv('sessionid'), 0, work_order_id
	from wo_est_monthly_cr a
	where revision is not null
	and work_order_id is not null
	and est_chg_type_id is not null
	and year is not null
	and est_monthly_id is not null;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Inserting into temp_work_order : " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	insert into wo_est_processing_temp
		(work_order_id, revision)
	select distinct a.work_order_id, a.revision
	from wo_est_monthly_cr a
	where revision is not null
	and work_order_id is not null
	and est_chg_type_id is not null
	and year is not null
	and est_monthly_id is not null
	and exists (
		select 1 from wo_est_month_is_editable z
		where z.work_order_id = a.work_order_id
		and z.revision = a.revision
		and z.revision_edit = 0
		);
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Inserting into wo_est_processing_temp : " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	new_revs += sqlca.sqlnrows
	
	if new_revs > 0 then
		uf_msg("Creating new revisions for applicable projects.")
		uo_revisions = create uo_budget_revision
		
		uo_revisions.i_messagebox_onerror = true
		if i_use_status_box then
			uo_revisions.i_status_box = true
			uo_revisions.i_pp_msgs = false
		else
			uo_revisions.i_status_box = false
			uo_revisions.i_pp_msgs = true
		end if
		uo_revisions.i_add_time_to_messages = true
		uo_revisions.i_statusbox_onerror = false
		uo_revisions.i_display_msg_on_sql_success = false
		uo_revisions.i_display_rowcount = false
		uo_revisions.i_progress_bar = false
		
		uo_revisions.i_new_bv_ids[1] = bv_id
		rtn = uo_revisions.uf_new_revision('fp')
		// ### CDM - Maint 7678 - need to re-use uo_revisions below
		destroy uo_revisions
		
		if rtn = -1 then
			uf_msg("ERROR: Creating new revisions.")
			rollback;
			return -1
		end if
		
		update wo_est_monthly_cr cr
		set revision = (select bvfp.revision from budget_version_fund_proj bvfp where bvfp.active = 1 and bvfp.budget_version_id = :bv_id and cr.work_order_id = bvfp.work_order_id)
		//where revision is null
		// ### CDM - Maint 7768 - make new revisions for locked revisions in this budget version
		where exists (
			select 1 from wo_est_processing_temp a
			where a.work_order_id = cr.work_order_id
			)
		;
		if sqlca.sqlcode < 0 then 
			uf_msg("ERROR: Updating revision on " + staging_table + ": " + sqlca.SQLErrText)
			rollback;
			return -1			
		end if
	end if
	
elseif upper(i_projects_table) = 'BUDGET_MONTHLY_DATA' then
	delete from wo_est_processing_temp;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Deleting from wo_est_processing_temp : " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	if not i_only_in_bv then
		// Add missing budget items to the budget version
		// Only applies if i_only_in_bv = false
		uf_msg("Checking for budget items not in the budget version.")
		insert into wo_est_processing_temp
			(work_order_id, revision)
		select distinct a.budget_id, 0
		from budget_monthly_data_cr a
		where budget_version_id is not null
		and budget_id is not null
		and est_chg_type_id is not null
		and year is not null
		and budget_monthly_id is not null
		and not exists (
			select 1 from budget_amounts b
			where b.budget_id = a.budget_id
			and b.budget_version_id = a.budget_version_id
			);
		
		if sqlca.sqlcode < 0 then 
			uf_msg("ERROR: Inserting into wo_est_processing_temp : " + sqlca.SQLErrText)
			rollback;
			return -1
		end if
		
		new_revs += sqlca.sqlnrows
	end if
	
	if new_revs > 0 then
		uf_msg("Associating applicable budget items to the budget version.")
		uo_revisions = create uo_budget_revision
		
		uo_revisions.i_messagebox_onerror = true
		if i_use_status_box then
			uo_revisions.i_status_box = true
			uo_revisions.i_pp_msgs = false
		else
			uo_revisions.i_status_box = false
			uo_revisions.i_pp_msgs = true
		end if
		uo_revisions.i_add_time_to_messages = true
		uo_revisions.i_statusbox_onerror = false
		uo_revisions.i_display_msg_on_sql_success = false
		uo_revisions.i_display_rowcount = false
		uo_revisions.i_progress_bar = false
		
		rtn = uo_revisions.uf_new_bi_version(bv_id)
		destroy uo_revisions
		
		if rtn = -1 then
			uf_msg("ERROR: Creating new budget_amounts.")
			rollback;
			return -1
		end if
	end if
	
	uf_msg("Checking for locked budget items in this budget version.")
	
	delete from temp_budget;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Deleting from temp_budget : " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	insert into temp_budget (budget_id, budget_version_id, batch_report_id, session_id, user_id)
	select distinct budget_id, budget_version_id, 0, userenv('sessionid'), user
	from budget_monthly_data_cr a
	where budget_version_id is not null
	and budget_id is not null
	and est_chg_type_id is not null
	and year is not null
	and budget_monthly_id is not null;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Inserting into temp_budget : " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	update budget_monthly_data_cr a
	set budget_version_id = -9
	where exists (
		select 1 from bi_est_month_is_editable b
		where b.work_order_id = a.budget_id
		and b.revision = a.budget_version_id
		and b.revision_edit = 0
		)
	and budget_version_id is not null
	and budget_id is not null
	and est_chg_type_id is not null
	and year is not null
	and budget_monthly_id is not null;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: Updating budget_monthly_data_cr (locked budgets/versions (1)): " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	rtn = f_get_column(results, "select distinct b.budget_number from budget_monthly_data_cr bmd, budget b where bmd.budget_version_id = -9 and bmd.budget_id = b.budget_id order by b.budget_number")
	if rtn > 0 then
		uf_msg("WARNING: The following budget items are locked and cannot be loaded for this budget version:")
		for ii = 1 to rtn
			uf_msg("   "+string(results[ii]))
		next
		uf_msg("WARNING: The above budget items are locked and cannot be loaded for this budget version:")
	end if
	
	delete from budget_monthly_data_cr a
	where budget_version_id = -9;
	if sqlca.sqlcode < 0 then
		uf_msg("ERROR: Deleting from budget_monthly_data_cr (locked budgets/versions (2)): " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
end if

uf_msg("Expand Project Dates")
delete from wo_est_processing_temp;
if staging_table = '"WO_EST_MONTHLY_CR"' then
	insert into wo_est_processing_temp(work_order_id, revision)
	select  distinct work_order_id, revision
	from WO_EST_MONTHLY_CR
	where work_order_id is not null
	 and revision is not null
	 and est_monthly_id is not null
	 and est_chg_type_id is not null
	group by work_order_id, revision
	;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: insert into wo_est_processing_temp " + staging_table + ": " + sqlca.SQLErrText)
		rollback;
		return -1			
	end if
elseif staging_table = '"BUDGET_MONTHLY_DATA_CR"' then
	insert into wo_est_processing_temp(work_order_id, revision)
	select  distinct budget_id, budget_version_id
	from BUDGET_MONTHLY_DATA_CR
	where budget_id is not null
	and est_chg_type_id is not null
	and budget_monthly_id is not null
	and budget_version_id is not null
	group by  budget_id, budget_version_id
	;
	if sqlca.sqlcode < 0 then 
		uf_msg("ERROR: insert into wo_est_processing_temp " + staging_table + ": " + sqlca.SQLErrText)
		rollback;
		return -1			
	end if
end if



if upper(i_projects_table) = 'WO_EST_MONTHLY' then


	update work_order_approval z
	set est_start_date = (
		select to_date(y.min,'YYYYMM')
		from (
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num)
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num 
				from wo_est_monthly_cr wem, pp_table_months p,  wo_est_processing_temp w
				where  w.work_order_id = wem.work_order_id
					and w.revision = wem.revision
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
				) y
		where y.work_order_id = z.work_order_id
			and y.revision = z.revision
			)
	where exists (
		select 1
		from (	
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num)
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num
				from wo_est_monthly_cr wem, pp_table_months p, wo_est_processing_temp w
				where wem.work_order_id = w.work_order_id
					and wem.revision = w.revision
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
			) y
		where  y.work_order_id = z.work_order_id
			and y.revision = z.revision
			and nvl(z.est_start_date,sysdate + 100000) >  to_date(y.min,'YYYYMM')
		)
	;


	update work_order_approval z
	set est_complete_date = (
		select to_date(y.max,'YYYYMM')
		from (
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num) max
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num
				from wo_est_monthly_cr wem, pp_table_months p,  wo_est_processing_temp w
				where w.work_order_id = wem.work_order_id
					and w.revision = wem.revision
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
				) y
		where y.work_order_id = z.work_order_id
			and y.revision = z.revision
			)
	where exists (
		select 1
		from (	
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num) max
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num
				from wo_est_monthly_cr wem, pp_table_months p,  wo_est_processing_temp w
				where w.work_order_id = wem.work_order_id
					and w.revision = wem.revision
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
			) y
		where  y.work_order_id = z.work_order_id
			and y.revision = z.revision
			and nvl(z.est_complete_date,sysdate - 100000) <  to_date(y.max,'YYYYMM')
			
		)
	;
else
	

	update budget_amounts z
	set start_date = (
		select to_date(y.min,'YYYYMM')
		from (
			select a.budget_id, a.budget_version_id, min(a.month_num) min, max(a.month_num)
			from (
				select bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num month_num 
				from budget_monthly_data_cr bmd, pp_table_months p, wo_est_processing_temp w
				where w.work_order_id = bmd.budget_id
					and w.revision = bmd.budget_version_id
				group by bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,bmd.JANUARY ,  2,bmd.february,   3,bmd.march, 4,bmd.april,5,bmd.may,
					6,bmd.june,7,bmd.july,8,bmd.august,9,bmd.september,10,bmd.october,11,bmd.november,12,bmd.december),0)) <> 0
				) a
			group by a.budget_id, a.budget_version_id
				) y
		where y.budget_id = z.budget_id
			and y.budget_version_id = z.budget_version_id
			)
	where exists (
		select 1
		from (	
			select a.budget_id, a.budget_version_id, min(a.month_num) min, max(a.month_num)
			from (
				select bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num month_num 
				from budget_monthly_data_cr bmd, pp_table_months p, wo_est_processing_temp w
				where w.work_order_id = bmd.budget_id
					and w.revision = bmd.budget_version_id
				group by bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,bmd.JANUARY ,  2,bmd.february,   3,bmd.march, 4,bmd.april,5,bmd.may,
					6,bmd.june,7,bmd.july,8,bmd.august,9,bmd.september,10,bmd.october,11,bmd.november,12,bmd.december),0)) <> 0
				) a
			group by a.budget_id, a.budget_version_id
			) y
		where  y.budget_id = z.budget_id
			and y.budget_version_id = z.budget_version_id
			and nvl(z.start_date,sysdate +100000) >  to_date(y.min,'YYYYMM')
		)
	;


	update budget_amounts z
	set close_date = (
		select to_date(y.max,'YYYYMM')
		from (
			select a.budget_id, a.budget_version_id, min(a.month_num) min, max(a.month_num) max
			from (
				select bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num month_num 
				from budget_monthly_data_cr bmd, pp_table_months p, wo_est_processing_temp w
				where w.work_order_id = bmd.budget_id
					and w.revision = bmd.budget_version_id
				group by bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,bmd.JANUARY ,  2,bmd.february,   3,bmd.march, 4,bmd.april,5,bmd.may,
					6,bmd.june,7,bmd.july,8,bmd.august,9,bmd.september,10,bmd.october,11,bmd.november,12,bmd.december),0)) <> 0
				) a
			group by a.budget_id, a.budget_version_id
				) y
		where y.budget_id = z.budget_id
			and y.budget_version_id = z.budget_version_id
			)
	where exists (
		select 1
		from (	
			select a.budget_id, a.budget_version_id, min(a.month_num) min, max(a.month_num) max
			from (
				select bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num month_num 
				from budget_monthly_data_cr bmd, pp_table_months p, wo_est_processing_temp w
				where w.work_order_id = bmd.budget_id
					and w.revision = bmd.budget_version_id
				group by bmd.budget_id, bmd.budget_version_id, bmd.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,bmd.JANUARY ,  2,bmd.february,   3,bmd.march, 4,bmd.april,5,bmd.may,
					6,bmd.june,7,bmd.july,8,bmd.august,9,bmd.september,10,bmd.october,11,bmd.november,12,bmd.december),0)) <> 0
				) a
			group by a.budget_id, a.budget_version_id
			) y
		where  y.budget_id = z.budget_id
			and y.budget_version_id = z.budget_version_id
			and nvl(z.close_date,sysdate - 100000) < to_date(y.max,'YYYYMM')
		)
	;
	
end if

// ### - CJR - 7446 - 20110427
//uf_msg("Inserting missing id/year combinations")
//
//if staging_table = '"WO_EST_MONTHLY_CR"' then
//	// Insert missing est_monthly_id/year combinations
//	insert into wo_est_monthly_cr
//	(work_order_id, est_monthly_id, revision, year, expenditure_type_id, est_chg_type_id, 
//		department_id, utility_account_id, job_task_id,  january,february, march,april,
//		may,june,july, august,september, october,november, december, total, long_description, 
//		hist_actuals, future_dollars, hrs_jan, hrs_feb, hrs_mar, hrs_apr, hrs_may, hrs_jun,
//		hrs_jul, hrs_aug, hrs_sep, hrs_oct, hrs_nov, hrs_dec, hrs_total, qty_jan, qty_feb,
//		qty_mar, qty_apr, qty_may, qty_jun, qty_jul, qty_aug, qty_sep, qty_oct, qty_nov,
//		qty_dec, qty_total
//	)
//	select work_order_id, est_monthly_id, revision, b.year, min(expenditure_type_id),
//		min(est_chg_type_id), min(department_id), min(utility_account_id), min(job_task_id), 0, 0,
//		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, min(long_description), 0, 0,
//		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//	from wo_est_monthly_cr a, pp_table_years b
//	where b.year >= (
//		select to_number(to_char(c.est_start_date,'yyyy'))
//		from work_order_approval c
//		where c.work_order_id = a.work_order_id
//		and c.revision = a.revision
//		)
//	and b.year <= (
//		select to_number(to_char(c.est_complete_date,'yyyy'))
//		from work_order_approval c
//		where c.work_order_id = a.work_order_id
//		and c.revision = a.revision
//		)
//	and not exists (
//		select 1 from wo_est_monthly_cr c 
//		where a.work_order_id = c.work_order_id
//		and a.revision = c.revision
//		and a.est_monthly_id = c.est_monthly_id
//		and c.year = b.year		
//		)
//	and revision is not null
//	and est_chg_type_id is not null
//	group by work_order_id, est_monthly_id, revision, b.year
//	;
//	
//elseif staging_table = "BUDGET_MONTHLY_DATA_CR" then 
//	
//	insert into budget_monthly_data_cr
//	(budget_id, budget_monthly_id, budget_version_id, year, expenditure_type_id, est_chg_type_id, 
//		department_id, budget_plant_class_id, job_task_id, january, february, march, april,
//		may, june, july, august,september, october, november, december, total
//		// ### - CJR - 7446 - 20110427
//		jan_local, feb_local, mar_local, apr_local, may_local, jun_local, jul_local, aug_local, sep_local, oct_local, nov_local, dec_local, tot_local
//	)
//	select budget_id, budget_monthly_id, budget_version_id, b.year, min(expenditure_type_id),
//		min(est_chg_type_id), min(department_id), min(budget_plant_class_id), min(job_task_id), 
//		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//	from budget_monthly_data_cr a, pp_table_years b
//	where b.year >= (
//		select to_number(to_char(c.start_date,'yyyy'))
//		from budget_amounts c
//		where c.budget_id = a.budget_id
//		and c.budget_version_id = a.budget_version_id
//		)
//	and b.year <= (
//		select to_number(to_char(c.close_date,'yyyy'))
//		from budget_amounts c
//		where c.budget_id = a.budget_id
//		and c.budget_version_id = a.budget_version_id
//		)
//	and not exists (
//		select 1 from budget_monthly_data_cr c 
//		where a.budget_id = c.budget_id
//		and a.budget_version_id = c.budget_version_id
//		and a.budget_monthly_id = c.budget_monthly_id
//		and c.year = b.year
//		)
//	and est_chg_type_id is not null
//	group by budget_id, budget_monthly_id, budget_version_id, b.year
//	;
//	
//end if
//	
//if sqlca.sqlcode < 0 then
//	uf_msg("  ")
//	uf_msg("ERROR: Inserting missing years into wo_est_monthly: " + sqlca.SQLErrText)
//	uf_msg("  ")
//	rollback;
//	return -1
//end if
	


// Delete from budget table and then insert stg data into budget table

if isnull(i_additional_delete_where_clause) then
	///CJR by default this variable is set to a single space if it is null something outside of the user object was trying to set it and it ended up with a null
	///This is bad and so lets exist and make them fix that process
	///As a note the i_additional_where_clause variable needs to start with and.  
	uf_msg('ERROR additional delete where clause criteria is nul')
	rollback;
	return -1
end if

// Get the delete where clause
if i_delete_where_clause = 0 or isnull(i_delete_where_clause) then
	// the user did not select a where clause so skip this part
else
	///This function always returns a where clause that starts with and
	delete_where_clause = uf_build_where_clause(i_delete_where_clause)
end if

if isnull(delete_where_clause) then 
	delete_where_clause = " "
end if

if delete_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the delete where clause.")
	rollback;
	return -1
end if

uf_msg("Deleting from " + projects_table)
//
//if delete_where_clause = "" and i_additional_delete_where_clause = ""  then
//	//sqls = "delete from " + projects_table + " "
//	///Should we delete the entire budget version maybe?  
//else
	
	sqls = "delete from " + projects_table + " pt " 
	
	if upper(i_projects_table) = "WO_EST_MONTHLY" then
		 	 	sqls += " where (work_order_id, revision) in (select work_order_id, revision from budget_version_fund_proj where active = 1 and budget_version_id = "+string(bv_id)+")" +&
			  		" "+ delete_where_clause  + i_additional_delete_where_clause
	else
		sqls += " where budget_version_id = " + string(bv_id) + &
			" "+ delete_where_clause  + i_additional_delete_where_clause
	end if
			  
//end if
			
execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Deleting from " + projects_table + ": " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if
uf_msg("Inserting into " + projects_table)

////Note we should put some check here to make sure that est_chg_type_id, work_order_id, year, or est_monthly_id are not null
////if they are warn the user and then run the insert below filtering out those records

sqls = "insert into " + projects_table + " (" + insert_sqls + ") " + &
		 "select " + insert_sqls + " from " + staging_table 
		if upper(i_projects_table) = 'WO_EST_MONTHLY' then
			sqls += " where work_order_id is not null "+&
			 " and est_chg_type_id is not null "+&
			 " and est_monthly_id is not null "+&
			 " and year is not null "+&
			 " and revision is not null "
		else
			 sqls +=" where budget_id is not null "+&
			 " and est_chg_type_id is not null "+&
			 " and budget_monthly_id is not null "+&
			 " and year is not null "	+&
			 " and budget_version_id is not null "
		end if
		
		 

execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into " + projects_table + ": " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

// ### CDM - Maint 7678 - need to update budget_amounts
if upper(i_projects_table) = 'WO_EST_MONTHLY' then
	// Funding Projects - don't need this
else
	// Budget Items - update budget_amounts
	uo_revisions = create uo_budget_revision
	
	uo_revisions.i_messagebox_onerror = true
	if i_use_status_box then
		uo_revisions.i_status_box = true
		uo_revisions.i_pp_msgs = false
	else
		uo_revisions.i_status_box = false
		uo_revisions.i_pp_msgs = true
	end if
	uo_revisions.i_add_time_to_messages = true
	uo_revisions.i_statusbox_onerror = false
	uo_revisions.i_display_msg_on_sql_success = false
	uo_revisions.i_display_rowcount = false
	uo_revisions.i_progress_bar = false
	
	rtn = uo_revisions.uf_update_budget_amounts()
	destroy uo_revisions
	
	if rtn = -1 then
		uf_msg("ERROR: Updating budget amounts.")
		rollback;
		return -1
	end if
end if

commit;

// Insert into cr_to_budget_to_cr_audit -- do this after the commit because it's not
// a big deal if this fails
uf_msg("Inserting into cr_to_budget_to_cr_audit")

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
finished_at = datetime(ddate, ttime)

select max(occurrence_id) into :max_id
  from cr_to_budget_to_cr_audit
 where id = :i_id;
 
if isnull(max_id) then max_id = 0
max_id++

if staging_table = '"WO_EST_MONTHLY_CR"' then
	insert into cr_to_budget_to_cr_audit (
		id, occurrence_id, budget_version, running_user_id, start_time, end_time, total_amount, 
		total_records, insert_where_clause, delete_where_clause, time_stamp, user_id)
	select :i_id,
			 :max_id,
			 :i_budget_version,
			 user,
			 :started_at,
			 :finished_at,
			 sum(total),
			 count(*),
			 :insert_where_clause||' '||:i_additional_where_clause,
			 :delete_where_clause||' '||:i_additional_delete_where_clause,
			 sysdate,
			 user
	  from wo_est_monthly_cr;
elseif staging_table = "BUDGET_MONTHLY_DATA_CR" then 
	insert into cr_to_budget_to_cr_audit (
		id, occurrence_id, budget_version, running_user_id, start_time, end_time, total_amount, 
		total_records, insert_where_clause, delete_where_clause, time_stamp, user_id)
	select :i_id,
			 :max_id,
			 :i_budget_version,
			 user,
			 :started_at,
			 :finished_at,
			 sum(total),
			 count(*),
			 :insert_where_clause||' '||:i_additional_where_clause,
			 :delete_where_clause||' '||:i_additional_delete_where_clause,
			 sysdate,
			 user
	  from budget_monthly_data_cr;
end if
  
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_to_budget_to_cr_audit: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if

return 1
end function

public function longlong uf_to_cr ();//*****************************************************************************************
//
//   Function  :  uf_to_cr
//
//	 Description : 	This function will be called from uf_read to send Projects data
//							(either wo_est_monthly or budget_monthly_data) to cr_budget_data. 
//                   This function should always be called from uf_read as uf_read 
//							determines which type of posting is running as well as doing some
//                   intial setup work, including intializing instance variables, that  
//							this function will need.
//
//  Arguments: None 
//
//*****************************************************************************************
string sqls_ds, sqls_insert, sqls_select, sqls_where, sqls_final, to_column_name, from_column, gl_journal_cat
boolean b_inception_to_date_amount, b_source_id, b_gl_journal_category, b_quantity
longlong rtn, i, source_id, from_table_column, max_id

string insert_sqls, select_sqls, sqls, field, ack_list, ack_default_value, ack_select_list, &
		 insert_where_clause, delete_where_clause, col_select_list, group_select_list, s_date
longlong num_rows, num_elements, ack_pos, col_order
date ddate
time ttime
datetime started_at, finished_at

/*### - MDZ - 10748 - 20120814*/
boolean is_fiscal
string table_to_use

is_fiscal = false

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
started_at = datetime(ddate, ttime)

if i_cr_table = 'none' then
	i_cr_table = 'CR_BUDGET_DATA'
end if

select costrepository.nextval into :g_batch_id from dual;
if isnull(g_batch_id) then g_batch_id = '0'

if g_batch_id = '0' then
	uf_msg("  ")
	uf_msg("ERROR: Could not get the interface_batch_id from costrepository.nextval")
	uf_msg("  ")
	return -1
end if		

/*### - MDZ - 10748 - 20120814*/
/*Are we using the fiscal year concept?  If yes, then this assumeS table pp_calendar is properly configured.*/
setnull(rtn)
rtn = long(g_cr.uf_get_control_value('INITIAL MONTH OF FISCAL YEAR')) //Autogenerated sql replace

if isnull(rtn) then rtn = 1

if rtn <> 1 then is_fiscal = true

if is_fiscal then
	/*wo_est_monthly_fy_bdg_v is a view that translates wo_est_monthly by fiscal month using pp_calendar.*/
	table_to_use = 'wo_est_monthly_fy_bdg_v'
else
	table_to_use = i_projects_table
end if

///use this value if they don't provide a gl_journal category in the mapping table
gl_journal_cat = 'PM'

b_inception_to_date_amount = false
b_source_id = false
b_gl_journal_category = false
b_quantity = false

// Truncate cr_budget_data_projects
sqlca.truncate_table('cr_budget_data_projects');
	
if sqlca.SQLCode < 0 then
	uf_msg("ERROR: Truncating cr_budget_data_projects : " + &
			 sqlca.SQLErrText )
	rollback;
	return -1
end if

// Build the posting columns ds
sqls_ds = " select replace(replace(replace(to_column_name,' ','_'),'-','_'),'/','_') to_column_name, " + &
	       "from_column from_column, " + &
		 	 "nvl(from_table_column, 0) from_table_column, " + &
			 ' "TYPE" col_type, "WIDTH" col_width, "DECIMAL" col_decimal, '+&
			 ' "ORDER" col_order ' + &
			 " from cr_to_budget_to_cr_columns" + &
			 " where id = " + string(i_id) + " order by " + '"ORDER"'
		 
uo_ds_top ds_columns
ds_columns = CREATE uo_ds_top
		 
f_create_dynamic_ds(ds_columns,"grid",sqls_ds,sqlca,true)
num_rows = ds_columns.rowcount()

if num_rows < 1 then
	uf_msg('ERROR: Unable to find Budget Columns with posting id ' + string(i_id))
	return -1
end if

// Build the CR elements ds, the ACK list, and the ACK part of the 
// col_select_list and group_select_list
uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
ds_elements.DataObject = "dw_cr_element_definitions"  //sorted by "ORDER"
ds_elements.SetTransObject(sqlca)
num_elements = ds_elements.RETRIEVE()

uf_msg("Building ACK list")

for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	col_order = ds_elements.GetItemNumber(i, "order") + 1
	
	ack_list = ack_list + '"' + upper(field) + '", '
	ack_select_list = ack_select_list + '"' + upper(field) + '", '
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
next

uf_msg("Building Insert SQLS")

// Build the insert statement to insert into cr_budget_data_projects
insert_sqls = insert_sqls + '"ID", ' + ack_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))	
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	Choose case to_column_name
		case "ID", "BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "AMOUNT", "MONTH_NUMBER", &
			  "MONTH_PERIOD", "GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", &
			  "ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", &			
  			  "WORK_ORDER_ID", "INTERFACE_BATCH_ID"
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case "QUANTITY"
			b_quantity = true
		case "INCEPTION_TO_DATE_AMOUNT"
			b_inception_to_date_amount = true
		case "GL_JOURNAL_CATEGORY"
			b_gl_journal_category = true
		case "SOURCE_ID"
			b_source_id = true
		case else
			// determine whether to_column_name is part of the ACK		
			if col_order >= 2 and col_order <= num_elements + 1 then
				// it is an ACK element, skip it
				continue
			end if
			
	end Choose
	
	insert_sqls = insert_sqls + '"' + to_column_name + '", '
	
next

insert_sqls = insert_sqls + '"AMOUNT", "MONTH_NUMBER", "WORK_ORDER_ID", '

// If the user mapped quantity, inception_to_date_amount, gl_journal_category,
// or source_id in the control window, the field(s) will already be in insert_sqls.
// Otherwise, we need to add them and we'll map default values below.
if not b_quantity	then
	insert_sqls = insert_sqls + '"QUANTITY", '
end if

if not b_inception_to_date_amount then
	insert_sqls = insert_sqls + '"INCEPTION_TO_DATE_AMOUNT", '
end if

if not b_gl_journal_category then
	insert_sqls = insert_sqls + '"GL_JOURNAL_CATEGORY", '
end if

if not b_source_id then
	setnull(source_id)
source_id = g_cr.uf_get_source_id("description",'POWERPLANT') //Autogenerated sql replace
	
	if isnull(source_id) then
		select min(source_id) into :source_id
		  from cr_sources;
	end if
	
	insert_sqls = insert_sqls + '"SOURCE_ID", '
end if

insert_sqls = insert_sqls + '"BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "MONTH_PERIOD",' +  &
			     '"GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", ' + &
				  '"ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", "INTERFACE_BATCH_ID" '

uf_msg("Building Select SQLS")

// Build the select statement for the ACK based on the mappings in the control window.
// NOTE: The idea here is to start from a list of the ACK and replace the field
//			with the mapping entered by the user.  
for i = 1 to num_rows
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if col_order >= 2 and col_order <= num_elements + 1 then
		// part of the ACK -- ID is always first column
		to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
		from_column = ds_columns.GetItemString(i, "from_column")
		from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
		
		if from_column = "'_'" then from_column = "' '"
		
		if from_table_column = 1 then
			from_column = uf_clean_string(from_column)
			// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
			// alias the columns with supp or pt.
			if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
				from_column = "SUPPLEMENTAL_TYPE_ID" then
				
				from_column = 'supp."' + upper(from_column) + '"'
			else
				from_column = 'pt."' + upper(from_column) + '"'
			end if
		end if
		
		ack_default_value = ds_elements.GetItemString(col_order - 1, "default_value")
		// We want to nvl everything since the ACK fields are non-nullable
		from_column = "nvl(" + from_column + ", '" + ack_default_value + "') c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, '"' + to_column_name + '"', from_column, 'first')
	end if
next

// Figure out which ACK elements are not mapped by the user and replace with the default value.
for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	field = '"' + upper(field) + '"'

	ack_pos = 0
	ack_pos = pos(ack_select_list, field)
	
	if isnull(ack_pos) then
		uf_msg("Null Value...")
		return -1
	elseif ack_pos > 0 then
		// has not been mapped yet
		ack_default_value = ds_elements.GetItemString(i, "default_value")
		
		col_order = ds_elements.GetItemNumber(i, "order") + 1
		from_column = "'" + ack_default_value + "' c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, field, from_column, 'first')
	end if
next

// Build the select statement -- we've already built the ACK part of the select statement, so
// now we need to build the rest of the statement starting with the non-ACK fields that the
// user mapped in the control window.
// NOTE: In order to allow for subselects to be entered in the mappings, I chose to build
//			the sql so that everything the user enters goes into it's own view, including the
// 		the amount and month number - which the user can't map, so that the sums will work
//			properly without requiring the user to be clever about the way they enter their
//			subselects.  
//			Below three things are built: 
//					select_sqls - contains the SQL going into the view mentioned above
//					col_select_list - the list of every column in the view (minus amount, 
//						month number, and work order id)
//					group_select_list - the list of every column in the view that is not
//						being summed (minus month number and work order id)
select_sqls = select_sqls + ack_select_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
	Choose case to_column_name
		case "ID", "BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "AMOUNT", "MONTH_NUMBER", &
			  "MONTH_PERIOD", "GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", &
			  "ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", &			
  			  "WORK_ORDER_ID", "INTERFACE_BATCH_ID"
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case else
			col_order = ds_columns.GetItemNumber(i, "col_order")
			if col_order >= 2 and col_order <= num_elements + 1 then
				// part of the ACK
				continue
			end if
	end Choose
	
	from_column = ds_columns.GetItemString(i, "from_column")
	from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if from_column = "'_'" then from_column = "' '"
	
	if from_table_column = 1 then
		
		from_column = uf_clean_string(from_column)
		// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
		// alias the columns with supp or pt.
		if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
			from_column = "SUPPLEMENTAL_TYPE_ID" then
			
			from_column = 'supp."' + upper(from_column) + '"'
		else
			from_column = 'pt."' + upper(from_column) + '"'
		end if
				
		
	else
		// Need to support sum -- i.e. the user can map into the quantity field
		if left(upper(from_column),4) = 'SUM(' then
			// trim off 'SUM(' from the front and ')' from the end
			from_column = left(from_column, len(from_column) - 1)
			from_column = right(from_column, len(from_column) - 4)
			
			select_sqls = select_sqls + from_column + " s" + string(col_order) + ", "
			col_select_list = col_select_list + " sum(y.s" + string(col_order) + "), "
			continue
		end if
	end if
	
	select_sqls = select_sqls + from_column + " c" + string(col_order) + ", "
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
	
next

// Get the insert where clause
if i_insert_where_clause = 0 or isnull(i_insert_where_clause) then
	// User did not pick a where clause so skip this part
else
	uf_msg("Building Insert Where Clause")
	insert_where_clause = uf_build_where_clause(i_insert_where_clause)
end if
if isnull(insert_where_clause) then insert_where_clause = ""

if insert_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the insert where clause.")
	rollback;
	return -1
end if

uf_msg("Inserting into cr_budget_data_projects")

// Build sqls
sqls  = 'insert into cr_budget_data_projects ( '
sqls += insert_sqls + ") "
sqls += ' select crbudgets.nextval, x.*, '
if not b_quantity then sqls += '0, '
if not b_inception_to_date_amount then sqls += '0, '
if not b_gl_journal_category then sqls += "'" + gl_journal_cat + "', "
if not b_source_id then sqls += string(source_id) + ', '
// ### - SEK - 5978 - 122110
//sqls += "'" + i_budget_version + "', "
sqls += "'" + i_cr_budget_version + "', "
sqls += "2, " //amount_type
sqls += 'decode(sign(x.amount),0,1,sign(x.amount)), ' //dr_cr_id
sqls += '1, ' //ledger_sign
sqls += '0, ' //month_period
sqls += 'null, ' //gl_id
sqls += string(g_batch_id) + ', ' //batch_id
sqls += 'null, ' //cwip_charge_status
sqls += 'sysdate, ' //time_stamp
sqls += 'user, ' //user_id
//"ALLOCATION_ID",
if i_alloc then
	sqls += "(select -99 from estimate_charge_type e where pt.est_chg_type_id = e.est_chg_type_id and nvl(e.afudc_flag,'U') = 'O'), "
else
	sqls += " null,"
end if
sqls += 'null, null, null, ' // "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY"
sqls += string(g_batch_id) + ' ' //"INTERFACE_BATCH_ID"
sqls += 'from ( '
sqls += 	'select ' + col_select_list 
sqls +=	 'sum(y.amount) amount, y.month_number, y.work_order_id from ( '
sqls +=		'select ' + select_sqls
if upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
	sqls +=	'nvl(decode(pp_table_months.month_num, 1, supp.january, 2, supp.february, ' + &
					  '3, supp.march, 4, supp.april, 5, supp.may, 6, supp.june, 7, supp.july, ' + &
					  '8, supp.august, 9, supp.september, 10, supp.october, 11, supp.november, ' + &
					  '12, supp.december), ' + &
					  'decode(pp_table_months.month_num, 1, pt.january, 2, pt.february, ' + &
					  '3, pt.march, 4, pt.april, 5, pt.may, 6, pt.june, 7, pt.july, ' + &
					  '8, pt.august, 9, pt.september, 10, pt.october, 11, pt.november, ' + &
					  '12, pt.december) ' + &
					 ') amount, '
else
	sqls +=	'decode(pp_table_months.month_num, 1, pt.january, 2, pt.february, ' + &
				 '3, pt.march, 4, pt.april, 5, pt.may, 6, pt.june, 7, pt.july, ' + &
				 '8, pt.august, 9, pt.september, 10, pt.october, 11, pt.november, ' + &
				 '12, pt.december) amount, '
end if
sqls +=			"to_number(pt.year||lpad(pp_table_months.month_num, 2, '0')) month_number, "
sqls +=			'pt.work_order_id work_order_id '
sqls +=		'from '
if upper(i_projects_table) = "WO_EST_MONTHLY" and not i_wo_est_proc_temp then
	/*### - MDZ - 10748 - 20120814*/
//	sqls += 	i_projects_table + ' pt, budget_version, budget_version_fund_proj, pp_table_months '
	sqls += 	table_to_use + ' pt, budget_version, budget_version_fund_proj, pp_table_months '
	sqls +=  'where pt.revision = budget_version_fund_proj.revision '
	sqls +='    and budget_version_fund_proj.active = 1 '
	sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
	sqls +=  '  and budget_version_fund_proj.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
elseif upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
	sqls += 	'wo_est_monthly pt, budget_version, budget_version_fund_proj, pp_table_months, '
	sqls +=  'wo_est_supplemental_data supp '
	sqls +=  'where pt.revision = budget_version_fund_proj.revision '
	sqls +='    and budget_version_fund_proj.active = 1 '
	sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
	sqls +=  '  and budget_version_fund_proj.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
	sqls +=  "  and pt.est_monthly_id = supp.est_monthly_id (+)"
	sqls +=  "  and pt.year = supp.year (+)"
elseif upper(i_projects_table) ='BUDGET_MONTHLY_DATA' then
	sqls +=  i_projects_table + ' pt, budget_version, pp_table_months '
	sqls +=  'where pt.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
elseif i_wo_est_proc_temp then
	sqls += 	i_projects_table + ' pt,  pp_table_months, wo_est_processing_temp '
	sqls +=  'where pt.revision = wo_est_processing_temp.revision '
	sqls +=  '  and pt.work_order_id = wo_est_processing_temp.work_order_id '
else
	return -1
end if
sqls += insert_where_clause +i_additional_where_clause+ ' ) y '
sqls += 'group by ' + group_select_list + ' y.month_number, y.work_order_id) x'


execute immediate :sqls;

if sqlca.sqlcode < 0 then
	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

//***********************************************
// CALL TO CUSTOM FUNCTION FOR PROCESSING OF DATA
//***********************************************
uf_msg("Calling custom function")
rtn = f_cr_to_budget_to_cr_custom(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr',i_cr_budget_version,this)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_to_cr' ) = 1 then
	rtn = sqlca.f_client_budget_to_cr(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr',i_cr_budget_version)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = string(i_id)
args[2] = insert_where_clause
args[3] = delete_where_clause
args[4] = i_cap_budget_version
args[5] = 'uf_to_cr'
args[6] = i_cr_budget_version
rtn = f_wo_validation_control(84,args)
if rtn < 0 then
	rollback;
	return -1
end if

// Get the delete where clause
if i_delete_where_clause = 0 or isnull(i_delete_where_clause) then
	// the user did not select a where clause so skip this part
else
	delete_where_clause = uf_build_where_clause(i_delete_where_clause)
end if
if isnull(delete_where_clause) then delete_where_clause = ""

if delete_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the delete where clause.")
	rollback;
	return -1
end if

uf_msg("Deleting from cr_budget_data")

// Delete from cr_budget_data and then insert stg data into cr_budget_data
// ### - SEK - 5978 - 122110
sqls = "delete from "+i_cr_table+" cr " + &
		 "where budget_version = '" + i_cr_budget_version + "' " + delete_where_clause + i_additional_delete_where_clause
//		 "where budget_version = '" + i_budget_version + "' " + delete_where_clause + i_additional_delete_where_clause
		
execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Deleting from " + i_cr_table + ": " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

uf_msg("Inserting into "+i_cr_table)
sqls = "insert into "+i_cr_table+" (" + insert_sqls + ") " + &
		 "select " + insert_sqls + " from cr_budget_data_projects"

execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into " + i_cr_table + ": " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

commit;

// Insert into cr_to_budget_to_cr_audit -- do this after the commit because it's not
// a big deal if this fails
uf_msg("Inserting into cr_to_budget_to_cr_audit")

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
finished_at = datetime(ddate, ttime)

select max(occurrence_id) into :max_id
  from cr_to_budget_to_cr_audit
 where id = :i_id;
 
if isnull(max_id) then max_id = 0
max_id++

insert into cr_to_budget_to_cr_audit (
	id, occurrence_id, budget_version, running_user_id, start_time, end_time, total_amount, 
	total_records, insert_where_clause, delete_where_clause, time_stamp, user_id)
select :i_id,
		 :max_id,
		 :i_budget_version,
		 user,
		 :started_at,
		 :finished_at,
		 sum(amount),
		 count(*),
			 :insert_where_clause||' '||:i_additional_where_clause,
			 :delete_where_clause||' '||:i_additional_delete_where_clause,
		 sysdate,
		 user
  from cr_budget_data_projects;
  
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_to_budget_to_cr_audit: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if  
		 


return 1


end function

public function string uf_build_where_clause (longlong a_where_clause_id);//*************************************************************************************************
//
//	UO Function : uf_build_where_clause
//
//	Description : This function builds the where clause from the where clause id that is
//					  passed in by the user.  
//
//	Arguments	: a_where_clause_id: long
//
// Returns		: string : the where clause
//
//*************************************************************************************************
string sqls, rtns, where_clause, left_paren, col_name, operator, not_upper_value1, orig_value1, value1, &
		between_and, value2, right_paren, and_or, structure_where, table_name, table_alias
longlong i, num_rows, structure_id, not_field

//  System switch for the new structure values table.
setnull(i_use_new_structures_table)
i_use_new_structures_table = upper(trim(g_cr.uf_get_control_value( 'USE NEW CR STRUCTURE VALUES TABLE' )))
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"

// Create ds_where
sqls = "select * from cr_budget_where_clause where where_clause_id = " + string(a_where_clause_id)

uo_ds_top ds_where
ds_where = CREATE uo_ds_top

rtns = f_create_dynamic_ds(ds_where, 'grid', sqls, sqlca, true)

if rtns <> 'OK' then
	uf_msg("ERROR: Creating ds_where; sqls " + sqls)
	return "ERROR"
end if

if ds_where.rowcount() < 1 then
	uf_msg("ERROR: Unable to find Where Clause columns with the Where Clause Id: " + string(a_where_clause_id))
	return "ERROR"
end if

select budget_or_actuals into :table_name from cr_budget_where where where_clause_id = :a_where_clause_id;

Choose case table_name
	case 'cr_budget_data'
		table_alias = "cr."
	case 'wo_est_monthly', 'budget_monthly_data'
		table_alias = "pt."
	case else
		table_alias = ""
end Choose

for i = 1 to ds_where.rowcount()
	
	if i = 1 then
		where_clause = where_clause + " and ( "
	end if
	
	left_paren  = trim(ds_where.GetItemString(i, "left_paren"))
	col_name    = trim(ds_where.GetItemString(i, "column_name"))
	operator    = upper(trim(ds_where.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(ds_where.GetItemString(i, "value1"))
	orig_value1 = ds_where.GetItemString(i, "value1")
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{" then
			value1 = not_upper_value1
		else
			value1 = trim(ds_where.GetItemString(i, "value1"))
		end if
	end if
	
	between_and = trim(ds_where.GetItemString(i, "between_and"))
	value2      = trim(ds_where.GetItemString(i, "value2"))
	right_paren = trim(ds_where.GetItemString(i, "right_paren"))
	and_or      = trim(ds_where.GetItemString(i, "and_or"))
	structure_id = ds_where.GetItemNumber(i, "structure_id")
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0

	//  If they manually entered an Oracle function, take it as-is.
	if pos(col_name, "(") = 0 then
		col_name = f_cr_clean_string(col_name)
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = uf_structure_value(structure_id, col_name, value1, not_field)
		if structure_where = "ERROR" then
			rollback;
			return "ERROR"
		end if
		where_clause = &
			where_clause + left_paren + structure_where + right_paren + " " + and_or + " "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			//  NO DOUBLE QUOTES ON COL_NAME!
			if pos(col_name, "(") <> 0 then
				where_clause = where_clause + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
			else
				where_clause = where_clause + &
										left_paren + ' ' + table_alias + col_name + ' ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
			end if
		else
			//  NO DOUBLE QUOTES ON COL_NAME!
			if pos(col_name, "(") <> 0 then
				where_clause = where_clause + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
			else
				where_clause = where_clause + &
										left_paren + ' ' + table_alias + col_name + ' ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
			end if
		end if
	else
		//  NO DOUBLE QUOTES ON COL_NAME!
		if pos(col_name, "(") <> 0 then
			where_clause = where_clause + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
		else
			where_clause = where_clause + &
									left_paren + ' ' + table_alias + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
		end if
	end if
	
	bottom:
	
	if i = ds_where.rowcount() then
		where_clause = where_clause + " )"
	end if
	
next

return where_clause

end function

public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field);//******************************************************************************************
//
//  UO Function  :  uf_structure_value
//
//  Description      :  Builds the piece of the where clause for hierarchical queries
//								that use the cr_structures.
//
//******************************************************************************************
longlong comma_pos, start_pos, counter, the_last_comma_pos, string_length, i, status
string rollup_value_id, where_clause, description_edit_values[], rollup_value_id_list


where_clause         = " "
rollup_value_id_list = " ("


//-------  Parse out the rollup values into an array, if the user selected more  -----------
//-------  than 1 rollup value.                                                  -----------
uf_parse(a_description_edit, "{", description_edit_values)


for i = 1 to upperbound(description_edit_values)
	
	//-------  First, must parse out the value_id from the chosen element_value  ------------
	start_pos = 1
	counter   = 0
	
	if i_use_new_structures_table = "YES" then
		//  These need to be harcoded to 0 in case the element_value contains a comma.
		comma_pos = 0
		the_last_comma_pos = 0
	else
		do while true
		
			counter++
		
			//  Endless loop protection ...
			if counter > 1000 then
				uf_msg("Endless loop in uf_structure_value.")
				exit
			end if
		
			comma_pos = pos(description_edit_values[i], ",", start_pos)
		
			if comma_pos = 0 then exit
		
			the_last_comma_pos = comma_pos
			start_pos          = comma_pos + 1
		
		loop
	end if

	string_length = len(description_edit_values[i])

	if i_use_new_structures_table = "YES" then
		//  "{CWIP}" goes to "CWIP}"
		rollup_value_id = right(description_edit_values[i], (string_length - the_last_comma_pos) - 1)
	else
		//  "{CWIP, 15}" goes to " 15}"
		rollup_value_id = right(description_edit_values[i], string_length - the_last_comma_pos)
	end if

	rollup_value_id = trim(left(rollup_value_id, len(rollup_value_id) - 1))

	status = 0
	if i_use_new_structures_table = "YES" then
		select status into :status from cr_structure_values2
		 where structure_id = :a_structure_id and element_value = :rollup_value_id;
	else
		select status into :status from cr_structure_values
		 where structure_id = :a_structure_id and value_id = :rollup_value_id;
	end if
	 
	if isnull(status) or status = 0 then
		uf_msg("ERROR: the value_id (" + rollup_value_id + ") is INACTIVE on " + &
			" structure_id (" + string(a_structure_id) + ")")
		return "ERROR"
	end if
	
	if i_use_new_structures_table = "YES" then
		rollup_value_id = "'" + rollup_value_id + "'"
	end if

	if i = upperbound(description_edit_values) then
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ") "
	else
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ","
	end if


	//-------  Build the piece of the where clause to be passed back to the  ---------------- 
	//-------  calling script                                                ----------------
	if i = upperbound(description_edit_values) then
		
		if a_not_field = 0 then
			//  The user clicked the "NOT" checkbox.
			where_clause = where_clause + " " + a_description + " not in ("
		else
			where_clause = where_clause + " " + a_description + " in ("
		end if
		
		if i = 1 then
			if i_use_new_structures_table = "YES" then
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value = " + rollup_value_id + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id = " + rollup_value_id + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		else
			if i_use_new_structures_table = "YES" then
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value in " + rollup_value_id_list + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id in " + rollup_value_id_list + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		end if
	end if

next

return where_clause
end function

public function longlong uf_parse (string a_stringtoparse, string a_separator, string a_array_to_hold[]);//*****************************************************************************************
//
//  UO Function  :  uf_parse
//
//  Description      :  This function will parse string  into an array of elements denoted
//								by the separator.  The number of elements parsed is the return 
//								value of the function.  For example, if ~t were the separator, 
//								"red~twhite~tblue" would be parsed into an array containing red, 
//								white and blue.  The value 3 would be returned.
//
//  Notes            :  A MODIFIED VERSION OF:  f_parsestringintoarray from ppsystem.
//								This functions MUST INCLUDE the "separator" in the string that is
//								placed in the array (The "{").
//
//  Arguments :	a_stringtoparse :		string			      : string to parse
//						a_separator	    :		string			      : what to look for
//						a_array_to_hold : 	string array(by ref) : gets filled with values
//
//   
//  Returns   :  longlong :  The number of elements returned into the array.
//								  If the separator is not found then the original string is 
//                        placed in array_to_hold and 1 is returned.
//
//******************************************************************************************
longlong separator_pos, separator_len, count, start_pos
string holder

start_pos     = 1
separator_len = Len(a_separator)
separator_pos = Pos(a_stringtoparse, a_separator, start_pos)
IF Trim(a_stringtoparse) = "" or IsNull(a_stringtoparse) THEN 
	RETURN 0
ELSEIF separator_pos = 0 THEN 
	a_array_to_hold[1] = trim(a_stringtoparse)
	RETURN 1
END IF
DO WHILE separator_pos > 0
	if count = 0 then  //  Modified
		count++         //  Modified
		goto bottom		 //  Modified
	end if				 //  Modified
	holder = Mid(a_stringtoparse, start_pos - 1, (separator_pos - start_pos) + 1)  //  Modified
	a_array_to_hold[count] = trim(holder)
	count++  //  Moved below the previous line
	bottom:  //  Modified
	start_pos = separator_pos + separator_len
	separator_pos =  Pos(a_stringtoparse, a_separator, start_pos)	
LOOP
holder = Mid(a_stringtoparse, start_pos - 1, Len(a_stringtoparse))
//  NOT IN THIS WINDOW FUNCTION ... //count = count + 1
a_array_to_hold[count] = trim(holder)

RETURN count


end function

public function string uf_clean_string (string a_string);//*****************************************************************************************************************
//
//  Function:     uf_clean_string (copy of f_cr_clean_string)
//
//  Description:  Change any bad characters to underscores
//
//*****************************************************************************************************************

string source, bad_characters[]
longlong start_pos
longlong i, counter

source  = a_string
counter = 0
bad_characters[1] = "/"
bad_characters[2] = " "
bad_characters[3] = "-"

for i = 1 to upperbound(bad_characters)
	start_pos = 1
	// Find the first occurrence of bad_characters.
	start_pos = pos(source, bad_characters[i], start_pos)

	// Only enter the loop if you find bad_characters.
	do while start_pos > 0

		// Replace bad_characters with "_".
		source = Replace(source, start_pos, len(bad_characters[i]), "_")

	// Find the next occurrence of old_str.

	start_pos = Pos(source, bad_characters[i], start_pos + 1)
	
	//  Endless loop protection ...
	counter ++
	if counter = 100 then 
		messagebox("!!!", "You have an endless loop.")
		exit
	end if
	
	loop
next

return source
end function

public function longlong uf_read (longlong a_id, longlong a_insert_where_clause, longlong a_delete_where_clause, string a_budget_version);//**************************************************************************************************
//
//  UO Function : uf_read
//
//	 Description : Driving function for posting Project data (budget monthly or 
//						wo_est_monthly) to cr_budget_data or the reverse sending 
//						cr_budget_data back to wo_est_monthly or budget_data.  The 
//						function selects the columns to be posted and builds the SQL
//
//  Arguments	 : a_id : The posting id the process should be running.  the posting Id 
//								 defines what tables are involved (to and from) and how the mapping 
//								 should work
//
//						a_insert_where_clause : a string that does not include the word where to be used
//								 as a where clause for sending partial budget data.  This where clause 
//								 needs to work on both the to and from table.  This gives us the ability 
//								 to send one funding project over at a time if needed.
//
//					   a_delete_where_clause
//
//					
//						a_budget_version_id : The budget version name being sent.  
//
//	 Notes		: 1. It is assumed that the Jan - Dec amount fields on 
//				 		  wo_est_monthly/budget_monthly_data go in amount field in cr_budget_data
//				 		  a row for them is not needed in the mapping table
//			 		  2. How do we deal with quantities and units? - 5978: SEK: Added a field on
//						  cr_to_budget_to_cr_control called map_quantity that indicates whether and
//						  and how the quantity should be mapped from the projects table to 
//						  cr_budget_data_entry... should log another maint request to extend this logic
//						  to the rest of the tables.
//**************************************************************************************************
string company_str, posting_description, sqls, typ, to_column_name, from_column, &
		 fields[], company
longlong rtn, to_projects, wid, deci, i, d, counter

uo_ds_top ds_columns
ds_columns = create uo_ds_top


setnull(to_projects)
setnull(i_projects_table)
setnull(company_str)
i_id = a_id
i_insert_where_clause = a_insert_where_clause
i_delete_where_clause = a_delete_where_clause
i_budget_version = a_budget_version

// ### - SEK - 5978 - 122110: Allowing the user to select the CR budget version
//		from w_cr_to_budget_to_cr window.  The CR budget version should be set
//		to i_cr_budget_version in the window prior to calling this function.  If
//		it is not set, the i_cr_budget_version will be defaulted to i_budget_version.
i_cap_budget_version = i_budget_version
if isnull(i_cr_budget_version) or i_cr_budget_version = "" then
	i_cr_budget_version = i_budget_version
end if


// Check to see that the budget version is set up in both projects and the CR
counter = 0
// ### - SEK - 5978 - 122110
select count(*) into :counter
  from cr_budget_version
 where budget_version = :i_cr_budget_version;  
// where budget_version = :i_budget_version;
if isnull(counter) then counter = 0 
 
if counter < 1 and mid(a_budget_version,1,4) <> 'FAKE' then 
	// ### - SEK - 5978 - 122110
//	uf_msg("ERROR: The selected budget version '" + i_budget_version + "' is not set up in the CR!")
	uf_msg("ERROR: The selected budget version '" + i_cr_budget_version + "' is not set up in the CR!")
	return -1
end if

// ### - SEK - 6619 - 021111: Moved from below...
// ### - SEK - 5978 - 113010: Added map_quantity
select replace(replace(replace(projects_table,' ','_'),'-','_'),'/','_'), to_projects, description, nvl(map_quantity,0)
  into :i_projects_table, :to_projects, :i_posting_description, :i_map_quantity
  from cr_to_budget_to_cr_control
 where id = :a_id;
 
if sqlca.sqlcode < 0 then
	uf_msg('SQL ERROR: Error selecting from cr_to_budget_to_cr_control: ' + sqlca.sqlerrtext)
	return -1
end if

if isnull(to_projects) then
	uf_msg('ERROR: To Projects indicator is null; cannot proceed')
	uf_msg('ERROR: please check setup and correct')
	return -1
end if

if isnull(i_projects_table) then
	uf_msg('ERROR: The Projects table is null; cannot proceed')
	uf_msg('ERROR: please check setup and correct')
	return -1
end if
 
// ### - SEK - 6619 - 021111
if upper(i_projects_table) = 'FCST_DEPR_LEDGER' or upper(i_projects_table) = 'FCST_CPR_DEPR' then
	// skip this - we know they picked a valid version
else
	counter = 0 
	// ### - SEK - 5978 - 122110
	select count(*) into :counter
	  from budget_version
	 where description = :i_cap_budget_version;
	// where description = :i_budget_version; 
	if isnull(counter) then counter = 0 
	 
	if counter < 1 and mid(a_budget_version,1,4) <> 'FAKE' then 
		// ### - SEK - 5978 - 122110
	//	uf_msg("ERROR: The selected budget version '" + i_budget_version + "' is not set up in the Projects module!")
		uf_msg("ERROR: The selected budget version '" + i_cap_budget_version + "' is not set up in the Projects module!")
		return -1
	end if
end if

// Call custom function
rtn = f_cr_to_budget_to_cr_custom(i_id, ' ', ' ', i_cap_budget_version, 'uf_read',i_cr_budget_version,this)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_to_cr' ) = 1 then
	rtn = sqlca.f_client_budget_to_cr(i_id, ' ', ' ', i_cap_budget_version, 'uf_read',i_cr_budget_version)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = string(i_id)
args[2] = ' '
args[3] = ' '
args[4] = i_cap_budget_version
args[5] = 'uf_read'
args[6] = i_cr_budget_version
rtn = f_wo_validation_control(84,args)
if rtn < 0 then
	rollback;
	return -1
end if

// ### - SEK - 6619 - 021111: Moved above
//// ### - SEK - 5978 - 113010: Added map_quantity
//select replace(replace(replace(projects_table,' ','_'),'-','_'),'/','_'), to_projects, description, nvl(map_quantity,0)
//  into :i_projects_table, :to_projects, :i_posting_description, :i_map_quantity
//  from cr_to_budget_to_cr_control
// where id = :a_id;
// 
//if sqlca.sqlcode < 0 then
//	uf_msg('SQL ERROR: Error selecting from cr_to_budget_to_cr_control: ' + sqlca.sqlerrtext)
//	return -1
//end if
//
//if isnull(to_projects) then
//	uf_msg('ERROR: To Projects indicator is null; cannot proceed')
//	uf_msg('ERROR: please check setup and correct')
//	return -1
//end if
//
//if isnull(i_projects_table) then
//	uf_msg('ERROR: The Projects table is null; cannot proceed')
//	uf_msg('ERROR: please check setup and correct')
//	return -1
//end if

uf_msg(i_posting_description)
uf_msg(' ')
if to_projects = 1 then
	// ### - SEK - 5978 - 122110
//	uf_msg('Sending CR Budget Data for Budget Version '+i_budget_version+' to Projects ('+i_projects_table+')')
	uf_msg('Sending CR Budget Data for Budget Version '+i_cr_budget_version+' to Projects ('+i_projects_table+')')
else
	// ### - SEK - 5978 - 122110
//	uf_msg('Sending Project Budget Data ('+i_projects_table+') for Budget Version '+i_budget_version+' to the CR')
	uf_msg('Sending Project Budget Data ('+i_projects_table+') for Budget Version '+i_cap_budget_version+' to the CR')
end if

if not isnull(i_insert_where_clause) and i_insert_where_clause <> 0 then
	uf_msg('--Insert Where Clause Id: ' + string(i_insert_where_clause))
end if

if not isnull(i_delete_where_clause) and i_delete_where_clause <> 0 then
	uf_msg('--Delete Where Clause Id: ' + string(i_delete_where_clause))
end if

if to_projects = 1 then
	uf_msg('Calling the uf_to_projects function')
	rtn = uf_to_projects()
	uf_msg("Finished")
	return rtn
else
	// ### - SEK - 5978 - 113010
	if i_cr_table = 'none' or lower(i_cr_table) = 'cr_budget_data' then
		// ### - SEK - 6618 - 020311
		if lower(i_projects_table) = 'afudc___wo_est_monthly' or lower(i_projects_table) = 'afudc___budget_monthly_data' then
			uf_msg('Calling the uf_to_cr_afudc_oh function')
			rtn = uf_to_cr_afudc_oh()
			uf_msg('Finished')
		// ### - SEK - 6619 - 020311
		elseif lower(i_projects_table) = 'fcst_depr_ledger' or lower(i_projects_table) = 'fcst_cpr_depr' then
			uf_msg('Calling the uf_to_cr_depr_fcst function')
			rtn = uf_to_cr_depr_fcst()
			uf_msg('Finished')
		else
			uf_msg('Calling the uf_to_cr function')
			rtn = uf_to_cr()
			uf_msg("Finished")
		end if
		return rtn
	else
		uf_msg('Calling the uf_to_cr_entry function')
		rtn = uf_to_cr_entry()
		uf_msg('Finished')
		return rtn
	end if
end if

return 1

return rtn
end function

public function integer uf_labor_cap_dollars ();//*****************************************************************************************
//
//  Function    : uf_cap_labor_dollars
//
//	 Description : This function will be called from within the CR budget entry,
//						labor entry, and labor report screens.  This function is not
//						referenced in any other functions in this user object and is 
//						intended to stand alone from the functionality in the rest of 
//						the user object.
//
//  Arguments   : None 
//
//  Notes		 : The following instance variables must be set in the script of 
//						the calling window:
//						i_id - the posting id from cr_to_budget_to_cr_control (this will determine 
//								 the projects table being used)
//						i_cr_bv_id - the cr budget version id 
//						i_budget_version - the cr budget version description
//						i_additional_where_clause - the where clause used for the insert into
//								 the temp table
//						i_additional_delete_where_clause - the where clause used for the
//								 delete from cr_budget_labor_cap_dollars
//
//*****************************************************************************************
string sqls_ds, sqls_insert, sqls_select, sqls_where, sqls_final, to_column_name, from_column, gl_journal_cat
boolean b_inception_to_date_amount, b_source_id, b_gl_journal_category, b_quantity
longlong rtn, i, source_id, from_table_column, max_id, counter

string insert_sqls, select_sqls, sqls, field, ack_list, ack_default_value, ack_select_list, &
		 insert_where_clause, delete_where_clause, col_select_list, group_select_list, s_date
longlong num_rows, num_elements, ack_pos, col_order
date ddate
time ttime
datetime started_at, finished_at
boolean b_map_lt

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
started_at = datetime(ddate, ttime)

boolean is_fiscal
string table_to_use
is_fiscal = false

i_cr_table = 'CR_BUDGET_LABOR_CAP_DOLLARS'

// ### - SEK - 5978 - 122110:  There's no requirement to use labor types
//		when balancing so b_map_lt should not be forced to true.
//b_map_lt = true
select count(*) into :counter
  from cr_budget_labor_lt_to_ect;
  
if isnull(counter) or counter = 0 then
	b_map_lt = false	
else
	b_map_lt = true
end if

// Get the projects table based on the id for the mapping control table
select replace(replace(replace(projects_table,' ','_'),'-','_'),'/','_'), description
  into :i_projects_table, :i_posting_description
  from cr_to_budget_to_cr_control
 where id = :i_id;
 
if sqlca.sqlcode < 0 then
	uf_msg('SQL ERROR: Error selecting from cr_to_budget_to_cr_control: ' + sqlca.sqlerrtext)
	return -1
end if

if isnull(i_projects_table) then
	uf_msg('ERROR: The Projects table is null; cannot proceed')
	uf_msg('ERROR: please check setup and correct')
	return -1
elseif upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
	uf_msg('ERROR: Cannot map labor capital dollars from the supplemental table')
	return -1
end if

// Get the two budget version ids
select budget_version_id
  into :i_bv_id
  from cr_budget_labor_version_map
 where cr_budget_version_id = :i_cr_bv_id;
 
if isnull(i_bv_id) then
	uf_msg('ERROR: Could not find the budget version id in cr_budget_labor_version_map!')
	return -1
end if

//Are we using the fiscal year concept? If yes, then this assumes table pp_calendar is properly configured./
setnull(rtn)
rtn = long(g_cr.uf_get_control_value('INITIAL MONTH OF FISCAL YEAR')) //Autogenerated sql replace
if isnull(rtn) then rtn = 1
if rtn <> 1 then is_fiscal = true
if is_fiscal then
//wo_est_monthly_fy_bdg_v is a view that translates wo_est_monthly by fiscal month using pp_calendar./
table_to_use = 'wo_est_monthly_fy_bdg_v'
else
table_to_use = i_projects_table
end if

/*### - MDZ - 10259 - 20120509*/
/*Get the description for the capital budget version*/
select description
  into :i_cap_budget_version
  from budget_version
 where budget_version_id = :i_bv_id;
 
if isnull(i_bv_id) then
	uf_msg('ERROR: Could not get the budget version description')
	return -1
end if
							
// Truncate cr_bdg_labor_cap_dollars_tmp
sqlca.truncate_table('cr_bdg_labor_cap_dollars_tmp');
	
if sqlca.SQLCode < 0 then
	uf_msg("ERROR: Truncating cr_bdg_labor_cap_dollars_tmp : " + &
			 sqlca.SQLErrText )
	rollback;
	return -1
end if

//uf_msg("Inserting into cr_bdg_labor_cap_dollars_tmp")

// save columns in insert_sqls to use in both insert statements
insert_sqls = "department, funding_project, cr_budget_version_id, year, " + &
				  "jan_amt,feb_amt,mar_amt,apr_amt,may_amt,jun_amt,jul_amt,aug_amt,sep_amt,oct_amt,nov_amt,dec_amt, " + &
				  "jan_qty,feb_qty,mar_qty,apr_qty,may_qty,jun_qty,jul_qty,aug_qty,sep_qty,oct_qty,nov_qty,dec_qty, " + &
				  "jan_hrs,feb_hrs,mar_hrs,apr_hrs,may_hrs,jun_hrs,jul_hrs,aug_hrs,sep_hrs,oct_hrs,nov_hrs,dec_hrs, " + &
				  "labor_type "

// Build insert
sqls  = "insert into cr_bdg_labor_cap_dollars_tmp ( " 
sqls += insert_sqls + ") " 
sqls += "select x.department, " // department
if i_map_fp then
	sqls += "x.funding_project, "
else
	sqls += "0, "
end if
sqls += string(i_cr_bv_id) + ", " // cr_budget_version_id
sqls += "x.year, " // year
sqls += "sum(jan_amt),sum(feb_amt),sum(mar_amt),sum(apr_amt),sum(may_amt),sum(jun_amt),sum(jul_amt),sum(aug_amt),sum(sep_amt),sum(oct_amt),sum(nov_amt),sum(dec_amt), " 
sqls += "sum(jan_qty),sum(feb_qty),sum(mar_qty),sum(apr_qty),sum(may_qty),sum(jun_qty),sum(jul_qty),sum(aug_qty),sum(sep_qty),sum(oct_qty),sum(nov_qty),sum(dec_qty), " 
sqls += "sum(jan_hrs),sum(feb_hrs),sum(mar_hrs),sum(apr_hrs),sum(may_hrs),sum(jun_hrs),sum(jul_hrs),sum(aug_hrs),sum(sep_hrs),sum(oct_hrs),sum(nov_hrs),sum(dec_hrs), " 
if b_map_lt then
	sqls += "x.labor_type "
else
	sqls += "'0' "
end if
sqls += "from ( " 
sqls += 	"select (select d.external_department_code from department d where d.department_id = pt.department_id) department, " 
if i_map_fp then
	if upper(i_projects_table) = "WO_EST_MONTHLY" then
		sqls +=	 "(select woc.work_order_number from work_order_control woc where woc.work_order_id = pt.work_order_id) funding_project, "
	else // BUDGET_MONTHLY_DATA
		sqls +=	 "(select bdg.budget_number from budget bdg where bdg.budget_id = pt.budget_id) funding_project, "
	end if
end if
sqls +=			 "pt.year year, "
if upper(i_projects_table) = "WO_EST_MONTHLY" then
	sqls += "january jan_amt, february feb_amt, march mar_amt, april apr_amt, may may_amt, june jun_amt, july jul_amt, august aug_amt, september sep_amt, " + &
			  "october oct_amt, november nov_amt, december dec_amt, qty_jan jan_qty, qty_feb feb_qty, qty_mar mar_qty, qty_apr apr_qty, qty_may may_qty, " + &
			  "qty_jun jun_qty, qty_jul jul_qty, qty_aug aug_qty, qty_sep sep_qty, qty_oct oct_qty, qty_nov nov_qty, qty_dec dec_qty, hrs_jan jan_hrs, " + &
			  "hrs_feb feb_hrs, hrs_mar mar_hrs, hrs_apr apr_hrs, hrs_may may_hrs, hrs_jun jun_hrs, hrs_jul jul_hrs, hrs_aug aug_hrs, hrs_sep sep_hrs, " + &
			  "hrs_oct oct_hrs, hrs_nov nov_hrs, hrs_dec dec_hrs"
else // BUDGET_MONTHLY_DATA
	sqls += "january jan_amt, february feb_amt, march mar_amt, april apr_amt, may may_amt, june jun_amt, july jul_amt, august aug_amt, september sep_amt, " + &
			  "october oct_amt, november nov_amt, december dec_amt, 0 jan_qty, 0 feb_qty, 0 mar_qty, 0 apr_qty, 0 may_qty, " + &
			  "0 jun_qty, 0 jul_qty, 0 aug_qty, 0 sep_qty, 0 oct_qty, 0 nov_qty, 0 dec_qty, 0 jan_hrs, " + &
			  "0 feb_hrs, 0 mar_hrs, 0 apr_hrs, 0 may_hrs, 0 jun_hrs, 0 jul_hrs, 0 aug_hrs, 0 sep_hrs, " + &
			  "0 oct_hrs, 0 nov_hrs, 0 dec_hrs"
end if
if b_map_lt then
	sqls += ", lt.labor_type "
//	sqls += ", (select e.labor_type from cr_budget_labor_lt_to_ect e where e.estimate_charge_type = pt.est_chg_type_id) labor_type "
end if
// ### - SEK - 7207 - 032311
sqls += " "
if upper(i_projects_table) = "WO_EST_MONTHLY" then
//	sqls +=  'from ' + i_projects_table + ' pt, budget_version_fund_proj '
	sqls += 'from ' + table_to_use + ' pt, budget_version_fund_proj, cr_budget_labor_version_map bv_map '
	if b_map_lt then
		/*### - MDZ - 29466 - 20130317*/
		/*Summarize away the qty_amt_hrs.  Pull all data for the estimate charge type and then sort out which values to use later.*/
		sqls += ", (select distinct estimate_charge_type estimate_charge_type, labor_type labor_type from cr_budget_labor_lt_to_ect) lt "
	end if
	sqls +=  'where pt.revision = budget_version_fund_proj.revision '
	sqls +=  '  and budget_version_fund_proj.active = 1 '
	sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
//	sqls +=  '  and budget_version_fund_proj.budget_version_id = ' + string(i_bv_id) + ""
	sqls += ' and budget_version_fund_proj.budget_version_id = bv_map.budget_version_id '
	sqls += ' and bv_map.cr_budget_version_id = ' + string(i_cr_bv_id)
	if b_map_lt then
		sqls += " and lt.estimate_charge_type = pt.est_chg_type_id "
	end if
else //BUDGET_MONTHLY_DATA 
	sqls +=  'from ' + i_projects_table + ' pt '
	if b_map_lt then
		/*### - MDZ - 29466 - 20130317*/
		/*Summarize away the qty_amt_hrs.  Pull all data for the estimate charge type and then sort out which values to use later.*/
		sqls += ", (select distinct estimate_charge_type estimate_charge_type, labor_type labor_type from cr_budget_labor_lt_to_ect) lt "
	end if
	sqls +=  'where pt.budget_version_id = ' + string(i_bv_id) + " "
	if b_map_lt then
		sqls += " and lt.estimate_charge_type = pt.est_chg_type_id "
	end if
end if

//sqls += i_additional_where_clause + " ) x "
sqls += " ) x " + i_additional_where_clause
sqls += " group by x.department, " 
if i_map_fp then
	sqls += "x.funding_project, "
end if
sqls += "x.year"
if b_map_lt then
	sqls += ",x.labor_type "
end if

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	uf_msg("ERROR: Inserting into cr_bdg_labor_cap_dollars_tmp: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

//***********************************************
// CALL TO CUSTOM FUNCTION FOR PROCESSING OF DATA
//***********************************************
//uf_msg("Calling custom function")
rtn = f_cr_to_budget_to_cr_custom(i_id, ' ', ' ', i_cap_budget_version, 'uf_labor_cap_dollars',i_cr_budget_version,this)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_to_cr' ) = 1 then
	rtn = sqlca.f_client_budget_to_cr(i_id, ' ', ' ', i_cap_budget_version, 'uf_labor_cap_dollars',i_cr_budget_version)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = string(i_id)
args[2] = ' '
args[3] = ' '
args[4] = i_cap_budget_version
args[5] = 'uf_labor_cap_dollars'
args[6] = i_cr_budget_version
rtn = f_wo_validation_control(84,args)
if rtn < 0 then
	rollback;
	return -1
end if

//uf_msg("Deleting from cr_budget_labor_cap_dollars")

// Delete from cr_budget_data and then insert stg data into cr_budget_data
sqls = "delete from "+i_cr_table+" cr " + &
		 "where cr_budget_version_id = " + string(i_cr_bv_id) + " " + i_additional_delete_where_clause
		
execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Deleting from cr_budget_labor_cap_dollars: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if



////uf_msg("Inserting into "+i_cr_table)
sqls = " insert into " + i_cr_table + " " + &
		" select * from cr_bdg_labor_cap_dollars_tmp "
		
execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into " + i_cr_table + ": " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

commit;

//sqls = "insert into cr_budget_data (" + insert_sqls + ") " + &
//		 "select " + insert_sqls + " from cr_bdg_labor_cap_dollars_tmp"
//
//execute immediate :sqls;
//		
//if sqlca.sqlcode < 0 then		
//	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
//	uf_msg("SQLS: " + sqls)
//	rollback;
//	return -1
//end if
//
//commit;
//
//// Insert into cr_to_budget_to_cr_audit -- do this after the commit because it's not
//// a big deal if this fails
////uf_msg("Inserting into cr_to_budget_to_cr_audit")
//
//s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
//ddate  = date(left(s_date, 10))
//ttime  = time(right(s_date, 8))
//finished_at = datetime(ddate, ttime)
//
//select max(occurrence_id) into :max_id
//  from cr_to_budget_to_cr_audit
// where id = :i_id;
// 
//if isnull(max_id) then max_id = 0
//max_id++
//
//insert into cr_to_budget_to_cr_audit (
//	id, occurrence_id, budget_version, running_user_id, start_time, end_time, total_amount, 
//	total_records, insert_where_clause, delete_where_clause, time_stamp, user_id)
//select :i_id,
//		 :max_id,
//		 :i_budget_version,
//		 user,
//		 :started_at,
//		 :finished_at,
//		 sum(jan_amt+feb_amt+mar_amt+apr_amt+may_amt+jun_amt+jul_amt+aug_amt+sep_amt+oct_amt+nov_amt+dec_amt),
//		 count(*),
//		 :i_additional_where_clause,
//		 :i_additional_delete_where_clause,
//		 sysdate,
//		 user
//  from cr_bdg_labor_cap_dollars_tmp;
//  
//if sqlca.sqlcode < 0 then		
//	uf_msg("ERROR: Inserting into cr_to_budget_to_cr_audit: " + sqlca.SQLErrText)
//	rollback;
//	return -1
//else
//	commit;
//end if  
//		 


return 1


end function

public function longlong uf_to_cr_entry ();//*****************************************************************************************
//
//    Function  :    uf_to_cr_entry
//
//	 Description : 	This function will be called from uf_read to send Projects data
//							(either wo_est_monthly or budget_monthly_data) to cr_budget_data_entry. 
//                   This function should always be called from uf_read as uf_read 
//							determines which type of posting is running as well as doing some
//                   intial setup work, including intializing instance variables, that  
//							this function will need.
//
//  Arguments: None 
//
//		NOTES :	This function was added due to maintenance request 5978
//
//*****************************************************************************************
string sqls_ds, sqls_insert, sqls_select, sqls_where, sqls_final, to_column_name, from_column, gl_journal_cat
boolean b_inception_to_date_amount, b_source_id, b_gl_journal_category, b_quantity, b_work_order_id, b_cr_txn_type
longlong rtn, i, source_id, from_table_column, max_id, cr_txn_type_counter

string insert_sqls, select_sqls, sqls, field, ack_list, ack_default_value, ack_select_list, &
		 insert_where_clause, delete_where_clause, col_select_list, group_select_list, s_date
longlong num_rows, num_elements, ack_pos, col_order
date ddate
time ttime
datetime started_at, finished_at

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
started_at = datetime(ddate, ttime)

/*### - MDZ - 10748 - 20120814*/
boolean is_fiscal
string table_to_use

is_fiscal = false

select costrepository.nextval into :g_batch_id from dual;
if isnull(g_batch_id) then g_batch_id = '0'

if g_batch_id = '0' then
	uf_msg("  ")
	uf_msg("ERROR: Could not get the interface_batch_id from costrepository.nextval")
	uf_msg("  ")
	return -1
end if							

///use this value if they don't provide a gl_journal category in the mapping table
gl_journal_cat = 'PM'

b_gl_journal_category = false
b_work_order_id = false

// ### - SEK - 5978 - 122110
cr_txn_type_counter = 0
select count(*) into :cr_txn_type_counter
  from all_tab_columns
 where table_name = 'CR_BUDGET_DATA_ENTRY'
   and column_name = 'CR_TXN_TYPE';
	
if isnull(cr_txn_type_counter) or cr_txn_type_counter = 0 then
	b_cr_txn_type = false
else
	b_cr_txn_type = true
end if

// ### - SEK - 5978 - 122110
select cr_budget_version_id into :i_cr_bv_id
  from cr_budget_version
 where budget_version = :i_cr_budget_version;
// where budget_version = :i_budget_version; 
 
if isnull(i_cr_bv_id) then i_cr_bv_id = 0

if i_cr_bv_id = 0 then
	uf_msg("  ")
	uf_msg("ERROR: Could not get the cr_budget_version_id for budget version'" + i_budget_version + "'")
	uf_msg("  ")
	return -1
end if

/*### - MDZ - 10748 - 20120814*/
/*Are we using the fiscal year concept?  If yes, then this assumes table pp_calendar is properly configured.*/
setnull(rtn)
rtn = long(g_cr.uf_get_control_value('INITIAL MONTH OF FISCAL YEAR')) //Autogenerated sql replace

if isnull(rtn) then rtn = 1

if rtn <> 1 then is_fiscal = true

if is_fiscal then
	/*wo_est_monthly_fy_bdg_v is a view that translates wo_est_monthly by fiscal month using pp_calendar.*/
	table_to_use = 'wo_est_monthly_fy_bdg_v'
else
	table_to_use = i_projects_table
end if


// Truncate cr_budget_data_entry_proj
sqlca.truncate_table('cr_budget_data_entry_proj');
	
if sqlca.SQLCode < 0 then
	uf_msg("ERROR: Truncating cr_budget_data_entry_proj : " + &
			 sqlca.SQLErrText )
	rollback;
	return -1
end if

// Build the posting columns ds
sqls_ds = " select replace(replace(replace(to_column_name,' ','_'),'-','_'),'/','_') to_column_name, " + &
	       "from_column from_column, " + &
		 	 "nvl(from_table_column, 0) from_table_column, " + &
			 ' "TYPE" col_type, "WIDTH" col_width, "DECIMAL" col_decimal, '+&
			 ' "ORDER" col_order ' + &
			 " from cr_to_budget_to_cr_columns" + &
			 " where id = " + string(i_id) + " order by " + '"ORDER"'
		 
uo_ds_top ds_columns
ds_columns = CREATE uo_ds_top
		 
f_create_dynamic_ds(ds_columns,"grid",sqls_ds,sqlca,true)
num_rows = ds_columns.rowcount()

if num_rows < 1 then
	uf_msg('ERROR: Unable to find Budget Columns with posting id ' + string(i_id))
	return -1
end if

// Build the CR elements ds, the ACK list, and the ACK part of the 
// col_select_list and group_select_list
uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
ds_elements.DataObject = "dw_cr_element_definitions"  //sorted by "ORDER"
ds_elements.SetTransObject(sqlca)
num_elements = ds_elements.RETRIEVE()

uf_msg("Building ACK list")

for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	col_order = ds_elements.GetItemNumber(i, "order") + 1
	
	ack_list = ack_list + '"' + upper(field) + '", '
	ack_select_list = ack_select_list + '"' + upper(field) + '", '
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
next

uf_msg("Building Insert SQLS")

// Build the insert statement to insert into cr_budget_data_entry_proj
insert_sqls = insert_sqls + '"ID", ' + ack_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))	
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	Choose case to_column_name
			// ### - SEK - 5978 - 122110: Added 'CR_TXN_TYPE'
		case 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'TOTAL', &
			  'TYPE', 'YEAR', 'CR_BUDGET_VERSION_ID', 'CURRENT_APPROVED_BUDGET', 'CURRENT_APPROVED_FORECAST', &
			  'PRIOR_YEAR_ACTUALS', 'CURRENT_YEAR_ACTUALS','ID', 'ALLOCATION_ID', &
			  'SPLIT_LABOR_ROW', 'GROSS_UP_ID', 'USER_ID', 'TIME_STAMP', 'CR_TXN_TYPE'
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case "GL_JOURNAL_CATEGORY"
			b_gl_journal_category = true
		case "WORK_ORDER_ID"
			b_work_order_id = true
		case else
			// determine whether to_column_name is part of the ACK		
			if col_order >= 2 and col_order <= num_elements + 1 then
				// it is an ACK element, skip it
				continue
			end if
			
	end Choose
	
	insert_sqls = insert_sqls + '"' + to_column_name + '", '
	
next

// If the user mapped gl_journal_category in the control window, the field
// will already be in insert_sqls.
// Otherwise, we need to add them and we'll map default values below.

insert_sqls = insert_sqls + '"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTAL", "YEAR", '
///### - CJR - Maint 5978: GL Journal Category is not a required field on cr_budget_data_entry
//if not b_gl_journal_category then
//	insert_sqls = insert_sqls + '"GL_JOURNAL_CATEGORY", '
//end if

if not b_work_order_id then
	insert_sqls = insert_sqls + '"WORK_ORDER_ID", '
end if

// ### - SEK - 5978 - 122110: Only add if this field exists on the table
if b_cr_txn_type then
	insert_sqls = insert_sqls + '"CR_TXN_TYPE", '
end if

insert_sqls = insert_sqls + '"TYPE", "CR_BUDGET_VERSION_ID", "ALLOCATION_ID", "USER_ID", "TIME_STAMP", "INTERFACE_BATCH_ID" '

uf_msg("Building Select SQLS")

// Build the select statement for the ACK based on the mappings in the control window.
// NOTE: The idea here is to start from a list of the ACK and replace the field
//			with the mapping entered by the user.  
for i = 1 to num_rows
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if col_order >= 2 and col_order <= num_elements + 1 then
		// part of the ACK -- ID is always first column
		to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
		from_column = ds_columns.GetItemString(i, "from_column")
		from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
		
		if from_column = "'_'" then from_column = "' '"
		
		if from_table_column = 1 then
			from_column = uf_clean_string(from_column)
			// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
			// alias the columns with supp or pt.
			if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
				from_column = "SUPPLEMENTAL_TYPE_ID" then
				
				from_column = 'supp."' + upper(from_column) + '"'
			else
				from_column = 'pt."' + upper(from_column) + '"'
			end if
		end if
		
		ack_default_value = ds_elements.GetItemString(col_order - 1, "default_value")
		// We want to nvl everything since the ACK fields are non-nullable
		from_column = "nvl(" + from_column + ", '" + ack_default_value + "') c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, '"' + to_column_name + '"', from_column, 'first')
	end if
next

// Figure out which ACK elements are not mapped by the user and replace with the default value.
for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	field = '"' + upper(field) + '"'

	ack_pos = 0
	ack_pos = pos(ack_select_list, field)
	
	if isnull(ack_pos) then
		uf_msg("Null Value...")
		return -1
	elseif ack_pos > 0 then
		// has not been mapped yet
		ack_default_value = ds_elements.GetItemString(i, "default_value")
		
		col_order = ds_elements.GetItemNumber(i, "order") + 1
		from_column = "'" + ack_default_value + "' c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, field, from_column, 'first')
	end if
next

// Build the select statement -- we've already built the ACK part of the select statement, so
// now we need to build the rest of the statement starting with the non-ACK fields that the
// user mapped in the control window.
// NOTE: In order to allow for subselects to be entered in the mappings, I chose to build
//			the sql so that everything the user enters goes into it's own view, including the
// 		the amount and month number - which the user can't map, so that the sums will work
//			properly without requiring the user to be clever about the way they enter their
//			subselects.  
//			Below three things are built: 
//					select_sqls - contains the SQL going into the view mentioned above
//					col_select_list - the list of every column in the view (minus amount, 
//						month number, and work order id)
//					group_select_list - the list of every column in the view that is not
//						being summed (minus month number and work order id)
select_sqls = select_sqls + ack_select_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
	Choose case to_column_name
		// ### - SEK - 5978 - 122110: Added 'CR_TXN_TYPE'	
		case 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'TOTAL', &
			  'TYPE', 'YEAR', 'CR_BUDGET_VERSION_ID', 'CURRENT_APPROVED_BUDGET', 'CURRENT_APPROVED_FORECAST', & 
			  'PRIOR_YEAR_ACTUALS', 'CURRENT_YEAR_ACTUALS','ID', 'ALLOCATION_ID', &
			  'SPLIT_LABOR_ROW', 'GROSS_UP_ID', 'USER_ID', 'TIME_STAMP', 'CR_TXN_TYPE'
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case else
			col_order = ds_columns.GetItemNumber(i, "col_order")
			if col_order >= 2 and col_order <= num_elements + 1 then
				// part of the ACK
				continue
			end if
	end Choose
	
	from_column = ds_columns.GetItemString(i, "from_column")
	from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if from_column = "'_'" then from_column = "' '"
	
	if from_table_column = 1 then
		
		from_column = uf_clean_string(from_column)
		// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
		// alias the columns with supp or pt.
		if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
			from_column = "SUPPLEMENTAL_TYPE_ID" then
			
			from_column = 'supp."' + upper(from_column) + '"'
		else
			from_column = 'pt."' + upper(from_column) + '"'
		end if
				
		
	else
		// Need to support sum -- i.e. the user can map into the quantity field
		if left(upper(from_column),4) = 'SUM(' then
			// trim off 'SUM(' from the front and ')' from the end
			from_column = left(from_column, len(from_column) - 1)
			from_column = right(from_column, len(from_column) - 4)
			
			select_sqls = select_sqls + from_column + " s" + string(col_order) + ", "
			col_select_list = col_select_list + " sum(y.s" + string(col_order) + "), "
			continue
		end if
	end if
	
	select_sqls = select_sqls + from_column + " c" + string(col_order) + ", "
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
	
next

// Get the insert where clause
if i_insert_where_clause = 0 or isnull(i_insert_where_clause) then
	// User did not pick a where clause so skip this part
else
	uf_msg("Building Insert Where Clause")
	insert_where_clause = uf_build_where_clause(i_insert_where_clause)
end if
if isnull(insert_where_clause) then insert_where_clause = ""

if insert_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the insert where clause.")
	rollback;
	return -1
end if

uf_msg("Inserting into cr_budget_data_entry_proj ('Amt')")

// Build sqls - Build the insert for type 'Amt' first, then build the insert for 'Qty' if it is supposed
//		to be mapped
sqls  = 'insert into cr_budget_data_entry_proj ( '
sqls += insert_sqls + ") "
sqls += ' select crbudgets.nextval, x.*, '
///### - CJR - Maint 5978: GL Journal Category is not a required field on cr_budget_data_entry
//if not b_gl_journal_category then sqls += "'" + gl_journal_cat + "', "
if not b_work_order_id then sqls += "null, "
// ### - SEK - 5978 - 122110
if b_cr_txn_type then sqls += "'PROJECTS', "
sqls += "'Amt', " // type
sqls += string(i_cr_bv_id) + ", " // cr_budget_version_id
//"ALLOCATION_ID",
if i_alloc then
	sqls += "(select -99 from estimate_charge_type e where pt.est_chg_type_id = e.est_chg_type_id and nvl(e.afudc_flag,'U') = 'O'), "
else
	sqls += " null,"
end if
sqls += 'user, ' //user_id
sqls += 'sysdate, ' //time_stamp
sqls += string(g_batch_id) + ' ' //"INTERFACE_BATCH_ID"
sqls += 'from ( '
sqls += 	'select ' + col_select_list 
//"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTAL", "YEAR"
sqls +=	 'sum(y.jan) jan, sum(y.feb) feb, sum(y.mar) mar, sum(y.apr) apr, sum(y.may) may, sum(y.jun) jun, ' 
sqls += 	 'sum(y.jul) jul, sum(y.aug) aug, sum(y.sep) sep, sum(y.oct) oct, sum(y.nov) nov, sum(y.dec) dec, '
sqls +=   'sum(y.jan + y.feb + y.mar + y.apr + y.may + y.jun + y.jul + y.aug + y.sep + y.oct + y.nov + y.dec) total, '
sqls +=   'y.year from ( '
sqls +=		'select ' + select_sqls
if upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
	sqls +=	'nvl(supp.january, pt.january) jan, '
	sqls +=  'nvl(supp.february, pt.february) feb, ' 
	sqls +=  'nvl(supp.march, pt.march) mar, ' 
	sqls +=  'nvl(supp.april, pt.april) apr, '
	sqls +=  'nvl(supp.may, pt.may) may, '
	sqls +=  'nvl(supp.june, pt.june) jun, ' 
	sqls +=  'nvl(supp.july, pt.july) jul, '
	sqls +=  'nvl(supp.august, pt.august) aug, '
	sqls +=  'nvl(supp.september, pt.september) sep, '
	sqls +=  'nvl(supp.october, pt.october) oct, '
	sqls +=  'nvl(supp.november, pt.november) nov, '
	sqls +=  'nvl(supp.december, pt.december) dec, '
else
	sqls +=	'pt.january jan, '
	sqls +=  'pt.february feb, ' 
	sqls +=  'pt.march mar, ' 
	sqls +=  'pt.april apr, '
	sqls +=  'pt.may may, '
	sqls +=  'pt.june jun, ' 
	sqls +=  'pt.july jul, '
	sqls +=  'pt.august aug, '
	sqls +=  'pt.september sep, '
	sqls +=  'pt.october oct, '
	sqls +=  'pt.november nov, '
	sqls +=  'pt.december dec, '
end if
sqls +=			"pt.year year "
sqls +=		'from '
if upper(i_projects_table) = "WO_EST_MONTHLY" and not i_wo_est_proc_temp then
	/*### - MDZ - 6443 - 20110112*/
	/*The pp_table_months table is not used in this statement and is causing numbers to be 12 times
		greater than they should be.*/
//	sqls += 	i_projects_table + ' pt, budget_version, budget_version_fund_proj, pp_table_months '
	/*### - MDZ - 10748 - 20120814*/
//	sqls += 	i_projects_table + ' pt, budget_version, budget_version_fund_proj '
	sqls += 	table_to_use + ' pt, budget_version, budget_version_fund_proj '
	sqls +=  'where pt.revision = budget_version_fund_proj.revision '
	sqls +='    and budget_version_fund_proj.active = 1 '
	sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
	sqls +=  '  and budget_version_fund_proj.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
elseif upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
	/*### - MDZ - 6443 - 20110112*/
	/*The pp_table_months table is not used in this statement and is causing numbers to be 12 times
		greater than they should be.*/
//	sqls += 	'wo_est_monthly pt, budget_version, budget_version_fund_proj, pp_table_months, '
	sqls += 	'wo_est_monthly pt, budget_version, budget_version_fund_proj, '
	sqls +=  'wo_est_supplemental_data supp '
	sqls +=  'where pt.revision = budget_version_fund_proj.revision '
	sqls +='    and budget_version_fund_proj.active = 1 '
	sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
	sqls +=  '  and budget_version_fund_proj.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
	sqls +=  "  and pt.est_monthly_id = supp.est_monthly_id (+)"
	sqls +=  "  and pt.year = supp.year (+)"
elseif upper(i_projects_table) ='BUDGET_MONTHLY_DATA' then
	sqls +=  i_projects_table + ' pt, budget_version '
	sqls +=  'where pt.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
elseif i_wo_est_proc_temp then
	/*### - MDZ - 6443 - 20110112*/
	/*The pp_table_months table is not used in this statement and is causing numbers to be 12 times
		greater than they should be.*/
//	sqls += 	i_projects_table + ' pt,  pp_table_months, wo_est_processing_temp '
	sqls += 	i_projects_table + ' pt, wo_est_processing_temp '
	sqls +=  'where pt.revision = wo_est_processing_temp.revision '
	sqls +=  '  and pt.work_order_id = wo_est_processing_temp.work_order_id '
else
	return -1
end if
sqls += insert_where_clause +i_additional_where_clause+ ' ) y '
sqls += 'group by ' + group_select_list + ' y.year) x'


execute immediate :sqls;

if sqlca.sqlcode < 0 then
	uf_msg("ERROR: Inserting into cr_budget_data_entry_proj ('Amt'): " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

// Build sqls - Build the insert for type 'Qty' 
if i_map_quantity = 1 or i_map_quantity = 2 then
	sqls  = 'insert into cr_budget_data_entry_proj ( '
	sqls += insert_sqls + ") "
	sqls += ' select crbudgets.nextval, x.*, '
	///### - CJR - Maint 5978: GL Journal Category is not a required field on cr_budget_data_entry
	//if not b_gl_journal_category then sqls += "'" + gl_journal_cat + "', "
	if not b_work_order_id then sqls += "null, "
	// ### - SEK - 5978 - 122110
	if b_cr_txn_type then sqls += "'PROJECTS', "
	sqls += "'Qty', " // type
	sqls += string(i_cr_bv_id) + ", " // cr_budget_version_id
	//"ALLOCATION_ID",
	if i_alloc then
		sqls += "(select -99 from estimate_charge_type e where pt.est_chg_type_id = e.est_chg_type_id and nvl(e.afudc_flag,'U') = 'O'), "
	else
		sqls += " null,"
	end if
	sqls += 'user, ' //user_id
	sqls += 'sysdate, ' //time_stamp
	sqls += string(g_batch_id) + ' ' //"INTERFACE_BATCH_ID"
	sqls += 'from ( '
	sqls += 	'select ' + col_select_list 
	//"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTAL", "YEAR"
	sqls +=	 'sum(y.jan) jan, sum(y.feb) feb, sum(y.mar) mar, sum(y.apr) apr, sum(y.may) may, sum(y.jun) jun, ' 
	sqls += 	 'sum(y.jul) jul, sum(y.aug) aug, sum(y.sep) sep, sum(y.oct) oct, sum(y.nov) nov, sum(y.dec) dec, '
	sqls +=   'sum(y.jan + y.feb + y.mar + y.apr + y.may + y.jun + y.jul + y.aug + y.sep + y.oct + y.nov + y.dec) total, '
	sqls +=   'y.year from ( '
	sqls +=		'select ' + select_sqls
	Choose case i_map_quantity
		case 1 // HOURS
			if upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
				sqls +=	'nvl(supp.hrs_jan, nvl(pt.hrs_jan,0)) jan, '
				sqls +=  'nvl(supp.hrs_feb, nvl(pt.hrs_feb,0)) feb, ' 
				sqls +=  'nvl(supp.hrs_mar, nvl(pt.hrs_mar,0)) mar, ' 
				sqls +=  'nvl(supp.hrs_apr, nvl(pt.hrs_apr,0)) apr, '
				sqls +=  'nvl(supp.hrs_may, nvl(pt.hrs_may,0)) may, '
				sqls +=  'nvl(supp.hrs_jun, nvl(pt.hrs_jun,0)) jun, ' 
				sqls +=  'nvl(supp.hrs_jul, nvl(pt.hrs_jul,0)) jul, '
				sqls +=  'nvl(supp.hrs_aug, nvl(pt.hrs_aug,0)) aug, '
				sqls +=  'nvl(supp.hrs_sep, nvl(pt.hrs_sep,0)) sep, '
				sqls +=  'nvl(supp.hrs_oct, nvl(pt.hrs_oct,0)) oct, '
				sqls +=  'nvl(supp.hrs_nov, nvl(pt.hrs_nov,0)) nov, '
				sqls +=  'nvl(supp.hrs_dec, nvl(pt.hrs_dec,0)) dec, '
			else
				sqls +=	'nvl(pt.hrs_jan,0) jan, '
				sqls +=  'nvl(pt.hrs_feb,0) feb, ' 
				sqls +=  'nvl(pt.hrs_mar,0) mar, ' 
				sqls +=  'nvl(pt.hrs_apr,0) apr, '
				sqls +=  'nvl(pt.hrs_may,0) may, '
				sqls +=  'nvl(pt.hrs_jun,0) jun, ' 
				sqls +=  'nvl(pt.hrs_jul,0) jul, '
				sqls +=  'nvl(pt.hrs_aug,0) aug, '
				sqls +=  'nvl(pt.hrs_sep,0) sep, '
				sqls +=  'nvl(pt.hrs_oct,0) oct, '
				sqls +=  'nvl(pt.hrs_nov,0) nov, '
				sqls +=  'nvl(pt.hrs_dec,0) dec, '
			end if
		case 2 // QUANTITY
			if upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
				sqls +=	'nvl(supp.qty_jan, nvl(pt.qty_jan,0)) jan, '
				sqls +=  'nvl(supp.qty_feb, nvl(pt.qty_feb,0)) feb, ' 
				sqls +=  'nvl(supp.qty_mar, nvl(pt.qty_mar,0)) mar, ' 
				sqls +=  'nvl(supp.qty_apr, nvl(pt.qty_apr,0)) apr, '
				sqls +=  'nvl(supp.qty_may, nvl(pt.qty_may,0)) may, '
				sqls +=  'nvl(supp.qty_jun, nvl(pt.qty_jun,0)) jun, ' 
				sqls +=  'nvl(supp.qty_jul, nvl(pt.qty_jul,0)) jul, '
				sqls +=  'nvl(supp.qty_aug, nvl(pt.qty_aug,0)) aug, '
				sqls +=  'nvl(supp.qty_sep, nvl(pt.qty_sep,0)) sep, '
				sqls +=  'nvl(supp.qty_oct, nvl(pt.qty_oct,0)) oct, '
				sqls +=  'nvl(supp.qty_nov, nvl(pt.qty_nov,0)) nov, '
				sqls +=  'nvl(supp.qty_dec, nvl(pt.qty_dec,0)) dec, '
			else
				sqls +=	'nvl(pt.qty_jan,0) jan, '
				sqls +=  'nvl(pt.qty_feb,0) feb, ' 
				sqls +=  'nvl(pt.qty_mar,0) mar, ' 
				sqls +=  'nvl(pt.qty_apr,0) apr, '
				sqls +=  'nvl(pt.qty_may,0) may, '
				sqls +=  'nvl(pt.qty_jun,0) jun, ' 
				sqls +=  'nvl(pt.qty_jul,0) jul, '
				sqls +=  'nvl(pt.qty_aug,0) aug, '
				sqls +=  'nvl(pt.qty_sep,0) sep, '
				sqls +=  'nvl(pt.qty_oct,0) oct, '
				sqls +=  'nvl(pt.qty_nov,0) nov, '
				sqls +=  'nvl(pt.qty_dec,0) dec, '
			end if
	end Choose
	sqls +=			"pt.year year "
	sqls +=		'from '
	if upper(i_projects_table) = "WO_EST_MONTHLY" and not i_wo_est_proc_temp then
		/*### - MDZ - 6443 - 20110112*/
		/*The pp_table_months table is not used in this statement and is causing numbers to be 12 times
			greater than they should be.*/
//		sqls += 	i_projects_table + ' pt, budget_version, budget_version_fund_proj, pp_table_months '
		/*### - MDZ - 10748 - 20120814*/
//		sqls += 	i_projects_table + ' pt, budget_version, budget_version_fund_proj '
		sqls += 	table_to_use + ' pt, budget_version, budget_version_fund_proj '
		sqls +=  'where pt.revision = budget_version_fund_proj.revision '
		sqls +='    and budget_version_fund_proj.active = 1 '
		sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
		sqls +=  '  and budget_version_fund_proj.budget_version_id = budget_version.budget_version_id '
		// ### - SEK - 5978 - 122110
//		sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
		sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
	elseif upper(i_projects_table) = "MONTHLY,SUPPLEMENTAL" then
		/*### - MDZ - 6443 - 20110112*/
		/*The pp_table_months table is not used in this statement and is causing numbers to be 12 times
			greater than they should be.*/
//		sqls += 	'wo_est_monthly pt, budget_version, budget_version_fund_proj, pp_table_months, '
		sqls += 	'wo_est_monthly pt, budget_version, budget_version_fund_proj, '
		sqls +=  'wo_est_supplemental_data supp '
		sqls +=  'where pt.revision = budget_version_fund_proj.revision '
		sqls +='    and budget_version_fund_proj.active = 1 '
		sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
		sqls +=  '  and budget_version_fund_proj.budget_version_id = budget_version.budget_version_id '
		// ### - SEK - 5978 - 122110
//		sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
		sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
		sqls +=  "  and pt.est_monthly_id = supp.est_monthly_id (+)"
		sqls +=  "  and pt.year = supp.year (+)"
	elseif upper(i_projects_table) ='BUDGET_MONTHLY_DATA' then
		sqls +=  i_projects_table + ' pt, budget_version '
		sqls +=  'where pt.budget_version_id = budget_version.budget_version_id '
		// ### - SEK - 5978 - 122110
//		sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
		sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
	elseif i_wo_est_proc_temp then
		/*### - MDZ - 6443 - 20110112*/
		/*The pp_table_months table is not used in this statement and is causing numbers to be 12 times
			greater than they should be.*/
//		sqls += 	i_projects_table + ' pt,  pp_table_months, wo_est_processing_temp '
		sqls += 	i_projects_table + ' pt, wo_est_processing_temp '
		sqls +=  'where pt.revision = wo_est_processing_temp.revision '
		sqls +=  '  and pt.work_order_id = wo_est_processing_temp.work_order_id '
	else
		return -1
	end if
	sqls += insert_where_clause +i_additional_where_clause+ ' ) y '
	sqls += 'group by ' + group_select_list + ' y.year) x'
	
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		uf_msg("ERROR: Inserting into cr_budget_data_entry_proj ('Qty'): " + sqlca.SQLErrText)
		uf_msg("SQLS: " + sqls)
		rollback;
		return -1
	end if
end if // if i_map_quantity = 1 or i_map_quantity = 2 then

//***********************************************
// CALL TO CUSTOM FUNCTION FOR PROCESSING OF DATA
//***********************************************
uf_msg("Calling custom function")
rtn = f_cr_to_budget_to_cr_custom(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr_entry',i_cr_budget_version,this)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_to_cr' ) = 1 then
	rtn = sqlca.f_client_budget_to_cr(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr_entry',i_cr_budget_version)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = string(i_id)
args[2] = insert_where_clause
args[3] = delete_where_clause
args[4] = i_cap_budget_version
args[5] = 'uf_to_cr_entry'
args[6] = i_cr_budget_version
rtn = f_wo_validation_control(84,args)
if rtn < 0 then
	rollback;
	return -1
end if

// Get the delete where clause
if i_delete_where_clause = 0 or isnull(i_delete_where_clause) then
	// the user did not select a where clause so skip this part
else
	delete_where_clause = uf_build_where_clause(i_delete_where_clause)
end if
if isnull(delete_where_clause) then delete_where_clause = ""

if delete_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the delete where clause.")
	rollback;
	return -1
end if

uf_msg("Deleting from cr_budget_data_entry")

// Delete from cr_budget_data and then insert stg data into cr_budget_data
sqls = "delete from "+i_cr_table+" cr " + &
		 "where cr_budget_version_id = " + string(i_cr_bv_id) + " " + delete_where_clause + i_additional_delete_where_clause
		
execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_budget_data_entry_proj: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

uf_msg("Inserting into "+i_cr_table)
sqls = "insert into "+i_cr_table+" (" + insert_sqls + ") " + &
		 "select " + insert_sqls + " from cr_budget_data_entry_proj"

execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_budget_data_entry_proj: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

commit;

// Insert into cr_to_budget_to_cr_audit -- do this after the commit because it's not
// a big deal if this fails
uf_msg("Inserting into cr_to_budget_to_cr_audit")

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
finished_at = datetime(ddate, ttime)

select max(occurrence_id) into :max_id
  from cr_to_budget_to_cr_audit
 where id = :i_id;
 
if isnull(max_id) then max_id = 0
max_id++

insert into cr_to_budget_to_cr_audit (
	id, occurrence_id, budget_version, running_user_id, start_time, end_time, total_amount, 
	total_records, insert_where_clause, delete_where_clause, time_stamp, user_id)
select :i_id,
		 :max_id,
		 :i_budget_version,
		 user,
		 :started_at,
		 :finished_at,
		 sum(total),
		 count(*),
			 :insert_where_clause||' '||:i_additional_where_clause,
			 :delete_where_clause||' '||:i_additional_delete_where_clause,
		 sysdate,
		 user
  from cr_budget_data_entry_proj
 where "TYPE" = 'Amt';
  
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_to_budget_to_cr_audit: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if  
		 


return 1


end function

public function longlong uf_to_cr_afudc_oh ();//*****************************************************************************************
//
//   Function  :  uf_to_cr_afudc_oh
//
//	 Description : 	This function will be called from uf_read to send Projects data
//							(either wo_est_monthly or budget_monthly_data) to cr_budget_data. 
//                   This function should always be called from uf_read as uf_read 
//							determines which type of posting is running as well as doing some
//                   intial setup work, including intializing instance variables, that  
//							this function will need.
//
//  Arguments: None 
//
//*****************************************************************************************
string sqls_ds, sqls_insert, sqls_select, sqls_where, sqls_final, to_column_name, from_column, &
		 gl_journal_cat, addl_sql
boolean b_inception_to_date_amount, b_source_id, b_gl_journal_category, b_quantity
longlong rtn, i, source_id, from_table_column, max_id

string insert_sqls, select_sqls, sqls, field, ack_list, ack_default_value, ack_select_list, &
		 insert_where_clause, delete_where_clause, col_select_list, group_select_list, s_date
longlong num_rows, num_elements, ack_pos, col_order
date ddate
time ttime
datetime started_at, finished_at

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
started_at = datetime(ddate, ttime)

if i_cr_table = 'none' then
	i_cr_table = 'CR_BUDGET_DATA'
end if

select costrepository.nextval into :g_batch_id from dual;
if isnull(g_batch_id) then g_batch_id = '0'

if g_batch_id = '0' then
	uf_msg("  ")
	uf_msg("ERROR: Could not get the interface_batch_id from costrepository.nextval")
	uf_msg("  ")
	return -1
end if							

///use this value if they don't provide a gl_journal category in the mapping table
gl_journal_cat = 'PM-AFUDC/OH'

select addl_sql into :addl_sql
  from cr_to_budget_to_cr_control
 where id = :i_id;
 
if isnull(addl_sql) then addl_sql = ""

b_inception_to_date_amount = false
b_source_id = false
b_gl_journal_category = false
b_quantity = false

// Truncate cr_budget_data_projects
sqlca.truncate_table('cr_budget_data_projects');
	
if sqlca.SQLCode < 0 then
	uf_msg("ERROR: Truncating cr_budget_data_projects : " + &
			 sqlca.SQLErrText )
	rollback;
	return -1
end if

// Build the posting columns ds
sqls_ds = " select replace(replace(replace(to_column_name,' ','_'),'-','_'),'/','_') to_column_name, " + &
	       "from_column from_column, " + &
		 	 "nvl(from_table_column, 0) from_table_column, " + &
			 ' "TYPE" col_type, "WIDTH" col_width, "DECIMAL" col_decimal, '+&
			 ' "ORDER" col_order ' + &
			 " from cr_to_budget_to_cr_columns" + &
			 " where id = " + string(i_id) + " order by " + '"ORDER"'
		 
uo_ds_top ds_columns
ds_columns = CREATE uo_ds_top
		 
f_create_dynamic_ds(ds_columns,"grid",sqls_ds,sqlca,true)
num_rows = ds_columns.rowcount()

if num_rows < 1 then
	uf_msg('ERROR: Unable to find Budget Columns with posting id ' + string(i_id))
	return -1
end if

// Build the CR elements ds, the ACK list, and the ACK part of the 
// col_select_list and group_select_list
uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
ds_elements.DataObject = "dw_cr_element_definitions"  //sorted by "ORDER"
ds_elements.SetTransObject(sqlca)
num_elements = ds_elements.RETRIEVE()

uf_msg("Building ACK list")

for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	col_order = ds_elements.GetItemNumber(i, "order") + 1
	
	ack_list = ack_list + '"' + upper(field) + '", '
	ack_select_list = ack_select_list + '"' + upper(field) + '", '
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
next

uf_msg("Building Insert SQLS")

// Build the insert statement to insert into cr_budget_data_projects
insert_sqls = insert_sqls + '"ID", ' + ack_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))	
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	Choose case to_column_name
		case "ID", "BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "AMOUNT", "MONTH_NUMBER", &
			  "MONTH_PERIOD", "GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", &
			  "ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", &			
  			  "WORK_ORDER_ID", "INTERFACE_BATCH_ID"
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case "QUANTITY"
			b_quantity = true
		case "INCEPTION_TO_DATE_AMOUNT"
			b_inception_to_date_amount = true
		case "GL_JOURNAL_CATEGORY"
			b_gl_journal_category = true
		case "SOURCE_ID"
			b_source_id = true
		case else
			// determine whether to_column_name is part of the ACK		
			if col_order >= 2 and col_order <= num_elements + 1 then
				// it is an ACK element, skip it
				continue
			end if
			
	end Choose
	
	insert_sqls = insert_sqls + '"' + to_column_name + '", '
	
next

insert_sqls = insert_sqls + '"AMOUNT", "MONTH_NUMBER", "WORK_ORDER_ID", '

// If the user mapped quantity, inception_to_date_amount, gl_journal_category,
// or source_id in the control window, the field(s) will already be in insert_sqls.
// Otherwise, we need to add them and we'll map default values below.
if not b_quantity	then
	insert_sqls = insert_sqls + '"QUANTITY", '
end if

if not b_inception_to_date_amount then
	insert_sqls = insert_sqls + '"INCEPTION_TO_DATE_AMOUNT", '
end if

if not b_gl_journal_category then
	insert_sqls = insert_sqls + '"GL_JOURNAL_CATEGORY", '
end if

if not b_source_id then
	setnull(source_id)
source_id = g_cr.uf_get_source_id("description",'POWERPLANT') //Autogenerated sql replace
	
	if isnull(source_id) then
		select min(source_id) into :source_id
		  from cr_sources;
	end if
	
	insert_sqls = insert_sqls + '"SOURCE_ID", '
end if

insert_sqls = insert_sqls + '"BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "MONTH_PERIOD",' +  &
			     '"GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", ' + &
				  '"ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", "INTERFACE_BATCH_ID" '

uf_msg("Building Select SQLS")

// Build the select statement for the ACK based on the mappings in the control window.
// NOTE: The idea here is to start from a list of the ACK and replace the field
//			with the mapping entered by the user.  
for i = 1 to num_rows
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if col_order >= 2 and col_order <= num_elements + 1 then
		// part of the ACK -- ID is always first column
		to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
		from_column = ds_columns.GetItemString(i, "from_column")
		from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
		
		if from_column = "'_'" then from_column = "' '"
		
		if from_table_column = 1 then
			from_column = uf_clean_string(from_column)
			// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
			// alias the columns with supp or pt.
			if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
				from_column = "SUPPLEMENTAL_TYPE_ID" then
				
				from_column = 'supp."' + upper(from_column) + '"'
			else
				from_column = 'pt."' + upper(from_column) + '"'
			end if
		end if
		
		ack_default_value = ds_elements.GetItemString(col_order - 1, "default_value")
		// We want to nvl everything since the ACK fields are non-nullable
		from_column = "nvl(" + from_column + ", '" + ack_default_value + "') c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, '"' + to_column_name + '"', from_column, 'first')
	end if
next

// Figure out which ACK elements are not mapped by the user and replace with the default value.
for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	field = '"' + upper(field) + '"'

	ack_pos = 0
	ack_pos = pos(ack_select_list, field)
	
	if isnull(ack_pos) then
		uf_msg("Null Value...")
		return -1
	elseif ack_pos > 0 then
		// has not been mapped yet
		ack_default_value = ds_elements.GetItemString(i, "default_value")
		
		col_order = ds_elements.GetItemNumber(i, "order") + 1
		from_column = "'" + ack_default_value + "' c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, field, from_column, 'first')
	end if
next

// Build the select statement -- we've already built the ACK part of the select statement, so
// now we need to build the rest of the statement starting with the non-ACK fields that the
// user mapped in the control window.
// NOTE: In order to allow for subselects to be entered in the mappings, I chose to build
//			the sql so that everything the user enters goes into it's own view, including the
// 		the amount and month number - which the user can't map, so that the sums will work
//			properly without requiring the user to be clever about the way they enter their
//			subselects.  
//			Below three things are built: 
//					select_sqls - contains the SQL going into the view mentioned above
//					col_select_list - the list of every column in the view (minus amount, 
//						month number, and work order id)
//					group_select_list - the list of every column in the view that is not
//						being summed (minus month number and work order id)
select_sqls = select_sqls + ack_select_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
	Choose case to_column_name
		case "ID", "BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "AMOUNT", "MONTH_NUMBER", &
			  "MONTH_PERIOD", "GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", &
			  "ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", &			
  			  "WORK_ORDER_ID", "INTERFACE_BATCH_ID"
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case else
			col_order = ds_columns.GetItemNumber(i, "col_order")
			if col_order >= 2 and col_order <= num_elements + 1 then
				// part of the ACK
				continue
			end if
	end Choose
	
	from_column = ds_columns.GetItemString(i, "from_column")
	from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if from_column = "'_'" then from_column = "' '"
	
	if from_table_column = 1 then
		
		from_column = uf_clean_string(from_column)
		// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
		// alias the columns with supp or pt.
		if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
			from_column = "SUPPLEMENTAL_TYPE_ID" then
			
			from_column = 'supp."' + upper(from_column) + '"'
		else
			from_column = 'pt."' + upper(from_column) + '"'
		end if
				
		
	else
		// Need to support sum -- i.e. the user can map into the quantity field
		if left(upper(from_column),4) = 'SUM(' then
			// trim off 'SUM(' from the front and ')' from the end
			from_column = left(from_column, len(from_column) - 1)
			from_column = right(from_column, len(from_column) - 4)
			
			select_sqls = select_sqls + from_column + " s" + string(col_order) + ", "
			col_select_list = col_select_list + " sum(y.s" + string(col_order) + "), "
			continue
		end if
	end if
	
	select_sqls = select_sqls + from_column + " c" + string(col_order) + ", "
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
	
next

// Get the insert where clause
if i_insert_where_clause = 0 or isnull(i_insert_where_clause) then
	// User did not pick a where clause so skip this part
else
	uf_msg("Building Insert Where Clause")
	insert_where_clause = uf_build_where_clause(i_insert_where_clause)
end if
if isnull(insert_where_clause) then insert_where_clause = ""

if insert_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the insert where clause.")
	rollback;
	return -1
end if

uf_msg("Inserting into cr_budget_data_projects")

// Build sqls
sqls  = 'insert into cr_budget_data_projects ( '
sqls += insert_sqls + ") "
sqls += ' select crbudgets.nextval, x.*, '
if not b_quantity then sqls += '0, '
if not b_inception_to_date_amount then sqls += '0, '
if not b_gl_journal_category then sqls += "'" + gl_journal_cat + "', "
if not b_source_id then sqls += string(source_id) + ', '
// ### - SEK - 5978 - 122110
//sqls += "'" + i_budget_version + "', "
sqls += "'" + i_cr_budget_version + "', "
sqls += "2, " //amount_type
sqls += 'decode(sign(x.amount),0,1,sign(x.amount)), ' //dr_cr_id
sqls += '1, ' //ledger_sign
sqls += '0, ' //month_period
sqls += 'null, ' //gl_id
sqls += string(g_batch_id) + ', ' //batch_id
sqls += 'null, ' //cwip_charge_status
sqls += 'sysdate, ' //time_stamp
sqls += 'user, ' //user_id
//"ALLOCATION_ID",
if i_alloc then
	sqls += "(select -99 from estimate_charge_type e where pt.est_chg_type_id = e.est_chg_type_id and nvl(e.afudc_flag,'U') = 'O'), "
else
	sqls += " null,"
end if
sqls += 'null, null, null, ' // "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY"
sqls += string(g_batch_id) + ' ' //"INTERFACE_BATCH_ID"
sqls += 'from ( '
sqls += 	'select ' + col_select_list 
sqls +=	 'sum(y.amount) amount, y.month_number, y.work_order_id from ( '
sqls +=		'select ' + select_sqls
sqls +=		 'decode(pp_table_months.month_num, 1, -pt.january, 2, -pt.february, ' + &
				 '3, -pt.march, 4, -pt.april, 5, -pt.may, 6, -pt.june, 7, -pt.july, ' + &
				 '8, -pt.august, 9, -pt.september, 10, -pt.october, 11, -pt.november, ' + &
				 '12, -pt.december) amount, '
sqls +=			"to_number(pt.year||lpad(pp_table_months.month_num, 2, '0')) month_number, "
sqls +=			'pt.work_order_id work_order_id '
sqls +=		'from '
if upper(i_projects_table) = "AFUDC___WO_EST_MONTHLY" and not i_wo_est_proc_temp then
	sqls += 	'wo_est_monthly pt, budget_version, budget_version_fund_proj, pp_table_months '
	sqls +=  'where pt.revision = budget_version_fund_proj.revision '
	sqls +='    and budget_version_fund_proj.active = 1 '
	sqls +=  '  and pt.work_order_id = budget_version_fund_proj.work_order_id '
	sqls +=  '  and budget_version_fund_proj.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
elseif upper(i_projects_table) ='AFUDC___BUDGET_MONTHLY_DATA' then
	sqls +=  'budget_monthly_data pt, budget_version '
	sqls +=  'where pt.budget_version_id = budget_version.budget_version_id '
	// ### - SEK - 5978 - 122110
//	sqls +=  "  and budget_version.description = '" + i_budget_version + "' "
	sqls +=  "  and budget_version.description = '" + i_cap_budget_version + "' "
elseif i_wo_est_proc_temp then
	sqls += 	'wo_est_monthly pt,  pp_table_months, wo_est_processing_temp '
	sqls +=  'where pt.revision = wo_est_processing_temp.revision '
	sqls +=  '  and pt.work_order_id = wo_est_processing_temp.work_order_id '
else
	return -1
end if
sqls += insert_where_clause + addl_sql +i_additional_where_clause+ ' ) y '
sqls += 'group by ' + group_select_list + ' y.month_number, y.work_order_id) x'


execute immediate :sqls;

if sqlca.sqlcode < 0 then
	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

//***********************************************
// CALL TO CUSTOM FUNCTION FOR PROCESSING OF DATA
//***********************************************
uf_msg("Calling custom function")
rtn = f_cr_to_budget_to_cr_custom(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr_afudc_oh',i_cr_budget_version,this)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_to_cr' ) = 1 then
	rtn = sqlca.f_client_budget_to_cr(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr_afudc_oh',i_cr_budget_version)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = string(i_id)
args[2] = insert_where_clause
args[3] = delete_where_clause
args[4] = i_cap_budget_version
args[5] = 'uf_to_cr_afudc_oh'
args[6] = i_cr_budget_version
rtn = f_wo_validation_control(84,args)
if rtn < 0 then
	rollback;
	return -1
end if

// Get the delete where clause
if i_delete_where_clause = 0 or isnull(i_delete_where_clause) then
	// the user did not select a where clause so skip this part
else
	delete_where_clause = uf_build_where_clause(i_delete_where_clause)
end if
if isnull(delete_where_clause) then delete_where_clause = ""

if delete_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the delete where clause.")
	rollback;
	return -1
end if

uf_msg("Deleting from cr_budget_data")

// Delete from cr_budget_data and then insert stg data into cr_budget_data
// ### - SEK - 5978 - 122110
sqls = "delete from "+i_cr_table+" cr " + &
		 "where budget_version = '" + i_cr_budget_version + "' " + delete_where_clause + i_additional_delete_where_clause
//		 "where budget_version = '" + i_budget_version + "' " + delete_where_clause + i_additional_delete_where_clause
		
execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

uf_msg("Inserting into "+i_cr_table)
sqls = "insert into "+i_cr_table+" (" + insert_sqls + ") " + &
		 "select " + insert_sqls + " from cr_budget_data_projects"

execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

commit;

// Insert into cr_to_budget_to_cr_audit -- do this after the commit because it's not
// a big deal if this fails
uf_msg("Inserting into cr_to_budget_to_cr_audit")

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
finished_at = datetime(ddate, ttime)

select max(occurrence_id) into :max_id
  from cr_to_budget_to_cr_audit
 where id = :i_id;
 
if isnull(max_id) then max_id = 0
max_id++

insert into cr_to_budget_to_cr_audit (
	id, occurrence_id, budget_version, running_user_id, start_time, end_time, total_amount, 
	total_records, insert_where_clause, delete_where_clause, time_stamp, user_id)
select :i_id,
		 :max_id,
		 :i_budget_version,
		 user,
		 :started_at,
		 :finished_at,
		 sum(amount),
		 count(*),
			 :insert_where_clause||' '||:i_additional_where_clause,
			 :delete_where_clause||' '||:i_additional_delete_where_clause,
		 sysdate,
		 user
  from cr_budget_data_projects;
  
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_to_budget_to_cr_audit: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if  
		 


return 1


end function

public function longlong uf_to_cr_depr_fcst ();//*****************************************************************************************
//
//   Function  :  uf_to_cr
//
//	 Description : 	This function will be called from uf_read to send Projects data
//							(either wo_est_monthly or budget_monthly_data) to cr_budget_data. 
//                   This function should always be called from uf_read as uf_read 
//							determines which type of posting is running as well as doing some
//                   intial setup work, including intializing instance variables, that  
//							this function will need.
//
//  Arguments: None 
//
//*****************************************************************************************
string sqls_ds, sqls_insert, sqls_select, sqls_where, sqls_final, to_column_name, from_column, gl_journal_cat
boolean b_inception_to_date_amount, b_source_id, b_gl_journal_category, b_quantity
longlong rtn, i, source_id, from_table_column, max_id, fcst_depr_version_id, cr_start_year, cr_end_year

string insert_sqls, select_sqls, sqls, field, ack_list, ack_default_value, ack_select_list, &
		 insert_where_clause, delete_where_clause, col_select_list, group_select_list, s_date, &
		 fcst_depr_amt_type, rtns, amount_fields
longlong num_rows, num_elements, ack_pos, col_order, k
date ddate
time ttime
datetime started_at, finished_at

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
started_at = datetime(ddate, ttime)

if i_cr_table = 'none' then
	i_cr_table = 'CR_BUDGET_DATA'
end if

// Get the fcst_depr_version_id
select fcst_depr_version_id into :fcst_depr_version_id
  from fcst_depr_version
 where upper(trim(description)) = upper(trim(:i_cap_budget_version));
 
if isnull(fcst_depr_version_id) then fcst_depr_version_id = 0

if fcst_depr_version_id = 0 then
	uf_msg("  ")
	uf_msg("ERROR: Could not find the fcst_depr_version_id for '" + i_cap_budget_version + "'!")
	uf_msg("  ")
	return -1
end if

// Get the start and end years for the CR budget version so we can
// restrict the data coming from fcst_depr_ledger
select start_year, end_year
  into :cr_start_year, :cr_end_year
  from cr_budget_version
 where budget_version = :i_cr_budget_version;
 
if isnull(cr_start_year) then cr_start_year = 0
if isnull(cr_end_year) then cr_end_year = 0

if cr_start_year = 0 or cr_end_year = 0 then
	uf_msg("  ")
	uf_msg("ERROR: Could not find the start and end years for CR version '" + i_cr_budget_version + "'!")
	uf_msg("  ")
	return -1
end if

select costrepository.nextval into :g_batch_id from dual;
if isnull(g_batch_id) then g_batch_id = '0'

if g_batch_id = '0' then
	uf_msg("  ")
	uf_msg("ERROR: Could not get the interface_batch_id from costrepository.nextval")
	uf_msg("  ")
	return -1
end if							

///use this value if they don't provide a gl_journal category in the mapping table
gl_journal_cat = 'PM'

b_inception_to_date_amount = false
b_source_id = false
b_gl_journal_category = false
b_quantity = false

// Build ds_fcst_amt_type to hold all of the amount types to be mapped
select fcst_depr_amt_type 
  into :fcst_depr_amt_type
  from cr_to_budget_to_cr_control
 where id = :i_id;

if isnull(fcst_depr_amt_type) then fcst_depr_amt_type = ""

if fcst_depr_amt_type = "" then
	uf_msg("  ")
	uf_msg("ERROR: Could not get the FCST DEPR Amount Types to map!")
	uf_msg("  ")
	return -1
end if

sqls = "select amount_fields from cr_to_bdg_to_cr_fcst_amt "

if fcst_depr_amt_type = '*' then
	// all of them; don't include a where clause
else
	sqls = sqls + " where description in (" + fcst_depr_amt_type + ")"
end if

uo_ds_top ds_fcst_amt_type
ds_fcst_amt_type = CREATE uo_ds_top

rtns = f_create_dynamic_ds(ds_fcst_amt_type,'grid',sqls,sqlca,true)

if rtns <> 'OK' then
	uf_msg("  ")
	uf_msg("ERROR: Building ds_fcst_amt_type: " + ds_fcst_amt_type.i_sqlca_sqlerrtext)
	uf_msg("SQLS: " + sqls)
	uf_msg("  ")
	return -1
end if

// Truncate cr_budget_data_projects
sqlca.truncate_table('cr_budget_data_projects');
	
if sqlca.SQLCode < 0 then
	uf_msg("ERROR: Truncating cr_budget_data_projects : " + &
			 sqlca.SQLErrText )
	rollback;
	return -1
end if

// Build the posting columns ds
sqls_ds = " select replace(replace(replace(to_column_name,' ','_'),'-','_'),'/','_') to_column_name, " + &
	       "from_column from_column, " + &
		 	 "nvl(from_table_column, 0) from_table_column, " + &
			 ' "TYPE" col_type, "WIDTH" col_width, "DECIMAL" col_decimal, '+&
			 ' "ORDER" col_order ' + &
			 " from cr_to_budget_to_cr_columns" + &
			 " where id = " + string(i_id) + " order by " + '"ORDER"'
		 
uo_ds_top ds_columns
ds_columns = CREATE uo_ds_top
		 
rtns = f_create_dynamic_ds(ds_columns,"grid",sqls_ds,sqlca,true)

if rtns <> 'OK' then
	uf_msg("  ")
	uf_msg("ERROR: Building ds_columns: " + ds_columns.i_sqlca_sqlerrtext)
	uf_msg("SQLS: " + sqls)
	uf_msg("  ")
	return -1
end if

num_rows = ds_columns.rowcount()

if num_rows < 1 then
	uf_msg('ERROR: Unable to find Budget Columns with posting id ' + string(i_id))
	return -1
end if

// Build the CR elements ds, the ACK list, and the ACK part of the 
// col_select_list and group_select_list
uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
ds_elements.DataObject = "dw_cr_element_definitions"  //sorted by "ORDER"
ds_elements.SetTransObject(sqlca)
num_elements = ds_elements.RETRIEVE()

uf_msg("Building ACK list")

for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	col_order = ds_elements.GetItemNumber(i, "order") + 1
	
	ack_list = ack_list + '"' + upper(field) + '", '
	ack_select_list = ack_select_list + '"' + upper(field) + '", '
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
next

uf_msg("Building Insert SQLS")

// Build the insert statement to insert into cr_budget_data_projects
insert_sqls = insert_sqls + '"ID", ' + ack_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))	
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	Choose case to_column_name
		case "ID", "BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "AMOUNT", "MONTH_NUMBER", &
			  "MONTH_PERIOD", "GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", &
			  "ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", &			
  			  "WORK_ORDER_ID", "INTERFACE_BATCH_ID"
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case "QUANTITY"
			b_quantity = true
		case "INCEPTION_TO_DATE_AMOUNT"
			b_inception_to_date_amount = true
		case "GL_JOURNAL_CATEGORY"
			b_gl_journal_category = true
		case "SOURCE_ID"
			b_source_id = true
		case else
			// determine whether to_column_name is part of the ACK		
			if col_order >= 2 and col_order <= num_elements + 1 then
				// it is an ACK element, skip it
				continue
			end if
			
	end Choose
	
	insert_sqls = insert_sqls + '"' + to_column_name + '", '
	
next

insert_sqls = insert_sqls + '"AMOUNT", "MONTH_NUMBER", '

// If the user mapped quantity, inception_to_date_amount, gl_journal_category,
// or source_id in the control window, the field(s) will already be in insert_sqls.
// Otherwise, we need to add them and we'll map default values below.
if not b_quantity	then
	insert_sqls = insert_sqls + '"QUANTITY", '
end if

if not b_inception_to_date_amount then
	insert_sqls = insert_sqls + '"INCEPTION_TO_DATE_AMOUNT", '
end if

if not b_gl_journal_category then
	insert_sqls = insert_sqls + '"GL_JOURNAL_CATEGORY", '
end if

if not b_source_id then
	setnull(source_id)
source_id = g_cr.uf_get_source_id("description",'POWERPLANT') //Autogenerated sql replace
	
	if isnull(source_id) then
		select min(source_id) into :source_id
		  from cr_sources;
	end if
	
	insert_sqls = insert_sqls + '"SOURCE_ID", '
end if

insert_sqls = insert_sqls + '"BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "MONTH_PERIOD",' +  &
			     '"GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", ' + &
				  '"ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", "INTERFACE_BATCH_ID" '

uf_msg("Building Select SQLS")

// Build the select statement for the ACK based on the mappings in the control window.
// NOTE: The idea here is to start from a list of the ACK and replace the field
//			with the mapping entered by the user.  
for i = 1 to num_rows
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if col_order >= 2 and col_order <= num_elements + 1 then
		// part of the ACK -- ID is always first column
		to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
		from_column = ds_columns.GetItemString(i, "from_column")
		from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
		
		if from_column = "'_'" then from_column = "' '"
		
		if from_table_column = 1 then
			from_column = uf_clean_string(from_column)
			// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
			// alias the columns with supp or pt.
			if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
				from_column = "SUPPLEMENTAL_TYPE_ID" then
				
				from_column = 'supp."' + upper(from_column) + '"'
			else
				from_column = 'pt."' + upper(from_column) + '"'
			end if
		end if
		
		ack_default_value = ds_elements.GetItemString(col_order - 1, "default_value")
		// We want to nvl everything since the ACK fields are non-nullable
		from_column = "nvl(" + from_column + ", '" + ack_default_value + "') c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, '"' + to_column_name + '"', from_column, 'first')
	end if
next

// Figure out which ACK elements are not mapped by the user and replace with the default value.
for i = 1 to num_elements
	field = ds_elements.GetItemString(i, "budgeting_element")
	field = uf_clean_string(field)
	field = '"' + upper(field) + '"'

	ack_pos = 0
	ack_pos = pos(ack_select_list, field)
	
	if isnull(ack_pos) then
		uf_msg("Null Value...")
		return -1
	elseif ack_pos > 0 then
		// has not been mapped yet
		ack_default_value = ds_elements.GetItemString(i, "default_value")
		
		col_order = ds_elements.GetItemNumber(i, "order") + 1
		from_column = "'" + ack_default_value + "' c" + string(col_order)
		
		ack_select_list = f_replace_string(ack_select_list, field, from_column, 'first')
	end if
next

// Build the select statement -- we've already built the ACK part of the select statement, so
// now we need to build the rest of the statement starting with the non-ACK fields that the
// user mapped in the control window.
// NOTE: In order to allow for subselects to be entered in the mappings, I chose to build
//			the sql so that everything the user enters goes into it's own view, including the
// 		the amount and month number - which the user can't map, so that the sums will work
//			properly without requiring the user to be clever about the way they enter their
//			subselects.  
//			Below three things are built: 
//					select_sqls - contains the SQL going into the view mentioned above
//					col_select_list - the list of every column in the view (minus amount, 
//						month number, and work order id)
//					group_select_list - the list of every column in the view that is not
//						being summed (minus month number and work order id)
select_sqls = select_sqls + ack_select_list
for i = 1 to num_rows
	to_column_name = upper(ds_columns.GetItemString(i, "to_column_name"))
	Choose case to_column_name
		case "ID", "BUDGET_VERSION", "AMOUNT_TYPE", "DR_CR_ID", "LEDGER_SIGN", "AMOUNT", "MONTH_NUMBER", &
			  "MONTH_PERIOD", "GL_ID", "BATCH_ID", "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID", &
			  "ALLOCATION_ID", "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY", &			
  			  "WORK_ORDER_ID", "INTERFACE_BATCH_ID"
			// these fields are not available to the user to map into in the 
			// posting control window
			continue
		case else
			col_order = ds_columns.GetItemNumber(i, "col_order")
			if col_order >= 2 and col_order <= num_elements + 1 then
				// part of the ACK
				continue
			end if
	end Choose
	
	from_column = ds_columns.GetItemString(i, "from_column")
	from_table_column = ds_columns.GetItemNumber(i, "from_table_column")
	col_order = ds_columns.GetItemNumber(i, "col_order")
	
	if from_column = "'_'" then from_column = "' '"
	
	if from_table_column = 1 then
		
		from_column = uf_clean_string(from_column)
		// If we are using the "MONTHLY,SUPPLEMENTAL" mappings, it is necessary to
		// alias the columns with supp or pt.
		if from_column = "SUPPLEMENTAL_DATA" or from_column = "SUPPLEMENTAL_ID" or &
			from_column = "SUPPLEMENTAL_TYPE_ID" then
			
			from_column = 'supp."' + upper(from_column) + '"'
		else
			from_column = 'pt."' + upper(from_column) + '"'
		end if
				
		
	else
		// Need to support sum -- i.e. the user can map into the quantity field
		if left(upper(from_column),4) = 'SUM(' then
			// trim off 'SUM(' from the front and ')' from the end
			from_column = left(from_column, len(from_column) - 1)
			from_column = right(from_column, len(from_column) - 4)
			
			select_sqls = select_sqls + from_column + " s" + string(col_order) + ", "
			col_select_list = col_select_list + " sum(y.s" + string(col_order) + "), "
			continue
		end if
	end if
	
	select_sqls = select_sqls + from_column + " c" + string(col_order) + ", "
	col_select_list = col_select_list + "y.c" + string(col_order) + ", "
	group_select_list = group_select_list + "y.c" + string(col_order) + ", "
	
next

// Build Amt Sub Selects

// Get the insert where clause
if i_insert_where_clause = 0 or isnull(i_insert_where_clause) then
	// User did not pick a where clause so skip this part
else
	uf_msg("Building Insert Where Clause")
	insert_where_clause = uf_build_where_clause(i_insert_where_clause)
end if
if isnull(insert_where_clause) then insert_where_clause = ""

if insert_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the insert where clause.")
	rollback;
	return -1
end if

uf_msg("Inserting into cr_budget_data_projects")

// Build sqls
sqls  = 'insert into cr_budget_data_projects ( '
sqls += insert_sqls + ") "
sqls += ' select crbudgets.nextval, x.*, '
if not b_quantity then sqls += '0, '
if not b_inception_to_date_amount then sqls += '0, '
if not b_gl_journal_category then sqls += "'" + gl_journal_cat + "', "
if not b_source_id then sqls += string(source_id) + ', '
// ### - SEK - 5978 - 122110
//sqls += "'" + i_budget_version + "', "
sqls += "'" + i_cr_budget_version + "', "
sqls += "2, " //amount_type
sqls += 'decode(sign(x.amount),0,1,sign(x.amount)), ' //dr_cr_id
sqls += '1, ' //ledger_sign
sqls += '0, ' //month_period
sqls += 'null, ' //gl_id
sqls += string(g_batch_id) + ', ' //batch_id
sqls += 'null, ' //cwip_charge_status
sqls += 'sysdate, ' //time_stamp
sqls += 'user, ' //user_id
//"ALLOCATION_ID",
if i_alloc then
	sqls += "(select -99 from estimate_charge_type e where pt.est_chg_type_id = e.est_chg_type_id and nvl(e.afudc_flag,'U') = 'O'), "
else
	sqls += " null,"
end if
sqls += 'null, null, null, ' // "TARGET_CREDIT", "REVERSAL_ID", "CROSS_CHARGE_COMPANY"
sqls += string(g_batch_id) + ' ' //"INTERFACE_BATCH_ID"
sqls += 'from ( '
sqls += 	'select ' + col_select_list 
sqls +=	 'sum(y.amount) amount, y.month_number from ( '
// Loop over ds_fcst_amt_type to build the selects for each of the amount types selected to 
//		be mapped
for k = 1 to ds_fcst_amt_type.rowcount()
	amount_fields = ds_fcst_amt_type.GetItemString(k,1)
		
	sqls +=		'select ' + select_sqls
	sqls += 			amount_fields + " amount,"
	if upper(i_projects_table) = 'FCST_DEPR_LEDGER' then
		sqls +=			"to_number(to_char(pt.gl_post_mo_yr,'YYYYMM')) month_number "
	else
		sqls +=			"to_number(to_char(pt.gl_posting_mo_yr,'YYYYMM')) month_number "
	end if
	sqls +=		'from ' + i_projects_table + ' pt, cr_to_bdg_to_cr_fcst_amt fa '
	sqls += 	  'where pt.fcst_depr_version_id = ' + string(fcst_depr_version_id) + ' '
	if upper(i_projects_table) = 'FCST_DEPR_LEDGER' then
		sqls +=		" and to_char(pt.gl_post_mo_yr,'YYYY') between '" + string(cr_start_year) + "' and '" + string(cr_end_year) + "' "
	else
		sqls +=		" and to_char(pt.gl_posting_mo_yr,'YYYY') between '" + string(cr_start_year) + "' and '" + string(cr_end_year) + "' "
	end if
	sqls +=		" and fa.amount_fields = '" + amount_fields + "' "
	sqls += 		" and pt.fcst_depr_group_id in ( " 
	if upper(i_projects_table) = 'FCST_DEPR_LEDGER' then
		sqls += " select fcst_depr_group_id from fcst_depr_group_version " 
		sqls += " where fcst_depr_version_id = " + string(fcst_depr_version_id) + ""	
		sqls += "   and subledger_type_id = 0 "
	else
		sqls += " select fcst_depr_group_id from fcst_depr_group_version f, subledger_control sc " 
		sqls += " where f.fcst_depr_version_id = " + string(fcst_depr_version_id) + ""	
		sqls += "   and f.subledger_type_id <> 0 "
		sqls += "   and f.subledger_type_id = sc.subledger_type_id "
		sqls += "   and sc.depreciation_indicator = 3 "
	end if
	sqls += ") "
	sqls += insert_where_clause +i_additional_where_clause
	
	if k <> ds_fcst_amt_type.rowcount() then
		sqls += " union all "
	end if
next	
sqls +=	' ) y '
sqls += 'group by ' + group_select_list + ' y.month_number) x'


execute immediate :sqls;

if sqlca.sqlcode < 0 then
	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

//***********************************************
// CALL TO CUSTOM FUNCTION FOR PROCESSING OF DATA
//***********************************************
uf_msg("Calling custom function")
rtn = f_cr_to_budget_to_cr_custom(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr_depr_fcst',i_cr_budget_version,this)
if rtn < 0 then
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_to_cr' ) = 1 then
	rtn = sqlca.f_client_budget_to_cr(i_id, insert_where_clause, delete_where_clause, i_cap_budget_version, 'uf_to_cr_depr_fcst',i_cr_budget_version)
	if rtn < 0 then
		rollback;
		return -1
	end if
end if
string args[]
args[1] = string(i_id)
args[2] = insert_where_clause
args[3] = delete_where_clause
args[4] = i_cap_budget_version
args[5] = 'uf_to_cr_depr_fcst'
args[6] = i_cr_budget_version
rtn = f_wo_validation_control(84,args)
if rtn < 0 then
	rollback;
	return -1
end if

// Get the delete where clause
if i_delete_where_clause = 0 or isnull(i_delete_where_clause) then
	// the user did not select a where clause so skip this part
else
	delete_where_clause = uf_build_where_clause(i_delete_where_clause)
end if
if isnull(delete_where_clause) then delete_where_clause = ""

if delete_where_clause = "ERROR" then
//	uf_msg("ERROR: Building the delete where clause.")
	rollback;
	return -1
end if

uf_msg("Deleting from cr_budget_data")

// Delete from cr_budget_data and then insert stg data into cr_budget_data
// ### - SEK - 5978 - 122110
sqls = "delete from "+i_cr_table+" cr " + &
		 "where budget_version = '" + i_cr_budget_version + "' " + delete_where_clause + i_additional_delete_where_clause
//		 "where budget_version = '" + i_budget_version + "' " + delete_where_clause + i_additional_delete_where_clause
		
execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

uf_msg("Inserting into "+i_cr_table)
sqls = "insert into "+i_cr_table+" (" + insert_sqls + ") " + &
		 "select " + insert_sqls + " from cr_budget_data_projects"

execute immediate :sqls;
		
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_budget_data_projects: " + sqlca.SQLErrText)
	uf_msg("SQLS: " + sqls)
	rollback;
	return -1
end if

commit;

// Insert into cr_to_budget_to_cr_audit -- do this after the commit because it's not
// a big deal if this fails
uf_msg("Inserting into cr_to_budget_to_cr_audit")

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
finished_at = datetime(ddate, ttime)

select max(occurrence_id) into :max_id
  from cr_to_budget_to_cr_audit
 where id = :i_id;
 
if isnull(max_id) then max_id = 0
max_id++

insert into cr_to_budget_to_cr_audit (
	id, occurrence_id, budget_version, running_user_id, start_time, end_time, total_amount, 
	total_records, insert_where_clause, delete_where_clause, time_stamp, user_id)
select :i_id,
		 :max_id,
		 :i_budget_version,
		 user,
		 :started_at,
		 :finished_at,
		 sum(amount),
		 count(*),
			 :insert_where_clause||' '||:i_additional_where_clause,
			 :delete_where_clause||' '||:i_additional_delete_where_clause,
		 sysdate,
		 user
  from cr_budget_data_projects;
  
if sqlca.sqlcode < 0 then		
	uf_msg("ERROR: Inserting into cr_to_budget_to_cr_audit: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if  
		 


return 1


end function

on uo_cr_to_budget_to_cr.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_to_budget_to_cr.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

