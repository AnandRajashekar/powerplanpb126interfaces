HA$PBExportHeader$uo_afudc_cpi_calc.sru
$PBExportComments$45241,45322
forward
global type uo_afudc_cpi_calc from nonvisualobject
end type
end forward

global type uo_afudc_cpi_calc from nonvisualobject
end type
global uo_afudc_cpi_calc uo_afudc_cpi_calc

type variables

longlong i_month_number, i_company_id, i_expenditure_type_id, i_currency_display_factor , i_je_id

datetime  i_month

boolean i_visual_calc, i_afudc_error, i_cap_int, i_cap_int_parent, i_afudc_processed 

string i_je_code, i_gl_je_code, i_estimate_cv, i_company_descr


end variables

forward prototypes
public subroutine of_msg (string a_msg, string a_msg_type)
public function longlong of_check_cap_interest ()
public function longlong of_config_validations ()
public function longlong of_cwip_charge ()
public function longlong of_cwip_charge_insert ()
public function longlong of_cwip_charge_insert_dw ()
public function longlong of_get_est_and_acts ()
public function longlong of_idle_months ()
public function longlong of_in_service_month ()
public function longlong of_last_compound_month ()
public function longlong of_late_stop_date ()
public function longlong of_min_months ()
public function longlong of_min_amt ()
public function longlong of_not_eligible ()
public function longlong of_status_and_rates ()
public function longlong of_triggers ()
public function longlong of_stop_date_month ()
public function longlong of_wolist ()
public function longlong of_cpi_deminimis ()
public function longlong of_pct_cpi ()
public function longlong of_rollforward_afudc_calc ()
public function longlong of_afudc_and_cpi_calc_main ()
public function longlong of_build_prior_month_base ()
public function longlong of_calc_afudc ()
public function longlong of_calc_cpi ()
public function longlong of_check_prior_charges_afudc_cpi ()
public function longlong of_cap_int_final_adjust_afudc_debt ()
public function longlong of__start_calc (longlong a_company_id, longlong a_month_number, datetime a_month)
public function longlong of_late_in_service_afc ()
public function longlong of_late_in_service_wo_update ()
public function longlong of_late_in_service_stop_date_same ()
public function longlong of_late_in_service_cpi ()
end prototypes

public subroutine of_msg (string a_msg, string a_msg_type);
// f_pp_msgs
if a_msg_type = 'ERROR' then
	f_pp_msgs("  ")
	f_pp_msgs(a_msg)
	f_pp_msgs("  ")
	
	if i_visual_calc then
		f_wo_status_box('UO_AFUDC',a_msg)
		messagebox('UO_AFUDC',a_msg)
	end if
else
	f_pp_msgs(a_msg)
	
	if i_visual_calc then
		f_wo_status_box('UO_AFUDC',a_msg)
	end if
end if
end subroutine

public function longlong of_check_cap_interest ();//**************************************************************************************************************
//This function determines if the company is a Cap Int company and sets the i_cap_int and i_cap_int_parent variables used later in the calculation logic
//**************************************************************************************************************
longlong counter

counter = 0

select count(company_id) into :counter
  from cap_interest_calc
 where company_id = :i_company_id
   and month_number = :i_month_number;
	
if isnull(counter) then counter = 0

if counter = 0 then
	i_cap_int = false
else
	i_cap_int = true
	
	counter = 0
	
	select count(cs.company_id) into :counter
	  from company_setup cs, cap_interest_wo ciw, work_order_control woc
	 where cs.company_id = :i_company_id
	   and ciw.parent_wo = woc.work_order_id
		and cs.company_id = woc.company_id;
		
	if isnull(counter) then counter = 0
	
	if counter <> 0 then 
		//This company is a parent
		i_cap_int_parent = true
	else
		//This company is a child
		i_cap_int_parent = false
	end if
end if 

return 1
end function

public function longlong of_config_validations ();//*****************************************************************************************************************************
//This function data validations:   rates, cpi de minimis, charge types
//******************************************************************************************************************************
longlong nrows, min_afudc_type_id, percent_book_basis, afudc_elig_indicator, cpi_elig_indicator

//Maint 30196:  verify the tax repairs charge types are setup properly where none of these three columns are set to 1
select max(nvl(percent_book_basis,0)), max(nvl(afudc_elig_indicator,0)), max(nvl(cpi_elig_indicator,0))
into :percent_book_basis, :afudc_elig_indicator, :cpi_elig_indicator
from charge_type_data
where charge_type_id in (
				select charge_type_id
				from charge_type
				where book_summary_id in (
									select book_summary_id from book_summary
									where lower(summary_name) like '%tax repair%'
									union 
									select book_summary_id from book_summary
									where lower(summary_name) like '%tax expense%'
									union
									select reversal_book_summary_id  
									from	tax_reversal_book_summary
									where tax_reversal_type = 'Repairs' 
									))
;

if percent_book_basis = 1 then
	of_msg("ERROR: bad configuration found for tax repairs CHARGE TYPES","ERROR")
	of_msg("ERROR:   the percent_book_basis should be set to No on CHARGE_TYPE_DATA table","ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if 

if afudc_elig_indicator = 1 then
	of_msg("ERROR: bad configuration found for tax repairs CHARGE TYPES","ERROR")
	of_msg("ERROR:   the afudc_elig_indicator should be set to No on CHARGE_TYPE_DATA table","ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if 

if cpi_elig_indicator = 1 then
	of_msg("ERROR: bad configuration found for tax repairs CHARGE TYPES","ERROR")
	of_msg("ERROR:   the cpi_elig_indicator should be set to No on CHARGE_TYPE_DATA table","ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if 

//first initiate the rates and status
update AFUDC_CALC_WO_LIST_TEMP x
set (x.debt_rate, x.equity_rate, x.cpi_rate, x.afudc_status_id, x.cpi_status_id) = (select ac.debt_rate, ac.equity_rate, ac.cpi_rate, ac.afudc_status_id, ac.cpi_status_id 
														from afudc_calc ac 
														where ac.work_order_id = x.work_order_id
														and to_char(ac.month,'yyyymm') = :i_month_number);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then 
	of_msg("ERROR: updating AFUDC_CALC_WO_LIST_TEMP.rates: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//Check for records that are missing the Debt Rate
min_afudc_type_id = 0
select min(a.afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP a 
where  a.debt_rate is null
and a.eligible_for_afudc = 1; 

if isnull(min_afudc_type_id) then min_afudc_type_id = 0

if min_afudc_type_id > 0 then
	of_msg("The Afudc Debt Rate needs to be configured for afudc_type_id = " + string(min_afudc_type_id) + ".  Please update the AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

//Check for records that are missing the Equity Rate
min_afudc_type_id = 0
select min(a.afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP a 
where  a.equity_rate is null
and a.eligible_for_afudc = 1; 

if isnull(min_afudc_type_id) then min_afudc_type_id = 0

if min_afudc_type_id > 0 then
	of_msg("The Afudc Equity Rate needs to be configured for afudc_type_id = " + string(min_afudc_type_id) + ".  Please update the AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

//Check for records that are missing the CPI Rate
min_afudc_type_id = 0
select min(a.afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP a 
where  a.cpi_rate is null
and a.eligible_for_cpi = 1; 

if isnull(min_afudc_type_id) then min_afudc_type_id = 0

if min_afudc_type_id > 0 then
	of_msg("The CPI Rate needs to be configured for afudc_type_id = " + string(min_afudc_type_id) + ".  Please update the AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

// Check for records that have CPI De Minimis set to Yes that do not have cpi_de_minimis_wo_or_fp configured
min_afudc_type_id = 0
select min(afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP
where nvl(cpi_de_minimis_yn,0) = 1
and nvl(cpi_de_minimis_wo_or_fp, 'X') not in  ('FP', 'WO THEN FP', 'WO')
and eligible_for_cpi = 1; 

if isnull(min_afudc_type_id) then min_afudc_type_id = 0

if min_afudc_type_id > 0 then
	of_msg("The CPI DeMinimis WO or FP needs to be configured properly for afudc_type_id = " + string(min_afudc_type_id) + ".  Please update the AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

// Check for records that have CPI De Minimis set to Yes that do not have cpi_de_minimis_amount configured
min_afudc_type_id = 0
select min(afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP
where nvl(cpi_de_minimis_yn,0) = 1
and cpi_de_minimis_amount is null
and eligible_for_cpi = 1; 
	
if isnull(min_afudc_type_id) then min_afudc_type_id = 0

if min_afudc_type_id > 0 then
	of_msg("The CPI DeMinimis Amount needs to be configured for afudc_type_id = " + string(min_afudc_type_id) + ".  Please update the AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

//Check charge types are not null
min_afudc_type_id = 0
select min(afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP
where DEBT_CHARGE_TYPE is null
and eligible_for_afudc = 1;

if min_afudc_type_id > 0 then 
	of_msg("The DEBT_CHARGE_TYPE for afudc_type_id = " + string(min_afudc_type_id) + " is missing.  Please update the DEBT_COST_ELEMENT on AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

min_afudc_type_id = 0
select min(afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP
where EQUITY_CHARGE_TYPE is null
and eligible_for_afudc = 1;

if min_afudc_type_id > 0 then 
	of_msg("The EQUITY_CHARGE_TYPE for afudc_type_id = " + string(min_afudc_type_id) + " is missing.  Please update the EQUITY_COST_ELEMENT on AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

min_afudc_type_id = 0
select min(afudc_type_id) into :min_afudc_type_id
from AFUDC_CALC_WO_LIST_TEMP
where CPI_CHARGE_TYPE is null
and eligible_for_cpi = 1; 

if min_afudc_type_id > 0 then 
	of_msg("The CPI_CHARGE_TYPE for afudc_type_id = " + string(min_afudc_type_id) + " is missing.  Please update the CPI_COST_ELEMENT on AFUDC Control table.","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

return 1
end function

public function longlong of_cwip_charge ();//***************************************************************************************************************************
// This function gathers information to determine how the CWIP_CHARGE table inserts is going to be handled
//    PP System Control = 'AFUDC Calc-Use External GL Account' = Yes allows for simple insert into CWIP_CHARGE table for default EXTERNAL_GL_ACCOUNT
//    PP System Control = 'AFUDC Calc-Use External GL Account' = No requires the use of a dw and wo loop to insert into CWIP_CHARGE table for custom EXTERNAL_GL_ACCOUNT
//***************************************************************************************************************************

string je_code_option, je_code, cwip_external_account_option
longlong je_id, rtn

je_id = -999
select je_id into :je_id 
from gl_je_control where process_id = 'AFUDC';

if je_id = -999 then
	of_msg("ERROR: Unable to find gl_je_control record for AFUDC","ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

je_code_option = f_pp_system_control_company('AFUDC Approval - Use External JE',i_company_id)
	
if je_code_option = 'yes' or je_code_option = '' then
	select external_je_code into :je_code 
	from standard_journal_entries where je_id = :je_id;
else
	select gl_je_code into :je_code 
	 from standard_journal_entries where je_id = :je_id;
end if

i_je_code = je_code

//If 'Yes', the AFUDC Calc will store the  External GL Account column from the GL account table into the external account of the Cwip charge. Dfault is 'No' which uses a custom function
cwip_external_account_option = f_pp_system_control_company('AFUDC Calc-Use External GL Account', i_company_id)
	
if isnull(cwip_external_account_option) or cwip_external_account_option = '' then
	cwip_external_account_option = 'no'  //no is the default
end if

of_msg(" AFUDC Calc-Use External GL Account = " + cwip_external_account_option + "  " + string(now()),"...") 

if cwip_external_account_option = 'no' then
	//need to use the old and slower way of looping through a datawindow (dw_wo_charges_summ) to call f_autogen_je_account for each cwip_charge table insert
	rtn = of_cwip_charge_insert_dw()
	if rtn < 0 then
		return -1
	end if 
else
	//this insert into cwip_charge uses the default external data
	rtn = of_cwip_charge_insert()	
	if rtn < 0 then
		return -1
	end if 
end if  

return 1
end function

public function longlong of_cwip_charge_insert ();//***********************************************************************************************************
// This function does simple inserts into CWIP_CHARGE table using defaults for EXTERNAL_GL_ACCOUNT column 
//***********************************************************************************************************
string ext_gl_acct_cpi
longlong nrows
of_msg("	Insert into CWIP_CHARGE  " + string(now()),"...")			
//the default for AFUDC is by the wo cwip_gl_account
  
//  *** Insert DEBT CHARGE into cwip_charge table ...
insert into cwip_charge (charge_id, gl_account_id, expenditure_type_id, 
	work_order_id, charge_type_id, charge_mo_yr, description, quantity, 
	bus_segment_id, amount, hours, department_id, cost_element_id, payment_date, 
	charge_audit_id, month_number, company_id, external_gl_account, status, journal_code) 
select pwrplant3.nextval,
		 wo.cwip_gl_account, 
		 1, 
		 ac.work_order_id, 
		 wo.debt_charge_type,  
		 ac.month, 
		 decode(wo.afudc_status_id, 3, 'AFUDC debt charge Adjustment', 'AFUDC debt charge'),
		 0, 
		 wo.bus_segment_id, 
		 ac.afudc_debt, 
		 0, 
		 wo.department_id, 
		 wo.debt_cost_element, 
		 ac.month, 
		 pwrplant3.currval,
		 :i_month_number, 
		 :i_company_id, 
		 trim(gl.external_account_code), 
		 1, 
		 :i_je_code
from afudc_calc ac, AFUDC_CALC_WO_LIST_TEMP wo, gl_account gl
where ac.work_order_id = wo.work_order_id  
and gl.gl_account_id= wo.cwip_gl_account
and wo.company_id = :i_company_id
and ac.month = :i_month   
and ac.afudc_debt <> 0;

nrows = sqlca.sqlnrows
	
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Inserting into cwip_charge for AFUDC Debt: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if


//  *** Insert EQUITY CHARGE into cwip_charge table ...
insert into cwip_charge (charge_id, gl_account_id, expenditure_type_id, 
	work_order_id, charge_type_id, charge_mo_yr, description, quantity, 
	bus_segment_id, amount, hours, department_id, cost_element_id, payment_date, 
	charge_audit_id, month_number, company_id, external_gl_account, status, journal_code) 
select pwrplant3.nextval,
		 wo.cwip_gl_account, 
		 1, 
		 ac.work_order_id, 
		 wo.equity_charge_type,  
		 ac.month, 
		 decode(wo.afudc_status_id, 3, 'AFUDC equity charge Adjustment', 'AFUDC equity charge'),
		 0, 
		 wo.bus_segment_id, 
		 ac.afudc_equity, 
		 0, 
		 wo.department_id, 
		 wo.equity_cost_element, 
		 ac.month, 
		 pwrplant3.currval,
		 :i_month_number, 
		 :i_company_id, 
		 trim(gl.external_account_code), 
		 1, 
		 :i_je_code
from afudc_calc ac, AFUDC_CALC_WO_LIST_TEMP wo, gl_account gl
where ac.work_order_id = wo.work_order_id  
and gl.gl_account_id= wo.cwip_gl_account
and wo.company_id = :i_company_id
and ac.month = :i_month   
and ac.afudc_equity <> 0;

nrows = sqlca.sqlnrows
	
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Inserting into cwip_charge for AFUDC Equity: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if


//default for CPI
select external_account_code into :ext_gl_acct_cpi
from gl_account 
where gl_account_id = 1;

if isnull(ext_gl_acct_cpi) then ext_gl_acct_cpi = ''

//  *** Insert CPI CHARGE into cwip_charge table ...
//  *** gl_account_id is always 1 for CPI ...
insert into cwip_charge (charge_id, gl_account_id, expenditure_type_id, 
	work_order_id, charge_type_id, charge_mo_yr, description, quantity, 
	bus_segment_id, amount, hours, department_id, cost_element_id, payment_date, 
	charge_audit_id, month_number, company_id, external_gl_account, status) 
select pwrplant3.nextval,
		 1, 
		 1, 
		 ac.work_order_id, 
		 wo.cpi_charge_type,  
		 ac.month, 
		 decode(wo.cpi_status_id, 3, 'CPI charge Adjustment', 'CPI charge'),
		 0, 
		 wo.bus_segment_id, 
		 ac.cpi, 
		 0, 
		 wo.department_id, 
		 wo.cpi_cost_element, 
		 ac.month, 
		 pwrplant3.currval,
		 :i_month_number, 
		 :i_company_id, 
		 :ext_gl_acct_cpi, 
		 1 
from afudc_calc ac, AFUDC_CALC_WO_LIST_TEMP wo 
where ac.work_order_id = wo.work_order_id   
and wo.company_id = :i_company_id
and ac.month = :i_month   
and ac.cpi <> 0;

nrows = sqlca.sqlnrows
	
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Inserting into cwip_charge for CPI: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

return 1
end function

public function longlong of_cwip_charge_insert_dw ();longlong rtn, i, rowcount, charge_id, wo_id, nrows
string ext_gl_acct
decimal{2} afudc_debt, afudc_equity, cpi

of_msg("	Insert into CWIP_CHARGE  " + string(now()),"...")			
//the columns specifications of dw_afudc_calc_autogen_dw is a mirror image of the old dw_wo_charges_summ_all
uo_ds_top ds_afudc_calc_autogen_dw
ds_afudc_calc_autogen_dw = CREATE uo_ds_top
ds_afudc_calc_autogen_dw.DataObject = "dw_afudc_calc_autogen_dw"
ds_afudc_calc_autogen_dw.SetTransObject(sqlca) 

rtn = ds_afudc_calc_autogen_dw.RETRIEVE(i_month_number, i_company_id)

if rtn < 0 then 
	of_msg("ERROR: ds_afudc_calc_autogen_dw.RETRIEVE failed: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

rowcount = ds_afudc_calc_autogen_dw.rowcount()

//  *** Insert DEBT CHARGE into cwip_charge table ...
for i = 1 to rowcount  
	
	wo_id = ds_afudc_calc_autogen_dw.getitemnumber(i, 'work_order_id') 
	afudc_debt = ds_afudc_calc_autogen_dw.getitemdecimal(i, 'afudc_debt') 
	afudc_equity = ds_afudc_calc_autogen_dw.getitemdecimal(i, 'afudc_equity') 
	cpi = ds_afudc_calc_autogen_dw.getitemdecimal(i, 'cpi') 
	
	setnull(ext_gl_acct)
	ext_gl_acct = f_autogen_je_account(ds_afudc_calc_autogen_dw, i, 4, 0, 1)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(ext_gl_acct,1,5) = 'ERROR' then
		of_msg("Error occurred while generating the transaction's gl account.  Generated value : " + ext_gl_acct,"ERROR")
		rollback;
		i_afudc_error = true
		destroy ds_afudc_calc_autogen_dw
		return -1 
	end if
	
	if afudc_debt <> 0 then	
		insert into cwip_charge (charge_id, gl_account_id, expenditure_type_id, 
			work_order_id, charge_type_id, charge_mo_yr, description, quantity, 
			bus_segment_id, amount, hours, department_id, cost_element_id, payment_date, 
			charge_audit_id, month_number, company_id, external_gl_account, status, journal_code) 
		select pwrplant3.nextval,
			 wo.cwip_gl_account, 
			 1, 
			 ac.work_order_id, 
			 wo.debt_charge_type,  
			 ac.month, 
			 decode(wo.afudc_status_id, 3,'AFUDC debt charge Adjustment', 'AFUDC debt charge'),
			 0, 
			 wo.bus_segment_id, 
			 ac.afudc_debt, 
			 0, 
			 wo.department_id, 
			 wo.debt_cost_element, 
			 ac.month, 
			 pwrplant3.currval,
			 :i_month_number, 
			 :i_company_id, 
			 :ext_gl_acct, 
			 1, 
			 :i_je_code
		from afudc_calc ac, AFUDC_CALC_WO_LIST_TEMP wo 
		where ac.work_order_id = wo.work_order_id   
		and wo.work_order_id = :wo_id 
		and ac.month = :i_month   
		and ac.afudc_debt <> 0;
	
		nrows = sqlca.sqlnrows
			
		if sqlca.sqlcode < 0 then
			of_msg("ERROR: Inserting into cwip_charge for AFUDC Debt (dw) for wo_id = " +string(wo_id) +": " + sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			destroy ds_afudc_calc_autogen_dw
			return -1 
		end if 
	end if  //if afudc_debt <> 0 then	
	
	if afudc_equity <> 0 then	
		insert into cwip_charge (charge_id, gl_account_id, expenditure_type_id, 
			work_order_id, charge_type_id, charge_mo_yr, description, quantity, 
			bus_segment_id, amount, hours, department_id, cost_element_id, payment_date, 
			charge_audit_id, month_number, company_id, external_gl_account, status, journal_code) 
		select pwrplant3.nextval,
			 wo.cwip_gl_account, 
			 1, 
			 ac.work_order_id, 
			 wo.equity_charge_type,  
			 ac.month, 
			 decode(wo.afudc_status_id, 3,'AFUDC equity charge Adjustment', 'AFUDC equity charge'),
			 0, 
			 wo.bus_segment_id, 
			 ac.afudc_equity, 
			 0, 
			 wo.department_id, 
			 wo.equity_cost_element, 
			 ac.month, 
			 pwrplant3.currval,
			 :i_month_number, 
			 :i_company_id, 
			 :ext_gl_acct, 
			 1, 
			 :i_je_code
		from afudc_calc ac, AFUDC_CALC_WO_LIST_TEMP wo 
		where ac.work_order_id = wo.work_order_id   
		and wo.work_order_id = :wo_id 
		and ac.month = :i_month   
		and ac.afudc_equity <> 0;
	
		nrows = sqlca.sqlnrows
			
		if sqlca.sqlcode < 0 then
			of_msg("ERROR: Inserting into cwip_charge for AFUDC Equity (dw) for wo_id = " +string(wo_id) +": " + sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			destroy ds_afudc_calc_autogen_dw
			return -1 
		end if 
	end if  //if afudc_equity <> 0 then	
	
	if cpi <> 0 then	
		insert into cwip_charge (charge_id, gl_account_id, expenditure_type_id, 
			work_order_id, charge_type_id, charge_mo_yr, description, quantity, 
			bus_segment_id, amount, hours, department_id, cost_element_id, payment_date, 
			charge_audit_id, month_number, company_id, external_gl_account, status) 
		select pwrplant3.nextval,
			 1, 
			 1, 
			 ac.work_order_id, 
			 wo.cpi_charge_type,  
			 ac.month, 
			 decode(wo.cpi_status_id, 3,'CPI charge Adjustment', 'CPI charge'),
			 0, 
			 wo.bus_segment_id, 
			 ac.cpi, 
			 0, 
			 wo.department_id, 
			 wo.cpi_cost_element, 
			 ac.month, 
			 pwrplant3.currval,
			 :i_month_number, 
			 :i_company_id, 
			 :ext_gl_acct, 
			 1 
		from afudc_calc ac, AFUDC_CALC_WO_LIST_TEMP wo 
		where ac.work_order_id = wo.work_order_id   
		and wo.work_order_id = :wo_id 
		and ac.month = :i_month   
		and ac.cpi <> 0;
	
		nrows = sqlca.sqlnrows
			
		if sqlca.sqlcode < 0 then
			of_msg("ERROR: Inserting into cwip_charge for CPI (dw) for wo_id = " +string(wo_id) +": " + sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			destroy ds_afudc_calc_autogen_dw
			return -1 
		end if 
	end if  //if cpi <> 0 then	
	
next  

destroy ds_afudc_calc_autogen_dw
  
return 1
end function

public function longlong of_get_est_and_acts ();//***************************************************************************************************************************
//This function updates estimate and actuals dollars for the work orders that are still eligible for afudc and cpi and need to go through tests that involve $$ compares
//The"has_prior_afc" and "has_prior_cpi" columns filter to the work orders that need this updated performed.
//Work order with prior afudc and cpi do not need to be evalulated for this tests using estimate/actual dollars, because they must have passed the test at some point in the past.
//***************************************************************************************************************************
longlong nrows, min_est_amt_afudc_distinct, min_est_amt_cpi_distinct, min_est_amt_afudc_all, min_est_amt_cpi_all, percent_est_for_cpi_distinct
boolean afudc_est_act_test_needed, cpi_est_act_test_needed, percent_ests_for_cpi_needed
decimal{8} percent_est_for_cpi_all

// Get the system switch to determine which estimates are used for the min. estimate check
i_estimate_cv = f_pp_system_control_company('WOEST - HEADER FROM WO_ESTIMATE', i_company_id)

afudc_est_act_test_needed = true
cpi_est_act_test_needed = true
percent_ests_for_cpi_needed = true 
min_est_amt_afudc_distinct = 0
min_est_amt_cpi_distinct = 0
percent_est_for_cpi_distinct = 0
min_est_amt_afudc_all = 0
min_est_amt_cpi_all = 0
percent_est_for_cpi_all = 0 

//Let's figure out if any Estimate and Actuals testing needs to be done  
select count(distinct min_est_amt_afudc), max(min_est_amt_afudc), count(distinct min_est_amt_cpi), max(min_est_amt_cpi), count(distinct percent_est_for_cpi), max(percent_est_for_cpi)
into : min_est_amt_afudc_distinct, :min_est_amt_afudc_all, :min_est_amt_cpi_distinct, :min_est_amt_cpi_all, :percent_est_for_cpi_distinct, :percent_est_for_cpi_all
from AFUDC_CALC_WO_LIST_TEMP; 

//if min_est_amt_afudc_distinct is greater than 1 then, there is mix of work orders needing to go through testing and others that do not
if min_est_amt_afudc_distinct = 1 and min_est_amt_afudc_all = 0 then
	//all of the work orders have MIN_EST_AMT_AFUDC = 0, which means the MIN_EST_AMT_AFUDC is not really being used 
	afudc_est_act_test_needed = false
end if  

//if min_est_amt_cpi_distinct is greater than 1 then, there is mix of work orders needing to go through testing and others that do not
if min_est_amt_cpi_distinct = 1 and min_est_amt_cpi_all = 0 then
	//all of the work orders have MIN_EST_AMT_CPI = 0, which means the MIN_EST_AMT_CPI is not really being used 
	cpi_est_act_test_needed = false
end if   

//if percent_est_for_cpi_distinct is greater than 1 then, there is mix of work orders needing to go through testing and others that do not
if percent_est_for_cpi_distinct = 1 and percent_est_for_cpi_all = 0 then
	//all of the work orders have PERCENT_EST_FOR_CPI = 0, which means the PERCENT_EST_FOR_CPI is not really being used 
	percent_ests_for_cpi_needed = false
end if   

//Use the information obtained above to see if we need to update the Estimate and Actuals data for work orders
if percent_ests_for_cpi_needed = false and cpi_est_act_test_needed = false and afudc_est_act_test_needed = false then
	//nothing to do
else
	
	of_msg("	Estimate dollars updates  " + string(now()),"...")
	
	if upper(trim(i_estimate_cv)) = 'YES' then 
		update AFUDC_CALC_WO_LIST_TEMP a
		set a.max_revision =  (select max(e.revision) from wo_estimate e
									 where e.work_order_id = a.work_order_id
										and e.revision <> 1000) 
		where (a.afudc_status_id = 1 or a.cpi_status_id = 2) 
		and abs(nvl(min_est_amt_afudc,0)) + abs(nvl(min_est_amt_cpi,0)) + abs(nvl(percent_est_for_cpi,0)) <> 0
		and abs(nvl(has_prior_afc,0)) + abs(nvl(has_prior_cpi,0)) in (0,1);

		nrows = sqlca.sqlnrows
												
		if sqlca.sqlcode < 0 then 
			of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.max_revision(1): " + sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			return -1 
		end if			
		
		// Use wo_estimate (excluding as-builts) and wo_est_capital_view2	 
		update AFUDC_CALC_WO_LIST_TEMP wo
		set wo.WO_EST_AMOUNT = (select EST.CAPITAL from wo_est_capital_view2 est
											 where est.work_order_id = wo.work_order_id 
												and est.revision = wo.max_revision 
												and wo.max_revision is not null)
		where (wo.afudc_status_id = 1 or wo.cpi_status_id = 2) 
		and wo.max_revision is not null;

		nrows = sqlca.sqlnrows
				
		if sqlca.sqlcode < 0 then 
			of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_EST_AMOUNT (1): " + sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			return -1 
		end if
		
	else
		update AFUDC_CALC_WO_LIST_TEMP a
		set a.max_revision = 	(select max(e.revision) from work_order_approval e
									where e.work_order_id = a.work_order_id)
		where (a.afudc_status_id = 1 or a.cpi_status_id = 2) 
		and abs(nvl(min_est_amt_afudc,0)) + abs(nvl(min_est_amt_cpi,0)) + abs(nvl(percent_est_for_cpi,0)) <> 0
		and abs(nvl(has_prior_afc,0)) + abs(nvl(has_prior_cpi,0)) in (0,1);		

		nrows = sqlca.sqlnrows		
												
		if sqlca.sqlcode < 0 then 
			of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.max_revision(2): " + sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			return -1 
		end if			
										
		// Use work_order_approval and wo_est_capital_view 	
		update AFUDC_CALC_WO_LIST_TEMP wo
		set wo.WO_EST_AMOUNT = (select EST.CAPITAL from wo_est_capital_view est
									 			where est.work_order_id = wo.work_order_id 
												and est.revision = wo.max_revision 
												and wo.max_revision is not null)
		where (wo.afudc_status_id = 1 or wo.cpi_status_id = 2) 
		and wo.max_revision is not null;		

		nrows = sqlca.sqlnrows
				
		if sqlca.sqlcode < 0 then 
			of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_EST_AMOUNT (2): " + sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			return -1 
		end if
		
	end if 	
	
	//If there are no estimates, the max_revision will be a Null value
	//need to set the WO_EST_AMOUNT = 0 and max_revision = 0 in this situation where the "min" options <> 0 and one of the "prior" values = 0
	update AFUDC_CALC_WO_LIST_TEMP wo
	set wo.WO_EST_AMOUNT = 0, wo.max_revision = 0
	where (wo.afudc_status_id = 1 or wo.cpi_status_id = 2) 
	and abs(nvl(wo.min_est_amt_afudc,0)) + abs(nvl(wo.min_est_amt_cpi,0)) + abs(nvl(wo.percent_est_for_cpi,0)) <> 0
	and abs(nvl(wo.has_prior_afc,0)) + abs(nvl(wo.has_prior_cpi,0)) in (0,1)
	and wo.max_revision is null;

	nrows = sqlca.sqlnrows
			
	if sqlca.sqlcode < 0 then 
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_EST_AMOUNT (no estimates): " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1 
	end if
		
	of_msg("	Actual dollars updates  " + string(now()),"...")
	
	//AFUDC
	update AFUDC_CALC_WO_LIST_TEMP a
	set a.AFUDC_ACT_AMOUNT = (select nvl(sum(nvl(b.afudc_elig_indicator,0)*c.wo_amount),0)
										from charge_type_data b , wo_summary c
										where a.work_order_id = c.work_order_id
										and b.charge_type_id = c.charge_type_id
										and c.expenditure_type_id = :i_expenditure_type_id
										and c.month_number <= :i_month_number
										group by c.work_order_id)
	where a.eligible_for_afudc = 1 
	and a.afudc_status_id = 1
	and abs(nvl(min_est_amt_afudc,0)) + abs(nvl(min_est_amt_cpi,0)) + abs(nvl(percent_est_for_cpi,0)) <> 0 
	and nvl(has_prior_afc, 0) = 0;			 

	nrows = sqlca.sqlnrows
				
	if sqlca.sqlcode < 0 then 
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_ACT_AMOUNT: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1 
	end if
	
	//CPI
	update AFUDC_CALC_WO_LIST_TEMP a
	set a.CPI_ACT_AMOUNT = (select nvl(sum(nvl(b.cpi_elig_indicator,0)*c.wo_amount),0)
										from charge_type_data b , wo_summary c
										where a.work_order_id = c.work_order_id
										and b.charge_type_id = c.charge_type_id
										and c.expenditure_type_id = :i_expenditure_type_id
										and c.month_number <= :i_month_number
										group by c.work_order_id)
	where a.eligible_for_cpi = 1  
	and a.cpi_status_id = 2
	and abs(nvl(min_est_amt_afudc,0)) + abs(nvl(min_est_amt_cpi,0)) + abs(nvl(percent_est_for_cpi,0)) <> 0
	and nvl(has_prior_cpi, 0) = 0;			

	nrows = sqlca.sqlnrows
				
	if sqlca.sqlcode < 0 then 
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.CPI_ACT_AMOUNT: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1 
	end if
 
end if  //if percent_ests_for_cpi_needed = false and cpi_est_act_test_needed = false and afudc_est_act_test_needed = false then

return 1
end function

public function longlong of_idle_months ();//**************************************************************************************************************
//This function identifies work orders that need to be flagged as "Failed Idle Months Check for AFUDC, No AFUDC Calculated"  afudc_status_id = 19
//                                                                                    and "Failed Idle Months Check for CPI, No CPI Calculated"  cpi_status_id = 20 
//The idle_afudc_months represents the number of months in between charges (w/ charge_type.idle_afudc_direct = 1 ) on a particular work order that can pass 
//        before the work order is considerded idle for AFUDC calculations. An idle work order implies that AFUDC will not be calculated for that work order during that month. 
//        afudc_control.idle_afudc_months field = null or 0 means the Idle AFUDC functionality is not being used.
//
//  These work orders will later get their rates set to 0 to generate $0 afudc or $0 CPI 
//**************************************************************************************************************
longlong nrows, idle_afudc_direct, idle_cpi_direct, idle_afudc_months_distinct, idle_cpi_months_distinct, idle_afudc_months_all, idle_cpi_months_all
boolean idle_afudc_months_needed, idle_cpi_months_needed 

idle_afudc_months_distinct = 0
idle_cpi_months_distinct = 0
idle_afudc_months_all = 0
idle_cpi_months_all = 0
idle_afudc_direct = 0
idle_cpi_direct = 0
idle_afudc_months_needed  = true
idle_cpi_months_needed  = true
 
//Let's figure out if any Idle months testing needs to be done
select count(distinct idle_afudc_months), max(idle_afudc_months), count(distinct idle_cpi_months), max(idle_cpi_months) 
into :idle_afudc_months_distinct, :idle_afudc_months_all, :idle_cpi_months_distinct, :idle_cpi_months_all 
from AFUDC_CALC_WO_LIST_TEMP; 

//if idle_afudc_months_distinct is greater than 1 then, there is mix of work orders needing to go through testing and others that do not
if idle_afudc_months_distinct = 1 and idle_afudc_months_all = 0 then
	//all of the work orders have idle_afudc_months = 0, which means the idle_afudc_months is not really being used 
	idle_afudc_months_needed = false
end if  

//if idle_cpi_months_distinct is greater than 1 then, there is mix of work orders needing to go through testing and others that do not
if idle_cpi_months_distinct = 1 and idle_cpi_months_all = 0 then
	//all of the work orders have idle_cpi_months = 0, which means the idle_cpi_months is not really being used 
	idle_cpi_months_needed = false
end if  

select count(charge_type_id) into :idle_afudc_direct
from charge_type where idle_afudc_direct = 1;
   
select count(charge_type_id) into :idle_cpi_direct
from charge_type where idle_cpi_direct = 1;

// AFUDC
if idle_afudc_direct > 0 and idle_afudc_months_needed then 
	
	of_msg("	Idle AFUDC Months Test  " + string(now()),"...")
	
	update AFUDC_CALC_WO_LIST_TEMP wo
	set wo.afudc_status_id = 19 
	where wo.afudc_status_id = 1 
	and wo.idle_afudc_months > 0    
	and wo.eligible_for_afudc = 1 
	and not exists (
				select 1 from WO_SUMMARY ws, CHARGE_TYPE ct
				where ws.work_order_id = wo.work_order_id
				and ct.charge_type_id = ws.charge_type_id
                  and ws.month_number between
                     to_number(to_char(add_months(to_date(:i_month_number, 'yyyymm'),-1*(wo.idle_afudc_months - 1)), 'yyyymm')) and :i_month_number
				and ct.idle_afudc_direct = 1
				and decode(wo.idle_exp_type_addition, 1, 1, ws.expenditure_type_id) = ws.expenditure_type_id 
				and rownum = 1
					);

	nrows = sqlca.sqlnrows
					
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 19: " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if   
					 
end if 

// CPI
if idle_cpi_direct > 0 and idle_cpi_months_needed then
	
	of_msg("	Idle CPI Months Test " + string(now()),"...")
	
	update AFUDC_CALC_WO_LIST_TEMP wo
	set wo.cpi_status_id = 20 
	where wo.cpi_status_id = 2  
	and wo.idle_cpi_months > 0   
	and wo.eligible_for_cpi = 1 
	and not exists (
				select 1 from WO_SUMMARY ws, CHARGE_TYPE ct
				where ws.work_order_id = wo.work_order_id
				and ct.charge_type_id = ws.charge_type_id
                  and ws.month_number between
                     to_number(to_char(add_months(to_date(:i_month_number, 'yyyymm'),-1*(wo.idle_cpi_months - 1)), 'yyyymm')) and :i_month_number
				and ct.idle_cpi_direct = 1
				and decode(wo.idle_exp_type_addition, 1, 1, ws.expenditure_type_id) = ws.expenditure_type_id 
				and rownum = 1
					);

	nrows = sqlca.sqlnrows
					
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 20: " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if   
					 
end if 
						
						
return 1
end function

public function longlong of_in_service_month ();//*****************************************************************************************************************************
//This function handles updates for work orders that have an in_serve_date
//1. for in_service_date = curent month, need to be flagged as "Current Month In-service Work Order"  afudc_status_id = 13 & cpi_status_id =13
//2. for in_service_date = current month, need to adjust the rates
//******************************************************************************************************************************
longlong nrows
of_msg("	In Service Month Updates  " + string(now()),"")

// identify work orders where in_service_date is less than the current month
update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = 13
where  nvl(to_number(to_char(in_service_date,'YYYYMM')),'999999') = :i_month_number 
and afudc_status_id in (1, 24)  // Maint 45322
and eligible_for_afudc = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 13: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = 13 
where nvl(to_number(to_char(in_service_date,'YYYYMM')),'999999') = :i_month_number 
and cpi_status_id in (2, 24)  // Maint 45322
and eligible_for_cpi = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 13: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

// identify work orders where in_service_date is equal to the current month (use the rules that apply to IN_SERVICE_OPTION)
//AFUDC 
update afudc_calc ac
	set (ac.afudc_status_id, ac.debt_rate, ac.equity_rate) = 
							(select wo.afudc_status_id, wo.debt_rate*stop_month_rate_adj, wo.equity_rate*stop_month_rate_adj
							from AFUDC_CALC_WO_LIST_TEMP wo
							where wo.work_order_id = ac.work_order_id
							and nvl(to_number(to_char(wo.in_service_date,'YYYYMM')),'999999') = :i_month_number
							and wo.afudc_status_id = 13
							and wo.eligible_for_afudc = 1)
where to_number(to_char(ac.month,'YYYYMM')) = :i_month_number
and ac.afudc_status_id in (1, 24)  // Maint 45322
and exists (
	select 1 from AFUDC_CALC_WO_LIST_TEMP w
	where ac.work_order_id = w.work_order_id  
	and nvl(to_number(to_char(w.in_service_date,'YYYYMM')),'999999') = :i_month_number
	and w.afudc_status_id = 13
	and w.eligible_for_afudc = 1
	);	

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.debt_rate & equity_rate  for in_service_date = month: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

//CPI 
update afudc_calc ac
set (ac.cpi_status_id, ac.cpi_rate) = 
							(select wo.cpi_status_id, wo.cpi_rate*stop_month_rate_adj from AFUDC_CALC_WO_LIST_TEMP wo
							where ac.work_order_id = wo.work_order_id
							and nvl(to_number(to_char(wo.in_service_date,'YYYYMM')),'999999') = :i_month_number
							and wo.cpi_status_id = 13
							and wo.eligible_for_cpi = 1)
where to_number(to_char(month,'YYYYMM')) = :i_month_number
and ac.cpi_status_id  in (2, 24)  // Maint 45322
and exists (
	select 1 from AFUDC_CALC_WO_LIST_TEMP w
	where ac.work_order_id = w.work_order_id
	and nvl(to_number(to_char(w.in_service_date,'YYYYMM')),'999999') = :i_month_number
	and w.cpi_status_id = 13
	and w.eligible_for_cpi = 1
	);	

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.cpi_rate for in_service_date = month: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

//Update work_order_control
update work_order_control a
set a.original_in_service_date = a.in_service_date,
a.orig_isd_month_number = :i_month_number
where a.original_in_service_date is null 
and exists (select 1 from AFUDC_CALC_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id
				and nvl(to_number(to_char(b.in_service_date,'YYYYMM')),'999999') = :i_month_number
				);  /*Maint 45322:  update the orig columns if wo header has in service date equal to calc month regardless of afudc_status_id/cpi_status_id*/ 

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (13) Updating work_order_control.original_in_service_date and orig_isd_month_number: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

return 1
end function

public function longlong of_last_compound_month ();//****************************************************************************************************
// Work orders that do not have afudc_calc table data for prior month have been identified
//  Prior month data with the correct afudc_base and cpi_base has been inserted
//  This function will identify the last compounded month for afudc and cpi.
//****************************************************************************************************
longlong nrows
string month_string

month_string = right(string(i_month_number), 2)

choose case month_string 
	case "01" 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case  
						when afudc_dec_com_ind = 1 then :i_month_number - 89   
						when afudc_nov_com_ind = 1 then :i_month_number - 90  
						when afudc_oct_com_ind = 1 then :i_month_number - 91 
						when afudc_sep_com_ind = 1 then :i_month_number - 92  
						when afudc_aug_com_ind = 1 then :i_month_number - 93  
						when afudc_jul_com_ind = 1 then :i_month_number - 94
						when afudc_jun_com_ind = 1 then :i_month_number - 95  
						when afudc_may_com_ind = 1 then :i_month_number - 96 
						when afudc_apr_com_ind = 1 then :i_month_number - 97  
						when afudc_mar_com_ind = 1 then :i_month_number - 98  
						when afudc_feb_com_ind = 1 then :i_month_number - 99 
						when afudc_jan_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_dec_com_ind = 1 then :i_month_number - 89   
						when cpi_nov_com_ind = 1 then :i_month_number - 90  
						when cpi_oct_com_ind = 1 then :i_month_number - 91 
						when cpi_sep_com_ind = 1 then :i_month_number - 92  
						when cpi_aug_com_ind = 1 then :i_month_number - 93  
						when cpi_jul_com_ind = 1 then :i_month_number - 94
						when cpi_jun_com_ind = 1 then :i_month_number - 95  
						when cpi_may_com_ind = 1 then :i_month_number - 96 
						when cpi_apr_com_ind = 1 then :i_month_number - 97  
						when cpi_mar_com_ind = 1 then :i_month_number - 98  
						when cpi_feb_com_ind = 1 then :i_month_number - 99 
						when cpi_jan_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "02" 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case  
						when afudc_jan_com_ind = 1 then :i_month_number - 1  
						when afudc_dec_com_ind = 1 then :i_month_number - 90   
						when afudc_nov_com_ind = 1 then :i_month_number - 91  
						when afudc_oct_com_ind = 1 then :i_month_number - 92 
						when afudc_sep_com_ind = 1 then :i_month_number - 93  
						when afudc_aug_com_ind = 1 then :i_month_number - 94  
						when afudc_jul_com_ind = 1 then :i_month_number - 95 
						when afudc_jun_com_ind = 1 then :i_month_number - 96  
						when afudc_may_com_ind = 1 then :i_month_number - 97 
						when afudc_apr_com_ind = 1 then :i_month_number - 98  
						when afudc_mar_com_ind = 1 then :i_month_number - 99  
						when afudc_feb_com_ind = 1 then :i_month_number - 100 
						else :i_month_number 
					end case ,
					case 
						when cpi_jan_com_ind = 1 then :i_month_number - 1  
						when cpi_dec_com_ind = 1 then :i_month_number - 90   
						when cpi_nov_com_ind = 1 then :i_month_number - 91  
						when cpi_oct_com_ind = 1 then :i_month_number - 92 
						when cpi_sep_com_ind = 1 then :i_month_number - 93  
						when cpi_aug_com_ind = 1 then :i_month_number - 94  
						when cpi_jul_com_ind = 1 then :i_month_number - 95 
						when cpi_jun_com_ind = 1 then :i_month_number - 96  
						when cpi_may_com_ind = 1 then :i_month_number - 97 
						when cpi_apr_com_ind = 1 then :i_month_number - 98  
						when cpi_mar_com_ind = 1 then :i_month_number - 99  
						when cpi_feb_com_ind = 1 then :i_month_number - 100 
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "03"   
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_feb_com_ind = 1 then :i_month_number - 1  
						when afudc_jan_com_ind = 1 then :i_month_number - 2  
						when afudc_dec_com_ind = 1 then :i_month_number - 91   
						when afudc_nov_com_ind = 1 then :i_month_number - 92  
						when afudc_oct_com_ind = 1 then :i_month_number - 93 
						when afudc_sep_com_ind = 1 then :i_month_number - 94  
						when afudc_aug_com_ind = 1 then :i_month_number - 95  
						when afudc_jul_com_ind = 1 then :i_month_number - 96 
						when afudc_jun_com_ind = 1 then :i_month_number - 97  
						when afudc_may_com_ind = 1 then :i_month_number - 98 
						when afudc_apr_com_ind = 1 then :i_month_number - 99  
						when afudc_mar_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_feb_com_ind = 1 then :i_month_number - 1  
						when cpi_jan_com_ind = 1 then :i_month_number - 2  
						when cpi_dec_com_ind = 1 then :i_month_number - 91   
						when cpi_nov_com_ind = 1 then :i_month_number - 92  
						when cpi_oct_com_ind = 1 then :i_month_number - 93 
						when cpi_sep_com_ind = 1 then :i_month_number - 94  
						when cpi_aug_com_ind = 1 then :i_month_number - 95  
						when cpi_jul_com_ind = 1 then :i_month_number - 96 
						when cpi_jun_com_ind = 1 then :i_month_number - 97  
						when cpi_may_com_ind = 1 then :i_month_number - 98 
						when cpi_apr_com_ind = 1 then :i_month_number - 99  
						when cpi_mar_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "04"  
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_mar_com_ind = 1 then :i_month_number - 1  
						when afudc_feb_com_ind = 1 then :i_month_number - 2  
						when afudc_jan_com_ind = 1 then :i_month_number - 3  
						when afudc_dec_com_ind = 1 then :i_month_number - 92  
						when afudc_nov_com_ind = 1 then :i_month_number - 93  
						when afudc_oct_com_ind = 1 then :i_month_number - 94 
						when afudc_sep_com_ind = 1 then :i_month_number - 95  
						when afudc_aug_com_ind = 1 then :i_month_number - 96  
						when afudc_jul_com_ind = 1 then :i_month_number - 97 
						when afudc_jun_com_ind = 1 then :i_month_number - 98  
						when afudc_may_com_ind = 1 then :i_month_number - 99 
						when afudc_apr_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_mar_com_ind = 1 then :i_month_number - 1  
						when cpi_feb_com_ind = 1 then :i_month_number - 2  
						when cpi_jan_com_ind = 1 then :i_month_number - 3  
						when cpi_dec_com_ind = 1 then :i_month_number - 92  
						when cpi_nov_com_ind = 1 then :i_month_number - 93  
						when cpi_oct_com_ind = 1 then :i_month_number - 94 
						when cpi_sep_com_ind = 1 then :i_month_number - 95  
						when cpi_aug_com_ind = 1 then :i_month_number - 96  
						when cpi_jul_com_ind = 1 then :i_month_number - 97 
						when cpi_jun_com_ind = 1 then :i_month_number - 98  
						when cpi_may_com_ind = 1 then :i_month_number - 99 
						when cpi_apr_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
		
	case "05"  
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_apr_com_ind = 1 then :i_month_number - 1  
						when afudc_mar_com_ind = 1 then :i_month_number - 2  
						when afudc_feb_com_ind = 1 then :i_month_number - 3  
						when afudc_jan_com_ind = 1 then :i_month_number - 4  
						when afudc_dec_com_ind = 1 then :i_month_number - 93  
						when afudc_nov_com_ind = 1 then :i_month_number - 94  
						when afudc_oct_com_ind = 1 then :i_month_number - 95 
						when afudc_sep_com_ind = 1 then :i_month_number - 96  
						when afudc_aug_com_ind = 1 then :i_month_number - 97  
						when afudc_jul_com_ind = 1 then :i_month_number - 98 
						when afudc_jun_com_ind = 1 then :i_month_number - 99  
						when afudc_may_com_ind = 1 then :i_month_number - 100 
						else :i_month_number 
					end case ,
					case 
						when cpi_apr_com_ind = 1 then :i_month_number - 1  
						when cpi_mar_com_ind = 1 then :i_month_number - 2  
						when cpi_feb_com_ind = 1 then :i_month_number - 3  
						when cpi_jan_com_ind = 1 then :i_month_number - 4  
						when cpi_dec_com_ind = 1 then :i_month_number - 93  
						when cpi_nov_com_ind = 1 then :i_month_number - 94  
						when cpi_oct_com_ind = 1 then :i_month_number - 95 
						when cpi_sep_com_ind = 1 then :i_month_number - 96  
						when cpi_aug_com_ind = 1 then :i_month_number - 97  
						when cpi_jul_com_ind = 1 then :i_month_number - 98 
						when cpi_jun_com_ind = 1 then :i_month_number - 99  
						when cpi_may_com_ind = 1 then :i_month_number - 100 
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "06"
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_may_com_ind = 1 then :i_month_number - 1 
						when afudc_apr_com_ind = 1 then :i_month_number - 2  
						when afudc_mar_com_ind = 1 then :i_month_number - 3  
						when afudc_feb_com_ind = 1 then :i_month_number - 4  
						when afudc_jan_com_ind = 1 then :i_month_number - 5  
						when afudc_dec_com_ind = 1 then :i_month_number - 94  
						when afudc_nov_com_ind = 1 then :i_month_number - 95  
						when afudc_oct_com_ind = 1 then :i_month_number - 96  
						when afudc_sep_com_ind = 1 then :i_month_number - 97  
						when afudc_aug_com_ind = 1 then :i_month_number - 98  
						when afudc_jul_com_ind = 1 then :i_month_number - 99 
						when afudc_jun_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_may_com_ind = 1 then :i_month_number - 1 
						when cpi_apr_com_ind = 1 then :i_month_number - 2  
						when cpi_mar_com_ind = 1 then :i_month_number - 3  
						when cpi_feb_com_ind = 1 then :i_month_number - 4  
						when cpi_jan_com_ind = 1 then :i_month_number - 5  
						when cpi_dec_com_ind = 1 then :i_month_number - 94  
						when cpi_nov_com_ind = 1 then :i_month_number - 95  
						when cpi_oct_com_ind = 1 then :i_month_number - 96  
						when cpi_sep_com_ind = 1 then :i_month_number - 97  
						when cpi_aug_com_ind = 1 then :i_month_number - 98  
						when cpi_jul_com_ind = 1 then :i_month_number - 99 
						when cpi_jun_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "07"		 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_jun_com_ind = 1 then :i_month_number - 1  
						when afudc_may_com_ind = 1 then :i_month_number - 2 
						when afudc_apr_com_ind = 1 then :i_month_number - 3  
						when afudc_mar_com_ind = 1 then :i_month_number - 4  
						when afudc_feb_com_ind = 1 then :i_month_number - 5  
						when afudc_jan_com_ind = 1 then :i_month_number - 6  
						when afudc_dec_com_ind = 1 then :i_month_number - 95  
						when afudc_nov_com_ind = 1 then :i_month_number - 96  
						when afudc_oct_com_ind = 1 then :i_month_number - 97  
						when afudc_sep_com_ind = 1 then :i_month_number - 98  
						when afudc_aug_com_ind = 1 then :i_month_number - 99  
						when afudc_jul_com_ind = 1 then :i_month_number - 100 
						else :i_month_number 
					end case ,
					case 
						when cpi_jun_com_ind = 1 then :i_month_number - 1  
						when cpi_may_com_ind = 1 then :i_month_number - 2 
						when cpi_apr_com_ind = 1 then :i_month_number - 3  
						when cpi_mar_com_ind = 1 then :i_month_number - 4  
						when cpi_feb_com_ind = 1 then :i_month_number - 5  
						when cpi_jan_com_ind = 1 then :i_month_number - 6  
						when cpi_dec_com_ind = 1 then :i_month_number - 95  
						when cpi_nov_com_ind = 1 then :i_month_number - 96  
						when cpi_oct_com_ind = 1 then :i_month_number - 97  
						when cpi_sep_com_ind = 1 then :i_month_number - 98  
						when cpi_aug_com_ind = 1 then :i_month_number - 99  
						when cpi_jul_com_ind = 1 then :i_month_number - 100 
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "08"		 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_jul_com_ind = 1 then :i_month_number - 1 
						when afudc_jun_com_ind = 1 then :i_month_number - 2  
						when afudc_may_com_ind = 1 then :i_month_number - 3 
						when afudc_apr_com_ind = 1 then :i_month_number - 4  
						when afudc_mar_com_ind = 1 then :i_month_number - 5  
						when afudc_feb_com_ind = 1 then :i_month_number - 6  
						when afudc_jan_com_ind = 1 then :i_month_number - 7  
						when afudc_dec_com_ind = 1 then :i_month_number - 96 
						when afudc_nov_com_ind = 1 then :i_month_number - 97  
						when afudc_oct_com_ind = 1 then :i_month_number - 98  
						when afudc_sep_com_ind = 1 then :i_month_number - 99  
						when afudc_aug_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_jul_com_ind = 1 then :i_month_number - 1 
						when cpi_jun_com_ind = 1 then :i_month_number - 2  
						when cpi_may_com_ind = 1 then :i_month_number - 3 
						when cpi_apr_com_ind = 1 then :i_month_number - 4  
						when cpi_mar_com_ind = 1 then :i_month_number - 5  
						when cpi_feb_com_ind = 1 then :i_month_number - 6  
						when cpi_jan_com_ind = 1 then :i_month_number - 7  
						when cpi_dec_com_ind = 1 then :i_month_number - 96 
						when cpi_nov_com_ind = 1 then :i_month_number - 97  
						when cpi_oct_com_ind = 1 then :i_month_number - 98  
						when cpi_sep_com_ind = 1 then :i_month_number - 99  
						when cpi_aug_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "09"		 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_aug_com_ind = 1 then :i_month_number - 1  
						when afudc_jul_com_ind = 1 then :i_month_number - 2 
						when afudc_jun_com_ind = 1 then :i_month_number - 3  
						when afudc_may_com_ind = 1 then :i_month_number - 4 
						when afudc_apr_com_ind = 1 then :i_month_number - 5  
						when afudc_mar_com_ind = 1 then :i_month_number - 6  
						when afudc_feb_com_ind = 1 then :i_month_number - 7  
						when afudc_jan_com_ind = 1 then :i_month_number - 8  
						when afudc_dec_com_ind = 1 then :i_month_number - 97 
						when afudc_nov_com_ind = 1 then :i_month_number - 98  
						when afudc_oct_com_ind = 1 then :i_month_number - 99  
						when afudc_sep_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_aug_com_ind = 1 then :i_month_number - 1  
						when cpi_jul_com_ind = 1 then :i_month_number - 2 
						when cpi_jun_com_ind = 1 then :i_month_number - 3  
						when cpi_may_com_ind = 1 then :i_month_number - 4 
						when cpi_apr_com_ind = 1 then :i_month_number - 5  
						when cpi_mar_com_ind = 1 then :i_month_number - 6  
						when cpi_feb_com_ind = 1 then :i_month_number - 7  
						when cpi_jan_com_ind = 1 then :i_month_number - 8  
						when cpi_dec_com_ind = 1 then :i_month_number - 97 
						when cpi_nov_com_ind = 1 then :i_month_number - 98  
						when cpi_oct_com_ind = 1 then :i_month_number - 99  
						when cpi_sep_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "10"		 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_sep_com_ind = 1 then :i_month_number - 1  
						when afudc_aug_com_ind = 1 then :i_month_number - 2  
						when afudc_jul_com_ind = 1 then :i_month_number - 3 
						when afudc_jun_com_ind = 1 then :i_month_number - 4  
						when afudc_may_com_ind = 1 then :i_month_number - 5 
						when afudc_apr_com_ind = 1 then :i_month_number - 6  
						when afudc_mar_com_ind = 1 then :i_month_number - 7 
						when afudc_feb_com_ind = 1 then :i_month_number - 8  
						when afudc_jan_com_ind = 1 then :i_month_number - 9  
						when afudc_dec_com_ind = 1 then :i_month_number - 98
						when afudc_nov_com_ind = 1 then :i_month_number - 99  
						when afudc_oct_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_sep_com_ind = 1 then :i_month_number - 1  
						when cpi_aug_com_ind = 1 then :i_month_number - 2  
						when cpi_jul_com_ind = 1 then :i_month_number - 3 
						when cpi_jun_com_ind = 1 then :i_month_number - 4  
						when cpi_may_com_ind = 1 then :i_month_number - 5 
						when cpi_apr_com_ind = 1 then :i_month_number - 6  
						when cpi_mar_com_ind = 1 then :i_month_number - 7 
						when cpi_feb_com_ind = 1 then :i_month_number - 8  
						when cpi_jan_com_ind = 1 then :i_month_number - 9  
						when cpi_dec_com_ind = 1 then :i_month_number - 98
						when cpi_nov_com_ind = 1 then :i_month_number - 99  
						when cpi_oct_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "11"		 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_oct_com_ind = 1 then :i_month_number - 1  
						when afudc_sep_com_ind = 1 then :i_month_number - 2  
						when afudc_aug_com_ind = 1 then :i_month_number - 3  
						when afudc_jul_com_ind = 1 then :i_month_number - 4 
						when afudc_jun_com_ind = 1 then :i_month_number - 5  
						when afudc_may_com_ind = 1 then :i_month_number - 6 
						when afudc_apr_com_ind = 1 then :i_month_number - 7  
						when afudc_mar_com_ind = 1 then :i_month_number - 8 
						when afudc_feb_com_ind = 1 then :i_month_number - 9  
						when afudc_jan_com_ind = 1 then :i_month_number - 10  
						when afudc_dec_com_ind = 1 then :i_month_number - 99
						when afudc_nov_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case ,
					case 
						when cpi_oct_com_ind = 1 then :i_month_number - 1  
						when cpi_sep_com_ind = 1 then :i_month_number - 2  
						when cpi_aug_com_ind = 1 then :i_month_number - 3  
						when cpi_jul_com_ind = 1 then :i_month_number - 4 
						when cpi_jun_com_ind = 1 then :i_month_number - 5  
						when cpi_may_com_ind = 1 then :i_month_number - 6 
						when cpi_apr_com_ind = 1 then :i_month_number - 7  
						when cpi_mar_com_ind = 1 then :i_month_number - 8 
						when cpi_feb_com_ind = 1 then :i_month_number - 9  
						when cpi_jan_com_ind = 1 then :i_month_number - 10  
						when cpi_dec_com_ind = 1 then :i_month_number - 99
						when cpi_nov_com_ind = 1 then :i_month_number - 100  
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
														
	case "12"		 
		
		update AFUDC_CALC_WO_LIST_TEMP w
		set (w.LAST_COMP_AFC_MN,  w.LAST_COMP_CPI_MN) =  
			(select case 
						when afudc_nov_com_ind = 1 then :i_month_number - 1  
						when afudc_oct_com_ind = 1 then :i_month_number - 2  
						when afudc_sep_com_ind = 1 then :i_month_number - 3  
						when afudc_aug_com_ind = 1 then :i_month_number - 4  
						when afudc_jul_com_ind = 1 then :i_month_number - 5 
						when afudc_jun_com_ind = 1 then :i_month_number - 6  
						when afudc_may_com_ind = 1 then :i_month_number - 7 
						when afudc_apr_com_ind = 1 then :i_month_number - 8  
						when afudc_mar_com_ind = 1 then :i_month_number - 9 
						when afudc_feb_com_ind = 1 then :i_month_number - 10  
						when afudc_jan_com_ind = 1 then :i_month_number - 11  
						when afudc_dec_com_ind = 1 then :i_month_number - 100
						else :i_month_number 
					end case ,
					case 
						when cpi_nov_com_ind = 1 then :i_month_number - 1  
						when cpi_oct_com_ind = 1 then :i_month_number - 2  
						when cpi_sep_com_ind = 1 then :i_month_number - 3  
						when cpi_aug_com_ind = 1 then :i_month_number - 4  
						when cpi_jul_com_ind = 1 then :i_month_number - 5 
						when cpi_jun_com_ind = 1 then :i_month_number - 6  
						when cpi_may_com_ind = 1 then :i_month_number - 7 
						when cpi_apr_com_ind = 1 then :i_month_number - 8  
						when cpi_mar_com_ind = 1 then :i_month_number - 9 
						when cpi_feb_com_ind = 1 then :i_month_number - 10  
						when cpi_jan_com_ind = 1 then :i_month_number - 11  
						when cpi_dec_com_ind = 1 then :i_month_number - 100
						else :i_month_number 
					end case 
				from afudc_data ad, (	select d.afudc_type_id, max(d.effective_date) effective_date
									  from afudc_data d
									  where d.effective_date <= :i_month
									  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id
			and w.AFUDC_CALC_MISSING in (1, 2)
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date)
		where w.AFUDC_CALC_MISSING in (1, 2);
		
	case else
		
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.LAST_COMP_AFC_MN and CPI_MN for invalid month = " + month_string ,"ERROR") 
		rollback;
		i_afudc_error = true
		return -1
		
end choose

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.LAST_COMP_AFC_MN and CPI_MN for month = " + month_string + " : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if					

//maint 43303:  overwrite the LAST_COMP_AFC_MN and LAST_COMP_CPI_MN to 0 values if the work order is setup for no compounding at all, meaning none of the months are configured for compounding
//the updates above would have updated LAST_COMP_AFC_MN and LAST_COMP_CPI_MN to equal i_month_number.
update AFUDC_CALC_WO_LIST_TEMP w
set w.LAST_COMP_AFC_MN = 0
where w.AFUDC_CALC_MISSING in (1, 2)
and w.LAST_COMP_AFC_MN = :i_month_number
and exists (select 1 from afudc_data ad, (select d.afudc_type_id, max(d.effective_date) effective_date
													  from afudc_data d
													  where d.effective_date <= :i_month
													  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id 
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date
			and nvl(afudc_jan_com_ind, 0) = 0
			and nvl(afudc_feb_com_ind, 0) = 0
			and nvl(afudc_mar_com_ind, 0) = 0
			and nvl(afudc_apr_com_ind, 0) = 0
			and nvl(afudc_may_com_ind, 0) = 0
			and nvl(afudc_jun_com_ind, 0) = 0
			and nvl(afudc_jul_com_ind, 0) = 0
			and nvl(afudc_aug_com_ind, 0) = 0
			and nvl(afudc_sep_com_ind, 0) = 0
			and nvl(afudc_oct_com_ind, 0) = 0
			and nvl(afudc_nov_com_ind, 0) = 0
			and nvl(afudc_dec_com_ind, 0) = 0
			);

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.LAST_COMP_AFC_MN = 0 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if			

update AFUDC_CALC_WO_LIST_TEMP w
set w.LAST_COMP_CPI_MN = 0
where w.AFUDC_CALC_MISSING in (1, 2)
and w.LAST_COMP_CPI_MN = :i_month_number
and exists (select 1 from afudc_data ad, (select d.afudc_type_id, max(d.effective_date) effective_date
													  from afudc_data d
													  where d.effective_date <= :i_month
													  group by d.afudc_type_id) current_afudc_view
			where w.afudc_type_id = ad.afudc_type_id 
			and ad.afudc_type_id = current_afudc_view.afudc_type_id
			and ad.effective_date = current_afudc_view.effective_date
			and nvl(cpi_jan_com_ind, 0) = 0
			and nvl(cpi_feb_com_ind, 0) = 0
			and nvl(cpi_mar_com_ind, 0) = 0
			and nvl(cpi_apr_com_ind, 0) = 0
			and nvl(cpi_may_com_ind, 0) = 0
			and nvl(cpi_jun_com_ind, 0) = 0
			and nvl(cpi_jul_com_ind, 0) = 0
			and nvl(cpi_aug_com_ind, 0) = 0
			and nvl(cpi_sep_com_ind, 0) = 0
			and nvl(cpi_oct_com_ind, 0) = 0
			and nvl(cpi_nov_com_ind, 0) = 0
			and nvl(cpi_dec_com_ind, 0) = 0
			);

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.LAST_COMP_CPI_MN = 0 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if			

return 1
end function

public function longlong of_late_stop_date ();//*****************************************************************************************************************************
//This function handles updates for work orders that have a late afudc_stop_date
//1. for afudc_stop_date < curent month, need to be flagged as "AFUDC Stop Date Entered, No AFUDC or CPI Calculated"  afudc_status_id = 14 & cpi_status_id =14 
//
//  These work orders will later get their rates set to 0 to generate $0 afudc or $0 CPI 
//******************************************************************************************************************************
longlong nrows
of_msg("	Late Stop Date Updates  " + string(now()),"")
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// identify work orders where afudc stop date is less than the current month
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = 14
where  nvl(to_number(to_char(afudc_stop_date,'YYYYMM')),'999999') < :i_month_number 
and afudc_status_id = 1
and eligible_for_afudc = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (11A) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 14 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = 14 
where nvl(to_number(to_char(afudc_stop_date,'YYYYMM')),'999999') < :i_month_number 
and cpi_status_id = 2
and eligible_for_cpi = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (11A) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 14 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

return 1
end function

public function longlong of_min_months ();//**************************************************************************************************************
// This function identifies work orders that need to be flagged as "Failed Estimate Time Test, No AFUDC Calculated"  afudc_status_id = 8
//                                                                                    and "Failed Estimate Time Test, No CPI Calculated"  cpi_status_id = 9
//                                                                                    and "Failed Minumum Estimate and Time Test, No AFUDC Calculated"  afudc_status_id = 10
//                                                                                    and "Failed Minumum Estimate and Time Test, No CPI Calculated "  cpi_status_id = 11
// This is the minimum number of months in the estimated work order period (estimated complete date less estimated start date) required of eligible work order in order to calculate AFUDC or book interest.
//  Example: Est Start Date = Jan 2005 and Est Complete Date = Dec 2005. This is a duration of 12 months. If the min_est_mos_afudc is 12, and the duration is at least 12, AFUDC will be calculated.
// This function is designed to be run after the of_afudccalcminamtupdate() which handles afudc_status_id = 6 and 7 
//
// The"has_prior_afc" and "has_prior_cpi" columns filter to the work orders that need this test performed.
// Work order with prior afudc and cpi do not need to be evalulated for this test, because they must have passed the test at some point in the past.
//
//  These work orders will later get their rates set to 0 to generate $0 afudc or $0 CPI 
//**************************************************************************************************************
longlong nrows
of_msg("	Minimum Month Tests  " + string(now()),"")

//AFUDC 
//for work orders with an afudc type that uses afudc_control.min_est_mos_afudc config, check the est_complete_date and est_start_date
update AFUDC_CALC_WO_LIST_TEMP wo
set wo.afudc_status_id = decode(wo.afudc_status_id,6,10,8) 
where wo.eligible_for_afudc = 1 
and wo.afudc_status_id in (1, 6)
and min_est_mos_afudc > 0 
and nvl(has_prior_afc, 0) = 0
and nvl(months_between(est_complete_date,est_start_date) + 1,0)  < min_est_mos_afudc;

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 8/10: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if					 
		
// CPI
//for work orders with an afudc type that uses afudc_control.min_est_mos_cpi config, check the est_complete_date and est_start_date
update AFUDC_CALC_WO_LIST_TEMP wo
set wo.cpi_status_id = decode(cpi_status_id,7,11,9) 
where wo.eligible_for_cpi = 1
and wo.cpi_status_id in (2,7)
and min_est_mos_cpi > 0 
and nvl(has_prior_cpi, 0) = 0
and nvl(months_between(est_complete_date,est_start_date) + 1,0)  < min_est_mos_cpi;

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 9/11: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if					 

return 1
end function

public function longlong of_min_amt ();//**************************************************************************************************************
//This function identifies work orders that need to be flagged as "Failed Minimum Estimate and Actuals Test, No AFUDC Calculated"  afudc_status_id = 6
//                                                                                    and "Failed Minimum Estimate and Actuals Test, No CPI Calculated"  cpi_status_id = 7
//There are two tests that are applied to determine "Failed Minimum Estimate and Actuals Test"
//Minimum dollar amount required in the estimate to calculate AFUDC, if the estimate is less than the minimum but actuals exceed the minimum, AFUDC will begin calculating.  
//PowerPlant System control (WOEST - Header from wo_estimate) determine if the Unit Estimate or Dollar Estimate is used for this option.
//
//The"has_prior_afc" and "has_prior_cpi" columns filter to the work orders that need this test performed.
//Work order with prior afudc and cpi do not need to be evalulated for this test, because they must have passed the test at some point in the past.
//
//These work orders will later get their rates set to 0 to generate $0 afudc or $0 CPI 
//**************************************************************************************************************
longlong nrows

of_msg("	Minimum Estimate and Actuals Tests  " + string(now()),"")

			 
//AFUDC 
//AFUDC_ACT_AMOUNT & WO_EST_AMOUNT are updated in of_afudc_calc_get_est_and_acts() function 
//max_revision is determined earlier in of_afudccalc_wolist()

//work orders that have no estimates and have the afudc_control.est_required = Yes with actuals less than the min_est_amt_afudc
update AFUDC_CALC_WO_LIST_TEMP 
set afudc_status_id = 6 
where afudc_status_id = 1  
and eligible_for_afudc = 1  
and nvl(max_revision,0) = 0 
and nvl(has_prior_afc,0) = 0
and nvl(afudc_act_amount,0) < nvl(min_est_amt_afudc ,0)
and upper(est_required) = 'YES';

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 6: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

//find all other work order that have wo_est_amount < min_est_amt_afudc and afudc_act_amount < min_est_amt_afudc
update AFUDC_CALC_WO_LIST_TEMP 
set afudc_status_id = 6 
where afudc_status_id = 1  
and eligible_for_afudc = 1 
and nvl(has_prior_afc,0) = 0
and nvl(wo_est_amount,0) < nvl(min_est_amt_afudc ,0)
and nvl(afudc_act_amount,0) < nvl(min_est_amt_afudc ,0)
/*and max_revision >= 1   <-- do not filter on revision.  for wos with no estimate, the estimate amount = 0 and compared to the min_est_amt_afudc*/
;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 6 (B1): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

//Maint 7524:  undo afudc_status_id = 6 on work orders with min_afudc_amt = 0 and UPPER(est_required) = 'NO'  and allow_neg_afudc = 1   
update AFUDC_CALC_WO_LIST_TEMP wo
set wo.afudc_status_id = 1 
where wo.afudc_status_id = 6 
and min_est_amt_afudc = 0  
and UPPER(est_required) = 'NO' 
and allow_neg_afudc = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.allow_neg_afudc = 1: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if


//CPI

//CPI_ACT_AMOUNT & WO_EST_AMOUNT is updated in of_afudc_calc_get_est_and_acts() function 
//max_revision is determined earlier in of_afudccalc_wolist()

//work orders that have no estimates and have the afudc_control.est_required = Yes with actuals less than the min_est_amt_cpi
update AFUDC_CALC_WO_LIST_TEMP 
set cpi_status_id = 7
where cpi_status_id = 2 
and eligible_for_cpi = 1  
and nvl(max_revision,0) = 0 
and nvl(has_prior_cpi,0) = 0
and nvl(cpi_act_amount,0) < nvl(min_est_amt_cpi ,0)
and upper(est_required) = 'YES';

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 7: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

//find all other work order that have wo_est_amount < min_est_amt_cpi and afudc_act_amount < min_est_amt_cpi
update AFUDC_CALC_WO_LIST_TEMP 
set cpi_status_id = 7 
where cpi_status_id = 2  
and eligible_for_cpi = 1
and nvl(has_prior_cpi,0) = 0
and nvl(wo_est_amount,0) < nvl(min_est_amt_cpi ,0)
and nvl(cpi_act_amount,0) < nvl(min_est_amt_cpi ,0)
/*and max_revision >= 1   <-- do not filter on revision.  for wos with no estimate, the estimate amount = 0 and compared to the min_est_amt_cpi*/
;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 7 (B1): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

//Maint 7524:  undo cpi_status_id = 7 on work orders with min_cpi_amt = 0 and UPPER(est_required) = 'NO'  and allow_neg_cpi = 1  
update AFUDC_CALC_WO_LIST_TEMP wo
set wo.cpi_status_id = 2 
where wo.cpi_status_id = 7
and min_est_amt_cpi = 0 
and UPPER(est_required) = 'NO' 
and allow_neg_cpi = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.allow_neg_cpi = 1 (cpi): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

return 1
end function

public function longlong of_not_eligible ();//**************************************************************************************************************
//This function identifies work orders that need to be flagged as "Not Eligible for AFUDC"  afudc_status_id = 16
//                                                                                      as "Not Eligible for CPI"  cpi_status_id = 17
//
//These work orders will later get their rates set to 0 to generate $0 afudc or $0 CPI
//*************************************************************************************************************
longlong nrows
of_msg("	Eligibility Updates  " + string(now()),"") 

//AFUDC
update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = 16 
where afudc_status_id = 1
and eligible_for_afudc = 0;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (11A) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 16 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 
 

//CPI
update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = 17 
where cpi_status_id = 2
and eligible_for_cpi = 0;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (11A) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 17 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 


return 1
end function

public function longlong of_status_and_rates ();//*************************************************************************************************** 
//This function updates the rates to 0 for the work orders that have failed specific tests
//  The afudc_status_id and cpi_status_id values are updated from AFUDC_CALC_WO_LIST_TEMP to AFUDC_CALC
//*************************************************************************************************** 
longlong nrows
 
//AFUDC & CPI Status that should not have the rate updated to 0%
//3:  Past In-service Work Order, Adjustments Calculated  <-- rate already made 0%
//13: Current Month In-service Work Order <-- rate adjusted for afudc_control.in_service_option
//24: Current Month Afudc Stop Date Work Order  <-- rate adjusted for afudc_control.in_service_option
 
//AFUDC Status needing rate update to 0%
//4: Failed AFUDC Triggers Test, No AFUDC Calculated
//6: Failed Minimum Estimate and Actuals Test, No AFUDC Calculated
//8: Failed Estimate Time Test, No AFUDC Calculated 
//10: Failed Minumum Estimate and Time Test, No AFUDC Calculated
//14: AFUDC Stop Date Entered, No AFUDC or CPI Calculated
//16: Not Eligible for AFUDC
//19: Failed Idle Months Check for AFUDC, No AFUDC Calculated

//CPI Status needing rate update to 0%
//5: Failed AFUDC Triggers Test, No CPI Calculated 
//7: Failed Minimum Estimate and Actuals Test, No CPI Calculated
//9: Failed Estimate Time Test, No CPI Calculated 
//11: Failed Minumum Estimate and Time Test, No CPI Calculated 
//12: Failed Percent of Estimate for CPI, No CPI Calculated
//14: AFUDC Stop Date Entered, No AFUDC or CPI Calculated
//17: Not Eligible for CPI
//20: Failed Idle Months Check for CPI, No CPI Calculated
//21: CPI De Minimis

of_msg("	Updating Status and Rates " + string(now()),"")

//update the rates to 0% for the work orders that have the status listed above; otherwise, keep the rates as is
update afudc_calc ac
	set (ac.debt_rate, ac.equity_rate, ac.cpi_rate, ac.afudc_status_id, ac.cpi_status_id)
		 = (select decode(x.afudc_status_id, 4, 0, 6, 0, 8, 0, 10, 0, 14, 0, 16, 0, 19, 0, ac.debt_rate),
			decode(x.afudc_status_id, 4, 0, 6, 0, 8, 0, 10, 0, 14, 0, 16, 0, 19, 0, ac.equity_rate),
			decode(x.cpi_status_id, 5, 0, 7, 0, 9, 0, 11, 0, 12, 0, 14, 0, 17, 0, 20, 0, 21, 0, ac.cpi_rate),
			x.afudc_status_id, 
			x.cpi_status_id 
			from AFUDC_CALC_WO_LIST_TEMP x
			where x.work_order_id = ac.work_order_id 
			 )
where ac.month = :i_month 
and exists (
			select 1 from AFUDC_CALC_WO_LIST_TEMP wo
			where ac.work_order_id = wo.work_order_id   
			);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.status & rates : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

of_msg("	Updating Ratio Status " + string(now()),"")
//update for input_afudc_ratio "Invalid Ratio, No Ratio was used" for -999 which was updated during of_afudc_calc_rollforward()

//Maint 36316
update afudc_calc ac
set ac.afudc_status_id = decode(ac.afudc_status_id, 1, 15, 3, 15, 13, 15, 24,15, ac.afudc_status_id), 
input_afudc_ratio = 1
where ac.month = :i_month 
and ac.input_afudc_ratio = -999
and ac.afudc_status_id in (1, 3, 13, 24) 
and exists (
		select 1 from AFUDC_CALC_WO_LIST_TEMP wo
		where ac.work_order_id = wo.work_order_id   
		);	

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.afudc_status_id = 15 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

//update for input_afudc_ratio "Invalid Ratio, No Ratio was used" 
update afudc_calc ac
set ac.cpi_status_id = decode(ac.cpi_status_id, 2, 15, 3, 15, 13, 15, 24, 15, ac.cpi_status_id), 
input_cpi_ratio = 1
where ac.month = :i_month 
and ac.input_cpi_ratio = -999
and ac.cpi_status_id in (2, 3, 13, 24)
and exists (
		select 1 from AFUDC_CALC_WO_LIST_TEMP wo
		where ac.work_order_id = wo.work_order_id   
		);	

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.cpi_status_id = 15 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

//////////////////////////////the two sqls below take longer to run than the one sql (1st sql above) ///////////////////////
//update afudc_calc ac
//	set ac.debt_rate = 0,
//		 ac.equity_rate = 0,  
//		 ac.afudc_status_id = (select x.afudc_status_id from AFUDC_CALC_WO_LIST_TEMP x
//		 							where x.work_order_id = ac.work_order_id
//									 and x.afudc_status_id in (4,6,8,10,14,16,19)  )
//where ac.month = :i_month 
//and ac.afudc_status_id = 1
//and exists (
//			select 1 from AFUDC_CALC_WO_LIST_TEMP wo
//			where ac.work_order_id = wo.work_order_id  
//			and wo.afudc_status_id  in (4,6,8,10,14,16,19)  
//			);	
//			
//update afudc_calc ac
//set ac.afudc_status_id = decode(ac.afudc_status_id, 1, 15, 3, 15, 13, 15, 24,15, ac.afudc_status_id), 
//input_afudc_ratio = 1
//where ac.month = :i_month 
//and ac.input_afudc_ratio = -999
//and ac.afudc_status_id in (2, 3, 13, 24)
//and exists (
//		select 1 from AFUDC_CALC_WO_LIST_TEMP wo
//		where ac.work_order_id = wo.work_order_id   
//		);	
//////////////////////////////the two sqls below take longer to run than the one sql (1st sql above) ///////////////////////

 
return 1
end function

public function longlong of_triggers ();//**************************************************************************************************************
//This function identifies work orders that need to be flagged as "Failed AFUDC Triggers Test, No AFUDC Calculated"  afudc_status_id = 4
//                                                                                    and "Failed AFUDC Triggers Test, No CPI Calculated"  cpi_status_id = 5 
//The skip_trigger is a Yes/No (1/0) option that indicates if charge type triggers are checked for AFUDC for a specific AFUDC type.  
//The"has_prior_afc" and "has_prior_cpi" columns filter to the work orders that need this test performed.
//Work order with prior afudc and cpi do not need to be evalulated for this test, because they must have passed the test at some point in the past.
//
//  These work orders will later get their rates set to 0 to generate $0 afudc or $0 CPI 
//**************************************************************************************************************
longlong nrows

// If no prior AFC and skip_triggers <> 1 and triggers on charge_type and records in wo_summary for triggers on charge_type
//Triggers Test:  Determine if work orders should get afudc calculated based on trigger data configurations and charge activity
longlong afudc_triggers, cpi_triggers, afudc_skip_triggers_distinct, cpi_skip_triggers_distinct, min_afudc_skip_all, min_cpi_skip_all
boolean  afudc_trigger_test_needed, cpi_trigger_test_needed


afudc_trigger_test_needed = true
cpi_trigger_test_needed = true
afudc_triggers = 0
cpi_triggers = 0
afudc_skip_triggers_distinct = 0
cpi_skip_triggers_distinct = 0

//Let's figure out if any Trigger testing needs to be done
select count(distinct skip_trigger), min(skip_trigger), count(distinct cpi_skip_trigger), min(cpi_skip_trigger)
into :afudc_skip_triggers_distinct, :min_afudc_skip_all, :cpi_skip_triggers_distinct, :min_cpi_skip_all
from AFUDC_CALC_WO_LIST_TEMP; 

//if afudc_skip_triggers_distinct is greater than 1 then, there is mix of work orders needing to go through triggers test and others that do not
if afudc_skip_triggers_distinct = 1 and min_afudc_skip_all = 1 then
	//all of the work orders have skip trigger for afudc set to Yes, which means no testing needed
	//if min(skip_trigger) were = 0, then that would mean all of of work orders have skip trigger for afudc set to No and would need to go through the testing
	afudc_trigger_test_needed = false
end if  

//if cpi_skip_triggers_distinct is greater than 1 then, there is mix of work orders needing to go through triggers test and others that do not
if cpi_skip_triggers_distinct = 1 and min_cpi_skip_all = 1 then
	//all of the work orders have skip trigger for cpi set to Yes, which means no testing needed
	//if min(cpi_skip_trigger) were = 0, then that would mean all of of work orders have skip trigger for cpi set to No and would need to go through the testing
	cpi_trigger_test_needed = false
end if  

if afudc_trigger_test_needed then 
	select count(1) into :afudc_triggers 
	from charge_type where trigger_for_afudc = 1;
end if 

if cpi_trigger_test_needed then 
	select count(1) into :cpi_triggers 
	from charge_type where trigger_for_cpi = 1;
end if 

//AFUDC
if afudc_triggers > 0 and afudc_trigger_test_needed then
	
	of_msg("	Trigger Afudc Tests  " + string(now()),"...")
	
	update AFUDC_CALC_WO_LIST_TEMP wo
	set wo.afudc_status_id = 4 
	where wo.afudc_status_id = 1 
	and nvl(wo.HAS_PRIOR_AFC, 0) = 0		
	and wo.skip_trigger = 0  
	and wo.eligible_for_afudc = 1 
	and not exists (
				select 1 from WO_SUMMARY ws, CHARGE_TYPE ct
				where ws.work_order_id = wo.work_order_id
				and ct.charge_type_id = ws.charge_type_id
				and ws.month_number <= :i_month_number
				and ct.trigger_for_afudc = 1
				and rownum = 1
					);

	nrows = sqlca.sqlnrows
					
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 4: " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if   
		
end if 

// CPI
if cpi_triggers > 0 and cpi_trigger_test_needed then
	
	of_msg("	Trigger CPI Tests  " + string(now()),"...")

	update AFUDC_CALC_WO_LIST_TEMP wo
	set wo.cpi_status_id = 5 
	where wo.cpi_status_id = 2  
	and nvl(wo.HAS_PRIOR_CPI, 0) = 0		
	and wo.cpi_skip_trigger = 0 
	and wo.eligible_for_cpi = 1 
	and not exists (
				select 1 from WO_SUMMARY ws, CHARGE_TYPE ct
				where ws.work_order_id = wo.work_order_id
				and ct.charge_type_id = ws.charge_type_id
				and ws.month_number <= :i_month_number
				and ct.trigger_for_cpi = 1
				and rownum = 1
					);

	nrows = sqlca.sqlnrows
					
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 5: " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if     
	
end if   

return 1
end function

public function longlong of_stop_date_month ();//*****************************************************************************************************************************
//This function handles updates for work orders that have an in_serve_date 
//1. for afudc stop date = current month, need to adjust the rates  "Current Month Afudc Stop Date Work Order" afudc_status_id = 24 and cpi_status_id = 24
//2. for afudc stop_date = current month, need to adjust the rates
//******************************************************************************************************************************
longlong nrows
of_msg("	Afudc Stop Month Updates  " + string(now()),"")

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// identify work orders where afudc_stop_date = current month
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = 24
where nvl(to_number(to_char(afudc_stop_date,'YYYYMM')),'999999') = :i_month_number 
and afudc_status_id = 1
and eligible_for_afudc = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_stop_date = 24: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = 24 
where nvl(to_number(to_char(afudc_stop_date,'YYYYMM')),'999999') = :i_month_number 
and cpi_status_id = 2
and eligible_for_cpi = 1;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 24: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// identify work orders where afudc stop date is equal to the current month (use the rules that apply to IN_SERVICE_OPTION)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//AFUDC 
update afudc_calc ac 
set (ac.afudc_status_id, ac.debt_rate, ac.equity_rate) =  
						(select wo.afudc_status_id, wo.debt_rate*stop_month_rate_adj, wo.equity_rate*stop_month_rate_adj
						from AFUDC_CALC_WO_LIST_TEMP wo
						where wo.work_order_id = ac.work_order_id
						and nvl(to_number(to_char(wo.afudc_stop_date,'YYYYMM')),'999999') = :i_month_number
						and wo.afudc_status_id = 24
						and wo.eligible_for_afudc = 1)
where to_number(to_char(ac.month,'YYYYMM')) = :i_month_number
and ac.afudc_status_id = 1
and exists (
	select 1 from AFUDC_CALC_WO_LIST_TEMP w
	where ac.work_order_id = w.work_order_id  
	and nvl(to_number(to_char(w.afudc_stop_date,'YYYYMM')),'999999') = :i_month_number
	and w.afudc_status_id = 24
	and w.eligible_for_afudc = 1
	);	
	
nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.debt_rate & equity_rate  for stop date = month: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

//CPI 
update afudc_calc ac
set (ac.cpi_status_id, ac.cpi_rate) = 
						(select wo.cpi_status_id, wo.cpi_rate*stop_month_rate_adj from AFUDC_CALC_WO_LIST_TEMP wo
						where ac.work_order_id = wo.work_order_id
						and nvl(to_number(to_char(wo.afudc_stop_date,'YYYYMM')),'999999') = :i_month_number
						and wo.cpi_status_id = 24
						and wo.eligible_for_cpi = 1)
where to_number(to_char(month,'YYYYMM')) = :i_month_number
and ac.cpi_status_id = 2
and exists (
	select 1 from AFUDC_CALC_WO_LIST_TEMP w
	where ac.work_order_id = w.work_order_id
	and nvl(to_number(to_char(w.afudc_stop_date,'YYYYMM')),'999999') = :i_month_number
	and w.cpi_status_id = 24
	and w.eligible_for_cpi = 1
	);	
	
nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.cpi_rate for stop date = month: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

return 1
end function

public function longlong of_wolist ();//******************************************************************************************
//This function identifies the list of work orders that will be incluced in the afudc/cpi calculation
//The sql is taken from dw_wo_charges_summ_all
//Additional updates are performed to stage the data for the various afudc/cpi tests performed to determine calc eligibility
//******************************************************************************************
longlong nrows
of_msg("	Gathering list of work orders to calculate  " + string(now()),"") 

//Clear out the global temp table
delete from AFUDC_CALC_WO_LIST_TEMP;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then 
	of_msg("ERROR: Deleting from AFUDC_CALC_WO_LIST_TEMP: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//Insert useful information used in the afudc and cpi calculation into this global temp table
//There are 3 afudc controls that are stored as VARCHAR2: in_service_option, est_required, cpi_de_minimis_wo_or_fp

if i_cap_int and i_cap_int_parent then

	//need to use a different sql join for parent cap int companies
	insert into AFUDC_CALC_WO_LIST_TEMP (work_order_id,  
	company_id,  
	afudc_type_id,   
	afudc_elig_amount,
	cpi_elig_amount,
	afudc_start_date,
	afudc_stop_date,   
	in_service_date,
	wo_status_id,
	department_id,
	bus_segment_id,
	cwip_gl_account,
	eligible_for_afudc,
	eligible_for_cpi,
	original_in_service_date,
	orig_isd_month_number,
	est_start_date,
	est_complete_date,  
	est_in_service_date,
	funding_wo_id,
	current_revision,
	equity_cost_element,
	debt_cost_element,
	cpi_cost_element,
	min_est_amt_afudc,
	min_est_amt_cpi,
	min_est_mos_afudc,
	min_est_mos_cpi,
	allow_neg_cpi,
	allow_neg_afudc,
	percent_est_for_cpi,
	ignore_current_month,  
	ignore_current_month_cpi,
	current_month_only,
	est_required,  //VARCHAR2
	in_service_option,  //VARCHAR2
	skip_trigger,
	cpi_skip_trigger, 
	idle_afudc_months,
	idle_cpi_months,
	idle_exp_type_addition,
	cpi_de_minimis_yn,
	cpi_de_minimis_wo_or_fp, //VARCHAR2
	cpi_de_minimis_amount, 
	cpi_de_minimis_max_days,
	min_charge_monum,
	debt_charge_type,
	equity_charge_type,
	cpi_charge_type
	)
	
	select woc.work_order_id work_order_id,   
	woc.company_id company_id,   
	woa.afudc_type_id afudc_type_id,    
	0 afudc_elig_amount,   
	0 cpi_elig_amount,   
	woa.afudc_start_date afudc_start_date,
	woa.afudc_stop_date afudc_stop_date,   
	woc.in_service_date in_service_date,
	woc.wo_status_id wo_status_id,
	woc.department_id department_id,
	woc.bus_segment_id bus_segment_id,
	woa.cwip_gl_account cwip_gl_account,
	nvl(woa.eligible_for_afudc,0) eligible_for_afudc,
	nvl(woa.eligible_for_cpi,0) eligible_for_cpi,
	woc.original_in_service_date original_in_service_date, 
	woc.orig_isd_month_number orig_isd_month_number,
	woc.est_start_date,
	woc.est_complete_date, 
	woc.est_in_service_date,
	woc.funding_wo_id,
	woc.current_revision,
	ac.equity_cost_element,
	ac.debt_cost_element, 
	ac.cpi_cost_element,
	nvl(min_est_amt_afudc,0),
	nvl(min_est_amt_cpi,0),
	nvl(min_est_mos_afudc,0), 
	nvl(min_est_mos_cpi,0), 
	nvl(allow_neg_cpi,0),
	nvl(allow_neg_afudc,0), 
	nvl(percent_est_for_cpi,0), 
	nvl(ignore_current_month, .5),
	nvl(ignore_current_month_cpi, .5), 
	nvl(current_month_only, 0),
	UPPER(nvl(est_required,'NO')),  //VARCHAR2
	UPPER(nvl(in_service_option,'HALF')),  //VARCHAR2
	nvl(skip_trigger,0), 
	nvl(cpi_skip_trigger,0), 
	nvl(idle_afudc_months, 0),
	nvl(idle_cpi_months, 0), 
	nvl(idle_exp_type_addition, 0),
	nvl(cpi_de_minimis_yn,0), 
	UPPER(trim(cpi_de_minimis_wo_or_fp)),  //VARCHAR2
	cpi_de_minimis_amount, 
	nvl(cpi_de_minimis_max_days,9999999),
	null,  /*min_charge_monum*/
	ce_debt.charge_type_id,  
	ce_equity.charge_type_id, 
	ce_cpi.charge_type_id 
	from cap_interest_wo parent_wos, work_order_account woa, work_order_control woc, afudc_control ac , 
	cost_element ce_debt, cost_element ce_equity, cost_element ce_cpi
	where parent_wos.parent_wo = woc.work_order_id
	and woc.work_order_id = woa.work_order_id 
	and woa.afudc_type_id = ac.afudc_type_id 
	and ce_debt.cost_element_id = ac.debt_cost_element
	and ce_equity.cost_element_id = ac.equity_cost_element
	and ce_cpi.cost_element_id = ac.cpi_cost_element 
	and woc.company_id = :i_company_id 	
	and (woa.eligible_for_afudc = 1 or woa.eligible_for_cpi = 1)
	
	group by woc.work_order_id, woa.afudc_start_date, woa.afudc_stop_date, woa.afudc_type_id,  
	woc.company_id, woc.in_service_date, woc.wo_status_id, woc.department_id, woc.bus_segment_id, woa.cwip_gl_account, woa.eligible_for_afudc,
	woa.eligible_for_cpi, woc.original_in_service_date, woc.orig_isd_month_number  ,	woc.est_start_date,	woc.est_complete_date,  
	woc.est_in_service_date, woc.funding_wo_id, woc.current_revision,
	ac.equity_cost_element,
	ac.debt_cost_element, 
	ac.cpi_cost_element,
	nvl(min_est_amt_afudc,0),
	nvl(min_est_amt_cpi,0),
	nvl(min_est_mos_afudc,0), 
	nvl(min_est_mos_cpi,0), 
	nvl(allow_neg_cpi,0),
	nvl(allow_neg_afudc,0), 
	nvl(percent_est_for_cpi,0), 
	nvl(ignore_current_month, .5),
	nvl(ignore_current_month_cpi, .5),
	nvl(current_month_only, 0), 
	UPPER(nvl(est_required,'NO')),
	UPPER(nvl(in_service_option,'HALF')),
	nvl(skip_trigger,0), 
	nvl(cpi_skip_trigger,0), 
	nvl(idle_afudc_months, 0),
	nvl(idle_cpi_months, 0), 
	nvl(idle_exp_type_addition, 0),
	nvl(cpi_de_minimis_yn,0), 
	UPPER(trim(cpi_de_minimis_wo_or_fp)), 
	cpi_de_minimis_amount, 
	nvl(cpi_de_minimis_max_days,9999999),		
	ce_debt.charge_type_id,  
	ce_equity.charge_type_id, 
	ce_cpi.charge_type_id;
	
	nrows = sqlca.sqlnrows
				
	if sqlca.sqlcode < 0 then 
		of_msg("ERROR: Insert into AFUDC_CALC_WO_LIST_TEMP for Cap Int: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1 
	end if  
	
	of_msg("	  - Inserted " + string(nrows) + " into AFUDC_CALC_WO_LIST_TEMP  " + string(now()),"")  
	
	//need to replace the afudc_elig_amount for Cap Int Parent work orders with the afudc_elig_amount belonging to the Cap Int Child work orders
	update AFUDC_CALC_WO_LIST_TEMP a
	set a.afudc_elig_amount = (select sum(w.wo_amount*c.afudc_elig_indicator)
										from wo_summary w, charge_type_data c,  cap_interest_wo i
										where w.charge_type_id = c.charge_type_id
										and w.work_order_id = i.work_order_id
										and a.work_order_id = i.parent_wo
										and w.month_number = :i_month_number
										and w.expenditure_type_id = 1
										and w.charge_type_id not in (a.debt_charge_type,a.equity_charge_type,a.cpi_charge_type) 										
										); 
	
	nrows = sqlca.sqlnrows
				
	if sqlca.sqlcode < 0 then 
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.afudc_elig_amount for Cap Int: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1 
	end if  
					
else
	
	//for non-cap int companies and child cap int companies
	insert into AFUDC_CALC_WO_LIST_TEMP (work_order_id,  
	company_id,  
	afudc_type_id,   
	afudc_elig_amount,
	cpi_elig_amount,
	afudc_start_date,
	afudc_stop_date,   
	in_service_date,
	wo_status_id,
	department_id,
	bus_segment_id,
	cwip_gl_account,
	eligible_for_afudc,
	eligible_for_cpi,
	original_in_service_date,
	orig_isd_month_number,
	est_start_date,
	est_complete_date,  
	est_in_service_date,
	funding_wo_id,
	current_revision,
	equity_cost_element,
	debt_cost_element,
	cpi_cost_element,
	min_est_amt_afudc,
	min_est_amt_cpi,
	min_est_mos_afudc,
	min_est_mos_cpi,
	allow_neg_cpi,
	allow_neg_afudc,
	percent_est_for_cpi,
	ignore_current_month,  
	ignore_current_month_cpi,
	current_month_only,
	est_required,  //VARCHAR2
	in_service_option,  //VARCHAR2
	skip_trigger,
	cpi_skip_trigger, 
	idle_afudc_months,
	idle_cpi_months,
	idle_exp_type_addition,
	cpi_de_minimis_yn,
	cpi_de_minimis_wo_or_fp, //VARCHAR2
	cpi_de_minimis_amount, 
	cpi_de_minimis_max_days,
	min_charge_monum,
	debt_charge_type,
	equity_charge_type,
	cpi_charge_type
	)
	
	select woc.work_order_id work_order_id,   
	woc.company_id company_id,   
	woa.afudc_type_id afudc_type_id,    
	sum(decode(ws.month_number, :i_month_number, ctd.afudc_elig_indicator * ws.wo_amount, 0)) afudc_elig_amount,   
	sum(decode(ws.month_number, :i_month_number, ctd.cpi_elig_indicator * ws.wo_amount,0)) cpi_elig_amount,   
	woa.afudc_start_date afudc_start_date,
	woa.afudc_stop_date afudc_stop_date,   
	woc.in_service_date in_service_date,
	woc.wo_status_id wo_status_id,
	woc.department_id department_id,
	woc.bus_segment_id bus_segment_id,
	woa.cwip_gl_account cwip_gl_account,
	nvl(woa.eligible_for_afudc,0) eligible_for_afudc,
	nvl(woa.eligible_for_cpi,0) eligible_for_cpi,
	woc.original_in_service_date original_in_service_date, 
	woc.orig_isd_month_number orig_isd_month_number,
	woc.est_start_date,
	woc.est_complete_date, 
	woc.est_in_service_date,
	woc.funding_wo_id,
	woc.current_revision,
	ac.equity_cost_element,
	ac.debt_cost_element, 
	ac.cpi_cost_element,
	nvl(min_est_amt_afudc,0),
	nvl(min_est_amt_cpi,0),
	nvl(min_est_mos_afudc,0), 
	nvl(min_est_mos_cpi,0), 
	nvl(allow_neg_cpi,0),
	nvl(allow_neg_afudc,0), 
	nvl(percent_est_for_cpi,0), 
	nvl(ignore_current_month, .5),
	nvl(ignore_current_month_cpi, .5), 
	nvl(current_month_only, 0),
	UPPER(nvl(est_required,'NO')),  //VARCHAR2
	UPPER(nvl(in_service_option,'HALF')),  //VARCHAR2
	nvl(skip_trigger,0), 
	nvl(cpi_skip_trigger,0), 
	nvl(idle_afudc_months, 0),
	nvl(idle_cpi_months, 0), 
	nvl(idle_exp_type_addition, 0),
	nvl(cpi_de_minimis_yn,0), 
	UPPER(trim(cpi_de_minimis_wo_or_fp)),  //VARCHAR2
	cpi_de_minimis_amount, 
	nvl(cpi_de_minimis_max_days,9999999),
	min(ws.month_number),
	ce_debt.charge_type_id,  
	ce_equity.charge_type_id, 
	ce_cpi.charge_type_id 
	from charge_type_data ctd, wo_summary ws, work_order_account woa, work_order_control woc, afudc_control ac , 
	cost_element ce_debt, cost_element ce_equity, cost_element ce_cpi
	where ctd.charge_type_id = ws.charge_type_id
	and ws.work_order_id = woa.work_order_id
	and woc.work_order_id = woa.work_order_id 
	and woa.afudc_type_id = ac.afudc_type_id 
	and ce_debt.cost_element_id = ac.debt_cost_element
	and ce_equity.cost_element_id = ac.equity_cost_element
	and ce_cpi.cost_element_id = ac.cpi_cost_element
	and ws.expenditure_type_id = :i_expenditure_type_id
	and ws.month_number <= :i_month_number 
	and woc.company_id = :i_company_id
	and ws.charge_type_id not in (ce_debt.charge_type_id,ce_equity.charge_type_id,ce_cpi.charge_type_id) 
	and (woa.eligible_for_afudc = 1
		  or
		  woa.eligible_for_cpi = 1
		  or
		  exists (
				select 1 from afudc_input ai
				 where ai.work_order_id = woc.work_order_id
					and to_number(to_char(ai.month,'YYYYMM')) = :i_month_number)
		 )
	and ((woc.wo_status_id not in (6,7,8)
			or
			exists (
				select 1 from afudc_input ai
				 where ai.work_order_id = woc.work_order_id
					and to_number(to_char(ai.month,'YYYYMM')) = :i_month_number)
		  or
		  (woc.wo_status_id in (6,7,8)  
			and 
			nvl(woc.orig_isd_month_number, :i_month_number) = :i_month_number)
			and
			to_number(to_char(woc.in_service_date,'YYYYMM')) <= :i_month_number)
		 )
	and (woc.original_in_service_date is null 
		  or
		  to_char(woc.original_in_service_date, 'YYYYMM') > to_char(woc.in_service_date, 'YYYYMM') /*maint-38594*/
		  or
		  woc.orig_isd_month_number = :i_month_number
		  or
		  exists (
				select 1 from afudc_input ai
				 where ai.work_order_id = woc.work_order_id
					and to_number(to_char(ai.month,'YYYYMM')) = :i_month_number)
		 )
		 
	and nvl(to_number(to_char(woa.afudc_start_date,'YYYYMM')),:i_month_number) <= :i_month_number 
	
	group by woc.work_order_id, woa.afudc_start_date, woa.afudc_stop_date, woa.afudc_type_id,  
	woc.company_id, woc.in_service_date, woc.wo_status_id, woc.department_id, woc.bus_segment_id, woa.cwip_gl_account, woa.eligible_for_afudc,
	woa.eligible_for_cpi, woc.original_in_service_date, woc.orig_isd_month_number  ,	woc.est_start_date,	woc.est_complete_date,  
	woc.est_in_service_date, woc.funding_wo_id, woc.current_revision,
	ac.equity_cost_element,
	ac.debt_cost_element, 
	ac.cpi_cost_element,
	nvl(min_est_amt_afudc,0),
	nvl(min_est_amt_cpi,0),
	nvl(min_est_mos_afudc,0), 
	nvl(min_est_mos_cpi,0), 
	nvl(allow_neg_cpi,0),
	nvl(allow_neg_afudc,0), 
	nvl(percent_est_for_cpi,0), 
	nvl(ignore_current_month, .5),
	nvl(ignore_current_month_cpi, .5),
	nvl(current_month_only, 0), 
	UPPER(nvl(est_required,'NO')),
	UPPER(nvl(in_service_option,'HALF')),
	nvl(skip_trigger,0), 
	nvl(cpi_skip_trigger,0), 
	nvl(idle_afudc_months, 0),
	nvl(idle_cpi_months, 0), 
	nvl(idle_exp_type_addition, 0),
	nvl(cpi_de_minimis_yn,0), 
	UPPER(trim(cpi_de_minimis_wo_or_fp)), 
	cpi_de_minimis_amount, 
	nvl(cpi_de_minimis_max_days,9999999),		
	ce_debt.charge_type_id,  
	ce_equity.charge_type_id, 
	ce_cpi.charge_type_id;
			
	nrows = sqlca.sqlnrows
	
	if sqlca.sqlcode < 0 then 
		of_msg("ERROR: Inserting into AFUDC_CALC_WO_LIST_TEMP: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if  
	
	of_msg("	  - Inserted " + string(nrows) + " into AFUDC_CALC_WO_LIST_TEMP  " + string(now()),"")  
end if 




//Update STOP_MONTH_RATE_ADJ (aka half_month in old code)
//Maint 34881
update AFUDC_CALC_WO_LIST_TEMP
set STOP_MONTH_RATE_ADJ = decode(IN_SERVICE_OPTION, 'HALF', .5, 'FULL', 1, 'WHOLE', 1, 'OLD', 1, 'NONE', 0, 'DAY', 
			round((to_number(to_char(nvl(in_service_date, afudc_stop_date), 'dd') -1))/to_number(to_char(last_day(nvl(in_service_date, afudc_stop_date)), 'dd')) , 8), .5)
where to_number(to_char(afudc_stop_date,'YYYYMM')) = :i_month_number
or to_number(to_char(in_service_date,'YYYYMM')) <= :i_month_number;

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then 
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.STOP_MONTH_RATE_ADJ: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if  

	
return 1
end function

public function longlong of_cpi_deminimis ();//**************************************************************************************************************
//This function identifies work orders that need to be flagged as "CPI De Minimis"  cpi_status_id = 21 
//CPI de Minimis WO or FP:  Apply the de minimus test to the $$HEX1$$1c20$$ENDHEX$$FP$$HEX2$$1d202000$$ENDHEX$$(Funding Project), $$HEX1$$1c20$$ENDHEX$$WO$$HEX2$$1d202000$$ENDHEX$$(Work Order), or $$HEX1$$1c20$$ENDHEX$$WO Then FP$$HEX2$$1d202000$$ENDHEX$$(i.e. if it does not find a work order for that date, apply it to the funding project).
//CPI de Minimis Max Days:  Maximum days getting de minimus treatment. I.e. 90 day (esimtated stop date $$HEX2$$13202000$$ENDHEX$$estimated start date).
//CPI de Minimis Amount:  The dollar amount used in the test, i.e. estimate book dollars $$HEX2$$64222000$$ENDHEX$$cpi deminimus dollar amount / est. project days maximum days getting de minimus treatment
//
//  These work orders will later get their rates set to 0 to generate $0 CPI 
//*************************************************************************************************************
longlong nrows, check_count 
of_msg("	CPI DeMinimis Tests  " + string(now()),"")

// CPI Only 

//check if the CPI de Minimis testing is being used for the set of work orders being calculated 
check_count = 0 
select count(1) into :check_count
from AFUDC_CALC_WO_LIST_TEMP
where CPI_DE_MINIMIS_YN = 1
and cpi_status_id = 2
and eligible_for_cpi = 1  
and cpi_de_minimis_wo_or_fp in ('FP', 'WO THEN FP', 'WO')
and rownum = 1;

if check_count > 0 then
	
	//first update CPI_DEMIN_EST_DAYS based on work_order_id
	update AFUDC_CALC_WO_LIST_TEMP 
	set CPI_DEMIN_EST_DAYS =  est_in_service_date - est_start_date 
	where CPI_DE_MINIMIS_YN = 1
	and est_in_service_date is not null
	and est_start_date is not null
	and est_in_service_date >= est_start_date
	and cpi_de_minimis_wo_or_fp in ('WO', 'WO THEN FP')
	and cpi_status_id = 2
	and eligible_for_cpi = 1  ;

	nrows = sqlca.sqlnrows
		
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.CPI_DEMIN_EST_DAYS(1): " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if		
	
	//next update CPI_DEMIN_EST_DAYS based on funding_wo_id, including the ones where the update above did not update CPI_DEMIN_EST_DAYS
	update AFUDC_CALC_WO_LIST_TEMP a
	set a.CPI_DEMIN_EST_DAYS =  (select b.est_in_service_date - b.est_start_date
											from work_order_control b
											where a.funding_wo_id = b.work_order_id
											and b.funding_wo_indicator = 1
											and b.est_in_service_date is not null
											and b.est_start_date is not null
											and b.est_in_service_date >= b.est_start_date)
	where a.CPI_DE_MINIMIS_YN = 1
	and a.CPI_DEMIN_EST_DAYS is null 
	and a.cpi_de_minimis_wo_or_fp in ('FP', 'WO THEN FP')
	and cpi_status_id = 2
	and eligible_for_cpi = 1  ;

	nrows = sqlca.sqlnrows
		
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.CPI_DEMIN_EST_DAYS(2): " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if		
	
	//last update CPI_DEMIN_EST_DAYS = 0 if both the updates above did not 
	update AFUDC_CALC_WO_LIST_TEMP 
	set CPI_DEMIN_EST_DAYS =  0
	where CPI_DE_MINIMIS_YN = 1
	and CPI_DEMIN_EST_DAYS is null  
	and cpi_status_id = 2
	and eligible_for_cpi = 1  
	and cpi_de_minimis_wo_or_fp in ('FP', 'WO THEN FP', 'WO');

	nrows = sqlca.sqlnrows
		
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.CPI_DEMIN_EST_DAYS(0): " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if		
	
	//compare the CPI_DEMIN_EST_DAYS to the CPI_DE_MINIMIS_MAX_DAYS and check if the CPI de Minimis testing needs to continue
	check_count = 0 
	select count(1) into :check_count
	from AFUDC_CALC_WO_LIST_TEMP
	where CPI_DE_MINIMIS_YN = 1
	and cpi_status_id = 2
	and eligible_for_cpi = 1  
	and CPI_DEMIN_EST_DAYS >= 1 
	and CPI_DEMIN_EST_DAYS <= cpi_de_minimis_max_days
	and cpi_de_minimis_wo_or_fp in ('FP', 'WO THEN FP', 'WO')
	and rownum = 1; 
	
	if check_count > 0 then
		//first update CPI_DEMIN_BUD_AMOUNT based on work_order_id
		update AFUDC_CALC_WO_LIST_TEMP a
		set a.CPI_DEMIN_BUD_AMOUNT =  (select   sum( e.total * nvl(ect.percent_tax_basis,1) ) 
												from wo_est_monthly e, estimate_charge_type ect 
												where e.est_chg_type_id = ect.est_chg_type_id
													and e.expenditure_type_id = 1
													and e.revision = a.current_revision
													and e.work_order_id = a.work_order_id
													group by e.work_order_id)
		where a.CPI_DE_MINIMIS_YN = 1
		and CPI_DEMIN_EST_DAYS >= 1 
		and CPI_DEMIN_EST_DAYS <= cpi_de_minimis_max_days
		and a.cpi_de_minimis_wo_or_fp in ('WO', 'WO THEN FP')
		and cpi_status_id = 2
		and eligible_for_cpi = 1  ;

		nrows = sqlca.sqlnrows
		
		if sqlca.sqlcode < 0 then
			of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.CPI_DEMIN_BUD_AMOUNT(1): " + &
					 sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			return -1
		end if		
		
		//first update CPI_DEMIN_BUD_AMOUNT based on funding_wo_id, including the ones where the update above did not update CPI_DEMIN_BUD_AMOUNT
		update AFUDC_CALC_WO_LIST_TEMP a
		set a.CPI_DEMIN_BUD_AMOUNT =  (select   sum( e.total * nvl(ect.percent_tax_basis,1) ) 
												from wo_est_monthly e, estimate_charge_type ect, work_order_control w 
												where e.est_chg_type_id = ect.est_chg_type_id
													and e.expenditure_type_id = 1
													and e.revision = w.current_revision
													and a.funding_wo_id = w.work_order_id
													and e.work_order_id = w.work_order_id
													and w.funding_wo_indicator = 1
													group by e.work_order_id)
		where a.CPI_DE_MINIMIS_YN = 1
		and CPI_DEMIN_EST_DAYS >= 1 
		and CPI_DEMIN_EST_DAYS <= cpi_de_minimis_max_days
		and a.cpi_de_minimis_wo_or_fp in ('FP', 'WO THEN FP')
		and cpi_status_id = 2
		and eligible_for_cpi = 1  ;

		nrows = sqlca.sqlnrows
		
		if sqlca.sqlcode < 0 then
			of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.CPI_DEMIN_BUD_AMOUNT(2): " + &
					 sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			return -1
		end if		
		
		//the last and final test relies on the information updated above to determine if the test matches cpi_status_id = 21
		update AFUDC_CALC_WO_LIST_TEMP wo
		set cpi_status_id = 21 
		where cpi_status_id = 2
		and CPI_DE_MINIMIS_YN = 1
		and CPI_DEMIN_EST_DAYS >= 1 
		and CPI_DEMIN_EST_DAYS <= cpi_de_minimis_max_days
		AND CPI_DEMIN_BUD_AMOUNT <= cpi_de_minimis_amount / CPI_DEMIN_EST_DAYS  
		and cpi_de_minimis_wo_or_fp in ('FP', 'WO THEN FP', 'WO') 
		and eligible_for_cpi = 1  ; 

		nrows = sqlca.sqlnrows
		
		if sqlca.sqlcode < 0 then
			of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 21: " + &
					 sqlca.SQLErrText,"ERROR")
			rollback;
			i_afudc_error = true
			return -1
		end if 
		
	end if 
	
end if  //if check_count > 0

return 1
end function

public function longlong of_pct_cpi ();//**************************************************************************************************************
//This function identifies work orders that need to be flagged as "Failed Percent of Estimate for CPI, No CPI Calculated"  cpi_status_id = 12 
//The percent_est_for_cpi is an Option to restrict start of CPI until a percentage of the total estimate is reached. 
//Example: Estimate of $1000 and the limit is 5%, then CPI will not begin until there is at least $50 in actual charges.
//
// The  "has_prior_cpi" column filter to the work orders that need this test performed.
// Work order with prior cpi do not need to be evalulated for this test, because they must have passed the test at some point in the past.
//
//  These work orders will later get their rates set to 0 to generate $0 CPI 
//**************************************************************************************************************
longlong nrows
of_msg("	Percent CPI Est Test  " + string(now()),"...")

// CPI only
 
// CPI_ACT_AMOUNT & WO_EST_AMOUNT is updated in of_afudc_calc_get_est_and_acts() function 
update AFUDC_CALC_WO_LIST_TEMP 
set cpi_status_id = 12
where cpi_status_id = 2 
and eligible_for_cpi = 1 
and nvl(has_prior_cpi, 0) = 0
and PERCENT_EST_FOR_CPI > 0
and nvl(CPI_ACT_AMOUNT,0) < PERCENT_EST_FOR_CPI*nvl(WO_EST_AMOUNT,0);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 12 (B1): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

return 1
end function

public function longlong of_rollforward_afudc_calc ();//*************************************************************************************************** 
// This function inserts the afudc_calc records for the current month, using the data from last month's records
//*************************************************************************************************** 
longlong nrows
of_msg("	Inserting into AFUDC_CALC  " + string(now()),"...")  

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//this insert into afudc_calc handles the following:
//1. afudc_input table records
//2. afudc_input_ratio table records
//3. beg_afudc_base and beg_cpi_base <-- obey current_month_only
//4. half_current_month_afc and half_current_month_cpi calc
//5. rates from afudc_data table records
//6. cm_charges_in_calc_afc and cm_charges_in_calc_cpi
//7. afudc_compound_amt and cpi_compound_amt   <-- obey current_month_only & in this insert, these values are temporarily the afudc_compound_base_amts from last month to be used later
//8. afudc_je_equity and cpi_je   <-- hijack these values to temporarily save the afudc and cpi compound indicators (0 or1) from AFUDC_DATA to be used later
//9. this will insert the current month record even when there is no prior month record in afudc_calc table
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
insert into afudc_calc (
	work_order_id, month, afudc_base, cpi_base, cpi_retro,
	half_current_month_afc, half_current_month_cpi, afudc_equity, afudc_debt, cpi, 
	afudc_compound_amt, cpi_compound_amt, beg_afudc_base, beg_cpi_base, 
	input_afudc_debt, input_afudc_equity, input_cpi, 
	afudc_base_used_in_calc, cpi_base_used_in_calc, input_afudc_ratio, input_cpi_ratio,
	afudc_status_id, cpi_status_id, debt_rate, equity_rate, cpi_rate,
	afudc_je_debt ,afudc_je_equity ,cpi_je ,afudc_isd_debt ,afudc_isd_equity ,cpi_isd ,
	cm_charges_in_calc_afc,cm_charges_in_calc_cpi)
select x.work_order_id work_order_id,
		 :i_month,
		 0 afudc_base,
		 0 cpi_base,
		 0 cpi_retro,
		 x.afudc_elig_amount * x.ignore_current_month half_current_month_afc,
		 x.cpi_elig_amount * x.ignore_current_month_cpi half_current_month_cpi,
		 0 afudc_equity,
		 0 afudc_debt,
		 0 cpi,
		 nvl(decode(nvl(x.current_month_only,0),1,0,calc.afudc_compound_amt),0) afudc_compound_amt,
		 nvl(decode(nvl(x.current_month_only,0),1,0,calc.cpi_compound_amt),0) cpi_compound_amt,
		 nvl(decode(nvl(x.current_month_only,0),1,0,calc.afudc_base),0) beg_afudc_base, 
		 nvl(decode(nvl(x.current_month_only,0),1,0,calc.cpi_base),0) beg_cpi_base,  
		 nvl((select ai.input_afudc_debt from afudc_input ai 
				 where ai.work_order_id = x.work_order_id
					and to_number(to_char(ai.month,'YYYYMM')) = :i_month_number), 0) input_afudc_debt,
		 nvl((select ai.input_afudc_equity from afudc_input ai 
				 where ai.work_order_id = x.work_order_id
					and to_number(to_char(ai.month,'YYYYMM')) = :i_month_number), 0) input_afudc_equity,
		 nvl((select ai.input_cpi from afudc_input ai 
				 where ai.work_order_id = x.work_order_id
					and to_number(to_char(ai.month,'YYYYMM')) = :i_month_number), 0) input_cpi,
		 0 afudc_base_used_in_calc,
		 0 cpi_base_used_in_calc,
		 nvl((select case when nvl(air.input_afudc_ratio,1) between -1 and 1 then nvl(air.input_afudc_ratio,1) else -999 end from afudc_input_ratio air
				 where air.work_order_id = x.work_order_id
					and to_number(to_char(air.month,'YYYYMM')) = :i_month_number), 1) input_afudc_ratio,
		 nvl((select case when nvl(air.input_cpi_ratio,1) between -1 and 1 then nvl(air.input_cpi_ratio,1) else -999 end from afudc_input_ratio air
				 where air.work_order_id = x.work_order_id
					and to_number(to_char(air.month,'YYYYMM')) = :i_month_number), 1) input_cpi_ratio,
		1 afudc_status_id,
		2 cpi_status_id,  
		 ad.debt_rate debt_rate,
		 ad.equity_rate equity_rate,
		 ad.cpi_rate cpi_rate,
		 0 afudc_je_debt,
		 
		decode(to_number(substr(to_char(:i_month_number),5,2)),1,ad.afudc_jan_com_ind,2,ad.afudc_feb_com_ind,3,ad.afudc_mar_com_ind,4,ad.afudc_apr_com_ind,
					  5,ad.afudc_may_com_ind,6,ad.afudc_jun_com_ind,7,ad.afudc_jul_com_ind,8,ad.afudc_aug_com_ind,9,ad.afudc_sep_com_ind,
					  10,ad.afudc_oct_com_ind,11,ad.afudc_nov_com_ind,12,ad.afudc_dec_com_ind,0) afudc_je_equity,
					  
		decode(to_number(substr(to_char(:i_month_number),5,2)),1,ad.cpi_jan_com_ind,2,ad.cpi_feb_com_ind,3,ad.cpi_mar_com_ind,4,ad.cpi_apr_com_ind,
					  5,ad.cpi_may_com_ind,6,ad.cpi_jun_com_ind,7,ad.cpi_jul_com_ind,8,ad.cpi_aug_com_ind,9,ad.cpi_sep_com_ind,
					  10,ad.cpi_oct_com_ind,11,ad.cpi_nov_com_ind,12,ad.cpi_dec_com_ind,0) cpi_je,
					  
		 0 afudc_isd_debt,
		 0 afudc_isd_equity,
		 0 cpi_isd,
		 x.afudc_elig_amount cm_charges_in_calc_afc,
		 x.cpi_elig_amount cm_charges_in_calc_cpi
  from afudc_calc calc,  afudc_data ad, AFUDC_CALC_WO_LIST_TEMP x, 
  		(select d.afudc_type_id, max(d.effective_date) effective_date
		  from afudc_data d
		  where d.effective_date <= :i_month
		  group by d.afudc_type_id) current_afudc_view
 where x.work_order_id = calc.work_order_id (+)
	and calc.month (+) = add_months(:i_month, -1) 
	and x.afudc_type_id = ad.afudc_type_id
	and ad.afudc_type_id = current_afudc_view.afudc_type_id
	and ad.effective_date = current_afudc_view.effective_date;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then 
	of_msg("ERROR: Inserting into AFUDC_CALC (roll forward): " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

of_msg("	 - Inserted " + string(nrows) + " into AFUDC_CALC  " + string(now()),"")  

return 1
end function

public function longlong of_afudc_and_cpi_calc_main ();// *************************************************************************************************************
//
//		Main function for afudc and cpi calculation
//
// *************************************************************************************************************
longlong currency_display_factor, wo_accr_ct, rtn
datetime overhead_approval, afudc_approval

//if not isvalid(w_wo_control) then 
//	//this uo object is NOT ready to be called outside of the w_wo_control window!!!
//	//do NOT call this uo object from an interface or other window!!!
//	of_msg("ERROR: afudc and cpi calculation must be called from w_wo_control","ERROR")
//	rollback;
//	i_afudc_error = true
//	return -1
//end if 

select description into  :i_company_descr
from company
where company_id = :i_company_id;

//verify the wo_process_control.overhead_approval and afudc_approval dates are null
setnull(overhead_approval)
setnull(afudc_approval)
select overhead_approval, afudc_approval into :overhead_approval, :afudc_approval
from wo_process_control
where company_id = :i_company_id
and accounting_month = :i_month;
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: unable to determine approval dates from wo_process_control: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

if isnull(overhead_approval) and isnull(afudc_approval) then
	//okay
else
	if not isnull(overhead_approval) then
		of_msg("ERROR:  wo_process_control.overhead_approval = " + string(overhead_approval) + " for Company = " + i_company_descr,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if 
	if not isnull(afudc_approval) then
		of_msg("ERROR:  wo_process_control.afudc_approval = " + string(afudc_approval) + " for Company = " + i_company_descr,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if 
end if 
 
of_msg("Starting AFUDC and CPI Calculation  " + String(now()),"")
of_msg(" -- Month: " + string(i_month_number) + ", Company: " + i_company_descr + ", Co ID: " + string(i_company_id),"")

//Get rounding value
setnull(currency_display_factor)
select 2 - 1/log(currency_display_factor ,10) into :currency_display_factor
from currency_schema a, currency b
where a.currency_id = b.currency_id
and a.currency_type_id = 1
and currency_display_factor <> 1
and company_id = :i_company_id ;
	
if isnull(currency_display_factor) then currency_display_factor = 2

i_currency_display_factor = currency_display_factor

i_expenditure_type_id = 1

// Delete previous AFUDC and After AFUDC OHs calcs for this month
of_msg("	Deleting prior run unapproved calculations  " + string(now()),"")

wo_accr_ct = long(f_pp_system_control_company("Work Order Accrual Charge Type",i_company_id))

delete from cwip_charge
 where month_number = :i_month_number
 	and (status = 1
	 	  or
		  status in (select clearing_id from clearing_wo_control where before_afudc_indicator = 0)
		 )
	and company_id = :i_company_id
	and charge_type_id <> :wo_accr_ct;
	
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Deleting cwip_charge AFUDC (big delete): " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

// Remove AFUDC and other overheads which have already been calculated for the month.
delete from afudc_calc a
 where month = :i_month
 	and work_order_id in (
	 	select b.work_order_id from work_order_control b
		 where b.company_id = :i_company_id
		 	and b.work_order_id = a.work_order_id);
			 
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Deleting from afudc_calc: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

// Call Extension Function
of_msg("	Pre AFUDC Function  " + string(now()), "")

if f_wo_control_pre_afudc(i_company_id, i_month_number) = -1 then
	of_msg("ERROR: Processing the f_wo_control_pre_afudc function for company " + string(i_company_id) + " at " + string(now()),"ERROR")
	return -1 
end if

 //Call Dynamic Validation option
f_pp_msgs("Calling dynamic validation.")
string args[]
longlong ext_rtn
args[1] = string(i_month_number)
args[2] = string(i_company_id)

ext_rtn = f_wo_validation_control(1052,args) //see https://powerme.pwrplan.com/display/PCM/Dynamic+Validation+Types for dynamic validation ID values

if ext_rtn < 0 then
	f_pp_msgs("AFUDC and CPI Calc (Dynamic Validations) Failed.")
	return -1
end if

// Update work order summary prior to afudc calc
rtn = f_update_wo_summary_month(i_company_id, i_month_number)

if rtn = -1 then
	of_msg("ERROR: Updating wo_summary (f_update_wo_summary_month)","ERROR")
	rollback;
	i_afudc_error = true
	return -1
else
	commit;  /*commit here before starting the afudc calc*/
end if

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start AFUDC and CPI calc 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Check to see if this is a cap interest company
if of_check_cap_interest() = -1 then 
	return -1
end if 

//get list of work orders
if of_wolist () < 0 then
	return -1
end if

// roll forward last month's AFUDC
if of_rollforward_afudc_calc() < 0 then
	return -1
end if
 
// validate afudc_control and afudc_data are configured correctly
if of_config_validations() < 0 then
	return -1
end if

//Maint 30014:  move eligibillty rules to top of the process flow steps
//WO Eligiblity
if of_not_eligible() < 0 then
	return -1
end if

///////////////////////////////////////////////////////////////////////////////////////
// WOs with LATE in service and afudc stop dates 
//         - not getting calc done for current month 
//         - evaluated for reversal afudc/cpi as needed
////////////////////////////////////////////////////////////////////////////////////////

//Maint 45322: take care of the work orders that have afudc_stop_date same as in_service_date
if of_late_in_service_stop_date_same() < 0 then 
	return -1
end if 

//  afc wos with in_service_date <= month getting reversals for calcs past in_service_date:
if of_late_in_service_afc() < 0 then
	return -1
end if

//  cpi wos with in_service_date <= month getting reversals for calcs past in_service_date:
if of_late_in_service_cpi() < 0 then
	return -1
end if

if of_late_in_service_wo_update() < 0 then
	return -1
end if

//  wos with afudc_stop_date < month getting afudc_status_id and cpi_status_id = 14:
if of_late_stop_date() < 0 then
	return -1
end if

////////////////////////////////////////////////////////////////////////////////////////
//WOs eligible for calc 
//       - there are a series of tests based on the AFUDC_CONTROL config
//       - each test would change the afudc_status_id/cpi_status_id so that
//          subsequent tests will not test the same work orders that have 
//          already failed another test ahead of the current test
////////////////////////////////////////////////////////////////////////////////////////


// Idle Months:  19
if of_idle_months() < 0 then
	return -1
end if

// CPI De Minimis
if of_cpi_deminimis() < 0 then
	return -1
end if

// Check for prior afudc/cpi charges
if of_check_prior_charges_afudc_cpi() < 0 then
	return -1
end if 

// Triggers:  4
if of_triggers() < 0 then
	return -1
end if  

// Determine data needed for Tests using Estimate/Actuals (this is needed before of_min_amt() and of_min_months() 
if of_get_est_and_acts() < 0 then
	return -1
end if

// Min Amt:  6
if of_min_amt() < 0 then
	return -1
end if

// Min Months:  8
if of_min_months() < 0 then
	return -1
end if

// Percent Est for CPI
if of_pct_cpi() < 0 then
	return -1
end if

//Maint 45322: WOs with in_service_date = month:  this needs to be ahead of of_stop_date_month  
if of_in_service_month() < 0 then
	return -1
end if 

//  WOs with afudc_stop_date = month:
if of_stop_date_month() < 0 then
	return -1
end if

// Final updates for afudc_status_id, cpi_status_id, and rates = 0 on afudc_calc table
if of_status_and_rates() < 0 then
	return -1
end if

// For work orders without a prior month's afudc_calc table record, obtain the beginning eligible wo balance for afudc base and cpi base
if of_build_prior_month_base() < 0 then
	return -1
end if   

// Calculate the afudc base and afudc debt and afudc equity
if of_calc_afudc() < 0 then
	return -1
end if 

// Calculate the cpi base and cpi
if of_calc_cpi() < 0 then
	return -1
end if 

// CWIP Charge Inserts
if of_cwip_charge() <> 1 then
	return -1
end if

// For Cap Int child companies, need to over-write the cwip_charge afudc_debt amount
if of_cap_int_final_adjust_afudc_debt() <> 1 then
	return -1
end if

i_afudc_processed = true 

return 1
end function

public function longlong of_build_prior_month_base ();//****************************************************************************************************
//  Identify work orders that do not have afudc_calc table data for prior month
//  Insert the prior month data with the correct afudc_base and cpi_base
//  Adjust for compounding
//  This insert will benefit subsequent calculations in the current month, because the prior month's record will exist next calculation run
//****************************************************************************************************
longlong nrows, nrows1, nrows2
of_msg("	Check Prior Month Base  " + string(now()),"...")

//update AFUDC_CALC_MISSING = 1 for the work orders in current month that do not have prior month's afudc_calc records
//exclude work orders where current month is the first month of charges, because these are not missing prior month's records 
//exclude work orders where the current_month_only = "YES," because these work orders need beginning month base to stay at $0.
update AFUDC_CALC_WO_LIST_TEMP
set AFUDC_CALC_MISSING = 1 
where nvl(AFUDC_CALC_MISSING, 0) <> 1
and min_charge_monum <> :i_month_number
and nvl(current_month_only, 0) <> 1
and work_order_id in 
	(
	select a.work_order_id
	from afudc_calc a
	where a.month = :i_month
	and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP x
				where a.work_order_id = x.work_order_id
				and (x.afudc_status_id <> 3 and x.eligible_for_afudc = 1
						or 
					x.cpi_status_id <> 3 and x.eligible_for_cpi = 1)
				)
	minus
	select b.work_order_id
	from afudc_calc b
	where b.month = add_months(:i_month, -1) 
	);

nrows1 = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_CALC_MISSING(1): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  

//if the update above found work orders with missing prior month records, then need to create data and calculate compounding needed.
if nrows1 > 0 then	
	//AFUDC & CPI
	//insert into afudc_calc for the prior month
	//need to save the amount of afudc and cpi for the prior month, even if they were loaded from charge interface, because the amounts are need for compounding 
	//need to exclude the afudc and cpi charge types from the base columns, because the "elig_indicators" may not be setup right (afudc/cpi types should have "elig_indicators" set to 0)
	insert into afudc_calc 
	(work_order_id, month, afudc_base, cpi_base, afudc_debt, afudc_equity, cpi, afudc_je_debt, afudc_je_equity, cpi_je, cpi_retro)
	
	select wo_summary.work_order_id as work_order_id, 
	add_months(:i_month, -1) as month,
	
	sum(decode(wo_summary.charge_type_id, AFUDC_CALC_WO_LIST_TEMP.debt_charge_type, 0, AFUDC_CALC_WO_LIST_TEMP.equity_charge_type, 0, AFUDC_CALC_WO_LIST_TEMP.cpi_charge_type, 0,
					wo_summary.wo_amount * charge_type_data.afudc_elig_indicator)) as afc_elig,
					
	sum(decode(wo_summary.charge_type_id, AFUDC_CALC_WO_LIST_TEMP.debt_charge_type, 0, AFUDC_CALC_WO_LIST_TEMP.equity_charge_type, 0, AFUDC_CALC_WO_LIST_TEMP.cpi_charge_type, 0,
					wo_summary.wo_amount * charge_type_data.cpi_elig_indicator)) as cpi_elig,
	
	sum(decode(wo_summary.gl_posting_mo_yr, add_months(:i_month, -1), decode(wo_summary.charge_type_id, AFUDC_CALC_WO_LIST_TEMP.debt_charge_type, wo_amount, 0), 0 )) as afudc_debt, 
	sum(decode(wo_summary.gl_posting_mo_yr, add_months(:i_month, -1), decode(wo_summary.charge_type_id, AFUDC_CALC_WO_LIST_TEMP.equity_charge_type, wo_amount, 0), 0 )) as afudc_equity,  
	sum(decode(wo_summary.gl_posting_mo_yr, add_months(:i_month, -1), decode(wo_summary.charge_type_id, AFUDC_CALC_WO_LIST_TEMP.cpi_charge_type, wo_amount, 0), 0 )) as cpi ,
	
	0 as afudc_je_debt, 
	0 as afudc_je_equity,  
	0 as cpi_je,
	0 as cpi_retro

	from wo_summary, charge_type_data, AFUDC_CALC_WO_LIST_TEMP
	where charge_type_data.charge_type_id = wo_summary.charge_type_id    
	and AFUDC_CALC_WO_LIST_TEMP.work_order_id = wo_summary.work_order_id    
	and wo_summary.expenditure_type_id = 1     
	and AFUDC_CALC_WO_LIST_TEMP.AFUDC_CALC_MISSING = 1 
	and wo_summary.company_id = :i_company_id
	and wo_summary.gl_posting_mo_yr <=  add_months(:i_month, -1)    
	group by wo_summary.work_order_id;
	
	nrows = sqlca.sqlnrows	
	
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: insert missing prior month afudc_calc: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if  
end if

//maint-43834:  re-compute the prior month's ending Afudc_Base if it has $0 for the prior month and afudc_status_id is null.  The afudc_status_id is never null for work order that were calculated at Month End.
//     CPI Retro might have made the row with $0 for Afudc_Base column, which will be wrong for current calc month for Afudc for work order that previous was not elig but is now elig for Afudc calculation.
//     Change AFUDC_CALC_MISSING to 2 for these work orders.  This "2" scenario is specifically meant for work orders that could have fake and incorrect prior month ending Afudc_Base on AFUDC_CALC.
update AFUDC_CALC_WO_LIST_TEMP a
set a.AFUDC_CALC_MISSING = 2 
where nvl(a.AFUDC_CALC_MISSING, 0) <> 1
and a.min_charge_monum <> :i_month_number
and nvl(a.current_month_only, 0) <> 1
and a.afudc_status_id <> 3 
and a.eligible_for_afudc = 1
and exists
	(
	select 1
	from afudc_calc b
	where a.work_order_id = b.work_order_id
	and b.month = add_months(:i_month, -1) 
	and NVL(b.AFUDC_BASE,0) = 0 
	and b.AFUDC_STATUS_ID IS NULL 
	)
and exists 
	(select 1 
	from wo_summary c
	where a.work_order_id = c.work_order_id
	and c.expenditure_type_id = 1     
	and c.gl_posting_mo_yr <= add_months(:i_month, -1) 
	);

nrows2 = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_CALC_MISSING(1b): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if  
		
if nrows2 > 0 then		
	update afudc_calc a
	set a.afudc_base = nvl((select sum(decode(b.charge_type_id, d.debt_charge_type, 0, d.equity_charge_type, 0, d.cpi_charge_type, 0, b.wo_amount * c.afudc_elig_indicator))
							from wo_summary b, charge_type_data c, AFUDC_CALC_WO_LIST_TEMP d
							where a.work_order_id = d.work_order_id
							and d.company_id = :i_company_id
							and d.AFUDC_CALC_MISSING = 2 
							and c.charge_type_id = b.charge_type_id 
							and d.work_order_id = b.work_order_id    
							and b.expenditure_type_id = 1     
							and b.gl_posting_mo_yr <=  add_months(:i_month, -1)    
							group by b.work_order_id
							),0)
	where a.month = add_months(:i_month, -1) 
	and exists (select 1
					from AFUDC_CALC_WO_LIST_TEMP d
					where a.work_order_id = d.work_order_id
					and d.company_id = :i_company_id
					and d.AFUDC_CALC_MISSING = 2  
					);
	
	nrows = sqlca.sqlnrows
	
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating afudc_calc.afudc_base(1c): " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if  
end if //nrows2 > 0 
	
if nrows1 > 0 or nrows2 > 0 then
	//Need to know the last compound month for these work orders (LAST_COMP_AFC_MN and LAST_COMP_CPI_MN)
	//The logic is in a separate function:  of_afudc_calc_last_compound_month() 
	if of_last_compound_month() < 0 then
		return -1
	end if 
	
	//afudc_base
	//Update the afudc_base for the newly created prior month that was missing using LAST_COMP_AFC_MN by adding the afudc activity prior to the LAST_COMP_AFC_MN
	update afudc_calc ac
	set ac.afudc_base = nvl(ac.afudc_base,0) + nvl((select sum(wo_amount)
														from wo_summary ws, AFUDC_CALC_WO_LIST_TEMP wo
														where ws.work_order_id = wo.work_order_id
														and ac.work_order_id = wo.work_order_id
														and AFUDC_CALC_MISSING in (1,2)
														and wo.eligible_for_afudc = 1
														and ws.month_number < wo.LAST_COMP_AFC_MN  
														and ws.expenditure_type_id = 1
														and ws.charge_type_id in ( wo.equity_charge_type, wo.debt_charge_type)
														group by ws.work_order_id 
														),0)
	where month = add_months(:i_month, -1)
	and exists (select 1
					from AFUDC_CALC_WO_LIST_TEMP x
					where ac.work_order_id = x.work_order_id
					and x.AFUDC_CALC_MISSING in (1,2)
					and x.eligible_for_afudc = 1);
	
	nrows = sqlca.sqlnrows 
	
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: update afudc_calc.afudc_base: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if  
	
	//afudc_compound_amt
	//Update the afudc_compound_amt for the newly created prior month that was missing using LAST_COMP_AFC_MN to be used in the current month's calc
	update afudc_calc ac
	set ac.afudc_compound_amt = nvl((select sum(wo_amount)
											from wo_summary ws, AFUDC_CALC_WO_LIST_TEMP wo
											where ws.work_order_id = wo.work_order_id
											and ac.work_order_id = wo.work_order_id
											and AFUDC_CALC_MISSING in (1,2)
											and wo.eligible_for_afudc = 1
											and ws.month_number >= wo.LAST_COMP_AFC_MN  
											and ws.month_number < :i_month_number
											and ws.expenditure_type_id = 1
											and ws.charge_type_id in ( wo.equity_charge_type, wo.debt_charge_type)
											group by ws.work_order_id 
											),0)
	where ac.month = add_months(:i_month, -1)
    and exists (select 1
					from AFUDC_CALC_WO_LIST_TEMP x
					where ac.work_order_id = x.work_order_id
					and x.AFUDC_CALC_MISSING in (1,2)
					and x.eligible_for_afudc = 1 );
	
	nrows = sqlca.sqlnrows
	
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: update afudc_calc.afudc_compound_amt: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if  
end if  //if nrows1 > 0 or nrows2 > 0

if nrows1 > 0 then
	//cpi_base  
	//Update the cpi_base for the newly created prior month that was missing using LAST_COMP_CPI_MN by adding the cpi activity prior to the LAST_COMP_CPI_MN
	update afudc_calc ac
	set ac.cpi_base = nvl(ac.cpi_base,0) + nvl((select sum(wo_amount)
														from wo_summary ws, AFUDC_CALC_WO_LIST_TEMP wo
														where ws.work_order_id = wo.work_order_id
														and ac.work_order_id = wo.work_order_id
														and AFUDC_CALC_MISSING = 1 
														and wo.eligible_for_cpi = 1
														and ws.month_number < wo.LAST_COMP_CPI_MN  
														and ws.expenditure_type_id = 1
														and ws.charge_type_id = wo.cpi_charge_type
														group by ws.work_order_id 
														),0)
	where month = add_months(:i_month, -1)
	and exists (select 1
					from AFUDC_CALC_WO_LIST_TEMP x
					where ac.work_order_id = x.work_order_id
					and x.AFUDC_CALC_MISSING = 1  
					and x.eligible_for_cpi = 1);
	
	nrows = sqlca.sqlnrows 
	
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: update afudc_calc.cpi_base: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if  
	
	//cpi_compound_amt
	//Update the cpi_compound_amt for the newly created prior month that was missing using LAST_COMP_CPI_MN to be used in the current month's calc
	update afudc_calc ac
	set ac.cpi_compound_amt = nvl((select sum(wo_amount)
											from wo_summary ws, AFUDC_CALC_WO_LIST_TEMP wo
											where ws.work_order_id = wo.work_order_id
											and ac.work_order_id = wo.work_order_id
											and AFUDC_CALC_MISSING = 1 
											and wo.eligible_for_cpi = 1
											and ws.month_number >= wo.LAST_COMP_CPI_MN  
											and ws.month_number < :i_month_number
											and ws.expenditure_type_id = 1
											and ws.charge_type_id = wo.cpi_charge_type
											group by ws.work_order_id 
											),0)
	where ac.month = add_months(:i_month, -1)
    and exists (select 1
					from AFUDC_CALC_WO_LIST_TEMP x
					where ac.work_order_id = x.work_order_id
					and x.AFUDC_CALC_MISSING = 1
					and x.eligible_for_cpi = 1);
	
	nrows = sqlca.sqlnrows
	
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: update afudc_calc.cpi_compound_amt: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if  
end if //if nrows1 > 0

if nrows1 > 0 or nrows2 > 0 then 
	//AFUDC & CPI
	// Rollfoward the prior month base that was missing to the current month's beg_afudc_base (include afudc_compound_amt to be used in the current month calc)
	update afudc_calc ac
	set (ac.beg_afudc_base, ac.afudc_compound_amt, ac.beg_cpi_base, ac.cpi_compound_amt) = 
						(select nvl(ac2.afudc_base,0), nvl(ac2.afudc_compound_amt,0), nvl(ac2.cpi_base,0), nvl(ac2.cpi_compound_amt,0)
							from afudc_calc ac2, AFUDC_CALC_WO_LIST_TEMP w
							where ac.work_order_id = ac2.work_order_id
							and w.work_order_id = ac2.work_order_id
							and w.AFUDC_CALC_MISSING in (1,2)
							and ac2.month = add_months(:i_month, -1)
							and ac.month =  :i_month
							)
	where ac.month = :i_month
    and exists (select 1
					from AFUDC_CALC_WO_LIST_TEMP x
					where ac.work_order_id = x.work_order_id
					and AFUDC_CALC_MISSING in (1,2)
					);
	
	nrows = sqlca.sqlnrows
	
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: update afudc_calc.beg and compound amounts from missing prior: " + sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if   
	
	of_msg("	Updated Prior Month Base  " + string(now()),"...")

end if //if nrows1 > 0 or nrows2 > 0  

return 1
end function

public function longlong of_calc_afudc ();//**************************************************************************************************************************
//This function handles the AFUDC Calculation where the base values are determined and the rate is multiplied to obtain the debt and equity amounts for the month
//**************************************************************************************************************************
longlong nrows
decimal {8} cap_int_adjust_ratio 

of_msg("	Calculate Current Month Afudc  " + string(now()),"...")


// For cap interest parent company, calculate a base adjustment for the external debt from all sub companies
if i_cap_int and i_cap_int_parent then 	
	update AFUDC_CALC_WO_LIST_TEMP w
	set w.AFUDC_BASE_ADJ = ( select d.base  
										from 	(select afudc_calc.work_order_id, round((c.debt * afudc_calc.afudc_base / c.total_base),2) base 
												from afudc_calc,
																(select  a.total_base, b.debt 
																from
																	(select sum(afudc_base) total_base   
																	from afudc_calc b
																	where month = Add_Months(:i_month, -1)
																	and work_order_id in 
																								(select work_order_id 
																								from work_order_control 
																								where company_id = :i_company_id
																								and funding_wo_indicator = 0)
																	) a, /*get last month's total afudc base for parent wo_ids*/
																	(select -sum(nvl(external_debt,0)) debt 
																	from cap_interest_calc
																	where month_number = :i_month_number
																	and company_id in (
																						select child_co.company_id
																						from work_order_control parent_co, cap_interest_wo cap_wo, work_order_control child_co
																						where parent_co.work_order_id = cap_wo.parent_wo
																						and child_co.work_order_id = cap_wo.work_order_id
																						and parent_co.company_id = :i_company_id)
																	) b /*get current month's total external_debt for child co_ids*/
																) c
												where afudc_calc.month = Add_Months(:i_month, -1)
												and afudc_calc.work_order_id in
																		(select work_order_id 
																		from work_order_control 
																		where company_id = :i_company_id
																		and funding_wo_indicator = 0) /*get the last month's afudc base by wo_id for parent wo_ids*/
												) d  
										where d.work_order_id = w.work_order_id
										)
	where w.afudc_status_id <> 3; 
	
else
	//not a cap int company, all other companies
	//update cwip_in_rate_base input adjustments used for afudc_base for work orders that did not go through late in service
	update AFUDC_CALC_WO_LIST_TEMP w
	set w.AFUDC_BASE_ADJ = (select  nvl(c.cwip_in_base,0) + nvl(c.retentions,0)
										from CWIP_IN_RATE_BASE c
										where c.work_order_id = w.work_order_id
										and to_number(to_char(effective_date,'yyyymm')) = :i_month_number
										and nvl(jurisdiction_id, -1) = -1)
	where w.afudc_status_id <> 3;  
end if //if i_cap_int and i_cap_int_parent then 	

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_BASE_ADJ: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   


update AFUDC_CALC_WO_LIST_TEMP w
set w.AFUDC_BASE_ADJ = 0
where w.AFUDC_BASE_ADJ is null;

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.AFUDC_BASE_ADJ = 0: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   


//Start calculating the "afudc_base_used_in_calc"  
//"afudc_base_used_in_calc" is initiated with $0 during the of_afudc_calc_rollforward() function call
//current_month_only = 1 means the base amount does not include prior month charges, so do not use the last month's base or compound amt

//1st: beg_afudc_base already obeys current_month_only during the rollfoward function call
//2nd: use half_current_month_afc
//3rd: afudc_compound_amt already obeys current_month_only during the of_afudc_calc_rollforward() function call
//        use the month's compound indicator saved in the "afudc_je_equity" column and afudc_compound_amt column saved during the rollfoward function call
//        (when month = the compound month, the "afudc_je_equity" column has 1, which means the last month's afudc_compound_amt needs to be added)  
//4th: use the AFUDC_BASE_ADJ saved in the sql above to add to the afudc_base_used_in_calc
//5th: exclude afudc_status_id = 3 (late in service)  those work orders do not need new calculations
update afudc_calc ac
set ac.afudc_base_used_in_calc = decode(ac.afudc_je_equity, 1,  nvl(ac.beg_afudc_base,0)+ nvl(ac.half_current_month_afc,0)+ nvl(ac.afudc_compound_amt,0),  nvl(ac.beg_afudc_base,0)+ nvl(ac.half_current_month_afc,0)) 
											+ (select w.AFUDC_BASE_ADJ from AFUDC_CALC_WO_LIST_TEMP w
												where w.work_order_id = ac.work_order_id
												and w.afudc_status_id <> 3)
where ac.month = :i_month
and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP wo
				where ac.work_order_id = wo.work_order_id 
				and wo.afudc_status_id <> 3);

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.afudc_base_used_in_calc (1): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

//update the afudc_debt, afudc_equity, and final afudc_base_used_in_calc.    
//afudc_base_used_in_calc is used in reports for a simple base * rate manual calc
update afudc_calc ac
set ac.afudc_debt = round(( nvl(ac.afudc_base_used_in_calc,0)*ac.input_afudc_ratio*ac.debt_rate) +  nvl(ac.input_afudc_debt,0), :i_currency_display_factor),
ac.afudc_equity = round(( nvl(ac.afudc_base_used_in_calc,0)*ac.input_afudc_ratio*ac.equity_rate) +  nvl(ac.input_afudc_equity,0), :i_currency_display_factor) 
where ac.month = :i_month
and exists (select 1
			from AFUDC_CALC_WO_LIST_TEMP wo
			where ac.work_order_id = wo.work_order_id 
			and wo.afudc_status_id <> 3); 

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.afudc_debt & afudc_equity: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

if i_cap_int then
	//Cap Int companies use Input afudc columns to manipulate the afudc debt results between child and parent work orders
	//Nothing should be done for the allow_neg_afudc option where the Input adjustment are used where allow_neg_afudc = 0
	
	//Something extra has to be done for parent cap int
	if i_cap_int_parent then
		cap_int_adjust_ratio = 0
		//get the child company's interst expense 
		//typically, the child's interest_expense columns will be 0, causing a divide by zero oracle error, resulting in cap_int_adjust_ratio = 0
		select sum(nvl(internal_interest_expense,0)) / 
						  sum(nvl(internal_interest_expense+external_interest_expense,0)) 
		into :cap_int_adjust_ratio 
		from cap_interest_calc 
		where month_number = :i_month_number 
		and company_id in (
				select child_co.company_id
				from work_order_control parent_co, cap_interest_wo cap_wo, work_order_control child_co
				where parent_co.work_order_id = cap_wo.parent_wo
				and child_co.work_order_id = cap_wo.work_order_id
				and parent_co.company_id = :i_company_id);
				
		if isnull(cap_int_adjust_ratio) then cap_int_adjust_ratio = 1
				
		if cap_int_adjust_ratio <> 0 then
			
			//clear out the global temp table to perform Cap Int ratio adjustments
			delete from CAP_INT_ADJUST_TEMP;
			
			nrows = sqlca.sqlnrows
				
			if sqlca.sqlcode < 0 then
				of_msg("ERROR: delete from CAP_INT_ADJUST_TEMP: " +  sqlca.SQLErrText,"ERROR")
				rollback;
				i_afudc_error = true
				return -1
			end if   
			
			//insert the afudc calc results from the Child work orders
			insert into CAP_INT_ADJUST_TEMP (parent_wo_id, child_total_debt, cap_int_adjust_ratio)
			select b.parent_wo,  nvl(sum(a.afudc_debt + a.input_afudc_debt),0), :cap_int_adjust_ratio 
			from afudc_calc a, cap_interest_wo b, work_order_control parent_wo
			where a.work_order_id = b.work_order_id
			and a.month = :i_month 
			and parent_wo.work_order_id = b.parent_wo
			and parent_wo.company_id = :i_company_id
			group by b.parent_wo ;
			
			nrows = sqlca.sqlnrows
				
			if sqlca.sqlcode < 0 then
				of_msg("ERROR: insert into CAP_INT_ADJUST_TEMP: " + sqlca.SQLErrText,"ERROR")
				rollback;
				i_afudc_error = true
				return -1
			end if    
			
			update afudc_calc ac
			set ac.afudc_debt = ac.afudc_debt - nvl((select t.child_total_debt*t.cap_int_adjust_ratio
															from CAP_INT_ADJUST_TEMP t
															where ac.work_order_id = t.parent_wo_id
															),0)
			where ac.month = :i_month
			and exists (select 1
						from AFUDC_CALC_WO_LIST_TEMP wo
						where ac.work_order_id = wo.work_order_id 
						and wo.afudc_status_id <> 3); 
						
			nrows = sqlca.sqlnrows
				
			if sqlca.sqlcode < 0 then
				of_msg("ERROR: update afudc_calc for adjusted_parent_debt: " + sqlca.SQLErrText,"ERROR")
				rollback;
				i_afudc_error = true
				return -1
			end if    
			
		end if 
	end if 
	
else
	//for non-cap int companies
	//need to overwrite calculations for work orders using allow_neg_afudc <> 1 option, meaning do not allow negative afudc and take only the Input adjustments
	update afudc_calc ac
	set ac.afudc_debt =  round( nvl(ac.input_afudc_debt,0), :i_currency_display_factor),
	ac.afudc_equity = round( nvl(ac.input_afudc_equity,0), :i_currency_display_factor) 
	where ac.month = :i_month
	and ac.afudc_base_used_in_calc < 0
	and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP wo
				where ac.work_order_id = wo.work_order_id 
				and wo.afudc_status_id <> 3 
				and allow_neg_afudc <> 1); 
	
	nrows = sqlca.sqlnrows
					
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating afudc_calc.afudc_debt & afudc_equity FOR allow_neg_afudc <> 1: " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		i_afudc_error = true
		return -1
	end if   
end if 
			
//update afudc_base, which will get used in next month's calc 
//use the month's compound indicator saved in the "afudc_je_equity" column and afudc_compound_amt column saved during the rollfoward function call
update afudc_calc ac
set ac.afudc_base = decode(ac.afudc_je_equity, 1,  nvl(ac.beg_afudc_base,0)+ nvl(ac.cm_charges_in_calc_afc,0)+ nvl(ac.afudc_compound_amt,0),  nvl(ac.beg_afudc_base,0)+ nvl(ac.cm_charges_in_calc_afc,0)) 
where ac.month = :i_month
and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP wo
				where ac.work_order_id = wo.work_order_id 
				and wo.afudc_status_id <> 3 ); 

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.afudc_base: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

//reset and update cpi_compound_amt to reflect the correct amount for this month, which will get used in next month's calc
//when the month is the compound month, the new afudc_compound_amt is simply the current month's  afudc_equity + afudc_debt
//when the month is not the compounding month, the afudc_compound_amt needs to increase by the  current month's afudc_equity + afudc_debt to be evaluated for next month's calc
//use the month's compound indicator saved in the "afudc_je_equity" column and afudc_compound_amt column saved during the rollfoward function call
update afudc_calc ac
set ac.afudc_compound_amt = decode(ac.afudc_je_equity, 1,   nvl(afudc_equity,0) +  nvl(afudc_debt,0) ,  nvl(afudc_compound_amt,0) +  nvl(afudc_equity,0) +  nvl(afudc_debt,0) )
where ac.month = :i_month
and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP wo
				where ac.work_order_id = wo.work_order_id 
				and wo.afudc_status_id <> 3 ); 

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.afudc_compound_amt: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

//update afudc_base_used_in_calc for reporting (need to do this after the allow_neg_afudc update above in case the "input_afudc_ratio" is negative
//update afudc_je_equity and set to $0, no need to hijack this column anymore
update afudc_calc ac
set ac.afudc_base_used_in_calc =  nvl(ac.afudc_base_used_in_calc,0)*ac.input_afudc_ratio,
ac.afudc_je_equity = 0
where ac.month = :i_month 
and exists (select 1
			from AFUDC_CALC_WO_LIST_TEMP wo
			where ac.work_order_id = wo.work_order_id); 

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating afudc_calc.afudc_base_used_in_calc (2): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   
			
			
return 1
end function

public function longlong of_calc_cpi ();//*****************************************************************************************************************************
//This function handles the CPI Calculation where the base values are determined and the rate is multiplied to obtain the cpi amounts for the month
//****************************************************************************************************************************** 
longlong nrows
of_msg("	Calculate Current Month CPI  " + string(now()),"...")


//Start calculating the "cpi_base_used_in_calc"  
//"cpi_base_used_in_calc" is initiated with $0 during the of_cpi_calc_rollforward() function call
//current_month_only = 1 means the base amount does not include prior month charges, so do not use the last month's base or compound amt

//1st: beg_cpi_base already obeys current_month_only during the rollfoward function call
//2nd: use half_current_month_cpi
//3rd: cpi_compound_amt already obeys current_month_only during the of_afudc_calc_rollforward() function call
//        use the month's compound indicator saved in the "cpi_je" column and cpi_compound_amt column saved during the rollfoward function call
//        (when month = the compound month, the "cpi_je" column has 1, which means the last month's cpi_compound_amt needs to be added)   
//4th: exclude cpi_status_id = 3 (late in service)  those work orders do not need new calculations
update afudc_calc ac
set ac.cpi_base_used_in_calc = decode(ac.cpi_je, 1, nvl(ac.beg_cpi_base,0)+ nvl(ac.half_current_month_cpi,0)+ nvl(ac.cpi_compound_amt,0),  nvl(ac.beg_cpi_base,0)+ nvl(ac.half_current_month_cpi,0))  
where ac.month = :i_month
and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP wo
				where ac.work_order_id = wo.work_order_id 
				and wo.cpi_status_id <> 3);

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating cpi_calc.cpi_base_used_in_calc (1): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

//update the cpi and final cpi_base_used_in_calc.    
//cpi_base_used_in_calc is used in reports for a simple base * rate manual calc
update afudc_calc ac
set ac.cpi = round(( nvl(ac.cpi_base_used_in_calc,0)*ac.input_cpi_ratio*ac.cpi_rate) +  nvl(ac.input_cpi,0), :i_currency_display_factor) 
where ac.month = :i_month
and exists (select 1
			from AFUDC_CALC_WO_LIST_TEMP wo
			where ac.work_order_id = wo.work_order_id 
			and wo.cpi_status_id <> 3); 

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating cpi_calc.cpi: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

//need to overwrite calculations for work orders using allow_neg_cpi <> 1 option, meaning do not allow negative cpi and take only the input adjustments
update afudc_calc ac
set ac.cpi =  round( nvl(ac.input_cpi,0), :i_currency_display_factor) 
where ac.month = :i_month
and ac.cpi_base_used_in_calc < 0
and exists (select 1
			from AFUDC_CALC_WO_LIST_TEMP wo
			where ac.work_order_id = wo.work_order_id 
			and wo.cpi_status_id <> 3 
			and allow_neg_cpi <> 1); 

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating cpi_calc.cpi FOR allow_neg_cpi <> 1: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   
			
//update cpi_base, which will get used in next month's calc 
//use the month's compound indicator saved in the "cpi_je" column and cpi_compound_amt column saved during the rollfoward function call
update afudc_calc ac
set ac.cpi_base = decode(ac.cpi_je, 1,  nvl(ac.beg_cpi_base,0)+ nvl(ac.cm_charges_in_calc_cpi,0)+ nvl(ac.cpi_compound_amt,0),  nvl(ac.beg_cpi_base,0)+ nvl(ac.cm_charges_in_calc_cpi,0)) 
where ac.month = :i_month
and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP wo
				where ac.work_order_id = wo.work_order_id 
				and wo.cpi_status_id <> 3 ); 

nrows = sqlca.sqlnrows
				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating cpi_calc.cpi_base: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

//reset and update cpi_compound_amt to reflect the correct amount for this month, which will get used in next month's calc
//when the month is the compound month, the new cpi_compound_amt is simply the current month's  cpi 
//when the month is not the compounding month, the cpi_compound_amt needs to increase by the  current month's cpi  to be evaluated for next month's calc
//use the month's compound indicator saved in the "cpi_je" column and cpi_compound_amt column saved during the rollfoward function call
update afudc_calc ac
set ac.cpi_compound_amt = decode(ac.cpi_je, 1,   nvl(cpi,0) ,  nvl(cpi_compound_amt,0) +  nvl(cpi,0) )
where ac.month = :i_month
and exists (select 1
				from AFUDC_CALC_WO_LIST_TEMP wo
				where ac.work_order_id = wo.work_order_id 
				and wo.cpi_status_id <> 3 ); 

nrows = sqlca.sqlnrows

				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating cpi_calc.cpi_compound_amt: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if   

//update cpi_base_used_in_calc for reporting (need to do this after the allow_neg_cpi update above in case the "input_cpi_ratio" is negative
//update cpi_je  and set to $0, no need to hijack this column anymore
update afudc_calc ac
set ac.cpi_base_used_in_calc =  nvl(ac.cpi_base_used_in_calc,0)*ac.input_cpi_ratio,
ac.cpi_je  = 0
where ac.month = :i_month 
and exists (select 1
			from AFUDC_CALC_WO_LIST_TEMP wo
			where ac.work_order_id = wo.work_order_id); 

nrows = sqlca.sqlnrows

				
if sqlca.sqlcode < 0 then
	of_msg("ERROR: Updating cpi_calc.cpi_base_used_in_calc (2): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if    

return 1
end function

public function longlong of_check_prior_charges_afudc_cpi ();//**************************************************************************************************************
//This function sets the "has_prior_afc" and "has_prior_cpi" columns which are later used for the following tests:
//
//4: Failed AFUDC Triggers Test, No AFUDC Calculated
//6: Failed Minimum Estimate and Actuals Test, No AFUDC Calculated
//8: Failed Estimate Time Test, No AFUDC Calculated 
//10: Failed Minimum Estimate and Time Test, No AFUDC Calculated
//
//5: Failed AFUDC Triggers Test, No CPI Calculated 
//7: Failed Minimum Estimate and Actuals Test, No CPI Calculated
//9: Failed Estimate Time Test, No CPI Calculated 
//11: Failed Minumum Estimate and Time Test, No CPI Calculated 
//12: Failed Percent of Estimate for CPI, No CPI Calculated  
// 
//**************************************************************************************************************

longlong nrows 

of_msg("	Check Prior Month Afudc and CPI  " + string(now()),"")

//Update has_prior_afc & has_prior_cpi
update AFUDC_CALC_WO_LIST_TEMP a
set a.has_prior_afc = 1
where exists (select 1 from WO_SUMMARY b
			where a.work_order_id = b.work_order_id
			and b.charge_type_id in (a.equity_charge_type, a.debt_charge_type )
			and rownum =1 
			)
and a.eligible_for_afudc = 1
and a.afudc_status_id = 1;

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then 
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.has_prior_afc: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if 

update AFUDC_CALC_WO_LIST_TEMP a
set a.has_prior_cpi = 1
where exists (select 1 from WO_SUMMARY b
			where a.work_order_id = b.work_order_id
			and b.charge_type_id = a.cpi_charge_type 
			and rownum =1 
			)
and a.eligible_for_cpi = 1
and a.cpi_status_id = 2;

nrows = sqlca.sqlnrows
			
if sqlca.sqlcode < 0 then 
	of_msg("ERROR: Updating AFUDC_CALC_WO_LIST_TEMP.has_prior_cpi: " + sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

return 1
end function

public function longlong of_cap_int_final_adjust_afudc_debt ();//**************************************************************************************************************************
//This function handles the AFUDC Debt charge amount for the CWIP_CHARGE table for Cap In companies
//**************************************************************************************************************************
longlong nrows, rowcount, wo_id, i 
decimal {2} total_cap_int, total_int, cap_int_adjust, input_afudc_debt

if i_cap_int = false then
	return 1
end if 

of_msg("	Afudc Debt for Cap Int Updates  " + string(now()),"")

uo_ds_top ds_cap_int_allo
ds_cap_int_allo = CREATE uo_ds_top
ds_cap_int_allo.DataObject = "dw_cap_int_allo"
ds_cap_int_allo.SetTransObject(sqlca)

total_cap_int = 0
total_int = 0
cap_int_adjust = 0
			
select sum(Afudc_Debt+Input_Afudc_Debt) into :total_cap_int 
from afudc_calc, work_order_control
where to_char(month,'yyyymm') = to_char(:i_month_number)
and  afudc_calc.work_order_id = work_order_control.work_order_id
and work_order_control.company_id = :i_company_id;

select internal_interest_expense+external_interest_expense 
into :total_int
from cap_interest_calc
where company_id = :i_company_id
and month_number = :i_month_number;

if i_cap_int_parent then 	

//			select sum(nvl(external_interest_expense,0)) into :int_adjust 
//			from cap_interest_calc where
//			company_id in (
//					select distinct company_id
//					from company_setup
//					where parent_company_id is not null
//					start with parent_company_id = :i_company
//					connect by prior company_id = parent_company_id);
//
//			total_int = total_int - int_adjust

end if 

if ABS(total_cap_int) > total_int then   // An adjustment is required
	
	cap_int_adjust = total_int - total_cap_int
	
	update cap_interest_calc set cap_interest_adjustment = :cap_int_adjust
	where company_id = :i_company_id
	and month_number = :i_month_number;
	
	nrows = sqlca.sqlnrows
				
	if sqlca.sqlcode < 0 then
		of_msg("ERROR: Updating cap_interest_calc.cap_interest_adjustment: " + &
				 sqlca.SQLErrText,"ERROR")
		rollback;
		destroy ds_cap_int_allo
		i_afudc_error = true
		return -1
	end if    
	
	rowcount =	ds_cap_int_allo.RETRIEVE(i_month_number,i_company_id,-cap_int_adjust)

	ds_cap_int_allo.groupcalc()
	
	for i = 1 to rowcount
		
			wo_id = ds_cap_int_allo.GetItemNumber(i, 'work_order_id')

			input_afudc_debt = - ds_cap_int_allo.GetItemdecimal(i, 'cap_int_adjust')
		
			update afudc_calc set input_afudc_debt = input_afudc_debt + :input_afudc_debt
			 where to_char(month, 'yyyymm') = to_char(:i_month_number) 
				and work_order_id = :wo_id ;
			
			nrows = sqlca.sqlnrows
						
			if sqlca.sqlcode < 0 then
				of_msg("ERROR: Updating afudc_calc.input_afudc_debt: " + &
						 sqlca.SQLErrText,"ERROR")
				rollback;
				destroy ds_cap_int_allo
				i_afudc_error = true
				return -1
			end if    
		
			update cwip_charge set cwip_charge.amount = cwip_charge.amount + :input_afudc_debt
			where cwip_charge.work_order_id = :wo_id 
			and cwip_charge.month_number = :i_month_number
			and cwip_charge.status = 1
			and cwip_charge.cost_element_id = (select debt_cost_element from afudc_control, work_order_account
											where work_order_account.work_order_id = cwip_charge.work_order_id
											and afudc_control.afudc_type_id = work_order_account.afudc_type_id
											)
			and cwip_charge.description = 'AFUDC debt charge';
			
			nrows = sqlca.sqlnrows
						
			if sqlca.sqlcode < 0 then
				of_msg("ERROR: Updating cwip_charge.amount: " + &
						 sqlca.SQLErrText,"ERROR")
				rollback;
				destroy ds_cap_int_allo
				i_afudc_error = true
				return -1
			end if    
			
	next
	
end if 	

destroy ds_cap_int_allo

return 1 
end function

public function longlong of__start_calc (longlong a_company_id, longlong a_month_number, datetime a_month);//************************************************************************************************
//Initiate the variables that are used throughout the uo
//************************************************************************************************
i_company_id = a_company_id
i_month_number  = a_month_number 
i_month = a_month 

//at this point, this function is only getting called from w_wo_control
//i_visual_calc = true 

//if not isvalid(w_wo_control) then 
//	//this uo object is NOT ready to be called outside of the w_wo_control window!!!
//	//do NOT call this uo object from an interface or other window!!!
//	of_msg("ERROR: afudc and cpi calculation must be called from w_wo_control","ERROR")
//	rollback;
//	i_afudc_error = true
//	return -1
//end if 

if of_afudc_and_cpi_calc_main () < 0 then
	rollback;
	i_afudc_error = true
	return -1
else
	commit; /*commit here after calculations are complete for company and month*/
end if

return 1
end function

public function longlong of_late_in_service_afc ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  This function handles work orders with in_service_date < current month.
//       Afudc >= to the wo in_service_date will be reversed in current month.
//       The IN_SERVICE_OPTION is used to determine how much of the in_service month's calculated amount will be reversed.
//
//  These work orders will later get their rates set to 0 to generate $0 afudc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong nrows, afc_nrows

of_msg("	Late In Service AFC Updates  " + string(now()),"...")

//Maint 30014: only do this for afc eligible wos
update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = 3 
where in_service_date is not null 
and to_number(to_char(in_service_date, 'yyyymm')) < :i_month_number
and afudc_status_id = 1 //do this only for afudc_status_id = 1 to know that late reversal adjustments are needed (Maint 45322)
and eligible_for_afudc = 1;

afc_nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id (afc): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//check if any records updated. if none, get out.
if afc_nrows = 0 then
	return 0
end if 

//for work orders with status_id = 3 from above:
//filter to wos where 1) original_in_service_date is null
//                            2) to_char(original_in_service_date, 'yyyymm') > to_char(in_service_date, 'yyyymm')
//                            3) orig_isd_month_number = current month

update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = -3 
where afudc_status_id = 3 
and original_in_service_date is null;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id afc1: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//orig in service month > late in service
update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = -3 
where afudc_status_id = 3 
and to_char(original_in_service_date, 'yyyymm') > to_char(in_service_date, 'yyyymm');

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id afc2: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//orig calc month = in service
update AFUDC_CALC_WO_LIST_TEMP
set afudc_status_id = -3 
where afudc_status_id = 3 
and orig_isd_month_number = :i_month_number;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id afc3: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//1st, get the amount of calc past the in service date
update AFUDC_CALC_WO_LIST_TEMP a
set (a.EQUITY_PAST_ISD_AMOUNT, a.DEBT_PAST_ISD_AMOUNT	) = 
	(select sum(nvl(afudc_equity, 0) - nvl(input_afudc_equity, 0)), 
	sum(nvl(afudc_debt,  0) - nvl(input_afudc_debt,  0)) 
	from afudc_calc b, AFUDC_CALC_WO_LIST_TEMP w
	where a.work_order_id = w.work_order_id
	and a.work_order_id = b.work_order_id
	and w.afudc_status_id = -3
	and to_char(b.month, 'J') > to_char(w.in_service_date,  'J')
	and to_char(b.month, 'J') < to_char(:i_month, 'J')
	)
where a.afudc_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.isd past amounts afc: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//2nd, get the amount of calc equal to the in service date
update AFUDC_CALC_WO_LIST_TEMP a
set (a.EQUITY_ISD_AMOUNT, 	a.DEBT_ISD_AMOUNT	) = 
			(select sum(nvl(afudc_equity, 0) - nvl(input_afudc_equity, 0)), 
			sum(nvl(afudc_debt,  0) - nvl(input_afudc_debt,  0))
			from afudc_calc b, AFUDC_CALC_WO_LIST_TEMP w
			where a.work_order_id = w.work_order_id
			and a.work_order_id = b.work_order_id
			and w.afudc_status_id = -3
			and to_char(b.month, 'yyyymm') =  to_char(w.in_service_date,  'yyyymm')
			and to_char(b.month, 'yyyymm') <> :i_month_number 
			)
where a.afudc_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.isd amounts afc: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//3rd, adjust the in service date amount by the STOP_MONTH_RATE_ADJ (IN_SERVICE_OPTION)
update AFUDC_CALC_WO_LIST_TEMP a 
set a.DEBT_ISD_AMOUNT =  a.DEBT_ISD_AMOUNT*(1 - a.STOP_MONTH_RATE_ADJ),
	a.EQUITY_ISD_AMOUNT =  a.EQUITY_ISD_AMOUNT*(1 - a.STOP_MONTH_RATE_ADJ) 
where a.afudc_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.isd amounts afc adj: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//update the "isd" columns on afudc_calc
update afudc_calc a
set (afudc_base,  half_current_month_afc,  beg_afudc_base,  afudc_base_used_in_calc, 
  afudc_compound_amt,  cm_charges_in_calc_afc,  
  afudc_isd_equity,  
  afudc_isd_debt, 
  afudc_status_id) = 
	 (select 0,0,0,0,
		0,0,
		(nvl(x.EQUITY_PAST_ISD_AMOUNT,0) + nvl(x.EQUITY_ISD_AMOUNT, 0))*-1, 
		(nvl(x.DEBT_PAST_ISD_AMOUNT,0) + nvl(x.DEBT_ISD_AMOUNT, 0))*-1, 
		3
	from AFUDC_CALC_WO_LIST_TEMP x
	where a.work_order_id = x.work_order_id 
	and x.afudc_status_id in ( -3, 3)
	)
where a.month = :i_month
and exists (select 1 from AFUDC_CALC_WO_LIST_TEMP w
				where a.work_order_id = w.work_order_id
				and w.afudc_status_id  in ( -3, 3)
				);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating afudc_calc.isd amounts afc: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if				
					
//update AFUDC_CALC_WO_LIST_TEMP afudc_status_id = -3 back to 3
update AFUDC_CALC_WO_LIST_TEMP 
set afudc_status_id = 3
where afudc_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id afc: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if
				
//update the main calc columns on afudc_calc
//for the wos where the afudc_status_id = -3 was never applied (orig_isd = in_service_date), they will get the input adjsutments with this update below
update afudc_calc a
set a.afudc_equity = round(nvl(a.afudc_isd_equity,0) + nvl(a.input_afudc_equity,0), :i_currency_display_factor), 
	a.afudc_debt =  round(nvl(a.afudc_isd_debt,0) + nvl(a.input_afudc_debt,0), :i_currency_display_factor), 
	a.afudc_status_id = 3,
	a.equity_rate = 0,
	a.debt_rate = 0
where a.month = :i_month
and exists (select 1 from AFUDC_CALC_WO_LIST_TEMP w
				where a.work_order_id = w.work_order_id
				and w.afudc_status_id = 3);

nrows = sqlca.sqlnrows
						
if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating afudc_calc main columns for late in service afc: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if
		  
return 1
end function

public function longlong of_late_in_service_wo_update ();//Update work_order_control only for the work orders that are going through the Late In Service for the first time
//There could be work orders that have been In Service for several months, but have input adjustments that are forcing inclusion in the calc months after In Service Date
longlong nrows

of_msg("	Late In Service WO Updates  " + string(now()),"...")

//maint-45385:  only update the "orig" columns if the original_in_service_date <> in_service_date 
update work_order_control a
set a.original_in_service_date = a.in_service_date,
a.orig_isd_month_number = :i_month_number
where  nvl(to_number(to_char(a.original_in_service_date,'YYYYMM')),999999) <>  nvl(to_number(to_char(in_service_date,'YYYYMM')),999999)
and a.in_service_date is not null
and exists (select 1 from AFUDC_CALC_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id
				and (b.afudc_status_id = 3 or b.cpi_status_id = 3)
				);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating work_order_control.original_in_service_date and orig_isd_month_number afc: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if
			
return 1
end function

public function longlong of_late_in_service_stop_date_same ();//Maint 45322:  need to avoid make reversals on a month were only a partial was done due to Afudc Stop Date.
//    This situation occurs where the wo gets Afudc Stop Date = 12/15/2015 in Dec Calc Month with no In Service Date.  Then, in Jan Calc Month, the In Service Date gets updated with 12/15/2015.
//    Need to prevent adjustment reversals being done in Jan Calc Month for this situation.
//only do this to avoid late reversals when all four of the following are true:
//   1.  the afudc stop date is the same as in service in yyyymm format
//   2.  there was a "stop afudc date" partial calculation (status_id = 14) in AFUDC_CALC for the calc month being the same as the in service date
//   3.  the in service date is < the current calc month
//   4.  the eligible_for_afudc is "yes"

//Having these four criteria allows truely late in service updates to peform proper late reversals, where if a work order
//   gets an in service date that is earlier than the stop date, then it is correct to reverse all of the afudc saved in the prior months of AFUDC_CALC, 
//   even if only a parital calc was done due to afudc stop date.

long nrows

of_msg("	Late InSrv = Stop Updates  " + string(now()),"...")

update AFUDC_CALC_WO_LIST_TEMP a
set a.afudc_status_id = 14
where nvl(to_number(to_char(a.in_service_date,'YYYYMM')),'999999') < :i_month_number 
and to_number(to_char(a.afudc_stop_date,'YYYYMM')) = to_number(to_char(a.in_service_date,'YYYYMM'))
and a.afudc_status_id = 1
and a.eligible_for_afudc = 1
and exists (select 1 from AFUDC_CALC b 
				where a.work_order_id = b.work_order_id
				and b.afudc_status_id = 24
				and to_number(to_char(a.in_service_date,'YYYYMM')) = to_number(to_char(b.month,'YYYYMM'))
				);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (afudc stop = in service) Updating AFUDC_CALC_WO_LIST_TEMP.afudc_status_id = 14 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if 

update AFUDC_CALC_WO_LIST_TEMP a
set a.cpi_status_id = 14 
where nvl(to_number(to_char(a.in_service_date,'YYYYMM')),'999999') < :i_month_number 
and to_number(to_char(a.afudc_stop_date,'YYYYMM')) = to_number(to_char(a.in_service_date,'YYYYMM'))
and a.cpi_status_id = 2
and a.eligible_for_cpi = 1
and exists (select 1 from AFUDC_CALC b 
				where a.work_order_id = b.work_order_id
				and b.cpi_status_id = 24
				and to_number(to_char(a.in_service_date,'YYYYMM')) = to_number(to_char(b.month,'YYYYMM'))
				);

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (afudc stop = in service) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id = 14 : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1
end if

//Update work_order_control
update work_order_control a
set a.original_in_service_date = a.in_service_date,
a.orig_isd_month_number = :i_month_number
where a.original_in_service_date is null
and a.company_id = :i_company_id 
and a.funding_wo_indicator = 0
and nvl(to_number(to_char(a.in_service_date,'YYYYMM')),'999999') < :i_month_number 
and exists (select 1 from AFUDC_CALC_WO_LIST_TEMP b
				where a.work_order_id = b.work_order_id
				and to_number(to_char(b.afudc_stop_date,'YYYYMM')) = to_number(to_char(a.in_service_date,'YYYYMM')) 
				and (b.afudc_status_id = 14 or b.cpi_status_id = 14)
				);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (afudc stop = in service) Updating work_order_control.original_in_service_date and orig_isd_month_number: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

return 1
end function

public function longlong of_late_in_service_cpi ();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  This function handles work orders with in_service_date < current month.
//       CPI >= to the wo in_service_date will be reversed in current month.
//       The IN_SERVICE_OPTION is used to determine how much of the in_service month's calculated amount will be reversed.
//
//  These work orders will later get their rates set to 0 to generate $0 CPI for the current month's calc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong nrows, cpi_nrows

of_msg("	Late In Service CPI Updates  " + string(now()),"...")

//Maint 30014: only do this for cpi eligible wos
update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = 3
where in_service_date is not null 
and to_number(to_char(in_service_date, 'yyyymm')) < :i_month_number
and cpi_status_id = 2 //do this only for afudc_status_id = 2 to know that late reversal adjustments are needed (Maint 45322)
and eligible_for_cpi = 1;

cpi_nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id cpi: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//check if any records updated. if none, get out.
if cpi_nrows = 0 then
	return 0
end if 

//for work orders with status_id = 3 from above:
//filter to wos where 1) original_in_service_date is null
//                            2) to_char(original_in_service_date, 'yyyymm') > to_char(in_service_date, 'yyyymm')
//                            3) orig_isd_month_number = current month

update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = -3 
where cpi_status_id = 3 
and original_in_service_date is null;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id cpi1: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//orig in service month > late in service
update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = -3 
where cpi_status_id = 3 
and to_char(original_in_service_date, 'yyyymm') > to_char(in_service_date, 'yyyymm');

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id cpi2: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//orig calc month = in service
update AFUDC_CALC_WO_LIST_TEMP
set cpi_status_id = -3 
where cpi_status_id = 3 
and orig_isd_month_number = :i_month_number;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id cpi3: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//1st, get the amount of calc past the in service date
update AFUDC_CALC_WO_LIST_TEMP a
set a.CPI_PAST_ISD_AMOUNT= 
	(select sum(nvl(cpi, 0) - nvl(input_cpi, 0))
	from afudc_calc b, AFUDC_CALC_WO_LIST_TEMP w
	where a.work_order_id = w.work_order_id
	and a.work_order_id = b.work_order_id
	and w.cpi_status_id = -3
	and to_char(b.month, 'J') > to_char(w.in_service_date,  'J')
	and to_char(b.month, 'J') < to_char(:i_month, 'J')
	)
where a.cpi_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.isd past amounts cpi: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//2nd, get the amount of calc equal to the in service date
update AFUDC_CALC_WO_LIST_TEMP a
set a.CPI_ISD_AMOUNT = 
			(select  	sum(nvl(cpi, 0) - nvl(input_cpi, 0))
						from afudc_calc b, AFUDC_CALC_WO_LIST_TEMP w
						where a.work_order_id = w.work_order_id
						and a.work_order_id = b.work_order_id
						and w.cpi_status_id = -3
						and to_char(b.month, 'yyyymm') =  to_char(w.in_service_date,  'yyyymm')
						and to_char(b.month, 'yyyymm') <> :i_month_number 
			)
where a.cpi_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.isd amounts cpi: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//3rd, adjust the in service date amount by the STOP_MONTH_RATE_ADJ (IN_SERVICE_OPTION)
update AFUDC_CALC_WO_LIST_TEMP a 
set a.CPI_ISD_AMOUNT  =  a.CPI_ISD_AMOUNT*(1 - a.STOP_MONTH_RATE_ADJ)
where a.cpi_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.isd amounts cpi: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if

//update the "isd" columns on afudc_calc
update afudc_calc a
set ( cpi_base,  half_current_month_cpi,  beg_cpi_base,  cpi_base_used_in_calc,
   cpi_compound_amt,  cm_charges_in_calc_cpi,  
  cpi_isd,  cpi_status_id) = 
	 (select 0,0,0,0,
		0,0, 
		(nvl(x.CPI_PAST_ISD_AMOUNT,0) + nvl(x.CPI_ISD_AMOUNT, 0))*-1,  3
	from AFUDC_CALC_WO_LIST_TEMP x
	where a.work_order_id = x.work_order_id 
	and x.cpi_status_id in ( -3, 3)
	)
where a.month = :i_month
and exists (select 1 from AFUDC_CALC_WO_LIST_TEMP w
				where a.work_order_id = w.work_order_id
				and w.cpi_status_id  in ( -3, 3)
				);

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating afudc_calc.isd amounts (cpi): " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if				
				
//update AFUDC_CALC_WO_LIST_TEMP cpi_status_id = -3 back to 3
update AFUDC_CALC_WO_LIST_TEMP 
set cpi_status_id = 3
where cpi_status_id = -3;

nrows = sqlca.sqlnrows

if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating AFUDC_CALC_WO_LIST_TEMP.cpi_status_id : " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if
				
//update the main calc columns on afudc_calc
//for the wos where the cpi_status_id = -3 was never applied (orig_isd = in_service_date), they will get the input adjsutments with this update below
update afudc_calc a
set a.cpi  =  round(nvl(a.cpi_isd,0) + nvl(a.input_cpi,0), :i_currency_display_factor), 
	a.cpi_status_id = 3, 
	a.cpi_rate = 0
where a.month = :i_month
and exists (select 1 from AFUDC_CALC_WO_LIST_TEMP w
				where a.work_order_id = w.work_order_id
				and w.cpi_status_id = 3);

nrows = sqlca.sqlnrows
						
if sqlca.sqlcode < 0 then
	of_msg("ERROR: (3) Updating afudc_calc main columns for late in service cpi: " + &
			 sqlca.SQLErrText,"ERROR")
	rollback;
	i_afudc_error = true
	return -1 
end if
		  
return 1
end function

on uo_afudc_cpi_calc.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_afudc_cpi_calc.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

