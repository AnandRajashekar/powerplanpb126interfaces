HA$PBExportHeader$uo_wip_computations.sru
$PBExportComments$37086
forward
global type uo_wip_computations from nonvisualobject
end type
end forward

global type uo_wip_computations from nonvisualobject
end type
global uo_wip_computations uo_wip_computations

type variables
longlong i_wip_comp
end variables

forward prototypes
public subroutine uf_msg (string a_msg)
public function integer uf_sqlca (string a_msg, boolean a_rows)
public function longlong uf_wip_comp_approve (longlong a_company_id, longlong a_month_number)
public function integer uf_wip_comp_closing (longlong a_company_id, longlong a_month_number, string a_closing_type, longlong a_wo_id)
public function integer uf_wip_comp_closing_from_archive (longlong a_company_id, longlong a_month_number, string a_closing_type)
public function integer uf_wip_comp_calc (longlong a_company_id, longlong a_month_number)
end prototypes

public subroutine uf_msg (string a_msg);

if g_process_id > 0 and not isnull(g_process_id) then
	f_pp_msgs(a_msg)
end if

if g_main_application then 
	f_wo_status_box('WIP Compuations',a_msg)
end if
end subroutine

public function integer uf_sqlca (string a_msg, boolean a_rows);if sqlca.sqlcode < 0 then
	uf_msg('SQL ERROR: ' + a_msg + " " + sqlca.sqlerrtext)
	return -1
end if

if a_rows then
	uf_msg('  Rows Affected: ' + string(sqlca.sqlnrows) + " " + a_msg)
end if

return 1
end function

public function longlong uf_wip_comp_approve (longlong a_company_id, longlong a_month_number);
string  company, je_code, month, where_str, acct,wo_process_ans, comp_descr, je_code_option
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i, ext_rtn
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
datetime dt_month
string wip_comp_desc

longlong je_method_id, amount_type


select to_date(:a_month_number,'yyyymm') into :dt_month
from dual;



//Call Extension Function
	
ext_rtn = f_wip_comp_extension(-1,a_month_number, 'approve',-1,a_company_id)

if ext_rtn = -1 then
	uf_msg("Error Occured in f_wip_comp_extension for wip computation")
	return -1
end if

// Generate the General Ledger Transactions.
	
	// create uo_ds_top
	uo_ds_top dw_gl_transaction
	dw_gl_transaction = CREATE uo_ds_top
	dw_gl_transaction.Dataobject = 'dw_gl_transaction'
	dw_gl_transaction.SetTransObject(sqlca)
	
	uo_ds_top dw_gl_charges
	dw_gl_charges = CREATE uo_ds_top
	dw_gl_charges.Dataobject = 'dw_gl_charges_wip_comp'
	dw_gl_charges.SetTransObject(sqlca)

	num_gl_rows    = dw_gl_charges.RETRIEVE(a_month_number,a_company_id)

	//JRD - Deal with To Company Id later
	select gl_company_no into :company 
	  from company where company_id = :a_company_id;
	
	
	// Process the AFUDC Equity transactions
	for i = 1 to num_gl_rows
		
		if mod(i, 50) = 0 then
			uf_msg("Creating GL Journal Entries (AFUDC Equity) ... Finished " + &
				string(i) + " of " + string(num_gl_rows))
		end if
		
		dw_gl_transaction.insertrow(1)
	
		//First do WIP Side
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
		dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
		dw_gl_transaction.setitem(1,'month',          dt_month)
		dw_gl_transaction.setitem(1,'company_number', company)
		dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
//		w_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')									
		afc_id = dw_gl_charges.getitemnumber(i, 'afudc_type_id')
		wip_comp_desc = dw_gl_charges.getitemstring(i,'description')
		acct_id = dw_gl_charges.getitemnumber(i, 'wip_gl_account')		
		je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
		amount_type  = dw_gl_charges.getitemdecimal(i,'amount_type')
		acct = f_autogen_je_account(dw_gl_charges, i, 33, acct_id,je_method_id)
		////	maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			uf_msg("Error occurred while generating the transaction's gl account.  Generated value : " + acct )
			rollback using sqlca;
			return -1
		end if
		
		je_code =	dw_gl_charges.getitemstring(i,'journal_code')
		je_code = trim(je_code)
		
		dw_gl_transaction.setitem(1,'gl_account',             acct)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 1) //original side the sign is same
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
		dw_gl_transaction.setitem(1,'source', left(wip_comp_desc,35))
		dw_gl_transaction.setitem(1,'amount_type',amount_type)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',33)
		
		//Now do Offset Side
		dw_gl_transaction.InsertRow(1)
	
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
		dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
		dw_gl_transaction.setitem(1,'month',          dt_month)
		dw_gl_transaction.setitem(1,'company_number', company)
		dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
		
		acct_id = dw_gl_charges.getitemnumber(i, 'offset_gl_account')		
		acct = f_autogen_je_account(dw_gl_charges, i, 34, acct_id,je_method_id)
		////	maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			uf_msg("Error occurred while generating the transaction's gl account.  Generated value : " + acct )
			rollback using sqlca;
			return -1
		end if
	
		dw_gl_transaction.setitem(1,'gl_account',             acct)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 0) //offset side sign on amount will be flipped
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		
		dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
		dw_gl_transaction.setitem(1,'source', left(wip_comp_desc,35))
		dw_gl_transaction.setitem(1,'amount_type',amount_type)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',34)
		
	next
	
	uf_msg("Updating ...")

	ret = dw_gl_transaction.Update()
	
	if ret <> 1 then
		uf_msg( + &
		"Error updating General Ledger Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext) 
		rollback using sqlca;
		return -1
	end if
	
	update wip_comp_calc
	set approval_date = sysdate
	where company_id = :a_company_id
	and month_number = :a_month_number
	;
	
	if uf_sqlca('Retrieve Wip Comp Calc Approval Dates',true) = -1 then return -1
	

return 1
end function

public function integer uf_wip_comp_closing (longlong a_company_id, longlong a_month_number, string a_closing_type, longlong a_wo_id);
string DESCRIPTION,COMP_TYPE,UNITIZATION_BASIS,SQL_ELIGIBILITY, SQLs
longlong COST_ELEMENT_ID,WIP_GL_ACCOUNT,OFFSET_GL_ACCOUNT,NON_UNIT_GL_ACCOUNT,UNITIZED_GL_ACCOUNT,COMPANY_ID_FROM, &
	OH_BASIS_ID,JURIS_CWIP_BASE_OPT,JURISDICTION_ID,CHARGE_OPTION_ID,UNITIZATION_BASIS_CHK,RELATED_ASSET_OPTION, &
	RETIREMENT_UNIT_ID,EXPENDITURE_TYPE_ID,BUD_OH_BASIS_ID,COMPANY_ID_TO,CMPD_JAN,CMPD_FEB,CMPD_MAR,CMPD_APR,CMPD_MAY, &
	CMPD_JUN,CMPD_JUL,CMPD_AUG,CMPD_SEP,CMPD_OCT,CMPD_NOV,CMPD_DEC, half_month_option, jur_allo_book_id,post_cwip,post_gl_entries
dec{2} cwip_in_base_to_alloc, total_base, total_allocated
longlong compound_month_number,cmpd, check2, check3, ext_rtn, sql_rownum

any results[], reset_array[]
longlong wip_comps[], i, i_rows,unit_basis[], j, wo_check, k, num_buckets
string cv_buckets
boolean messages 

if a_wo_id <> 0 then
	messages = false
else
	messages = true
end if
//This function should have no commits or rollbacks, the caller function controls commits.

if messages then uf_msg('Starting WIP Computations for Company_id ' + string(a_company_id))


//Call Extension Function
	
	ext_rtn = f_wip_comp_extension(-1,a_month_number, 'closing_start',a_wo_id,a_company_id)
	
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_extension for wip computation closing start")
		return -1
	end if
	

//0. List of work orders to process.
//if a_wo_id = 0 then all for a_company
//if a_wo_id <> 0 then process for 1.

//ADD HERE Need to add for non-timed unitization or 106 processing must pull from pend_trans_archive

delete from wip_comp_temp_wo
;
if uf_sqlca('Delete wip_comp_temp_wo',messages) = -1 then return -1

if a_wo_id = 0 then
	choose case lower(a_closing_type)
		case 'non-unitized'	
			
			//// JRH 20090224 --> Company Constraint
			////###sjh 7/12/2011 Maint 7689
			insert into wip_comp_temp_wo
			(work_order_id)
			(select distinct work_order_id from work_order_control a,pend_transaction b, gl_account gl
			where a.work_order_number = b.work_order_number
			and a.company_id = :a_company_id
			and a.company_id = b.company_id
			and a.funding_wo_indicator = 0
			and b.gl_account_id = gl.gl_account_id
			and to_number(to_char(b.gl_posting_mo_yr,'yyyymm')) = :a_month_number 
			and trim(upper(b.activity_code)) in ('UADD')
			and b.ferc_activity_code = 1 
			and (gl.account_type_id = 12 or b.retirement_unit_id between 1 and 5)
			and exists (select 1 from wip_comp_charges d
							where a.work_order_id = d.work_order_id 
							and nvl(d.non_unitized_status,0) = 0   
							and nvl(d.unitized_status,0) = 0 
							and d.month_number <= :a_month_number)
			)
			;
			if uf_sqlca('Insert wip_comp_temp_wo from pend trans',messages) = -1 then return -1
		case 'unitization'	
			
			//// JRH 20090224 --> Company Constraint
			////###sjh 7/12/2011 Maint 7691
			insert into wip_comp_temp_wo
			(work_order_id)
			(select distinct work_order_id from work_order_control a,pend_transaction b, gl_account gl
			where a.work_order_number = b.work_order_number
			and a.company_id = :a_company_id
			and a.company_id = b.company_id
			and a.funding_wo_indicator = 0
			and b.gl_account_id = gl.gl_account_id
			and (gl.account_type_id <> 12 and b.retirement_unit_id not between 1 and 5)
			and trim(upper(b.activity_code)) in ('UADD','MADD')
			and b.ldg_depr_group_id < 0 
			and to_number(to_char(b.gl_posting_mo_yr,'yyyymm')) = :a_month_number 
			and exists (select 1 from wip_comp_charges d
							where a.work_order_id = d.work_order_id
							and nvl(d.unitized_status,0) = 0 
							and nvl(d.non_unitized_status,0) <> 100  
							and d.month_number <= :a_month_number)
			)
			;
			 
			if uf_sqlca('Insert wip_comp_temp_wo from pend trans',messages) = -1 then return -1
				
	end choose
	
else
	insert into wip_comp_temp_wo
	(work_order_id)
	values (:a_wo_id)
	;
	if uf_sqlca('Insert wip_comp_temp_wo',messages) = -1 then return -1
end if

//Call Extensions:  ---- good to update not eligible flag or insert additional work order ids
ext_rtn = 0
ext_rtn = f_wip_comp_dynamic_ext(-1,a_month_number, 'closing-after-eligibility',a_wo_id,a_company_id)

if ext_rtn = -1 then
	uf_msg("Error Occured in f_wip_comp_dynamic_ext for wip computation = " + description)
	return -1
end if


//If no work orders exit
wo_check = 0
select count(*)
into :wo_check
from wip_comp_temp_wo
;

if wo_check = 0 then return 1


//Load list of work orders into Global Temp Table.

//1.	Compute Charges to be closed based on the type - WIP_COMP_UNIT_CALC

////EKB 03022011 -- Delete same month records that might still persist. 
////It's either this or change insert below to join on sessionid /*and a.processing_session = userenv('sessionid')*/ 
//delete from wip_comp_unit_calc a
//where a.month_number = :a_month_number
//and a.UNIT_TYPE = :a_closing_type
//and exists (select 1 from wip_comp_temp_wo b
//						where a.work_order_id = b.work_order_id
//						)
//and nvl(pend_transaction, 0) = 0
//;

/*sjh 7/22/2011 Maint 8133*/
delete from wip_comp_unit_calc a 
where a.UNIT_TYPE = :a_closing_type
and exists (select 1 from wip_comp_temp_wo b
						where a.work_order_id = b.work_order_id
						)
and nvl(pend_transaction, 0) = 0
;
if uf_sqlca('Delete previous runs from WIP COMP UNIT CALC',messages) = -1 then return -1
	
//	update wip_comp_charges a
//	set wip_comp_summary_id = decode(lower(:a_closing_type),'non-unitized',null,wip_comp_summary_id), wip_comp_sum_unitize_id = decode(lower(:a_closing_type),'unitization',null,wip_comp_sum_unitize_id)
//	where a.month_number = :a_month_number
//	and exists (select 1 from wip_comp_temp_wo b
//	where a.work_order_id = b.work_order_id
//	)
//	; 

 // maint 7883 6/29/2011
update wip_comp_charges a
set wip_comp_summary_id = decode(lower(:a_closing_type),'non-unitized', decode(nvl(a.non_unitized_status,0) ,0,null),wip_comp_summary_id), 
wip_comp_sum_unitize_id = decode(lower(:a_closing_type),'unitization',decode(nvl(a.unitized_status,0) ,0,null),wip_comp_sum_unitize_id)
where exists (select 1 from wip_comp_temp_wo b where a.work_order_id = b.work_order_id)
;
if uf_sqlca('Update wip_comp_charges.wip_comp_summary_id',messages) = -1 then return -1
	
//// JRH 20090224 --> group by ... a.month_number to group by... :a_month_number
choose case lower(a_closing_type)
	case 'non-unitized'
		
		/*sjh 7/12/2011 Maint 7689*/
		insert into wip_comp_unit_calc (
		WIP_COMP_SUMMARY_ID,
		WIP_COMPUTATION_ID,
		WORK_ORDER_ID,
		MONTH_NUMBER,
		TOTAL_AMOUNT,
		BOOK_SUMMARY_ID,
		UNIT_TYPE,
		PROCESS_SESSION)
		(select pwrplant1.nextval, a.* 
				from (select a.wip_computation_id, a.work_order_id, :a_month_number, sum(a.amount), b.book_summary_id,:a_closing_type,userenv('sessionid')
						from wip_comp_charges a, wip_comp_temp_wo c,  charge_type b, cost_element e
						where a.work_order_id = c.work_order_id
						and a.cost_element_id = e.cost_element_id
						and e.charge_Type_id = b.charge_type_id
						and a.wip_comp_summary_id is null
						and nvl(a.non_unitized_status,0) = 0
						and nvl(a.unitized_status,0) = 0 
						and a.month_number <= :a_month_number 
						and exists (select 1 from wip_computation j
										where a.wip_computation_id = j.wip_computation_id
										and nvl(j.close_to_asset,1) = 1
										and j.expenditure_type_id = 5)
						group by a.wip_computation_id, a.work_order_id, :a_month_number, b.book_summary_id,:a_closing_type,userenv('sessionid')
						) a
						
			)
		;
		
		if uf_sqlca('Insert WIP COMP UNIT CALC',messages) = -1 then return -1		
		
		//2.	Mark charges with summary id
		
		////JRH --> Month Number Update
		update wip_comp_charges b
		set wip_comp_summary_id = (select wip_comp_summary_id
												from wip_comp_unit_calc a,  charge_type d, cost_element e
												where a.wip_computation_id = b.wip_computation_id
												and a.work_order_id = b.work_order_id
												and a.month_number = :a_month_number
												and a.book_summary_id = d.book_summary_id
												 and b.cost_element_id = e.cost_element_id
												and e.charge_Type_id = d.charge_type_id
												/*EKB 03152011 -- In case both unitization and non-unitization being run for the month*/
												and a.UNIT_TYPE = :a_closing_type
												/* ### 8116: JAK: 20110719:  If there are multiple closing of the same WIP comp, need to pickup only the latest run */
												and nvl(a.pend_transaction,0) = 0 
												)
		where exists (select 1 from wip_comp_temp_wo c
							where b.work_order_id = c.work_order_id
							)
		and b.wip_comp_summary_id is null
		and nvl(b.non_unitized_status,0) = 0
		and nvl(b.unitized_status,0) = 0
		and b.month_number <= :a_month_number /*sjh 7/12/2011 Maint 7689*/
		and exists (select 1 from wip_computation j
										where b.wip_computation_id = j.wip_computation_id
										and nvl(j.close_to_asset,1) = 1
										and j.expenditure_type_id = 5)
		;
		if uf_sqlca('Insert WIP COMP CHARGES Update',messages) = -1 then return -1
		
		
	case 'unitization'
		
		//// JRH 20090224 --> Month Number Fix
		insert into wip_comp_unit_calc (
		WIP_COMP_SUMMARY_ID,
		WIP_COMPUTATION_ID,
		WORK_ORDER_ID,
		MONTH_NUMBER,
		TOTAL_AMOUNT,
		BOOK_SUMMARY_ID,
		UNIT_TYPE,
		PROCESS_SESSION)
		(select pwrplant1.nextval, a.* 
				from (select a.wip_computation_id, a.work_order_id, :a_month_number, sum(a.amount), b.book_summary_id,:a_closing_type,userenv('sessionid')
						from wip_comp_charges a, wip_comp_temp_wo c,  charge_type b, cost_element e
						where a.work_order_id = c.work_order_id
						and a.cost_element_id = e.cost_element_id
						and e.charge_Type_id = b.charge_type_id
						and a.wip_comp_sum_unitize_id is null
						and nvl(a.unitized_status,0) =  0
						and nvl(a.non_unitized_status,0) <> 100 
						and a.month_number <= :a_month_number 
						and exists (select 1 from wip_computation j
										where a.wip_computation_id = j.wip_computation_id
										and nvl(j.close_to_asset,1) = 1
										and j.expenditure_type_id = 5)
						group by a.wip_computation_id, a.work_order_id, :a_month_number, b.book_summary_id,:a_closing_type,userenv('sessionid')
						) a
						
			)
		;
		
		if uf_sqlca('Insert WIP COMP UNIT CALC Unitized',messages) = -1 then return -1		
		
		//2.	Mark charges with summary id	
		
		//// JRH 20090224 --> Month Number Fix
		update wip_comp_charges b
		set wip_comp_sum_unitize_id = (select wip_comp_summary_id
												from wip_comp_unit_calc a,  charge_type d, cost_element e
												where a.wip_computation_id = b.wip_computation_id
												and a.work_order_id = b.work_order_id
												and a.month_number = :a_month_number
												and a.book_summary_id = d.book_summary_id
												 and b.cost_element_id = e.cost_element_id
												and e.charge_Type_id = d.charge_type_id
												and a.UNIT_TYPE = :a_closing_type // CWB 20100709 in case they're processing more than one type (ex. unitization & non-untiization) during the same month 
												and b.wip_computation_id = a.wip_computation_id //cwb 20100317 added wip_computation_id to the join
												/* ### 8116: JAK: 20110719:  If there are multiple closing of the same WIP comp, need to pickup only the latest run */
												and nvl(a.pend_transaction,0) = 0 
												)
		where exists (select 1 from wip_comp_temp_wo c
							where b.work_order_id = c.work_order_id
							)
		and b.wip_comp_sum_unitize_id is null
		and b.month_number <= :a_month_number 
		and nvl(b.unitized_status,0) =  0
		and nvl(b.non_unitized_status,0) <> 100
		and exists (select 1 from wip_computation j
										where b.wip_computation_id = j.wip_computation_id
										and nvl(j.close_to_asset,1) = 1
										and j.expenditure_type_id = 5)
		;
		if uf_sqlca('Insert WIP COMP CHARGES Update Unitized',messages) = -1 then return -1	
end choose 						
	

//3.	Get list of current pending transactions from into WIP_COMP_PEND_TRANS table

// Delete on unused or last run of wip_comp_pending_trans records where pend transactions no longer exist
delete from wip_comp_pending_trans b
where exists (select 1 from wip_comp_temp_wo c
						where b.work_order_id = c.work_order_id
						)
and nvl(pend_transaction,0) = 0
;
if uf_sqlca('Delete wip_comp_pending_trans where pend_transaction = 0 ',messages) = -1 then return -1	

////ADD HERE Deal with the case of Pend Trans Archive for Post in Service Carrying Costs 

choose case lower(a_closing_type)
	case 'non-unitized'	
		insert into wip_comp_pending_trans
		(WIP_COMP_SUMMARY_ID,
		ORIG_PEND_TRANS_ID,
		WIP_COMPUTATION_ID,
		WORK_ORDER_ID,
		MONTH_NUMBER,
		POSTING_AMOUNT,
		PEND_BASIS_AMOUNT,
		NEW_PEND_TRANS_ID,
		AMOUNT,
		BOOK_SUMMARY_ID,
		PEND_TRANSACTION,
		PROCESS_SESSION)
		(select a.wip_comp_summary_id, b.pend_trans_id, a.wip_computation_id, a.work_order_id, :a_month_number, b.posting_amount, 0, 0, 0, a.book_summary_id,
		0, userenv('sessionid')
		from wip_comp_unit_calc a,pend_transaction b, gl_account gl, work_order_control woc
		where woc.work_order_number = b.work_order_number
		and woc.company_id = b.company_id
		and woc.funding_wo_indicator = 0
		and b.gl_account_id = gl.gl_account_id
		and (gl.account_type_id = 12 or b.retirement_unit_id between 1 and 5)
		and trim(upper(b.activity_code)) in ('UADD')   /*##sjh 7/11/2011  Maint 7689*/
		and b.ferc_activity_code = 1 
		and a.work_order_id = woc.work_order_id
		and nvl(a.pend_transaction,0) = 0 
		and to_number(to_char(b.gl_posting_mo_yr,'yyyymm')) = :a_month_number
 		and A.UNIT_TYPE = :a_closing_type  /*##sjh 7/11/2011  Maint 7997*/
		and exists (select 1 from wip_comp_temp_wo c
							where a.work_order_id = c.work_order_id)
		)
		;
		if uf_sqlca('Insert wip_comp_pending_trans from pend trans non-unitized',messages) = -1 then return -1
		
		update pend_transaction
		set posting_status = 7
		where pend_trans_id in (select orig_pend_trans_id from wip_comp_pending_trans
												where nvl(pend_transaction,0) = 0)
		and posting_status = 1
		;
		if uf_sqlca('update pend_transaction set posting status to 7 on existing pend transactions',false) = -1 then return -1
		
	case 'unitization'	
		
		//Maint 8241: for "unitized (101)" closings, add the wip_comp101_rollup_id
		insert into wip_comp_pending_trans
		(WIP_COMP_SUMMARY_ID,
		ORIG_PEND_TRANS_ID,
		WIP_COMPUTATION_ID,
		WORK_ORDER_ID,
		MONTH_NUMBER,
		POSTING_AMOUNT,
		PEND_BASIS_AMOUNT,
		NEW_PEND_TRANS_ID,
		AMOUNT,
		BOOK_SUMMARY_ID,
		PEND_TRANSACTION,
		PROCESS_SESSION, 
		wip_comp101_rollup_id)
		(select a.wip_comp_summary_id, b.pend_trans_id, a.wip_computation_id, a.work_order_id, :a_month_number, b.posting_amount, 0, 0, 0, a.book_summary_id,
		0, userenv('sessionid'), wc.wip_comp101_rollup_id
		from wip_comp_unit_calc a,pend_transaction b, work_order_control woc, gl_account gl, wip_computation wc
		where woc.work_order_number = b.work_order_number
		and wc.wip_computation_id = a.wip_computation_id
		and woc.company_id = b.company_id
		and woc.funding_wo_indicator = 0  
		and b.gl_account_id = gl.gl_account_id
		and (gl.account_type_id <> 12 and b.retirement_unit_id not between 1 and 5)
		and a.work_order_id = woc.work_order_id
		and trim(upper(b.activity_code)) in ('UADD','MADD')
		and b.ldg_depr_group_id < 0    /*##sjh 7/11/2011  Maint 7691*/
		and nvl(a.pend_transaction,0) = 0 
		and to_number(to_char(b.gl_posting_mo_yr,'yyyymm')) = :a_month_number
 		and A.UNIT_TYPE = :a_closing_type  /*##sjh 7/11/2011  Maint 7997*/
		and exists (select 1 from wip_comp_temp_wo c
							where a.work_order_id = c.work_order_id)
		)
		;
		if uf_sqlca('Insert wip_comp_pending_trans from pend trans unitized',messages) = -1 then return -1
			
end choose


//maint 45692: fix this sql to be specific to the work order being unitized
sqls = 'select distinct a.WIP_COMPUTATION_ID from wip_comp_pending_trans a, wip_computation b where a.WIP_COMPUTATION_ID = b.WIP_COMPUTATION_ID and b.unitization_basis_chk = 1 and nvl(a.pend_transaction,0) = 0 and a.work_order_id = ' + string(a_wo_id)
results = reset_array
i_rows = f_get_column(results,sqls)
wip_comps = results

if upperbound(wip_comps) > 0 then
	for i = 1 to upperbound(wip_comps)
		setnull(unitization_basis)
		select unitization_basis
		into :unitization_basis
		from wip_computation
		where wip_computation_id = :wip_comps[i]
		;
		if isnull(unitization_basis) then
			MessageBox("Error", "Error the Unitization Basis and Basis Check fields not in sync for wip_computation_id = " + string(wip_comps[i]))
			return -1
		end if
		f_parsestringintonumarray (unitization_basis,',',unit_basis)
		
		sqls = 'update wip_comp_pending_trans a set a.PEND_BASIS_AMOUNT = (select '
		for j = 1 to upperbound(unit_basis)
			if j = 1 then
				sqls+= 'BASIS_' + string(unit_basis[j])			
			else
				sqls+= ' + BASIS_' + string(unit_basis[j])
			end if
		next
		sqls+= '     from pend_basis b '
		sqls+= '    where a.ORIG_PEND_TRANS_ID = b.pend_trans_id '
		sqls+= '     and a.wip_computation_id = ' + string(wip_comps[i])
		sqls+= '     and nvl(a.pend_transaction,0) = 0 '
		sqls+= '     and exists (select 1 from pend_basis c where a.ORIG_PEND_TRANS_ID = c.pend_trans_id) '
		sqls+= '     and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) ) ' //// JRH 20090224 --> Added Missing Parenthesis
		sqls+= ' where a.work_order_id = ' + string(a_wo_id) + '  '  /*add missing filters for the table being updated!!! arg!!!*/
		sqls+= ' and a.wip_computation_id = ' + string(wip_comps[i]) + '  '
		sqls+= ' and nvl(a.pend_transaction,0) = 0 '
		
		execute immediate :sqls;
		if uf_sqlca('update wip_comp_pending_trans pend basis amount from pend basis unitized. ' + sqls ,false) = -1 then return -1		
		sql_rownum = sqlca.sqlnrows
		
		//if pend_basis_amount is sum zero for a work order id then update to posting amount
		update wip_comp_pending_trans a 
		set a.PEND_BASIS_AMOUNT = a.POSTING_AMOUNT, a.pend_basis_override = 'WO BASIS ZERO USE POSTING AMT'
		where a.work_order_id = :a_wo_id
		and nvl(a.pend_transaction,0) = 0 
		and a.wip_computation_id = :wip_comps[i]
		and a.work_order_id in (select b.work_order_id  
									from wip_comp_pending_trans b
									where nvl(b.pend_transaction,0) = 0
									and b.wip_computation_id = :wip_comps[i]
									and b.work_order_id = :a_wo_id
									group by b.work_order_id
									having sum(nvl(b.pend_basis_amount,0)) = 0
									)   
		;
		if uf_sqlca('update wip_comp_pending_trans pend basis amount from posting amount where basis n = 0'  ,false) = -1 then return -1
		sql_rownum = sqlca.sqlnrows
		
		//if pend_basis_amount is still 0, then use 1 for even split
		update wip_comp_pending_trans a set a.PEND_BASIS_AMOUNT = 1, a.pend_basis_override = 'WO ZERO POSTING AMT DFLT to 1'
		where a.work_order_id = :a_wo_id
		and nvl(a.pend_transaction,0) = 0 
		and a.wip_computation_id = :wip_comps[i]
		and a.work_order_id in (select b.work_order_id 
									from wip_comp_pending_trans b
									where nvl(b.pend_transaction,0) = 0 
									and b.wip_computation_id = :wip_comps[i]
									and b.work_order_id = :a_wo_id
									group by b.work_order_id
									having sum(nvl(b.pend_basis_amount,0)) = 0)  
		;
		if uf_sqlca('update wip_comp_pending_trans pend basis amount = 1 for even split where basis n = 0'  ,false) = -1 then return -1
		sql_rownum = sqlca.sqlnrows
		
	next
end if

//If they are not using specific basis amounts update to posting amount
update wip_comp_pending_trans a set a.PEND_BASIS_AMOUNT = POSTING_AMOUNT
where a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(unitization_basis_chk,0) = 0)
and  nvl(a.pend_transaction,0) = 0 
and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) 
;
if uf_sqlca('update wip_comp_pending_trans pend basis amount from posting amount '  ,false) = -1 then return -1
sql_rownum = sqlca.sqlnrows


/*MOVE THIS INSIDE THE LOOP THAT PERFORMS BASIS SPECIFIC ALLOCATION and ADDED MORE SQL FILTERS!!! */
////if pend_basis_amount is sum zero for a work order id then update to posting amount
//update wip_comp_pending_trans a set PEND_BASIS_AMOUNT = POSTING_AMOUNT, pend_basis_override = 'WO BASIS ZERO USE POSTING AMT'
//where wip_computation_id in (select wip_computation_id from wip_computation where nvl(unitization_basis_chk,0) = 1)
//and  nvl(pend_transaction,0) = 0 
//and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) 
//and work_order_id in (select work_order_id 
//							from wip_comp_pending_trans
//							where nvl(pend_transaction,0) = 0 
//							and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	) 
//							group by work_order_id
//							having sum(nvl(pend_basis_amount,0)) = 0)  
//;
//if uf_sqlca('update wip_comp_pending_trans pend basis amount from posting amount where wo = 0'  ,false) = -1 then return -1
//sql_rownum = sqlca.sqlnrows

//don't want to fail so set pend_basis_amount = 1 if the posting_amount were all 0.00.
update wip_comp_pending_trans a set a.PEND_BASIS_AMOUNT = 1, a.pend_basis_override = 'WO ZERO POSTING AMT DFLT to 1'
where nvl(a.pend_transaction,0) = 0 
and a.work_order_id = :a_wo_id
and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(unitization_basis_chk,0) = 0) 
and (a.work_order_id, a.wip_computation_id) in (select b.work_order_id , b.wip_computation_id
							from wip_comp_pending_trans b
							where nvl(b.pend_transaction,0) = 0 
							and b.work_order_id = :a_wo_id
							and b.wip_computation_id in (select wip_computation_id from wip_computation where nvl(unitization_basis_chk,0) = 0)  
							group by b.work_order_id, b.wip_computation_id 
							having sum(nvl(b.pend_basis_amount,0)) = 0)  
;
if uf_sqlca('update wip_comp_pending_trans pend basis amount from posting amount where wo = 0 for posting amount'  ,false) = -1 then return -1
sql_rownum = sqlca.sqlnrows

	//4.	Allocate UNIT_CALC over the PEND_TRANS
	//ratio_to_report(<amt>) over(Partion by <field>)
	update wip_comp_pending_trans a
	set amount = (select sum(allocated_amt) allocated_amt
				from (
				select wip_comp_summary_id, orig_pend_trans_id, allocated_amt
				from (
				select a.wip_comp_summary_id,a.ORIG_PEND_TRANS_ID,
				ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) alloc_pct,
				b.total_amount,
				round(ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) * b.total_amount,2) allocated_amt
				from wip_comp_pending_trans a, wip_comp_unit_calc b
				where a.wip_comp_summary_id = b.wip_comp_summary_id
				and nvl(a.pend_transaction,0) = 0
				and nvl(b.pend_transaction,0) = 0
				and B.UNIT_TYPE = :a_closing_type  /*##sjh 7/11/2011  Maint 7997*/
				)
				union all /*maint 38389 needs to be "union all"*/
				select a.wip_comp_summary_id, 
				 d.orig_pend_trans_id,
				/*sum(allocated_amt) - b.total_amount allocated_amt   <-- this calc is backwards ###sjh 7/5/2011  Maint 7958*/
				 b.total_amount - sum(allocated_amt) allocated_amt
				from (select a.wip_comp_summary_id,a.ORIG_PEND_TRANS_ID,
				ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) alloc_pct,
				b.total_amount,
				round(ratio_to_report(a.pend_basis_amount) over (partition by a.wip_comp_summary_id) * b.total_amount,2) allocated_amt
				from wip_comp_pending_trans a, wip_comp_unit_calc b
				where a.wip_comp_summary_id = b.wip_comp_summary_id
				and nvl(a.pend_transaction,0) = 0
				and nvl(b.pend_transaction,0) = 0
				and B.UNIT_TYPE = :a_closing_type  /*##sjh 7/11/2011  Maint 7997*/
				) a, wip_comp_unit_calc b, 
				(select min(orig_pend_trans_id) orig_pend_trans_id,d.wip_comp_summary_id from wip_comp_pending_trans d where nvl(d.pend_transaction,0) = 0 group by d.wip_comp_summary_id) d
													
				where a.wip_comp_summary_id = b.wip_comp_summary_id
				and d.wip_comp_summary_id = a.wip_comp_summary_id
				group by a.wip_comp_summary_id, b.total_amount,d.orig_pend_trans_id
				) aa
				where aa.wip_comp_summary_id = a.wip_comp_summary_id
				and aa.orig_pend_trans_id = a.orig_pend_trans_id
	)
	where  nvl(a.pend_transaction,0) = 0
	and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
	;
	if uf_sqlca('update wip_comp_pending_trans set amount from allocation. ' ,messages) = -1 then return -1
	

	
	//5.	Insert Pending Transactions
	//a.	Keep all attributes except gl_account and amounts
	//ADD HERE Need to add for non-timed unitization or 106 processing must pull from pend_trans_archive
	//Logic for Minor Add that doesn't use the GL Account from WIP Computation
	update wip_comp_pending_trans a set gl_account_id = 
	(select decode(nvl(b.post_minor_add,0),1,c.gl_account_id,decode(:a_closing_type, 'non-unitized',non_unit_gl_account,'unitization',unitized_gl_account))
		from wip_computation b, pend_transaction c
		where a.wip_computation_id = b.wip_computation_id
			and c.pend_trans_id = a.orig_pend_Trans_id
		)
	where  nvl(a.pend_transaction,0) = 0
	and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
	;
	if uf_sqlca('update wip_comp_pending_trans set gl account. ' ,messages) = -1 then return -1

	//b.	Retirement Unit option, allows for a specific retirement unit
	update wip_comp_pending_trans a set retirement_unit_id = 
	(select decode(:a_closing_type, 'non-unitized',1,decode(nvl(b.post_minor_add,0),1,c.retirement_unit_id,nvl(b.retirement_unit_id, c.retirement_unit_id)))
		from wip_computation b, pend_transaction c
		where a.wip_computation_id = b.wip_computation_id
		and c.pend_trans_id = a.orig_pend_Trans_id
		)
	where  nvl(a.pend_transaction,0) = 0
	and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
	;
	if uf_sqlca('update wip_comp_pending_trans set retirement unit. ' ,messages) = -1 then return -1
	
	//c.	If option is to use Minor Adds,
	
	update wip_comp_pending_trans a set minor_pend_tran_id = orig_pend_trans_id
	where  nvl(a.pend_transaction,0) = 0
	and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
	and a.wip_computation_id in (select wip_computation_id from wip_computation where nvl(post_minor_add,0) = 1)
	;
	if uf_sqlca('update wip_comp_pending_trans set minor_pend_tran_id. ' ,messages) = -1 then return -1
		
	//d.	Flag as regulatory entries -  pend_transaction.wip_comp_transaction = 1
	//e.	Assign New Pend Trans Id
	delete from wip_comp_pend_trans_temp
	;
	if uf_sqlca('update wip_comp_pend_trans_temp delete. ' ,false) = -1 then return -1

	//EKB 02242011 -- Fix wip_computation_id in insert below to only happen for 106
	//EKB 03142011 -- Fix it again to insert 0 on wip_computation_id as it must be part of the PKEY for this to work
	
	//	insert into wip_comp_pend_trans_temp
	//	(orig_pend_trans_id, work_order_id, month_number,gl_account_id, retirement_unit_id, minor_pend_tran_id, wip_computation_id)
	//	(select distinct orig_pend_trans_id, work_order_id, month_number,gl_account_id, retirement_unit_id, nvl(minor_pend_tran_id,0),wip_computation_id
	//	from wip_comp_pending_trans aa
	//	where  nvl(aa.pend_transaction,0) = 0
	//	and exists (select 1 from wip_comp_temp_wo c where aa.work_order_id = c.work_order_id	)
	//	)
	//	;
	
	//Maint 8241: for "unitized (101)" closings, save the nvl(wip_comp101_rollup_id,0) in the wip_computation_id field
	//                  for "non-unitized (106)" closings, the wip_computation_id value is saved as is 	
	INSERT INTO wip_comp_pend_trans_temp 
					(orig_pend_trans_id, 
					 work_order_id, 
					 month_number, 
					 gl_account_id, 
					 retirement_unit_id, 
					 minor_pend_tran_id, 
					 wip_computation_id,
					 wip_comp101_rollup_id) 
	(SELECT DISTINCT aa.orig_pend_trans_id, 
						  aa.work_order_id, 
						  aa.month_number, 
						  aa.gl_account_id, 
						  aa.retirement_unit_id, 
						  Nvl(aa.minor_pend_tran_id, 0), 
						  Decode(Lower(:a_closing_type), 'non-unitized', aa.wip_computation_id, /*NULL*/ 0) ,
						  Decode(Lower(:a_closing_type), 'non-unitized', null, bb.wip_comp101_rollup_id) 
	 FROM   wip_comp_pending_trans aa, wip_computation bb
	 WHERE  aa.wip_computation_id = bb.wip_computation_id and Nvl(aa.pend_transaction, 0) = 0 
			  AND EXISTS (SELECT 1 
							  FROM   wip_comp_temp_wo c 
							  WHERE  aa.work_order_id = c.work_order_id));  
							  
	if uf_sqlca('update wip_comp_pend_trans_temp insert. ' ,false) = -1 then return -1
	
	update wip_comp_pend_trans_temp
	set new_pend_trans_id = pwrplant1.nextval
	;
	
	if uf_sqlca('update wip_comp_pend_trans_temp update. ' ,false) = -1 then return -1 
	
	//EKB 02242011: Fixing update below to group pending transactions correctly
	
	//	update wip_comp_pending_trans a
	//		set new_pend_trans_id = (select new_pend_trans_id
	//											from wip_comp_pend_trans_temp b
	//											where a.orig_pend_trans_id = b.orig_pend_trans_id
	//											and a.work_order_id = b.work_order_id
	//											and a.month_number = b.month_number
	//											and a.gl_account_id = b.gl_account_id
	//											and a.retirement_unit_id = b.retirement_unit_id
	//											and nvl(a.minor_pend_tran_id,0) = b.minor_pend_tran_id
	//											and a.wip_computation_id = b.wip_computation_id
	//											)
	//		where  nvl(a.pend_transaction,0) = 0
	//		and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
	//		;

		UPDATE wip_comp_pending_trans a 
		SET    new_pend_trans_id = (SELECT new_pend_trans_id 
											 FROM   wip_comp_pend_trans_temp b 
											 WHERE  a.orig_pend_trans_id = b.orig_pend_trans_id 
													  AND a.work_order_id = b.work_order_id 
													  AND a.month_number = b.month_number 
													  AND a.gl_account_id = b.gl_account_id 
													  AND a.retirement_unit_id = 
															b.retirement_unit_id 
													  AND Nvl(a.minor_pend_tran_id, 0) = 
															b.minor_pend_tran_id 
													  AND ( a.wip_computation_id = b.wip_computation_id 
																OR Lower(:a_closing_type) <> 'non-unitized' ) 
													  AND nvl(a.wip_comp101_rollup_id,0) = nvl(b.wip_comp101_rollup_id ,0)
																/*Maint 8241: for "unitized (101)" closings, save the nvl(wip_comp101_rollup_id,0) in the wip_computation_id field*/ 
													 ) 
		WHERE  Nvl(a.pend_transaction, 0) = 0 
				 AND EXISTS (SELECT 1 
								 FROM   wip_comp_temp_wo c 
								 WHERE  a.work_order_id = c.work_order_id);  
								 
		if uf_sqlca('update wip_comp_pending_trans set new_pend_trans_id. ' ,messages) = -1 then return -1	 										
									
	//7.	Insert Related Pend Trans Records
	//ADD HERE Need to add for non-timed unitization or 106 processing must pull from pend_trans_archive
	insert into pend_transaction
	(PEND_TRANS_ID,
	LDG_ASSET_ID,
	LDG_DEPR_GROUP_ID,
	BOOKS_SCHEMA_ID,
	RETIREMENT_UNIT_ID,
	UTILITY_ACCOUNT_ID,
	BUS_SEGMENT_ID,
	FUNC_CLASS_ID,
	SUB_ACCOUNT_ID,
	ASSET_LOCATION_ID,
	GL_ACCOUNT_ID,
	COMPANY_ID,
	GL_POSTING_MO_YR,
	SUBLEDGER_INDICATOR,
	ACTIVITY_CODE,
	GL_JE_CODE,
	WORK_ORDER_NUMBER,
	POSTING_QUANTITY,
	USER_ID1,
	POSTING_AMOUNT,
	IN_SERVICE_YEAR,
	DESCRIPTION,
	LONG_DESCRIPTION,
	PROPERTY_GROUP_ID,
	RETIRE_METHOD_ID,
	POSTING_ERROR,
	POSTING_STATUS,
	COST_OF_REMOVAL,
	SALVAGE_CASH,
	SALVAGE_RETURNS,
	FERC_ACTIVITY_CODE,
	SERIAL_NUMBER,
	RESERVE_CREDITS,
	minor_pend_Tran_id,wip_comp_transaction,wip_computation_id)
	(select distinct b.NEW_PEND_TRANS_ID,
	decode(b.minor_pend_tran_id,null,null,LDG_ASSET_ID),
	a.LDG_DEPR_GROUP_ID,
	a.BOOKS_SCHEMA_ID,
	b.RETIREMENT_UNIT_ID,
	a.UTILITY_ACCOUNT_ID,
	a.BUS_SEGMENT_ID,
	a.FUNC_CLASS_ID,
	a.SUB_ACCOUNT_ID,
	a.ASSET_LOCATION_ID,
	b.GL_ACCOUNT_ID,
	a.COMPANY_ID,
	a.GL_POSTING_MO_YR,
	a.SUBLEDGER_INDICATOR,
	a.ACTIVITY_CODE,
	a.GL_JE_CODE,
	a.WORK_ORDER_NUMBER,
	0 POSTING_QUANTITY,
	a.USER_ID1,
	0 POSTING_AMOUNT,
	a.IN_SERVICE_YEAR,
	a.DESCRIPTION,
	a.LONG_DESCRIPTION,
	a.PROPERTY_GROUP_ID,
	pu.RETIRE_METHOD_ID,
	null POSTING_ERROR,
	1 POSTING_STATUS,
	0 COST_OF_REMOVAL,
	0 SALVAGE_CASH,
	0 SALVAGE_RETURNS,
	a.FERC_ACTIVITY_CODE,
	a.SERIAL_NUMBER,
	0 RESERVE_CREDITS,
	b.minor_pend_Tran_id, 1 wip_comp_transaction,
	decode(b.minor_pend_tran_id,null,decode(lower(:a_closing_type), 'unitization', decode(w.wip_comp101_rollup_id, null, null, w.wip_comp101_rollup_id), b.wip_computation_id),null) wip_computation_id /*<--if minor add, wip_computation_id = null */ 
	/*Maint 8241: for "unitized (101)" closings, save the wip_comp101_rollup_id in the wip_computation_id field*/ 
	from pend_transaction a, wip_comp_pending_trans b, property_unit pu, retirement_Unit ru, wip_computation w
	where b.orig_pend_trans_Id = a.pend_trans_id
	and nvl(b.pend_transaction,0) = 0
	and w.wip_computation_id = b.wip_computation_id 
	and b.retirement_Unit_id = ru.retirement_unit_id
	and ru.property_unit_id = pu.property_unit_id
		and exists (select 1 from wip_comp_temp_wo c where b.work_order_id = c.work_order_id	)
	)	;
	if uf_sqlca('insert pending transactions for wip comp close.' ,messages) = -1 then return -1	
	
	//-- Pend Basis
	
	//Get Max Buckets from Book Summary
	select count(*) into :num_buckets from book_summary;
	if uf_sqlca('Get Count of Book Summary' ,false) = -1 then return -1
	
	sqls = ''
	sqls = 'insert into pend_basis (pend_trans_id '
	for k = 1 to num_buckets
		sqls+= ', basis_' + string(k)
	next
	sqls += ' ) '
	sqls += ' ( select b.new_pend_trans_id '
	for k = 1 to num_buckets
		sqls+= ', sum(decode(b.book_summary_id, ' + string(k) + ', amount, 0))'
	next
	sqls += ' from wip_comp_pending_trans b, pend_transaction c where b.new_pend_trans_id = c.pend_trans_id  '
	sqls+= ' and nvl(b.pend_transaction,0) = 0 '
		sqls+= '  and exists (select 1 from wip_comp_temp_wo c where b.work_order_id = c.work_order_id	) '
		sqls+= ' group by b.new_pend_trans_id ) '
	
	execute immediate :sqls;
	if uf_sqlca('insert into pend_basis for wip comp close ' + sqls ,false) = -1 then return -1
	
	//Update Pend Transaction with Posting Amount
	sqls = ''
	sqls = 'update pend_transaction c set posting_amount = (select sum( '
	for k = 1 to num_buckets
		if k = 1 then
			sqls+= 'nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
		else
			sqls+= ' + nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
		end if
	next
	sqls += ') from pend_basis a, set_of_books b  '
	sqls += ' where c.pend_trans_id =  a.pend_trans_id '
	sqls += ' and b.set_of_books_id = 1 '
	sqls += ') ' // end of sum select
	sqls += ' where c.pend_trans_id in (select new_pend_trans_id from wip_comp_pending_trans bb  '
	sqls += ' 									where bb.new_pend_trans_id = c.pend_trans_id '
	sqls+= ' 										and nvl(bb.pend_transaction,0) = 0 '
	sqls+= '  								and exists (select 1 from wip_comp_temp_wo cc where bb.work_order_id = cc.work_order_id	) ) '
		
	execute immediate :sqls;
	if uf_sqlca('update pend transaction posting amount from pend_basis for wip comp close ' + sqls ,false) = -1 then return -1
	
//	JRH 20090224 --> Distinct 
	insert into class_code_pending_trans
	(class_code_Id, pend_Trans_id, value)
	(select distinct class_code_Id, new_pend_Trans_id, value
	from  class_code_pending_trans a, wip_comp_pending_trans b
		where b.orig_pend_trans_Id = a.pend_trans_id
		and nvl(b.pend_transaction,0) = 0
			and exists (select 1 from wip_comp_temp_wo c where b.work_order_id = c.work_order_id	)
	)
	;
	if uf_sqlca('insert into pend class code table for wip comp close ' ,false) = -1 then return -1
	
	
	//a.	Option for related assets
	//Maint 37086:  only create related asset data for 101 transactions
	if lower(a_closing_type) = 'unitization' then
		insert into pend_related_asset
		(pend_related_asset_id, pend_trans_id, related_pend_trans_id, related_asset_type_id, co_retire)
		(select distinct new_pend_trans_id, orig_pend_trans_id,new_pend_trans_id,c.related_asset_option, b.co_retire
		from wip_comp_pending_trans a, related_asset_type b, wip_computation c
		where a.wip_computation_id = c.wip_computation_id
		and c.related_asset_option = b.related_type_id
		and a.retirement_unit_id > 1
		and nvl(c.related_asset_option,0) <> 0
			and nvl(a.pend_transaction,0) = 0
			and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
		)
		;
		if uf_sqlca('insert into pend related asset for wip comp close ' ,messages) = -1 then return -1
	end if 
	
	//8.	Mark Records as Pending Status
	
	update wip_comp_pending_trans a
	set pend_transaction = 100
	where nvl(a.pend_transaction,0) = 0
		and exists (select 1 from wip_comp_temp_wo c where a.work_order_id = c.work_order_id	)
		and exists (select 1 from pend_transaction b where b.pend_trans_id = a.new_pend_trans_id)
	;
	if uf_sqlca('update wip_comp_pend_trans pend_transaction ' ,messages) = -1 then return -1
	update wip_comp_unit_calc aa
	set pend_transaction = 100
	where nvl(aa.pend_transaction,0) = 0
		and exists (select 1 from wip_comp_temp_wo c where aa.work_order_id = c.work_order_id	)
		and exists (select 1 from wip_comp_pending_trans a, pend_transaction b where b.pend_trans_id = a.new_pend_trans_id
						and aa.wip_comp_summary_id = a.wip_comp_summary_id)

	;
	if uf_sqlca('update wip_comp_unit_calc pend_transaction ' ,messages) = -1 then return -1
	
	choose case lower(a_closing_type)
		case 'non-unitized'	
			update wip_comp_charges aa
			set non_unitized_status = 100
			where nvl(aa.non_unitized_status,0) = 0
			and exists (select 1 from wip_comp_temp_wo c where aa.work_order_id = c.work_order_id	)
			and exists (select 1 from wip_comp_pending_trans a, pend_transaction b where b.pend_trans_id = a.new_pend_trans_id
							and aa.wip_comp_summary_id = a.wip_comp_summary_id)

			;
			if uf_sqlca('update wip_comp_charges pend_transaction ' ,messages) = -1 then return -1
		case 'unitization'	
			update wip_comp_charges aa
			set unitized_status = 100
			where nvl(aa.unitized_status,0) = 0
			and exists (select 1 from wip_comp_temp_wo c where aa.work_order_id = c.work_order_id	)
			and exists (select 1 from wip_comp_pending_trans a, pend_transaction b where b.pend_trans_id = a.new_pend_trans_id
							and aa.wip_comp_sum_unitize_id = a.wip_comp_summary_id)

			;
			if uf_sqlca('update wip_comp_charges pend_transaction ' ,messages) = -1 then return -1
				
	end choose
	
	//Now Mark Pending Transaction Back to Original Status so they can get approved
	update pend_transaction
	set posting_status = 1
	where pend_trans_id in (select orig_pend_trans_id from wip_comp_pending_trans
											where nvl(pend_transaction,0) = 100)
	and posting_status = 7
	;
	if uf_sqlca('update pend_transaction set posting status to 7 on existing pend transactions',false) = -1 then return -1
		
//Call Extension Function
	
	ext_rtn = f_wip_comp_extension(-1,a_month_number, 'closing_end',a_wo_id,a_company_id)
	
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_extension for wip computation closing start")
		return -1
	end if
	
	//Call Extensions:  ---- good to update not eligible flag or insert additional work order ids
ext_rtn = 0
ext_rtn = f_wip_comp_dynamic_ext(-1,a_month_number, 'closing-after-pending',a_wo_id,a_company_id)

if ext_rtn = -1 then
	uf_msg("Error Occured in f_wip_comp_dynamic_ext for wip computation = " + description)
	return -1
end if

	
return 1
end function

public function integer uf_wip_comp_closing_from_archive (longlong a_company_id, longlong a_month_number, string a_closing_type);return 1
end function

public function integer uf_wip_comp_calc (longlong a_company_id, longlong a_month_number);
/************************************************************************************************************************************************************
**
** uf_wip_comp_calc()
** 
** Performs the wip computation calculation for a given company and month
** 
** Parameters : 	longlong 	:	(a_company_id) The company ID of the company being processed.
**                       string	:	(a_msg) The error message to display to the user if an error occurrs (in addition to the Oracle-returned error).
** 
** Returns : (integer) 	:	-1	: There was an error.
**                                  	1 	: There was no error.
**
************************************************************************************************************************************************************/

longlong tot_computations, wip_computation_ids[], check2, i, cost_element_id, wip_gl_account, offset_gl_account, non_unit_gl_account, unitized_gl_account, &
	company_id_from, oh_basis_id, juris_cwip_base_opt, jurisdiction_id, charge_option_id, unitization_basis_chk, related_asset_option, retirement_unit_id, &
	expenditure_type_id, bud_oh_basis_id, company_id_to, cmpd_jan, cmpd_feb, cmpd_mar, cmpd_apr, cmpd_may, cmpd_jun, cmpd_jul, cmpd_aug, &
	cmpd_sep, cmpd_oct, cmpd_nov, cmpd_dec, half_month_option, jur_allo_book_id, post_cwip, post_gl_entries, post_minor_add, check, ext_rtn, &
	compound_month_number, cmpd
string description, comp_type, unitization_basis, sql_eligibility, isd_compound_option, sqls, stop_option
any results[]
dec{2} cwip_in_base_to_alloc, total_base, total_allocated

uf_msg('Starting WIP Computations for Company_id ' + string(a_company_id))

tot_computations = f_get_column(results,"select wip_computation_id from wip_computation where valid_for_budget in (0,1) and company_id_from = " + string(a_company_id) + "	and lower(comp_type) <> 'external' " +" order by priority ")  //JRD  ###6295

if tot_computations < 0 then
	uf_msg('  Error Getting Computations')
	uf_sqlca('Erorr Details',false)
	return -1
elseif tot_computations = 0 then 
	uf_msg('  No WIP Computations for this Company')
	return 1
else
	uf_msg('  Total WIP Computations ' + string(tot_computations))
end if

wip_computation_ids = results

check2 = 0
select count(*) into :check2
from clearing_wo_control
where clearing_id in (select wip_computation_id 
						from wip_computation 
						where company_id_From = :a_company_id)
;

if isnull(check2) then check2 = 0
if check2 > 0 then
	uf_msg('Computation id = Same as Clearing Id, this is an invalid configuration and will cause issues wtih Overhead Calcuatoins, calculation stopped, please re-configure!')
	return -1
end if

//Maint 8726
check2 = 0
select count(*) into :check2
from repair_schema
where repair_schema_id in (select wip_computation_id 
						from wip_computation 
						where company_id_From = :a_company_id);

if isnull(check2) then check2 = 0
if check2 > 0 then
	uf_msg('Computation id = Same as Repair Schema Id, this is an invalid configuration and will cause issues wtih Tax Repairs, calculation stopped, please re-configure!')
	return -1
end if

check2 = 0
select count(*) into :check2
from wip_computation
where wip_computation_id in (8,9,1, -99, -100)
and company_id_From = :a_company_id;

if isnull(check2) then check2 = 0
if check2 > 0 then
	uf_msg('Computation id = 8,9, 1, -99, or -100  This is an invalid configuration and will cause issues wtih Afudc/Accruals, calculation stopped, please re-configure!')
	return -1
end if


uf_msg(' Clear Prior Calculation Runs ')
delete from wip_comp_calc
where wip_computation_id in (select wip_computation_id 
						from wip_computation 
						where company_id_From = :a_company_id
						and lower(comp_type) <> 'external') //JRD  ###6295
and company_id = :a_company_id
and month_number = :a_month_number
;
if uf_sqlca('Retrieve Wip Comp Calc Delete',true) = -1 then return -1

delete from wip_comp_charges
where wip_computation_id in (select wip_computation_id 
						from wip_computation 
						where company_id_From = :a_company_id
						and lower(comp_type) <> 'external') //JRD  ###6295
and company_id = :a_company_id
and month_number = :a_month_number
;
if uf_sqlca('Retrieve Wip Comp Charges Delete',false) = -1 then return -1

delete from cwip_charge
where status in (select wip_computation_id 
						from wip_computation 
						where company_id_From = :a_company_id
						and lower(comp_type) <> 'external') //JRD  ###6295
and company_id = :a_company_id
and month_number = :a_month_number
;
if uf_sqlca('Retrieve Cwip Charges Delete',false) = -1 then return -1



for i = 1 to tot_computations
	i_wip_comp = wip_computation_ids[i]
	select DESCRIPTION,	COMP_TYPE,	COST_ELEMENT_ID,	WIP_GL_ACCOUNT,	OFFSET_GL_ACCOUNT,	NON_UNIT_GL_ACCOUNT,
	UNITIZED_GL_ACCOUNT,	COMPANY_ID_FROM,	OH_BASIS_ID,	JURIS_CWIP_BASE_OPT,	JURISDICTION_ID,	CHARGE_OPTION_ID,
	UNITIZATION_BASIS,	UNITIZATION_BASIS_CHK,	RELATED_ASSET_OPTION,	RETIREMENT_UNIT_ID,	EXPENDITURE_TYPE_ID,
	OH_BASIS_BDG_ID,	COMPANY_ID_TO,	SQL_ELIGIBILITY,	nvl(CMPD_JAN,0), nvl(	CMPD_FEB,0), nvl(	CMPD_MAR,0), nvl(	CMPD_APR,0), nvl(	CMPD_MAY,0), 
	nvl(	CMPD_JUN,0), nvl(	CMPD_JUL,0), nvl(	CMPD_AUG,0), nvl(	CMPD_SEP,0), nvl(	CMPD_OCT,0), nvl(	CMPD_NOV,0), nvl( CMPD_DEC,0),  half_month_option, //jrd 2/25/09 add nvl to 0 - no
	jur_allo_book_id,post_cwip,post_gl_entries, nvl(lower(isd_compound_option),'skip'), nvl(stop_option,'3'), post_minor_add
	into :DESCRIPTION,:COMP_TYPE,:COST_ELEMENT_ID,:WIP_GL_ACCOUNT,:OFFSET_GL_ACCOUNT,:NON_UNIT_GL_ACCOUNT,:UNITIZED_GL_ACCOUNT,
	:COMPANY_ID_FROM,:OH_BASIS_ID,:JURIS_CWIP_BASE_OPT,:JURISDICTION_ID,:CHARGE_OPTION_ID,:UNITIZATION_BASIS,:UNITIZATION_BASIS_CHK,
	:RELATED_ASSET_OPTION,:RETIREMENT_UNIT_ID,:EXPENDITURE_TYPE_ID,:BUD_OH_BASIS_ID,:COMPANY_ID_TO,:SQL_ELIGIBILITY,
	:CMPD_JAN,:CMPD_FEB,:CMPD_MAR,:CMPD_APR,:CMPD_MAY,:CMPD_JUN,
	:CMPD_JUL,:CMPD_AUG,:CMPD_SEP,:CMPD_OCT,:CMPD_NOV,:CMPD_DEC, 
	:half_month_option,:jur_allo_book_id,:post_cwip,:post_gl_entries, :isd_compound_option, :stop_option, :post_minor_add
	from wip_computation
	where wip_computation_id = :wip_computation_ids[i]
	;
	
	if uf_sqlca('Retrieve Wip Computation Details',false) = -1 then return -1
	
	
	uf_msg('Computation ' + string(i) + ' of ' + string(tot_computations) + ' : ' + description)
	
	
	if i_wip_comp = 1 then //this is bad AFUDC has a status on cwip_charge = 1 don't allow to move forward
		uf_msg('Computation id = 1, this is an invalid configuration and will cause issues wtih AFUDC, calculation stopped, please re-configure!')
		return -1
	end if
	
	check2 = 0
	select count(*) into :check2
	from clearing_wo_control
	where clearing_id = :i_wip_comp
	;
	
	if isnull(check2) then check2 = 0
	if check2 > 0 then
		uf_msg('Computation id = Same as Clearing Id, this is an invalid configuration and will cause issues wtih Overhead Calcuatoins, calculation stopped, please re-configure!')
		return -1
	end if
	
	//Call Extension Function
	ext_rtn = f_wip_comp_extension(i_wip_comp,a_month_number, 'calc',-1,a_company_id)
	
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_extension for wip computation = " + description)
		return -1
	end if	 
	
	// Call Dynamic Validation option
	f_pp_msgs("Calling dynamic validation.")
	string args[]
	args[1] = string(a_month_number)
	args[2] = string(a_company_id)
	
	ext_rtn = f_wo_validation_control(1059,args) //see https://powerme.pwrplan.com/display/PCM/Dynamic+Validation+Types for dynamic validation ID values
	
	if ext_rtn < 0 then
		f_pp_msgs("WIP Computations (Dynamic Validations) Failed.")
		return -1
	end if
	
	// Successful Validation
	commit;
	
	//CWB 2-23-2010 - sync the wo_override values based on the related wip computation
		delete from wip_comp_wo_override 
		where wip_computation_id in (
				select wip_computation_id 
				from wip_computation 
				where related_wip_comp_id is not null)
		and wip_computation_id = :i_wip_comp
		;
		//Maint 7680
		if uf_sqlca('Delete from wip_comp_wo_override ',false) = -1 then return -1
		
		insert into wip_comp_wo_override (wip_computation_id, work_order_id, include_exclude)
		select b.wip_computation_id, a.work_order_id,  a.include_exclude
		from wip_comp_wo_override a,
				wip_computation b 
		where a.wip_computation_id = b.related_wip_comp_id
		and b.wip_computation_id = :i_wip_comp
		;
		//Maint 7680
		if uf_sqlca('Insert into wip_comp_wo_override ',false) = -1 then return -1
	
//	3.	Insert the eligible work orders based eligibility tables and eligibility sql
	
	
	uf_msg(' Determine Eligible Work Orders ')
	
	select count(*) into :check from  wip_comp_wo_type;

	// lkk  if wip_comp_wo_type is empty the sql takes a lot longer
	if check > 0 then 
		sqls = " "
		sqls += " insert into wip_comp_calc "
		sqls += " (wip_computation_id, work_order_id, month_number, company_id) "
		sqls += " select " + string(i_wip_comp) + ", work_order_id,  " + string(a_month_number) + ", " + string(a_company_id)
		sqls += " from work_order_control "
		sqls += " where company_id = " + string(a_company_id)
		sqls += " and  ( exists (select 1 "
		sqls += " 					from wip_comp_wo_type a "
		sqls += " 					where a.work_order_type_id = work_order_control.work_order_type_id "
		sqls += " 					and a.wip_computation_id = " + string(i_wip_comp) + " )"
		sqls += " 	or exists (select 1 "
		sqls += " 				from wip_comp_wo_override a "
		sqls += " 				where a.work_order_id = work_order_control.work_order_id "
		sqls += " 					and a.wip_computation_id = " + string(i_wip_comp) 
		sqls += " 					and include_exclude = 1) "
		sqls += " 		) "
		sqls += " and not exists (select 1  "
		sqls += " 				from wip_comp_wo_override a "
		sqls += " 				where a.work_order_id = work_order_control.work_order_id "
		sqls += " 					and a.wip_computation_id =  " + string(i_wip_comp)
		sqls += " 					and include_exclude = 0) "
	else
		sqls = " "
		sqls += " insert into wip_comp_calc "
		sqls += " (wip_computation_id, work_order_id, month_number, company_id) "
		sqls += " select " + string(i_wip_comp) + ", work_order_id,  " + string(a_month_number) + ", " + string(a_company_id)
		sqls += " from work_order_control "
		sqls += " where company_id = " + string(a_company_id)
		 sqls += "  and exists (select 1 "
		sqls += " 				from wip_comp_wo_override a "
		sqls += " 				where a.work_order_id = work_order_control.work_order_id "
		sqls += " 					and a.wip_computation_id = " + string(i_wip_comp) 
		sqls += " 					and include_exclude = 1) "
		sqls += " and not exists (select 1  "
		sqls += " 				from wip_comp_wo_override a "
		sqls += " 				where a.work_order_id = work_order_control.work_order_id "
		sqls += " 					and a.wip_computation_id =  " + string(i_wip_comp)
		sqls += " 					and include_exclude = 0) "
		
	end if
	
	if not isnull(sql_eligibility) then
		//##EKB 20110119 Maint# Replace key word  <month_number>
		sql_eligibility = f_replace_string(sql_eligibility,"<month_number>", string(a_month_number),'all')				
		sqls += sql_eligibility
	end if
		
	execute immediate :sqls;
	
	if uf_sqlca('Retrieve Wip Comp Calc Eligibility',true) = -1 then 
		uf_msg(sqls)
		return -1
	end if
	
	
	//Update Records based on Stop Options:
	
	choose case stop_option 
		case "1"
			//Stop Option = 1 = Stop at In-Service
		
			//Not Eligble Flags
			// 1 = Not Eligible
			// 2 = Eligible for ISD Adjustment
			
			//Maint 29573:  added closing_option_id = 5
	
			update wip_comp_calc a
			set (not_eligible_flag, calc_in_service_date, calculation_notes) =
			
			(select 2, in_service_date, trim(a.calculation_notes||' '||'Stop at In-Service Non-Blanket - Calculated ISD Adjustments;')
				from work_order_control b where a.work_order_id = b.work_order_id)
			where exists (select 1 from work_order_control c, work_order_account d
							where c.work_order_id = d.work_order_id
							and c.work_order_id = a.work_order_Id
							and d.closing_option_Id in (1,2,3,4,5)
							and c.in_service_date is not null
							and to_number(to_char(c.in_service_date,'yyyymm')) <= :a_month_number ) //JRD 2010-09-08 */
			and wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			;
			
			if uf_sqlca('Wip Comp Calc Stop Option 1 - Non Blankets ',false) = -1 then return -1
			

			update wip_comp_calc a
			set not_eligible_flag = 0
			where not_eligible_flag = 2
			and to_number(to_char(calc_in_service_date,'yyyymm')) = :a_month_number
			and wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			;
			
			if uf_sqlca('Wip Comp Calc Stop Option 1 - Non Blankets ',false) = -1 then return -1
	
			//Stop Option = 1 = Stop at In-Service
			
				//Not Eligble Flags
				// 1 = Not Eligible
				// 2 = Eligible for ISD Adjustment
			
			update wip_comp_calc a
			set (not_eligible_flag, calc_in_service_date, calculation_notes) =
			
			(select 1, null, trim(a.calculation_notes||' '||'Stop at In-Service Blanket - No Calculation;')
				from work_order_control b where a.work_order_id = b.work_order_id)
			where exists (select 1 from work_order_control c, work_order_account d
							where c.work_order_id = d.work_order_id
							and c.work_order_id = a.work_order_Id
							and d.closing_option_Id > 5)
			and wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			;
			
			if uf_sqlca('Wip Comp Calc Stop Option 1 Blankets',false) = -1 then return -1
		case "2"
			//Stop Option = 2 = Start at In-Service
		
			//Not Eligble Flags
			// 1 = Not Eligible
			// 2 = Eligible for ISD Adjustment
			
			//Maint 29573:  added closing_option_id = 5
	
			update wip_comp_calc a
			set (not_eligible_flag, calc_in_service_date, calculation_notes) =
			
			(select 1, in_service_date, trim(a.calculation_notes||' '||'Start at In-Service Non-Blanket - No ISD Date - No Calc;')
				from work_order_control b where a.work_order_id = b.work_order_id)
			where exists (select 1 from work_order_control c, work_order_account d
							where c.work_order_id = d.work_order_id
							and c.work_order_id = a.work_order_Id
							and d.closing_option_Id in (1,2,3,4,5 )
							and c.in_service_date is null)
			and wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			;
			
			if uf_sqlca('Wip Comp Calc Stop Option 2 - Non Blankets ',false) = -1 then return -1
			
	
			//Stop Option = 2 = Start at In-Service
			//Blanket case not needed, they are in-service immediately.
			//No catch up is done for late in-service reporting, only retro-reversal for past in-service reporting
	end choose
			
	//Call Extensions:  ---- good to update not eligible flag or insert additional work order ids
	ext_rtn = 0
	ext_rtn = f_wip_comp_dynamic_ext(i_wip_comp,a_month_number, 'calc-after-eligibility',-1,a_company_id)
	
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_dynamic_ext for wip computation = " + description)
		return -1
	end if
	
//	4.	Calculate Amounts on the Calc Table
	//Now Need to Address with the Compuation Variables
	//Comp Types
		//straight_rate
		//jur_afudc
		//jur_afudc_offset
		//post_in_service_cost
	
	//Charge Option ID
		//1 - Current Month
		//2 - Balance
	
	//Half Month Option
		//1 = Yes
		//0 = No
	
	//Stop option
	// 1 = Stop at In-Service
	// 2 = Start at In-Service
	// 3 = Always
	
	//ISD Compound Option
		//skip
		//full
		//half
		//day
		//none
		
		

	//		a.	AFUDC Base from AFUDC_CALC
	//		b.	JURIS CWIP in Base from CWIP_IN_RATE_BASE
	//		c.	Base Amount from WO_SUMMARY based on OH_BASIS tables
	//		d.	JURIS Percent from Jurisdicational tables
	//		e.	Rate from WIP_COMP_RATE

	
	update wip_comp_calc 
	set RATE = 0,
	AFUDC_BASE = 0,
	JUR_CWIP_IN_BASE = 0,
	BASE_AMOUNT = 0,
	COMPOUND_BASE = 0,
	COMPOUND_CALC = 0,
	UNCOMPOUND_BASE = 0,
	COMPOUND_RATE = 0,
	CALC_AMOUNT = 0,
	JURIS_PCT = 0
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	;
	
	if uf_sqlca('Wip Comp Calc Zeros',false) = -1 then return -1

	// CWB 20100120: added an option to default to the afudc_debt or afudc_equity rate
	
	//update the rate where rate_rule = 0
	update wip_comp_calc a
	set rate =
		(select rate from wip_comp_rate b
		where wip_computation_id = :i_wip_comp 
			and effective_date = (select max(effective_date) from wip_comp_rate
										where wip_computation_id = :i_wip_comp
										and to_number(to_char(effective_date,'yyyymm')) <= :a_month_number
										)
			and a.wip_computation_id = b.wip_computation_id
			and nvl(b.rate_rule, 0) = 0
		)
	where exists	 (select rate from wip_comp_rate b
					 		where wip_computation_id = :i_wip_comp 
							and effective_date = (select max(effective_date) from wip_comp_rate
													where wip_computation_id = :i_wip_comp
													and to_number(to_char(effective_date,'yyyymm')) <= :a_month_number
													)
							and a.wip_computation_id = b.wip_computation_id
							and nvl(b.rate_rule, 0) = 0
						)
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0
	and a.wip_computation_id = :i_wip_comp //CWB 20100721 added wip_comp filter
	;
	if uf_sqlca('Updating the rate where the rate_rule = 0 ',true) = -1 then return -1

	//update the rate where rate_rule = 1 2 or 3
	///The 3 option doesn't actually exist as an option on the wip comp setup window but just putting the code here to stay in sync with compound update below
	//### - 6331 - Changed to < to <=
	update wip_comp_calc a
	  set rate =
			(select decode(d.rate_rule,1,debt_rate,2,equity_rate,3,debt_rate + equity_rate)  //CWB 20100721 Changed to a decode for all three rate rules
			from afudc_data b,
					  work_order_account c,
					  wip_comp_rate d               
			where c.work_order_id = a.work_order_id
				 and c.afudc_type_id = b.afudc_type_id
				and nvl(d.rate_rule,0) in (1,2,3)
				and d.effective_date = (select max(effective_date) 
												from wip_comp_rate dd 
												where d.wip_computation_id = dd.wip_computation_id 
												and dd.effective_date <= to_date(:a_month_number,'yyyymm')
											)
				and b.effective_date = (select max(bb.effective_date)
															from afudc_data bb                                               
									where b.afudc_type_id = bb.afudc_type_id
									and bb.effective_date <= to_date(:a_month_number,'yyyymm')
															  )
				 and wip_computation_id = :i_wip_comp  //CWB 20100721 Added filter for wip comp
				 and company_id = :a_company_id
				 and month_number = :a_month_number
				 and nvl(not_eligible_flag,0) = 0)
	  where exists (select 1
					from wip_comp_rate d               
					where wip_computation_id = :i_wip_comp
					and d.effective_date = (select max(effective_date) 
												from wip_comp_rate dd 
												where d.wip_computation_id = dd.wip_computation_id 
												and dd.effective_date <= to_date(:a_month_number,'yyyymm')
												)
							 and nvl(d.rate_rule,0) in (1,2,3)  //CWB 20100721 Changed to a decode for all three rate rules
				)
	  and nvl(a.not_eligible_flag,0) = 0
	  and wip_computation_id = :i_wip_comp
	  and month_number= :a_month_number	  
	;

	if uf_sqlca('Updating the rate where the rate_rule in (1,2,3) ',true) = -1 then return -1

	
	//update the comp_rate where comp_rate_rule = 0
	update wip_comp_calc a
	set compound_rate =
		(select compound_rate from wip_comp_rate b
		where wip_computation_id = :i_wip_comp 
			and effective_date = (select max(effective_date) from wip_comp_rate
										where wip_computation_id = :i_wip_comp
										and to_number(to_char(effective_date,'yyyymm')) <= :a_month_number
										)
			and a.wip_computation_id = b.wip_computation_id
			and nvl(b.comp_rate_rule, 0) = 0
		)
	where exists	 (select compound_rate from wip_comp_rate b
					 where wip_computation_id = :i_wip_comp 
						and effective_date = (select max(effective_date) from wip_comp_rate
													where wip_computation_id = :i_wip_comp
													and to_number(to_char(effective_date,'yyyymm')) <= :a_month_number
													)
		and a.wip_computation_id = b.wip_computation_id
		and nvl(b.comp_rate_rule, 0) = 0
	)
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0
	and a.wip_computation_id = :i_wip_comp //CWB 20100721 Added filter for wip comp
	;

	if uf_sqlca('Updating the compound_rate where the comp_rate_rule = 0 ',true) = -1 then return -1
	
	//update the comp_rate where comp_rate_rule in (1,2,3)
	update wip_comp_calc a
	  set compound_rate =
			(select decode(d.comp_rate_rule,1,debt_rate,2,equity_rate,3,debt_rate + equity_rate) //CWB 20100721 Changed to a decode for all three rate rules
			from afudc_data b,
					  work_order_account c,
					  wip_comp_rate d               
			where c.work_order_id = a.work_order_id
				 and c.afudc_type_id = b.afudc_type_id
				 and nvl(d.comp_rate_rule,0) in (1,2,3)
				and d.effective_date = (select max(effective_date) 
												from wip_comp_rate dd 
												where d.wip_computation_id = dd.wip_computation_id 
												and dd.effective_date <= to_date(:a_month_number,'yyyymm')  /*Maint 9739*/
											)
				and b.effective_date = (select max(bb.effective_date)
															from afudc_data bb                                               
									where b.afudc_type_id = bb.afudc_type_id
									and bb.effective_date <= to_date(:a_month_number,'yyyymm')   /*Maint 9739*/
															  )
				 and wip_computation_id = :i_wip_comp
				 and company_id = :a_company_id
				 and month_number = :a_month_number
				 and nvl(not_eligible_flag,0) = 0)
	  where exists (select 1
					from wip_comp_rate d               
					where wip_computation_id = :i_wip_comp
					and d.effective_date = (select max(effective_date) 
												from wip_comp_rate dd 
												where d.wip_computation_id = dd.wip_computation_id 
												and dd.effective_date <= to_date(:a_month_number,'yyyymm')  /*Maint 9739*/
												)
							 and nvl(d.comp_rate_rule,0) in (1,2,3) //CWB 20100721 Changed to a decode for all three rate rules
				)
	  and nvl(a.not_eligible_flag,0) = 0
	and a.company_id = :a_company_id
	and a.month_number = :a_month_number
	and a.wip_computation_id = :i_wip_comp 	  //CWB 20100721 Added filter for wip comp
	;

	if uf_sqlca('Updating the compound_rate where the comp_rate_rule in (1,2,3) ',true) = -1 then return -1
	
	//Compute Base Amount
	//Charge Option ID
		//1 - Current Month
		//2 - Balance
	
	//Half Month Option - half_month_option
		//1 = Yes
		//0 = No
		

	update wip_comp_calc a 
	set (BASE_AMOUNT,CURRENT_MONTH_BASE_AMOUNT,HALF_MONTH_OPTION) =  //jrd 2-25-09 add current month data
	(select sum(decode(b.month_number,:a_month_number,0,b.amount)) base_amount,
			 sum(decode(b.month_number,:a_month_number,decode(:half_month_option,1,.5 * b.amount,b.amount),0)) current_month_base_amount , 
			:half_month_option	
		from cwip_charge b, oh_alloc_basis c, work_order_control woc
		where overhead_basis_id = :OH_BASIS_ID
		and a.work_order_id = b.work_order_id
		and b.work_order_id = woc.work_order_id
		and b.expenditure_type_id = c.expenditure_type_id
		and b.charge_type_id = c.charge_type_id
		and b.month_number <= :a_month_number
		and b.month_number >= decode(:CHARGE_OPTION_ID,1,:a_month_number,0)
	)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc Base Amount Set',false) = -1 then return -1
	
	update wip_comp_calc a 
	set (AFUDC_BASE) = 
	(select afudc_base_used_in_calc
		from afudc_calc b
		where a.work_order_id = b.work_order_id
		and b.month = to_date(:a_month_number,'yyyymm')
	)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc AFUDC Base Set',false) = -1 then return -1
	
	
	update wip_comp_calc a 
	set (JUR_CWIP_IN_BASE) = 
	(select nvl(cwip_in_base,0)
		from cwip_in_rate_base b
		where a.work_order_id = b.work_order_id
		and b.effective_date = to_date(:a_month_number,'yyyymm')
		and b.jurisdiction_id = :JURISDICTION_ID
	)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc JUR CWIP in Base Set',false) = -1 then return -1
	
	if not isnull(jur_allo_book_id) then
		//need to set juris_pct
		update wip_comp_calc 
		set 	JURIS_PCT = (select jur_allo_book_pct
								from jur_allo_book
								where jur_allo_book_id = :jur_allo_book_id
								and jurisdiction_id = :jurisdiction_id
								and effective_date = (select max(effective_date) from jur_allo_book
										where jur_allo_book_id = :jur_allo_book_id
										and to_number(to_char(effective_date,'yyyymm')) <= :a_month_number
										)
								)
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and nvl(not_eligible_flag,0) = 0;
		
		if uf_sqlca('Wip Comp Calc JUR PCT 1',false) = -1 then return -1
	else
		update wip_comp_calc 
		set 	JURIS_PCT = 1
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and nvl(not_eligible_flag,0) = 0;
		
		if uf_sqlca('Wip Comp Calc JUR PCT 2',false) = -1 then return -1
	end if
	
	
	
//	5.	Allocate a CWIP in Base Amount if it is on the WIP_COMP_RATE table

	select cwip_in_base_alloc
	into :cwip_in_base_to_alloc
	from wip_comp_rate
	where wip_computation_id = :i_wip_comp 
	and effective_date = (select max(effective_date) from wip_comp_rate
								where wip_computation_id = :i_wip_comp
								and to_number(to_char(effective_date,'yyyymm')) <= :a_month_number
								)
	;
	
	if uf_sqlca('Wip Comp Calc CWIP to Alloc',false) = -1 then return -1
	
	if cwip_in_base_to_alloc <> 0 and not isnull(cwip_in_base_to_alloc) then
		//get total base amount
		select sum(nvl(base_amount,0) + nvl(current_month_base_amount,0))
		into :total_base
		from wip_comp_calc
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and nvl(not_eligible_flag,0) = 0;
		
		if total_base <> 0 and not isnull(total_base) then
			update wip_comp_calc
			set alloc_cwip_in_base = ((nvl(base_amount,0) + nvl(current_month_base_amount,0)) / :total_base) * (:cwip_in_base_to_alloc)
			where wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			and nvl(not_eligible_flag,0) = 0;
			if uf_sqlca('Wip Comp Calc alloc_cwip_in_base 1',false) = -1 then return -1
			
			
			select sum(nvl(alloc_cwip_in_base,0))
			into :total_allocated
			from wip_comp_calc
			where wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			and nvl(not_eligible_flag,0) = 0;
			
			if total_allocated <> cwip_in_base_to_alloc then
				cwip_in_base_to_alloc = cwip_in_base_to_alloc - total_allocated
				update wip_comp_calc
				set alloc_cwip_in_base = alloc_cwip_in_base + :cwip_in_base_to_alloc
				where wip_computation_id = :i_wip_comp
				and company_id = :a_company_id
				and month_number = :a_month_number
				and nvl(not_eligible_flag,0) = 0
				and work_order_id = (select min(work_order_id) 
											from wip_comp_calc
											where wip_computation_id = :i_wip_comp
											and company_id = :a_company_id
											and month_number = :a_month_number
											and base_amount = (select max(base_amount)
																	from wip_comp_calc
																	where wip_computation_id = :i_wip_comp
																	and company_id = :a_company_id
																	and month_number = :a_month_number
																	)
											)
				;
				if uf_sqlca('Wip Comp Calc alloc_cwip_in_base 1 total check',false) = -1 then return -1
			end if
		end if
	else
		update wip_comp_calc 
		set 	alloc_cwip_in_base = 0
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and nvl(not_eligible_flag,0) = 0;
		
		if uf_sqlca('Wip Comp Calc alloc_cwip_in_base 2',false) = -1 then return -1
	end if
	
	
	//Call Extensions --- update base amounts or fill in the other_base_adjustment for adjustments - add to calculation notes
	ext_rtn = 0
	ext_rtn = f_wip_comp_dynamic_ext(i_wip_comp,a_month_number, 'calc-after-base-amount',-1,a_company_id)
	
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_dynamic_ext for wip computation = " + description)
		return -1
	end if
	
	
//	6.	Calculate Amount
//		a.	Formula based on WIP_COMP options
	//Comp Types
		//straight_rate
		//jur_afudc
		//jur_afudc_offset
		//post_in_service_cost

	update wip_comp_calc 
	set RATE = NVL(RATE,0),
	AFUDC_BASE = NVL(AFUDC_BASE,0),
	JUR_CWIP_IN_BASE = NVL(JUR_CWIP_IN_BASE,0),
	BASE_AMOUNT = NVL(BASE_AMOUNT,0),
	CURRENT_MONTH_BASE_AMOUNT = NVL(CURRENT_MONTH_BASE_AMOUNT,0),
	COMPOUND_BASE = NVL(COMPOUND_BASE,0),
	COMPOUND_CALC = NVL(COMPOUND_CALC,0),
	UNCOMPOUND_BASE = NVL(UNCOMPOUND_BASE,0),
	COMPOUND_RATE = NVL(COMPOUND_RATE,0),
	CALC_AMOUNT = NVL(CALC_AMOUNT,0),
	JURIS_PCT = NVL(JURIS_PCT,0),
	alloc_cwip_in_base = nvl(alloc_cwip_in_base,0),
	total_amount = nvl(total_amount,0),
	input_amount = nvl(input_amount,0),
	other_base_adjustment = nvl(other_base_adjustment,0)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	;
	
	if uf_sqlca('Wip Comp Calc NVLs',false) = -1 then return -1
	
	choose case	trim(lower(comp_type))
		case 'straight_rate','post_in_service_cost'
			update wip_comp_calc 
			set 	CALC_AMOUNT = (base_amount + current_month_base_amount + other_base_adjustment ) * rate
			where wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			and nvl(not_eligible_flag,0) = 0;
			
			if uf_sqlca('Wip Comp Calc straight rate/piscc',false) = -1 then return -1
		case 'jur_afudc'
			update wip_comp_calc 
			set 	CALC_AMOUNT = decode(AFUDC_BASE,0,0,(base_amount + current_month_base_amount + other_base_adjustment ) * rate * juris_pct * ((alloc_cwip_in_base+ JUR_CWIP_IN_BASE)/AFUDC_BASE))
			where wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			and nvl(not_eligible_flag,0) = 0;
			
			if uf_sqlca('Wip Comp Calc jur_afudc',false) = -1 then return -1
		case 'jur_afudc_offset'
			update wip_comp_calc 
			set 	CALC_AMOUNT = decode(AFUDC_BASE,0,0,(base_amount + current_month_base_amount + other_base_adjustment ) * rate * juris_pct * (1 - ((alloc_cwip_in_base+ JUR_CWIP_IN_BASE)/AFUDC_BASE)))
			where wip_computation_id = :i_wip_comp
			and company_id = :a_company_id
			and month_number = :a_month_number
			and nvl(not_eligible_flag,0) = 0;
			
			if uf_sqlca('Wip Comp Calc jur_afudc_offset',false) = -1 then return -1
		
		case 'input'
			//do nothing for computed input amounts
		case else
			uf_msg('Comp Type not supported: ' + comp_type)
			return -1
	end choose
	
	//All Cases Update Input Amounts
	
	update wip_comp_calc  a
		set 	input_amount = (select sum(amount)
								from wip_comp_input b
								where wip_computation_id = :i_wip_comp
								and company_id = :a_company_id
								and month_number = :a_month_number
								and a.work_order_id = b.work_order_id
								)
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		;
		
		if uf_sqlca('Wip Comp Calc Input Amount',false) = -1 then return -1

//	7.	Compute Compounding Base
//	8.	Compute Uncomounded Base
//	9.	Compounding Rate - done above

	//step 1 - get total compound
		update wip_comp_calc a 
		set (COMPOUND_BASE) = 
		(select sum(TOTAL_AMOUNT) 
			from wip_comp_calc b
			where a.work_order_id = b.work_order_id
			and b.month_number < :a_month_number
			and b.wip_computation_id = :i_wip_comp
			and b.company_id = :a_company_id
		)
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and nvl(not_eligible_flag,0) = 0;
		
	if uf_sqlca('Wip Comp Calc Compund Base Amount Set',false) = -1 then return -1
	
	//step 2 - get total uncompounded base
	
	compound_month_number = a_month_number
	if right(string(compound_month_number),2) = '00' then compound_month_number = compound_month_number - 88
	
	//	:CMPD_JAN,:CMPD_FEB,:CMPD_MAR,:CMPD_APR,:CMPD_MAY,:CMPD_JUN,
	//	:CMPD_JUL,:CMPD_AUG,:CMPD_SEP,:CMPD_OCT,:CMPD_NOV,:CMPD_DEC
	// Indicator of Compound Jan = Yes means that the charges through december are compounded at the begining of Jan
	
	if CMPD_JAN = 0 and CMPD_FEB = 0 and CMPD_MAR = 0 and CMPD_APR = 0 and CMPD_MAY = 0 and CMPD_JUN = 0 and &
		CMPD_JUL = 0 and CMPD_AUG = 0 and CMPD_SEP = 0 and CMPD_OCT = 0 and CMPD_NOV = 0 and CMPD_DEC = 0 then
		//no compounding 
		//###sjh 5/13/2011 maint #7187 need to set compound_month_number = 0 for when all months are set to no compounding
		//	compound_month_number = a_month_number
		compound_month_number = 0
	
	else
		for cmpd = 1 to 12 
			choose case right(string(compound_month_number),2)
				case '01'
					if CMPD_JAN = 1 then exit
				case '02'
					if CMPD_FEB = 1 then exit
				case '03'
					if CMPD_MAR = 1 then exit
				case '04'
					if CMPD_APR = 1 then exit
				case '05'
					if CMPD_MAY = 1 then exit
				case '06'
					if CMPD_JUN = 1 then exit
				case '07'
					if CMPD_JUL = 1 then exit
				case '08'
					if CMPD_AUG = 1 then exit
				case '09'
					if CMPD_SEP = 1 then exit
				case '10'
					if CMPD_OCT = 1 then exit
				case '11'
					if CMPD_NOV = 1 then exit
				case '12'
					if CMPD_DEC = 1 then exit
			end choose
//			compound_month_number = a_month_number - 1
			compound_month_number = compound_month_number - 1 //cwb 20100317 changed the code so that the compound month number counts down correctly
			if right(string(compound_month_number),2) = '00' then compound_month_number = compound_month_number - 88
		next
	end if	


	// if compound month of Jul is flagged then compound_month_number equals yyyy07
	//  Jul compound indicates that compounding of June ending balances occured at the beginning of July so include
	//  so inlude compound_month_number in the query for uncompounded dollars
	
	update wip_comp_calc a 
	set (UNCOMPOUND_BASE) = 
	(select sum(TOTAL_AMOUNT) 
		from wip_comp_calc b
		where a.work_order_id = b.work_order_id
		and b.month_number < :a_month_number
		and b.month_number >= :compound_month_number
		and b.wip_computation_id = :i_wip_comp
		and b.company_id = :a_company_id
	)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc Uncompund Base Amount Set',false) = -1 then return -1
	
	//step 3 - set compound base as compoudn base less uncompounded base
	update wip_comp_calc a 
	set (COMPOUND_BASE) = nvl(COMPOUND_BASE,0) - nvl(UNCOMPOUND_BASE,0)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc Compound Base Amount2  Set',false) = -1 then return -1
	
//	10.	Compound Calc Amount

	update wip_comp_calc 
	set 
	COMPOUND_BASE = NVL(COMPOUND_BASE,0),
	COMPOUND_CALC = NVL(COMPOUND_CALC,0),
	UNCOMPOUND_BASE = NVL(UNCOMPOUND_BASE,0),
	COMPOUND_RATE = NVL(COMPOUND_RATE,0)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc NVLs',false) = -1 then return -1

	update wip_comp_calc 
	set 	COMPOUND_CALC = COMPOUND_base * COMPOUND_rate
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	and nvl(not_eligible_flag,0) = 0;
	
	if uf_sqlca('Wip Comp Calc compound calc',false) = -1 then return -1
	
	//ISD Adjustment Amounts: only apply if the stop option = 1 (Stop at In-Service)
	if stop_option = '1' and lower(isd_compound_option) <> 'skip' then
		//ISD Compound Option
			//skip
			//full
			//half
			//day
			//none
		
		//ISD Impact = Percentage of Current Month Calc to adjust by...
		
		choose case lower(isd_compound_option)
			case 'half'
				update wip_comp_calc 
				set isd_impact = .5
				where wip_computation_id = :i_wip_comp
				and company_id = :a_company_id
				and month_number = :a_month_number
				and calc_in_service_date <= last_day(to_date(:a_month_number, 'yyyymm')) //cwb 20100305 fix to calculated isd correctly - JRD 2010-09-08 add last day
				and calc_in_service_date is not null
				;			
				if uf_sqlca('Wip Comp Calc ISD Impact',false) = -1 then return -1
			case 'none' //reverse all for the month of in-service
				update wip_comp_calc 
				set isd_impact = 1
				where wip_computation_id = :i_wip_comp
				and company_id = :a_company_id
				and month_number = :a_month_number
			    and calc_in_service_date <= last_day(to_date(:a_month_number, 'yyyymm')) //cwb 20100305 fix to calculated isd correctly - JRD 2010-09-08 add last day
				and calc_in_service_date is not null
				;			
				if uf_sqlca('Wip Comp Calc ISD Impact',false) = -1 then return -1
			case 'day'
				update wip_comp_calc 
				set isd_impact = 1 - to_number(to_char(calc_in_service_date,'dd'))/to_number(to_char(last_day(calc_in_service_date),'dd'))
				where wip_computation_id = :i_wip_comp
				and company_id = :a_company_id
				and month_number = :a_month_number
				and calc_in_service_date <= last_day(to_date(:a_month_number, 'yyyymm')) //cwb 20100305 fix to calculated isd correctly - JRD 2010-09-08 add last day
				and calc_in_service_date is not null
				;			
				if uf_sqlca('Wip Comp Calc ISD Impact',false) = -1 then return -1	
			case 'full' //reverse nothing for the month if in-service
				update wip_comp_calc 
				set isd_impact = 0
				where wip_computation_id = :i_wip_comp
				and company_id = :a_company_id
				and month_number = :a_month_number
				and calc_in_service_date <= last_day(to_date(:a_month_number, 'yyyymm')) //cwb 20100305 fix to calculated isd correctly - JRD 2010-09-08 add last day
				and calc_in_service_date is not null
				;			
				if uf_sqlca('Wip Comp Calc ISD Impact',false) = -1 then return -1
		end choose

		//ISD Adjustment is the amount of the adjustment.
		
		//if not eligible flag = 0 then current month adjustmnet
		update wip_comp_calc 
		set isd_adjustment = nvl(isd_impact,0) * (nvl(COMPOUND_CALC ,0) + nvl(CALC_AMOUNT,0)) * -1
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and calc_in_service_date <= last_day(to_date(:a_month_number, 'yyyymm')) //cwb 20100305 fix to calculated isd correctly - JRD 2010-09-08 add last day
		and calc_in_service_date is not null
		and nvl(not_eligible_flag,0) = 0
		;			
		if uf_sqlca('Wip Comp Calc ISD Adjustment - Current Month',false) = -1 then return -1
		
		//if not eligible flag = 2 then reverse all after in-service month and take adjustmnet for in-service month.
		
		update wip_comp_calc  a 
		set isd_adjustment = (select -1 * sum(decode(b.month_number, to_number(to_Char(a.calc_in_service_date,'yyyymm')),
													nvl(b.isd_adjustment,0) + (nvl(a.isd_impact,0) * (nvl(b.COMPOUND_CALC ,0) + nvl(b.CALC_AMOUNT,0))),
													nvl(b.isd_adjustment,0) +  (nvl(b.COMPOUND_CALC ,0) + nvl(b.CALC_AMOUNT,0))
													)//decode
													)//sum
								from wip_comp_calc b
								where b.wip_computation_id = :i_wip_comp
									and b.company_id = :a_company_id
									and b.month_number >= to_number(to_Char(a.calc_in_service_date,'yyyymm'))
								    and b.month_number <= :a_month_number //jrd 2010-09-08 limit months to only past
									and a.work_order_id = b.work_order_id
		
		)
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and calc_in_service_date is not null
		and nvl(not_eligible_flag,0) = 2
		;			
		if uf_sqlca('Wip Comp Calc ISD Adjustment - Prior Month',false) = -1 then return -1
	
	
		//So that rounding does't get transactions constantly going back and forth with rounding, any adjustmets less than 10 cents is zeroed out.
		update wip_comp_calc  a 
		set isd_adjustment = 0
		where wip_computation_id = :i_wip_comp
		and company_id = :a_company_id
		and month_number = :a_month_number
		and isd_adjustment between -.1 and .1
		and nvl(not_eligible_flag,0) = 2
		;			
		if uf_sqlca('Wip Comp Calc ISD Adjustment - Prior Month',false) = -1 then return -1
		
		//
	end if
	
	//Call Extensions ---- update other_computed_adj with any special adjustments
		ext_rtn = 0
	ext_rtn = f_wip_comp_dynamic_ext(i_wip_comp,a_month_number, 'calc-after-calc-amount',-1,a_company_id)
	
	if ext_rtn = -1 then
		uf_msg("Error Occured in f_wip_comp_dynamic_ext for wip computation = " + description)
		return -1
	end if

	
//	11.	Add Compound Calc Amount to the Calc Amount to get Total Amount 

	update wip_comp_calc 
	set RATE = NVL(RATE,0),
	AFUDC_BASE = NVL(AFUDC_BASE,0),
	JUR_CWIP_IN_BASE = NVL(JUR_CWIP_IN_BASE,0),
	BASE_AMOUNT = NVL(BASE_AMOUNT,0),
	CURRENT_MONTH_BASE_AMOUNT = NVL(CURRENT_MONTH_BASE_AMOUNT,0),
	COMPOUND_BASE = NVL(COMPOUND_BASE,0),
	COMPOUND_CALC = NVL(COMPOUND_CALC,0),
	UNCOMPOUND_BASE = NVL(UNCOMPOUND_BASE,0),
	COMPOUND_RATE = NVL(COMPOUND_RATE,0),
	CALC_AMOUNT = NVL(CALC_AMOUNT,0),
	JURIS_PCT = NVL(JURIS_PCT,0),
	alloc_cwip_in_base = nvl(alloc_cwip_in_base,0),
	total_amount = nvl(total_amount,0),
	input_amount = nvl(input_amount,0),
	isd_impact = nvl(isd_impact,0),
	isd_adjustment = nvl(isd_adjustment,0),
	other_computed_adj = nvl(other_computed_adj,0)
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	;
	
	if uf_sqlca('Wip Comp Calc NVLs',false) = -1 then return -1

	update wip_comp_calc 
	set 	TOTAL_AMOUNT = COMPOUND_CALC + CALC_AMOUNT + INPUT_AMOUNT + isd_adjustment + other_computed_adj
	where wip_computation_id = :i_wip_comp
	and company_id = :a_company_id
	and month_number = :a_month_number
	;
	
	if uf_sqlca('Wip Comp Calc compound calc',false) = -1 then return -1
//	12.	Insert into the WIP_COMP_CHARGES table
	uf_msg(' Calculations Complete, Create Transaction Records')
	insert into WIP_COMP_CHARGES
	(WIP_COMPUTATION_ID,
	WORK_ORDER_ID,
	MONTH_NUMBER,
	COST_ELEMENT_ID,
	WIP_GL_ACCOUNT,
	OFFSET_GL_ACCOUNT,
	CURRENT_GL_ACCOUNT,
	AMOUNT,
	NON_UNITIZED_STATUS,
	UNITIZED_STATUS,
	CLOSED_MONTH_NUMBER,
	UNIT_CLOSED_MONTH_NUMBER,
	WIP_COMP_SUMMARY_ID,
	company_id,
	charge_id)
	
	(select a.WIP_COMPUTATION_ID,
	a.WORK_ORDER_ID,
	a.MONTH_NUMBER,
	b.COST_ELEMENT_ID,
	decode(b.EXPENDITURE_TYPE_ID,1,decode(ct.processing_type_Id,5,1,woa.cwip_gl_account),
												2, removal_gl_account,
												3, expense_gl_account,
												4, jobbing_gl_account,
												b.WIP_GL_ACCOUNT),
	b.OFFSET_GL_ACCOUNT,
	decode(b.EXPENDITURE_TYPE_ID,1,decode(ct.processing_type_Id,5,1,woa.cwip_gl_account),
												2, removal_gl_account,
												3, expense_gl_account,
												4, jobbing_gl_account,
												b.WIP_GL_ACCOUNT) CURRENT_GL_ACCOUNT,
	a.total_amount AMOUNT,
	null NON_UNITIZED_STATUS,
	null UNITIZED_STATUS,
	null CLOSED_MONTH_NUMBER,
	null UNIT_CLOSED_MONTH_NUMBER,
	null WIP_COMP_SUMMARY_ID,
	a.company_id,
	pwrplant3.nextval
	
	from wip_comp_calc a, wip_computation b, work_order_account woa, cost_element ce, charge_type ct
	where a.wip_computation_id = :i_wip_comp
	and a.company_id = :a_company_id
	and b.cost_element_Id = ce.cost_element_id
	and ce.charge_type_id = ct.charge_type_id
	and a.work_order_id = woa.work_order_id
	and a.month_number = :a_month_number
	and b.wip_computation_id = :i_wip_comp
	and total_amount <> 0
	)
	;
	
	if uf_sqlca('Wip Comp Charges Insert',true) = -1 then return -1

//	13.	Need to handle cases where company on the result is different than company on the charge

//JRDJRD

//	14.	Insert into CWIP Charge

//
	if isnull(post_cwip) then post_cwip = 1
	if post_cwip = 1 then
		uf_msg(" Creating Charge Transaction in CWIP Charge")
		insert into cwip_charge
		(CHARGE_ID,
		CHARGE_TYPE_ID,
		EXPENDITURE_TYPE_ID,
		WORK_ORDER_ID,
		CHARGE_MO_YR,
		DESCRIPTION,
		QUANTITY,
		AMOUNT,
		HOURS,
		PAYMENT_DATE,
		ID_NUMBER,
		DEPARTMENT_ID,
		CHARGE_AUDIT_ID,
		JOURNAL_CODE,
		COST_ELEMENT_ID,
		EXTERNAL_GL_ACCOUNT,
		GL_ACCOUNT_ID,
		STATUS,
		COMPANY_ID,
		MONTH_NUMBER,
		bus_segment_id)
		(select a.CHARGE_ID,
		e.CHARGE_TYPE_ID,
		b.EXPENDITURE_TYPE_ID,
		a.WORK_ORDER_ID,
		to_date(a.month_number,'yyyymm') CHARGE_MO_YR,
		substr(b.DESCRIPTION,1,35),
		0 QUANTITY,
		a.AMOUNT,
		0 HOURS,
		trunc(sysdate) PAYMENT_DATE,
		a.wip_computation_id ID_NUMBER,
		d.DEPARTMENT_ID,
		a.charge_ID,
		b.JOURNAL_CODE,
		a.COST_ELEMENT_ID,
		c.external_account_code EXTERNAL_GL_ACCOUNT,
		a.wip_gl_account GL_ACCOUNT_ID,
		a.wip_computation_id STATUS,
		a.COMPANY_ID,
		a.MONTH_NUMBER,
		d.bus_segment_id
		from wip_comp_charges a, wip_computation b, gl_account c,
			work_order_control d, cost_element e
		where a.wip_computation_id = :i_wip_comp
		and a.company_id = :a_company_id
		and a.month_number = :a_month_number
		and b.wip_computation_id = :i_wip_comp
		and c.gl_account_id = a.wip_gl_account
		and a.work_order_id = d.work_order_id
		and e.cost_element_id = a.cost_element_id
		)
		;
		
		if uf_sqlca('CWIP Charges Insert',true) = -1 then return -1
	end if
	
next //computation id

return 1
end function

on uo_wip_computations.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wip_computations.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

