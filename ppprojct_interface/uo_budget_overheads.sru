HA$PBExportHeader$uo_budget_overheads.sru
$PBExportComments$7767-Update budget_amounts after processing budget items
forward
global type uo_budget_overheads from nonvisualobject
end type
end forward

global type uo_budget_overheads from nonvisualobject
end type
global uo_budget_overheads uo_budget_overheads

type variables

boolean i_status_box = false
boolean i_progress_bar = true
boolean i_pp_msgs = false
boolean i_messagebox_onerror = true
boolean i_statusbox_onerror = true
boolean i_add_time_to_messages = true
boolean i_display_rowcount = true 
boolean i_display_msg_on_sql_success = true
boolean i_allow_clearings = false
string i_table_name
string i_calling_window
longlong i_status_position
longlong i_funding_wo_indicator
longlong i_actuals_month
longlong i_budget_version
longlong i_cwip_start_month//not used
longlong i_start_month_number // not used



end variables

forward prototypes
public function longlong uf_budget_ovhs_custom (longlong a_clearing_id, longlong a_sum_or_org)
public function longlong uf_read (string a_calling_window, string a_table_name)
public function longlong uf_budget_transpose ()
public function longlong uf_delete_ovhs ()
public function longlong uf_insert_ovhs ()
public subroutine uf_msg (string a_msg, string a_error)
public function longlong uf_budget_overheads_cr ()
public function longlong uf_get_actuals_mn ()
public function integer uf_budget_ovhs_clearing (longlong a_clearing_id, longlong a_sum_or_org, longlong a_load_by_jt, longlong a_load_by_dept, longlong a_load_by_ua, longlong a_load_by_desc, longlong a_load_by_wo, longlong a_load_by_attr[])
public function longlong uf_budget_ovhs_loading (longlong a_clearing_id, longlong a_sum_or_org, longlong a_load_by_jt, longlong a_load_by_dept, longlong a_load_by_ua, longlong a_load_by_desc, longlong a_load_by_wo, longlong a_load_by_attr[])
public function longlong uf_budget_ovhs_reimb (longlong a_clearing_id, longlong a_sum_or_org, longlong a_load_by_jt, longlong a_load_by_dept, longlong a_load_by_ua, longlong a_load_by_desc, longlong a_load_by_wo, longlong a_load_by_attr[])
end prototypes

public function longlong uf_budget_ovhs_custom (longlong a_clearing_id, longlong a_sum_or_org);//			
//			
/////NEED TO CODE CUSTOM DATAWINDOW TYPE
//	elseif clear_wo_type = 5 then
////		
////		ds_custom.dataobject = ds.getitemstring(i,'dw_name')
////		ds_custom.settransobject(sqlca)
////		start_year = long(mid(string(starting_month),1,4))
////		end_year = long(mid(string(ending_month),1,4))
////		
////		
////		
////		sqls = 'delete from wo_est_monthly_trans_temp where i_session_id = '+string(i_session_id)
////		execute immediate :sqls;
////		
////						
////		sqls = 'delete from wo_est_monthly_mo_id where i_session_id = '+string(i_session_id)
////		execute immediate :sqls;
////		
////		
////		
////		id_counter = 0
////		For j = round(starting_month/100,0) to round(ending_month/100,0)
//////////		Make the datawindow update the table wo_est_monthly_trans_temp
////			 
////			
////			
////			ds_custom.retrieve(j,clear_id)
////			
////			For k = 1 to ds_custom.rowcount()
////				id_counter ++
////				budget_id = ds_custom.getitemnumber(k,"budget_id")
////				budget_version_id = ds_custom.getitemnumber(k,"budget_version_id")
////				company_id = ds_custom.getitemnumber(k,"company_id")
////				est_chg_type_id = ds_custom.getitemnumber(k,"est_chg_type_id")
////				expenditure_type_id = ds_custom.getitemnumber(k,"expenditure_type_id")
//////				department_id = ds_custom.getitemnumber(k,"department_id")
////				job_task_id = ds_custom.getitemstring(k,"job_task_id")
//////				budget_plant_class_id = ds_custom.getitemnumber(k,"budget_plant_class_id")
////				amount = ds_custom.getitemdecimal(k,"amount")
////				long_description = ds_custom.getitemstring(k,"long_description")
////				month_number = ds_custom.getitemnumber(k,"month_number")
////				year = ds_custom.getitemnumber(k,"year")
////				new_old = ds_custom.getitemnumber(k,"new_old")
////				wo_id = ds_custom.getItemNumber(k, "work_order_id")
////				id = id_counter
////			
////				insert into budget_monthly_data_trans_temp
////				(work_order_id, budget_id,budget_version_id,company_id,est_chg_type_id,expenditure_type_id,job_task_id,
////				amount,long_description,
////				month_number,year,new_old,id
////				)
////				values (:wo_id, :budget_id,:budget_version_id,:company_id,:est_chg_type_id,:expenditure_type_id,:job_task_id,
////				:amount,:long_description,
////				:month_number,:year,:new_old,:id
////				)
////				;
////				if sqlca.sqlcode < 0 then
////					MessageBox('ERROR','Could not insert into budget_monthly_trans_temp. ' +sqlca.sqlerrtext)
////					rollback;
////					return -1
////				end if
////				
////				
////			Next
////			
////			
////			
////		Next
////			
////			insert into budget_monthly_data_mo_id
////			(budget_monthly_id,work_order_id, est_chg_type_id,expenditure_type_id,job_task_id,
////			long_description,budget_id,company_id)
////			(select  a.*
////				from 
////					(select distinct work_order_id, est_chg_type_id,expenditure_type_id,job_task_id,
////				long_description,budget_id,company_id
////				from budget_monthly_data_trans_temp			
////				) a
////			)
////			;
////			if sqlca.sqlcode < 0 then
////				Messagebox('ERROR','Could not load mo_id data table for custom DW allocation. '+sqlca.sqlerrtext)
////				rollback;
////				return -1
////			end if
////
////			
////			insert into budget_monthly_data_transpose
////			(work_order_id, budget_monthly_id,est_chg_type_id,expenditure_type_id,job_task_id,
////			long_description,budget_id,
////			month_number,amount, new_old,year,company_id
////			)
////			(select mo_id.work_order_id, mo_id.budget_monthly_id,mo_id.est_chg_type_id,mo_id.expenditure_type_id,mo_id.job_task_id,
////			
////			mo_id.long_description,
////			mo_id.budget_id,
////			temp.month_number,sum(temp.amount),1,temp.year,temp.company_id
////			from budget_monthly_data_trans_temp temp, budget_monthly_data_mo_id mo_id
////			where mo_id.est_chg_type_id = temp.est_chg_type_id
////				and mo_id.company_id = temp.company_id
////				and mo_id.expenditure_type_id = temp.expenditure_type_id
////				and nvl(mo_id.long_description,'-99') = nvl(temp.long_description,'-99')
////				and nvl(mo_id.job_task_id,'-99') = nvl(temp.job_task_id,'-99')
////				and mo_id.budget_id = temp.budget_id
////				and temp.work_order_id = mo_id.work_order_id
////				group by mo_id.work_order_id, mo_id.budget_monthly_id,mo_id.est_chg_type_id,mo_id.expenditure_type_id,
////			
////			mo_id.long_description,
////			mo_id.budget_id,
////			temp.month_number,temp.company_id,mo_id.job_task_id,temp.year
////			)
////			;
////			if sqlca.sqlcode < 0 then
////				MessageBox('ERROR','Could not insert rows into transpose for custom dw clearing ' +&
////				string(clear_id)+'. '+sqlca.sqlerrtext)
////				rollback;
////				return -1
////			end if
////			
////		
////		
////	
////
//	end if




return 1
end function

public function longlong uf_read (string a_calling_window, string a_table_name);
//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_read() 
//
//	  Purpose  :	Driving function for preforming Budget Overheads  on FP's calls other functions to do clearing, loading, reimbursable, or custom overheads
//					this function assumes the list of work order id's and revisions have been placed in the temp table wo_est_processing_temp
//		             
//
//   Arguments:	a_call  : string calling window window or interface this object is being called from
//					a_table_name: string table these overheads are ran against (WO_EST_MONTHLY, BUDGET_MONTHLY_DATA, WO_EST_MONTHLY_SUBS_DE
//	
//   Returns :   1 if successfuly -1 if a failure occurs
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    		--------   -----------    ----------------------------------------------- 
//    05-17-99    PowerPlan  	Version 2.5   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
longlong rtn, i, clearing_indicator, clearing_id, sum_or_org, increment_amount, ovh_count, load_by_desc
uo_ds_top ds, ds_custom
string sqls, rets, time_string, description, ret
longlong load_by_jt, load_by_ua, load_by_dept, wo_check, load_by_wo, load_by_attr[10]
uo_budget_revision uo_revisions

////OPEN ISSUES/TO DO list
	////How do we handle local currency when doing this on budget monthly data
	////Should we build logic to calculate quantities and hours based on rates if a rate applies to the budget line? ///NOT FOR NOW
	///Make the messages function work for progress bar
	///NEED TO ADD CODE TO UPDATE BUDGET AMOUNTS THERE SHOULD BE STANDARD FUNCTION TO DO THIS IF NOT WE SHOULD BUILD ONE
	///Custom overheads


////NOTES
	////This process assumes that revisions in the wo_est_processing_temp table are available to have their overheads calculated meaning no approved revisions or no revisions
	    ////hooked to subs only budget version, not open for entry 
	////Steps
		///Transpose data from the table named passed in (excludes estimate charges types with an afudc flag of 'O')
			///This inserts all records 
		///Loop over the list of overheads and process them
		///Deletes the overheads currently in the table passed in
		///Inserts the newly calculated overheads from the transpose table back to the table passed in
		
i_status_position = 0	


if upper(a_table_name) <> 'BUDGET_MONTHLY_DATA' then
	select count(*)
	into :wo_check
	from work_order_control woc, wo_est_processing_temp t
	where woc.funding_wo_indicator = 0
	  and t.work_order_id = woc.work_order_id
	;
	
	if isnull(wo_check) then wo_check = 0
	if wo_check > 0 then
		i_funding_wo_indicator = 0
	else
		i_funding_wo_indicator = 1
	end if
else
	i_funding_wo_indicator = 2
end if

ret = f_wo_est_processing_protected_revs(i_funding_wo_indicator ) 
if ret <> 'OK' then
	uf_msg('ERROR Checking for locked revisions','E')
	uf_msg(ret,'E')
	return -1
end if	

setpointer(hourglass!)


i_table_name = upper(a_table_name)
i_calling_window = a_calling_window
uf_msg('--------------------------------------------------------------------------------','I')
uf_msg('Staging Budget Data ','I')
rtn = uf_get_actuals_mn()
if rtn < 0 then
	rollback;
	return -1
end if

//Take data from wo_est_monthly or budget_monthly_data and transpose it to a month number table
rtn = uf_budget_transpose()
if rtn < 0 then
	rollback;
	return -1

	
end if

if rtn = 2 then
	uf_msg('Nothing to Process','I')
	return 2
end if
uf_msg('Analyzing Staged Budget Data ','I')
sqlca.analyze_table('wo_est_processing_temp')




//Create datastore of overheads to loop over
//If being called from the estimate screen it will only run loading overhead types

ds = create uo_ds_top

//START: Maint 6342: ZJS: 04242011
if i_funding_wo_indicator = 0 then
	sqls = 'select * from clearing_wo_control_bdg where wo = 1 and active = 1 order by processing_priority '
elseif i_funding_wo_indicator = 1 then
	sqls = 'select * from clearing_wo_control_bdg where fp = 1 and active = 1 order by processing_priority '
else
	sqls = 'select * from clearing_wo_control_bdg where bi = 1 and active = 1 order by processing_priority '
end if
//END: Maint 6342: ZJS: 04242011
	
rets = f_create_dynamic_ds(ds,'grid',sqls,sqlca,true)
if rets <> "OK" then
	uf_msg('Could not retrieve overheads and allocations list. '+rets,'E')
	rollback;
	return -1
end if

ovh_count = ds.rowcount()
for i = 1 to ds.rowcount()
	if i = 1 then
		increment_amount = 80/ovh_count
	end if
	i_status_position = i_status_position + increment_amount
	setnull(clearing_indicator)
	setnull(sum_or_org)
	////////	clearing types (by indicator id)
	////////	1 = clearing 
	////////	2 = loading 
	////////	3 = company split
	////////	4 = reimbursement
	///////		5 = custom

	
	
	clearing_indicator = ds.getitemnumber(i,"clearing_indicator")
	if isNull(clearing_indicator) or clearing_indicator < 0 then
		uf_msg('Could not get clearing indicator from the overhead.','E')
		rollback;
		destroy ds
		return -1
	end if	
	
	//If this code is going to be run from any window other then the budget processing control can not run clearing overheads
	if not i_allow_clearings and clearing_indicator = 1 then continue
	if not i_allow_clearings and clearing_indicator = 3 then continue
	
	clearing_id = ds.getitemnumber(i,"clearing_id")
	sum_or_org = ds.getitemnumber(i,"target_type")
	load_by_jt = ds.getitemnumber(i,"load_by_jt")
	load_by_dept = ds.getitemnumber(i,"load_by_dept")
	load_by_ua = ds.getitemnumber(i,"load_by_ua")
	load_by_desc = ds.getitemnumber(i,"load_by_description")
	load_by_wo = ds.getitemnumber(i,"load_by_wo")//AWW PC-1084
	load_by_attr[1] = ds.getitemnumber(i,"load_by_attr01")
	load_by_attr[2] = ds.getitemnumber(i,"load_by_attr02")
	load_by_attr[3] = ds.getitemnumber(i,"load_by_attr03")
	load_by_attr[4] = ds.getitemnumber(i,"load_by_attr04")
	load_by_attr[5] = ds.getitemnumber(i,"load_by_attr05")
	load_by_attr[6] = ds.getitemnumber(i,"load_by_attr06")
	load_by_attr[7] = ds.getitemnumber(i,"load_by_attr07")
	load_by_attr[8] = ds.getitemnumber(i,"load_by_attr08")
	load_by_attr[9] = ds.getitemnumber(i,"load_by_attr09")
	load_by_attr[10] = ds.getitemnumber(i,"load_by_attr10")
	description = ds.getitemstring(i,"description")
	uf_msg('Processing overhead '+description+' overhead '+string(i)+' of '+string(ds.rowcount()),'I')
	if isnull(load_by_jt) then load_by_jt = 0
	if isnull(load_by_dept) then load_by_dept = 0
	if isnull(load_by_ua) then load_by_ua = 0
	if isnull(load_by_wo) then load_by_wo = 0//AWW PC-1084
	if sum_or_org < 0 or isNull(sum_or_org) then
		uf_msg('Could not find what target type the allocation at row '+string(i)+' is.','E')
		rollback;
		destroy ds
		return -1
	end if
	
	choose case clearing_indicator
		case 1
			rtn = uf_budget_ovhs_clearing(clearing_id,sum_or_org,load_by_jt, load_by_dept, load_by_ua, load_by_desc, load_by_wo, load_by_attr)//AWW PC-1084
		case 2
			rtn = uf_budget_ovhs_loading(clearing_id,sum_or_org,load_by_jt, load_by_dept, load_by_ua, load_by_desc, load_by_wo, load_by_attr)//AWW PC-1084
		case 3
			//rtn = uf_budget_ovhs_company_split(clearing_id,sum_or_org)
		case 4
			rtn = uf_budget_ovhs_reimb(clearing_id,sum_or_org,load_by_jt, load_by_dept, load_by_ua, load_by_desc, load_by_wo, load_by_attr)//AWW PC-1084
		case 5
			//rtn = uf_budget_ovhs_custom(clearing_id,sum_or_org)
	end choose
	
	if rtn < 0 then
		rollback;
		destroy ds
		return -1
	end if
	
next

rtn =  f_budget_overheads_custom(2, i_calling_window, i_table_name)
if rtn < 0 then
	rollback;
	destroy ds
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_overheads' ) = 1 then
	rtn = sqlca.f_client_budget_overheads(2,i_calling_window, i_table_name)
	if rtn < 0 then
		rollback;
		destroy ds
		return -1
	end if
end if
string args[]
args[1] = '2'
args[2] = i_table_name
args[3] = string(i_actuals_month)
rtn = f_wo_validation_control(82,args)
if rtn < 0 then
	rollback;
	destroy ds
	return -1
end if
i_status_position = 85

////Delete previously calculated overheads
rtn = uf_delete_ovhs()
if rtn < 0 then
	rollback;
	destroy ds
	return -1
end if
i_status_position = 90

rtn = uf_insert_ovhs()
if rtn < 0 then
	rollback;
	destroy ds
	return -1
end if
i_status_position = 100

// ### CDM - Maint 7767 - Update budget_amounts after processing budget items
if upper(a_table_name) = 'BUDGET_MONTHLY_DATA' then
	uf_msg('Updating Budget Amounts','I')
	uo_revisions = create uo_budget_revision
	
	uo_revisions.i_messagebox_onerror = i_messagebox_onerror
	uo_revisions.i_status_box = i_status_box
	uo_revisions.i_pp_msgs = i_pp_msgs
	uo_revisions.i_add_time_to_messages = i_add_time_to_messages
	uo_revisions.i_statusbox_onerror = i_statusbox_onerror
	uo_revisions.i_display_msg_on_sql_success = i_display_msg_on_sql_success
	uo_revisions.i_display_rowcount = i_display_rowcount
	uo_revisions.i_progress_bar = i_progress_bar
	
	rtn = uo_revisions.uf_update_budget_amounts()
	destroy uo_revisions
	
	if rtn = -1 then
		rollback;
		destroy ds
		return -1
	end if
end if

uf_msg('Completed Successfully ','I')

uf_msg('--------------------------------------------------------------------------------','I')
rtn =  f_budget_overheads_custom(3, i_calling_window, i_table_name)
if rtn < 0 then
	rollback;
	destroy ds
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_overheads' ) = 1 then
	rtn = sqlca.f_client_budget_overheads(3,i_calling_window, i_table_name)
	if rtn < 0 then
		rollback;
		destroy ds
		return -1
	end if
end if
args[1] = '3'
args[2] = i_table_name
args[3] = string(i_actuals_month)
rtn = f_wo_validation_control(82,args)
if rtn < 0 then
	rollback;
	destroy ds
	return -1
end if
commit;

setpointer(arrow!)
f_progressbar('Title', 'close(w_progressbar)', 100, -1)
destroy ds

return 1
 
 
 
 
 
end function

public function longlong uf_budget_transpose ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_transpose_data() 
//
//	  Purpose  :	takes data fromw wo_est_monthly and budget_monthly_data (not currently coded) and insert into
//					wo_est_processing_transpose in a month number format
//		             
//
//   Arguments:	a_call  : string calling window window or interface this object is being called from
//	
//   Returns :   1 if successfuly -1 if a failure occurs
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//   
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 

longlong rtn
string sqls, revision_col, wo_id_col, from_wo_col, sub_col, utility_account_col, budget_id_col, id_col, est_id_col

if isnull(i_actuals_month) then i_actuals_month = 0

choose case i_table_name
	case 'WO_EST_MONTHLY'
		revision_col = 'a.revision'
		wo_id_col = 'a.work_order_id'
		from_wo_col = 'null'
		sub_col = 'null'
		utility_account_col = 'a.utility_account_id'
		budget_id_col = 'a.budget_id'
		id_col = 'a.work_order_id'
		est_id_col = 'a.est_monthly_id'
	case 'BUDGET_MONTHLY_DATA'
		revision_col = 'a.budget_version_id'
		wo_id_col = 'a.work_order_id'
		from_wo_col = 'null'
		sub_col = 'null'
		utility_account_col = 'a.budget_plant_class_id'
		budget_id_col = 'a.budget_id'
		id_col = 'a.budget_id'
		est_id_col = 'a.budget_monthly_id'
	case 'WO_EST_MONTHLY_SUBS_DET'
		revision_col = 'a.revision'
		wo_id_col = 'a.work_order_id'
		from_wo_col = 'a.from_work_order_id'
		sub_col = 'a.substition_id'
		utility_account_col = 'a.utility_account_id'
		budget_id_col = 'null'
		id_col = 'a.work_order_id'
		est_id_col = 'a.est_monthly_id'
	case else
		uf_msg(i_table_name+' Is not a recognized table name ','E')
		return -1
end choose
		

uf_msg('Cleaning out transpose staging table ','I')
delete from wo_est_processing_transpose ;
if sqlca.sqlcode < 0 then
	uf_msg('Could not delete transpose table. '+sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if
uf_msg('--Deleted '+string(sqlca.sqlnrows),'I')

uf_msg('Staging Basis Records','I')
sqls = ' '
sqls+= ' insert into /*+append+*/ wo_est_processing_transpose(id, work_order_id, est_monthly_id , month_number  ,  EXPENDITURE_TYPE_ID, EST_CHG_TYPE_ID, '
sqls+= ' amount, '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls+= ' hours, quantity, '
end if
sqls+= ' utility_account_id,    DEPARTMENT_ID ,  LONG_DESCRIPTION, revision, '
sqls+= ' JOB_TASK_ID, '
if i_table_name = 'WO_EST_MONTHLY' then //AWW PC-1084
	sqls+= 'wo_work_order_id, '
end if	
sqls+= 'attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, '
sqls+= 'new_old,year, user_id, substitution_id, from_work_order_id, budget_id, actuals_month_number)'
sqls+= ' select  pwrplant1.nextval, '+wo_id_col+', '
sqls+= " "+est_id_col+" , to_number(a.year||lpad(b.month_num,2,'0') )  ,  a.EXPENDITURE_TYPE_ID, a.EST_CHG_TYPE_ID, "
sqls+= ' decode(b.month_num,1,a.JANUARY , '
sqls+= '							  2,a.february, '
sqls+= '							  3,a.march, '
sqls+= '							  4,a.april, '
sqls+= '							  5,a.may, '
sqls+= '							  6,a.june, '
sqls+= '							  7,a.july, '
sqls+= '							  8,a.august, '
sqls+= '							  9,a.september, '
sqls+= '							  10,a.october, '
sqls+= '							  11,a.november, '
sqls+= '							  12,a.december) amount,  '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls+= ' decode(b.month_num,1,a.hrs_jan , '
	sqls+= '							  2,a.hrs_feb, '
	sqls+= '							  3,a.hrs_mar, '
	sqls+= '							  4,a.hrs_apr, '
	sqls+= '							  5,a.hrs_may, '
	sqls+= '							  6,a.hrs_jun, '
	sqls+= '							  7,a.hrs_jul, '
	sqls+= '							  8,a.hrs_aug, '
	sqls+= '							  9,a.hrs_sep, '
	sqls+= '							  10,a.hrs_oct, '
	sqls+= '							  11,a.hrs_nov, '
	sqls+= '							  12,a.hrs_dec)  hours,  '
	sqls+= ' decode(b.month_num,1,a.qty_jan , '
	sqls+= '							  2,a.qty_feb, '
	sqls+= '							  3,a.qty_mar, '
	sqls+= '							  4,a.qty_apr, '
	sqls+= '							  5,a.qty_may, '
	sqls+= '							  6,a.qty_jun, '
	sqls+= '							  7,a.qty_jul, '
	sqls+= '							  8,a.qty_aug, '
	sqls+= '							  9,a.qty_sep, '
	sqls+= '							  10,a.qty_oct, '
	sqls+= '							  11,a.qty_nov, '
	sqls+= '							  12,a.qty_dec)  quantity,  '
end if
sqls+=utility_account_col+', a.DEPARTMENT_ID ,  a.LONG_DESCRIPTION, '+revision_col+', '
sqls+= ' a.JOB_TASK_ID, '
if i_table_name = 'WO_EST_MONTHLY' then //AWW PC-1084
	sqls+= 'a.wo_work_order_id, '
end if	
sqls+= 'a.attribute01_id, a.attribute02_id, a.attribute03_id, a.attribute04_id, a.attribute05_id, a.attribute06_id, a.attribute07_id, a.attribute08_id, a.attribute09_id, a.attribute10_id, '
sqls+= '0 new_old, a.year, user user_id, '+sub_col+', '+from_wo_col+', '+budget_id_col+', '+string(i_actuals_month)
sqls+= ' from pp_table_months b, wo_est_processing_temp t, '+i_table_name+' a'
sqls+= ' where t.work_order_id = '+id_col
sqls+= '     and t.revision = '+revision_col
sqls+= '     and (decode(b.month_num,1,a.JANUARY , '
sqls+= '							  2,a.february, '
sqls+= '							  3,a.march, '
sqls+= '							  4,a.april, '
sqls+= '							  5,a.may, '
sqls+= '							  6,a.june, '
sqls+= '							  7,a.july, '
sqls+= '							  8,a.august, '
sqls+= '							  9,a.september, '
sqls+= '							  10,a.october, '
sqls+= '							  11,a.november, '
sqls+= '							  12,a.december) <> 0  '
if i_table_name <> 'BUDGET_MONTHLY_DATA' then
	sqls+= '  or decode(b.month_num,1,a.hrs_jan , '
	sqls+= '							  2,a.hrs_feb, '
	sqls+= '							  3,a.hrs_mar, '
	sqls+= '							  4,a.hrs_apr, '
	sqls+= '							  5,a.hrs_may, '
	sqls+= '							  6,a.hrs_jun, '
	sqls+= '							  7,a.hrs_jul, '
	sqls+= '							  8,a.hrs_aug, '
	sqls+= '							  9,a.hrs_sep, '
	sqls+= '							  10,a.hrs_oct, '
	sqls+= '							  11,a.hrs_nov, '
	sqls+= '							  12,a.hrs_dec) <> 0   '
	sqls+= '  or decode(b.month_num,1,a.qty_jan , '
	sqls+= '							  2,a.qty_feb, '
	sqls+= '							  3,a.qty_mar, '
	sqls+= '							  4,a.qty_apr, '
	sqls+= '							  5,a.qty_may, '
	sqls+= '							  6,a.qty_jun, '
	sqls+= '							  7,a.qty_jul, '
	sqls+= '							  8,a.qty_aug, '
	sqls+= '							  9,a.qty_sep, '
	sqls+= '							  10,a.qty_oct, '
	sqls+= '							  11,a.qty_nov, '
	sqls+= '							  12,a.qty_dec) <> 0 ) '
else
	sqls += ' ) '
end if
execute immediate :sqls;
if sqlca.sqlcode < 0 then
	uf_msg('Error Transposing data from '+i_table_name+' '+sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if

if sqlca.sqlnrows = 0 then
	uf_msg('There is nothing process ','I')
	return 2
else
	uf_msg('--Staged '+string(sqlca.sqlnrows)+' rows to be processed ','I')
end if





if i_table_name = 'BUDGET_MONTHLY_DATA' then


	uf_msg('Updating Staged Data company id','I')
	update wo_est_processing_transpose a
	set (a.company_id)=
	(select w.company_id
	from budget w
	where a.budget_id = w.budget_id
	)
	where company_id is null
	 ;
	if sqlca.sqlcode < 0 then
		uf_msg('Error Updating Company Id '+i_table_name+' '+sqlca.sqlerrtext,'E')
		rollback;
		return -1
	end if
	uf_msg('--Updated '+string(sqlca.sqlnrows)+' rows ','I')
else
	uf_msg('--Updated '+string(sqlca.sqlnrows)+' rows ','I')
	uf_msg('Updating Staged Data Company Id & Budget Id','I')
	update wo_est_processing_transpose a
	set (a.company_id, a.budget_id)=
	(select w.company_id, w.budget_id
	from work_order_control w
	where a.work_order_id = w.work_order_id
	)
	where company_id is null
	 ;
	if sqlca.sqlcode < 0 then
		uf_msg('Error Updating Company Id '+i_table_name+' '+sqlca.sqlerrtext,'E')
		rollback;
		return -1
	end if
	uf_msg('--Updated '+string(sqlca.sqlnrows)+' rows ','I')
	
end if

uf_msg('Deleting previous overheads from staged data ','I')
delete from wo_est_processing_transpose a
where month_number > nvl(actuals_month_number,0)
and exists (
		select  1
		from estimate_charge_type e
		where a.est_chg_type_id = e.est_chg_type_id
			and nvl(e.afudc_flag,'Q') = 'O')
;
if sqlca.sqlcode < 0 then
		uf_msg('Error Deleting staged overhead values '+i_table_name+' '+sqlca.sqlerrtext,'E')
		rollback;
		return -1
end if
uf_msg('--Deleted '+string(sqlca.sqlnrows)+' rows ','I')

//AWW 5-28-2014
sqls = 'delete from wo_est_processing_transpose a '
sqls += ' where exists ( '
if i_table_name = 'BUDGET_MONTHLY_DATA' then
   sqls += ' select 1 from clearing_wo_control_bdg b'
   sqls += ' where b.budget_id = a.work_order_id'
else
   sqls += ' select 1 from clearing_wo_control_bdg b, work_order_control woc'
   sqls += ' where b.budget_id = woc.budget_id'
   sqls += ' and woc.work_order_id = a.work_order_id'
end if
   sqls += ' )' 

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	uf_msg('Error deleting actuals being cleared from wo_est_processing_transpose '+sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if

uf_msg('Marking actual overheads from staged data ','I')
update wo_est_processing_transpose a
set new_old = 1
where month_number <= nvl(actuals_month_number,0)
and exists (
		select  1
		from estimate_charge_type e
		where a.est_chg_type_id = e.est_chg_type_id
			and nvl(e.afudc_flag,'Q') = 'O')
;
if sqlca.sqlcode < 0 then
		uf_msg('Error Updating Actual overhead records '+i_table_name+' '+sqlca.sqlerrtext,'E')
		rollback;
		return -1
end if
uf_msg('--Updated '+string(sqlca.sqlnrows)+' rows ','I')

///Call custom function we want to do this hear because this function may be called from other places
rtn =  f_budget_overheads_custom(1, i_calling_window, i_table_name)
if rtn < 0 then
	//any messaging should be handled by the function
	rollback;
	return -1
end if
if sqlca.f_client_extension_exists( 'f_client_budget_overheads' ) = 1 then
	rtn = sqlca.f_client_budget_overheads(1,i_calling_window, i_table_name)
	if rtn < 0 then
		//any messaging should be handled by the function
		rollback;
		return -1
	end if
end if
string args[]
args[1] = '1'
args[2] = i_table_name
args[3] = string(i_actuals_month)
rtn = f_wo_validation_control(82,args)
if rtn < 0 then
	//any messaging should be handled by the function
	rollback;
	return -1
end if


return 1
end function

public function longlong uf_delete_ovhs ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_delete_ovhs() 
//
//	  Purpose  :	This function deletes all previously calculated overheads for the work orders id's and revisions loaded in wo_est_processing_temp 
//		             
//
//   Arguments:	 none
//	
//   Returns :   1 if successfuly -1 if failed (SQL Error)
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 	

string sqls, revision, spread_table, spread_field, sqls2

///This is really slow at some clients and it doesn't really matter if there are extra est_monthly_id stranded.  We should give them a tool to clean this up 
///periodicly but for now it should be fine to just leave them
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	spread_table = 'BUDGET_MONTHLY_SPREAD'
	spread_field = 'BUDGET_MONTHLY_ID'
	revision = 'BUDGET_VERSION_ID'
else 
	spread_table = 'WO_EST_MONTHLY_SPREAD'
	spread_field = 'EST_MONTHLY_ID'
	revision = 'REVISION'
end if
//uf_msg('Deleting Spread factors','I')
//sqls = " "
//sqls += " delete from "+spread_table+ " c "
//sqls += " where exists ( "
//sqls += " select 1 "
//sqls += " from estimate_charge_type e, wo_est_processing_temp w, "+i_table_name+" a "
//sqls += " where e.est_chg_type_id = a.est_chg_type_id "
//sqls += " and nvl(e.afudc_flag,'Q') = 'O' "
//if i_table_name = 'BUDGET_MONTHLY_DATA' then
//	sqls+= " and a.budget_id = w.work_order_id "
//else
//	sqls += "   and a.work_order_id = w.work_order_id "
//end if
//sqls += "   and w.revision = a."+revision
//sqls += "   and a.est_chg_type_id = e.est_chg_type_id "
//sqls += "   and a."+spread_field+" = c."+spread_field
//sqls +=" ) "
//execute immediate :sqls;
//if sqlca.SQLCode < 0 then
//	uf_msg( "ERROR: Deleting Previously Calc Spread id's~n~n" + sqlca.SQLErrText,'E')
//	rollback;
//	return -1
//end if

uf_msg('Deleted '+string(sqlca.sqlnrows),'I')
uf_msg('Deleting Previouslly calculated overheads','I')
//sqls2 = " "
//sqls2 += " delete from "+i_table_name+" a "
//sqls2 += " where nvl(clearing_id,-1) > 0 "
sqls2 = " "
sqls2 += " delete from "+i_table_name+" a "
sqls2 += " where exists  ( "
sqls2 += "	select 1 "
sqls2 += "	from estimate_charge_type e, wo_est_processing_temp w "
sqls2 += "	where e.est_chg_type_id = a.est_chg_type_id "
sqls2 += "	   and nvl(e.afudc_flag,'Q') = 'O' "
if i_table_name = 'BUDGET_MONTHLY_DATA'then
	sqls2 += "   and a.budget_id = w.work_order_id "
else
	sqls2 += "   and a.work_order_id = w.work_order_id "
end if
sqls2 += "   and w.revision = a."+revision
sqls2 += " ) "
sqls2 +=" and not exists ( "
sqls2 +="  select 1 from clearing_wo_control_bdg b "
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	sqls2 +=" where b.budget_id = a.budget_id "
else
	sqls2 +=" ,work_order_control woc  where woc.budget_id = b.budget_id "
	sqls2 +="  and woc.work_order_id = a.work_order_id "
end if
sqls2 +=" ) "
execute immediate :sqls2;
if sqlca.SQLCode < 0 then
	uf_msg( "ERROR: Deleting Previously Calc Est's~n~n" + sqlca.SQLErrText,'E')
	rollback;
	return -1
end if
uf_msg('Deleted '+string(sqlca.sqlnrows),'I')



return 1
end function

public function longlong uf_insert_ovhs ();string sqls, spread_table, spread_field, revision, utility_account_col, sqls2, start_date_col, end_date_col, date_table, id_col, pk_col_name
string sqls3

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	spread_table = 'BUDGET_MONTHLY_SPREAD'
	spread_field = 'BUDGET_MONTHLY_ID'
	revision = 'BUDGET_VERSION_ID'
	utility_account_col = 'budget_plant_class_id'
	start_date_col = 'start_date'
	end_date_col = 'close_date'
	date_table = "budget_amounts"
	id_col = 'budget_id'
	pk_col_name = " budget_monthly_id "
else 
	spread_table = 'WO_EST_MONTHLY_SPREAD'
	spread_field = 'EST_MONTHLY_ID'
	revision = 'REVISION'
	utility_account_col = 'utility_account_id'
	start_date_col = 'est_start_date'
	end_date_col = 'est_complete_date'
	date_table = 'work_order_approval'
	id_col = 'work_order_id'
	pk_col_name = " est_monthly_id "
end if
uf_msg('Updating est_monthly_id','I')
// ### CDM - Maint 9123 - do not need est_monthly_id to be updated here
//// ### CDM - Maint 5815 - add index for performance...add ability to add hint to sql
//sqls = &
//"update wo_est_processing_transpose a "+&
//"set est_monthly_id = (select min(b.est_monthly_id) "+&
//"							 from wo_est_processing_transpose b "+&
//"							 where nvl(a.work_order_id,-9933) = nvl(b.work_order_id,-9933) "+&
//"								and a.revision = b.revision "+&
//"								and a.expenditure_type_id = b.expenditure_type_id "+&
//"								and a.est_chg_type_id = b.est_chg_type_id "+&
//"								and nvl(a.utility_account_id,-9933) = nvl(b.utility_account_id, -9933) "+&
//"								and nvl(a.department_id,-9933) = nvl(b.department_id, -9933) "+&
//"								and nvl(a.long_description,'-9933') = nvl(b.long_description, '-9933') "+&
//"								and nvl(a.job_task_id,'-9933') = nvl(b.job_task_id, '-9933') "+&
//"								and nvl(a.substitution_id,-9933) = nvl(b.substitution_id, -9933) "+&
//"								and a.company_id = b.company_id "+&
//"								and a.budget_id = b.budget_id "+&
//"								and decode(a.new_old,2,a.clearing_id,0) = decode(b.new_old,2,b.clearing_id,0) "+ /* CDM */&
//"								and nvl(a.from_work_order_id,-9933) = nvl(b.from_work_order_id,-9933) "+&
//"								) "+&
//"where nvl(new_old,0) <> 0 " /* CDM */
//sqls = f_sql_add_hint(sqls,"uo_ovhs.uf_insert_ovhs10")
//execute immediate :sqls;
//
//if sqlca.sqlcode < 0 then
//	uf_msg('Could not update est monthly id. '+sqlca.sqlerrtext,'E')
//	rollback;
//	return -1
//end if
//uf_msg('--Updated '+string(sqlca.sqlnrows)+' rows ','I')

uf_msg('Inserting new overhead results','I')
sqls = ' '
sqls += ' insert into '+i_table_name
sqls += ' (work_order_id, '+pk_col_name+', '+revision+', '
sqls += ' year , '
sqls += ' expenditure_type_id , '
sqls += ' est_chg_type_id , '
sqls += utility_account_col +', '
sqls += ' january , '
sqls += ' february , '
sqls += ' march, '
sqls += ' april , '
sqls += ' may, '
sqls += ' june, '
sqls += ' july , '
sqls += ' august , '
sqls += ' september , '
sqls += ' october , '
sqls += ' november, '
sqls += ' december, '
sqls += ' total , '
sqls += ' future_dollars , '
sqls += ' department_id, '
sqls += ' long_description , '
sqls += ' hist_actuals,  '
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	sqls += ' budget_id, '
end if
if i_table_name = 'WO_EST_MONTHLY' then //AWW PC-1084
	sqls += ' wo_work_order_id, '
end if
sqls += ' attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, '
sqls += ' job_task_id ) '
sqls += '  (select '
sqls += ' work_order_id, '
sqls += ' min(est_monthly_id),revision, '
sqls += ' year , '
sqls += ' expenditure_type_id , '
sqls += ' est_chg_type_id , '
sqls += ' utility_account_id , '
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'01',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'02',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'03',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'04',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'05',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'06',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'07',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'08',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'09',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'10',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'11',amount,0)),0), "
sqls += " nvl(sum(decode(substr(to_char(trans.month_number),-2),'12',amount,0)),0), "
sqls += " nvl(sum(amount),0), "
sqls += ' decode(trans.new_old,2,clearing_id,0) future_dollars , '
sqls += ' department_id, '
sqls += ' long_description , '
sqls += ' 0, '
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	sqls += ' budget_id, '
end if
if i_table_name = 'WO_EST_MONTHLY' then //AWW PC-1084
	sqls += ' wo_work_order_id, '
end if
sqls += ' attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id, '
sqls += ' job_task_id '
sqls += ' from wo_est_processing_transpose trans '
sqls += ' where nvl(new_old,0) <> 0 '
sqls += ' group by work_order_id, revision, department_id,long_description,job_task_id, '
sqls += ' year , '
sqls += ' expenditure_type_id , '
sqls += 'decode(new_old,2,clearing_id,0), '
sqls += ' est_chg_type_id , '
sqls += ' utility_account_id '
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	sqls += ' ,budget_id '
end if
if i_table_name = 'WO_EST_MONTHLY' then //AWW PC-1084
	sqls += ' ,wo_work_order_id '
end if
sqls += ' ,attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id '
sqls += ' ) '
execute  immediate :sqls;
if sqlca.sqlcode < 0 then
	uf_msg('Could not load monthly data from monthly_transpose table. '+sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if
uf_msg('Inserted new rows '+string(sqlca.sqlnrows),'I')
// ### CDM - Maint 9123 - do not insert zero dollar rows for missing years - also no need to insert spread records
//if i_table_name <> 'WO_EST_MONTHLY_SUBS_DET' then
//	uf_msg('Inserting missing combinations','I')
//	sqls2 = " "
//	sqls2 += ' insert into '+i_table_name
//	sqls2 += " (work_order_id, "+pk_col_name+", "+revision+", year, expenditure_type_id, est_chg_type_id, "
//	sqls2 += "	department_id, "+utility_account_col+", job_task_id,  january,february, march,april, "
//	sqls2 += "	may,june,july, august,september, october,november, december, total, long_description, "
//	sqls2 += "	hist_actuals, future_dollars "
//	if i_table_name = 'BUDGET_MONTHLY_DATA' then
//		sqls2+= ' ,budget_id '
//	end if
//	sqls2 += " ) "
//	sqls2 += " select a.work_order_id, "+pk_col_name+", a."+revision+", b.year, min(a.expenditure_type_id), "
//	sqls2 += "	min(a.est_chg_type_id), min(a.department_id), min(a."+utility_account_col+"), min(a.job_task_id), 0, 0, "
//	sqls2 += "	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, min(a.long_description), 0, 0 "
//	if i_table_name = 'BUDGET_MONTHLY_DATA' then
//		sqls2+= ' ,a.budget_id '
//	end if
//	sqls2 += " from "+i_table_name+" a, pp_table_years b "
//	sqls2 += " where b.year >= ( "
//	sqls2 += "	select to_number(to_char(c."+start_date_col+",'yyyy')) "
//	sqls2 += "	from "+date_table+" c "
//	sqls2 += "	where c."+id_col+" = a."+id_col
//	sqls2 += "     and c."+revision+" = a."+revision
//	sqls2 += "	) "
//	sqls2 += " and b.year <= ( "
//	sqls2 += "	select to_number(to_char(d."+end_date_col+",'yyyy')) "
//	sqls2 += "	from "+date_table+" d "
//	sqls2 += "	where d."+id_col+" = a."+id_col
//	sqls2 += "       and d."+revision+" = a."+revision
//	sqls2 += "	) "
//	sqls2 += " and exists ( "
//	sqls2 += "	select 1 "
//	sqls2 += "	from wo_est_processing_temp q "
//	sqls2 += "	where q.work_order_id = a."+id_col
//	sqls2 += "		and q.revision = a."+revision
//	sqls2 += "	) "
//	sqls2 += " and not exists ( "
//	sqls2 += "	select 1 from "+i_table_name+" e "
//	sqls2 += "	where a."+id_col+" = e."+id_col
//	sqls2 += "	and a."+revision+" = e."+revision
//	sqls2 += "	and a."+pk_col_name+" = e."+pk_col_name
//	sqls2 += "	and b.year = e.year "
//	sqls2 +="	) "
//	sqls2 +=" group by work_order_id, "+pk_col_name+", "+revision+", b.year "
//	if i_table_name = 'BUDGET_MONTHLY_DATA' then
//		sqls2 += ' ,a.budget_id '
//	end if
//	execute immediate :sqls2;
//	if sqlca.sqlcode < 0 then
//		uf_msg('Could not insert into missing combinations into  '+i_table_name+' for new monthly entries. '+sqlca.sqlerrtext,'E')
//		rollback;
//		return -1
//	end if
//	
//	uf_msg('Inserted new rows '+string(sqlca.sqlnrows),'I')
//	uf_msg('Inserting spread data','I')
//	sqls3 = ' '
//	sqls3 += ' insert into '+spread_table 
//	sqls3 += '	( '+spread_field+' ) '
//	sqls3 += ' select  est_monthly_id '
//	sqls3 += '	from wo_est_processing_transpose a '
//	sqls3 += '	where  nvl(a.new_old ,0) <> 0'
//	sqls3 +='  minus '
//	sqls3 +='  select '+spread_field+' from '+spread_table 
//	execute immediate :sqls3;
//	if sqlca.sqlcode < 0 then
//		uf_msg('Could not insert into '+i_table_name+'_SPREAD for new monthly entries. '+sqlca.sqlerrtext,'E')
//		rollback;
//		return -1
//	end if
//	uf_msg('Inserted new rows '+string(sqlca.sqlnrows),'I')
//end if






return 1

end function

public subroutine uf_msg (string a_msg, string a_error);
//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_msg() 
//
//	  Purpose  :	returns messages to the user based on boolean variables 
//		             
//
//   Arguments:	a_msg  : string message to return to the user
//					a_error : string that tells what type of message
//								E = Error Message
//								W = Warning Message
//								I = Information Message
//	
//   Returns :   none
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_title, time_string

if i_add_time_to_messages then
	time_string = string(now())
	a_msg = a_msg + ' '+time_string
end if

message_title = 'Budget Overhead Calculation'

if i_status_box then
	f_status_box(message_title,a_msg)
end if


if i_pp_msgs then
	f_pp_msgs(a_msg)
end if

if i_progress_bar  then
	f_progressbar(message_title,a_msg,100,i_status_position)
end if

if i_statusbox_onerror and a_error = 'E' then
	f_status_box(message_title,a_msg)
end if

if i_messagebox_onerror and a_error = 'E' then
	messagebox(message_title,a_msg)
end if


end subroutine

public function longlong uf_budget_overheads_cr ();
string sqls, msg_box, bv_descr, description, sqls2, create_sqls, create_sqls2, table_name_entry, table_name_data, &
	element, delete_1, month_array[], exists_where, dev_role, cmd, dd_key, null_string
longlong g, num_rows, rtn, process_id, num_months, start_month, end_month, allocation_id, start_month_reset, sessionid, occurrence_id, null_num
longlong posting_id

//setnull(null_string)
//setnull(null_num)
//sqls = "select b.description,a.run_order,b.allocation_id from cr_allocation_control_bdg b " + &
//	" where nvl(b.capital_entry_allocation,0) = 1 "
//	
//uo_ds_top ds_allocations
//ds_allocations = create uo_ds_top
//f_create_dynamic_ds(ds_allocations,"grid",sqls,sqlca,true)
//
//ds_allocations.setsort('#2 A')
//ds_allocations.sort()
//num_rows = ds_allocations.rowcount()
//
//
//if i_funding_wo_indicator <> 2 then
//	select min(nvl(est_start_date,sysdate + 1000000)), max(nvl(est_complete_date,sysdate - 1000000))
//	into :start_month, :end_month
//	from work_order_approval woa
//	where exists (
//		select 1
//		from wo_est_processing_temp w
//		where w.work_order_id = woa.work_order_id
//		  and w.revision = woa.revision)
//	;
//else
//	select min(nvl(start_date,to_date(bv.start_year||'01','YYYMM'))), max(nvl(close_date,to_date(bv.end_year||'12','YYYYMM')))
//	into :start_month, :end_month
//	from budget_amounts woa, budget_version bv
//	where exists (
//		select 1
//		from wo_est_processing_temp w
//		where w.work_order_id = woa.budget_id
//		  and w.revision = woa.budget_version_id)
//		and woa.budget_version_id = bv.budget_version_id
//	;
//end if
//
//
//
//
//start_month = i_actuals_month
//
/////This is a global temp table clear it out in case the user has run these allocations twice i the same session
//delete from cr_budget_data_temp;
//
//
//
//
//select process_id into :process_id from pp_processes
//where lower(description) = 'cr - budget entry allocations';
//
//if isnull(process_id) or process_id = 0 then 
//	messagebox("","ERROR: Could not find process_id for CR - Budget Entry Allocations~n~nThe allocation will not be run")
//	return -1
//end if
//
//g_sqlca_logs.uf_connect()
//
//if g_sqlca_logs.SQLCode = 0 then
//	
//	if upper(s_user_info.user_id) <> 'PWRPLANT' then
//		//	Begin: Code from Roger (w_login.cb_ok) to assign the role
//		if  s_sys_info.dbms = 'oracle' then  
//			setnull(dev_role)
//			if isnull(dev_role) then
//				dev_role = 'pwrplant_role_dev'
//			end if
//			// set role
//			dev_role =  g_sqlca_logs.uf_get_default_roles(dev_role) + dev_role 
//			cmd = "begin dbms_session.set_role(~'" + dev_role + " identified by Tr9uI22k~'); end;"
//			execute immediate :cmd using g_sqlca_logs;
//			if sqlca.sqlcode = -1 then 
//				  messagebox("ORACLE Error Setting Roles ", sqlca.SQLErrText)
//			end if
//		end if
//		//	End : Code from Roger
//	end if
//	
//	rtn = f_pp_msgs_start_log(process_id)
//	
//	if g_sqlca_logs.SQLCode = 0 then
//		occurrence_id = g_occurrence_id
//		commit using g_sqlca_logs;
//	else
//		messagebox("Validate", "ERROR: error in f_pp_msgs_start_log()~n~n" + &
//			g_sqlca_logs.SQLErrText, Exclamation!)
//		rollback using g_sqlca_logs;
//		g_sqlca_logs.uf_disconnect()
//		if g_sqlca_logs.SQLCode < 0 then
//			messagebox("Validate", "ERROR: in g_sqlca_logs disconnecting from database.~n~n" + &
//				g_sqlca_logs.SQLErrText, Exclamation!)	
//		end if
//		return -1
//	end if
//	
//else
//	messagebox("Validate", "ERROR: g_sqlca_logs could not connect to the database.")
//	g_sqlca_logs.uf_disconnect()
//	if g_sqlca_logs.SQLCode < 0 then
//		messagebox("Validate", "ERROR: in g_sqlca_logs disconnecting from database.~n~n" + &
//			g_sqlca_logs.SQLErrText, Exclamation!)	
//	end if
//	return -1
//end if
//
//
//
////f_pp_msgs_start_log(process_id)
//open(w_cr_budget_entry_alloc_msg)
//
/////need code here to transpose data to cr_budget_data_temp I should transpose everything and then not process actuals month
/////Make sure that the actuals month have the allocation indicator set
//
//select id into :posting_id
//  from cr_to_budget_to_cr_control
// where to_projects = 0
//	and projects_table = :i_table_name;
//
//	
//if isnull(posting_id) or posting_id = 0 then
//	messagebox("", "ERROR: Could not find posting id!", stopsign!)
//	return -1
//end if
//
//uo_cr_to_budget_to_cr uo_cr
//uo_cr = CREATE uo_cr_to_budget_to_cr
//
//uo_cr.i_only_in_bv = false
//uo_cr.i_use_status_box = false//false means it will use f_pp_msgs
//uo_cr.i_wo_est_proc_temp = true
//uo_cr.i_alloc = true
//
//
//rtn = uo_cr.uf_read(posting_id, null_num, null_num, 'FAKE BV FOR CAP' )
//
//if rtn < 0 then
//	messagebox("", "ERROR: Running uo_cr_to_budget_to_cr.uf_read~n~nSee " + &
//				  "online logs for details.")
//	return -1
//end if
//
//delete from cr_budget_data_temp
//where nvl(allocation_id,0) = -99
//and month_number > :start_month
//;
//
//uo_cr_allocations_bdg_entry uo_alloc
//uo_alloc = create uo_cr_allocations_bdg_entry
//
//start_month_reset = start_month
//
//dd_key = 'CAPITAL ENTRY ALLOCATIONS'
//
//for g = 1 to num_rows
//	
//	allocation_id = ds_allocations.getitemnumber(g,"allocation_id")
//	select description into :description from cr_allocation_control_bdg
//	where allocation_id = :allocation_id;
//	
//	start_month = start_month_reset
//	
//	f_pp_msgs( "*******************************************")
//	f_pp_msgs("Started: " + string(allocation_id) + ": " + description )
//	f_pp_msgs("")	
//
//	do while start_month <= end_month
//		
//		f_pp_msgs(" -- " + string(start_month))
//		
//		uo_alloc.i_allocation_id  = allocation_id
//		uo_alloc.i_budget_version = bv_descr
//		uo_alloc.i_month_number   = start_month
//		uo_alloc.i_month_period   = 0
//		uo_alloc.i_msgbox         = false
//		uo_alloc.i_run_as_a_test  = false
//		uo_alloc.i_whole_month    = true
//		uo_alloc.i_budget_entry_dd_key = dd_key
//			
//		rtn = uo_alloc.uf_start_allocations()
//		
//		if rtn < 0 then 
//			w_cr_budget_entry_alloc_msg.cb_retrieve.triggerevent(clicked!)
//			return -1
//		end if
//		
//		start_month++
//		if long(right(string(start_month),2)) > 12 then start_month = start_month + 88
//				
//	loop
//	
//	f_pp_msgs("")
//	f_pp_msgs( "Finished: " + string(allocation_id) + ": " + description )
//	f_pp_msgs ("*******************************************")
//	w_cr_budget_entry_alloc_msg.cb_retrieve.triggerevent(clicked!)
//next
//
//uo_cr.i_only_in_bv = false
//uo_cr.i_use_status_box = false//false means it will use f_pp_msgs
//uo_cr.i_wo_est_proc_temp = true
//uo_cr.i_alloc = true
//uo_cr.i_additional_where_clause = ' and allocation_id is not null '
//
//select id into :posting_id
//from cr_to_budget_to_cr_control
//where to_projects = 1
//	and projects_table = :i_table_name;
//	
//
//
//
//rtn = uo_cr.uf_read(posting_id, null_num, null_num, 'FAKE BV FOR CAP' )
//
//if rtn < 0 then
//	messagebox("", "ERROR: Running uo_cr_to_budget_to_cr.uf_read~n~nSee " + &
//				  "online logs for details.")
//	return -1
//end if
//
/////need code here to bring the cr_budget_data_temp data back to wo_est_monthly where allocation indicator is not null no deletes needed they were handled earlier
//w_cr_budget_entry_alloc_msg.cb_retrieve.triggerevent(clicked!)
//
//f_pp_msgs_end_log()
//
//if g_sqlca_logs.SQLCode <> 0 then
//	messagebox("Validate", "ERROR: error in f_pp_msgs_end_log()")
//	g_sqlca_logs.uf_disconnect()
//	if g_sqlca_logs.SQLCode < 0 then
//		messagebox("Validate", "ERROR: in g_sqlca_logs disconnecting from database.~n~n" + &
//			g_sqlca_logs.SQLErrText, Exclamation!)	
//	end if
//	return -1
//end if
//
//g_sqlca_logs.uf_disconnect()
//
//if g_sqlca_logs.SQLCode < 0 then
//	messagebox("Validate", "ERROR: in g_sqlca_logs disconnecting from database.~n~n" + &
//		g_sqlca_logs.SQLErrText, Exclamation!)	
//end if

return 1
	








end function

public function longlong uf_get_actuals_mn ();
longlong start_month

longlong woa_start_month, woa_act_month, bv_start_month, bv_act_month

setnull(woa_start_month)
setnull(woa_act_month)
setnull(bv_start_month)
setnull(bv_act_month)
///First lets get the actuals month if it wasn't passed in it will be null at this point if it wasn't passed in
///I'm intentionally not using the i_budget_Version variable because this is set when calculating AFUDC for an entire budget version
///When this is done an actuals month is always set. so none of this logic will fire
/*- Funding Projects
		- max work order approval actauls month number
		- if above is null then max actuals month number on any budget version the funding project is hooked to
		- if above is null then the min estimate start date - 1 month 
 - Budget Items
		- min actuals month number on any budget version that the funding project revisions are in
		- min Dec of year prior to start year on any budget versions that the funding project revisons are in
 - Work Orders
 		- max w
 		- min est_start_month - 1
*/

if i_funding_wo_indicator <> 2 then///For funding projects and work orders get the woa 
	select min(to_number(to_char(est_start_date, 'yyyymm'))),
		decode(max(nvl(woa.actuals_month_number,0)),0,min(nvl(to_number(to_char(add_months(est_start_date,-1), 'yyyymm')),0)),max(nvl(woa.actuals_month_number,0)))
	into :woa_start_month, :woa_act_month
	from work_order_approval woa, wo_est_processing_temp t
	where woa.work_order_id = t.work_order_id
	and woa.revision =t.revision
	;
	if sqlca.sqlcode < 0 then 
		uf_msg('Error Selecting actuals month from work_order_approval (FP/WO)','E')
		uf_msg(sqlca.sqlerrtext,'E')
		return -1
	end if
else
	select min(to_number(to_char(nvl(start_date,sysdate), 'yyyymm'))),
			0
	into :woa_start_month, :woa_act_month
	from budget_amounts woa, wo_est_processing_temp t
	where woa.budget_id = t.work_order_id
	and woa.budget_version_id =t.revision
	;
	if sqlca.sqlcode < 0 then 
		uf_msg('Error Selecting actuals month from work_order_approval (BI)','E')
		uf_msg(sqlca.sqlerrtext,'E')
		return -1
	end if
end if

if i_budget_version <> 0 then
	select min(start_year*100+1),
			max(actuals_month)
	into :bv_start_month, :bv_act_month
	from budget_version bv
	where bv.budget_version_id = :i_budget_version
	 ;
	if sqlca.sqlcode < 0 then 
		uf_msg('Error Selecting actuals month from budget version (bv_id)','E')
		uf_msg(sqlca.sqlerrtext,'E')
		return -1
	end if
else
	if i_funding_wo_indicator = 1  then	
		select min(start_year*100+1),
				max(actuals_month)
		into :bv_start_month, :bv_act_month
		from budget_version bv
		where exists (
			select 1
			from budget_version_fund_proj bvfp, wo_est_processing_temp w
			where bvfp.budget_version_id = bv.budget_version_id
				and bvfp.work_order_id = w.work_order_id
				and bvfp.revision = w.revision
				and bvfp.active = 1)
		 ;
		 if sqlca.sqlcode < 0 then 
			uf_msg('Error Selecting actuals month from budget_Version(FP)','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
	elseif i_funding_wo_indicator = 0 then
		bv_start_month = 0
		bv_act_month = 0
	else 
		select min(start_year*100+1),
				max(actuals_month)
		into :bv_start_month, :bv_act_month
		from budget_version bv, wo_est_processing_temp t
		where bv.budget_version_id = t.revision
		 ;
		if sqlca.sqlcode < 0 then 
			uf_msg('Error Selecting actuals month from budget version (BI)','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
	end if
end if



if isnull(woa_start_month) then woa_start_month = 0
if isnull(woa_act_month) then woa_act_month = 0
if isnull(bv_start_month) then bv_start_month = 0
if isnull(bv_act_month) then bv_act_month = 0

choose case i_funding_wo_indicator
	case 1/*Funding Projects*/
		///For actuals pick the greater of the budget version and work order approval actuals months
		if  woa_act_month > bv_act_month then
			i_actuals_month = woa_act_month
		else
			i_actuals_month = bv_act_month
		end if	
		
		if isnull(i_actuals_month) then
			i_actuals_month = 0
		end if
		
		///Cwip start month should come from the budget version otherwise just set it to the actuals month
		 if bv_start_month > 190001 then
			i_cwip_start_month = bv_start_month
		else
			i_cwip_start_month = i_actuals_month
		end if					
		
		///if the cwip start_month > actuals month set it to the actuals month
		if i_cwip_start_month > i_actuals_month then
			i_cwip_start_month = i_actuals_month
		end if
		
	case 0 /*Work Orders*/
		///For work orders the actuals month and start month should be the same
		if woa_act_month > 190001 then
			i_cwip_start_month = woa_act_month
			i_actuals_month = woa_act_month
		else
			i_cwip_start_month = woa_start_month
			i_actuals_month = woa_start_month
		end if
					
	case 2 /*Budget Items*/
		i_actuals_month = bv_act_month
		i_cwip_start_month = bv_start_month
		
		if i_cwip_start_month > i_actuals_month then
			i_cwip_start_month = i_actuals_month
		end if
		
end choose
		
	
		
if isnull(i_actuals_month) or i_actuals_month < 190001 then 
	uf_msg('ERROR finding actuals month for i_funding_wo_indicator = '+string(i_funding_wo_indicator),'E')
	return -1
end if

if isnull(i_cwip_start_month) then
	i_cwip_start_month = i_actuals_month
end if

///Now the calc start month should be just one month after actuals month
if mid(string(i_actuals_month),5,2) = '12' then
	i_start_month_number = i_actuals_month + 89
else
	i_start_month_number = i_actuals_month + 1
end if
		
		
  
//if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then
//	Choose case i_funding_wo_indicator
//	case 1
//		select min(actuals_month), min(start_year * 100 + 1)
//		into :i_actuals_month_number, :i_cwip_start_month
//		from budget_version bv
//		where exists (
//			select 1
//			from budget_version_fund_proj bvfp, wo_est_processing_temp w
//			where bvfp.budget_version_id = bv.budget_version_id
//				and bvfp.work_order_id = w.work_order_id
//				and bvfp.revision = w.revision
//				and bvfp.active = 1)
//		 ;
//		if uf_check_sql('Selecting actuals month from budget version') <> 1 then return -1
//		if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then
//			select ((min(start_year) - 1) * 100)+12
//			into :i_actuals_month_number
//			from budget_version bv
//			where exists (
//				select 1
//				from budget_version_fund_proj bvfp, wo_est_processing_temp w
//				where bvfp.budget_version_id = bv.budget_version_id
//					and bvfp.work_order_id = w.work_order_id
//					and bvfp.revision = w.revision
//					and bvfp.active = 1)
//			 ;
//			 if uf_check_sql('Selecting start month from budget version') <> 1 then return -1
//		end if
//	case 2
//		select min(actuals_month), start_year * 100 + 1
//		into :i_actuals_month_number, :i_cwip_start_month
//		from budget_version bv
//		where exists (
//			select 1
//			from wo_est_processing_temp w
//			where w.revision = bv.budget_version_id)
//		 ;
//		if uf_check_sql('Selecting actuals month from budget version') <> 1 then return -1
//		if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then
//			select ((min(start_year) - 1) * 100)+12
//			into :i_actuals_month_number
//			from budget_version bv
//			where exists (
//				select 1
//				from wo_est_processing_temp w
//				where w.revision = bv.budget_version_id)
//			;
//			 if uf_check_sql('Selecting start month from budget version') <> 1 then return -1
//		end if
//	end choose
//	
//	if i_funding_wo_indicator <> 2 then
//		if isnull(i_actuals_month_number) or i_actuals_month_number < 190001 then 
//			select add_months(min(est_start_date),-1)
//			into :i_actuals_month_number
//			from work_order_approval woa
//			where exists (
//					select 1
//					from  wo_est_processing_temp w
//					where woa.work_order_id = w.work_order_id
//						and woa.revision = w.revision)
//			;
//		end if
//	end if
//end if//if actuals month is null or < 190001



	

///Now lets find the CWIP start month.  THis is how far back we will calculate CWIP balances prior to the actuals month.  
//
//if i_budget_version <> 0 then 
//	select start_year * 100 + 1
//	into :i_cwip_start_month
//	from budget_version
//	where budget_version_id = :i_budget_version
//	;
//end if

///CJR moved above
//if  isnull(start_month) and i_funding_wo_indicator <> 0 then 
//	if i_table_name = 'WO_EST_MONTHLY'  then/*FUnding Projects*/
//		select min(start_year * 100 + 1)
//		into :start_month
//		from budget_version b
//		where exists (
//			select 1
//			from budget_version_fund_proj bvfp, wo_est_processing_temp w
//			where bvfp.budget_version_id = b.budget_version_id
//				and bvfp.work_order_id = w.work_order_id
//				and bvfp.revision = w.revision)
//		;
//	else/*Budget ITems*/
//		select min(start_year * 100 + 1)
//		into :start_month
//		from budget_version b
//		where exists (
//			select 1
//			from  wo_est_processing_temp w
//			where w.revision = b.budget_version_id)
//		;
//	end if
//end if
	


//if isnull(i_cwip_start_month) or i_cwip_start_month > i_actuals_month_number or i_cwip_start_month < 190001 then 
//	start_month = i_actuals_month_number
//end if


return 1
end function

public function integer uf_budget_ovhs_clearing (longlong a_clearing_id, longlong a_sum_or_org, longlong a_load_by_jt, longlong a_load_by_dept, longlong a_load_by_ua, longlong a_load_by_desc, longlong a_load_by_wo, longlong a_load_by_attr[]);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads_fp.uf_budget_ovhs_loading() 
//
//	  Purpose  :	This function calculates the loading type overheads and stores them in the transpose table
//		             
//
//   Arguments:	 none
//	
//   Returns :   1 if successfuly -1 if failed (SQL Error)
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    		--------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 	

string sqls, sqls2, description, sqls_rate, &
	elig_table, elig_column, elig_table_header, elig_table_join, elig_table_join_pk, &
	ex_table, ex_column
longlong budget_id, work_order_id, clear_wo_id, clear_by_dept, dollar_qty_indicator
decimal{2} clear_amount

select description, rates_by_department, dollar_quantity_indicator
into :description, :clear_by_dept, :dollar_qty_indicator
from clearing_wo_control_bdg
where clearing_id = :a_clearing_id
;
if sqlca.sqlcode < 0 then
	uf_msg('Could not Select Description for this allocation. ','I')
end if

if isnull(clear_by_dept) then clear_by_dept = 0
if isnull(dollar_qty_indicator) then dollar_qty_indicator = 1


	
///1 = dollars
///2 = hours
///0 = quantity 

if i_table_name = 'BUDGET_MONTHLY_DATA' and dollar_qty_indicator <> 1 then 
	uf_msg('ERROR Budget allocations can not be run against quantities or hours '+description+' please check the setup and try again','E')
	return 2
end if


uf_msg('Cleaning out processing table','I')
delete from wo_est_processing_mo_id 
;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation. ','E')
	rollback;
	return -1
end if
uf_msg('--Deleted '+string(sqlca.sqlnrows)+' rows ','I')

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	//	 a_sum_or_org = 1 means that we base off of the budget_summary_id
	if a_sum_or_org = 1 then
		elig_table = 'budget_summary_clear_dflt_bdg'
		elig_column = 'budget_summary_id'
		elig_table_header = 'budget_summary'
	else 
		elig_table = 'budget_org_clear_dflt_bdg'
		elig_column = 'budget_organization_id'
		elig_table_header = 'budget_organization'
	end if
	elig_table_join = 'budget'
	elig_table_join_pk = 'budget_id'
	
	ex_table = 'wo_clear_over_bdg'
	ex_column = 'budget_id'
else
	elig_table = 'wo_type_clear_dflt_bdg'
	elig_column = 'work_order_type_id'
	elig_table_header = 'work_order_type'
	elig_table_join = 'work_order_control'
	elig_table_join_pk = 'work_order_id'
	
	ex_table = 'wo_fp_clear_over_bdg'
	ex_column = 'work_order_id'
end if

select budget_id
into :budget_id
from clearing_wo_control_bdg
where clearing_id = :a_clearing_id
;

if isnull(budget_id) then
	uf_msg('ERROR retrieving clearing project for this clearing '+description+' please check the setup and try again','E')
	return 2
end if


if i_funding_wo_indicator <> 2 then
	setnull(clear_wo_id)
	select max(work_order_id)
	into :clear_wo_id
	from work_order_control
	where budget_id = :budget_id
	and funding_wo_indicator = :i_funding_wo_indicator
	;
else
	setnull(clear_wo_id)
	select max(work_order_id)
	into :clear_wo_id
	from work_order_control
	where budget_id = :budget_id
	and funding_wo_indicator = 1
	;
end if

delete from clearing_wo_rate_bdg
where clearing_id = :a_clearing_id
;

sqls_rate = "insert into clearing_wo_rate_bdg(clearing_id, accounting_month, calc_rate, company_id, department_id) "

sqls_rate += "select "+string(a_clearing_id)+", pot.month_number, decode(sum(base.base),0,0,max(pot.pot)/sum(base.base)), pot.company_id "  
if clear_by_dept = 1 then
	sqls_rate +=", pot.department_id "
else
	sqls_rate +=", -1 "
end if
sqls_rate += "from  " + & 
"	( select woc.company_id, wem.year * 100 + p.month_num month_number, " 
if clear_by_dept = 1 then
	sqls_rate +="  wem.department_id, "
end if
sqls_rate+="		sum(decode(p.month_num,1,wem.JANUARY ,  " + & 
"							  2,wem.february,  " + & 
"							  3,wem.march,  " + & 
"							  4,wem.april,  " + & 
"							  5,wem.may,  " + & 
"							  6,wem.june,  " + & 
"							  7,wem.july,  " + & 
"							  8,wem.august,  " + & 
"							  9,wem.september,  " + & 
"							  10,wem.october,  " + & 
"							  11,wem.november,  " + & 
"							  12,wem.december)) pot "
if i_funding_wo_indicator <> 2 then//Funding Projects
	sqls_rate = sqls_rate + "	   from work_order_control woc, "+i_table_name+" wem, budget_version_fund_proj bvfp, pp_table_months p, " + & 
	"				clearing_wo_control_bdg c " + & 
	"	  where woc.work_order_id = "+string(clear_wo_id) + & 
	"		and wem.work_order_id = "+string(clear_wo_id) + & 
	"		and wem.work_order_id = bvfp.work_order_id " + & 
	"		and wem.revision = bvfp.revision " + & 
	"		and bvfp.budget_version_id = "+string(i_budget_version) + & 
	"		and bvfp.active = 1 "
else///Budget Items
	sqls_rate = sqls_rate +"	   from budget woc, "+i_table_name+" wem, pp_table_months p, 	clearing_wo_control_bdg c " + & 
	"	  where woc.budget_id = "+string(budget_id) + & 
	"		and wem.budget_id = "+string(budget_id) + &  
	"		and wem.budget_version_id = "+string(i_budget_version) 
end if
sqls_rate = sqls_rate + "		and c.clearing_id = "+string(a_clearing_id) + & 
"		and c.est_chg_type_id = wem.est_chg_type_id " + & 
"		and nvl(wem.future_dollars,-1) <> "+string(a_clearing_id) +&
"	group by woc.company_id, wem.year * 100 + p.month_num"
if clear_by_dept = 1 then
	sqls_rate +=" ,wem.department_id "
end if
sqls_rate += " ) pot , " + & 
"	( select wem.company_id,  wem.month_number, sum(decode("+string(dollar_qty_indicator)+",1,wem.amount,2,wem.hours,wem.quantity)) base		 " 
if clear_by_dept = 1 then
	sqls_rate += ", wem.department_id "
end if
sqls_rate += "	from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb, "+elig_table_join+" elig_join, "+elig_table+" elig, wo_est_processing_transpose wem " + & 
"	where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+"  " + & 
"		and nvl(elig_join."+elig_column+",-998833) = elig."+elig_column+" " + & 
"		and cwcb.clearing_id = elig.clearing_id  " + & 
"		and cwcb.clearing_id = "+string(a_clearing_id) + & 
"		and cwcb.overhead_basis_id = oabb.overhead_basis_id  " +&
"		and oabb.est_chg_type_id = wem.est_chg_type_id "+&
"		and oabb.expenditure_type_id = wem.expenditure_type_id "+&
" 		and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "+&
"		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "+&
"		and nvl(nvl(oabb.job_task_id,wem.job_task_id),'-998833') = nvl(wem.job_task_id,'-998833') "+&
"		and wem.month_number > nvl(wem.actuals_month_number,0)  " + & 
"		and wem.budget_id <> "+string(budget_id)+&
"		and not exists (  " + & 
"				select 1 from "+ex_table+" ex " + & 
"				where ex."+ex_column+" = elig_join."+ex_column+"  " + & 
"					and ex.include_exclude = 0  " + & 
"					and ex.clearing_id = "+string(a_clearing_id) + & 
"					)  " + & 
"Group by wem.company_id, wem.month_number " 
if clear_by_dept = 1 then
	sqls_rate += ", wem.department_id "
end if
sqls_rate += "	union all " + & 
"select wem.company_id,  wem.month_number month_number, sum(amount) base " 
if clear_by_dept = 1 then
	sqls_rate += ", wem.department_id "
end if
sqls_rate+="from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,   " + & 
"		"+elig_table_join+" elig_join, wo_est_processing_transpose wem, "+ex_table+" ex	 " + & 
"		where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+"  " + & 
"		and elig_join."+ex_column+" = ex."+ex_column+"  " + & 
"		and ex.include_exclude = 1  " + & 
"		and ex.clearing_id ="+string(a_clearing_id) + & 
"		and cwcb.clearing_id ="+string(a_clearing_id) + & 
"		and cwcb.overhead_basis_id = oabb.overhead_basis_id  " + & 
"		and oabb.est_chg_type_id = wem.est_chg_type_id "+&
"		and oabb.expenditure_type_id = wem.expenditure_type_id "+&
" 		and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "+&
"		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "+&
"		and nvl(nvl(oabb.job_task_id,wem.job_task_id),'-998833') = nvl(wem.job_task_id,'-998833') "+&
"		and wem.budget_id <> "+string(budget_id)+&
"		and nvl(elig_join."+elig_column+",-993388)  in  " + & 
"		( select "+elig_column+" from "+elig_table_header+" " + & 
  "			minus  " + & 
       "select "+elig_column + & 
"		from "+elig_table+" a " + & 
"		where a.clearing_id = cwcb.clearing_id  " + & 
"				union   " + & 
"		select -993388 from dual  " + & 
"					)  " + & 
"Group by wem.company_id, wem.month_number "
if clear_by_dept = 1 then
	sqls_rate += ", wem.department_id) base " 
else
	sqls_rate += " ) base "
end if
sqls_rate +="where base.month_number = pot.month_number " + & 
"and base.company_id = pot.company_id " 
if clear_by_dept = 1 then
	sqls_rate += "and base.department_id = pot.department_id "
end if
sqls_rate += " Group by pot.company_id,  pot.month_number "
if clear_by_dept = 1 then
	sqls_rate += " , pot.department_id "
end if
execute immediate :sqls_rate;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert clearing rates for allocation into clearing_wo_rate_bdg. '+sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if





///Insert the eligible charges this includes
	///estimate lines in the basis on Budget Items that have a budget sum/budget org in the eligible list
	///estimate lines in the basis on Budget Items that have a budget sum/budget org not in the eligible list but in the include list
///Excludes
	///Estimate lines that are in the basis on budget items that have a budget sum/budget org in the eligible list but in the excluding list
	///Estime lines not in the basis
	///Estimate lines on budget items that have a budget sum/budget org not in the eligible list and not in the include list
uf_msg('Staging new overhead line items','I')
sqls = ""
sqls += "insert into wo_est_processing_mo_id "
sqls += "(work_order_id, est_chg_type_id,expenditure_type_id, "
if a_load_by_jt = 1 then
	sqls += " job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" long_description, " 
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls += " wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls += ' attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' attribute10_id,  '
end if
sqls += "budget_id,company_id, substitution_id, from_work_order_id, est_monthly_id) "
sqls += "( "
sqls += "	select a.*, pwrplant1.nextval "
sqls += "from "
sqls += "		("
sqls += "			select distinct wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,"
if a_load_by_jt = 1 then
	sqls += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls += " wem.wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' wem.attribute10_id,  '
end if
sqls += "	wem.budget_id,wem.company_id, wem.substitution_id, wem.from_work_order_id "	
sqls += "	from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "
sqls += elig_table + " elig, wo_est_processing_transpose wem, "+elig_table_join+" elig_join "
sqls += "where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls += " 	and nvl(elig_join."+elig_column+",-998833) = elig."+elig_column
sqls +="		and elig.clearing_id = cwcb.clearing_id "
sqls +="		and cwcb.clearing_id = "+string(a_clearing_id)
sqls +="		and wem.budget_id <> "+string(budget_id)
sqls +="		and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls +="		and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls +="		and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls +="		and nvl(nvl(oabb.job_task_id,wem.job_task_id),'-998833') = nvl(wem.job_task_id,'-998833') "
sqls +="		and not exists ( "
sqls +="				select 1 from "+ex_table+" ex"
sqls +="				where ex."+ex_column+" = elig_join."+ex_column+" "
sqls +="					and include_exclude = 0 "
sqls +="					and ex.clearing_id = "+string(a_clearing_id)
sqls +="					) "
sqls +="			union "
sqls +="		select distinct wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id, "
if a_load_by_jt = 1 then
	sqls += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls += " wem.wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' wem.attribute10_id,  '
end if
sqls +="		wem.budget_id,wem.company_id, wem.substitution_id, wem.from_work_order_id	"
sqls +="		from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "
sqls +="		"+elig_table_join+" elig_join, wo_est_processing_transpose wem, "+ex_table+" ex	"	
sqls +="		where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls +="		and ex."+ex_column+" = elig_join."+ex_column+" "
sqls +="		and ex.include_exclude = 1 "
sqls +="		and ex.clearing_id = "+string(a_clearing_id)
sqls +="		and cwcb.clearing_id = "+string(a_clearing_id)
sqls +="		and wem.budget_id <> "+string(budget_id)
sqls +="		and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls +="		and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls +="		and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls +="		and nvl(nvl(oabb.job_task_id,wem.job_task_id),'-998833') = nvl(wem.job_task_id,'-998833') "
sqls +="		and nvl(elig_join."+elig_column+",-993388)  in "
sqls +="		( select "+elig_column +" from "+elig_table_header
sqls +="  			minus "
sqls +="       select "+elig_column
sqls +="		from "+elig_table
sqls +="		where clearing_id = cwcb.clearing_id "
sqls +="				union   "
sqls +="		select -993388 from dual "
sqls +="					) ) a ) "

execute immediate :sqls;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation into wo_est_processing_mo_id. ','E')
	rollback;
	return -1
end if

if sqlca.sqlnrows = 0 then
	uf_msg('There are not eligible charges for this overhead proceeding to next overhead','I')
	return 2
else
	uf_msg('Inserted '+string(sqlca.sqlnrows)+' new rows to process','I')
end if
			
uf_msg('Staging new overhead amounts','I')

sqls2 = " "
sqls2 +=" insert into wo_est_processing_transpose "
sqls2 +=" (id, clearing_id,work_order_id, amount,est_chg_type_id,expenditure_type_id, budget_id,month_number, "
if a_load_by_jt = 1 then
	sqls2 += " job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" long_description, "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 += " wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls2 += ' attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' attribute10_id,  '
end if
sqls2 += " est_monthly_id,new_old,year,company_id,  revision, substitution_id,from_work_order_id) "
sqls2 += " select pwrplant1.nextval, "+string(a_clearing_id)+", x.* from ( "
sqls2 += " select wem.work_order_id, round(sum(decode("+string(dollar_qty_indicator)+",1,wem.amount,2,wem.hours,wem.quantity)*cwrb.calc_rate),2), "
sqls2 += "cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 += " wem.wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,1,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id " //KSY: Change the new_old from 2 to 1
sqls2 += "from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb, "+elig_table_join+" elig_join, "+elig_table+" elig, "
sqls2 +="wo_est_processing_transpose wem, "
sqls2 +="clearing_wo_rate_bdg cwrb, wo_est_processing_mo_id mo_id	"
sqls2 +="where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls2 += "	and mo_id.est_chg_type_id = cwcb.est_chg_type_id "
if clear_by_dept = 1 then
	sqls2 +=" and cwrb.department_id = wem.department_id "
else
	sqls2 +="	and cwrb.company_id = wem.company_id "
end if
sqls2 +="	and mo_id.company_id = wem.company_id "
sqls2 +="	and cwrb.accounting_month = wem.month_number "
sqls2 +="	and mo_id.expenditure_type_id = wem.expenditure_type_id "
if a_load_by_jt = 1 then
	sqls2 +="	and nvl(mo_id.job_task_id,'-99') = nvl(wem.job_task_id,'-99') "
end if
if a_load_by_ua = 1 then
	sqls2 +="	and nvl(mo_id.utility_account_id, -99) = nvl(wem.utility_account_id, -99) "
end if
if a_load_by_dept = 1 then
	sqls2 +="	and nvl(mo_id.department_id, -99) = nvl(wem.department_id, -99) "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 += "	and nvl(mo_id.wo_work_order_id, -99) = nvl(wem.wo_work_order_id, -99) "
end if	
if a_load_by_attr[1] = 1 then
	sqls2 +="	and nvl(mo_id.attribute01_id,'-99') = nvl(wem.attribute01_id,'-99') "
end if
if a_load_by_attr[2] = 1 then
	sqls2 +="	and nvl(mo_id.attribute02_id,'-99') = nvl(wem.attribute02_id,'-99') "
end if
if a_load_by_attr[3] = 1 then
	sqls2 +="	and nvl(mo_id.attribute03_id,'-99') = nvl(wem.attribute03_id,'-99') "
end if
if a_load_by_attr[4] = 1 then
	sqls2 +="	and nvl(mo_id.attribute04_id,'-99') = nvl(wem.attribute04_id,'-99') "
end if
if a_load_by_attr[5] = 1 then
	sqls2 +="	and nvl(mo_id.attribute05_id,'-99') = nvl(wem.attribute05_id,'-99') "
end if
if a_load_by_attr[6] = 1 then
	sqls2 +="	and nvl(mo_id.attribute06_id,'-99') = nvl(wem.attribute06_id,'-99') "
end if
if a_load_by_attr[7] = 1 then
	sqls2 +="	and nvl(mo_id.attribute07_id,'-99') = nvl(wem.attribute07_id,'-99') "
end if
if a_load_by_attr[8] = 1 then
	sqls2 +="	and nvl(mo_id.attribute08_id,'-99') = nvl(wem.attribute08_id,'-99') "
end if
if a_load_by_attr[9] = 1 then
	sqls2 +="	and nvl(mo_id.attribute09_id,'-99') = nvl(wem.attribute09_id,'-99') "
end if
if a_load_by_attr[10] = 1 then
	sqls2 +="	and nvl(mo_id.attribute10_id,'-99') = nvl(wem.attribute10_id,'-99') "
end if
sqls2 +="	and mo_id.budget_id = wem.budget_id "
sqls2 +="	and cwrb.clearing_id = cwcb.clearing_id "
sqls2 +="	and nvl(elig_join."+elig_column+",-998833) = elig."+elig_column
sqls2 +="	and cwcb.clearing_id = elig.clearing_id "
sqls2 +="	and cwcb.clearing_id = "+string(a_clearing_id)
sqls2 +="	and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls2 +="	and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls2 +="	and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls2 +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls2 +="	and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls2 +="	and nvl(nvl(oabb.job_task_id,wem.job_task_id),'-998833') = nvl(wem.job_task_id,'-998833') "
if i_table_name = "BUDGET_MONTHLY_DATA" then
	sqls2 +="	and wem.budget_id = mo_id.budget_id "
else
	sqls2 +="	and wem.work_order_id = mo_id.work_order_id	"
end if
sqls2 +="    and wem.month_number > nvl(wem.actuals_month_number,0) "
sqls2 +="	and wem.budget_id <> "+string(budget_id)
sqls2 +="	and not exists ( "
sqls2 +="				select 1 from "+ex_table+" ex"
sqls2 +="				where ex."+ex_column+" = wem."+ex_column+" "
sqls2 +="					and ex.include_exclude = 0 "
sqls2 +="					and ex.clearing_id = "+string(a_clearing_id)
sqls2 +="					) "
sqls2 += " Group by wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 += " wem.wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id "
sqls2 +=" 	union "
sqls2 += " select wem.work_order_id, round(sum(decode("+string(dollar_qty_indicator)+",1,wem.amount,2,wem.hours,wem.quantity)*cwrb.calc_rate),2), "
sqls2 +=" cwcb.est_chg_type_id,wem.expenditure_type_id, wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 += " wem.wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,1,wem.year,wem.company_id, wem.revision, wem.substitution_id,  wem.from_work_order_id	"	
sqls2 +=" from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "+elig_table_join+" elig_join, wo_est_processing_transpose wem, "
sqls2 +=" clearing_wo_rate_bdg cwrb, wo_est_processing_mo_id mo_id, "+ex_table+" ex "	
sqls2 +=" where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls2 +="	and mo_id.est_chg_type_id = cwcb.est_chg_type_id "
if clear_by_dept = 1 then
	sqls2 +=" and cwrb.department_id = wem.department_id "
else
	sqls2 +="	and cwrb.company_id = wem.company_id "
end if
sqls2 +="    and mo_id.company_id = wem.company_id "
sqls2 +="	and cwrb.accounting_month = wem.month_number "
sqls2  +="	and mo_id.expenditure_type_id = wem.expenditure_type_id "
if a_load_by_jt = 1 then
	sqls2 +="	and nvl(mo_id.job_task_id,'-99') = nvl(wem.job_task_id,'-99') "
end if
if a_load_by_ua = 1 then
	sqls2 +="	and nvl(mo_id.utility_account_id, -99) = nvl(wem.utility_account_id, -99) "
end if
if a_load_by_dept = 1 then
	sqls2 +="	and nvl(mo_id.department_id, -99) = nvl(wem.department_id, -99) "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 += "	and nvl(mo_id.wo_work_order_id, -99) = nvl(wem.wo_work_order_id, -99) "
end if	
if a_load_by_attr[1] = 1 then
	sqls2 +="	and nvl(mo_id.attribute01_id,'-99') = nvl(wem.attribute01_id,'-99') "
end if
if a_load_by_attr[2] = 1 then
	sqls2 +="	and nvl(mo_id.attribute02_id,'-99') = nvl(wem.attribute02_id,'-99') "
end if
if a_load_by_attr[3] = 1 then
	sqls2 +="	and nvl(mo_id.attribute03_id,'-99') = nvl(wem.attribute03_id,'-99') "
end if
if a_load_by_attr[4] = 1 then
	sqls2 +="	and nvl(mo_id.attribute04_id,'-99') = nvl(wem.attribute04_id,'-99') "
end if
if a_load_by_attr[5] = 1 then
	sqls2 +="	and nvl(mo_id.attribute05_id,'-99') = nvl(wem.attribute05_id,'-99') "
end if
if a_load_by_attr[6] = 1 then
	sqls2 +="	and nvl(mo_id.attribute06_id,'-99') = nvl(wem.attribute06_id,'-99') "
end if
if a_load_by_attr[7] = 1 then
	sqls2 +="	and nvl(mo_id.attribute07_id,'-99') = nvl(wem.attribute07_id,'-99') "
end if
if a_load_by_attr[8] = 1 then
	sqls2 +="	and nvl(mo_id.attribute08_id,'-99') = nvl(wem.attribute08_id,'-99') "
end if
if a_load_by_attr[9] = 1 then
	sqls2 +="	and nvl(mo_id.attribute09_id,'-99') = nvl(wem.attribute09_id,'-99') "
end if
if a_load_by_attr[10] = 1 then
	sqls2 +="	and nvl(mo_id.attribute10_id,'-99') = nvl(wem.attribute10_id,'-99') "
end if
sqls2 +="	and mo_id.budget_id = wem.budget_id "
sqls2 +="	and cwrb.clearing_id = cwcb.clearing_id "
sqls2 +="	and cwcb.clearing_id = "+string(a_clearing_id)
sqls2 +="    and ex.clearing_id = "+string(a_clearing_id)
sqls2 +="	and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls2 +="		and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls2 +="		and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls2 +=" 		and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls2 +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls2 +="		and nvl(nvl(oabb.job_task_id,wem.job_task_id),'-998833') = nvl(wem.job_task_id,'-998833') "
sqls2 +="		and wem.budget_id <> "+string(budget_id)
sqls2 +="	and elig_join."+ex_column+" = ex."+ex_column+" "
sqls2 +="	and ex.include_exclude = 1 "
if i_table_name = "BUDGET_MONTHLY_DATA" then
	sqls2 +="	and wem.budget_id = mo_id.budget_id "
else
	sqls2 +="	and wem.work_order_id = mo_id.work_order_id	"
end if
sqls2 +="		and nvl(elig_join."+elig_column+",-993388)  in "
sqls2 +="		( select "+elig_column +" from "+elig_table_header
sqls2 +="  			minus "
sqls2 +="       select "+elig_column
sqls2 +="		from "+elig_table
sqls2 +="		where clearing_id = cwcb.clearing_id "
sqls2 +="				union   "
sqls2 +="		select -993388 from dual "
sqls2 +="					) "
sqls2 +=" and wem.month_number > nvl(wem.actuals_month_number,0) "
sqls2 += " Group by wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 += " wem.wo_work_order_id, "
end if	
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id ) x"
execute immediate :sqls2;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation to wo_est_processing_transpose. ' + sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if

////now lets put the credit back to the clearing work order
///first delete the clearing amount (this is stored in the future year dollars)
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	delete from budget_monthly_data
	where budget_id = :budget_id
		and future_dollars = :a_clearing_id
		and budget_version_id = :i_budget_version
	;
else
	delete from wo_est_monthly
	where work_order_id = :clear_wo_id
		and future_dollars = :a_clearing_id
		and revision = (select revision from wo_est_processing_temp where work_order_id = :clear_wo_id)
	;
end if
if sqlca.sqlcode < 0 then
	uf_msg('Could not delete previous clearing credits  ' + sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if


insert into wo_est_processing_mo_id(work_order_id, expenditure_type_id, est_chg_type_id, job_task_id, budget_id, long_description, company_id, 
	department_id, utility_account_id, est_monthly_id, clearing_id)
select :clear_wo_id, a.*, pwrplant1.nextval, :a_clearing_id
from (
	select distinct expenditure_type_id, est_chg_type_id, null job_task_Id, :budget_id, null long_description, company_id, decode(:clear_by_dept,1,department_id,null) department_id, null utility_account_id
	from wo_est_processing_transpose w 
	where w.clearing_id = :a_clearing_id) a
;
if sqlca.sqlcode < 0 then
	uf_msg('Error inserting into MO id table  ' + sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if


//KSY Clear OH @ BI Level add system control
if i_funding_wo_indicator <> 2 then	
		insert into wo_est_processing_transpose(id, work_order_id, est_monthly_id, month_number, expenditure_type_id, est_chg_type_id, amount, utility_account_id,
			department_id, long_description, revision, 	job_task_id, new_old, year, company_id, budget_id, clearing_id)
		select pwrplant1.nextval, a.*, :a_clearing_id
		from (
			select mo.work_order_id, mo.est_monthly_id, wem.month_number, mo.expenditure_type_id, mo.est_chg_type_id, -1*sum(wem.amount), null utility_account_id, mo.department_id, null long_description, bvfp.revision,
			null job_task_id, 2 new_old, wem.year, mo.company_id, mo.budget_id
			from wo_est_processing_transpose wem, wo_est_processing_mo_id mo, budget_version_fund_proj bvfp
			where wem.company_id = mo.company_id
				and  wem.est_chg_type_id = mo.est_chg_type_id
				and  nvl(wem.department_id,99999999) = nvl(mo.department_id,nvl(wem.department_id,99999999)) //KSY Put NVL around department since its nullable
				and  wem.expenditure_type_id = mo.expenditure_type_id
				and  wem.clearing_id = mo.clearing_id
				and  wem.new_old = 1
				and mo.work_order_id = bvfp.work_order_id
				and bvfp.active = 1
				and bvfp.budget_version_id = :i_budget_version
			group by  mo.work_order_id, mo.est_monthly_id, wem.month_number, mo.expenditure_type_id, mo.est_chg_type_id, mo.department_id,  bvfp.revision, wem.year, mo.company_id, mo.budget_id
			) a
		;
		if sqlca.sqlcode < 0 then
			uf_msg('Error inserting into wo_est_processing_transpose table  ' + sqlca.sqlerrtext,'E')
			rollback;
			return -1
		end if
		
		uf_msg('--Inserted '+string(sqlca.sqlnrows)+' rows ','I')

else 
	//KSY Added offset to clear @ the BI level
		insert into wo_est_processing_transpose(id, work_order_id, est_monthly_id, month_number, expenditure_type_id, est_chg_type_id, amount, utility_account_id,
							 department_id, long_description, revision,         job_task_id, new_old, year, company_id, budget_id, clearing_id)
		select pwrplant1.nextval, a.*, :a_clearing_id
		from (
				 select mo.work_order_id, mo.est_monthly_id, wem.month_number, mo.expenditure_type_id, mo.est_chg_type_id, -1*sum(wem.amount), null utility_account_id, mo.department_id, null long_description, bvfp.budget_version_id,
				 null job_task_id, 2 new_old, wem.year, mo.company_id, mo.budget_id
				 from wo_est_processing_transpose wem, wo_est_processing_mo_id mo, budget_amounts bvfp
				 where wem.company_id = mo.company_id
									  and  wem.est_chg_type_id = mo.est_chg_type_id
									  and  nvl(wem.department_id,99999999) = nvl(mo.department_id,nvl(wem.department_id,99999999)) //KSY Put NVL around department since its nullable
									  and  wem.expenditure_type_id = mo.expenditure_type_id
									  and  wem.clearing_id = mo.clearing_id
									  and  wem.new_old = 1
									  and mo.budget_id = bvfp.budget_id
									  and bvfp.budget_version_id = :i_budget_version
				 group by  mo.work_order_id, mo.est_monthly_id, wem.month_number, mo.expenditure_type_id, mo.est_chg_type_id, mo.department_id,  bvfp.budget_version_id, wem.year, mo.company_id, mo.budget_id
				 ) a
		;
		if sqlca.sqlcode < 0 then
			uf_msg('Error inserting into wo_est_processing_transpose table  ' + sqlca.sqlerrtext,'E')
			rollback;
			return -1
		end if
		
		uf_msg('--Inserted '+string(sqlca.sqlnrows)+' rows ','I')
	
end if



return 1
 
 
 

end function

public function longlong uf_budget_ovhs_loading (longlong a_clearing_id, longlong a_sum_or_org, longlong a_load_by_jt, longlong a_load_by_dept, longlong a_load_by_ua, longlong a_load_by_desc, longlong a_load_by_wo, longlong a_load_by_attr[]);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads_fp.uf_budget_ovhs_loading() 
//
//	  Purpose  :	This function calculates the loading type overheads and stores them in the transpose table
//		             
//
//   Arguments:	 none
//	
//   Returns :   1 if successfuly -1 if failed (SQL Error)
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    		--------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 	

string sqls, sqls2, description, &
	elig_table, elig_column, elig_table_header, elig_table_join, elig_table_join_pk, &
	ex_table, ex_column
longlong clear_by_dept, dollar_qty_indicator

select description, rates_by_department, dollar_quantity_indicator
into :description, :clear_by_dept, :dollar_qty_indicator
from clearing_wo_control_bdg
where clearing_id = :a_clearing_id
;
if sqlca.sqlcode < 0 then
	uf_msg('Could not Select Description for this allocation. ','I')
end if

if isnull(clear_by_dept) then clear_by_dept = 0
if isnull(dollar_qty_indicator) then dollar_qty_indicator = 1




uf_msg('Cleaning out processing table','I')
delete from wo_est_processing_mo_id 
;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation. ','E')
	rollback;
	return -1
end if
uf_msg('--Deleted '+string(sqlca.sqlnrows)+' rows ','I')

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	//	 a_sum_or_org = 1 means that we base off of the budget_summary_id
	if a_sum_or_org = 1 then
		elig_table = 'budget_summary_clear_dflt_bdg'
		elig_column = 'budget_summary_id'
		elig_table_header = 'budget_summary'
	else 
		elig_table = 'budget_org_clear_dflt_bdg'
		elig_column = 'budget_organization_id'
		elig_table_header = 'budget_organization'
	end if
	elig_table_join = 'budget'
	elig_table_join_pk = 'budget_id'
	
	ex_table = 'wo_clear_over_bdg'
	ex_column = 'budget_id'
else
	elig_table = 'wo_type_clear_dflt_bdg'
	elig_column = 'work_order_type_id'
	elig_table_header = 'work_order_type'
	elig_table_join = 'work_order_control'
	elig_table_join_pk = 'work_order_id'
	
	ex_table = 'wo_fp_clear_over_bdg'
	ex_column = 'work_order_id'
end if




///Insert the eligible charges this includes
	///estimate lines in the basis on Budget Items that have a budget sum/budget org in the eligible list
	///estimate lines in the basis on Budget Items that have a budget sum/budget org not in the eligible list but in the include list
///Excludes
	///Estimate lines that are in the basis on budget items that have a budget sum/budget org in the eligible list but in the excluding list
	///Estime lines not in the basis
	///Estimate lines on budget items that have a budget sum/budget org not in the eligible list and not in the include list
uf_msg('Staging new overhead line items','I')
sqls = ""
sqls += "insert into wo_est_processing_mo_id "
sqls += "(work_order_id, est_chg_type_id,expenditure_type_id, "
if a_load_by_jt = 1 then
	sqls += " job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" long_description, " 
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls +=" wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls += ' attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' attribute10_id,  '
end if
sqls += "budget_id,company_id, substitution_id, from_work_order_id, est_monthly_id) "
sqls += "( "
sqls += "	select a.*, pwrplant1.nextval "
sqls += "from "
sqls += "		("
sqls += "			select distinct wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,"
if a_load_by_jt = 1 then
	sqls += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls +=" wem.wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' wem.attribute10_id,  '
end if
sqls += "	wem.budget_id,wem.company_id, wem.substitution_id, wem.from_work_order_id "	
sqls += "	from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "
sqls += elig_table + " elig, wo_est_processing_transpose wem, "+elig_table_join+" elig_join "
sqls += "where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls += " 	and nvl(elig_join."+elig_column+",-998833) = elig."+elig_column
sqls +="		and elig.clearing_id = cwcb.clearing_id "
sqls +="		and cwcb.clearing_id = "+string(a_clearing_id)
sqls +="		and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls +="		and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls +="		and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls +="		and not exists ( "
sqls +="				select 1 from "+ex_table+" ex"
sqls +="				where ex."+ex_column+" = elig_join."+ex_column+" "
sqls +="					and ex.include_exclude = 0 "
sqls +="					and ex.clearing_id = "+string(a_clearing_id)
sqls +="					) "
sqls +="			union "
sqls +="		select distinct wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id, "
if a_load_by_jt = 1 then
	sqls += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls +=" wem.wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' wem.attribute10_id,  '
end if
sqls +="		wem.budget_id,wem.company_id, wem.substitution_id, wem.from_work_order_id	"
sqls +="		from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "
sqls +="		"+elig_table_join+" elig_join, wo_est_processing_transpose wem, "+ex_table+" ex	"	
sqls +="		where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls +="		and ex."+ex_column+" = elig_join."+ex_column+" "
sqls +="		and ex.include_exclude = 1 "
sqls +="		and ex.clearing_id = "+string(a_clearing_id)
sqls +="		and cwcb.clearing_id = "+string(a_clearing_id)
sqls +="		and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls +="		and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls +="		and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls +=" 		and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls +="		and nvl(elig_join."+elig_column+",-993388)  in "
sqls +="		( select "+elig_column +" from "+elig_table_header
sqls +="  			minus "
sqls +="       select "+elig_column
sqls +="		from "+elig_table
sqls +="		where clearing_id = cwcb.clearing_id "
sqls +="				union   "
sqls +="		select -993388 from dual "
sqls +="					) ) a ) "


execute immediate :sqls;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation into wo_est_processing_mo_id. ','E')
	rollback;
	return -1
end if

if sqlca.sqlnrows = 0 then
	uf_msg('There are not eligible charges for this overhead proceeding to next overhead','I')
	return 2
else
	uf_msg('Inserted '+string(sqlca.sqlnrows)+' new rows to process','I')
end if
			
uf_msg('Staging new overhead amounts','I')

sqls2 = " "
sqls2 +=" insert into wo_est_processing_transpose "
sqls2 +=" (id,  clearing_id, work_order_id, amount,est_chg_type_id,expenditure_type_id, budget_id,month_number, "
if a_load_by_jt = 1 then
	sqls2 += " job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" long_description, "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' attribute10_id,  '
end if
sqls2 += " est_monthly_id,new_old,year,company_id,  revision, substitution_id,from_work_order_id) "
sqls2 += " select pwrplant1.nextval, "+string(a_clearing_id)+", x.* from ( "
sqls2 += " select wem.work_order_id, round(sum(decode("+string(dollar_qty_indicator)+",1,wem.amount,2,wem.hours,wem.quantity)*cwrb.input_rate),2), "
sqls2 += "cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wem.wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,1,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id "
sqls2 += "from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb, "+elig_table_join+" elig_join, "+elig_table+" elig, "
sqls2 +="wo_est_processing_transpose wem, "
sqls2 +="clearing_wo_rate_bdg cwrb, wo_est_processing_mo_id mo_id	"
sqls2 +="where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls2 += "	and mo_id.est_chg_type_id = cwcb.est_chg_type_id "
if clear_by_dept = 1 then
	sqls2 +=" and cwrb.department_id = wem.department_id "
end if
sqls2 +="	and cwrb.company_id = wem.company_id "
sqls2 +="	and mo_id.company_id = wem.company_id "
sqls2 +="	and cwrb.accounting_month = wem.month_number "
sqls2 +="	and mo_id.expenditure_type_id = wem.expenditure_type_id "
if a_load_by_jt = 1 then
	sqls2 +="	and nvl(mo_id.job_task_id,'-99') = nvl(wem.job_task_id,'-99') "
end if
if a_load_by_ua = 1 then
	sqls2 +="	and nvl(mo_id.utility_account_id, -99) = nvl(wem.utility_account_id, -99) "
end if
if a_load_by_dept = 1 then
	sqls2 +="	and nvl(mo_id.department_id, -99) = nvl(wem.department_id, -99) "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +="	and nvl(mo_id.wo_work_order_id, -99) = nvl(wem.wo_work_order_id, -99) "
end if
if a_load_by_attr[1] = 1 then
	sqls2 +="	and nvl(mo_id.attribute01_id,'-99') = nvl(wem.attribute01_id,'-99') "
end if
if a_load_by_attr[2] = 1 then
	sqls2 +="	and nvl(mo_id.attribute02_id,'-99') = nvl(wem.attribute02_id,'-99') "
end if
if a_load_by_attr[3] = 1 then
	sqls2 +="	and nvl(mo_id.attribute03_id,'-99') = nvl(wem.attribute03_id,'-99') "
end if
if a_load_by_attr[4] = 1 then
	sqls2 +="	and nvl(mo_id.attribute04_id,'-99') = nvl(wem.attribute04_id,'-99') "
end if
if a_load_by_attr[5] = 1 then
	sqls2 +="	and nvl(mo_id.attribute05_id,'-99') = nvl(wem.attribute05_id,'-99') "
end if
if a_load_by_attr[6] = 1 then
	sqls2 +="	and nvl(mo_id.attribute06_id,'-99') = nvl(wem.attribute06_id,'-99') "
end if
if a_load_by_attr[7] = 1 then
	sqls2 +="	and nvl(mo_id.attribute07_id,'-99') = nvl(wem.attribute07_id,'-99') "
end if
if a_load_by_attr[8] = 1 then
	sqls2 +="	and nvl(mo_id.attribute08_id,'-99') = nvl(wem.attribute08_id,'-99') "
end if
if a_load_by_attr[9] = 1 then
	sqls2 +="	and nvl(mo_id.attribute09_id,'-99') = nvl(wem.attribute09_id,'-99') "
end if
if a_load_by_attr[10] = 1 then
	sqls2 +="	and nvl(mo_id.attribute10_id,'-99') = nvl(wem.attribute10_id,'-99') "
end if
sqls2 +="	and mo_id.budget_id = wem.budget_id "
sqls2 +="	and cwrb.clearing_id = cwcb.clearing_id "
sqls2 +="	and nvl(elig_join."+elig_column+",-998833) = elig."+elig_column
sqls2 +="	and cwcb.clearing_id = elig.clearing_id "
sqls2 +="	and cwcb.clearing_id = "+string(a_clearing_id)
sqls2 +="	and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls2 +="	and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls2 +="	and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls2 +=" 		and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls2 +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
// ### CDM - Maint #### - Add "nvl" around work_order_id for budget item estimates
sqls2 +="	and nvl(wem.work_order_id,-99) = nvl(mo_id.work_order_id,-99)	"
sqls2 +="    and wem.month_number > nvl(wem.actuals_month_number,0) "
sqls2 +="		and not exists ( "
sqls2 +="				select 1 from "+ex_table+" ex"
sqls2 +="				where ex."+ex_column+" = wem."+ex_column+" "
sqls2 +="					and ex.include_exclude = 0 "
sqls2 +="					and ex.clearing_id = "+string(a_clearing_id)
sqls2 +="					) "
sqls2 += " Group by wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW JIRA PC-1084
	sqls2 +=" wem.wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id "
sqls2 +=" 	union "
sqls2 += " select wem.work_order_id, round(sum(decode("+string(dollar_qty_indicator)+",1,wem.amount,2,wem.hours,wem.quantity)*cwrb.input_rate),2), "
sqls2 +=" cwcb.est_chg_type_id,wem.expenditure_type_id, wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wem.wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,1,wem.year,wem.company_id, wem.revision, wem.substitution_id,  wem.from_work_order_id	"	
sqls2 +=" from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "+elig_table_join+" elig_join,  wo_est_processing_transpose wem, "
sqls2 +=" clearing_wo_rate_bdg cwrb, wo_est_processing_mo_id mo_id, "+ex_table+" ex "	
sqls2 +=" where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls2 +="	and mo_id.est_chg_type_id = cwcb.est_chg_type_id "
if clear_by_dept = 1 then
	sqls2 +=" and cwrb.department_id = wem.department_id "
end if
sqls2 +="	and cwrb.company_id = wem.company_id "
sqls2 +="    and mo_id.company_id = wem.company_id "
sqls2 +="	and cwrb.accounting_month = wem.month_number "
sqls2  +="	and mo_id.expenditure_type_id = wem.expenditure_type_id "
if a_load_by_jt = 1 then
	sqls2 +="	and nvl(mo_id.job_task_id,'-99') = nvl(wem.job_task_id,'-99') "
end if
if a_load_by_ua = 1 then
	sqls2 +="	and nvl(mo_id.utility_account_id, -99) = nvl(wem.utility_account_id, -99) "
end if
if a_load_by_dept = 1 then
	sqls2 +="	and nvl(mo_id.department_id, -99) = nvl(wem.department_id, -99) "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +="	and nvl(mo_id.wo_work_order_id, -99) = nvl(wem.wo_work_order_id, -99) "
end if
if a_load_by_attr[1] = 1 then
	sqls2 +="	and nvl(mo_id.attribute01_id,'-99') = nvl(wem.attribute01_id,'-99') "
end if
if a_load_by_attr[2] = 1 then
	sqls2 +="	and nvl(mo_id.attribute02_id,'-99') = nvl(wem.attribute02_id,'-99') "
end if
if a_load_by_attr[3] = 1 then
	sqls2 +="	and nvl(mo_id.attribute03_id,'-99') = nvl(wem.attribute03_id,'-99') "
end if
if a_load_by_attr[4] = 1 then
	sqls2 +="	and nvl(mo_id.attribute04_id,'-99') = nvl(wem.attribute04_id,'-99') "
end if
if a_load_by_attr[5] = 1 then
	sqls2 +="	and nvl(mo_id.attribute05_id,'-99') = nvl(wem.attribute05_id,'-99') "
end if
if a_load_by_attr[6] = 1 then
	sqls2 +="	and nvl(mo_id.attribute06_id,'-99') = nvl(wem.attribute06_id,'-99') "
end if
if a_load_by_attr[7] = 1 then
	sqls2 +="	and nvl(mo_id.attribute07_id,'-99') = nvl(wem.attribute07_id,'-99') "
end if
if a_load_by_attr[8] = 1 then
	sqls2 +="	and nvl(mo_id.attribute08_id,'-99') = nvl(wem.attribute08_id,'-99') "
end if
if a_load_by_attr[9] = 1 then
	sqls2 +="	and nvl(mo_id.attribute09_id,'-99') = nvl(wem.attribute09_id,'-99') "
end if
if a_load_by_attr[10] = 1 then
	sqls2 +="	and nvl(mo_id.attribute10_id,'-99') = nvl(wem.attribute10_id,'-99') "
end if
sqls2 +="	and mo_id.budget_id = wem.budget_id "
sqls2 +="	and cwrb.clearing_id = cwcb.clearing_id "
sqls2 +="	and cwcb.clearing_id = "+string(a_clearing_id)
sqls2 +="   and ex.clearing_id = "+string(a_clearing_id)
sqls2 +="	and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls2 +="	and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls2 +="	and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls2 +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls2 +="	and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls2 +="	and elig_join."+ex_column+" = ex."+ex_column+" "
sqls2 +="	and ex.include_exclude = 1 "
sqls2 +="	and nvl(wem.work_order_id,-99) = nvl(mo_id.work_order_id,-99)	"
sqls2 +="		and nvl(elig_join."+elig_column+",-993388)  in "
sqls2 +="		( select "+elig_column +" from "+elig_table_header
sqls2 +="  			minus "
sqls2 +="       select "+elig_column
sqls2 +="		from "+elig_table
sqls2 +="		where clearing_id = cwcb.clearing_id "
sqls2 +="				union   "
sqls2 +="		select -993388 from dual "
sqls2 +="					) "
sqls2 +=" and wem.month_number > nvl(wem.actuals_month_number,0) "
sqls2 += " Group by wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wem.wo_work_order_id, " 
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id ) x"
execute immediate :sqls2;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation to wo_est_processing_transpose. ' + sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if

uf_msg('--Inserted '+string(sqlca.sqlnrows)+' rows ','I')

return 1
 
 
 

end function

public function longlong uf_budget_ovhs_reimb (longlong a_clearing_id, longlong a_sum_or_org, longlong a_load_by_jt, longlong a_load_by_dept, longlong a_load_by_ua, longlong a_load_by_desc, longlong a_load_by_wo, longlong a_load_by_attr[]);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads_fp.uf_budget_ovhs_reimb() 
//
//	  Purpose  :	This function calculates the reimb type overheads and stores them in the transpose table
//		             
//
//   Arguments:	 none
//	
//   Returns :   1 if successfuly -1 if failed (SQL Error)
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    		--------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 	

string sqls, sqls2, description, &
	elig_table, elig_column, elig_table_header, elig_table_join, elig_table_join_pk, &
	ex_table, ex_column

select description 
into :description
from clearing_wo_control_bdg
where clearing_id = :a_clearing_id
;
if sqlca.sqlcode < 0 then
	uf_msg('Could not Select Description for this allocation. ','I')
end if

uf_msg('Cleaning out processing table','I')
delete from wo_est_processing_mo_id 
;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation. ','E')
	rollback;
	return -1
end if
uf_msg('--Deleted '+string(sqlca.sqlnrows)+' rows ','I')

if i_table_name = 'BUDGET_MONTHLY_DATA' then
	//	 a_sum_or_org = 1 means that we base off of the budget_summary_id
	if a_sum_or_org = 1 then
		elig_table = 'budget_summary_clear_dflt_bdg'
		elig_column = 'budget_summary_id'
		elig_table_header = 'budget_summary'
	else 
		elig_table = 'budget_org_clear_dflt_bdg'
		elig_column = 'budget_organization_id'
		elig_table_header = 'budget_organization'
	end if
	elig_table_join = 'budget'
	elig_table_join_pk = 'budget_id'
	
	ex_table = 'wo_clear_over_bdg'
	ex_column = 'budget_id'
else
	elig_table = 'wo_type_clear_dflt_bdg'
	elig_column = 'work_order_type_id'
	elig_table_header = 'work_order_type'
	elig_table_join = 'work_order_control'
	elig_table_join_pk = 'work_order_id'
	
	ex_table = 'wo_fp_clear_over_bdg'
	ex_column = 'work_order_id'
end if




///Insert the eligible charges this includes
	///estimate lines in the basis on Budget Items that have a budget sum/budget org in the eligible list
	///estimate lines in the basis on Budget Items that have a budget sum/budget org not in the eligible list but in the include list
///Excludes
	///Estimate lines that are in the basis on budget items that have a budget sum/budget org in the eligible list but in the excluding list
	///Estime lines not in the basis
	///Estimate lines on budget items that have a budget sum/budget org not in the eligible list and not in the include list
uf_msg('Staging new overhead line items','I')
sqls = ""
sqls += "insert into wo_est_processing_mo_id "
sqls += "(work_order_id, est_chg_type_id,expenditure_type_id, "
if a_load_by_jt = 1 then
	sqls += " job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" long_description, "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls +=" wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls += ' attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' attribute10_id,  '
end if
sqls += "budget_id,company_id, substitution_id, from_work_order_id, est_monthly_id) "
sqls += "( "
sqls += "	select a.*, pwrplant1.nextval "
sqls += "from "
sqls += "		("
sqls += "			select distinct wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,"
if a_load_by_jt = 1 then
	sqls += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls +=" wem.wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' wem.attribute10_id,  '
end if
sqls += "	wem.budget_id,wem.company_id, wem.substitution_id, wem.from_work_order_id "	
sqls += "	from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "
sqls += elig_table + " elig, wo_est_processing_transpose wem, "+elig_table_join+" elig_join "
sqls += "where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls += " 	and nvl(elig_join."+elig_column+",-998833) = elig."+elig_column
sqls +="		and elig.clearing_id = cwcb.clearing_id "
sqls +="		and cwcb.clearing_id = "+string(a_clearing_id)
sqls +="		and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls +="		and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls +="		and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls +="		and not exists ( "
sqls +="				select 1 from "+ex_table+" ex"
sqls +="				where ex."+ex_column+" = elig_join."+ex_column+" "
sqls +="					and ex.include_exclude = 0 "
sqls +="					and ex.clearing_id = "+string(a_clearing_id)
sqls +="					) "
sqls +="			union "
sqls +="		select distinct wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id, "
if a_load_by_jt = 1 then
	sqls += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls +=" wem.wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls += ' wem.attribute10_id,  '
end if
sqls +="		wem.budget_id,wem.company_id, wem.substitution_id, wem.from_work_order_id	"
sqls +="		from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "
sqls +="		"+elig_table_join+" elig_join, wo_est_processing_transpose wem, "+ex_table+" ex	"	
sqls +="		where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls +="		and ex."+ex_column+" = elig_join."+ex_column+" "
sqls +="		and ex.include_exclude = 1 "
sqls +="		and cwcb.clearing_id = "+string(a_clearing_id)
sqls +="		and ex.clearing_id = "+string(a_clearing_id)
sqls +="		and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls +="		and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls +="		and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls +="		and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls +="		and nvl(elig_join."+elig_column+",-993388)  in "
sqls +="		( select "+elig_column +" from "+elig_table_header
sqls +="  			minus "
sqls +="       select "+elig_column
sqls +="		from "+elig_table
sqls +="		where clearing_id = cwcb.clearing_id "
sqls +="				union   "
sqls +="		select -993388 from dual "
sqls +="					) ) a ) "


execute immediate :sqls;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation into wo_est_processing_mo_id. ','E')
	rollback;
	return -1
end if

if sqlca.sqlnrows = 0 then
	uf_msg('There are not eligible charges for this overhead proceeding to next overhead','I')
	return 2
else
	uf_msg('Inserted '+string(sqlca.sqlnrows)+' new rows to process','I')
end if
			
uf_msg('Staging new overhead amounts','I')

sqls2 = " "
sqls2 +=" insert into wo_est_processing_transpose "
sqls2 +=" (id, clearing_id, work_order_id, amount,est_chg_type_id,expenditure_type_id, budget_id,month_number, "
if a_load_by_jt = 1 then
	sqls2 += " job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" long_description, "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' attribute10_id,  '
end if
sqls2 += " est_monthly_id,new_old,year,company_id,  revision, substitution_id,from_work_order_id) "
sqls2 += " select pwrplant1.nextval, "+string(a_clearing_id)+", x.* from ( "
sqls2 += " select wem.work_order_id, round(sum(wem.amount*brr.percent_reim*-1),2), "
sqls2 += "cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wem.wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,1,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id "
sqls2 += "from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb, "+elig_table_join+" elig_join, "+elig_table+" elig, "
sqls2 +="wo_est_processing_transpose wem, "
sqls2 +="budget_reimb_rates brr, wo_est_processing_mo_id mo_id "
if i_funding_wo_indicator <> 2 then
	sqls2 +=", work_order_account woa "
end if
sqls2 +="where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
sqls2 += "	and mo_id.est_chg_type_id = cwcb.est_chg_type_id "
sqls2 +="	and mo_id.company_id = wem.company_id "
if i_funding_wo_indicator = 2 then
	sqls2 +="   and brr.reimbursable_type_id = elig_join.reimbursable_type_id "
else
	sqls2 +="	and woa.work_order_id (+) = mo_id.work_order_id "
	sqls2 +="   and brr.reimbursable_type_id = woa.reimbursable_type_id "
end if
sqls2 +="   and brr.est_chg_type_id = cwcb.est_chg_type_id "
sqls2 +="	and mo_id.expenditure_type_id = wem.expenditure_type_id "
if a_load_by_jt = 1 then
	sqls2 +="	and nvl(mo_id.job_task_id,'-99') = nvl(wem.job_task_id,'-99') "
end if
if a_load_by_ua = 1 then
	sqls2 +="	and nvl(mo_id.utility_account_id, -99) = nvl(wem.utility_account_id, -99) "
end if
if a_load_by_dept = 1 then
	sqls2 +="	and nvl(mo_id.department_id, -99) = nvl(wem.department_id, -99) "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +="	and nvl(mo_id.wo_work_order_id, -99) = nvl(wem.wo_work_order_id, -99) "
end if
if a_load_by_attr[1] = 1 then
	sqls2 +="	and nvl(mo_id.attribute01_id,'-99') = nvl(wem.attribute01_id,'-99') "
end if
if a_load_by_attr[2] = 1 then
	sqls2 +="	and nvl(mo_id.attribute02_id,'-99') = nvl(wem.attribute02_id,'-99') "
end if
if a_load_by_attr[3] = 1 then
	sqls2 +="	and nvl(mo_id.attribute03_id,'-99') = nvl(wem.attribute03_id,'-99') "
end if
if a_load_by_attr[4] = 1 then
	sqls2 +="	and nvl(mo_id.attribute04_id,'-99') = nvl(wem.attribute04_id,'-99') "
end if
if a_load_by_attr[5] = 1 then
	sqls2 +="	and nvl(mo_id.attribute05_id,'-99') = nvl(wem.attribute05_id,'-99') "
end if
if a_load_by_attr[6] = 1 then
	sqls2 +="	and nvl(mo_id.attribute06_id,'-99') = nvl(wem.attribute06_id,'-99') "
end if
if a_load_by_attr[7] = 1 then
	sqls2 +="	and nvl(mo_id.attribute07_id,'-99') = nvl(wem.attribute07_id,'-99') "
end if
if a_load_by_attr[8] = 1 then
	sqls2 +="	and nvl(mo_id.attribute08_id,'-99') = nvl(wem.attribute08_id,'-99') "
end if
if a_load_by_attr[9] = 1 then
	sqls2 +="	and nvl(mo_id.attribute09_id,'-99') = nvl(wem.attribute09_id,'-99') "
end if
if a_load_by_attr[10] = 1 then
	sqls2 +="	and nvl(mo_id.attribute10_id,'-99') = nvl(wem.attribute10_id,'-99') "
end if
sqls2 +="	and mo_id.budget_id = wem.budget_id "
sqls2 +="	and nvl(elig_join."+elig_column+",-998833) = elig."+elig_column
sqls2 +="	and cwcb.clearing_id = elig.clearing_id "
sqls2 +="	and cwcb.clearing_id = "+string(a_clearing_id)
sqls2 +="	and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls2 +="	and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls2 +="	and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls2 +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls2 +="	and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls2 +="	and nvl(wem.work_order_id,-99) = nvl(mo_id.work_order_id,-99)	"
sqls2 +="   and wem.month_number > nvl(wem.actuals_month_number,0) "
sqls2 +="	and not exists ( "
sqls2 +="				select 1 from "+ex_table+" ex"
sqls2 +="				where ex."+ex_column+" = wem."+ex_column+" "
sqls2 +="					and ex.include_exclude = 0 "
sqls2 +="					and ex.clearing_id = "+string(a_clearing_id)
sqls2 +="					) "
sqls2 += " Group by wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wem.wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id "
sqls2 +=" 	union "
sqls2 += " select wem.work_order_id, round(sum(wem.amount*brr.percent_reim*-1),2), "
sqls2 +=" cwcb.est_chg_type_id,wem.expenditure_type_id, wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wem.wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,1,wem.year,wem.company_id, wem.revision, wem.substitution_id,  wem.from_work_order_id	"	
sqls2 +=" from clearing_wo_control_bdg cwcb, oh_alloc_basis_bdg oabb,  "+elig_table_join+" elig_join,  wo_est_processing_transpose wem, "
sqls2 +=" budget_reimb_rates brr, wo_est_processing_mo_id mo_id, "+ex_table+" ex "
if i_funding_wo_indicator <> 2 then
	sqls2 +=", work_order_account woa "
end if	
sqls2 +=" where elig_join."+elig_table_join_pk+" = wem."+elig_table_join_pk+" "
if i_funding_wo_indicator = 2 then
	sqls2 +="   and brr.reimbursable_type_id = elig_join.reimbursable_type_id "
else
	sqls2 +="	and woa.work_order_id (+) = mo_id.work_order_id "
	sqls2 +="   and brr.reimbursable_type_id = woa.reimbursable_type_id "
end if
sqls2 +="    and brr.est_chg_type_id = cwcb.est_chg_type_id "
sqls2 +="	and mo_id.est_chg_type_id = cwcb.est_chg_type_id "
sqls2 +="    and mo_id.company_id = wem.company_id "
sqls2  +="	and mo_id.expenditure_type_id = wem.expenditure_type_id "
if a_load_by_jt = 1 then
	sqls2 +="	and nvl(mo_id.job_task_id,'-99') = nvl(wem.job_task_id,'-99') "
end if
if a_load_by_ua = 1 then
	sqls2 +="	and nvl(mo_id.utility_account_id, -99) = nvl(wem.utility_account_id, -99) "
end if
if a_load_by_dept = 1 then
	sqls2 +="	and nvl(mo_id.department_id, -99) = nvl(wem.department_id, -99) "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +="	and nvl(mo_id.wo_work_order_id, -99) = nvl(wem.wo_work_order_id, -99) "
end if
if a_load_by_attr[1] = 1 then
	sqls2 +="	and nvl(mo_id.attribute01_id,'-99') = nvl(wem.attribute01_id,'-99') "
end if
if a_load_by_attr[2] = 1 then
	sqls2 +="	and nvl(mo_id.attribute02_id,'-99') = nvl(wem.attribute02_id,'-99') "
end if
if a_load_by_attr[3] = 1 then
	sqls2 +="	and nvl(mo_id.attribute03_id,'-99') = nvl(wem.attribute03_id,'-99') "
end if
if a_load_by_attr[4] = 1 then
	sqls2 +="	and nvl(mo_id.attribute04_id,'-99') = nvl(wem.attribute04_id,'-99') "
end if
if a_load_by_attr[5] = 1 then
	sqls2 +="	and nvl(mo_id.attribute05_id,'-99') = nvl(wem.attribute05_id,'-99') "
end if
if a_load_by_attr[6] = 1 then
	sqls2 +="	and nvl(mo_id.attribute06_id,'-99') = nvl(wem.attribute06_id,'-99') "
end if
if a_load_by_attr[7] = 1 then
	sqls2 +="	and nvl(mo_id.attribute07_id,'-99') = nvl(wem.attribute07_id,'-99') "
end if
if a_load_by_attr[8] = 1 then
	sqls2 +="	and nvl(mo_id.attribute08_id,'-99') = nvl(wem.attribute08_id,'-99') "
end if
if a_load_by_attr[9] = 1 then
	sqls2 +="	and nvl(mo_id.attribute09_id,'-99') = nvl(wem.attribute09_id,'-99') "
end if
if a_load_by_attr[10] = 1 then
	sqls2 +="	and nvl(mo_id.attribute10_id,'-99') = nvl(wem.attribute10_id,'-99') "
end if
sqls2 +="	and mo_id.budget_id = wem.budget_id "
sqls2 +="	and cwcb.clearing_id = "+string(a_clearing_id)
sqls2 +="   and ex.clearing_id = "+string(a_clearing_id)
sqls2 +="	and cwcb.overhead_basis_id = oabb.overhead_basis_id "
sqls2 +="	and oabb.est_chg_type_id = wem.est_chg_type_id "
sqls2 +="	and oabb.expenditure_type_id = wem.expenditure_type_id "
sqls2 +=" 	and nvl(nvl(oabb.utility_account_id,wem.utility_account_id),-998833) = nvl(wem.utility_account_id,-998833) "
sqls2 +="	and nvl(nvl(oabb.department_id,wem.department_id),-998833) = nvl(wem.department_id,-998833) "
sqls2 +="	and elig_join."+ex_column+" = ex."+ex_column+" "
sqls2 +="	and ex.include_exclude = 1 "
sqls2 +="	and wem.work_order_id = mo_id.work_order_id "
sqls2 +="		and nvl(elig_join."+elig_column+",-993388)  in "
sqls2 +="		( select "+elig_column +" from "+elig_table_header
sqls2 +="  			minus "
sqls2 +="       select "+elig_column
sqls2 +="		from "+elig_table
sqls2 +="		where clearing_id = cwcb.clearing_id "
sqls2 +="				union   "
sqls2 +="		select -993388 from dual "
sqls2 +="					) "
sqls2 +=" and wem.month_number > nvl(wem.actuals_month_number,0) "
sqls2 += " Group by wem.work_order_id, cwcb.est_chg_type_id,wem.expenditure_type_id,wem.budget_id,wem.month_number, "
if a_load_by_jt = 1 then
	sqls2 += " wem.job_task_id, "
end if
if a_load_by_dept = 1 then
	sqls2 += " wem.department_id, "
end if
if a_load_by_ua = 1 then
	sqls2 +=" wem.utility_account_id, "
end if
if a_load_by_desc = 1 then
	sqls2 +=" '"+description+"', "
end if
if a_load_by_wo = 1 then//AWW PC-1084
	sqls2 +=" wem.wo_work_order_id, "
end if
if a_load_by_attr[1] = 1 then
	sqls2 += ' wem.attribute01_id,  '
end if
if a_load_by_attr[2] = 1 then
	sqls2 += ' wem.attribute02_id,  '
end if
if a_load_by_attr[3] = 1 then
	sqls2 += ' wem.attribute03_id,  '
end if
if a_load_by_attr[4] = 1 then
	sqls2 += ' wem.attribute04_id,  '
end if
if a_load_by_attr[5] = 1 then
	sqls2 += ' wem.attribute05_id,  '
end if
if a_load_by_attr[6] = 1 then
	sqls2 += ' wem.attribute06_id,  '
end if
if a_load_by_attr[7] = 1 then
	sqls2 += ' wem.attribute07_id,  '
end if
if a_load_by_attr[8] = 1 then
	sqls2 += ' wem.attribute08_id,  '
end if
if a_load_by_attr[9] = 1 then
	sqls2 += ' wem.attribute09_id,  '
end if
if a_load_by_attr[10] = 1 then
	sqls2 += ' wem.attribute10_id,  '
end if
sqls2 +=" mo_id.est_monthly_id,wem.year,wem.company_id, wem.revision, wem.substitution_id, wem.from_work_order_id ) x"
execute immediate :sqls2;
if sqlca.sqlcode < 0 then
	uf_msg('Could not insert new overhead amounts for allocation to wo_est_processing_transpose. ' + sqlca.sqlerrtext,'E')
	rollback;
	return -1
end if

uf_msg('--Inserted '+string(sqlca.sqlnrows)+' rows ','I')

return 1
end function

on uo_budget_overheads.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_budget_overheads.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

