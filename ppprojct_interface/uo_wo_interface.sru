HA$PBExportHeader$uo_wo_interface.sru
$PBExportComments$8176-fix budget item class code insert
forward
global type uo_wo_interface from nonvisualobject
end type
end forward

global type uo_wo_interface from nonvisualobject
end type
global uo_wo_interface uo_wo_interface

type variables
longlong i_month_numbers[]
longlong i_month_periods[]
longlong i_bv_id
boolean i_pp_msgs = true
boolean i_status_box = false
boolean i_visual = false
boolean no_updates = false
longlong i_max_cc = 30
end variables

forward prototypes
public subroutine uf_msgs (string a_msg)
public function integer uf_print_dw (datastore dw_1)
public function integer uf_create_cc ()
public subroutine wf_ddl ()
public function integer uf_create_fps (longlong a_company, string a_batch)
public function longlong uf_check_sql ()
public function integer uf_update_fps (longlong a_company, string a_batch)
public function integer uf_create_wos (longlong a_company, string a_batch)
public function integer uf_update_wos (longlong a_company, string a_batch)
public function integer uf_load_unit_est (longlong a_company, boolean a_replace, boolean a_process_ocr, string a_batch)
public function integer uf_create_fps (longlong a_company)
public function integer uf_create_wos (longlong a_company)
public function integer uf_create_wos_fps (longlong a_company, string a_batch, longlong a_funding_wo_ind)
public function integer uf_update_wos_fps (longlong a_company, string a_batch, longlong a_funding_wo_ind)
public function integer uf_create_fps (string a_batch)
public function integer uf_create_wos (string a_batch)
public function integer uf_load_monthly_est (longlong a_company, boolean a_replace, string a_batch, boolean a_replace_year, longlong a_funding_wo_indicator)
end prototypes

public subroutine uf_msgs (string a_msg);
if i_pp_msgs then
	f_pp_msgs(string(now()) + ": " + a_msg)
end if

if i_status_box and i_visual then
	f_status_box('',a_msg)
end if

end subroutine

public function integer uf_print_dw (datastore dw_1);longlong i, j

if dw_1.rowcount() > 0 then return 1

for i = 1 to dw_1.rowcount()
	for j = 1 to dw_1.Object.DataWindow.Column.Count
		choose case lower(left(dw_1.Describe("#" + string(j) + ".Type"),3))
			case 'dec'
				uf_msgs('Row ' + string(i) + ' Column ' + string(j) + ' ' + string(dw_1.getitemnumber(i,j)))
			case 'cha' 
				uf_msgs('Row ' + string(i) + ' Column ' + string(j) + ' ' + dw_1.getitemstring(i,j))
			case 'dat'
				uf_msgs('Row ' + string(i) + ' Column ' + string(j) + ' ' + string(dw_1.getitemdatetime(i,j)))
		end choose
	next
next

return 1
end function

public function integer uf_create_cc ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_wo_interface.uf_create_fps
//
//   Purpose  :   Creates Funding Projects from pre-staged data in the table wo_interface_staging.
//					Assumes that the Funding Project Number, Work_order_type_id and desciption are not null in the wo_interface_staging table.
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
longlong max_id

select max(class_code_id)
into :max_id
from class_code;

uf_msgs( " Insert into Class Code " )
insert into class_code (
	CLASS_CODE_ID, DESCRIPTION, CWIP_INDICATOR, TAX_INDICATOR, CPR_INDICATOR, BUDGET_INDICATOR,
	LONG_DESCRIPTION, CLASS_CODE_TYPE, CLASS_CODE_CATEGORY, REIMB_INDICATOR, REQUIRED,
	FP_INDICATOR, INHERIT_FROM_FP, IGNORE_FOR_MASS_ADD, JOB_TASK_INDICATOR
	)
select :max_id + rownum, substr(description,1,35), 1, 0, 0, 1,
	description, 'standard', null, null, 1,
	funding_wo_indicator, 0 ,0 , 0
from 
(select distinct trim(class_code1) description, funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code2), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code3), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code4), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code5), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code6), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code7), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code8), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code9), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code10), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code11), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code12), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code13), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code14), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code15), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code16), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code17), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code18), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code19), funding_wo_indicator
from wo_interface_staging
union
select distinct trim(class_code20), funding_wo_indicator
from wo_interface_staging
minus
select trim(a.description), b.yes_no_id
from 	 class_code a, yes_no b
minus
select null, b.yes_no_id
from dual, yes_no b);
if sqlca.sqlcode < 0 then 
	uf_msgs("SQLERROR: " +    sqlca.sqlerrtext)
	
	return -1
end if
uf_msgs("Rows Effected: " + string(sqlca.sqlnrows))

uf_msgs( " Insert into Class Code Values " )
insert into class_code_values (class_code_id, value)
(select distinct cc.class_code_id, w.class_value1
from class_code cc, wo_interface_staging w
where trim(w.class_code1) = trim(cc.description)
and w.class_value1 is not null
union
select distinct cc.class_code_id, w.class_value2
from class_code cc, wo_interface_staging w
where trim(w.class_code2) = trim(cc.description)
and w.class_value2 is not null
union
select distinct cc.class_code_id, w.class_value3
from class_code cc, wo_interface_staging w
where trim(w.class_code3) = trim(cc.description)
and w.class_value3 is not null
union
select distinct cc.class_code_id, w.class_value4
from class_code cc, wo_interface_staging w
where trim(w.class_code4) = trim(cc.description)
and w.class_value4 is not null
union
select distinct cc.class_code_id, w.class_value5
from class_code cc, wo_interface_staging w
where trim(w.class_code5) = trim(cc.description)
and w.class_value5 is not null
union
select distinct cc.class_code_id, w.class_value6
from class_code cc, wo_interface_staging w
where trim(w.class_code6) = trim(cc.description)
and w.class_value6 is not null
union
select distinct cc.class_code_id, w.class_value7
from class_code cc, wo_interface_staging w
where trim(w.class_code7) = trim(cc.description)
and w.class_value7 is not null
union
select distinct cc.class_code_id, w.class_value8
from class_code cc, wo_interface_staging w
where trim(w.class_code8) = trim(cc.description)
and w.class_value8 is not null
union
select distinct cc.class_code_id, w.class_value9
from class_code cc, wo_interface_staging w
where trim(w.class_code9) = trim(cc.description)
and w.class_value9 is not null
union
select distinct cc.class_code_id, w.class_value10
from class_code cc, wo_interface_staging w
where trim(w.class_code10) = trim(cc.description)
and w.class_value10 is not null
union
select distinct cc.class_code_id, w.class_value11
from class_code cc, wo_interface_staging w
where trim(w.class_code11) = trim(cc.description)
and w.class_value11 is not null
union
select distinct cc.class_code_id, w.class_value12
from class_code cc, wo_interface_staging w
where trim(w.class_code12) = trim(cc.description)
and w.class_value12 is not null
union
select distinct cc.class_code_id, w.class_value13
from class_code cc, wo_interface_staging w
where trim(w.class_code13) = trim(cc.description)
and w.class_value13 is not null
union
select distinct cc.class_code_id, w.class_value14
from class_code cc, wo_interface_staging w
where trim(w.class_code14) = trim(cc.description)
and w.class_value14 is not null
union
select distinct cc.class_code_id, w.class_value15
from class_code cc, wo_interface_staging w
where trim(w.class_code15) = trim(cc.description)
and w.class_value15 is not null
union
select distinct cc.class_code_id, w.class_value16
from class_code cc, wo_interface_staging w
where trim(w.class_code16) = trim(cc.description)
and w.class_value16 is not null
union
select distinct cc.class_code_id, w.class_value17
from class_code cc, wo_interface_staging w
where trim(w.class_code17) = trim(cc.description)
and w.class_value17 is not null
union
select distinct cc.class_code_id, w.class_value18
from class_code cc, wo_interface_staging w
where trim(w.class_code18) = trim(cc.description)
and w.class_value18 is not null
union
select distinct cc.class_code_id, w.class_value19
from class_code cc, wo_interface_staging w
where trim(w.class_code19) = trim(cc.description)
and w.class_value19 is not null
union
select distinct cc.class_code_id, w.class_value20
from class_code cc, wo_interface_staging w
where trim(w.class_code20) = trim(cc.description)
and w.class_value20 is not null
minus
select class_code_id, value
from class_code_values);
if sqlca.sqlcode < 0 then 
	uf_msgs("SQLERROR: " +    sqlca.sqlerrtext)
	
	return -1
end if
uf_msgs("Rows Effected: " + string(sqlca.sqlnrows))

return 1
end function

public subroutine wf_ddl ();

////THESE TABLES ARE REQUIED FOR THIS INTERFACE TO WORK
//
//
//drop table wo_interface_staging;
//CREATE TABLE PWRPLANT."WO_INTERFACE_STAGING" ("WORK_ORDER_ID" NUMBER(22,0), "COMPANY_ID" NUMBER(22,0), 
//"BUS_SEGMENT_ID" NUMBER(22,0), "BUDGET_ID" NUMBER(22,0), "BUDGET_VERSION_ID" NUMBER(22,0) NOT NULL, 
//"WORK_ORDER_NUMBER" VARCHAR2(35), "REASON_CD_ID" NUMBER(22,0), "WORK_ORDER_TYPE_ID" NUMBER(22,0) NOT NULL, 
//"FUNDING_WO_ID" NUMBER(22,0), "FUNDING_PROJ_NUMBER" VARCHAR2(35), "MAJOR_LOCATION_ID" NUMBER(22,0), 
//"ASSET_LOCATION_ID" NUMBER(22,0), "DESCRIPTION" VARCHAR2(35) NOT NULL, "LONG_DESCRIPTION" VARCHAR2(2000), 
//"FUNDING_WO_INDICATOR" NUMBER(22,0), "EST_START_DATE" DATE, "EST_COMPLETE_DATE" DATE, "NOTES" VARCHAR2(2000),
// "DEPARTMENT_ID" NUMBER(22,0), "EST_IN_SERVICE_DATE" DATE, "WORK_ORDER_GRP_ID" NUMBER(22,0), 
// "STATUS" NUMBER(22,0), "ACTION" VARCHAR2(1), "NEW_BUDGET" NUMBER(22,0), "IN_SERVICE_DATE" DATE, "COMPLETION_DATE" DATE,
// "INITIATION_DATE" DATE, "ROW_ID" NUMBER(22,0),  "CLOSE_DATE" DATE, 
//future_projects varchar2(2000), alternatives varchar2(2000),financial_analysis varchar2(2000),
//"REASON_FOR_WORK" VARCHAR2(2000),"BACKGROUND" VARCHAR2(2000), 
//"ENGINEER" VARCHAR2(18), "PLANT_ACCOUNTANT" VARCHAR2(18), 
//"PROJECT_MANAGER" VARCHAR2(18), "CONTRACT_ADMIN" VARCHAR2(18), "OTHER_CONTACT" VARCHAR2(18),
// "BASE_YEAR" NUMBER(4,0), "APPROVAL_TYPE_ID" NUMBER(22,0),
//"AFUDC_STOP_DATE" date, "AFUDC_START_DATE" date, "EST_ANNUAL_REV" number(22,0), "EXTERNAL_WO_NUMBER" varchar2(35),
//out_of_service_date date, suspended_date date, wo_approval_group_id number(22,0),initiator varchar2(18),
//"WO_STATUS_ID" NUMBER(22,0), "CLASS_CODE1" VARCHAR2(254), "CLASS_VALUE1" VARCHAR2(254), 
//"CLASS_CODE2" VARCHAR2(254), "CLASS_VALUE2" VARCHAR2(254), "CLASS_CODE3" VARCHAR2(254), 
//"CLASS_VALUE3" VARCHAR2(254), "CLASS_CODE4" VARCHAR2(254), "CLASS_VALUE4" VARCHAR2(254), 
//"CLASS_CODE5" VARCHAR2(254), "CLASS_VALUE5" VARCHAR2(254), "CLASS_CODE6" VARCHAR2(254), 
//"CLASS_VALUE6" VARCHAR2(254), "CLASS_CODE7" VARCHAR2(254), "CLASS_VALUE7" VARCHAR2(254), 
//"CLASS_CODE8" VARCHAR2(254), "CLASS_VALUE8" VARCHAR2(254), "CLASS_CODE9" VARCHAR2(254),
// "CLASS_VALUE9" VARCHAR2(254), "CLASS_CODE10" VARCHAR2(254), "CLASS_VALUE10" VARCHAR2(254), 
//"CLASS_CODE11" VARCHAR2(254), "CLASS_VALUE11" VARCHAR2(254), "CLASS_CODE12" VARCHAR2(254), 
//"CLASS_VALUE12" VARCHAR2(254), "CLASS_CODE13" VARCHAR2(254), "CLASS_VALUE13" VARCHAR2(254),
// "CLASS_CODE14" VARCHAR2(254), "CLASS_VALUE14" VARCHAR2(254), "CLASS_CODE15" VARCHAR2(254), 
//"CLASS_VALUE15" VARCHAR2(254), "CLASS_CODE16" VARCHAR2(254), "CLASS_VALUE16" VARCHAR2(254),
// "CLASS_CODE17" VARCHAR2(254), "CLASS_VALUE17" VARCHAR2(254), "CLASS_CODE18" VARCHAR2(254),
// "CLASS_VALUE18" VARCHAR2(254), "CLASS_CODE19" VARCHAR2(254), "CLASS_VALUE19" VARCHAR2(254), 
//"CLASS_CODE20" VARCHAR2(254), "CLASS_VALUE20" VARCHAR2(254), "CLASS_CODE21" VARCHAR2(254),
// "CLASS_VALUE21" VARCHAR2(254), "CLASS_CODE22" VARCHAR2(254), "CLASS_VALUE22" VARCHAR2(254),
// "CLASS_CODE23" VARCHAR2(254), "CLASS_VALUE23" VARCHAR2(254), "CLASS_CODE24" VARCHAR2(254),
// "CLASS_VALUE24" VARCHAR2(254), "CLASS_CODE25" VARCHAR2(254), "CLASS_VALUE25" VARCHAR2(254),
// "CLASS_CODE26" VARCHAR2(254), "CLASS_VALUE26" VARCHAR2(254), "CLASS_CODE27" VARCHAR2(254), 
//"CLASS_VALUE27" VARCHAR2(254), "CLASS_CODE28" VARCHAR2(254), "CLASS_VALUE28" VARCHAR2(254), 
//"CLASS_CODE29" VARCHAR2(254), "CLASS_VALUE29" VARCHAR2(254), "CLASS_CODE30" VARCHAR2(254),
// "CLASS_VALUE30" VARCHAR2(254)) ;
//CREATE INDEX "IX_WO_INTERFACE_STG_STAT" ON PWRPLANT."WO_INTERFACE_STAGING" ("STATUS" ) ;
//CREATE INDEX "IX_WO_INTERFACE_STG_FUND" ON PWRPLANT."WO_INTERFACE_STAGING" ("FUNDING_WO_INDICATOR" ) ;
//CREATE INDEX "IX_WO_INTERFACE_STG_ACT" ON PWRPLANT."WO_INTERFACE_STAGING" ("ACTION" ) ;
//CREATE INDEX "IX_WO_INTERFACE_STG_WONUM" ON PWRPLANT."WO_INTERFACE_STAGING" ("FUNDING_PROJ_NUMBER" ) ;
//CREATE INDEX "IX_WO_INTERFACE_COMP" ON PWRPLANT."WO_INTERFACE_STAGING" ("COMPANY_ID" ) ;
//CREATE INDEX "WO_INTERFACE_STGG_IDX_WON" ON PWRPLANT."WO_INTERFACE_STAGING" ("WORK_ORDER_NUMBER" ) ;
//
//
//create or replace public synonym WO_INTERFACE_STAGING for pwrplant.WO_INTERFACE_STAGING;
//grant all on WO_INTERFACE_STAGING to pwrplant_role_dev;
//
//drop table wo_interface_staging_arc;
//CREATE TABLE PWRPLANT."WO_INTERFACE_STAGING_ARC" ("WORK_ORDER_ID" NUMBER(22,0), "COMPANY_ID" NUMBER(22,0), 
//"BUS_SEGMENT_ID" NUMBER(22,0), "BUDGET_ID" NUMBER(22,0), "BUDGET_VERSION_ID" NUMBER(22,0) NOT NULL, 
//"WORK_ORDER_NUMBER" VARCHAR2(35), "REASON_CD_ID" NUMBER(22,0), "WORK_ORDER_TYPE_ID" NUMBER(22,0) NOT NULL, 
//"FUNDING_WO_ID" NUMBER(22,0), "FUNDING_PROJ_NUMBER" VARCHAR2(35), "MAJOR_LOCATION_ID" NUMBER(22,0), 
//"ASSET_LOCATION_ID" NUMBER(22,0), "DESCRIPTION" VARCHAR2(35) NOT NULL, "LONG_DESCRIPTION" VARCHAR2(2000), 
//"FUNDING_WO_INDICATOR" NUMBER(22,0), "EST_START_DATE" DATE, "EST_COMPLETE_DATE" DATE, "NOTES" VARCHAR2(2000),
// "DEPARTMENT_ID" NUMBER(22,0), "EST_IN_SERVICE_DATE" DATE, "WORK_ORDER_GRP_ID" NUMBER(22,0), 
// "STATUS" NUMBER(22,0), "ACTION" VARCHAR2(1), "NEW_BUDGET" NUMBER(22,0), "IN_SERVICE_DATE" DATE, "COMPLETION_DATE" DATE,
// "INITIATION_DATE" DATE, "ROW_ID" NUMBER(22,0),  "CLOSE_DATE" DATE, 
//future_projects varchar2(2000), alternatives varchar2(2000),financial_analysis varchar2(2000),
//"REASON_FOR_WORK" VARCHAR2(2000),"BACKGROUND"  VARCHAR2(2000), 
//"ENGINEER" VARCHAR2(18), "PLANT_ACCOUNTANT" VARCHAR2(18), 
//"PROJECT_MANAGER" VARCHAR2(18), "CONTRACT_ADMIN" VARCHAR2(18), "OTHER_CONTACT" VARCHAR2(18),
// "BASE_YEAR" NUMBER(4,0), "APPROVAL_TYPE_ID" NUMBER(22,0),
//"AFUDC_STOP_DATE" date, "AFUDC_START_DATE" date, "EST_ANNUAL_REV" number(22,0), "EXTERNAL_WO_NUMBER" varchar2(35),
//out_of_service_date date, suspended_date date, wo_approval_group_id number(22,0),initiator varchar2(18),
//"WO_STATUS_ID" NUMBER(22,0), "CLASS_CODE1" VARCHAR2(254), "CLASS_VALUE1" VARCHAR2(254), 
//"CLASS_CODE2" VARCHAR2(254), "CLASS_VALUE2" VARCHAR2(254), "CLASS_CODE3" VARCHAR2(254), 
//"CLASS_VALUE3" VARCHAR2(254), "CLASS_CODE4" VARCHAR2(254), "CLASS_VALUE4" VARCHAR2(254), 
//"CLASS_CODE5" VARCHAR2(254), "CLASS_VALUE5" VARCHAR2(254), "CLASS_CODE6" VARCHAR2(254), 
//"CLASS_VALUE6" VARCHAR2(254), "CLASS_CODE7" VARCHAR2(254), "CLASS_VALUE7" VARCHAR2(254), 
//"CLASS_CODE8" VARCHAR2(254), "CLASS_VALUE8" VARCHAR2(254), "CLASS_CODE9" VARCHAR2(254),
// "CLASS_VALUE9" VARCHAR2(254), "CLASS_CODE10" VARCHAR2(254), "CLASS_VALUE10" VARCHAR2(254), 
//"CLASS_CODE11" VARCHAR2(254), "CLASS_VALUE11" VARCHAR2(254), "CLASS_CODE12" VARCHAR2(254), 
//"CLASS_VALUE12" VARCHAR2(254), "CLASS_CODE13" VARCHAR2(254), "CLASS_VALUE13" VARCHAR2(254),
// "CLASS_CODE14" VARCHAR2(254), "CLASS_VALUE14" VARCHAR2(254), "CLASS_CODE15" VARCHAR2(254), 
//"CLASS_VALUE15" VARCHAR2(254), "CLASS_CODE16" VARCHAR2(254), "CLASS_VALUE16" VARCHAR2(254),
// "CLASS_CODE17" VARCHAR2(254), "CLASS_VALUE17" VARCHAR2(254), "CLASS_CODE18" VARCHAR2(254),
// "CLASS_VALUE18" VARCHAR2(254), "CLASS_CODE19" VARCHAR2(254), "CLASS_VALUE19" VARCHAR2(254), 
//"CLASS_CODE20" VARCHAR2(254), "CLASS_VALUE20" VARCHAR2(254), "CLASS_CODE21" VARCHAR2(254),
// "CLASS_VALUE21" VARCHAR2(254), "CLASS_CODE22" VARCHAR2(254), "CLASS_VALUE22" VARCHAR2(254),
// "CLASS_CODE23" VARCHAR2(254), "CLASS_VALUE23" VARCHAR2(254), "CLASS_CODE24" VARCHAR2(254),
// "CLASS_VALUE24" VARCHAR2(254), "CLASS_CODE25" VARCHAR2(254), "CLASS_VALUE25" VARCHAR2(254),
// "CLASS_CODE26" VARCHAR2(254), "CLASS_VALUE26" VARCHAR2(254), "CLASS_CODE27" VARCHAR2(254), 
//"CLASS_VALUE27" VARCHAR2(254), "CLASS_CODE28" VARCHAR2(254), "CLASS_VALUE28" VARCHAR2(254), 
//"CLASS_CODE29" VARCHAR2(254), "CLASS_VALUE29" VARCHAR2(254), "CLASS_CODE30" VARCHAR2(254),
// "CLASS_VALUE30" VARCHAR2(254), run_date date) ;
//
//create or replace public synonym WO_INTERFACE_STAGING_ARC for pwrplant.WO_INTERFACE_STAGING_ARC;
//grant all on WO_INTERFACE_STAGING_ARC to pwrplant_role_dev;
////
////
/////OTHER NOTES
////1. Add Approval Status to work Order approval
////2. New fields on work order account (accural,cr derivation)
//
//
end subroutine

public function integer uf_create_fps (longlong a_company, string a_batch);longlong rtn

rtn = uf_create_wos_fps(a_company, a_batch, 1)

return rtn


////***************************************************************************************** 
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////
////   Subsystem:   system
////
////   Event    :   uo_wo_interface.uf_create_fps
////
////   Purpose  :   Creates Funding Projects from pre-staged data in the table wo_interface_staging.
////					Assumes that the Funding Project Number, Work_order_type_id and desciption are not null in the wo_interface_staging table.
////                     
//// 					
////   DATE                           NAME                            REVISION                     CHANGES
////  --------      --------     -----------   --------------------------------------------- 
////  07-23-2008      PowerPlan           Version 1.0   Initial Version
////
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////*****************************************************************************************
//longlong checkers, counts, i, rtn, update_counts, bv_id, num_batches, ii
//any results[]
//string workflow_appr, cc_sqls_wocc, cc_sqls_bcc, co_validate, bud_sum, bud_org, bv_by_company, bi_workflow_appr
//
//if isnull(a_batch) then a_batch = 'null'
//
//uf_msgs("------------------------------- ")
//uf_msgs("------------------------------- ")
//uf_msgs("Begin FP Loading - Company Id = "+string(a_company))
//
//
////--------------------------------------------------------------------------------
////
//// PROCESSING STATUS
////
////--------------------------------------------------------------------------------
//uf_msgs("Setting Status")
//update wo_interface_staging a
//set status = 1,
//	funding_wo_indicator = nvl(a.funding_wo_indicator, 0),
//	error_message = null
//where nvl(status,1) <> 2
//and nvl(batch_id,'null') = :a_batch
//and funding_wo_indicator = 1
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// COMPANY
////  - always required
////
////--------------------------------------------------------------------------------
//uf_msgs("Validating Company")
//
//////
//////	If an external value is provided, try to map on it and error out if it cannot be mapped
//////
//
//// Map on gl_company_no
//update wo_interface_staging a
//set a.company_id = (
//	select p.company_id
//	from company p
//	where p.gl_company_no = a.ext_company
//	)
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 1
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//// Map on company description
//update wo_interface_staging a
//set a.company_id = (
//	select p.company_id
//	from company p
//	where p.description = a.ext_company
//	)
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 1
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//// Map on company_id
//update wo_interface_staging a
//set a.company_id = (
//	select p.company_id
//	from company p
//	where to_char(p.company_id) = a.ext_company
//	)
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 1
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//// Flag records where ext_company could not be mapped
//update wo_interface_staging a
//set a.error_message = a.error_message || 'FP - cannot derive company_id; '
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 1
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//////
//////	No external value was provided
//////
//
//// Flag records missing company_id as errors
//update wo_interface_staging a
//set a.error_message = a.error_message || 'FP - company_id is required; '
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 1
//and a.company_id is null
//and a.ext_company is null
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// WORK ORDER NUMBER
////  - always required
////
////--------------------------------------------------------------------------------
//uf_msgs("Validating Funding Project Number")
//
//// Flag records missing work_order_number as errors
//update wo_interface_staging a
//set a.error_message = a.error_message || 'FP - work_order_number is required; '
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 1
//and a.work_order_number is null
//;
//if uf_check_sql() = -1 then return -1
//// Field is not null...this should never find anything
//
//
////--------------------------------------------------------------------------------
////
//// VALIDATION
////   - must have a valid COMPANY_ID and WORK_ORDER_NUMBER
////
////--------------------------------------------------------------------------------
//uf_msgs("Flagging Errors (missing Company or Work Order Number)")
//
//// Flag records that already have errors as unable to continue processing (either invalid company_id or work_order_number)
//update wo_interface_staging a
//set a.status = -11
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
//and funding_wo_indicator = 1
//and a.error_message is not null
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// DETERMINE THE NUMBER OF BATCHES
////   - Loop over batches of work orders
////
////--------------------------------------------------------------------------------
//uf_msgs("Identifying Process Batches")
//
//// Determine the number of loops needed to process all work orders in sequence
//select max(batches)
//into :num_batches
//from (
//	select work_order_number, company_id, count(distinct nvl(seq_id,0)) batches
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	group by work_order_number, company_id
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//uf_msgs("Updating status to Pending")
//
//// Update records that have not yet been processed to status = 3 (pending
//update wo_interface_staging a
//set a.status = 3 // pending
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 1
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// LOOP OVER THE BATCHES
////
////--------------------------------------------------------------------------------
//for ii = 1 to num_batches
//	uf_msgs(" ")
//	uf_msgs("***** Batch "+string(ii)+" of "+string(num_batches)+" *****")
//	uf_msgs(" ")
//	
//	uf_msgs("Updating status to In-Process")
//	update wo_interface_staging a
//	set a.status = 1 // in-process
//	where a.status = 3
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and nvl(seq_id,0) in (
//		select min(nvl(seq_id,0))
//		from wo_interface_staging b
//		where b.status = 3
//		and nvl(b.batch_id,'null') = :a_batch
//		and (b.company_id = :a_company or :a_company = -1)
//		and b.funding_wo_indicator = 1
//		and b.work_order_number = a.work_order_number
//		and b.company_id = a.company_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DETERMINE ACTION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Setting Action")
//	
//	// Reset action code
//	update wo_interface_staging a
//	set a.action = null,
//		a.work_order_id = null
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Identify "update" records
//	update wo_interface_staging a
//	set a.action = 'U',
//		(a.work_order_id, a.old_wo_status_id) = (
//			select b.work_order_id, b.wo_status_id
//			from work_order_control b 
//			where b.work_order_number = a.work_order_number
//			and b.company_id = a.company_id
//			and b.funding_wo_indicator = a.funding_wo_indicator
//			)
//	where exists (
//		select 1 from work_order_control b 
//		where b.work_order_number = a.work_order_number
//		and b.company_id = a.company_id
//		and b.funding_wo_indicator = a.funding_wo_indicator
//		)
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Identify "insert" records
//	update wo_interface_staging a
//	set action = 'I',
//		work_order_id = pwrplant1.nextval,
//		old_wo_status_id = null
//	where nvl(action,'I') <> 'U'
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	// This should never find anything anyways because we are processing in batches
//	////--------------------------------------------------------------------------------
//	////
//	//// SETTING OLD RECORDS AS PROCESSED BASED ON SEQUENCE (SEQ_ID)
//	////   If a newer record for the same funding project already exists, mark the
//	////   old one as processed, and continue processing the latest one
//	////
//	////--------------------------------------------------------------------------------
//	//uf_msgs("Checking Sequence")
//	//update wo_interface_staging a
//	//set a.status = 2
//	//where a.status = 1
//	//and nvl(a.batch_id,'null') = :a_batch
//	//and (a.company_id = :a_company or :a_company = -1)
//	//and funding_wo_indicator = 1
//	//and nvl(a.seq_id,0) < (
//	//	select nvl(max(b.seq_id),0)
//	//	from wo_interface_staging b
//	//	where b.status = 1
//	//	and nvl(b.batch_id,'null') = :a_batch
//	//	and (b.company_id = :a_company or :a_company = -1)
//	//	and b.funding_wo_indicator = 1
//	//	and b.work_order_id = a.work_order_id
//	//	)
//	//;
//	//if uf_check_sql() = -1 then return -1
//	
//	// Flag duplicate records where the same funding project for the same company exists more than once
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - duplicate records exist for this funding project with identical/blank seq_ids; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_id in (
//		select c.work_order_id
//		from wo_interface_staging c
//		where c.status = 1
//		and nvl(c.batch_id,'null') = :a_batch
//		and (c.company_id = :a_company or :a_company = -1)
//		and c.funding_wo_indicator = 1
//		group by c.work_order_id
//		having count(*) > 1
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COUNT RECORDS TO PROCESS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Checking for records to process")
//	
//	// Look for any records to process whether Insert or Update
//	select count(*) into :counts
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	;
//	
//	if counts = 0 then
//		uf_msgs("0 records to process")
//		uf_msgs("-------------------------------")
//		uf_msgs("-------------------------------")
//		return 1
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DESCRIPTION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Validating Funding Project Description")
//	
//	// Flag records missing work_order_number as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - description is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.description is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// WORK ORDER TYPE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Work Order Type")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external_work_order_type
//	update wo_interface_staging a
//	set a.work_order_type_id = (
//		select p.work_order_type_id
//		from work_order_type p
//		where p.external_work_order_type = a.ext_work_order_type
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work_order_type description
//	update wo_interface_staging a
//	set a.work_order_type_id = (
//		select p.work_order_type_id
//		from work_order_type p
//		where p.description = a.ext_work_order_type
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work_order_type_id
//	update wo_interface_staging a
//	set a.work_order_type_id = (
//		select p.work_order_type_id
//		from work_order_type p
//		where to_char(p.work_order_type_id) = a.ext_work_order_type
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_work_order_type could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive work_order_type_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided
//	////
//	
//	// Flag records missing work_order_type_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - work_order_type_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// FUNDING WO ID
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Updating Funding Wo Id to null")
//	
//	// Funding Projects should have a null funding_wo_id
//	update wo_interface_staging a
//	set funding_wo_id = null
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// BUDGET ITEM
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Budget Item")
//	
//	// Check if company on budget item and funding project must be the same
//	co_validate = lower(trim(f_pp_system_control_company('FPINT-Validate WO TYPE/Bud Item Co', a_company)))
//	if isnull(co_validate) then
//		co_validate = 'yes'
//	end if
//	
//	if co_validate = 'yes' then
//		// Update budget_company_id
//		update wo_interface_staging a
//		set a.budget_company_id = a.company_id
//		where a.status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and a.funding_wo_indicator = 1
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on budget number
//	update wo_interface_staging a
//	set a.budget_id = (
//		select b.budget_id
//		from budget b
//		where b.budget_number = a.budget_number
//		and b.company_id = nvl(a.budget_company_id, a.company_id)
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.budget_id is null
//	and a.budget_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on description
//	update wo_interface_staging a
//	set a.budget_id = (
//		select b.budget_id
//		from budget b
//		where b.description = a.budget_number
//		and b.company_id = nvl(a.budget_company_id, a.company_id)
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.budget_id is null
//	and a.budget_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on budget_id
//	update wo_interface_staging a
//	set a.budget_id = (
//		select b.budget_id
//		from budget b
//		where to_char(b.budget_id) = a.budget_number
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.budget_id is null
//	and a.budget_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where budget_number could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive budget_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.budget_id is null
//	and a.budget_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided
//	////
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// BUSINESS SEGMENT
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Business Segment")
//	
//	////
//	////	When updating, bus_segment_id must be null - cannot be updated
//	////
//	
//	// Null out bus_segment_id...cannot be changed on a funding project - ONLY FOR UPDATES
//	update wo_interface_staging a
//	set a.bus_segment_id = null
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	When inserting, bus_segment_id must come from the work_order_type
//	////
//	
//	// Map on work order type - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.bus_segment_id = (
//		select b.bus_segment_id
//		from work_order_type b
//		where b.work_order_type_id = a.work_order_type_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	//and a.bus_segment_id is null --must always come from the Work Order Type
//	and a.work_order_type_id is not null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records missing bus_segment_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive bus_segment_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.bus_segment_id is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// ASSET LOCATION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Asset Location")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on ext_asset_location
//	update wo_interface_staging a
//	set a.asset_location_id = (
//		select b.asset_location_id
//		from asset_location b
//		where b.ext_asset_location = a.ext_asset_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on asset location description
//	update wo_interface_staging a
//	set a.asset_location_id = (
//		select b.asset_location_id
//		from asset_location b
//		where b.long_description = a.ext_asset_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on asset_location_id
//	update wo_interface_staging a
//	set a.asset_location_id = (
//		select b.asset_location_id
//		from asset_location b
//		where to_char(b.asset_location_id) = a.ext_asset_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_asset_location could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive asset_location_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided
//	////
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// MAJOR LOCATION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Major Location")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external_location_id
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from major_location b
//		where b.external_location_id = a.ext_major_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on major location description
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from major_location b
//		where b.description = a.ext_major_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on major_location_id
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from major_location b
//		where to_char(b.major_location_id) = a.ext_major_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_major_location could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive major_location_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided, try to derive some other way
//	////
//	
//	// If we were able to derive an asset_location_id above, then map on asset_location_id 
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from asset_location b
//		where b.asset_location_id = a.asset_location_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.major_location_id is null
//	and a.asset_location_id is not null
//	and a.ext_major_location is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on budget item - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from budget b
//		where b.budget_id = a.budget_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.major_location_id is null
//	and a.budget_id is not null
//	and a.ext_major_location is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records missing major_location_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - major_location_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.major_location_id is null
//	and a.ext_major_location is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DEPARTMENT
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Department")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external_department_code
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from department b
//		where b.external_department_code = a.ext_department
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on department description
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from department b
//		where b.description = a.ext_department
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on department_id
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from department b
//		where to_char(b.department_id) = a.ext_department
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_department could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive department_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided, try to derive some other way
//	////
//	
//	// Map on budget department - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from budget b
//		where b.budget_id = a.budget_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.department_id is null
//	and a.budget_id is not null
//	and a.ext_department is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records missing department_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - department_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.department_id is null
//	and a.ext_department is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// WORK ORDER GROUP
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Work Order Group")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on work order group description
//	update wo_interface_staging a
//	set a.work_order_grp_id = (
//		select b.work_order_grp_id
//		from work_order_group b
//		where b.description = a.ext_work_order_group
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work_order_grp_id
//	update wo_interface_staging a
//	set a.work_order_grp_id = (
//		select b.work_order_grp_id
//		from work_order_group b
//		where to_char(b.work_order_grp_id) = a.ext_work_order_group
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_department could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive work_order_grp_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided, try to derive some other way
//	////
//	
//	// Map on work order type - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.work_order_grp_id = (
//		select b.work_order_grp_id
//		from wo_grp_wo_type b
//		where b.work_order_type_id = a.work_order_type_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// WORK ORDER STATUS
//	//    MUST populate BOTH wo_status_id AND all corresponding status dates in order to process status updates
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Work Order Status")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external work order status
//	update wo_interface_staging a
//	set a.wo_status_id = (
//		select b.wo_status_id
//		from work_order_status b
//		where b.external_work_order_status = a.ext_work_order_status
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work order status description
//	update wo_interface_staging a
//	set a.wo_status_id = (
//		select b.wo_status_id
//		from work_order_status b
//		where b.description = a.ext_work_order_status
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on wo status id
//	update wo_interface_staging a
//	set a.wo_status_id = (
//		select b.wo_status_id
//		from work_order_status b
//		where to_char(b.wo_status_id) = a.ext_work_order_status
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_work_order_status could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive wo_status_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	If wo_status_id is provided, that means we are updating the status
//	////		Validate that the appropriate dates are populated - ALL STATUS DATES MUST BE PROVIDED WHEN WO_STATUS_ID IS POPULATED, including null dates
//	////		Status dates include:
//	////			- in_service_date
//	////			- completion_date
//	////			- close_date
//	////			- suspended_date
//	////		Validate that the status is changing from a valid previous status
//	////
//	
//	// Flag records where wo_status_id and dates are out of sync - Cancelled
//	// - Cancelled can only happen for work orders in an open, in service, or completed status.  Status dates should agree with one of those statuses
//	// - Cannot Cancel from Unitized or Posted to CPR status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (Cancelled - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 8
//	and (
//		not (
//			(close_date is not null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = open*/ OR
//			(close_date is not null and completion_date is null and in_service_date is not null and suspended_date is null) /*dates = in service*/ OR
//			(close_date is not null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = complete*/
//			)
//		OR
//		nvl(old_wo_status_id,8) in (6,7)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Posted to CPR
//	// !!!!! Posted to CPR status is ONLY allowed when initiating new work orders, presumably for conversion purposes !!!!!
//	// !!!!! This logic does NOT allow changing the work order from any other status to Posted to CPR !!!!!
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (Posted to CPR - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 7
//	and (
//		not (close_date is not null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = posted to cpr*/
//		OR
//		nvl(old_wo_status_id,7) in (1,2,3,4,5,6,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Unitized
//	// !!!!! Unitized status is ONLY meaningful inside of PowerPlan workflow. This status cannot be interfaced for any reason !!!!!
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (Unitized - cannot process this status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 6
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Completed
//	// - Complete can only happen for work orders in an Open or In Service status...Initiated or Suspended work orders presumably came through the Open and In Service statuses
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (Completed - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 5
//	and (
//		not (close_date is null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = completed*/
//		OR
//		nvl(old_wo_status_id,5) in (6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - In Service
//	// - In Service can only happen for work orders in an Open or Completed status...Initiated or Suspended work orders presumably came through the Open status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (In Service - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 4
//	and (
//		not (close_date is null and completion_date is null and in_service_date is not null and suspended_date is null) /*dates = in service*/
//		OR
//		nvl(old_wo_status_id,4) in (6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Suspended
//	// - Suspended can only happen for work orders in an Initiated or Open status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (Suspended - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 3
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is not null) /*dates = initiated OR open*/
//		OR
//		nvl(old_wo_status_id,3) in (4,5,6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Open
//	// - Open can only happen for work orders in an Initiated or Suspended status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (Open - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 2
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = open*/
//		OR
//		nvl(old_wo_status_id,2) in (4,5,6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Initiated
//	// - Initiated can only happen for work orders in a Suspended status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (Initiated - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id = 1
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = initiated*/
//		OR
//		nvl(old_wo_status_id,1) in (2,4,5,6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id is not a valid id
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - wo_status_id validation error (wo_status_id must be in (1,2,3,4,5,6,7,8)); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id is not null
//	and a.wo_status_id not in (1,2,3,4,5,6,7,8)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No wo_status_id was provided
//	////
//	
//	// Flag records where wo_status_id is not populated but one or more dates are populated
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - one or more status dates are populated...this is not allowed unless populating wo_status_id with a valid status change; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.wo_status_id is null
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = none*/
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map Open work orders based on auto-approved approval types - ONLY FOR INSERTS
//	workflow_appr = lower(trim(f_pp_system_control_company('WORKFLOW USER OBJECT', a_company)))
//	if workflow_appr = 'yes' then
//		update wo_interface_staging a
//		set wo_status_id = 2
//		where status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and funding_wo_indicator = 1
//		and exists (
//			select 1 from work_order_type b, workflow_type c
//			where b.work_order_type_id = a.work_order_type_id
//			and b.approval_type_id = c.workflow_type_id
//			and not exists (
//				select 1 from workflow_type_rule wtr
//				where wtr.workflow_type_id = c.workflow_type_id
//				)
//			)
//		and action = 'I'
//		and wo_status_id is null
//		and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
//		;
//		if uf_check_sql() = -1 then return -1
//	else
//		update wo_interface_staging a
//		set wo_status_id = 2
//		where status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and funding_wo_indicator = 1
//		and exists (
//			select 1 from work_order_type b, approval c
//			where b.work_order_type_id = a.work_order_type_id
//			and b.approval_type_id = c.approval_type_id
//			and auth_level1 is null
//			and auth_level2 is null
//			and auth_level3 is null
//			and auth_level4 is null
//			and auth_level5 is null
//			and auth_level6 is null
//			and auth_level7 is null
//			and auth_level8 is null
//			and auth_level9 is null
//			and auth_level10 is null
//			)
//		and action = 'I'
//		and wo_status_id is null
//		and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	// Map remaining work orders as Initiated - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set wo_status_id = 1
//	where status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and funding_wo_indicator = 1
//	and action = 'I'
//	and wo_status_id is null 
//	and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id could not be mapped - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - cannot derive wo_status_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and action = 'I'
//	and a.wo_status_id is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// BUDGET VERSION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Current Budget Version")
//	
//	// Map based on current version by company
//	update wo_interface_staging z
//	set z.budget_version_id = (
//		select a.budget_version_id
//		from budget_version a
//		where a.current_version = 1
//		and a.open_for_entry = 1
//		and a.company_id = z.company_id
//		)
//	where z.status = 1
//	and nvl(z.batch_id,'null') = :a_batch
//	and (z.company_id = :a_company or :a_company = -1)
//	and z.funding_wo_indicator = 1
//	and z.action = 'I'
//	and exists (
//		select 1 from pp_system_control_companies pp
//		where pp.company_id = z.company_id
//		and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
//		and upper(trim(pp.control_value)) = upper(trim('yes'))
//		);
//	if uf_check_sql() = -1 then return -1
//	
//	// Map based on current version by company summary
//	update wo_interface_staging z
//	set z.budget_version_id = (
//		select a.budget_version_id
//		from budget_version a, company b, company c
//		where a.current_version = 1
//		and a.open_for_entry = 1
//		and a.company_id = b.company_id
//		and b.company_summary_id = c.company_summary_id
//		and c.company_id = z.company_id
//		)
//	where z.status = 1
//	and nvl(z.batch_id,'null') = :a_batch
//	and (z.company_id = :a_company or :a_company = -1)
//	and z.funding_wo_indicator = 1
//	and z.action = 'I'
//	and exists (
//		select 1 from pp_system_control_companies pp
//		where pp.company_id = z.company_id
//		and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
//		and upper(trim(pp.control_value)) = upper(trim('summary'))
//		);
//	if uf_check_sql() = -1 then return -1
//	
//	// Map based on current version for entire system
//	update wo_interface_staging z
//	set z.budget_version_id = (
//		select budget_version_id
//		from budget_version a
//		where current_version = 1
//		and open_for_entry = 1
//		)
//	where z.status = 1
//	and nvl(z.batch_id,'null') = :a_batch
//	and (z.company_id = :a_company or :a_company = -1)
//	and z.funding_wo_indicator = 1
//	and z.action = 'I'
//	and not exists (
//		select 1 from pp_system_control_companies pp
//		where pp.company_id = z.company_id
//		and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
//		and upper(trim(pp.control_value)) in (upper(trim('yes')), upper(trim('summary')))
//		);
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT NEW BUDGET ITEMS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Creating New Budget Items")
//	
//	// Flag new budget items to be created
//	update wo_interface_staging a
//	set a.budget_id = pwrplant1.nextval,
//		a.new_budget = 1
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.budget_id is null
//	and a.budget_number is null
//	and action = 'I'
//	and (a.company_id, a.work_order_number) in (
//		select c.company_id, c.work_order_number
//		from wo_interface_staging c
//		where c.status = 1
//		and nvl(c.batch_id,'null') = :a_batch
//		and (c.company_id = :a_company or :a_company = -1)
//		and c.funding_wo_indicator = 1
//		and c.budget_id is null
//		and c.budget_number is null
//		and c.action = 'I'
//		minus
//		select b.company_id, b.budget_number
//		from budget b
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Check if mappings from fp values to bi values exist
//	bud_sum = lower(trim(f_pp_system_control_company('CAPBUD - WOT to Budget Summary', a_company)))
//	if isnull(bud_sum) or trim(bud_sum) = '' then
//		bud_sum = 'no'
//	end if
//	bud_org = lower(trim(f_pp_system_control_company('CAPBUD - WOG to Budget Org', a_company)))
//	if isnull(bud_org) or trim(bud_org) = '' then
//		bud_org = 'no'
//	end if
//	
//	// Insert new budget items
//	insert into budget (
//		budget_id, budget_number, budget_status_id, major_location_id, long_description,
//		company_id, description, resp_person, department_id, bus_segment_id, afudc_type_id,
//		notes,agreemnt_id, eligible_for_afudc, eligible_for_cpi,reimbursable_type_id,approved_amount, approved_amount_local,
//		budget_summary_id, budget_organization_id
//		)
//	select a.budget_id, nvl(a.budget_number, a.work_order_number), 1 budget_status_id, a.major_location_id, substr(a.long_description,1,254),
//		nvl(a.budget_company_id, a.company_id), a.description, user, a.department_id, a.bus_segment_id, b.default_afc_type_id, 
//		a.notes, nvl(a.agreement_id, b.agreemnt_id), b.afc_elig_indicator, b.cpi_elig_indicator, b.reimbursable_type_id, 0, 0,
//		decode(:bud_sum, 'yes', c.budget_summary_id, null), decode(:bud_org, 'yes', d.budget_organization_id, null)
//	from wo_interface_staging a, work_order_type b,
//		(	select work_order_type_id, min(budget_summary_id) budget_summary_id
//			from work_order_type_budget_summary
//			group by work_order_type_id
//		) c,
//		work_order_group_budget_org d
//	where a.work_order_type_Id = b.work_order_type_id
//	and a.work_order_type_id = c.work_order_type_id(+)
//	and a.work_order_grp_id = d.work_order_grp_id (+)
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and nvl(a.new_budget,0) = 1
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	bv_by_company = lower(trim(f_pp_system_control_company('BV Current Version by Company', a_company)))
//	if isnull(bv_by_company) or trim(bv_by_company) = '' then
//		bv_by_company = 'no'
//	end if
//	
//	// Insert Budget_Amounts
//	insert into budget_amounts	(
//		budget_id, budget_version_id, start_date, close_date, budget_total_dollars, curr_year_budget_dollars, closing_pattern_id,
//		in_service_date, closing_option_id, late_chg_wait_period, base_year,
//		escalation_id, budget_total_local, curr_year_budget_local, hist_amount, hist_amount_local
//		)
//	select a.budget_id, a.budget_version_id, a.est_start_date, a.est_complete_date, 0, 0, b.closing_pattern_id,
//		nvl(a.est_in_service_date,a.est_complete_date), b.closing_option_id, b.late_chg_wait_period, a.base_year,
//		b.escalation_id, 0, 0, 0, 0
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and nvl(a.new_budget,0) = 1
//	and a.budget_version_id is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Insert Budget_Approval
//	insert into budget_approval (
//		budget_id, budget_version_id, approval_number, approval_status_id,
//		approved_amount, approved_amount_local, hist_amount, hist_amount_local)
//	select a.budget_id, a.budget_version_id, 1, 1,
//		0, 0, 0, 0
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_Id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and nvl(a.new_budget,0) = 1
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Insert Workflow
//	bi_workflow_appr = lower(trim(f_pp_system_control_company('Workflow User Object - Bud Item', a_company)))
//	if bi_workflow_appr = 'yes' then
//		insert into workflow (
//			workflow_id, workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
//			approval_amount, approval_status_id,use_limits,sql_approval_amount )
//		select pwrplant1.nextval, c.workflow_type_id, c.description, 'bi_approval', a.budget_id, 1,
//			0 approval_amount, decode(a.wo_status_id,1,1,3), c.use_limits, c.sql_approval_amount
//		from wo_interface_staging a, work_order_type b,
//			(	select * from workflow_type z
//				where z.workflow_type_id = (
//					select max(zz.workflow_type_id)
//					from workflow_type zz
//					where lower(zz.subsystem) like '%bi_approval%'
//					)
//			) c
//		where a.work_order_type_Id = b.work_order_type_id
//		and a.status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and a.funding_wo_indicator = 1
//		and nvl(a.new_budget,0) = 1
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	// Flag Budget Items as having been inserted
//	update wo_interface_staging a
//	set a.new_budget = 2
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and nvl(a.new_budget,0) = 1
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DATA MODEL VALIDATIONS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Validating Data Model Relationships")
//	
//	uf_msgs("   - Company (Work Order / Work Order Type)")
//	// Flag records where company_id does not match company_id on work order type
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - company_id mismatch with work order type company_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 1
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.work_order_type_id is not null
//	and a.company_id <> (
//		select nvl(p.company_id,a.company_id)
//		from work_order_control p
//		where p.work_order_id = a.work_order_type_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Company / Bus Segment")
//	// Flag records where company_id / bus_segment_id relationship is not valid
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - invalid company_id / bus_segment_id relationship; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 1
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.bus_segment_id is not null
//	and not exists (
//		select 1 from company_bus_segment_control p
//		where p.company_id = a.company_id
//		and p.bus_segment_id = a.bus_segment_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Company / Major Location")
//	// Flag records where company_id / major_location_id relationship is not valid
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - invalid company_id / major_location_id relationship; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 1
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.major_location_id is not null
//	and not exists (
//		select 1 from company_major_location p
//		where p.company_id = a.company_id
//		and p.major_location_id = a.major_location_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Major Location / Asset Location")
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'FP - invalid major_location_id / asset_location_id relationship; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 1
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.major_location_id is not null
//	and a.asset_location_id is not null
//	and not exists (
//		select 1 from asset_location p
//		where p.asset_location_id = a.asset_location_id
//		and p.major_location_id = a.major_location_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERTS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting New Funding Projects")
//	
//	// Check for any new funding projects to process
//	select count(*) into :counts
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	
//	if isnull(counts) or counts = 0 then
//		uf_msgs("No New Funding Projects to Process")
//		goto updates
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_CONTROL
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Control")
//	
//	// Insert into work_order_control
//	insert into work_order_control (
//		WORK_ORDER_ID, WORK_ORDER_NUMBER, BUS_SEGMENT_ID, COMPANY_ID,
//		BUDGET_ID, WO_STATUS_ID, REASON_CD_ID, WORK_ORDER_TYPE_ID,
//		FUNDING_WO_ID, MAJOR_LOCATION_ID, ASSET_LOCATION_ID, CURRENT_REVISION,
//		DESCRIPTION, LONG_DESCRIPTION, FUNDING_WO_INDICATOR, EST_START_DATE,
//		EST_COMPLETE_DATE, LATE_CHG_WAIT_PERIOD, NOTES, DEPARTMENT_ID,
//		EST_IN_SERVICE_DATE, COMPLETION_DATE, IN_SERVICE_DATE, close_date,
//		EST_ANNUAL_REV, EXTERNAL_WO_NUMBER, OUT_OF_SERVICE_DATE, SUSPENDED_DATE,
//		WO_APPROVAL_GROUP_ID
//		)
//	select a.WORK_ORDER_ID, a.WORK_ORDER_NUMBER, a.BUS_SEGMENT_ID, a.COMPANY_ID,
//		a.BUDGET_ID, a.wo_status_id, a.REASON_CD_ID, a.WORK_ORDER_TYPE_ID,
//		null FUNDING_WO_ID, a.MAJOR_LOCATION_ID, a.ASSET_LOCATION_ID, decode(a.wo_status_id,1,0,1) current_revision,
//		a.DESCRIPTION, a.LONG_DESCRIPTION, 1 FUNDING_WO_INDICATOR, nvl(a.EST_START_DATE,sysdate),
//		nvl(a.EST_COMPLETE_DATE,sysdate), b.LATE_CHG_WAIT_PERIOD, a.NOTES, a.DEPARTMENT_ID,
//		nvl(a.EST_IN_SERVICE_DATE,nvl(a.EST_COMPLETE_DATE,sysdate)), a.COMPLETION_DATE, a.IN_SERVICE_DATE, a.close_date,
//		a.EST_ANNUAL_REV, a.EXTERNAL_WO_NUMBER, a.OUT_OF_SERVICE_DATE, a.SUSPENDED_DATE,
//		a.WO_APPROVAL_GROUP_ID
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_ACCOUNT
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Account")
//	
//	// Insert into work_order_account
//	insert into work_order_account (
//		WORK_ORDER_ID, REIMBURSABLE_TYPE_ID, AGREEMNT_ID, WORK_ORDER_GRP_ID,
//		ESTIMATING_OPTION_ID, CLOSING_OPTION_ID, ALLOC_METHOD_TYPE_ID, RETIREMENT_TYPE,
//		ELIGIBLE_FOR_AFUDC, AFUDC_TYPE_ID, CWIP_GL_ACCOUNT, EXPENSE_GL_ACCOUNT,
//		NON_UNITIZED_GL_ACCOUNT, UNITIZED_GL_ACCOUNT, ELIGIBLE_FOR_CPI, SALVAGE_GL_ACCOUNT,
//		REMOVAL_GL_ACCOUNT, RETIREMENT_GL_ACCOUNT, JOBBING_GL_ACCOUNT, FUNC_CLASS_ID,
//		UNITIZE_BY_ACCOUNT, TOLERANCE_ID, CLOSING_PATTERN_ID, base_year,
//		AFUDC_START_DATE, AFUDC_STOP_DATE, ACCRUAL_TYPE_ID, CR_DERIVER_TYPE_DETAIL,
//		CR_DERIVER_TYPE_ESTIMATE, ESCALATION_ID, EST_UNIT_ITEM_OPTION, WORK_TYPE_ID,
//		WO_EST_HIERARCHY_ID, rwip_type_id
//		)
//	select a.work_order_id, b.REIMBURSABLE_TYPE_ID, nvl(a.agreement_id, b.agreemnt_id), a.work_order_grp_id,
//		b.estimating_option_id, b.closing_option_Id, b.ALLOC_METHOD_TYPE_ID, null retirement_type,
//		b.afc_elig_indicator, b.default_afc_type_id, b.CWIP_GL_ACCOUNT, b.EXPENSE_GL_ACCOUNT,
//		b.NON_UNITIZED_GL_ACCOUNT, b.UNITIZED_GL_ACCOUNT, b.cpi_elig_indicator, b.SALVAGE_GL_ACCOUNT,
//		b.REMOVAL_GL_ACCOUNT, b.RETIREMENT_GL_ACCOUNT, b.JOBBING_GL_ACCOUNT, b.FUNC_CLASS_ID,
//		b.UNITIZE_BY_ACCOUNT, b.TOLERANCE_ID, b.CLOSING_PATTERN_ID, a.base_year,
//		a.AFUDC_START_DATE, a.AFUDC_STOP_DATE, b.ACCRUAL_TYPE_ID, b.CR_DERIVER_TYPE_DETAIL,
//		b.CR_DERIVER_TYPE_ESTIMATE, b.ESCALATION_ID, b.EST_UNIT_ITEM_OPTION, b.WORK_TYPE_ID,
//		b.WO_EST_HIERARCHY_ID, b.rwip_type_id
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_INITIATOR
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Initiator")
//	
//	// Insert into work_order_initiator
//	insert into work_order_initiator (
//		work_order_id, users, initiation_date, responsibility_type,
//		proj_mgr, plant_accountant, contract_adm, engineer, other
//		)
//	select a.work_order_id, nvl(a.initiator,user), nvl(a.initiation_date,sysdate), 'prepare',
//		project_manager, plant_accountant, contract_admin, engineer, other_contact
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_APPROVAL
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Approval")
//	
//	// Insert into work_order_approval
//	insert into work_order_approval (
//		work_order_id, revision, budget_version_id, approval_type_id,
//		est_start_date, est_complete_date, est_in_service_date, approval_status_id, rejected
//		)
//	select a.work_order_id, 1, null budget_version_id, nvl(a.approval_type_id, b.approval_type_id),
//		nvl(a.est_start_date,sysdate), nvl(a.est_complete_date,sysdate), nvl(a.est_in_service_date,nvl(a.est_complete_date,sysdate)), decode(a.wo_status_id,1,1,3), decode(a.wo_status_id,1,2,null)
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORKFLOW
//	//
//	//--------------------------------------------------------------------------------
//	if workflow_appr = 'yes' then
//		uf_msgs("Inserting Workflow")
//		
//		// Insert into workflow
//		insert into workflow (
//			workflow_id, workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
//			approval_amount, approval_status_id, use_limits, sql_approval_amount
//			)
//		select pwrplant1.nextval, c.workflow_type_id, c.description, 'fp_approval', a.work_order_id, 1,
//			0 approval_amount, decode(a.wo_status_id,1,1,3) approval_status_id, c.use_limits, c.sql_approval_amount
//		from wo_interface_staging a, work_order_type b,workflow_type c
//		where a.work_order_type_id = b.work_order_type_id
//		and nvl(a.approval_type_id, b.APPROVAL_TYPE_ID) = c.workflow_type_id
//		and a.status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and a.funding_wo_indicator = 1
//		and a.action = 'I'
//		and a.error_message is null
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT BUDGET_VERSION_FUND_PROJ
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Budget Version Fund Proj")
//	
//	// Insert into budget_version_fund_proj
//	insert into budget_version_fund_proj (
//		work_order_id, revision, budget_version_id, active, date_added, person_added, date_active, has_respread
//		)
//	select a.work_order_id, 1, nvl(a.budget_version_id, :bv_id), 1, sysdate, user, sysdate, 0
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT FUNDING_JUSTIFICATION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Funding Justification")
//	
//	// Insert into funding_justification
//	insert into funding_justification (
//		work_order_id, reason_for_work, background, future_projects, alternatives, financial_analysis
//		)
//	select a.work_order_id,  a.reason_for_work, a.background, a.future_projects, a.alternatives, a.financial_analysis
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// UPDATING DEFAULT APPROVERS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Updating Default Approvers")
//	
//	// Update default approvers
//	// Only for unapproved revisions
//	select count(*) into :checkers
//	from approval_defaults
//	where work_order_type_id in (
//		select a.work_order_type_id
//		from wo_interface_staging a
//		where a.status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and a.funding_wo_indicator = 1
//		and a.action = 'I'
//		and a.error_message is null
//		and a.wo_status_id = 1
//		)
//	;
//	
//	if checkers > 0 then
//		update work_order_approval a
//		set (authorizer1, authorizer2, authorizer3, authorizer4, authorizer5, authorizer6, authorizer7, authorizer8, authorizer9, authorizer10) = (
//			select auth_level1, auth_level2, auth_level3, auth_level4, auth_level5, auth_level6, auth_level7, auth_level8, auth_level9, auth_level10
//			from approval_defaults q, work_order_control c
//			where c.work_order_type_Id = q.work_order_type_id
//			and c.work_order_id = a.work_order_id
//			)
//		where a.work_order_id in (
//			select b.work_order_id
//			from wo_interface_staging b
//			where b.status = 1
//			and nvl(b.batch_id,'null') = :a_batch
//			and (b.company_id = :a_company or :a_company = -1)
//			and b.funding_wo_indicator = 1
//			and b.action = 'I'
//			and b.error_message is null
//			and b.wo_status_id = 1
//			)
//		and a.revision = 1	
//		;
//		if uf_check_sql() = -1 then return -1
//		
//		insert into wo_approval_multiple (
//			work_order_id, revision, approval_type_id, auth_level,
//			users, category_id, required, requiring_user, auth_level_id
//			)
//		select a.work_order_id, 1 revision, a.approval_type_id, c.auth_level,
//			c.users, c.category_id, c.required, user, d.auth_level_id
//		from work_order_approval a, wo_interface_staging b, wo_approval_multiple_default c,
//			(	select approval_type_id, 1 auth_level_id, auth_level1 auth_level from approval union all
//				select approval_type_id, 2 auth_level_id, auth_level2 auth_level from approval union all
//				select approval_type_id, 3 auth_level_id, auth_level3 auth_level from approval union all
//				select approval_type_id, 4 auth_level_id, auth_level4 auth_level from approval union all
//				select approval_type_id, 5 auth_level_id, auth_level5 auth_level from approval union all
//				select approval_type_id, 6 auth_level_id, auth_level6 auth_level from approval union all
//				select approval_type_id, 7 auth_level_id, auth_level7 auth_level from approval union all
//				select approval_type_id, 8 auth_level_id, auth_level8 auth_level from approval union all
//				select approval_type_id, 9 auth_level_id, auth_level9 auth_level from approval union all
//				select approval_type_id, 10 auth_level_id, auth_level10 auth_level from approval
//			) d
//		where a.work_order_id = b.work_order_id
//		and a.approval_type_id = c.approval_type_id
//		and c.approval_type_id = d.approval_type_id
//		and lower(trim(c.auth_level)) = lower(trim(d.auth_level))
//		and b.status = 1
//		and nvl(b.batch_id,'null') = :a_batch
//		and (b.company_id = :a_company or :a_company = -1)
//		and b.funding_wo_indicator = 1
//		and b.action = 'I'
//		and b.error_message is null
//		and b.wo_status_id = 1
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// CLASS CODES
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Class Codes")
//	
//	// Create new class codes
//	//rtn = uf_create_cc() -- I think we should set these up if we know we are going to need them
//	//if rtn = -1 then
//	//	return -1
//	//end if
//	
//	for i = 1 to i_max_cc
//		uf_msgs( " Class Code Insert "+string(i))
//		cc_sqls_wocc = &
//			'insert into work_order_class_code (work_order_id, class_code_id, "VALUE") '+&
//			'select a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
//			'from wo_interface_staging a, class_code cc '+&
//			'where a.status = 1 '+&
//			"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//			'and a.funding_wo_indicator = 1 '+&
//			'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//			"and a.action = 'I' "+&
//			"and a.error_message is null "+&
//			'and a.class_code'+string(i)+' is not null '+&
//			'and class_value'+string(i)+' is not null '+&
//			'and cc.description = a.class_code'+string(i)+' '+&
//			'and cc.fp_indicator = 1 '
//		execute immediate :cc_sqls_wocc;
//		if uf_check_sql() = -1 then return -1
//		
//		cc_sqls_bcc = &
//			'insert into budget_class_code (budget_id, class_code_id, value, budget_version_id)  '+&
//			'select a.budget_id, c.class_code_id, max(a.class_value'+string(i)+'), nvl(a.budget_version_id,'+string(bv_id)+') '+&
//			'from wo_interface_staging a, class_code c '+&
//			'where a.status = 1 '+&
//			"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//			'and a.funding_wo_indicator = 1 '+&
//			'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//			"and a.action = 'I' "+&
//			"and a.error_message is null "+&
//			'and a.class_code'+string(i)+' is not null '+&
//			'and class_value'+string(i)+' is not null '+&
//			'and a.class_code'+string(i)+' = c.description '+&
//			'and c.budget_indicator = 1 '+&
//			'and (a.budget_id, c.class_code_id, a.budget_version_id) in ( '+&
//			'	select a.budget_id, c.class_code_id, nvl(a.budget_version_id,'+string(bv_id)+') '+&
//			'	from wo_interface_staging a, class_code c'+&
//			'	where a.status = 1 '+&
//			"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//			'	and a.funding_wo_indicator = 1 '+&
//			'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//			"	and a.action = 'I' "+&
//			"	and a.error_message is null "+&
//			'	and a.class_code'+string(i)+' is not null '+&
//			'	and class_value'+string(i)+' is not null '+&
//			'	and a.class_code'+string(i)+' = c.description '+&
//			'	and c.budget_indicator = 1 '+&
//			'	minus '+&
//			'	select budget_id, class_code_id, budget_version_id '+&
//			'	from budget_class_code '+&
//			'	) '+&
//			'group by a.budget_id, c.class_code_id, nvl(a.budget_version_id,'+string(bv_id)+') '
//		execute immediate :cc_sqls_bcc;
//		if uf_check_sql() = -1 then return -1
//	next
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// CLASS CODE DEFAULTS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Class Code Defaults")
//	
//	// Create new class codes
//	uf_msgs( " Class Code Defaults ")
//	insert into work_order_class_code (work_order_id, class_code_id, value)
//	select a.work_order_id, b.class_code_id, b.value
//	from wo_interface_staging a, wo_type_class_code_default b
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	and a.work_order_type_id = b.work_order_type_id
//	and not exists (
//		select 1 from work_order_class_code c
//		where c.work_order_id = a.work_order_id
//		and c.class_code_id = b.class_code_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// MARK RECORDS AS PROCESSED
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Interface Status Update")
//	
//	// Update status to indicate it has been processed
//	update wo_interface_staging a
//	set status = 2
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// UPDATES
//	//
//	//--------------------------------------------------------------------------------
//	updates:
//	
//	uf_msgs("------------------------------- ")
//	uf_msgs("Updating Existing Funding Projects")
//	
//	select count(*)
//	into :update_counts
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	and a.error_message is null
//	;
//	
//	if isnull(update_counts) then update_counts = 0
//	if update_counts = 0 then
//		uf_msgs("No Existing Funding Projects to Process")
//	end if
//	
//	if no_updates or update_counts = 0 then
//		// Do not run updates
//	else
//		rtn = uf_update_fps(a_company, a_batch)
//		if rtn = -1 then
//			return -1
//		end if
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//next
//
//
////--------------------------------------------------------------------------------
////
//// CLASS_CODE_VALUES
////
////--------------------------------------------------------------------------------
//uf_msgs("Backfilling Class Code Values")
//insert into class_code_values (class_code_id, value)
//select class_code_id, value
//from work_order_class_code
//minus
//select class_code_id, value
//from class_code_values
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// ARCHIVE RECORDS
////
////--------------------------------------------------------------------------------
//uf_msgs("Archiving Records")
//
//// Insert into archive table
//insert into wo_interface_staging_arc (
//	WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,
//	FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,
//	EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,
//	IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,
//	BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,APPROVAL_TYPE_ID,AFUDC_STOP_DATE,
//	AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,
//	WO_STATUS_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,
//	CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,
//	CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,
//	CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,
//	CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,CLASS_VALUE23,
//	CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,
//	CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,RUN_DATE,
//	batch_id, REIMBURSABLE_TYPE_ID, ext_company, ext_work_order_type, ext_major_location, budget_number, budget_company_id, ext_department,
//	ext_work_order_group, ext_asset_location, seq_id, USER_ID, TIME_STAMP, agreement_id, error_message, old_wo_status_id, ext_work_order_status
//	)
//select WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,
//	FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,
//	EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,
//	IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,
//	BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,APPROVAL_TYPE_ID,AFUDC_STOP_DATE,
//	AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,
//	WO_STATUS_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,
//	CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,
//	CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,
//	CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,
//	CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,CLASS_VALUE23,
//	CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,
//	CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,sysdate,
//	batch_id, REIMBURSABLE_TYPE_ID, ext_company, ext_work_order_type, ext_major_location, budget_number, budget_company_id, ext_department,
//	ext_work_order_group, ext_asset_location, seq_id, USER_ID, TIME_STAMP, agreement_id, error_message, old_wo_status_id, ext_work_order_status
//from wo_interface_staging a
//where nvl(a.status,1) = 2
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 1
//and a.error_message is null
//;
//if uf_check_sql() = -1 then return -1
//
//// Delete Archived Data
//delete from wo_interface_staging a
//where nvl(a.status,1) = 2
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 1
//and a.error_message is null
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// COMMIT
////
////--------------------------------------------------------------------------------
//uf_msgs("Committing records")
//
//// Commit here
//commit;
//
//uf_msgs("Finished FP Loading - Company Id = "+string(a_company))
//uf_msgs("------------------------------- ")
//uf_msgs("------------------------------- ")
//
//
//return 1
end function

public function longlong uf_check_sql ();if sqlca.sqlcode < 0 then 
	uf_msgs("SQLERROR: "+ sqlca.sqlerrtext)
	return -1
end if
uf_msgs("  Rows Effected: " + string(sqlca.sqlnrows))

return 1
end function

public function integer uf_update_fps (longlong a_company, string a_batch);return 1

////***************************************************************************************** 
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////
////   Subsystem:   system
////
////   Event    :   uo_wo_interface.uf_update_fps
////
////   Purpose  :   Creates Work Orders from pre-staged data in the table wo_interface_staging.
////					Assumes that the Funding Project Number, Work_order_type_id and desciption are not null in the wo_interface_staging table.
////                    Updates valid entrys to status of 2.
////                    This functions is automatically called through the function uf_create_fps()
////                     
//// 					
////   DATE                           NAME                            REVISION                     CHANGES
////  --------      --------     -----------   --------------------------------------------- 
////  07-23-2008      PowerPlan           Version 1.0   Initial Version
////
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////*****************************************************************************************
//longlong checkers, i
//string cc_insert, cc_update
//
//if isnull(a_batch) then a_batch = 'null'
//
//sqlca.analyze_table('wo_interface_staging')
//
////--------------------------------------------------------------------------------
////
//// WORK ORDER CONTROL
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Work Order Control")
//update work_order_control z
//set (BUS_SEGMENT_ID, COMPANY_ID, BUDGET_ID, WO_STATUS_ID,
//	REASON_CD_ID, WORK_ORDER_TYPE_ID, FUNDING_WO_ID, MAJOR_LOCATION_ID,
//	ASSET_LOCATION_ID, DESCRIPTION, LONG_DESCRIPTION, EST_START_DATE,
//	EST_COMPLETE_DATE, NOTES, DEPARTMENT_ID, EST_IN_SERVICE_DATE,
//	COMPLETION_DATE, IN_SERVICE_DATE, CLOSE_DATE, EST_ANNUAL_REV,
//	EXTERNAL_WO_NUMBER, OUT_OF_SERVICE_DATE, SUSPENDED_DATE, WO_APPROVAL_GROUP_ID
//	) = (
//	select nvl(a.BUS_SEGMENT_ID,z.bus_segment_Id), nvl(a.COMPANY_ID,z.company_id), nvl(a.BUDGET_ID,z.budget_id), nvl(a.wo_status_id,z.wo_status_id),
//		nvl(a.REASON_CD_ID,z.reason_cd_id), nvl(a.WORK_ORDER_TYPE_ID,z.work_order_type_id), nvl(a.FUNDING_WO_ID,z.funding_wo_id), nvl(a.MAJOR_LOCATION_ID,z.major_location_id),
//		nvl(a.ASSET_LOCATION_ID,z.asset_location_id), nvl(a.DESCRIPTION,z.description), nvl(a.LONG_DESCRIPTION,z.long_description), nvl(a.EST_START_DATE,z.est_start_date),
//		nvl(a.EST_COMPLETE_DATE,z.EST_COMPLETE_DATE), nvl(a.NOTES,z.NOTES), nvl(a.DEPARTMENT_ID,z.DEPARTMENT_ID), nvl(a.EST_IN_SERVICE_DATE,z.EST_IN_SERVICE_DATE),
//		decode(a.wo_status_id, null, z.COMPLETION_DATE, a.COMPLETION_DATE), decode(a.wo_status_id, null, z.IN_SERVICE_DATE, a.IN_SERVICE_DATE), decode(a.wo_status_id, null, z.CLOSE_DATE, a.CLOSE_DATE), nvl(a.EST_ANNUAL_REV, z.EST_ANNUAL_REV),
//		nvl(a.EXTERNAL_WO_NUMBER, z.EXTERNAL_WO_NUMBER), nvl(a.OUT_OF_SERVICE_DATE, z.OUT_OF_SERVICE_DATE), decode(a.wo_status_id, null, z.SUSPENDED_DATE, a.SUSPENDED_DATE), nvl(a.WO_APPROVAL_GROUP_ID, z.WO_APPROVAL_GROUP_ID)
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	and a.error_message is null
//	and a.work_order_id = z.work_order_id
//	)
//where z.work_order_id in (
//	select work_order_id
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	and a.error_message is null
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// WORK ORDER ACCOUNT
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Work Order Account")
//update work_order_account z
//set (WORK_ORDER_GRP_ID, AFUDC_START_DATE, AFUDC_STOP_DATE, BASE_YEAR) = (
//	select nvl(a.work_order_grp_id, z.work_order_grp_id),
//		nvl(a.AFUDC_START_DATE, z.AFUDC_START_DATE),
//		nvl(a.AFUDC_STOP_DATE, z.AFUDC_STOP_DATE),
//		nvl(a.BASE_YEAR, z.BASE_YEAR)
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	and a.error_message is null
//	and a.work_order_id = z.work_order_id
//	)
//where z.work_order_id in (
//	select work_order_id
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	and a.error_message is null
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// WORK_ORDER_INITIATOR
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Work Order Initiator")
//
//// Insert into work_order_initiator
//update work_order_initiator z
//set (users, initiation_date, proj_mgr, plant_accountant, contract_adm, engineer, other) = (
//	select nvl(a.initiator,z.users), nvl(a.initiation_date,z.initiation_date), nvl(a.project_manager,z.proj_mgr), nvl(a.plant_accountant,z.plant_accountant), nvl(a.contract_admin,z.contract_adm), nvl(a.engineer,z.engineer), nvl(a.other_contact,z.other)
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	and a.error_message is null
//	and a.work_order_id = z.work_order_id
//	)
//where z.work_order_id in (
//	select work_order_id
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 1
//	and a.action = 'U'
//	and a.error_message is null
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// CLASS CODES
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Class Codes")
//for i = 1 to i_max_cc
//	cc_insert = &
//		'insert into work_order_class_code (work_order_id, class_code_id, value) '+&
//		'select a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
//		'from wo_interface_staging a, class_code cc '+&
//		'where a.status = 1 '+&
//		"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//		'and a.funding_wo_indicator = 1 '+&
//		'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//		"and a.action = 'U' "+&
//		"and a.error_message is null "+&
//		'and a.class_code'+string(i)+' is not null '+&
//		'and a.class_value'+string(i)+' is not null '+&
//		'and trim(a.class_code'+string(i)+') = trim(cc.description) '+&
//		'and cc.fp_indicator = 1 '+&
//		'and not exists ( '+&
//		'	select 1 from work_order_class_code c '+&
//		'	where c.work_order_id = a.work_order_id '+&
//		'	and c.class_code_id = cc.class_code_id '+&
//		'	) '
//	execute immediate :cc_insert;
//	if uf_check_sql() = -1 then return -1
//	
//	cc_update = &
//		"update work_order_class_code z "+&
//		"set value = ( "+&
//		"	select a.class_value"+string(i)+" "+&
//		"	from wo_interface_staging a, class_code cc "+&
//		'	where a.status = 1 '+&
//		"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//		'	and a.funding_wo_indicator = 1 '+&
//		'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//		"	and a.action = 'U' "+&
//		"	and a.error_message is null "+&
//		"	and a.class_code"+string(i)+" is not null "+&
//		"	and a.class_value"+string(i)+" is not null "+&
//		"	and z.work_order_id = a.work_order_id "+&
//		"	and z.class_code_id = cc.class_code_id "+&
//		"	and trim(cc.description) = trim(a.class_code"+string(i)+") "+&
//		"	and cc.fp_indicator = 1 "+&
//		"	) "+&
//		"where (work_order_id, class_code_id) in ( "+&
//		"	select work_order_id, cc.class_code_id "+&
//		"	from wo_interface_staging a, class_code cc "+&
//		'	where a.status = 1 '+&
//		"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//		'	and a.funding_wo_indicator = 1 '+&
//		'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//		"	and a.action = 'U' "+&
//		"	and a.error_message is null "+&
//		"	and a.class_code"+string(i)+" is not null "+&
//		"	and a.class_value"+string(i)+" is not null "+&
//		"	and trim(cc.description) = trim(a.class_code"+string(i)+") "+&
//		"	and cc.fp_indicator = 1 "+&
//		"	) "
//	execute immediate :cc_update;
//	if uf_check_sql() = -1 then return -1
//next
//
//
////--------------------------------------------------------------------------------
////
//// MARK RECORDS AS PROCESSED
////
////--------------------------------------------------------------------------------
//uf_msgs("Interface Status Update")
//
//// Update status to indicate it has been processed
//update wo_interface_staging a
//set status = 2
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 1
//and a.action = 'U'
//and a.error_message is null
//;
//if uf_check_sql() = -1 then return -1
//
//
//return 1
end function

public function integer uf_create_wos (longlong a_company, string a_batch);longlong rtn

rtn = uf_create_wos_fps(a_company, a_batch, 0)

return rtn


////***************************************************************************************** 
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////
////   Subsystem:   system
////
////   Event    :   uo_wo_interface.uf_create_wos
////
////   Purpose  :   Creates Work Orders from pre-staged data in the table wo_interface_staging.
////					Assumes that the Funding Project Number, Work_order_type_id and desciption are not null in the wo_interface_staging table.
////                    Updates valid entrys to status of 2.
////                     
//// 					
////   DATE                           NAME                            REVISION                     CHANGES
////  --------      --------     -----------   --------------------------------------------- 
////  07-23-2008      PowerPlan           Version 1.0   Initial Version
////
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////*****************************************************************************************
//longlong checkers, counts, i, rtn, update_counts, num_batches, ii
//any results[]
//string workflow_appr, cc_sqls_wocc, cc_sqls_bcc
//
//if isnull(a_batch) then a_batch = 'null'
//
//uf_msgs("------------------------------- ")
//uf_msgs("------------------------------- ")
//uf_msgs("Begin WO Loading - Company Id = "+string(a_company))
//
//
////--------------------------------------------------------------------------------
////
//// PROCESSING STATUS
////
////--------------------------------------------------------------------------------
//uf_msgs("Setting Status")
//update wo_interface_staging a
//set status = 1,
//	funding_wo_indicator = nvl(a.funding_wo_indicator, 0),
//	error_message = null
//where nvl(status,1) <> 2
//and nvl(batch_id,'null') = :a_batch
//and funding_wo_indicator = 0
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// COMPANY
////  - always required
////
////--------------------------------------------------------------------------------
//uf_msgs("Validating Company")
//
//////
//////	If an external value is provided, try to map on it and error out if it cannot be mapped
//////
//
//// Map on gl_company_no
//update wo_interface_staging a
//set a.company_id = (
//	select p.company_id
//	from company p
//	where p.gl_company_no = a.ext_company
//	)
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 0
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//// Map on company description
//update wo_interface_staging a
//set a.company_id = (
//	select p.company_id
//	from company p
//	where p.description = a.ext_company
//	)
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 0
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//// Map on company_id
//update wo_interface_staging a
//set a.company_id = (
//	select p.company_id
//	from company p
//	where to_char(p.company_id) = a.ext_company
//	)
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 0
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//// Flag records where ext_company could not be mapped
//update wo_interface_staging a
//set a.error_message = a.error_message || 'WO - cannot derive company_id; '
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 0
//and a.company_id is null
//and a.ext_company is not null
//;
//if uf_check_sql() = -1 then return -1
//
//////
//////	No external value was provided
//////
//
//// Flag records missing company_id as errors
//update wo_interface_staging a
//set a.error_message = a.error_message || 'WO - company_id is required; '
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and a.funding_wo_indicator = 0
//and a.company_id is null
//and a.ext_company is null
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// WORK ORDER NUMBER
////  - always required
////
////--------------------------------------------------------------------------------
//uf_msgs("Validating Work Order Number")
//
//// Flag records missing work_order_number as errors
//update wo_interface_staging a
//set a.error_message = a.error_message || 'WO - work_order_number is required; '
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 0
//and a.work_order_number is null
//;
//if uf_check_sql() = -1 then return -1
//// Field is not null...this should never find anything
//
//
////--------------------------------------------------------------------------------
////
//// VALIDATION
////   - must have a valid COMPANY_ID and WORK_ORDER_NUMBER
////
////--------------------------------------------------------------------------------
//uf_msgs("Flagging Errors (missing Company or Work Order Number)")
//
//// Flag records that already have errors as unable to continue processing (either invalid company_id or work_order_number)
//update wo_interface_staging a
//set a.status = -11
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
//and funding_wo_indicator = 0
//and a.error_message is not null
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// DETERMINE THE NUMBER OF BATCHES
////   - Loop over batches of work orders
////
////--------------------------------------------------------------------------------
//uf_msgs("Identifying Process Batches")
//
//// Determine the number of loops needed to process all work orders in sequence
//select max(batches)
//into :num_batches
//from (
//	select work_order_number, company_id, count(distinct nvl(seq_id,0)) batches
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	group by work_order_number, company_id
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//uf_msgs("Updating status to Pending")
//
//// Update records that have not yet been processed to status = 3 (pending
//update wo_interface_staging a
//set a.status = 3 // pending
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 0
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// LOOP OVER THE BATCHES
////
////--------------------------------------------------------------------------------
//for ii = 1 to num_batches
//	uf_msgs(" ")
//	uf_msgs("***** Batch "+string(ii)+" of "+string(num_batches)+" *****")
//	uf_msgs(" ")
//	
//	uf_msgs("Updating status to In-Process")
//	update wo_interface_staging a
//	set a.status = 1 // in-process
//	where a.status = 3
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and nvl(seq_id,0) in (
//		select min(nvl(seq_id,0))
//		from wo_interface_staging b
//		where b.status = 3
//		and nvl(b.batch_id,'null') = :a_batch
//		and (b.company_id = :a_company or :a_company = -1)
//		and b.funding_wo_indicator = 0
//		and b.work_order_number = a.work_order_number
//		and b.company_id = a.company_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DETERMINE ACTION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Setting Action")
//	
//	// Reset action code
//	update wo_interface_staging a
//	set a.action = null,
//		a.work_order_id = null
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Identify "update" records
//	update wo_interface_staging a
//	set a.action = 'U',
//		(a.work_order_id, a.old_wo_status_id) = (
//			select b.work_order_id, b.wo_status_id
//			from work_order_control b 
//			where b.work_order_number = a.work_order_number
//			and b.company_id = a.company_id
//			and b.funding_wo_indicator = a.funding_wo_indicator
//			)
//	where exists (
//		select 1 from work_order_control b 
//		where b.work_order_number = a.work_order_number
//		and b.company_id = a.company_id
//		and b.funding_wo_indicator = a.funding_wo_indicator
//		)
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Identify "insert" records
//	update wo_interface_staging a
//	set action = 'I',
//		work_order_id = pwrplant1.nextval,
//		old_wo_status_id = null
//	where nvl(action,'I') <> 'U'
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	// This should never find anything anyways because we are processing in batches
//	////--------------------------------------------------------------------------------
//	////
//	//// SETTING OLD RECORDS AS PROCESSED BASED ON SEQUENCE (SEQ_ID)
//	////   If a newer record for the same work order already exists, mark the
//	////   old one as processed, and continue processing the latest one
//	////
//	////--------------------------------------------------------------------------------
//	//uf_msgs("Checking Sequence")
//	//update wo_interface_staging a
//	//set a.status = 2
//	//where a.status = 1
//	//and nvl(a.batch_id,'null') = :a_batch
//	//and (a.company_id = :a_company or :a_company = -1)
//	//and funding_wo_indicator = 0
//	//and nvl(a.seq_id,0) < (
//	//	select nvl(max(b.seq_id),0)
//	//	from wo_interface_staging b
//	//	where b.status = 1
//	//	and nvl(b.batch_id,'null') = :a_batch
//	//	and (b.company_id = :a_company or :a_company = -1)
//	//	and b.funding_wo_indicator = 0
//	//	and b.work_order_id = a.work_order_id
//	//	)
//	//;
//	//if uf_check_sql() = -1 then return -1
//	
//	// Flag duplicate records where the same work order for the same company exists more than once
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - duplicate records exist for this work order with identical/blank seq_ids; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_id in (
//		select c.work_order_id
//		from wo_interface_staging c
//		where c.status = 1
//		and nvl(c.batch_id,'null') = :a_batch
//		and (c.company_id = :a_company or :a_company = -1)
//		and c.funding_wo_indicator = 0
//		group by c.work_order_id
//		having count(*) > 1
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COUNT RECORDS TO PROCESS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Checking for records to process")
//	
//	// Look for any records to process whether Insert or Update
//	select count(*) into :counts
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	;
//	
//	if counts = 0 then
//		uf_msgs("0 records to process")
//		uf_msgs("-------------------------------")
//		uf_msgs("-------------------------------")
//		return 1
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DESCRIPTION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Validating Work Order Description")
//	
//	// Flag records missing work_order_number as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - description is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.description is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// WORK ORDER TYPE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Work Order Type")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external_work_order_type
//	update wo_interface_staging a
//	set a.work_order_type_id = (
//		select p.work_order_type_id
//		from work_order_type p
//		where p.external_work_order_type = a.ext_work_order_type
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work_order_type description
//	update wo_interface_staging a
//	set a.work_order_type_id = (
//		select p.work_order_type_id
//		from work_order_type p
//		where p.description = a.ext_work_order_type
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work_order_type_id
//	update wo_interface_staging a
//	set a.work_order_type_id = (
//		select p.work_order_type_id
//		from work_order_type p
//		where to_char(p.work_order_type_id) = a.ext_work_order_type
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_work_order_type could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive work_order_type_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided
//	////
//	
//	// Flag records missing work_order_type_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - work_order_type_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_type_id is null
//	and a.ext_work_order_type is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// FUNDING PROJECT
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Funding Project")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on work_order_number
//	update wo_interface_staging a
//	set a.funding_wo_id = (
//		select p.work_order_id
//		from work_order_control p
//		where p.funding_wo_indicator = 1
//		and p.work_order_number = a.funding_proj_number
//		and p.company_id = a.company_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.funding_wo_id is null
//	and a.funding_proj_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on external_wo_number
//	update wo_interface_staging a
//	set a.funding_wo_id = (
//		select p.work_order_id
//		from work_order_control p
//		where p.funding_wo_indicator = 1
//		and p.external_wo_number = a.funding_proj_number
//		and p.company_id = a.company_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.funding_wo_id is null
//	and a.funding_proj_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on description
//	update wo_interface_staging a
//	set a.funding_wo_id = (
//		select p.work_order_id
//		from work_order_control p
//		where p.funding_wo_indicator = 1
//		and p.description = a.funding_proj_number
//		and p.company_id = a.company_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.funding_wo_id is null
//	and a.funding_proj_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work_order_id
//	update wo_interface_staging a
//	set a.funding_wo_id = (
//		select p.work_order_id
//		from work_order_control p
//		where p.funding_wo_indicator = 1
//		and to_char(p.work_order_id) = a.funding_proj_number
//		and p.company_id = a.company_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.funding_wo_id is null
//	and a.funding_proj_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where funding_proj_number could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive funding_wo_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.funding_wo_id is null
//	and a.funding_proj_number is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided
//	////
//	
//	// Flag records missing funding_wo_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	//set a.status = -12
//	set a.error_message = a.error_message || 'WO - funding_wo_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.funding_wo_id is null
//	and a.funding_proj_number is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// BUDGET ITEM
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Budget Item")
//	
//	////
//	//// Whether inserting or updating, the budget_id always comes from the funding project
//	////
//	
//	// Map on funding_wo_id
//	update wo_interface_staging a
//	set a.budget_id = (
//		select p.budget_id
//		from work_order_control p
//		where p.work_order_id = a.funding_wo_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	//and a.budget_id is null -- must come from the Funding Project
//	and a.funding_wo_id is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Budget Item must come from the Funding Project, so if the funding project is null (i.e. not being updated), then null out the budget item so that it is not updated
//	update wo_interface_staging a
//	set a.budget_id = null
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.funding_wo_id is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records missing budget_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - budget_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.budget_id is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// BUSINESS SEGMENT
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Business Segment")
//	
//	////
//	////	When updating, bus_segment_id must be null - cannot be updated
//	////
//	
//	// Null out bus_segment_id...cannot be changed on a work order - ONLY FOR UPDATES
//	update wo_interface_staging a
//	set a.bus_segment_id = null
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	When inserting, bus_segment_id must come from the work_order_type
//	////
//	
//	// Map on work order type - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.bus_segment_id = (
//		select b.bus_segment_id
//		from work_order_type b
//		where b.work_order_type_id = a.work_order_type_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	//and a.bus_segment_id is null --must always come from the Work Order Type
//	and a.work_order_type_id is not null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records missing bus_segment_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive bus_segment_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.bus_segment_id is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// ASSET LOCATION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Asset Location")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on ext_asset_location
//	update wo_interface_staging a
//	set a.asset_location_id = (
//		select b.asset_location_id
//		from asset_location b
//		where b.ext_asset_location = a.ext_asset_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on asset location description
//	update wo_interface_staging a
//	set a.asset_location_id = (
//		select b.asset_location_id
//		from asset_location b
//		where b.long_description = a.ext_asset_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on asset_location_id
//	update wo_interface_staging a
//	set a.asset_location_id = (
//		select b.asset_location_id
//		from asset_location b
//		where to_char(b.asset_location_id) = a.ext_asset_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_asset_location could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive asset_location_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.asset_location_id is null
//	and a.ext_asset_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided, try to derive some other way
//	////
//	
//	// Map on funding project - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.asset_location_id = (
//		select b.asset_location_id
//		from work_order_control b
//		where b.work_order_id = a.funding_wo_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.asset_location_id is null
//	and a.funding_wo_id is not null
//	and a.ext_asset_location is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Obey system control that requires asset_location_id upon initiation - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - asset_location_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and exists (
//		select 1 from pp_system_control_companies p
//		where p.company_id = a.company_id
//		and lower(trim(p.control_name)) = lower('WOINIT - Require Location')
//		and lower(trim(p.control_value)) = 'yes'
//		)
//	and a.asset_location_id is null
//	and a.ext_asset_location is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// MAJOR LOCATION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Major Location")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external_location_id
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from major_location b
//		where b.external_location_id = a.ext_major_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on major location description
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from major_location b
//		where b.description = a.ext_major_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on major_location_id
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from major_location b
//		where to_char(b.major_location_id) = a.ext_major_location
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_major_location could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive major_location_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.major_location_id is null
//	and a.ext_major_location is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided, try to derive some other way
//	////
//	
//	// If we were able to derive an asset_location_id above, then map on asset_location_id 
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from asset_location b
//		where b.asset_location_id = a.asset_location_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.major_location_id is null
//	and a.asset_location_id is not null
//	and a.ext_major_location is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on funding project - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.major_location_id = (
//		select b.major_location_id
//		from work_order_control b
//		where b.work_order_id = a.funding_wo_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.major_location_id is null
//	and a.funding_wo_id is not null
//	and a.ext_major_location is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records missing major_location_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - major_location_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.major_location_id is null
//	and a.ext_major_location is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DEPARTMENT
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Department")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external_department_code
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from department b
//		where b.external_department_code = a.ext_department
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on department description
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from department b
//		where b.description = a.ext_department
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on department_id
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from department b
//		where to_char(b.department_id) = a.ext_department
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_department could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive department_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.department_id is null
//	and a.ext_department is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided, try to derive some other way
//	////
//	
//	// Map on funding project department - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.department_id = (
//		select b.department_id
//		from work_order_control b
//		where b.work_order_id = a.funding_wo_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.department_id is null
//	and a.funding_wo_id is not null
//	and a.ext_department is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records missing department_id as errors - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - department_id is required; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.department_id is null
//	and a.ext_department is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// WORK ORDER GROUP
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Work Order Group")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on work order group description
//	update wo_interface_staging a
//	set a.work_order_grp_id = (
//		select b.work_order_grp_id
//		from work_order_group b
//		where b.description = a.ext_work_order_group
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work_order_grp_id
//	update wo_interface_staging a
//	set a.work_order_grp_id = (
//		select b.work_order_grp_id
//		from work_order_group b
//		where to_char(b.work_order_grp_id) = a.ext_work_order_group
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_department could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive work_order_grp_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided, try to derive some other way
//	////
//	
//	// Map on work order type - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.work_order_grp_id = (
//		select b.work_order_grp_id
//		from wo_grp_wo_type b
//		where b.work_order_type_id = a.work_order_type_id
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.work_order_grp_id is null
//	and a.ext_work_order_group is null
//	and a.action = 'I'
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// WORK ORDER STATUS
//	//    MUST populate BOTH wo_status_id AND all corresponding status dates in order to process status updates
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Work Order Status")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external work order status
//	update wo_interface_staging a
//	set a.wo_status_id = (
//		select b.wo_status_id
//		from work_order_status b
//		where b.external_work_order_status = a.ext_work_order_status
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on work order status description
//	update wo_interface_staging a
//	set a.wo_status_id = (
//		select b.wo_status_id
//		from work_order_status b
//		where b.description = a.ext_work_order_status
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on wo status id
//	update wo_interface_staging a
//	set a.wo_status_id = (
//		select b.wo_status_id
//		from work_order_status b
//		where to_char(b.wo_status_id) = a.ext_work_order_status
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_work_order_status could not be mapped
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive wo_status_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id is null
//	and a.ext_work_order_status is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	If wo_status_id is provided, that means we are updating the status
//	////		Validate that the appropriate dates are populated - ALL STATUS DATES MUST BE PROVIDED WHEN WO_STATUS_ID IS POPULATED, including null dates
//	////		Status dates include:
//	////			- in_service_date
//	////			- completion_date
//	////			- close_date
//	////			- suspended_date
//	////		Validate that the status is changing from a valid previous status
//	////
//	
//	// Flag records where wo_status_id and dates are out of sync - Cancelled
//	// - Cancelled can only happen for work orders in an open, in service, or completed status.  Status dates should agree with one of those statuses
//	// - Cannot Cancel from Unitized or Posted to CPR status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (Cancelled - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 8
//	and (
//		not (
//			(close_date is not null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = open*/ OR
//			(close_date is not null and completion_date is null and in_service_date is not null and suspended_date is null) /*dates = in service*/ OR
//			(close_date is not null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = complete*/
//			)
//		OR
//		nvl(old_wo_status_id,8) in (6,7)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Posted to CPR
//	// !!!!! Posted to CPR status is ONLY allowed when initiating new work orders, presumably for conversion purposes !!!!!
//	// !!!!! This logic does NOT allow changing the work order from any other status to Posted to CPR !!!!!
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (Posted to CPR - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 7
//	and (
//		not (close_date is not null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = posted to cpr*/
//		OR
//		nvl(old_wo_status_id,7) in (1,2,3,4,5,6,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Unitized
//	// !!!!! Unitized status is ONLY meaningful inside of PowerPlan workflow. This status cannot be interfaced for any reason !!!!!
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (Unitized - cannot process this status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 6
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Completed
//	// - Complete can only happen for work orders in an Open or In Service status...Initiated or Suspended work orders presumably came through the Open and In Service statuses
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (Completed - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 5
//	and (
//		not (close_date is null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = completed*/
//		OR
//		nvl(old_wo_status_id,5) in (6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - In Service
//	// - In Service can only happen for work orders in an Open or Completed status...Initiated or Suspended work orders presumably came through the Open status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (In Service - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 4
//	and (
//		not (close_date is null and completion_date is null and in_service_date is not null and suspended_date is null) /*dates = in service*/
//		OR
//		nvl(old_wo_status_id,4) in (6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Suspended
//	// - Suspended can only happen for work orders in an Initiated or Open status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (Suspended - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 3
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is not null) /*dates = initiated OR open*/
//		OR
//		nvl(old_wo_status_id,3) in (4,5,6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Open
//	// - Open can only happen for work orders in an Initiated or Suspended status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (Open - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 2
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = open*/
//		OR
//		nvl(old_wo_status_id,2) in (4,5,6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id and dates are out of sync - Initiated
//	// - Initiated can only happen for work orders in a Suspended status
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (Initiated - invalid dates or previous status); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id = 1
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = initiated*/
//		OR
//		nvl(old_wo_status_id,1) in (2,4,5,6,7,8)
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id is not a valid id
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - wo_status_id validation error (wo_status_id must be in (1,2,3,4,5,6,7,8)); '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id is not null
//	and a.wo_status_id not in (1,2,3,4,5,6,7,8)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No wo_status_id was provided
//	////
//	
//	// Flag records where wo_status_id is not populated but one or more dates are populated
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - one or more status dates are populated...this is not allowed unless populating wo_status_id with a valid status change; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.wo_status_id is null
//	and (
//		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = none*/
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map Open work orders based on auto-approved approval types - ONLY FOR INSERTS
//	workflow_appr = lower(trim(f_pp_system_control_company('WORKFLOW USER OBJECT', a_company)))
//	if workflow_appr = 'yes' then
//		update wo_interface_staging a
//		set wo_status_id = 2
//		where status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and funding_wo_indicator = 0
//		and exists (
//			select 1 from work_order_type b, workflow_type c
//			where b.work_order_type_id = a.work_order_type_id
//			and b.approval_type_id = c.workflow_type_id
//			and not exists (
//				select 1 from workflow_type_rule wtr
//				where wtr.workflow_type_id = c.workflow_type_id
//				)
//			)
//		and action = 'I'
//		and wo_status_id is null
//		and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
//		;
//		if uf_check_sql() = -1 then return -1
//	else
//		update wo_interface_staging a
//		set wo_status_id = 2
//		where status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and funding_wo_indicator = 0
//		and exists (
//			select 1 from work_order_type b, approval c
//			where b.work_order_type_id = a.work_order_type_id
//			and b.approval_type_id = c.approval_type_id
//			and auth_level1 is null
//			and auth_level2 is null
//			and auth_level3 is null
//			and auth_level4 is null
//			and auth_level5 is null
//			and auth_level6 is null
//			and auth_level7 is null
//			and auth_level8 is null
//			and auth_level9 is null
//			and auth_level10 is null
//			)
//		and action = 'I'
//		and wo_status_id is null
//		and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	// Map remaining work orders as Initiated - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set wo_status_id = 1
//	where status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and funding_wo_indicator = 0
//	and action = 'I'
//	and wo_status_id is null 
//	and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where wo_status_id could not be mapped - ONLY FOR INSERTS
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - cannot derive wo_status_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and action = 'I'
//	and a.wo_status_id is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// DATA MODEL VALIDATIONS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Validating Data Model Relationships")
//	
//	uf_msgs("   - Company (Work Order / Work Order Type)")
//	// Flag records where company_id does not match company_id on work order type
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - company_id mismatch with work order type company_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 0
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.work_order_type_id is not null
//	and a.company_id <> (
//		select nvl(p.company_id,a.company_id)
//		from work_order_control p
//		where p.work_order_id = a.work_order_type_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Company (Work Order / Funding Project)")
//	// Flag records where company_id does not match company_id on funding project
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - company_id mismatch with funding project company_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 0
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_id is not null
//	and a.company_id <> (
//		select p.company_id
//		from work_order_control p
//		where p.work_order_id = a.funding_wo_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Company / Bus Segment")
//	// Flag records where company_id / bus_segment_id relationship is not valid
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - invalid company_id / bus_segment_id relationship; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 0
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.bus_segment_id is not null
//	and not exists (
//		select 1 from company_bus_segment_control p
//		where p.company_id = a.company_id
//		and p.bus_segment_id = a.bus_segment_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Company / Major Location")
//	// Flag records where company_id / major_location_id relationship is not valid
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - invalid company_id / major_location_id relationship; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 0
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.major_location_id is not null
//	and not exists (
//		select 1 from company_major_location p
//		where p.company_id = a.company_id
//		and p.major_location_id = a.major_location_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Major Location / Asset Location")
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - invalid major_location_id / asset_location_id relationship; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 0
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.major_location_id is not null
//	and a.asset_location_id is not null
//	and not exists (
//		select 1 from asset_location p
//		where p.asset_location_id = a.asset_location_id
//		and p.major_location_id = a.major_location_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	uf_msgs("   - Budget Item (Work Order / Funding Project")
//	// Flag records where budget_id is not the same on the work order and funding project
//	update wo_interface_staging a
//	set a.error_message = a.error_message || 'WO - budget_id mismatch with funding project budget_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and a.funding_wo_indicator = 0
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.budget_id is not null
//	and a.funding_wo_id is not null
//	and not exists (
//		select 1 from work_order_control fp
//		where fp.work_order_id = a.funding_wo_id
//		and fp.budget_id = a.budget_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERTS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting New Work Orders")
//	
//	// Check for any new work orders to process
//	select count(*) into :counts
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	
//	if isnull(counts) or counts = 0 then
//		uf_msgs("No New Work Orders to Process")
//		goto updates
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_CONTROL
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Control")
//	
//	// Insert into work_order_control
//	insert into work_order_control (
//		WORK_ORDER_ID, WORK_ORDER_NUMBER, BUS_SEGMENT_ID, COMPANY_ID,
//		BUDGET_ID, WO_STATUS_ID, REASON_CD_ID, WORK_ORDER_TYPE_ID,
//		FUNDING_WO_ID, MAJOR_LOCATION_ID, ASSET_LOCATION_ID, CURRENT_REVISION,
//		DESCRIPTION, LONG_DESCRIPTION, FUNDING_WO_INDICATOR, EST_START_DATE,
//		EST_COMPLETE_DATE, LATE_CHG_WAIT_PERIOD, NOTES, DEPARTMENT_ID,
//		EST_IN_SERVICE_DATE, COMPLETION_DATE, IN_SERVICE_DATE, close_date,
//		EST_ANNUAL_REV, EXTERNAL_WO_NUMBER, OUT_OF_SERVICE_DATE, SUSPENDED_DATE,
//		WO_APPROVAL_GROUP_ID
//		)
//	select a.WORK_ORDER_ID, a.WORK_ORDER_NUMBER, a.BUS_SEGMENT_ID, a.COMPANY_ID,
//		a.BUDGET_ID, a.wo_status_id, a.REASON_CD_ID, a.WORK_ORDER_TYPE_ID,
//		a.FUNDING_WO_ID, a.MAJOR_LOCATION_ID, a.ASSET_LOCATION_ID, decode(a.wo_status_id,1,0,1) current_revision,
//		a.DESCRIPTION, a.LONG_DESCRIPTION, 0 FUNDING_WO_INDICATOR, nvl(a.EST_START_DATE,sysdate),
//		nvl(a.EST_COMPLETE_DATE,sysdate), b.LATE_CHG_WAIT_PERIOD, a.NOTES, a.DEPARTMENT_ID,
//		nvl(a.EST_IN_SERVICE_DATE,nvl(a.EST_COMPLETE_DATE,sysdate)), a.COMPLETION_DATE, a.IN_SERVICE_DATE, a.close_date,
//		a.EST_ANNUAL_REV, a.EXTERNAL_WO_NUMBER, a.OUT_OF_SERVICE_DATE, a.SUSPENDED_DATE,
//		a.WO_APPROVAL_GROUP_ID
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_ACCOUNT
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Account")
//	
//	// Insert into work_order_account
//	insert into work_order_account (
//		WORK_ORDER_ID, REIMBURSABLE_TYPE_ID, AGREEMNT_ID, WORK_ORDER_GRP_ID,
//		ESTIMATING_OPTION_ID, CLOSING_OPTION_ID, ALLOC_METHOD_TYPE_ID, RETIREMENT_TYPE,
//		ELIGIBLE_FOR_AFUDC, AFUDC_TYPE_ID, CWIP_GL_ACCOUNT, EXPENSE_GL_ACCOUNT,
//		NON_UNITIZED_GL_ACCOUNT, UNITIZED_GL_ACCOUNT, ELIGIBLE_FOR_CPI, SALVAGE_GL_ACCOUNT,
//		REMOVAL_GL_ACCOUNT, RETIREMENT_GL_ACCOUNT, JOBBING_GL_ACCOUNT, FUNC_CLASS_ID,
//		UNITIZE_BY_ACCOUNT, TOLERANCE_ID, CLOSING_PATTERN_ID, base_year,
//		AFUDC_START_DATE, AFUDC_STOP_DATE, ACCRUAL_TYPE_ID, CR_DERIVER_TYPE_DETAIL,
//		CR_DERIVER_TYPE_ESTIMATE, ESCALATION_ID, EST_UNIT_ITEM_OPTION, WORK_TYPE_ID,
//		WO_EST_HIERARCHY_ID, rwip_type_id
//		)
//	select a.work_order_id, b.REIMBURSABLE_TYPE_ID, nvl(a.agreement_id, b.agreemnt_id), a.work_order_grp_id,
//		b.estimating_option_id, b.closing_option_Id, b.ALLOC_METHOD_TYPE_ID, null retirement_type,
//		b.afc_elig_indicator, b.default_afc_type_id, b.CWIP_GL_ACCOUNT, b.EXPENSE_GL_ACCOUNT,
//		b.NON_UNITIZED_GL_ACCOUNT, b.UNITIZED_GL_ACCOUNT, b.cpi_elig_indicator, b.SALVAGE_GL_ACCOUNT,
//		b.REMOVAL_GL_ACCOUNT, b.RETIREMENT_GL_ACCOUNT, b.JOBBING_GL_ACCOUNT, b.FUNC_CLASS_ID,
//		b.UNITIZE_BY_ACCOUNT, b.TOLERANCE_ID, b.CLOSING_PATTERN_ID, a.base_year,
//		a.AFUDC_START_DATE, a.AFUDC_STOP_DATE, b.ACCRUAL_TYPE_ID, b.CR_DERIVER_TYPE_DETAIL,
//		b.CR_DERIVER_TYPE_ESTIMATE, b.ESCALATION_ID, b.EST_UNIT_ITEM_OPTION, b.WORK_TYPE_ID,
//		b.WO_EST_HIERARCHY_ID, b.rwip_type_id
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_INITIATOR
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Initiator")
//	
//	// Insert into work_order_initiator
//	insert into work_order_initiator (
//		work_order_id, users, initiation_date, responsibility_type,
//		proj_mgr, plant_accountant, contract_adm, engineer, other
//		)
//	select a.work_order_id, nvl(a.initiator,user), nvl(a.initiation_date,sysdate), 'prepare',
//		project_manager, plant_accountant, contract_admin, engineer, other_contact
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORK_ORDER_APPROVAL
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Work Order Approval")
//	
//	// Insert into work_order_approval
//	insert into work_order_approval (
//		work_order_id, revision, budget_version_id, approval_type_id,
//		est_start_date, est_complete_date, est_in_service_date, approval_status_id, rejected
//		)
//	select a.work_order_id, 1, null budget_version_id, nvl(a.approval_type_id, b.approval_type_id),
//		nvl(a.est_start_date,sysdate), nvl(a.est_complete_date,sysdate), nvl(a.est_in_service_date,nvl(a.est_complete_date,sysdate)), decode(a.wo_status_id,1,1,3), decode(a.wo_status_id,1,2,null)
//	from wo_interface_staging a, work_order_type b
//	where a.work_order_type_id = b.work_order_type_id
//	and a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT WORKFLOW
//	//
//	//--------------------------------------------------------------------------------
//	if workflow_appr = 'yes' then
//		uf_msgs("Inserting Workflow")
//		
//		// Insert into workflow
//		insert into workflow (
//			workflow_id, workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
//			approval_amount, approval_status_id, use_limits, sql_approval_amount
//			)
//		select pwrplant1.nextval, c.workflow_type_id, c.description, 'wo_approval', a.work_order_id, 1,
//			0 approval_amount, decode(a.wo_status_id,1,1,3) approval_status_id, c.use_limits, c.sql_approval_amount
//		from wo_interface_staging a, work_order_type b,workflow_type c
//		where a.work_order_type_id = b.work_order_type_id
//		and nvl(a.approval_type_id, b.APPROVAL_TYPE_ID) = c.workflow_type_id
//		and a.status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and a.funding_wo_indicator = 0
//		and a.action = 'I'
//		and a.error_message is null
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// INSERT FUNDING_JUSTIFICATION
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Funding Justification")
//	
//	// Insert into funding_justification
//	insert into funding_justification (
//		work_order_id, reason_for_work, background, future_projects, alternatives, financial_analysis
//		)
//	select a.work_order_id,  a.reason_for_work, a.background, a.future_projects, a.alternatives, a.financial_analysis
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// UPDATING DEFAULT APPROVERS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Updating Default Approvers")
//	
//	// Update default approvers
//	// Only for unapproved revisions
//	select count(*) into :checkers
//	from approval_defaults
//	where work_order_type_id in (
//		select a.work_order_type_id
//		from wo_interface_staging a
//		where a.status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and a.funding_wo_indicator = 0
//		and a.action = 'I'
//		and a.error_message is null
//		and a.wo_status_id = 1
//		)
//	;
//	
//	if checkers > 0 then
//		update work_order_approval a
//		set (authorizer1, authorizer2, authorizer3, authorizer4, authorizer5, authorizer6, authorizer7, authorizer8, authorizer9, authorizer10) = (
//			select auth_level1, auth_level2, auth_level3, auth_level4, auth_level5, auth_level6, auth_level7, auth_level8, auth_level9, auth_level10
//			from approval_defaults q, work_order_control c
//			where c.work_order_type_Id = q.work_order_type_id
//			and c.work_order_id = a.work_order_id
//			)
//		where a.work_order_id in (
//			select b.work_order_id
//			from wo_interface_staging b
//			where b.status = 1
//			and nvl(b.batch_id,'null') = :a_batch
//			and (b.company_id = :a_company or :a_company = -1)
//			and b.funding_wo_indicator = 0
//			and b.action = 'I'
//			and b.error_message is null
//			and b.wo_status_id = 1
//			)
//		and a.revision = 1	
//		;
//		if uf_check_sql() = -1 then return -1
//		
//		insert into wo_approval_multiple (
//			work_order_id, revision, approval_type_id, auth_level,
//			users, category_id, required, requiring_user, auth_level_id
//			)
//		select a.work_order_id, 1 revision, a.approval_type_id, c.auth_level,
//			c.users, c.category_id, c.required, user, d.auth_level_id
//		from work_order_approval a, wo_interface_staging b, wo_approval_multiple_default c,
//			(	select approval_type_id, 1 auth_level_id, auth_level1 auth_level from approval union all
//				select approval_type_id, 2 auth_level_id, auth_level2 auth_level from approval union all
//				select approval_type_id, 3 auth_level_id, auth_level3 auth_level from approval union all
//				select approval_type_id, 4 auth_level_id, auth_level4 auth_level from approval union all
//				select approval_type_id, 5 auth_level_id, auth_level5 auth_level from approval union all
//				select approval_type_id, 6 auth_level_id, auth_level6 auth_level from approval union all
//				select approval_type_id, 7 auth_level_id, auth_level7 auth_level from approval union all
//				select approval_type_id, 8 auth_level_id, auth_level8 auth_level from approval union all
//				select approval_type_id, 9 auth_level_id, auth_level9 auth_level from approval union all
//				select approval_type_id, 10 auth_level_id, auth_level10 auth_level from approval
//			) d
//		where a.work_order_id = b.work_order_id
//		and a.approval_type_id = c.approval_type_id
//		and c.approval_type_id = d.approval_type_id
//		and lower(trim(c.auth_level)) = lower(trim(d.auth_level))
//		and b.status = 1
//		and nvl(b.batch_id,'null') = :a_batch
//		and (b.company_id = :a_company or :a_company = -1)
//		and b.funding_wo_indicator = 0
//		and b.action = 'I'
//		and b.error_message is null
//		and b.wo_status_id = 1
//		;
//		if uf_check_sql() = -1 then return -1
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// CLASS CODES
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Class Codes")
//	
//	// Create new class codes
//	//rtn = uf_create_cc() -- I think we should set these up if we know we are going to need them
//	//if rtn = -1 then
//	//	return -1
//	//end if
//	
//	for i = 1 to i_max_cc
//		uf_msgs( " Class Code Insert "+string(i))
//		cc_sqls_wocc = &
//			'insert into work_order_class_code (work_order_id, class_code_id, "VALUE") '+&
//			'select a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
//			'from wo_interface_staging a, class_code cc '+&
//			'where a.status = 1 '+&
//			"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//			'and a.funding_wo_indicator = 0 '+&
//			'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//			"and a.action = 'I' "+&
//			"and a.error_message is null "+&
//			'and a.class_code'+string(i)+' is not null '+&
//			'and class_value'+string(i)+' is not null '+&
//			'and cc.description = a.class_code'+string(i)+' '+&
//			'and cc.cwip_indicator = 1 '
//		execute immediate :cc_sqls_wocc;
//		if uf_check_sql() = -1 then return -1
//	next
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// CLASS CODE DEFAULTS
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Inserting Class Code Defaults")
//	
//	// Create new class codes
//	uf_msgs( " Class Code Defaults ")
//	insert into work_order_class_code (work_order_id, class_code_id, value)
//	select a.work_order_id, b.class_code_id, b.value
//	from wo_interface_staging a, wo_type_class_code_default b
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	and a.work_order_type_id = b.work_order_type_id
//	and not exists (
//		select 1 from work_order_class_code c
//		where c.work_order_id = a.work_order_id
//		and c.class_code_id = b.class_code_id
//		)
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// MARK RECORDS AS PROCESSED
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Interface Status Update")
//	
//	// Update status to indicate it has been processed
//	update wo_interface_staging a
//	set status = 2
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'I'
//	and a.error_message is null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// UPDATES
//	//
//	//--------------------------------------------------------------------------------
//	updates:
//	
//	uf_msgs("------------------------------- ")
//	uf_msgs("Updating Existing Work Orders")
//	
//	select count(*)
//	into :update_counts
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	and a.error_message is null
//	;
//	
//	if isnull(update_counts) then update_counts = 0
//	if update_counts = 0 then
//		uf_msgs("No Existing Work Orders to Process")
//	end if
//	
//	if no_updates or update_counts = 0 then
//		// Do not run updates
//	else
//		rtn = uf_update_wos(a_company, a_batch)
//		if rtn = -1 then
//			return -1
//		end if
//	end if
//	
//	
//	//--------------------------------------------------------------------------------
//	//
//	// COMMIT HERE
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Committing Progress")
//	
//	// Commit progress thus far
//	commit;
//	
//next
//
//
////--------------------------------------------------------------------------------
////
//// CLASS_CODE_VALUES
////
////--------------------------------------------------------------------------------
//uf_msgs("Backfilling Class Code Values")
//insert into class_code_values (class_code_id, value)
//select class_code_id, value
//from work_order_class_code
//minus
//select class_code_id, value
//from class_code_values
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// ARCHIVE RECORDS
////
////--------------------------------------------------------------------------------
//uf_msgs("Archiving Records")
//
//// Insert into archive table
//insert into wo_interface_staging_arc (
//	WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,
//	FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,
//	EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,
//	IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,
//	BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,APPROVAL_TYPE_ID,AFUDC_STOP_DATE,
//	AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,
//	WO_STATUS_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,
//	CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,
//	CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,
//	CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,
//	CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,CLASS_VALUE23,
//	CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,
//	CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,RUN_DATE,
//	batch_id, REIMBURSABLE_TYPE_ID, ext_company, ext_work_order_type, ext_major_location, budget_number, budget_company_id, ext_department,
//	ext_work_order_group, ext_asset_location, seq_id, USER_ID, TIME_STAMP, agreement_id, error_message, old_wo_status_id, ext_work_order_status
//	)
//select WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,
//	FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,
//	EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,
//	IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,
//	BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,APPROVAL_TYPE_ID,AFUDC_STOP_DATE,
//	AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,
//	WO_STATUS_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,
//	CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,
//	CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,
//	CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,
//	CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,CLASS_VALUE23,
//	CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,
//	CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,sysdate,
//	batch_id, REIMBURSABLE_TYPE_ID, ext_company, ext_work_order_type, ext_major_location, budget_number, budget_company_id, ext_department,
//	ext_work_order_group, ext_asset_location, seq_id, USER_ID, TIME_STAMP, agreement_id, error_message, old_wo_status_id, ext_work_order_status
//from wo_interface_staging a
//where nvl(a.status,1) = 2
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 0
//and a.error_message is null
//;
//if uf_check_sql() = -1 then return -1
//
//// Delete Archived Data
//delete from wo_interface_staging a
//where nvl(a.status,1) = 2
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 0
//and a.error_message is null
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// COMMIT
////
////--------------------------------------------------------------------------------
//uf_msgs("Committing records")
//
//// Commit here
//commit;
//
//uf_msgs("Finished WO Loading - Company Id = "+string(a_company))
//uf_msgs("------------------------------- ")
//uf_msgs("------------------------------- ")
//
//
//return 1
end function

public function integer uf_update_wos (longlong a_company, string a_batch);return 1

////***************************************************************************************** 
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////
////   Subsystem:   system
////
////   Event    :   uo_wo_interface.uf_update_wos
////
////   Purpose  :   Creates Work Orders from pre-staged data in the table wo_interface_staging.
////					Assumes that the Funding Project Number, Work_order_type_id and desciption are not null in the wo_interface_staging table.
////                    Updates valid entrys to status of 2.
////                    This functions is automatically called through the function uf_create_wos()
////                     
//// 					
////   DATE                           NAME                            REVISION                     CHANGES
////  --------      --------     -----------   --------------------------------------------- 
////  07-23-2008      PowerPlan           Version 1.0   Initial Version
////
////  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
////*****************************************************************************************
//longlong checkers, i
//string cc_update, cc_insert
//
//if isnull(a_batch) then a_batch = 'null'
//
//sqlca.analyze_table('wo_interface_staging')
//
////--------------------------------------------------------------------------------
////
//// WORK ORDER CONTROL
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Work Order Control")
//update work_order_control z
//set (BUS_SEGMENT_ID, COMPANY_ID, BUDGET_ID, WO_STATUS_ID,
//	REASON_CD_ID, WORK_ORDER_TYPE_ID, FUNDING_WO_ID, MAJOR_LOCATION_ID,
//	ASSET_LOCATION_ID, DESCRIPTION, LONG_DESCRIPTION, EST_START_DATE,
//	EST_COMPLETE_DATE, NOTES, DEPARTMENT_ID, EST_IN_SERVICE_DATE,
//	COMPLETION_DATE, IN_SERVICE_DATE, CLOSE_DATE, EST_ANNUAL_REV,
//	EXTERNAL_WO_NUMBER, OUT_OF_SERVICE_DATE, SUSPENDED_DATE, WO_APPROVAL_GROUP_ID
//	) = (
//	select nvl(a.BUS_SEGMENT_ID,z.bus_segment_Id), nvl(a.COMPANY_ID,z.company_id), nvl(a.BUDGET_ID,z.budget_id), nvl(a.wo_status_id,z.wo_status_id),
//		nvl(a.REASON_CD_ID,z.reason_cd_id), nvl(a.WORK_ORDER_TYPE_ID,z.work_order_type_id), nvl(a.FUNDING_WO_ID,z.funding_wo_id), nvl(a.MAJOR_LOCATION_ID,z.major_location_id),
//		nvl(a.ASSET_LOCATION_ID,z.asset_location_id), nvl(a.DESCRIPTION,z.description), nvl(a.LONG_DESCRIPTION,z.long_description), nvl(a.EST_START_DATE,z.est_start_date),
//		nvl(a.EST_COMPLETE_DATE,z.EST_COMPLETE_DATE), nvl(a.NOTES,z.NOTES), nvl(a.DEPARTMENT_ID,z.DEPARTMENT_ID), nvl(a.EST_IN_SERVICE_DATE,z.EST_IN_SERVICE_DATE),
//		decode(a.wo_status_id, null, z.COMPLETION_DATE, a.COMPLETION_DATE), decode(a.wo_status_id, null, z.IN_SERVICE_DATE, a.IN_SERVICE_DATE), decode(a.wo_status_id, null, z.CLOSE_DATE, a.CLOSE_DATE), nvl(a.EST_ANNUAL_REV, z.EST_ANNUAL_REV),
//		nvl(a.EXTERNAL_WO_NUMBER, z.EXTERNAL_WO_NUMBER), nvl(a.OUT_OF_SERVICE_DATE, z.OUT_OF_SERVICE_DATE), decode(a.wo_status_id, null, z.SUSPENDED_DATE, a.SUSPENDED_DATE), nvl(a.WO_APPROVAL_GROUP_ID, z.WO_APPROVAL_GROUP_ID)
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	and a.error_message is null
//	and a.work_order_id = z.work_order_id
//	)
//where z.work_order_id in (
//	select work_order_id
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	and a.error_message is null
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// WORK ORDER ACCOUNT
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Work Order Account")
//update work_order_account z
//set (WORK_ORDER_GRP_ID, AFUDC_START_DATE, AFUDC_STOP_DATE, BASE_YEAR) = (
//	select nvl(a.work_order_grp_id, z.work_order_grp_id),
//		nvl(a.AFUDC_START_DATE, z.AFUDC_START_DATE),
//		nvl(a.AFUDC_STOP_DATE, z.AFUDC_STOP_DATE),
//		nvl(a.BASE_YEAR, z.BASE_YEAR)
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	and a.error_message is null
//	and a.work_order_id = z.work_order_id
//	)
//where z.work_order_id in (
//	select work_order_id
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	and a.error_message is null
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// WORK_ORDER_INITIATOR
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Work Order Initiator")
//
//// Insert into work_order_initiator
//update work_order_initiator z
//set (users, initiation_date, proj_mgr, plant_accountant, contract_adm, engineer, other) = (
//	select nvl(a.initiator,z.users), nvl(a.initiation_date,z.initiation_date), nvl(a.project_manager,z.proj_mgr), nvl(a.plant_accountant,z.plant_accountant), nvl(a.contract_admin,z.contract_adm), nvl(a.engineer,z.engineer), nvl(a.other_contact,z.other)
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	and a.error_message is null
//	and a.work_order_id = z.work_order_id
//	)
//where z.work_order_id in (
//	select work_order_id
//	from wo_interface_staging a
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.funding_wo_indicator = 0
//	and a.action = 'U'
//	and a.error_message is null
//	)
//;
//if uf_check_sql() = -1 then return -1
//
//
////--------------------------------------------------------------------------------
////
//// CLASS CODES
////
////--------------------------------------------------------------------------------
//uf_msgs("Updating Class Codes")
//for i = 1 to i_max_cc
//	cc_insert = &
//		'insert into work_order_class_code (work_order_id, class_code_id, value) '+&
//		'select a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
//		'from wo_interface_staging a, class_code cc '+&
//		'where a.status = 1 '+&
//		"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//		'and a.funding_wo_indicator = 0 '+&
//		'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//		"and a.action = 'U' "+&
//		"and a.error_message is null "+&
//		'and a.class_code'+string(i)+' is not null '+&
//		'and a.class_value'+string(i)+' is not null '+&
//		'and trim(a.class_code'+string(i)+') = trim(cc.description) '+&
//		'and cc.cwip_indicator = 1 '+&
//		'and not exists ( '+&
//		'	select 1 from work_order_class_code c '+&
//		'	where c.work_order_id = a.work_order_id '+&
//		'	and c.class_code_id = cc.class_code_id '+&
//		'	) '
//	execute immediate :cc_insert;
//	if uf_check_sql() = -1 then return -1
//	
//	cc_update = &
//		"update work_order_class_code z "+&
//		"set value = ( "+&
//		"	select a.class_value"+string(i)+" "+&
//		"	from wo_interface_staging a, class_code cc "+&
//		'	where a.status = 1 '+&
//		"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//		'	and a.funding_wo_indicator = 0 '+&
//		'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//		"	and a.action = 'U' "+&
//		"	and a.error_message is null "+&
//		"	and a.class_code"+string(i)+" is not null "+&
//		"	and a.class_value"+string(i)+" is not null "+&
//		"	and z.work_order_id = a.work_order_id "+&
//		"	and z.class_code_id = cc.class_code_id "+&
//		"	and trim(cc.description) = trim(a.class_code"+string(i)+") "+&
//		"	and cc.cwip_indicator = 1 "+&
//		"	) "+&
//		"where (work_order_id, class_code_id) in ( "+&
//		"	select work_order_id, cc.class_code_id "+&
//		"	from wo_interface_staging a, class_code cc "+&
//		'	where a.status = 1 '+&
//		"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
//		'	and a.funding_wo_indicator = 0 '+&
//		'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
//		"	and a.action = 'U' "+&
//		"	and a.error_message is null "+&
//		"	and a.class_code"+string(i)+" is not null "+&
//		"	and a.class_value"+string(i)+" is not null "+&
//		"	and trim(cc.description) = trim(a.class_code"+string(i)+") "+&
//		"	and cc.cwip_indicator = 1 "+&
//		"	) "
//	execute immediate :cc_update;
//	if uf_check_sql() = -1 then return -1
//next
//
//
////--------------------------------------------------------------------------------
////
//// MARK RECORDS AS PROCESSED
////
////--------------------------------------------------------------------------------
//uf_msgs("Interface Status Update")
//
//// Update status to indicate it has been processed
//update wo_interface_staging a
//set status = 2
//where a.status = 1
//and nvl(a.batch_id,'null') = :a_batch
//and (a.company_id = :a_company or :a_company = -1)
//and a.funding_wo_indicator = 0
//and a.action = 'U'
//and a.error_message is null
//;
//if uf_check_sql() = -1 then return -1
//
//
//return 1
end function

public function integer uf_load_unit_est (longlong a_company, boolean a_replace, boolean a_process_ocr, string a_batch);longlong rtn, ocr_est_ids[], cnt, num_batches, ii, csac_count
string sqls, dflt_add, dflt_retire, dflt_rmvl, ua_fc_filter, al_ml_filter
any results[]
uo_budget_revision uo_rev
uo_original_cost_retirement uo_ocr
longlong i

if isnull(a_batch) then a_batch = 'null'
if isnull(a_company) then a_company = -1

uf_msgs("Begin WO Unit Estimate Loading - Company Id = "+string(a_company))


//--------------------------------------------------------------------------------
//
// PROCESSING STATUS
//
//--------------------------------------------------------------------------------
uf_msgs("Setting Status")
// PP-41914
update wo_interface_unit a
set status = 1,
	funding_wo_indicator = nvl(a.funding_wo_indicator, 0),
	process_level = nvl(a.process_level, 'wo'),
	error_message = null,
	work_order_number = upper(a.work_order_number)
where nvl(batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and nvl(status,1) <> 2
;	 
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// COMPANY
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Mapping Company")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on gl_company_no
update wo_interface_unit a
set a.company_id = (
	select p.company_id
	from company p
	where p.gl_company_no = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Map on company description
update wo_interface_unit a
set a.company_id = (
	select p.company_id
	from company p
	where p.description = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Map on company_id
update wo_interface_unit a
set a.company_id = (
	select p.company_id
	from company p
	where to_char(p.company_id) = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Flag records where ext_company could not be mapped
update wo_interface_unit a
set a.error_message = a.error_message || 'Unit - cannot derive company_id; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

////
////	No external value was provided
////

// Flag records missing company_id as errors
update wo_interface_unit a
set a.error_message = a.error_message || 'Unit - company_id is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// WORK ORDER NUMBER
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Work Order Number")

// Flag records missing work_order_number as errors
update wo_interface_unit a
set a.error_message = a.error_message || 'Unit - work_order_number is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and a.work_order_number is null
;
if uf_check_sql() = -1 then return -1
// Field is not null...this should never find anything


//--------------------------------------------------------------------------------
//
// EXT JOB TASK
//  - always required where process_level = 'task'
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Ext Job Task")

// Flag records missing work_order_number as errors
update wo_interface_unit a
set a.error_message = a.error_message || 'Unit - ext_job_task is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and a.ext_job_task is null
and a.process_level = 'task'
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// VALIDATION
//   - must have a valid COMPANY_ID and WORK_ORDER_NUMBER and JOB_TASK_ID
//
//--------------------------------------------------------------------------------
uf_msgs("Flagging Errors (missing Company or Work Order Number (or Ext Job Task))")

// Flag records that already have errors as unable to continue processing (either invalid company_id or work_order_number or ext_job_task)
update wo_interface_unit a
set a.status = -11
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and a.error_message is not null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// DETERMINE THE NUMBER OF BATCHES
//   - Loop over batches of work order/job task estimates
//
//--------------------------------------------------------------------------------
uf_msgs("Identifying Process Batches")

// Determine the number of loops needed to process all work orders in sequence
select max(batches)
into :num_batches
from (
	select max(batches) batches
	from (
		select work_order_number, company_id, count(distinct nvl(seq_id,0)) batches
		from wo_interface_unit a
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.process_level = 'wo'
		group by work_order_number, company_id
		)
	union all
	select max(batches) batches
	from (
		select work_order_number, company_id, ext_job_task, count(distinct nvl(seq_id,0)) batches
		from wo_interface_unit a
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.process_level = 'task'
		group by work_order_number, company_id, ext_job_task
		)
	)
;
if uf_check_sql() = -1 then return -1

uf_msgs("Updating status to Pending")

// Update records that have not yet been processed to status = 3 (pending
update wo_interface_unit a
set a.status = 3 // pending
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// LOOP OVER THE BATCHES
//
//--------------------------------------------------------------------------------
for ii = 1 to num_batches
	uf_msgs("***** Batch "+string(ii)+" of "+string(num_batches)+" *****")
	uf_msgs(" ")
	
	uf_msgs("Updating status to In-Process for Work Order Estimates")
	update wo_interface_unit a
	set a.status = 1 // in-process
	where a.status = 3
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.process_level = 'wo'
	and nvl(seq_id,0) in (
		select min(nvl(seq_id,0))
		from wo_interface_unit b
		where b.status = 3
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and a.process_level = 'wo'
		and b.work_order_number = a.work_order_number
		and b.company_id = a.company_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Updating status to In-Process for Job Task Estimates")
	update wo_interface_unit a
	set a.status = 1 // in-process
	where a.status = 3
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.process_level = 'task'
	and nvl(seq_id,0) in (
		select min(nvl(seq_id,0))
		from wo_interface_unit b
		where b.status = 3
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and a.process_level = 'task'
		and b.work_order_number = a.work_order_number
		and b.company_id = a.company_id
		and b.ext_job_task = a.ext_job_task
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// WORK ORDER ID
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Work Order Id")
	
	// Map on work_order_number
	update wo_interface_unit a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.work_order_number = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on external_wo_number
	update wo_interface_unit a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.external_wo_number = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_unit a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.description = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on work_order_id
	update wo_interface_unit a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where to_char(p.work_order_id) = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing work_order_id as errors
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive work_order_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from work_order_control b
		where b.work_order_id = nvl(a.work_order_id,-596)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	
	//--------------------------------------------------------------------------------
	//
	// JOB TASK
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Job Task Id (job_task)")
	
	// Map on job_task_id
	update wo_interface_unit a
	set a.job_task_id = (
		select p.job_task_id
		from job_task p
		where p.job_task_id = a.ext_job_task
		and p.work_order_id = a.work_order_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.job_task_id is null
	and a.ext_job_task is not null
	and exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on external_job_task
	update wo_interface_unit a
	set a.job_task_id = (
		select p.job_task_id
		from job_task p
		where p.external_job_task = a.ext_job_task
		and p.work_order_id = a.work_order_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.job_task_id is null
	and a.ext_job_task is not null
	and exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on job_task description
	update wo_interface_unit a
	set a.job_task_id = (
		select p.job_task_id
		from job_task p
		where p.description = a.ext_job_task
		and p.work_order_id = a.work_order_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.job_task_id is null
	and a.ext_job_task is not null
	and exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_job_task could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive job_task_id (job_task); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and (a.job_task_id is not null or a.ext_job_task is not null)
	and exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	and not exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		and p.job_task_id = nvl(a.job_task_id,'-596')
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// JOB TASK LIST
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Job Task Id (job_task_list)")
	
	// Map on job_task_id
	update wo_interface_unit a
	set a.job_task_id = (
		select p.job_task_id
		from job_task_list p
		where p.job_task_id = a.ext_job_task
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.job_task_id is null
	and a.ext_job_task is not null
	and not exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on external_job_task
	update wo_interface_unit a
	set a.job_task_id = (
		select p.job_task_id
		from job_task_list p
		where p.external_job_task = a.ext_job_task
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.job_task_id is null
	and a.ext_job_task is not null
	and not exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on job_task description
	update wo_interface_unit a
	set a.job_task_id = (
		select p.job_task_id
		from job_task_list p
		where p.description = a.ext_job_task
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.job_task_id is null
	and a.ext_job_task is not null
	and not exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_job_task could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive job_task_id (job_task_list); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and (a.job_task_id is not null or a.ext_job_task is not null)
	and not exists (
		select 1 from job_task p
		where p.work_order_id = a.work_order_id
		)
	and not exists (
		select 1 from job_task_list p
		where p.job_task_id = nvl(a.job_task_id,'-596')
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	// These should not find anything anyways because we are processing in batches
	////--------------------------------------------------------------------------------
	////
	//// SETTING OLD RECORDS AS PROCESSED BASED ON SEQUENCE (SEQ_ID)
	////   If newer records for the same work order already exist, mark the
	////   old ones as processed, and continue processing the latest ones
	////
	////--------------------------------------------------------------------------------
	//uf_msgs("Checking Sequence for Work Order Estimates")
	//update wo_interface_unit a
	//set a.status = 2
	//where a.status = 1
	//and nvl(a.batch_id,'null') = :a_batch
	//and (a.company_id = :a_company or :a_company = -1)
	//and a.process_level = 'wo'
	//and nvl(a.seq_id,0) < (
	//	select nvl(max(b.seq_id),0)
	//	from wo_interface_unit b
	//	where b.status = 1
	//	and nvl(b.batch_id,'null') = :a_batch
	//	and (b.company_id = :a_company or :a_company = -1)
	//	and b.process_level = 'wo'
	//	and b.work_order_id = a.work_order_id
	//	)
	//;
	//if uf_check_sql() = -1 then return -1
	//
	//uf_msgs("Checking Sequence for Job Task Estimates")
	//update wo_interface_unit a
	//set a.status = 2
	//where a.status = 1
	//and nvl(a.batch_id,'null') = :a_batch
	//and (a.company_id = :a_company or :a_company = -1)
	//and a.process_level = 'task'
	//and nvl(a.seq_id,0) < (
	//	select nvl(max(b.seq_id),0)
	//	from wo_interface_unit b
	//	where b.status = 1
	//	and nvl(b.batch_id,'null') = :a_batch
	//	and (b.company_id = :a_company or :a_company = -1)
	//	and b.process_level = 'task'
	//	and b.work_order_id = a.work_order_id
	//	and b.job_task_id = a.job_task_id
	//	)
	//;
	//if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	
	
	//--------------------------------------------------------------------------------
	//
	// EXPENDITURE TYPE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Expenditure Type")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on description
	update wo_interface_unit a
	set a.expenditure_type_id = (
		select p.expenditure_type_id
		from expenditure_type p
		where p.description = a.ext_expenditure_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on expenditure_type_id
	update wo_interface_unit a
	set a.expenditure_type_id = (
		select p.expenditure_type_id
		from expenditure_type p
		where to_char(p.expenditure_type_id) = a.ext_expenditure_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on notes
	update wo_interface_unit a
	set a.expenditure_type_id = (
		select p.expenditure_type_id
		from expenditure_type p
		where p.notes = a.ext_expenditure_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where expenditure_type_id could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive expenditure_type_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	// Flag records missing expenditure_type_id as errors
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - expenditure_type_id is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// ESTIMATE CHARGE TYPE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Estimate Charge Type")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_est_chg_type
	update wo_interface_unit a
	set a.est_chg_type_id = (
		select p.est_chg_type_id
		from estimate_charge_type p
		where p.external_est_chg_type = a.ext_est_chg_type
		and nvl(p.funding_charge_indicator,0) = a.funding_wo_indicator
		and p.processing_type_id <> 5
		and nvl(p.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on estimate_charge_type description
	update wo_interface_unit a
	set a.est_chg_type_id = (
		select p.est_chg_type_id
		from estimate_charge_type p
		where p.description = a.ext_est_chg_type
		and nvl(p.funding_charge_indicator,0) = a.funding_wo_indicator
		and p.processing_type_id <> 5
		and nvl(p.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on est_chg_type_id
	update wo_interface_unit a
	set a.est_chg_type_id = (
		select p.est_chg_type_id
		from estimate_charge_type p
		where to_char(p.est_chg_type_id) = a.ext_est_chg_type
		and nvl(p.funding_charge_indicator,0) = a.funding_wo_indicator
		and p.processing_type_id <> 5
		and nvl(p.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on external_cost_element
	update wo_interface_unit a
	set a.est_chg_type_id = (
		select decode(a.funding_wo_indicator, 1, ect.funding_chg_type, ect.est_chg_type_id)
		from work_order_account woa, wo_est_hierarchy_map map, cost_element ce, estimate_charge_type ect
		where woa.work_order_id = a.work_order_id
		and woa.wo_est_hierarchy_id = map.wo_est_hierarchy_id
		and ce.external_cost_element = a.ext_est_chg_type
		and ce.cost_element_id = map.cost_element_id
		and map.est_chg_type_id = ect.est_chg_type_id
		and nvl(ect.funding_charge_indicator, 0) = 0
		and ect.processing_type_id <> 5
		and nvl(ect.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on cost_element description
	update wo_interface_unit a
	set a.est_chg_type_id = (
		select decode(a.funding_wo_indicator, 1, ect.funding_chg_type, ect.est_chg_type_id)
		from work_order_account woa, wo_est_hierarchy_map map, cost_element ce, estimate_charge_type ect
		where woa.work_order_id = a.work_order_id
		and woa.wo_est_hierarchy_id = map.wo_est_hierarchy_id
		and ce.description = a.ext_est_chg_type
		and ce.cost_element_id = map.cost_element_id
		and map.est_chg_type_id = ect.est_chg_type_id
		and nvl(ect.funding_charge_indicator, 0) = 0
		and ect.processing_type_id <> 5
		and nvl(ect.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where est_chg_type_id could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive est_chg_type_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////	
	
	////	BCM 20130110 - Default est_chg_type_id based on system control
	////	if f_pp_system_control_company function returns a null value because the value in
	////	pp_system_control_company is null or the input system control does not exist,
	////	the default charge types will be defaulted to -99.
	dflt_add = lower(trim(f_pp_system_control_company('WOEST - Dflt Add Charge Type', a_company)))
	dflt_retire = lower(trim(f_pp_system_control_company('WOEST - Dflt Retire Charge Type', a_company)))
	dflt_rmvl = lower(trim(f_pp_system_control_company('WOEST - DFLT REMOVAL CHARGE TYPE', a_company)))

	update wo_interface_unit a
	set a.est_chg_type_id = (
		select est_chg_type_id
		from estimate_charge_type
		where est_chg_type_id = decode(	a.expenditure_type_id,  
													1, nvl(:dflt_add,-99),
													2, decode(	nvl2(a.retire_vintage,-99,3000),
																	-99, nvl(:dflt_retire,-99),
																	decode(	nvl2(asset_id, -99,3000),
																				-99, nvl(:dflt_retire,-99),
																				nvl(:dflt_rmvl,-99))),
													null)
		)
	where   a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing expenditure_type_id as errors
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - est_chg_type_id is required and no valid default provided; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// DEPARTMENT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Department")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_department_code
	update wo_interface_unit a
	set a.department_id = (
		select p.department_id
		from department p
		where p.external_department_code = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on department description
	update wo_interface_unit a
	set a.department_id = (
		select p.department_id
		from department p
		where p.description = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on department_id
	update wo_interface_unit a
	set a.department_id = (
		select p.department_id
		from department p
		where to_char(p.department_id) = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where department_id could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive department_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// BUS SEGMENT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Bus Segment")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_bus_segment
	update wo_interface_unit a
	set a.bus_segment_id = (
		select p.bus_segment_id
		from business_segment p
		where p.external_bus_segment = a.ext_bus_segment
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.bus_segment_id is null
	and a.ext_bus_segment is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on business_segment description
	update wo_interface_unit a
	set a.bus_segment_id = (
		select p.bus_segment_id
		from business_segment p
		where p.description = a.ext_bus_segment
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.bus_segment_id is null
	and a.ext_bus_segment is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on bus_segment_id
	update wo_interface_unit a
	set a.bus_segment_id = (
		select p.bus_segment_id
		from business_segment p
		where to_char(p.bus_segment_id) = a.ext_bus_segment
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.bus_segment_id is null
	and a.ext_bus_segment is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_work_order_type could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive bus_segment_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.bus_segment_id is null
	and a.ext_bus_segment is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	// Default bus_segment_id from work_order_control
	update wo_interface_unit a
	set a.bus_segment_id = (
		select p.bus_segment_id
		from work_order_control p
		where p.work_order_id = a.work_order_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.bus_segment_id is null
	and a.ext_bus_segment is null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing bus_segment_id as errors
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - bus_segment_id is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.bus_segment_id is null
	and a.ext_bus_segment is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// UTILITY ACCOUNT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Utility Account")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_account_code
	update wo_interface_unit a
	set a.utility_account_id = (
		select p.utility_account_id
		from utility_account p
		where p.external_account_code = a.ext_utility_account
		and p.bus_segment_id = a.bus_segment_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.utility_account_id is null
	and a.ext_utility_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on utility_account description
	update wo_interface_unit a
	set a.utility_account_id = (
		select p.utility_account_id
		from utility_account p
		where p.description = a.ext_utility_account
		and p.bus_segment_id = a.bus_segment_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.utility_account_id is null
	and a.ext_utility_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on utility_account_id
	update wo_interface_unit a
	set a.utility_account_id = (
		select p.utility_account_id
		from utility_account p
		where to_char(p.utility_account_id) = a.ext_utility_account
		and p.bus_segment_id = a.bus_segment_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.utility_account_id is null
	and a.ext_utility_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_utility_account could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive utility_account_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.utility_account_id is null
	and a.ext_utility_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	

	//--------------------------------------------------------------------------------
	//
	// SUB ACCOUNT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Sub Account")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_account_code
	update wo_interface_unit a
	set a.sub_account_id = (
		select p.sub_account_id
		from sub_account p
		where p.bus_segment_id = a.bus_segment_id
		and p.utility_account_id = a.utility_account_id
		and p.external_account_code = a.ext_sub_account
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.sub_account_id is null
	and a.utility_account_id is not null
	and a.ext_sub_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on sub_account description
	update wo_interface_unit a
	set a.sub_account_id = (
		select p.sub_account_id
		from sub_account p
		where p.bus_segment_id = a.bus_segment_id
		and p.utility_account_id = a.utility_account_id
		and p.description = a.ext_sub_account
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.sub_account_id is null
	and a.utility_account_id is not null
	and a.ext_sub_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on sub_account_id
	update wo_interface_unit a
	set a.sub_account_id = (
		select p.sub_account_id
		from sub_account p
		where p.bus_segment_id = a.bus_segment_id
		and p.utility_account_id = a.utility_account_id
		and to_char(p.sub_account_id) = a.ext_sub_account )
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.sub_account_id is null
	and a.utility_account_id is not null
	and a.ext_sub_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_sub_account could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive sub_account_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.sub_account_id is null
	and a.utility_account_id is not null
	and a.ext_sub_account is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
		update wo_interface_unit a
		set a.sub_account_id = (
			select p.sub_account_id
			from sub_account p
			where p.bus_segment_id = a.bus_segment_id
			and p.utility_account_id = a.utility_account_id
			and to_char(p.sub_account_id) = a.ext_sub_account
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.sub_account_id is null
		and a.utility_account_id is not null
		and a.ext_sub_account is not null
		;
		if uf_check_sql() = -1 then return -1
	
	////	BCM 20130109 - Additional logic for mapping sub_account_id 	
	////
	////	If no sub_account_id or ext._sub_account is passed, utility_account_id is 
	////	passed, and there's only a single sub account for the utility account, map 
	////	sub account based on the utility_account_id
	////
	
	//	Map on WO_INTERFACE_UNIT utility_account_id
	update wo_interface_unit a
	set a.sub_account_id = (
		select p.sub_account_id
		from sub_account p
		where p.utility_account_id in (
			select utility_account_id
			from sub_account
			group by utility_account_id
			having count(*) = 1)
		and a.utility_account_id = p.utility_account_id
		and a.bus_segment_id = p.bus_segment_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.sub_account_id is null
	and a.ext_sub_account is null
	and a.utility_account_id is not null
	;
	if uf_check_sql() = -1 then return -1
	


	
	///CJR moving this to after retirement unit
//	//--------------------------------------------------------------------------------
//	//
//	// PROPERTY GROUP
//	//
//	//--------------------------------------------------------------------------------
//	uf_msgs("Mapping Property Group")
//	
//	////
//	////	If an external value is provided, try to map on it and error out if it cannot be mapped
//	////
//	
//	// Map on external_property_group
//	update wo_interface_unit a
//	set a.property_group_id = (
//		select p.property_group_id
//		from property_group p
//		where p.external_property_group = a.ext_property_group
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.property_group_id is null
//	and a.ext_property_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on property_group description
//	update wo_interface_unit a
//	set a.property_group_id = (
//		select p.property_group_id
//		from property_group p
//		where p.description = a.ext_property_group
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.property_group_id is null
//	and a.ext_property_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Map on property_group_id
//	update wo_interface_unit a
//	set a.property_group_id = (
//		select p.property_group_id
//		from property_group p
//		where to_char(p.property_group_id) = a.ext_property_group
//		)
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.property_group_id is null
//	and a.ext_property_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	// Flag records where ext_property_group could not be mapped
//	update wo_interface_unit a
//	set a.error_message = a.error_message || 'Unit - cannot derive property_group_id; '
//	where a.status = 1
//	and nvl(a.batch_id,'null') = :a_batch
//	and (a.company_id = :a_company or :a_company = -1)
//	and a.property_group_id is null
//	and a.ext_property_group is not null
//	;
//	if uf_check_sql() = -1 then return -1
//	
//	////
//	////	No external value was provided
//	////
	
	
	
	
	//--------------------------------------------------------------------------------
	//
	// RETIREMENT UNIT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Retirement Unit")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_retire_unit
	update wo_interface_unit a
	set a.retirement_unit_id = (
		select p.retirement_unit_id
		from retirement_unit p
		where p.external_retire_unit = a.ext_retirement_unit
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.retirement_unit_id is null
	and a.ext_retirement_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_unit a
	set a.retirement_unit_id = (
		select p.retirement_unit_id
		from retirement_unit p
		where p.description = a.ext_retirement_unit
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.retirement_unit_id is null
	and a.ext_retirement_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_unit a
	set a.retirement_unit_id = (
		select p.retirement_unit_id
		from retirement_unit p
		where to_char(p.retirement_unit_id) = a.ext_retirement_unit
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.retirement_unit_id is null
	and a.ext_retirement_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_retirement_unit could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive retirement_unit_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.retirement_unit_id is null
	and a.ext_retirement_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// PROPERTY GROUP
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Property Group")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_property_group
	update wo_interface_unit a
	set a.property_group_id = (
		select p.property_group_id
		from property_group p
		where p.external_property_group = a.ext_property_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.property_group_id is null
	and a.ext_property_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on property_group description
	update wo_interface_unit a
	set a.property_group_id = (
		select p.property_group_id
		from property_group p
		where p.description = a.ext_property_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.property_group_id is null
	and a.ext_property_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on property_group_id
	update wo_interface_unit a
	set a.property_group_id = (
		select p.property_group_id
		from property_group p
		where to_char(p.property_group_id) = a.ext_property_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.property_group_id is null
	and a.ext_property_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_property_group could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive property_group_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.property_group_id is null
	and a.ext_property_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////

	
		///fill in property group based on retirement unit picking min is fine this is what unitization would do anyway if it was left blank
		update wo_interface_unit a
		set property_group_id = (
			select min(property_group_id)
			from prop_group_prop_unit p, retirement_unit r
			where r.property_unit_id = p.property_unit_id
			and a.retirement_unit_id = r.retirement_unit_id)
		where a.status = 1
		   and nvl(a.batch_id,'null') = :a_batch
		   and (a.company_id = :a_company or :a_company = -1)
		   and a.property_group_id is null
		   and a.ext_property_group is null
		   and a.retirement_unit_id is not null
		;
		if uf_check_sql() = -1 then return -1
	
	//--------------------------------------------------------------------------------
	//
	// ASSET LOCATION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Asset Location")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on ext_asset_location
	update wo_interface_unit a
	set a.asset_location_id = (
		select p.asset_location_id
		from asset_location p
		where p.ext_asset_location = a.ext_asset_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_unit a
	set a.asset_location_id = (
		select p.asset_location_id
		from asset_location p
		where p.long_description = a.ext_asset_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on asset_location_id
	// 29826: JAK: 2013-04-30:  Added a to_char to the asset_location_id mapping.
	update wo_interface_unit a
	set a.asset_location_id = (
		select p.asset_location_id
		from asset_location p
		where to_char(p.asset_location_id) = a.ext_asset_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_asset_location could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive asset_location_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
		// Default asset location from work_order_control
		update wo_interface_unit a
		set a.asset_location_id = (
			select p.asset_location_id
			from work_order_control p
			where p.work_order_id = a.work_order_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.asset_location_id is null
		and a.ext_asset_location is null
		;
		if uf_check_sql() = -1 then return -1
	
		
	//--------------------------------------------------------------------------------
	//
	// ASSET
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Asset Id")
	
	// Map on asset_id
	// ???
	
	
	//--------------------------------------------------------------------------------
	//
	// STOCK KEEPING UNIT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Stock Keeping Unit")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on stck_keep_unit description
	update wo_interface_unit a
	set a.stck_keep_unit_id = (
		select p.stck_keep_unit_id
		from stck_keep_unit p
		where p.description = a.ext_stck_keep_unit
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.stck_keep_unit_id is null
	and a.ext_stck_keep_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on stck_keep_unit stock_unit
	update wo_interface_unit a
	set a.stck_keep_unit_id = (
		select p.stck_keep_unit_id
		from stck_keep_unit p
		where p.stock_unit = a.ext_stck_keep_unit
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.stck_keep_unit_id is null
	and a.ext_stck_keep_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on stck_keep_unit_id
	update wo_interface_unit a
	set a.stck_keep_unit_id = (
		select p.stck_keep_unit_id
		from stck_keep_unit p
		where to_char(p.stck_keep_unit_id) = a.ext_stck_keep_unit
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.stck_keep_unit_id is null
	and a.ext_stck_keep_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_stck_keep_unit could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive stck_keep_unit_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.stck_keep_unit_id is null
	and a.ext_stck_keep_unit is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	
	//--------------------------------------------------------------------------------
	//
	// WO EST TRANS TYPE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Wo Est Trans Type")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on wo_est_trans_type description
	update wo_interface_unit a
	set a.wo_est_trans_type_id = (
		select p.wo_est_trans_type_id
		from wo_est_trans_type p
		where p.description = a.ext_wo_est_trans_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.wo_est_trans_type_id is null
	and a.ext_wo_est_trans_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on wo_est_trans_type_id
	update wo_interface_unit a
	set a.wo_est_trans_type_id = (
		select p.wo_est_trans_type_id
		from wo_est_trans_type p
		where to_char(p.wo_est_trans_type_id) = a.ext_wo_est_trans_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.wo_est_trans_type_id is null
	and a.ext_wo_est_trans_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_wo_est_trans_type could not be mapped
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive wo_est_trans_type_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.wo_est_trans_type_id is null
	and a.ext_wo_est_trans_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
		update wo_interface_unit a
		set a.wo_est_trans_type_id = decode(a.expenditure_type_id,1,1,2,6)
		where status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.wo_est_trans_type_id is null
		and a.ext_wo_est_trans_type is  null
		;
		if uf_check_sql() = -1 then return -1
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	//--------------------------------------------------------------------------------
	//
	// DATAMODEL VALIDATIONS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Validating Expenditure Type / Est Charge Type Relationship")
	
	// Flag records with invalid mappings between expenditure_type_id and est_chg_type_id as errors
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid expenditure type/est charge type combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from exp_type_est_chg_type_view p
		where p.expenditure_type_id = a.expenditure_type_id
		and p.est_chg_type_id = a.est_chg_type_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Expenditure Type / Trans Type Relationship")
	
	// Flag records with invalid exp_type and wo_est_trans_type mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid expenditure type/trans type combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and (
		(a.expenditure_type_id = 1 and nvl(wo_est_trans_type_id,1) not in (1,3,4,5,6))
		or
		(a.expenditure_type_id = 2 and nvl(wo_est_trans_type_id,0) in (1,5))
		)
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Company / Business Segment Relationship")
	
	// Flag records with invalid company/bus segment mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid company/business segment combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from work_order_control woc, company_bus_segment_control cbsc
		where woc.work_order_id = a.work_order_id
		and woc.company_id = cbsc.company_id
		and cbsc.bus_segment_id = a.bus_segment_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Utility Account / Sub Account Relationship")
	
	// Flag records with invalid utility account/sub account mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid utility account/sub account combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from sub_account sa
		where sa.bus_segment_id = a.bus_segment_id
		and sa.utility_account_id = a.utility_account_id
		and sa.sub_account_id = a.sub_account_id
		)
	and a.bus_segment_id is not null
	and a.utility_account_id is not null
	and a.sub_account_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Utility Account / Property Unit Relationship")
	
	// Flag records with invalid utility account/property unit mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid utility account/property unit combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from retirement_unit ru, util_acct_prop_unit uapu
		where ru.retirement_unit_id = a.retirement_unit_id
		and ru.property_unit_id = uapu.property_unit_id
		and uapu.bus_segment_id = a.bus_segment_id
		and uapu.utility_account_id = a.utility_account_id
		)
	and a.utility_account_id is not null
	and a.retirement_unit_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Property Group / Property Unit Relationship")
	
	// Flag records with invalid property group/property unit mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid property group/property unit combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from retirement_unit ru, prop_group_prop_unit pgpu
		where ru.retirement_unit_id = a.retirement_unit_id
		and ru.property_unit_id = pgpu.property_unit_id
		and pgpu.property_group_id = a.property_group_id
		)
	and a.property_group_id is not null
	and a.retirement_unit_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Company / Property Unit Relationship")
	
	// Flag records with invalid company/property unit mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid company/property unit combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from retirement_unit ru, work_order_control woc, company_property_unit cpu
		where ru.retirement_unit_id = a.retirement_unit_id
		and woc.work_order_id = a.work_order_id
		and ru.property_unit_id = cpu.property_unit_id
		and woc.company_id = cpu.company_id
		)
	and a.retirement_unit_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Company / Major Location Relationship")
	
	// Flag records with invalid company/major location mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid company/major location combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from asset_location al, work_order_control woc, company_major_location cml
		where al.asset_location_id = a.asset_location_id
		and woc.work_order_id = a.work_order_id
		and al.major_location_id = cml.major_location_id
		and woc.company_id = cml.company_id
		)
	and a.asset_location_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Validating Functional Class / Location Type Relationship")
	
	// Flag records with invalid func class/loc type mappings
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - invalid functional class/location type combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from utility_account ua, asset_location al, major_location ml, func_class_loc_type fclt
		where ua.bus_segment_id = a.bus_segment_id
		and ua.utility_account_id = a.utility_account_id
		and al.asset_location_id = a.asset_location_id
		and al.major_location_id = ml.major_location_id
		and ua.func_class_id = fclt.func_class_id
		and ml.location_type_id = fclt.location_type_id
		)
	and a.utility_account_id is not null
	and a.asset_location_id is not null
	;
	if uf_check_sql() = -1 then return -1
	
	/* AWW 20130508 - Per Chris Mardis, this check is not needed as the work order ID as already been assigned previously in the API
	////
	////	BCM 20130109 - estimates should be flagged as an error where 
	////	estimate Company ID <> work order Company ID.
	////	
	
	// Flag records as errors
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - company_id does not match work order company_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and work_order_number is not null
	and trim(a.company_id) <> (
		select woc.company_id
		from work_order_control woc
		where woc.work_order_number = a.work_order_number
	)
	;
	if uf_check_sql() = -1 then return -1
	*/
	
	
	////	BCM 20130110 - if company_sub_account_control is populated, validate that the sub account
	////	is valid for the given company.
	
	select count(*) into :csac_count from company_sub_account_control;

	
	if csac_count > 0 then
		update wo_interface_unit a
		set a.error_message = a.error_message || 'Unit - sub account not valid for given company; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.company_id is not null
		and a.sub_account_id is not null
		and trim(a.sub_account_id) not in (
			select trim(csac.sub_account_id)
			from company_sub_account_control csac
			where (csac.company_id = :a_company or :a_company = -1)
		)
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	////	BCM 20130110
	////	Validate that the work order Functional Class agrees with the utility account Functional Class
	////	where the WOEST - Filter UA by FC on open is set to yes.  If Functional Class is null, let rows pass.
	
	ua_fc_filter = lower(trim(f_pp_system_control_company('WOEST - Filter UA by FC on open', a_company)))	
	
	if ua_fc_filter = 'yes' then
		//	Flag records where work order functional class <> utility account functional class
		update wo_interface_unit a
		set a.error_message = a.error_message || ' Unit - work order functional class does not agree with estimate utility account functional class; '
		where exists (	
				select 1
				from utility_account ua, work_order_account woa
				where ua.utility_account_id = a.utility_account_id
				and ua.bus_segment_id = a.bus_segment_id
				and woa.work_order_id = a.work_order_id 
				and ua.func_class_id <> nvl(woa.func_class_id,ua.func_class_id))
		and a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is not null
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// CHECK FOR PROCESSING ERRORS
	//  - do not process a work order if REPLACING and ANY of the records for that work order are in error
	//  - do not process a job task if REPLACING and ANY of the records for that job task are in error
	//  - do not process any OCRs whose corresponding estimate_id is in error due to the above conditions
	//
	//--------------------------------------------------------------------------------
	if a_replace then
		uf_msgs("Flagging Processing Errors - WO")
		
		// If replacing, all records for a work order must be valid (no errors)
		update wo_interface_unit a
		set a.error_message = a.error_message || 'Unit - cannot process work orders due to errors; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and exists (
			select 1 from wo_interface_unit p
			where p.work_order_id = a.work_order_id
			and p.error_message is not null
			)
		and a.process_level = 'wo'
		;
		if uf_check_sql() = -1 then return -1
		
		uf_msgs("Flagging Processing Errors - Task")
		
		// If replacing, all records for a job task must be valid (no errors)
		update wo_interface_unit a
		set a.error_message = a.error_message || 'Unit - cannot process job tasks due to errors; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and exists (
			select 1 from wo_interface_unit p
			where p.work_order_id = a.work_order_id
			and p.job_task_id = a.job_task_id
			and p.error_message is not null
			)
		and a.process_level = 'task'
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	//--------------------------------------------------------------------------------
	//
	// REVISION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Revision")
	
	// Clear out old revision numbers
	update wo_interface_unit a
	set a.revision = null
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and nvl(a.revision,0) <> 1000
	;
	if uf_check_sql() = -1 then return -1
	
	// Create new revisions where the max revision is uneditable
	delete from wo_est_processing_temp;
	
	insert into wo_est_processing_temp (work_order_id, revision)
	select distinct a.work_order_id, b.revision
	from wo_interface_unit a,
		(	select work_order_id, max(revision) revision
			from work_order_approval
			group by work_order_id
		) b,
		work_order_approval c
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.revision is null
	and a.error_message is null
	and a.work_order_id = b.work_order_id
	and b.work_order_id = c.work_order_id
	and b.revision = c.revision
	and (c.approval_status_id in (2,3,5) 
		or ((select lower(trim(p.control_value)) from pp_system_control_companies p
		where p.company_id = a.company_id
		and lower(trim(p.control_name)) = lower('WOEST - API Always New Revision')
		) = 'yes'
		and exists (select 1 from wo_estimate z where b.work_order_id = z.work_order_id and b.revision = z.revision)))
	;
	if uf_check_sql() = -1 then return -1
	
	if sqlca.sqlnrows > 0 then
		uo_rev = create uo_budget_revision
		
		uo_rev.i_messagebox_onerror = false
		uo_rev.i_pp_msgs = true
		uo_rev.i_add_time_to_messages = true
		uo_rev.i_display_msg_on_sql_success = true
		uo_rev.i_display_rowcount = true
		
		rtn = uo_rev.uf_new_revision('wo')
		
		destroy uo_rev
		
		if rtn = -1 then
			uf_msgs("ERROR: Error creating new revisions")
			return -1
		end if
	end if
	
	// Map on max revision where it is editable
	update wo_interface_unit a
	set a.revision = (
		select max(p.revision)
		from work_order_approval p
		where p.work_order_id = a.work_order_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.revision is null
	and exists (
		select 1 from
			(	select work_order_id, max(revision) revision
				from work_order_approval
				group by work_order_id
			) b,
			work_order_approval c
		where a.work_order_id = b.work_order_id
		and b.work_order_id = c.work_order_id
		and b.revision = c.revision
		and c.approval_status_id not in (2,3,5)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing revision as errors
	update wo_interface_unit a
	set a.error_message = a.error_message || 'Unit - cannot derive revision; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and revision is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	
	//--------------------------------------------------------------------------------
	//
	// CHECK FOR UNITIZED ESTIMATES IF REPLACING
	//
	//--------------------------------------------------------------------------------
	if a_replace then
		uf_msgs("Flagging Unitized Estimates - WO")
		
		// Flag work order records as errors if there are already unitized estimates and the interface is trying to replace them
		update wo_interface_unit a
		set a.error_message = a.error_message || 'Unit - cannot replace unitized work order estimates; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and exists (
			select 1 from wo_estimate e
			where e.work_order_id = a.work_order_id
			and e.revision = a.revision
			and e.batch_unit_item_id is not null
			)
		and a.process_level = 'wo'
		;
		if uf_check_sql() = -1 then return -1
		
		uf_msgs("Flagging Unitized Estimates - Task")
		
		// Flag job task records as errors if there are already unitized estimates and the interface is trying to replace them
		update wo_interface_unit a
		set a.error_message = a.error_message || 'Unit - cannot replace unitized job task estimates; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and exists (
			select 1 from wo_estimate e
			where e.work_order_id = a.work_order_id
			and e.revision = a.revision
			and e.job_task_id = a.job_task_id
			and e.batch_unit_item_id is not null
			)
		and a.process_level = 'task'
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// POPULATE ESTIMATE_ID
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Populating Estimate Ids")
	
	// backfill estimate_ids
	update wo_interface_unit a
	set (a.estimate_id) = pwrplant1.nextval
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	;
	if uf_check_sql() = -1 then return -1
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	//--------------------------------------------------------------------------------
	//
	// DELETE EXISTING ESTIMATES IF REPLACING
	//
	//--------------------------------------------------------------------------------
	if a_replace then
		uf_msgs("Deleting All Work Order Estimates Being Replaced")
		
		// Delete old work order estimates for existing revisions
		// ### MJS - Adding a line where to not delete the Retirements
		// adding "and a.error_message is null" and "and z.batch_unit_item_id is null"
		delete from wo_estimate z
		where (z.work_order_id, z.revision) in (
			select distinct a.work_order_id, a.revision
			from wo_interface_unit a
			where a.status = 1
			and nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.process_level = 'wo'
			and a.error_message is null
			)
		and not (est_chg_type_id in (select ect.est_chg_type_id from estimate_charge_Type ect where processing_type_id = 1) and expenditure_type_id = 2)
		and z.batch_unit_item_id is null
		;
		if uf_check_sql() = -1 then return -1
		
		uf_msgs("Deleting All Job Task Estimates Being Replaced")
		
		// Delete old job task estimates for existing revisions
		// ### MJS - Adding a line where to not delete the Retirements
		// adding "and a.error_message is null" and "and z.batch_unit_item_id is null"
		delete from wo_estimate z
		where (z.work_order_id, z.revision, z.job_task_id) in (
			select distinct a.work_order_id, a.revision, a.job_task_id
			from wo_interface_unit a
			where a.status = 1
			and nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.process_level = 'task'
			and a.error_message is null
			)
			and not (est_chg_type_id in (select ect.est_chg_type_id from estimate_charge_Type ect where processing_type_id = 1) and expenditure_type_id = 2)
			and z.batch_unit_item_id is null
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT UNIT ESTIMATES
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Unit Estimates")
	
	// Insert into wo_estimates
	insert into wo_estimate (
		estimate_id, revision, work_order_id, expenditure_type_id, job_task_id, utility_account_id, est_chg_type_id,
		sub_account_id, stck_keep_unit_id, retirement_unit_id, bus_segment_id, asset_id, quantity, hours,
		amount, notes, department_id, property_group_id, asset_location_id, replacement_amount,
		serial_number, wo_est_trans_type_id, est_in_service_date, unit_desc, unit_long_desc, percent, field_1,
		field_2, field_3, field_4, field_5, retire_vintage
		)
	select estimate_id, revision, work_order_id, expenditure_type_id, job_task_id, utility_account_id, est_chg_type_id,
		sub_account_id, stck_keep_unit_id, retirement_unit_id, bus_segment_id, asset_id, nvl(quantity,0), nvl(hours,0),
		nvl(amount,0), notes, department_id, property_group_id, asset_location_id, replacement_amount,
		serial_number, wo_est_trans_type_id, est_in_service_date, unit_desc, unit_long_desc, percent, field_1,
		field_2, field_3, field_4, field_5, retire_vintage
	from wo_interface_unit a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// SEND UNIT ESTIMATES TO MONTHLY ESTIMATES
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Sending Unit Estimates to Monthly Estimates")
	
	// Insert into wo_estimates
	uo_rev = create uo_budget_revision
	
	uo_rev.i_messagebox_onerror = false
	uo_rev.i_pp_msgs = true
	uo_rev.i_add_time_to_messages = true
	uo_rev.i_display_msg_on_sql_success = true
	uo_rev.i_display_rowcount = true
	
	delete from wo_est_processing_temp;
	
	insert into wo_est_processing_temp (work_order_id, revision)
	select distinct a.work_order_id, a.revision
	from wo_interface_unit a
	where a.revision <> 1000
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.error_message is null
	and a.funding_wo_indicator = 0
	and exists (
		select 1 from pp_system_control_companies p
		where p.company_id = a.company_id
		and lower(trim(p.control_name)) = lower('woest - copy wo wo_estimate to wem')
		and lower(trim(p.control_value)) = lower('yes')
		);
	
	if sqlca.sqlnrows > 0 then
		rtn = uo_rev.uf_wo_to_wem()
	end if
	
	if rtn = -1 then
		uf_msgs("ERROR: Error sending to monthly estimates - WO")
		destroy uo_rev
		return -1
     end if
	
	delete from wo_est_processing_temp;
		
	insert into wo_est_processing_temp (work_order_id, revision)
	select distinct a.work_order_id, a.revision
	from wo_interface_unit a
	where a.revision <> 1000
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.error_message is null
	and a.funding_wo_indicator = 1
	and exists (
		select 1 from pp_system_control_companies p
		where p.company_id = a.company_id
		and lower(trim(p.control_name)) = lower('fpest - copy fp wo_estimate to wem')
		and lower(trim(p.control_value)) = lower('yes')
		);
	
	if sqlca.sqlnrows > 0 then
		rtn = uo_rev.uf_wo_to_wem()
	end if
	
	if rtn = -1 then
		uf_msgs("ERROR: Error sending to monthly estimates - FP")
		destroy uo_rev
		return -1
     end if
  
	destroy uo_rev
	
	//--------------------------------------------------------------------------------
	//
	// LOAD ORIGINAL COST RETIREMENTS
	//
	//--------------------------------------------------------------------------------
	if a_process_ocr then
		
		// Get the list of estimate_ids being loaded for original cost retirements processing below
		sqls = &
		"select distinct work_order_id " +&
		"from wo_interface_unit a " +&
		"where a.status = 2 " +&
		"and nvl(a.batch_id,'null') = '"+a_batch+"' " +&
		"and (a.company_id = "+string(a_company)+" or "+string(a_company)+" = -1) " +&
		"and expenditure_type_id = 2 " +&
		"and retirement_unit_id is not null " +&
		"and utility_account_id is not null " +&
		"and asset_location_id is not null "
		
		cnt = f_get_column(results,sqls)
		
		if cnt > 0 then
			uo_ocr = create uo_original_cost_retirement
		end if
		
		for i = 1 to cnt
			uf_msgs("Processing Original Cost Retirements for Loaded Estimates")
			// function is currently stubbed and will always return a warning.
			rtn = uo_ocr.uf_process_ocr_from_estimate(results[i])
			
			if rtn < 0 then
				uf_msgs("WARNING: Work Order ID " + results[i] + " was unable to create OCR records.  Return="+string(rtn))
				// need to think about rolling back estimate errors when the OCRs error for the next version.
			end if
		next
	end if	
	
	//--------------------------------------------------------------------------------
	//
	// MARK RECORDS AS PROCESSED
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Interface Status Update")
	
	// Update status to indicate it has been processed
	update wo_interface_unit a
	set status = 2
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1	
next


//--------------------------------------------------------------------------------
//
// ARCHIVE RECORDS
//
//--------------------------------------------------------------------------------
uf_msgs("Archiving Records")

// Insert into archive table
insert into wo_interface_unit_arc (
	work_order_number, estimate_id, revision, work_order_id, expenditure_type_id, job_task_id, utility_account_id, est_chg_type_id, sub_account_id, 
	stck_keep_unit_id, retirement_unit_id, bus_segment_id, user_id, asset_id, quantity, hours, time_stamp, amount, notes, department_id, property_group_id, 
	asset_location_id, replacement_amount, serial_number, wo_est_trans_type_id, est_in_service_date, status, unit_desc, unit_long_desc, percent, field_1,
	field_2, field_3, field_4, field_5, company_id, batch_id, ext_company, funding_wo_indicator, ext_expenditure_type, ext_est_chg_type, ext_stck_keep_unit,
	ext_wo_est_trans_type, ext_department, ext_job_task, ext_bus_segment, ext_utility_account, ext_sub_account, ext_property_group, ext_retirement_unit,
	ext_asset_location, retire_vintage, seq_id, row_id, process_level
	)
select work_order_number, estimate_id, revision, work_order_id, expenditure_type_id, job_task_id, utility_account_id, est_chg_type_id, sub_account_id, 
	stck_keep_unit_id, retirement_unit_id, bus_segment_id, user_id, asset_id, quantity, hours, time_stamp, amount, notes, department_id, property_group_id, 
	asset_location_id, replacement_amount, serial_number, wo_est_trans_type_id, est_in_service_date, status, unit_desc, unit_long_desc, percent, field_1,
	field_2, field_3, field_4, field_5, company_id, batch_id, ext_company, funding_wo_indicator, ext_expenditure_type, ext_est_chg_type, ext_stck_keep_unit,
	ext_wo_est_trans_type, ext_department, ext_job_task, ext_bus_segment, ext_utility_account, ext_sub_account, ext_property_group, ext_retirement_unit,
	ext_asset_location, retire_vintage, seq_id, row_id, process_level
from wo_interface_unit a
where nvl(status,1) = 2
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.error_message is null
;
if uf_check_sql() = -1 then return -1

// Delete Archived Data
delete from wo_interface_unit a
where nvl(a.status,1) = 2
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.error_message is null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// COMMIT
//
//--------------------------------------------------------------------------------
uf_msgs("Committing records")

// Commit here
commit;

uf_msgs("Finished WO Unit Estimate Loading - Company Id = "+string(a_company))
uf_msgs("------------------------------- ")
uf_msgs("------------------------------- ")


return 1
end function

public function integer uf_create_fps (longlong a_company);longlong rtn
string null_str
setnull(null_str)

rtn = uf_create_fps(a_company, null_str)

return rtn

end function

public function integer uf_create_wos (longlong a_company);longlong rtn
string null_str
setnull(null_str)

rtn = uf_create_wos(a_company, null_str)

return rtn

end function

public function integer uf_create_wos_fps (longlong a_company, string a_batch, longlong a_funding_wo_ind);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_wo_interface.uf_create_wos_fps
//
//   Purpose  :   Creates Work Orders / Funding Projects from pre-staged data in the table wo_interface_staging.
//					Assumes that the Funding Project Number, Work_order_type_id and desciption are not null in the wo_interface_staging table.
//					Updates valid entrys to status of 2.
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
longlong checkers, counts, i, rtn, update_counts, num_batches, ii
any results[]
string workflow_appr, cc_sqls_wocc, cc_sqls_bcc, co_validate, bud_sum, bud_org, bv_by_company, bi_workflow_appr, wo_fp, wos_fps, class_code_sql

// PP-44415
if isnull(a_funding_wo_ind) then a_funding_wo_ind = 0

if a_funding_wo_ind = 1 then
	wo_fp = 'FP'
	wos_fps = 'Funding Project'
else
	a_funding_wo_ind = 0
	wo_fp = 'WO'
	wos_fps = 'Work Order'
end if

if isnull(a_batch) then a_batch = 'null'
if isnull(a_company) then a_company = -1

uf_msgs("------------------------------- ")
uf_msgs("------------------------------- ")
uf_msgs("Begin "+wo_fp+" Loading - Company Id = "+string(a_company))


//--------------------------------------------------------------------------------
//
// PROCESSING STATUS
//
//--------------------------------------------------------------------------------
uf_msgs("Setting Status")
// PP-44415
update wo_interface_staging a
set status = 1,
	funding_wo_indicator = nvl(a.funding_wo_indicator, 0),
	error_message = null,
	work_order_number = upper(a.work_order_number),
	funding_proj_number = upper(a.funding_proj_number)
where nvl(status,1) <> 2
and nvl(batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1) /* IAES - Maint - 31327*/
and nvl(funding_wo_indicator,0) = :a_funding_wo_ind
;
if uf_check_sql() = -1 then return -1 


//--------------------------------------------------------------------------------
//
// COMPANY
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Company")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on gl_company_no
update wo_interface_staging a
set a.company_id = (
	select p.company_id
	from company_setup p
	where p.gl_company_no = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.funding_wo_indicator = :a_funding_wo_ind
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Map on company description
update wo_interface_staging a
set a.company_id = (
	select p.company_id
	from company_setup p
	where p.description = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.funding_wo_indicator = :a_funding_wo_ind
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Map on company_id
update wo_interface_staging a
set a.company_id = (
	select p.company_id
	from company_setup p
	where to_char(p.company_id) = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.funding_wo_indicator = :a_funding_wo_ind
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

// Flag records where ext_company could not be mapped
update wo_interface_staging a
set a.error_message = a.error_message || :wo_fp || ' - cannot derive company_id; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.funding_wo_indicator = :a_funding_wo_ind
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql() = -1 then return -1

////
////	No external value was provided
////

// Flag records missing company_id as errors
update wo_interface_staging a
set a.error_message = a.error_message || :wo_fp || ' - company_id is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.funding_wo_indicator = :a_funding_wo_ind
and a.company_id is null
and a.ext_company is null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// WORK ORDER NUMBER
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Validating "+wos_fps+" Number")

// Flag records missing work_order_number as errors
update wo_interface_staging a
set a.error_message = a.error_message || :wo_fp || ' - work_order_number is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and a.funding_wo_indicator = :a_funding_wo_ind
and a.work_order_number is null
;
if uf_check_sql() = -1 then return -1
// Field is not null...this should never find anything


//--------------------------------------------------------------------------------
//
// VALIDATION
//   - must have a valid COMPANY_ID and WORK_ORDER_NUMBER
//
//--------------------------------------------------------------------------------
uf_msgs("Flagging Errors (missing Company or Work Order Number)")

// Flag records that already have errors as unable to continue processing (either invalid company_id or work_order_number)
update wo_interface_staging a
set a.status = -11
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and funding_wo_indicator = :a_funding_wo_ind
and a.error_message is not null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// DETERMINE THE NUMBER OF BATCHES
//   - Loop over batches of work orders
//
//--------------------------------------------------------------------------------
uf_msgs("Identifying Process Batches")

// Determine the number of loops needed to process all work orders in sequence
select max(batches)
into :num_batches
from (
	select work_order_number, company_id, count(distinct nvl(seq_id,0)) batches
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	group by work_order_number, company_id
	)
;
if uf_check_sql() = -1 then return -1

uf_msgs("Updating status to Pending")

// Update records that have not yet been processed to status = 3 (pending
update wo_interface_staging a
set a.status = 3 // pending
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.funding_wo_indicator = :a_funding_wo_ind
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// LOOP OVER THE BATCHES
//
//--------------------------------------------------------------------------------
for ii = 1 to num_batches
	uf_msgs(" ")
	uf_msgs("***** Batch "+string(ii)+" of "+string(num_batches)+" *****")
	uf_msgs(" ")
	
	uf_msgs("Updating status to In-Process")
	update wo_interface_staging a
	set a.status = 1 // in-process
	where a.status = 3
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and nvl(seq_id,0) in (
		select min(nvl(seq_id,0))
		from wo_interface_staging b
		where b.status = 3
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and b.funding_wo_indicator = :a_funding_wo_ind
		and b.work_order_number = a.work_order_number
		and b.company_id = a.company_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// DETERMINE ACTION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Setting Action")
	
	// Reset action code
	update wo_interface_staging a
	set a.action = null,
		a.work_order_id = null
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	;
	if uf_check_sql() = -1 then return -1
	
	// Identify "update" records
	update wo_interface_staging a
	set a.action = 'U',
		(a.work_order_id, a.old_wo_status_id) = (
			select b.work_order_id, b.wo_status_id
			from work_order_control b 
			where b.work_order_number = a.work_order_number
			and b.company_id = a.company_id
			and b.funding_wo_indicator = a.funding_wo_indicator
			)
	where exists (
		select 1 from work_order_control b 
		where b.work_order_number = a.work_order_number
		and b.company_id = a.company_id
		and b.funding_wo_indicator = a.funding_wo_indicator
		)
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	;
	if uf_check_sql() = -1 then return -1
	
	// Identify "insert" records
	update wo_interface_staging a
	set action = 'I',
		work_order_id = pwrplant1.nextval,
		old_wo_status_id = null
	where nvl(action,'I') <> 'U'
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	;
	if uf_check_sql() = -1 then return -1
	
	
	// This should never find anything anyways because we are processing in batches
	////--------------------------------------------------------------------------------
	////
	//// SETTING OLD RECORDS AS PROCESSED BASED ON SEQUENCE (SEQ_ID)
	////   If a newer record for the same work order / funding project already exists, mark the
	////   old one as processed, and continue processing the latest one
	////
	////--------------------------------------------------------------------------------
	//uf_msgs("Checking Sequence")
	//update wo_interface_staging a
	//set a.status = 2
	//where a.status = 1
	//and nvl(a.batch_id,'null') = :a_batch
	//and (a.company_id = :a_company or :a_company = -1)
	//and funding_wo_indicator = :a_funding_wo_ind
	//and nvl(a.seq_id,0) < (
	//	select nvl(max(b.seq_id),0)
	//	from wo_interface_staging b
	//	where b.status = 1
	//	and nvl(b.batch_id,'null') = :a_batch
	//	and (b.company_id = :a_company or :a_company = -1)
	//	and b.funding_wo_indicator = :a_funding_wo_ind
	//	and b.work_order_id = a.work_order_id
	//	)
	//;
	//if uf_check_sql() = -1 then return -1
	
	// Flag duplicate records where the same work order / funding project for the same company exists more than once
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - duplicate records exist for this ' || lower(:wos_fps) || ' with identical/blank seq_ids or there was a lower sequence id that had failed and therefore this record cannot be processed until the prior record is fixed.; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.work_order_number, a.company_id)  in (
		select c.work_order_number, c.company_id
		from wo_interface_staging c
		where c.status = 1
		and nvl(c.batch_id,'null') = :a_batch
		and (c.company_id = :a_company or :a_company = -1)
		and c.funding_wo_indicator = :a_funding_wo_ind
		group by c.work_order_number, c.company_id
		having count(*) > 1
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	//--------------------------------------------------------------------------------
	//
	// COUNT RECORDS TO PROCESS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Checking for records to process")
	
	// Look for any records to process whether Insert or Update
	select count(*) into :counts
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	;
	
	if counts = 0 then
		uf_msgs("0 records to process")
		uf_msgs("-------------------------------")
		uf_msgs("-------------------------------")
		return 1
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// DESCRIPTION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Validating "+wos_fps+" Description")
	
	// Flag records missing work_order_number as errors - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - description is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.description is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// WORK ORDER TYPE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Work Order Type")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_work_order_type
	update wo_interface_staging a
	set a.work_order_type_id = (
		select p.work_order_type_id
		from work_order_type p
		where p.external_work_order_type = a.ext_work_order_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_type_id is null
	and a.ext_work_order_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on work_order_type description
	update wo_interface_staging a
	set a.work_order_type_id = (
		select p.work_order_type_id
		from work_order_type p
		where p.description = a.ext_work_order_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_type_id is null
	and a.ext_work_order_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on work_order_type_id
	update wo_interface_staging a
	set a.work_order_type_id = (
		select p.work_order_type_id
		from work_order_type p
		where to_char(p.work_order_type_id) = a.ext_work_order_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_type_id is null
	and a.ext_work_order_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_work_order_type could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive work_order_type_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_type_id is null
	and a.ext_work_order_type is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	// Flag records missing work_order_type_id as errors - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - work_order_type_id is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_type_id is null
	and a.ext_work_order_type is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	if a_funding_wo_ind = 0 then
		// ONLY FOR WORK ORDERS
		
		//--------------------------------------------------------------------------------
		//
		// FUNDING PROJECT
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Mapping Funding Project")
		
		////
		////	If an external value is provided, try to map on it and error out if it cannot be mapped
		////
		
		// Map on work_order_number
		update wo_interface_staging a
		set a.funding_wo_id = (
			select p.work_order_id
			from work_order_control p
			where p.funding_wo_indicator = 1
			and p.work_order_number = a.funding_proj_number
			and p.company_id = a.company_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.funding_wo_id is null
		and a.funding_proj_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on external_wo_number
		update wo_interface_staging a
		set a.funding_wo_id = (
			select p.work_order_id
			from work_order_control p
			where p.funding_wo_indicator = 1
			and p.external_wo_number = a.funding_proj_number
			and p.company_id = a.company_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.funding_wo_id is null
		and a.funding_proj_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on description
		update wo_interface_staging a
		set a.funding_wo_id = (
			select p.work_order_id
			from work_order_control p
			where p.funding_wo_indicator = 1
			and p.description = a.funding_proj_number
			and p.company_id = a.company_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.funding_wo_id is null
		and a.funding_proj_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on work_order_id
		update wo_interface_staging a
		set a.funding_wo_id = (
			select p.work_order_id
			from work_order_control p
			where p.funding_wo_indicator = 1
			and to_char(p.work_order_id) = a.funding_proj_number
			and p.company_id = a.company_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.funding_wo_id is null
		and a.funding_proj_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Flag records where funding_proj_number could not be mapped
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - cannot derive funding_wo_id; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.funding_wo_id is null
		and a.funding_proj_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		////
		////	No external value was provided
		////
		
		// Flag records missing funding_wo_id as errors - ONLY FOR INSERTS
		update wo_interface_staging a
		//set a.status = -12
		set a.error_message = a.error_message || :wo_fp || ' - funding_wo_id is required; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.funding_wo_id is null
		and a.funding_proj_number is null
		and a.action = 'I'
		;
		if uf_check_sql() = -1 then return -1
		
	else
		// ONLY FOR FUNDING PROJECTS
		
		//--------------------------------------------------------------------------------
		//
		// FUNDING WO ID
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Updating Funding Wo Id to null")
		
		// Funding Projects should have a null funding_wo_id
		update wo_interface_staging a
		set funding_wo_id = null
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	
	if a_funding_wo_ind = 0 then
		// ONLY FOR WORK ORDERS
		
		//--------------------------------------------------------------------------------
		//
		// BUDGET ITEM
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Mapping Budget Item")
		
		////
		//// Whether inserting or updating, the budget_id always comes from the funding project
		////
		
		// Map on funding_wo_id
		update wo_interface_staging a
		set a.budget_id = (
			select p.budget_id
			from work_order_control p
			where p.work_order_id = a.funding_wo_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		//and a.budget_id is null -- must come from the Funding Project
		and a.funding_wo_id is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Budget Item must come from the Funding Project, so if the funding project is null (i.e. not being updated), then null out the budget item so that it is not updated
		update wo_interface_staging a
		set a.budget_id = null
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.funding_wo_id is null
		;
		if uf_check_sql() = -1 then return -1
		
		// Flag records missing budget_id as errors - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - budget_id is required; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.budget_id is null
		and a.action = 'I'
		;
		if uf_check_sql() = -1 then return -1
		
	else
		// ONLY FOR FUNDING PROJECTS
		
		//--------------------------------------------------------------------------------
		//
		// BUDGET ITEM
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Mapping Budget Item")
		
		// Check if company on budget item and funding project must be the same
		co_validate = lower(trim(f_pp_system_control_company('FPINT-Validate WO TYPE/Bud Item Co', a_company)))
		if isnull(co_validate) then
			co_validate = 'yes'
		end if
		
		if co_validate = 'yes' then
			// Update budget_company_id
			update wo_interface_staging a
			set a.budget_company_id = a.company_id
			where a.status = 1
			and nvl(a.batch_id,'null') = :a_batch
			and (a.company_id = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_ind
			;
			if uf_check_sql() = -1 then return -1
		end if
		
		////
		////	If an external value is provided, try to map on it and error out if it cannot be mapped
		////
		
		// Map on budget number
		update wo_interface_staging a
		set a.budget_id = (
			select b.budget_id
			from budget b
			where b.budget_number = a.budget_number
			and b.company_id = nvl(a.budget_company_id, a.company_id)
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.budget_id is null
		and a.budget_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on description
		update wo_interface_staging a
		set a.budget_id = (
			select b.budget_id
			from budget b
			where b.description = a.budget_number
			and b.company_id = nvl(a.budget_company_id, a.company_id)
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.budget_id is null
		and a.budget_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on budget_id
		update wo_interface_staging a
		set a.budget_id = (
			select b.budget_id
			from budget b
			where to_char(b.budget_id) = a.budget_number
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.budget_id is null
		and a.budget_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Flag records where budget_number could not be mapped
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - cannot derive budget_id; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.budget_id is null
		and a.budget_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		////
		////	No external value was provided
		////
		
		// Flag records where creating a new budget_number from this work_order_number will cause problems
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - cannot create a new budget item from this FP number. Either change the FP number or specify a budget item; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.budget_id is null
		and a.budget_number is null
		and (a.company_id, a.work_order_number) in (
			select b.company_id, b.budget_number
			from budget b
			)
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// BUSINESS SEGMENT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Business Segment")
	
	////
	////	When updating, bus_segment_id must be null - cannot be updated
	////
	
	// Null out bus_segment_id...cannot be changed on a work order or funding project - ONLY FOR UPDATES
	update wo_interface_staging a
	set a.bus_segment_id = null
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	When inserting, bus_segment_id must come from the work_order_type
	////
	
	// Map on work order type - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.bus_segment_id = (
		select b.bus_segment_id
		from work_order_type b
		where b.work_order_type_id = a.work_order_type_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	//and a.bus_segment_id is null --must always come from the Work Order Type
	and a.work_order_type_id is not null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing bus_segment_id as errors - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive bus_segment_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.bus_segment_id is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// ASSET LOCATION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Asset Location")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on ext_asset_location
	update wo_interface_staging a
	set a.asset_location_id = (
		select b.asset_location_id
		from asset_location b
		where b.ext_asset_location = a.ext_asset_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on asset location description
	update wo_interface_staging a
	set a.asset_location_id = (
		select b.asset_location_id
		from asset_location b
		where b.long_description = a.ext_asset_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on asset_location_id
	update wo_interface_staging a
	set a.asset_location_id = (
		select b.asset_location_id
		from asset_location b
		where to_char(b.asset_location_id) = a.ext_asset_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_asset_location could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive asset_location_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.asset_location_id is null
	and a.ext_asset_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////		For work orders, try to derive some other way
	////
	
	if a_funding_wo_ind = 0 then
		// ONLY FOR WORK ORDERS
		
		// Map on funding project - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.asset_location_id = (
			select b.asset_location_id
			from work_order_control b
			where b.work_order_id = a.funding_wo_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.asset_location_id is null
		and a.funding_wo_id is not null
		and a.ext_asset_location is null
		and a.action = 'I'
		;
		if uf_check_sql() = -1 then return -1
		
		// Obey system control that requires asset_location_id upon initiation - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - asset_location_id is required; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and exists (
			select 1 from pp_system_control_companies p
			where p.company_id = a.company_id
			and lower(trim(p.control_name)) = lower('WOINIT - Require Location')
			and lower(trim(p.control_value)) = 'yes'
			)
		and a.asset_location_id is null
		and a.ext_asset_location is null
		and a.action = 'I'
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// MAJOR LOCATION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Major Location")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_location_id
	update wo_interface_staging a
	set a.major_location_id = (
		select b.major_location_id
		from major_location b
		where b.external_location_id = a.ext_major_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.major_location_id is null
	and a.ext_major_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on major location description
	update wo_interface_staging a
	set a.major_location_id = (
		select b.major_location_id
		from major_location b
		where b.description = a.ext_major_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.major_location_id is null
	and a.ext_major_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on major_location_id
	update wo_interface_staging a
	set a.major_location_id = (
		select b.major_location_id
		from major_location b
		where to_char(b.major_location_id) = a.ext_major_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.major_location_id is null
	and a.ext_major_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_major_location could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive major_location_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.major_location_id is null
	and a.ext_major_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided, try to derive some other way
	////
	
	// If we were able to derive an asset_location_id above, then map on asset_location_id 
	update wo_interface_staging a
	set a.major_location_id = (
		select b.major_location_id
		from asset_location b
		where b.asset_location_id = a.asset_location_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.major_location_id is null
	and a.asset_location_id is not null
	and a.ext_major_location is null
	;
	if uf_check_sql() = -1 then return -1
	
	if a_funding_wo_ind = 0 then
		// ONLY FOR WORK ORDERS
		
		// Map on funding project - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.major_location_id = (
			select b.major_location_id
			from work_order_control b
			where b.work_order_id = a.funding_wo_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.major_location_id is null
		and a.funding_wo_id is not null
		and a.ext_major_location is null
		and a.action = 'I'
		and a.asset_location_id is null
		and a.ext_asset_location is null
		and not exists (
			select 1 from pp_system_control_companies p
			where p.company_id = a.company_id
			and lower(trim(p.control_name)) = lower('WOINIT - Require Location')
			and lower(trim(p.control_value)) = 'yes'
			)
		;
		if uf_check_sql() = -1 then return -1
		
	else
		// ONLY FOR FUNDING PROJECTS
		
		// Map on budget item - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.major_location_id = (
			select b.major_location_id
			from budget b
			where b.budget_id = a.budget_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.major_location_id is null
		and a.budget_id is not null
		and a.ext_major_location is null
		and a.action = 'I'
		and a.asset_location_id is null
		and a.ext_asset_location is null
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	// Flag records missing major_location_id as errors - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - major_location_id is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.major_location_id is null
	and a.ext_major_location is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// DEPARTMENT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Department")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_department_code
	update wo_interface_staging a
	set a.department_id = (
		select b.department_id
		from department b
		where b.external_department_code = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on department description
	update wo_interface_staging a
	set a.department_id = (
		select b.department_id
		from department b
		where b.description = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on department_id
	update wo_interface_staging a
	set a.department_id = (
		select b.department_id
		from department b
		where to_char(b.department_id) = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_department could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive department_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.department_id is null
	and a.ext_department is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided, try to derive some other way
	////
	
	if a_funding_wo_ind = 0 then
		// ONLY FOR WORK ORDERS
		
		// Map on funding project department - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.department_id = (
			select b.department_id
			from work_order_control b
			where b.work_order_id = a.funding_wo_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.department_id is null
		and a.funding_wo_id is not null
		and a.ext_department is null
		and a.action = 'I'
		;
		if uf_check_sql() = -1 then return -1
		
	else
		// ONLY FOR FUNDING PROJECTS
		
		// Map on budget department - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.department_id = (
			select b.department_id
			from budget b
			where b.budget_id = a.budget_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.department_id is null
		and a.budget_id is not null
		and a.ext_department is null
		and a.action = 'I'
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	// Flag records missing department_id as errors - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - department_id is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.department_id is null
	and a.ext_department is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// WORK ORDER GROUP
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Work Order Group")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on work order group description
	update wo_interface_staging a
	set a.work_order_grp_id = (
		select b.work_order_grp_id
		from work_order_group b
		where b.description = a.ext_work_order_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_grp_id is null
	and a.ext_work_order_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on work_order_grp_id
	update wo_interface_staging a
	set a.work_order_grp_id = (
		select b.work_order_grp_id
		from work_order_group b
		where to_char(b.work_order_grp_id) = a.ext_work_order_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_grp_id is null
	and a.ext_work_order_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_department could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive work_order_grp_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_grp_id is null
	and a.ext_work_order_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided, try to derive some other way
	////
	
	// Map on work order type - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.work_order_grp_id = (
		select b.work_order_grp_id
		from wo_grp_wo_type b
		where b.work_order_type_id = a.work_order_type_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.work_order_grp_id is null
	and a.ext_work_order_group is null
	and a.action = 'I'
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// WORK ORDER STATUS
	//    MUST populate BOTH wo_status_id AND all corresponding status dates in order to process status updates
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Work Order Status")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external work order status
	update wo_interface_staging a
	set a.wo_status_id = (
		select b.wo_status_id
		from work_order_status b
		where b.external_work_order_status = a.ext_work_order_status
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id is null
	and a.ext_work_order_status is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on work order status description
	update wo_interface_staging a
	set a.wo_status_id = (
		select b.wo_status_id
		from work_order_status b
		where b.description = a.ext_work_order_status
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id is null
	and a.ext_work_order_status is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on wo status id
	update wo_interface_staging a
	set a.wo_status_id = (
		select b.wo_status_id
		from work_order_status b
		where to_char(b.wo_status_id) = a.ext_work_order_status
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id is null
	and a.ext_work_order_status is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_work_order_status could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive wo_status_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id is null
	and a.ext_work_order_status is not null
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	If wo_status_id is provided, that means we are updating the status
	////		Validate that the appropriate dates are populated - ALL STATUS DATES MUST BE PROVIDED WHEN WO_STATUS_ID IS POPULATED, including null dates
	////		Status dates include:
	////			- in_service_date
	////			- completion_date
	////			- close_date
	////			- suspended_date
	////		Validate that the status is changing from a valid previous status
	////
	
	// Flag records where wo_status_id and dates are out of sync - Cancelled
	// - Cancelled can only happen for work orders in an initiated, open, or suspended status.  Status dates should agree with one of those statuses
	// - Main app allows for cancellation of work orders in in-service or completed status only if they have net-zero exp type 1, exp type 2, AND non-unitized 
	// -          charges in cwip_charge. This should be a very rare occurrence and not functionality we need to add to an interface.
	// - Cannot Cancel from Unitized or Posted to CPR status
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (Cancelled - invalid dates or previous status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 8
	and (
		not (
			(close_date is not null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = initiated OR open*/ OR
			(close_date is not null and completion_date is null and in_service_date is null and suspended_date is not null) /*dates = suspended*/
			)
		OR
		nvl(old_wo_status_id,8) in (4,5,6,7)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id and dates are out of sync - Posted to CPR
	// !!!!! Posted to CPR status is ONLY allowed when initiating new work orders, presumably for conversion purposes !!!!!
	// !!!!! This logic does NOT allow changing the work order from any other status to Posted to CPR !!!!!
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (Posted to CPR - invalid dates or previous status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 7
	and (
		not (close_date is not null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = posted to cpr*/
		OR
		nvl(old_wo_status_id,7) in (1,2,3,4,5,6,8)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id and dates are out of sync - Unitized
	// !!!!! Unitized status is ONLY meaningful inside of PowerPlan workflow. This status cannot be interfaced for any reason !!!!!
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (Unitized - cannot process this status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 6
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id and dates are out of sync - Completed
	// - Complete can only happen for work orders in an Open or In Service status...Initiated or Suspended work orders presumably came through the Open and In Service statuses
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (Completed - invalid dates or previous status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 5
	and (
		not (close_date is null and completion_date is not null and in_service_date is not null and suspended_date is null) /*dates = completed*/
		OR
		nvl(old_wo_status_id,5) in (6,7,8)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id and dates are out of sync - In Service
	// - In Service can only happen for work orders in an Open or Completed status...Initiated or Suspended work orders presumably came through the Open status
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (In Service - invalid dates or previous status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 4
	and (
		not (close_date is null and completion_date is null and in_service_date is not null and suspended_date is null) /*dates = in service*/
		OR
		nvl(old_wo_status_id,4) in (6,7,8)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id and dates are out of sync - Suspended
	// - Suspended can only happen for work orders in an Initiated or Open status
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (Suspended - invalid dates or previous status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 3
	and (
		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is not null) /*dates = initiated OR open*/
		OR
		nvl(old_wo_status_id,3) in (4,5,6,7,8)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id and dates are out of sync - Open
	// - Open can only happen for work orders in an Initiated or Suspended status
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (Open - invalid dates or previous status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 2
	and (
		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = open*/
		OR
		nvl(old_wo_status_id,2) in (4,5,6,7,8)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id and dates are out of sync - Initiated
	// - Initiated can only happen for work orders in a Suspended status
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (Initiated - invalid dates or previous status); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id = 1
	and (
		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = initiated*/
		OR
		nvl(old_wo_status_id,1) in (2,4,5,6,7,8)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id is not a valid id
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - wo_status_id validation error (wo_status_id must be in (1,2,3,4,5,6,7,8)); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id is not null
	and a.wo_status_id not in (1,2,3,4,5,6,7,8)
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No wo_status_id was provided
	////
	
	// Flag records where wo_status_id is not populated but one or more dates are populated
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - one or more status dates are populated...this is not allowed unless populating wo_status_id with a valid status change; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id is null
	and (
		not (close_date is null and completion_date is null and in_service_date is null and suspended_date is null) /*dates = none*/
		)
	;
	if uf_check_sql() = -1 then return -1
	
	// Map Open work orders based on auto-approved approval types - ONLY FOR INSERTS
	workflow_appr = lower(trim(f_pp_system_control_company('WORKFLOW USER OBJECT', a_company)))
	update wo_interface_staging a
	set auto_approved = 0
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_status_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	if workflow_appr = 'yes' then
		update wo_interface_staging a
		set wo_status_id = 2, auto_approved = 1
		where status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and funding_wo_indicator = :a_funding_wo_ind
		and exists (
			select 1 from work_order_type b, workflow_type c
			where b.work_order_type_id = a.work_order_type_id
			and b.approval_type_id = c.workflow_type_id
			and not exists (
				select 1 from workflow_type_rule wtr
				where wtr.workflow_type_id = c.workflow_type_id
				)
			)
		and action = 'I'
		and wo_status_id is null
		and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
		;
		if uf_check_sql() = -1 then return -1
	else
		update wo_interface_staging a
		set wo_status_id = 2, auto_approved = 1
		where status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and funding_wo_indicator = :a_funding_wo_ind
		and exists (
			select 1 from work_order_type b, approval c
			where b.work_order_type_id = a.work_order_type_id
			and b.approval_type_id = c.approval_type_id
			and auth_level1 is null
			and auth_level2 is null
			and auth_level3 is null
			and auth_level4 is null
			and auth_level5 is null
			and auth_level6 is null
			and auth_level7 is null
			and auth_level8 is null
			and auth_level9 is null
			and auth_level10 is null
			)
		and action = 'I'
		and wo_status_id is null
		and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	// Map remaining work orders as Initiated - ONLY FOR INSERTS
	update wo_interface_staging a
	set wo_status_id = 1
	where status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and funding_wo_indicator = :a_funding_wo_ind
	and action = 'I'
	and wo_status_id is null 
	and (close_date is null and completion_date is null and in_service_date is null and suspended_date is null)
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where wo_status_id could not be mapped - ONLY FOR INSERTS
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive wo_status_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and action = 'I'
	and a.wo_status_id is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	if a_funding_wo_ind = 1 then
		// ONLY FOR FUNDING PROJECTS
		
		//--------------------------------------------------------------------------------
		//
		// BUDGET VERSION
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Mapping Current Budget Version")
		
		// Map based on current version by company
		update wo_interface_staging z
		set z.budget_version_id = (
			select a.budget_version_id
			from budget_version a
			where a.current_version = 1
			and a.open_for_entry = 1
			and a.company_id = z.company_id
			)
		where z.status = 1
		and nvl(z.batch_id,'null') = :a_batch
		and (z.company_id = :a_company or :a_company = -1)
		and z.funding_wo_indicator = :a_funding_wo_ind
		and z.action = 'I'
		and z.budget_version_id is null
		and exists (
			select 1 from pp_system_control_companies pp
			where pp.company_id = z.company_id
			and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
			and upper(trim(pp.control_value)) = upper(trim('yes'))
			);
		if uf_check_sql() = -1 then return -1
		
		// Map based on current version by company summary
		update wo_interface_staging z
		set z.budget_version_id = (
			select a.budget_version_id
			from budget_version a, company b, company c
			where a.current_version = 1
			and a.open_for_entry = 1
			and a.company_id = b.company_id
			and b.company_summary_id = c.company_summary_id
			and c.company_id = z.company_id
			)
		where z.status = 1
		and nvl(z.batch_id,'null') = :a_batch
		and (z.company_id = :a_company or :a_company = -1)
		and z.funding_wo_indicator = :a_funding_wo_ind
		and z.action = 'I'
		and z.budget_version_id is null
		and exists (
			select 1 from pp_system_control_companies pp
			where pp.company_id = z.company_id
			and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
			and upper(trim(pp.control_value)) = upper(trim('summary'))
			);
		if uf_check_sql() = -1 then return -1
		
		// Map based on current version for entire system
		update wo_interface_staging z
		set z.budget_version_id = (
			select budget_version_id
			from budget_version a
			where current_version = 1
			and open_for_entry = 1
			)
		where z.status = 1
		and nvl(z.batch_id,'null') = :a_batch
		and (z.company_id = :a_company or :a_company = -1)
		and z.funding_wo_indicator = :a_funding_wo_ind
		and z.action = 'I'
		and z.budget_version_id is null
		and not exists (
			select 1 from pp_system_control_companies pp
			where pp.company_id = z.company_id
			and upper(trim(pp.control_name)) = upper(trim('BV Current Version by Company'))
			and upper(trim(pp.control_value)) in (upper(trim('yes')), upper(trim('summary')))
			);
		if uf_check_sql() = -1 then return -1
		
		// Flag records where budget version is not open - ONLY FOR INSERTS
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - budget_version_id is not an open budget version; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and action = 'I'
		and a.budget_version_id not in (select budget_version_id from budget_version where nvl(open_for_entry, 0) = 1)
		;
		if uf_check_sql() = -1 then return -1
		
	end if
		
	//--------------------------------------------------------------------------------
	//
	// REASON CODE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Reason Code")
	
	////
	////	Maps on external value and error out if it cannot be mapped
	////
	
	// Map on external_reason_cd
	update wo_interface_staging a
	set a.reason_cd_id = (
		select b.reason_cd_id
		from reason_code b
		where b.external_reason_cd = a.ext_reason_cd
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.reason_cd_id is null
	and a.ext_reason_cd is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_staging a
	set a.reason_cd_id = (
		select b.reason_cd_id
		from  reason_code b
		where b.description = a.ext_reason_cd
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.reason_cd_id is null
	and a.ext_reason_cd is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on reason_cd_id
	update wo_interface_staging a
	set a.reason_cd_id = (
		select b.reason_cd_id
		from reason_code b
		where to_char(b.reason_cd_id) = a.ext_reason_cd
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.reason_cd_id is null
	and a.ext_reason_cd is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_reason_cd could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive reason_cd_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.reason_cd_id is null
	and a.ext_reason_cd is not null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// REPAIR LOCATION CODE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Repair Location Code")
	
	////
	////	Maps on extenal values and error out if it cannot be mapped
	////
	
	// Map on description
	update wo_interface_staging a
	set a.repair_location_id = (
		select b.repair_location_id
		from  repair_location b
		where b.description = a.ext_repair_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.repair_location_id is null
	and a.ext_repair_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on long_description
	update wo_interface_staging a
	set a.repair_location_id = (
		select b.repair_location_id
		from repair_location b
		where b.long_description = a.ext_repair_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.repair_location_id is null
	and a.ext_repair_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on repair_location_id
	update wo_interface_staging a
	set a.repair_location_id = (
		select b.repair_location_id
		from repair_location b
		where to_char(b.repair_location_id) = a.ext_repair_location
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.repair_location_id is null
	and a.ext_repair_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_repair_location could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive repair_location_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.repair_location_id is null
	and a.ext_repair_location is not null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// WO APPROVAL GROUP
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping WO Approval Group")
	
	////
	////	Maps on external value and error out if it cannot be mapped
	////
	
	// Map on external_wo_approval_group
	update wo_interface_staging a
	set a.wo_approval_group_id = (
		select b.wo_approval_group_id
		from wo_approval_group b
		where b.external_wo_approval_group = a.ext_wo_approval_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_approval_group_id is null
	and a.ext_wo_approval_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_staging a
	set a.wo_approval_group_id = (
		select b.wo_approval_group_id
		from wo_approval_group b
		where b.description = a.ext_wo_approval_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_approval_group_id is null
	and a.ext_wo_approval_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on wo_approval_group_id
	update wo_interface_staging a
	set a.wo_approval_group_id = (
		select b.wo_approval_group_id
		from wo_approval_group b
		where to_char(b.wo_approval_group_id) = a.ext_wo_approval_group
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_approval_group_id is null
	and a.ext_wo_approval_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_wo_approval_group could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive wo_approval_group_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.wo_approval_group_id is null
	and a.ext_wo_approval_group is not null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// AGREEMENT ID
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Agreement ID")
	
	
	////
	////	Maps on external value and error out if it cannot be mapped
	////
	
	// Map on external_agreement
	update wo_interface_staging a
	set a.agreement_id = (
		select b.agreemnt_id
		from co_tenancy_agreement b
		where b.external_agreement = a.ext_agreement
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.agreement_id is null
	and a.ext_agreement is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_staging a
	set a.agreement_id = (
		select b.agreemnt_id
		from co_tenancy_agreement b
		where b.description = a.ext_agreement
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.agreement_id is null
	and a.ext_agreement is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on agreement_id
	update wo_interface_staging a
	set a.agreement_id = (
		select b.agreemnt_id
		from co_tenancy_agreement b
		where to_char(b.agreemnt_id) = a.ext_agreement
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.agreement_id is null
	and a.ext_agreement is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where ext_agreement could not be mapped
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - cannot derive agreement_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.agreement_id is null
	and a.ext_agreement is not null
	;
	if uf_check_sql() = -1 then return -1
	
	//--------------------------------------------------------------------------------
	//
	// CLASS CODES
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Validating Class Code Values")
	
	//Update any class code values which match another in the values table
	for i = 1 to i_max_cc
		class_code_sql = &
		"update wo_interface_staging a "+&
		"set class_value"+string(i)+" = ( "+&
			"select max(ccv.value) from class_code cc, class_code_values ccv "+&
			"where upper(trim(cc.description)) = upper(trim(a.class_code"+string(i)+")) "+&
			"and lower(trim(cc.class_code_type)) = 'standard' "+&
			"and cc.class_code_id = ccv.class_code_id "+&
			"and lower(trim(ccv.value)) = lower(trim(a.class_value"+string(i)+")) "+&
			") "+&
		"where a.status = 1 "+&
		"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
		"and (company_id = "+string(a_company)+" or "+string(a_company)+" = -1) "+&
		"and a.funding_wo_indicator = "+string(a_funding_wo_ind)+" "+&
		"and a.class_code"+string(i)+" is not null "+&
		"and class_value"+string(i)+" is not null "+&
		"and exists ( "+&
			"select 1 from class_code cc, class_code_values ccv "+&
			"where upper(trim(cc.description)) = upper(trim(a.class_code"+string(i)+")) "+&
			"and lower(trim(cc.class_code_type)) = 'standard' "+&
			"and cc.class_code_id = ccv.class_code_id "+&
			"and lower(trim(ccv.value)) = lower(trim(a.class_value"+string(i)+")) "+&
			"and ccv.value <> a.class_value"+string(i)+" "+&
			") "
		execute immediate :class_code_sql;
		if uf_check_sql() = -1 then return -1
		
		class_code_sql = &
		"update wo_interface_staging a "+&
		"set error_message = a.error_message || '"+wo_fp+"' || ' - non-standard class code value used in standard class code type; ' "+&
		"where a.status = 1 "+&
		"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
		"and (company_id = "+string(a_company)+" or "+string(a_company)+" = -1) "+&
		"and a.funding_wo_indicator = "+string(a_funding_wo_ind)+" "+&
		"and a.class_code"+string(i)+" is not null "+&
		"and class_value"+string(i)+" is not null "+&
		"and exists ( "+&
			"select 1 from class_code cc "+&
			"where upper(trim(cc.description)) = upper(trim(a.class_code"+string(i)+")) "+&
			"and lower(trim(cc.class_code_type)) = 'standard' "+&
		") "+&
		"and not exists ( "+&
			"select 1 from class_code cc, class_code_values ccv "+&
			"where upper(trim(cc.description)) = upper(trim(a.class_code"+string(i)+")) "+&
			"and lower(trim(cc.class_code_type)) = 'standard' "+&
			"and cc.class_code_id = ccv.class_code_id "+&
			"and ccv.value = a.class_value"+string(i)+" "+&
			") "
		execute immediate :class_code_sql;
		if uf_check_sql() = -1 then return -1
	next
	
	//--------------------------------------------------------------------------------
	//
	// DATA MODEL VALIDATIONS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Validating Data Model Relationships")
	
	uf_msgs("   - Company (Work Order / Work Order Type)")
	// Flag records where company_id does not match company_id on work order type
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - company_id mismatch with work order type company_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_type_id is not null
	and a.company_id <> (
		select nvl(p.company_id,a.company_id)
		from work_order_type p
		where p.work_order_type_id = a.work_order_type_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	if a_funding_wo_ind = 0 then
		// ONLY FOR WORK ORDERS
		
		uf_msgs("   - Company (Work Order / Funding Project)")
		// Flag records where company_id does not match company_id on funding project
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - company_id mismatch with funding project company_id; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and a.funding_wo_indicator = :a_funding_wo_ind
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_id is not null
		and a.company_id <> (
			select p.company_id
			from work_order_control p
			where p.work_order_id = a.funding_wo_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	uf_msgs("   - Company / Bus Segment")
	// Flag records where company_id / bus_segment_id relationship is not valid
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - invalid company_id / bus_segment_id relationship; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.company_id = :a_company or :a_company = -1)
	and a.bus_segment_id is not null
	and not exists (
		select 1 from company_bus_segment_control p
		where p.company_id = a.company_id
		and p.bus_segment_id = a.bus_segment_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("   - Company / Major Location")
	// Flag records where company_id / major_location_id relationship is not valid
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - invalid company_id / major_location_id relationship; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.company_id = :a_company or :a_company = -1)
	and a.major_location_id is not null
	and not exists (
		select 1 from company_major_location p
		where p.company_id = a.company_id
		and p.major_location_id = a.major_location_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("   - Major Location / Asset Location")
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - invalid major_location_id / asset_location_id relationship; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.company_id = :a_company or :a_company = -1)
	and a.major_location_id is not null
	and a.asset_location_id is not null
	and not exists (
		select 1 from asset_location p
		where p.asset_location_id = a.asset_location_id
		and p.major_location_id = a.major_location_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	if a_funding_wo_ind = 0 then
		// ONLY FOR WORK ORDERS
		
		uf_msgs("   - Budget Item (Work Order / Funding Project")
		// Flag records where budget_id is not the same on the work order and funding project
		update wo_interface_staging a
		set a.error_message = a.error_message || :wo_fp || ' - budget_id mismatch with funding project budget_id; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and a.funding_wo_indicator = :a_funding_wo_ind
		and (a.company_id = :a_company or :a_company = -1)
		and a.budget_id is not null
		and a.funding_wo_id is not null
		and not exists (
			select 1 from work_order_control fp
			where fp.work_order_id = a.funding_wo_id
			and fp.budget_id = a.budget_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	uf_msgs("   - Estimated Dates (Inserts)")
	// Must have est_start_date <= est_in_service_date <= est_complete_date
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - Est Complete Date must be on or after the Est Start Date and the Est In Service Date must fall in between the two (Inserts); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.company_id = :a_company or :a_company = -1)
	and a.action = 'I'
	and (nvl(est_start_date,sysdate) > nvl(est_in_service_date,nvl(est_complete_date,sysdate))
		or nvl(est_in_service_date,nvl(est_complete_date,sysdate)) > nvl(est_complete_date,nvl(est_in_service_date,sysdate)))
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("   - Estimated Dates (Updates)")
	// Must have est_start_date <= est_in_service_date <= est_complete_date
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - Est Complete Date must be on or after the Est Start Date and the Est In Service Date must fall in between the two (Updates); '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.company_id = :a_company or :a_company = -1)
	and a.action = 'U'
	and (est_start_date is not null or est_in_service_date is not null or est_complete_date is not null)
	and (nvl(est_start_date,(select est_start_date from work_order_control b where a.work_order_id = b.work_order_id)) > 
		nvl(est_in_service_date,(select est_in_service_date from work_order_control b where a.work_order_id = b.work_order_id))
		or nvl(est_in_service_date,(select est_in_service_date from work_order_control b where a.work_order_id = b.work_order_id)) > 
		nvl(est_complete_date,(select est_complete_date from work_order_control b where a.work_order_id = b.work_order_id)))
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("   - Estimated Dates (Updates)")
	// Must have completion_date >= in_service_date if system control wocomplete - insrv < complete rule is yes
	update wo_interface_staging a
	set a.error_message = a.error_message || :wo_fp || ' - Completion Date can not be before In Service Date; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and a.funding_wo_indicator = :a_funding_wo_ind
	and (a.company_id = :a_company or :a_company = -1)
	and a.in_service_date is not null 
	and a.completion_date is not null
	and a.in_service_date > a.completion_date
	and exists (
			select 1 from pp_system_control_companies pp
			where pp.company_id = a.company_id
			and upper(trim(pp.control_name)) = upper(trim('wocomplete - insrv < complete rule'))
			and upper(trim(pp.control_value)) = upper(trim('yes'))
			)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERTS
	//
	//--------------------------------------------------------------------------------
	
	if a_funding_wo_ind = 1 then
		//--------------------------------------------------------------------------------
		//
		// INSERT NEW BUDGET ITEMS
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Creating New Budget Items")
		
		// Flag new budget items to be created
		update wo_interface_staging a
		set a.budget_id = pwrplant1.nextval,
			a.new_budget = 1
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.budget_id is null
		and a.budget_number is null
		and action = 'I'
		and (a.company_id, a.work_order_number) in (
			select c.company_id, c.work_order_number
			from wo_interface_staging c
			where c.status = 1
			and nvl(c.batch_id,'null') = :a_batch
			and (c.company_id = :a_company or :a_company = -1)
			and c.funding_wo_indicator = :a_funding_wo_ind
			and c.budget_id is null
			and c.budget_number is null
			and c.action = 'I'
			minus
			select b.company_id, b.budget_number
			from budget b
			)
		;
		if uf_check_sql() = -1 then return -1
		
		// Check if mappings from fp values to bi values exist
		bud_sum = lower(trim(f_pp_system_control_company('CAPBUD - WOT to Budget Summary', a_company)))
		if isnull(bud_sum) or trim(bud_sum) = '' then
			bud_sum = 'no'
		end if
		bud_org = lower(trim(f_pp_system_control_company('CAPBUD - WOG to Budget Org', a_company)))
		if isnull(bud_org) or trim(bud_org) = '' then
			bud_org = 'no'
		end if
//###CWB 20130307 added the distinct		
		// Insert new budget items
		insert into budget (
			budget_id, budget_number, budget_status_id, major_location_id, long_description,
			company_id, description, resp_person, department_id, bus_segment_id, afudc_type_id,
			notes,agreemnt_id, eligible_for_afudc, eligible_for_cpi,reimbursable_type_id,approved_amount, approved_amount_local,
			budget_summary_id, budget_organization_id
			)
		select distinct a.budget_id, nvl(a.budget_number, a.work_order_number), 1 budget_status_id, a.major_location_id, substr(a.long_description,1,254),
			nvl(a.budget_company_id, a.company_id), a.description, user, a.department_id, a.bus_segment_id, b.default_afc_type_id, 
			a.notes, nvl(a.agreement_id, b.agreemnt_id), b.afc_elig_indicator, b.cpi_elig_indicator, b.reimbursable_type_id, 0, 0,
			decode(:bud_sum, 'yes', c.budget_summary_id, null), decode(:bud_org, 'yes', d.budget_organization_id, null)
		from wo_interface_staging a, work_order_type b,
			(	select work_order_type_id, min(budget_summary_id) budget_summary_id
				from work_order_type_budget_summary
				group by work_order_type_id
			) c,
			work_order_group_budget_org d
		where a.work_order_type_Id = b.work_order_type_id
		and a.error_message is null //###CWB Maint 29530
		and a.work_order_type_id = c.work_order_type_id(+)
		and a.work_order_grp_id = d.work_order_grp_id (+)
		and a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and nvl(a.new_budget,0) = 1
		;
		if uf_check_sql() = -1 then return -1
		
		bv_by_company = lower(trim(f_pp_system_control_company('BV Current Version by Company', a_company)))
		if isnull(bv_by_company) or trim(bv_by_company) = '' then
			bv_by_company = 'no'
		end if
		
		// Insert Budget_Amounts
		insert into budget_amounts	(
			budget_id, budget_version_id, start_date, close_date, budget_total_dollars, curr_year_budget_dollars, closing_pattern_id,
			in_service_date, closing_option_id, late_chg_wait_period, base_year,
			escalation_id, budget_total_local, curr_year_budget_local, hist_amount, hist_amount_local
			)
		select a.budget_id, a.budget_version_id, a.est_start_date, a.est_complete_date, 0, 0, b.closing_pattern_id,
			nvl(a.est_in_service_date,a.est_complete_date), b.closing_option_id, b.late_chg_wait_period, a.base_year,
			b.escalation_id, 0, 0, 0, 0
		from wo_interface_staging a, work_order_type b
		where a.work_order_type_id = b.work_order_type_id
		and a.error_message is null //###CWB Maint 29530
		and a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and nvl(a.new_budget,0) = 1
		and a.budget_version_id is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Insert Budget_Approval
		insert into budget_approval (
			budget_id, budget_version_id, approval_number, approval_status_id,
			approved_amount, approved_amount_local, hist_amount, hist_amount_local)
		select a.budget_id, a.budget_version_id, 1, 1,
			0, 0, 0, 0
		from wo_interface_staging a, work_order_type b
		where a.work_order_type_Id = b.work_order_type_id
		and a.error_message is null //###CWB Maint 29530
		and a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and nvl(a.new_budget,0) = 1
		;
		if uf_check_sql() = -1 then return -1
		
		// Insert Workflow
		bi_workflow_appr = lower(trim(f_pp_system_control_company('Workflow User Object - Bud Item', a_company)))
		if bi_workflow_appr = 'yes' then
			insert into workflow (
				workflow_id, workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
				approval_amount, approval_status_id,use_limits,sql_approval_amount )
			select pwrplant1.nextval, c.workflow_type_id, c.description, 'bi_approval', a.budget_id, 1,
				0 approval_amount, decode(a.wo_status_id,1,1,3), c.use_limits, c.sql_approval_amount
			from wo_interface_staging a, work_order_type b,
				(	select * from workflow_type z
					where z.workflow_type_id = (
						select max(zz.workflow_type_id)
						from workflow_type zz
						where lower(zz.subsystem) like '%bi_approval%'
						)
				) c
			where a.work_order_type_Id = b.work_order_type_id
			and a.error_message is null //###CWB Maint 29530
			and a.status = 1
			and nvl(a.batch_id,'null') = :a_batch
			and (a.company_id = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_ind
			and nvl(a.new_budget,0) = 1
			;
			if uf_check_sql() = -1 then return -1
		end if
		
		// Flag Budget Items as having been inserted
		update wo_interface_staging a
		set a.new_budget = 2
		where a.status = 1
		and a.error_message is null /* IAES - Maint-31329*/ 
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and nvl(a.new_budget,0) = 1
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	uf_msgs("Inserting New "+wos_fps+"s")
	
	// Check for any new work orders / funding projects to process
	select count(*) into :counts
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	;
	
	if isnull(counts) or counts = 0 then
		uf_msgs("No New "+wos_fps+"s to Process")
		goto updates
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT WORK_ORDER_CONTROL
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Work Order Control")
	
	// Insert into work_order_control
	insert into work_order_control (
		WORK_ORDER_ID, WORK_ORDER_NUMBER, BUS_SEGMENT_ID, COMPANY_ID,
		BUDGET_ID, WO_STATUS_ID, REASON_CD_ID, WORK_ORDER_TYPE_ID,
		FUNDING_WO_ID, MAJOR_LOCATION_ID, ASSET_LOCATION_ID, CURRENT_REVISION,
		DESCRIPTION, LONG_DESCRIPTION, FUNDING_WO_INDICATOR, EST_START_DATE,
		EST_COMPLETE_DATE, LATE_CHG_WAIT_PERIOD, NOTES, DEPARTMENT_ID,
		EST_IN_SERVICE_DATE, COMPLETION_DATE, IN_SERVICE_DATE, close_date,
		EST_ANNUAL_REV, EXTERNAL_WO_NUMBER, OUT_OF_SERVICE_DATE, SUSPENDED_DATE,
		WO_APPROVAL_GROUP_ID
		)
	select a.WORK_ORDER_ID, upper(a.WORK_ORDER_NUMBER), a.BUS_SEGMENT_ID, a.COMPANY_ID,
		a.BUDGET_ID, a.wo_status_id, a.REASON_CD_ID, a.WORK_ORDER_TYPE_ID,
		decode(:a_funding_wo_ind,0,a.FUNDING_WO_ID,null), a.MAJOR_LOCATION_ID, a.ASSET_LOCATION_ID, decode(a.wo_status_id,1,0,1) current_revision,
		a.DESCRIPTION, a.LONG_DESCRIPTION, :a_funding_wo_ind FUNDING_WO_INDICATOR, nvl(a.EST_START_DATE,sysdate),
		nvl(a.EST_COMPLETE_DATE,nvl(a.EST_IN_SERVICE_DATE,sysdate)), b.LATE_CHG_WAIT_PERIOD, a.NOTES, a.DEPARTMENT_ID,
		nvl(a.EST_IN_SERVICE_DATE,nvl(a.EST_COMPLETE_DATE,sysdate)), a.COMPLETION_DATE, a.IN_SERVICE_DATE, a.close_date,
		a.EST_ANNUAL_REV, a.EXTERNAL_WO_NUMBER, nvl(a.OUT_OF_SERVICE_DATE,a.IN_SERVICE_DATE), a.SUSPENDED_DATE,
		a.WO_APPROVAL_GROUP_ID
	from wo_interface_staging a, work_order_type b
	where a.work_order_type_id = b.work_order_type_id
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT WORK_ORDER_ACCOUNT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Work Order Account")
	
	// Insert into work_order_account
	insert into work_order_account (
		WORK_ORDER_ID, REIMBURSABLE_TYPE_ID, AGREEMNT_ID, WORK_ORDER_GRP_ID,
		ESTIMATING_OPTION_ID, CLOSING_OPTION_ID, ALLOC_METHOD_TYPE_ID, RETIREMENT_TYPE,
		ELIGIBLE_FOR_AFUDC, AFUDC_TYPE_ID, CWIP_GL_ACCOUNT, EXPENSE_GL_ACCOUNT,
		NON_UNITIZED_GL_ACCOUNT, UNITIZED_GL_ACCOUNT, ELIGIBLE_FOR_CPI, SALVAGE_GL_ACCOUNT,
		REMOVAL_GL_ACCOUNT, RETIREMENT_GL_ACCOUNT, JOBBING_GL_ACCOUNT, FUNC_CLASS_ID,
		UNITIZE_BY_ACCOUNT, TOLERANCE_ID, CLOSING_PATTERN_ID, base_year,
		AFUDC_START_DATE, AFUDC_STOP_DATE, ACCRUAL_TYPE_ID, CR_DERIVER_TYPE_DETAIL,
		CR_DERIVER_TYPE_ESTIMATE, ESCALATION_ID, EST_UNIT_ITEM_OPTION, WORK_TYPE_ID,
		WO_EST_HIERARCHY_ID, rwip_type_id
		)
	select a.work_order_id, b.REIMBURSABLE_TYPE_ID, nvl(a.agreement_id, b.agreemnt_id), a.work_order_grp_id,
		b.estimating_option_id, b.closing_option_Id, b.ALLOC_METHOD_TYPE_ID, null retirement_type,
		b.afc_elig_indicator, b.default_afc_type_id, b.CWIP_GL_ACCOUNT, b.EXPENSE_GL_ACCOUNT,
		b.NON_UNITIZED_GL_ACCOUNT, b.UNITIZED_GL_ACCOUNT, b.cpi_elig_indicator, b.SALVAGE_GL_ACCOUNT,
		b.REMOVAL_GL_ACCOUNT, b.RETIREMENT_GL_ACCOUNT, b.JOBBING_GL_ACCOUNT, b.FUNC_CLASS_ID,
		b.UNITIZE_BY_ACCOUNT, b.TOLERANCE_ID, b.CLOSING_PATTERN_ID, a.base_year,
		a.AFUDC_START_DATE, a.AFUDC_STOP_DATE, b.ACCRUAL_TYPE_ID, b.CR_DERIVER_TYPE_DETAIL,
		b.CR_DERIVER_TYPE_ESTIMATE, b.ESCALATION_ID, b.EST_UNIT_ITEM_OPTION, b.WORK_TYPE_ID,
		b.WO_EST_HIERARCHY_ID, b.rwip_type_id
	from wo_interface_staging a, work_order_type b
	where a.work_order_type_id = b.work_order_type_id
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT WORK_ORDER_INITIATOR
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Work Order Initiator")
	
	// Insert into work_order_initiator
	insert into work_order_initiator (
		work_order_id, users, initiation_date, responsibility_type,
		proj_mgr, plant_accountant, contract_adm, engineer, other
		)
	select a.work_order_id, nvl(a.initiator,user), nvl(a.initiation_date,sysdate), 'prepare',
		project_manager, plant_accountant, contract_admin, engineer, other_contact
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT WORK_ORDER_APPROVAL
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Work Order Approval")
	
	// Insert into work_order_approval
	insert into work_order_approval (
		work_order_id, revision, budget_version_id, approval_type_id,
		est_start_date, est_complete_date, est_in_service_date, approval_status_id, rejected
		)
	select a.work_order_id, 1, null budget_version_id, b.approval_type_id, //nvl(a.approval_type_id, b.approval_type_id),
		nvl(a.est_start_date,sysdate), nvl(a.est_complete_date,sysdate), nvl(a.est_in_service_date,nvl(a.est_complete_date,sysdate)), decode(a.wo_status_id,1,1,decode(a.auto_approved,1,6,3)), decode(a.wo_status_id,1,2,null)
	from wo_interface_staging a, work_order_type b
	where a.work_order_type_id = b.work_order_type_id
	and a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT WORKFLOW
	//
	//--------------------------------------------------------------------------------
	if workflow_appr = 'yes' then
		uf_msgs("Inserting Workflow")
		
		// Insert into workflow
		insert into workflow (
			workflow_id, workflow_type_id, workflow_type_desc, subsystem, id_field1, id_field2,
			approval_amount, approval_status_id, use_limits, sql_approval_amount
			)
		select pwrplant1.nextval, c.workflow_type_id, c.description, decode(:a_funding_wo_ind,0,'wo_approval','fp_approval'), a.work_order_id, 1,
			0 approval_amount, decode(a.wo_status_id,1,1,decode(a.auto_approved,1,6,3)) approval_status_id, c.use_limits, c.sql_approval_amount
		from wo_interface_staging a, work_order_type b,workflow_type c
		where a.work_order_type_id = b.work_order_type_id
//		and nvl(a.approval_type_id, b.APPROVAL_TYPE_ID) = c.workflow_type_id
		and b.APPROVAL_TYPE_ID = c.workflow_type_id
		and a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.action = 'I'
		and a.error_message is null
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	
	if a_funding_wo_ind = 1 then
		// ONLY FOR FUNDING PROJECTS
		
		//--------------------------------------------------------------------------------
		//
		// INSERT BUDGET_VERSION_FUND_PROJ
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Inserting Budget Version Fund Proj")
		
		// Insert into budget_version_fund_proj
		insert into budget_version_fund_proj (
			work_order_id, revision, budget_version_id, active, date_added, person_added, date_active, has_respread
			)
		select a.work_order_id, 1, nvl(a.budget_version_id, :i_bv_id), 1, sysdate, user, sysdate, 0
		from wo_interface_staging a
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.action = 'I'
		and a.error_message is null
		and a.budget_version_id is not null
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// INSERT FUNDING_JUSTIFICATION
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Funding Justification")
	
	// Insert into funding_justification
	insert into funding_justification (
		work_order_id, reason_for_work, background, future_projects, alternatives, financial_analysis
		)
	select a.work_order_id,  a.reason_for_work, a.background, a.future_projects, a.alternatives, a.financial_analysis
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// UPDATING DEFAULT APPROVERS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Updating Default Approvers")
	
	// Update default approvers
	// Only for unapproved revisions
	select count(*) into :checkers
	from approval_defaults
	where work_order_type_id in (
		select a.work_order_type_id
		from wo_interface_staging a
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_ind
		and a.action = 'I'
		and a.error_message is null
		and a.wo_status_id = 1
		)
	;
	
	if checkers > 0 then
		update work_order_approval a
		set (authorizer1, authorizer2, authorizer3, authorizer4, authorizer5, authorizer6, authorizer7, authorizer8, authorizer9, authorizer10) = (
			select auth_level1, auth_level2, auth_level3, auth_level4, auth_level5, auth_level6, auth_level7, auth_level8, auth_level9, auth_level10
			from approval_defaults q, work_order_control c
			where c.work_order_type_Id = q.work_order_type_id
			and c.work_order_id = a.work_order_id
			)
		where a.work_order_id in (
			select b.work_order_id
			from wo_interface_staging b
			where b.status = 1
			and nvl(b.batch_id,'null') = :a_batch
			and (b.company_id = :a_company or :a_company = -1)
			and b.funding_wo_indicator = :a_funding_wo_ind
			and b.action = 'I'
			and b.error_message is null
			and b.wo_status_id = 1
			)
		and a.revision = 1	
		;
		if uf_check_sql() = -1 then return -1
		
		insert into wo_approval_multiple (
			work_order_id, revision, approval_type_id, auth_level,
			users, category_id, required, requiring_user, auth_level_id
			)
		select a.work_order_id, 1 revision, a.approval_type_id, c.auth_level,
			c.users, c.category_id, c.required, user, d.auth_level_id
		from work_order_approval a, wo_interface_staging b, wo_approval_multiple_default c,
			(	select approval_type_id, 1 auth_level_id, auth_level1 auth_level from approval union all
				select approval_type_id, 2 auth_level_id, auth_level2 auth_level from approval union all
				select approval_type_id, 3 auth_level_id, auth_level3 auth_level from approval union all
				select approval_type_id, 4 auth_level_id, auth_level4 auth_level from approval union all
				select approval_type_id, 5 auth_level_id, auth_level5 auth_level from approval union all
				select approval_type_id, 6 auth_level_id, auth_level6 auth_level from approval union all
				select approval_type_id, 7 auth_level_id, auth_level7 auth_level from approval union all
				select approval_type_id, 8 auth_level_id, auth_level8 auth_level from approval union all
				select approval_type_id, 9 auth_level_id, auth_level9 auth_level from approval union all
				select approval_type_id, 10 auth_level_id, auth_level10 auth_level from approval
			) d
		where a.work_order_id = b.work_order_id
		and a.approval_type_id = c.approval_type_id
		and c.approval_type_id = d.approval_type_id
		and lower(trim(c.auth_level)) = lower(trim(d.auth_level))
		and b.status = 1
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and b.funding_wo_indicator = :a_funding_wo_ind
		and b.action = 'I'
		and b.error_message is null
		and b.wo_status_id = 1
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// CLASS CODES
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Class Codes")
	
	// Create new class codes
	//rtn = uf_create_cc() -- I think we should set these up if we know we are going to need them
	//if rtn = -1 then
	//	return -1
	//end if
	
	for i = 1 to i_max_cc
		uf_msgs( " Class Code Insert "+string(i))
		cc_sqls_wocc = &
			'insert into work_order_class_code (work_order_id, class_code_id, "VALUE") '+&
			'select a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
			'from wo_interface_staging a, class_code cc '+&
			'where a.status = 1 '+&
			"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
			'and a.funding_wo_indicator = '+string(a_funding_wo_ind)+' '+&
			'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
			"and a.action = 'I' "+&
			"and a.error_message is null "+&
			'and a.class_code'+string(i)+' is not null '+&
			'and class_value'+string(i)+' is not null '+&
			'and upper(trim(cc.description)) = upper(trim(a.class_code'+string(i)+')) '
		if a_funding_wo_ind = 0 then
			cc_sqls_wocc += &
			'and cc.cwip_indicator = 1 '
		else
			cc_sqls_wocc += &
			'and cc.fp_indicator = 1 '
		end if
		execute immediate :cc_sqls_wocc;
		if uf_check_sql() = -1 then return -1
		
		if a_funding_wo_ind = 1 then
			// ONLY FOR FUNDING PROJECTS
			
			cc_sqls_bcc = &
				'insert into budget_class_code (budget_id, class_code_id, value, budget_version_id)  '+&
				'select a.budget_id, c.class_code_id, max(a.class_value'+string(i)+'), nvl(a.budget_version_id,'+string(i_bv_id)+') '+&
				'from wo_interface_staging a, class_code c '+&
				'where a.status = 1 '+&
				"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
				'and a.funding_wo_indicator = '+string(a_funding_wo_ind)+' '+&
				'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
				"and a.action = 'I' "+&
				"and a.error_message is null "+&
				'and a.budget_version_id is not null '+&
				'and a.class_code'+string(i)+' is not null '+&
				'and class_value'+string(i)+' is not null '+&
				'and upper(trim(a.class_code'+string(i)+')) = upper(trim(c.description)) '+&
				'and c.budget_indicator = 1 '+&
				'and (a.budget_id, c.class_code_id, a.budget_version_id) in ( '+&
				'	select a.budget_id, c.class_code_id, nvl(a.budget_version_id,'+string(i_bv_id)+') '+&
				'	from wo_interface_staging a, class_code c'+&
				'	where a.status = 1 '+&
				"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
				'	and a.funding_wo_indicator = '+string(a_funding_wo_ind)+' '+&
				'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
				"	and a.action = 'I' "+&
				"	and a.error_message is null "+&
				'    and a.budget_version_id is not null '+&
				'	and a.class_code'+string(i)+' is not null '+&
				'	and class_value'+string(i)+' is not null '+&
				'	and upper(trim(a.class_code'+string(i)+')) = upper(trim(c.description)) '+&
				'	and c.budget_indicator = 1 '+&
				'	minus '+&
				'	select budget_id, class_code_id, budget_version_id '+&
				'	from budget_class_code '+&
				'	) '+&
				'group by a.budget_id, c.class_code_id, nvl(a.budget_version_id,'+string(i_bv_id)+') '
			execute immediate :cc_sqls_bcc;
			if uf_check_sql() = -1 then return -1
			
		end if
		
	next
	
	
	//--------------------------------------------------------------------------------
	//
	// CLASS CODE DEFAULTS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Class Code Defaults")
	
	// Create new class codes
	uf_msgs( " Class Code Defaults ")
	insert into work_order_class_code (work_order_id, class_code_id, value)
	select a.work_order_id, b.class_code_id, b.value
	from wo_interface_staging a, wo_type_class_code_default b
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	and a.work_order_type_id = b.work_order_type_id
	and not exists (
		select 1 from work_order_class_code c
		where c.work_order_id = a.work_order_id
		and c.class_code_id = b.class_code_id
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// ESTIMATE TEMPLATES & DEPARTMENT TEMPLATES
	//
	//--------------------------------------------------------------------------------
	// PP-41865
	uf_msgs("Inserting Template Defaults")
	
	delete from wo_est_processing_temp;

	insert into wo_est_processing_temp
	  (work_order_id, revision)
	  select work_order_id, work_order_type_id
		from wo_interface_staging a
	   where a.status = 1
		 and nvl(a.batch_id, 'null') = :a_batch
		 and (a.company_id = :a_company or :a_company = -1)
		 and a.funding_wo_indicator = :a_funding_wo_ind
		 and a.action = 'I'
		 and a.error_message is null;
		 
	if uf_check_sql() = -1 then return -1

	uo_wo_template_initiate wti
	wti = create uo_wo_template_initiate
	if wti.uf_wo_est_template_initiate()< 0 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// WORK ORDER TAX REPAIRS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Inserting Work Order Tax Repairs - Test Id and Repair Location")
	
	insert into work_order_tax_repairs
		(work_order_id, tax_expense_test_id, repair_location_id, repair_location_method_id)
			(select a.work_order_id, b.tax_expense_test_id, a.repair_location_id, b.repair_location_method_id
				from wo_interface_staging a, work_order_type b
			  where a.work_order_type_id = b.work_order_type_id
				 and a.status = 1
				 and nvl(a.batch_id,'null') = :a_batch
				 and (a.company_id = :a_company or :a_company = -1)
				 and a.funding_wo_indicator = :a_funding_wo_ind
				 and a.action = 'I'
				 and a.error_message is null);
	
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// MARK RECORDS AS PROCESSED
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Interface Status Update")
	
	// Update status to indicate it has been processed
	update wo_interface_staging a
	set status = 2
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'I'
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	
	//--------------------------------------------------------------------------------
	//
	// UPDATES
	//
	//--------------------------------------------------------------------------------
	updates:
	
	uf_msgs("------------------------------- ")
	uf_msgs("Updating Existing "+wos_fps+"s")
	
	select count(*)
	into :update_counts
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	and a.error_message is null
	;
	
	if isnull(update_counts) then update_counts = 0
	if update_counts = 0 then
		uf_msgs("No Existing "+wos_fps+"s to Process")
	end if
	
	if no_updates or update_counts = 0 then
		// Do not run updates
	else
		rtn = uf_update_wos_fps(a_company, a_batch, a_funding_wo_ind)
		if rtn = -1 then
			return -1
		end if
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
next


//--------------------------------------------------------------------------------
//
// CLASS_CODE_VALUES
//
//--------------------------------------------------------------------------------
uf_msgs("Backfilling Class Code Values")
insert into class_code_values (class_code_id, value)
select class_code_id, value
from work_order_class_code
minus
select class_code_id, value
from class_code_values
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// ARCHIVE RECORDS
//
//--------------------------------------------------------------------------------
uf_msgs("Archiving Records")

// Insert into archive table
// PP-41927
insert into wo_interface_staging_arc (
	WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,
	FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,
	EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,
	IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,
	BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,//APPROVAL_TYPE_ID,
	AFUDC_STOP_DATE,	AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,
	WO_STATUS_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,
	CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,
	CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,
	CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,
	CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,CLASS_VALUE23,
	CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,
	CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,RUN_DATE,
	batch_id, REIMBURSABLE_TYPE_ID, ext_company, ext_work_order_type, ext_major_location, budget_number, budget_company_id, ext_department,
	ext_work_order_group, ext_asset_location, seq_id, USER_ID, TIME_STAMP, agreement_id, error_message, old_wo_status_id, ext_work_order_status,
	REPAIR_LOCATION_ID, EXT_REPAIR_LOCATION, AUTO_APPROVED
	)
select WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,
	FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,
	EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,
	IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,
	BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,//APPROVAL_TYPE_ID,
	AFUDC_STOP_DATE,	AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,
	WO_STATUS_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,
	CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,
	CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,
	CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,
	CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,CLASS_VALUE23,
	CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,
	CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,sysdate,
	batch_id, REIMBURSABLE_TYPE_ID, ext_company, ext_work_order_type, ext_major_location, budget_number, budget_company_id, ext_department,
	ext_work_order_group, ext_asset_location, seq_id, USER_ID, TIME_STAMP, agreement_id, error_message, old_wo_status_id, ext_work_order_status,
	REPAIR_LOCATION_ID, EXT_REPAIR_LOCATION, AUTO_APPROVED
from wo_interface_staging a
where nvl(a.status,1) = 2
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.funding_wo_indicator = :a_funding_wo_ind
and a.error_message is null
;
if uf_check_sql() = -1 then return -1

// Delete Archived Data
delete from wo_interface_staging a
where nvl(a.status,1) = 2
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.funding_wo_indicator = :a_funding_wo_ind
and a.error_message is null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// COMMIT
//
//--------------------------------------------------------------------------------
uf_msgs("Committing records")

// Commit here
commit;

uf_msgs("Finished "+wo_fp+" Loading - Company Id = "+string(a_company))
uf_msgs("------------------------------- ")
uf_msgs("------------------------------- ")


return 1
end function

public function integer uf_update_wos_fps (longlong a_company, string a_batch, longlong a_funding_wo_ind);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Event    :   uo_wo_interface.uf_update_wos_fps
//
//   Purpose  :   Creates Work Orders / Funding Projects from pre-staged data in the table wo_interface_staging.
//					Assumes that the Funding Project Number, Work_order_type_id and desciption are not null in the wo_interface_staging table.
//                    Updates valid entrys to status of 2.
//                    This functions is automatically called through the function uf_create_wos_fps()
//                     
// 					
//   DATE                           NAME                            REVISION                     CHANGES
//  --------      --------     -----------   --------------------------------------------- 
//  07-23-2008      PowerPlan           Version 1.0   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
longlong checkers, i
string cc_update, cc_insert

if a_funding_wo_ind = 1 then
else
	a_funding_wo_ind = 0
end if

if isnull(a_batch) then a_batch = 'null'
if isnull(a_company) then a_company = -1

//--------------------------------------------------------------------------------
//
// WORK ORDER CONTROL
//
//--------------------------------------------------------------------------------
uf_msgs("Updating Work Order Control")
update work_order_control z
set (BUS_SEGMENT_ID, COMPANY_ID, BUDGET_ID, WO_STATUS_ID,
	REASON_CD_ID, WORK_ORDER_TYPE_ID, FUNDING_WO_ID, MAJOR_LOCATION_ID,
	ASSET_LOCATION_ID, DESCRIPTION, LONG_DESCRIPTION, EST_START_DATE,
	EST_COMPLETE_DATE, NOTES, DEPARTMENT_ID, EST_IN_SERVICE_DATE,
	COMPLETION_DATE, IN_SERVICE_DATE, CLOSE_DATE, EST_ANNUAL_REV,
	EXTERNAL_WO_NUMBER, OUT_OF_SERVICE_DATE, SUSPENDED_DATE, WO_APPROVAL_GROUP_ID
	) = (
	select nvl(a.BUS_SEGMENT_ID,z.bus_segment_Id), nvl(a.COMPANY_ID,z.company_id), nvl(a.BUDGET_ID,z.budget_id), nvl(a.wo_status_id,z.wo_status_id),
		nvl(a.REASON_CD_ID,z.reason_cd_id), nvl(a.WORK_ORDER_TYPE_ID,z.work_order_type_id), nvl(a.FUNDING_WO_ID,z.funding_wo_id), nvl(a.MAJOR_LOCATION_ID,z.major_location_id),
		nvl(a.ASSET_LOCATION_ID,z.asset_location_id), nvl(a.DESCRIPTION,z.description), nvl(a.LONG_DESCRIPTION,z.long_description), nvl(a.EST_START_DATE,z.est_start_date),
		nvl(a.EST_COMPLETE_DATE,z.EST_COMPLETE_DATE), nvl(a.NOTES,z.NOTES), nvl(a.DEPARTMENT_ID,z.DEPARTMENT_ID), nvl(a.EST_IN_SERVICE_DATE,z.EST_IN_SERVICE_DATE),
		decode(a.wo_status_id, null, z.COMPLETION_DATE, a.COMPLETION_DATE), decode(a.wo_status_id, null, z.IN_SERVICE_DATE, a.IN_SERVICE_DATE), decode(a.wo_status_id, null, z.CLOSE_DATE, a.CLOSE_DATE), nvl(a.EST_ANNUAL_REV, z.EST_ANNUAL_REV),
		nvl(a.EXTERNAL_WO_NUMBER, z.EXTERNAL_WO_NUMBER), nvl(a.OUT_OF_SERVICE_DATE, nvl(z.OUT_OF_SERVICE_DATE, a.IN_SERVICE_DATE)), decode(a.wo_status_id, null, z.SUSPENDED_DATE, a.SUSPENDED_DATE), nvl(a.WO_APPROVAL_GROUP_ID, z.WO_APPROVAL_GROUP_ID)
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	and a.error_message is null
	and a.work_order_id = z.work_order_id
	)
where z.work_order_id in (
	select work_order_id
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	and a.error_message is null
	)
;
if uf_check_sql() = -1 then return -1


// PP-44418
update work_order_control z
set wo_status_id = 7, 
	close_date = sysdate
where z.work_order_id in (
	select work_order_id
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.funding_wo_indicator = 1
	and a.action = 'U'
	and a.error_message is null
	and a.wo_status_id = 5
	)
and exists (
	select 1 from pp_system_control_companies p
	where p.company_id = z.company_id
	and lower(trim(p.control_name)) = lower('FP Allow Completed Status')
	and lower(trim(p.control_value)) = lower('no')
	)
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// WORK ORDER ACCOUNT
//
//--------------------------------------------------------------------------------
uf_msgs("Updating Work Order Account")
update work_order_account z
set (WORK_ORDER_GRP_ID, AFUDC_START_DATE, AFUDC_STOP_DATE, BASE_YEAR) = (
	select nvl(a.work_order_grp_id, z.work_order_grp_id),
		nvl(a.AFUDC_START_DATE, z.AFUDC_START_DATE),
		nvl(a.AFUDC_STOP_DATE, z.AFUDC_STOP_DATE),
		nvl(a.BASE_YEAR, z.BASE_YEAR)
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	and a.error_message is null
	and a.work_order_id = z.work_order_id
	)
where z.work_order_id in (
	select work_order_id
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	and a.error_message is null
	)
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// WORK_ORDER_INITIATOR
//
//--------------------------------------------------------------------------------
uf_msgs("Updating Work Order Initiator")

// Insert into work_order_initiator
update work_order_initiator z
set (users, initiation_date, proj_mgr, plant_accountant, contract_adm, engineer, other) = (
	select nvl(a.initiator,z.users), nvl(a.initiation_date,z.initiation_date), nvl(a.project_manager,z.proj_mgr), nvl(a.plant_accountant,z.plant_accountant), nvl(a.contract_admin,z.contract_adm), nvl(a.engineer,z.engineer), nvl(a.other_contact,z.other)
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	and a.error_message is null
	and a.work_order_id = z.work_order_id
	)
where z.work_order_id in (
	select work_order_id
	from wo_interface_staging a
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_ind
	and a.action = 'U'
	and a.error_message is null
	)
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// CLASS CODES
//
//--------------------------------------------------------------------------------
uf_msgs("Updating Class Codes")
for i = 1 to i_max_cc
	cc_insert = &
		'insert into work_order_class_code (work_order_id, class_code_id, value) '+&
		'select a.work_order_id, cc.class_code_id, a.class_value'+string(i)+' '+&
		'from wo_interface_staging a, class_code cc '+&
		'where a.status = 1 '+&
		"and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
		'and a.funding_wo_indicator = '+string(a_funding_wo_ind)+' '+&
		'and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
		"and a.action = 'U' "+&
		"and a.error_message is null "+&
		'and a.class_code'+string(i)+' is not null '+&
		'and a.class_value'+string(i)+' is not null '+&
		'and upper(trim(a.class_code'+string(i)+')) = upper(trim(cc.description)) '
	if a_funding_wo_ind = 0 then
		cc_insert += &
		'and cc.cwip_indicator = 1 '
	else
		cc_insert += &
		'and cc.fp_indicator = 1 '
	end if
	cc_insert += &
		'and not exists ( '+&
		'	select 1 from work_order_class_code c '+&
		'	where c.work_order_id = a.work_order_id '+&
		'	and c.class_code_id = cc.class_code_id '+&
		'	) '
	execute immediate :cc_insert;
	if uf_check_sql() = -1 then return -1
	
	cc_update = &
		"update work_order_class_code z "+&
		"set value = ( "+&
		"	select a.class_value"+string(i)+" "+&
		"	from wo_interface_staging a, class_code cc "+&
		'	where a.status = 1 '+&
		"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
		'	and a.funding_wo_indicator = '+string(a_funding_wo_ind)+' '+&
		'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
		"	and a.action = 'U' "+&
		"	and a.error_message is null "+&
		"	and a.class_code"+string(i)+" is not null "+&
		"	and a.class_value"+string(i)+" is not null "+&
		"	and z.work_order_id = a.work_order_id "+&
		"	and z.class_code_id = cc.class_code_id "+&
		"	and upper(trim(cc.description)) = upper(trim(a.class_code"+string(i)+")) "
	if a_funding_wo_ind = 0 then
		cc_update += &
		"	and cc.cwip_indicator = 1 "
	else
		cc_update += &
		"	and cc.fp_indicator = 1 "
	end if
	cc_update += &
		"	) "+&
		"where (work_order_id, class_code_id) in ( "+&
		"	select work_order_id, cc.class_code_id "+&
		"	from wo_interface_staging a, class_code cc "+&
		'	where a.status = 1 '+&
		"	and nvl(a.batch_id,'null') = '"+a_batch+"' "+&
		'	and a.funding_wo_indicator = '+string(a_funding_wo_ind)+' '+&
		'	and (a.company_id = '+string(a_company)+' or '+string(a_company)+' = -1) '+&
		"	and a.action = 'U' "+&
		"	and a.error_message is null "+&
		"	and a.class_code"+string(i)+" is not null "+&
		"	and a.class_value"+string(i)+" is not null "+&
		"	and upper(trim(cc.description)) = upper(trim(a.class_code"+string(i)+")) "
	if a_funding_wo_ind = 0 then
		cc_update += &
		"	and cc.cwip_indicator = 1 "
	else
		cc_update += &
		"	and cc.fp_indicator = 1 "
	end if
	cc_update += &
		"	) "
	execute immediate :cc_update;
	if uf_check_sql() = -1 then return -1
next


//--------------------------------------------------------------------------------
//
// MARK RECORDS AS PROCESSED
//
//--------------------------------------------------------------------------------
uf_msgs("Interface Status Update")

// Update status to indicate it has been processed
update wo_interface_staging a
set status = 2
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.funding_wo_indicator = :a_funding_wo_ind
and a.action = 'U'
and a.error_message is null
;
if uf_check_sql() = -1 then return -1


return 1
end function

public function integer uf_create_fps (string a_batch);longlong rtn, neg_one = -1

rtn = uf_create_fps(neg_one, a_batch)

return rtn

end function

public function integer uf_create_wos (string a_batch);longlong rtn, neg_one = -1

rtn = uf_create_wos(neg_one, a_batch)

return rtn

end function

public function integer uf_load_monthly_est (longlong a_company, boolean a_replace, string a_batch, boolean a_replace_year, longlong a_funding_wo_indicator);longlong rtn, bv_id[], cnt, num_batches, i,  ii, iii
string sqls, dflt_add, dflt_retire, dflt_rmvl, ua_fc_filter, al_ml_filter, wo_fp, dep_blank
any results[]
uo_budget_revision uo_revisions

/********************************************/
if isnull(a_batch) then a_batch = 'null'
if isnull(a_company) then a_company = -1
if a_replace_year = true then a_replace = true
// PP-44415
if isnull(a_funding_wo_indicator) then a_funding_wo_indicator = 0




//--------------------------------------------------------------------------------
//
// PROCESSING STATUS
//
//--------------------------------------------------------------------------------
uf_msgs("Setting Status")
// PP-41914
// PP-44415
update wo_interface_monthly a
set status = 1,
	funding_wo_indicator = nvl(a.funding_wo_indicator, 0),
	process_level = nvl(a.process_level, 'wo'),
	error_message = null,
	budget_version_id = decode(:a_funding_wo_indicator,0,null,budget_version_id),
	work_order_number = upper(a.work_order_number),
	ext_wo_work_order_number = upper(a.ext_wo_work_order_number)
where nvl(batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and nvl(funding_wo_indicator,0) = :a_funding_wo_indicator
and nvl(status,1) <> 2
;	 
if uf_check_sql() = -1 then return -1

// PP-44415
update wo_interface_monthly a
set error_message = error_message ||'  invalid process level; '
where nvl(batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and funding_wo_indicator = :a_funding_wo_indicator
and process_level = 'fp'
and funding_wo_indicator <> 1
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// COMPANY
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Mapping Company")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on gl_company_no
update wo_interface_monthly a
set a.company_id = (
	select p.company_id
	from company p
	where p.gl_company_no = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1


// Map on company description
update wo_interface_monthly a
set a.company_id = (
	select p.company_id
	from company p
	where p.description = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1

// Map on company_id
update wo_interface_monthly a
set a.company_id = (
	select p.company_id
	from company p
	where to_char(p.company_id) = a.ext_company
	)
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1


// Flag records where ext_company could not be mapped
update wo_interface_monthly a
set a.error_message = a.error_message ||'  Monthly - cannot derive company_id; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is not null
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1


// Flag records missing company_id as errors
update wo_interface_monthly a
set a.error_message = a.error_message ||'  Monthly - company_id is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and a.company_id is null
and a.ext_company is null
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1

//--------------------------------------------------------------------------------
//
// WORK ORDER NUMBER
//  - always required
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Work Order Number")

// Flag records missing work_order_number as errors
update wo_interface_monthly a
set a.error_message = a.error_message ||'  Monthly - work_order_number is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and a.work_order_number is null
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1

//--------------------------------------------------------------------------------
//
// EXT JOB TASK
//  - always required where process_level = 'task'
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Ext Job Task")

// Flag records missing work_order_number as errors
//Have to check whether we will have a project level task AKK
update wo_interface_monthly a
set a.error_message = a.error_message ||'  Monthly - ext_job_task is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and a.ext_job_task is null
and a.process_level = 'task'
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1

//--------------------------------------------------------------------------------
//
// EXT WO Work Order Number
//  - always required where process_level = 'wo'
//
//--------------------------------------------------------------------------------
uf_msgs("Validating Ext Job Task")

// Flag records missing work_order_number as errors
//Have to check whether we will have a project level task AKK
update wo_interface_monthly a
set a.error_message = a.error_message ||'  Monthly - ext_wo_work_order_number is required; '
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
and a.process_level = 'wo'
and a.funding_wo_indicator = 1
and a.ext_wo_work_order_number is null
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// VALIDATION
//   - must have a valid COMPANY_ID and WORK_ORDER_NUMBER and JOB_TASK_ID
//
//--------------------------------------------------------------------------------
uf_msgs("Flagging Errors (missing Company or Work Order Number (or Ext Job Task))")

// Flag records that already have errors as unable to continue processing (either invalid company_id or work_order_number or ext_job_task)
//BCM 3/13/2013 - added 'and error_message' is not null
update wo_interface_monthly a
set a.status = -11
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
and a.funding_wo_indicator = :a_funding_wo_indicator
and a.error_message is not null
;
if uf_check_sql() = -1 then return -1


//--------------------------------------------------------------------------------
//
// DETERMINE THE NUMBER OF BATCHES
//   - Loop over batches of work order/job task estimates
//
//--------------------------------------------------------------------------------
uf_msgs("Identifying Process Batches")

// Determine the number of loops needed to process all work orders in sequence
select max(batches)
into :num_batches
from (
	select max(batches) batches
	from (
		select work_order_number, company_id, count(distinct nvl(seq_id,0)) batches
		from wo_interface_monthly a
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and ((a.process_level = 'wo' and a.funding_wo_indicator = 0) or a.process_level = 'fp')
		and a.funding_wo_indicator = :a_funding_wo_indicator
		group by work_order_number, company_id
		)
	union all
	select max(batches) batches
	from (
		select work_order_number, company_id, ext_job_task, count(distinct nvl(seq_id,0)) batches
		from wo_interface_monthly a
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.process_level = 'task'
		and a.funding_wo_indicator = :a_funding_wo_indicator
		group by work_order_number, company_id, ext_job_task
		)
	union all
	select max(batches) batches
	from (
		select work_order_number, company_id, ext_wo_work_order_number, count(distinct nvl(seq_id,0)) batches
		from wo_interface_monthly a
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.process_level = 'wo'
		and a.funding_wo_indicator = 1
		and a.funding_wo_indicator = :a_funding_wo_indicator
		group by work_order_number, company_id, ext_wo_work_order_number
		)
	)
;
if uf_check_sql() = -1 then return -1

uf_msgs("Updating status to Pending")

// Update records that have not yet been processed to status = 3 (pending
update wo_interface_monthly a
set a.status = 3 // pending
where a.status = 1
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.funding_wo_indicator = :a_funding_wo_indicator
;
if uf_check_sql() = -1 then return -1

//--------------------------------------------------------------------------------
//
// LOOP OVER THE BATCHES
//
//--------------------------------------------------------------------------------
for ii = 1 to num_batches
	uf_msgs("***** Batch "+string(ii)+" of "+string(num_batches)+" *****")
	uf_msgs(" ")
	
	//BCM 03/13/2013 - Updated to just have process level = 'wo' becauase the process level of 'fp' was made 'Illegal' in beginning of code
	
	uf_msgs("Updating status to In-Process for Work Order Estimates")
	update wo_interface_monthly a
	set a.status = 1 // in-process
	where a.status = 3
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and ((a.process_level = 'wo' and a.funding_wo_indicator = 0) or a.process_level = 'fp')
	//and a.process_level = 'wo'
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and nvl(seq_id,0) in (
		select min(nvl(seq_id,0))
		from wo_interface_monthly b
		where b.status = 3
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and ((a.process_level = 'wo' and a.funding_wo_indicator = 0) or a.process_level = 'fp')
		//and a.process_level = 'wo'
		and b.work_order_number = a.work_order_number
		and b.company_id = a.company_id
		and a.funding_wo_indicator = :a_funding_wo_indicator
		)
	;
	if uf_check_sql() = -1 then return -1
	
	uf_msgs("Updating status to In-Process for Job Task Estimates")
	update wo_interface_monthly a
	set a.status = 1 // in-process
	where a.status = 3
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.process_level = 'task'
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and nvl(seq_id,0) in (
		select min(nvl(seq_id,0))
		from wo_interface_monthly b
		where b.status = 3
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and a.process_level = 'task'
		and b.work_order_number = a.work_order_number
		and b.company_id = a.company_id
		and b.ext_job_task = a.ext_job_task
		and a.funding_wo_indicator = :a_funding_wo_indicator
		)
	;
	if uf_check_sql() = -1 then return -1
	
	//BCM - Is this supposed to be for funding work orders? this isn't working for some reason but this looks like it
	//would resolve the problem that I pointed to above 
	//Why the two funding wo indicator constraints? --> could maybe cause an issue?
	
	uf_msgs("Updating status to In-Process for WO Estimates")
	update wo_interface_monthly a
	set a.status = 1 // in-process
	where a.status = 3
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.process_level = 'wo'
	and a.funding_wo_indicator = 1
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and nvl(seq_id,0) in (
		select min(nvl(seq_id,0))
		from wo_interface_monthly b
		where b.status = 3
		and nvl(b.batch_id,'null') = :a_batch
		and (b.company_id = :a_company or :a_company = -1)
		and a.process_level = 'wo'
		and a.funding_wo_indicator = 1
		and b.work_order_number = a.work_order_number
		and b.company_id = a.company_id
		and b.ext_job_task = a.ext_job_task
		and a.funding_wo_indicator = :a_funding_wo_indicator
		)
	;
	if uf_check_sql() = -1 then return -1

	//--------------------------------------------------------------------------------
	//
	// WORK ORDER ID
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Work Order Id")
	
	// Map on work_order_number
	update wo_interface_monthly a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.work_order_number = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and a.work_order_id is null
	;
	if uf_check_sql() = -1 then return -1

// Map on external_wo_number
	update wo_interface_monthly a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where p.external_wo_number = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	//BCM 03/13/2013 - Updated to MAX(WO ID) because description may return multiple work orders
	// Map on description
	update wo_interface_monthly a
	set a.work_order_id = (
		select max(p.work_order_id)
		from work_order_control p
		where p.description = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on work_order_id
	update wo_interface_monthly a
	set a.work_order_id = (
		select p.work_order_id
		from work_order_control p
		where to_char(p.work_order_id) = a.work_order_number
		and p.company_id = a.company_id
		and p.funding_wo_indicator = a.funding_wo_indicator
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.work_order_id is null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing work_order_id as errors
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - cannot derive work_order_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and not exists (
		select 1 from work_order_control b
		where b.work_order_id = nvl(a.work_order_id,-596)
		)
	;
	if uf_check_sql() = -1 then return -1
	

	//--------------------------------------------------------------------------------
	//
	// JOB TASK
	//
	//--------------------------------------------------------------------------------
	string task_table
	if a_funding_wo_indicator = 1 then
		task_table = lower(trim(f_pp_system_control_company('FP Ests-Job Task Source Table', a_company)))
	else
		task_table = lower(trim(f_pp_system_control_company('WO Ests-Job Task Source Table', a_company)))
	end if
	
	if task_table = 'job_task' then
		uf_msgs("Mapping Job Task Id (job_task)")
		
		// Map on job_task_id
		update wo_interface_monthly a
		set a.job_task_id = (
			select p.job_task_id
			from job_task p
			where p.job_task_id = a.ext_job_task
			and p.work_order_id = a.work_order_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.job_task_id is null
		and a.ext_job_task is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on external_job_task
		update wo_interface_monthly a
		set a.job_task_id = (
			select p.job_task_id
			from job_task p
			where p.external_job_task = a.ext_job_task
			and p.work_order_id = a.work_order_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.job_task_id is null
		and a.ext_job_task is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on job_task description
		update wo_interface_monthly a
		set a.job_task_id = (
			select p.job_task_id
			from job_task p
			where p.description = a.ext_job_task
			and p.work_order_id = a.work_order_id
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.job_task_id is null
		and a.ext_job_task is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
		// Flag records where ext_job_task could not be mapped
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - cannot derive job_task_id (job_task); '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and (a.job_task_id is not null or a.ext_job_task is not null)
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		and not exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			and p.job_task_id = nvl(a.job_task_id,'-596')
			)
		;
		if uf_check_sql() = -1 then return -1
	else
	
		//--------------------------------------------------------------------------------
		//
		// JOB TASK LIST
		//
		//--------------------------------------------------------------------------------
		uf_msgs("Mapping Job Task Id (job_task_list)")
		
		// Map on job_task_id
		update wo_interface_monthly a
		set a.job_task_id = (
			select p.job_task_id
			from job_task_list p
			where p.job_task_id = a.ext_job_task
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.job_task_id is null
		and a.ext_job_task is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and not exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on external_job_task
		update wo_interface_monthly a
		set a.job_task_id = (
			select p.job_task_id
			from job_task_list p
			where p.external_job_task = a.ext_job_task
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.job_task_id is null
		and a.ext_job_task is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and not exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on job_task description
		update wo_interface_monthly a
		set a.job_task_id = (
			select p.job_task_id
			from job_task_list p
			where p.description = a.ext_job_task
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.job_task_id is null
		and a.ext_job_task is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and not exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		;
		if uf_check_sql() = -1 then return -1
		
		// Flag records where ext_job_task could not be mapped
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - cannot derive job_task_id (job_task_list); '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and (a.job_task_id is not null or a.ext_job_task is not null)
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and not exists (
			select 1 from job_task p
			where p.work_order_id = a.work_order_id
			)
		and not exists (
			select 1 from job_task_list p
			where p.job_task_id = nvl(a.job_task_id,'-596')
			)
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// WO WORK ORDER ID
	//
	//--------------------------------------------------------------------------------
	if a_funding_wo_indicator = 1 then
		uf_msgs("Mapping WO Work Order Id")
		
		// Map on work_order_number
		update wo_interface_monthly a
		set a.wo_work_order_id = (
			select p.work_order_id
			from work_order_control p
			where p.work_order_number = a.ext_wo_work_order_number
			and p.company_id = a.company_id
			and p.funding_wo_indicator = 0
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = 1
		and a.wo_work_order_id is null
		and a.ext_wo_work_order_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on external_wo_number
		update wo_interface_monthly a
		set a.wo_work_order_id = (
			select p.work_order_id
			from work_order_control p
			where p.external_wo_number = a.ext_wo_work_order_number
			and p.company_id = a.company_id
			and p.funding_wo_indicator = 0
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = 1
		and a.wo_work_order_id is null
		and a.ext_wo_work_order_number is not null
		;
		if uf_check_sql() = -1 then return -1
		
		//BCM 03/13/2013 - Updated to pick max description
		//BCM 03/13/2013 - Updated to set it to the right field on wo_interface_monthly
		// Map on description
		update wo_interface_monthly a
		//set a.work_order_id = (
		set a.wo_work_order_id = (
			select max(p.work_order_id)
			from work_order_control p
			where p.description = a.ext_wo_work_order_number
			and p.company_id = a.company_id
			and p.funding_wo_indicator = 0
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.wo_work_order_id is null
		and a.ext_wo_work_order_number is not null
		and a.funding_wo_indicator = 1
		;
		if uf_check_sql() = -1 then return -1
		
		//--BCM 03/13/2013 - Updated to set it to the right field on wo_interface_monthly (work_order_id to wo_work_order_id)
		//-- Updated to set matching to ext_wo_work_order_number instead of work_order_number
		//--Updated to where funding_wo_indicator = 0 on Work Order Controls
		// Map on work_order_id
		update wo_interface_monthly a
		//set a.work_order_id = (
		set a.wo_work_order_id = (
			select p.work_order_id
			from work_order_control p
			where to_char(p.work_order_id) = a.ext_wo_work_order_number //a.work_order_number
			and p.company_id = a.company_id
			and p.funding_wo_indicator = 0 //a.funding_wo_indicator
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.wo_work_order_id is null
		and a.ext_wo_work_order_number is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		// Flag records missing work_order_id as errors
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - cannot derive wo work_order_id; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.funding_wo_indicator = 1
		and a.ext_wo_work_order_number is not null
		and not exists (
			select 1 from work_order_control b
			where b.work_order_id = nvl(a.wo_work_order_id,-596)
			)
		;
		if uf_check_sql() = -1 then return -1
	end if
	



     //--------------------------------------------------------------------------------
	//
	// EXPENDITURE TYPE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Expenditure Type")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on description
	update wo_interface_monthly a
	set a.expenditure_type_id = (
		select p.expenditure_type_id
		from expenditure_type p
		where p.description = a.ext_expenditure_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on expenditure_type_id
	update wo_interface_monthly a
	set a.expenditure_type_id = (
		select p.expenditure_type_id
		from expenditure_type p
		where to_char(p.expenditure_type_id) = a.ext_expenditure_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on notes
	update wo_interface_monthly a
	set a.expenditure_type_id = (
		select p.expenditure_type_id
		from expenditure_type p
		where p.notes = a.ext_expenditure_type
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where expenditure_type_id could not be mapped
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - cannot derive expenditure_type_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	// Flag records missing expenditure_type_id as errors
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - expenditure_type_id is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.expenditure_type_id is null
	and a.ext_expenditure_type is null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
     //--------------------------------------------------------------------------------
	//
	// ESTIMATE CHARGE TYPE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Estimate Charge Type")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_est_chg_type
	update wo_interface_monthly a
	set a.est_chg_type_id = (
		select p.est_chg_type_id
		from estimate_charge_type p
		where p.external_est_chg_type = a.ext_est_chg_type
		and nvl(p.funding_charge_indicator,0) = a.funding_wo_indicator
		and p.processing_type_id <> 5
		and nvl(p.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on estimate_charge_type description
	update wo_interface_monthly a
	set a.est_chg_type_id = (
		select p.est_chg_type_id
		from estimate_charge_type p
		where p.description = a.ext_est_chg_type
		and nvl(p.funding_charge_indicator,0) = a.funding_wo_indicator
		and p.processing_type_id <> 5
		and nvl(p.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on est_chg_type_id
	update wo_interface_monthly a
	set a.est_chg_type_id = (
		select p.est_chg_type_id
		from estimate_charge_type p
		where to_char(p.est_chg_type_id) = a.ext_est_chg_type
		and nvl(p.funding_charge_indicator,0) = a.funding_wo_indicator
		and p.processing_type_id <> 5
		and nvl(p.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on external_cost_element
	update wo_interface_monthly a
	set a.est_chg_type_id = (
		select decode(a.funding_wo_indicator, 1, ect.funding_chg_type, ect.est_chg_type_id)
		from work_order_account woa, wo_est_hierarchy_map map, cost_element ce, estimate_charge_type ect
		where woa.work_order_id = a.work_order_id
		and woa.wo_est_hierarchy_id = map.wo_est_hierarchy_id
		and ce.external_cost_element = a.ext_est_chg_type
		and ce.cost_element_id = map.cost_element_id
		and map.est_chg_type_id = ect.est_chg_type_id
		and nvl(ect.funding_charge_indicator, 0) = 0
		and ect.processing_type_id <> 5
		and nvl(ect.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on cost_element description
	update wo_interface_monthly a
	set a.est_chg_type_id = (
		select decode(a.funding_wo_indicator, 1, ect.funding_chg_type, ect.est_chg_type_id)
		from work_order_account woa, wo_est_hierarchy_map map, cost_element ce, estimate_charge_type ect
		where woa.work_order_id = a.work_order_id
		and woa.wo_est_hierarchy_id = map.wo_est_hierarchy_id
		and ce.description = a.ext_est_chg_type
		and ce.cost_element_id = map.cost_element_id
		and map.est_chg_type_id = ect.est_chg_type_id
		and nvl(ect.funding_charge_indicator, 0) = 0
		and ect.processing_type_id <> 5
		and nvl(ect.afudc_flag,'Q') <> 'W'
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where est_chg_type_id could not be mapped
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - cannot derive est_chg_type_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	// Flag records missing est_chg_type_id as errors
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - estimate-charge-type is required; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.est_chg_type_id is null
	and a.ext_est_chg_type is null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
     //--------------------------------------------------------------------------------
	//
	// DEPARTMENT
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Department")
	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	
	// Map on external_department_code
	update wo_interface_monthly a
	set a.department_id = (
		select p.department_id
		from department p
		where p.external_department_code = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on department description
	update wo_interface_monthly a
	set a.department_id = (
		select p.department_id
		from department p
		where p.description = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on department_id
	update wo_interface_monthly a
	set a.department_id = (
		select p.department_id
		from department p
		where to_char(p.department_id) = a.ext_department
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records where department_id could not be mapped
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - cannot derive department_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.department_id is null
	and a.ext_department is not null
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1
	
	////
	////	No external value was provided
	////
	
	//--------------------------------------------------------------------------------
	//
	// Utility Account
	//
	//--------------------------------------------------------------------------------
	
	if a_funding_wo_indicator = 0 then
		//Map UA ID on description for Work Orders
		update wo_interface_monthly a
		set a.utility_account_id = (
			select utility_account_id
			from utility_account u, work_order_control woc
			where u.bus_segment_id = woc.bus_segment_id
				and woc.work_order_id = a.work_order_id
				and u.description = a.ext_utility_account)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		//Map UA ID on external account code for Work Orders
		update wo_interface_monthly a
		set a.utility_account_id = (
			select utility_account_id
			from utility_account u, work_order_control woc
			where u.bus_segment_id = woc.bus_segment_id
				and woc.work_order_id = a.work_order_id
				and u.external_account_code= a.ext_utility_account)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		// Map UA ID on utility_account_id for Work Orders 
		update wo_interface_monthly a
		set a.utility_account_id = (
			select utility_account_id
			from utility_account u, work_order_control woc
			where u.bus_segment_id = woc.bus_segment_id
				and woc.work_order_id = a.work_order_id
				and to_char(u.utility_account_id) = a.ext_utility_account)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		//BCM 03/13/2013 updated error_message so that it concatenates instead of overwrites
		
		update wo_interface_monthly a
		set error_message = error_message||'; Monthly - cannot derive Utility Account'
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator 
		;
		if uf_check_sql() = -1 then return -1
		
	elseif a_funding_wo_indicator = 1 then
		
		update wo_interface_monthly a
		set a.utility_account_id = (
			select budget_plant_class_id
			from budget_plant_class bpc
			where  bpc.description = a.ext_utility_account)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
	
		update wo_interface_monthly a
		set a.utility_account_id = (
			select budget_plant_class_id
			from budget_plant_class bpc
			where  bpc.external_account_code = a.ext_utility_account)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		update wo_interface_monthly a
		set a.utility_account_id = (
			select budget_plant_class_id
			from budget_plant_class bpc
			where  to_char(bpc.budget_plant_class_id) = a.ext_utility_account)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		//BCM 03/13/2013 Updated error_message so that it concatenates to itself
		
		update wo_interface_monthly a
		set error_message = error_message||'; Monthly - cannot derive Budget Plant Class'
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.utility_account_id is null
		and a.ext_utility_account is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator 
		;
		if uf_check_sql() = -1 then return -1
		
	end if
	
	
	//--------------------------------------------------------------------------------
	//
	// BUDGET ID
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Mapping Budget Id")
	
	// Map on budget_number
	update wo_interface_monthly a
	set a.budget_id = (
		select p.budget_id
		from budget p
		where p.budget_number = a.ext_budget_number
		and p.company_id = a.company_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator 
	and a.budget_id is null
	and a.ext_budget_number is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on description
	update wo_interface_monthly a
	set a.budget_id = (
		select max(p.budget_id)
		from budget p
		where p.description = a.ext_budget_number
		and p.company_id = a.company_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator 
	and a.budget_id is null
	and a.ext_budget_number is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Map on budget_id
	update wo_interface_monthly a
	set a.budget_id = (
		select p.budget_id
		from budget p
		where to_char(p.budget_id) = a.ext_budget_number
		and p.company_id = a.company_id
		)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and a.budget_id is null
	and a.ext_budget_number is not null
	;
	if uf_check_sql() = -1 then return -1
	
	// Flag records missing work_order_id as errors
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - cannot derive budget_id; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and a.ext_budget_number is not null
	and not exists (
		select 1 from budget b
		where b.budget_id = nvl(a.budget_id,-596)
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	//--------------------------------------------------------------------------------
	//
	// ATTRIBUTE 01 - 10
	//
	//--------------------------------------------------------------------------------
	for iii = 1 to 10
		
		uf_msgs("Mapping Attribute "+string(ii,"00"))
		
		// No configurable mappings exist for these fields yet - stage them as-is into the ID fields
		// PP-45857
		sqls = &
		"update wo_interface_monthly a "+&
		"set a.attribute"+string(iii,"00")+"_id = a.ext_attribute"+string(iii,"00")+" "+&
		"where a.status = 1 "+&
		"and nvl(a.batch_id,'null') = '" + a_batch + "' "+&
		"and (a.company_id = " + string(a_company) + " or " + string(a_company) + " = -1) "+&
		"and a.funding_wo_indicator = " + string(a_funding_wo_indicator) + " "+&
		"and a.attribute"+string(iii,"00")+"_id is null "+&
		"and a.ext_attribute"+string(iii,"00")+" is not null "
		
		execute immediate :sqls;
		
		if uf_check_sql() = -1 then return -1
		
	next
	
	
	
	
	//--------------------------------------------------------------------------------
	//
	// BUDGET VERSION
	//
	//--------------------------------------------------------------------------------
	if a_funding_wo_indicator = 1 then
		uf_msgs("Mapping Budget Version")
		
		////
		////	If an external value is provided, try to map on it and error out if it cannot be mapped
		////
		
		// Map on description
		update wo_interface_monthly a
		set a.budget_version_id = (
			select p.budget_version_id
			from budget_version p
			where p.description = a.ext_budget_version
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.budget_version_id is null
		and a.ext_budget_version is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		//BCM 03/13/2013 Updated p.budget_version_id to 'to_char(p.budget_version)
		// Map on Budget Version ID
		update wo_interface_monthly a
		set a.budget_version_id = (
			select p.budget_version_id
			from budget_version p
			where to_char(p.budget_version_id) = a.ext_budget_version
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.budget_version_id is null
		and a.ext_budget_version is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		// Map on external code
		update wo_interface_monthly a
		set a.budget_version_id = (
			select p.budget_version_id
			from budget_version p
			where p.external_budget_version = a.ext_budget_version
			)
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.budget_version_id is null
		and a.ext_budget_version is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		// Flag records where expenditure_type_id could not be mapped
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - cannot derive Budget Version; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
		and a.budget_version_id is null
		and a.ext_budget_version is not null
		and a.funding_wo_indicator = :a_funding_wo_indicator
		;
		if uf_check_sql() = -1 then return -1
		
		////
		////	No external value was provided
		////
		
				///BUDGET VERSION is not required
	end if

		
	//--------------------------------------------------------------------------------
	//
	// Est Monthly ID
	//
	//--------------------------------------------------------------------------------

	
	////
	////	If an external value is provided, try to map on it and error out if it cannot be mapped
	////
	uf_msgs("Mapping Est Monthly Id")
	// Map on external_department_code
	//BCM 03/13/2013 add 'and a.error_message is null'
	update wo_interface_monthly a
	set a.est_monthly_id = pwrplant1.nextval
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1
	
	//--------------------------------------------------------------------------------
	//
	//Fix Totals and null values
	//
	//--------------------------------------------------------------------------------

	
	////
	////	FIX NULLS AND TOTALS
	////
	uf_msgs("Updating Totals")
	// Map on external_department_code
	update wo_interface_monthly a
	set JANUARY = nvl(JANUARY,0), 
		FEBRUARY = nvl(FEBRUARY,0), 
		MARCH = nvl(MARCH,0), 
		APRIL = nvl(APRIL,0), 
		MAY = nvl(MAY,0), 
		JUNE = nvl(JUNE,0), 
		JULY = nvl(JULY,0), 
		AUGUST = nvl(AUGUST,0), 
		SEPTEMBER = nvl(SEPTEMBER,0), 
		OCTOBER = nvl(OCTOBER,0), 
		NOVEMBER = nvl(NOVEMBER,0), 
		DECEMBER = nvl(DECEMBER,0), 
		TOTAL = NVL(JANUARY,0) + NVL(FEBRUARY,0) + NVL(MARCH,0) + NVL(APRIL,0) + NVL(MAY,0) + NVL(JUNE,0) + 
					NVL(JULY,0) +	NVL(AUGUST,0) + NVL(SEPTEMBER,0) + NVL(OCTOBER,0) + NVL(NOVEMBER,0) + NVL(DECEMBER,0),
		HRS_JAN = nvl(HRS_JAN,0), 
		HRS_FEB = nvl(HRS_FEB,0), 
		HRS_MAR = nvl(HRS_MAR,0), 
		HRS_APR = nvl(HRS_APR,0), 
		HRS_MAY = nvl(HRS_MAY,0), 
		HRS_JUN = nvl(HRS_JUN,0), 
		HRS_JUL = nvl(HRS_JUL,0), 
		HRS_AUG = nvl(HRS_AUG,0), 
		HRS_SEP = nvl(HRS_SEP,0), 
		HRS_OCT = nvl(HRS_OCT,0), 
		HRS_NOV = nvl(HRS_NOV,0), 
		HRS_DEC = nvl(HRS_DEC,0), 
		HRS_TOTAL = nvl(HRS_JAN,0) + nvl(HRS_FEB,0) +  nvl(HRS_MAR,0) + nvl(HRS_APR,0) + nvl(HRS_MAY,0) +  nvl(HRS_JUN,0) + 
							nvl(HRS_JUL,0) + nvl(HRS_AUG,0) + nvl(HRS_SEP,0) +  nvl(HRS_OCT,0) + nvl(HRS_NOV,0) + nvl(HRS_DEC,0),
		QTY_JAN = nvl(QTY_JAN,0), 
		QTY_FEB = nvl(QTY_FEB,0), 
		QTY_MAR = nvl(QTY_MAR,0), 
		QTY_APR = nvl(QTY_APR,0), 
		QTY_MAY = nvl(QTY_MAY,0), 
		QTY_JUN = nvl(QTY_JUN,0), 
		QTY_JUL = nvl(QTY_JUL,0), 
		QTY_AUG = nvl(QTY_AUG,0), 
		QTY_SEP = nvl(QTY_SEP,0), 
		QTY_OCT = nvl(QTY_OCT,0), 
		QTY_NOV = nvl(QTY_NOV,0), 
		QTY_DEC = nvl(QTY_DEC,0), 
		QTY_TOTAL =  nvl(QTY_JAN,0) + nvl(QTY_FEB,0) +  nvl(QTY_MAR,0) + nvl(QTY_APR,0) +  nvl(QTY_MAY,0) + nvl(QTY_JUN,0) + 
							nvl(QTY_JUL,0) + nvl(QTY_AUG,0) +  nvl(QTY_SEP,0) + nvl(QTY_OCT,0) + nvl(QTY_NOV,0) + nvl(QTY_DEC,0)
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.funding_wo_indicator = :a_funding_wo_indicator
	;
	if uf_check_sql() = -1 then return -1

  
    //--------------------------------------------------------------------------------
	//
	// DATAMODEL VALIDATIONS
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Validating Expenditure Type / Est Charge Type Relationship")
	
	// Flag records with invalid mappings between expenditure_type_id and est_chg_type_id as errors
	update wo_interface_monthly a
	set a.error_message = a.error_message ||'  Monthly - invalid expenditure type/est charge type combination; '
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and not exists (
		select 1 from exp_type_est_chg_type_view p
		where p.expenditure_type_id = a.expenditure_type_id
		and p.est_chg_type_id = a.est_chg_type_id
		)
	;
	if uf_check_sql() = -1 then return -1
	string dep_validate
	dep_validate = lower(trim(f_pp_system_control_company('WOEST - RESTRICT DEPT', a_company)))

//PP-44420
	if dep_validate = 'yes' then
		uf_msgs("Validating Department/ Company Relationship")
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - invalid department /Company combination; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and not exists (
			select 1 from department p, division d
			where p.department_id = a.department_id
			and d.company_id = a.company_id
			and p.division_id = d.division_id
			and a.department_id is not null
			)
		;
		if uf_check_sql() = -1 then return -1		
	end if
	
	
	
//	dep_blank = lower(trim(f_pp_system_control_company('WOEST - REQUIRE DEPT FOR BLANKETS', a_company)))
//	
//	if dep_blank = 'yes' then
//		uf_msgs("Validating Blanket Department Relationship")
//		update wo_interface_monthly a
//		set a.error_message = a.error_message ||'  Monthly - Departments required on blankets; '
//		where a.status = 1
//		and nvl(a.batch_id,'null') = :a_batch
//		and (a.company_id = :a_company or :a_company = -1)
//		and a.department_id is null
//		and exists (select 1 from work_order_account woa where woa.work_order_id = a.work_order_id and woa.closing_option_id in (6,7,8,10,11))
//		;
//		if uf_check_sql() = -1 then return -1	
//	end if
		
	
		
		




     //--------------------------------------------------------------------------------
	//
	// CHECK FOR PROCESSING ERRORS
	//  - do not process a work order if REPLACING and ANY of the records for that work order are in error
	//  - do not process a job task if REPLACING and ANY of the records for that job task are in error
	//  - do not process any OCRs whose corresponding estimate_id is in error due to the above conditions
	//
	//--------------------------------------------------------------------------------
	if a_replace then
		uf_msgs("Flagging Processing Errors - WO")
		
		// If replacing, all records for a work order must be valid (no errors)
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - cannot process work orders due to errors; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and exists (
			select 1 from wo_interface_monthly p
			where p.work_order_id = a.work_order_id
			and p.error_message is not null
			)
		and ((a.process_level = 'wo' and funding_wo_indicator = 0) or a.process_level = 'fp')
		;
		if uf_check_sql() = -1 then return -1
		
		uf_msgs("Flagging Processing Errors - Task")
		
		// If replacing, all records for a job task must be valid (no errors)
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - cannot process job tasks due to errors; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and exists (
			select 1 from wo_interface_monthly p
			where p.work_order_id = a.work_order_id
			and p.job_task_id = a.job_task_id
			and p.error_message is not null
			)
		and a.process_level = 'task'
		;
		if uf_check_sql() = -1 then return -1
		
			uf_msgs("Flagging Processing Errors - Wo work Order")
		
		// If replacing, all records for a work order must be valid (no errors)
		update wo_interface_monthly a
		set a.error_message = a.error_message ||'  Monthly - cannot process wo work orders due to errors; '
		where a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and exists (
			select 1 from wo_interface_monthly p
			where p.work_order_id = a.work_order_id
			and p.error_message is not null
			)
		and a.process_level = 'wo' 
		and funding_wo_indicator = 1
		;
		if uf_check_sql() = -1 then return -1
	end if
	//--------------------------------------------------------------------------------
	//
	// COMMIT HERE
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Committing Progress")
	
	// Commit progress thus far
	commit;
	
	insert into temp_work_order (work_order_id, session_id, user_id, batch_report_id)
	select distinct a.work_order_id, userenv('sessionid'), user,0
	from wo_interface_monthly a
	where  a.status = 1
		and nvl(a.batch_id,'null') = :a_batch
		and (a.company_id = :a_company or :a_company = -1)
		and a.error_message is null
		;
	//--------------------------------------------------------------------------------
	//
	// Determine where new REvisions are required
	//
	//--------------------------------------------------------------------------------	

		uf_msgs("Determine New Revisions")
		if a_funding_wo_indicator = 1 then
			update wo_interface_monthly a
			set old_revision = (	
				select revision
				from budget_version_fund_proj bvfp
				where a.work_order_id = bvfp.work_order_id
					and a.budget_version_id = bvfp.budget_version_id
					and bvfp.active = 1)
			 where a.status = 1
				and nvl(a.batch_id,'null') = :a_batch
				and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
				and a.budget_version_id is not null
				and a.funding_wo_indicator = :a_funding_wo_indicator
				and a.old_revision is null
				and a.error_message is null
			;
			if uf_check_sql() = -1 then return -1
			
			update wo_interface_monthly a
			set old_revision = (	
				select max(revision)
				from budget_version_fund_proj bvfp
				where a.work_order_id = bvfp.work_order_id
					and a.budget_version_id = bvfp.budget_version_id)
			 where a.status = 1
				and nvl(a.batch_id,'null') = :a_batch
				and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
				and a.budget_version_id is not null
				and a.funding_wo_indicator = :a_funding_wo_indicator
				and a.old_revision is null
				and a.error_message is null
			;
			if uf_check_sql() = -1 then return -1
		end if
		
		update wo_interface_monthly a
		set old_revision = (
			select max(revision)
			from work_order_approval woa
			where woa.work_order_id = a.work_order_id)
		where a.status = 1
			and nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.budget_version_id is  null
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.old_revision is null
			and a.error_message is null
		;
		if uf_check_sql() = -1 then return -1
		
		update wo_interface_monthly a
		set new_revision = 1
		where a.status = 1
			and nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.old_revision is null
			and a.error_message is null
		;
		if uf_check_sql() = -1 then return -1
		
		update wo_interface_monthly a
		set new_revision = 1
		where a.status = 1
			and nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.old_revision is not null
			and a.error_message is null
			and (exists (
				select 1 from wo_est_month_is_editable t
				where t.work_order_id = a.work_order_id
				and t.revision = a.old_revision
				and t.revision_edit = 0
				)
				or 
				((select lower(trim(p.control_value)) from pp_system_control_companies p
				where p.company_id = a.company_id
				and lower(trim(p.control_name)) = lower('WOEST - API Always New Revision')
				) = 'yes'
				and exists (select 1 from wo_est_monthly z where z.work_order_id = a.work_order_id and z.revision = a.old_revision))) 
		;
		if uf_check_sql() = -1 then return -1
		
		
		uo_revisions = create uo_budget_revision
		sqls = 'select distinct nvl(budget_version_id,-11) from wo_interface_monthly a'+&
					 ' where a.status = 1 '+&
				 "    and nvl(a.batch_id,'null') = '"+a_batch + "' " +&
				 "    and (nvl(company_id,"+string(a_company)+") =  "+string(a_company)+" or "+string(a_company)+" = -1) "+&
				 "    and a.funding_wo_indicator = "+string(a_funding_wo_indicator)+&
				 "    and nvl(new_revision,0) = 1 "+&
				 "    and a.error_message is null "
		rtn = f_get_column(results, sqls)
		if rtn > 0 then
			uf_msgs("Creating new revisions")
			bv_id = results
			for i = 1 to upperbound(bv_id)
				
				if bv_id[i] <> -11 and a_funding_wo_indicator = 1 then
					uo_revisions.i_new_bv_ids[1] = bv_id[i]
				end if
				delete from wo_est_processing_temp;
				if uf_check_sql() = -1 then return -1
				insert into wo_est_processing_temp (work_order_id, revision)
				select distinct a.work_order_id, nvl(a.old_revision,0)
				from wo_interface_monthly a
				where a.status = 1
						and  nvl(a.batch_id,'null') = :a_batch
						and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
						and a.funding_wo_indicator = :a_funding_wo_indicator
						and nvl(new_revision,0) = 1
						and nvl(a.budget_version_id,-11) = :bv_id[i]
				;
				if uf_check_sql() = -1 then return -1
				
				if a_funding_wo_indicator = 1 then 
					wo_fp = 'fp'
				else
					wo_fp = 'wo'
				end if
				rtn = uo_revisions.uf_new_revision(wo_fp)
				if rtn = -1 then return -1
			next
		end if
		
		
		///Now backfill revision
		uf_msgs("Backfilling new Revision Number")
	if a_funding_wo_indicator = 1 then
		//
		// For FPs - load to revision under this BV
		//
		update wo_interface_monthly a
		set revision = (
			select c.revision from budget_version_fund_proj c
			where c.budget_version_id = a.budget_version_id
			and a.work_order_id = c.work_order_id
			and c.active = 1
			)
		where a.status = 1
			and  nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.budget_version_id is not null
		;
		if uf_check_sql() = -1 then return -1
	end if
	//
	// For WOs and BV with not budget version load to max revision
	//
	

	update wo_interface_monthly a
	set revision = (
		select max(d.revision) from work_order_approval d
		where a.work_order_id = d.work_order_id
		)
	where a.status = 1
		and  nvl(a.batch_id,'null') = :a_batch
		and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
		and a.funding_wo_indicator = :a_funding_wo_indicator
		and a.budget_version_id is  null
	;
	if uf_check_sql() = -1 then return -1
	

		
	//--------------------------------------------------------------------------------
	//
	//This shouldn't happen but just in case validate Revision
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Validating Revision")
		update wo_interface_monthly a
		set a.error_message = a.error_message||' Could not derive Revision'
		where a.status = 1
			and  nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.revision is null
			and a.error_message is null
		;
		if uf_check_sql() = -1 then return -1
		
		update wo_interface_monthly a
		set a.error_message = a.error_message||' Revision does not exist'
		where a.status = 1
			and  nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.revision is not null
			and not exists (
				select 1 from work_order_approval t
				where t.work_order_id = a.work_order_id
				and t.revision = a.revision
				)
			
		;
		if uf_check_sql() = -1 then return -1
		
		update wo_interface_monthly a
		set a.error_message = a.error_message||' Revision is locked'
		where a.status = 1
			and  nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.revision is not null
			and exists (
				select 1 from wo_est_month_is_editable t
				where t.work_order_id = a.work_order_id
				and t.revision = a.revision
				and t.revision_edit = 0
				)			
		;
		if uf_check_sql() = -1 then return -1
		
	//--------------------------------------------------------------------------------
	//
	//If we are appending or only replacing year we need to pull back additional data from wo_est_monthly
	//
	//--------------------------------------------------------------------------------
	
	if a_replace and not a_replace_year then
		uf_msgs("Removing Estimates being replaced")
		delete from wo_est_monthly wem
		where exists (
			select 1
			from wo_interface_monthly a
			where a.work_order_id = wem.work_order_id
			and a.revision = wem.revision
			and a.status = 1
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
			and a.error_message is null
			and (a.process_level = 'fp' or (a.process_level = 'wo' and funding_wo_indicator = 0)))
		;
		if uf_check_sql() = -1 then return -1
		
		delete from wo_est_monthly wem
		where exists (
			select 1
			from wo_interface_monthly a
			where a.work_order_id = wem.work_order_id
			and a.revision = wem.revision
			and a.status = 1
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
			and a.error_message is null
			and a.process_level = 'task'
			and a.job_task_id = wem.job_task_id)
		;
		if uf_check_sql() = -1 then return -1
		
		delete from wo_est_monthly wem
		where exists (
			select 1
			from wo_interface_monthly a
			where a.work_order_id = wem.work_order_id
			and a.revision = wem.revision
			and a.status = 1
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
			and a.error_message is null
			and a.process_level = 'wo'
			and a.funding_wo_indicator = 1
			and a.wo_work_order_id = wem.wo_work_order_id)
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	if a_replace and a_replace_year then
		uf_msgs("Removing Estimates being replaced (year)")
		delete from wo_est_monthly wem
		where exists (
			select 1
			from wo_interface_monthly a
			where a.work_order_id = wem.work_order_id
			and a.revision = wem.revision
			and a.status = 1
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
			and a.error_message is null
			and a.year = wem.year
			and (a.process_level = 'fp' or (a.process_level = 'wo' and funding_wo_indicator = 0)))
		;
		if uf_check_sql() = -1 then return -1
		
		delete from wo_est_monthly wem
		where exists (
			select 1
			from wo_interface_monthly a
			where a.work_order_id = wem.work_order_id
			and a.revision = wem.revision
			and a.status = 1
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
			and a.error_message is null
			and a.process_level = 'task'
			and a.year = wem.year
			and a.job_task_id = wem.job_task_id)
		;
		if uf_check_sql() = -1 then return -1
		
		delete from wo_est_monthly wem
		where exists (
			select 1
			from wo_interface_monthly a
			where a.work_order_id = wem.work_order_id
			and a.revision = wem.revision
			and a.status = 1
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and (nvl(a.company_id,:a_company) = :a_company or :a_company = -1)
			and a.error_message is null
			and a.process_level = 'wo'
			and a.funding_wo_indicator = 1
			and a.year = wem.year
			and a.wo_work_order_id = wem.wo_work_order_id)
		;
		if uf_check_sql() = -1 then return -1
	end if
	
	uf_msgs("Sliding Dates")
	
	////Now set  est start dates to the min date with any budgeted dollars where est start date is greater then this min date
	update work_order_approval z
	set est_start_date = (
		select to_date(y.min,'YYYYMM')
		from (
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num)
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num 
				from wo_interface_monthly wem, pp_table_months p
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
				) y
		where y.work_order_id = z.work_order_id
			and y.revision = z.revision
			)
	where exists (
		select 1
		from (   
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num)
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num 
				from wo_interface_monthly wem, pp_table_months p
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
			) y
		where  y.work_order_id = z.work_order_id
			and y.revision = z.revision
			and nvl(z.est_start_date,sysdate+100000) >  to_date(y.min,'YYYYMM')
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	
	////next set  est comp dates to the max date with any budgeted dollars where est comp date less then the max date
	update work_order_approval z
	set est_complete_date = (
		select add_months(to_date(y.max,'YYYYMM'),1) - 1
		from (
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num) max
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num 
				from wo_interface_monthly wem, pp_table_months p
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
				) y
		where y.work_order_id = z.work_order_id
			and y.revision = z.revision
			)
	where exists (
		select 1
		from (   
			select a.work_order_id, a.revision, min(a.month_num) min, max(a.month_num) max
			from (
				select wem.work_order_id, wem.revision, wem.year*100+p.month_num month_num 
				from wo_interface_monthly wem, pp_table_months p
				group by wem.work_order_id, wem.revision, wem.year*100+p.month_num
				having sum(nvl(decode(p.month_num,1,wem.JANUARY ,  2,wem.february,   3,wem.march, 4,wem.april,5,wem.may,
					6,wem.june,7,wem.july,8,wem.august,9,wem.september,10,wem.october,11,wem.november,12,wem.december),0)) <> 0
				) a
			group by a.work_order_id, a.revision
			) y
		where  y.work_order_id = z.work_order_id
			and y.revision = z.revision
			and nvl(z.est_complete_date,sysdate - 100000) <  to_date(y.max,'YYYYMM')
		)
	;
	if uf_check_sql() = -1 then return -1
	
	
	UPDATE work_order_control woc
	SET (est_start_date, est_complete_date) =
		(SELECT woa.est_start_date, woa.est_complete_date
		FROM work_order_approval woa, wo_est_revision_view rev
		WHERE woa.work_order_id = woc.work_order_id
		AND woa.work_order_id = rev.work_order_id
		AND woa.revision = rev.max_revision)
	WHERE EXISTS
		(SELECT 1
		FROM wo_interface_monthly w
		WHERE w.work_order_id = woc.work_order_id)
	AND EXISTS 
		(SELECT 1
		FROM pp_system_control_companies p
		WHERE p.company_id = woc.company_id
		AND UPPER(TRIM(p.control_name)) = UPPER(TRIM(DECODE(:a_funding_wo_indicator, 1, 'WOEST - FP DATE CHANGE', 'WOEST - WO DATE CHANGE')))
		AND INSTR(UPPER(TRIM(p.control_value)), UPPER(TRIM('always update'))) > 0);
	if uf_check_sql() = -1 then return -1


		
		

	  //-------------------------------------------------------------------------------
	  //Insert into wo_interface_monthly table
	  //-------------------------------------------------------------------------------
	  uf_msgs("Insert into wo est monthly")
	  insert into wo_est_monthly (
	  EST_MONTHLY_ID, 	WORK_ORDER_ID, 	REVISION, 	YEAR, 	EXPENDITURE_TYPE_ID, 	EST_CHG_TYPE_ID, 
		DEPARTMENT_ID, 	JANUARY, 	FEBRUARY, 	MARCH, 	APRIL, 	MAY, 	JUNE, JULY, 	AUGUST, 	SEPTEMBER, 	OCTOBER, 	NOVEMBER, 	DECEMBER, 
		TOTAL, 	UTILITY_ACCOUNT_ID, LONG_DESCRIPTION, 	FUTURE_DOLLARS, 	HIST_ACTUALS, 	JOB_TASK_ID,
		HRS_JAN, 	HRS_FEB, 	HRS_MAR, 	HRS_APR, 	HRS_MAY, 	HRS_JUN, 	HRS_JUL, 	HRS_AUG, 	HRS_SEP, 	HRS_OCT, 
		HRS_NOV, 	HRS_DEC, 	HRS_TOTAL, 	QTY_JAN, 	QTY_FEB, 	QTY_MAR, 	QTY_APR, QTY_MAY, 	QTY_JUN, 	QTY_JUL, 	QTY_AUG, 
		QTY_SEP, 	QTY_OCT, 	QTY_NOV, 	QTY_DEC, 	QTY_TOTAL, 	WO_WORK_ORDER_ID, BUDGET_ID,
		ATTRIBUTE01_ID, ATTRIBUTE02_ID, ATTRIBUTE03_ID, ATTRIBUTE04_ID, ATTRIBUTE05_ID, ATTRIBUTE06_ID, ATTRIBUTE07_ID, ATTRIBUTE08_ID, ATTRIBUTE09_ID, ATTRIBUTE10_ID)
		select EST_MONTHLY_ID, 	WORK_ORDER_ID, 	REVISION, 	YEAR, 	EXPENDITURE_TYPE_ID, 	EST_CHG_TYPE_ID, 	DEPARTMENT_ID, 
		JANUARY, 	FEBRUARY, 	MARCH, 	APRIL, 	MAY, 	JUNE, 	JULY, 	AUGUST, 	SEPTEMBER, 	OCTOBER, 	NOVEMBER, 	DECEMBER, 
		TOTAL, 	UTILITY_ACCOUNT_ID, 	LONG_DESCRIPTION, 	0 FUTURE_DOLLARS, 	0 HIST_ACTUALS, 	JOB_TASK_ID, hrs_jan,	HRS_FEB, 	HRS_MAR, 	HRS_APR, 	HRS_MAY, 	HRS_JUN, 
		HRS_JUL, 	HRS_AUG, 	HRS_SEP, 	HRS_OCT, 	HRS_NOV, 	HRS_DEC, 	HRS_TOTAL, 	QTY_JAN, QTY_FEB, 	QTY_MAR, 	QTY_APR, 	QTY_MAY, 
		QTY_JUN, 	QTY_JUL, 	QTY_AUG, 	QTY_SEP, 	QTY_OCT, 	QTY_NOV, 	QTY_DEC, 	QTY_TOTAL, 	WO_WORK_ORDER_ID, BUDGET_ID,
		ATTRIBUTE01_ID, ATTRIBUTE02_ID, ATTRIBUTE03_ID, ATTRIBUTE04_ID, ATTRIBUTE05_ID, ATTRIBUTE06_ID, ATTRIBUTE07_ID, ATTRIBUTE08_ID, ATTRIBUTE09_ID, ATTRIBUTE10_ID
		FROM WO_INTERFACE_MONTHLY  a
		WHERE a.status = 1
			and  nvl(a.batch_id,'null') = :a_batch
			and (nvl(company_id,:a_company) = :a_company or :a_company = -1)
			and a.funding_wo_indicator = :a_funding_wo_indicator
			and a.error_message is null
		;
		if uf_check_sql() = -1 then return -1

	  
	  //--------------------------------------------------------------------------------
	//
	// MARK RECORDS AS PROCESSED
	//
	//--------------------------------------------------------------------------------
	uf_msgs("Interface Status Update")
	
	// Update status to indicate it has been processed
	update wo_interface_monthly a
	set status = 2
	where a.status = 1
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.error_message is null
	;
	if uf_check_sql() = -1 then return -1

Next

insert into wo_interface_monthly_arc(
ROW_ID,TIME_STAMP,USER_ID,WORK_ORDER_NUMBER,EXT_COMPANY,EXT_EXPENDITURE_TYPE,EXT_EST_CHG_TYPE,EXT_DEPARTMENT,
EXT_JOB_TASK,EXT_UTILITY_ACCOUNT,LONG_DESCRIPTION,EXT_WO_WORK_ORDER_NUMBER,YEAR,JANUARY,
FEBRUARY,MARCH,APRIL,MAY,JUNE,JULY,AUGUST,SEPTEMBER,OCTOBER,NOVEMBER,DECEMBER,TOTAL,HRS_JAN,HRS_FEB,HRS_MAR,HRS_APR,HRS_MAY,
HRS_JUN,HRS_JUL,HRS_AUG,HRS_SEP,HRS_OCT,HRS_NOV,HRS_DEC,HRS_TOTAL,QTY_JAN,QTY_FEB,QTY_MAR,QTY_APR,QTY_MAY,QTY_JUN,
QTY_JUL,QTY_AUG,QTY_SEP,QTY_OCT,QTY_NOV,QTY_DEC,QTY_TOTAL,EXT_BUDGET_VERSION,WORK_ORDER_ID,COMPANY_ID,EXPENDITURE_TYPE_ID,
EST_CHG_TYPE_ID,DEPARTMENT_ID,JOB_TASK_ID,UTILITY_ACCOUNT_ID,WO_WORK_ORDER_ID,BUDGET_VERSION_ID,EST_MONTHLY_ID,BATCH_ID,
FUNDING_WO_INDICATOR,SEQ_ID,PROCESS_LEVEL,ERROR_MESSAGE,STATUS,OLD_REVISION,NEW_REVISION,REVISION, BUDGET_ID, EXT_BUDGET_NUMBER,
ATTRIBUTE01_ID, ATTRIBUTE02_ID, ATTRIBUTE03_ID, ATTRIBUTE04_ID, ATTRIBUTE05_ID, ATTRIBUTE06_ID, ATTRIBUTE07_ID, ATTRIBUTE08_ID, ATTRIBUTE09_ID, ATTRIBUTE10_ID,
EXT_ATTRIBUTE01, EXT_ATTRIBUTE02, EXT_ATTRIBUTE03, EXT_ATTRIBUTE04, EXT_ATTRIBUTE05, EXT_ATTRIBUTE06, EXT_ATTRIBUTE07, EXT_ATTRIBUTE08, EXT_ATTRIBUTE09, EXT_ATTRIBUTE10)
select ROW_ID,TIME_STAMP,USER_ID,WORK_ORDER_NUMBER,EXT_COMPANY,EXT_EXPENDITURE_TYPE,EXT_EST_CHG_TYPE,EXT_DEPARTMENT,
EXT_JOB_TASK,EXT_UTILITY_ACCOUNT,LONG_DESCRIPTION,EXT_WO_WORK_ORDER_NUMBER,YEAR,JANUARY,
FEBRUARY,MARCH,APRIL,MAY,JUNE,JULY,AUGUST,SEPTEMBER,OCTOBER,NOVEMBER,DECEMBER,TOTAL,HRS_JAN,HRS_FEB,HRS_MAR,HRS_APR,HRS_MAY,
HRS_JUN,HRS_JUL,HRS_AUG,HRS_SEP,HRS_OCT,HRS_NOV,HRS_DEC,HRS_TOTAL,QTY_JAN,QTY_FEB,QTY_MAR,QTY_APR,QTY_MAY,QTY_JUN,
QTY_JUL,QTY_AUG,QTY_SEP,QTY_OCT,QTY_NOV,QTY_DEC,QTY_TOTAL,EXT_BUDGET_VERSION,WORK_ORDER_ID,COMPANY_ID,EXPENDITURE_TYPE_ID,
EST_CHG_TYPE_ID,DEPARTMENT_ID,JOB_TASK_ID,UTILITY_ACCOUNT_ID,WO_WORK_ORDER_ID,BUDGET_VERSION_ID,EST_MONTHLY_ID,BATCH_ID,
FUNDING_WO_INDICATOR,SEQ_ID,PROCESS_LEVEL,ERROR_MESSAGE,STATUS,OLD_REVISION,NEW_REVISION,REVISION, BUDGET_ID, EXT_BUDGET_NUMBER,
ATTRIBUTE01_ID, ATTRIBUTE02_ID, ATTRIBUTE03_ID, ATTRIBUTE04_ID, ATTRIBUTE05_ID, ATTRIBUTE06_ID, ATTRIBUTE07_ID, ATTRIBUTE08_ID, ATTRIBUTE09_ID, ATTRIBUTE10_ID,
EXT_ATTRIBUTE01, EXT_ATTRIBUTE02, EXT_ATTRIBUTE03, EXT_ATTRIBUTE04, EXT_ATTRIBUTE05, EXT_ATTRIBUTE06, EXT_ATTRIBUTE07, EXT_ATTRIBUTE08, EXT_ATTRIBUTE09, EXT_ATTRIBUTE10
from wo_interface_monthly a
where status = 2
	and nvl(a.batch_id,'null') = :a_batch
	and (a.company_id = :a_company or :a_company = -1)
	and a.error_message is null
;
if uf_check_sql() = -1 then return -1

delete from wo_interface_monthly a
where a.status = 2
and nvl(a.batch_id,'null') = :a_batch
and (a.company_id = :a_company or :a_company = -1)
and a.error_message is null
;
if uf_check_sql() = -1 then return -1



return 1
end function

on uo_wo_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_wo_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

