HA$PBExportHeader$uo_fp_est_restrict.sru
$PBExportComments$merge from jrd;
forward
global type uo_fp_est_restrict from nonvisualobject
end type
end forward

global type uo_fp_est_restrict from nonvisualobject
end type
global uo_fp_est_restrict uo_fp_est_restrict

forward prototypes
public function longlong uf_check_restrict (longlong a_fp_id, longlong a_wo_id, decimal a_total_est_amount)
public function longlong uf_check_restrict_approved (longlong a_fp_id, longlong a_wo_id, decimal a_total_est_amount)
public function longlong uf_check_restrict_budget (longlong a_fp_id, longlong a_wo_id, decimal a_total_est_amount)
public function longlong uf_check_fp_restrict_approved (longlong a_budget_id, longlong a_fp_id, decimal a_total_est_amount)
end prototypes

public function longlong uf_check_restrict (longlong a_fp_id, longlong a_wo_id, decimal a_total_est_amount);decimal {2} available_dollars
string restrict, fp_name, est_table, est_amount
longlong ret, company_id, open_count

if isnull(a_total_est_amount) or a_total_est_amount = 0 then return 1

select company_id into :company_id from work_order_control where work_order_id = :a_wo_id;

// get restrict state from pp_system_control
setnull(restrict)
restrict = lower(f_pp_system_control_company('FP Estimate Restrict', company_id))
if isnull(restrict) or trim(restrict) = "" then restrict = "off"

if pos(restrict,'open only') > 0 then
	select	count(*)
	into :open_count
	from work_order_control
	where work_order_id = :a_fp_id
	and wo_status_id = 2;
	
	if isNull(open_count) then open_count = 0
	
	if open_count = 0 then return 1
	
	if pos(restrict,'budget') > 0 then
		restrict = 'restrict-budget'
	else
		restrict = 'restrict-approved'
	end if
end if

if pos(restrict,'off') > 0 then
	return 1
elseif pos(restrict,'budget') > 0 then
	return uf_check_restrict_budget(a_fp_id, a_wo_id, a_total_est_amount)
else //if pos(restrict,'approved') > 0 then
	return uf_check_restrict_approved(a_fp_id, a_wo_id, a_total_est_amount)
end if

end function

public function longlong uf_check_restrict_approved (longlong a_fp_id, longlong a_wo_id, decimal a_total_est_amount);decimal {2} available_dollars
string restrict, fp_name, est_table, sqls
longlong ret, cnt, company_id
any results[]

if isnull(a_total_est_amount) or a_total_est_amount = 0 then return 1

select company_id into :company_id from work_order_control where work_order_id = :a_wo_id;

// get restrict state from pp_system_control
setnull(restrict)
restrict = lower(f_pp_system_control_company('FP Estimate Restrict', company_id))
if isnull(restrict) or restrict = "" then restrict = "off"

// if restrict is 'off' then do nothing'
if pos(restrict,'approved') = 0 then return 1

// comparing wo_estimate or wo_est_monthly
setnull(est_table)
est_table = lower(f_pp_system_control_company('WOEST - Header from wo_estimate', company_id))
if isnull(est_table) or trim(est_table) = "" then est_table = "no"

if est_table = "yes" then
	//est_table = "wo_estimate"
	est_table = "wo_est_all_view2app"
else
	//est_table = "wo_est_monthly"
	est_table = "wo_est_all_viewapp"
end if

// find available dollars in this funding project
sqls = &
"select nvl(fp_dollars,0) - nvl(spent_dollars,0) ~r~n" +&
"from ~r~n" +&
"	(	select nvl(sum(nvl(wem.total,0)),0) fp_dollars ~r~n" +&
"		from work_order_control woc, wo_est_monthly wem ~r~n" +&
"		where woc.work_order_id = "+string(a_fp_id)+" ~r~n" +&
"		and woc.work_order_id = wem.work_order_id ~r~n" +&
"		and woc.current_revision = wem.revision ~r~n" +&
"	), ~r~n" +&
"	(	select sum(greatest(nvl(appr_dollars,0),nvl(pend_dollars,0),nvl(wo_dollars,0))) spent_dollars ~r~n" +&
"		from ~r~n" +&
"			(	select woc.work_order_id, nvl(sum(nvl(wem.capital,0)),0) + nvl(sum(nvl(wem.expense,0)),0) + nvl(sum(nvl(wem.removal,0)),0) + nvl(sum(nvl(wem.jobbing,0)),0) + nvl(sum(nvl(wem.credits,0)),0) appr_dollars ~r~n" +&
"				from work_order_control woc, "+est_table+" wem ~r~n" +&
"				where woc.funding_wo_id = "+string(a_fp_id)+" ~r~n" +&
"				and woc.work_order_id <> "+string(a_wo_id)+" ~r~n" +&
"				and woc.wo_status_id < 7 ~r~n" +&
"				and woc.work_order_id = wem.work_order_id ~r~n" +&
"				and woc.current_revision = wem.revision ~r~n" +&
"				group by woc.work_order_id ~r~n" +&
"			) appr, ~r~n" +&
"			(	select woc.work_order_id, nvl(sum(nvl(wem.capital,0)),0) + nvl(sum(nvl(wem.expense,0)),0) + nvl(sum(nvl(wem.removal,0)),0) + nvl(sum(nvl(wem.jobbing,0)),0) + nvl(sum(nvl(wem.credits,0)),0) pend_dollars ~r~n" +&
"				from work_order_control woc, work_order_approval woa, "+est_table+" wem ~r~n" +&
"				where woc.funding_wo_id = "+string(a_fp_id)+" ~r~n" +&
"				and woc.work_order_id <> "+string(a_wo_id)+" ~r~n" +&
"				and woc.wo_status_id < 7 ~r~n" +&
"				and woc.work_order_id = woa.work_order_id ~r~n" +&
"				and woa.revision = ( ~r~n" +&
"					select max(a.revision) from work_order_approval a ~r~n" +&
"					where a.work_order_id = woa.work_order_id ~r~n" +&
"					and woa.approval_status_id = 2 ~r~n" +&
"					) ~r~n" +&
"				and woa.work_order_id = wem.work_order_id ~r~n" +&
"				and woa.revision = wem.revision ~r~n" +&
"				group by woc.work_order_id ~r~n" +&
"			) pend, ~r~n" +&
"			(	SELECT work_order_control.work_order_id, nvl(sum(nvl(wo_amount, 0)),0) wo_dollars ~r~n" +&
"				FROM work_order_control, wo_summary, charge_type ~r~n" +&
"				where work_order_control.work_order_id = wo_summary.work_order_id (+) ~r~n" +&
"				and work_order_control.funding_wo_id = "+string(a_fp_id)+" ~r~n" +&
"				and work_order_control.work_order_id <> "+string(a_wo_id)+" ~r~n" +&
"				and wo_summary.charge_type_id = charge_type.charge_type_id ~r~n" +&
"				and nvl(charge_type.exclude_from_total_charges,0) <> 1 ~r~n" +&
"				GROUP BY work_order_control.work_order_id ~r~n" +&
"			) wo ~r~n" +&
"		where appr.work_order_id (+) = wo.work_order_id ~r~n" +&
"		and pend.work_order_id (+) = wo.work_order_id ~r~n" +&
"	) "

cnt = f_get_column(results,sqls)

if cnt > 0 then
	available_dollars = results[1]
else
	available_dollars = 0
end if

//	DML: 010705: If there is no estimate on the funding project available_dollars will be null
if isnull(available_dollars) then available_dollars = 0

// check estimate against available dollars
if a_total_est_amount > available_dollars then
	select work_order_number into :fp_name from work_order_control where work_order_id = :a_fp_id;
	if pos(restrict,'restrict') > 0 then
		messagebox("Funding Project Restriction","You have exceeded the total estimate for funding project: " + fp_name + ".~n~n" +&
			"There is $" + string(available_dollars,'$#,##0.00') + " under the funding project available for this work order.~n~n" +&
			"Estimates will not be saved.", stopsign!)
		return -1
	else
		ret = messagebox("Funding Project Warning","You have exceeded the total estimate for funding project: " + fp_name + ".~n~n" +&
			"There is $" + string(available_dollars,'$#,##0.00') + " under the funding project available for this work order.~n~n" +&
			"Would you like to save changes?", stopsign!, yesno!, 2)
		if ret = 1 then
			return 1
		else
			return -1
		end if 
	end if
end if

return 1

end function

public function longlong uf_check_restrict_budget (longlong a_fp_id, longlong a_wo_id, decimal a_total_est_amount);decimal {2} available_dollars
string restrict, fp_name, est_table, sqls
longlong ret, cnt, company_id
any results[]

if isnull(a_total_est_amount) or a_total_est_amount = 0 then return 1

select company_id into :company_id from work_order_control where work_order_id = :a_wo_id;

// get restrict state from pp_system_control
setnull(restrict)
restrict = lower(f_pp_system_control_company('FP Estimate Restrict', company_id))
if isnull(restrict) or trim(restrict) = "" then restrict = "off"

// if restrict is 'off' then do nothing'
if pos(restrict,'budget') = 0 then return 1

// comparing wo_estimate or wo_est_monthly
setnull(est_table)
est_table = lower(f_pp_system_control_company('WOEST - Header from wo_estimate', company_id))
if isnull(est_table) or trim(est_table) = "" then est_table = "no"

if est_table = "yes" then
	//est_table = "wo_estimate"
	est_table = "wo_est_all_view2app"
else
	//est_table = "wo_est_monthly"
	est_table = "wo_est_all_viewapp"
end if

// find available dollars in this funding project
sqls = &
"select nvl(fp_dollars,0) - nvl(spent_dollars,0) ~r~n" +&
"from ~r~n" +&
"	(	select nvl(sum(nvl(wem.total,0)),0) fp_dollars ~r~n" +&
"		from work_order_control woc, budget_version_fund_proj bvfp, wo_est_monthly wem, budget_version_view bvv ~r~n" +&
"		where woc.work_order_id = "+string(a_fp_id)+" ~r~n" +&
"		and woc.work_order_id = bvfp.work_order_id ~r~n" +&
"		and bvfp.active = 1 ~r~n" +&
"		and bvfp.budget_version_id = bvv.revised_budget ~r~n" +&
"		and woc.company_id = bvv.company_id ~r~n" +&
"		and bvfp.work_order_id = wem.work_order_id ~r~n" +&
"		and bvfp.revision = wem.revision ~r~n" +&
"	), ~r~n" +&
"	(	select sum(greatest(nvl(appr_dollars,0),nvl(pend_dollars,0),nvl(wo_dollars,0))) spent_dollars ~r~n" +&
"		from ~r~n" +&
"			(	select woc.work_order_id, nvl(sum(nvl(wem.capital,0)),0) + nvl(sum(nvl(wem.expense,0)),0) + nvl(sum(nvl(wem.removal,0)),0) + nvl(sum(nvl(wem.jobbing,0)),0) + nvl(sum(nvl(wem.credits,0)),0) appr_dollars ~r~n" +&
"				from work_order_control woc, "+est_table+" wem ~r~n" +&
"				where woc.funding_wo_id = "+string(a_fp_id)+" ~r~n" +&
"				and woc.work_order_id <> "+string(a_wo_id)+" ~r~n" +&
"				and woc.wo_status_id < 7 ~r~n" +&
"				and woc.work_order_id = wem.work_order_id ~r~n" +&
"				and woc.current_revision = wem.revision ~r~n" +&
"				group by woc.work_order_id ~r~n" +&
"			) appr, ~r~n" +&
"			(	select woc.work_order_id,nvl(sum(nvl(wem.capital,0)),0) + nvl(sum(nvl(wem.expense,0)),0) + nvl(sum(nvl(wem.removal,0)),0) + nvl(sum(nvl(wem.jobbing,0)),0) + nvl(sum(nvl(wem.credits,0)),0) pend_dollars ~r~n" +&
"				from work_order_control woc, work_order_approval woa, "+est_table+" wem ~r~n" +&
"				where woc.funding_wo_id = "+string(a_fp_id)+" ~r~n" +&
"				and woc.work_order_id <> "+string(a_wo_id)+" ~r~n" +&
"				and woc.wo_status_id < 7 ~r~n" +&
"				and woc.work_order_id = woa.work_order_id ~r~n" +&
"				and woa.revision = ( ~r~n" +&
"					select max(a.revision) from work_order_approval a ~r~n" +&
"					where a.work_order_id = woa.work_order_id ~r~n" +&
"					and woa.approval_status_id = 2 ~r~n" +&
"					) ~r~n" +&
"				and woa.work_order_id = wem.work_order_id ~r~n" +&
"				and woa.revision = wem.revision ~r~n" +&
"				group by woc.work_order_id ~r~n" +&
"			) pend, ~r~n" +&
"			(	SELECT work_order_control.work_order_id, nvl(sum(nvl(wo_amount, 0)),0) wo_dollars ~r~n" +&
"				FROM work_order_control, wo_summary, charge_type ~r~n" +&
"				where work_order_control.work_order_id = wo_summary.work_order_id (+) ~r~n" +&
"				and work_order_control.funding_wo_id = "+string(a_fp_id)+" ~r~n" +&
"				and work_order_control.work_order_id <> "+string(a_wo_id)+" ~r~n" +&
"				and wo_summary.charge_type_id = charge_type.charge_type_id ~r~n" +&
"				and nvl(charge_type.exclude_from_total_charges,0) <> 1 ~r~n" +&
"				GROUP BY work_order_control.work_order_id ~r~n" +&
"			) wo ~r~n" +&
"		where appr.work_order_id (+) = wo.work_order_id ~r~n" +&
"		and pend.work_order_id (+) = wo.work_order_id ~r~n" +&
"	) "

cnt = f_get_column(results,sqls)

if cnt > 0 then
	available_dollars = results[1]
else
	available_dollars = 0
end if

//	DML: 010705: If there is no estimate on the funding project available_dollars will be null
if isnull(available_dollars) then available_dollars = 0

// check estimate against available dollars
if a_total_est_amount > available_dollars then
	select work_order_number into :fp_name from work_order_control where work_order_id = :a_fp_id;
	if pos(restrict,'restrict') > 0 then
		messagebox("Funding Project Restriction","You have exceeded the total estimate for funding project: " + fp_name + ".~n~n" +&
			"There is $" + string(available_dollars,'$#,##0.00') + " under the funding project available for this work order.~n~n" +&
			"Estimates will not be saved.", stopsign!)
		return -1
	else
		ret = messagebox("Funding Project Warning","You have exceeded the total estimate for funding project: " + fp_name + ".~n~n" +&
			"There is $" + string(available_dollars,'$#,##0.00') + " under the funding project available for this work order.~n~n" +&
			"Would you like to save changes?", stopsign!, yesno!, 2)
		if ret = 1 then
			return 1
		else
			return -1
		end if 
	end if
end if

return 1

end function

public function longlong uf_check_fp_restrict_approved (longlong a_budget_id, longlong a_fp_id, decimal a_total_est_amount);decimal {2} available_dollars
string  restrict, budget_name
int ret
longlong company_id

////THIS FUNCTION IS USED TO RESTRICT FUNDING PROJECT APPROVALS FROM EXCEEDING APPROVED BUDGET ITEM AMOUNTS

if isnull(a_total_est_amount) or a_total_est_amount = 0 then return 1

select company_id into :company_id from work_order_control where work_order_id = :a_fp_id;

// find available dollars in this funding project
select budget_dollars - nvl(spent_dollars,0) into :available_dollars
from
	(	select nvl(nvl(approved_amount_local,approved_amount),0) budget_dollars
		from budget
		where budget_id = :a_budget_id
	),
	(	select sum(total) spent_dollars
		from wo_est_monthly est, work_order_control woc
		where woc.work_order_id = est.work_order_id
		and woc.current_revision = est.revision
		and woc.budget_id = :a_budget_id
		and woc.funding_wo_indicator = 1
		and woc.work_order_id <> :a_fp_id
	);

//	DML: 010705: If there is no estimate on the funding project available_dollars will be null
if isnull(available_dollars) then available_dollars = 0

// check estimate against available dollars
if a_total_est_amount > available_dollars then
	select budget_number into :budget_name from budget where budget_id = :a_budget_id;
	messagebox("Budget Restriction","You have exceeded the total Approved amount for Budget Item: " &
			+ budget_name + ".~n~nThere are $" + string(available_dollars) + &
			" under the Budget Item available.~nEstimates will not be sent for approval.", stopsign!)
		return -1
end if

return 1


end function

on uo_fp_est_restrict.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_fp_est_restrict.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

