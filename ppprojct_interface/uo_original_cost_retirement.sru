HA$PBExportHeader$uo_original_cost_retirement.sru
forward
global type uo_original_cost_retirement from nonvisualobject
end type
end forward

global type uo_original_cost_retirement from nonvisualobject
end type
global uo_original_cost_retirement uo_original_cost_retirement

type variables
longlong i_charge_type_id, i_cost_element_id, i_book_summary_id, i_est_chg_type_id
string i_bk_summary_name
boolean i_ct_lookup_complete = false


end variables

forward prototypes
private function integer uf_process_ocr_validate (string a_batch)
public function integer uf_process_ocr (string a_batch)
private function boolean uf_check_sql_err ()
private function boolean uf_check_sql_err (string a_msg)
private function boolean uf_check_sql_err (string a_msg, boolean a_log_record_count)
private function integer uf_process_ocr_load (string a_batch)
private function integer uf_process_ocr_translate (string a_batch)
private function integer uf_process_ocr_archive (string a_batch)
public function longlong uf_process_ocr_from_estimate (longlong a_work_order_id)
private function longlong uf_lookup_ct ()
public function longlong uf_rollback (longlong a_return)
end prototypes

private function integer uf_process_ocr_validate (string a_batch);longlong rtn, check_count 
string msg

// *******************************************************************************
//  All general validations are handled in this function.  The one validation that is not is the lookup
//	against CPR_LEDGER to validate that assets exists.  This will be done inside of the process
//	loop so that if the same asset is being retired by multiple records, we can fail subsequent records
// *******************************************************************************
f_pp_msgs("Performing data validations")

// *******************************************************************************
//	Proper charge type and cost elements are setup for retirements
// *******************************************************************************
if uf_lookup_ct() < 0 then
	return -1
end if


// *******************************************************************************
//	Validate all required fields are populated
// *******************************************************************************
msg = 'Company is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where company_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Work Order is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where work_order_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'GL Account is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where gl_account_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Business Segment is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where bus_segment_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Utility Account is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where utility_account_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Sub Account is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where sub_account_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Property Group is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where property_group_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Retirement Unit is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where retirement_unit_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Asset Location is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where asset_location_id is null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

// *******************************************************************************
//	Company on work order header must match company id on the retirement
// *******************************************************************************
msg = 'Company value must match the company on the work order'
update WO_INTERFACE_OCR_STG a
		set validation_message = validation_message || :msg || '; '
		where a.company_id <> (select b.company_id from work_order_control b where a.work_order_id = b.work_order_id)
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

// *******************************************************************************
//	Work Order ID cannot point to a funding project
// *******************************************************************************
msg = 'Funding Projects cannot be used for original cost retirements'
update WO_INTERFACE_OCR_STG a
		set validation_message = validation_message || :msg || '; '
		where exists  (select 0 from work_order_control b where a.work_order_id = b.work_order_id and b.funding_wo_indicator = 1)
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1


// *******************************************************************************
//	Validate to ensure only unitized assets are being retired
//		retirements against non-unitized assets are not valid as defined by the retirement unit
// *******************************************************************************
msg = 'Non-unitized assets cannot be retired'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where retirement_unit_id between 1 and 5
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

// *******************************************************************************
//	Quantity and amount cannot both be zero
// *******************************************************************************
msg = 'Non-zero quantity or amount is required'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where quantity = 0 and amount = 0
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

// *******************************************************************************
//	Sign of quantity and amount must match
// *******************************************************************************
msg = 'Quantity and amount must have the same sign (positive / negative)'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where sign(quantity) * sign(amount) < 0
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1


// *******************************************************************************
//	For specific retirements (vintage is populated):
//		Either quantity or amount must be 0.
// *******************************************************************************
msg = '(SPECIFIC ONLY) Quantity and Amount cannot both have non-zero values'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where vintage is not null
		and quantity <> 0 and amount <> 0
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

// *******************************************************************************
//	For mass retirements (vintage is null):
//		Quantity is required
// *******************************************************************************
msg = '(MASS ONLY) Quantity cannot be zero'
update WO_INTERFACE_OCR_STG
		set validation_message = validation_message || :msg || '; '
		where vintage is null
		and quantity = 0 
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

// *******************************************************************************
//	Validate retirement method with the provided vintage
//			1	Specific					Specific only
//			2	FIFO						Specific or mass
//			3	Retire Curve Auto		Neither
//			4	Retire Curve			Specific or mass
//			5	Life Auto					Neither
//			7	HW $$HEX2$$13202000$$ENDHEX$$Curve				Specific or mass
//			6	HW $$HEX2$$13202000$$ENDHEX$$FIFO				Specific or mass
//			8	ARO						Neither
//			9	Specific/Life Auto		Check retire method on property_unit_default_life
// *******************************************************************************
msg = 'Mass retirements cannot be used against specific property'
update WO_INTERFACE_OCR_STG a
		set validation_message = validation_message || :msg || '; '
		where vintage is null
		and exists 
			(select 1
			from retirement_unit ru, property_unit pu
			where ru.retirement_unit_id = a.retirement_unit_id
			and ru.property_unit_id = pu.property_unit_id
			and pu.retire_method_id in (1))
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'OCR retirements cannot be used on the selected retirement method'
update WO_INTERFACE_OCR_STG a
		set validation_message = validation_message || :msg || '; '
		where exists 
			(select 1
			from retirement_unit ru, property_unit pu
			where ru.retirement_unit_id = a.retirement_unit_id
			and ru.property_unit_id = pu.property_unit_id
			and pu.retire_method_id in (3,5,8))
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'Mass retirements cannot be used against specific property (method from property_unit_default_life)'
update WO_INTERFACE_OCR_STG a
		set validation_message = validation_message || :msg || '; '
		where vintage is null
		and exists 
			(select 1
			from retirement_unit ru, property_unit pu
			where ru.retirement_unit_id = a.retirement_unit_id
			and ru.property_unit_id = pu.property_unit_id
			and pu.retire_method_id in (9))
		and exists
			(select 1
			from retirement_unit ru, property_unit_default_life pudl
			where ru.retirement_unit_id = a.retirement_unit_id
			and ru.property_unit_id = pudl.property_unit_id
			and a.company_id = pudl.company_id
			and a.bus_segment_id = pudl.bus_segment_id
			and a.utility_account_id = pudl.utility_account_id
			and nvl(pudl.retire_method_id,1) in (1))
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

msg = 'OCR retirements cannot be used on the selected retirement method (method from property_unit_default_life)'
update WO_INTERFACE_OCR_STG a
		set validation_message = validation_message || :msg || '; '
		where exists 
			(select 1
			from retirement_unit ru, property_unit pu
			where ru.retirement_unit_id = a.retirement_unit_id
			and ru.property_unit_id = pu.property_unit_id
			and pu.retire_method_id in (9))
		and exists
			(select 1
			from retirement_unit ru, property_unit_default_life pudl
			where ru.retirement_unit_id = a.retirement_unit_id
			and ru.property_unit_id = pudl.property_unit_id
			and a.company_id = pudl.company_id
			and a.bus_segment_id = pudl.bus_segment_id
			and a.utility_account_id = pudl.utility_account_id
			and nvl(pudl.retire_method_id,1) in (3,5,8))
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1


// *******************************************************************************
//  Flag records with a validation message as errors
// *******************************************************************************
update WO_INTERFACE_OCR_STG a
		set upload_indicator = 'E'
		where validation_message is not null
		and upload_indicator = 'I'
		and batch_id = :a_batch;
if uf_check_sql_err(msg,true) then return -1

return 0
end function

public function integer uf_process_ocr (string a_batch);longlong rtn

// Populate default fields up front and reset prior kickouts
f_pp_msgs("Defaulting standard fields")
update wo_interface_ocr_stg
	set batch_id = coalesce(batch_id,'null'),
		upload_indicator = coalesce(upload_indicator,'N');
if uf_check_sql_err('Defaulting batch_id and upload_indicator') then return -1

update wo_interface_ocr_stg
	set upload_indicator = 'I',
		validation_message = null,
		work_order_number = upper(work_order_number)
	where batch_id = :a_batch
	and upload_indicator <> 'D';
if uf_check_sql_err('Resetting prior validation kickouts') then return -1

commit;

// Translate
rtn = uf_process_ocr_translate(a_batch)
if rtn < 0 then return uf_rollback(rtn)
commit;

// Validations
rtn = uf_process_ocr_validate(a_batch)
if rtn < 0 then return uf_rollback(rtn)
commit;

// Processing / Load
rtn = uf_process_ocr_load(a_batch)
if rtn < 0 then return uf_rollback(rtn)
commit;

// Archive
rtn = uf_process_ocr_archive(a_batch)
if rtn < 0 then return uf_rollback(rtn)
commit;


return 0
end function

private function boolean uf_check_sql_err ();if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: " + sqlca.sqlerrtext)
	rollback;
	return true
else
	return false
end if
end function

private function boolean uf_check_sql_err (string a_msg);return uf_check_sql_err(a_msg,false)
end function

private function boolean uf_check_sql_err (string a_msg, boolean a_log_record_count);if sqlca.SQLCode < 0 then
	// SQL Error
	f_pp_msgs("ERROR: " + a_msg + ": " + sqlca.sqlerrtext)
	rollback;
	return true
elseif sqlca.SQLNRows > 0 and a_log_record_count then
	// Log the record count
	f_pp_msgs(a_msg + ": " + string(sqlca.SQLNrows) + " rows")
end if

return false
end function

private function integer uf_process_ocr_load (string a_batch);longlong i, num_rets, j, rtn, alloc_id, gain_loss_default
string sqls, msg
uo_ds_top ds_ocr
boolean skip_estimate

longlong row_id, charge_id, charge_group_id, unit_item_id, estimate_id
longlong company_id, work_order_id, asset_id, bus_segment_id, utility_account_id, sub_account_id, &
	property_group_id, retirement_unit_id, asset_location_id, gl_account_id, vintage
string serial_number
double quantity, amount, quantity_array[], amount_array[], empty_double_array[1000]
double asset_id_array[], empty_long_array[1000]
string activity_code, description, long_description

// *******************************************************************************
//	Retrieve the records to be processed.
//		Order by row_id so it processes in the order they were recieved
// *******************************************************************************

//KNW - 21050813 - Added description and long description to the wo_interface_ocr_stg table so that the client can choose if they want the descriptions defaulted to 'OCR' which is how it treats the rows currently, 
//				or use the description and long descriptions from the retirement unit (which is how it is done through the front end). If these columns are left null, 'OCR' will be used.

sqls = "select row_id, company_id, work_order_id, asset_id, bus_segment_id, utility_account_id, sub_account_id, " + &
	"property_group_id, retirement_unit_id, asset_location_id,	gl_account_id, serial_number, vintage, quantity, amount, estimate_id, description, long_description " + &
	"from wo_interface_ocr_stg where batch_id = '" + a_batch + "' and upload_indicator = 'I' order by row_id asc"
ds_ocr = create uo_ds_top
f_create_dynamic_ds(ds_ocr,'grid',sqls,sqlca,true)

f_pp_msgs(string(ds_ocr.rowcount()) + ' records to process.')
for i = 1 to ds_ocr.rowcount()
	if mod(i,50) = 0 or i= ds_ocr.rowcount() then f_pp_msgs(' -- Processing ' + string(i) + ' of ' + string(ds_ocr.rowcount()))
	setnull(row_id)
	setnull(company_id)
	setnull(work_order_id)
	setnull(asset_id)
	setnull(bus_segment_id)
	setnull(utility_account_id)
	setnull(sub_account_id)
	setnull(property_group_id)
	setnull(retirement_unit_id)
	setnull(asset_location_id)
	setnull(gl_account_id)
	setnull(estimate_id)
	setnull(vintage)
	setnull(serial_number)
	setnull(quantity)
	setnull(amount)
	setnull(description)
	setnull(long_description)
	row_id = ds_ocr.GetItemNumber(i,'row_id')
	company_id = ds_ocr.GetItemNumber(i,'company_id')
	work_order_id = ds_ocr.GetItemNumber(i,'work_order_id')
	asset_id = ds_ocr.GetItemNumber(i,'asset_id')
	bus_segment_id = ds_ocr.GetItemNumber(i,'bus_segment_id')
	utility_account_id = ds_ocr.GetItemNumber(i,'utility_account_id')
	sub_account_id = ds_ocr.GetItemNumber(i,'sub_account_id')
	property_group_id = ds_ocr.GetItemNumber(i,'property_group_id')
	retirement_unit_id = ds_ocr.GetItemNumber(i,'retirement_unit_id')
	asset_location_id = ds_ocr.GetItemNumber(i,'asset_location_id')
	gl_account_id = ds_ocr.GetItemNumber(i,'gl_account_id')
	estimate_id = ds_ocr.GetItemNumber(i,'estimate_id')
	vintage = ds_ocr.GetItemNumber(i,'vintage')
	serial_number = ds_ocr.GetItemString(i,'serial_number')
	quantity = ds_ocr.GetItemNumber(i,'quantity')
	amount = ds_ocr.GetItemNumber(i,'amount')
	description = ds_ocr.GetItemString(i,'description')
	long_description = ds_ocr.GetItemString(i,'long_description')
	
	/* empty arrays have 1000 elements each.  Need the arrays to be bigger than the PLSQL Functions will return */
	asset_id_array = empty_long_array
	quantity_array = empty_double_array
	amount_array = empty_double_array
	
	if not isnull(estimate_id) then
		// If they provided an estimate, simply going to link to that and not create a new estimate ourselves.
		skip_estimate = true
	else
		skip_estimate = false
	end if
	
	if isnull(vintage) or vintage = 0 then
		// Mass Retirement
		
		// Validate the mass retirement
		rtn = sqlca.f_pp_ocr_mass_validate(company_id, bus_segment_id, utility_account_id, sub_account_id, property_group_id, &
			retirement_unit_id, asset_location_id, gl_account_id, quantity);
		if uf_check_sql_err('Calling f_pp_ocr_mass_validate') then return -1
		
		if rtn < 0 then
			choose case rtn
				case -10
					msg = 'Quantity cannot be 0 for mass retirements'
				case -11
					msg = 'Quantity must be a whole number for mass retirements'
				case else
					msg = 'Insufficient quantity exists for the retirement'
			end choose
			
			update wo_interface_ocr_stg
				set validation_message = :msg || '; ',
					upload_indicator = 'E'
				where row_id = :row_id;
			if uf_check_sql_err('Updating wo_interface_ocr_stg for mass validation errors') then return -1
			
			continue
		end if
			
		setnull(asset_id)
		asset_id_array[1] = asset_id
		quantity_array[1] = quantity
		amount_array[1] = amount
		num_rets = 1
	else
		// Specific Retirement		
		if not isnull(asset_id) and asset_id <> 0 then
			// Validate the selected asset retirement
			rtn = sqlca.f_pp_ocr_asset_id_validate(asset_id, quantity, amount);
			if uf_check_sql_err('Calling f_pp_ocr_asset_id_validate') then return -1
			
			asset_id_array[1] = asset_id
			quantity_array[1] = quantity
			amount_array[1] = amount
			num_rets = 1
		else
			// Lookup and validate the specific asset retirement
			rtn = sqlca.f_pp_ocr_asset_lookup(company_id, bus_segment_id, utility_account_id, sub_account_id, property_group_id, &
				retirement_unit_id, asset_location_id, gl_account_id,serial_number, vintage, quantity, amount, &
				asset_id_array, quantity_array, amount_array)
			num_rets = rtn
			if sqlca.sqldbcode = 6513 then
				/* ORA-06513: PL/SQL: index for PL/SQL table out of range for host language array */
				rtn = -999
			else
				if uf_check_sql_err('Calling f_pp_ocr_asset_lookup') then return -1
			end if
		end if
		
		if rtn < 0 then
			choose case rtn
				case -10
					msg = 'Quantity or Amount must be non-zero for specific retirements'
				case -11
					msg = 'Quantities and Amounts are limited to 2 decimals'
				case -12
					msg = 'Quantity and Amount must have the same sign'
				case -13
					msg = 'Only Quantity or Amount can be provided for specific assets - not both'
				case -999
					msg = 'Limit of 1000 individual retirements per staged record.  Cannot process this record'
				case else
					msg = 'Insufficient quantity/amount exists for the specific retirement'
			end choose
			
			update wo_interface_ocr_stg
				set validation_message = :msg || '; ',
					upload_indicator = 'E'
				where row_id = :row_id;
			if uf_check_sql_err('Updating wo_interface_ocr_stg for mass validation errors') then return -1
			
			commit;
			continue
		end if
				
	end if
	
	
	for j = 1 to num_rets
		// Had to do this as seperate statements.  One statement with mutliple pwrplant1.nextval retrieved the same value for all 3 records.
		
//PP-44417
//		select pwrplant3.nextval, pwrplant1.nextval
//			into :charge_id, :charge_group_id
//			from dual;			
//		if uf_check_sql_err('Retrieving IDs from sequences') then return -1
//		
//		select pwrplant1.nextval
//			into :unit_item_id
//			from dual;
//		if uf_check_sql_err('Retrieving IDs from sequences') then return -1

		select pwrplant3.nextval
			into :charge_id
			from dual;			
		if uf_check_sql_err('Retrieving IDs from sequences') then return -1

		select  nvl(max(charge_group_id),0) + 1
			into  :charge_group_id
			from  CHARGE_GROUP_CONTROL 
							where work_order_id = :work_order_id
							and charge_group_id >= 0;			
		if uf_check_sql_err('Retrieving ID from charge_group_control') then return -1

		select nvl(max(unit_item_id),0) + 1
					into :unit_item_id
					from UNITIZED_WORK_ORDER b
							where work_order_id = :work_order_id
							and unit_item_id >= 0;
		if uf_check_sql_err('Retrieving ID from unitized_work_order') then return -1
		

		
		if not skip_estimate then
			select pwrplant1.nextval
				into :estimate_id
				from dual;
			if uf_check_sql_err('Retrieving IDs from sequences') then return -1
		end if
		
		// Lookup the alloc id
		setnull(alloc_id)
		select alloc_id into :alloc_id
			from work_order_account woa, unit_alloc_meth_control uamc
			where woa.work_order_id = :work_order_id
			and woa.alloc_method_type_id = uamc.alloc_method_type_id 
			and uamc.charge_type_id = :i_charge_type_id;
		if uf_check_sql_err('Retrieving Alloc ID for OCR Charge Type') then return -1
		
		// If we have an asset_id, lookup the gain loss default
		if isnull(asset_id_array[j]) or asset_id_array[j] = 0 then
			// No asset id >> MRET
			activity_code = 'MRET'
		else
			// Asset ID >> URET
			// Look at the gain_loss_default on the DG for URET / URGL
			select gain_loss_default into :gain_loss_default
				from depr_group dg, cpr_ledger cpr
				where cpr.asset_id = :asset_id_array[j]
				and cpr.depr_group_id = dg.depr_group_id;
			if uf_check_sql_err('Retrieving Gain Loss Default for Asset') then return -1
			
			if isnull(gain_loss_default) or gain_loss_default = 0 then
				activity_code = 'URET'
			else
				activity_code = 'URGL'
			end if
		end if
		
		// *******************************************************************************
		//	UNITIZED_WORK_ORDER
		// *******************************************************************************
		insert into unitized_work_order (unit_item_id,work_order_id,retirement_unit_id,
			property_group_id,utility_account_id,bus_segment_id,sub_account_id,activity_code,
			description,amount,quantity,ldg_asset_id,asset_location_id,company_id,gl_account_id,
			short_description,replacement_amount, unit_estimate_id, retire_vintage)
		values
			(:unit_item_id,:work_order_id,:retirement_unit_id,
			:property_group_id,:utility_account_id,:bus_segment_id,:sub_account_id,:activity_code,
			nvl(:long_description,'OCR') ,:amount_array[j],:quantity_array[j],:asset_id_array[j], :asset_location_id, :company_id, :gl_account_id,
			nvl(:description, 'OCR') ,null, :estimate_id, :vintage);
		if uf_check_sql_err('Insert into unitized_work_order') then return -1
		if sqlca.sqlnrows <> 1 then 
			f_pp_msgs("Invalid number of rows on insert into unitized_work_order: " + string(sqlca.sqlnrows))
			rollback;
			return -1
		end if
		
		// *******************************************************************************
		//	CWIP_CHARGE
		// *******************************************************************************
		insert into cwip_charge (charge_id, charge_type_id, expenditure_type_id,
			work_order_id, retirement_unit_id, charge_mo_yr, description, quantity, utility_account_id,
			bus_segment_id, amount, sub_account_id, notes, department_id, charge_audit_id, journal_code,
			cost_element_id, external_gl_account, gl_account_id, company_id, month_number, asset_location_id, 
			serial_number)
		select :charge_id, :i_charge_type_id, 2 expenditure_type_id,
			:work_order_id, :retirement_unit_id, mn.charge_mo_yr, nvl(:description, 'OCR') description, :quantity_array[j], :utility_account_id,
			:bus_segment_id, :amount_array[j], :sub_account_id, 'OCR API ' || to_char(to_date(sysdate, 'YYYY-MM-DD')) notes, woc.department_id, :charge_id, 'OCR' journal_code,
			:i_cost_element_id, gla.external_account_code, :gl_account_id, :company_id, mn.month_number, :asset_location_id, 
			:serial_number
		from
			(select to_number(to_char(min(accounting_month), 'yyyymm')) month_number, min(accounting_month) charge_mo_yr 
			from wo_process_control where company_id = :company_id and powerplant_closed is null) mn,
				gl_account gla, work_order_control woc
		where gla.gl_account_id = :gl_account_id
		and woc.work_order_id = :work_order_id;
		if uf_check_sql_err('Insert into cwip_charge') then return -1
		if sqlca.sqlnrows <> 1 then 
			f_pp_msgs("Invalid number of rows on insert into unitized_work_order: " + string(sqlca.sqlnrows))
			rollback;
			return -1
		end if
		
		// *******************************************************************************
		//	CHARGE_GROUP_CONTROL
		// *******************************************************************************
		insert into charge_group_control (charge_group_id,work_order_id,alloc_id,unit_item_id,
			utility_account_id,bus_segment_id,sub_account_id,charge_type_id,expenditure_type_id,
			retirement_unit_id,property_group_id,description,group_indicator,allocation_priority,
			unitization_summary,amount, quantity,book_summary_name,journal_code	)
		values (:charge_group_id, :work_order_id, :alloc_id,:unit_item_id,
			:utility_account_id,:bus_segment_id,:sub_account_id,:i_charge_type_id, 2,
			:retirement_unit_id,:property_group_id,nvl(:description, 'OCR'), 0, 1,
			:i_bk_summary_name, :amount_array[j], :quantity_array[j], :i_bk_summary_name,'OCR');
		if uf_check_sql_err('Insert into charge_group_control') then return -1
		if sqlca.sqlnrows <> 1 then 
			f_pp_msgs("Invalid number of rows on insert into charge_group_control: " + string(sqlca.sqlnrows))
			rollback;
			return -1
		end if
		
		
		// *******************************************************************************
		//	WORK_ORDER_CHARGE_GROUP
		// *******************************************************************************
		insert into work_order_charge_group (work_order_id, charge_id, charge_group_id)
			values
			(:work_order_id, :charge_id, :charge_group_id);
		if uf_check_sql_err('Insert into work_order_charge_group') then return -1
		if sqlca.sqlnrows <> 1 then 
			f_pp_msgs("Invalid number of rows on insert into work_order_charge_group: " + string(sqlca.sqlnrows))
			rollback;
			return -1
		end if
		
		
		// *******************************************************************************
		//	WO_ESTIMATE
		// *******************************************************************************
		if not skip_estimate then
			insert into WO_ESTIMATE
				(estimate_id, revision, work_order_id, expenditure_type_id, utility_account_id, est_chg_type_id, sub_account_id, 
				retirement_unit_id, bus_segment_id, asset_id, quantity, hours, amount, notes, department_id,  
				property_group_id, asset_location_id, replacement_amount, serial_number, wo_est_trans_type_id, retire_vintage, ocr_api )
			select :estimate_id, we.revision, :work_order_id, 2, :utility_account_id, :i_est_chg_type_id, :sub_account_id, 
				:retirement_unit_id, :bus_segment_id, :asset_id_array[j], :quantity_array[j], 0, :amount_array[j], 'OCR API ' || to_char(to_date(sysdate, 'YYYY-MM-DD')), woc.department_id,  
				:property_group_id, :asset_location_id, null, :serial_number, 2, :vintage, 2
				from 
					(select nvl(max(revision),1) revision from wo_estimate where work_order_id = :work_order_id) we,
					work_order_control woc
					where woc.work_order_id = :work_order_id;				
			if uf_check_sql_err('Insert into wo_estimate') then return -1
			if sqlca.sqlnrows <> 1 then 
				f_pp_msgs("Invalid number of rows on insert into wo_estimate: " + string(sqlca.sqlnrows))
				rollback;
				return -1
			end if
		end if
		
		// *******************************************************************************
		//	WO_INTERFACE_OCR_RESULTS
		// *******************************************************************************
		if skip_estimate then
			// Don't populate wo_estimate_id.  This table only contains the records generated by this API.
			insert into WO_INTERFACE_OCR_RESULTS
				(row_id, unit_item_id, charge_group_id, charge_id)
				values
				(:row_id, :unit_item_id, :charge_group_id, :charge_id);
		else
			insert into WO_INTERFACE_OCR_RESULTS
				(row_id, unit_item_id, wo_estimate_id, charge_group_id, charge_id)
				values
				(:row_id, :unit_item_id, :estimate_id, :charge_group_id, :charge_id);
		end if
				
		if uf_check_sql_err('Insert into wo_interface_ocr_results') then return -1
		if sqlca.sqlnrows <> 1 then 
			f_pp_msgs("Invalid number of rows on insert into wo_interface_ocr_results: " + string(sqlca.sqlnrows))
			rollback;
			return -1
		end if
	next
	
	// Row successfully processed.  Mark it as done
	update wo_interface_ocr_stg
		set upload_indicator = 'D'
		where row_id = :row_id;
		if uf_check_sql_err('Update upload_indicator to D') then return -1
	
	// Safe to commit after each row is processed.
	commit;
next

return 1
end function

private function integer uf_process_ocr_translate (string a_batch);//--------------------------------------------------------------------------------
//
// If ASSET_ID is populated...
//
//--------------------------------------------------------------------------------
f_pp_msgs("Performing data translations")
f_pp_msgs("--- Updating records where asset_id is populated")
update wo_interface_ocr_stg
	set
		(company_id, ext_company, bus_segment_id, ext_bus_segment, utility_account_id, ext_utility_account, 
		sub_account_id, ext_sub_account, property_group_id, ext_property_group, retirement_unit_id, ext_retirement_unit,
		asset_location_id, ext_asset_location, gl_account_id, ext_gl_account, serial_number, vintage) =
		(select
		company_id, null, bus_segment_id, null, utility_account_id, null, 
		sub_account_id, null, property_group_id, null, retirement_unit_id, null,
		asset_location_id, null, gl_account_id, null, serial_number, to_char(eng_in_service_year,'yyyy')
		from cpr_ledger
		where cpr_ledger.asset_id = wo_interface_ocr_stg.asset_id)
	where asset_id is not null
	and batch_id = :a_batch
	and upload_indicator = 'I';
	
if uf_check_sql_err() then return -1

//--------------------------------------------------------------------------------
//
// COMPANY
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Company")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on gl_company_no
update wo_interface_ocr_stg a
set a.company_id = (
	select p.company_id
	from company p
	where p.gl_company_no = a.ext_company
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql_err() then return -1

// Map on company description
update wo_interface_ocr_stg a
set a.company_id = (
	select p.company_id
	from company p
	where p.description = a.ext_company
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql_err() then return -1

// Map on company_id
update wo_interface_ocr_stg a
set a.company_id = (
	select p.company_id
	from company p
	where to_char(p.company_id) = a.ext_company
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_company could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive company_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.company_id is null
and a.ext_company is not null
;
if uf_check_sql_err() then return -1

////
////	No external value was provided
////

	// Default company from work_order_control
	update wo_interface_ocr_stg a
	set a.company_id = (
		select p.company_id
		from work_order_control p
		where p.work_order_id = a.work_order_id
		)
	where a.upload_indicator = 'I'
	and a.batch_id = :a_batch
	and a.company_id is null
	and a.ext_company is null
	and a.work_order_id is not null
	;
	if uf_check_sql_err() then return -1
	


//--------------------------------------------------------------------------------
//
// WORK ORDER
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Work Order")

// Map on work_order_number
update wo_interface_ocr_stg a
set a.work_order_id = (
	select p.work_order_id
	from work_order_control p
	where p.work_order_number = a.work_order_number
	and p.company_id = a.company_id
	and p.funding_wo_indicator = 0
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.work_order_id is null
and a.work_order_number is not null
and a.company_id is not null;
if uf_check_sql_err() then return -1

// Map on description
////#####External Work Order Number
////#####Join Company ID & Funding_wo_indicator = 0
update wo_interface_ocr_stg a
set a.work_order_id = (
	select p.work_order_id
	from work_order_control p
	where p.description = a.work_order_number
	and p.company_id = a.company_id
	and p.funding_wo_indicator = 0
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.work_order_id is null
and a.company_id  is not null;
if uf_check_sql_err() then return -1


// Map on work_order_id
update wo_interface_ocr_stg a
set a.work_order_id = (
	select p.work_order_id
	from work_order_control p
	where to_char(p.work_order_id) = a.work_order_number
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.work_order_id is null
and a.work_order_number is not null;
if uf_check_sql_err() then return -1

// Flag records where work_order_id could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive work_order_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.work_order_id is null
and a.work_order_number is not null
;
if uf_check_sql_err() then return -1




//--------------------------------------------------------------------------------
//
// GL ACCOUNT
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping GL Account")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on ext_gl_account
update wo_interface_ocr_stg a
set a.gl_account_id = (
	select p.gl_account_id
	from gl_account p
	where p.external_account_code = a.ext_gl_account
	and p.account_type_id = 2
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.gl_account_id is null
and a.ext_gl_account is not null;
if uf_check_sql_err() then return -1

// Map on description
update wo_interface_ocr_stg a
set a.gl_account_id = (
	select p.gl_account_id
	from gl_account p
	where p.description = a.ext_gl_account
	and p.account_type_id = 2
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.gl_account_id is null
and a.ext_gl_account is not null
;
if uf_check_sql_err() then return -1

// Map on gl_account_id
update wo_interface_ocr_stg a
set a.gl_account_id = (
	select p.gl_account_id
	from gl_account p
	where to_char(p.gl_account_id) = a.ext_gl_account
	and p.account_type_id = 2
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.gl_account_id is null
and a.ext_gl_account is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_gl_account could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive gl_account_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.gl_account_id is null
and a.ext_gl_account is not null
;
if uf_check_sql_err() then return -1

////
////	No external value was provided
////

// Default gl_account_id from work_order_account
update wo_interface_ocr_stg a
set a.gl_account_id = (
	select p.unitized_gl_account
	from work_order_account p
	where p.work_order_id = a.work_order_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.gl_account_id is null
and a.ext_gl_account is null
and a.work_order_id is not null
;
if uf_check_sql_err() then return -1



//--------------------------------------------------------------------------------
//
// BUS SEGMENT
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Bus Segment")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on external_bus_segment
update wo_interface_ocr_stg a
set a.bus_segment_id = (
	select p.bus_segment_id
	from business_segment p
	where p.external_bus_segment = a.ext_bus_segment
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.bus_segment_id is null
and a.ext_bus_segment is not null
;
if uf_check_sql_err() then return -1

// Map on description
update wo_interface_ocr_stg a
set a.bus_segment_id = (
	select p.bus_segment_id
	from business_segment p
	where p.description = a.ext_bus_segment
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.bus_segment_id is null
and a.ext_bus_segment is not null
;
if uf_check_sql_err() then return -1

// Map on bus_segment_id
update wo_interface_ocr_stg a
set a.bus_segment_id = (
	select p.bus_segment_id
	from business_segment p
	where to_char(p.bus_segment_id) = a.ext_bus_segment
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.bus_segment_id is null
and a.ext_bus_segment is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_work_order_type could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive bus_segment_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.bus_segment_id is null
and a.ext_bus_segment is not null
;
if uf_check_sql_err() then return -1

////
////	No external value was provided
////

//	Map with utility_account_id
update wo_interface_ocr_stg a
set a.bus_segment_id = (
	select p.bus_segment_id
	from utility_account p
	where p.utility_account_id in (
		select utility_account_id
		from utility_account
		group by utility_account_id
		having count(*) = 1)
	and a.utility_account_id = p.utility_account_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.bus_segment_id is null
and a.ext_bus_segment is null
and a.utility_account_id is not null
;
if uf_check_sql_err() then return -1

///Check to see if there is only one record in company_bus_segent_control for this company if so update to that business segment
update wo_interface_ocr_stg a
set a.bus_segment_id = (
	select p.bus_segment_id
	from company_bus_segment_control p
	where p.bus_segment_id in (
		select c.bus_segment_id
		from company_bus_segment_control c
		group by bus_segment_id
		having count(*) = 1)
	and p.company_id = a.company_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.bus_segment_id is null
and a.ext_bus_segment is null
and a.company_id is not null
;
if uf_check_sql_err() then return -1



//--------------------------------------------------------------------------------
//
// UTILITY ACCOUNT
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Utility Account")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on external_account_code
update wo_interface_ocr_stg a
set a.utility_account_id = (
	select p.utility_account_id
	from utility_account p
	where p.external_account_code = a.ext_utility_account
	and p.bus_segment_id = a.bus_segment_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.utility_account_id is null
and a.ext_utility_account is not null
;
if uf_check_sql_err() then return -1

// Map on description
update wo_interface_ocr_stg a
set a.utility_account_id = (
	select p.utility_account_id
	from utility_account p
	where p.description = a.ext_utility_account
	and p.bus_segment_id = a.bus_segment_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.utility_account_id is null
and a.ext_utility_account is not null
;
if uf_check_sql_err() then return -1

// Map on utility_account_id
update wo_interface_ocr_stg a
set a.utility_account_id = (
	select p.utility_account_id
	from utility_account p
	where to_char(p.utility_account_id) = a.ext_utility_account
	and p.bus_segment_id = a.bus_segment_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.utility_account_id is null
and a.ext_utility_account is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_utility_account could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive utility_account_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.utility_account_id is null
and a.ext_utility_account is not null
;
if uf_check_sql_err() then return -1



//--------------------------------------------------------------------------------
//
// SUB ACCOUNT
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Sub Account")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on ext_sub_account
update wo_interface_ocr_stg a
set a.sub_account_id = (
	select p.sub_account_id
	from sub_account p
	where p.bus_segment_id = a.bus_segment_id
	and p.utility_account_id = a.utility_account_id
	and p.external_account_code = a.ext_sub_account
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.sub_account_id is null
and a.utility_account_id is not null
and a.ext_sub_account is not null
and a.bus_segment_id is not null
;
if uf_check_sql_err() then return -1

// Map on description
update wo_interface_ocr_stg a
set a.sub_account_id = (
	select p.sub_account_id
	from sub_account p
	where p.bus_segment_id = a.bus_segment_id
	and p.utility_account_id = a.utility_account_id
	and p.description = a.ext_sub_account
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.sub_account_id is null
and a.utility_account_id is not null
and a.ext_sub_account is not null
and a.bus_segment_id is not null
;
if uf_check_sql_err() then return -1

// Map on sub_account_id
update wo_interface_ocr_stg a
set a.sub_account_id = (
	select p.sub_account_id
	from sub_account p
	where p.bus_segment_id = a.bus_segment_id
	and p.utility_account_id = a.utility_account_id
	and to_char(p.sub_account_id) = a.ext_sub_account )
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.sub_account_id is null
and a.utility_account_id is not null
and a.ext_sub_account is not null
and a.bus_segment_id is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_sub_account could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive sub_account_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.sub_account_id is null
and a.utility_account_id is not null
and a.ext_sub_account is not null
;
if uf_check_sql_err() then return -1

////	BCM 20130109 - Additional logic for mapping sub_account_id 	
////
////	If no sub_account_id or ext._sub_account is passed, utility_account_id is 
////	passed, and there's only a single sub account for the utility account, map 
////	sub account based on the utility_account_id
////

//	Map with utility_account_id
update wo_interface_ocr_stg a
set a.sub_account_id = (
	select p.sub_account_id
	from sub_account p
	where (p.utility_account_id,p.bus_segment_id) in (
		select s.utility_account_id, s.bus_segment_id
		from sub_account s
		group by utility_account_id, bus_segment_id
		having count(*) = 1)
	and a.utility_account_id = p.utility_account_id
	and a.bus_segment_id = p.bus_segment_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.sub_account_id is null
and a.ext_sub_account is null
and a.utility_account_id is not null
and a.bus_segment_id is not null
;
if uf_check_sql_err() then return -1




//--------------------------------------------------------------------------------
//
// RETIREMENT UNIT
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Retirement Unit")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on external_retire_unit
update wo_interface_ocr_stg a
set a.retirement_unit_id = (
	select p.retirement_unit_id
	from retirement_unit p
	where p.external_retire_unit = a.ext_retirement_unit
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.retirement_unit_id is null
and a.ext_retirement_unit is not null
;
if uf_check_sql_err() then return -1

// Map on long_description
update wo_interface_ocr_stg a
set a.retirement_unit_id = (
	select p.retirement_unit_id
	from retirement_unit p
	where p.long_description = a.ext_retirement_unit
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.retirement_unit_id is null
and a.ext_retirement_unit is not null
;
if uf_check_sql_err() then return -1

// Map on retirement_unit_id
update wo_interface_ocr_stg a
set a.retirement_unit_id = (
	select p.retirement_unit_id
	from retirement_unit p
	where to_char(p.retirement_unit_id) = a.ext_retirement_unit
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.retirement_unit_id is null
and a.ext_retirement_unit is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_retirement_unit could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive retirement_unit_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.retirement_unit_id is null
and a.ext_retirement_unit is not null
;
if uf_check_sql_err() then return -1



//--------------------------------------------------------------------------------
//
// PROPERTY GROUP
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Property Group")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on external_property_group
update wo_interface_ocr_stg a
set a.property_group_id = (
	select p.property_group_id
	from property_group p
	where p.external_property_group = a.ext_property_group
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.property_group_id is null
and a.ext_property_group is not null
;
if uf_check_sql_err() then return -1

// Map on long_description
update wo_interface_ocr_stg a
set a.property_group_id = (
	select p.property_group_id
	from property_group p
	where p.description = a.ext_property_group
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.property_group_id is null
and a.ext_property_group is not null
;
if uf_check_sql_err() then return -1

// Map on property_group_id
update wo_interface_ocr_stg a
set a.property_group_id = (
	select p.property_group_id
	from property_group p
	where to_char(p.property_group_id) = a.ext_property_group
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.property_group_id is null
and a.ext_property_group is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_property_group could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive property_group_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.property_group_id is null
and a.ext_property_group is not null
;
if uf_check_sql_err() then return -1

////
////	No external value was provided
////

///fill in property group based on retirement unit picking min is fine this is what unitization would do anyway if it was left blank
update wo_interface_ocr_stg a
set property_group_id = (
	select min(property_group_id)
	from prop_group_prop_unit p, retirement_unit r
	where r.property_unit_id = p.property_unit_id
	and a.retirement_unit_id = r.retirement_unit_id)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.property_group_id is null
and a.ext_property_group is null
and a.retirement_unit_id is not null
;
if uf_check_sql_err() then return -1




//--------------------------------------------------------------------------------
//
// ASSET LOCATION
//
//--------------------------------------------------------------------------------
f_pp_msgs("--- Mapping Asset Location")

////
////	If an external value is provided, try to map on it and error out if it cannot be mapped
////

// Map on ext_asset_location
update wo_interface_ocr_stg a
set a.asset_location_id = (
	select p.asset_location_id
	from asset_location p
	where p.ext_asset_location = a.ext_asset_location
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.asset_location_id is null
and a.ext_asset_location is not null
;
if uf_check_sql_err() then return -1

// Map on long_description
update wo_interface_ocr_stg a
set a.asset_location_id = (
	select p.asset_location_id
	from asset_location p
	where p.long_description = a.ext_asset_location
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.asset_location_id is null
and a.ext_asset_location is not null
;
if uf_check_sql_err() then return -1

// Map on asset_location_id
update wo_interface_ocr_stg a
set a.asset_location_id = (
	select p.asset_location_id
	from asset_location p
	where to_char(p.asset_location_id) = a.ext_asset_location
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.asset_location_id is null
and a.ext_asset_location is not null
;
if uf_check_sql_err() then return -1

// Flag records where ext_asset_location could not be mapped
update wo_interface_ocr_stg a
set a.validation_message = a.validation_message || 'cannot derive asset_location_id; '
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.asset_location_id is null
and a.ext_asset_location is not null
;
if uf_check_sql_err() then return -1

////
////	No external value was provided
////

// Default asset location from work_order_control
update wo_interface_ocr_stg a
set a.asset_location_id = (
	select p.asset_location_id
	from work_order_control p
	where p.work_order_id = a.work_order_id
	)
where a.upload_indicator = 'I'
and a.batch_id = :a_batch
and a.asset_location_id is null
and a.ext_asset_location is null
and a.work_order_id is not null
;
if uf_check_sql_err() then return -1

return 0
end function

private function integer uf_process_ocr_archive (string a_batch);insert into  wo_interface_ocr_arc
	(company_id, ext_company, work_order_id, work_order_number, asset_id, bus_segment_id, ext_bus_segment, utility_account_id, ext_utility_account, 
	sub_account_id, ext_sub_account, property_group_id, ext_property_group, retirement_unit_id, ext_retirement_unit, asset_location_id, 
	ext_asset_location, gl_account_id, ext_gl_account, serial_number, vintage, quantity, amount, upload_indicator, validation_message, 
	batch_id, estimate_id, row_id, archive_time_stamp, description, long_description)
select company_id, ext_company, work_order_id, work_order_number, asset_id, bus_segment_id, ext_bus_segment, utility_account_id, ext_utility_account, 
	sub_account_id, ext_sub_account, property_group_id, ext_property_group, retirement_unit_id, ext_retirement_unit, asset_location_id, 
	ext_asset_location, gl_account_id, ext_gl_account, serial_number, vintage, quantity, amount, upload_indicator, validation_message, 
	batch_id, estimate_id, row_id, sysdate, description, long_description
from wo_interface_ocr_stg
where batch_id = :a_batch
and upload_indicator = 'D';

if uf_check_sql_err() then return -1;

delete from wo_interface_ocr_stg
where batch_id = :a_batch
and upload_indicator = 'D';

if uf_check_sql_err() then return -1;

return 0
end function

public function longlong uf_process_ocr_from_estimate (longlong a_work_order_id);longlong counter, pend_transaction
string batch

// Disable this logic until we can update the assumptions below.
//	Need to remove the assumptions about no manual create OCRs and the already posted OCRs.
f_pp_msgs("OCR creation from estimates is not enabled")
return -1

// *******************************************************************************
//		Create OCR records from estimates.
//		- Will be created for any OCR records in wo_estimate that don't have a record in the unitization
//		tables already.
//		- The max revision from wo_estimate will always be used to parallel unitization
//		- The logic will always be a "replacement" so any existing OCR records created via this API
//		will be deleted first and recreated.
//		- If any of the OCR records have already posted, the work order will produce an error
//		and create no new records.
//
//		- This function will not process a work order if OCR records exist in the unitization table that were
//		created outside of this API (since there is no clean link between wo_estimate and the unitization
//		tables the risk of duplicating the OCR data in this instance is too high)
//
//		Return Values: 
//			-1:		General Error during processing of staged data (deletes complete and records in OCR STG)
//			-2: 	General Error during insert to OCR STG
//			-11: 	CE/CT/ECT validation error
//			-12: 	Non-API OCR records exists
//			-13: 	OCR records posted
//			-14: 	OCR records in pending
//			-15: 	Error during delete
//			1: 		Success
// *******************************************************************************

batch = 'ESTtoOCR-' + string(a_work_order_id)
// *******************************************************************************
//	Proper charge type and cost elements need to be setup
// *******************************************************************************
if uf_lookup_ct() < 0 then return -11

// *******************************************************************************
//		- This function will not process a work order if OCR records exist in the unitization table that were
//		created outside of this API (since there is no clean link between wo_estimate and the unitization
//		tables the risk of duplicating the OCR data in this instance is too high)
// *******************************************************************************
select count(*)
	into :counter
	from charge_group_control cgc
	where cgc.charge_type_id = :i_charge_type_id
	and cgc.expenditure_type_id = 2
	and cgc.work_order_id = :a_work_order_id
	and not exists
		(select 1 from wo_interface_ocr_arc arc, wo_interface_ocr_results results
		where arc.row_id = results.row_id
		and arc.work_order_id = cgc.work_order_id
		and cgc.charge_group_id = results.charge_group_id);
if uf_check_sql_err("Checking for non-API Retirements") then return -2

if counter > 0 then return -12


// *******************************************************************************
//		- If any of the OCR records have already posted, the work order will produce an error
//		and create no new records.
// *******************************************************************************
select count(*), max(pend_transaction)
	into :counter, :pend_transaction
	from charge_group_control cgc
	where cgc.charge_type_id = :i_charge_type_id
	and cgc.expenditure_type_id = 2
	and cgc.work_order_id = :a_work_order_id
	and exists
		(select 1 from wo_interface_ocr_arc arc, wo_interface_ocr_results results
		where arc.row_id = results.row_id
		and arc.work_order_id = cgc.work_order_id
		and cgc.charge_group_id = results.charge_group_id);
if uf_check_sql_err("Checking for posted pend_transactions") then return -2
if pend_transaction = 1 then return -13
if pend_transaction = 100 then return -14

if counter > 0 then 
	// Need to delete the old records.  All deletes should match the record count above.
		
	// *******************************************************************************
	//	WO_ESTIMATE - do we need to delete these?  Thinking not for now...  Since the estimates
	//		are feeding the process, the API will not be generating estimates so the wo_estimate_id
	//		on the WO_INTERFACE_OCR_RESULTS will always be null
	// *******************************************************************************
		
	// *******************************************************************************
	//	WORK_ORDER_CHARGE_GROUP
	// *******************************************************************************
	delete from work_order_charge_group wocg
		where wocg.work_order_id = :a_work_order_id
		and exists
			(select 1 from wo_interface_ocr_arc arc, wo_interface_ocr_results results
			where arc.row_id = results.row_id
			and arc.work_order_id = wocg.work_order_id
			and wocg.charge_id = results.charge_id);
	if uf_check_sql_err("Delete from work_order_charge_group") then return uf_rollback(-2)
	if sqlca.sqlnrows <> counter then 
		f_pp_msgs("ERROR: Record count mismatch during delete from work_order_charge_group")
		return uf_rollback(-15)
	end if
	
	// *******************************************************************************
	//	CHARGE_GROUP_CONTROL
	// *******************************************************************************	
	delete from charge_group_control cgc
		where cgc.work_order_id = :a_work_order_id
		and exists
			(select 1 from wo_interface_ocr_arc arc, wo_interface_ocr_results results
			where arc.row_id = results.row_id
			and arc.work_order_id = cgc.work_order_id
			and cgc.charge_group_id = results.charge_group_id);
	if uf_check_sql_err("Delete from charge_group_control") then return uf_rollback(-2)
	if sqlca.sqlnrows <> counter then 
		f_pp_msgs("ERROR: Record count mismatch during delete from work_order_charge_group")
		return uf_rollback(-15)
	end if
	
	// *******************************************************************************
	//	CWIP_CHARGE
	// *******************************************************************************
	delete from cwip_charge cc
		where cc.work_order_id = :a_work_order_id
		and exists
			(select 1 from wo_interface_ocr_arc arc, wo_interface_ocr_results results
			where arc.row_id = results.row_id
			and arc.work_order_id = cc.work_order_id
			and cc.charge_id = results.charge_id);
	if uf_check_sql_err("Delete from cwip_charge") then return uf_rollback(-2)
	if sqlca.sqlnrows <> counter then 
		f_pp_msgs("ERROR: Record count mismatch during delete from cwip_charge")
		return uf_rollback(-15)
	end if
	
	// *******************************************************************************
	//	UNITIZED_WORK_ORDER
	// *******************************************************************************
	delete from unitized_work_order uwo
		where uwo.work_order_id = :a_work_order_id
		and exists
			(select 1 from wo_interface_ocr_arc arc, wo_interface_ocr_results results
			where arc.row_id = results.row_id
			and arc.work_order_id = uwo.work_order_id
			and uwo.unit_item_id = results.unit_item_id);
	if uf_check_sql_err("Delete from unitized_work_order") then return uf_rollback(-2)
	if sqlca.sqlnrows <> counter then 
		f_pp_msgs("ERROR: Record count mismatch during delete from unitized_work_order")
		return uf_rollback(-15)
	end if
	
	// *******************************************************************************
	//	WO_ESTIMATE
	// *******************************************************************************
	delete from wo_estimate woest
		where woest.work_order_id = :a_work_order_id
		and exists
			(select 1 from wo_interface_ocr_arc arc, wo_interface_ocr_results results
			where arc.row_id = results.row_id
			and arc.work_order_id = woest.work_order_id
			and woest.estimate_id = results.wo_estimate_id);
	if uf_check_sql_err("Delete from wo_estimate") then return uf_rollback(-2)
	if sqlca.sqlnrows <> counter then 
		f_pp_msgs("ERROR: Record count mismatch during delete from wo_estimate")
		return uf_rollback(-15)
	end if
	
	// *******************************************************************************
	//	WO_INTERFACE_OCR_RESULTS
	// *******************************************************************************
	delete from wo_interface_ocr_results results
		where exists
			(select 1 from wo_interface_ocr_arc arc
			where arc.row_id = results.row_id
			and arc.work_order_id = :a_work_order_id);
	if uf_check_sql_err("Delete from wo_interface_ocr_results") then return uf_rollback(-2)
	if sqlca.sqlnrows <> counter then 
		f_pp_msgs("ERROR: Record count mismatch during delete from wo_interface_ocr_results")
		return uf_rollback(-15)
	end if
	
	// *******************************************************************************
	//	WO_INTERFACE_OCR_ARC - mark the validation message = 'REPLACED' for clarity
	// *******************************************************************************
	update wo_interface_ocr_arc
		set validation_message = 'REPLACED - ' || to_char(sysdate,'yyyy-mm-dd')
		where work_order_id = :a_work_order_id
		and validation_message is null;
	if uf_check_sql_err("Updating wo_interface_ocr_arc") then return uf_rollback(-2)
	
	
	// *******************************************************************************
	//	WO_INTERFACE_OCR_STG - delete any staged records for the WO
	// *******************************************************************************
	delete from wo_interface_ocr_stg
		where work_order_id = :a_work_order_id
		and batch_id = :batch;
	if uf_check_sql_err("Delete from wo_interface_ocr_stg") then return uf_rollback(-2)
end if
	
		
	
f_pp_msgs("Loading original_cost_retirement_stg from wo_estimate")
insert into WO_INTERFACE_OCR_STG ( 
	company_id, work_order_id, bus_segment_id, utility_account_id, sub_account_id,  
		property_group_id, retirement_unit_id, asset_location_id,  gl_account_id, serial_number, vintage, quantity,  
		amount, upload_indicator, batch_id, estimate_id) 
select 
	woc.company_id, est.work_order_id, est.bus_segment_id, est.utility_account_id, est.sub_account_id,
		est.property_group_id, est.retirement_unit_id, nvl(est.asset_location_id, woc.asset_location_id), woa.unitized_gl_account,   
		est.serial_number, est.retire_vintage, est.quantity,  0, 'N', :batch, est.estimate_id  
from wo_estimate est, work_ordeR_account woa, work_order_control woc 
where est.work_order_id =woa.work_order_id 
and est.work_order_id =woc.work_order_id 
and est.work_order_id = :a_work_order_id
and est.expenditure_type_id = 2
and est.est_chg_type_id = :i_est_chg_type_id
and est.bus_segment_id is not null
and est.utility_account_id is not null
and est.retirement_unit_id is not null
and est.revision = (select max(revision) from wo_estimate est2 where est2.work_order_id = :a_work_order_id);
if uf_check_sql_err("Loading original_cost_retirement_stg from wo_estimate") then return uf_rollback(-2)

return uf_process_ocr(batch)
end function

private function longlong uf_lookup_ct ();longlong check_count

// If the lookup has already been done successfully, don't do it again.
if i_ct_lookup_complete then return 1
	
// *******************************************************************************
//	Proper charge type and cost elements are setup for retirements
// *******************************************************************************
//	verify / lookup charge type and book summary
check_count = 0
select count(*), min(charge_type_id), min(book_summary_id)
	into :check_count, :i_charge_type_id, :i_book_summary_id
	from charge_type where processing_type_id = 1;

if check_count = 1 then
	// Good
else
	// Unable to find a charge type.  Hard stop error for all records.
	f_pp_msgs("ERROR:  Unable to find original cost retirement CHARGE TYPE!")
	f_pp_msgs(" No retirements will be processed!")	
	return -1
end if

// book summary name
select trim(summary_name) into :i_bk_summary_name 
	from book_summary where book_summary_id = :i_book_summary_id;

if i_bk_summary_name = '' or isnull(i_bk_summary_name) then
	// Unable to find a book summary.  Hard stop error for all records.
	f_pp_msgs("ERROR:  Unable to find original cost retirement BOOK SUMMARY NAME!")
	f_pp_msgs(" No retirements will be processed!")
	return  -1
end if 

// Cost element
check_count = 0
select count(*), min(cost_element_id) 
	into :check_count, :i_cost_element_id
	from cost_element where charge_type_id = :i_charge_type_id;

if check_count = 1 then
	// Good
else
	// Unable to find a book summary.  Hard stop error for all records.
	f_pp_msgs("ERROR:  Unable to find original cost retirement COST ELEMENT!")
	f_pp_msgs(" No retirements will be processed!")
	return  -1
end if

// ECT
setnull(i_est_chg_type_id)
select min(est_chg_type_id) into :i_est_chg_type_id
	From estimate_charge_Type where processing_type_id = 1 and funding_charge_indicator <> 1;
	
if isnull(i_est_chg_type_id) then
	f_pp_msgs("ERROR:  Unable to find original cost retirement ESTIMATE CHARGE TYPE!")
	f_pp_msgs(" No retirements will be processed!")
	return  -1
end if

i_ct_lookup_complete = true

return 1
end function

public function longlong uf_rollback (longlong a_return);rollback;
return a_return 
end function

on uo_original_cost_retirement.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_original_cost_retirement.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

