HA$PBExportHeader$uo_budget_escalation.sru
$PBExportComments$7767-update budget_amounts after processing budget items
forward
global type uo_budget_escalation from nonvisualobject
end type
end forward

global type uo_budget_escalation from nonvisualobject
end type
global uo_budget_escalation uo_budget_escalation

type variables
boolean i_status_box = false
boolean i_progress_bar = true
boolean i_pp_msgs = false
boolean i_messagebox_onerror = false
boolean i_statusbox_onerror = true
boolean i_add_time_to_messages = true
boolean i_display_rowcount = true 
boolean i_display_msg_on_sql_success = true
longlong i_status_position = 0
longlong i_actuals_month
string i_table_name = 'NONE'
boolean i_return_results = true
longlong i_budget_version = 0
boolean 	deesc_called_from_esc = false
end variables

forward prototypes
public subroutine uf_msg (string a_msg, string a_error)
public function longlong uf_find_actuals_month (longlong a_funding_wo)
public function longlong uf_escalate (longlong a_funding_wo)
public function longlong uf_deescalate (longlong a_funding_wo)
public function longlong uf_escalate_bi (longlong a_year)
public function longlong uf_escalate_fp (longlong a_year)
public function longlong uf_roll_forward (longlong a_year, longlong a_funding_wo)
public function longlong uf_return_results (longlong a_funding_wo)
public function longlong uf_pre_escalation ()
public function longlong uf_post_escalation ()
end prototypes

public subroutine uf_msg (string a_msg, string a_error);
//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//
//   Function :  uo_budget_overheads.uf_msg() 
//
//	  Purpose  :	returns messages to the user based on boolean variables 
//		             
//
//   Arguments:	a_msg  : string message to return to the user
//					a_error : string that tells what type of message
//								E = Error Message
//								W = Warning Message
//								I = Information Message
//	
//   Returns :   none
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 
string message_title, time_string


if i_add_time_to_messages then
	time_string = string(now())
	a_msg = a_msg + ' '+time_string
end if

message_title = 'Budget Escalations'

if i_status_box then
	f_status_box(message_title,a_msg)
end if


if i_pp_msgs then
	f_pp_msgs(a_msg)
end if

if i_progress_bar  then
	f_progressbar(message_title,a_msg,100,i_status_position)
end if

if i_statusbox_onerror and a_error = 'E' then
	f_status_box(message_title,a_msg)
end if

if i_messagebox_onerror and a_error = 'E' then
	messagebox(message_title,a_msg)
end if


end subroutine

public function longlong uf_find_actuals_month (longlong a_funding_wo);
longlong start_month

longlong woa_act_month,  bv_act_month

setnull(woa_act_month)
setnull(bv_act_month)
///First lets get the actuals month if it wasn't passed in it will be null at this point if it wasn't passed in
///I'm intentionally not using the i_budget_Version variable because this is set when calculating AFUDC for an entire budget version
///When this is done an actuals month is always set. so none of this logic will fire
/*- Funding Projects
		- max work order approval actauls month number
		- if above is null then max actuals month number on any budget version the funding project is hooked to
		- if above is null then the min estimate start date - 1 month 
 - Budget Items
		- min actuals month number on any budget version that the funding project revisions are in
		- min Dec of year prior to start year on any budget versions that the funding project revisons are in
 - Work Orders
 		- max w
 		- min est_start_month - 1
*/

if a_funding_wo <> 2 then///For funding projects and work orders get the woa 
// ### CDC: Maint 5727: 10/26/2010
//	select max(nvl(woa.actuals_month_number,nvl(to_number(to_char(add_months(est_start_date,-1), 'yyyymm')),0)))
	select decode(max(nvl(woa.actuals_month_number,0)),0,min(nvl(to_number(to_char(add_months(est_start_date,-1), 'yyyymm')),0)),max(nvl(woa.actuals_month_number,0)))
// ### CDC: END Maint 5727: 10/26/2010
	into  :woa_act_month
	from work_order_approval woa, wo_est_processing_temp t
	where woa.work_order_id = t.work_order_id
	and woa.revision =t.revision
	;
	if sqlca.sqlcode < 0 then 
		uf_msg('Error Selecting actuals month from work_order_approval','E')
		uf_msg(sqlca.sqlerrtext,'E')
		return -1
	end if
end if

if i_budget_version <> 0 then
	select max(actuals_month)
	into :bv_act_month
	from budget_version bv
	where bv.budget_version_id = :i_budget_version
	 ;
	if sqlca.sqlcode < 0 then 
		uf_msg('Error Selecting actuals month from budget_version','E')
		uf_msg(sqlca.sqlerrtext,'E')
		return -1
	end if
else
	if a_funding_wo = 1  then	
		select max(actuals_month)
		into :bv_act_month
		from budget_version bv
		where exists (
			select 1
			from budget_version_fund_proj bvfp, wo_est_processing_temp w
			where bvfp.budget_version_id = bv.budget_version_id
				and bvfp.work_order_id = w.work_order_id
				and bvfp.revision = w.revision
				and bvfp.active = 1)
		 ;
		if sqlca.sqlcode < 0 then 
			uf_msg('Error Selecting actuals month from budget_version (temp)','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
	elseif a_funding_wo = 0 then
		bv_act_month = 0
	else 
		select max(actuals_month)
		into  :bv_act_month
		from budget_version bv, wo_est_processing_temp t
		where bv.budget_version_id = t.revision
		 ;
		 if sqlca.sqlcode < 0 then 
			uf_msg('Error Selecting actuals month from budget_version (temp)','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
	end if
end if




if isnull(woa_act_month) then woa_act_month = 0
if isnull(bv_act_month) then bv_act_month = 0

choose case a_funding_wo
	case 1/*Funding Projects*/
		///For actuals pick the greater of the budget version and work order approval actuals months
		if  woa_act_month > bv_act_month then
			i_actuals_month = woa_act_month
		else
			i_actuals_month = bv_act_month
		end if			
	case 0 /*Work Orders*/
		///For work orders the actuals month and start month should be the same
			i_actuals_month = woa_act_month
	case 2 /*Budget Items*/
		i_actuals_month = bv_act_month
end choose

if isnull(i_actuals_month) then i_actuals_month = 0
return i_actuals_month
		
	
		
		


end function

public function longlong uf_escalate (longlong a_funding_wo);

longlong rtn

if i_table_name = 'NONE' or i_table_name = 'WO_EST_MONTHLY' or i_table_name = 'BUDGET_MONTHLY_DATA' then
	//if someone sets the i_table_name variable it means we are escalating something other then wo_est_Monthly or budget_monthly data
	///This table should already be descalated amounts so no need to call uf_deescalate()
	///Step one bring costs back to base dollars
	i_status_position = 1
	deesc_called_from_esc = true
	rtn = uf_deescalate(a_funding_wo)
	deesc_called_from_esc = false
	if rtn < 0 then
		return -1
	end if
end if




choose case a_funding_wo
	case 2
		if i_table_name = 'NONE' then i_table_name = 'BUDGET_MONTHLY_DATA'
		rtn = uf_escalate_bi(0)	
	case else
		update work_order_approval woa
		set base_year = (select base_year from work_order_account woc where woc.work_order_id = woa.work_order_id)
		where woa.base_year is null
		and exists (
			select 1
			from wo_est_processing_temp w
			where w.work_order_id = woa.work_order_id
				and w.revision = woa.revision);		
		if i_table_name = 'NONE' then i_table_name = 'WO_EST_MONTHLY'
		rtn = uf_escalate_fp(0)	//work orders and funding projects are the same
end choose

if rtn < 0 then 
	rollback;
	return -1
else
	commit;
	return rtn
end if

return rtn
end function

public function longlong uf_deescalate (longlong a_funding_wo);string ret
longlong rtn
boolean des_esc_only
uo_budget_revision uo_revisions

if i_status_position = 0 then 
	des_esc_only = true
else
	des_esc_only = false
end if

i_actuals_month = uf_find_actuals_month(a_funding_wo)
if isnull(i_actuals_month) then i_actuals_month = 0

uf_msg('Checking for protected revisions','I')
ret = f_wo_est_processing_protected_revs(a_funding_wo)

if ret <> 'OK' then
	uf_msg('ERROR checking for protected revisions','E')
	uf_msg(ret,'E')
	return -1
end if
if des_esc_only then
	i_status_position = 20
else
	i_status_position = 5
end if

uf_msg('De-escalating amounts ','I')
choose case a_funding_wo
	case 2 ///Budget Item's
		update budget_monthly_data a
		set (january, february, march, april, may, june, july, august, september, october, november, december, total) = 
				(select nvl(a.january,0) - decode(sign(to_number(b.year||'01') - :i_actuals_month),1,nvl(b.january,0),0), 
				           nvl(a.february,0) - decode(sign(to_number(b.year||'02') - :i_actuals_month),1,nvl(b.february,0),0), 
						  nvl(a.march,0) - decode(sign(to_number(b.year||'03') - :i_actuals_month),1,nvl(b.march,0),0), 
						  nvl(a.april,0) - decode(sign(to_number(b.year||'04') - :i_actuals_month),1,nvl(b.april,0),0), 
						  nvl(a.may,0) - decode(sign(to_number(b.year||'05') - :i_actuals_month),1,nvl(b.may,0),0), 
						  nvl(a.june,0) - decode(sign(to_number(b.year||'06') - :i_actuals_month),1,nvl(b.june,0),0), 
						  nvl(a.july,0) - decode(sign(to_number(b.year||'07') - :i_actuals_month),1,nvl(b.july,0),0), 
						  nvl(a.august,0) - decode(sign(to_number(b.year||'08') - :i_actuals_month),1,nvl(b.august,0),0), 
						  nvl(a.september,0) - decode(sign(to_number(b.year||'09') - :i_actuals_month),1,nvl(b.september,0),0), 
						  nvl(a.october,0) - decode(sign(to_number(b.year||'10') - :i_actuals_month),1,nvl(b.october,0),0), 
						  nvl(a.november,0) - decode(sign(to_number(b.year||'11') - :i_actuals_month),1,nvl(b.november,0),0), 
						  nvl(a.december,0) - decode(sign(to_number(b.year||'12') - :i_actuals_month),1,nvl(b.december,0),0), 
						 
						 nvl(a.january,0) - decode(sign(to_number(b.year||'01') - :i_actuals_month),1,nvl(b.january,0),0)+
				           nvl(a.february,0) - decode(sign(to_number(b.year||'02') - :i_actuals_month),1,nvl(b.february,0),0)+ 
						  nvl(a.march,0) - decode(sign(to_number(b.year||'03') - :i_actuals_month),1,nvl(b.march,0),0)+
						  nvl(a.april,0) - decode(sign(to_number(b.year||'04') - :i_actuals_month),1,nvl(b.april,0),0)+
						  nvl(a.may,0) - decode(sign(to_number(b.year||'05') - :i_actuals_month),1,nvl(b.may,0),0)+
						  nvl(a.june,0) - decode(sign(to_number(b.year||'06') - :i_actuals_month),1,nvl(b.june,0),0)+
						  nvl(a.july,0) - decode(sign(to_number(b.year||'07') - :i_actuals_month),1,nvl(b.july,0),0)+
						  nvl(a.august,0) - decode(sign(to_number(b.year||'08') - :i_actuals_month),1,nvl(b.august,0),0)+
						  nvl(a.september,0) - decode(sign(to_number(b.year||'09') - :i_actuals_month),1,nvl(b.september,0),0)+
						  nvl(a.october,0) - decode(sign(to_number(b.year||'10') - :i_actuals_month),1,nvl(b.october,0),0)+
						  nvl(a.november,0) - decode(sign(to_number(b.year||'11') - :i_actuals_month),1,nvl(b.november,0),0)+
						  nvl(a.december,0) - decode(sign(to_number(b.year||'12') - :i_actuals_month),1,nvl(b.december,0),0)
				 from budget_monthly_data_escalation b
				 where a.budget_monthly_id = b.budget_monthly_id
					 and a.year = b.year)
		 where exists (
		    select 1 from budget_monthly_data_escalation d
			where d.budget_monthly_id = a.budget_monthly_id
				and a.year = d.year
			)
		 and exists (
		    select 1 from wo_est_processing_temp e
			    where a.budget_id = e.work_order_id
				and a.budget_version_id = e.revision
			)
			;
			if sqlca.sqlcode < 0 then 
				uf_msg('Error De-escalating Budget Monthly Data','E')
				uf_msg(sqlca.sqlerrtext,'E')
				return -1
			end if
			if des_esc_only then
				i_status_position = 70
			else
				i_status_position = 15
			end if
			
		delete from budget_monthly_data_escalation a
		where exists (
			select 1
			from wo_est_processing_temp b, budget_monthly_data c
			where b.work_order_id = c.budget_id
			   and b.revision = c.budget_version_id
			   and a.budget_monthly_id = c.budget_monthly_id
			   /*and a.year = c.year*/  // ### CDM - Maint 7184 - delete where est_monthly_id still exists in budget_monthly_data
			   )
		;
		if sqlca.sqlcode < 0 then
			
			uf_msg('Error Deleting previous escalations data','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
		if des_esc_only then
			i_status_position = 100
		else
			i_status_position = 20
		end if
	///DO I NEED TO TOUCH BUDGET AMOUNTS?
		
		
	case else///Funding Projects or Work Orders
		
		update wo_est_monthly a
		set (january, february, march, april, may, june, july, august, september, october, november, december, total) = 
				(select nvl(a.january,0) - decode(sign(to_number(b.year||'01') - :i_actuals_month),1,nvl(b.january,0),0), 
				           nvl(a.february,0) - decode(sign(to_number(b.year||'02') - :i_actuals_month),1,nvl(b.february,0),0), 
						  nvl(a.march,0) - decode(sign(to_number(b.year||'03') - :i_actuals_month),1,nvl(b.march,0),0), 
						  nvl(a.april,0) - decode(sign(to_number(b.year||'04') - :i_actuals_month),1,nvl(b.april,0),0), 
						  nvl(a.may,0) - decode(sign(to_number(b.year||'05') - :i_actuals_month),1,nvl(b.may,0),0), 
						  nvl(a.june,0) - decode(sign(to_number(b.year||'06') - :i_actuals_month),1,nvl(b.june,0),0), 
						  nvl(a.july,0) - decode(sign(to_number(b.year||'07') - :i_actuals_month),1,nvl(b.july,0),0), 
						  nvl(a.august,0) - decode(sign(to_number(b.year||'08') - :i_actuals_month),1,nvl(b.august,0),0), 
						  nvl(a.september,0) - decode(sign(to_number(b.year||'09') - :i_actuals_month),1,nvl(b.september,0),0), 
						  nvl(a.october,0) - decode(sign(to_number(b.year||'10') - :i_actuals_month),1,nvl(b.october,0),0), 
						  nvl(a.november,0) - decode(sign(to_number(b.year||'11') - :i_actuals_month),1,nvl(b.november,0),0), 
						  nvl(a.december,0) - decode(sign(to_number(b.year||'12') - :i_actuals_month),1,nvl(b.december,0),0), 
						 
						 nvl(a.january,0) - decode(sign(to_number(b.year||'01') - :i_actuals_month),1,nvl(b.january,0),0)+
				           nvl(a.february,0) - decode(sign(to_number(b.year||'02') - :i_actuals_month),1,nvl(b.february,0),0)+ 
						  nvl(a.march,0) - decode(sign(to_number(b.year||'03') - :i_actuals_month),1,nvl(b.march,0),0)+
						  nvl(a.april,0) - decode(sign(to_number(b.year||'04') - :i_actuals_month),1,nvl(b.april,0),0)+
						  nvl(a.may,0) - decode(sign(to_number(b.year||'05') - :i_actuals_month),1,nvl(b.may,0),0)+
						  nvl(a.june,0) - decode(sign(to_number(b.year||'06') - :i_actuals_month),1,nvl(b.june,0),0)+
						  nvl(a.july,0) - decode(sign(to_number(b.year||'07') - :i_actuals_month),1,nvl(b.july,0),0)+
						  nvl(a.august,0) - decode(sign(to_number(b.year||'08') - :i_actuals_month),1,nvl(b.august,0),0)+
						  nvl(a.september,0) - decode(sign(to_number(b.year||'09') - :i_actuals_month),1,nvl(b.september,0),0)+
						  nvl(a.october,0) - decode(sign(to_number(b.year||'10') - :i_actuals_month),1,nvl(b.october,0),0)+
						  nvl(a.november,0) - decode(sign(to_number(b.year||'11') - :i_actuals_month),1,nvl(b.november,0),0)+
						  nvl(a.december,0) - decode(sign(to_number(b.year||'12') - :i_actuals_month),1,nvl(b.december,0),0)
				 from wo_est_monthly_escalation b
				 where a.est_monthly_id = b.est_monthly_id
					 and b.year = a.year)
		 where exists (
		    select 1 from wo_est_monthly_escalation d
			where d.est_monthly_id = a.est_monthly_id
				and a.year = d.year
			)
		 and exists (
		    select 1 from wo_est_processing_temp e
			    where a.work_order_id = e.work_order_id
				and a.revision = e.revision
			)
			;
			if sqlca.sqlcode < 0 then 
				
				uf_msg('Error De-escalating Monthly Data','E')
				uf_msg(sqlca.sqlerrtext,'E')
				return -1
			end if
			
			if des_esc_only then
				i_status_position = 70
			else
				i_status_position = 15
			end if
			
		delete from wo_est_monthly_escalation a
		where exists (
			select 1
			from wo_est_processing_temp b, wo_est_monthly c
			where b.work_order_id = c.work_order_id
			   and a.est_monthly_id = c.est_monthly_id
			   and b.revision = c.revision
			   /*and a.year = c.year*/) // ### CDM - Maint 7184 - delete where est_monthly_id still exists in wo_est_monthly
		;
		if sqlca.sqlcode < 0 then
			
			uf_msg('Error Deleting previous FP escalations data','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
		if des_esc_only then
			i_status_position = 100
		else
			i_status_position = 20
		end if
				
				

end choose

if deesc_called_from_esc = false then
	// ### CDM - Maint 7767 - update budget_amounts after processing budget items
	if a_funding_wo = 2 then
		uf_msg('Updating Budget Amounts','I')
		uo_revisions = create uo_budget_revision
		
		uo_revisions.i_messagebox_onerror = i_messagebox_onerror
		uo_revisions.i_status_box = i_status_box
		uo_revisions.i_pp_msgs = i_pp_msgs
		uo_revisions.i_add_time_to_messages = i_add_time_to_messages
		uo_revisions.i_statusbox_onerror = i_statusbox_onerror
		uo_revisions.i_display_msg_on_sql_success = i_display_msg_on_sql_success
		uo_revisions.i_display_rowcount = i_display_rowcount
		uo_revisions.i_progress_bar = i_progress_bar
		
		rtn = uo_revisions.uf_update_budget_amounts()
		destroy uo_revisions
		
		if rtn = -1 then
			return -1
		end if
	end if
	
	if isvalid(w_progressbar) then
		close(w_progressbar)
	end if
end if

return 1
end function

public function longlong uf_escalate_bi (longlong a_year);string sqls, sqls2, ret, sql_esc1, sql_esc2, sql_esc3
longlong i, rtn
uo_ds_top ds_check
uo_ds_top ds_check2
ds_check = create uo_ds_top
ds_check2 = create uo_ds_top
uo_budget_revision uo_revisions

if isnull(a_year) then a_year = 0

///Verify that all base years exist in the escalation table
sqls2 = '  select woa.base_year, woa.escalation_id from budget_amounts woa '
sqls2 +=' where exists ( '
sqls2 +='	 select 1 from wo_est_processing_temp b '
sqls2 +='	 where woa.budget_id = b.work_order_id '
sqls2 +='  and woa.budget_version_id = b.revision '
sqls2 +='		) '
sqls2 +='	 and  woa.escalation_id is not null '
sqls2 +=' and woa.base_year <> -1 '
sqls2 +=' minus '
sqls2 +=' select ber.year, ber.escalation_id '
sqls2 +=' from budget_escalations_rates ber '


ret = f_create_dynamic_ds(ds_check2,'grid',sqls2,sqlca,true)
if ret <> 'OK' then 
	uf_msg('ERROR checking for base escalation years','E')
	return -1
end if

if ds_check2.rowcount() > 0 then
	uf_msg('The following base years are missing from the escalation table ','I')
	for i = 1 to ds_check2.rowcount()
		uf_msg(string(ds_check2.getitemnumber(i,1)),'I')
	next 
	uf_msg('The processing will continue but 1 will be assumed as the escalation factor for the missing years','I')
end if

destroy ds_check
destroy ds_check2

i_status_position = i_status_position + 10

uf_msg('Deleting previous escalation runs','I')
delete from wo_est_monthly_escalation_temp;
if sqlca.sqlcode < 0 then 
	uf_msg('Error Deleting from wo_est_monthly_escalation_temp','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Inserting escalated amounts into temp table- no estimate charge type','I')
sql_esc1 = ' insert into wo_est_monthly_escalation_temp (est_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december,  '
sql_esc1 += '	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id) '
sql_esc1 += '	select wem.budget_monthly_id, wem.year, '
sql_esc1 += "	decode(sign(to_number(wem.year||'01') - "+string(i_actuals_month)+"),1,(wem.january * ((nvl(ber.january,1) - nvl(ber_old.january,1))/decode(nvl(ber_old.january,1),0,1,nvl(ber_old.january,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'02') - "+string(i_actuals_month)+"),1,(wem.february * ((nvl(ber.february,1) - nvl(ber_old.february,1))/decode(nvl(ber_old.february,1),0,1,nvl(ber_old.february,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'03') - "+string(i_actuals_month)+"),1,(wem.march * ((nvl(ber.march,1) - nvl(ber_old.march,1))/decode(nvl(ber_old.march,1),0,1,nvl(ber_old.march,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'04') - "+string(i_actuals_month)+"),1,(wem.april * ((nvl(ber.april,1) - nvl(ber_old.april,1))/decode(nvl(ber_old.april,1),0,1,nvl(ber_old.april,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'05') - "+string(i_actuals_month)+"),1,(wem.may * ((nvl(ber.may,1) - nvl(ber_old.may,1))/decode(nvl(ber_old.may,1),0,1,nvl(ber_old.may,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'06') - "+string(i_actuals_month)+"),1,(wem.june * ((nvl(ber.june,1) - nvl(ber_old.june,1))/decode(nvl(ber_old.june,1),0,1,nvl(ber_old.june,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'07') - "+string(i_actuals_month)+"),1,(wem.july * ((nvl(ber.july,1) - nvl(ber_old.july,1))/decode(nvl(ber_old.july,1),0,1,nvl(ber_old.july,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'08') - "+string(i_actuals_month)+"),1,(wem.august * ((nvl(ber.august,1) - nvl(ber_old.august,1))/decode(nvl(ber_old.august,1),0,1,nvl(ber_old.august,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'09') - "+string(i_actuals_month)+"),1,(wem.september * ((nvl(ber.september,1) - nvl(ber_old.september,1))/decode(nvl(ber_old.september,1),0,1,nvl(ber_old.september,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'10') - "+string(i_actuals_month)+"),1,(wem.october * ((nvl(ber.october,1) - nvl(ber_old.october,1))/decode(nvl(ber_old.october,1),0,1,nvl(ber_old.october,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'11') - "+string(i_actuals_month)+"),1,(wem.november * ((nvl(ber.november,1) - nvl(ber_old.november,1))/decode(nvl(ber_old.november,1),0,1,nvl(ber_old.november,1)))),0), "
sql_esc1 += "	decode(sign(to_number(wem.year||'12') - "+string(i_actuals_month)+"),1,(wem.december * ((nvl(ber.december,1) - nvl(ber_old.december,1))/decode(nvl(ber_old.december,1),0,1,nvl(ber_old.december,1)))),0), "
sql_esc1 += "	ber.january, ber.february, ber.march, ber.april, ber.may, ber.june, ber.july, ber.august, ber.september, ber.october, ber.november, ber.december, "
sql_esc1 += "	nvl(woa.base_year,to_number(to_char(sysdate,'YYYY'))), ber.escalation_id "
sql_esc1 += "	from "+i_table_name+" wem, budget_escalations_rates ber_old, budget_amounts woa, wo_est_processing_temp w, budget_escalations_rates ber "
sql_esc1 += "	where wem.est_chg_type_id = nvl(ber_old.est_chg_type_id, wem.est_chg_type_id) "
sql_esc1 += "		and wem.est_chg_type_id = ber.est_chg_type_id "
sql_esc1 += "		and decode("+string(a_year)+",0,wem.year,"+string(a_year)+") = ber.year "
sql_esc1 += "		and nvl(woa.base_year,to_number(to_char(sysdate,'YYYY'))) = ber_old.year (+) "
sql_esc1 += "		and wem.budget_id = w.work_order_id "
sql_esc1 += "		and wem.budget_version_id = w.revision "
sql_esc1 += "		and ber.escalation_id = woa.escalation_id "
sql_esc1 += "		and nvl(ber_old.escalation_id,woa.escalation_id) = woa.escalation_id "
sql_esc1 += "		and w.work_order_id = woa.budget_id "
sql_esc1 += "		and w.revision = woa.budget_version_id "
sql_esc1 += "		and nvl(woa.base_year,to_number(to_char(sysdate,'YYYY'))) <> -1 "
execute immediate :sql_esc1;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Inserting Escalation Data FP (est charge type)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Inserting escalated amounts into temp table- estimate charge type','I')
sql_esc2 = ' insert into wo_est_monthly_escalation_temp (est_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december,  '
sql_esc2 += '	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id) '
sql_esc2 += '	select wem.budget_monthly_id, wem.year, '
sql_esc2 += "	decode(sign(to_number(wem.year||'01') - "+string(i_actuals_month)+"),1,(wem.january * ((nvl(ber.january,1) - nvl(ber_old.january,1))/decode(nvl(ber_old.january,1),0,1,nvl(ber_old.january,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'02') - "+string(i_actuals_month)+"),1,(wem.february * ((nvl(ber.february,1) - nvl(ber_old.february,1))/decode(nvl(ber_old.february,1),0,1,nvl(ber_old.february,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'03') - "+string(i_actuals_month)+"),1,(wem.march * ((nvl(ber.march,1) - nvl(ber_old.march,1))/decode(nvl(ber_old.march,1),0,1,nvl(ber_old.march,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'04') - "+string(i_actuals_month)+"),1,(wem.april * ((nvl(ber.april,1) - nvl(ber_old.april,1))/decode(nvl(ber_old.april,1),0,1,nvl(ber_old.april,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'05') - "+string(i_actuals_month)+"),1,(wem.may * ((nvl(ber.may,1) - nvl(ber_old.may,1))/decode(nvl(ber_old.may,1),0,1,nvl(ber_old.may,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'06') - "+string(i_actuals_month)+"),1,(wem.june * ((nvl(ber.june,1) - nvl(ber_old.june,1))/decode(nvl(ber_old.june,1),0,1,nvl(ber_old.june,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'07') - "+string(i_actuals_month)+"),1,(wem.july * ((nvl(ber.july,1) - nvl(ber_old.july,1))/decode(nvl(ber_old.july,1),0,1,nvl(ber_old.july,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'08') - "+string(i_actuals_month)+"),1,(wem.august * ((nvl(ber.august,1) - nvl(ber_old.august,1))/decode(nvl(ber_old.august,1),0,1,nvl(ber_old.august,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'09') - "+string(i_actuals_month)+"),1,(wem.september * ((nvl(ber.september,1) - nvl(ber_old.september,1))/decode(nvl(ber_old.september,1),0,1,nvl(ber_old.september,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'10') - "+string(i_actuals_month)+"),1,(wem.october * ((nvl(ber.october,1) - nvl(ber_old.october,1))/decode(nvl(ber_old.october,1),0,1,nvl(ber_old.october,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'11') - "+string(i_actuals_month)+"),1,(wem.november * ((nvl(ber.november,1) - nvl(ber_old.november,1))/decode(nvl(ber_old.november,1),0,1,nvl(ber_old.november,1)))),0), "
sql_esc2 += "	decode(sign(to_number(wem.year||'12') - "+string(i_actuals_month)+"),1,(wem.december * ((nvl(ber.december,1) - nvl(ber_old.december,1))/decode(nvl(ber_old.december,1),0,1,nvl(ber_old.december,1)))),0), "
sql_esc2 += "	ber.january, ber.february, ber.march, ber.april, ber.may, ber.june, ber.july, ber.august, ber.september, ber.october, ber.november, ber.december, "
sql_esc2 += "	nvl(woa.base_year,to_number(to_char(sysdate,'YYYY'))), ber.escalation_id "
sql_esc2 += "	from "+i_table_name+" wem, budget_escalations_rates ber_old, budget_amounts woa, wo_est_processing_temp w, budget_escalations_rates ber "
sql_esc2+=" where nvl(ber_old.est_chg_type_id,-1) = -1 "
sql_esc2+=" 	and ber.est_chg_type_id = -1 "
sql_esc2 += "		and decode("+string(a_year)+",0,wem.year,"+string(a_year)+") = ber.year "
sql_esc2 += "		and nvl(woa.base_year,to_number(to_char(sysdate,'YYYY'))) = ber_old.year (+) "
sql_esc2 += "		and wem.budget_id = w.work_order_id "
sql_esc2 += "		and wem.budget_version_id = w.revision "
sql_esc2 += "		and ber.escalation_id = woa.escalation_id "
sql_esc2 += "		and nvl(ber_old.escalation_id,woa.escalation_id) = woa.escalation_id "
sql_esc2 += "		and w.work_order_id = woa.budget_id "
sql_esc2 += "		and w.revision = woa.budget_version_id "
sql_esc2 += "		and nvl(woa.base_year,to_number(to_char(sysdate,'YYYY'))) <> -1 "
sql_esc2 += "	and not exists ( "
sql_esc2 += "		select 1 from budget_escalations_rates a "
sql_esc2 += "		where a.escalation_id = woa.escalation_id "
sql_esc2 += "		and a.est_chg_type_id = wem.est_chg_type_id "
sql_esc2 += "		and a.year = decode("+string(a_year)+",0,wem.year,"+string(a_year)+") "
sql_esc2 += "		) "
execute immediate :sql_esc2;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Inserting Escalation Data FP (est charge type = -1)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if

i_status_position = i_status_position + 10
uf_msg('Updating Temp Table - setting nulls to 0','I')
update wo_est_monthly_escalation_temp
set january = nvl(january, 0),
	february = nvl(february,0),
	march = nvl(march,0),
	april = nvl(april,0),
	may = nvl(may,0),
	june = nvl(june,0),
	july = nvl(july,0),
	august = nvl(august,0),
	september = nvl(september,0),
	october = nvl(october,0),
	november = nvl(november,0),
	december = nvl(december,0)
;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Updating nulls to zeros on temp table','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Updating Temp Table - updating totals','I')

update wo_est_monthly_escalation_temp
set total = january + february + march + april + may + june + july + august + september + october + november + december
;
if sqlca.sqlcode < 0 then 
	uf_msg('Error Updating total on temp table','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Updating monthly table - monthly amounts','I')

sql_esc3 = ' update '+i_table_name+' a '
sql_esc3 +=' set (january, february, march, april, may, june, july, august, september, october, november, december, total ) =  '
sql_esc3 +=' (select a.january + b.january, '
sql_esc3 +=' 		a.february + b.february, '
sql_esc3 +=' 		a.march + b.march, '
sql_esc3 +=' 		a.april + b.april, '
sql_esc3 +=' 		a.may + b.may, '
sql_esc3 +=' 		a.june + b.june, '
sql_esc3 +=' 		a.july + b.july, '
sql_esc3 +=' 		a.august + b.august, '
sql_esc3 +=' 		a.september + b.september,  '
sql_esc3 +=' 		a.october + b.october, '
sql_esc3 +=' 		a.november + b.november, '
sql_esc3 +=' 		a.december + b.december,	'
sql_esc3 +='  		a.january + b.january + a.february + b.february + a.march + b.march + a.april + b.april + a.may + b.may +a.june  + b.june + '
sql_esc3 +='            a.july + b.july + a.august + b.august + a.september + b.september + a.october + b.october + a.november + b.november + a.december + b.december '
sql_esc3 +=' from wo_est_monthly_escalation_temp b '
sql_esc3 +=' where a.budget_monthly_id = b.est_monthly_id  '
sql_esc3 +=' 	and a.year = b.year '
sql_esc3 +=' ) '
sql_esc3 +=' where exists ( '
sql_esc3 +=' 	select 1 from wo_est_monthly_escalation_temp b '
sql_esc3 +=' 	where a.budget_monthly_id = b.est_monthly_id and a.year = b.year) '
execute immediate :sql_esc3;
if sqlca.sqlcode < 0 then 
	uf_msg('Error Updating BUDGET MONTHLY DATA','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10 + 10
if i_table_name = 'BUDGET_MONTHLY_DATA' then
	update budget_monthly_data
	set jan_local = nvl(january,0),
		feb_local = nvl(february,0),
		mar_local = nvL(march,0),
		apr_local = nvl(april,0),
		may_local = nvl(may,0),
		jun_local = nvl(june,0),
		jul_local = nvl(july,0),
		aug_local = nvl(august,0),
		sep_local = nvl(september,0),
		oct_local = nvl(october,0),
		nov_local = nvL(november,0),
		dec_local = nvl(december,0),
		tot_local = nvl(total,0)
	where (budget_id, budget_version_id) in (
		select work_order_id, revision
		from wo_est_processing_temp
	)
	;

	if sqlca.sqlcode < 0 then
		uf_msg('Could not update local amounts. '+sqlca.sqlerrtext,'E')
		rollback;
		return -1
	end if	
	//	update budget_amounts a
	//	set budget_total_dollars = 
	//	(select sum(total) 
	//	from  budget_monthly_data b, wo_est_processing_temp w
	//	where a.budget_id = b.budget_id
	//	and a.budget_id = b.budget_version_id
	//	and a.budget_id = w.work_order_id
	//	and a.budget_version_id = w.revision
	//	)
	//	where (a.budget_id, a.budget_version_id) in  (
	//	   select w.work_order_id, w.revision
	//	   from wo_est_processing_temp w
	//		)
	//	;
	//	if sqlca.sqlcode < 0 then
	//		uf_msg('Could not update budget total dollars on budget amounts table. '+sqlca.sqlerrtext,'E')
	//		//rollback;
	//		//return -1
	//	end if
	//	
	//	update budget_amounts a
	//	set curr_year_budget_dollars = 
	//	nvl((select sum(decode(year,bv.start_year,total,0) )
	//	from  budget_monthly_data b, wo_est_processing_temp w, budget_version bv
	//	where a.budget_id = b.budget_id
	//	and a.budget_id = b.budget_version_id
	//	and a.budget_id = w.work_order_id
	//	and a.budget_version_id = w.revision
	//	and b.budget_version_id = bv.budget_version_id
	//	),0)
	//	where (a.budget_id, a.budget_version_id) in  (
	//	   select w.work_order_id, w.revision
	//	   from wo_est_processing_temp w
	//		)
	//	;
	//	if sqlca.sqlcode < 0 then
	//		uf_msg('Could not update curr_year_budget_dollars on budget amounts table. '+sqlca.sqlerrtext,'E')
	//		//rollback;
	//		//return -1
	//	end if
	
	// ### CDM - Maint 7767 - update budget_amounts after processing budget items
	uf_msg('Updating Budget Amounts','I')
	uo_revisions = create uo_budget_revision
	
	uo_revisions.i_messagebox_onerror = i_messagebox_onerror
	uo_revisions.i_status_box = i_status_box
	uo_revisions.i_pp_msgs = i_pp_msgs
	uo_revisions.i_add_time_to_messages = i_add_time_to_messages
	uo_revisions.i_statusbox_onerror = i_statusbox_onerror
	uo_revisions.i_display_msg_on_sql_success = i_display_msg_on_sql_success
	uo_revisions.i_display_rowcount = i_display_rowcount
	uo_revisions.i_progress_bar = i_progress_bar
	
	rtn = uo_revisions.uf_update_budget_amounts()
	destroy uo_revisions
	
	if rtn = -1 then
		return -1
	end if
	
end if
	
if a_year = 0 and i_return_results then
	rtn = uf_return_results(2)
	if rtn < 0 then
		return rtn
	end if
elseif not i_return_results then
	if isvalid(w_progressbar) then
		close(w_progressbar)
	end if
end if


return 1

end function

public function longlong uf_escalate_fp (longlong a_year);string sqls, sqls2, ret, sql_esc1, sql_esc2, sql_esc3
longlong i, rtn
uo_ds_top ds_check
uo_ds_top ds_check2
ds_check = create uo_ds_top
ds_check2 = create uo_ds_top
uf_msg('Checking for missing years','I')
///Verify that all years exist in the escalation table
//sqls = ' select a.year, woa.escalation_id from wo_est_monthly a, work_order_account woa '
//sqls +=' where exists ( '
//sqls +='	select 1 from wo_est_processing_temp  b '
//sqls +='	where a.work_order_id = b.work_order_id '
//sqls +='		and a.revision = b.revision '
//sqls +='		) '
//sqls +='	and a.work_order_id = woa.work_order_id '
//sqls +='minus '
//sqls +='select ber.year, ber.escalation_id '
//sqls +='from budget_escalations_rates ber '
//
//
//ret = f_create_dynamic_ds(ds_check,'grid',sqls,sqlca,true)
//if ret <> 'OK' then 
//	uf_msg('ERROR checking for escalation years','E')
//	uf_msg(ret,'E')
//	return -1
//end if
//
//if ds_check.rowcount() > 0 then
//	uf_msg('The following years are missing from the escalation table ','E')
//	for i = 1 to ds_check.rowcount()
//		uf_msg(string(ds_check.getitemnumber(i,1)),'E')
//	next 
//	uf_msg('The processing will continue but 1 will be assumed as the escalation factor for the missing years','E')
//end if

if isnull(a_year) then a_year = 0
///Verify that all base years exist in the escalation table
sqls2 = ' select woaa.base_year, woa.escalation_id from work_order_account woa, work_order_approval woaa '
sqls2 +=' where exists ( '
sqls2 +='	 select 1 from wo_est_processing_temp b '
sqls2 +='	 where woaa.work_order_id = b.work_order_id '
sqls2 +='   and woaa.revision = b.revision '
sqls2 +='		) '
sqls2 +=' and woa.escalation_id is not null '
sqls2 +=' and woa.work_order_id = woaa.work_order_id '
sqls2 +=' and woaa.base_year <> -1 '
sqls2 +=' minus '
sqls2 +=' select ber.year, ber.escalation_id '
sqls2 +=' from budget_escalations_rates ber '


ret = f_create_dynamic_ds(ds_check2,'grid',sqls2,sqlca,true)
if ret <> 'OK' then 
	uf_msg('ERROR checking for base escalation years','E')
	uf_msg(ret,'E')
	return -1
end if

if ds_check2.rowcount() > 0 then
	uf_msg('The following base years are missing from the escalation table ','I')
	for i = 1 to ds_check2.rowcount()
		uf_msg(string(ds_check2.getitemnumber(i,1)),'I')
	next 
	uf_msg('The processing will continue but 1 will be assumed as the escalation factor for the missing years','I')
end if

destroy ds_check
destroy ds_check2


i_status_position = i_status_position + 10
uf_msg('Deleting previous escalation runs','I')
delete from wo_est_monthly_escalation_temp;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Deleting from wo_est_monthly_escalation_temp','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Inserting escalated amounts into temp table- estimate charge type','I')
sql_esc1  =" insert into wo_est_monthly_escalation_temp (est_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december,  "
sql_esc1+=" 	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id) "
sql_esc1+=" select wem.est_monthly_id, wem.year,  "
sql_esc1+=" 	decode(sign(to_number(wem.year||'01') - "+string(i_actuals_month)+"),1,(wem.january * ((nvl(ber.january,1) - nvl(ber_old.january,1))/decode(nvl(ber_old.january,1),0,1,nvl(ber_old.january,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'02') - "+string(i_actuals_month)+"),1,(wem.february * ((nvl(ber.february,1) - nvl(ber_old.february,1))/decode(nvl(ber_old.february,1),0,1,nvl(ber_old.february,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'03') - "+string(i_actuals_month)+"),1,(wem.march * ((nvl(ber.march,1) - nvl(ber_old.march,1))/decode(nvl(ber_old.march,1),0,1,nvl(ber_old.march,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'04') - "+string(i_actuals_month)+"),1,(wem.april * ((nvl(ber.april,1) - nvl(ber_old.april,1))/decode(nvl(ber_old.april,1),0,1,nvl(ber_old.april,1 )))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'05') - "+string(i_actuals_month)+"),1,(wem.may * ((nvl(ber.may,1) - nvl(ber_old.may,1))/decode(nvl(ber_old.may,1),0,1,nvl(ber_old.may,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'06') - "+string(i_actuals_month)+"),1,(wem.june * ((nvl(ber.june,1) - nvl(ber_old.june,1))/decode(nvl(ber_old.june,1),0,1,nvl(ber_old.june,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'07') - "+string(i_actuals_month)+"),1,(wem.july * ((nvl(ber.july,1) - nvl(ber_old.july,1))/decode(nvl(ber_old.july,1),0,1,nvl(ber_old.july,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'08') - "+string(i_actuals_month)+"),1,(wem.august * ((nvl(ber.august,1) - nvl(ber_old.august,1))/decode(nvl(ber_old.august,1),0,1,nvl(ber_old.august,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'09') - "+string(i_actuals_month)+"),1,(wem.september * ((nvl(ber.september,1) - nvl(ber_old.september,1))/decode(nvl(ber_old.september,1),0,1,nvl(ber_old.september,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'10') - "+string(i_actuals_month)+"),1,(wem.october * ((nvl(ber.october,1) - nvl(ber_old.october,1))/decode(nvl(ber_old.october,1),0,1,nvl(ber_old.october,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'11') - "+string(i_actuals_month)+"),1,(wem.november * ((nvl(ber.november,1) - nvl(ber_old.november,1))/decode(nvl(ber_old.november,1),0,1,nvl(ber_old.november,1)))),0), "
sql_esc1+=" 	decode(sign(to_number(wem.year||'12') - "+string(i_actuals_month)+"),1,(wem.december * ((nvl(ber.december,1) - nvl(ber_old.december,1))/decode(nvl(ber_old.december,1),0,1,nvl(ber_old.december,1)))),0), "
sql_esc1+=" ber.january, ber.february, ber.march, ber.april, ber.may, ber.june, ber.july, ber.august, ber.september, ber.october, ber.november, ber.december,  "
sql_esc1+=" nvl(woaa.base_year,to_number(to_char(sysdate,'YYYY'))), ber.escalation_id "
sql_esc1+=" from "+i_table_name+"  wem, budget_escalations_rates ber_old, work_order_account woa, wo_est_processing_temp w, budget_escalations_rates ber, work_order_approval woaa "
sql_esc1+=" where wem.est_chg_type_id = nvl(ber_old.est_chg_type_id, wem.est_chg_type_id) "
sql_esc1+=" 	and wem.est_chg_type_id = ber.est_chg_type_id "
sql_esc1+=" 	and decode("+string(a_year)+",0,wem.year,"+string(a_year)+") = ber.year "
sql_esc1+=" 	and nvl(woaa.base_year,to_number(to_char(sysdate,'YYYY'))) = ber_old.year (+) "
sql_esc1+=" 	and wem.work_order_id = w.work_order_id "
sql_esc1+=" 	and wem.revision = w.revision "
sql_esc1+=" 	and ber.escalation_id = woa.escalation_id "
sql_esc1+=" 	and nvl(ber_old.escalation_id,woa.escalation_id) = woa.escalation_id "
sql_esc1+=" 	and w.work_order_id = woa.work_order_id "
sql_esc1+=" 	and nvl(woaa.base_year,to_number(to_char(sysdate,'YYYY'))) <> -1 "
sql_esc1+="     and woaa.work_order_id = w.work_order_id "
sql_esc1+="     and woaa.revision = w.revision "
execute immediate :sql_esc1;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Inserting Escalation Data FP (est charge type)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Inserting escalated amounts into temp table- no estimate charge type','I')
sql_esc2 = " insert into wo_est_monthly_escalation_temp (est_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december,  "
sql_esc2+=" 	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id) "
sql_esc2+=" select wem.est_monthly_id, wem.year,  "
sql_esc2+=" 	decode(sign(to_number(wem.year||'01') - "+string(i_actuals_month)+"),1,(wem.january * ((nvl(ber.january,1) - nvl(ber_old.january,1))/decode(nvl(ber_old.january,1),0,1,nvl(ber_old.january,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'02') - "+string(i_actuals_month)+"),1,(wem.february * ((nvl(ber.february,1) - nvl(ber_old.february,1))/decode(nvl(ber_old.february,1),0,1,nvl(ber_old.february,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'03') - "+string(i_actuals_month)+"),1,(wem.march * ((nvl(ber.march,1) - nvl(ber_old.march,1))/decode(nvl(ber_old.march,1),0,1,nvl(ber_old.march,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'04') - "+string(i_actuals_month)+"),1,(wem.april * ((nvl(ber.april,1) - nvl(ber_old.april,1))/decode(nvl(ber_old.april,1),0,1,nvl(ber_old.april,1 )))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'05') - "+string(i_actuals_month)+"),1,(wem.may * ((nvl(ber.may,1) - nvl(ber_old.may,1))/decode(nvl(ber_old.may,1),0,1,nvl(ber_old.may,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'06') - "+string(i_actuals_month)+"),1,(wem.june * ((nvl(ber.june,1) - nvl(ber_old.june,1))/decode(nvl(ber_old.june,1),0,1,nvl(ber_old.june,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'07') - "+string(i_actuals_month)+"),1,(wem.july * ((nvl(ber.july,1) - nvl(ber_old.july,1))/decode(nvl(ber_old.july,1),0,1,nvl(ber_old.july,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'08') - "+string(i_actuals_month)+"),1,(wem.august * ((nvl(ber.august,1) - nvl(ber_old.august,1))/decode(nvl(ber_old.august,1),0,1,nvl(ber_old.august,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'09') - "+string(i_actuals_month)+"),1,(wem.september * ((nvl(ber.september,1) - nvl(ber_old.september,1))/decode(nvl(ber_old.september,1),0,1,nvl(ber_old.september,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'10') - "+string(i_actuals_month)+"),1,(wem.october * ((nvl(ber.october,1) - nvl(ber_old.october,1))/decode(nvl(ber_old.october,1),0,1,nvl(ber_old.october,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'11') - "+string(i_actuals_month)+"),1,(wem.november * ((nvl(ber.november,1) - nvl(ber_old.november,1))/decode(nvl(ber_old.november,1),0,1,nvl(ber_old.november,1)))),0), "
sql_esc2+=" 	decode(sign(to_number(wem.year||'12') - "+string(i_actuals_month)+"),1,(wem.december * ((nvl(ber.december,1) - nvl(ber_old.december,1))/decode(nvl(ber_old.december,1),0,1,nvl(ber_old.december,1)))),0), "
sql_esc2+=" ber.january, ber.february, ber.march, ber.april, ber.may, ber.june, ber.july, ber.august, ber.september, ber.october, ber.november, ber.december,  "
sql_esc2+=" nvl(woaa.base_year,to_number(to_char(sysdate,'YYYY'))), ber.escalation_id "
sql_esc2+=" from "+i_table_name+"  wem, budget_escalations_rates ber_old, work_order_account woa, wo_est_processing_temp w, budget_escalations_rates ber, work_order_approval woaa "
sql_esc2+=" where nvl(ber_old.est_chg_type_id,-1) = -1 "
sql_esc2+=" 	and ber.est_chg_type_id = -1 "
sql_esc2+=" 	and decode("+string(a_year)+",0,wem.year,"+string(a_year)+") = ber.year "
sql_esc2+=" 	and nvl(woaa.base_year,to_number(to_char(sysdate,'YYYY'))) = ber_old.year (+) "
sql_esc2+=" 	and wem.work_order_id = w.work_order_id "
sql_esc2+=" 	and wem.revision = w.revision "
sql_esc2+=" 	and ber.escalation_id = woa.escalation_id "
sql_esc2+=" 	and nvl(ber_old.escalation_id,woa.escalation_id) = woa.escalation_id "
sql_esc2+=" 	and w.work_order_id = woa.work_order_id "
sql_esc2+=" 	and nvl(woaa.base_year,to_number(to_char(sysdate,'YYYY'))) <> -1 "
sql_esc2+="     and woaa.work_order_id = w.work_order_id "
sql_esc2+="    and woaa.revision = w.revision " //CDM "revision" was misspelled
sql_esc2+=" and not exists ( "
sql_esc2+=" 	select 1 from budget_escalations_rates a "
sql_esc2+=" 	where a.escalation_id = woa.escalation_id "
sql_esc2+=" 	and a.est_chg_type_id = wem.est_chg_type_id "
sql_esc2+=" 	and a.year = decode("+string(a_year)+",0,wem.year,"+string(a_year)+") "
sql_esc2+=" 	) "
execute immediate :sql_esc2;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Inserting Escalation Data FP (est charge type = -1)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Updating Temp Table - setting nulls to 0','I')
update wo_est_monthly_escalation_temp
set january = nvl(january, 0),
	february = nvl(february,0),
	march = nvl(march,0),
	april = nvl(april,0),
	may = nvl(may,0),
	june = nvl(june,0),
	july = nvl(july,0),
	august = nvl(august,0),
	september = nvl(september,0),
	october = nvl(october,0),
	november = nvl(november,0),
	december = nvl(december,0)
;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Updating nulls to zeros on temp table','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Updating Temp Table - totals','I')
update wo_est_monthly_escalation_temp
set total = january + february + march + april + may + june + july + august + september + october + november + december
;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Updating total on temp table','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if
i_status_position = i_status_position + 10
uf_msg('Updating monthly table - monthly amounts','I')
sql_esc3 = ' update '+i_table_name+' a '
sql_esc3 +=' set (january, february, march, april, may, june, july, august, september, october, november, december, total ) =  '
sql_esc3 +=' (select a.january + b.january, '
sql_esc3 +=' 		a.february + b.february, '
sql_esc3 +=' 		a.march + b.march, '
sql_esc3 +=' 		a.april + b.april, '
sql_esc3 +=' 		a.may + b.may, '
sql_esc3 +=' 		a.june + b.june, '
sql_esc3 +=' 		a.july + b.july, '
sql_esc3 +=' 		a.august + b.august, '
sql_esc3 +=' 		a.september + b.september,  '
sql_esc3 +=' 		a.october + b.october, '
sql_esc3 +=' 		a.november + b.november, '
sql_esc3 +=' 		a.december + b.december,	'
sql_esc3 +='  		a.january + b.january + a.february + b.february + a.march + b.march + a.april + b.april + a.may + b.may +a.june  + b.june + '
sql_esc3 +='            a.july + b.july + a.august + b.august + a.september + b.september + a.october + b.october + a.november + b.november + a.december + b.december '
sql_esc3 +=' from wo_est_monthly_escalation_temp b '
sql_esc3 +=' where a.est_monthly_id = b.est_monthly_id  '
sql_esc3 +=' 	and a.year = b.year '
sql_esc3 +=' ) '
sql_esc3 +=' where exists ( '
sql_esc3 +=' 	select 1 from wo_est_monthly_escalation_temp b '
sql_esc3 +=' 	where a.est_monthly_id = b.est_monthly_id and a.year = b.year) '
execute immediate :sql_esc3;
if sqlca.sqlcode < 0 then 
	
	uf_msg('Error Updating WO EST MONTHLY','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if

if a_year = 0 and i_return_results then
	rtn = uf_return_results(1)
	if rtn < 0 then
		return rtn
	end if
elseif not i_return_results then
	if isvalid(w_progressbar) then
		close(w_progressbar)
	end if
end if

return 1






end function

public function longlong uf_roll_forward (longlong a_year, longlong a_funding_wo);



longlong rtn
string cv

///Step one bring costs back to base dollars
i_status_position = 1
// ### CDM - Maint 7767 - need this variable here as well
deesc_called_from_esc = true
rtn = uf_deescalate(a_funding_wo)
deesc_called_from_esc = false
if rtn < 0 then
	return -1
end if




choose case a_funding_wo
	case 2
		rtn = uf_escalate_bi(a_year)	
	case else
		rtn = uf_escalate_fp(a_year)	//work orders and funding projects are the same
end choose

if rtn < 0 then 
	rollback;
	return -1
else
	if a_funding_wo <> 2 then
		update work_order_approval a
		set base_year = :a_year
		where exists (select 1 from wo_est_processing_temp b where a.work_order_id = b.work_order_id and a.revision = b.revision)
		;
		if sqlca.sqlcode < 0 then 
			uf_msg('Error Updating Base Year on work order approval ','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
//		if a_funding_wo = 1 then	
//			cv = lower(f_pp_system_control('WOEST - FP DATE CHANGE'))
//			if isnull(cv) then cv = 'never update'
//		else 	
//			cv = lower(f_pp_system_control('WOEST - WO DATE CHANGE'))
//			if isnull(cv) then cv = 'never update'
//		end if			
//		if cv = 'always update' then
			update work_order_account woa
			set		base_year = :a_year
			where exists (select 1 from wo_est_processing_temp w where w.work_order_id = woa.work_order_id )
			;
			if sqlca.sqlcode < 0 then 
				uf_msg('Error Updating Base Year on work order account ','E')
				uf_msg(sqlca.sqlerrtext,'E')
				return -1
			end if
//		end if
	else
		update budget_amounts a
		set base_year = :a_year
		where exists (select 1 from wo_est_processing_temp b where a.budget_id = b.work_order_id and a.budget_version_id = b.revision)
		;
		if sqlca.sqlcode < 0 then 
			uf_msg('Error Updating Base Year ','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if		
	end if
	commit;
	return rtn
end if

return rtn
return 1
end function

public function longlong uf_return_results (longlong a_funding_wo);


	uf_msg('Saving off escalation results','I')
	
	if a_funding_wo = 2 then 
		insert into budget_monthly_data_escalation(budget_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december, 
			jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id, total)
		select est_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december, 
			jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id, total 
		from wo_est_monthly_escalation_temp
		;
		if sqlca.sqlcode < 0 then 
			uf_msg('Error Inserting into escalation monthly table','E')
			uf_msg(sqlca.sqlerrtext,'E')
			rollback;
			return -1
		end if
	else
		insert into wo_est_monthly_escalation(est_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december, 
			jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id, total)
		select est_monthly_id, year, january, february, march, april, may, june, july, august, september, october, november, december, 
			jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate, jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate, base_year, rate_type_id, total 
		from wo_est_monthly_escalation_temp
		;
		if sqlca.sqlcode < 0 then 
			rollback;
			uf_msg('Error Inserting into escalation monthly table','E')
			uf_msg(sqlca.sqlerrtext,'E')
			return -1
		end if
	end if
	
	
if isvalid(w_progressbar) then
	close(w_progressbar)
end if


return 1
end function

public function longlong uf_pre_escalation ();//
// If exclude_results = 1 then add "would-be" escalations before escalating
//
i_status_position = i_status_position + 10
uf_msg('Setting base year for exclude_results','I')
update work_order_approval woaa
set base_year = (
	select to_number(to_char(woa.est_start_date,'yyyy'))
	from work_order_approval woa, wo_est_processing_temp t
	where woa.work_order_id = t.work_order_id
	and woa.revision = t.revision
	and woa.work_order_id = woaa.work_order_id
	and woa.revision = woaa.revision
	)
where exists (
	select 1 from wo_est_processing_temp t, budget_escalations be, work_order_account acct
	where t.work_order_id = acct.work_order_id
	and acct.escalation_id = be.escalation_id
	and nvl(be.exclude_results,0) = 1
	and woaa.work_order_id = t.work_order_id
	and woaa.revision = t.revision
	);

if sqlca.sqlcode < 0 then 
	uf_msg('Error Updating base year (exclude_results = 1)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if


i_status_position = i_status_position + 10
uf_msg('Inserting would-be escalations for exclude_results','I')
insert into wo_est_monthly_escalation (
	est_monthly_id, year,
	january, february, march, april, may, june,
	july, august, september, october, november, december, total,
	jan_rate, feb_rate, mar_rate, apr_rate, may_rate, jun_rate,
	jul_rate, aug_rate, sep_rate, oct_rate, nov_rate, dec_rate,
	base_year, rate_type_id )
select wem.est_monthly_id,
	wem.year,
	round(wem.january - (wem.january / (1 + ((ber.january - ber_base.january)/ber_base.january))),2) january,
	round(wem.february - (wem.february / (1 + ((ber.february - ber_base.february)/ber_base.february))),2) february,
	round(wem.march - (wem.march / (1 + ((ber.march - ber_base.march)/ber_base.march))),2) march,
	round(wem.april - (wem.april / (1 + ((ber.april - ber_base.april)/ber_base.april))),2) april,
	round(wem.may - (wem.may / (1 + ((ber.may - ber_base.may)/ber_base.may))),2) may,
	round(wem.june - (wem.june / (1 + ((ber.june - ber_base.june)/ber_base.june))),2) june,
	round(wem.july - (wem.july / (1 + ((ber.july - ber_base.july)/ber_base.july))),2) july,
	round(wem.august - (wem.august / (1 + ((ber.august - ber_base.august)/ber_base.august))),2) august,
	round(wem.september - (wem.september / (1 + ((ber.september - ber_base.september)/ber_base.september))),2) september,
	round(wem.october - (wem.october / (1 + ((ber.october - ber_base.october)/ber_base.october))),2) october,
	round(wem.november - (wem.november / (1 + ((ber.november - ber_base.november)/ber_base.november))),2) november,
	round(wem.december - (wem.december / (1 + ((ber.december - ber_base.december)/ber_base.december))),2) december,
	round(wem.january - (wem.january / (1 + ((ber.january - ber_base.january)/ber_base.january))),2) +
		round(wem.february - (wem.february / (1 + ((ber.february - ber_base.february)/ber_base.february))),2) +
		round(wem.march - (wem.march / (1 + ((ber.march - ber_base.march)/ber_base.march))),2) +
		round(wem.april - (wem.april / (1 + ((ber.april - ber_base.april)/ber_base.april))),2) +
		round(wem.may - (wem.may / (1 + ((ber.may - ber_base.may)/ber_base.may))),2) +
		round(wem.june - (wem.june / (1 + ((ber.june - ber_base.june)/ber_base.june))),2) +
		round(wem.july - (wem.july / (1 + ((ber.july - ber_base.july)/ber_base.july))),2) +
		round(wem.august - (wem.august / (1 + ((ber.august - ber_base.august)/ber_base.august))),2) +
		round(wem.september - (wem.september / (1 + ((ber.september - ber_base.september)/ber_base.september))),2) +
		round(wem.october - (wem.october / (1 + ((ber.october - ber_base.october)/ber_base.october))),2)  +
		round(wem.november - (wem.november / (1 + ((ber.november - ber_base.november)/ber_base.november))),2) +
		round(wem.december - (wem.december / (1 + ((ber.december - ber_base.december)/ber_base.december))),2) total,
	ber.january jan_rate,
	ber.february feb_rate,
	ber.march mar_rate,
	ber.april apr_rate,
	ber.may may_rate,
	ber.june jun_rate,
	ber.july jul_rate,
	ber.august aug_rate,
	ber.september sep_rate,
	ber.october oct_rate,
	ber.november nov_rate,
	ber.december dec_rate,
	nvl(woaa.base_year,to_number(to_char(sysdate,'yyyy'))) base_year,
	acct.escalation_id rate_type_id
from wo_est_monthly wem, work_order_account acct,
	budget_escalations_rates ber_base, budget_escalations_rates ber,
	wo_est_processing_temp t, budget_escalations be, work_order_approval woaa
where wem.work_order_id = t.work_order_id
and wem.revision = t.revision
and t.work_order_id = acct.work_order_id
and acct.escalation_id = nvl(ber_base.escalation_id,acct.escalation_id)
and acct.escalation_id = ber.escalation_id
and ber.escalation_id = be.escalation_id
and nvl(be.exclude_results,0) = 1
and ber_base.year (+) = nvl(woaa.base_year,to_number(to_char(sysdate,'yyyy')))
and ber.year = wem.year
and ber.est_chg_type_id = wem.est_chg_type_id
and nvl(ber_base.est_chg_type_id,wem.est_chg_type_id) = wem.est_chg_type_id
and t.work_order_id = woaa.work_order_id
and t.revision = woaa.revision
union all
select wem.est_monthly_id,
	wem.year,
	round(wem.january - (wem.january / (1 + ((ber.january - ber_base.january)/ber_base.january))),2) january,
	round(wem.february - (wem.february / (1 + ((ber.february - ber_base.february)/ber_base.february))),2) february,
	round(wem.march - (wem.march / (1 + ((ber.march - ber_base.march)/ber_base.march))),2) march,
	round(wem.april - (wem.april / (1 + ((ber.april - ber_base.april)/ber_base.april))),2) april,
	round(wem.may - (wem.may / (1 + ((ber.may - ber_base.may)/ber_base.may))),2) may,
	round(wem.june - (wem.june / (1 + ((ber.june - ber_base.june)/ber_base.june))),2) june,
	round(wem.july - (wem.july / (1 + ((ber.july - ber_base.july)/ber_base.july))),2) july,
	round(wem.august - (wem.august / (1 + ((ber.august - ber_base.august)/ber_base.august))),2) august,
	round(wem.september - (wem.september / (1 + ((ber.september - ber_base.september)/ber_base.september))),2) september,
	round(wem.october - (wem.october / (1 + ((ber.october - ber_base.october)/ber_base.october))),2) october,
	round(wem.november - (wem.november / (1 + ((ber.november - ber_base.november)/ber_base.november))),2) november,
	round(wem.december - (wem.december / (1 + ((ber.december - ber_base.december)/ber_base.december))),2) december,
	round(wem.january - (wem.january / (1 + ((ber.january - ber_base.january)/ber_base.january))),2) +
		round(wem.february - (wem.february / (1 + ((ber.february - ber_base.february)/ber_base.february))),2) +
		round(wem.march - (wem.march / (1 + ((ber.march - ber_base.march)/ber_base.march))),2) +
		round(wem.april - (wem.april / (1 + ((ber.april - ber_base.april)/ber_base.april))),2) +
		round(wem.may - (wem.may / (1 + ((ber.may - ber_base.may)/ber_base.may))),2) +
		round(wem.june - (wem.june / (1 + ((ber.june - ber_base.june)/ber_base.june))),2) +
		round(wem.july - (wem.july / (1 + ((ber.july - ber_base.july)/ber_base.july))),2) +
		round(wem.august - (wem.august / (1 + ((ber.august - ber_base.august)/ber_base.august))),2) +
		round(wem.september - (wem.september / (1 + ((ber.september - ber_base.september)/ber_base.september))),2) +
		round(wem.october - (wem.october / (1 + ((ber.october - ber_base.october)/ber_base.october))),2)  +
		round(wem.november - (wem.november / (1 + ((ber.november - ber_base.november)/ber_base.november))),2) +
		round(wem.december - (wem.december / (1 + ((ber.december - ber_base.december)/ber_base.december))),2) total,
	ber.january jan_rate,
	ber.february feb_rate,
	ber.march mar_rate,
	ber.april apr_rate,
	ber.may may_rate,
	ber.june jun_rate,
	ber.july jul_rate,
	ber.august aug_rate,
	ber.september sep_rate,
	ber.october oct_rate,
	ber.november nov_rate,
	ber.december dec_rate,
	nvl(woaa.base_year,to_number(to_char(sysdate,'yyyy'))) base_year,
	acct.escalation_id rate_type_id
from wo_est_monthly wem, work_order_account acct,
	budget_escalations_rates ber_base, budget_escalations_rates ber,
	wo_est_processing_temp t, budget_escalations be, work_order_approval woaa
where wem.work_order_id = t.work_order_id
and wem.revision = t.revision
and t.work_order_id = acct.work_order_id
and acct.escalation_id = nvl(ber_base.escalation_id,acct.escalation_id)
and acct.escalation_id = ber.escalation_id
and ber.escalation_id = be.escalation_id
and nvl(be.exclude_results,0) = 1
and ber_base.year (+) = nvl(woaa.base_year,to_number(to_char(sysdate,'yyyy')))
and ber.year = wem.year
and ber.est_chg_type_id = -1
and nvl(ber_base.est_chg_type_id,-1) = -1
and t.work_order_id = woaa.work_order_id
and t.revision = woaa.revision
and not exists (
	select 1 from budget_escalations_rates a
	where a.escalation_id = acct.escalation_id
	and a.est_chg_type_id = wem.est_chg_type_id
	and a.year = wem.year
	);

if sqlca.sqlcode < 0 then 
	uf_msg('Error Inserting Escalation Data (exclude_results = 1)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if

if isvalid(w_progressbar) then
	close(w_progressbar)
end if

return 1

end function

public function longlong uf_post_escalation ();//
// If exclude_results = 1 then add "would-be" escalations before escalating
//
i_status_position = i_status_position + 10
uf_msg('Deleting would-be escalations for exclude_results','I')
delete from wo_est_monthly_escalation esc
where exists (
	select 1 from wo_est_processing_temp t, wo_est_monthly wem, work_order_account acct, budget_escalations be
	where t.work_order_id = wem.work_order_id
	and t.revision = wem.revision
	and wem.est_monthly_id = esc.est_monthly_id
	and wem.year = esc.year
	and t.work_order_id = acct.work_order_id
	and acct.escalation_id = be.escalation_id
	and nvl(be.exclude_results,0) = 1
	);

if sqlca.sqlcode < 0 then 
	uf_msg('Error Inserting Escalation Data (exclude_results = 1)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if


i_status_position = i_status_position + 10
uf_msg('Setting base year for exclude_results','I')
update work_order_approval woaa
set base_year = -1
where exists (
	select 1 from wo_est_processing_temp t, budget_escalations be, work_order_account acct
	where t.work_order_id = acct.work_order_id
	and acct.escalation_id = be.escalation_id
	and woaa.revision = t.revision
	and woaa.work_order_id = t.work_order_id
	and nvl(be.exclude_results,0) = 1
	);

if sqlca.sqlcode < 0 then 
	uf_msg('Error Updating base year (exclude_results = 1)','E')
	uf_msg(sqlca.sqlerrtext,'E')
	return -1
end if

if isvalid(w_progressbar) then
	close(w_progressbar)
end if

return 1

end function

on uo_budget_escalation.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_budget_escalation.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

