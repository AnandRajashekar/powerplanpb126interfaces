HA$PBExportHeader$uo_cr_derivation.sru
$PBExportComments$6329:  Trueup Quantities
forward
global type uo_cr_derivation from nonvisualobject
end type
end forward

global type uo_cr_derivation from nonvisualobject
end type
global uo_cr_derivation uo_cr_derivation

type variables
string pi_s_src_table
string pi_s_res_table
string pi_s_res_arg

string i_last_sql, i_debug, i_deriver_table, i_suppl_where_clause
longlong i_counter

uo_ds_top i_ds_oob_strings, i_ds_orig_amount, i_ds_targ_amount, i_ds_elements, &
			 i_ds_oob_strings_ids

uo_ds_top i_ds_columns, i_ds_columns2, i_ds_add_fields

string i_budget_version, i_split_quantities_all_wc

boolean i_called_for_budget = false, i_qty_skip_remaining_rounding_issues = false

string i_gl_jcat, i_rounding_order_by, i_id_field, i_rate_field, i_effmn_cv, i_co_field, i_amount_field, i_cv_normalize, i_addl_rounding_field

// ### 7838: JAK: 2011-10-26: i_split_quantities_wc
string i_split_quantities_wc

longlong i_num_elements, i_num_add_fields

// ### BSB: Maint 10143: uo to store the override 2 attributes
private uo_ds_top i_ds_override2

// JAK: Datastore for default values.  Will be used so fields don't have to be backfilled after the fact.
private uo_ds_top i_ds_defaults
end variables
forward prototypes
public function longlong uf_deriver_update_not_column_src (string a_type, string a_table_name, string a_join, string a_src_table)
public function longlong uf_deriver_update_not_column (string a_type, string a_table_name, string a_join)
public function longlong uf_deriver_update_mult (string a_type, string a_table_name, string a_join_field[], string a_ifb_id, boolean a_visual, string a_concat)
public function longlong uf_deriver_custom (string a_type, string a_table_name, string a_join)
public function integer uf_deriver_update_override (string a_override_name, string a_col_name)
public function longlong uf_deriver (string a_type, string a_table_name, string a_join)
public function longlong uf_deriver_with_src_table (string a_type, string a_table_name, string a_join, string a_src_table)
public function integer uf_setup_tables (string a_src, string a_res)
public function longlong uf_service_company (string a_type, string a_table_name, string a_join)
public function longlong uf_deriver_update (string a_type, string a_table_name, string a_join_field, string a_ifb_id, boolean a_visual)
public function longlong uf_deriver_rounding_error (string a_type, string a_table_name, string a_join_original, string a_join_target, string a_identify_target, string a_table_name_field)
public function longlong uf_deriver_audits (string a_type, string a_audit_name)
public function longlong uf_deriver_create_offset (string a_type_original, string a_type_offset, longlong a_substr_start, longlong a_substr_length)
public function longlong uf_deriver_virtual (string a_type, string a_table_name, string a_string, string a_non_masked_fields, string a_filter1, string a_join1, string a_type_offset, string a_join2)
public function longlong uf_split_quantities (string a_table_name, string a_ifb_id)
public function longlong uf_split_quantities_trueup (string a_table_name)
public function longlong uf_deriver_rounding_error_ids (string a_type, string a_table_name, string a_join_original, string a_join_target, string a_identify_target, string a_table_name_field)
public function longlong uf_split_quantities_all (string a_table_name, string a_ifb_id)
private subroutine uf_system_control (string a_control)
public function integer uf_deriver_override ()
public function integer uf_deriver_override (string a_table_name, string a_src_table, string a_where_clause)
public function string uf_check_element (string a_budget_or_actual, string a_column)
public subroutine uf_deriver_defaults_reset ()
public subroutine uf_deriver_defaults_add (string a_column_name, string a_column_value, integer a_literal)
public function string uf_deriver_defaults_get (string a_table_name, string a_column_name)
end prototypes

public function longlong uf_deriver_update_not_column_src (string a_type, string a_table_name, string a_join, string a_src_table);//*****************************************************************************************
//
//  Function  :  uf_deriver_update_not_column_src
//
//  Description  :  THIS FUNCTION IS A DERIVATION UPDATE FUNCTION USED WHEN THE JOINING
//                  STRING IS NOT SIMPLY A COLUMN(S) FROM A_TABLE_NAME.  IT WILL GET
//                  CALLED WITH THE TYPE OF DERIVATION (a_type), A FIELD THAT WILL 
//                  BE USED FOR THE JOINING STRING, AND A SRC_TABLE.
//
//                  CALL THIS FUNCTION WHEN NO NEW TRANSACTIONS ARE TO BE CREATED AND YOU
//                  JUST WANT TO UPDATE EXISTING ONES.  CALL THIS FUNCTION WHEN THE
//                  TARGET TABLE (A_TABLE_NAME) IS DIFFERENT FROM THE CRITERIA TABLE
//                  (THE CRITERIA FOR THE DERIVATION --- A_SRC_TABLE) ... FOR EXAMPLE,
//                  IF YOU WANT TO UPDATE A CR_DETAIL table based on transactions
//                  in CR_STG table).
//
//  Notes:
//  ------
//  THIS FUNCTION GETS CALLED BY uf_deriver_update_not_column ... IN WHICH CASE THE TABLE
//  AND SRC TABLES ARE THE SAME ... (MEANING THE UPDATE OCCURS BASED ON ROWS IN THE
//  SAME TABLE)
//
//*****************************************************************************************
longlong source_id, num_elements, i, num_columns, counter
string sqls, column

uo_ds_top ds_columns
ds_columns = create uo_ds_top

//	I assume the table being derived against will have the accounting key.
// only updates the accounting key, so only retrieve accounting key fields
sqls = "select description from cr_elements "

f_create_dynamic_ds(ds_columns,"grid",sqls,sqlca,true)
num_columns = ds_columns.rowcount()

sqls = " update " +  a_table_name + " set ( "

//	Build the insert
for i = 1 to num_columns
	column = upper(f_cr_clean_string(ds_columns.getitemstring(i,1)))
	sqls = sqls + '"' + column + '", '
next 

sqls = left(sqls,len(sqls) - 2) + ') = '

////****************************************************************************
////	Build the select
////		- Table aliases will not be used!!  a_join must not contain aliases.
////****************************************************************************

sqls = sqls + " (select "

for i = 1 to num_columns
	column = upper(f_cr_clean_string(ds_columns.getitemstring(i,1)))
	
	// pull from cr_deriver_control (unless it is a '*')
	sqls = sqls + 'decode(nvl(cr_deriver_control."' + column + '", '
	sqls = sqls + "' '), "
	sqls = sqls + "'*',"
	sqls = sqls + a_src_table + '."' + column + '",nvl(cr_deriver_control."' + column + '" '
	sqls = sqls + ",' ')), "
next 
sqls = left(sqls,len(sqls) - 2)

sqls = sqls + " from cr_deriver_control " +&
	" where " + a_join + " and cr_deriver_control."
sqls = sqls + '"TYPE" = ' 	
sqls = sqls + "'" + a_type + "')"
execute immediate :sqls;

i_last_sql = sqls

if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Updating " + a_table_name + " : " + sqlca.sqlerrtext )
	f_write_log(g_log_file,sqls)
	return -1
end if

return 1
end function

public function longlong uf_deriver_update_not_column (string a_type, string a_table_name, string a_join);//*****************************************************************************************
//
//  Function  :  uf_deriver_update_not_column
//
//  Description  :  THIS FUNCTION IS A DERIVATION UPDATE FUNCTION USED WHEN THE JOINING
//                  STRING IS NOT SIMPLY A COLUMN(S) FROM A_TABLE_NAME.  IT WILL GET
//                  CALLED WITH THE TYPE OF DERIVATION (a_type), AND A FIELD THAT WILL 
//                  BE USED FOR THE JOINING STRING.
//
//                  CALL THIS FUNCTION WHEN NO NEW TRANSACTIONS ARE TO BE CREATED AND YOU
//                  JUST WANT TO UPDATE EXISTING ONES.
//
//*****************************************************************************************
return uf_deriver_update_not_column_src(a_type, a_table_name, a_join, a_table_name)
end function

public function longlong uf_deriver_update_mult (string a_type, string a_table_name, string a_join_field[], string a_ifb_id, boolean a_visual, string a_concat);//*****************************************************************************************
//
//  Function  :  uf_deriver_update_mult
//
//  Description  :  THIS FUNCTION IS THE BASIC DERIVATION UPDATE FUNCTION.  IT WILL GET
//                  CALLED WITH THE TYPE OF DERIVATION (a_type), AN ARRAY OF FIELDS THAT
//                  WILL BE USED FOR THE JOINING STRING, AS WELL AS A CONCATENATION 
//                  CHARACTER.
//
//                  CALL THIS FUNCTION WHEN NO NEW TRANSACTIONS ARE TO BE CREATED AND YOU
//                  JUST WANT TO UPDATE THEM BASED ON THE VALUE OF A MULTIPLE COLUMNS FROM 
//                  A_TABLE_NAME.
//
//  Notes:
//  ------
//  CALL THIS FUNCTION WHEN THE UPDATE IS A "MULTIPLE-COLUMN" UPDATE ... UPDATES THAT 
//  REQUIRE A CONCATENATION OF COLUMNS (E.G. WO AND TASK).
//
//  IF THIS FUNCTION IS CALLED FROM uf_deriver_update, then THE ARRAY WILL ONLY BE OF
//  LENGTH OF ONE WITHOUT A CONCATENATION CHARACTER.
//
//  This is the old UF_DERIVER_UPDATE code from PINNACLE.  Pinnacle's custom functions
//  will still be able to call uf_deriver_update since they have a single-column update.
//
//*****************************************************************************************
longlong i, num_columns, counter, num, val2
string sqls, column, join_field, val1, column_bdg
longlong added_to_end

uf_system_control('DERIVATIONS - EFFMN')

//
//  Build the join field from the concatination of columns in the string array
//
//  Something like: a_join_field[1] = "COMPANY", a_join_field[2] = "WORK_ORDER"
//  Becomes:        table_name."COMPANY"||'-'||table_name."WORK_ORDER"||'-'||
//
join_field = ''
num = upperBound(a_join_field)
for i = 1 to num
	join_field = join_field + a_table_name + &
						'."' + upper(trim(a_join_field[i])) + '"||' + &
						"'" + a_concat + "'||"
next

//  Strip the trailing ||'-'||
added_to_end = 6 + len(a_concat)
join_field = left(join_field, len(join_field) - added_to_end)

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** join_field = " + join_field)
end if


//***************************************************************************************************
//
//  IF EFFECTIVE DATING:
//
//  Need an additional audit to avoid "cannot update to NULL" errors.
//  For example, if there are strings with date ranges of 200701-200712 and 200801-200812,
//  a 2009 transaction will bomb out since the update will pass the outer where clause on
//  string, but will not pass the inner where clause that includes the MN range.
//
//  Note that if they had a record with NULL in the effmn fields, then this SQL would return
//  no rows and the update below would work.  So this case is "there are effective-dated rows
//  for this string, and none of them overlap with the transaction's MN, and there are no
//  rows in CDC for this string with nulls in the effmns".
//
//  SQLS is of the folowing form:
//     select table_name.company||'-'||table_name.work_order, month_number
//       from table_name
//      where exists (
//               select 1 from cr_deriver_control 
//                where "TYPE" = 'THE A_TYPE VALUE'
//                  and table_name.company||'-'||table_name.work_order = cr_deriver_control.string
//                  and table_name.month_number not between 
//                         nvl(cr_deriver_control.effective_month_number, 0) 
//                     and nvl(cr_deriver_control.effective_month_number2, 999999))
//     minus
//     select table_name.company||'-'||table_name.work_order, month_number
//       from table_name
//      where exists (
//               select 1 from cr_deriver_control 
//                where "TYPE" = 'THE A_TYPE VALUE'
//                  and table_name.company||'-'||table_name.work_order = cr_deriver_control.string
//                  and table_name.month_number between 
//                         nvl(cr_deriver_control.effective_month_number, 0) 
//                     and nvl(cr_deriver_control.effective_month_number2, 999999))
//
//  NOTE ON THE COMMENTS ABOVE WITH REGARDS TO INSERT DERIVATIONS:
//  We did not port this audit over to the insert derivations.  It is not needed there since
//  missing records cannot cause a SQL error.  Also, transaction that do not match the STRING are
//  supposed to fall out of the insert derivations, so we don't want to deviate from that
//  methodology.
//
//***************************************************************************************************
if i_effmn_cv = "YES" then
	
	sqls = &
		"select " + join_field + ", month_number " + &
		  "from " + a_table_name + " " + &
		 "where exists (" + &
				"select 1 from cr_deriver_control " + &
				 'where "TYPE" = ' + "'" + a_type + "' and cr_deriver_control.string = " + join_field + " " + &
				   "and " + a_table_name + ".month_number not between " + &
					             "nvl(cr_deriver_control.effective_month_number, 0) " + &
								"and nvl(cr_deriver_control.effective_month_number2, 999999)) "
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** table_name = " + a_table_name)
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** type = " + a_type)
	end if
	
	// ### BAT - Maint 9799 - Remove reference to allocation_id = interface_batch_id
	if upper(a_table_name) = "CR_BUDGET_DATA" or upper(a_table_name) = "CR_BUDGET_DATA_TEST" or &
		upper(a_table_name) = "CR_BUDGET_REVERSALS" or upper(a_table_name) = "CR_BUDGET_DATA_TEMP" &
	then
		sqls = sqls + " and " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' " + &
			"and " + a_table_name + ".budget_version = '" + i_budget_version + "' "
	else
		sqls = sqls + " and " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' "
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** a_ifb_id = " + a_ifb_id)
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	sqls = sqls + "minus "

	sqls = sqls + &
		"select " + join_field + ", month_number " + &
		  "from " + a_table_name + " " + &
		 "where exists (" + &
				"select 1 from cr_deriver_control " + &
				 'where "TYPE" = ' + "'" + a_type + "' and cr_deriver_control.string = " + join_field + " " + &
				   "and " + a_table_name + ".month_number between " + &
					             "nvl(cr_deriver_control.effective_month_number, 0) " + &
								"and nvl(cr_deriver_control.effective_month_number2, 999999)) "
	
	// ### BAT - Maint 9799 - Remove reference to allocation_id = interface_batch_id
	if upper(a_table_name) = "CR_BUDGET_DATA" or upper(a_table_name) = "CR_BUDGET_DATA_TEST" or &
		upper(a_table_name) = "CR_BUDGET_REVERSALS" or upper(a_table_name) = "CR_BUDGET_DATA_TEMP" &
	then
		sqls = sqls + " and " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' " + &
			"and " + a_table_name + ".budget_version = '" + i_budget_version + "' "
	else
		sqls = sqls + " and " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' "
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	uo_ds_top ds_effmn_audit
	ds_effmn_audit = CREATE uo_ds_top
	
	f_create_dynamic_ds(ds_effmn_audit, "grid", sqls, sqlca, true)
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG***  ds_effmn_audit.i_sqlerrtext = " + ds_effmn_audit.i_sqlca_sqlerrtext)
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG***  ds_effmn_audit.i_sqlsyntax = " + ds_effmn_audit.i_sqlca_sqlsyntax)
	end if
	
	if ds_effmn_audit.RowCount() > 0 then
		f_pp_msgs("  ")
		f_pp_msgs("AUDIT ERROR (" + a_type + "): Effective dated derivation rows do not exist for the following combinations:")
		f_pp_msgs("  ")
		for i = 1 to ds_effmn_audit.RowCount()
			val1 = ds_effmn_audit.GetItemString(i, 1)
			val2 = ds_effmn_audit.GetItemNumber(i, 2)
			f_pp_msgs(val1 + "   " + string(val2))
		next
		f_pp_msgs("  ")
		f_pp_msgs("AUDIT ERROR (" + a_type + "):")
		
		// ### 8923: JAK: 2011-10-31: Just write SQL to the logs instead of to a text file.  
		//	f_pp_msgs will break it into multiple messages as needed.
		f_pp_msgs("SQL:  " + sqls)
		f_pp_msgs("  ")
		rollback; // Not clear if this is OK here or not.
		return -1
	end if
	
end if


uo_ds_top ds_columns
ds_columns = create uo_ds_top


//*****************************************************************************************
//
//  GET THE COLUMNS FOR THE A_TABLE.
//
//	 Use all_tab_columns to get the column names instead of cr_sources_fields in case we 
//  perform derivations against a table that is not a source. For instance ... if we need 
//  to run derivations against a "pre-stage" client table before loading the CR.  This 
//  function has a "table_name" argument instead of a source_id argument.
//
//  The table being derived against MUST have the accounting key.
//
//*****************************************************************************************
sqls = "select column_name,column_id from sys.all_tab_columns " + &
	"where owner = 'PWRPLANT' and table_name = '" + upper(trim(a_table_name)) + "'"

f_create_dynamic_ds(ds_columns,"grid",sqls,sqlca,true)
num_columns = ds_columns.rowcount()
if i_debug = "YES" then
	f_pp_msgs("***DEBUG***  ds_columns.i_sqlerrtext = " + ds_columns.i_sqlca_sqlerrtext)
end if

//	Sort the datastore by the column_id to build the insert in the order the
//		columns are on the table.
ds_columns.setsort("#2 A")
ds_columns.sort()


//*******************************************************************************************************
//
//  BUILD THE UPDATE COMMAND.
//
//  SQLS is of the folowing form (if FERC is the derived field): 
//
//  update a_table_name set "FERC" = (
//        select nvl( decode(cr_deriver_control."FERC",'*',a_table_name."FERC",cr_deriver_control."FERC") , ' ')
//          from cr_deriver_control
//         where cr_deriver_control."TYPE" = 'THE A_TYPE VALUE'
//           and a_table_name.interface_batch_id = '123'
//           and cr_deriver_control.string = a_table_name."COMPANY"||'-'||a_table_name."WORK_ORDER" )
//   where interface_batch_id = '123'
//     and a_table_name."COMPANY"||'-'||a_table_name."WORK_ORDER" in
//            (select string from cr_deriver_control  where "TYPE" = 'THE A_TYPE VALUE')
//
//  THIS IS A LITTLE DIFFERENT THAN THE CODE IN THE "INSERT" DERIVATIONS.
//  In those, we have min(effective_month_number) code to try to eliminate overlapping date ranges.
//  If the user happens to double-up a range exactly (e.g. 2 sets of splits for a string each with the
//  same date range), then tough, all of those records go into the splits - correct or not.  We can
//  include other auditing elsewhere.  On these update derivations however, I cannot just say "tough"
//  because 2 records with the same effmn will both pass a where clause like in the "inserts" and
//  we'll get a single-row subquery error.
//
//  Addition to the above ... we did add the min(effmn) where clause so truly overlapping date ranges
//  would behave in the same manner in both functions.  The only time the min() in the select will
//  fire will be if there are 2 of the same "TYPE"/string records with the same effective_month_number.
//
//*******************************************************************************************************
for i = 1 to num_columns
	
	column = upper(trim(ds_columns.GetItemString(i, 1)))
	
	//  If the "deriver" field is actually an ACK field, no need to derive it.
	if column = join_field then
		continue
	end if
	
	choose case column
		case "ID", "DR_CR_ID", "LEDGER_SIGN", "QUANTITY", "AMOUNT", "MONTH_NUMBER", &
			  "MONTH_PERIOD", "GL_JOURNAL_CATEGORY", "AMOUNT_TYPE", "DRILLDOWN_KEY", &
			  "CWIP_CHARGE_STATUS", "TIME_STAMP", "USER_ID"
			//  No need to perform a lookup ... we know these are not in the ACK.
			continue
		case else
			////	i_called_for_budget is not set in this user object.  This allows no impact to any clients currently
			////		using derivations.  If someone wants to use the derivations for budget they'll have to set
			////		i_called_for_budget when calling the derivations.  Derivations for budget don't exist anywhere right now.
			////		Right now, we will only have update budget derivations so this variable is only evaluated in this function.
			if not i_called_for_budget then
				//  If this column is not an ACK field, continue.
				counter = 0
				select count(*) into :counter	from cr_elements
				 where upper(replace(replace(replace(description,'-','_'),'/','_'),' ','_')) = :column;
				if isnull(counter) then counter = 0
				if counter = 0 then continue
			else
				//  If this column is not an ACK field, continue.
				counter = 0
				select count(*) into :counter	from cr_elements
				 where upper(replace(replace(replace(budgeting_element,'-','_'),'/','_'),' ','_')) = :column;
				if isnull(counter) then counter = 0
				if counter = 0 then continue
				//	cr_deriver_control will have the actuals ACK.  What is the actuals element for this budget element?
				column_bdg = column
column = upper(f_replace_string(f_replace_string(f_replace_string(g_cr.uf_get_element_description(g_cr.uf_get_element_id("f_replace_string(f_replace_string(f_replace_string(budgeting_element,'-','_','all'),'/','_','all'),' ','_','all')",column)),'-','_','all'),'/','_','all'),' ','_','all')) //Autogenerated sql replace
			end if
	end choose
	
	if not a_visual then
		if a_table_name = "cr_allocations_stg" or a_table_name = "cr_allocations_test_stg" or &
		   a_table_name = "cr_inter_company_stg" or a_table_name = "cr_inter_company_test_stg" or &
			a_table_name = "cr_budget_data" or a_table_name = "cr_budget_data_temp" or &
			a_table_name = "cr_budget_data_test" or a_table_name = "cr_budget_data_entry" &
		then
			//  No messages.
		else
			f_pp_msgs("-- Element = " + column)
		end if
	end if
	
	if not i_called_for_budget then
		sqls = "update " +  a_table_name + ' set "' + column + '" = (select '
	else
		sqls = "update " +  a_table_name + ' set "' + column_bdg + '" = (select '
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	if i_effmn_cv = "YES" then
		//  This SQL needs a min() in case they accidentally overlap date ranges.  We can have
		//  auditing somewhere else to detect this case, but I don't want the derivation to
		//  throw up just because of the overlap.  We created this if-then-else to be
		//  un-intrusive to the existing code.
		if not i_called_for_budget then
			sqls = sqls + ' min( nvl( decode(cr_deriver_control."' + column + '",' + &
				"'*'," + a_table_name + '."' + column + '",' + &
				'cr_deriver_control."' + column + '") , ' + "' ') ) " 
		else
			sqls = sqls + ' min( nvl( decode(cr_deriver_control."' + column + '",' + &
				"'*'," + a_table_name + '."' + column_bdg + '",' + &
				'cr_deriver_control."' + column + '") , ' + "' ') ) " 
		end if
	else
		if not i_called_for_budget then
			sqls = sqls + ' nvl( decode(cr_deriver_control."' + column + '",' + &
				"'*'," + a_table_name + '."' + column + '",' + &
				'cr_deriver_control."' + column + '") , ' + "' ') " 
		else
			sqls = sqls + ' nvl( decode(cr_deriver_control."' + column + '",' + &
				"'*'," + a_table_name + '."' + column_bdg + '",' + &
				'cr_deriver_control."' + column + '") , ' + "' ') "
		end if
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** column = " + column)
	end if
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	sqls = sqls + " from cr_deriver_control " 
	
	sqls = sqls + 'where cr_deriver_control."TYPE" = ' + "'" + a_type + "' "
	
	if i_debug = "YES" then
		if isnull(sqls) then sqls = 'null'
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
		
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** type = " + a_type)
	end if
	
	// ### BAT - Maint 9799 - Remove reference to allocation_id = interface_batch_id
	if upper(a_table_name) = "CR_BUDGET_DATA" or upper(a_table_name) = "CR_BUDGET_DATA_TEST" or &
		upper(a_table_name) = "CR_BUDGET_REVERSALS" or upper(a_table_name) = "CR_BUDGET_DATA_TEMP" &
	then
		sqls = sqls + " and " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' " + &
			"and " + a_table_name + ".budget_version = '" + i_budget_version + "' "
	else
		sqls = sqls + " and " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' "
	end if
	
	if i_debug = "YES" then
		if isnull(a_ifb_id) then a_ifb_id = 'null'
		f_pp_msgs("***DEBUG*** a_ifb_id = " + a_ifb_id)
	end if
	
	if i_debug = "YES" then
		if isnull(sqls) then sqls = 'null'
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	sqls = sqls + " and cr_deriver_control.string = " + join_field + ' '
	
	if i_debug = "YES" then
		if isnull(sqls) then sqls = 'null'
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	if i_effmn_cv = "YES" then
		//  Restrict based on the month_number range.
		sqls = sqls + " and " + a_table_name + ".month_number between " + &
				"nvl(cr_deriver_control.effective_month_number, 0) " + &
		  "and nvl(cr_deriver_control.effective_month_number2, 999999) "
		 
		sqls = sqls + 'and nvl(cr_deriver_control.effective_month_number, 0) in ' + &
			'(select min(nvl(effective_month_number, 0)) from cr_deriver_control x ' + &
			  'where cr_deriver_control.string = x.string and cr_deriver_control."TYPE" = x."TYPE") '
	end if
	
	sqls = sqls + ") "
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** SQLS = " + sqls)
	end if
	
	
	// ### BAT - Maint 9799 - Remove reference to allocation_id = interface_batch_id
	if upper(a_table_name) = "CR_BUDGET_DATA" or upper(a_table_name) = "CR_BUDGET_DATA_TEST" or &
		upper(a_table_name) = "CR_BUDGET_REVERSALS" or upper(a_table_name) = "CR_BUDGET_DATA_TEMP" &
	then
		sqls = sqls + " where " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' " + &
			"and " + a_table_name + ".budget_version = '" + i_budget_version + "' "
	else
		sqls = sqls + " where " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' "
	end if
	
	sqls = sqls + ' and ' + join_field + ' in (select string from cr_deriver_control '
	
	sqls = sqls + ' where "TYPE" = ' + "'" + a_type + "')"
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** sqls = " + sqls)
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		// ### 8923: JAK: 2011-10-31: Just write SQL to the logs instead of to a text file.  
		//	f_pp_msgs will break it into multiple messages as needed.
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating " + a_table_name + "." + column + ": " + &
		sqlca.SQLErrText)
		f_pp_msgs("SQL:  " + sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	else
		commit;
	end if
	
next


return 1
end function

public function longlong uf_deriver_custom (string a_type, string a_table_name, string a_join);
return 1
end function

public function integer uf_deriver_update_override (string a_override_name, string a_col_name);//*****************************************************************************************
//
//  Function  :  uf_deriver_update_override
//
//  Description  :  THIS FUNCTION IS A DERIVATION UPDATE FUNCTION THAT ALLOWS YOU TO USE
//                  COMPLEX SQL-BASED DERIVATIONS.  IT KEYS OFF OF CR_DERIVER_OVERRIDE2
//                  WHICH IS MAINTAINED ONLINE.  IN CR_DERIVER_CONTROL, ENTER A "-" IN
//                  THE FIELD WHERE YOU WANT TO USE THIS LOGIC (THE MAIN TAB ON THE 
//                  DERIVATION MAINT WINDOW).
//
//*****************************************************************************************
string update_sql, s_sql, l_col_name
longlong start_pos

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** entered uf_deriver_update_override")
	f_pp_msgs("***DEBUG*** a_override_name = " + a_override_name)
	f_pp_msgs("***DEBUG*** a_col_name = " + a_col_name)
	f_pp_msgs("***DEBUG*** pi_s_res_table = " + pi_s_res_table)
	f_pp_msgs("***DEBUG*** pi_s_res_arg = " + pi_s_res_arg)
	f_pp_msgs("***DEBUG*** pi_s_src_table = " + pi_s_src_table)
	f_pp_msgs("***DEBUG*** i_suppl_where_clause = " + i_suppl_where_clause)
end if

select update_sql
into :update_sql
from cr_deriver_override2
where override_name = :a_override_name
and col_name = :a_col_name;

if isnull(update_sql) then
	return 0
end if

l_col_name = upper(f_cr_clean_string(a_col_name))

start_pos = pos(update_sql, "res_table")
do while start_pos > 0
	update_sql = replace(update_sql, start_pos, 9, pi_s_res_table)
	start_pos = pos(update_sql, "res_table")
loop

start_pos = pos(update_sql, "src_table")
do while start_pos > 0
	update_sql = replace(update_sql, start_pos, 9, pi_s_src_table)
	start_pos = pos(update_sql, "src_table")
loop

start_pos = pos(i_suppl_where_clause, "res_arg")
do while start_pos > 0
	i_suppl_where_clause = replace(i_suppl_where_clause, start_pos, 9, pi_s_res_arg)
	start_pos = pos(i_suppl_where_clause, "res_arg")
loop

if isnull(i_suppl_where_clause) then i_suppl_where_clause = ""

s_sql = 	"update " + pi_s_res_table + ' set "' + l_col_name + '"' + &
					" = " + update_sql + " where " + pi_s_res_table +&
					'."' + l_col_name + '"' + " = '-' " + i_suppl_where_clause

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** uf_deriver_update_override: s_sql = " + s_sql)
end if

i_last_sql = s_sql
execute immediate :s_sql;

if sqlca.sqlcode < 0 then
	// ### 8923: JAK: 2011-10-31: Just write SQL to the logs instead of to a text file.  
	//	f_pp_msgs will break it into multiple messages as needed.
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: in derivation overrides (2):~n~n" + sqlca.SQLErrText)
	f_pp_msgs("SQL:  " + s_sql)
	f_pp_msgs("  ")
	return -1
end if

return 1

end function

public function longlong uf_deriver (string a_type, string a_table_name, string a_join);//*****************************************************************************************
//
//  Function  :  uf_deriver
//
//  Description  :  THIS FUNCTION IS THE BASIC DERIVATION INSERT FUNCTION.  IT WILL GET 
//                  CALLED WITH THE TYPE OF DERIVATION (a_type), THE JOINING STRING (a_join),
//                  AND THE TABLE WHICH WILL BE USED FOR THE DERIVATION.  WHEN THIS FUNCTION
//                  GETS CALLED, THE TRANSACTIONS THAT GET CREATED BY THE DERIVATION WILL
//                  BE INSERTED INTO THE SAME TABLE AS THEY ARE READ FROM.
//
//  Notes:
//  ------
//  CALL THIS FUNCTION WHEN THE SOURCE AND TARGET TABLE ARE THE SAME ... THIS FUNCTION
//  IS ALSO HERE TO SUPPORT ANY CURRENT CLIENTS THAT WERE USING UF_DERIVER BEFORE THE
//  CHANGES.  THE OLD UF_DERIVER CODE HAS BEEN MOVED TO UF_DERIVER_WITH_SRC_TABLE.
//
//*****************************************************************************************
return uf_deriver_with_src_table(a_type, a_table_name, a_join, a_table_name)
end function

public function longlong uf_deriver_with_src_table (string a_type, string a_table_name, string a_join, string a_src_table);//*****************************************************************************************
//
//  Function  :  uf_deriver
//
//  Description  :  THIS FUNCTION IS THE BASIC DERIVATION INSERT FUNCTION.  IT WILL GET 
//                  CALLED WITH THE TYPE OF DERIVATION (a_type), THE JOINING STRING (a_join),
//                  AND THE TABLE WHICH WILL BE USED FOR THE DERIVATION.  WHEN THIS FUNCTION
//                  GETS CALLED, THE TRANSACTIONS THAT GET CREATED BY THE DERIVATION WILL
//                  BE INSERTED INTO THE TABLE SPECIFIED BY A_TABLE_NAME (UNLIKE THE
//                  UF_DERIVER FUNCTION).  THE SOURCE TRANSACTIONS ARE READ FROM THE 
//                  A_SRC_TABLE.
//
//  Notes:
//  ------
//  IF THE TWO TABLES ARE THE SAME (AS WILL BE THE CASE WHEN uf_deriver CALLS THIS 
//  FUNCTION), THE DERIVED TRANSACTIONS WILL BE INSERTED INTO THE SAME TABLE AS THEY 
//  ARE BEING READ FROM.
//
//  CALL THIS FUNCTION WHEN THE SOURCE AND TARGET TABLE ARE DIFFERENT ... UF_DERIVER
//  IS ALSO IN THIS UO TO SUPPORT ANY CURRENT CLIENTS THAT WERE USING UF_DERIVER BEFORE
//  THE CHANGES.  THE OLD UF_DERIVER CODE HAS BEEN MOVED TO THIS FUNCTION.
//
//  ***  DMJ: NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
string sqls, column, msg_str, deriver_column
longlong i, num_columns 


//
//  Determine the "deriver" table.  If the instance variable is null, then
//  use cr_deriver_control.
//
if isnull(i_deriver_table) or trim(i_deriver_table) = "" then
	i_deriver_table = "cr_deriver_control"
end if

//NO LONGER NEEDED DUE TO I VAR: uo_ds_top ds_columns
//NO LONGER NEEDED DUE TO I VAR: ds_columns = create uo_ds_top

pi_s_src_table = a_src_table
pi_s_res_table = a_table_name

//	Use all_tab_columns to get the column names instead of cr_sources_fields
//		in case we perform derivations against a table that is not a source.
//		For instance...if we need to run derivations against a "pre-stage" 
//		client table before loading the CR.
//		This function has a "table_name" argument instead of a source_id argument.

//	I assume the table being derived against will have the accounting key.

sqls = "select column_name,column_id from sys.all_tab_columns " + &
	"where table_name = '" + upper(trim(a_table_name)) + "' and owner = 'PWRPLANT'"

//NO LONGER NEEDED DUE TO I VAR: f_create_dynamic_ds(ds_columns,"grid",sqls,sqlca,true)
i_ds_columns2.SetSQLSelect(sqls)
i_ds_columns2.RETRIEVE()
num_columns = i_ds_columns2.rowcount()

//	Sort the datastore by the column_id to build the insert in the order the
//		columns are on the table.
i_ds_columns2.setsort("#2 A")
i_ds_columns2.sort()

uf_system_control('DERIVATIONS : AMOUNT FIELD')
uf_system_control('DERIVATIONS - EFFMN')
uf_system_control('DERIVATIONS - PERCENT ATTRIBUTE')
uf_system_control('DERIVATIONS - ID ATTRIBUTE')
uf_system_control('DERIVATIONS - NORMALIZE PERCENT')

// Populate default values
if not isnull(i_gl_jcat) and trim(i_gl_jcat) <> "" then uf_deriver_defaults_add('gl_journal_category',i_gl_jcat,1)
uf_deriver_defaults_add(i_id_field,a_table_name + '."ID"',0)
	
sqls = " insert into " +  a_table_name + " ( "

//	Build the insert

for i = 1 to num_columns
	
	column = i_ds_columns2.getitemstring(i,1)
	
	// ### 10622: JAK: 2012-07-02: Skip CWIP Charge Status as well so it doesn't get carried from old records to new.
	choose case column
		case 'TIME_STAMP','USER_ID','DRILLDOWN_KEY','CWIP_CHARGE_STATUS'
			continue
		case else
			sqls += '"' + column + '", '
	end choose
next 

// Trim trailing comma
sqls = mid(sqls,1,len(sqls) - 2) + ') '

////****************************************************************************
////	Build the select
////		- Table aliases will not be used!!  a_join must not contain aliases.
////****************************************************************************
sqls += " select "

for i = 1 to num_columns
	
	column = i_ds_columns2.getitemstring(i,1)
	
	// ### 10622: JAK: 2012-07-02: Skip CWIP Charge Status as well so it doesn't get carried from old records to new.
	choose case column
		case 'TIME_STAMP','USER_ID','DRILLDOWN_KEY','CWIP_CHARGE_STATUS'
			continue
			
		case i_amount_field
			if i_cv_normalize = "YES" then
				//  The denominator is abs() so we don't mess things up like the -1 used
				//  for credits.
				if i_effmn_cv = "YES" then
					sqls += &
						'round(' + a_table_name + '.' + i_amount_field + &
						' * (' + i_deriver_table + '.percent ' + &
						'/ abs(' + &
						'(select sum(x.percent) from ' + i_deriver_table + ' x ' + &
						 ' where ' + f_replace_string(a_join, i_deriver_table + ".", "x.", "all") + " " + &
							 'and x."TYPE" = ' + "'" + a_type + "' " + &
							 'and ' + a_table_name + '.month_number between ' + &
											  'nvl(x.effective_month_number, 0) ' + &
										 'and nvl(x.effective_month_number2, 999999) ' + &
							 'and nvl(x.effective_month_number, 0) = ' + &
									'(select min(nvl(effective_month_number,0)) from ' + i_deriver_table + ' y ' + &
									  'where x.string = y.string and x."TYPE" = y."TYPE") ' + &
						 ")" + &
						 ')), 2), '
				else
					sqls += &
						'round(' + a_table_name + '.' + i_amount_field + &
						' * (' + i_deriver_table + '.percent ' + &
						'/ abs(' + &
						'(select sum(x.percent) from ' + i_deriver_table + ' x ' + &
						 ' where ' + f_replace_string(a_join, i_deriver_table + ".", "x.", "all") + " " + &
							 'and x."TYPE" = ' + "'" + a_type + "' " + &
						 ")" + &
						 ')), 2), '
				end if
			else
				sqls += 'round(' + a_table_name + '.' + i_amount_field + ' * ' + i_deriver_table + '.percent,2), '
			end if
			
		case 'ID'
			if upper(a_table_name) = "CR_BUDGET_DATA" or upper(a_table_name) = "CR_BUDGET_DATA_TEST" or &
				upper(a_table_name) = "CR_BUDGET_REVERSALS" or upper(a_table_name) = "CR_BUDGET_DATA_TEMP" or &
				upper(a_Table_name) = "CRB_BUDGET_DATA" then
				
				sqls += 'crbudgets.nextval, '
			else
				sqls += 'crdetail.nextval, '
			end if
				
		case i_rate_field
			if i_cv_normalize = "YES" then
				if i_effmn_cv = "YES" then
					//  The denominator is abs() so we don't mess things up like the -1 used
					//  for credits.
					sqls += &
						i_deriver_table + '."PERCENT" ' + &
						'/ abs(' + &
						'(select sum(x."PERCENT") from ' + i_deriver_table + ' x ' + &
						 ' where ' + f_replace_string(a_join, i_deriver_table + ".", "x.", "all") + " " + &
							 'and x."TYPE" = ' + "'" + a_type + "' " + &
							 'and ' + a_table_name + '.month_number between ' + &
							              'nvl(x.effective_month_number, 0) ' + &
										 'and nvl(x.effective_month_number2, 999999) ' + &
							 'and nvl(x.effective_month_number, 0) = ' + &
						 		'(select min(nvl(effective_month_number,0)) from ' + i_deriver_table + ' y ' + &
								  'where x.string = y.string and x."TYPE" = y."TYPE") ' + &
						 ")" + &
						 '), '
				else
					//  The denominator is abs() so we don't mess things up like the -1 used
					//  for credits.
					sqls += &
						i_deriver_table + '."PERCENT" ' + &
						'/ abs(' + &
						'(select sum(x."PERCENT") from ' + i_deriver_table + ' x ' + &
						 ' where ' + f_replace_string(a_join, i_deriver_table + ".", "x.", "all") + " " + &
							 'and x."TYPE" = ' + "'" + a_type + "' " + &
						 ")" + &
						 '), '
				end if
			else
				sqls += i_deriver_table + '."PERCENT", '
			end if
			
		case else
			// Accounting key fields and detail attributes.
			
			if upper(a_table_name) = "CR_BUDGET_DATA" or upper(a_table_name) = "CR_BUDGET_DATA_TEST" or &
				upper(a_table_name) = "CR_BUDGET_REVERSALS" or upper(a_table_name) = "CR_BUDGET_DATA_TEMP" or &
				upper(a_Table_name) = "CRB_BUDGET_DATA" then
				
				deriver_column = uf_check_element('BUDGET',column)
			else 
				deriver_column = uf_check_element('ACTUAL',column)
			end if
			
			if deriver_column = '0' then
				// detailed attribute - check the defaults
				sqls += uf_deriver_defaults_get(a_table_name, column) + ', '
			else
				// accounting key.
				sqls += &
					'substr( ' + i_deriver_table + '."' + deriver_column + '", 1, decode( instr(' + i_deriver_table + '."' + deriver_column + '", ' + "'*'" + ') - 1, -1, 100, instr(' + i_deriver_table + '."' + deriver_column + '", ' + "'*') - 1 ) )"
				sqls += "||"
				sqls += 'substr(' + a_table_name + '."' + column + '", decode( instr(' + i_deriver_table + '."' + deriver_column + '", ' + "'*'" + '), 0, 100, instr(' + i_deriver_table + '."' + deriver_column + '", ' + "'*') ), 100), "
			end if
	end choose
next 

sqls = mid(sqls,1,len(sqls) - 2)

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	DO I NEED AN ARGUMENT FOR INTERFACE_BATCH_ID OR WILL IT BE INCLUDED IN A_JOIN?
//		- INCLUDE IT IN A_JOIN FOR NOW...
//
//	A_TYPE IS INCLUDED IN THE WHERE CLAUSE SEPARATELY (AND NOT PART OF A_JOIN) 
//	SINCE THAT COLUMN IS PART OF CR_DERIVER_CONTROL AND NOT PART OF A_TABLE.
//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

sqls += " from " + a_table_name + ", " + i_deriver_table + "  " + &
	" where " + a_join + " and " + i_deriver_table + "."
sqls += '"TYPE" = ' 
sqls += "'" + a_type + "'"

if i_effmn_cv = "YES" then
	sqls += " and " + a_table_name + ".month_number between " + &
									    "nvl(" + i_deriver_table + ".effective_month_number, 0) " + &
									"and nvl(" + i_deriver_table + ".effective_month_number2, 999999)"
end if

if i_debug = "YES" then
	//  This string could go over the 2000 character field...  f_pp_msgs does this now.
	f_pp_msgs("***DEBUG*** sqls = ")
	f_pp_msgs(sqls)
end if

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Inserting into " + a_table_name + " : " + sqlca.sqlerrtext )
	f_pp_msgs(sqls)
	f_pp_msgs(" ")
	return -1
end if

return 1
end function

public function integer uf_setup_tables (string a_src, string a_res);// A WAY TO SETUP THE SRC AND RES TABLES IN ORDER TO PERFORM AN OVERRIDE

pi_s_src_table = a_src
pi_s_res_table = a_res

return 1
end function

public function longlong uf_service_company (string a_type, string a_table_name, string a_join);longlong source_id, num_elements, i, num_columns, counter, sco_effective_month
string sqls, column

uo_ds_top ds_columns
ds_columns = create uo_ds_top

//	Use all_tab_columns to get the column names instead of cr_sources_fields
//		in case we perform derivations against a table that is not a source.
//		For instance...if we need to run derivations against a "pre-stage" 
//		client table before loading the CR.
//		This function has a "table_name" argument instead of a source_id argument.

//	I assume the table being derived against will have the accounting key.

sqls = "select column_name,column_id from sys.all_tab_columns " + &
	"where owner = 'PWRPLANT' and table_name = '" + upper(trim(a_table_name)) + "'"

f_create_dynamic_ds(ds_columns,"grid",sqls,sqlca,true)
num_columns = ds_columns.rowcount()

//	Sort the datastore by the column_id to build the insert in the order the
//		columns are on the table.
ds_columns.setsort("#2 A")
ds_columns.sort()


uf_system_control('DERIVATIONS : AMOUNT FIELD')
// ### 7529 DML: 051011: Capture the ID and Percent
uf_system_control('DERIVATIONS - PERCENT ATTRIBUTE')
uf_system_control('DERIVATIONS - ID ATTRIBUTE')

//	Look up the effective month for the billing rates.
sco_effective_month = long(g_cr.uf_get_control_value('SCO BILLING TYPE EFFECTIVE MONTH')) //Autogenerated sql replace

sqls = " insert into " +  a_table_name + " ( "

//	Build the insert

for i = 1 to num_columns
	
	column = ds_columns.getitemstring(i,1)
	
	if column = 'TIME_STAMP' or column = 'USER_ID' or column = 'DRILLDOWN_KEY' then 
		//	goto versus a continue just in case the time_stamp or user_id
		//	are the last columns on the table.
		goto skip_insert_column
	end if
	
	sqls = sqls + '"' + column + '", '
	
	skip_insert_column:
	
	if i = num_columns then
		sqls = mid(sqls,1,len(sqls) - 2) + ') '
	end if
	
next 

////****************************************************************************
////	Build the select
////		- Table aliases will not be used!!  a_join must not contain aliases.
////****************************************************************************

sqls = sqls + " select "

for i = 1 to num_columns
	
	column = ds_columns.getitemstring(i,1)
	
	if column = 'TIME_STAMP' or column = 'USER_ID' or column = 'DRILLDOWN_KEY' then 
		//	goto versus a continue just in case the time_stamp or user_id
		//	are the last columns on the table.
		goto skip_select_column
	end if
	
	if column = i_amount_field then
		sqls = sqls + 'round(' + a_table_name + '.' + i_amount_field + ' * cr_deriver_control.percent * cr_sco_billing_type_rates.rate,2) , '
		goto skip_select_column
	end if
	// DML: 051011: 7529: Capture the ID and Percent
	if column = i_id_field then
		sqls = sqls + a_table_name + ".ID , "
		goto skip_select_column
	end if
	// DML: 051011: 7529: Capture the ID and Percent
	if column = i_rate_field then
		sqls = sqls + "round(cr_deriver_control.percent * cr_sco_billing_type_rates.rate,2) , "
		goto skip_select_column
	end if
	
	//	QUESTION: how do i handle the primary key field?
	//		punt for now...assume "ID" as if it is a source table.
//	if column = 'ID' then
//		sqls = sqls + 'crdetail.nextval, '
//		goto skip_select_column
//	end if
	if column = 'ID' then
		if upper(a_table_name) = "CR_BUDGET_DATA" or upper(a_table_name) = "CR_BUDGET_DATA_TEST" or &
			upper(a_table_name) = "CR_BUDGET_REVERSALS" or upper(a_table_name) = "CR_BUDGET_DATA_TEMP" &
		then
			sqls = sqls + 'crbudgets.nextval, '
		else
			sqls = sqls + 'crdetail.nextval, '
		end if
		goto skip_select_column
	end if
	
	//	Check if this column is an accounting key element.
	//	If yes, pull from cr_deriver_control
	//		However, if the column is a '*' then pull from a_table_name.
	//	If no,  pull from a_table_name
	counter = 0
	select count(*) into :counter
	from cr_elements
	where upper(replace(replace(replace(description,'-','_'),'/','_'),' ','_')) = 
		:column;
		
	if isnull(counter) then counter = 0
	
	if counter = 1	then // pull from cr_deriver_control (unless it is a '*')
		sqls = sqls + 'decode(nvl(cr_deriver_control."' + column + '", '
		sqls = sqls + "' '), "
		sqls = sqls + "'*',"
		sqls = sqls + a_table_name + '."' + column + '",nvl(cr_deriver_control."' + column + '" '
		sqls = sqls + ",' ')),  "
	else	// pull from a_table
		sqls = sqls + a_table_name + '."' + column + '", '
	end if
	
	skip_select_column:
	
	if i = num_columns then
		sqls = mid(sqls,1,len(sqls) - 2)
	end if
	
next 

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	DO I NEED AN ARGUMENT FOR INTERFACE_BATCH_ID OR WILL IT BE INCLUDED IN A_JOIN?
//		- INCLUDE IT IN A_JOIN FOR NOW...
//
//	A_TYPE IS INCLUDED IN THE WHERE CLAUSE SEPARATELY (AND NOT PART OF A_JOIN) 
//	SINCE THAT COLUMN IS PART OF CR_DERIVER_CONTROL AND NOT PART OF A_TABLE.
//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//	a_join should only include a join between a_table_name and cr_deriver_control
//	if this function is called it should be expected that cr_sco_billing_type_rates
//	will be added to the where clause.

uf_system_control('COMPANY FIELD')

//sqls = sqls + " from " + a_table_name + ", cr_deriver_control, cr_sco_billing_type_rates  " + &
//	" where " + a_join + " and cr_deriver_control."
//sqls = sqls + '"TYPE" = ' 
//sqls = sqls + "'" + a_type + "' and cr_sco_billing_type_rates.sco_billing_type_id = " + &
//	"cr_deriver_control.sco_billing_type_id and cr_deriver_control." + &
//	i_co_field + " = cr_sco_billing_type_rates." + i_co_field

sqls = sqls + " from " + a_table_name + ", cr_deriver_control, " + &
	" (select * from cr_sco_billing_type_rates " + &
	" 	where (sco_billing_type_id," + i_co_field + ",effective_month_number) in ( " + &
	" 		select sco_billing_type_id," + i_co_field + ",max(effective_month_number) effective_month_number " + &
	" 		from cr_sco_billing_type_rates where effective_month_number <= " + string(sco_effective_month) + &
	" 		group by sco_billing_type_id," + i_co_field + ")) cr_sco_billing_type_rates " + &
	" where " + a_join + " and cr_deriver_control."
sqls = sqls + '"TYPE" = ' 
sqls = sqls + "'" + a_type + "' and cr_sco_billing_type_rates.sco_billing_type_id = " + &
	"cr_deriver_control.sco_billing_type_id and cr_deriver_control." + &
	i_co_field + " = cr_sco_billing_type_rates." + i_co_field
	
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Inserting into " + a_table_name + " : " + sqlca.sqlerrtext )
	f_write_log(g_log_file,sqls)
	rollback;
	return -1
else
	commit;
end if

return 1
end function

public function longlong uf_deriver_update (string a_type, string a_table_name, string a_join_field, string a_ifb_id, boolean a_visual);//*****************************************************************************************
//
//  Function  :  uf_deriver_update
//
//  Description  :  THIS FUNCTION IS THE BASIC DERIVATION UPDATE FUNCTION.  IT WILL GET
//                  CALLED WITH THE TYPE OF DERIVATION (a_type) and A FIELD THAT WILL BE USED
//                  FOR THE JOINING STRING.  THIS FUNCTION CREATES AN ARRAY FROM THAT FIELD
//                  AND THEN CALLS A FUNCTION THAT PERFORMS THE WORK.
//
//                  CALL THIS FUNCTION WHEN NO NEW TRANSACTIONS ARE TO BE CREATED AND YOU
//                  JUST WANT TO UPDATE THEM BASED ON THE VALUE OF A SINGLE COLUMN FROM 
//                  A_TABLE_NAME.
//
//  Notes:
//  ------
//  CALL THIS FUNCTION WHEN THE UPDATE IS A "SINGLE-COLUMN" UPDATE.  UPDATES THAT REQUIRE
//  A CONCATENATION OF COLUMNS (E.G. WO AND TASK) SHOULD CALL UF_DERIVER_UPDATE_MULT.
//
//  THE OLD UF_DERIVER_UPDATE CODE (PINNACLE) HAS BEEN MOVED TO 
//  UF_DERIVER_UPDATE_MULT.
//
//*****************************************************************************************
string a[1]

a[1] = a_join_field

return uf_deriver_update_mult(a_type, a_table_name, a, a_ifb_id, a_visual, '')
end function

public function longlong uf_deriver_rounding_error (string a_type, string a_table_name, string a_join_original, string a_join_target, string a_identify_target, string a_table_name_field);//*****************************************************************************************
//
//  Function     :  uf_deriver_rounding_error
//
//  Description  :  This function aligns with the functionality in uf_deriver and
//                  uf_deriver_with_src_table.  Those 2 functions are used to split
//                  "orignal" transactions into 1 or more records.  When splitting into
//                  more than one target record, there exists the possibility for rounding
//                  error.  This function trues up the target results in the case of
//                  rounding error.
//
//  Args         :  a_type  =  The "TYPE" field on cr_deriver_control.  Should match the
//                             a_type that was used in the call to uf_deriver.
//
//                  a_table_name  =  The table being updated.  Should match the
//                                   a_table_name used in uf_deriver.
//
//                  a_join_original  =  The join (and any other pertinent where clause)
//                                      used to join a_table_name to cr_deriver_control to
//                                      determine the original dollar amounts.  Should be
//                                      similar to the a_join_string used in uf_deriver.
//
//                  a_join_target  =  The join (and any other pertinent where clause)
//                                    used to join a_table_name to cr_deriver_control to
//                                    determine the target dollar amounts.  Should be
//                                    similar to the a_join_string used in uf_deriver.
//
//                  a_identify_target  =  Similar to the a_join_target.  This is the
//                                        where clause that identifies the target records
//                                        in a_table_name.  Used in the rounding update.
//                                        This is probably a_join_target without the actual
//                                        join between a_table_name and cr_deriver_control.
//
//                  a_table_name_field  =  The "field" from the transaction table being
//                                         joined to cr_deriver_control."STRING".  This
//                                         can be a concatenation of fields.
//
//  Notes:
//  ------
//  1)  In the joins and the a_table_name_field passed to this function, the caller must
//      include table prefixes (e.g. cr_accounts_payable.work_order).  I could have
//      appended the a_table_name onto the front of many of these variables, but I wanted
//      the calling methodology to be the same in all functions and args in this object.
//
//  2)  This creates SQL of the form:
//
//        update cr_accounts_payable set AMOUNT = AMOUNT + 0.01
//         where cr_accounts_payable.cr_txn_type = 'TARGET' 
//           and cr_accounts_payable.work_order = 'Z1199'
//           and cr_accounts_payable.id = 
//             (select min(id) from cr_accounts_payable x 
//               where x.work_order = 'Z1199' and x.cr_txn_type = 'TARGET')
//
//  ***  DMJ: NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
longlong num_rows, i, dot_pos, rtn
// SEK 082410: Changing tbl_id just in case (though this sequence
//		will most likely never get above 2147483647).
longlong tbl_id
string      sqls, string_val, orig_sqls, targ_sqls, x_identify_target, &
				x_table_name_field
decimal {2} orig_amount, targ_amount, diff


//
//  Determine the "deriver" table.  If the instance variable is null, then
//  use cr_deriver_control.
//
if isnull(i_deriver_table) or trim(i_deriver_table) = "" then
	i_deriver_table = "cr_deriver_control"
end if


//
//  Setup.
//
uf_system_control('DERIVATIONS : AMOUNT FIELD')
uf_system_control('COMPANY FIELD')
uf_system_control('DERIVATIONS - ADDL ROUNDING')
// ### 7595: JAK: 2011-05-17:  New system control that will logically determine the record to plug the OOB
uf_system_control('DERIVATION: ROUNDING PRIORITY')

//  The a_identify_target arg looks like:
//    cr_accounts_payable_stg.cr_txn_type = 'TARGET'
//  It needs to look like:
//    x.cr_txn_type = 'TARGET'
//  for the actual rounding update (sub-select in where clause for min(id))
//
//  Ditto for the a_table_name_field arg (looks like "cr_accounts_payable_stg.work_order").
//  Note that this method also deals with concatenated fields.
x_identify_target  = f_replace_string(a_identify_target,  a_table_name + ".", "x.", "all")
x_table_name_field = f_replace_string(a_table_name_field, a_table_name + ".", "x.", "all")

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** x_identify_target = " + x_identify_target)
	f_pp_msgs("***DEBUG*** x_table_name_field = " + x_table_name_field)
end if


//
//  Identify any OOB "string" values.
//
//NO LONGER NEEDED DUE TO I VAR: ds_oob_strings = CREATE datastore

sqls = &
	'select ' + i_co_field + '||' + i_addl_rounding_field + '||' + i_deriver_table + '."STRING", sum(' + a_table_name + ".amount) " + &
	  "from " + a_table_name + ", " + &
		'(select distinct "STRING" from ' + i_deriver_table + ' ' + &
		  'where "TYPE" = ' + " '" + a_type + "') " + i_deriver_table + " " + &
	 "where " + a_join_original + " " + &
	 'group by ' + i_co_field + '||' + i_addl_rounding_field + '||' + i_deriver_table + '."STRING" ' + &
	 "minus " + &
	 'select ' + i_co_field + '||' + i_addl_rounding_field + '||' + i_deriver_table + '."STRING", sum(' + a_table_name + ".amount) " + &
		"from " + a_table_name + ", " + &
		'(select distinct "STRING" from ' + i_deriver_table + ' ' + &
		  'where "TYPE" = ' + " '" + a_type + "') " + i_deriver_table + " " + &
	 "where " + a_join_target + " " + &
	 'group by ' + i_co_field + '||' + i_addl_rounding_field + '||' + i_deriver_table + '."STRING"'

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** i_ds_oob_strings sqls = " + sqls)
end if

//NO LONGER NEEDED DUE TO I VAR: f_create_dynamic_ds(i_ds_oob_strings, "grid", sqls, sqlca, true)
rtn = i_ds_oob_strings.SetSQLSelect(sqls)

if rtn < 0 then
	// Retrieve failed
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: SetSQLSelect failed: " + i_ds_oob_strings_ids.i_sqlca_sqlerrtext)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	return -1
end if

rtn = i_ds_oob_strings.RETRIEVE()

if rtn < 0 then
	// Retrieve failed
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieve failed: " + i_ds_oob_strings_ids.i_sqlca_sqlerrtext)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	return -1
end if

num_rows = i_ds_oob_strings.RowCount()


//
//  Loop over any OOB "string" values and apply the rounding plug.
//
//NO LONGER NEEDED DUE TO I VAR: ds_orig_amount = CREATE datastore
//NO LONGER NEEDED DUE TO I VAR: ds_targ_amount = CREATE datastore

for i = 1 to num_rows
	
	string_val = i_ds_oob_strings.GetItemString(i, 1)
	
	//
	//  What is the original amount ?
	//
	orig_sqls = &
		'select ' + i_deriver_table + '."STRING", sum(' + a_table_name + ".amount) " + &
		  "from " + a_table_name + ", " + &
			'(select distinct "STRING" from ' + i_deriver_table + ' ' + &
			  'where "TYPE" = ' + " '" + a_type + "') " + i_deriver_table + " " + &
		 "where " + a_join_original + " " + &
		 "and " + i_co_field + '||' + i_addl_rounding_field + '||' + a_table_name_field + " = '" + string_val + "' " + &
		 'group by ' + i_deriver_table + '."STRING"'
	
	//
	//  What is the target amount ?
	//
	targ_sqls = &
		'select ' + i_deriver_table + '."STRING", sum(' + a_table_name + ".amount) " + &
		  "from " + a_table_name + ", " + &
			'(select distinct "STRING" from ' + i_deriver_table + ' ' + &
			  'where "TYPE" = ' + " '" + a_type + "') " + i_deriver_table + " " + &
		 "where " + a_join_target + " " + &
		 "and " + i_co_field + '||' + i_addl_rounding_field + '||' + a_table_name_field + " = '" + string_val + "' " + &
		 'group by ' + i_deriver_table + '."STRING"'
	
	if i = 1 then
		//NO LONGER NEEDED DUE TO I VAR: f_create_dynamic_ds(ds_orig_amount, "grid", orig_sqls, sqlca, true)
		//NO LONGER NEEDED DUE TO I VAR: f_create_dynamic_ds(ds_targ_amount, "grid", targ_sqls, sqlca, true)
		i_ds_orig_amount.SetSQLSelect(orig_sqls)
		i_ds_targ_amount.SetSQLSelect(targ_sqls)
		i_ds_orig_amount.RETRIEVE()
		i_ds_targ_amount.RETRIEVE()
	else
		i_ds_orig_amount.SetSQLSelect(orig_sqls)
		i_ds_targ_amount.SetSQLSelect(targ_sqls)
		i_ds_orig_amount.RETRIEVE()
		i_ds_targ_amount.RETRIEVE()
	end if
	
	//
	//  If we are in this loop, this "STRING" has rounding error.  If the "amount" datastores
	//  come back with no records, we have a problem.
	//
	if i_ds_orig_amount.RowCount() = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: i_ds_orig_amount returned 0 records in rounding adjustment " + &
			"for string: " + string_val)
		f_pp_msgs("orig_sqls = " + orig_sqls)
		f_pp_msgs("  ")
		//
		// ### DMJ: MAINT 5631: 10/12/10
		// IF CR_BUDGET_DATA, DO NOT CREATE THE TABLE (TOO BIG)
		//
		if upper(a_table_name) = "CR_BUDGET_DATA" then
			//  Do nothing ...
		else
			select crmaint.nextval into :tbl_id from dual;
			if isnull(tbl_id) then tbl_id = 0
			sqls = "create table crdround_" + string(tbl_id) + " as select * from " + a_table_name
			execute immediate :sqls;
		end if
		
		return -1
	end if
	
	if i_ds_targ_amount.RowCount() = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: i_ds_targ_amount returned 0 records in rounding adjustment " + &
			"for string: " + string_val)
		f_pp_msgs("targ_sqls = " + targ_sqls)
		f_pp_msgs("  ")
		//
		// ### DMJ: MAINT 5631: 10/12/10
		// IF CR_BUDGET_DATA, DO NOT CREATE THE TABLE (TOO BIG)
		//
		if upper(a_table_name) = "CR_BUDGET_DATA" then
			//  Do nothing ...
		else
			select crmaint.nextval into :tbl_id from dual;
			if isnull(tbl_id) then tbl_id = 0
			sqls = "create table crdround_" + string(tbl_id) + " as select * from " + a_table_name
			execute immediate :sqls;
		end if
		
		return -1
	end if
	
	orig_amount = i_ds_orig_amount.GetItemDecimal(1, 2)
	targ_amount = i_ds_targ_amount.GetItemDecimal(1, 2)
	diff        = orig_amount - targ_amount
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** i = " + string(i))
		f_pp_msgs("***DEBUG*** ----------------")
		f_pp_msgs("***DEBUG*** orig_amount = " + string(orig_amount))
		f_pp_msgs("***DEBUG*** targ_amount = " + string(targ_amount))
	end if
	


	//
	//  Plug the record with the min(id)
	//
	sqls = &
		"update " + a_table_name + &
		  " set " + i_amount_field + " = " + i_amount_field + " + " + string(diff) + " " + &
		 "where " + a_identify_target + " " + &
			"and " + i_co_field + '||' + i_addl_rounding_field + '||' + a_table_name_field + " = '" + string_val + "' " + &
			"and " + a_table_name + ".id = " + &
				"(select min(id) "
	
	if i_rounding_order_by = 'none' then 
		// Use the old method ... no extra SQL here	
	else 
		// Use the ordered method.
		sqls += "KEEP (DENSE_RANK first ORDER BY " + i_rounding_order_by + ") "
	end if

	sqls +=						" from " + a_table_name + " x " + &
				  "where " + i_co_field + '||' + i_addl_rounding_field + '||' + x_table_name_field + " = '" + string_val + "' " + &
					 "and " + x_identify_target + ")"
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** update sqls = " + sqls)
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating " + a_table_name + " with rounding adjustment " + &
			"for string: " + string_val)
		f_pp_msgs("ERROR = " + sqlca.SQLErrText)
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		return -1
	end if
	
	//
	//  If we are in this loop, this "STRING" has rounding error.  If the "amount" datastores
	//  come back with no records, we have a problem.
	//
	if sqlca.SQLNRows = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: NO ROWS UPDATED: updating " + a_table_name + " with rounding adjustment " + &
			"for string: " + string_val)
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		return -1
	end if
	
next


//NO LONGER NEEDED DUE TO I VAR: DESTROY ds_oob_strings
//NO LONGER NEEDED DUE TO I VAR: DESTROY ds_orig_amount
//NO LONGER NEEDED DUE TO I VAR: DESTROY ds_targ_amount

return 1

end function

public function longlong uf_deriver_audits (string a_type, string a_audit_name);//*****************************************************************************************
//
//  Function     :  uf_deriver_audits
//
//  Description  :  Performs certain standard audits against cr_deriver_control records to
//                  ensure data integrity.
//
//  Returns      :  -1 if the audit fails, 1 if successful
//
//  Notes        :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION.  THE CALLER MAY BE TRYING
//                  TO MANAGE TRANSACTIONS.
//
//                  No messages, only return variables.  Messages are the responsibility
//                  of the caller.
//
//*****************************************************************************************
longlong counter


counter   = 0
i_counter = 0


//
//  Negative percentages in the derivation splits may not be desirable (especially if the
//  normalization switch is turned on).  This audit ensures that a given "TYPE" does not
//  contain any negative percentages.
//
if upper(a_audit_name) = "DISALLOW NEGATIVE PERCENTS" then
	
	select count(*) into :counter from cr_deriver_control
	 where "TYPE" = :a_type and percent < 0;
	if isnull(counter) then counter = 0
	i_counter = counter
	
	if counter > 0 then return -1
	
end if


//
//  If "default" types are set up for those "STRING" values that do not have a record in
//  cr_deriver_control (e.g. assign any work orders that have no derivation record to
//  100% capital), then it could be disasterous if there is more than one record in this
//  "TYPE" (since we generaly don't join the STG table to cr_deriver_control for this
//  case).  This audit ensures that the default "TYPE" has only 1 record.  This audit
//  should be called once to the target default type and once for the offset default type.
//
//  DMJ: 3/28/07: Changed to be "counter <> 1" since having 0 default records is just as
//  disasterous.  You will get an OOB error in the interface.
//
if upper(a_audit_name) = "SINGLE RECORD FOR DEFAULT TYPE" then
	
	select count(*) into :counter from cr_deriver_control
	 where "TYPE" = :a_type;
	if isnull(counter) then counter = 0
	i_counter = counter
	
	if counter <> 1 then return -1
	
end if


//
//  In many cases, the offsets for the derivations may be back to a single accounting key
//  distribution per "STRING".  This audit ensures that we do not have multiple records for
//  any given "STRING" within a "TYPE".
//
if upper(a_audit_name) = "SINGLE RECORD FOR OFFSETS" then
	
	select count(*) into :counter from (
	select "STRING", count(*)
	  from cr_deriver_control where "TYPE" = :a_type
	 group by "STRING" having count(*) <> 1);
	if isnull(counter) then counter = 0
	i_counter = counter
	
	if counter > 0 then return -1
	 
end if


return 1
end function

public function longlong uf_deriver_create_offset (string a_type_original, string a_type_offset, longlong a_substr_start, longlong a_substr_length);//*****************************************************************************************
//
//  Function  :  uf_deriver_create_offset
//
//  Description  :  Creates cr_deriver_control records of a certain "TYPE" with a
//                  percent = -1 for strings that are missing based on another type.
//                  For example, if there are 100 string values in the "This Derivation"
//                  type, and only 75 strings in the "This Derivation - Offset" type,
//                  this function will insert the missing 25 records, so derivations
//                  do not create an OOB condition.
//
//  ***  DMJ: NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
longlong source_id, num_elements, i, num_columns, counter, loop_limit
string sqls, column, msg_str, addl_minus_columns
boolean add_wo

addl_minus_columns = ""

//
//  Determine the "deriver" table.  If the instance variable is null, then
//  use cr_deriver_control.
//
if isnull(i_deriver_table) or trim(i_deriver_table) = "" then
	i_deriver_table = "cr_deriver_control"
end if

// If WORK_ORDER_ID is on the derivation table, then it needs to be copied to the offset
select count(*) into :counter
	from all_tab_columns
	where owner = 'PWRPLANT'
	and table_name = upper(:i_deriver_table)
	and column_name = 'WORK_ORDER_ID';
	
if counter > 0 then
	add_wo = true
else
	add_wo = false
end if


//NO LONGER NEEDED DUE TO I VAR: uo_ds_top ds_columns
//NO LONGER NEEDED DUE TO I VAR: ds_columns = create uo_ds_top

//	Use all_tab_columns to get the column names instead of cr_sources_fields
//		in case we perform derivations against a table that is not a source.
//		For instance...if we need to run derivations against a "pre-stage" 
//		client table before loading the CR.
//		This function has a "table_name" argument instead of a source_id argument.

//	I assume the table being derived against will have the accounting key.

sqls = "select column_name, column_id from sys.all_tab_columns " + &
	"where owner = 'PWRPLANT' and table_name = '" + upper(trim(i_deriver_table)) + "'"

//NO LONGER NEEDED DUE TO I VAR: f_create_dynamic_ds(ds_columns, "grid", sqls, sqlca, true)
i_ds_columns.SetSQLSelect(sqls)
i_ds_columns.RETRIEVE()
num_columns = i_ds_columns.rowcount()

//	Sort the datastore by the column_id to build the insert in the order the
//		columns are on the table.
i_ds_columns.setsort("#2 A")
i_ds_columns.sort()

sqls = " insert into " + i_deriver_table + " ( "

//	Build the insert

for i = 1 to num_columns
	
	column = upper(i_ds_columns.getitemstring(i,1))
	
	choose case column
		case "TIME_STAMP", "USER_ID", "VALIDATION_MESSAGE" 
			//	goto versus a continue just in case the time_stamp or user_id
			//	are the last columns on the table.
			goto skip_insert_column
	end choose
	
	sqls = sqls + '"' + column + '", '
	
	skip_insert_column:
	
	if i = num_columns then
		sqls = mid(sqls,1,len(sqls) - 2) + ') '
	end if	
next 

//****************************************************************************
//	Build the select
//		- Table aliases will not be used!!  a_join must not contain aliases.
//****************************************************************************

sqls = sqls + " select "

for i = 1 to num_columns
	
	column = i_ds_columns.getitemstring(i,1)
	
	//
	//  "IF" statements for hardcoded fields.
	//
	//  Goto versus a continue just in case the time_stamp or user_id are the
	//  last columns on the table.  Note that this code was borrowed from another
	//  function and could have been coded differently from scratch.
	if column = "ID" then
		sqls = sqls + "crdetail.nextval, "
		goto skip_select_column
	end if
	
	choose case column
		case "TIME_STAMP", "USER_ID", "VALIDATION_MESSAGE"
			goto skip_select_column
	end choose
	
	if column = "TYPE" then
		sqls = sqls + "'" + a_type_offset + "' , "
		goto skip_select_column
	end if
	
	if column = "PERCENT" then
		sqls = sqls + "-1 , "
		goto skip_select_column
	end if
	
	if column = "DESCRIPTION" then
		sqls = sqls + "'Created by uf_deriver_create_offset' , "
		goto skip_select_column
	end if
	
	//
	//  Code to handle all non-hardcoded fields and ACK fields.
	//
	
	//	 Is this an accounting key element ?  If so, we will hardcode the masking.
	//  If not, just pull the existing value from the minus against cr_deriver_control.
	counter = 0
	select count(*) into :counter	from cr_elements
	 where upper(replace(replace(replace(
	 	description,'-','_'),'/','_'),' ','_')) = :column;
	if isnull(counter) then counter = 0
	
	if counter = 1	then
		//  ACK fields always get masked since these are offset derivation records.
		sqls = sqls + "'*', "
		goto skip_select_column
	else
		//  Pull from the minus against cr_deriver_control.
		//sqls = sqls + '"' + column + '", '  // THIS IS NOW HANDLED BELOW
		// SINCE WE ARE EVALUATING DETAILED ATTRIBUTES NOW !!!
	end if
	
	//	 Is this an detailed attribute ?  If so, we will include the field name and
	//  include it in the minus below.
	counter = 0
	select count(*) into :counter	from cr_deriver_additional_fields
	 where upper(replace(replace(replace(
	 	description,'-','_'),'/','_'),' ','_')) = :column;
	if isnull(counter) then counter = 0
	
	if counter = 1	then
		//  Pull from the minus against cr_deriver_control.
		sqls = sqls + '"' + column + '", '
		addl_minus_columns = addl_minus_columns + ", " + column
	else
		//  Also pull from the minus ... we have sco_billing_type_id that is
		//  hardcoded on the table.  We have two blocks here since this column
		//  was originally hardcoded in the minus below.
		sqls = sqls + '"' + column + '", '
		//  NOT AN ADDL_MINUS_COLUMN SINCE IT IS HARDCODED BELOW !!!
	end if
	
	skip_select_column:
	
	if i = num_columns then
		sqls = mid(sqls,1,len(sqls) - 2)
	end if
	
next 

sqls = sqls + " from ( "

//  DMJ: COMMENTED OUT JAK CODE SINCE WE ADDED DETAILED ATTRIBUTE CODE ABOVE THAT
//  WILL HANDLE WORK_ORDER_ID AND ALL OTHER DETAILED ATTRIBUTES.
if a_substr_start = 0 then
	//// JAK -- added work order id here
	//if add_wo then
	//	sqls = sqls + &
	//		'select "STRING", sco_billing_type_id, work_order_id ' + &
	//		  "from " + i_deriver_table + " " + &
	//		 'where "TYPE" = ' + "'" + a_type_original + "' " + &
	//		 "minus " + &
	//		'select "STRING", sco_billing_type_id, work_order_id ' + &
	//		  "from " + i_deriver_table + " " + &
	//		 'where "TYPE" = ' + "'" + a_type_offset + "')"
	//else
		sqls = sqls + &
			'select "STRING", sco_billing_type_id ' + addl_minus_columns + " " + &
			  "from " + i_deriver_table + " " + &
			 'where "TYPE" = ' + "'" + a_type_original + "' " + &
			 "minus " + &
			'select "STRING", sco_billing_type_id ' + addl_minus_columns + " " + &
			  "from " + i_deriver_table + " " + &
			 'where "TYPE" = ' + "'" + a_type_offset + "')"
	//end if
else
	//  Using the substr() option implies that offsets are being created at the work order
	//  level of detail.  Since clients can have varying lengths for their work order numbers
	//  (e.g. perhaps order are 7 characters and wbs elements are 12), this function would end
	//  up being called twice ... with each call creating a subset of offset records that
	//  are incorrect (i.e. the 1 to 7 of a wbs in this example would be meaningless).  Thus,
	//  in the calling script, a delete command should be executed immediately after this call
	//  to remove any cr_deriver_control offset records where the "STRING" is not really a
	//  work order.
	//if add_wo then
	//	sqls = sqls + &
	//		'select substr("STRING", ' + string(a_substr_start) + ', ' + string(a_substr_length) + ') as "STRING", ' + &
	//		'sco_billing_type_id, work_order_id ' + &
	//		"from " + i_deriver_table + " " + &
	//		'where "TYPE" = ' + "'" + a_type_original + "' " + &
	//		"minus " + &
	//		'select "STRING", sco_billing_type_id, work_order_id ' + &
	//		"from " + i_deriver_table + " " + &
	//		'where "TYPE" = ' + "'" + a_type_offset + "')"
	//else
		sqls = sqls + &
			'select substr("STRING", ' + string(a_substr_start) + ', ' + string(a_substr_length) + ') as "STRING", ' + &
					 'sco_billing_type_id ' + addl_minus_columns + " " + &
			  "from " + i_deriver_table + " " + &
			 'where "TYPE" = ' + "'" + a_type_original + "' " + &
			 "minus " + &
			'select "STRING", sco_billing_type_id ' + addl_minus_columns + " " + &
			  "from " + i_deriver_table + " " + &
			 'where "TYPE" = ' + "'" + a_type_offset + "')"
	//end if
end if

if i_debug = "YES" then
	//  This string could go over the 2000 character field.
	loop_limit = len(sqls) / 2000
	if mod(len(sqls), 2000) <> 0 then
		loop_limit++
	end if
	for i = 1 to loop_limit
		msg_str = mid(sqls, 2000 * (i - 1) + 1, 2000)
		if i = 1 then f_pp_msgs("***DEBUG*** sqls = ")
		f_pp_msgs(msg_str)
	next
end if

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: In uf_deriver_create_offset: " + sqlca.SQLErrText )
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	return -1
end if

return 1
end function

public function longlong uf_deriver_virtual (string a_type, string a_table_name, string a_string, string a_non_masked_fields, string a_filter1, string a_join1, string a_type_offset, string a_join2);//*****************************************************************************************
//
//  Function  :  uf_deriver_virtual
//
//  Description  :  VIRTUAL DERIVATION FUNCTION THAT USES CALCULATED DERIVATION
//                  PERCENTAGES.
//
//  Methodology:
//  ------------
//  1)  Insert the calculated derivation percentages into cr_deriver_control_virtual.
//      This is a global temporary table.
//  2)  Call uf_deriver_create_offset to create the offsets in cr_deriver_control_virtual.
//  3)  Call uf_deriver to perform the derivation.
//
//  Notes:
//  ------
//  There is no call to the rounding error function here.  The number of args required
//  for this function - if we called the rounding function - would get ridiculous.  My
//  approach to rounding will be for the caller to call uf_deriver_rounding_error after
//  calling this function.  In an interface this might mean:
//    1 - Call all the deriver stuff for the "normal" transactions.
//    2 - Call uf_deriver_rounding_error since you'd want the "normal" stuff to have its
//        rounding cleaned up before using those results as part of a virtual derivation.
//        I realize this is splitting hairs, but the consequence is potentially having to
//        defined slight variations in the virtual derivations vs. the end results of the
//        "normal" stuff.  Pick your poison.
//    3 - Call this function to perform a virtual derivation.
//    4 - Call uf_deriver_rounding_error to clean up the rounding in the virtual derivation
//        results.
//  THE CALLER MUST SET THE i_deriver_table variable to cr_deriver_control_virtual and
//  then null it out after the call to uf_deriver_rounding_error (see the technique
//  below).  THE CALLER MUST ALSO truncate the cr_deriver_control_virtual if they feel
//  that the records might get in the way of a subsequent virtual derivation.  Otherwise,
//  those records will simply evaporate when the session ends (cr_deriver_control_virtual
//  is a global temp table with "on commit preserve rows").
//
//  Also, to stay within the spirit of the derivations, there is no code in this function
//  to set cr_txn_type = 'TARGET'.  Remember, the derivations were designed to work against
//  non-CR tables if needed (e.g. staging tables).  The caller is responsible for setting
//  cr_txn_type = 'TARGET', just like interfaces do when calling uf_deriver().  Note that
//  if this step is skipped in the caller, the rounding function will have no way to 
//  identify the targets that need to be checked against the original transactions.
//
//  ***  DMJ: NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
longlong num_elements, i, ii, rtn, loop_limit
string sqls, elements_sqls, element, insert_sqls, elements_sqls_for_select, &
	non_masked_fields_array[], array_value, a_string_array[], msg_str
boolean masked


//  -----------------------------------  0)  SETUP  ---------------------------------------


//  Parse out the non-masked fields.  These fields will be referenced in the select
//  instead of inserting '*'.  You would think that the account field would always
//  be one of these.
f_parsestringintostringarray(a_non_masked_fields, ",", non_masked_fields_array)

//  Parse out the a_string variable on the || ... array values beginning with "'" are
//  ignored in the group by while the others are included due to the scalar sub-select.
//  E.G. work_order||'CIAC'
f_parsestringintostringarray(a_string, "||", a_string_array)



//  --------------------  1)  INSERT INTO CR_DERIVER_CONTROL_VIRTUAL  ---------------------
//
//  --------------------      This step creates SQL of the form:      --------------------
//
//insert into cr_deriver_control_virtual (
//	id, "TYPE", "STRING", company, business_area, cost_element, work_order, funding_project,
//	ferc, cost_center, posting_order, posting_cost_center, assignment, percent, description) (
//select 0 as id, 'SAP Ferc Derivation' as "TYPE", work_order||'CIAC' as "STRING",
//		 '*' as company, '*' as business_area, '*' as cost_element, '*' as work_order,
//		 '*' as funding_project, ferc, '*' as cost_center, '*' as posting_order,
//		 '*' as posting_cost_center, '*' as assignment,
//		 round(sum(amount) / (select sum(amount) from cr_accounts_payable_stg aa
//		                       where aa.work_order = a.work_order 
//									    and CR_TXN_TYPE = 'TARGET'), 8) as percent,
//		 'Virtual Derivation' as description
//  from cr_accounts_payable_stg a
// where cr_txn_type = 'TARGET'
// group by work_order, ferc, work_order
//having round(sum(amount) / (select sum(amount) from cr_accounts_payable_stg aa
//                             where aa.work_order = a.work_order
//									    and cr_txn_type = 'TARGET'), 8) <> 0)


f_pp_msgs("-- Calculating percentages at " + string(now()))

//  Build the accounting key fields for the insert and select.

//NO LONGER NEEDED DUE TO I VAR: 
//(AND NO NEED TO RE-RETRIEVE SINCE IT IS JUST SELECT * FROM CR_ELEMENTS)
//datastore ds_elements
//ds_elements = CREATE datastore
//sqls = "select * from cr_elements"
//f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)
//ds_elements.SetSort("order a")
//ds_elements.Sort()

num_elements = i_ds_elements.RowCount()
elements_sqls = ""
elements_sqls_for_select = ""

for i = 1 to num_elements
	element = upper(trim(i_ds_elements.GetItemString(i, "description")))
	element = f_cr_clean_string(element)
	elements_sqls = elements_sqls + '"' + element + '", '
	//  Need to determine if this ACK field is masked or not to determine its representation
	//  in the select statement.
	masked = true
	for ii = 1 to upperbound(non_masked_fields_array)
		array_value = upper(trim(non_masked_fields_array[ii]))
		array_value = f_cr_clean_string(array_value)
		if array_value = element then
			//  Not masked.
			masked = false
			exit
		else
			//  Masked.  Variable is already true.
		end if
	next
	if masked then
		elements_sqls_for_select = elements_sqls_for_select + "'*' as " + '"' + element + '", '
	else
		elements_sqls_for_select = elements_sqls_for_select + '"' + element + '", '
	end if
next


//  Build the front of the insert.
insert_sqls = 'insert into cr_deriver_control_virtual (id, "TYPE", "STRING", '

insert_sqls = insert_sqls + elements_sqls

insert_sqls = insert_sqls + "percent, description) ("


//  Build the select portion of the insert command.
insert_sqls = insert_sqls + "select 0 as id,"

insert_sqls = insert_sqls + "'" + a_type + "' as " + '"TYPE", '

insert_sqls = insert_sqls + a_string + ' as "STRING", '  //  Note that this needs
	// to look something like work_order||'CIAC'.  E.G. the work order field + the
	// cr_derivation_rollup value that is being derived using the virtual function.

insert_sqls = insert_sqls + elements_sqls_for_select  //  This adds the '*' part
	// of the select statement.  E.G. '*' as company, '*' as cost_element, etc.

insert_sqls = insert_sqls + &
	"round(sum(amount) " + &
	"/ " + &
	"(select sum(amount) from " + a_table_name + " aa " + &
	  "where " + a_join1 + " and " + a_filter1 + "), 8) as percent, "  //  The a_join1 
	// will generally look something like: aa.work_order = a.work_order, but might
	// need to include company or even references to cost elements in certain scenarios.
	// prefixes must be "a." and "aa." since I'm referencing the same table twice.
	// The a_filter1 arg will generally look something like: cr_txn_type = 'TARGET' and 
	// cr_derivation_rollup in (select cr_derivation_rollup from cr_derivation_rollup
	// where basis_for_virtual_derivation = 1) ... to limit the calculation to only 'TARGET'
	// results to this point and only cr_derivation_rollup values that are defined as
	// being included in the basis.

insert_sqls = insert_sqls + "'Virtual Derivation' as description "

insert_sqls = insert_sqls + "from " + a_table_name + " a "

insert_sqls = insert_sqls + "where " + a_filter1 + " "  //  How do we limit ourselves to only
	// the results of all prior derivations?  This will generally look like:
	// cr_txn_type = 'TARGET', but is flexible enough to work against other table is needed.
	// if for some reason there is no a_filter1, use "1=1" or "id > 0".

//  Add the group by ... it will be all elements in both arrays ... any duplication
//  should not matter.
insert_sqls = insert_sqls + "group by "

for ii = 1 to upperbound(a_string_array)
	array_value = upper(trim(a_string_array[ii]))
	array_value = f_cr_clean_string(array_value)
	if left(array_value, 1) = "'" then continue
	insert_sqls = insert_sqls + '"' + array_value + '", '
next

for ii = 1 to upperbound(non_masked_fields_array)
	array_value = upper(trim(non_masked_fields_array[ii]))
	array_value = f_cr_clean_string(array_value)
	if left(array_value, 1) = "'" then continue
	insert_sqls = insert_sqls + '"' + array_value + '", '
next

//  Trim the last comma.
insert_sqls = left(insert_sqls, len(insert_sqls) - 2)

//  Don't insert 0's.  Add a space before "having" to allow for the trimmed comma.
insert_sqls = insert_sqls + &
	" having round(sum(amount) " + &
			 "/ " + &
			 "(select sum(amount) from " + a_table_name + " aa " + &
			   "where " + a_join1 + " and " + a_filter1 + "), 8) <> 0"

//  Close the insert.
insert_sqls = insert_sqls + ")"

if i_debug = "YES" then
	//  This string could go over the 2000 character field.
	loop_limit = len(insert_sqls) / 2000
	if mod(len(insert_sqls), 2000) <> 0 then
		loop_limit++
	end if
	for i = 1 to loop_limit
		msg_str = mid(insert_sqls, 2000 * (i - 1) + 1, 2000)
		if i = 1 then f_pp_msgs("***DEBUG*** insert_sqls = ")
		f_pp_msgs(msg_str)
	next
end if

execute immediate :insert_sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into " + a_table_name + " : " + sqlca.sqlerrtext )
	f_pp_msgs("  ")
	f_write_log(g_log_file, sqls)
	return -1
end if


//  ---------  2)  CALL UF_DERIVER_CREATE_OFFSET FOR THE NEWLY INSERTED RECORDS  ----------


f_pp_msgs("-- Calling uf_deriver_create_offset at " + string(now()))

i_deriver_table = "cr_deriver_control_virtual" // Set the table for the next call.

rtn = uf_deriver_create_offset(a_type, a_type_offset, 0, 0)

setnull(i_deriver_table) // Null the table back to cr_deriver_control.

if rtn <> 1 then
	f_pp_msgs("  ")
	f_pp_msgs("UF_DERIVER_CREATE_OFFSET ERROR: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if


//  -----------------  3)  CALL UF_DERIVER TO PERFORM THE VIRTUAL SPLIT  ------------------


f_pp_msgs("-- Calling uf_deriver at " + string(now()))

i_deriver_table = "cr_deriver_control_virtual" // Set the table for the next call.

rtn = uf_deriver(a_type, a_table_name, a_join2)

setnull(i_deriver_table) // Null the table back to cr_deriver_control.

if rtn <> 1 then
	f_pp_msgs("  ")
	f_pp_msgs("UF_DERIVER ERROR: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if


return 1

end function

public function longlong uf_split_quantities (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  Function  :  uf_split_quantities
//
//  Description  :  THIS FUNCTION IS USED AFTER SPLITS HAVE BEEN PERFORMED TO TAKE ONE TRANSACTION
//					   AND SPLIT IT INTO MULTIPLES.  WITHOUT THIS FUNCTION, THE ORIGINAL QUANTITIES IS APPLIED
//					   TO EACH OF THE NEW TRANSACTIONS.  THIS FUNCTION WILL SPLIT THE QUANTITIES IN THE SAME
//					   FORM THAT THE DOLLARS ARE SPLIT.  THE CR_DERIVATION_ROLLUP TABLE HAS A COLUMN TO 
//					   TO INDICATE IF FOR THAT TYPE OF ROLLUP IF THE SPLITS ARE PERFORMED AND TO WHAT PRECISION
//					   THEY ARE SPLIT
//
//  ***  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
string sqls, rtn, rc
longlong i, id
decimal{4} difference, original_quantity, offset_quantity, target_quantity

// ### 7838: JAK: 2011-10-26: i_split_quantities_wc
if isnull(i_split_quantities_wc) then i_split_quantities_wc = ""

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Table Name = " + a_table_name)
	f_pp_msgs("***DEBUG*** IFB = " + a_ifb_id)
end if

uf_system_control('DERIVATIONS - PERCENT ATTRIBUTE')

//// Update the quantities based on the amount split -- the percent is already in the cr_derivation_rate
//// JAK 20080902:  Removed "and amount <> 0 " from below CR_TXN_TYPE clause below...
// ### 7838: JAK: 2011-10-26: i_split_quantities_wc
sqls = &
	"update " + a_table_name + " a " + &
		"set quantity =  " + &
			"round(quantity * " + i_rate_field + ",  " + &
			"(select nvl(number_of_decimals,2) from cr_derivation_rollup c where a.cr_derivation_rollup = c.cr_derivation_rollup)) " + &
		"where CR_TXN_TYPE in ('TARGET','OFFSET') " + i_split_quantities_wc + " " + &
		"and cr_derivation_rollup in  " + &
			"(select cr_derivation_rollup  " + &
			"from cr_derivation_rollup " + &
			"where split_quantities = 1) and interface_batch_id = '" + a_ifb_id + "'"
			
if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Update SQLS = " + sqls)
end if

//// OLD SQL 
//	"update " + a_table_name + " a " + &
//		"set quantity =  " + &
//			"round(amount * (select quantity / amount from " + a_table_name + " b where b.id = a.cr_derivation_id),  " + &
//			"(select nvl(number_of_decimals,2) from cr_derivation_rollup c where a.cr_derivation_rollup = c.cr_derivation_rollup)) " + &
//		"where CR_TXN_TYPE in ('TARGET','OFFSET')  " + &
//		"and amount <> 0 " + &
//		"and cr_derivation_rollup in  " + &
//			"(select cr_derivation_rollup  " + &
//			"from cr_derivation_rollup " + &
//			"where split_quantities = 1) and interface_batch_id = '" + a_ifd_id + "'"
			
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Updating quantities: " + sqlca.sqlerrtext )
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if


// ### 7595: JAK: 2011-05-17:  New system control that will logically determine the record to plug the OOB
uf_system_control('DERIVATION: ROUNDING PRIORITY')

//// Clean-up any rounding issues -- TARGETS
if i_rounding_order_by = 'none' then 
	// Use the old method ... no extra SQL here	
	// ### 7838: JAK: 2011-10-26: i_split_quantities_wc
	sqls = "select difference, max(c.id) id  " + &
	"from " + a_table_name + " c,  " + &
		"(select y.cr_derivation_id, z.quantity - sum(y.quantity) difference, sum(y.quantity) quantity, max(abs(y.amount)) amount  " + &
		"from " + a_table_name + " y, " + a_table_name + " z " + &
		"where y.CR_TXN_TYPE = 'TARGET'  " + &
		"and y.interface_batch_id = '" + a_ifb_id + "' " + &
		"and z.interface_batch_id = '" + a_ifb_id + "'  " + &
		"and y.cr_derivation_rollup in  (select cr_derivation_rollup  from cr_derivation_rollup where split_quantities = 1)  " + &
		"and y.cr_derivation_id = z.id " + &
		"group by y.cr_derivation_id, z.quantity " + &
		"having sum(y.quantity) <> z.quantity) a " + &
	"where c.CR_TXN_TYPE = 'TARGET'  " + i_split_quantities_wc + " " + &
	"and abs(c.amount) = a.amount  " + &
	"and c.cr_derivation_id = a.cr_derivation_id " + &
	"and c.interface_batch_id = '" + a_ifb_id + "'  " + &
	"group by difference,a.cr_derivation_id"

else 
	// Use the ordered method.
	sqls = "select (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id)  - sum(quantity) difference, " + &
		"min(id) KEEP (DENSE_RANK first ORDER BY " + i_rounding_order_by + ") id " + &
		"from " + a_table_name + " " + &
		"where CR_TXN_TYPE = 'TARGET'  " + i_split_quantities_wc + " " + &
		"and interface_batch_id = '" + a_ifb_id + "' " + &
		"and exists (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) " + &
		"and cr_derivation_rollup in  (select cr_derivation_rollup  from cr_derivation_rollup where split_quantities = 1)   " + &
		"group by cr_derivation_id " + &
		"having (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id)  - sum(quantity) <> 0 "
end if

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Target Cleanup = " + sqls)
end if

uo_ds_top uo_ds_rounding 
uo_ds_rounding = create uo_ds_top

rtn = f_create_dynamic_ds(uo_ds_rounding,'grid',sqls,sqlca,true)

if rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving uo_ds_rounding (TARGET): " + sqls)
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if

for i = 1 to uo_ds_rounding.rowcount()
	difference = uo_ds_rounding.getitemnumber(i,'difference')
	id = uo_ds_rounding.getitemnumber(i,'id')
	
	sqls = &
		"update " + a_table_name + " " + &
			"set quantity = quantity + " + string(difference) + &
			" where id = " + string(id)
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** Target Update = " + sqls)
	end if

	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Updating quantities (rounding issues - TARGET): " + sqlca.sqlerrtext )
		f_pp_msgs(" ")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs(" ")
		f_write_log(g_log_file,sqls)
		return -1
	end if

next

//// Clean-up any rounding issues -- OFFSETS
if i_rounding_order_by = 'none' then 
	// Use the old method ... no extra SQL here	
	sqls = "select difference, max(c.id) id  " + &
	"from " + a_table_name + " c,  " + &
		"(select y.cr_derivation_id, z.quantity + sum(y.quantity) difference, sum(y.quantity) quantity, max(abs(y.amount)) amount  " + &
		"from " + a_table_name + " y, " + a_table_name + " z " + &
		"where y.CR_TXN_TYPE = 'OFFSET'  " + &
		"and y.interface_batch_id = '" + a_ifb_id + "' " + &
		"and z.interface_batch_id = '" + a_ifb_id + "'  " + &
		"and y.cr_derivation_rollup in  (select cr_derivation_rollup  from cr_derivation_rollup where split_quantities = 1)  " + &
		"and y.cr_derivation_id = z.id " + &
		"group by y.cr_derivation_id, z.quantity " + &
		"having sum(y.quantity) <> -1 * z.quantity) a " + &
	"where c.CR_TXN_TYPE = 'OFFSET'  " + i_split_quantities_wc + " " + &
	"and abs(c.amount) = a.amount  " + &
	"and c.cr_derivation_id = a.cr_derivation_id " + &
	"and c.interface_batch_id = '" + a_ifb_id + "'  " + &
	"group by difference,a.cr_derivation_id"

else 
	// Use the ordered method.
	sqls = "select (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) + sum(quantity) difference, " + &
		"min(id) KEEP (DENSE_RANK first ORDER BY " + i_rounding_order_by + ") id " + &
		"from " + a_table_name + " " + &
		"where CR_TXN_TYPE = 'OFFSET'  " + i_split_quantities_wc + " " + &
		"and interface_batch_id = '" + a_ifb_id + "' " + &
		"and exists (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) " + &
		"and cr_derivation_rollup in  (select cr_derivation_rollup  from cr_derivation_rollup where split_quantities = 1)   " + &
		"group by cr_derivation_id " + &
		"having (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) + sum(quantity) <> 0 "
end if


if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Offset Cleanup = " + sqls)
end if

rtn = f_create_dynamic_ds(uo_ds_rounding,'grid',sqls,sqlca,true)

if rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving uo_ds_rounding (OFFSET): " + sqls)
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if

for i = 1 to uo_ds_rounding.rowcount()
	difference = uo_ds_rounding.getitemnumber(i,'difference')
	id = uo_ds_rounding.getitemnumber(i,'id')
	
	sqls = &
		"update " + a_table_name + " " + &
			"set quantity = quantity - " + string(difference) + &
			" where id = " + string(id)
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** Offset Update = " + sqls)
	end if
			
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Updating quantities (rounding issues - OFFSET): " + sqlca.sqlerrtext )
		f_pp_msgs(" ")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs(" ")
		f_write_log(g_log_file,sqls)
		return -1
	end if

next

//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:
//    I'm not wild about this call, but we absolutely have to skip this check at Southern
//    for certain budget derivation types.  We can put a shell of this custom function
//    in the visual base and deal with any upgrade issues.
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom_bdg("split quantities UO", 0, " ", a_table_name)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if

if rc = "SKIP" then goto after_check_for_rounding_issues

//// Check for rounding issues that still remain
sqls = &
	"select a.id, a.quantity, b.offset_quantity, b.target_quantity " + &
		"from " + a_table_name + " a,  " + &
			"(select cr_derivation_id,  " + &
			"sum(decode(cr_txn_type,'OFFSET',quantity,0)) offset_quantity,  " + &
			"sum(decode(cr_txn_type,'TARGET',quantity,0)) target_quantity " + &
			"from " + a_table_name + "  " + &
			"where CR_TXN_TYPE in ('TARGET','OFFSET') " + i_split_quantities_wc + " " + &
			"and interface_batch_id = '" + a_ifb_id + "' " + &
			"and cr_derivation_rollup in   " + &
				"(select cr_derivation_rollup   " + &
				"from cr_derivation_rollup  " + &
				"where split_quantities = 1) " + &
			"group by cr_derivation_id) b " + &
		"where a.id = b.cr_derivation_id " + &
		"and a.interface_batch_id = '" + a_ifb_id + "' " + &
		"and (a.quantity <> b.target_quantity " + &
			"or -1 * a.quantity <> b.offset_quantity)"

uo_ds_top uo_ds_rounding2 
uo_ds_rounding2 = create uo_ds_top

rtn = f_create_dynamic_ds(uo_ds_rounding2,'grid',sqls,sqlca,true)

if rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving uo_ds_rounding2: " + sqls)
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if
	
for i = 1 to uo_ds_rounding2.rowcount()	
	id = uo_ds_rounding2.getitemnumber(i,'id')
	original_quantity = uo_ds_rounding2.getitemnumber(i,2)
	offset_quantity = uo_ds_rounding2.getitemnumber(i,3)
	target_quantity = uo_ds_rounding2.getitemnumber(i,4)
	
	if i = 1 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Rounding issues still exist for quantities for the following IDs: ")
		f_pp_msgs(" ")
	end if
	f_pp_msgs(string(id))	
	f_pp_msgs('-----Original Quantity:  ' + string(original_quantity))	
	f_pp_msgs('-----Target Quantity:  ' + string(target_quantity))
	f_pp_msgs('-----Offset Quantity:  ' + string(offset_quantity))
next

if uo_ds_rounding2.rowcount() > 0 then return -1	

after_check_for_rounding_issues:

return 1
end function

public function longlong uf_split_quantities_trueup (string a_table_name);//*****************************************************************************************
//
//  Function  :  uf_split_quantities_trueup
//
//  Description  :  THIS FUNCTION IS USED AFTER SPLITS HAVE BEEN PERFORMED TO TAKE ONE TRANSACTION
//					   AND SPLIT IT INTO MULTIPLES.  WITHOUT THIS FUNCTION, THE ORIGINAL QUANTITIES IS APPLIED
//					   TO EACH OF THE NEW TRANSACTIONS.  THIS FUNCTION WILL SPLIT THE QUANTITIES IN THE SAME
//					   FORM THAT THE DOLLARS ARE SPLIT.  THE CR_DERIVATION_ROLLUP TABLE HAS A COLUMN TO 
//					   TO INDICATE IF FOR THAT TYPE OF ROLLUP IF THE SPLITS ARE PERFORMED AND TO WHAT PRECISION
//					   THEY ARE SPLIT
//
//  ***  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
string sqls, rtn
longlong i, id
decimal{4} difference

uf_system_control('DERIVATIONS - PERCENT ATTRIBUTE')
 
//// If they are not splitting a certain rollup, force the quantity to 0 as this is the way the process
//// worked before the splitting quantities was added
sqls = &
	"update " + a_table_name + " a " + &
		"set quantity =  0 " + &
		"where cr_derivation_rollup in " + &
			"(select cr_derivation_rollup  " + &
			"from cr_derivation_rollup " + &
			"where split_quantities = 0)"
			
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Updating quantities: " + sqlca.sqlerrtext )
	f_write_log(g_log_file,sqls)
	return -1
end if

//// Update the quantities based on the amount split -- the percent is already in the cr_derivation_rate
// ### 6329: JAK: needed the exists clause below for the quantity trueup to work since this function gets called multiple times for seperate companies / types
sqls = &
	"update " + a_table_name + " a " + &
		"set quantity =  " + &
			"round(quantity * " + i_rate_field + ",  " + &
			"(select nvl(number_of_decimals,2) from cr_derivation_rollup c where a.cr_derivation_rollup = c.cr_derivation_rollup)) " + &
		"where drilldown_key = 'RE-DERIVATION'  " + &
		"and exists (select 1 from " + a_table_name + " b where a.cr_derivation_id = b.id) " + &
		"and cr_derivation_rollup in  " + &
			"(select cr_derivation_rollup  " + &
			"from cr_derivation_rollup " + &
			"where split_quantities = 1)"
			
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Updating quantities: " + sqlca.sqlerrtext )
	f_write_log(g_log_file,sqls)
	return -1
end if


// ### 7595: JAK: 2011-05-17:  New system control that will logically determine the record to plug the OOB
uf_system_control('DERIVATION: ROUNDING PRIORITY')

//// Clean-up any rounding issues -- TARGETS
if i_rounding_order_by = 'none' then 
	// Use the old method ... no extra SQL here	
	sqls = "select difference, max(c.id) id  " + &
	"from " + a_table_name + " c,  " + &
		"(select y.cr_derivation_id, z.quantity - sum(y.quantity) difference, sum(y.quantity) quantity, max(abs(y.amount)) amount  " + &
		"from " + a_table_name + " y, " + a_table_name + " z " + &
		"where y.drilldown_key = 'RE-DERIVATION'  " + &
		"and y.cr_derivation_rollup in  (select cr_derivation_rollup  from cr_derivation_rollup where split_quantities = 1)  " + &
		"and y.cr_derivation_id = z.id " + &
		"group by y.cr_derivation_id, z.quantity " + &
		"having sum(y.quantity) <> z.quantity) a " + &
	"where c.drilldown_key = 'RE-DERIVATION'  " + &
	"and abs(c.amount) = a.amount  " + &
	"and c.cr_derivation_id = a.cr_derivation_id " + &
	"group by difference,a.cr_derivation_id"

else 
	// Use the ordered method.
	sqls = "select (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id)  - sum(quantity) difference, " + &
		"min(id) KEEP (DENSE_RANK first ORDER BY " + i_rounding_order_by + ") id  " + &
		"from " + a_table_name + " " + &
		"where drilldown_key = 'RE-DERIVATION'   " + &
		"and exists (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) " + &
		"and cr_derivation_rollup in  (select cr_derivation_rollup  from cr_derivation_rollup where split_quantities = 1)   " + &
		"group by cr_derivation_id " + &
		"having (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id)  - sum(quantity) <> 0 "
end if

uo_ds_top uo_ds_rounding 
uo_ds_rounding = create uo_ds_top

rtn = f_create_dynamic_ds(uo_ds_rounding,'grid',sqls,sqlca,true)

if rtn <> "OK" then
	f_pp_msgs("ERROR: Retrieving uo_ds_rounding (TARGET): " + sqls)
	f_write_log(g_log_file,sqls)
	return -1
end if

for i = 1 to uo_ds_rounding.rowcount()
	difference = uo_ds_rounding.getitemnumber(i,'difference')
	id = uo_ds_rounding.getitemnumber(i,'id')
	
	sqls = &
		"update " + a_table_name + " " + &
			"set quantity = quantity + " + string(difference) + &
			" where id = " + string(id)
			
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Updating quantities (rounding issues - TARGET): " + sqlca.sqlerrtext )
		f_write_log(g_log_file,sqls)
		return -1
	end if

next

return 1
end function

public function longlong uf_deriver_rounding_error_ids (string a_type, string a_table_name, string a_join_original, string a_join_target, string a_identify_target, string a_table_name_field);//*****************************************************************************************
//
//  Function     :  uf_deriver_rounding_error_ids
//
//  ------------------                                                     ----------------
//  ------------------  THESE COMMENTS ARE FROM UF_DERIVER_ROUNDING_ERROR  ----------------
//  ------------------                                                     ----------------
//
//  Description  :  This function aligns with the functionality in uf_deriver and
//                  uf_deriver_with_src_table.  Those 2 functions are used to split
//                  "orignal" transactions into 1 or more records.  When splitting into
//                  more than one target record, there exists the possibility for rounding
//                  error.  This function trues up the target results in the case of
//                  rounding error.
//
//  Args         :  a_type  =  The "TYPE" field on cr_deriver_control.  Should match the
//                             a_type that was used in the call to uf_deriver.
//
//                  a_table_name  =  The table being updated.  Should match the
//                                   a_table_name used in uf_deriver.
//
//                  a_join_original  =  The join (and any other pertinent where clause)
//                                      used to join a_table_name to cr_deriver_control to
//                                      determine the original dollar amounts.  Should be
//                                      similar to the a_join_string used in uf_deriver.
//
//                  a_join_target  =  The join (and any other pertinent where clause)
//                                    used to join a_table_name to cr_deriver_control to
//                                    determine the target dollar amounts.  Should be
//                                    similar to the a_join_string used in uf_deriver.
//
//                  a_identify_target  =  Similar to the a_join_target.  This is the
//                                        where clause that identifies the target records
//                                        in a_table_name.  Used in the rounding update.
//                                        This is probably a_join_target without the actual
//                                        join between a_table_name and cr_deriver_control.
//
//                  a_table_name_field  =  The "field" from the transaction table being
//                                         joined to cr_deriver_control."STRING".  This
//                                         can be a concatenation of fields.
//
//  Notes:
//  ------
//  1)  In the joins and the a_table_name_field passed to this function, the caller must
//      include table prefixes (e.g. cr_accounts_payable.work_order).  I could have
//      appended the a_table_name onto the front of many of these variables, but I wanted
//      the calling methodology to be the same in all functions and args in this object.
//
//  2)  This creates SQL of the form:
//
//        update cr_accounts_payable set AMOUNT = AMOUNT + 0.01
//         where cr_accounts_payable.cr_txn_type = 'TARGET' 
//           and cr_accounts_payable.work_order = 'Z1199'
//           and cr_accounts_payable.id = 
//             (select min(id) from cr_accounts_payable x 
//               where x.work_order = 'Z1199' and x.cr_txn_type = 'TARGET')
//
//  ------------------                                                     ----------------
//  ------------------                      END OF:                        ----------------
//  ------------------  THESE COMMENTS ARE FROM UF_DERIVER_ROUNDING_ERROR  ----------------
//  ------------------                                                     ----------------
//
//
//  Notes for this function !!!  All notes above are form uf_deriver_rounding_error:
//  --------------------------------------------------------------------------------
//  There are cases where the traditional balancing technique does not work.  An example
//  would be when the string contains one or more of the fields that are being derived.
//  Think of 108 being reclassed to expense.  In this case, the join in the bottom part
//  of the minus falls apart since that field is not the same in CDC.STRING and the values
//  in the results table.  In that case, we can alter the SQL slightly to audit OOB
//  conditions by id.
//
//  Note that cr_derivation_id is required as a detailed attribute to perform this
//  kind of balancing (or whatever it is named in cr_system_control).
//
//
//  ***  DMJ: NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
longlong        num_rows, i, dot_pos, id
string      sqls, string_val, orig_sqls, targ_sqls, x_identify_target, &
				x_table_name_field
decimal {2} orig_amount, targ_amount, diff
longlong rtn

// ### - MDZ - 11231 - 20121003*/
longlong orig_id

//
//  Determine the "deriver" table.  If the instance variable is null, then
//  use cr_deriver_control.
//
if isnull(i_deriver_table) or trim(i_deriver_table) = "" then
	i_deriver_table = "cr_deriver_control"
end if

//
//  Setup.
//
uf_system_control('DERIVATIONS : AMOUNT FIELD')
uf_system_control('COMPANY FIELD')
uf_system_control('DERIVATIONS - ID ATTRIBUTE')

//  The a_identify_target arg looks like:
//    cr_accounts_payable_stg.cr_txn_type = 'TARGET'
//  It needs to look like:
//    x.cr_txn_type = 'TARGET'
//  for the actual rounding update (sub-select in where clause for min(id))
//
//  Ditto for the a_table_name_field arg (looks like "cr_accounts_payable_stg.work_order").
//  Note that this method also deals with concatenated fields.
x_identify_target  = f_replace_string(a_identify_target,  a_table_name + ".", "x.", "all")
x_table_name_field = f_replace_string(a_table_name_field, a_table_name + ".", "x.", "all")

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** x_identify_target = " + x_identify_target)
	f_pp_msgs("***DEBUG*** x_table_name_field = " + x_table_name_field)
end if


//
//  Identify any OOB "string" values.
//
//NO LONGER NEEDED DUE TO I VAR: ds_oob_strings = CREATE datastore


// ### JAK : 5839 : 20101108: Change this SQL to calculate the plug and the id to update.  That way it doesn't
// have to be in the loop and can save processing time
sqls = &
	"select id, sum(orig_amount) orig_amount, sum(target_amount) target_amount, " + &
		"sum(orig_amount) - sum(target_amount) plug_amount, min(id_to_update) id_to_update " + &
	"from ( " + &
		"select id, amount orig_amount, 0 target_amount, null id_to_update " + &
		  "from " + a_table_name + ", " + &
			'(select distinct "STRING" from ' + i_deriver_table + ' ' + &
			  'where "TYPE" = ' + " '" + a_type + "') " + i_deriver_table + " " + &
		 "where " + a_join_original + " " + &
		 "union all " + &
		 "select " + i_id_field + ", 0 orig_amount, sum(amount) target_amount, min(id) "
		
// ### 7595: JAK: 2011-05-17:  New system control that will logically determine the record to plug the OOB
uf_system_control('DERIVATION: ROUNDING PRIORITY')

if i_rounding_order_by = 'none' then 
	// Use the old method ... no extra SQL here	
else 
	// Use the ordered method.
	sqls += "KEEP (DENSE_RANK first ORDER BY " + i_rounding_order_by + ") "
end if

sqls += 																										" id_to_update " + &
			"from " + a_table_name + " " + &
		 "where " + a_join_target + " " + &
		 'group by ' + i_id_field + &
	") " + &
	"group by id having sum(orig_amount) - sum(target_amount) <> 0 "
	
if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** i_ds_oob_strings_ids sqls = " + sqls)
end if

//NO LONGER NEEDED DUE TO I VAR: f_create_dynamic_ds(i_ds_oob_strings_ids, "grid", sqls, sqlca, true)
rtn = i_ds_oob_strings_ids.SetSQLSelect(sqls)

if rtn < 0 then
	// Retrieve failed
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: SetSQLSelect failed: " + i_ds_oob_strings_ids.i_sqlca_sqlerrtext)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	return -1
end if

rtn = i_ds_oob_strings_ids.RETRIEVE()

if rtn < 0 then
	// Retrieve failed
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieve failed: " + i_ds_oob_strings_ids.i_sqlca_sqlerrtext)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	return -1
end if

num_rows = i_ds_oob_strings_ids.RowCount()

//
//  Loop over any OOB "string" values and apply the rounding plug.
//
for i = 1 to num_rows
	diff = i_ds_oob_strings_ids.GetItemNumber(i, 4)
	id = i_ds_oob_strings_ids.GetItemNumber(i, 5)	
	
	// ### - MDZ - 11231 - 20121003
	//  	If using the effective month feature on cr_deriver_control and the month has expired, then there will be no TARGET record
	//		for the original transaction.  This breaks the rounding update SQL later in the function because id is nulll.
	//		Simply filter it away at this point.
	orig_id = i_ds_oob_strings_ids.GetItemNumber(i,1)
	if isnull(id) or id = 0 then
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** The original record with Id " + string(orig_id) + " did not generate any derivation transactions.~n")
		end if
		
		continue
	end if

	if i_debug = "YES" then
		orig_amount = i_ds_oob_strings_ids.GetItemNumber(i, 2)
		targ_amount = i_ds_oob_strings_ids.GetItemNumber(i, 3)
		
		f_pp_msgs("***DEBUG*** i = " + string(i) + " of " + string(num_rows))
		f_pp_msgs("***DEBUG*** ----------------")
		f_pp_msgs("***DEBUG*** orig_amount = " + string(orig_amount))
		f_pp_msgs("***DEBUG*** targ_amount = " + string(targ_amount))
	end if

	//
	//  Plug the record with the min(id)
	//
	sqls = &
		"update " + a_table_name + &
		  " set " + i_amount_field + " = " + i_amount_field + " + " + string(diff) + " " + &
		  "where id = " + string(id)
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** update sqls = " + sqls)
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating " + a_table_name + " with rounding adjustment " + &
			"for string: " + string_val)
		f_pp_msgs("ERROR = " + sqlca.SQLErrText)
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		return -1
	end if
	
	//
	//  If we are in this loop, this "STRING" has rounding error.  If the "amount" datastores
	//  come back with no records, we have a problem.
	//
	if sqlca.SQLNRows = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: NO ROWS UPDATED: updating " + a_table_name + " with rounding adjustment " + &
			"for string: " + string_val)
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		return -1
	end if
	
next


//NO LONGER NEEDED DUE TO I VAR: DESTROY ds_oob_strings
//NO LONGER NEEDED DUE TO I VAR: DESTROY ds_orig_amount
//NO LONGER NEEDED DUE TO I VAR: DESTROY ds_targ_amount

return 1
end function

public function longlong uf_split_quantities_all (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  Function  :  uf_split_quantities
//
//  Description  :  THIS FUNCTION IS USED AFTER SPLITS HAVE BEEN PERFORMED TO TAKE ONE TRANSACTION
//					   AND SPLIT IT INTO MULTIPLES.  WITHOUT THIS FUNCTION, THE ORIGINAL QUANTITIES IS APPLIED
//					   TO EACH OF THE NEW TRANSACTIONS.  THIS FUNCTION WILL SPLIT THE QUANTITIES IN THE SAME
//					   FORM THAT THE DOLLARS ARE SPLIT.  THE CR_DERIVATION_ROLLUP TABLE HAS A COLUMN TO 
//					   TO INDICATE IF FOR THAT TYPE OF ROLLUP IF THE SPLITS ARE PERFORMED AND TO WHAT PRECISION
//					   THEY ARE SPLIT
//
//  ***  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!  A CALLING SCRIPT LIKE AN
//  ***       INTERFACE WILL WANT TO EVALUATE THE SQLCA.SQLCODE AND SQLERRTEXT !!!
//
//*****************************************************************************************
string sqls, rtn, rc
longlong i, id
decimal{4} difference, original_quantity, offset_quantity, target_quantity

if isnull(i_split_quantities_all_wc) then i_split_quantities_all_wc = ""

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Table Name = " + a_table_name)
	f_pp_msgs("***DEBUG*** IFB = " + a_ifb_id)
end if

uf_system_control('DERIVATIONS - PERCENT ATTRIBUTE')

//// Update the quantities based on the amount split -- the percent is already in the cr_derivation_rate
//// JAK 20080902:  Removed "and amount <> 0 " from below CR_TXN_TYPE clause below...
sqls = &
	"update " + a_table_name + " a " + &
		"set quantity =  " + &
			"round(quantity * " + i_rate_field + ",  " + &
			"2) " + &
		"where CR_TXN_TYPE in ('TARGET','OFFSET') " + i_split_quantities_all_wc + " " + &
		"and interface_batch_id = '" + a_ifb_id + "'"
			
if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Update SQLS = " + sqls)
end if

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Updating quantities: " + sqlca.sqlerrtext )
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if

// ### 7595: JAK: 2011-05-17:  New system control that will logically determine the record to plug the OOB
uf_system_control('DERIVATION: ROUNDING PRIORITY')

//// Clean-up any rounding issues -- TARGETS
if i_rounding_order_by = 'none' then 
	// Use the old method ... no extra SQL here	
	sqls = "select difference, max(c.id) id  " + &
	"from " + a_table_name + " c,  " + &
		"(select y.cr_derivation_id, z.quantity - sum(y.quantity) difference, sum(y.quantity) quantity, max(abs(y.amount)) amount  " + &
		"from " + a_table_name + " y, " + a_table_name + " z " + &
		"where y.CR_TXN_TYPE = 'TARGET'  " + &
		"and y.interface_batch_id = '" + a_ifb_id + "' " + &
		"and z.interface_batch_id = '" + a_ifb_id + "'  " + &
		"and y.cr_derivation_id = z.id " + &
		"group by y.cr_derivation_id, z.quantity " + &
		"having sum(y.quantity) <> z.quantity) a " + &
	"where c.CR_TXN_TYPE = 'TARGET'  " + i_split_quantities_all_wc + " " + &
	"and abs(c.amount) = a.amount  " + &
	"and c.cr_derivation_id = a.cr_derivation_id " + &
	"and c.interface_batch_id = '" + a_ifb_id + "'  " + &
	"group by difference,a.cr_derivation_id"

else 
	// Use the ordered method.
	sqls = "select (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id)  - sum(quantity) difference, " + &
		"min(id) KEEP (DENSE_RANK first ORDER BY " + i_rounding_order_by + ") id " + &
		"from " + a_table_name + " " + &
		"where CR_TXN_TYPE = 'TARGET'  " + i_split_quantities_all_wc + " " + &
		"and interface_batch_id = '" + a_ifb_id + "' " + &
		"and exists (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) " + &
		"group by cr_derivation_id " + &
		"having (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id)  - sum(quantity) <> 0 "
end if

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Target Cleanup = " + sqls)
end if

uo_ds_top uo_ds_rounding 
uo_ds_rounding = create uo_ds_top

rtn = f_create_dynamic_ds(uo_ds_rounding,'grid',sqls,sqlca,true)

if rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving uo_ds_rounding (TARGET): " + sqls)
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if

for i = 1 to uo_ds_rounding.rowcount()
	difference = uo_ds_rounding.getitemnumber(i,'difference')
	id = uo_ds_rounding.getitemnumber(i,'id')
	
	sqls = &
		"update " + a_table_name + " " + &
			"set quantity = quantity + " + string(difference) + &
			" where id = " + string(id)
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** Target Update = " + sqls)
	end if

	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Updating quantities (rounding issues - TARGET): " + sqlca.sqlerrtext )
		f_pp_msgs(" ")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs(" ")
		f_write_log(g_log_file,sqls)
		return -1
	end if

next

//// Clean-up any rounding issues -- OFFSETS
if i_rounding_order_by = 'none' then 
	// Use the old method ... no extra SQL here	
	sqls = "select difference, max(c.id) id  " + &
	"from " + a_table_name + " c,  " + &
		"(select y.cr_derivation_id, z.quantity + sum(y.quantity) difference, sum(y.quantity) quantity, max(abs(y.amount)) amount  " + &
		"from " + a_table_name + " y, " + a_table_name + " z " + &
		"where y.CR_TXN_TYPE = 'OFFSET'  " + &
		"and y.interface_batch_id = '" + a_ifb_id + "' " + &
		"and z.interface_batch_id = '" + a_ifb_id + "'  " + &
		"and y.cr_derivation_id = z.id " + &
		"group by y.cr_derivation_id, z.quantity " + &
		"having sum(y.quantity) <> -1 * z.quantity) a " + &
	"where c.CR_TXN_TYPE = 'OFFSET'  " + i_split_quantities_all_wc + " " + &
	"and abs(c.amount) = a.amount  " + &
	"and c.cr_derivation_id = a.cr_derivation_id " + &
	"and c.interface_batch_id = '" + a_ifb_id + "'  " + &
	"group by difference,a.cr_derivation_id"

else 
	// Use the ordered method.
	sqls = "select (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id)  + sum(quantity) difference, " + &
		"min(id) KEEP (DENSE_RANK first ORDER BY " + i_rounding_order_by + ") id " + &
		"from " + a_table_name + " " + &
		"where CR_TXN_TYPE = 'OFFSET'  " + i_split_quantities_all_wc + " " + &
		"and interface_batch_id = '" + a_ifb_id + "' " + &
		"and exists (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) " + &
		"group by cr_derivation_id " + &
		"having (select quantity from " + a_table_name + " inside where inside.id = " + a_table_name + ".cr_derivation_id) + sum(quantity) <> 0 "
end if

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** Offset Cleanup = " + sqls)
end if

rtn = f_create_dynamic_ds(uo_ds_rounding,'grid',sqls,sqlca,true)

if rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving uo_ds_rounding (OFFSET): " + sqls)
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if

for i = 1 to uo_ds_rounding.rowcount()
	difference = uo_ds_rounding.getitemnumber(i,'difference')
	id = uo_ds_rounding.getitemnumber(i,'id')
	
	sqls = &
		"update " + a_table_name + " " + &
			"set quantity = quantity - " + string(difference) + &
			" where id = " + string(id)
	
	if i_debug = "YES" then
		f_pp_msgs("***DEBUG*** Offset Update = " + sqls)
	end if
			
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Updating quantities (rounding issues - OFFSET): " + sqlca.sqlerrtext )
		f_pp_msgs(" ")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs(" ")
		f_write_log(g_log_file,sqls)
		return -1
	end if

next



//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:
//    I'm not wild about this call, but we absolutely have to skip this check at Southern
//    for certain budget derivation types.  We can put a shell of this custom function
//    in the visual base and deal with any upgrade issues.
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom_bdg("split quantities all UO", 0, " ", a_table_name)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if


if rc = "SKIP" then goto after_check_for_rounding_issues


//// Check for rounding issues that still remain
sqls = &
	"select a.id, a.quantity, b.offset_quantity, b.target_quantity " + &
		"from " + a_table_name + " a,  " + &
			"(select cr_derivation_id,  " + &
			"sum(decode(cr_txn_type,'OFFSET',quantity,0)) offset_quantity,  " + &
			"sum(decode(cr_txn_type,'TARGET',quantity,0)) target_quantity " + &
			"from " + a_table_name + "  " + &
			"where CR_TXN_TYPE in ('TARGET','OFFSET') " + i_split_quantities_all_wc + " " + &
			"and interface_batch_id = '" + a_ifb_id + "' " + &
			"group by cr_derivation_id) b " + &
		"where a.id = b.cr_derivation_id " + &
		"and a.interface_batch_id = '" + a_ifb_id + "' " + &
		"and (a.quantity <> b.target_quantity " + &
			"or -1 * a.quantity <> b.offset_quantity)"

uo_ds_top uo_ds_rounding2 
uo_ds_rounding2 = create uo_ds_top

rtn = f_create_dynamic_ds(uo_ds_rounding2,'grid',sqls,sqlca,true)

if rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving uo_ds_rounding2: " + sqls)
	f_pp_msgs(" ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	f_write_log(g_log_file,sqls)
	return -1
end if
	
for i = 1 to uo_ds_rounding2.rowcount()	
	id = uo_ds_rounding2.getitemnumber(i,'id')
	original_quantity = uo_ds_rounding2.getitemnumber(i,2)
	offset_quantity = uo_ds_rounding2.getitemnumber(i,3)
	target_quantity = uo_ds_rounding2.getitemnumber(i,4)
	
	if i = 1 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Rounding issues still exist for quantities for the following IDs: ")
		f_pp_msgs(" ")
	end if
	f_pp_msgs(string(id))	
	f_pp_msgs('-----Original Quantity:  ' + string(original_quantity))	
	f_pp_msgs('-----Target Quantity:  ' + string(target_quantity))
	f_pp_msgs('-----Offset Quantity:  ' + string(offset_quantity))
next

if uo_ds_rounding2.rowcount() > 0 then return -1	

after_check_for_rounding_issues:

return 1
end function

private subroutine uf_system_control (string a_control);choose case upper(trim(a_control))
	case 'DERIVATION: ROUNDING PRIORITY'
		// ### 7595: JAK: 2011-05-17:  New system control that will logically determine the record
		//	to plug the OOB rather than using the default min(id).  The control value should be an 
		//	order by statement such as "abs(amount) desc, account, wbs_number"
		if isnull(i_rounding_order_by) or trim(i_rounding_order_by) = '' then
i_rounding_order_by = upper(trim(g_cr.uf_get_control_value('DERIVATION: ROUNDING PRIORITY'))) //Autogenerated sql replace
			
			if isnull(i_rounding_order_by) or trim(i_rounding_order_by) = '' then i_rounding_order_by = 'none'
		end if
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** rounding_order_by = " + i_rounding_order_by)
		end if
		
	case 'DERIVATIONS - ID ATTRIBUTE'
		if isnull(i_id_field) or trim(i_id_field) = '' then
			//	Look up the "id attribute" field from cr_system_control.  This is the id value
			// of the original CR Detail transactions that generated the derivation result.
i_id_field = upper(trim(g_cr.uf_get_control_value('DERIVATIONS - ID ATTRIBUTE'))) //Autogenerated sql replace
			
			if isnull(i_id_field) then i_id_field = "***"
		end if
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** id_field = " + i_id_field)
		end if
		
	case 'DERIVATIONS - PERCENT ATTRIBUTE'
		if isnull(i_rate_field) or trim(i_rate_field) = '' then
			//	Look up the "percent attribute" field from cr_system_control.  This IS NOT used
			// for a lookup on cr_deriver_control.  Rather, it is the detailed attribute on the
			// CR Detail table where the rate will be saved.
i_rate_field = upper(trim(g_cr.uf_get_control_value('DERIVATIONS - PERCENT ATTRIBUTE'))) //Autogenerated sql replace
			
			if isnull(i_rate_field) then i_rate_field = "***"
		end if
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** rate_field = " + i_rate_field)
		end if
		
	case 'DERIVATIONS - EFFMN'
		if isnull(i_effmn_cv) or trim(i_effmn_cv) = '' then
			//	 Effective dating the CDC records ?
i_effmn_cv = upper(trim(g_cr.uf_get_control_value('DERIVATIONS - EFFMN'))) //Autogenerated sql replace
				
			if isnull(i_effmn_cv) or trim(i_effmn_cv) <> 'YES' then i_effmn_cv = 'NO'
		end if 
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** effmn_cv = " + i_effmn_cv)
		end if
		
	case 'DERIVATIONS : AMOUNT FIELD'
		if isnull(i_amount_field) or trim(i_amount_field) = '' then
i_amount_field = upper(g_cr.uf_get_control_value('DERIVATIONS : AMOUNT FIELD')) //Autogenerated sql replace
				
			if isnull(i_amount_field) or trim(i_amount_field) = '' then i_amount_field = "AMOUNT"
		end if
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** amount_field = " + i_amount_field)
		end if

	case 'COMPANY FIELD'
		if isnull(i_co_field) or trim(i_co_field) = '' then
i_co_field = upper(trim(g_cr.uf_get_control_value('COMPANY FIELD'))) //Autogenerated sql replace
		
			if isnull(i_co_field) then i_co_field = " " // spaces so ||'s evaluate properly
		end if
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** co_field = " + i_co_field)
		end if
		
	case 'DERIVATIONS - NORMALIZE PERCENT'
		if isnull(i_cv_normalize) or trim(i_cv_normalize) = '' then
i_cv_normalize = upper(trim(g_cr.uf_get_control_value('DERIVATIONS - NORMALIZE PERCENT'))) //Autogenerated sql replace
		
			if isnull(i_cv_normalize) or i_cv_normalize <> 'YES' then i_cv_normalize = "NO"
		end if
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** cv_normalize = " + i_cv_normalize)
		end if
		
	case 'DERIVATIONS - ADDL ROUNDING'
		if isnull(i_addl_rounding_field) or trim(i_addl_rounding_field) = '' then
i_addl_rounding_field = upper(trim(g_cr.uf_get_control_value('DERIVATIONS - ADDL ROUNDING'))) //Autogenerated sql replace
		
			if isnull(i_addl_rounding_field) or trim(i_addl_rounding_field) = '' then i_addl_rounding_field = "' '"
		end if
		
		if i_debug = "YES" then
			f_pp_msgs("***DEBUG*** addl_rounding_field = " + i_addl_rounding_field)
		end if
end choose		


end subroutine

public function integer uf_deriver_override ();//*****************************************************************************************
//	$$$ BSB: Maint 10143
//	Add a function in uo_cr_derivation that loops over the derivation override 2 table and makes calls to uf_deriver_update_override
//  Function  :  uf_deriver_override
//
//  Description  :  Wrapper function to invoke uf_deriver_update_override.  
//
//	Note:			MUST BE CALLED AFTER UF_DERIVER OR UF_DERIVER_WITH_SRC_TABLE SUCH THAT THE APPROPRIATE
//					INSTANCE VARIABLES ARE POPULATED
//
//  Description  :  THIS FUNCTION IS A DERIVATION UPDATE FUNCTION THAT ALLOWS YOU TO USE
//                  COMPLEX SQL-BASED DERIVATIONS.  IT KEYS OFF OF CR_DERIVER_OVERRIDE2
//                  WHICH IS MAINTAINED ONLINE.  IN CR_DERIVER_CONTROL, ENTER A "-" IN
//                  THE FIELD WHERE YOU WANT TO USE THIS LOGIC (THE MAIN TAB ON THE 
//                  DERIVATION MAINT WINDOW).
//*****************************************************************************************
longlong i, num, ret
string override_name, col_name

num = i_ds_override2.rowCount()

if i_debug = "YES" then
	f_pp_msgs("***DEBUG*** entered uf_deriver_override")
	f_pp_msgs("***DEBUG*** pi_s_res_table = " + pi_s_res_table)
	f_pp_msgs("***DEBUG*** pi_s_res_arg = " + pi_s_res_arg)
	f_pp_msgs("***DEBUG*** pi_s_src_table = " + pi_s_src_table)
	f_pp_msgs("***DEBUG*** i_suppl_where_clause = " + i_suppl_where_clause)
	f_pp_msgs("***DEBUG*** number of overrides = " + string(num))
end if

ret = 1
for i = 1 to num
	override_name = i_ds_override2.getItemString(i, "override_name")
	col_name = i_ds_override2.getItemString(i, "col_name")
	ret = this.uf_deriver_update_override(override_name, col_name)
	if ret = -1 then
		exit
	end if
next

return ret
// $$$ BSB: END Maint 10143

end function

public function integer uf_deriver_override (string a_table_name, string a_src_table, string a_where_clause);//*****************************************************************************************
//	$$$ BSB: Maint 10143
//	Add a function in uo_cr_derivation that loops over the derivation override 2 table and makes calls to uf_deriver_update_override
//  Function  :  uf_deriver_override
//
//  Description  :  Wrapper function to invoke uf_deriver_update_override.  
//
//	Arguments:
//		a_table_name:		The table name to be updated
//		a_src_table:			Source table that will be used to replace the "src_table" keyword in the override2 SQL (optional)
//		a_where_clause:	Supplimental where clause to restrict the update against a_table_name (optional -- otherwise only updates fields with '-')
//
//*****************************************************************************************
longlong i, num, ret
string override_name, col_name

if isnull(a_src_table) or trim(a_src_table) = '' then a_src_table = a_table_name

if isnull(a_where_clause) then
	a_where_clause = ""
else
	if mid(trim(lower(a_where_clause)),1,5) = 'where' then a_where_clause = mid(trim(a_where_clause),6)
	if mid(trim(lower(a_where_clause)),1,3) <> 'and' then a_where_clause = 'and ' + a_where_clause
end if

pi_s_res_table = a_table_name
pi_s_src_table = a_src_table
i_suppl_where_clause = a_where_clause
pi_s_res_arg = ''

// Call the general function now that the arguments are populated.
return uf_deriver_override()

end function

public function string uf_check_element (string a_budget_or_actual, string a_column);// Checks if A_COLUMN is in the ACK for either A_BUDGET_OR_ACTUAL (BUDGET for Budget ACK and ACTUAL for Actual ACK)
//	Returns "0" if the column is not part of the ACK
//	Returns the Element Description (field that will be on CR Deriver Control) if it is found
longlong i 

a_column = f_cr_clean_string(upper(trim(a_column)))
if upper(trim(a_budget_or_actual)) = 'BUDGET' then
	a_budget_or_actual = 'budgeting_element_clean'
else
	a_budget_or_actual = 'description_clean'
end if

for i = 1 to i_num_elements
	if a_column = i_ds_elements.GetItemString(i,a_budget_or_actual) then return i_ds_elements.GetItemString(i,'description_clean')
next 

for i = 1 to i_num_add_fields
	if i_ds_add_fields.GetItemNumber(i,'derive_value') = 1 then
		if a_column = i_ds_add_fields.GetItemString(i,'description_clean') then return i_ds_add_fields.GetItemString(i,'description_clean')
	end if
next 

return "0"

end function

public subroutine uf_deriver_defaults_reset ();i_ds_defaults.reset()
end subroutine

public subroutine uf_deriver_defaults_add (string a_column_name, string a_column_value, integer a_literal);// Add a default value for a column when derivations are performed in uf_deriver_with_src_table
//	Arguments:
//		a_column_name	Column the default value will be applied
//		a_column_value	Value to be used for a_column_name.  Can either be a literal value (a_literal = 1) or field_name / SQL
//		a_literal				1 means a_column_value is a literal, 0 means a_column_value is direct SQL
//
//	Examples:
//		Populate GL Journal Category with 'DERIVATIONS'
//			uf_deriver_defaults_add('gl_journal_category','DERIVATIONS',1)
//
//		Populate CR Derivation ID with the ID
//			uf_deriver_defaults_add('cr_derivation_id','cr.id',0)
//			uf_deriver_defaults_add('cr_derivation_id',a_table_name + '.id',0)
//
//	Note if the field already exists in the default table, the new default will override the old.
longlong row

a_column_name = upper(trim(a_column_name))
if a_literal = 1 then a_column_value = "'" + f_replace_string(a_column_value,"'","''","all") + "'"

row = i_ds_defaults.find("column_name = '" + a_column_name + "'",1,i_ds_defaults.rowcount())

if row > 0 then
	// Update the existing default
else
	// Add a row
	row = i_ds_defaults.InsertRow(0)
	i_ds_defaults.SetItem(row,'column_name',a_column_name)
end if

i_ds_defaults.SetItem(row,'column_value',a_column_value)
end subroutine

public function string uf_deriver_defaults_get (string a_table_name, string a_column_name);// Retrieves the default value for a column when derivations are performed in uf_deriver_with_src_table
//	Arguments:
//		a_column_name	Column the default value will be applied
//		a_table_name		Table the derivations are being run against
//
//	Returns the string to populate a_column_name.  Returns a_table_name.a_column_name if the field is not mapped.

longlong row

a_column_name = upper(trim(a_column_name))

row = i_ds_defaults.find("column_name = '" + a_column_name + "'",1,i_ds_defaults.rowcount())

if row > 0 then
	// return the default
	return f_replace_string(i_ds_defaults.GetItemString(row,'column_value'),'cr.',a_table_name + '.','all')
else
	return a_table_name + '."' + a_column_name + '"'
end if
end function

on uo_cr_derivation.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_derivation.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string sqls
longlong i

//  For uf_deriver_rounding_error:
i_ds_oob_strings = CREATE uo_ds_top
i_ds_orig_amount = CREATE uo_ds_top
i_ds_targ_amount = CREATE uo_ds_top
sqls = &
	'select cr_deriver_control."STRING", sum(cr_cost_repository.amount) ' + &
	  "from cr_cost_repository, " + &
				'(select distinct "STRING" from cr_deriver_control ' + &
				  'where "TYPE"' + " = 'asdfnotype' and id = -678) cr_deriver_control " + &
	 'where cr_deriver_control."STRING" = cr_cost_repository.drilldown_key ' + &
		"and cr_cost_repository.id = -678 " + &
	 'group by cr_deriver_control."STRING"'
f_create_dynamic_ds(i_ds_oob_strings, "grid", sqls, sqlca, true)
f_create_dynamic_ds(i_ds_orig_amount, "grid", sqls, sqlca, true)
f_create_dynamic_ds(i_ds_targ_amount, "grid", sqls, sqlca, true)

// ### JAK : 5839 : 20101108:  This DS contains different info now...update the create statement
i_ds_oob_strings_ids = CREATE uo_ds_top
sqls = &
	"select id, amount orig_amount, amount target_amount, " + &
		"amount plug_amount, id id_to_update " + &
	  "from cr_cost_repository " + &
	 "where id = -678 "
//sqls = &
//	'select id, amount ' + &
//	  "from cr_cost_repository " + &
//	 "where id = -678 "
f_create_dynamic_ds(i_ds_oob_strings_ids, "grid", sqls, sqlca, true)


//  For uf_deriver_create_offset:
i_ds_columns  = CREATE uo_ds_top
i_ds_columns2 = CREATE uo_ds_top
sqls = "select column_name, column_id from sys.all_tab_columns " + &
	"where owner = 'PWRPLANT' and table_name = 'asdfnotableasdf'"
f_create_dynamic_ds(i_ds_columns,  "grid", sqls, sqlca, true)
f_create_dynamic_ds(i_ds_columns2, "grid", sqls, sqlca, true)


//  For uf_deriver_virtual:
i_ds_elements = CREATE uo_ds_top
sqls = 'select description, description description_clean, "ORDER", "TYPE", "WIDTH", "DECIMAL", element_table,  ' + &
	'element_column, element_column element_column_clean, gl_element, gl_element gl_element_clean,  ' + &
	'budgeting_element, budgeting_element budgeting_element_clean from cr_elements '
f_create_dynamic_ds(i_ds_elements, "grid", sqls, sqlca, true)
i_ds_elements.SetSort("order a")
i_ds_elements.Sort()
i_num_elements = i_ds_elements.rowcount()

for i = 1 to i_num_elements
	i_ds_elements.SetItem(i,	'description_clean',			f_cr_clean_string(upper(trim(	i_ds_elements.GetItemString(i,	'description')			))))
	i_ds_elements.SetItem(i,	'element_column_clean',		f_cr_clean_string(upper(trim(	i_ds_elements.GetItemString(i,	'element_column')		))))
	i_ds_elements.SetItem(i,	'gl_element_clean',			f_cr_clean_string(upper(trim(	i_ds_elements.GetItemString(i,	'gl_element')			))))
	i_ds_elements.SetItem(i,	'budgeting_element_clean',	f_cr_clean_string(upper(trim(	i_ds_elements.GetItemString(i,	'budgeting_element')	))))
next 

// ### BSB: Maint 10143: uo to store the override 2 attributes
i_ds_override2 = CREATE uo_ds_top
sqls = "select override_name, col_name from cr_deriver_override2"
f_create_dynamic_ds(i_ds_override2, "grid", sqls, sqlca, true)

// JAK: Datastore for default values.  Will be used so fields don't have to be backfilled after the fact.
i_ds_defaults = create uo_ds_top
sqls = "select lpad(' ',30,' ') column_name, lpad(' ',2000,' ') column_value from dual where -1 = 0"
f_create_dynamic_ds(i_ds_defaults, "grid", sqls, sqlca, true)

i_ds_add_fields = CREATE uo_ds_top
sqls = 'select description, description description_clean, "ORDER", "TYPE", "WIDTH", "DECIMAL", nvl(derive_value,0) derive_value from cr_deriver_additional_fields '
f_create_dynamic_ds(i_ds_add_fields, "grid", sqls, sqlca, true)
i_ds_add_fields.SetSort("order a")
i_ds_add_fields.Sort()
i_num_add_fields = i_ds_add_fields.rowcount()

for i = 1 to i_num_add_fields
	i_ds_add_fields.SetItem(i,'description_clean',f_cr_clean_string(upper(trim(i_ds_add_fields.GetItemString(i,'description')))))
next
end event

event destructor;
DESTROY i_ds_add_fields
end event

