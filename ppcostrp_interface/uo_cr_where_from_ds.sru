HA$PBExportHeader$uo_cr_where_from_ds.sru
$PBExportComments$JAK: Added 'DO NOT UPDATE' logic so certain fields will not be updated if necessary.  IE master element table with a field we are manually maintaining.
forward
global type uo_cr_where_from_ds from nonvisualobject
end type
end forward

global type uo_cr_where_from_ds from nonvisualobject
end type
global uo_cr_where_from_ds uo_cr_where_from_ds

type prototypes
function long  ShellExecuteA(long hwnd,string operation,string filename,string parm,string dir,long showcmd) library "shell32.dll"
end prototypes

type variables
string i_use_new_structures_table
end variables

forward prototypes
public function string uf_build_sql_where (uo_ds_top a_ds)
private function string uf_structure_value (longlong a_structure_id, string a_rollups)
end prototypes

public function string uf_build_sql_where (uo_ds_top a_ds);// Expects a datastore with the following columns as an argument:
//	row_id, left_paren, column_name, operator, value1, between_and, value2, and_or, right_paren, structure_id
// Returns a where clause.  There where clause does not contain a leading or trailing and/or/where

string sqls, sql_where, module_column, table_name, str_rtn
longlong num_rows, i, find_row

string left_paren, col_name, operator, value1, between_and, value2, right_paren, and_or
longlong left_paren_count, right_paren_count, structure_id
boolean structure_criteria

// System control lookups
if isnull(i_use_new_structures_table) or (i_use_new_structures_table <> 'YES' and i_use_new_structures_table <> 'NO') then
	//  Get the Structures Variable:  New variable to determine which structure values table is being used.
	setnull(i_use_new_structures_table)
	i_use_new_structures_table = upper(trim(f_cr_system_control('USE NEW CR STRUCTURE VALUES TABLE')))
	if i_use_new_structures_table <> 'YES' or isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"
end if

// validate we have all of the right columns up front:
str_rtn = a_ds.Describe('row_id.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "ROW_ID" column'

str_rtn = a_ds.Describe('left_paren.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "LEFT_PAREN" column'

str_rtn = a_ds.Describe('column_name.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "COLUMN_NAME" column'

str_rtn = a_ds.Describe('operator.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "OPERATOR" column'

str_rtn = a_ds.Describe('value1.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "VALUE1" column'

str_rtn = a_ds.Describe('between_and.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "BETWEEN_AND" column'

str_rtn = a_ds.Describe('value2.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "VALUE2" column'

str_rtn = a_ds.Describe('and_or.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "AND_OR" column'

str_rtn = a_ds.Describe('right_paren.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "RIGHT_PAREN" column'

str_rtn = a_ds.Describe('structure_id.x')
if str_rtn = '!' or str_rtn = '?' then return 'ERROR:  DS missing the "STRUCTURE_ID" column'

a_ds.SetSort('ROW_ID ASC')
a_ds.Sort()

num_rows = a_ds.RowCount()
if num_rows = 0 then return '1=1'

left_paren_count = 0
right_paren_count = 0

for i = 1 to num_rows
	left_paren  = a_ds.GetItemString(i, "left_paren")
	col_name = a_ds.GetItemString(i, "column_name")
	operator = a_ds.GetItemString(i, "operator")
	value1 = a_ds.GetItemString(i, "value1")	
	between_and = a_ds.GetItemString(i, "between_and")
	value2 = a_ds.GetItemString(i, "value2")
	right_paren = a_ds.GetItemString(i, "right_paren")
	and_or = a_ds.GetItemString(i, "and_or")
	structure_id = a_ds.GetItemNumber(i, "structure_id")
	
	operator = upper(trim(operator))
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id)      then structure_id      = 0
	
	if pos(col_name, "(") = 0 and operator <> '<NONE>' then
		//  If they manually entered an Oracle function, take it as-is.
		col_name = '"' + f_cr_clean_string(upper(trim(col_name))) + '"'
	end if
	
	if left(trim(value1), 1) = "{" then
		structure_criteria = true
		// using structure where clause.
		if operator <> 'IN' and operator <> 'NOT IN' then
			// they gave us a bad operator
			choose case operator
				case '=', 'LIKE'
					// assume in
					operator = 'IN'
				case '<>', 'NOT LIKE'
					// assume not in
					operator = 'NOT IN'
				case else
					// can't assume...
					return 'ERROR: Invalid operator for structure based criteria'
			end choose
		end if 
	else
		structure_criteria = false
	end if
	
	if i <> num_rows and trim(and_or) = '' then return 'ERROR:  And/Or field is required on allows rows except the last'
	
	choose case operator
		// ******************************************************
		case '=','<>','>','>=','<','<='
		// ******************************************************
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value1), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value1 + ')'
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case 'BETWEEN','NOT BETWEEN'
		// ******************************************************
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value1), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value1 + ')'
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
				
			if mid(trim(value2),1,1) = "'" then
				// have quotes
			elseif pos(value2, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value2), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value2 + ')'
			else
				// Need to add quotes
				value2 = "'" + value2 + "'"
			end if
				
		// ******************************************************
		case 'IN','NOT IN'
		// ******************************************************
			// did they build the parens and quotes for me?
			if structure_criteria then
				// Translate the criteria and structure id into a where clause
				value1 = uf_structure_value(structure_id, value1)
				
				if mid(value1,1,5) = 'ERROR' then return value1
			end if
		
			if mid(trim(value1),1,1) = '(' then
				// Parens=Yes, did they build the quotes for me too?
				if mid(trim(mid(trim(value1),2,100)),1,1) = "'" then
					// Good syntax
				elseif upper(mid(trim(mid(trim(value1),2,100)),1,6)) = "SELECT" then
					// Scalar - Good syntax
				else
					// Need to add quotes
					value1 = trim(value1)
					value1 = f_replace_string(value1,",","','","all")
					value1 = "('" + mid(value1,2,len(value1)-2) + "')"
				end if
			else
				// Parens=No, did they build the quotes for me too?
				if mid(trim(value1),1,1) = "'" then
					// have quotes, just need parens
					value1 = "(" + trim(value1) + ")"
				elseif (mid(trim(value1),1,6)) = "SELECT" then
					// have scalar, just need parens
					value1 = "(" + trim(value1) + ")"
				else
					// Need to add quotes and parens
					value1 = trim(value1)
					value1 = f_replace_string(value1,",","','","all")
					value1 = "('" + value1 + "')"
				end if
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case 'LIKE','NOT LIKE'
		// ******************************************************
			
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value1), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value1 + ')'
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
				
			// Did they build the wildcards for me?
			if pos(value1,'%') > 0 then
				// yes
			else
				// no, double side wildcard
				value1 = "'%' || " + value1 + " || '%'"
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case '<NONE>'
		// ******************************************************
			// Just take col_name exactly as it.  Allows for exists, not exists, etc.
			operator = ''
			value1 = ''
			between_and = ''
			value2 = ''
	end choose
	
	if i = num_rows then and_or = ''
	
	if trim(left_paren) = '(' then left_paren_count++
	if trim(right_paren) = ')' then right_paren_count++
	
	sql_where += left_paren + " " + col_name + " " + operator + " " + value1 + " " +  between_and + " " + value2 + " " + right_paren + " " + and_or + " "
next

if left_paren_count <> right_paren_count then return 'ERROR:  Parentheses count mismatch.  Left Count=' + string(left_paren_count) + ', Right Count= ' + string(right_paren_count)

return sql_where

end function

private function string uf_structure_value (longlong a_structure_id, string a_rollups);//******************************************************************************************
//
//  Window Function  :  wf_structure_value
//
//  Description      :  Builds the piece of the where clause for hierarchical queries
//								that use the cr_structures.
//
//******************************************************************************************
string where_clause, rollups_array[], rollup_value_list
longlong i, the_last_comma_pos, status

//-------  Parse out the rollup values into an array, if the user selected more  -----------
//-------  than 1 rollup value.                                                  -----------
// remove the first "{" then will use f_parsestringintostringarray
a_rollups = mid(trim(a_rollups),2)
f_parsestringintostringarray(a_rollups, "{", rollups_array)

for i = 1 to upperbound(rollups_array)
	// Value will be like "CWIP}" or "CWIP,15}".  Strip the trailing "}"
	rollups_array[i] = mid(rollups_array[i],1,len(rollups_array[i]) - 1)
	
	// Parse out the value found on the structure
	if i_use_new_structures_table = "YES" then
		// Take the value as is -- commas don't matter.  Just strip the last character.
		//  "CWIP}" goes to "CWIP"
	else
		// Just need the ID at the end.  Parse it out.
		the_last_comma_pos = lastpos(rollups_array[i], ",")
		rollups_array[i] = mid(rollups_array[i],the_last_comma_pos + 1)
	end if

	status = 0
	if i_use_new_structures_table = "YES" then
		select status into :status from cr_structure_values2
		 where structure_id = :a_structure_id and element_value = :rollups_array[i];
	else
		select status into :status from cr_structure_values
		 where structure_id = :a_structure_id and value_id = :rollups_array[i];
	end if
	
	if sqlca.sqlnrows = 0 then
		return "ERROR: The value (" + rollups_array[i] + ") does not exist on the structure (ID: " + string(a_structure_id) + ")"
	elseif isnull(status) or status = 0 then
		return "ERROR: The value (" + rollups_array[i] + ") is INACTIVE on the structure (ID: " + string(a_structure_id) + ")"
	end if
	
	if i_use_new_structures_table = "YES" then
		rollups_array[i] = "'" + rollups_array[i] + "'"
	end if

	rollup_value_list += rollups_array[i] + ', '
next

// Trim off the trailing comma
rollup_value_list = mid(rollup_value_list,1,len(rollup_value_list) - 2)

if i_use_new_structures_table = "YES" then
	where_clause = &
		"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) " + &
		" from cr_structure_values2 " + &
		" where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
		" start with structure_id = " + string(a_structure_id) + " and element_value in (" + rollup_value_list + ") " + &
		" connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + ")"
else
	where_clause = &
		"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) " + &
		" from cr_structure_values " + &
		" where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
		" start with structure_id = " + string(a_structure_id) + " and value_id in (" + rollup_value_list + ") " + &
		" connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + ")"
end if
			
return where_clause
end function

on uo_cr_where_from_ds.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_where_from_ds.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

