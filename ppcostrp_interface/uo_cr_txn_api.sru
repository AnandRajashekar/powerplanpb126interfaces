HA$PBExportHeader$uo_cr_txn_api.sru
forward
global type uo_cr_txn_api from nonvisualobject
end type
end forward

global type uo_cr_txn_api from nonvisualobject
end type
global uo_cr_txn_api uo_cr_txn_api

type variables
string i_data_set, i_table_name, i_stg_table_name, i_api_table_name, i_api_arc_table_name, i_batch_id
string i_batch_list[]

longlong i_source_id, i_interface_id

boolean i_debug

uo_cr_validation i_uo_cr_validations
uo_cr_derivation i_uo_cr_derivation
uo_ds_top i_ds_elements, i_ds_columns, i_cr_deriver_type_sources, i_cr_deriver_override2


end variables

forward prototypes
public subroutine of_logmessage (string a_msg, boolean a_error_msg_yn)
public function longlong of_gettablenames (longlong a_source_id)
public function longlong of_clearintercorecords ()
public function longlong of_priorvalidationkickouts ()
public function longlong of_markrecords (string a_upload_indicator)
public function longlong of_getbatchid ()
public function longlong of_balanceapitostg ()
public function longlong of_applymonthperiod ()
public function longlong of_getinterfaceid ()
public function longlong of_intercobalancing ()
public function longlong of_balancebatchbycompany ()
public function longlong of_insertfromstgtodetail ()
public function longlong of_clearstgtable ()
public function longlong of_loadcrinterfacedates ()
public subroutine of_buildbatchlist ()
public function longlong of_insertfromapitostg ()
public function longlong of_populateorigfields ()
public function longlong of_archiveapitable ()
public function longlong of_derivations ()
public function longlong of_read (string a_data_set, longlong a_source_id, boolean a_balance, boolean a_post_to_projects, boolean a_post_to_gl)
public function longlong of_summarize (boolean a_post_to_projects, boolean a_post_to_gl)
end prototypes

public subroutine of_logmessage (string a_msg, boolean a_error_msg_yn);
if a_error_msg_yn then
	f_pp_msgs("  ")
end if

f_pp_msgs(a_msg)

if a_error_msg_yn then
	f_pp_msgs("  ")
end if
end subroutine

public function longlong of_gettablenames (longlong a_source_id);//*****************************************************************************************
//
//  Function     :  of_getTableNames
//
//	 Parameters	  : longlong 	 : (a_source_id) 
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//	 Note			  :  
//
//*****************************************************************************************

select lower(trim(table_name)) into :i_table_name
  from cr_sources
 where source_id = :a_source_id;
 
if isnull(i_table_name) or i_table_name = "" then
	of_logMessage("ERROR: Retrieving TABLE NAME from cr_sources for source id " + string(a_source_id), true)
	return -1
end if

// Staging table name: CR_<SOURCE>_STG
i_stg_table_name = i_table_name + "_stg"

// API Table names: CRA_<SOURCE>_STG, CRA_<SOURCE>_ARC
i_api_table_name = left(i_table_name,2) + "a" + right(i_table_name,len(i_table_name)-2) + "_stg"
i_api_arc_table_name = left(i_table_name,2) + "a" + right(i_table_name,len(i_table_name)-2) + "_arc"

i_source_id = a_source_id

return 1
end function

public function longlong of_clearintercorecords ();//*****************************************************************************************
//
//  Function     :  of_clearIntercoRecords
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls

of_logMessage("Clearing any prior intercompany transactions at " + string(now()), false)

sqls = "delete from " + i_stg_table_name + &
		 " where drilldown_key = 'INTERCO' " + &
		 "   and api_data_set = '" + i_data_set + "'"

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Deleting prior intercompany transactions from " + i_stg_table_name + ": " + sqlca.SQLErrText, true)
	return -1
end if

return 1
end function

public function longlong of_priorvalidationkickouts ();//*****************************************************************************************
//
//  Function     :  of_read
//
//	 Parameters	  : (none) 
//
//	 Returns		  :  0		 : No prior validation kickouts or records left over in the staging table
//						 -1		 :	An error occurred during processing					
//						 else     : The interface batch id for the records left in the staging table 
//
//*****************************************************************************************
string sqls
any results[]
longlong num_rows, rtn

of_logMessage("Checking cr_validations_invalid_ids for prior validation kickouts at " + string(now()), false)
	
num_rows = 0
select count(*) into :num_rows
from cr_validations_invalid_ids
where lower(table_name) = :i_stg_table_name;

if num_rows > 0 then
	//  THERE WERE VALIDAITON KICKOUTS IN THE PREVIOUS RUN.
	of_logMessage("There are oustanding records from a previous run.  The interface will finish processing the existing records before new data is loaded.", true)
	
	//	Get the batch_id from the previous run to pass to validations
	sqls = "select min(abs(interface_batch_id)) from " + i_stg_table_name
	rtn = f_get_column(results[],sqls)
	
	if rtn <> 1 then
		of_logMessage("ERROR: Could not get the batch_id from the staging table: " + sqlca.SQLErrText, true)
		of_logMessage("SQL: " + sqls, true)
		return -1
	else
		i_batch_id = string(results[1])
	end if
	
	return long(i_batch_id)
else	
	//  THERE WERE NO CR VALIDATION KICKOUTS IN THE PREVIOUS RUN.  HOWEVER ...
	//   If there are records in staging table, use that batch_id.
	//  These would be due to other errors in the interface.  

	sqls = "select count(*) from " + i_stg_table_name
	rtn = f_get_column(results[],sqls)
	
	if rtn <> 1 then
		of_logMessage("ERROR: Checking record count from staging table: " + sqlca.SQLErrText, true)
		of_logMessage("SQL: " + sqls, true)
		return -1
	end if
	
	if results[1] > 0 then
		//	Get the batch_id from the previous run to pass to validations
		sqls = "select min(abs(interface_batch_id)) from " + i_stg_table_name
		rtn = f_get_column(results[],sqls)
		
		if rtn <> 1 then
			of_logMessage("ERROR: Could not get the batch_id from the staging table (2): " + sqlca.SQLErrText, true)
			of_logMessage("SQL: " + sqls, true)
			return -1
		else
			i_batch_id = string(results[1])
		end if
		
		//  THERE WERE VALIDAITON KICKOUTS IN THE PREVIOUS RUN.
		of_logMessage("There are oustanding records from a previous run.  The interface will finish processing the existing records before new data is loaded.", true)
		
		return long(i_batch_id)
	end if
	
end if


return 0
end function

public function longlong of_markrecords (string a_upload_indicator);//*****************************************************************************************
//
//  Function     :  of_markRecords
//
//	 Parameters	  : string 	 : (a_upload_indicator) 
//
//	 Returns		  :  0		 : No prior validation kickouts or records left over in the staging table
//						 -1		 :	An error occurred during processing					
//						 else     : The interface batch id for the records left in the staging table 
//
//*****************************************************************************************
string sqls
longlong counter, rtn
any results[]

counter = 0
choose case a_upload_indicator
	case 'I'
		of_logMessage("Marking records to be read from " + i_api_table_name + " at " + string(now()), false)
		// Mark records to be read for processing
		sqls = "update " + i_api_table_name + &
				 "   set upload_indicator = 'I' " + &
				 " where nvl(upload_indicator, 'N') = 'N' " + &
				 "   and api_data_set = '" + i_data_set + "'"
				 
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			of_logMessage("ERROR: Updating " + i_api_table_name + ".upload_indicator to 'I': " + sqlca.SQLErrText, true)
			return -1
		end if
		
		// Are there records to process?
		sqls = "select count(*) from " + i_api_table_name + " where upload_indicator = 'I' and api_data_set = '" + i_data_set + "'"
		rtn = f_get_column(results[],sqls)
		
		if rtn <> 1 then
			of_logMessage("ERROR: Could not retrieve the number of records in " + i_api_table_name, true)
			return -1
		else
			counter = long(results[1])
			if isnull(counter) then counter = 0
			
			if counter = 0 then
				of_logMessage("There are no records in " + i_api_table_name + " waiting to be posted to the CR.", true)
			end if
		end if
	case 'D'
		of_logMessage("Marking records as read from " + i_api_table_name + " at " + string(now()), false)
		// Mark records as read
		// ### 11277: JAK: 2012-01-12: Update the interface batch id so there is a link from archive to detail
		sqls = "update " + i_api_table_name + &
				 "   set upload_indicator = 'D', interface_batch_id = '" + i_batch_id + "' " + &
				 " where nvl(upload_indicator, 'N') = 'I' " + &
				 "   and api_data_set = '" + i_data_set + "'"
				 
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			of_logMessage("ERROR: Updating " + i_api_table_name + ".upload_indicator to 'D': " + sqlca.SQLErrText, true)
			return -1
		else
			counter = sqlca.sqlnrows
			if isnull(counter) then counter = 0
		end if
	case else
		of_logMessage("ERROR: Unknown upload_indicator passed into of_markRecords", true)
		return -1
end choose

return counter
end function

public function longlong of_getbatchid ();//*****************************************************************************************
//
//  Function     :  of_read
//
//	 Parameters	  : longlong 	 : (a_source_id) 
//
//	 Returns		  : 1			 : Success
//						 else     : An error occurred during processing
//
//	 Note			  :  
//
//*****************************************************************************************

select costrepository.nextval into :i_batch_id from dual;
if isnull(i_batch_id) then i_batch_id = '0'

if i_batch_id = '0' then
	of_logMessage("ERROR: Could not get a batch_id from costrepository.nextval", true)
	return -1
end if

return 1
end function

public function longlong of_balanceapitostg ();//*****************************************************************************************
//
//  Function     :  of_balanceAPItoSTG
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls_api, sqls_stg, rtns
longlong count_api, count_stg
dec {2} amount_api, amount_stg

uo_ds_top ds_results_api, ds_results_stg

of_logMessage("Balancing " + i_api_table_name + " to " + i_stg_table_name + " at " + string(now()), false)

// API
sqls_api = "select count(*), sum(nvl(amount,0)) from " + i_api_table_name + &
		 	  " where upload_indicator = 'I' " + &
			  "   and api_data_set = '" + i_data_set + "'"
ds_results_api = CREATE uo_ds_top

rtns = f_create_dynamic_ds(ds_results_api,'grid',sqls_api,sqlca,true)

if rtns <> "OK" then
	of_logMessage("ERROR: Creating ds_results_api: " + ds_results_api.i_sqlca_sqlerrtext, true)
	return -1
end if

count_api  = ds_results_api.GetItemNumber(1,1)
amount_api = ds_results_api.GetItemNumber(1,2)
if isnull(count_api) then count_api = 0
if isnull(amount_api) then amount_api = 0
		 
// STG		 
sqls_stg = "select count(*), sum(nvl(amount,0)) from " + i_stg_table_name + &
		 	  " where interface_batch_id = '" + i_batch_id + "'"
ds_results_stg = CREATE uo_ds_top

rtns = f_create_dynamic_ds(ds_results_stg,'grid',sqls_stg,sqlca,true)

if rtns <> "OK" then
	of_logMessage("ERROR: Creating ds_results_stg : " + ds_results_stg.i_sqlca_sqlerrtext, true)
	return -1
end if

count_stg  = ds_results_stg.GetItemNumber(1,1)
amount_stg = ds_results_stg.GetItemNumber(1,2)
if isnull(count_stg) then count_stg = 0
if isnull(amount_stg) then amount_stg = 0

// Do they balance?
if count_api <> count_stg or amount_api <> amount_stg then
	of_logMessage("ERROR: " + i_api_table_name + " and " + i_stg_table_name + " are out of balance!", true)
	of_logMessage(" -- " + i_api_table_name + " has a record count = " + string(count_api) + " and a dollar amount = " + string(amount_api) + ".", false)
	of_logMessage(" -- " + i_stg_table_name + " has a record count = " + string(count_stg) + " and a dollar amount = " + string(amount_stg) + ".", false)
	of_logMessage("The staged data will not be loaded into the CR!", true)
	return -1
end if

DESTROY ds_results_api
DESTROY ds_results_stg

return 1
end function

public function longlong of_applymonthperiod ();//*****************************************************************************************
//
//  Function     :  of_applyMonthPeriod
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls

of_logMessage("Updating month period at " + string(now()), false)

//	 Increment the month_period (existing months) on cr_interface_month_period.
sqls  = "update cr_interface_month_period a "
sqls += "   set month_period = month_period + 1 "
sqls += " where source_id = " + string(i_source_id) + " "
sqls += "   and exists ("
sqls += "		select 1 from " + i_stg_table_name + " b "
sqls += "		 where a.month_number = b.month_number "
sqls += "			and b.interface_batch_id = '" + i_batch_id + "' "
sqls += "			and b.month_period = 0) "
	
execute immediate :sqls;
	
if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Updating cr_interface_month_period (existing months) : " + sqlca.sqlerrtext, true)
	return -1
end if

//  Insert new month numbers and their corresponding month periods.
sqls  = "insert into cr_interface_month_period (source_id, month_number, month_period) "
sqls += "select distinct " + string(i_source_id) + ", month_number, 1 "
sqls += "  from " + i_stg_table_name + " a "
sqls += " where month_period = 0 "
sqls += "   and interface_batch_id = '" + i_batch_id + "'"
sqls += "   and not exists (select 1 from cr_interface_month_period b where a.month_number = b.month_number and b.source_id = " + string(i_source_id) + ") "
	
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Inserting into cr_interface_month_period (new months): " + sqlca.sqlerrtext, true)
	return -1
end if  

//  Update the month period (existing months) on staging table.
sqls  = "update " + i_stg_table_name + " a "
sqls += "   set month_period = ( "
sqls += "  		select b.month_period  "
sqls += "  		  from cr_interface_month_period b "
sqls += "  		 where a.month_number = b.month_number "
sqls += "  			and b.source_id = " + string(i_source_id) + ") "
sqls += " where month_period = 0 "
sqls += "   and interface_batch_id = '" + i_batch_id + "'"
	
execute immediate :sqls;
	
if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Updating " + i_stg_table_name + ".month_period: " + sqlca.sqlerrtext, true)
	return -1
end if

return 1
end function

public function longlong of_getinterfaceid ();//*****************************************************************************************
//
//  Function     :  of_read
//
//	 Parameters	  : (none)
//
//	 Returns		  : 1			 : Success
//						 else     : An error occurred during processing
//
//	 Note			  :  
//
//*****************************************************************************************
string src_descr

of_logMessage("Retrieving interface id for source id " + string(i_source_id) + " at " + string(now()), false)

select description into :src_descr
  from cr_sources
 where source_id = :i_source_id;
 
if isnull(src_descr) or src_descr = "" then
	of_logMessage("ERROR: Retreiving source description for source id " + string(i_source_id), true)
	return -1
end if

i_interface_id = 0
select interface_id into :i_interface_id
  from cr_interfaces
 where lower(description) = lower(:src_descr)||': '||lower(:i_data_set);

if isnull(i_interface_id) then i_interface_id = 0

if i_interface_id = 0 then
	of_logMessage("ERROR: No '" + src_descr + ": " + i_data_set + "' entry exists in cr_interfaces" , true)
	return -1
end if

return 1
end function

public function longlong of_intercobalancing ();//*****************************************************************************************
//
//  Function     :  of_intercoBalancing
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//  UO_CR_INTERCO_BALANCING.UF_INTERCO_BALANCING Return Codes  :
//    -1  =  source_id not found for a_table_name
//    -2  =  company field not found in cr_system_control
//    -3  =  account field not found in cr_system_control
//    -4  =  error on insert
//
//*****************************************************************************************
string sqls, rtns
longlong num_rows, i, rtn

select count(*) into :num_rows
  from cr_interco_balancing;
  
if isnull(num_rows) then num_rows = 0

if num_rows > 0 then
	of_logMessage("Running Interco Balancing at " + string(now()), false)
	
	sqls = "select distinct month_number, month_period from " + i_stg_table_name + &
			 " where interface_batch_id = '" + i_batch_id + "'"
	
	uo_ds_top ds_months
	ds_months = create uo_ds_top
	rtns = f_create_dynamic_ds(ds_months,'grid',sqls,sqlca,true)
	
	if rtns <> 'OK' then
		of_logMessage("ERROR: Creating ds_months: " + ds_months.i_sqlca_sqlerrtext, true)
		return -1
	end if
	
	uo_cr_interco_balancing l_uo_cr_interco_balancing
	l_uo_cr_interco_balancing = CREATE uo_cr_interco_balancing
		
	for i = 1 to ds_months.rowcount()
		of_logMessage(" -- Month: " + string(ds_months.getitemnumber(i,'month_number')), false)
		of_logMessage(" -- Month Period: " + string(ds_months.getitemnumber(i,'month_period')), false)
		
		//Start Inter Co Balancing
		rtn = l_uo_cr_interco_balancing.uf_interco_balancing_x_charge(&
					i_stg_table_name, & 	
					"interface_batch_id", &
					i_batch_id, &
					ds_months.getitemnumber(i,'month_number'), &
					ds_months.getitemnumber(i,'month_period'))
					
		if rtn < 0 then
			// Message logged by uo_cr_interco_balancing
			return -1
		end if
	next
	
	DESTROY ds_months
end if

return 1
end function

public function longlong of_balancebatchbycompany ();//*****************************************************************************************
//
//  Function     :  of_balanceBatchByCompany
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls, company_field
longlong counter, rtn
any results[]

select upper(trim(control_value)) 
  into :company_field
  from cr_system_control 
 where upper(control_name) = 'COMPANY FIELD';

if trim(company_field) = '' or isnull(company_field) then
	of_logMessage("ERROR: Getting company field from CR System Control!", true)
	return -1
else
	company_field = f_cr_clean_string(company_field)
end if 

of_logMessage("Balancing the batch by " + company_field + " at " + string(now()), false)

counter = 0
sqls = "select count(*) from ( " + &
		 "   select " + company_field + ", gl_journal_category, month_number, sum(amount) " + &
		 "     from " + i_stg_table_name + &
		 "	   where interface_batch_id = '" + i_batch_id + "' " + &
		 "    group by " + company_field + ", gl_journal_category, month_number " + &
		 "   having sum(amount) <> 0) "
		 
rtn = f_get_column(results[],sqls)
		
if rtn < 0 then
	of_logMessage("ERROR: Balancing batch by " + company_field + " in " + i_stg_table_name, true)
	return -1
else
	counter = long(results[1])
	if isnull(counter) then counter = 0
	
	if counter > 0 then
		of_logMessage("ERROR: The batch of transactions does not balance by " + company_field + ", GL Journal Category, and Month Number!", true)
		return 0
	end if
end if

return 1
end function

public function longlong of_insertfromstgtodetail ();//*****************************************************************************************
//
//  Function     :  of_insertFromSTGtoDetail
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls

of_logMessage("Inserting into " + i_table_name + " from " + i_stg_table_name + " at " + string(now()), false)

sqls = "insert into " + i_table_name + &
		 " select * from " + i_stg_table_name + " where interface_batch_id = '" + i_batch_id + "' "
		 
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Insert into " + i_table_name + ": " + sqlca.SQLErrText, true)
	return -1
end if


return 1
end function

public function longlong of_clearstgtable ();//*****************************************************************************************
//
//  Function     :  of_clearSTGTable
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls
longlong rtn
any results[]

of_logMessage("Deleting records from " + i_stg_table_name + " at "+ string(now()), false)

sqls = "delete from " + i_stg_table_name + " where interface_batch_id = '" + i_batch_id + "' "

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Deleting records from " + i_stg_table_name + " for interface_batch_id '" + i_batch_id + "': " + sqlca.SQLErrText, true)
	of_logMessage("The records must be removed manually before the interface runs again!", false)
	return -1
else
	// If the staging table is empty now, truncate the table
	sqls = "select count(*) from " + i_stg_table_name
	rtn = f_get_column(results[], sqls)
	
	if rtn > 0 then
		if long(results[1]) = 0 then sqlca.truncate_table(i_stg_table_name)
	end if
end if


return 1
end function

public function longlong of_loadcrinterfacedates ();//*****************************************************************************************
//
//  Function     :  of_loadCRInterfaceDates
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls

of_logMessage("Inserting into cr_interface_dates at " + string(now()), false)

sqls = "insert into cr_interface_dates (" + &
		 "   interface_id, month_number, month_period, process_date, total_dollars, total_records, " + &
		 "   feeder_system_id, total_debits, total_credits) " + &
		 "select " + string(i_interface_id) + ", month_number, month_period, sysdate, sum(amount), count(*),  " + &
		 "       interface_batch_id, sum(decode(sign(amount),1,amount,0)), sum(decode(sign(amount),-1,amount,0)) " + &
		 "  from " + i_table_name + " " + &
		 " where interface_batch_id = '" + i_batch_id + "' " + &
		 " group by month_number, month_period, interface_batch_id"

execute immediate :sqls;
	
if sqlca.SQLCode < 0 then
	of_logMessage("ERROR: Inserting into cr_interface_dates: " + sqlca.SQLErrText, true)
	of_logMessage("The interface records WERE committed.", false)
	return -1
else
	if sqlca.SQLNRows = 0 then
		//  No rows were inserted, meaning that there were no transactions.
		//  Still need to insert a record into cr_interfaces. 
		insert into cr_interface_dates
			(interface_id, month_number, month_period, process_date, total_dollars, total_records, feeder_system_id)
		values
			(:i_interface_id, 0, 0, sysdate, 0, 0, :g_batch_id || ' - No Transactions to Post');
	
		if sqlca.SQLCode < 0 then
			of_logMessage("ERROR: Inserting into cr_interface_dates: " + sqlca.SQLErrText, true)
			of_logMessage("The interface records WERE committed.", false)
			return -1
		end if
	end if
end if

return 1
end function

public subroutine of_buildbatchlist ();//*****************************************************************************************
//
//  Function     :  of_buildBatchList
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls
longlong num_rows, rtn, i
any results[]
boolean b_records = false

of_logMessage("Checking for prior validation kickouts at " + string(now()), false)
	
num_rows = 0
select count(*) into :num_rows
from cr_validations_invalid_ids
where lower(table_name) = :i_stg_table_name;

if num_rows > 0 then
	//  THERE WERE VALIDAITON KICKOUTS IN THE PREVIOUS RUN.
	b_records = true
end if

sqls = "select distinct interface_batch_id from " + i_stg_table_name + " where api_data_set = '" + i_data_set + "'"

rtn = f_get_column(results[], sqls)

if rtn < 0 then
	of_logMessage("ERROR: Retreiving distinct list of interface batch id's in " + i_stg_table_name, true)
	return
end if

for i = 1 to upperbound(results)
	i_batch_list[i] = string(results[i])
	b_records = true
next

// Make sure that any records in the API table that haven't been processed can be
i_batch_list[upperbound(results) + 1] = 'NEW'

if b_records then
	of_logMessage("There are outstanding records from a previous run.  The interface will finish processing the existing records before new data is loaded.", true)
end if

return
end subroutine

public function longlong of_insertfromapitostg ();//*****************************************************************************************
//
//  Function     :  of_insertFromAPItoSTG
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls, insert_sqls, select_sqls, rtns, field, default_value
longlong i, num_elements

of_logMessage("Inserting from " + i_api_table_name + " into " + i_stg_table_name + " at " + string(now()), false)

num_elements = i_ds_elements.rowcount()

// Build datastore of columns on the API table
sqls = "select lower(column_name) column_name from all_tab_columns " + &
		 "  where table_name = '" + upper(i_stg_table_name) + "' " + &
		 " order by column_id"
		 
i_ds_columns.SetSQLSelect(sqls)
i_ds_columns.Retrieve()

// Build Insert and Select SQLS starting with CR Elements
insert_sqls = "id, "
select_sqls = "crdetail.nextval, "
for i = 1 to num_elements
	field = i_ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)
	
	default_value = i_ds_elements.GetItemString(i, "default_value")
	if isnull(default_value) or default_value = "" then default_value = " "
	
	insert_sqls += field + ", "
	select_sqls += "nvl(" + field + ",'" + default_value + "'), "
next

// Build the insert for the rest of the columns
for i = 1 to i_ds_columns.rowcount()
	field = i_ds_columns.GetItemString(i, "column_name")
	
	Choose case upper(field)
		case 'ID','TIME_STAMP','USER_ID','DRILLDOWN_KEY','CWIP_CHARGE_STATUS',&
			  'CR_DERIVATION_ROLLUP','CR_TXN_TYPE','CR_DERIVATION_RATE','CR_DERIVATION_ID','CR_DERIVATION_STATUS'
			// any values in these fields should be ignored
			continue
		case 'LEDGER_SIGN'
			insert_sqls += field + ", "
			select_sqls += "1, "
		case 'MONTH_PERIOD'
			insert_sqls += field + ", "
			select_sqls += "0, "
		// ### 11679: JAK: 2013-01-06:  DR CR ID automation so external system doesnt have to provide the value
		case 'DR_CR_ID'
			insert_sqls += field + ", "
			select_sqls += "decode(sign(amount),-1,-1,1), "
		case 'INTERFACE_BATCH_ID'
			insert_sqls += field + ", "
			select_sqls += "'" + i_batch_id + "', "
		case else
			// determine whether to_column is part of the ACK		
			if i >= 2 and i <= num_elements + 1 then
				// it is an ACK element, skip it
				continue
			end if
			
			insert_sqls += field + ", "
			select_sqls += field + ", "
	end Choose
next

insert_sqls = left(insert_sqls, len(insert_sqls) - 2)
select_sqls = left(select_sqls, len(select_sqls) - 2)

// Build the Insert statement
sqls  = "insert into " + i_stg_table_name + " ( "
sqls += insert_sqls + ")"
sqls += "select " + select_sqls
sqls += "  from " + i_api_table_name
sqls += " where upload_indicator = 'I' " 
sqls += "   and api_data_set = '" + i_data_set + "'"

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Inserting into " + i_stg_table_name + " from " + i_api_table_name + ": " + sqlca.SQLErrText, true)
	return -1
end if

return 1
end function

public function longlong of_populateorigfields ();//*****************************************************************************************
//
//  Function     :  of_populateOrigFields
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string orig_list, field, sqls
longlong i, num_elements, counter

of_logMessage("Updating orig fields at " + string(now()), false)

num_elements = i_ds_elements.rowcount()

for i = 1 to num_elements
	field = i_ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)
	
	counter = 0
	select count(*) into :counter
	  from all_tab_columns
	 where table_name = upper(:i_table_name)
	   and column_name = 'ORIG_'||upper(:field);
		
	if isnull(counter) then counter = 0
	
	if counter > 0 then
		orig_list += 'orig_' + field + " = " + field + ","
	end if
next

orig_list = left(orig_list, len(orig_list) - 1)

// ### DMJ: Maint 10981: 8/29/12
if isnull(orig_list) or trim(orig_list) = "" then
	//  Do nothing
else
	sqls = "update " + i_stg_table_name + &
			 "   set " + orig_list + &
			 " where interface_batch_id = '" + i_batch_id + "'"
			 
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		of_logMessage("ERROR: Updating orig fields " + i_stg_table_name + ": " + sqlca.SQLErrText, true)
		return -1
	end if
end if


return 1
end function

public function longlong of_archiveapitable ();//*****************************************************************************************
//
//  Function     :  of_archiveAPITable
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
string sqls, insert_sqls, select_sqls, rtns, field, default_value
longlong i, num_elements, rtn
any results[]

of_logMessage("Archiving " + i_api_table_name + " at " + string(now()), false)

num_elements = i_ds_elements.rowcount()

// Build datastore of columns on the API table
sqls = "select lower(column_name) column_name from all_tab_columns " + &
		 "  where table_name = '" + upper(i_api_arc_table_name) + "' " + &
		 " order by column_id"
		 
i_ds_columns.SetSQLSelect(sqls)
i_ds_columns.Retrieve()

// Build Insert and Select SQLS starting with CR Elements
insert_sqls = ""
select_sqls = ""

// Build the insert for the rest of the columns
for i = 1 to i_ds_columns.rowcount()
	field = i_ds_columns.GetItemString(i, "column_name")
	
	Choose case upper(field)
		case 'ARCHIVE_DATETIME'
			insert_sqls += field + ", "
			select_sqls += "sysdate, "
		case else
			insert_sqls += field + ", "
			select_sqls += field + ", "
	end Choose
next

insert_sqls = left(insert_sqls, len(insert_sqls) - 2)
select_sqls = left(select_sqls, len(select_sqls) - 2)

// Build the Insert statement
sqls  = "insert into " + i_api_arc_table_name + " ( "
sqls += insert_sqls + ")"
sqls += "select " + select_sqls
sqls += "  from " + i_api_table_name
sqls += " where upload_indicator = 'D' " 
sqls += "   and api_data_set = '" + i_data_set + "'"

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Inserting into " + i_api_arc_table_name + " from " + i_api_table_name + ": " + sqlca.SQLErrText, true)
	of_logMessage("The interface records were committed and the data in " + i_api_table_name + " must be archived by hand before re-running the interface!", true)
	return -1
end if

// Delete from the API table where upload_indicator = 'D'
sqls = "delete from " + i_api_table_name + &
		 " where upload_indicator = 'D' " + &
		 "   and api_data_set = '" + i_data_set + "'"
		 
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	of_logMessage("ERROR: Deleting from " + i_api_table_name + ": " + sqlca.SQLErrText, true)
	of_logMessage("The interface records were committed and the data in " + i_api_table_name + " must be removed by hand before re-running the interface!", true)
	return -1
else
	// If the staging table is empty now, truncate the table
	sqls = "select count(*) from " + i_api_table_name
	rtn = f_get_column(results[], sqls)
	
	if rtn > 0 then
		if long(results[1]) = 0 then sqlca.truncate_table(i_api_table_name)
	end if
end if

return 1
end function

public function longlong of_derivations ();//*****************************************************************************************
//
//  Function     :  of_applyMonthPeriod
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//  Description  :  Calls base derivations as identified in cr_deriver_type_sources.
//
//	 cr_deriver_type_sources
//  -----------------------
//  TYPE              :  the derivation type (same as entered in cr_deriver_control)
//  SOURCE_ID         :  from cr_sources ... identifies the table to apply derivations against
//  RUN_ORDER         :  for this source_id, the order in which the derivations should run
//  BASIS             :  for update derivations,
//                          the a_join_field argument ... if multiples, comma-separate them
//                       for insert derivations,
//                          the a_join argument ... which is the where clause ... use
//                          "cr." as the prefix for the transaction table
//  INSERT_OR_UPDATE  :  'Update' or 'Insert' to identify which kind of derivation this is
//  DELIMITER         :  for update types only ... the delimiter separating the basis fields
//  OFFSET_TYPE       :  for insert types only ... the type value for the offsets ... if null
//                       then you need a separate record for the offset type (although both
//                       sets of records would be tagged as TARGET)
//  OFFSET_BASIS      :  see BASIS ... for the offset to an insert derivation
//  INCLUDE_DERIVATION_ROLLLUP  :  FUTURE DEVELOPMENT FOR SAP TYPE DERIVATIONS ???
//
//*****************************************************************************************
longlong rtn, max_id, num_types, t, select_counter, override_counter, counter
string sqls, join_fields[], lookup_table, the_type, delimiter, basis, &
       insert_or_update, offset_type, offset_basis, or_name, col_name, suppl_wc, &
		 basis_2nd_arg, basis_6th_arg, id_balancing_basis, null_str_array[]

of_logMessage("Starting Derivations at " + string(now()), false)

//**********************************************************************************************
//
// Determine whether any derivations are set up for this source
//
//**********************************************************************************************
select_counter = 0
select count(*) into :select_counter from cr_deriver_type_sources
 where source_id = :i_source_id;

if isnull(select_counter) or select_counter = 0 then
	num_types = 0
else
	sqls = "select * from cr_deriver_type_sources " + &
		    " where source_id = " + string(i_source_id) 
	i_cr_deriver_type_sources.SetSQLSelect(sqls)
	i_cr_deriver_type_sources.RETRIEVE()
	i_cr_deriver_type_sources.SetSort("run_order a")
	i_cr_deriver_type_sources.Sort()
	num_types = i_cr_deriver_type_sources.RowCount()
end if

if num_types <= 0 then
	of_logMessage("DERIVATIONS: No derivations defined for: " + i_stg_table_name, false)
	return 1 // Return, but allow to continue.
end if


//**********************************************************************************************
//
//  Loop over the derivation types
//
//**********************************************************************************************
for t = 1 to num_types
	
	the_type         = trim(i_cr_deriver_type_sources.GetItemString(t, "type"))
	basis            = trim(i_cr_deriver_type_sources.GetItemString(t, "basis"))
	insert_or_update = upper(trim(i_cr_deriver_type_sources.GetItemString(t, "insert_or_update")))
	delimiter        = i_cr_deriver_type_sources.GetItemString(t, "delimiter")
	offset_type      = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_type"))
	offset_basis     = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_basis"))
	
	if isnull(the_type)  or the_type = "" then continue // useless record
	if isnull(basis)     or basis    = "" then continue // useless record
	if isnull(delimiter)                  then delimiter    = ""
	if isnull(offset_type)                then offset_type  = "" // for g_debug below
	if isnull(offset_basis)               then offset_basis = "" // for g_debug below
	if offset_type  = "" and insert_or_update = "INSERT" then continue // useless record
	if offset_basis = "" and insert_or_update = "INSERT" then continue // useless record
	
	if i_debug then
		f_pp_msgs("type = " + the_type)
		f_pp_msgs("basis = " + basis)
		f_pp_msgs("insert_or_update = " + insert_or_update)
		f_pp_msgs("delimiter = " + delimiter)
		f_pp_msgs("offset_type = " + offset_type)
		f_pp_msgs("offset_basis = " + offset_basis)
	end if

	// Only allowing for update derivations for now...
	if insert_or_update = "INSERT" then continue
	//*******************************************************************************************
	//
	//  Update Derivations:
	//
	//*******************************************************************************************
	
	//  Parse the basis value (e.g. "company,work_order") into an array for the update function.
	join_fields = null_str_array
	f_parsestringintostringarray(basis, ",", join_fields)
	
	rtn = i_uo_cr_derivation.uf_deriver_update_mult( &
		the_type, &
		i_stg_table_name, &
		join_fields, &
		i_batch_id, &
		false, &
		delimiter)
	
	if rtn <> 1 then
		return -1
	end if
	
	continue // a derivation can't be both Update AND Insert

	//*******************************************************************************************
	//
	//  Place Holder for Insert Derivations
	//
	//*******************************************************************************************

NEXT  //  for t = 1 to num_types ...


//*******************************************************************************************
//
//  Derivation Overrides:  #2 table
//
//    Re-using variables in this section from above.
//
//*******************************************************************************************
override_counter = 0
select count(*) into :override_counter from cr_deriver_override2_sources
 where source_id = :i_source_id and rownum = 1;

//if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: override_counter = " + string(override_counter))

if isnull(override_counter) or override_counter = 0 then
	num_types = 0
else
	sqls = "select * from cr_deriver_override2_sources where source_id = " + string(i_source_id)
	i_cr_deriver_override2.SetSQLSelect(sqls)
	i_cr_deriver_override2.RETRIEVE()
	i_cr_deriver_override2.SetSort("run_order a")
	i_cr_deriver_override2.Sort()
	num_types = i_cr_deriver_override2.RowCount()
end if

if num_types <= 0 then
	of_logMessage("DERIVATIONS: No derivation overrides defined for: " + i_stg_table_name, false)
	return 1 // Return, but allow to continue.
end if

for t = 1 to num_types
	
	or_name  = trim(i_cr_deriver_override2.GetItemString(t, "override_name"))
	col_name = trim(i_cr_deriver_override2.GetItemString(t, "col_name"))
	suppl_wc = trim(i_cr_deriver_override2.GetItemString(t, "suppl_where_clause"))
	
	if isnull(or_name)  or or_name  = "" then continue // useless record
	if isnull(col_name) or col_name = "" then continue // useless record
	if isnull(suppl_wc) then suppl_wc = "" // good record
		
	if i_debug then
		f_pp_msgs("or_name = " + or_name)
		f_pp_msgs("col_name = " + col_name)
		f_pp_msgs("suppl_wc = " + suppl_wc)
	end if
	
	// Add API Data Set to suppl_wc
	suppl_wc = suppl_wc + " and api_data_set = '" + i_data_set + "'"
	
	//  Setting these to the staging_table ought to work just fine since you'd only be using
	//  these references if you're trying to update the table from itself (using the "res_table"
	//  and "src_table" keywords.  Otherwise, you'd have an override set up for allocations that
	//  probably has the "src_table" hardcoded (like work_order_control, a custom table, etc.).
	//  In all cases, I imagine, the "src table" will be referenced in the override_sql and
	//  replaced in the uf call.
	i_uo_cr_derivation.pi_s_res_table       = i_stg_table_name
	i_uo_cr_derivation.pi_s_src_table       = i_stg_table_name
	i_uo_cr_derivation.pi_s_res_arg         = i_batch_id
	i_uo_cr_derivation.i_suppl_where_clause = suppl_wc
	
	rtn = i_uo_cr_derivation.uf_deriver_update_override(or_name, col_name)
	
	if rtn <> 1 then
		return -1
	end if
NEXT  //  for t = 1 to num_types ...

of_logMessage("Derivations Complete at " + string(now()), false)

return 1
end function

public function longlong of_read (string a_data_set, longlong a_source_id, boolean a_balance, boolean a_post_to_projects, boolean a_post_to_gl);//*****************************************************************************************
//
//  Function     :  of_read
//
//	 Parameters	  : string 	 : (a_data_set) 
//
//	 Returns		  : 1			 : Success
//						 else     : An error occurred during processing
//
//	 Note			  :  
//
//*****************************************************************************************
string sqls, rtns
longlong rtn, i

// ### 11277: JAK: 2012-01-12: New balance argument to control balancing for CWIP only interfaces.
if isnull(a_balance) then a_balance = true

// The data set determines which data should be processed in the source API table
i_data_set = a_data_set

// Get the table names used by this interface
rtn = of_getTableNames(a_source_id)

if rtn < 0 then
	return -1
end if

// Build datastore of CR Elements
sqls = 'select * from cr_elements order by "ORDER"'

i_ds_elements = CREATE uo_ds_top

rtns = f_create_dynamic_ds(i_ds_elements,'grid',sqls,sqlca,true)

if rtns <> "OK" then
	of_logMessage("ERROR: Building i_ds_elements: " + i_ds_elements.i_sqlca_sqlerrtext, true)
	return -1
end if

// Build datastore of columns on the API table
sqls = "select lower(column_name) column_name from all_tab_columns " + &
		 "  where table_name = '" + upper(i_api_table_name) + "' " + &
		 " order by column_id"
		 
i_ds_columns = CREATE uo_ds_top

rtns = f_create_dynamic_ds(i_ds_columns,'grid',sqls,sqlca,true)

if rtns <> "OK" then
	of_logMessage("ERROR: Building i_ds_columns: " + i_ds_columns.i_sqlca_sqlerrtext, true)
	return -1
end if

// Build ds for cr_deriver_type_sources
sqls = "select * from cr_deriver_type_sources " + &
		    " where source_id = " + string(i_source_id) 
i_cr_deriver_type_sources = CREATE uo_ds_top

rtns = f_create_dynamic_ds(i_cr_deriver_type_sources,'grid',sqls,sqlca,true)

if rtns <> "OK" then
	of_logMessage("ERROR: Building i_cr_deriver_type_sources: " + i_cr_deriver_type_sources.i_sqlca_sqlerrtext, true)
	return -1
end if

// Build ds for cr_deriver_override2_sources
sqls = "select * from cr_deriver_override2_sources where source_id = " + string(i_source_id)
i_cr_deriver_override2 = CREATE uo_ds_top

rtns = f_create_dynamic_ds(i_cr_deriver_override2,'grid',sqls,sqlca,true)

if rtns <> "OK" then
	of_logMessage("ERROR: Building i_cr_deriver_override2: " + i_cr_deriver_override2.i_sqlca_sqlerrtext, true)
	return -1
end if

i_uo_cr_validations = CREATE uo_cr_validation
i_uo_cr_derivation  = CREATE uo_cr_derivation

// Get the interface_id from cr_interfaces
rtn = of_getInterfaceID()

if rtn < 0 then
	return -1
end if

// Clear out any intercompany records that are left over from prior validation kickouts
rtn = of_clearIntercoRecords()

if rtn < 0 then
	rollback;
	return -1
end if

// Build list of batches to process
of_buildBatchList()

if upperbound(i_batch_list) = 0 then
	of_logMessage("ERROR: Unable to build batch list.",true)
	rollback;
	return -1
end if

for i = 1 to upperbound(i_batch_list)
	// Set current batch to process
	i_batch_id = i_batch_list[i]
	
	if i_batch_id = 'NEW' then	
		of_logMessage("Processing new transactions at " + string(now()), false)
		
		// Mark records to be read and determine whether there are records to be processed
		rtn = of_markRecords('I')
		
		if rtn < 0 then
			rollback;
			return -1
		elseif rtn = 0 then
			// No records to process
			continue
		end if
		
		// Get an interface batch id if necessary
		rtn = of_getBatchID()
		
		if rtn < 0 then
			rollback;
			return -1
		end if
		
		// Insert records from the API table to the staging table
		rtn = of_insertFromAPItoSTG()
		
		if rtn < 0 then
			rollback;
			return -1
		end if
		
		// Balance records in API table to records in staging table
		rtn = of_balanceAPItoSTG()
		
		if rtn < 0 then
			rollback;
			return -1
		end if
		
		// Mark the records as read
		rtn = of_markRecords('D')
		
		if rtn < 0 then
			rollback;
			return -1
		end if
		
		// Ensure ORIG fields are populated if they exist
		rtn = of_populateOrigFields()
		
		if rtn < 0 then
			rollback;
			return -1
		end if
		
	else
		of_logMessage("Processing transactions from batch '" + i_batch_id + "' at " + string(now()), false)
	end if
	
	// Apply the Month Period
	rtn = of_applyMonthPeriod()
	
	if rtn < 0 then
		rollback;
		return -1
	end if
	
	// Update Derivations
	rtn = of_derivations()
	
	if rtn < 0 then
		rollback;
		return -1
	end if
	
	// ### 11277: JAK: 2012-01-12: New balance argument to control balancing for CWIP only interfaces.
	if a_balance then
		// Perform intercompany balancing
		rtn = of_intercoBalancing()
		
		if rtn < 0 then
			rollback;
			return -1
		end if
		
		// Ensure that the batch balances by company
		rtn = of_balanceBatchByCompany()
		
		if rtn < 0 then
			rollback;
			return -1
		elseif rtn = 0 then
			// Batch does not balance
			rollback;
			continue
		end if
	end if
	
	// Perform Intercompany Derivation??

	// Perform Validations
	of_logMessage("Analyzing " + i_stg_table_name + " table at " + string(now()), false)
	sqlca.analyze_table(i_stg_table_name)
	
	rtn = i_uo_cr_validations.uf_runValidations(i_stg_table_name, i_batch_id, a_source_id)
	
	if rtn = -1 then
		// Error occurred
		rollback;
		return -1
	elseif rtn = -2 then
		// Validation Kickouts
		continue
	end if
	
	// Insert from Stg table to Detail table
	rtn = of_insertFromSTGtoDetail()
		
	if rtn < 0 then
		rollback;
		return -1
	end if
	
	// Summarize transactions in detail table
	rtn = of_summarize(a_post_to_projects,a_post_to_gl)
	
	if rtn < 0 then
		rollback;
		return -1
	end if
	
	// Clear out the stg table
	rtn = of_clearSTGTable()
	
	if rtn < 0 then
		rollback;
		return -1
	end if
	
	// Commit
	// -- Don't want to kill the interface without loading new transactions if the only thing that fails is the insert
	//		into cr_interface_dates or archiving the API table below; both are easy to do by hand but must be done before 
	// 	the next run of the interface.
	commit;
	
	// Archive API table
	rtn = of_archiveAPITable()
	
	if rtn < 0 then
		rollback;
		return -1
	end if
	
	// Log interface statistics in cr_interface_dates
	rtn = of_loadCRInterfaceDates()
	
	if rtn < 0 then
		rollback;
		return -1
	end if
	
	commit;
next

DESTROY i_ds_elements
DESTROY i_ds_columns
DESTROY i_cr_deriver_type_sources
DESTROY i_cr_deriver_override2

DESTROY i_uo_cr_validations
DESTROY i_uo_cr_derivation

return 1
end function

public function longlong of_summarize (boolean a_post_to_projects, boolean a_post_to_gl);//*****************************************************************************************
//
//  Function     :  of_summarize
//
//	 Parameters	  : (none)
//
//	 Returns		  :  1	  : Success
//						 -1     : An error occurred during processing
//
//*****************************************************************************************
longlong rtn
string cwip_charge_status, batch_id

of_logMessage("Updating cr_cost_repository at " + string(now()), false)

uo_cr_cost_repository uo_cr
uo_cr = CREATE uo_cr_cost_repository

if a_post_to_projects then 
	cwip_charge_status = ''
else
	cwip_charge_status = '-1*id'
end if

if a_post_to_gl then
	batch_id = ''
else 
	batch_id = '-1*id'
end if

rtn = uo_cr.uf_insert_to_cr_general(i_source_id, "where interface_batch_id = '" + i_batch_id + "'",batch_id,batch_id,cwip_charge_status)

if rtn = 1 then
else
	choose case rtn
		case -1
			of_logMessage("ERROR: inserting into cr_temp_cr: " + sqlca.SQLErrText, true)
		case -2
			of_logMessage("ERROR: cr_temp_cr.id UPDATE failed: " + sqlca.SQLErrText, true)
		case -3
			of_logMessage("ERROR: cr_temp_cr.drilldown_key UPDATE failed: " + sqlca.SQLErrText, true)
		case -4
			of_logMessage("ERROR: " + i_table_name + ".drilldown_key UPDATE failed: " + sqlca.SQLErrText, true)
		case -5
			of_logMessage("ERROR: inserting into cr_cost_repository: " + sqlca.SQLErrText, true)
		case -6
			of_logMessage("ERROR: deleting from cr_temp_cr UPDATE failed: " + sqlca.SQLErrText, true)
		case else
			of_logMessage("ERROR: unknown error in uo_cr_cost_repository: " + sqlca.SQLErrText, true)
	end choose
	return -1
end if

return 1
end function

on uo_cr_txn_api.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_txn_api.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

