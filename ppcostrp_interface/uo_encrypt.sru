HA$PBExportHeader$uo_encrypt.sru
$PBExportComments$JAK 20100114:  New object to call Oracle's Encryption / Decryption Logic.
forward
global type uo_encrypt from nonvisualobject
end type
type size from structure within uo_encrypt
end type
end forward

type size from structure
	longlong	cx
	longlong	cy
end type

global type uo_encrypt from nonvisualobject
end type
global uo_encrypt uo_encrypt

type prototypes
// Functions for default column width
Function ulong GetDC ( ulong hWnd) Library "user32.dll"
Function ulong SelectObject ( ulong hdc, ulong hWnd ) Library "gdi32.dll"
Function boolean GetTextExtentPoint32 (ulong hdcr, string lpString, long nCount, Ref size size) Library "gdi32.dll" Alias For "GetTextExtentPoint32W"
Function long ReleaseDC (ulong hWnd, ulong hdcr ) Library "user32.dll"

end prototypes

type variables
window i_parent_window
end variables

forward prototypes
public function string uf_encrypt (string a_password, string a_passkey)
public function string uf_decrypt (string a_hex_password, string a_passkey)
public function string uf_blob_to_hex (blob a_blob)
public function blob uf_hex_to_blob (string a_hex)
public function string uf_byte_to_hex (byte a_byte)
end prototypes

public function string uf_encrypt (string a_password, string a_passkey);// ********************************************************************************************
//
// 	Encrypts the a_password values and returns the hex version of the 
//	encrypted password (still a string value).  The same passkey that
//	was used to encrypt the password is required to decrypt it.
//
// ********************************************************************************************

blob encrypted_blob
string encrypted_string

if mod(len(a_password),8) > 0 then
	a_password = f_rpad(a_password,8 * ceiling(len(a_password)/8),' ')
end if
if mod(len(a_passkey),8) > 0 then
	a_passkey = f_rpad(a_passkey,8 * ceiling(len(a_passkey)/8),' ')
end if

DECLARE encrypt PROCEDURE FOR 
	dbms_obfuscation_toolkit.DESEncrypt(
	input => utl_raw.cast_to_raw(:a_password), 
	key => utl_raw.cast_to_raw(:a_passkey)) using SQLCA;
EXECUTE encrypt;
if SQLCA.SqlCode < 0 then
	RETURN 'ERROR on EXECUTE: ' + sqlca.sqlerrtext
	CLOSE encrypt;
end if

FETCH encrypt INTO :encrypted_blob;
if SQLCA.SqlCode < 0 then
	RETURN 'ERROR on FETCH: ' + sqlca.sqlerrtext
	CLOSE encrypt;
end if

CLOSE encrypt;

encrypted_string = uf_blob_to_hex(encrypted_blob)
if isnull(encrypted_string) or trim(encrypted_string) = '' or mod(len(encrypted_string),16) <> 0 then
	RETURN 'ERROR in ENCRYPTION:  Encrypted string returned is blank or of an invalid length (' + encrypted_string + ')'
else
	return encrypted_string
end if
end function

public function string uf_decrypt (string a_hex_password, string a_passkey);// ********************************************************************************************
//
// 	Decrypts the a_hex_password values and returns the decrypted version 
//	of the password.  The same passkey that was used to encrypt the 
//	password is required to decrypt it.
//
// ********************************************************************************************

blob decrypted_blob
string decrypted_string


if isnull(a_hex_password) or trim(a_hex_password) = '' or mod(len(a_hex_password),16) <> 0 then
	RETURN 'ERROR in DECRYPTION:  Hex Password is blank or of an invalid length (' + a_hex_password + ')'
end if
	
if mod(len(a_passkey),8) > 0 then
	a_passkey = f_rpad(a_passkey,8 * ceiling(len(a_passkey)/8),' ')
end if

DECLARE decrypt PROCEDURE FOR 
	dbms_obfuscation_toolkit.DESDecrypt(
	input => hextoraw(:a_hex_password), 
	key => utl_raw.cast_to_raw(:a_passkey)) using SQLCA;
EXECUTE decrypt;
if SQLCA.SqlCode < 0 then
	RETURN 'ERROR on EXECUTE: ' + sqlca.sqlerrtext
	CLOSE decrypt;
end if

FETCH decrypt INTO :decrypted_blob;
if SQLCA.SqlCode < 0 then
	RETURN 'ERROR on FETCH: ' + sqlca.sqlerrtext
	CLOSE decrypt;
end if

CLOSE decrypt;

decrypted_string = uf_blob_to_hex(decrypted_blob)

select trim(utl_raw.cast_to_varchar2(hextoraw(:decrypted_string)))
	into :decrypted_string
	from dual;


if isnull(decrypted_string) or trim(decrypted_string) = '' then
	RETURN 'ERROR in DECRYPTION:  Decrypted value returned is blank'
else
	return decrypted_string
end if
end function

public function string uf_blob_to_hex (blob a_blob);byte byte_array[]
string hex
longlong i

byte_array[] = GetByteArray(a_blob)
for i = 1 to upperbound(byte_array[])
	hex += uf_byte_to_hex(byte_array[i])
next

return hex
end function

public function blob uf_hex_to_blob (string a_hex);string str
byte byte_array[], byte_value
longlong i, j

for i = 1 to len(a_hex)
	j = ceiling(i/2)
	
	choose case mid(a_hex,1,1)
		case 'A'
			byte_value = 10
		case 'B'
			byte_value = 11
		case 'C'
			byte_value = 12
		case 'D'
			byte_value = 13
		case 'E'
			byte_value = 14
		case 'F'
			byte_value = 15
		case else
			byte_value = byte(mid(a_hex,1,1))
	end choose
	
	if mod(i,2) = 0 then 
		byte_array[j] += 16 * byte_value
	else
		byte_array[j] += byte_value
	end if
next

return blob(byte_array)
end function

public function string uf_byte_to_hex (byte a_byte);string str1, str2
choose case mod(a_byte,16)
	case 10
		str1 = 'A'		
	case 11
		str1 = 'B'
	case 12
		str1 = 'C'
	case 13
		str1 = 'D'
	case 14
		str1 = 'E'
	case 15
		str1 = 'F'
	case else
		str1 = string(mod(a_byte,16))
end choose

choose case mod(truncate(a_byte/16,0),16)
	case 10
		str2 = 'A'		
	case 11
		str2 = 'B'
	case 12
		str2 = 'C'
	case 13
		str2 = 'D'
	case 14
		str2 = 'E'
	case 15
		str2 = 'F'
	case else
		str2 = string(mod(truncate(a_byte/16,0),16))
end choose

return str2 + str1


end function

on uo_encrypt.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_encrypt.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

