HA$PBExportHeader$uo_cr_validation_bdg_custom.sru
$PBExportComments$ppcostrp: combos: effective_month_number2
forward
global type uo_cr_validation_bdg_custom from nonvisualobject
end type
end forward

global type uo_cr_validation_bdg_custom from nonvisualobject
end type
global uo_cr_validation_bdg_custom uo_cr_validation_bdg_custom

type variables
boolean i_called_from_validator = false, &
		  i_called_from_allocations_suspense = false
string  i_co_field, i_use_new_structures_table, i_hint_str
boolean	i_me_autoextend

uo_ds_top i_ds_rules, i_ds_rule, i_ds_count, i_ds_control, i_ds_elements, i_ds_msg_one_col, i_ds_msg_three_col, &
	i_ds_extend, i_ds_invalid_mn, i_ds_rules_proj
longlong i_num_elements

string i_add_where, i_cv_control_or_combos, i_use_intermediate_tbl, i_log_combo_kickouts

string i_effmn_cv, i_effmn2_cv, i_effmn2_multi_rec_cv, i_cv_combo_vals, i_cv_delete_me, i_cv_use_exists, i_cv_acct_range
string i_wo_field, i_pp_co_field, i_cv_req_obey_default

end variables

forward prototypes
public function longlong uf_validate_custom (string a_table_name, string a_ifb_id)
end prototypes

public function longlong uf_validate_custom (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation_custom
//
//  UO Function  :  uf_validate_custom
//
//  Return Codes :   1 = Success
//						  -1 = SQL Error
// 					  -2 = Validation kickouts occurred
//
//*****************************************************************************************

longlong num_rows, i
string  sqls, table_name, invalid_value
boolean kickouts

datastore ds_count
ds_count = CREATE datastore

kickouts = false
	
//
//  ---------------  EXAMPLE CODE FROM PNM TO ILLUSTRATE TECHNIQUE  ---------------//
//



////  No detailed attribute validations for cr_allocations or cr_powerplant.
//if g_process_id = 6 or g_process_id = 19 then
//	goto after_detailed_attribute_validations
//end if
//
//
////
////
////****************************  DETAILED ATTRIBUTE VALIDATIONS  ***************************
////
////****  We are being very explicit about these validations and are not using dynamic  *****
////****  SQL to detect which sources have the particular detailed attribute fields.    *****
////****  Otherwise we leave ourselves open to SQL that does not compile in this        *****
////****  script and is very hard to verify and debug.                                  *****
////****  This will also speed up validations for the other sources.                    *****
////****                                                                                *****
////
//
//
////***************************
////
////  1)  EMPLOYEE
////
////***************************
//
////  Accounts Payable, Daar, Div Cash, Journal Lines, Payroll, and Transportation are the
////  only sources that have the vehicle_nbr field.
//choose case g_process_id 
//	case 5
//		table_name = "cr_accounts_payable_stg"
//	case 9
//		table_name = "cr_daar_stg"
//	case 10
//		table_name = "cr_div_cash_stg"
//	case 15
//		table_name = "cr_journal_lines_stg"
//	case 20
//		table_name = "cr_oasys_weekly_stg"
//	case 12
//		table_name = "cr_payroll_stg"
//	case 17
//		table_name = "cr_transportation_stg"
//	case else
//		//  Do nothing.
//		goto after_emplid
//end choose
//
//datastore ds_emplid
//ds_emplid = CREATE datastore
//sqls = &
//	"select emplid from " + table_name + " " + &
//	 "where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + &
// 	   "and account in (select account from pnm_emplid_accts) " + &
//		"and je_nbr not in (select je_nbr from pnm_emplid_exclude_je_nbr) minus " + &
//	"select alter_emplid from ps_pnm_cr_empl_mv"
//f_create_dynamic_ds(ds_emplid, "grid", sqls, sqlca, true)
//num_rows = ds_emplid.RowCount()
//
//if num_rows > 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: THE FOLLOWING EMPLOYEE IDS DO NOT PASS VALIDATION !")
//	f_pp_msgs("-----------------------------------------------------------------------------")
//	for i = 1 to num_rows
//		invalid_value = ds_emplid.GetItemString(i, 1)
//		if isnull(invalid_value) then invalid_value = " "
//		invalid_value = "emplid : " + invalid_value
//		f_pp_msgs(invalid_value)
//	next
//	f_pp_msgs("  ")
//	kickouts = true
//end if
//
//sqls = "insert into cr_validations_invalid_ids (table_name, id) (select " + &
//	"'" + table_name + "', id from " + table_name + " where emplid in ( " + sqls + &
//	") minus select table_name, id from cr_validations_invalid_ids)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: inserting into cr_validations_invalid_ids:" + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//DESTROY ds_emplid
//
//after_emplid:
//
//
////***************************
////
////  2)  VEHICLE NUMBER
////
////***************************
//
////  Accounts Payable, Journal Lines, Oasys Weekly, Passport, Payroll, and Transportation
////  are the only sources that have the vehicle_nbr field.
//choose case g_process_id 
//	case 5
//		table_name = "cr_accounts_payable_stg"
//	case 15
//		table_name = "cr_journal_lines_stg"
//	case 20
//		table_name = "cr_oasys_weekly_stg"
//	case 13
//		table_name = "cr_passport_stg"
//	case 12
//		table_name = "cr_payroll_stg"
//	case 17
//		table_name = "cr_transportation_stg"
//	case else
//		//  Do nothing.
//		goto after_vehicle_nbr
//end choose
//
//datastore ds_vehicle_nbr
//ds_vehicle_nbr = CREATE datastore
//sqls = &
//	"select distinct vehicle_nbr from (" + &
//	"select vehicle_nbr, gl_location, home_center from " + table_name + " " + &
//	 "where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + &
// 	   "and account in (select account from pnm_vehicle_nbr_accts) minus " + &
//	"select unit, location, home_center from pnm_valid_vehicle)"
//f_create_dynamic_ds(ds_vehicle_nbr, "grid", sqls, sqlca, true)
//num_rows = ds_vehicle_nbr.RowCount()
//
//if num_rows > 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: THE FOLLOWING VEHICLE NUMBERS DO NOT PASS VALIDATION !")
//	f_pp_msgs("-----------------------------------------------------------------------------")
//	for i = 1 to num_rows
//		invalid_value = ds_vehicle_nbr.GetItemString(i, 1)
//		if isnull(invalid_value) then invalid_value = " "
//		invalid_value = "vehicle_nbr : " + invalid_value
//		f_pp_msgs(invalid_value)
//	next
//	f_pp_msgs("  ")
//	kickouts = true
//end if
//
////  Need to completely redo this one due to the concatenation.
//sqls = "insert into cr_validations_invalid_ids (table_name, id) (select " + &
//	"'" + table_name + "', id from " + table_name + &
//	" where vehicle_nbr||gl_location||home_center in ( " + &
//	" select distinct vehicle_nbr||gl_location||home_center from ( " + &
//	"select vehicle_nbr, gl_location, home_center from cr_accounts_payable_stg " + &
//	"where account in (select account from pnm_vehicle_nbr_accts) minus " + &
//	"select unit, location, home_center from pnm_valid_vehicle) " + &
//	") minus select table_name, id from cr_validations_invalid_ids)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: inserting into cr_validations_invalid_ids:" + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//DESTROY ds_vehicle_nbr
//
//after_vehicle_nbr:
//
//
////***************************
////
////  3)  CUSTOMER ID
////
////***************************
//
////  Daar, Div Cash, Gas Master and Journal Lines are the only sources that have the 
////  customer_nbr field.
//choose case g_process_id 
//	case 9 
//		table_name = "cr_daar_stg"
//	case 10
//		table_name = "cr_div_cash_stg"
//	case 11
//		table_name = "cr_gas_master_stg"
//	case 15
//		table_name = "cr_journal_lines_stg"
//	case else
//		//  Do nothing.
//		goto after_customer_id
//end choose
//
//datastore ds_customer_id
//ds_customer_id = CREATE datastore
//sqls = &
//	"select customer_nbr from " + table_name + " " + &
//	 "where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + &
// 	   "and account in (select account from pnm_customer_id_accts) minus " + &
//	"select customer_id from pnm_valid_customer_id"
//f_create_dynamic_ds(ds_customer_id, "grid", sqls, sqlca, true)
//num_rows = ds_customer_id.RowCount()
//
//if num_rows > 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: THE FOLLOWING CUSTOMER IDS DO NOT PASS VALIDATION !")
//	f_pp_msgs("-----------------------------------------------------------------------------")
//	for i = 1 to num_rows
//		invalid_value = ds_customer_id.GetItemString(i, 1)
//		if isnull(invalid_value) then invalid_value = " "
//		invalid_value = "customer_nbr : " + invalid_value
//		f_pp_msgs(invalid_value)
//	next
//	f_pp_msgs("  ")
//	kickouts = true
//end if
//
//sqls = "insert into cr_validations_invalid_ids (table_name, id) (select " + &
//	"'" + table_name + "', id from " + table_name + " where customer_nbr in ( " + sqls + &
//	") minus select table_name, id from cr_validations_invalid_ids)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: inserting into cr_validations_invalid_ids:" + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//DESTROY ds_customer_id
//
//after_customer_id:
//
//
////***************************
////
////  4)  BANNER GL CLASS CODE
////
////***************************
//
////  Banner is the only source that has the banner_gl_class_code field.
//choose case g_process_id 
//	case 8 
//		table_name = "cr_banner_stg"
//	case else
//		//  Do nothing.
//		goto after_banner_gl_class_code
//end choose
//
//datastore ds_banner_gl_class_code
//ds_banner_gl_class_code = CREATE datastore
//sqls = &
//	"select banner_gl_class_code from " + table_name + " " + &
//	 "where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + &
// 	   "and account in (select account from pnm_banner_gl_class_code_accts) minus " + &
//	"select banner_gl_class_code from pnm_valid_banner_gl_class_code"
//f_create_dynamic_ds(ds_banner_gl_class_code, "grid", sqls, sqlca, true)
//num_rows = ds_banner_gl_class_code.RowCount()
//
//if num_rows > 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: THE FOLLOWING BANNER GL CLASS CODES DO NOT PASS VALIDATION !")
//	f_pp_msgs("-----------------------------------------------------------------------------")
//	for i = 1 to num_rows
//		invalid_value = ds_banner_gl_class_code.GetItemString(i, 1)
//		if isnull(invalid_value) then invalid_value = " "
//		invalid_value = "banner_gl_class_code : " + invalid_value
//		f_pp_msgs(invalid_value)
//	next
//	f_pp_msgs("  ")
//	kickouts = true
//end if
//
//sqls = "insert into cr_validations_invalid_ids (table_name, id) (select " + &
//	"'" + table_name + "', id from " + table_name + " where banner_gl_class_code in ( " + sqls + &
//	") minus select table_name, id from cr_validations_invalid_ids)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: inserting into cr_validations_invalid_ids:" + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//DESTROY ds_banner_gl_class_code
//
//after_banner_gl_class_code:
//
//
////***************************
////
////  5)  GAS COMPONENT
////
////***************************
//
////  Banner and Gas Master are the only sources that have the banner_gas_component field.
//choose case g_process_id 
//	case 8 
//		table_name = "cr_banner_stg"
//	case 11
//		table_name = "cr_gas_master_stg"
//	case else
//		//  Do nothing.
//		goto after_gas_component
//end choose
//
//datastore ds_gas_component
//ds_gas_component = CREATE datastore
//sqls = &
//	"select banner_gas_component from " + table_name + " " + &
//	 "where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " minus " + &
//	"select gas_component from pnm_valid_gas_component"
//f_create_dynamic_ds(ds_gas_component, "grid", sqls, sqlca, true)
//num_rows = ds_gas_component.RowCount()
//
//if num_rows > 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: THE FOLLOWING GAS COMPONENTS DO NOT PASS VALIDATION !")
//	f_pp_msgs("-----------------------------------------------------------------------------")
//	for i = 1 to num_rows
//		invalid_value = ds_gas_component.GetItemString(i, 1)
//		if isnull(invalid_value) then invalid_value = " "
//		invalid_value = "banner_gas_component : " + invalid_value
//		f_pp_msgs(invalid_value)
//	next
//	f_pp_msgs("  ")
//	kickouts = true
//end if
//
//sqls = "insert into cr_validations_invalid_ids (table_name, id) (select " + &
//	"'" + table_name + "', id from " + table_name + " where banner_gas_component in ( " + sqls + &
//	") minus select table_name, id from cr_validations_invalid_ids)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: inserting into cr_validations_invalid_ids:" + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//DESTROY ds_gas_component
//
//after_gas_component:
//
//
////
////
////*************************  END OF DETAILED ATTRIBUTE VALIDATIONS  ***********************
////
////
//
//
//after_detailed_attribute_validations:
//



//
//  ---------------                    END OF                       ---------------//
//  ---------------  EXAMPLE CODE FROM PNM TO ILLUSTRATE TECHNIQUE  ---------------//
//



if kickouts then
	return -2
else
	return 1
end if

end function

on uo_cr_validation_bdg_custom.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_validation_bdg_custom.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string sqls

//  NEED THE COMPANY FIELD IF VALUES_BY_COMPANY IS TURNED ON FOR AN ACK ELEMENT.
setnull(i_co_field)
i_co_field = upper(trim(g_cr.uf_get_control_value( 'COMPANY FIELD' )))
if isnull(i_co_field) or i_co_field = '' then i_co_field = "NONE"  //  This will make it obvious
	//  that the company field is not set up properly.

// New variable to use the master element autoextend...in interfaces it is on by default; in the app
//	it is off by default.  The autoextend messages are still logged, but the ME table is not actually updated
if g_main_application then 
	i_me_autoextend = false
else
	i_me_autoextend = true
end if

// DMJ: 02/04/10: To Avoid Mem Leaks
i_ds_count = CREATE uo_ds_top
sqls = 'select count(*) count from cr_elements where element_id = -1'
f_create_dynamic_ds(i_ds_count, "grid", sqls, sqlca, true)

i_ds_rules = CREATE uo_ds_top
sqls = "select * from cr_validation_rules"
f_create_dynamic_ds(i_ds_rules, "grid", sqls, sqlca, true)
i_ds_rules.SetSort("rule a")
i_ds_rules.Sort()

i_ds_rules_proj = CREATE uo_ds_top
sqls = "select * from cr_validation_rules_projects where active = 'Y' order by rule_id"
f_create_dynamic_ds(i_ds_rules_proj, "grid", sqls, sqlca, true)
i_ds_rules_proj.SetSort("rule a")
i_ds_rules_proj.Sort()

i_ds_rule = CREATE uo_ds_top
sqls = "select * from cr_validation_rules where -1 = 0"
f_create_dynamic_ds(i_ds_rule, "grid", sqls, sqlca, true)

i_ds_control = CREATE uo_ds_top
sqls = "select * from cr_validation_control where rule_id = 0"
f_create_dynamic_ds(i_ds_control, "grid", sqls, sqlca, true)	

i_ds_elements = g_cr.uf_get_cr_elements( '' )
i_ds_elements.SetSort("order a")
i_ds_elements.Sort()
i_num_elements = i_ds_elements.RowCount()

i_ds_msg_one_col = create uo_ds_top
sqls = "select rpad(' ',4000) from dual"
f_create_dynamic_ds(i_ds_msg_one_col, "grid", sqls, sqlca, true)

i_ds_msg_three_col = create uo_ds_top
sqls = "select rpad(' ',4000), 0, rpad(' ',4000) from dual"
f_create_dynamic_ds(i_ds_msg_three_col, "grid", sqls, sqlca, true)

i_ds_extend = create uo_ds_top
sqls = 'select * from cr_master_element_autoextend where -1 = 0'
f_create_dynamic_ds(i_ds_extend,"grid",sqls,sqlca,true)

i_ds_invalid_mn = create uo_ds_top
sqls = "select " + i_co_field + ", source_id, month_number from cr_cost_repository where -1 = 0 "
f_create_dynamic_ds(i_ds_invalid_mn,"grid",sqls,sqlca,true)

i_add_where = "amount_type <> 99"

i_cv_control_or_combos = upper(g_cr.uf_get_control_value( 'VALIDATIONS - CONTROL OR COMBOS' ))
	
if i_cv_control_or_combos <> 'CONTROL' or isnull(i_cv_control_or_combos) then i_cv_control_or_combos = 'COMBOS'

//	DML: 080206: What technique do we use?  Includes all references to "i_use_intermediate_tbl" below.
i_use_intermediate_tbl = upper(g_cr.uf_get_control_value( 'VALIDATIONS - USE INTERMEDIATE TBL' ));

if isnull(i_use_intermediate_tbl) or i_use_intermediate_tbl <> "YES" then 
	i_use_intermediate_tbl = 'NO'
end if

//	JAK: 20101014: Do we write combo kickouts to the log
i_log_combo_kickouts = upper(g_cr.uf_get_control_value( 'Validations - Log Combo Kickouts' ));

if isnull(i_log_combo_kickouts) or i_log_combo_kickouts <> "YES" then 
	i_log_combo_kickouts = 'NO'
end if

// Read as many of the system controls up front as possible...
i_effmn_cv = upper(trim(g_cr.uf_get_control_value( 'validations: effective month number' )))
if isnull(i_effmn_cv) or i_effmn_cv <> "YES" then i_effmn_cv = 'NO'

i_effmn2_cv = upper(trim(g_cr.uf_get_control_value( 'validation combos: eff mn 2' )))
if isnull(i_effmn2_cv) or i_effmn2_cv <> "YES" then i_effmn2_cv = 'NO'

i_effmn2_multi_rec_cv = upper(trim(g_cr.uf_get_control_value( 'combos: effmn2: multiple records' )))
if isnull(i_effmn2_multi_rec_cv) or i_effmn2_multi_rec_cv <> "YES" then i_effmn2_multi_rec_cv = 'NO'

//-------  System switch for the new structure values table.  -------
i_use_new_structures_table = upper(trim(g_cr.uf_get_control_value( 'USE NEW CR STRUCTURE VALUES TABLE' )))
if isnull(i_use_new_structures_table) or i_use_new_structures_table <> 'YES' then i_use_new_structures_table = "NO"

//	CONTROL/COMBO VALIDATION IS CALLED FIRST...ONLY RESET VALID_ONLINE IF 
//	COMBO VALIDATION IS TURNED OFF OR I WILL OVERRIDE ANY VALIDATION THAT 
//	OCCURRED IN UF_VALIDATE_CONTROL OR UF_VALIDAT_COMBOS.
i_cv_combo_vals = upper(trim(g_cr.uf_get_control_value( 'ENABLE VALIDATIONS - COMBO' )))
if isnull(i_cv_combo_vals) or i_cv_combo_vals <> 'YES' then i_cv_combo_vals = "NO"

i_cv_delete_me = upper(trim(g_cr.uf_get_control_value( 'DELETE MASTER ELEMENTS' )))
if isnull(i_cv_delete_me) or i_cv_delete_me <> "YES" then i_cv_delete_me = 'NO'

// JAK:  20090709:  New control to use exists statements for include rules
i_cv_use_exists = upper(trim(g_cr.uf_get_control_value( 'VALIDATIONS - USE EXISTS CLAUSE' )))
if isnull(i_cv_use_exists) or i_cv_use_exists <> "YES" then i_cv_use_exists = 'NO'

//  Are acct range validations enabled ?
i_cv_acct_range = upper(trim(g_cr.uf_get_control_value( 'enable validations - acct range' )))
if isnull(i_cv_acct_range) or i_cv_acct_range <> "YES" then i_cv_acct_range = "NO"

//GET Tehe work order num field
i_wo_field = upper(trim(g_cr.uf_get_control_value( 'WORK ORDER FIELD' )))
if isnull(i_wo_field) or trim(i_wo_field) = '' then i_wo_field = 'NONE'
i_wo_field = f_cr_clean_string(i_wo_field)

//GET The powerplant company field
i_pp_co_field = upper(trim(g_cr.uf_get_control_value( 'POWERPLANT COMPANY FIELD' )))
if isnull(i_pp_co_field) or trim(i_pp_co_field) = '' then i_pp_co_field = 'NONE'
i_pp_co_field = f_cr_clean_string(i_pp_co_field)

//  System switch to understand the default values that are invalid.
i_cv_req_obey_default = upper(trim(g_cr.uf_get_control_value( 'VALIDATIONS-REQUIRED - OBEY DEFAULT' )))
if isnull(i_cv_req_obey_default) or i_cv_req_obey_default <> "YES" then i_cv_req_obey_default = 'NO'



end event

