HA$PBExportHeader$uo_cr_data_mover.sru
$PBExportComments$JAK: Added 'DO NOT UPDATE' logic so certain fields will not be updated if necessary.  IE master element table with a field we are manually maintaining.
forward
global type uo_cr_data_mover from nonvisualobject
end type
end forward

global type uo_cr_data_mover from nonvisualobject
end type
global uo_cr_data_mover uo_cr_data_mover

type prototypes
function long  ShellExecuteA(long hwnd,string operation,string filename,string parm,string dir,long showcmd) library "shell32.dll"
end prototypes

type variables
boolean i_cv_debug


uo_ds_top i_ds_data_mover, i_ds_map, i_ds_filter
uo_sqlca i_sqlca2
uo_encrypt i_uo_encrypt

string i_ini_file, i_dev_role

uo_ds_top  i_ds_module_tables, i_ds_module_columns, i_ds_module_joins
longlong i_module_tables, i_module_columns, i_module_joins

string i_null_str_array[]
longlong i_null_long_array[]
any i_temp_any_array[], i_null_any_array[]

end variables

forward prototypes
protected subroutine uf_msgs (string a_title, string a_msg)
public function string uf_build_sql_where (longlong a_filter_id, ref string a_sql_from, ref string a_sql_join, string a_module)
private function string uf_build_sql_add_table (string a_table_name, ref string a_sql_from, ref string a_sql_join, longlong a_level)
protected function string uf_build_sql_merge (string a_current_snapshot_table, string a_prior_snapshot_table, string a_target_cols[], uo_sqlca a_extract_sqlca, string a_direction)
protected function integer uf_get_col_list (string a_dba_owner, string a_dba_table, uo_sqlca a_sqlca, boolean a_pk, string a_direction, ref string a_cols[], ref longlong a_pks[])
protected function integer uf_get_col_list (string a_dba_owner, string a_dba_table, uo_sqlca a_sqlca, boolean a_pk, string a_direction, ref string a_cols[], ref longlong a_pks[], string a_cols_filter[])
protected function longlong uf_db_pipeline (string a_extract_sql, string a_target_table, string a_target_cols[], string a_delete_clause, string a_action, longlong a_max_errors, longlong a_commit_count, uo_sqlca a_extract_sqlca, uo_sqlca a_target_sqlca, string a_direction, string a_description)
protected function string uf_build_sql (longlong a_map_id, longlong a_filter_id)
public function longlong uf_run_data_mover (string a_batch)
public function longlong uf_run_data_mover (string a_batch, longlong a_database_id)
public function longlong uf_run_data_mover_where_clause (string a_where_clause)
public function longlong uf_run_data_mover_where_clause (string a_where_clause, longlong a_database_id)
protected function integer uf_get_col_list_sqlserver (string a_dba_owner, string a_dba_table, uo_sqlca a_sqlca, boolean a_pk, string a_direction, ref string a_cols[], ref longlong a_pks[], string a_cols_filter[])
end prototypes

protected subroutine uf_msgs (string a_title, string a_msg);if g_main_application then 
	f_status_box(a_title,a_msg)
else
	f_pp_msgs(a_msg)
end if
end subroutine

public function string uf_build_sql_where (longlong a_filter_id, ref string a_sql_from, ref string a_sql_join, string a_module);string sqls, sql_where, module_column, table_name, str_rtn
longlong num_rows, i, find_row

string left_paren, col_name, operator, value1, between_and, value2, right_paren, and_or
longlong paren_count

sqls = 'select * from cr_data_mover_filter_clause where cr_data_mover_filter_id = ' + string(a_filter_id) + ' order by row_id'

if not isvalid(i_ds_filter) then
	i_ds_filter = create uo_ds_top
	f_create_dynamic_ds(i_ds_filter,'grid',sqls,sqlca,true)
else
	i_ds_filter.Reset()
	i_ds_filter.SetSQLSelect(sqls)
	i_ds_filter.Retrieve()
end if

num_rows = i_ds_filter.RowCount()

if num_rows = 0 then return '1=1'

paren_count = 0
for i = 1 to num_rows
	left_paren  = i_ds_filter.GetItemString(i, "left_paren")
	col_name = i_ds_filter.GetItemString(i, "column_name")
	operator = i_ds_filter.GetItemString(i, "operator")
	value1 = i_ds_filter.GetItemString(i, "value1")	
	between_and = i_ds_filter.GetItemString(i, "between_and")
	value2 = i_ds_filter.GetItemString(i, "value2")
	right_paren = i_ds_filter.GetItemString(i, "right_paren")
	and_or = i_ds_filter.GetItemString(i, "and_or")
	
	operator = upper(trim(operator))
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	
	if pos(upper(col_name),'MODULE.' + a_module + '.') > 0 then
		// module mapping
		module_column = mid(col_name,len('MODULE.' + a_module + '.') + 1)
		find_row = i_ds_module_columns.Find("description = '" + module_column + "'", 1, i_module_columns)
		
		if find_row <= 0 then
			// Couldn't resolve the keyword to table / sql.  Return an error.
			uf_msgs("Run","ERROR: Unable to lookup the table and SQL for from_col '" + col_name + "'!")
			return 'ERROR'
		end if
		
		table_name = i_ds_module_columns.GetItemString(find_row,'table_name')
		col_name = i_ds_module_columns.GetItemString(find_row,'sql')
		
		str_rtn = uf_build_sql_add_table(table_name, a_sql_from, a_sql_join, 1)
		if str_rtn = 'ERROR' then return str_rtn
		
	elseif pos(col_name, "(") = 0 and operator <> '<NONE>' and pos(col_name, '"') <> 1 then
		//  If they manually entered an Oracle function, take it as-is.
		col_name = '"' + f_cr_clean_string(upper(trim(col_name))) + '"' //SRM: This line was adding double quotes to hardcoded column names, fixed under 29335
	end if
	
	choose case operator
		// ******************************************************
		case '=','<>','>','>=','<','<='
		// ******************************************************
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value1), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value1 + ')'
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case 'BETWEEN','NOT BETWEEN'
		// ******************************************************
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value1), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value1 + ')'
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
				
			if mid(trim(value2),1,1) = "'" then
				// have quotes
			elseif pos(value2, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value2), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value2 + ')'
			else
				// Need to add quotes
				value2 = "'" + value2 + "'"
			end if
				
		// ******************************************************
		case 'IN','NOT IN'
		// ******************************************************
			// did they build the parens and quotes for me?
			if mid(trim(value1),1,1) = '(' then
				// Parens=Yes, did they build the quotes for me too?
				if mid(trim(mid(trim(value1),2,100)),1,1) = "'" then
					// Good syntax
				elseif upper(mid(trim(mid(trim(value1),2,100)),1,6)) = "SELECT" then
					// Scalar - Good syntax
				else
					// Need to add quotes
					value1 = trim(value1)
					value1 = f_replace_string(value1,",","','","all")
					value1 = "('" + mid(value1,2,len(value1)-2) + "')"
				end if
			else
				// Parens=No, did they build the quotes for me too?
				if mid(trim(value1),1,1) = "'" then
					// have quotes, just need parens
					value1 = "(" + trim(value1) + ")"
				elseif (mid(trim(value1),1,6)) = "SELECT" then
					// have scalar, just need parens
					value1 = "(" + trim(value1) + ")"
				else
					// Need to add quotes and parens
					value1 = trim(value1)
					value1 = f_replace_string(value1,",","','","all")
					value1 = "('" + value1 + "')"
				end if
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case 'LIKE','NOT LIKE'
		// ******************************************************
			
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// ### 29335: JAK: 2013-02-20:  Are they using Oracle functions / scalar selects etc
				// don't need quotes.
			elseif pos(upper(value1), "SELECT") = 1 then
				// they didn't add parens.  Add them here
				value1 = '(' + value1 + ')'
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
				
			// Did they build the wildcards for me?
			if pos(value1,'%') > 0 then
				// yes
			else
				// no, double side wildcard
				value1 = "'%' || " + value1 + " || '%'"
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case '<NONE>'
		// ******************************************************
			// Just take col_name exactly as it.  Allows for exists, not exists, etc.
			operator = ''
			value1 = ''
			between_and = ''
			value2 = ''
	end choose
	
	if i = num_rows then and_or = ''
	
	sql_where += left_paren + " " + col_name + " " + operator + " " + value1 + " " +  between_and + " " + value2 + " " + right_paren + " " + and_or + " "
next

i_ds_filter.reset()

// 
sql_where = f_replace_string(sql_where,"'PARAMETER1'","PARAMETER1","all")
sql_where = f_replace_string(sql_where,"'PARAMETER2'","PARAMETER2","all")
sql_where = f_replace_string(sql_where,"'PARAMETER3'","PARAMETER3","all")
sql_where = f_replace_string(sql_where,"'PARAMETER4'","PARAMETER4","all")

return sql_where

end function

private function string uf_build_sql_add_table (string a_table_name, ref string a_sql_from, ref string a_sql_join, longlong a_level);longlong find_row
string db_table_name, table_name_parent, join_string, str_rtn

if a_level > 100 then 
	// circular reference in the joins
	//	stop or error?
	//	stop for now...  could have work_order_account pointed to work_order_control 
	//		and vice versa creating a circle, but once both are included in the SQL, we're fine...
	return 'OK'
end if

// do we need to add the table to the from / where?
if pos(a_sql_from,'  ' + a_table_name + ', ') = 0 then
	// need to add the table
	
	// get the db_table_name
	find_row = i_ds_module_tables.Find("table_name = '" + a_table_name + "'", 1, i_module_tables)
	
	if find_row <= 0 then
		// Couldn't resolve the keyword to table / sql.  Return an error.
		uf_msgs("Run","ERROR: Unable to lookup the table in i_ds_module_tables for '" + a_table_name + "'!")
		return 'ERROR'
	else
		db_table_name = i_ds_module_tables.GetItemString(find_row,'db_table_name')
	end if
	
	// get the table_name_parent & join_string
	find_row = i_ds_module_joins.Find("table_name = '" + a_table_name + "'", 1, i_module_tables)
	
	if find_row <= 0 then
		// Couldn't resolve the keyword to table / sql.  Return an error.
		uf_msgs("Run","ERROR: Unable to lookup the table in i_ds_module_joins for '" + a_table_name + "'!")
		return 'ERROR'
	else
		table_name_parent = i_ds_module_joins.GetItemString(find_row,'table_name_parent')
		join_string = i_ds_module_joins.GetItemString(find_row,'join_string')
	end if
	
	a_sql_from += db_table_name + '  ' + a_table_name + ', '
	
	if not isnull(join_string) and trim(join_string) <> '' then
		a_sql_join += '(' + join_string + ') and '
	end if
	
	if not isnull(table_name_parent) and trim(table_name_parent) <> '' then
		// we have a parent, need to make sure to add it too.
		a_level++
		str_rtn = uf_build_sql_add_table(table_name_parent, a_sql_from, a_sql_join, a_level)
		if str_rtn = 'ERROR' then return str_rtn
	end if	
else
	// table is already there...good to go
	return 'OK'
end if
end function

protected function string uf_build_sql_merge (string a_current_snapshot_table, string a_prior_snapshot_table, string a_target_cols[], uo_sqlca a_extract_sqlca, string a_direction);string prior_cols[], current_cols[], sql, sql_select, sql_pk, sql_non_pk, sqls_inner, sqls_outer
longlong prior_pks[], current_pks[], rtn, i, j
boolean column_exists 

// Need the table definitions of the current table and the prior table.  Only care about the PK on the prior table
//	in the event that the current snapshot is a view.
rtn = uf_get_col_list('',a_current_snapshot_table,a_extract_sqlca,false, a_direction, current_cols[], current_pks[])
if rtn < 0 then 
	return 'ERROR'
end if

rtn = uf_get_col_list('',a_prior_snapshot_table,a_extract_sqlca,true, a_direction, prior_cols[], prior_pks[])
if rtn < 0 then 
	return 'ERROR'
end if

// Make sure the the prior table has a PK
for i = 1 to upperbound(prior_pks)
	if prior_pks[i] = 1 then exit
	
	if i = upperbound(prior_pks) then
		uf_msgs(a_direction, "WARNING:  No primary key found for " + a_prior_snapshot_table + ".  Merge cannot be performed!")
		return 'ERROR'
	end if
next

// Make sure that the current table and the prior table have the same columns
if upperbound(current_cols) <> upperbound(prior_cols) then
	uf_msgs(a_direction, "ERROR:  Current and prior snapshot tables have different numbers of columns.  The snapshots must have the same columns!")
	uf_msgs(a_direction, "ERROR:  Current snapshot columns count: " + string(upperbound(current_cols)))
	uf_msgs(a_direction, "ERROR:  Prior snapshot columns count: " + string(upperbound(prior_cols)))
	return 'ERROR'
end if

sql_select = ''
sql_pk = ''
sql_non_pk = ''
sqls_inner = ''
sqls_outer = ''

for i = 1 to upperbound(current_cols)
	if current_cols[i] <> prior_cols[i] then
		uf_msgs(a_direction, "ERROR:  Current and prior snapshot tables must have the same columns!")
		uf_msgs(a_direction, "ERROR:  Current snapshot column " + string(i) + ": " + current_cols[i])
		uf_msgs(a_direction, "ERROR:  Prior snapshot column " + string(i) + ": " +prior_cols[i])
		return 'ERROR'
	end if
	
	// build the select list at the same time
	sql_select += 'a.' + prior_cols[i] + ', '
	if prior_pks[i] = 1 then
		sql_pk += 'a.' + prior_cols[i] + ' = b.' + prior_cols[i] + ' and '
	else
		// easier to add more or statements to handle nulls instead of trying to wrap in nvl() statements
		//	ands get evaulated before ors so no need for extra parens.
		if prior_cols[i] <> 'USER_ID' and prior_cols[i] <> 'TIME_STAMP' then
			sql_non_pk += 'a.' + prior_cols[i] + ' <> b.' + prior_cols[i] + ' or '
			sql_non_pk += 'a.' + prior_cols[i] + ' is null and b.' + prior_cols[i] + ' is not null or '
			sql_non_pk += 'a.' + prior_cols[i] + ' is not null and b.' + prior_cols[i] + ' is null or '
		end if
	end if
next

// trim off the trailing comma/and/or
sql_select = mid(sql_select,1,len(sql_select) - 2)
sql_pk = mid(sql_pk,1,len(sql_pk) - 5)
sql_non_pk = mid(sql_non_pk,1,len(sql_non_pk) - 4)
	
// New rows
sqls_inner = &
	"select 'INSERT' row_action, " + sql_select + " from " + a_current_snapshot_table + " a " + &
	"	where not exists (select 1 from " + a_prior_snapshot_table + " b where " + sql_pk + ") "
	
// Deleted rows
sqls_inner += &
	"union all " + &
	"select 'DELETE' row_action, " + sql_select + " from " + a_prior_snapshot_table + " a " + &
	"	where not exists (select 1 from " + a_current_snapshot_table + " b where " + sql_pk + ") "

// Updated rows
if trim(sql_non_pk) = '' or isnull(sql_non_pk) then
	// if we only have PK fields, then updates will never occur
else
	sqls_inner += &
	"union all " + &
	"select 'UPDATE' row_action, " + sql_select + " from " + a_current_snapshot_table + " a " + &
	"	where exists (select 1 from " + a_prior_snapshot_table + " b where " + sql_pk + " and (" + sql_non_pk + ")) "
end if

// Wrap the inner SQL with an outer layer that matches the target cols
sqls_outer = 'select '
for i = 1 to upperbound(a_target_cols)
	// Is the column on the snapshot tables?  If so, use it directly.
	column_exists = false
	for j = 1 to upperbound(prior_cols)
		if a_target_cols[i] = prior_cols[j] then
			column_exists = true
			exit
		end if
	next
	
	if column_exists then
		sqls_outer += a_target_cols[i] + ', '
		continue
	end if
	
	// Is it one of the fields we'll hardcode?
	choose case a_target_cols[i]
		case 'ACTION'
			sqls_outer += 'row_action, '
		case 'UPLOAD_INDICATOR', 'RECORD_STATUS'
			sqls_outer += "'N', "
		case 	'POWERPLAN_DATETIME', 'POWERPLAN_TIMESTAMP', 'POWERPLAN_DATE', 'POWERPLAN_TIME_STAMP', &
				'MODIFIED_DATETIME', 'MODIFIED_TIMESTAMP', 'MODIFIED_DATE', 'MODIFIED_TIME_STAMP', &
				'UPDATE_DATETIME', 'UPDATE_TIMESTAMP', 'UPDATE_DATE', 'UPDATE_TIME_STAMP'
			sqls_outer += "sysdate, "
		case else
			sqls_outer += "null, "
	end choose
next
sqls_outer = mid(sqls_outer,1,len(sqls_outer) - 2)

return sqls_outer + ' from (' + sqls_inner + ')'
end function

protected function integer uf_get_col_list (string a_dba_owner, string a_dba_table, uo_sqlca a_sqlca, boolean a_pk, string a_direction, ref string a_cols[], ref longlong a_pks[]);return uf_get_col_list(a_dba_owner, a_dba_table, a_sqlca, a_pk, a_direction, a_cols, a_pks, i_null_str_array)
end function

protected function integer uf_get_col_list (string a_dba_owner, string a_dba_table, uo_sqlca a_sqlca, boolean a_pk, string a_direction, ref string a_cols[], ref longlong a_pks[], string a_cols_filter[]);longlong cols, i
string sqls, col_name
boolean pk_found, include_column = true

if pos(upper(a_sqlca.dbms),'ODBC') > 0 and pos(upper(a_sqlca.dbparm),'SQL SERVER') > 0 then
	return uf_get_col_list_sqlserver(a_dba_owner, a_dba_table, a_sqlca, a_pk, a_direction, a_cols, a_pks, a_cols_filter)
end if

DynamicStagingArea l_sqlsa
DynamicDescriptionArea l_sqlda

a_cols = i_null_str_array[]
a_pks = i_null_long_array[]

l_sqlsa = create DynamicStagingArea
l_sqlda = create DynamicDescriptionArea

a_dba_owner = upper(trim(a_dba_owner))
a_dba_table = upper(trim(a_dba_table))

// Find the table / owner if they weren't provided
if isnull(a_dba_owner) or a_dba_owner = '' then
	if pos(a_dba_table,'.') > 0 then
		a_dba_owner = mid(a_dba_table,1,pos(a_dba_table,'.') - 1)
		a_dba_table = mid(a_dba_table, pos(a_dba_table,'.') + 1, 100)
	else
		// Don't have the owner explicit...check for a public synonym
		select table_owner, table_name 
			into :a_dba_owner, :a_dba_table
			from all_synonyms
			where owner = 'PUBLIC'
			and synonym_name = upper(:a_dba_table)
			using a_sqlca;
		
		if isnull(a_dba_owner) or a_dba_owner = '' then
			// No synonym...only place to use is the current user
			a_dba_owner = upper(trim(a_sqlca.LogID))
		end if
	end if
end if			

// Retrieve columns
cols = 0 
sqls = &
	"select column_name " + &
	"from all_tab_columns " + &
	"where owner = '" + a_dba_owner + "' " + &
	"and table_name = '" + a_dba_table + "' " + &
	"order by column_id asc "

l_sqlsa = create DynamicStagingArea
l_sqlda = create DynamicDescriptionArea
PREPARE l_sqlsa from :sqls using a_sqlca;
if a_sqlca.sqlcode < 0 then
	uf_msgs(a_direction, "ERROR:  Retrieving tabling definitions (prepare): " + a_sqlca.sqlerrtext)
	uf_msgs(a_direction, a_dba_owner)
	uf_msgs(a_direction, a_dba_table)
	destroy l_sqlsa
	destroy l_sqlda
	return -1
end if
DECLARE target_curs DYNAMIC CURSOR FOR l_sqlsa;
OPEN DYNAMIC target_curs USING DESCRIPTOR l_sqlda;

DO until a_sqlca.sqlcode <> 0
	FETCH target_curs USING DESCRIPTOR l_sqlda ;

	// check that we actually get results
	if upperbound(l_sqlda.outParmType) > 0 then 
		col_name = l_sqlda.GetDynamicString(1)
		if a_sqlca.sqlcode = 0 then
			if upperbound(a_cols_filter) > 0 then
				// need to make sure it's in their filter list
				for i = 1 to upperbound(a_cols_filter)
					include_column = false
					if col_name = a_cols_filter[i] then
						include_column = true
						exit
					end if
				next
			else
				include_column = true
			end if
			
			if include_column then
				cols++
				a_cols[cols] = col_name
				a_pks[cols] = 0
			end if
		end if
	end if
LOOP

if a_sqlca.sqlcode < 0 then
	uf_msgs(a_direction, "ERROR:  Retrieving tabling definitions (prepare): " + a_sqlca.sqlerrtext)
	uf_msgs(a_direction, a_dba_owner)
	uf_msgs(a_direction, a_dba_table)
	close target_curs;
	destroy l_sqlsa
	destroy l_sqlda
	return -1
else		
	close target_curs;
	destroy l_sqlsa
	destroy l_sqlda
end if

if cols <= 0 then
	uf_msgs(a_direction, "ERROR:  Getting tabling definitions.")
	uf_msgs(a_direction, a_dba_owner)
	uf_msgs(a_direction, a_dba_table)
	return -1
end if

// Really want to make sure the sort is the same as a_cols_filter if it was provided.
//	At this point, the 2 arrays should be the same size unless they asked for a column that doesn't exist
//	check and error if that's the case.
//	if they are the same, replace a_cols with a_cols_filter.
if upperbound(a_cols_filter) > 0 then
	if upperbound(a_cols_filter) = cols then
		a_cols = a_cols_filter
	else
		uf_msgs(a_direction, "ERROR:  All requested columns do not exist on the table.")
		uf_msgs(a_direction, a_dba_owner)
		uf_msgs(a_direction, a_dba_table)
		return -1
	end if
end if
	
if a_pk then	
	sqls = &
		"select b.column_name " + &
		"from all_constraints a, all_cons_columns b " + &
		"where a.constraint_name = b.constraint_name " + &
		"and a.constraint_type = 'P' " + &
		"and a.owner = '" + a_dba_owner + "' " + &
		"and b.owner = '" + a_dba_owner + "' " + &
		"and a.table_name = '" + a_dba_table + "' " + &
		"and b.table_name = '" + a_dba_table + "' "

		
	l_sqlsa = create DynamicStagingArea
	l_sqlda = create DynamicDescriptionArea
	PREPARE l_sqlsa from :sqls using a_sqlca;
	if a_sqlca.sqlcode < 0 then
		uf_msgs(a_direction, "ERROR:  Retrieving tabling definitions: " + a_sqlca.sqlerrtext)
		uf_msgs(a_direction, a_dba_owner)
		uf_msgs(a_direction, a_dba_table)
		destroy l_sqlsa
		destroy l_sqlda
		return -1
	end if
	DECLARE target_curs2 DYNAMIC CURSOR FOR l_sqlsa;
	OPEN DYNAMIC target_curs2 USING DESCRIPTOR l_sqlda;
	
	DO until a_sqlca.sqlcode <> 0
		FETCH target_curs2 USING DESCRIPTOR l_sqlda ;
	
		// check that we actually get results
		if upperbound(l_sqlda.outParmType) > 0 then 
			col_name = l_sqlda.GetDynamicString(1)
			if a_sqlca.sqlcode = 0 then
				for i = 1 to upperbound(a_cols)
					if a_cols[i] = col_name then
						a_pks[i] = 1
					end if
				next
			end if
		end if
	loop
	
	close target_curs2;
	destroy l_sqlsa
	destroy l_sqlda
end if

return upperbound(a_cols)

end function

protected function longlong uf_db_pipeline (string a_extract_sql, string a_target_table, string a_target_cols[], string a_delete_clause, string a_action, longlong a_max_errors, longlong a_commit_count, uo_sqlca a_extract_sqlca, uo_sqlca a_target_sqlca, string a_direction, string a_description);//*****************************************************************************************************************
//
//		UF_DB_PIPELINE:				Provides similar functionality to a PB database pipeline without having to create a database pipeline object
//												This function pulls data from an external database into the current database.
//		
//		Arguments:				a_extract_sql		SQL to be run to extract the data from the current database.  
//									a_target_table		The table the records will be written to in the target database.  The insert will be of the form:
//																insert into a_table_name values (...)
//																Thus, all columns need to have a value mapped in the a_extract_sql.
//									a_delete_clause	If performing a 'Delete' or 'Replace', the where clause used when deleting.
//									a_action				'Insert' will append records to the table.
//															'Replace' will delete from the table and reload it.
//															'Delete' will delete from the table.
//															'Update' will update existing records.
//															'Update/Insert' will update existing records and append new records as defined by the primary key.
//															'Direct SQL' will run the SQL directly in the target database.
//									a_max_errors		Max number of errors that will be allowed before exiting.
//									a_commit_count	How often a commit is called.  A value <= 0 will prevent commits.
//									a_extract_sqlca		Connection to the extract database.
//									a_target_sqlca		Connection to the target database.
//									a_direction			Direction.  Only included in the messaging...not used in the code
//
//		Version					1.0					JAK 2009-04-20
//									1.1					JAK 2010-04-28
//
//*****************************************************************************************************************
string sqls, sqls2, where_clause, sqls3, dba_owner, dba_table, ret, temp_str, col_name, cols[], current_snapshot_table, prior_snapshot_table
longlong rtn, rtn2, i, errors, num_rows, updates, pk_cols, to_cols, from_cols,inserts, deletes, cols_pk[]
any results
boolean pk, extract_curs_open, target_curs_open, commit_extract, fast_write, truncate_success, a_target_sqlserver


if pos(upper(a_target_sqlca.dbms),'ODBC') > 0 and pos(upper(a_target_sqlca.dbparm),'SQL SERVER') > 0 then
	a_target_sqlserver = true
else
	a_target_sqlserver = false
end if

DynamicDescriptionArea extract_sqlda
DynamicStagingArea extract_sqlsa

extract_sqlsa = create DynamicStagingArea
extract_sqlda = create DynamicDescriptionArea
target_curs_open = false
extract_curs_open = false
commit_extract = false
truncate_success = false
rtn = 1
				
a_action = lower(trim(a_action))
// ### Maximo: Data Mover Base Enhancements:  new 'Direct SQL' action type
if a_action <> 'replace' and a_action <> 'update' and a_action <> 'update/insert' and a_action <> 'insert' and a_action <> 'delete' and a_action <> 'direct sql' and a_action <> 'merge' then 
	uf_msgs(a_direction, "WARNING:  Invalid action '" + a_action + "' used.")	
		rtn = -1 
		goto disconnect_and_return
end if

if a_action <> 'direct sql' then
	if trim(a_description) = '' or isnull(a_description) then
		uf_msgs(a_direction, "Starting '" + a_action + "' on '" + a_target_table + "' at " + string(now()))	
	else
		uf_msgs(a_direction, "Starting '" + a_description + "' at " + string(now()))	
	end if
end if

if a_extract_sqlca = a_target_sqlca then
	// Can't support partial commits here due to cursor closings
	if a_commit_count > 0 and a_commit_count <> 999999999 then
		uf_msgs(a_direction, "Partial commits are not supported when performing intra-database steps.  Defaulting to a single commit at the end.")		
		a_commit_count = 999999999
	end if
end if

//*****************************************************************************************************************
//
//		CHECK THEIR INPUTS AND MAKE SURE THEY ARE VALID
//
//*****************************************************************************************************************
if a_max_errors <= 0 then a_max_errors = 1
if mid(upper(trim(a_delete_clause)),1,5) <> 'WHERE' then a_delete_clause = 'where ' + a_delete_clause
if isnull(a_delete_clause) or trim(upper(a_delete_clause)) = 'WHERE' then a_delete_clause = ''


//*****************************************************************************************************************
//
//		CAN WE USE THE FAST METHOD
//			- if intra-database
//			- if insert only (insert, replace, merge)
//			- no partial commits (commit_counter = 0 or 999999999)		Should always be true based on above code related to partial commits
//			- no error tolerance (a_max_errors <= 1)
//
//*****************************************************************************************************************
if 	(a_action = 'replace' or a_action = 'insert' or a_action = 'merge') and &
	a_extract_sqlca = a_target_sqlca and &
	(a_commit_count = 0 or a_commit_count = 999999999) and &
	a_max_errors <= 1 then
	
	fast_write = true
else
	fast_write = false
end if

//*****************************************************************************************************************
//
//		DEBUGGING MESSAGES
//
//*****************************************************************************************************************
if i_cv_debug then
	uf_msgs(a_direction, "Action: " + a_action)
	uf_msgs(a_direction, "Direction: " + a_direction)
	uf_msgs(a_direction, "Commit Count: " + string(a_commit_count))
	uf_msgs(a_direction, "Max Errors: " + string(a_max_errors))
	uf_msgs(a_direction, "Fast Write: " + string(fast_write))
	
	uf_msgs(a_direction, "Connected extract instance. ")
	uf_msgs(a_direction, "    Server: " + a_extract_sqlca.ServerName)
	uf_msgs(a_direction, "    DBMS:   " + a_extract_sqlca.DBMS)
	uf_msgs(a_direction, "    LogID:  " + a_extract_sqlca.LogId)
	uf_msgs(a_direction, "Connected target instance. ")
	uf_msgs(a_direction, "    Server: " + a_target_sqlca.ServerName)
	uf_msgs(a_direction, "    DBMS:   " + a_target_sqlca.DBMS)
	uf_msgs(a_direction, "    LogID:  " + a_target_sqlca.LogId)
	
end if

//*****************************************************************************************************************
//
//		PREPARE TARGET SIDE
//
//*****************************************************************************************************************
a_target_table = trim(upper(a_target_table))

deletes = 0
if a_action = 'replace' or a_action = 'delete' then
	if i_cv_debug then uf_msgs(a_direction, "Deleting old data at " + string(now()))
	
	//	 Lesson learned -- truncate throw off commit logic.  Maybe an action type in the future.
	//	In the short term we can use it for a delete where commit count = 999999999 (end of step)
	if trim(a_delete_clause) = '' and a_action = 'delete' and a_commit_count = 999999999 then
		// No filter and current database -- try to truncate
		a_target_sqlca.truncate_table(a_target_table)
		
		if a_target_sqlca.sqlcode < 0 then
			// truncate failed.  Use delete method.
		else
			truncate_success = true
		end if
	end if
	
	if not truncate_success then
		sqls = 'delete from ' + a_target_table + ' ' + a_delete_clause
		if i_cv_debug then uf_msgs(a_direction, "Delete SQL: " + sqls)
		execute immediate :sqls using a_target_sqlca;
	end if
	
	if a_target_sqlca.sqlcode < 0 then
		uf_msgs(a_direction, "ERROR:  Deleting from " + a_target_table + ": " + a_target_sqlca.sqlerrtext)
		uf_msgs(a_direction, "SQLS: " + sqls)
		rtn = -1 
		goto disconnect_and_return
	else
		deletes = a_target_sqlca.sqlnrows
	end if
end if

// ### Maximo: Data Mover Base Enhancements:  new 'Direct SQL' action type
if a_action = 'direct sql' then
	if trim(a_description) = '' or isnull(a_description) then
		uf_msgs(a_direction, "Issuing direct SQL at " + string(now()))
	else
		uf_msgs(a_direction, "Starting '" + a_description + "' at " + string(now()))	
	end if
	
	if i_cv_debug then uf_msgs(a_direction, "Direct SQL: " + a_extract_sql)
	execute immediate :a_extract_sql using a_target_sqlca;
	if a_target_sqlca.sqlcode < 0 then
		uf_msgs(a_direction, "ERROR:  Issuing direct SQL: " + a_target_sqlca.sqlerrtext)
		uf_msgs(a_direction, "SQLS: " + a_extract_sql)
		rtn = -1 
		goto disconnect_and_return
	else
		choose case mid(upper(trim(a_extract_sql)),1,6)
			case 'DELETE'
				deletes = a_target_sqlca.SQLNRows
			case 'UPDATE'
				updates = a_target_sqlca.SQLNRows
				num_rows = updates
			case 'INSERT'
				inserts = a_target_sqlca.SQLNRows
				num_rows = inserts
			case else
				// Unknown what was done...put in num rows so it gets logged but the details will be blank
				num_rows = a_target_sqlca.SQLNRows
		end choose
	end if
end if

if a_action = 'delete' or a_action = 'direct sql' then goto disconnect_and_return

//// Retrieve target side.
if mid(a_action,1,6) = 'update' then
	// need to get PK too
	rtn = uf_get_col_list('', a_target_table, a_target_sqlca, true, a_direction, cols, cols_pk, a_target_cols)
		
	if rtn < 0 then
		rtn = -1 
		goto disconnect_and_return
	end if
			
	for i = 1 to upperbound(cols_pk)
		if cols_pk[i] = 1 then exit
		
		if i = upperbound(cols_pk) then
			uf_msgs(a_direction, "WARNING:  No primary key found for " + a_target_table + ".  No updates can be performed!")
			a_action = 'insert'
		end if
	next
else
	rtn = uf_get_col_list('', a_target_table, a_target_sqlca, false, a_direction, cols, cols_pk, a_target_cols)
		
	if rtn < 0 then
		rtn = -1 
		goto disconnect_and_return
	end if
end if

to_cols = upperbound(cols)

//*****************************************************************************************************************
//
//		PREPARE EXTRACT SIDE
//
//*****************************************************************************************************************
if a_action = 'merge' then
	// we need to build the extract SQL
	// a_extract_sql will be of the form "current_snapshot_table,prior_snapshot_table"
	current_snapshot_table = 	upper(trim(mid(a_extract_sql,1,pos(a_extract_sql,",") - 1)))
	prior_snapshot_table = 		upper(trim(mid(a_extract_sql,pos(a_extract_sql,",") + 1)))
	a_extract_sql = uf_build_sql_merge(current_snapshot_table, prior_snapshot_table, cols, a_extract_sqlca, a_direction)
	
	if a_extract_sql = 'ERROR' then
		uf_msgs(a_direction, "ERROR:  Building 'merge' SQL")
		rtn = -1 
		goto disconnect_and_return
	end if
elseif mid(upper(trim(a_extract_sql)) ,1,6) = 'SELECT' then
	// They've already built the select statement
else
	// Assume we just have the table name
	a_extract_sql = 'select * from ' + a_extract_sql
end if

// Retrieve from side.
if i_cv_debug then uf_msgs(a_direction, "Extract SQL: " + a_extract_sql)

PREPARE extract_sqlsa from :a_extract_sql using a_extract_sqlca;
if a_extract_sqlca.sqlcode < 0 then
	uf_msgs(a_direction, "ERROR:  Preparing cursor for from: " + a_extract_sqlca.sqlerrtext)
	uf_msgs(a_direction, a_extract_sql)
	rtn = -1 
	goto disconnect_and_return
end if
DESCRIBE extract_sqlsa INTO extract_sqlda;
if a_extract_sqlca.sqlcode < 0 then
	uf_msgs(a_direction, "ERROR:  Preparing cursor for from: " + a_extract_sqlca.sqlerrtext)
	uf_msgs(a_direction, a_extract_sql)
	rtn = -1 
	goto disconnect_and_return
end if

DECLARE extract_curs DYNAMIC CURSOR FOR extract_sqlsa;
from_cols = upperbound(extract_sqlda.outParmType)

// TNN 2010 08 25 --  If the extract uses an ODBC connection, the cursor method may not work.
// Below is another way to check number of columns if the above method returns 0.
if from_cols = 0 and match(a_extract_sqlca.DBMS, 'ODBC') then
	
	uo_ds_top ds_extract
	ds_extract = create uo_ds_top
	
	// do not return rows
	ret = f_create_dynamic_ds(ds_extract, 'GRID', a_extract_sql, a_extract_sqlca, false)
	
	if ret <> 'OK' then
		uf_msgs(a_direction, "ERROR: Creating dynamic ds for from. Err msg: " + ret)
		uf_msgs(a_direction, a_extract_sql)
		rtn = -1
		goto disconnect_and_return
	end if
	
	from_cols = long(ds_extract.Describe("DataWindow.Column.Count"))
end if

//*****************************************************************************************************************
//
//		VALIDATE THAT THE TWO SIDES HAVE THE SAME NUMBER OF COLUMNS
//
//*****************************************************************************************************************
// Verify that the two DS are shaped the same
if from_cols <> to_cols then
	uf_msgs(a_direction, "ERROR:  From and target statements do not match columns!")
	uf_msgs(a_direction, "ERROR:  From columns count: " + string(from_cols))
	uf_msgs(a_direction, "ERROR:  To columns count: " + string(to_cols))
	rtn = -1
	goto disconnect_and_return
end if


//*****************************************************************************************************************
//
//		EVERYTHING LOOKS GOOD BETWEEN THE DATABASES.  OPEN THE
//			CURSOR AND START PROCESSING THE DATA
//
//*****************************************************************************************************************
if i_cv_debug then uf_msgs(a_direction, "Processing data at " + string(now()))


if fast_write then
	// If we're within the same database, use direct SQL statements rather than read/write with a cursor for performance.
	//	All constraints on when this can be done are above when updating the fast_write variable.
	
	if mid(a_action,1,6) = 'update' then
		// not supported yet...
		// will have to use merge syntax?
	else
		// Insert, replace (insert at this point), merge (insert at this point)
		// Just do a big "insert into ... select from ..."
		sqls2 = 'insert into ' + a_target_table + ' ('
		
		// ### 6603: JAK: 2011-02-02:  Specify columnns on target table
		for i = 1 to to_cols
			if a_target_sqlserver then
				sqls2 += cols[i] + ', '
			else
				sqls2 += '"' + cols[i] + '", '
			end if
		next
		
		sqls2 = mid(sqls2,1,len(sqls2) - 2) + ') '

		sqls2 += a_extract_sql
					
		if isnull(sqls2) then 
			errors++
			uf_msgs(a_direction, "ERROR:  SQL2 is null!")
			rtn = -1
			goto disconnect_and_return
		end if
		
		execute immediate :sqls2 using a_target_sqlca;
		
		if a_target_sqlca.sqlcode < 0 then
			errors++
			if a_max_errors = 1 or i_cv_debug then 
				// If they only allow for 1 error, then always show this...otherwise, just in debug mode
				uf_msgs(a_direction, "ERROR:  Inserting into " + a_target_table + ": " + a_target_sqlca.sqlerrtext)
				uf_msgs(a_direction, "SQL: " + sqls2)
			end if
			
			if errors >= a_max_errors then 
				uf_msgs(a_direction, "MAX ERRORS REACHED!")
				rtn = -1 
				goto disconnect_and_return
			end if
		elseif a_target_sqlca.sqlnrows > 0 then
			inserts += a_target_sqlca.sqlnrows
			num_rows += inserts
		end if
	end if		
else
	if i_cv_debug then uf_msgs(a_direction, "Opening cursor at " + string(now()))
	OPEN DYNAMIC extract_curs USING DESCRIPTOR extract_sqlda;
	FETCH extract_curs USING DESCRIPTOR extract_sqlda;
	extract_curs_open = true
	
	DO until a_extract_sqlca.sqlcode <> 0
		num_rows++
		sqls2 = 'insert into ' + a_target_table + ' ('
		
		// ### 6603: JAK: 2011-02-02:  Specify columnns on target table
		for i = 1 to to_cols
			if a_target_sqlserver then
				sqls2 += cols[i] + ', '
			else
				sqls2 += '"' + cols[i] + '", '
			end if
		next
		
		sqls2 = mid(sqls2,1,len(sqls2) - 2) + ') values ('
				
		sqls3 = 'update ' + a_target_table + ' set '
		where_clause = " where "
	
		for i = 1 to to_cols
			CHOOSE CASE extract_sqlda.outParmType[i]
				CASE TypeString!
					results = extract_sqlda.GetDynamicString(i)
				CASE TypeInteger!, TypeReal!, TypeLong!, TypeDecimal!, TypeDouble!, TypeReal!, TypeLongLong!,TypeUInt!,TypeULong!,TypeByte!,TypeReal!
					results = extract_sqlda.GetDynamicNumber(i)
				CASE TypeDate!
					results = extract_sqlda.GetDynamicDate(i)
				CASE TypeDateTime!
					results = extract_sqlda.GetDynamicDateTime(i)
				CASE TypeTime!
					results = extract_sqlda.GetDynamicTime(i)
				CASE TypeBoolean!,TypeUnknown!
					uf_msgs(a_direction, "ERROR:  Datatype for from column " + string(i) + " is not supported!")
					rtn = -1
					goto disconnect_and_return
			END CHOOSE
			
			// ### 6787: 2011-02-16: JAK: If the value is greater than 4k characters, will try to handle the field as a CLOB.
			if len(string(results)) > 4000 then
				temp_str = string(results)
				results = ''
				do until temp_str = '' or isnull(temp_str)
					results += "to_clob('" + string(f_replace_string(mid(temp_str,1,4000),"'","''","all")) + "') || "
					temp_str = mid(temp_str,4001)
				loop
				results = mid(results,1,len(string(results)) - 3)
			end if
			
			if isnull(results) or string(results) = 'DO NOT UPDATE' then
				sqls2 = sqls2 + 'null,'
			else
				CHOOSE CASE extract_sqlda.outParmType[i]
					CASE TypeString!
						// ### 6787: 2011-02-16: JAK: If the value is greater than 4k characters, will try to handle the field as a CLOB.
						if len(string(results)) > 4000 then
							sqls2 = sqls2 +string(results) + ","
						else
							sqls2 = sqls2 +"'" + string(f_replace_string(results,"'","''","all")) + "',"
						end if
					CASE TypeInteger!, TypeReal!, TypeLong!, TypeDecimal!, TypeDouble!, TypeReal!, TypeLongLong!,TypeUInt!,TypeULong!,TypeByte!,TypeReal!
						sqls2 = sqls2 + string(results) + ","
					CASE TypeDate!
						sqls2 = sqls2 + "to_date('" + string(results,'yyyy-mm-dd') + "','yyyy-mm-dd'),"
					CASE TypeDateTime!
						sqls2 = sqls2 + "to_date('" + string(results,'yyyy-mm-dd hh:mm:ss') + "','yyyy-mm-dd hh24:mi:ss'),"
					CASE TypeTime!
						sqls2 = sqls2 + "to_date('" + string(results,'hh:mm:ss') + "','hh24:mi:ss'),"
					CASE TypeBoolean!,TypeUnknown!
						uf_msgs(a_direction, "ERROR:  Datatype for to column " + string(i) + " is not supported!")
						rtn = -1
						goto disconnect_and_return
				END CHOOSE
			end if
			
			if mid(a_action,1,6) = 'update' then
				if cols_pk[i] > 0 then
					pk = true
				else 
					pk = false
				end if
				
				// ### 6603: JAK: Wrap double quotes around col_name.
				if a_target_sqlserver then
					col_name = cols[i]
				else
					col_name = '"' + cols[i] + '"'
				end if
				
				if isnull(results) then
					sqls3 += col_name + '=null,'
					if pk then where_clause = where_clause + col_name + ' is null and '
				elseif string(results) <> 'DO NOT UPDATE' then
					CHOOSE CASE extract_sqlda.outParmType[i]
						CASE TypeString!
							// ### 6787: 2011-02-16: JAK: If the value is greater than 4k characters, will try to handle the field as a CLOB.
							if len(string(results)) > 4000 then
								sqls3 = sqls3 + col_name + "='" + string(results) + "',"
								if pk then where_clause = where_clause + col_name + "='" + string(results) + "' and "
							else
								sqls3 = sqls3 + col_name + "='" + string(f_replace_string(results,"'","''","all")) + "',"
								if pk then where_clause = where_clause + col_name + "='" + string(f_replace_string(results,"'","''","all")) + "' and "
							end if
						CASE TypeInteger!, TypeReal!, TypeLong!, TypeDecimal!, TypeDouble!, TypeReal!, TypeLongLong!,TypeUInt!,TypeULong!,TypeByte!,TypeReal!
							sqls3 = sqls3 + col_name + "=" + string(results) + ","
							if pk then where_clause = where_clause + col_name + "=" + string(results) + " and "
						CASE TypeDate!
							sqls3 = sqls3 + col_name + "= to_date('" + string(results,'yyyy-mm-dd') + "','yyyy-mm-dd'),"
							if pk then where_clause = where_clause + col_name + "= to_date('" + string(results,'yyyy-mm-dd') + "','yyyy-mm-dd') and "
						CASE TypeDateTime!
							sqls3 = sqls3 + col_name + "= to_date('" + string(results,'yyyy-mm-dd hh:mm:ss') + "','yyyy-mm-dd hh24:mi:ss'),"
							if pk then where_clause = where_clause + col_name + "= to_date('" + string(results,'yyyy-mm-dd hh:mm:ss') + "','yyyy-mm-dd hh24:mi:ss') and "
						CASE TypeTime!
							sqls3 = sqls3 + col_name + "= to_date('" + string(results,'hh:mm:ss') + "','hh24:mi:ss'),"
							if pk then where_clause = where_clause + col_name + "= to_date('" + string(results,'hh:mm:ss') + "','hh24:mi:ss') and "
						CASE TypeBoolean!,TypeUnknown!
							uf_msgs(a_direction, "ERROR:  Datatype for to column " + string(i) + " is not supported!")
							rtn = -1
							goto disconnect_and_return
					END CHOOSE
				end if
			end if
		next	
		
		if mid(a_action,1,6) = 'update' then
			sqls3 = mid(sqls3,1,len(sqls3)-1) + mid(where_clause,1,len(where_clause)-4)
			
			if isnull(sqls3) then 
				errors++
				uf_msgs(a_direction, "ERROR:  SQL3 is null!")
				rtn = -1
				goto disconnect_and_return
			end if
				
			execute immediate :sqls3 using a_target_sqlca;
			
			if a_target_sqlca.sqlcode < 0 then
				errors++
				if a_max_errors = 1 or i_cv_debug then 
					// If they only allow for 1 error, then always show this...otherwise, just in debug mode
					uf_msgs(a_direction, "ERROR:  Updating " + a_target_table + ": " + a_target_sqlca.sqlerrtext)
					uf_msgs(a_direction, "SQL: " + sqls3)
				end if
				
				if errors >= a_max_errors then 
					uf_msgs(a_direction, "MAX ERRORS REACHED!")
					rtn = -1 
					goto disconnect_and_return
				end if
			elseif a_target_sqlca.sqlnrows > 0 then
				updates += a_target_sqlca.sqlnrows
			end if
		end if
			
		if a_action = 'merge' or a_action = 'insert' or a_action = 'replace' or (a_action = 'update/insert' and a_target_sqlca.sqlnrows = 0) then
			sqls2 = mid(sqls2,1,len(sqls2)-1) + ')'
			
			if isnull(sqls2) then 
				errors++
				uf_msgs(a_direction, "ERROR:  SQL2 is null!")
				rtn = -1
				goto disconnect_and_return
			end if
				
			execute immediate :sqls2 using a_target_sqlca;
			
			if a_target_sqlca.sqlcode < 0 then
				errors++
				if a_max_errors = 1 or i_cv_debug then 
					// If they only allow for 1 error, then always show this...otherwise, just in debug mode
					uf_msgs(a_direction, "ERROR:  Inserting into " + a_target_table + ": " + a_target_sqlca.sqlerrtext)
					uf_msgs(a_direction, "SQL: " + sqls2)
				end if
				
				if errors >= a_max_errors then 
					uf_msgs(a_direction, "MAX ERRORS REACHED!")
					rtn = -1 
					goto disconnect_and_return
				end if
			elseif a_target_sqlca.sqlnrows > 0 then
				inserts += a_target_sqlca.sqlnrows
			end if
		end if
		
		// JAK 20100428:  If commit count = 0, we will be doing NO commits
		if a_commit_count > 0 then
			if mod(num_rows,a_commit_count) = 0 then
				// Can't commit extract connection or the cursor will close.  Set a boolean and commit later.
				commit_extract = true
				commit using a_target_sqlca;
				
				uf_msgs(a_direction, "(" + string(now()) + ") COMMITTED: " + string(num_rows) + " rows       ERRORS: " + string(errors))
			end if
		end if
		
		FETCH extract_curs USING DESCRIPTOR extract_sqlda;
	LOOP 
	
	if a_extract_sqlca.sqlcode < 0 then
		uf_msgs(a_direction, "ERROR:  Fetching new records from extract database: " + a_extract_sqlca.sqlerrtext)
		rtn = -1 
		goto disconnect_and_return
	end if
end if

		
//*****************************************************************************************************************
//
//		CLOSE ALL CURSORS, DESCRIPTORS ETC
//
//*****************************************************************************************************************
disconnect_and_return:

if rtn < 0 then 
			
	rollback using a_target_sqlca;
	
	if commit_extract then
		// extract would have committed earlier...  commit now.
		commit using a_extract_sqlca;
	else
		rollback using a_extract_sqlca;
	end if
	
	if i_cv_debug then uf_msgs(a_direction,"Rollback issued.")
else
	rtn = num_rows
	choose case lower(a_action)
		case 'delete','replace','direct sql'
			if truncate_success then
				uf_msgs(a_direction, "DELETES: Truncate Performed")
			else
				uf_msgs(a_direction, "DELETES: " + string(deletes))
			end if
			uf_msgs(a_direction, "COMPLETE: " + string(num_rows) + &
				" rows (INSERTS:" + string(inserts) + "   UPDATES:" + string(updates) + "   ERRORS:" + string(errors) + ")")
		case else
			uf_msgs(a_direction, "COMPLETE: " + string(num_rows) + &
				" rows (INSERTS:" + string(inserts) + "   UPDATES:" + string(updates) + "   ERRORS:" + string(errors) + ").")
	end choose

	// JAK 20100428:  If commit count = 0, we will be doing NO commits
	if a_commit_count > 0 then
		commit using a_target_sqlca;
		commit using a_extract_sqlca;  // Always commit both sides
		if i_cv_debug then uf_msgs(a_direction,"Commit issued.")
	end if
end if
	
try
	if extract_curs_open then
		CLOSE extract_curs;
	end if
	
	destroy extract_sqlsa
	destroy extract_sqlda
catch (RuntimeError rte)
	uf_msgs(a_direction, "A runtime error has occurred.  Please review the logs above.")
finally
	uf_msgs(a_direction, " ")
end try


// Update snapshot table.current_snapshot_table
if a_action = 'merge' and rtn >= 0 then
	// Only need to do the update snapshot step if there were actually changes.  
	if num_rows > 0 then
		rtn2 = uf_db_pipeline(current_snapshot_table, prior_snapshot_table, i_null_str_array, '', 'replace', a_max_errors, a_commit_count, a_extract_sqlca, a_extract_sqlca, a_direction, a_description + ' - Step 2: Refresh Snapshot')
		
		if rtn2 < 0 then 
			rtn = rtn2
		else
			rtn = rtn + rtn2
		end if
	else
		uf_msgs(a_direction,"No snapshot update required due to no changes.")
		uf_msgs(a_direction," ")
	end if
end if

return rtn
end function

protected function string uf_build_sql (longlong a_map_id, longlong a_filter_id);string sqls, sql_rtn, sql_from, sql_join, sql_where, to_col, from_col, from_function, sql_outer, sql_group
longlong num_rows, i, find_row
string module, module_column, db_table_name, table_name_parent, join_string, str_rtn, table_name
boolean module_mode, needs_outer

// need to lookup the table name from cr_data_mover_map
select from_table_name 
	into :table_name
	from cr_data_mover_map
	where cr_data_mover_map_id = :a_map_id;

if sqlca.SQLCode = 100 or isnull(table_name) or trim(table_name) = '' then
	uf_msgs("Run","ERROR: Unable to lookup the from table name from the mappings!")
	return 'ERROR'
elseif mid(table_name,1,8) = 'MODULE: ' then
	// module name is built into the table name such as "MODULE: PM"
	module = mid(table_name,9)
	module_mode = true
		
	// **************************************************************************
	// retrieve module tables
	// **************************************************************************
	sqls = "select table_name, db_table_name from cr_data_mover_module_table where module = '" + module + "'"
	if not isvalid(i_ds_module_tables) then
		i_ds_module_tables = create uo_ds_top
		f_create_dynamic_ds(i_ds_module_tables,'grid',sqls,sqlca,true)
		i_module_tables = i_ds_module_tables.RowCount()
	else
		i_ds_module_tables.Reset()
		i_ds_module_tables.SetSQLSelect(sqls)
		i_module_tables = i_ds_module_tables.Retrieve()
	end if
	
	// **************************************************************************
	// retrieve module tables
	// **************************************************************************
	sqls = "select description, table_name, sql from cr_data_mover_module_column where module = '" + module + "'"
	if not isvalid(i_ds_module_columns) then
		i_ds_module_columns = create uo_ds_top
		f_create_dynamic_ds(i_ds_module_columns,'grid',sqls,sqlca,true)
		i_module_columns = i_ds_module_columns.RowCount()
	else
		i_ds_module_columns.Reset()
		i_ds_module_columns.SetSQLSelect(sqls)
		i_module_columns = i_ds_module_columns.Retrieve()
	end if
	
	// **************************************************************************
	// retrieve join tables
	// **************************************************************************
	sqls = "select table_name, table_name_parent, join_string from cr_data_mover_module_join where module = '" + module + "'"
	if not isvalid(i_ds_module_joins) then
		i_ds_module_joins = create uo_ds_top
		f_create_dynamic_ds(i_ds_module_joins,'grid',sqls,sqlca,true)
		i_module_joins = i_ds_module_joins.RowCount()
	else
		i_ds_module_joins.Reset()
		i_ds_module_joins.SetSQLSelect(sqls)
		i_module_joins = i_ds_module_joins.Retrieve()
	end if
else 
	// non module... need to add the table name to sql_from.  Comma will get trimmed at the end
	sql_from += table_name + ', '
	module_mode = false
end if



// **************************************************************************
// retrieve mappings
//	### 10798: Added from_function
// **************************************************************************
sqls = 'select to_column_name, from_column_name, from_function from cr_data_mover_map_columns where cr_data_mover_map_id = ' + string(a_map_id) + ' order by "ORDER"'
if not isvalid(i_ds_map) then
	i_ds_map = create uo_ds_top
	f_create_dynamic_ds(i_ds_map,'grid',sqls,sqlca,true)
else
	i_ds_map.Reset()
	i_ds_map.SetSQLSelect(sqls)
	i_ds_map.Retrieve()
end if

num_rows = i_ds_map.RowCount()
if num_rows = 0 then
	uf_msgs('uf_build_sql','ERROR:  No records returned from the mapping table!')
	uf_msgs('uf_build_sql','SQL: ' + sqls)
	return 'ERROR'
end if

sql_rtn = 'select '
sql_outer = 'select '
sql_group = 'group by '
needs_outer = false
for i = 1 to num_rows
	setnull(to_col)
	setnull(from_col)
	setnull(from_function)
	
	to_col = i_ds_map.GetItemString(i,1)
	to_col = f_cr_clean_string(upper(trim(to_col)))
	from_col = i_ds_map.GetItemString(i,2)
	from_function = i_ds_map.GetItemString(i,3)
	
	if isnull(from_col) or trim(from_col) = '' then from_col = 'null'
	if isnull(from_function) or trim(from_function) = '' then from_function = ''
	
	if pos(upper(from_col),'MODULE.' + module + '.') > 0 and module_mode then
		// module mapping
		module_column = mid(from_col,len('MODULE.' + module + '.') + 1)
		find_row = i_ds_module_columns.Find("description = '" + module_column + "'", 1, i_module_columns)
		
		if find_row <= 0 then
			// Couldn't resolve the keyword to table / sql.  Return an error.
			uf_msgs("Run","ERROR: Unable to lookup the table and SQL for from_col '" + from_col + "'!")
			return 'ERROR'
		end if
		
		table_name = i_ds_module_columns.GetItemString(find_row,'table_name')
		from_col = i_ds_module_columns.GetItemString(find_row,'sql')
		
		sql_rtn += from_col + ' as "' + to_col + '", '
		
		str_rtn = uf_build_sql_add_table(table_name, sql_from, sql_join, 1)
		if str_rtn = 'ERROR' then return str_rtn
	else
		// direct SQL
		sql_rtn += from_col + ' as "' + to_col + '", '
	end if
	
	if from_function = '' then
		sql_outer += '"' + to_col + '", '
		sql_group += '"' + to_col + '", '
	else
		sql_outer += from_function + '("' + to_col + '") "' + to_col + '", '
		// nothing to group
		needs_outer = true
	end if
next

if isnull(a_filter_id) then
	// no other where clause
else
	sql_where = uf_build_sql_where(a_filter_id, sql_from, sql_join, module)
end if

sql_rtn = mid(sql_rtn,1,len(sql_rtn) - 2)				// ends in ", "
sql_from = mid(sql_from,1,len(sql_from) - 2)		// ends in ", "
sql_outer = mid(sql_outer,1,len(sql_outer) - 2)		// ends in ", "
sql_group = mid(sql_group,1,len(sql_group) - 2)	// ends in ", "
sql_join = mid(sql_join,1,len(sql_join) - 5)			// ends in ") and "
i_ds_map.Reset()

if isnull(sql_join) or trim(sql_join) = '' then sql_join = '1=1'
if isnull(sql_where) or trim(sql_where) = '' then sql_where = '1=1'

sql_rtn += ' from ' + sql_from + ' where (' + sql_join + ') and (' + sql_where + ')'
if needs_outer then
	sql_rtn = sql_outer + ' from (' + sql_rtn + ') ' + sql_group
end if

return sql_rtn


end function

public function longlong uf_run_data_mover (string a_batch);//*****************************************************************************************************************
//
//		UF_RUN_DATA_MOVER:		Provides similar functionality to a PB database pipeline without having to create a database pipeline object
//													Runs a series of moves as defined in cr_data_mover
//		
//		Arguments:			a_batch		Batch name from cr_data_mover.  Runs all moves for the selected batch.
//
//		Version					1.0			JAK 2009-04-20
//
//	### 29278: JAK: 2013-02-11:  Change return type to longlong from int to support processing of >32k rows.
//*****************************************************************************************************************
string where_clause

// ### Maximo: Data Mover Base Enhancements:  Run locked processes as well
where_clause = "where lower(batch) = lower('" + a_batch + "') and status in (1,2)"
return uf_run_data_mover_where_clause(where_clause,0)

end function

public function longlong uf_run_data_mover (string a_batch, longlong a_database_id);//*****************************************************************************************************************
//
//		UF_RUN_DATA_MOVER:		Provides similar functionality to a PB database pipeline without having to create a database pipeline object
//													Runs a series of moves as defined in cr_data_mover
//		
//		Arguments:			a_batch		Batch name from cr_data_mover.  Runs all moves for the selected batch.
//									a_database_id	Optional database_id to override the database_id in the config
//
//		Version					1.0			JAK 2009-04-20
//
//	### 29278: JAK: 2013-02-11:  Change return type to longlong from int to support processing of >32k rows.
//*****************************************************************************************************************
string where_clause

// ### Maximo: Data Mover Base Enhancements:  Run locked processes as well
where_clause = "where lower(batch) = lower('" + a_batch + "') and status in (1,2)"
return uf_run_data_mover_where_clause(where_clause,a_database_id)

end function

public function longlong uf_run_data_mover_where_clause (string a_where_clause);//*****************************************************************************************************************
//
//		UF_RUN_DATA_MOVER:		Provides similar functionality to a PB database pipeline without having to create a database pipeline object
//													Runs a series of moves as defined in cr_data_mover
//		
//		Arguments:			a_where_clause		A where clause to identify the recorsd in cr_data_mover to run.
//
//		Version					1.0			JAK 2009-04-20
//
//	### 29278: JAK: 2013-02-11:  Change return type to longlong from int to support processing of >32k rows.
//*****************************************************************************************************************

return uf_run_data_mover_where_clause(a_where_clause,0)
end function

public function longlong uf_run_data_mover_where_clause (string a_where_clause, longlong a_database_id);//*****************************************************************************************************************
//
//		UF_RUN_DATA_MOVER:		Provides similar functionality to a PB database pipeline without having to create a database pipeline object
//													Runs a series of moves as defined in cr_data_mover
//		
//		Arguments:			a_where_clause		A where clause to identify the recorsd in cr_data_mover to run.
//									a_database_id	Optional database_id to override the database_id in the config
//
//		Version					1.0			JAK 2009-04-20
//
//	### 29278: JAK: 2013-02-11:  Change return type to longlong from int to support processing of >32k rows.
//*****************************************************************************************************************
string sqls, direction, odbc_string, delete_where_clause, description, null1, null2
string database_server, database_userid, database_password, dbms, table_name, &
	parameter1, parameter2, parameter3, parameter4, action, parameter1_value, parameter2_value, parameter3_value, parameter4_value, &
	encrypted_password, target_columns, target_col_array[]
longlong commit_counter, max_errors, rowcount, i, rtn, j, status, database_id, old_database_id, encrypted_flag, cr_data_mover_map_id, cr_data_mover_filter_id, counter

old_database_id = -981239

// ### 6603: JAK: 2011-02-02:  Instanced datastores
if mid(upper(trim(a_where_clause)),1,5) <> 'WHERE' and not isnull(a_where_clause) and trim(a_where_clause) <> '' then a_where_clause = 'WHERE ' + a_where_clause
sqls = "select * from cr_data_mover " + a_where_clause + " order by priority asc"
i_ds_data_mover.Reset()
i_ds_data_mover.SetSQLSelect(sqls)
rowcount = i_ds_data_mover.retrieve()

if i_ds_data_mover.i_sqlca_sqlcode <> 0 then
	uf_msgs("Run","ERROR:  Retrieving records from cr_data_mover: " + i_ds_data_mover.i_sqlca_SQLErrText)
	rtn = -1
	goto disconnect_and_return
end if

uf_msgs("Run", "Starting CR Data Mover. ")

// If they have an "replace group" records, they really translate into a delete record and then an insert record
i = i_ds_data_mover.Find("lower(action) = 'replace group'", 1, i_ds_data_mover.rowcount())
if i > 0 then
	// Need to update the existing records to insert, and insert a "delete" record for each in the reverse priority
	i_ds_data_mover.SetFilter("lower(action) = 'replace group'")
	i_ds_data_mover.Filter()
	
	// Update the existing rows to 'insert' and then copy them to the filtered buffer.
	for i = 1 to i_ds_data_mover.RowCount()
		i_ds_data_mover.setitem(i,'action','insert')
	next
	rtn = i_ds_data_mover.RowsCopy(1, i_ds_data_mover.RowCount(), Primary!, i_ds_data_mover, 1, Filter!)
	if rtn < 0 then
		uf_msgs("Run","ERROR:  Using RowsCopy for 'replace group' data.  Return=" + string(rtn))
		rtn = -1
		goto disconnect_and_return
	end if
	
	// Update the existing rows to 'delete' in the opposite priority
	for i = 1 to i_ds_data_mover.RowCount()
		i_ds_data_mover.setitem(i,'action','delete')
		i_ds_data_mover.setitem(i,'priority',-1 * i_ds_data_mover.getitemnumber(i,'priority'))
	next
	
	// Remove the filter and re-sort
	i_ds_data_mover.SetFilter("")
	i_ds_data_mover.Filter()
	i_ds_data_mover.SetSort("priority a")
	i_ds_data_mover.Sort()
end if

// If they have provided a specific database they want to run them for, use that here.
if a_database_id > 0 then
	for i = 1 to i_ds_data_mover.rowcount()
		i_ds_data_mover.setitem(i,'database_id',a_database_id)
	next 
end if
			
for i = 1 to i_ds_data_mover.rowcount()		
	
	setnull(database_id)
	setnull(sqls)
	setnull(table_name)
	setnull(target_columns)
	setnull(parameter1)
	setnull(parameter2)
	setnull(parameter3)
	setnull(parameter4)
	setnull(action)
	setnull(commit_counter)
	setnull(max_errors)
	setnull(direction)
	setnull(delete_where_clause)
	setnull(cr_data_mover_map_id)
	setnull(cr_data_mover_filter_id)
	
	database_id = i_ds_data_mover.getitemnumber(i,'database_id')
	sqls = i_ds_data_mover.getitemstring(i,'sql')
	table_name = i_ds_data_mover.getitemstring(i,'table_name')
	target_columns = i_ds_data_mover.getitemstring(i,'target_columns')
	parameter1 = i_ds_data_mover.getitemstring(i,'parameter1')
	parameter2 = i_ds_data_mover.getitemstring(i,'parameter2')
	parameter3 = i_ds_data_mover.getitemstring(i,'parameter3')
	parameter4 = i_ds_data_mover.getitemstring(i,'parameter4')
	action = i_ds_data_mover.getitemstring(i,'action')
	commit_counter = i_ds_data_mover.getitemnumber(i,'commit_counter')
	max_errors = i_ds_data_mover.getitemnumber(i,'max_errors')
	direction = upper(trim(i_ds_data_mover.getitemstring(i,'direction')))
	delete_where_clause = i_ds_data_mover.getitemstring(i,'delete_where_clause')
	cr_data_mover_map_id = i_ds_data_mover.getitemnumber(i,'cr_data_mover_map_id')
	cr_data_mover_filter_id = i_ds_data_mover.getitemnumber(i,'cr_data_mover_filter_id')
	description = i_ds_data_mover.getitemstring(i,'description')
	
	if trim(target_columns) = '' or isnull(target_columns) then
		target_col_array = i_null_str_array
	elseif target_columns = '<Mappings / Filters>' then
		f_get_column(i_temp_any_array, 'select to_column_name from cr_data_mover_map_columns where cr_data_mover_map_id = ' + string(cr_data_mover_map_id) + ' order by "ORDER"')
		target_col_array = i_temp_any_array
		i_temp_any_array = i_null_any_array
	else
		f_parsestringintostringarray(target_columns,',',target_col_array)
	end if
	
	for j = 1 to upperbound(target_col_array)
		target_col_array[j] = f_replace_string(f_cr_clean_string(trim(upper(target_col_array[j]))),'"','','all')
	next			
	
	parameter1_value = ''
	parameter2_value = ''
	parameter3_value = ''
	parameter4_value = ''
	
	// ### Base enhancements for Maximo project.
	if table_name = '<Mappings / Filters>' then
		// need to lookup the table name from cr_data_mover_map
		select to_table_name 
			into :table_name
			from cr_data_mover_map
			where cr_data_mover_map_id = :cr_data_mover_map_id;
			
		if sqlca.SQLCode = 100 or isnull(table_name) or trim(table_name) = '' then
			uf_msgs("Run","ERROR: Unable to lookup the table name from the mappings!")
			rtn = -1
			goto disconnect_and_return
		end if
	end if
	
	if sqls = '<Mappings / Filters>' then
		// need to build the SQL based on the mappings and filters
		sqls = uf_build_sql(cr_data_mover_map_id,cr_data_mover_filter_id)
		
		if sqls = 'ERROR' then
			uf_msgs("Run","ERROR: Unable to build SQL from mappings and filters!")
			rtn = -1
			goto disconnect_and_return
		end if
	end if
	
	if delete_where_clause = '<Mappings / Filters>' then
		// need to build the SQL based on the mappings and filters
		delete_where_clause = uf_build_sql_where(cr_data_mover_filter_id, null1, null2, '') 	// shouldn't be using Module logic here -- last 3 arguments don't matter
		
		if delete_where_clause = 'ERROR' then
			uf_msgs("Run","ERROR: Unable to build Delete Where Clause from filters!")
			rtn = -1
			goto disconnect_and_return
		end if
	end if
	
	if database_id = old_database_id or (isnull(database_id) and isnull(old_database_id)) then
		// No need to reconnect
	else
		old_database_id = database_id
		
		// New connect
		select database_server, database_userid, database_password, dbms, odbc_string, encrypted_password
		into :database_server, :database_userid, :database_password, :dbms, :odbc_string, :encrypted_flag
		from cr_data_mover_database
		where database_id = :database_id;
		
		if isnull(encrypted_flag) then encrypted_flag = 0
		
		if encrypted_flag = 1 and trim(database_password) <> '' and not isnull(database_password) then
			// Need to decrypt the password
			database_password = i_uo_encrypt.uf_decrypt(database_password,'CRDataMoverKey')
			if mid(database_password,1,5) = 'ERROR' then
				uf_msgs("Run","ERROR:  The password decryption failed.  " + encrypted_password)
				rtn = -1
				goto disconnect_and_return
			end if
		elseif encrypted_flag = 0 and trim(database_password) <> '' and not isnull(database_password) then
			// The password has never been encrypted -- do that here
			encrypted_password = i_uo_encrypt.uf_encrypt(database_password,'CRDataMoverKey')
			if mid(encrypted_password,1,5) <> 'ERROR' then
				update cr_data_mover_database
					set database_password = :encrypted_password,
					encrypted_password = 1					
					where database_id = :database_id;
			end if
		end if 
		
		if isnull(database_server) or trim(database_server) = '' then database_server = sqlca.ServerName
		if isnull(database_userid) or trim(database_userid) = '' then database_userid = sqlca.LogId
		if isnull(database_password) or trim(database_password) = '' then database_password = sqlca.LogPass
		if isnull(dbms) or trim(dbms) = '' then dbms = sqlca.DBMS
		if isnull(odbc_string) or trim(odbc_string) = '' then odbc_string = sqlca.DBParm
		
		// no need to do this during the first loop
		if i > 1 then
			// Always commit before disconnecting...
			commit using i_sqlca2;
			commit using sqlca;
			if i_cv_debug then uf_msgs("Run" ,"Commit issued prior to database connection change.")
			
			disconnect using i_sqlca2;
		end if
		
		i_sqlca2.ServerName = database_server
		i_sqlca2.DBMS = dbms
		i_sqlca2.LogId = database_userid
		i_sqlca2.LogPass = database_password
		i_sqlca2.DBParm = odbc_string
	
		// ### 10570: JAK: 2012-06-20:  If connecting to the same database for i_sqlca2, no need to create the 2nd tranasction.  Will just share the first.
		if trim(upper(i_sqlca2.ServerName)) = trim(upper(sqlca.ServerName)) and  trim(upper(i_sqlca2.LogId)) = trim(upper(sqlca.LogId)) then
				// same connection
			else
				
			connect using i_sqlca2;
			
			if i_sqlca2.SQLCode = 0 then
				uf_msgs("Run", "Connected to second instance. ")
				uf_msgs("Run", "    Server: " + i_sqlca2.ServerName)
				uf_msgs("Run", "    DBMS:   " + i_sqlca2.DBMS)
				uf_msgs("Run", "    LogID:  " + i_sqlca2.LogId)
				uf_msgs("Run", " ")
				
				// See if we connected to another PowerPlan database and need to assign the default role.
				select count(*) into :counter
				from user_ROLE_PRIVS where username = upper(ltrim(rtrim(:database_userid))) and granted_role = :i_dev_role and default_role = 'NO'
				using i_sqlca2;
				
				if counter > 0 then
					i_sqlca2.uf_set_role(i_dev_role)
				end if
			else
				uf_msgs("Run", "Error connecting to second instance: " + i_sqlca2.SQLErrText)
				uf_msgs("Run", "    Server: " + i_sqlca2.ServerName)
				uf_msgs("Run", "    DBMS:   " + i_sqlca2.DBMS)
				uf_msgs("Run", "    LogID:  " + i_sqlca2.LogId)
				uf_msgs("Run", "    DBParm: "+ i_sqlca2.dbparm)
				uf_msgs("Run", "    SQLCode: " + string(i_sqlca2.sqlcode))
				uf_msgs("Run", "    SQLDBCode: " + string(i_sqlca2.sqldbcode))
				uf_msgs("Run", "    SQLErrText: " + string(i_sqlca2.SQLErrText))
				//  Erroring out without running the close script of the application.
				rtn = -1
				goto disconnect_and_return
			end if
		end if
	end if
	
	if pos(upper(parameter1),'SELECT') > 0 then
		// Scalar select...go get the value
		f_get_column(i_temp_any_array,parameter1)
		for j = 1 to upperbound(i_temp_any_array)
			parameter1_value = parameter1_value + "'" + string(i_temp_any_array[j]) + "',"
		next
		parameter1_value = mid(parameter1_value,1, len(parameter1_value) - 1)
		i_temp_any_array = i_null_any_array
	else
		parameter1_value = parameter1
	end if
	
	if pos(upper(parameter2),'SELECT') > 0 then
		// Scalar select...go get the value
		f_get_column(i_temp_any_array,parameter2)
		for j = 1 to upperbound(i_temp_any_array)
			parameter2_value = parameter2_value + "'" + string(i_temp_any_array[j]) + "',"
		next
		parameter2_value = mid(parameter2_value,1, len(parameter2_value) - 1)
		i_temp_any_array = i_null_any_array
	else
		parameter2_value = parameter2
	end if
	
	if pos(upper(parameter3),'SELECT') > 0 then
		// Scalar select...go get the value
		f_get_column(i_temp_any_array,parameter3)
		for j = 1 to upperbound(i_temp_any_array)
			parameter3_value = parameter3_value + "'" + string(i_temp_any_array[j]) + "',"
		next
		parameter3_value = mid(parameter3_value,1, len(parameter3_value) - 1)
		i_temp_any_array = i_null_any_array
	else
		parameter3_value = parameter3
	end if
	
	if pos(upper(parameter4),'SELECT') > 0 then
		// Scalar select...go get the value
		f_get_column(i_temp_any_array,parameter4)
		for j = 1 to upperbound(i_temp_any_array)
			parameter4_value = parameter4_value + "'" + string(i_temp_any_array[j]) + "',"
		next
		parameter4_value = mid(parameter4_value,1, len(parameter4_value) - 1)
		i_temp_any_array = i_null_any_array
	else
		parameter4_value = parameter4
	end if
	
	if not isnull(parameter1_value) and trim(parameter1_value) <> '' then 
		if i_cv_debug then uf_msgs("Run","Parameter1: " + parameter1_value)
		sqls = f_replace_string(sqls,'PARAMETER1',parameter1_value,'all')
		delete_where_clause = f_replace_string(delete_where_clause,'PARAMETER1',parameter1_value,'all')
	end if
	if not isnull(parameter2_value) and trim(parameter2_value) <> '' then 
		if i_cv_debug then uf_msgs("Run","Parameter2: " + parameter2_value)
		sqls = f_replace_string(sqls,'PARAMETER2',parameter2_value,'all')
		delete_where_clause = f_replace_string(delete_where_clause,'PARAMETER2',parameter2_value,'all')
	end if
	if not isnull(parameter3_value) and trim(parameter3_value) <> '' then 
		if i_cv_debug then uf_msgs("Run","Parameter3: " + parameter3_value)
		sqls = f_replace_string(sqls,'PARAMETER3',parameter3_value,'all')
		delete_where_clause = f_replace_string(delete_where_clause,'PARAMETER3',parameter3_value,'all')
	end if
	if not isnull(parameter4_value) and trim(parameter4_value) <> '' then 
		if i_cv_debug then uf_msgs("Run","Parameter4: " + parameter4_value)
		sqls = f_replace_string(sqls,'PARAMETER4',parameter4_value,'all')
		delete_where_clause = f_replace_string(delete_where_clause,'PARAMETER4',parameter4_value,'all')
	end if
	
	choose case direction
		case 'PULL'
			// ### 10570: JAK: 2012-06-20:  If connecting to the same database for i_sqlca2, no need to create the 2nd tranasction.  Will just share the first.
			if i_sqlca2.ServerName = sqlca.ServerName and  i_sqlca2.LogId = sqlca.LogId then
				rtn = uf_db_pipeline(sqls,table_name,target_col_array,delete_where_clause,action,max_errors,commit_counter,sqlca,sqlca,'Pull',description)
			else
				rtn = uf_db_pipeline(sqls,table_name,target_col_array,delete_where_clause,action,max_errors,commit_counter,i_sqlca2,sqlca,'Pull',description)
			end if
		case 'PUSH'
			// ### 10570: JAK: 2012-06-20:  If connecting to the same database for i_sqlca2, no need to create the 2nd tranasction.  Will just share the first.
			if i_sqlca2.ServerName = sqlca.ServerName and  i_sqlca2.LogId = sqlca.LogId then
				rtn = uf_db_pipeline(sqls,table_name,target_col_array,delete_where_clause,action,max_errors,commit_counter,sqlca,sqlca,'Push',description)
			else
				rtn = uf_db_pipeline(sqls,table_name,target_col_array,delete_where_clause,action,max_errors,commit_counter,sqlca,i_sqlca2,'Push',description)
			end if
		case else
			uf_msgs("Run","Invalid Direction value: " + direction + "!")
	end choose
	
	if rtn < 0 then goto disconnect_and_return
next
uf_msgs("Run", "CR Data Mover Complete. ")

disconnect_and_return:
if rtn < 0 then
	rollback using sqlca;
	rollback using i_sqlca2;
	if i_cv_debug then uf_msgs("Run" ,"Commit issued.")
else
	commit using sqlca;
	commit using i_sqlca2;
	if i_cv_debug then uf_msgs("Run" ,"Commit issued.")
end if
	
try	
	disconnect using i_sqlca2;
finally
	uf_msgs("Run", " ")		
end try

return rtn
end function

protected function integer uf_get_col_list_sqlserver (string a_dba_owner, string a_dba_table, uo_sqlca a_sqlca, boolean a_pk, string a_direction, ref string a_cols[], ref longlong a_pks[], string a_cols_filter[]);longlong cols, i
string sqls, col_name
boolean pk_found, include_column = true

DynamicStagingArea l_sqlsa
DynamicDescriptionArea l_sqlda

a_cols = i_null_str_array[]
a_pks = i_null_long_array[]

l_sqlsa = create DynamicStagingArea
l_sqlda = create DynamicDescriptionArea

a_dba_owner = upper(trim(a_dba_owner))
a_dba_table = upper(trim(a_dba_table))

// Find the table / owner if they weren't provided
if isnull(a_dba_owner) or a_dba_owner = '' then
	if pos(a_dba_table,'.') > 0 then
		a_dba_owner = mid(a_dba_table,1,pos(a_dba_table,'.') - 1)
		a_dba_table = mid(a_dba_table, pos(a_dba_table,'.') + 1, 100)
	else
		// Don't have the owner explicit...check for a public synonym
		select table_owner, table_name 
			into :a_dba_owner, :a_dba_table
			from all_synonyms
			where owner = 'PUBLIC'
			and synonym_name = upper(:a_dba_table)
			using a_sqlca;
		
		if isnull(a_dba_owner) or a_dba_owner = '' then
			// No synonym...only place to use is the current user
			a_dba_owner = upper(trim(a_sqlca.LogID))
		end if
	end if
end if			

// Retrieve columns
cols = 0 
sqls = &
	"select column_name " + &
	"from information_schema.columns " + &
	"where upper(table_schema) = '" + a_dba_owner + "' " + &
	"and upper(table_name) = '" + a_dba_table + "' " + &
	"order by ordinal_position asc "

l_sqlsa = create DynamicStagingArea
l_sqlda = create DynamicDescriptionArea
PREPARE l_sqlsa from :sqls using a_sqlca;
if a_sqlca.sqlcode < 0 then
	uf_msgs(a_direction, "ERROR:  Retrieving tabling definitions (prepare): " + a_sqlca.sqlerrtext)
	uf_msgs(a_direction, a_dba_owner)
	uf_msgs(a_direction, a_dba_table)
	destroy l_sqlsa
	destroy l_sqlda
	return -1
end if
DECLARE target_curs DYNAMIC CURSOR FOR l_sqlsa;
OPEN DYNAMIC target_curs USING DESCRIPTOR l_sqlda;

DO until a_sqlca.sqlcode <> 0
	FETCH target_curs USING DESCRIPTOR l_sqlda ;

	// check that we actually get results
	if upperbound(l_sqlda.outParmType) > 0 then 
		col_name = l_sqlda.GetDynamicString(1)
		if a_sqlca.sqlcode = 0 then
			if upperbound(a_cols_filter) > 0 then
				// need to make sure it's in their filter list
				for i = 1 to upperbound(a_cols_filter)
					include_column = false
					if upper(col_name) = upper(a_cols_filter[i]) then
						include_column = true
						exit
					end if
				next
			else
				include_column = true
			end if
			
			if include_column then
				cols++
				a_cols[cols] = col_name
				a_pks[cols] = 0
			end if
		end if
	end if
LOOP

if a_sqlca.sqlcode < 0 then
	uf_msgs(a_direction, "ERROR:  Retrieving tabling definitions (prepare): " + a_sqlca.sqlerrtext)
	uf_msgs(a_direction, a_dba_owner)
	uf_msgs(a_direction, a_dba_table)
	close target_curs;
	destroy l_sqlsa
	destroy l_sqlda
	return -1
else		
	close target_curs;
	destroy l_sqlsa
	destroy l_sqlda
end if

if cols <= 0 then
	uf_msgs(a_direction, "ERROR:  Getting tabling definitions.")
	uf_msgs(a_direction, a_dba_owner)
	uf_msgs(a_direction, a_dba_table)
	return -1
end if

// Really want to make sure the sort is the same as a_cols_filter if it was provided.
//	At this point, the 2 arrays should be the same size unless they asked for a column that doesn't exist
//	check and error if that's the case.
//	if they are the same, replace a_cols with a_cols_filter.
if upperbound(a_cols_filter) > 0 then
	if upperbound(a_cols_filter) = cols then
		a_cols = a_cols_filter
	else
		uf_msgs(a_direction, "ERROR:  All requested columns do not exist on the table.")
		uf_msgs(a_direction, a_dba_owner)
		uf_msgs(a_direction, a_dba_table)
		return -1
	end if
end if
	
if a_pk then	
	sqls = &
		"select column_name " + &
		"from INFORMATION_SCHEMA.KEY_COLUMN_USAGE " + &
		"where upper(table_schema) = '" + a_dba_owner + "' " + &
		"and upper(table_name) = '" + a_dba_table + "' "

	l_sqlsa = create DynamicStagingArea
	l_sqlda = create DynamicDescriptionArea
	PREPARE l_sqlsa from :sqls using a_sqlca;
	if a_sqlca.sqlcode < 0 then
		uf_msgs(a_direction, "ERROR:  Retrieving tabling definitions: " + a_sqlca.sqlerrtext)
		uf_msgs(a_direction, a_dba_owner)
		uf_msgs(a_direction, a_dba_table)
		destroy l_sqlsa
		destroy l_sqlda
		return -1
	end if
	DECLARE target_curs2 DYNAMIC CURSOR FOR l_sqlsa;
	OPEN DYNAMIC target_curs2 USING DESCRIPTOR l_sqlda;
	
	DO until a_sqlca.sqlcode <> 0
		FETCH target_curs2 USING DESCRIPTOR l_sqlda ;
	
		// check that we actually get results
		if upperbound(l_sqlda.outParmType) > 0 then 
			col_name = l_sqlda.GetDynamicString(1)
			if a_sqlca.sqlcode = 0 then
				for i = 1 to upperbound(a_cols)
					if a_cols[i] = col_name then
						a_pks[i] = 1
					end if
				next
			end if
		end if
	loop
	
	close target_curs2;
	destroy l_sqlsa
	destroy l_sqlda
end if

return upperbound(a_cols)

end function

on uo_cr_data_mover.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_data_mover.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string cv, sqls

cv = upper(trim(g_cr.uf_get_control_value('DATA MOVER DEBUG'))) //Autogenerated sql replace
	
if cv = 'YES' then 
	i_cv_debug = true
else
	i_cv_debug = false
end if


// ### 6603: JAK: 2011-02-02:  Instanced datastores
i_ds_data_mover = create uo_ds_top
sqls = "select * from cr_data_mover where -1 = 0"
f_create_dynamic_ds(i_ds_data_mover,'grid',sqls,sqlca,true)

i_uo_encrypt = create uo_encrypt
i_sqlca2 = create uo_sqlca

i_ini_file = f_get_ini_file()
i_dev_role = ProfileString(i_ini_file,  "Application", "Developer_Role", "")
if i_dev_role = "" then i_dev_role = 'pwrplant_role_dev'
i_dev_role = upper(i_dev_role)
end event

event destructor;destroy i_sqlca2
destroy i_uo_encrypt
end event

