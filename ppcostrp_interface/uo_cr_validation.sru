HA$PBExportHeader$uo_cr_validation.sru
$PBExportComments$10.3.1.1 Build 1
forward
global type uo_cr_validation from nonvisualobject
end type
end forward

global type uo_cr_validation from nonvisualobject
end type
global uo_cr_validation uo_cr_validation

type variables
boolean i_called_from_validator = false, &
		  i_called_from_allocations_suspense = false
string  i_co_field, i_use_new_structures_table, i_hint_str
boolean	i_me_autoextend

uo_ds_top i_ds_rules, i_ds_rule, i_ds_count, i_ds_control, i_ds_elements, i_ds_msg_one_col, i_ds_msg_three_col, &
	i_ds_extend, i_ds_invalid_mn, i_ds_rules_proj
longlong i_num_elements

string i_add_where, i_cv_control_or_combos, i_use_intermediate_tbl, i_log_combo_kickouts

string i_effmn_cv, i_effmn2_cv, i_effmn2_multi_rec_cv, i_cv_combo_vals, i_cv_delete_me, i_cv_use_exists, i_cv_acct_range
string i_wo_field, i_pp_co_field, i_cv_req_obey_default

uo_cr_validation_custom i_uo_cr_validation_custom
end variables

forward prototypes
public function longlong uf_validate_custom (string a_table_name, string a_ifb_id)
public function longlong uf_suspense (string a_table_name, string a_ifb_id)
public function longlong uf_validate_projects (string a_table_name, string a_ifb_id)
public function longlong uf_delete_invalid_ids (string a_table_name, string a_ifb_id)
public function longlong uf_validate_combos (string a_table_name, string a_ifb_id)
public function longlong uf_validate_control (string a_table_name, string a_ifb_id)
public function longlong uf_validate_combos_exclude (string a_table_name, string a_ifb_id, longlong a_rule_id)
public function longlong uf_validate_combos_required (string a_table_name, string a_ifb_id, longlong a_rule_id)
public function longlong uf_validate_me (string a_table_name, string a_ifb_id)
public function longlong uf_validate_combos_range_based (string a_table_name, string a_ifb_id, longlong a_rule_id)
public function longlong uf_validate_combos_exists (string a_table_name, string a_ifb_id, longlong a_rule_id)
public function longlong uf_validate_me_autoextend (string a_table_name, string a_ifb_id)
public function string uf_validate_combos_update_sql (string a_sqls)
public function longlong uf_validate_month_number (string a_table_name, string a_ifb_id, longlong a_source_id)
public function longlong uf_validate_month_number (string a_table_name, string a_ifb_id)
public function longlong uf_runvalidations (string a_table_name, string a_ifb_id, longlong a_source_id)
public function longlong uf_validate_me_combined (string a_table_name, string a_ifb_id, boolean a_is_bdg_element)
end prototypes

public function longlong uf_validate_custom (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_custom
//
//	EFFECTIVE 10.3:
//
//	CUSTOM VALIDATIONS SHOULD BE PLACED IN UO_CR_VALIDATION_CUSTOM!
//		DO NOT WRITE CUSTOM CODE HERE!
//
//*****************************************************************************************
return i_uo_cr_validation_custom.uf_validate_custom(a_table_name, a_ifb_id)

/***********************************/
/*		DO NOT WRITE CUSTOM CODE HERE!      */
/*		CUSTOM VALIDATIONS ARE NOW IN		  */
/*		UO_CR_VALIDATION_CUSTOM			  	  */
/***********************************/


end function

public function longlong uf_suspense (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_suspense
//
//	 Note			  :  Applies the suspense account to the invalid transactions in a stg
//                  table, so they can post and be corrected later.
//
//  Return Codes  :
//     1  =  The function executed successfully.  The txns in the batch can be posted.
//     2  =  Suspense accounting is turned off in cr_system_control.  The batch should not
//           be posted if there are validation errors.
//    -1  =  SQL Errors
//    -2  =  company field not found in cr_system_control
//    -3  =  NO LONGER USED: account field not found in cr_system_control
//    -4  =  NO LONGER USED: company master element table and element field not found
//    -5  =  some companies not found in the cr_suspense_account table
//
//
//  NOTES:  THIS FUNCTION SHOULD NOT NEED ANY REFERENCES TO THE INVALID_IDS2 TABLE
//          SINCE WE ARE NOT FLAGGING VALIDATION KICKOUTS IN THIS FUNCTION.
//
//*****************************************************************************************
longlong rtn, counter, i, num_elements
string cv, sqls, element


//*****************************************************************************************
//
//  ARE THEY USING SUSPENSE ACCOUNTING ... IF NOT, SPLIT.
//
//*****************************************************************************************
if i_called_from_allocations_suspense then
	cv = "YES"  //  Allocations have their own switch that was already evaluated.
else
	setnull(cv)
cv = upper(trim(g_cr.uf_get_control_value('VALIDATIONS - SUSPENSE ACCOUNTING'))) //Autogenerated sql replace
	if isnull(cv) then cv = "NO"
end if

//  The caller may need to know that suspense accounting is not being used.  Return
//  a 2 as the value.
if cv <> "YES" then return 2


if i_called_from_allocations_suspense then
	//  No msgs
else
	f_pp_msgs(" -- Performing suspense accounting at: " + string(now()))
end if


//*****************************************************************************************
//
//  GET SOME OTHER VARIABLES.
//
//*****************************************************************************************
//
////  Company Field:
//setnull(company_field)
//select upper(trim(control_value)) into :company_field from cr_system_control
// where upper(trim(control_name)) = 'COMPANY FIELD';
// 
//if isnull(company_field) or company_field = "" then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_SUSPENSE: cannot find the Company Field !")
//	f_pp_msgs("  ")
//	return -2
//end if
//
//
//  DON'T NEED THIS WITH THE NEW METHOD:
//
////  Account Field:
//setnull(account_field)
//select upper(trim(control_value)) into :account_field from cr_system_control
// where upper(trim(control_name)) = 'FERC ACCOUNT FIELD';
// 
//if isnull(account_field) or account_field = "" then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_SUSPENSE: cannot find the Ferc Account Field !")
//	f_pp_msgs("  ")
//	return -3
//end if

//
//  DON'T NEED THIS WITH THE NEW METHOD:
//
////  Company Master Element Table:
//setnull(company_me_table)
//setnull(company_me_field)
//select upper(trim(element_table)), upper(trim(element_column)) 
//  into :company_me_table, :company_me_field
//  from cr_elements
// where upper(trim(description)) = :company_field;
//
//if isnull(company_me_table) or company_me_table = "" or &
//   isnull(company_me_field) or company_me_field = "" &
//then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_SUSPENSE: " + &
//		"cannot find the Company master element table or master element field !")
//	f_pp_msgs("  ")
//	return -4
//end if


//*****************************************************************************************
//
//  ALL COMPANIES MUST BE IN THE CR_SUSPENSE_ACCOUNT TABLE OR WE CANNOT UNIVERSALLY APPLY
//  THE SUSPENSE ACCOUNTING.
//
//*****************************************************************************************
datastore ds_count
ds_count = CREATE datastore

//
//  OLD METHOD:
//
//sqls = "select count(*) from ( " + &
//		    'select "' + i_co_field + '" from ' + a_table_name + &
//		    " minus " + &
//		    'select "' + i_co_field + '" from ' + company_me_table + ")"

sqls = "select count(*) from ( " + &
		    'select "' + i_co_field + '" from ' + a_table_name + &
		    " minus " + &
		    'select "' + i_co_field + '" from cr_suspense_account)'

f_create_dynamic_ds(ds_count, "grid", sqls, sqlca, true)

if ds_count.RowCount() = 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_SUSPENSE: checking for missing companies !")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
else
	counter = ds_count.GetItemNumber(1, 1)
	if counter <> 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: There are " + string(counter) + &
			" companies in the transactions that do not exist in the cr_suspense_account table !")
		f_pp_msgs("Cannot perform the suspense accounting !")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		return -5
	end if
end if

DESTROY ds_count


//*****************************************************************************************
//
//  UPDATE THE STG TABLE WITH THE SUSPENSE ACCOUNTS (INVALID TRANSACTIONS ONLY).
//
//*****************************************************************************************

//
//  OLD METHOD:
//
//sqls = &
//	"update " + a_table_name + ' a set "' + account_field + '" = ( ' + &
//	'   select cr_suspense_account from "' + company_me_table + '" b ' + &
//	'    where a."' + i_co_field + '" = b."' + company_me_field + '" ' + &
//	'		 and a.id in (select id from cr_validations_invalid_ids ' + &
//	                   " where table_name = '" + a_table_name + "') " + &
//	"      and a.interface_batch_id = '" + a_ifb_id + "' and a." + i_add_where + ") " + &
//	" where id in (select id from cr_validations_invalid_ids " + &
//	             " where table_name = '" + a_table_name + "') " + &
//	"   and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where

datastore ds_elements
ds_elements = CREATE datastore

sqls = "select * from cr_elements"

f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)

ds_elements.SetSort("order a")
ds_elements.Sort()

num_elements = ds_elements.RowCount()

for i = 1 to num_elements
	
	element = upper(ds_elements.GetItemString(i, "description"))
	
	element = f_cr_clean_string(element)
	
	//  Do not update the company field.  It is the basis for the join.
	if element = i_co_field then continue
	
	//  DMJ: 3/23/2009: Added masking so fields that are critical for balancing can be masked
	//  through from the underlying transactions.  Would be used if fields like journal name or
	//  journal id are included in the accounting key.
	sqls = &
		"update " + a_table_name + ' a set "' + element + '" = ( ' + &
		'	select decode( b."' + element + '",' + " '*', " + 'a."' + element + '", nvl("' + element + '"' + ", ' ') ) " + &
		   ' from cr_suspense_account b ' + &
		'	 where a."' + i_co_field + '" = b."' + i_co_field + '" ' + &
		'		and a.id in (select id from cr_validations_invalid_ids ' + &
								" where table_name = '" + a_table_name + "') " + &
		"		and a.interface_batch_id = '" + a_ifb_id + "' and a." + i_add_where + ") " + &
		" where id in (select id from cr_validations_invalid_ids " + &
						  " where table_name = '" + a_table_name + "') " + &
		"	and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where
	
	if isnull(sqls) then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_SUSPENSE: applying suspense accounts !")
		f_pp_msgs("ERROR MESSAGE: The sqls variable is NULL !")
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_SUSPENSE: applying suspense accounts !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs("SQL = " + sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
next  //  for i = 1 to num_elements ...


//*****************************************************************************************
//
//  ARCHIVE THE RECORDS INTO THE CR_VALIDATIONS_INVALID_IDS3 TABLE TO BE ACCESSED FROM
//  QUERY RESULTS IN THE CR.
//
//*****************************************************************************************
if upper(a_table_name) = 'CR_JOURNAL_LINES_STG' then
	sqls = &
		"insert into cr_validations_invalid_ids3 (table_name, id, msg) ( " + &
		"select table_name, id, msg from cr_validations_invalid_ids2 " + &
		 "where table_name = '" + a_table_name + "' " + &
		 	"and id in (select id from " + a_table_name + &
			           " where journal_id = " + a_ifb_id + " and " + i_add_where + " ))"
else
	sqls = &
		"insert into cr_validations_invalid_ids3 (table_name, id, msg) ( " + &
		"select table_name, id, msg from cr_validations_invalid_ids2 " + &
		 "where table_name = '" + a_table_name + "' " + &
		 	"and id in (select id from " + a_table_name + &
			           " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + "))"
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_SUSPENSE: inserting into cr_validations_invalid_ids3 !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	f_pp_msgs("SQL = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//*****************************************************************************************
//
//  IF THE UPDATE FIRED SUCCESSFULLY, THE TRANSACTIONS ARE GOING TO POST.  DELETE ALL
//  RECORDS OUT OF CR_VALIDATIONS_INVALID_IDS SINCE THERE ARE NOT REALLY ANY KICKOUTS.
//
//*****************************************************************************************
if i_called_from_allocations_suspense then
	//  No msgs
else
	f_pp_msgs(" -- Deleting from cr_validations_invalid_ids at: " + string(now()))
end if

rtn = uf_delete_invalid_ids(a_table_name, a_ifb_id);

if rtn <> 1 then
	//  Message logged in uf_delete_invalid_ids.
	rollback;
	return -1
end if


commit;


return 1
end function

public function longlong uf_validate_projects (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_projects
//
//  Return Codes :   1 = Success
//						  -1 = SQL Error
// 					  -2 = Validation kickouts occurred
//
//	 Note			  : There is no reference to the cr_validation_exclusion table in this f(x).
//						 It is assumed that a rule will not include elements that should not be 
//						 validated.
//
//*****************************************************************************************
longlong i, num_rules, num_cols, c, counter, rule_id, ncount, table_source_id
string  sqls, validate_sqls, col_name, col_val, anchor_col_name, rule, count_sqls, &
		  select_statement, outer_sqls, src_ids_that_ignore
string 	cr_el_col_field, cr_element_col, pp_element_column, pp_table
string	joining_table, joining_where, where_sql, from_sql, top_where_sql, top_from_sql
string	comps_obeying, comp_where
boolean kickouts, called_from_kickouts = false

//
//	MAKE THE ASSUMPTION IF THERE ARE RECORDS IN CR_VALIDATIONS_INVALID_IDS
//		FOR A_TABLE_NAME AND A_IFB THEN THIS USER OBJECT WAS CALLED FROM THE
//		KICKOUT WINDOW SINCE THE INTERFACE AND JE WINDOW WOULD HAVE CALLED
//		UF_DELETE_INVALID_IDS.
//
//datastore ds_check_if_online
//ds_check_if_online = create datastore
sqls = "select count(*) from cr_validations_invalid_ids where id in (" + &
	"select id from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + ")"
	
//f_create_dynamic_ds(ds_check_if_online,"grid",sqls,sqlca,true)
i_ds_count.Reset()
i_ds_count.SetSQLSelect(sqls)
i_ds_count.Retrieve()
counter = i_ds_count.getitemnumber(1,1)

if isnull(counter) then counter = 0

if counter > 0 then
	called_from_kickouts = true
end if

//	Start off by marking everything valid and then make them invalid in the loop.
//	This method is preferred for a number of reasons.
//	1) Avoid using "not in" while touching as little existing code as possible.
//	2) If I have two rules and during the first rule everything passes so 
//		valid_online is set to one.  They fail the second rule but the first rule
//		marked them as valid so we would have to go back and unmark them.  It is
//		easier to just invalidate something that fails during a rule rather than
//		making it valid and then worrying about making it invalid in another rule.
sqls = "update cr_validations_invalid_ids set valid_online = 1 where " + &
	"(table_name,id) in (select '" + a_table_name + "',id from " + a_table_name + &
	" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + ")"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_PROJECTS: updating cr_validations_invalid_ids.valid_online !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("SQLS = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//
//  SETUP.
//
//datastore ds_count
//ds_count = CREATE datastore

//sqls = 'select count(*) from cr_elements where element_id = -1'
//// Create the shell of this datastore
//f_create_dynamic_ds(ds_count, "grid", sqls, sqlca, true)

kickouts = false

//datastore ds_rules
//ds_rules = CREATE datastore
//
//sqls = "select * from cr_validation_rules_projects where active = 'Y' order by rule_id"
//
//f_create_dynamic_ds(ds_rules, "grid", sqls, sqlca, true)

num_rules = i_ds_rules_proj.RowCount()


////GET Tehe work order num field
//setnull(i_wo_field)
//select upper(rtrim(control_value))
//into :i_wo_field
//from cr_system_control 
//where upper(trim(control_name)) = 'WORK ORDER FIELD';
//
////GET The powerplant company field
//setnull(i_pp_co_field)
//select upper(rtrim(control_value))
//into :i_pp_co_field
//from cr_system_control 
//where upper(trim(control_name)) = 'POWERPLANT COMPANY FIELD';
//
//i_wo_field = f_cr_clean_string(i_wo_field)
//i_pp_co_field = f_cr_clean_string(i_pp_co_field)

//
//  LOOP OVER EACH RULE.
//
for i = 1 to num_rules
	setnull(cr_el_col_field)
	setnull(cr_element_col)
	setnull(pp_element_column)
	setnull(pp_table)
	setnull(joining_table)
	setnull(joining_where)
	setnull(rule)
	setnull(comps_obeying)
	
	rule          			= i_ds_rules_proj.GetItemString(i,"rule")		
	cr_el_col_field      = i_ds_rules_proj.GetItemString(i,"master_element_column")
	pp_table         	 	= i_ds_rules_proj.GetItemString(i,"pp_table")
	pp_element_column    = upper(trim(i_ds_rules_proj.GetItemString(i,"pp_column")))
	joining_table        = i_ds_rules_proj.GetItemString(i,"joining_table")
	joining_where        = i_ds_rules_proj.GetItemString(i,"joining_where")
	comps_obeying			= i_ds_rules_proj.GetItemString(i,"companies_obeying_rule")
	src_ids_that_ignore  = trim(i_ds_rules_proj.GetItemString(i,"source_ids_that_ignore"))
	
	if isnull(src_ids_that_ignore) or src_ids_that_ignore = "" then
		src_ids_that_ignore = "NONE"
	end if
	
	table_source_id = 0
table_source_id = g_cr.uf_get_source_id("table_name",a_table_name) //Autogenerated sql replace
	if isnull(table_source_id) then table_source_id = 0
	
	if isnull(comps_obeying) or comps_obeying = "" then
		comp_where = ""
	else
		comp_where = ' and ' + a_table_name + '."' +&
						i_pp_co_field + '" in (' + comps_obeying + ')'
	end if
	
cr_element_col = upper(trim(g_cr.uf_get_control_value(cr_el_col_field))) //Autogenerated sql replace
	
	if isnull(cr_element_col) then cr_element_col = upper(cr_el_col_field)
	
	f_cr_clean_string(cr_element_col)
	
	if isnull(rule) then rule = ""
	
	//  DMJ:  ADDED interface_batch_id OR ELSE WE PULL OTHER STUFF FROM THE STG TABLE
	//        THAT PASSES THE "IN".  IT WOULD HAPPEN A LOT ON JOURNAL ENTRIES.
	//
	//        ADDED NVL'S TO CATCH PROBLEMS WITH NULL VALUES IN JOURNAL ENTRIES.
//	outer_sqls       = &
//		"insert into cr_validations_invalid_ids2 " + &
//			"(table_name, id, msg) " + &
//				"( select '" + a_table_name + "', id, " + " 'PROJECT (" + rule + ")' " + &				
//				" from " + a_table_name + ' where ( "' + i_wo_field + '", "' + cr_element_col +&
//				'" ) in '
	outer_sqls       = &
		"insert into cr_validations_invalid_ids2 " + &
			"(table_name, id, msg) " + &
				"( select '" + a_table_name + "', id, " + " 'PROJECT (" + rule + ")' " + &				
				   " from " + a_table_name + &
				  " where interface_batch_id = '" + a_ifb_id + "' " + " and " + i_add_where + &
				    ' and ( nvl("' + i_wo_field + '",' + "'  ')" + ', nvl("' + cr_element_col +&
				'",' + "'  ')" + ' ) in '
	
	
	from_sql 		= pp_table + ", " + a_table_name
	top_from_sql 	= a_table_name
	if not (isnull(joining_table) or trim(joining_table) = "") then
		from_sql 		= from_sql 		+ ", " + joining_table
		top_from_sql 	= top_from_sql + ", " + joining_table
	end if
	
	top_where_sql = &
					" work_order_control.work_order_number = " + a_table_name + '."' + i_wo_field +&
					'" and work_order_control.funding_wo_indicator = 0' +&
					" and " + a_table_name + ".interface_batch_id = '" + a_ifb_id + "' and " + a_table_name + "." + i_add_where + " "

	where_sql = top_where_sql
					
	if not (isnull(joining_table) or trim(joining_table) = "") then
		where_sql = where_sql +&
				" and " + joining_where
	end if

// interface_batch_id is part of top_where_sql
validate_sqls = "SELECT nvl(" + a_table_name + '."' + i_wo_field + '",' + "'  ')" + ', ' +&
						"nvl(" + a_table_name + '."' + cr_element_col + &
						'",' + "'  ')" + ' FROM ' + top_from_sql + &
						" WHERE " + top_where_sql + comp_where
	
	if src_ids_that_ignore <> "NONE" then
		if lower(a_table_name) = 'cr_all_details_dr_stg' &
		or lower(a_table_name) = 'cr_all_details_orig_stg' then
			//  Must look at the source_id field on these tables ... this will filter out the
			//  sources that ignore this rule (e.g. PowerPlant for blank AFUDC task).
			validate_sqls = validate_sqls + " and source_id not in (" + src_ids_that_ignore + ") "
		else
			//  The source_id was retrieved above ... use it.  For example ... PowerPlant source = 20
			//  and 20 is the ignore source yields "and 20 not in (20)" which is false and thus the
			//  top part of the sqls with the CR STG transactions returns no records.  Conversely, if
			//  something like AP where source_id = 1 then "and 1 not in (20)" is true and the top
			//  sqls returns all CR STG records.
			validate_sqls = validate_sqls + " and " + string(table_source_id) + " not in (" + src_ids_that_ignore + ") "
		end if
	end if
	
	// THE MINUS
	validate_sqls = validate_sqls +&
					" MINUS SELECT work_order_control.work_order_number, " +&
					"nvl(" + pp_table + '."' + pp_element_column + '", ' +&
					a_table_name + '."' + cr_element_col + '")' +&
					' FROM ' + from_sql +&
					" WHERE " + where_sql
	
	//  Must be before the command that adds the outer_sqls.
	count_sqls  = "select count(*) from (" + validate_sqls  + ")"
	
	
	//
	//  CLEANUP:
	//
	//  Since more than one column could be invalid and currently we cannot insert the
	//  id more than once.
	//
	validate_sqls  = outer_sqls + "( " + validate_sqls  + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	i_ds_count.Reset()
	i_ds_count.SetSQLSelect(count_sqls)
	i_ds_count.RETRIEVE()

	ncount = i_ds_count.GetItemNumber(1, 1)
	
	
	if ncount > 0 then
		f_pp_msgs("************  PROJECT (" + rule + "): THERE ARE " + &
			string(ncount) + " INVALID COMBINATIONS.")
		kickouts = true
	end if
	
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_PROJECTS: inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
next  //  for i = 1 to num_rules ...


insert into cr_validations_invalid_ids (table_name, id) (
	select distinct table_name, id from cr_validations_invalid_ids2
	 where table_name = :a_table_name
	minus
	select table_name, id from cr_validations_invalid_ids);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_PROJECTS: inserting into cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_count
	return -1
end if


//  DMJ: 12/27/05: This update has an inherent problem.  It does not restrict by interface_batch_id or journal_id.
//                 Since we coded around this in the Delete Line button of m_cr_journals ...  and since it really
//                 should not affect feeder sources, I am going to leave this alone.  I feel it is too dangerous
//                 to change.  If it causes any client problems we can change it later.
//  JAK: 11/13/06: Pinnacle hit a deadlock error a few times when many users were hammering on journal entries
//						 Added an interface_batch_id restriction to the below SQL to try and alleviate this.
//update cr_validations_invalid_ids
//set valid_online = 0
//where id in (
//	select id from cr_validations_invalid_ids2
//	where table_name = :a_table_name)
//and table_name = :a_table_name;
if lower(a_table_name) = "cr_journal_lines_stg" then
	sqls = 'update cr_validations_invalid_ids ' + &
		'set valid_online = 0 ' + &
		'where id in ( ' + &
		'select id from cr_validations_invalid_ids2 ' + &
		"where table_name = '" + a_table_name + "' " + &
		"and id in (select id from " + a_table_name + &
		" where journal_id = '" + a_ifb_id + "' and " + i_add_where + ")) " + &
		"and table_name = '" + a_table_name + "'"
else
	sqls = 'update cr_validations_invalid_ids ' + &
		'set valid_online = 0 ' + &
		'where id in ( ' + &
		'select id from cr_validations_invalid_ids2 ' + &
		"where table_name = '" + a_table_name + "' " + &
		"and id in (select id from " + a_table_name + &
		" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + ")) " + &
		"and table_name = '" + a_table_name + "'"
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_PROJECTS: updating cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_count
	return -1
end if


commit;


if kickouts then
	return -2
else
	return 1
end if


end function

public function longlong uf_delete_invalid_ids (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_delete_invalid_ids
//
//	 Note			  :  No other validation functions will delete from this table.  The delete
//						  must be called separately from within the interface before calling the
//						  other functions.
//
//*****************************************************************************************
string sqls


//
//  *****  CR_VALIDATIONS_INVALID_IDS2  *****  //
//
if upper(a_table_name) = 'CR_JOURNAL_LINES_STG' then
	sqls = &
	"delete from cr_validations_invalid_ids2 " + &
	" where table_name = '" + a_table_name + "' " + &
	 "  and id in (select id from " + a_table_name + &
					  " where journal_id = " + a_ifb_id + ")"
else
	sqls = &
	"delete from cr_validations_invalid_ids2 " + &
	" where table_name = '" + a_table_name + "' " + &
	 "  and id in (select id from " + a_table_name + &
					  " where interface_batch_id = '" + a_ifb_id + "')"
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN VALIDATIONS: deleting from cr_validations_invalid_ids2 !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//
//  *****  CR_VALIDATIONS_INVALID_IDS  *****  //
//
if upper(a_table_name) = 'CR_JOURNAL_LINES_STG' then
	sqls = &
	"delete from cr_validations_invalid_ids " + &
	" where table_name = '" + a_table_name + "' " + &
	 "  and id in (select id from " + a_table_name + &
					  " where journal_id = " + a_ifb_id + ")"
else
	sqls = &
	"delete from cr_validations_invalid_ids " + &
	" where table_name = '" + a_table_name + "' " + &
	 "  and id in (select id from " + a_table_name + &
					  " where interface_batch_id = '" + a_ifb_id + "')"
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN VALIDATIONS: deleting from cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

return 1
end function

public function longlong uf_validate_combos (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_combos
//
//  Return Codes :   1 = Success
//						  -1 = SQL Error
// 					  -2 = Validation kickouts occurred
//
//	 Note			  : There is no reference to the cr_validation_exclusion table in this f(x).
//						 It is assumed that a rule will not include elements that should not be 
//						 validated.
//
//  12/09/2004 : Wild-Card (value required) rules added to validations.
//  ---------------------------------------------------------------------------------------
//  1)  Add a flag to the Rules table to identify the rule as a wild-card rule.  The only
//      value allowed in the control table will be * for the validation fields.  Need an
//      SQL for upgrading existing clients and also add code to w_cr_setup (make this a
//      combo_type field that could ultimately have the following values:
//      (Include, Exclude, Required).  Need DDLB on the rules window for those 3 values.
//  2)  Add a big if-then-else block (or call another function) in the rules loop.  If the
//      rule is a wild-card, then do not go through the traditional processing.  Use this
//      technique for "Not" validations also.
//  3)  In the wild-card logic, use the following construct:
//        select decode(field1,' ','!','*'), decode(field2,' ','!','*') from table_name 
//         where anchor_col in (select anchor_col from cr_validation_combos)
//        minus
//        select field from cr_validation_combos
//  4)  In the "Not" logic, use the count_sqls construct where we join the tables together
//      and simply flip the operators.  This will make those txns that would normally pass
//      validations in that time-frame, fail them and be inserted into the invalid table.
//
//*****************************************************************************************
longlong i, num_rules, num_cols, c, counter, rule_id, ncount, rtn, range_based
string  sqls, validate_sqls, col_name, col_val, anchor_col_name, rule, count_sqls, &
		  select_statement, outer_sqls, effmn_and_clause, validate_sqls2, &
		  count_sqls2, combo_type
//  DML: MULTI-ANCHOR
string  anchor_col_name_no_alias, anchor_col_name_alias, anchor_col_name_select
boolean kickouts, called_from_kickouts = false

//	MAKE THE ASSUMPTION IF THERE ARE RECORDS IN CR_VALIDATIONS_INVALID_IDS
//		FOR A_TABLE_NAME AND A_IFB THEN THIS USER OBJECT WAS CALLED FROM THE
//		KICKOUT WINDOW SINCE THE INTERFACE AND JE WINDOW WOULD HAVE CALLED
//		UF_DELETE_INVALID_IDS.
sqls = "select count(*) from cr_validations_invalid_ids where id in (" + &
	"select id from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + ")"
	
sqls = uf_validate_combos_update_sql(sqls)
f_create_dynamic_ds(i_ds_count,"grid",sqls,sqlca,true)

counter = i_ds_count.getitemnumber(1,1)

if isnull(counter) then counter = 0

if counter > 0 then
	called_from_kickouts = true
end if

//	Start off by marking everything valid and then make them invalid in the loop.
//	This method is preferred for a number of reasons.
//	1) Avoid using "not in" while touching as little existing code as possible.
//	2) If I have two rules and during the first rule everything passes so 
//		valid_online is set to one.  They fail the second rule but the first rule
//		marked them as valid so we would have to go back and unmark them.  It is
//		easier to just invalidate something that fails during a rule rather than
//		making it valid and then worrying about making it invalid in another rule.
sqls = "update cr_validations_invalid_ids set valid_online = 1 where " + &
	"(table_name,id) in (select '" + a_table_name + "',id from " + a_table_name + &
	" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + ")"

sqls = uf_validate_combos_update_sql(sqls)
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: updating cr_validations_invalid_ids.valid_online !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("SQLS = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//
//  SETUP.
//
//datastore ds_count
//ds_count = CREATE datastore
//
//sqls = 'select count(*) from cr_elements where element_id = -1'
//// Create the shell of this datastore
//f_create_dynamic_ds(ds_count, "grid", sqls, sqlca, true)
//
//setnull(i_effmn_cv)
//select upper(trim(control_value)) into :i_effmn_cv from cr_system_control
// where lower(control_name) = 'validations: effective month number';
//if isnull(i_effmn_cv) or i_effmn_cv = "" then i_effmn_cv = 'NO'
//
//setnull(i_effmn2_cv)
//select upper(trim(control_value)) into :i_effmn2_cv from cr_system_control
// where lower(control_name) = 'validation combos: eff mn 2';
//if isnull(i_effmn2_cv) or i_effmn2_cv = "" then i_effmn2_cv = 'NO'
//
//setnull(effmn2_multi_rec_cv)
//select upper(trim(control_value)) into :effmn2_multi_rec_cv from cr_system_control
// where lower(control_name) = 'combos: effmn2: multiple records';
//if isnull(effmn2_multi_rec_cv) or effmn2_multi_rec_cv = "" then effmn2_multi_rec_cv = 'NO'
//
//// JAK:  20090709:  New control to use exists statements for include rules
//setnull(exists_cv)
//select upper(trim(control_value))	into :exists_cv from cr_system_control 
//	where upper(control_name) = 'VALIDATIONS - USE EXISTS CLAUSE';
//if isnull(exists_cv) or exists_cv = "" then exists_cv = 'NO'

kickouts = false

//datastore ds_rules
//ds_rules = CREATE datastore
//
//sqls = "select * from cr_validation_rules"
//
//f_create_dynamic_ds(ds_rules, "grid", sqls, sqlca, true)
//
//ds_rules.SetSort("rule a")
//ds_rules.Sort()

num_rules = i_ds_rules.RowCount()
num_cols  = long(i_ds_rules.Object.DataWindow.Column.Count)


//
//  LOOP OVER EACH RULE.
//
for i = 1 to num_rules
	
	effmn_and_clause = ""
	counter          = 1
	rule_id          = i_ds_rules.GetItemNumber(i,"rule_id")
	combo_type       = i_ds_rules.GetItemString(i,"combo_type")
	range_based 	  = i_ds_rules.GetItemNumber(i,"range_based")
	if isnull(range_based) then range_based = 0
	
	/*20100922:  To help with speed of the combos processes a hints table was created (cr_validation_rules_hints)
	that holds custom hints that clients can put on a table.  If no hint is used then i_hint_str is simply a space
	which should not affect any of the sql statements, otherwise the hint must be fully defined in the table.*/
	i_hint_str = ''
	select hint
	into :i_hint_str
	from cr_validation_rules_hints
	where table_name = :a_table_name
	and rule_id = :rule_id;
	
	if isnull(i_hint_str) or i_hint_str = '' then
		i_hint_str = ''
	else
		/*Add a space to the end as a just in case*/
		i_hint_str = ' ' + i_hint_str + ' '
	end if
	
	validate_sqls    = "select " + i_hint_str
	validate_sqls2   = "select " + i_hint_str + "distinct "
	
	if range_based = 1 then
		
		//  Call the Range Based combo function and skip the rest of this script.
		rtn = uf_validate_combos_range_based(a_table_name, a_ifb_id, rule_id)
		
		if rtn = -1 then
			rollback;
			return -1
		end if
		
		if rtn = -2 then
			kickouts = true
		end if
		
		goto next_rule
		
	end if
	
	if combo_type = "Exclude" then
		
		//  Call the "Exclude" combo function and skip the rest of this script.
		rtn = uf_validate_combos_exclude(a_table_name, a_ifb_id, rule_id)
		
		if rtn = -1 then
			rollback;
			return -1
		end if
		
		if rtn = -2 then
			kickouts = true
		end if
		
		goto next_rule
		
	end if
	
	if combo_type = "Required" then
		
		//  Call the "Exclude" combo function and skip the rest of this script.
		rtn = uf_validate_combos_required(a_table_name, a_ifb_id, rule_id)
		
		if rtn = -1 then
			rollback;
			return -1
		end if
		
		if rtn = -2 then
			kickouts = true
		end if
		
		goto next_rule
		
	end if
	
	// JAK:  20090709:  New function to use exists statements for include rules
	if combo_type = "Include" and i_cv_use_exists = 'YES' then
		//  Call the "Exists" combo function and skip the rest of this script.
		rtn = uf_validate_combos_exists(a_table_name, a_ifb_id, rule_id)
		if rtn = -1 then
			rollback;
			return -1
		end if
		if rtn = -2 then
			kickouts = true
		end if
		goto next_rule
	end if
	
	rule = ""
	select rule into :rule from cr_validation_rules where rule_id = :rule_id;
	if isnull(rule) then rule = ""
	//	DML: 080406: In addition to the intermediate table check, also add an interface_batch_id or
	//		journal_id restriction to the outer sqls.  The way it is now, any rows with the same
	//		values in the combo fields are inserted.  The inner sql has a journal_id/interface_batch_id
	//		restriction giving us a set of rows.  The outer sqls pulls everything from a_table_name
	//		matching "in" the inner sqls.  An additional restriction is needed to do this within 
	//		the journal or batch and not for all values in a_table_name.
	if i_use_intermediate_tbl = 'NO' then
		outer_sqls       = &
			"insert into cr_validations_invalid_ids2 " + &
				"(table_name, id, msg) " + &
					"( select " + i_hint_str + "'" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
					" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and "
	else
		outer_sqls       = &
			"insert into cr_validations_invalid_ids2a " + &
				"(table_name, id, msg) " + &
					"( select " + i_hint_str + "'" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
					" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " and "
	end if
	anchor_col_name  = ""
	select_statement = ""
	
	//  DML: MULTI-ANCHOR
	anchor_col_name_alias = "("
	anchor_col_name_no_alias = "("
	anchor_col_name_select = " "
	for c = 1 to num_cols
		col_name = upper(i_ds_rules.Describe("#" + string(c) + ".Name"))
		choose case col_name
			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
				//  Do nothing.
			case else
				col_name = '"' + col_name + '"'
				col_val = i_ds_rules.GetItemString(i, c)
				if col_val = "2" then
					anchor_col_name_alias = anchor_col_name_alias + "a." + col_name + ","
					anchor_col_name_no_alias = anchor_col_name_no_alias + col_name + ","
					anchor_col_name_select = anchor_col_name_select +  col_name + ","
				end if
		end choose
	next
	anchor_col_name_alias = mid(anchor_col_name_alias,1,len(anchor_col_name_alias) - 1) + ") "
	anchor_col_name_no_alias = mid(anchor_col_name_no_alias,1,len(anchor_col_name_no_alias) - 1) + ") "
	anchor_col_name_select = mid(anchor_col_name_select,1,len(anchor_col_name_select) - 1) + " "
	
	//
	//  1ST SELECT:
	//
	for c = 1 to num_cols
		
		col_name = upper(i_ds_rules.Describe("#" + string(c) + ".Name"))
				
		choose case col_name
			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
				//  Do nothing.
			case else
				//  Build the SQL.
				col_val = i_ds_rules.GetItemString(i, c)
//  DML: MULTI-ANCHOR: COMMENTED OUT THIS 1 LINE
//				if col_val = "2" then anchor_col_name = col_name
				if col_val = "1" or col_val = "2" then
					effmn_and_clause = effmn_and_clause + &
						' and a."' + col_name + '" = b."' + col_name + '"'
					if i_effmn2_multi_rec_cv = "YES" then
						effmn_and_clause = effmn_and_clause + " (+)"
					end if
					if counter = 1 then
						if i_effmn_cv = "YES" then
							validate_sqls    = validate_sqls    + 'a."' + col_name + '"'
////////							validate_sqls2   = validate_sqls2   + '"' + col_name + '"'
							validate_sqls2   = validate_sqls2   + 'nvl("' + col_name + '",' + "'  ')"
						else
							validate_sqls    = validate_sqls    + '"' + col_name + '"'
						end if
						select_statement = select_statement + '"' + col_name + '"'
////////						outer_sqls       = outer_sqls       + '("' + col_name + '"'
						outer_sqls       = outer_sqls       + '(nvl("' + col_name + '",' + "'  ')"
					else
						if i_effmn_cv = "YES" then
							//
							//  11/01/04:  PNM PERFORMANCE CHANGE:
							//    Comment out 2 lines and add 2 lines with commas instead of ||.
							//
//							validate_sqls    = validate_sqls    + "||a." + '"' + col_name + '"'
//							validate_sqls2   = validate_sqls2   + "||" + '"' + col_name + '"'
							validate_sqls    = validate_sqls    + ",a." + '"' + col_name + '"'
////////							validate_sqls2   = validate_sqls2   + "," + '"' + col_name + '"'
							validate_sqls2   = validate_sqls2   + ",nvl(" + '"' + col_name + '",' + "'  ')"
						else
							//
							//  11/01/04:  PNM PERFORMANCE CHANGE:
							//    Comment out 1 line and add 1 line with commas instead of ||.
							//
//							validate_sqls    = validate_sqls    + "||" + '"' + col_name + '"'
							validate_sqls    = validate_sqls    + "," + '"' + col_name + '"'
						end if
						//
						//  11/01/04:  PNM PERFORMANCE CHANGE:
						//    Comment out 2 lines and add 2 lines with commas instead of ||.
						//
//						select_statement = select_statement + "||" + '"' + col_name + '"'
//						outer_sqls       = outer_sqls       + "||" + '"' + col_name + '"'
						select_statement = select_statement + "," + '"' + col_name + '"'
////////						outer_sqls       = outer_sqls       + "," + '"' + col_name + '"'
						outer_sqls       = outer_sqls       + ",nvl(" + '"' + col_name + '",' + "'  ')"
					end if
					counter++
				end if
		end choose
		
		
		//  11/01/04:  PNM PERFORMANCE CHANGE:
		//    Added this code when I changed the || method to comma-separated method for 
		//    PNM performance.
		if c = num_cols then
			outer_sqls = outer_sqls + ")"
		end if
		
		
	next  //  for c = 1 to num_cols ...
	
	
	//
	//  1ST FROM:
	//
	if i_effmn_cv = "YES" then
		validate_sqls  = validate_sqls  + " from " + a_table_name + " a, cr_validation_combos b "
		validate_sqls2 = validate_sqls2 + " from " + a_table_name + " "
	else
		validate_sqls = validate_sqls + " from " + a_table_name + " "
	end if
	outer_sqls    = outer_sqls    + " in ( "
	
	
	//
	//  1ST WHERE CLAUSE:
	//
	if i_effmn_cv = "YES" then
		
		//  DML: MULTI-ANCHOR
		validate_sqls = validate_sqls + " where " + anchor_col_name_alias + ' in (' + &
			"select " + anchor_col_name_select + &
			' from cr_validation_combos where rule_id = ' + &
			"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//			validate_sqls = validate_sqls + " where a." + '"' + anchor_col_name + '" in (' + &
//				"select " + '"' + anchor_col_name + &
//				'" from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
		
		if i_effmn2_cv = "YES" then
			if i_effmn2_multi_rec_cv = "YES" then
				validate_sqls = validate_sqls + " " + effmn_and_clause + &
					" and b.rule_id (+) = " + string(rule_id) + &
					" and a.month_number between b.effective_month_number (+) and b.effective_month_number2 (+)" + &
					" and b.status (+) = 1 and b.effective_month_number is null"
			else
				validate_sqls = validate_sqls + " " + effmn_and_clause + &
					" and b.rule_id = " + string(rule_id) + &
					" and ( ((a.month_number < b.effective_month_number or a.month_number > b.effective_month_number2) and b.status = 1) or " + &
					" (a.month_number >= b.effective_month_number and a.month_number <= b.effective_month_number2 and b.status = 0) )"
			end if
		else
			validate_sqls = validate_sqls + " " + effmn_and_clause + &
				" and b.rule_id = " + string(rule_id) + &
				" and (a.month_number <  b.effective_month_number and b.status = 1 or " + &
				" a.month_number >= b.effective_month_number and b.status = 0)"
		end if
		
		//  DML: MULTI-ANCHOR
		validate_sqls2 = validate_sqls2 + " where " + anchor_col_name_no_alias + ' in (' + &
			"select " + anchor_col_name_select + &
			' from cr_validation_combos where rule_id = ' + &
			"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//			validate_sqls2 = validate_sqls2 + " where " + '"' + anchor_col_name + '" in (' + &
//				"select " + '"' + anchor_col_name + &
//				'" from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
		
	else
		//  DML: MULTI-ANCHOR
		validate_sqls = validate_sqls + " where " + anchor_col_name_no_alias + ' in (' + &
			"select " + anchor_col_name_select + &
			' from cr_validation_combos where rule_id = ' + &
			"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//			validate_sqls = validate_sqls + " where " + '"' + anchor_col_name + '" in (' + &
//				"select " + '"' + anchor_col_name + &
//				'" from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
	end if
	
	
	//
	//  MINUS:
	//
	if i_effmn_cv = "YES" then
		//  Do nothing to validate_sqls.
		validate_sqls2 = validate_sqls2 + " minus "
	else
		validate_sqls = validate_sqls + " minus "
	end if
	
	
	//
	//  2ND SELECT:
	//
	if i_effmn_cv = "YES" then
		//  Do nothing to validate_sqls.
		validate_sqls2 = validate_sqls2 + " select distinct " + select_statement
	else
		validate_sqls = validate_sqls + " select " + select_statement
	end if
	
	
	//
	//  2ND FROM:
	//
	if i_effmn_cv = "YES" then
		//  Do nothing to validate_sqls.
		validate_sqls2 = validate_sqls2 + " from cr_validation_combos "
	else
		validate_sqls = validate_sqls + " from cr_validation_combos "
	end if
	
	
	//
	//  2ND WHERE:
	//
	if i_effmn_cv = "YES" then
		//  DML: MULTI-ANCHOR
		validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
			' and ' + anchor_col_name_no_alias + ' in ' + &
				'(select ' + anchor_col_name_select + ' from ' + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
//			validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
//				' and "' + anchor_col_name + '" in ' + &
//					'(select "' + anchor_col_name + '" from ' + a_table_name + &
//					 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
	else
		//  DML: MULTI-ANCHOR
		validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
			' and ' + anchor_col_name_no_alias + ' in ' + &
				'(select ' + anchor_col_name_select + ' from ' + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
//			validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
//				' and "' + anchor_col_name + '" in ' + &
//					'(select "' + anchor_col_name + '" from ' + a_table_name + &
//					 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
	end if
	
	//  Must be before the command that adds the outer_sqls.
	count_sqls  = "select count(*) from (" + validate_sqls  + ")"
	count_sqls2 = "select count(*) from (" + validate_sqls2 + ")"
	
	//
	//  CLEANUP:
	//
	//  Since more than one column could be invalid and currently we cannot insert the
	//  id more than once.
	//
	if i_use_intermediate_tbl = 'NO' then
		validate_sqls  = outer_sqls + validate_sqls  + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
		validate_sqls2 = outer_sqls + validate_sqls2 + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
	else
		//	remove the minus if using the intermediate table.  That's the whole purpose of the table.
		validate_sqls  = outer_sqls + validate_sqls  + " ) ) "
		validate_sqls2 = outer_sqls + validate_sqls2 + " ) ) "
	end if
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	i_ds_count.Reset()
	count_sqls = uf_validate_combos_update_sql(count_sqls)
	i_ds_count.SetSQLSelect(count_sqls)
	i_ds_count.RETRIEVE()

	ncount = i_ds_count.GetItemNumber(1, 1)
	
	//  If they are using the effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
	//  would be needed to evaluate for missing combo values.
	if ncount = 0 and i_effmn_cv = "YES" then
		i_ds_count.Reset()
		count_sqls2 = uf_validate_combos_update_sql(count_sqls2)
		i_ds_count.SetSQLSelect(count_sqls2)
		i_ds_count.RETRIEVE()
	
		ncount = i_ds_count.GetItemNumber(1, 1)
	end if
	
	if ncount > 0 then
		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
			string(ncount) + " INVALID COMBINATIONS.")
		kickouts = true
	ELSE
		GOTO NEXT_RULE
	end if
	
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	
	validate_sqls = uf_validate_combos_update_sql(validate_sqls)
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
//  MOVED ABOVE
//	if sqlca.SQLNRows > 0 then
//		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
//			string(ncount) + " INVALID COMBINATIONS.")
//		kickouts = true
//	end if

	//  If they are using effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
	//  would be needed to evaluate for missing combo values.
	if i_effmn_cv = "YES" then
		
		if sqlca.SQLNRows > 0 then
			//  We got a validation kickout on validate_sqls ... no need to evaluate validate_sqls2.
			//  Continue on to the next rule.
			if i_use_intermediate_tbl = 'NO' then
				continue
			else
				goto clear_intermediate_tbl
			end if
		else
			//  The validate_sqls did not get a kickout ... evaluate validate_sqls2 next.
			
			//
			//  INSERT THE INVALID IDS:
			//
			validate_sqls2 = uf_validate_combos_update_sql(validate_sqls2)
			execute immediate :validate_sqls2;
			
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
				f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				f_pp_msgs(validate_sqls2)
				f_pp_msgs("  ")
				rollback;
//				DESTROY ds_count
				return -1
			end if
			
		end if
		
	end if
	
	//	This insert needs to be in the loop.  We are trying to avoid a minus between the 
	//		validate_sqls and cr_validations_invalid_ids2(or 2a) if use_intermediate_tbl = 'YES'.  
	//		We are inserting into cr_validations_invalid_ids2a so without the minus it needs
	//		to be cleared out in the loop.
	clear_intermediate_tbl:
	if i_use_intermediate_tbl = 'NO' then 
	else
		
		//	Insert into 2 from 2a here.  2a must also be cleared out before continuing on in case another
		//		function tries to insert into 2a.  This is needed since there isn't a minus anymore.
		insert into cr_validations_invalid_ids2 (table_name, id, msg)
		select table_name, id, msg from cr_validations_invalid_ids2a
		minus
		select table_name, id, msg from cr_validations_invalid_ids2;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Inserting into cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			//	Don't return...always clear out cr_validations_invalid_ids2a.
//			return -1
		end if
		
		sqlca.truncate_table('cr_validations_invalid_ids2a')
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Truncating cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	end if

	//	the truncate will fire a commit.  Go ahead and put one here to be explicit.
	commit;
	
	next_rule:

next  //  for i = 1 to num_rules ...


insert into cr_validations_invalid_ids (table_name, id) (
	select distinct table_name, id from cr_validations_invalid_ids2
	 where table_name = :a_table_name
	minus
	select table_name, id from cr_validations_invalid_ids);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_count
	return -1
end if


//  DMJ: 12/27/05: This update has an inherent problem.  It does not restrict by interface_batch_id or journal_id.
//                 Since we coded around this in the Delete Line button of m_cr_journals ...  and since it really
//                 should not affect feeder sources, I am going to leave this alone.  I feel it is too dangerous
//                 to change.  If it causes any client problems we can change it later.
//  JAK: 11/13/06: Pinnacle hit a deadlock error a few times when many users were hammering on journal entries
//						 Added an interface_batch_id restriction to the below SQL to try and alleviate this.
//update cr_validations_invalid_ids
//set valid_online = 0
//where id in (
//	select id from cr_validations_invalid_ids2
//	where table_name = :a_table_name)
//and table_name = :a_table_name;
if lower(a_table_name) = "cr_journal_lines_stg" then
	sqls = 'update cr_validations_invalid_ids ' + &
		'set valid_online = 0 ' + &
		'where id in ( ' + &
		'select id from cr_validations_invalid_ids2 ' + &
		"where table_name = '" + a_table_name + "' " + &
		"and id in (select id from " + a_table_name + &
		" where journal_id = '" + a_ifb_id + "' and " + i_add_where + " )) " + &
		"and table_name = '" + a_table_name + "'"
else
	sqls = 'update cr_validations_invalid_ids ' + &
		'set valid_online = 0 ' + &
		'where id in ( ' + &
		'select id from cr_validations_invalid_ids2 ' + &
		"where table_name = '" + a_table_name + "' " + &
		"and id in (select id from " + a_table_name + &
		" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " )) " + &
		"and table_name = '" + a_table_name + "'"
end if

sqls = uf_validate_combos_update_sql(sqls)
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: updating cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_count
	return -1
end if


commit;


if kickouts then
	return -2
else
	return 1
end if


end function

public function longlong uf_validate_control (string a_table_name, string a_ifb_id);////*****************************************************************************************
////
////  User Object  :  uo_cr_validation
////
////  UO Function  :  uf_validate_control
////
////  Return Codes :   1 = Success
////						  -1 = SQL Error
//// 					  -2 = Validation kickouts occurred
////
////	 Note			  : There is no reference to the cr_validation_exclusion table in this f(x).
////						 It is assumed that a rule will not include elements that should not be 
////						 validated.
////
////*****************************************************************************************

// JAK: Call uf_validate_combos now and then just change the table name at the end
return uf_validate_combos(a_table_name, a_ifb_id)

//longlong i, num_rules, num_cols, c, counter, rule_id, ncount
//string  sqls, validate_sqls, col_name, col_val, anchor_col_name, rule, count_sqls, &
//		  select_statement, outer_sqls, effmn_and_clause, validate_sqls2, &
//		  count_sqls2
////  DML: MULTI-ANCHOR
//string  anchor_col_name_no_alias, anchor_col_name_alias, anchor_col_name_select, &
//	use_intermediate_tbl
//boolean kickouts, called_from_kickouts = false
//
////	DML: 080206: What technique do we use?  Includes all references to "use_intermediate_tbl" below.
//select upper(control_value) into :use_intermediate_tbl
//from cr_system_control
//where upper(control_name) = 'VALIDATIONS - USE INTERMEDIATE TBL';
//
//if isnull(use_intermediate_tbl) or use_intermediate_tbl = "" then 
//	use_intermediate_tbl = 'NO'
//end if
//
////
////	MAKE THE ASSUMPTION IF THERE ARE RECORDS IN CR_VALIDATIONS_INVALID_IDS
////		FOR A_TABLE_NAME AND A_IFB THEN THIS USER OBJECT WAS CALLED FROM THE
////		KICKOUT WINDOW SINCE THE INTERFACE AND JE WINDOW WOULD HAVE CALLED
////		UF_DELETE_INVALID_IDS.
////
//datastore ds_check_if_online
//ds_check_if_online = create datastore
//sqls = "select count(*) from cr_validations_invalid_ids where id in (" + &
//	"select id from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " )"
//	
//f_create_dynamic_ds(ds_check_if_online,"grid",sqls,sqlca,true)
//
//counter = ds_check_if_online.getitemnumber(1,1)
//
//if isnull(counter) then counter = 0
//
//if counter > 0 then
//	called_from_kickouts = true
//end if
//
////	Start off by marking everything valid and then make them invalid in the loop.
////	This method is preferred for a number of reasons.
////	1) Avoid using "not in" while touching as little existing code as possible.
////	2) If I have two rules and during the first rule everything passes so 
////		valid_online is set to one.  They fail the second rule but the first rule
////		marked them as valid so we would have to go back and unmark them.  It is
////		easier to just invalidate something that fails during a rule rather than
////		making it valid and then worrying about making it invalid in another rule.
//sqls = "update cr_validations_invalid_ids set valid_online = 1 where " + &
//	"(table_name,id) in (select '" + a_table_name + "',id from " + a_table_name + &
//	" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " )"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: updating cr_validations_invalid_ids.valid_online !")
//	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//	f_pp_msgs("SQLS = " + sqls)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if
//
////
////  SETUP.
////
//datastore ds_count
//ds_count = CREATE datastore
//
//sqls = 'select count(*) from cr_elements where element_id = -1'
//// Create the shell of this datastore
//f_create_dynamic_ds(ds_count, "grid", sqls, sqlca, true)
//
//setnull(i_effmn_cv)
//select upper(trim(control_value)) into :i_effmn_cv from cr_system_control
// where lower(control_name) = 'validations: effective month number';
//if isnull(i_effmn_cv) or i_effmn_cv = "" then i_effmn_cv = 'NO'
//
//kickouts = false
//
//datastore ds_rules
//ds_rules = CREATE datastore
//
//sqls = "select * from cr_validation_rules"
//
//f_create_dynamic_ds(ds_rules, "grid", sqls, sqlca, true)
//
//ds_rules.SetSort("rule a")
//ds_rules.Sort()
//
//num_rules = ds_rules.RowCount()
//num_cols  = long(ds_rules.Object.DataWindow.Column.Count)
//
//
////
////  LOOP OVER EACH RULE.
////
//for i = 1 to num_rules
//	
//	effmn_and_clause = ""
//	counter          = 1
//	validate_sqls    = "select "
//	validate_sqls2   = "select "
//	
//	// JAK 20090316:  moved this block up from below...the rule variable needs to be populated when
//	//		building the select statement for the rule to be included in the kickouts.
//	rule_id          = ds_rules.GetItemNumber(i,"rule_id")
//	rule = ""
//	select rule into :rule from cr_validation_rules where rule_id = :rule_id;
//	if isnull(rule) then rule = ""
//	
//	//	DML: 080406: In addition to the intermediate table check, also add an interface_batch_id or
//	//		journal_id restriction to the outer sqls.  The way it is now, any rows with the same
//	//		values in the combo fields are inserted.  The inner sql has a journal_id/interface_batch_id
//	//		restriction giving us a set of rows.  The outer sqls pulls everything from a_table_name
//	//		matching "in" the inner sqls.  An additional restriction is needed to do this within 
//	//		the journal or batch and not for all values in a_table_name.
//	if use_intermediate_tbl = 'NO' then
//		if lower(a_table_name) = "cr_journal_lines_stg" then
//			outer_sqls       = &
//				"insert into cr_validations_invalid_ids2 " + &
//					"(table_name, id, msg) " + &
//						"( select '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
//						" from " + a_table_name + " where journal_id = " + a_ifb_id + "  and " + i_add_where + " and "
//		else
//			outer_sqls       = &
//				"insert into cr_validations_invalid_ids2 " + &
//					"(table_name, id, msg) " + &
//						"( select '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
//						" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and "
//		end if
//	else
//		if lower(a_table_name) = "cr_journal_lines_stg" then
//			outer_sqls       = &
//				"insert into cr_validations_invalid_ids2a " + &
//					"(table_name, id, msg) " + &
//						"( select '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
//						" from " + a_table_name + " where journal_id = " + a_ifb_id + "  and " + i_add_where + " and "
//		else
//			outer_sqls       = &
//				"insert into cr_validations_invalid_ids2a " + &
//					"(table_name, id, msg) " + &
//						"( select '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
//						" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and "
//		end if
//	end if
//	anchor_col_name  = ""
//	select_statement = ""	
//	
//	//  DML: MULTI-ANCHOR
//	anchor_col_name_alias = "("
//	anchor_col_name_no_alias = "("
//	anchor_col_name_select = " "
//	for c = 1 to num_cols
//		col_name = upper(ds_rules.Describe("#" + string(c) + ".Name"))
//		choose case col_name
//			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
//				//  Do nothing.
//			case else
//				col_name = '"' + col_name + '"'
//				col_val = ds_rules.GetItemString(i, c)
//				if col_val = "2" then
//					anchor_col_name_alias = anchor_col_name_alias + "a." + col_name + ","
//					anchor_col_name_no_alias = anchor_col_name_no_alias + col_name + ","
//					anchor_col_name_select = anchor_col_name_select +  col_name + ","
//				end if
//		end choose
//	next
//	anchor_col_name_alias = mid(anchor_col_name_alias,1,len(anchor_col_name_alias) - 1) + ") "
//	anchor_col_name_no_alias = mid(anchor_col_name_no_alias,1,len(anchor_col_name_no_alias) - 1) + ") "
//	anchor_col_name_select = mid(anchor_col_name_select,1,len(anchor_col_name_select) - 1) + " "
//	
//	
//	
//	//
//	//  1ST SELECT:
//	//
//	for c = 1 to num_cols
//		
//		col_name = upper(ds_rules.Describe("#" + string(c) + ".Name"))
//				
//		choose case col_name
//			case "ID", "RULE_ID", "STATUS", "NOTES","TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
//				//  Do nothing.
//			case else
//				//  Build the SQL.
//				col_val = ds_rules.GetItemString(i, c)
////  DML: MULTI-ANCHOR: COMMENTED OUT 1 LINE BELOW
////				if col_val = "2" then anchor_col_name = col_name
//				if col_val = "1" or col_val = "2" then
//					effmn_and_clause = effmn_and_clause + &
//						' and a."' + col_name + '" = b."' + col_name + '"'
//					if counter = 1 then
//						if i_effmn_cv = "YES" then
//							validate_sqls    = validate_sqls    + 'a."' + col_name + '"'
//							validate_sqls2   = validate_sqls2   + '"' + col_name + '"'
//						else
//							validate_sqls    = validate_sqls    + '"' + col_name + '"'
//						end if
//						
//						select_statement = select_statement + '"' + col_name + '"'
//						outer_sqls       = outer_sqls       + '"' + col_name + '"'
//					else
//						if i_effmn_cv = "YES" then
//							validate_sqls    = validate_sqls    + "||a." + '"' + col_name + '"'
//							validate_sqls2   = validate_sqls2   + "||" + '"' + col_name + '"'
//						else
//							validate_sqls    = validate_sqls    + "||" + '"' + col_name + '"'
//						end if
//						
//						select_statement = select_statement + "||" + '"' + col_name + '"'
//						outer_sqls       = outer_sqls       + "||" + '"' + col_name + '"'
//					end if
//					counter++
//				end if
//		end choose
//		
//	next  //  for c = 1 to num_cols ...
//	
//	
//	//
//	//  1ST FROM:
//	//
//	if i_effmn_cv = "YES" then
//		validate_sqls  = validate_sqls  + " from " + a_table_name + " a, cr_validation_control b "
//		validate_sqls2 = validate_sqls2 + " from " + a_table_name + " "
//	else
//		validate_sqls = validate_sqls + " from " + a_table_name + " "
//	end if
//	outer_sqls    = outer_sqls    + " in ( "
//	
//	
//	//
//	//  1ST WHERE CLAUSE:
//	//
//	if i_effmn_cv = "YES" then
//		
//		//  DML: MULTI-ANCHOR
//		validate_sqls = validate_sqls + " where " + anchor_col_name_alias + ' in (' + &
//			"select " + anchor_col_name_select + &
//			' from cr_validation_control where rule_id = ' + &
//			"'" + string(rule_id) + "') and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//		
//		validate_sqls = validate_sqls + " " + effmn_and_clause + &
//			" and b.rule_id = " + string(rule_id) + &
//			" and (a.month_number <  b.effective_month_number and b.status = 1 or " + &
//	      " a.month_number >= b.effective_month_number and b.status = 0)"
//		
//		validate_sqls2 = validate_sqls2 + " where " + anchor_col_name_no_alias + ' in (' + &
//			"select " + anchor_col_name_select + &
//			' from cr_validation_control where rule_id = ' + &
//			"'" + string(rule_id) + "') and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//		
////		validate_sqls = validate_sqls + " where a." + '"' + anchor_col_name + '" in (' + &
////			"select " + '"' + anchor_col_name + &
////			'" from cr_validation_control where rule_id = ' + &
////			"'" + string(rule_id) + "') and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
////		
////		validate_sqls = validate_sqls + " " + effmn_and_clause + &
////			" and b.rule_id = " + string(rule_id) + &
////			" and (a.month_number <  b.effective_month_number and b.status = 1 or " + &
////	      " a.month_number >= b.effective_month_number and b.status = 0)"
////		
////		validate_sqls2 = validate_sqls2 + " where " + '"' + anchor_col_name + '" in (' + &
////			"select " + '"' + anchor_col_name + &
////			'" from cr_validation_control where rule_id = ' + &
////			"'" + string(rule_id) + "') and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//		
//	else
//		//  DML: MULTI-ANCHOR
//		validate_sqls = validate_sqls + " where " + anchor_col_name_no_alias + ' in (' + &
//			"select " + anchor_col_name_select + &
//			' from cr_validation_control where rule_id = ' + &
//			"'" + string(rule_id) + "') and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
////		validate_sqls = validate_sqls + " where " + '"' + anchor_col_name + '" in (' + &
////			"select " + '"' + anchor_col_name + &
////			'" from cr_validation_control where rule_id = ' + &
////			"'" + string(rule_id) + "') and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//	end if
//		
//	
//	//
//	//  MINUS:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		validate_sqls2 = validate_sqls2 + " minus "
//	else
//		validate_sqls = validate_sqls + " minus "
//	end if
//	
//	
//	//
//	//  2ND SELECT:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		validate_sqls2 = validate_sqls2 + " select " + select_statement
//	else
//		validate_sqls = validate_sqls + " select " + select_statement
//	end if
//	
//	
//	//
//	//  2ND FROM:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		validate_sqls2 = validate_sqls2 + " from cr_validation_control "
//	else
//		validate_sqls = validate_sqls + " from cr_validation_control "
//	end if
//	
//	
//	//
//	//  2ND WHERE:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		//  DML: MULTI-ANCHOR
//		validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
//			' and ' + anchor_col_name_no_alias + ' in ' + &
//				'(select ' + anchor_col_name_select + ' from ' + a_table_name + ") "
////		validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
////			' and "' + anchor_col_name + '" in ' + &
////				'(select "' + anchor_col_name + '" from ' + a_table_name + ") "
//	else
//		//  DML: MULTI-ANCHOR
//		validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
//			' and ' + anchor_col_name_no_alias + ' in ' + &
//				'(select ' + anchor_col_name_select + ' from ' + a_table_name + ") "
////		validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
////			' and "' + anchor_col_name + '" in ' + &
////				'(select "' + anchor_col_name + '" from ' + a_table_name + ") "
//	end if
//	
//	
//	//  Must be before the command that adds the outer_sqls.
//	count_sqls  = "select count(*) from (" + validate_sqls  + ")"
//	count_sqls2 = "select count(*) from (" + validate_sqls2 + ")"
//	
//	
//	//
//	//  CLEANUP:
//	//
//	//  Since more than one column could be invalid and currently we cannot insert the
//	//  id more than once.
//	//
//	if use_intermediate_tbl = 'NO' then
//		validate_sqls  = outer_sqls + validate_sqls  + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
//		validate_sqls2 = outer_sqls + validate_sqls2 + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
//	else
//		//	remove the minus if using the intermediate table.  That's the whole purpose of the table.
//		validate_sqls  = outer_sqls + validate_sqls  + " ) ) "
//		validate_sqls2 = outer_sqls + validate_sqls2 + " ) ) "
//	end if
//	
//	
//	//
//	//  LOG THE VALIDATION ERRORS:
//	//
//	ds_count.Reset()
//	ds_count.SetSQLSelect(count_sqls)
//	ds_count.RETRIEVE()
//
//	ncount = ds_count.GetItemNumber(1, 1)
//	
//	//  If they are using the effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
//	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
//	//  would be needed to evaluate for missing combo values.
//	if ncount = 0 and i_effmn_cv = "YES" then
//		ds_count.Reset()
//		ds_count.SetSQLSelect(count_sqls2)
//		ds_count.RETRIEVE()
//	
//		ncount = ds_count.GetItemNumber(1, 1)
//	end if
//	
//	if ncount > 0 then
//		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
//			string(ncount) + " INVALID COMBINATIONS.")
//		kickouts = true
//	ELSE
//		GOTO NEXT_RULE
//	end if
//	
//	//
//	//  LOG THE VALIDATION ERRORS:
//	//
//	execute immediate :validate_sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
//		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
//		f_pp_msgs("  ")
//		rollback;
//		return -1
//	end if
//	
////  MOVED ABOVE
////	if sqlca.SQLNRows > 0 then
////		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
////			string(ncount) + " INVALID COMBINATIONS.")
////		kickouts = true
////	end if
//
//	//  If they are using effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
//	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
//	//  would be needed to evaluate for missing combo values.
//	if i_effmn_cv = "YES" then
//		
//		if sqlca.SQLNRows > 0 then
//			//  We got a validation kickout on validate_sqls ... no need to evaluate validate_sqls2.
//			//  Continue on to the next rule.
//			if use_intermediate_tbl = 'NO' then
//				continue
//			else
//				goto clear_intermediate_tbl
//			end if
//		else
//			//  The validate_sqls did not get a kickout ... evaluate validate_sqls2 next.
//			
//			//
//			//  INSERT THE INVALID IDS:
//			//
//			execute immediate :validate_sqls2;
//			
//			if sqlca.SQLCode < 0 then
//				f_pp_msgs("  ")
//				f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
//				f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//				f_pp_msgs("  ")
//				f_pp_msgs(validate_sqls2)
//				f_pp_msgs("  ")
//				rollback;
//				DESTROY ds_count
//				return -1
//			end if
//			
//		end if
//		
//	end if
//	
//	//	This insert needs to be in the loop.  We are trying to avoid a minus between the 
//	//		validate_sqls and cr_validations_invalid_ids2(or 2a) if use_intermediate_tbl = 'YES'.  
//	//		We are inserting into cr_validations_invalid_ids2a so without the minus it needs
//	//		to be cleared out in the loop.
//	clear_intermediate_tbl:
//	if use_intermediate_tbl = 'NO' then 
//	else
//		
//		//	Insert into 2 from 2a here.  2a must also be cleared out before continuing on in case another
//		//		function tries to insert into 2a.  This is needed since there isn't a minus anymore.
//		insert into cr_validations_invalid_ids2 (table_name, id, msg)
//		select table_name, id, msg from cr_validations_invalid_ids2a
//		minus
//		select table_name, id, msg from cr_validations_invalid_ids2;
//		
//		if sqlca.SQLCode < 0 then
//			f_pp_msgs("  ")
//			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Inserting into cr_validations_invalid_ids2a !")
//			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//			f_pp_msgs("  ")
//			rollback;
//			//	Don't return...always clear out cr_validations_invalid_ids2a.
////			return -1
//		end if
//		
//		sqlca.truncate_table('cr_validations_invalid_ids2a')
//		
//		if sqlca.SQLCode < 0 then
//			f_pp_msgs("  ")
//			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Truncating cr_validations_invalid_ids2a !")
//			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//			f_pp_msgs("  ")
//			rollback;
//			return -1
//		end if
//		
//	end if
//	
//	//	the truncate will fire a commit.  Go ahead and put one here to be explicit.
//	commit;
//	
//	next_rule:
//	
//next  //  for i = 1 to num_rules ...
//
//
//insert into cr_validations_invalid_ids (table_name, id) (
//	select distinct table_name, id from cr_validations_invalid_ids2
//	 where table_name = :a_table_name
//	minus
//	select table_name, id from cr_validations_invalid_ids);
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_VALIDATE_CUSTOM: inserting into cr_validations_invalid_ids !")
//	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	DESTROY ds_count
//	return -1
//end if
//
//
////  DMJ: 12/27/05: This update has an inherent problem.  It does not restrict by interface_batch_id or journal_id.
////                 Since we coded around this in the Delete Line button of m_cr_journals ...  and since it really
////                 should not affect feeder sources, I am going to leave this alone.  I feel it is too dangerous
////                 to change.  If it causes any client problems we can change it later.
////  JAK: 11/13/06: Pinnacle hit a deadlock error a few times when many users were hammering on journal entries
////						 Added an interface_batch_id restriction to the below SQL to try and alleviate this.
////update cr_validations_invalid_ids
////set valid_online = 0
////where id in (
////	select id from cr_validations_invalid_ids2
////	where table_name = :a_table_name)
////and table_name = :a_table_name;
//if lower(a_table_name) = "cr_journal_lines_stg" then
//	sqls = 'update cr_validations_invalid_ids ' + &
//		'set valid_online = 0 ' + &
//		'where id in ( ' + &
//		'select id from cr_validations_invalid_ids2 ' + &
//		"where table_name = '" + a_table_name + "' " + &
//		"and id in (select id from " + a_table_name + &
//		" where journal_id = '" + a_ifb_id + "' and " + i_add_where + " )) " + &
//		"and table_name = '" + a_table_name + "'"
//else
//	sqls = 'update cr_validations_invalid_ids ' + &
//		'set valid_online = 0 ' + &
//		'where id in ( ' + &
//		'select id from cr_validations_invalid_ids2 ' + &
//		"where table_name = '" + a_table_name + "' " + &
//		"and id in (select id from " + a_table_name + &
//		" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " )) " + &
//		"and table_name = '" + a_table_name + "'"
//end if
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: updating cr_validations_invalid_ids !")
//	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	DESTROY ds_count
//	return -1
//end if
//
//
//commit;
//
//
//if kickouts then
//	return -2
//else
//	return 1
//end if
//
//
end function

public function longlong uf_validate_combos_exclude (string a_table_name, string a_ifb_id, longlong a_rule_id);////*****************************************************************************************
////
////  User Object  :  uo_cr_validation
////
////  UO Function  :  uf_validate_combos_exclude
////
////  Return Codes :   1 = Success
////						  -1 = SQL Error
//// 					  -2 = Validation kickouts occurred
////
////	 Note			  : There is no reference to the cr_validation_exclusion table in this f(x).
////						 It is assumed that a rule will not include elements that should not be 
////						 validated.
////
////*****************************************************************************************
longlong i, num_rules, num_cols, c, counter, ncount, rtn, range_based
string  sqls, validate_sqls, col_name, col_val, rule, count_sqls
boolean kickouts

string exists_clause, select_list

select rule into :rule from cr_validation_rules where rule_id = :a_rule_id;
if isnull(rule) or rule = '' then rule = "Rule " + string(a_rule_id)

sqls = "select * from cr_validation_rules where rule_id = " + string(a_rule_id)
f_create_dynamic_ds(i_ds_rule, "grid", sqls, sqlca, true)
num_cols  = long(i_ds_rule.Object.DataWindow.Column.Count)

//  EXISTS CLAUSE
exists_clause = ''
for c = 1 to num_cols
	col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
	choose case col_name
		case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
			//  Do nothing.
		case else
			col_name = '"' + col_name + '"'
			col_val = i_ds_rule.GetItemString(1, c)
			
			if col_val = "1" or col_val = "2" then // validate or anchor
				exists_clause = exists_clause + "a." + col_name + " = b." + col_name + " and "
				select_list = select_list + "a." + col_name + "||'-'||"
			end if
	end choose
next
select_list = mid(select_list,1,len(select_list) - 7)

exists_clause = '(select 1 from cr_validation_combos b where ' + exists_clause + ' rule_id = ' + string(a_rule_id)

if i_effmn_cv = "YES" then 
	if i_effmn2_cv = "YES" then
		if i_effmn2_multi_rec_cv = "YES" then
			exists_clause = exists_clause + &
				" and a.month_number between b.effective_month_number (+) and b.effective_month_number2 (+)" + &
				" and b.status (+) = 1 and b.effective_month_number is null"
		else
			exists_clause = exists_clause + &
				" and (( a.month_number between b.effective_month_number and b.effective_month_number2 and b.status = 1) or " + &
				" (a.month_number not between b.effective_month_number and b.effective_month_number2 and b.status = 0))"
		end if
	else
		exists_clause = exists_clause + &
			" and ((a.month_number >=  b.effective_month_number and b.status = 1) or " + &
			" (a.month_number < b.effective_month_number and b.status = 0))"
	end if
end if
exists_clause = exists_clause + ')'

count_sqls = 'select ' + i_hint_str + ' count(*) from ' + a_table_name + ' a ' + &
	"where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and " + &
	"exists " + exists_clause
	
validate_sqls = "insert into cr_validations_invalid_ids2 (table_name, id, msg) " + &
	"(select " + i_hint_str + " '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
	' from ' + a_table_name + ' a ' + &
	"where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and " + &
	"exists " + exists_clause

select_list = 'select  ' + i_hint_str + ' distinct ' + select_list + ' from ' + a_table_name + ' a ' + &
	"where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and " + &
	"exists " + exists_clause

////// Update the SQL based on the system controls
if i_use_intermediate_tbl = 'YES' then 
	validate_sqls = f_replace_string(validate_sqls,'cr_validations_invalid_ids2','cr_validations_invalid_ids2a','all')
	validate_sqls  = validate_sqls + " ) "
else
	validate_sqls  = validate_sqls + " minus select table_name, id, msg from cr_validations_invalid_ids2 where table_name = '" + a_table_name + "' and msg = 'COMBOS (" + rule + ")') "
end if

//
//  LOG THE VALIDATION ERRORS:
//
i_ds_count.Reset()
count_sqls = uf_validate_combos_update_sql(count_sqls)
i_ds_count.SetSQLSelect(count_sqls)
i_ds_count.RETRIEVE()

ncount = i_ds_count.GetItemNumber(1, 1)

if ncount > 0 then
	f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
		string(ncount) + " INVALID COMBINATIONS ... ")
	kickouts = true
	
	if i_log_combo_kickouts = 'YES' then
		i_ds_msg_one_col.Reset()
		select_list = uf_validate_combos_update_sql(select_list)
		i_ds_msg_one_col.SetSQLSelect(select_list)
		i_ds_msg_one_col.RETRIEVE()
		
		for i = 1 to i_ds_msg_one_col.rowcount()
			f_pp_msgs(i_ds_msg_one_col.getitemstring(i,1))
		next
	end if
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	validate_sqls = uf_validate_combos_update_sql(validate_sqls)
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	if i_use_intermediate_tbl = 'NO' then 
	else
		
		//	Insert into 2 from 2a here.  2a must also be cleared out before continuing on in case another
		//		function tries to insert into 2a.  This is needed since there isn't a minus anymore.
		insert into cr_validations_invalid_ids2 (table_name, id, msg)
		select table_name, id, msg from cr_validations_invalid_ids2a
		minus
		select table_name, id, msg from cr_validations_invalid_ids2;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Inserting into cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			// Always clear!
			sqlca.truncate_table('cr_validations_invalid_ids2a')
			return -1
		end if
		
		sqlca.truncate_table('cr_validations_invalid_ids2a')
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Truncating cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	end if

end if

//	the truncate will fire a commit.  Go ahead and put one here to be explicit.
commit;

if kickouts then
	return -2
else
	return 1
end if



//
//
// OLD CODE REPLACED BY EXISTS METHOD ABOVE
//
//
//longlong i, num_rules, num_cols, c, counter, rule_id, ncount, rtn
//string  sqls, validate_sqls, col_name, col_val, anchor_col_name, rule, count_sqls, &
//		  select_statement, outer_sqls, effmn_and_clause, validate_sqls2, &
//		  count_sqls2
////  DML: MULTI-ANCHOR
//string  anchor_col_name_no_alias, anchor_col_name_alias, anchor_col_name_select
//boolean kickouts, called_from_kickouts = false
//
////
////  WE DO NOT NEED TO DO ANYTHING WITH REGARDS TO THE VALID_ONLINE LOGIC.  IT HAS ALREADY
////  BEEN PERFORMED IN UF_VALIDATE_COMBOS WHICH IS THE CALLER.
////
//
//
////
////  SETUP.
////
//datastore ds_count
//ds_count = CREATE datastore
//
//sqls = 'select count(*) from cr_elements where element_id = -1'
//// Create the shell of this datastore
//f_create_dynamic_ds(ds_count, "grid", sqls, sqlca, true)
//
//setnull(i_effmn_cv)
//select upper(trim(control_value)) into :i_effmn_cv from cr_system_control
// where lower(control_name) = 'validations: effective month number';
//if isnull(i_effmn_cv) or i_effmn_cv = "" then i_effmn_cv = 'NO'
//
//kickouts = false
//
//datastore ds_rules
//ds_rules = CREATE datastore
//
//sqls = "select * from cr_validation_rules where rule_id = " + string(a_rule_id)
//
//f_create_dynamic_ds(ds_rules, "grid", sqls, sqlca, true)
//
//ds_rules.SetSort("rule a")
//ds_rules.Sort()
//
//num_rules = ds_rules.RowCount()
//num_cols  = long(ds_rules.Object.DataWindow.Column.Count)
//
//
////
////  LOOP OVER EACH RULE.
////
//for i = 1 to num_rules
//	
//	effmn_and_clause = ""
//	counter          = 1
//	validate_sqls    = "select "
//	validate_sqls2   = "select distinct "
//	rule_id          = ds_rules.GetItemNumber(i,"rule_id")
//		
//	rule = ""
//	select rule into :rule from cr_validation_rules where rule_id = :rule_id;
//	if isnull(rule) then rule = ""
//	if i_use_intermediate_tbl = 'NO' then
//		outer_sqls       = &
//			"insert into cr_validations_invalid_ids2 " + &
//				"(table_name, id, msg) " + &
//					"( select '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
//					" from " + a_table_name + " where "
//	else
//		outer_sqls       = &
//			"insert into cr_validations_invalid_ids2a " + &
//				"(table_name, id, msg) " + &
//					"( select '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
//					" from " + a_table_name + " where "
//	end if
//	anchor_col_name  = ""
//	select_statement = ""
//	
//	//  DML: MULTI-ANCHOR
//	anchor_col_name_alias = "("
//	anchor_col_name_no_alias = "("
//	anchor_col_name_select = " "
//	for c = 1 to num_cols
//		col_name = upper(ds_rules.Describe("#" + string(c) + ".Name"))
//		choose case col_name
//			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
//				//  Do nothing.
//			case else
//				col_name = '"' + col_name + '"'
//				col_val = ds_rules.GetItemString(i, c)
//				if col_val = "2" then
//					anchor_col_name_alias = anchor_col_name_alias + "a." + col_name + ","
//					anchor_col_name_no_alias = anchor_col_name_no_alias + col_name + ","
//					anchor_col_name_select = anchor_col_name_select +  col_name + ","
//				end if
//		end choose
//	next
//	anchor_col_name_alias = mid(anchor_col_name_alias,1,len(anchor_col_name_alias) - 1) + ") "
//	anchor_col_name_no_alias = mid(anchor_col_name_no_alias,1,len(anchor_col_name_no_alias) - 1) + ") "
//	anchor_col_name_select = mid(anchor_col_name_select,1,len(anchor_col_name_select) - 1) + " "
//	
//	
//	//
//	//  1ST SELECT:
//	//
//	for c = 1 to num_cols
//		
//		col_name = upper(ds_rules.Describe("#" + string(c) + ".Name"))
//				
//		choose case col_name
//			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
//				//  Do nothing.
//			case else
//				//  Build the SQL.
//				col_val = ds_rules.GetItemString(i, c)
////  DML: MULTI-ANCHOR: COMMENTED OUT 1 LINE BELOW.
////				if col_val = "2" then anchor_col_name = col_name
//				if col_val = "1" or col_val = "2" then
//					effmn_and_clause = effmn_and_clause + &
//						' and a."' + col_name + '" = b."' + col_name + '"'
//					if counter = 1 then
//						if i_effmn_cv = "YES" then
//							validate_sqls    = validate_sqls    + 'a."' + col_name + '"'
//////////							validate_sqls2   = validate_sqls2   + '"' + col_name + '"'
//							validate_sqls2   = validate_sqls2   + 'nvl("' + col_name + '",' + "'  ')"
//						else
//							validate_sqls    = validate_sqls    + '"' + col_name + '"'
//						end if
//						select_statement = select_statement + '"' + col_name + '"'
//////////						outer_sqls       = outer_sqls       + '("' + col_name + '"'
//						outer_sqls       = outer_sqls       + '(nvl("' + col_name + '",' + "'  ')"
//					else
//						if i_effmn_cv = "YES" then
//							//
//							//  11/01/04:  PNM PERFORMANCE CHANGE:
//							//    Comment out 2 lines and add 2 lines with commas instead of ||.
//							//
////							validate_sqls    = validate_sqls    + "||a." + '"' + col_name + '"'
////							validate_sqls2   = validate_sqls2   + "||" + '"' + col_name + '"'
//							validate_sqls    = validate_sqls    + ",a." + '"' + col_name + '"'
//////////							validate_sqls2   = validate_sqls2   + "," + '"' + col_name + '"'
//							validate_sqls2   = validate_sqls2   + ",nvl(" + '"' + col_name + '",' + "'  ')"
//						else
//							//
//							//  11/01/04:  PNM PERFORMANCE CHANGE:
//							//    Comment out 1 line and add 1 line with commas instead of ||.
//							//
////							validate_sqls    = validate_sqls    + "||" + '"' + col_name + '"'
//							validate_sqls    = validate_sqls    + "," + '"' + col_name + '"'
//						end if
//						//
//						//  11/01/04:  PNM PERFORMANCE CHANGE:
//						//    Comment out 2 lines and add 2 lines with commas instead of ||.
//						//
////						select_statement = select_statement + "||" + '"' + col_name + '"'
////						outer_sqls       = outer_sqls       + "||" + '"' + col_name + '"'
//						select_statement = select_statement + "," + '"' + col_name + '"'
//////////						outer_sqls       = outer_sqls       + "," + '"' + col_name + '"'
//						outer_sqls       = outer_sqls       + "," + 'nvl("' + col_name + '",' + "'  ')"
//					end if
//					counter++
//				end if
//		end choose
//		
//		
//		//  11/01/04:  PNM PERFORMANCE CHANGE:
//		//    Added this code when I changed the || method to comma-separated method for 
//		//    PNM performance.
//		if c = num_cols then
//			outer_sqls = outer_sqls + ")"
//		end if
//		
//		
//	next  //  for c = 1 to num_cols ...
//	
//	
//	//
//	//  1ST FROM:
//	//
//	if i_effmn_cv = "YES" then
//		validate_sqls  = validate_sqls  + " from " + a_table_name + " a, cr_validation_combos b "
//		validate_sqls2 = validate_sqls2 + " from " + a_table_name + " "
//	else
//		validate_sqls = validate_sqls + " from " + a_table_name + " "
//	end if
//	outer_sqls    = outer_sqls    + " in ( "
//	
//	
//	//
//	//  1ST WHERE CLAUSE:
//	//
//	if i_effmn_cv = "YES" then
//		
//		if lower(a_table_name) = "cr_journal_lines_stg" then
//			//  DML: MULTI-ANCHOR:
//			validate_sqls = validate_sqls + " where " + anchor_col_name_alias + ' in (' + &
//				"select " + anchor_col_name_select + &
//				' from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and journal_id = " + a_ifb_id + " and " + i_add_where + " "
////			validate_sqls = validate_sqls + " where a." + '"' + anchor_col_name + '" in (' + &
////				"select " + '"' + anchor_col_name + &
////				'" from cr_validation_combos where rule_id = ' + &
////				"" + string(rule_id) + ") and journal_id = " + a_ifb_id + " and " + i_add_where + " "
//		else
//			//  DML: MULTI-ANCHOR:
//			validate_sqls = validate_sqls + " where " + anchor_col_name_alias + ' in (' + &
//				"select " + anchor_col_name_select + &
//				' from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
////			validate_sqls = validate_sqls + " where a." + '"' + anchor_col_name + '" in (' + &
////				"select " + '"' + anchor_col_name + &
////				'" from cr_validation_combos where rule_id = ' + &
////				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//		end if
//				
//		validate_sqls = validate_sqls + " " + effmn_and_clause + &
//			" and b.rule_id = " + string(rule_id) + &
//			" and (a.month_number >=  b.effective_month_number and b.status = 1 or " + &
//	      " a.month_number < b.effective_month_number and b.status = 0)"
//		
//		if lower(a_table_name) = "cr_journal_lines_stg" then
//			//  DML: MULTI-ANCHOR:
//			validate_sqls2 = validate_sqls2 + " where " + anchor_col_name_no_alias + ' in (' + &
//				"select " + '"' + anchor_col_name + &
//				' from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and journal_id = " + a_ifb_id + " and " + i_add_where + " "
////			validate_sqls2 = validate_sqls2 + " where " + '"' + anchor_col_name + '" in (' + &
////				"select " + '"' + anchor_col_name + &
////				'" from cr_validation_combos where rule_id = ' + &
////				"" + string(rule_id) + ") and journal_id = " + a_ifb_id + " and " + i_add_where + " "
//		else
//			//  DML: MULTI-ANCHOR:
//			validate_sqls2 = validate_sqls2 + " where " + anchor_col_name_no_alias + ' in (' + &
//				"select " + anchor_col_name_select + &
//				' from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
////			validate_sqls2 = validate_sqls2 + " where " + '"' + anchor_col_name + '" in (' + &
////				"select " + '"' + anchor_col_name + &
////				'" from cr_validation_combos where rule_id = ' + &
////				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//		end if
//		
//	else
//		if lower(a_table_name) = "cr_journal_lines_stg" then
//			//  DML: MULTI-ANCHOR:
//			validate_sqls = validate_sqls + " where " + anchor_col_name_no_alias + ' in (' + &
//				"select " + anchor_col_name_select + &
//				' from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and journal_id = " + a_ifb_id + " and " + i_add_where + " "
////			validate_sqls = validate_sqls + " where " + '"' + anchor_col_name + '" in (' + &
////				"select " + '"' + anchor_col_name + &
////				'" from cr_validation_combos where rule_id = ' + &
////				"" + string(rule_id) + ") and journal_id = " + a_ifb_id + " and " + i_add_where + " "
//		else
//			//  DML: MULTI-ANCHOR:
//			validate_sqls = validate_sqls + " where " + anchor_col_name_no_alias + ' in (' + &
//				"select " + anchor_col_name_select + &
//				' from cr_validation_combos where rule_id = ' + &
//				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
////			validate_sqls = validate_sqls + " where " + '"' + anchor_col_name + '" in (' + &
////				"select " + '"' + anchor_col_name + &
////				'" from cr_validation_combos where rule_id = ' + &
////				"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
//		end if
//	end if
//	
//	
//	//
//
//	//  MINUS:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		validate_sqls2 = validate_sqls2 + " minus "
//	else
//		validate_sqls = validate_sqls + " minus "
//	end if
//	
//	
//	//
//	//  2ND SELECT:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		validate_sqls2 = validate_sqls2 + " select distinct " + select_statement
//	else
//		validate_sqls = validate_sqls + " select " + select_statement
//	end if
//	
//	
//	//
//	//  2ND FROM:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		validate_sqls2 = validate_sqls2 + " from cr_validation_combos "
//	else
//		validate_sqls = validate_sqls + " from cr_validation_combos "
//	end if
//	
//	
//	//
//	//  2ND WHERE:
//	//
//	if i_effmn_cv = "YES" then
//		//  Do nothing to validate_sqls.
//		if lower(a_table_name) = "cr_journal_lines_stg" then
//			//  DML: MULTI-ANCHOR:
//			validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
//				' and ' + anchor_col_name_no_alias + ' in ' + &
//					'(select ' + anchor_col_name_select + ' from ' + a_table_name + &
//					 " where journal_id = " + a_ifb_id + " and " + i_add_where + " ) "
////			validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
////				' and "' + anchor_col_name + '" in ' + &
////					'(select "' + anchor_col_name + '" from ' + a_table_name + &
////					 " where journal_id = " + a_ifb_id + " and " + i_add_where + " ) "
//		else
//			//  DML: MULTI-ANCHOR:
//			validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
//				' and ' + anchor_col_name_no_alias + ' in ' + &
//					'(select ' + anchor_col_name_select + ' from ' + a_table_name + &
//					 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
////			validate_sqls2 = validate_sqls2 + " where rule_id = " + string(rule_id) + " " + &
////				' and "' + anchor_col_name + '" in ' + &
////					'(select "' + anchor_col_name + '" from ' + a_table_name + &
////					 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
//		end if
//	else
//		if lower(a_table_name) = "cr_journal_lines_stg" then
//			//  DML: MULTI-ANCHOR:
//			validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
//				' and ' + anchor_col_name_no_alias + ' in ' + &
//					'(select ' + anchor_col_name_select + ' from ' + a_table_name + &
//					 " where journal_id = " + a_ifb_id + " and " + i_add_where + " ) "
////			validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
////				' and "' + anchor_col_name + '" in ' + &
////					'(select "' + anchor_col_name + '" from ' + a_table_name + &
////					 " where journal_id = " + a_ifb_id + " and " + i_add_where + " ) "
//		else
//			//  DML: MULTI-ANCHOR:
//			validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
//				' and ' + anchor_col_name_no_alias + ' in ' + &
//					'(select ' + anchor_col_name_select + ' from ' + a_table_name + &
//					 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
////			validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
////				' and "' + anchor_col_name + '" in ' + &
////					'(select "' + anchor_col_name + '" from ' + a_table_name + &
////					 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
//		end if
//	end if
//	
//	
//	//  Must be before the command that adds the outer_sqls.
//	count_sqls  = "select count(*) from (" + validate_sqls  + ")"
//	count_sqls2 = "select count(*) from (" + validate_sqls2 + ")"
//	
//	
//	//
//	//  CLEANUP:
//	//
//	//  Since more than one column could be invalid and currently we cannot insert the
//	//  id more than once.
//	//
//	if i_use_intermediate_tbl = 'NO' then
//		validate_sqls  = outer_sqls + validate_sqls  + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
//		validate_sqls2 = outer_sqls + validate_sqls2 + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
//	else
//		//	remove the minus if using the intermediate table.  That's the whole purpose of the table.
//		validate_sqls  = outer_sqls + validate_sqls  + " ) ) "
//		validate_sqls2 = outer_sqls + validate_sqls2 + " ) ) "
//	end if
//	
//	
//	//
//	//  LOG THE VALIDATION ERRORS:
//	//
//	ds_count.Reset()
//	ds_count.SetSQLSelect(count_sqls)
//	ds_count.RETRIEVE()
//
//	ncount = ds_count.GetItemNumber(1, 1)
//		
//	//
//	//  WE SHOULD NOT NEED TO DO THIS FOR "NOT" VALIDATIONS.  A VALUE IS EITHER THERE OR IT IS
//	//  NOT.  FURTHERMORE, THE MINUS TECHNIQUE WOULD NOT SUPPORT THE EFFECTIVE MN.
//	//
////	//  If they are using the effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
////	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
////	//  would be needed to evaluate for missing combo values.
////	if ncount = 0 and i_effmn_cv = "YES" then
////		ds_count.Reset()
////		ds_count.SetSQLSelect(count_sqls2)
////		ds_count.RETRIEVE()
////	
////		ncount = ds_count.GetItemNumber(1, 1)
////	end if
//	
//	if ncount > 0 then
//		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
//			string(ncount) + " INVALID COMBINATIONS.")
//		kickouts = true
//	ELSE
//		GOTO NEXT_RULE
//	end if
//	
//	//
//	//  LOG THE VALIDATION ERRORS:
//	//
//	
//	execute immediate :validate_sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
//		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
//		f_pp_msgs("  ")
//		rollback;
//		return -1
//	end if
//	
////  MOVED ABOVE
////	if sqlca.SQLNRows > 0 then
////		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
////			string(ncount) + " INVALID COMBINATIONS.")
////		kickouts = true
////	end if
//
//	//  If they are using effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
//	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
//	//  would be needed to evaluate for missing combo values.
//	if i_effmn_cv = "YES" then
//		
//		if sqlca.SQLNRows > 0 then
//			//  We got a validation kickout on validate_sqls ... no need to evaluate validate_sqls2.
//			//  Continue on to the next rule.
//			if i_use_intermediate_tbl = 'NO' then
//				continue
//			else
//				goto clear_intermediate_tbl
//			end if
//		else
//			
//			//  DO NOT EXECUTE THE "2" SQL FOR "NOT" VALUE VALIDATIONS.
//			
////			//  The validate_sqls did not get a kickout ... evaluate validate_sqls2 next.
////			
////			//
////			//  INSERT THE INVALID IDS:
////			//
////			execute immediate :validate_sqls2;
////			
////			if sqlca.SQLCode < 0 then
////				f_pp_msgs("  ")
////				f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
////				f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
////				f_pp_msgs("  ")
////				f_pp_msgs(validate_sqls2)
////				f_pp_msgs("  ")
////				rollback;
////				DESTROY ds_count
////				return -1
////			end if
//			
//		end if
//		
//	end if
//	
//	//	This insert needs to be in the loop.  We are trying to avoid a minus between the 
//	//		validate_sqls and cr_validations_invalid_ids2(or 2a) if use_intermediate_tbl = 'YES'.  
//	//		We are inserting into cr_validations_invalid_ids2a so without the minus it needs
//	//		to be cleared out in the loop.
//	clear_intermediate_tbl:
//	if i_use_intermediate_tbl = 'NO' then 
//	else
//		
//		//	Insert into 2 from 2a here.  2a must also be cleared out before continuing on in case another
//		//		function tries to insert into 2a.  This is needed since there isn't a minus anymore.
//		insert into cr_validations_invalid_ids2 (table_name, id, msg)
//		select table_name, id, msg from cr_validations_invalid_ids2a
//		minus
//		select table_name, id, msg from cr_validations_invalid_ids2;
//		
//		if sqlca.SQLCode < 0 then
//			f_pp_msgs("  ")
//			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Inserting into cr_validations_invalid_ids2a !")
//			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//			f_pp_msgs("  ")
//			rollback;
//			//	Don't return...always clear out cr_validations_invalid_ids2a.
////			return -1
//		end if
//		
//		sqlca.truncate_table('cr_validations_invalid_ids2a')
//		
//		if sqlca.SQLCode < 0 then
//			f_pp_msgs("  ")
//			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Truncating cr_validations_invalid_ids2a !")
//			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//			f_pp_msgs("  ")
//			rollback;
//			return -1
//		end if
//		
//	end if
//
//	//	the truncate will fire a commit.  Go ahead and put one here to be explicit.
//	commit;
//	
//	next_rule:
//	
//next  //  for i = 1 to num_rules ...
//
//
//insert into cr_validations_invalid_ids (table_name, id) (
//	select distinct table_name, id from cr_validations_invalid_ids2
//	 where table_name = :a_table_name
//	minus
//	select table_name, id from cr_validations_invalid_ids);
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids !")
//	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	DESTROY ds_count
//	return -1
//end if
//
//
////  WE ARE NOT INSERTING INTO CR_VALIDATION_INVALID_IDS HERE.  THAT WILL HAPPEN IN
////  UF_VALIDATE_COMBOS AFTER ALL RULES ARE EVALUATED.
//
//
////  WE CANNOT UPDATE THE VALID_ONLINE FIELD HERE.  THAT MUST HAPPEN IN UF_VALIDATE_COMBOS
////  AFTER ALL RULES ARE EVALUATED.
//
//
//commit;
//
//
//if kickouts then
//	return -2
//else
//	return 1
//end if
//
//
end function

public function longlong uf_validate_combos_required (string a_table_name, string a_ifb_id, longlong a_rule_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_combos_required
//
//  Return Codes :   1 = Success
//						  -1 = SQL Error
// 					  -2 = Validation kickouts occurred
//
//	 Note			  : There is no reference to the cr_validation_exclusion table in this f(x).
//						 It is assumed that a rule will not include elements that should not be 
//						 validated.
//
//*****************************************************************************************
longlong i, num_rules, num_cols, c, counter, rule_id, ncount, rtn
string  sqls, validate_sqls, col_name, col_val, anchor_col_name, rule, count_sqls, &
		  select_statement, outer_sqls, effmn_and_clause
//  DML: MULTI-ANCHOR
string  anchor_col_name_no_alias, anchor_col_name_alias, anchor_col_name_select, dflt_val, prepend_validate_sqls
boolean kickouts, called_from_kickouts = false, FIRST_TIME = TRUE

//
//  WE DO NOT NEED TO DO ANYTHING WITH REGARDS TO THE VALID_ONLINE LOGIC.  IT HAS ALREADY
//  BEEN PERFORMED IN UF_VALIDATE_COMBOS WHICH IS THE CALLER.
//


//
//  SETUP.
//

kickouts = false

sqls = "select * from cr_validation_rules where rule_id = " + string(a_rule_id)

i_ds_rule.Reset()
i_ds_rule.SetSQLSelect(sqls)
i_ds_rule.RETRIEVE()

num_rules = i_ds_rule.RowCount()
num_cols  = long(i_ds_rule.Object.DataWindow.Column.Count)


//
//  LOOP OVER EACH RULE.
//
for i = 1 to num_rules
	
	effmn_and_clause = ""
	counter          = 1
	// need to select specifc column names, so we will prepend select statement later
	validate_sqls    = "select " + i_hint_str
	rule_id          = i_ds_rule.GetItemNumber(i,"rule_id")
	
	rule = ""
	select rule into :rule from cr_validation_rules where rule_id = :rule_id;
	if isnull(rule) then rule = ""

	if i_use_intermediate_tbl = 'NO' then
		outer_sqls       = &
			"insert into cr_validations_invalid_ids2 " + &
				"(table_name, id, msg) " + &
					"( select " + i_hint_str + "'" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
					" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and "
	else
		outer_sqls       = &
			"insert into cr_validations_invalid_ids2a " + &
				"(table_name, id, msg) " + &
					"( select " + i_hint_str + "'" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
					" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and "
	end if
	anchor_col_name  = ""
	select_statement = ""
	
	
	//  DML: MULTI-ANCHOR
	anchor_col_name_alias = "("
	anchor_col_name_no_alias = "("
	anchor_col_name_select = " "
	for c = 1 to num_cols
		col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
		choose case col_name
			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
				//  Do nothing.
			case else
				col_name = '"' + col_name + '"'
				col_val = i_ds_rule.GetItemString(i, c)
				if col_val = "2" then
					anchor_col_name_alias = anchor_col_name_alias + "a." + col_name + ","
					anchor_col_name_no_alias = anchor_col_name_no_alias + col_name + ","
					anchor_col_name_select = anchor_col_name_select +  col_name + ","
					effmn_and_clause = effmn_and_clause + &
							' and a.' + col_name + ' = b.' + col_name 
				end if
		end choose
	next
	anchor_col_name_alias = mid(anchor_col_name_alias,1,len(anchor_col_name_alias) - 1) + ") "
	anchor_col_name_no_alias = mid(anchor_col_name_no_alias,1,len(anchor_col_name_no_alias) - 1) + ") "
	anchor_col_name_select = mid(anchor_col_name_select,1,len(anchor_col_name_select) - 1) + " "
	
	//
	//  1ST SELECT:
	//
	for c = 1 to num_cols
		
		col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
				
		choose case col_name
			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
				//  Do nothing.
			case else
				
				//  Get the default value that is invalid (if needed).
				if i_cv_req_obey_default = "YES" then
					setnull(dflt_val)
dflt_val = g_cr.uf_get_element_default_value(g_cr.uf_get_element_id("f_replace_string(f_replace_string(f_replace_string(description,' ','_','all'),'-',' ','all'),'/',' ','all')",col_name)) //Autogenerated sql replace
					if isnull(dflt_val) then dflt_val = ' ' // must assume single-space is the dummy value
				end if
				
				//  Build the SQL.
				col_val = i_ds_rule.GetItemString(i, c)
				if col_val = "1" or col_val = "2" then


					//  FOR REQUIRED VALUE VALIDATIONS, ADD THE CHECK TO SEE IF THERE IS A BLANK VALUE.
//					if effmn_and_clause = "" then
					//  JAK:  20090209:  Only need col_val = 1 in this block...
					if col_val = "1" then
						IF first_time then
							//  First time through ... add the "(".
							//  Default value logic written this way to avoid bugs w/ existing clients.
							if i_cv_req_obey_default = "YES" then
								effmn_and_clause = effmn_and_clause + &
									' and (a."' + col_name + '" = ' + "'" + dflt_val + "'"
							else
								effmn_and_clause = effmn_and_clause + &
									' and (a."' + col_name + '" = ' + "' '"
							end if
						else
							//  Not the first time through ... use an "or".
							//  Default value logic written this way to avoid bugs w/ existing clients.
							if i_cv_req_obey_default = "YES" then
								effmn_and_clause = effmn_and_clause + &
									' or a."' + col_name + '" = ' + "'" + dflt_val + "'"
							else
								effmn_and_clause = effmn_and_clause + &
									' or a."' + col_name + '" = ' + "' '"
							end if
						end if
						
						first_time = false
					end if

					if counter = 1 then
						if i_effmn_cv = "YES" then
							validate_sqls    = validate_sqls    + 'a."' + col_name + '"'
						else
							validate_sqls    = validate_sqls    + '"' + col_name + '"'
						end if
						select_statement = select_statement + '"' + col_name + '"'

						outer_sqls       = outer_sqls       + '(nvl("' + col_name + '",' + "'  ')"
					else
						if i_effmn_cv = "YES" then
							
							validate_sqls    = validate_sqls    + ",a." + '"' + col_name + '"'
						else

							validate_sqls    = validate_sqls    + "," + '"' + col_name + '"'
						end if

						select_statement = select_statement + "," + '"' + col_name + '"'

						outer_sqls       = outer_sqls       + "," + 'nvl("' + col_name + '",' + "'  ')"
					end if
					counter++
				end if
		end choose
		
		
		//  11/01/04:  PNM PERFORMANCE CHANGE:
		//    Added this code when I changed the || method to comma-separated method for 
		//    PNM performance.
		if c = num_cols then
			outer_sqls = outer_sqls + ")"
		end if
		
		
	next  //  for c = 1 to num_cols ...
	
	
	//  FOR REQUIRED VALUE VALIDATIONS:  Need to close off the parens on effmn_and_clause.
	effmn_and_clause = effmn_and_clause + ") "
	
	
	//
	//  1ST FROM:
	//
	if i_effmn_cv = "YES" then
		validate_sqls  = validate_sqls  + " from " + a_table_name + " a, cr_validation_combos b "
	else
		validate_sqls = validate_sqls + " from " + a_table_name + " "
	end if
	outer_sqls    = outer_sqls    + " in ( "
	
	
	//
	//  1ST WHERE CLAUSE:
	//
	if i_effmn_cv = "YES" then
		
		//  DML: MULTI-ANCHOR:
		validate_sqls = validate_sqls + " where " + anchor_col_name_alias + ' in (' + &
			"select " + anchor_col_name_select + &
			' from cr_validation_combos where rule_id = ' + &
			"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "

		
		//  FOR REQUIRED VALUE VALIDATIONS THE SIGNS ARE REVERSED FROM THE "INCLUDE" VALIDATIONS.
		validate_sqls = validate_sqls + " " + effmn_and_clause + &
			" and b.rule_id = " + string(rule_id) + &
			" and (a.month_number >= b.effective_month_number and b.status = 1 or " + &
	      " a.month_number < b.effective_month_number and b.status = 0)"		
	else
		//  DML: MULTI-ANCHOR:
		validate_sqls = validate_sqls + " where " + anchor_col_name_no_alias + ' in (' + &
			"select " + anchor_col_name_select + &
			' from cr_validation_combos where rule_id = ' + &
			"" + string(rule_id) + ") and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "

	end if
	
	
	//

	//  Left join (will be exclusive b/c of where null below):
	//
	if i_effmn_cv = "YES" then
		//  Do nothing to validate_sqls.
	else
		validate_sqls = validate_sqls + ") vals left join"
	end if
	
	
	//
	//  2ND SELECT:
	//
	if i_effmn_cv = "YES" then
		//  Do nothing to validate_sqls.
	else
		validate_sqls = validate_sqls + "( select " + select_statement
	end if
	
	
	//
	//  2ND FROM:
	//
	if i_effmn_cv = "YES" then
		//  Do nothing to validate_sqls.
	else
		validate_sqls = validate_sqls + " from cr_validation_combos "
	end if
	
	
	//
	//  2ND WHERE:
	//
	if i_effmn_cv = "YES" then
		//  Do nothing to validate_sqls.
	else
		//  DML: MULTI-ANCHOR:
		validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
			' and ' + anchor_col_name_no_alias + ' in ' + &
				'(select ' + anchor_col_name_select + ' from ' + a_table_name + &
				 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
//			validate_sqls = validate_sqls + " where rule_id = " + string(rule_id) + " " + &
//				' and "' + anchor_col_name + '" in ' + &
//					'(select "' + anchor_col_name + '" from ' + a_table_name + &
//					 " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " ) "
	
	
	//Build the join on clase
	validate_sqls = validate_sqls + ") valid on "
	
	first_time = true
	
	for c = 1 to num_cols
		
		col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
		
		col_val = i_ds_rule.GetItemString(i, c)
		
		if col_val <> "null" then
				
		choose case col_name
			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED", "COMBO_TYPE", "RULE"
				//  Do nothing.
			case else
				if first_time then 
					first_time = false
				else
					validate_sqls = validate_sqls + " AND "
				end if
				validate_sqls = validate_sqls + " vals." + col_name + " = decode(valid." + col_name + ", '*', vals." + col_name +", valid." + col_name + ") "
		end choose
	end if
	next
	
	//Exclude vals that are in both tables
	
	first_time = true
	
	for c = 1 to num_cols
		
		col_val = i_ds_rule.GetItemString(i, c)
		
		if col_val <> "null" and col_val <> "" then
				
			col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
				
			choose case col_name
				case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED", "COMBO_TYPE", "RULE"
				//  Do nothing.
				case else
					if first_time then
						validate_sqls = validate_sqls + " where "
						first_time = false
					else
						validate_sqls = validate_sqls + " or "
					end if
					//assuming single space is dummy value
					validate_sqls = validate_sqls + "(vals." + col_name +" IS NULL or vals." + col_name + " = ' ' or valid." + col_name + " IS NULL ) "
			end choose
		end if
	next
	
	//select from the newly joined table
	prepend_validate_sqls = " select "
	
	//prepend select statement columns to validate_sqls
	first_time = true
	for c = num_cols to 1 Step -1
		
		col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
		
		col_val = i_ds_rule.GetItemString(i, c)
		
		if col_val <> "null" then
				
		choose case col_name
			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED", "COMBO_TYPE", "RULE"
				//  Do nothing.
			case else
				if first_time then 
					first_time = false
				else 
					prepend_validate_sqls = prepend_validate_sqls + ", "
				end if
				prepend_validate_sqls = prepend_validate_sqls + "vals." + col_name + " "
		end choose
	end if
	next
	
	//build outer select statement. above validate_sqls has the select statment for pulling values from the first table of the join. Need to select from the joined table
	
	validate_sqls = prepend_validate_sqls + " from (" + validate_sqls
	
end if
	
	//  Must be before the command that adds the outer_sqls.
	count_sqls  = "select count(*) from (" + validate_sqls  + ")"
	
	
	//
	//  CLEANUP:
	//
	//  Since more than one column could be invalid and currently we cannot insert the
	//  id more than once.
	//
	
	if i_use_intermediate_tbl = 'NO' then
		validate_sqls  = outer_sqls + validate_sqls  + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
	else
		//	remove the minus if using the intermediate table.  That's the whole purpose of the table.
		validate_sqls  = outer_sqls + validate_sqls  + " ) ) "
	end if
	
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	i_ds_count.Reset()
	count_sqls = uf_validate_combos_update_sql(count_sqls)
	i_ds_count.SetSQLSelect(count_sqls)
	i_ds_count.RETRIEVE()

	ncount = i_ds_count.GetItemNumber(1, 1)
	
	
	if ncount > 0 then
		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
			string(ncount) + " INVALID COMBINATIONS.")
		kickouts = true
	ELSE
		GOTO NEXT_RULE
	end if
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	
	validate_sqls = uf_validate_combos_update_sql(validate_sqls)
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	

	//  If they are using effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
	//  would be needed to evaluate for missing combo values.
	if i_effmn_cv = "YES" then
		
		if sqlca.SQLNRows > 0 then
			//  We got a validation kickout on validate_sqls ... no need to evaluate validate_sqls2.
			//  Continue on to the next rule.
			if i_use_intermediate_tbl = 'NO' then
				continue
			else
				goto clear_intermediate_tbl
			end if
		else
			
			//  DO NOT EXECUTE THE "2" SQL FOR REQUIRED VALUE VALIDATIONS.
			
			
		end if
		
	end if
	
	//	This insert needs to be in the loop.  We are trying to avoid a minus between the 
	//		validate_sqls and cr_validations_invalid_ids2(or 2a) if use_intermediate_tbl = 'YES'.  
	//		We are inserting into cr_validations_invalid_ids2a so without the minus it needs
	//		to be cleared out in the loop.
	clear_intermediate_tbl:
	if i_use_intermediate_tbl = 'NO' then 
	else
		
		//	Insert into 2 from 2a here.  2a must also be cleared out before continuing on in case another
		//		function tries to insert into 2a.  This is needed since there isn't a minus anymore.
		insert into cr_validations_invalid_ids2 (table_name, id, msg)
		select table_name, id, msg from cr_validations_invalid_ids2a
		minus
		select table_name, id, msg from cr_validations_invalid_ids2;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Inserting into cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			//	Don't return...always clear out cr_validations_invalid_ids2a.
//			return -1
		end if
		
		sqlca.truncate_table('cr_validations_invalid_ids2a')
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Truncating cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	end if

	//	the truncate will fire a commit.  Go ahead and put one here to be explicit.
	commit;
	
	next_rule:
	
next  //  for i = 1 to num_rules ...


//  WE ARE NOT INSERTING INTO CR_VALIDATION_INVALID_IDS HERE.  THAT WILL HAPPEN IN
//  UF_VALIDATE_COMBOS AFTER ALL RULES ARE EVALUATED.


//  WE CANNOT UPDATE THE VALID_ONLINE FIELD HERE.  THAT MUST HAPPEN IN UF_VALIDATE_COMBOS
//  AFTER ALL RULES ARE EVALUATED.


if kickouts then
	return -2
else
	return 1
end if


end function

public function longlong uf_validate_me (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_me
//
//  Return Codes :   1 = Success
//						  -1 = SQL Error
// 					  -2 = Validation kickouts occurred
//
//*****************************************************************************************

longlong rtn

rtn = uf_validate_me_combined(a_table_name, a_ifb_id, false)

return rtn
end function

public function longlong uf_validate_combos_range_based (string a_table_name, string a_ifb_id, longlong a_rule_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_combos_range_based
//
//  Return Codes :   1 = Success
//						  -1 = SQL Error
// 					  -2 = Validation kickouts occurred
//
//	 Note			  : There is no reference to the cr_validation_exclusion table in this f(x).
//						 It is assumed that a rule will not include elements that should not be 
//						 validated.
//
//*****************************************************************************************
longlong i, num_rules, num_cols, c, counter, rule_id, ncount, rtn, range_based, &
		  num_control_rows, num_control_cols, j, k, a, v, comma_pos, start_pos, &
		  counter2, the_last_comma_pos, element_order, structure_id, effmn, effmn2, status
string  sqls, validate_sqls, col_name, col_val, anchor_col_name, rule, count_sqls, &
		  select_statement, outer_sqls, effmn_and_clause, validate_sqls2, &
		  count_sqls2, combo_type
//  DML: MULTI-ANCHOR
string  anchor_col_name_no_alias, anchor_col_name_alias, anchor_col_name_select, &
		  anchor_col_list[], validate_col_list[], anchor, anchor_value, range_sqls1, &
		  range1, range_start, range_end, range2, range_sqls2, validate_col, validate_col_value
boolean kickouts, called_from_kickouts = false, skip_this_control_record

// DMJ: 02/04/10: To Avoid Mem Leaks
//datastore ds_count
//ds_count = CREATE datastore

// DMJ: 02/04/10: To Avoid Mem Leaks
//sqls = 'select count(*) from cr_elements where element_id = -1'
//// Create the shell of this datastore
//f_create_dynamic_ds(i_ds_count, "grid", sqls, sqlca, true)
//
//setnull(effmn_cv)
//select upper(trim(control_value)) into :effmn_cv from cr_system_control
// where lower(control_name) = 'validations: effective month number';
//if isnull(effmn_cv) or effmn_cv = "" then effmn_cv = 'NO'
//
//setnull(effmn2_cv)
//select upper(trim(control_value)) into :effmn2_cv from cr_system_control
// where lower(control_name) = 'validation combos: eff mn 2';
//if isnull(effmn2_cv) or effmn2_cv = "" then effmn2_cv = 'NO'

kickouts = false

// DMJ: 02/04/10: To Avoid Mem Leaks
//datastore ds_rules
//ds_rules = CREATE datastore

sqls = "select * from cr_validation_rules where rule_id = " + string(a_rule_id)
// DMJ: 02/04/10: To Avoid Mem Leaks
//f_create_dynamic_ds(ds_rules, "grid", sqls, sqlca, true)
i_ds_rule.Reset()
i_ds_rule.SetSQLSelect(sqls)
i_ds_rule.RETRIEVE()

num_rules = i_ds_rule.RowCount()
num_cols  = long(i_ds_rule.Object.DataWindow.Column.Count)

// DMJ: 02/04/10: To Avoid Mem Leaks
//datastore ds_control
//ds_control = CREATE datastore

sqls = "select * from cr_validation_control where rule_id = " + string(a_rule_id)
// DMJ: 02/04/10: To Avoid Mem Leaks
//f_create_dynamic_ds(ds_control, "grid", sqls, sqlca, true)
i_ds_control.Reset()
i_ds_control.SetSQLSelect(sqls)
i_ds_control.RETRIEVE()

num_control_rows = i_ds_control.RowCount()
num_control_cols = long(i_ds_control.Object.DataWindow.Column.Count)

for i = 1 to num_rules
	
	effmn_and_clause = ""
	counter = 1
	
	rule = ""
	select rule into :rule from cr_validation_rules where rule_id = :a_rule_id;
	if isnull(rule) then rule = ""
	//	DML: 080406: In addition to the intermediate table check, also add an interface_batch_id or
	//		journal_id restriction to the outer sqls.  The way it is now, any rows with the same
	//		values in the combo fields are inserted.  The inner sql has a journal_id/interface_batch_id
	//		restriction giving us a set of rows.  The outer sqls pulls everything from a_table_name
	//		matching "in" the inner sqls.  An additional restriction is needed to do this within 
	//		the journal or batch and not for all values in a_table_name.
	if i_use_intermediate_tbl = 'NO' then
		if lower(a_table_name) = "cr_journal_lines_stg" then
			outer_sqls       = &
				"insert into cr_validations_invalid_ids2 " + &
					"(table_name, id, msg) " + &
						"( select " + i_hint_str + " '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
						" from " + a_table_name + " where journal_id = " + a_ifb_id + " and " + i_add_where + "  and "
		else
			outer_sqls       = &
				"insert into cr_validations_invalid_ids2 " + &
					"(table_name, id, msg) " + &
						"( select " + i_hint_str + "'" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
						" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + "  and "
		end if
	else
		if lower(a_table_name) = "cr_journal_lines_stg" then
			outer_sqls       = &
				"insert into cr_validations_invalid_ids2a " + &
					"(table_name, id, msg) " + &
						"( select " + i_hint_str + " '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
						" from " + a_table_name + " where journal_id = " + a_ifb_id + " and " + i_add_where + "  and "
		else
			outer_sqls       = &
				"insert into cr_validations_invalid_ids2a " + &
					"(table_name, id, msg) " + &
						"( select " + i_hint_str + " '" + a_table_name + "', id, " + " 'COMBOS (" + rule + ")' " + &				
						" from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + "  and "
		end if
	end if
	
	outer_sqls = outer_sqls + " id in (" 
	
	//
	//	 Create the lists of anchor and validate points
	//
	a = 1
	v = 1
	for c = 1 to num_cols
		col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
		choose case col_name
			case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
				//  Do nothing.
			case else
//				col_name = '"' + col_name + '"'
				col_val = i_ds_rule.GetItemString(i, c)
				if col_val = "2" then
					anchor_col_list[a] = col_name
					a++
				elseif col_val = "1" then
					validate_col_list[v] = col_name
					v++
				end if
		end choose
	next
	
	
	//
	//  1ST SELECT:
	//
	validate_sqls = "select " + i_hint_str + "id from " + a_table_name + " "
	
	//
	//  1ST & 2ND WHERE CLAUSE: range_sqls1 is the first where clause and only contains
	//			the anchor points; range_sqls2 is the second where clause and contains
	//			the anchor points, validate points, and effective month number logic
	//
	
	range_sqls1 = ""
	range_sqls2 = ""
	
	for j = 1 to num_control_rows
		skip_this_control_record = false
		
		range1 = "(" 
		range2 = "("
		
		for k = 1 to upperbound(anchor_col_list)
			anchor = anchor_col_list[k]
			anchor_value = i_ds_control.GetItemString(j,anchor)
			
			// Anchor points
			if left(anchor_value,1) = '{' then
				
				start_pos = 1
				counter2 = 0
				
				if i_use_new_structures_table = "YES" then
					comma_pos = 0
					the_last_comma_pos = 0
				else			
					do while true
						counter2++
						
						//Endless loop protection...
						if counter2 > 1000 then
							f_pp_msgs("Endless loop in uf_validate_combos_range_based. (1)")
							exit
						end if
						
						comma_pos = pos(anchor_value,",",start_pos)
						
						if comma_pos = 0 then exit
						
						the_last_comma_pos = comma_pos
						start_pos			 = comma_pos + 1
					loop
				end if
				
				if i_use_new_structures_table = "YES" then
					anchor_value = right(anchor_value, (len(anchor_value) - the_last_comma_pos) - 1)
				else
					anchor_value = right(anchor_value, len(anchor_value) - the_last_comma_pos)
				end if
				
				anchor_value = trim(left(anchor_value, len(anchor_value) - 1))
				
				select "ORDER" into :element_order
				  from cr_elements
				 where upper(replace(replace(replace(description,' ','_'),'-','_'),'/','_')) = :anchor;
				 
				structure_id = i_ds_control.GetItemNumber(j,"structure_id_"+string(element_order))
				
				if i_use_new_structures_table = "YES" then
					select range_start, range_end into :range_start, :range_end
					  from cr_structure_values2
					 where structure_id = :structure_id
						and element_value = :anchor_value;
				else
					select range_start, range_end into :range_start, :range_end
					  from cr_structure_values
					 where structure_id = :structure_id
						and value_id = :anchor_value;
				end if
				
				if isnull(range_start) then range_start = ""
				if isnull(range_end) then range_end = ""
				
				if range_start = "" or range_end = "" then
					// The structure point must have ranges entered if they want to use it
					// so skip this control record entirely if any part is bad
					skip_this_control_record = true
					exit
				end if
				
				range1 = range1 + ' "' + anchor + '"' + " between '" + range_start + "' and '" + range_end + "' "
				range2 = range2 + ' "' + anchor + '"' + " between '" + range_start + "' and '" + range_end + "' "
				
			else
				range1 = range1 + ' "' + anchor + '"' + " = '" + anchor_value + "' "
				range2 = range2 + ' "' + anchor + '"' + " = '" + anchor_value + "' "
			end if //if left(anchor_value,1) = '{' then
			
			if k <> upperbound(anchor_col_list) then
				range1 = range1 + " and " 
				range2 = range2 + " and "
			end if
			
		next // k = 1 to upperbound(anchor_col_list)
		
		if skip_this_control_record then
			continue
		end if
		
		// Validate points
		for k = 1 to upperbound(validate_col_list)
			validate_col = validate_col_list[k]
			validate_col_value = i_ds_control.GetItemString(j,validate_col)
			
			if left(validate_col_value,1) = '{' then
				
				start_pos = 1
				counter2 = 0
				
				if i_use_new_structures_table = "YES" then
					comma_pos = 0
					the_last_comma_pos = 0
				else			
					do while true
						counter2++
						
						//Endless loop protection...
						if counter2 > 1000 then
							f_pp_msgs("Endless loop in uf_validate_combos_range_based. (2)")
							exit
						end if
						
						comma_pos = pos(validate_col_value,",",start_pos)
						
						if comma_pos = 0 then exit
						
						the_last_comma_pos = comma_pos
						start_pos			 = comma_pos + 1
					loop
				end if
				
				if i_use_new_structures_table = "YES" then
					validate_col_value = right(validate_col_value, (len(validate_col_value) - the_last_comma_pos) - 1)
				else
					validate_col_value = right(validate_col_value, len(validate_col_value) - the_last_comma_pos)
				end if
				
				validate_col_value = trim(left(validate_col_value, len(validate_col_value) - 1))
				
				select "ORDER" into :element_order
				  from cr_elements
				 where upper(replace(replace(replace(description,' ','_'),'-','_'),'/','_')) = :validate_col;
				 
				structure_id = i_ds_control.GetItemNumber(j,"structure_id_"+string(element_order))
				
				if i_use_new_structures_table = "YES" then
					select range_start, range_end into :range_start, :range_end
					  from cr_structure_values2
					 where structure_id = :structure_id
						and element_value = :validate_col_value;
				else
					select range_start, range_end into :range_start, :range_end
					  from cr_structure_values
					 where structure_id = :structure_id
						and value_id = :validate_col_value;
				end if
				
				if isnull(range_start) then range_start = ""
				if isnull(range_end) then range_end = ""
				
				if range_start = "" or range_end = "" then
					// The structure point must have ranges entered if they want to use it
					// so skip this control record entirely if any part is bad
					skip_this_control_record = true
					exit
				end if
				
				// There will always be at least one anchor, so we need to add the 'and' here before
				// adding the validate columns
				range2 = range2 + ' and "' + validate_col + '"' + " between '" + range_start + "' and '" + range_end + "' "
			else
				range2 = range2 + ' and "' + validate_col + '"' + " = '" + validate_col_value + "' "
			end if //if left(anchor_value,1) = '{' then
			
//			if k <> upperbound(validate_col_list) then
//				range2 = range2 + " and " 
//			end if
			
		next //for k = 1 to upperbound(validate_col_list)
		
		if skip_this_control_record then
			continue
		end if
		
		// Effective Month Number
		if i_effmn_cv = "YES" then
			status = i_ds_control.GetItemNumber(j,"status")
			effmn  = i_ds_control.GetItemNumber(j,"effective_month_number")
			effmn2 = i_ds_control.GetItemNumber(j,"effective_month_number2")
			if isnull(status) then status = 0
			
			if i_effmn2_cv = "YES" then
				if status = 1 then
					range2 = range2 + " and month_number between " + string(effmn) + " and " + string(effmn2) + " "
				else
					range2 = range2 + " and (month_number < " + string(effmn) + " or month_number > " + string(effmn2) + ") "
				end if
			else
				if status = 1 then
					range2 = range2 + " and month_number >= " + string(effmn) + " "
				else
					range2 = range2 + " and month_number < " + string(effmn) + " "
				end if
			end if
		end if
		
		range1 = range1 + ")"
		range2 = range2 + ")"
		
		range_sqls1 = range_sqls1 + range1
		range_sqls2 = range_sqls2 + range2
		
		if j <> num_control_rows then
			range_sqls1 = range_sqls1 + " or "
			range_sqls2 = range_sqls2 + " or "
		end if
		
	next //for j = 1 to num_control_rows
	
	//
	// Check to make sure both range_sqls1 and range_sqls2 are populated;
	// if they aren't, let the user know and move on to the next rule.
	//
	if (isnull(range_sqls1) or range_sqls1 = "") and &
		(isnull(range_sqls2) or range_sqls2 = "") then
		
		f_pp_msgs("WARNING IN UF_VALIDATE_COMBOS_RANGE_BASED: The where clause is empty for rule '" + &
					 rule + "'.  Please verify that all structure points for this rule have assigned ranges.")
		f_pp_msgs("THIS RULE WILL BE SKIPPED!")
		goto next_rule
		
	end if
	
	// ### - SEK - 5946 - 111910
	if right(range_sqls1,3) = "or " then
		range_sqls1 = left(range_sqls1,len(range_sqls1)-3)
	end if
	if right(range_sqls2,3) = "or " then
		range_sqls2 = left(range_sqls2,len(range_sqls2)-3)
	end if

	//
	// Add the 1st where clause
	// 
	if lower(a_table_name) = "cr_journal_lines_stg" then
		validate_sqls = validate_sqls + " where (" + range_sqls1 + ") " + &
							 " and journal_id = " + a_ifb_id + " and " + i_add_where + " "
	else
		validate_sqls = validate_sqls + " where (" + range_sqls1 + ") " + &
							 " and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
	end if
		
	//
	//  MINUS:
	//
	validate_sqls = validate_sqls + " minus "
		
	//
	//  2ND SELECT:
	//
	validate_sqls = validate_sqls + " select " + i_hint_str + "id from " + a_table_name + " "
	
	
	//
	// Add the 2nd where clause
	// 
	if lower(a_table_name) = "cr_journal_lines_stg" then
		//  DML: MULTI-ANCHOR
		validate_sqls = validate_sqls + " where (" + range_sqls2 + ") " + &
							 " and journal_id = " + a_ifb_id + " and " + i_add_where + " "
	else
		//  DML: MULTI-ANCHOR
		validate_sqls = validate_sqls + " where (" + range_sqls2 + ") " + &
							 " and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " "
	end if
	
	
	//  Must be before the command that adds the outer_sqls.
	count_sqls  = "select count(*) from (" + validate_sqls  + ")"	
	
	//
	//  CLEANUP:
	//
	//  Since more than one column could be invalid and currently we cannot insert the
	//  id more than once.
	//
	if i_use_intermediate_tbl = 'NO' then
		validate_sqls  = outer_sqls + validate_sqls  + " ) minus select table_name, id, msg from cr_validations_invalid_ids2) "
	else
		//	remove the minus if using the intermediate table.  That's the whole purpose of the table.
		validate_sqls  = outer_sqls + validate_sqls  + " ) ) "
	end if
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
		
	i_ds_count.Reset()
	count_sqls = uf_validate_combos_update_sql(count_sqls)
	i_ds_count.SetSQLSelect(count_sqls)
	i_ds_count.RETRIEVE()
	
	ncount = i_ds_count.GetItemNumber(1, 1)
	
	if ncount > 0 then
		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
			string(ncount) + " INVALID COMBINATIONS.")
		kickouts = true
	ELSE
		GOTO NEXT_RULE
	end if
	
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
		
	validate_sqls = uf_validate_combos_update_sql(validate_sqls)
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	//  MOVED ABOVE
	//	if sqlca.SQLNRows > 0 then
	//		f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
	//			string(ncount) + " INVALID COMBINATIONS.")
	//		kickouts = true
	//	end if
	
	//  If they are using effective_month_number (i_effmn_cv = "YES") then we must evaluate 2
	//  SQL statements ... The SQL does not allow the outer joins with an OR clause that
	//  would be needed to evaluate for missing combo values.
	if i_effmn_cv = "YES" then
		
		if sqlca.SQLNRows > 0 then
			//  We got a validation kickout on validate_sqls ... no need to evaluate validate_sqls2.
			//  Continue on to the next rule.
			if i_use_intermediate_tbl = 'NO' then
				continue
			else
				goto clear_intermediate_tbl
			end if
		else
			//  The validate_sqls did not get a kickout ... 
			
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS_RANGE_BASED: inserting into cr_validations_invalid_ids2 !")
				f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				f_pp_msgs(validate_sqls)
				f_pp_msgs("  ")
				rollback;
				// DMJ: 02/04/10: To Avoid Mem Leaks
				//DESTROY ds_count
				return -1
			end if
			
		end if
		
	end if
	
	//	This insert needs to be in the loop.  We are trying to avoid a minus between the 
	//		validate_sqls and cr_validations_invalid_ids2(or 2a) if use_intermediate_tbl = 'YES'.  
	//		We are inserting into cr_validations_invalid_ids2a so without the minus it needs
	//		to be cleared out in the loop.
	clear_intermediate_tbl:
	if i_use_intermediate_tbl = 'NO' then 
	else
		
		//	Insert into 2 from 2a here.  2a must also be cleared out before continuing on in case another
		//		function tries to insert into 2a.  This is needed since there isn't a minus anymore.
				
		insert into cr_validations_invalid_ids2 (table_name, id, msg)
		select table_name, id, msg from cr_validations_invalid_ids2a
		minus
		select table_name, id, msg from cr_validations_invalid_ids2;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Inserting into cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			//	Don't return...always clear out cr_validations_invalid_ids2a.
	//			return -1
		end if
		
		sqlca.truncate_table('cr_validations_invalid_ids2a')
				
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Truncating cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	end if
	
	//	the truncate will fire a commit.  Go ahead and put one here to be explicit.
	commit;
	
	next_rule:


next


//  WE ARE NOT INSERTING INTO CR_VALIDATION_INVALID_IDS HERE.  THAT WILL HAPPEN IN
//  UF_VALIDATE_COMBOS AFTER ALL RULES ARE EVALUATED.


//  WE CANNOT UPDATE THE VALID_ONLINE FIELD HERE.  THAT MUST HAPPEN IN UF_VALIDATE_COMBOS
//  AFTER ALL RULES ARE EVALUATED.


commit;

if kickouts then
	return -2
else
	return 1
end if


end function

public function longlong uf_validate_combos_exists (string a_table_name, string a_ifb_id, longlong a_rule_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_combos_exists
//
//  Return Codes :   1 = Success
//						  	-1 = SQL Error
// 					  		-2 = Validation kickouts occurred
//
//	 Note			  : There is no reference to the cr_validation_exclusion table in this f(x).
//						 It is assumed that a rule will not include elements that should not be 
//						 validated.
//
//						This function calls is an alterate to the include logic in uf_validate_combos.
//						In certain cases, the logic there with "in" statements and "minus" statements
//						does not perform well regardless of indexes.  The system control can be set
//						to use the "exists" logic instead.
//
//*****************************************************************************************
longlong i, num_rules, num_cols, c, counter, ncount, rtn, range_based
string  sqls, validate_sqls, col_name, col_val, rule, count_sqls
boolean kickouts

string exists_clause, not_exists_clause, select_list

select rule into :rule from cr_validation_rules where rule_id = :a_rule_id;
if isnull(rule) or rule = '' then rule = "Rule " + string(a_rule_id)

sqls = "select * from cr_validation_rules where rule_id = " + string(a_rule_id)
f_create_dynamic_ds(i_ds_rule, "grid", sqls, sqlca, true)
num_cols  = long(i_ds_rule.Object.DataWindow.Column.Count)

//  EXISTS CLAUSE
exists_clause = ''
not_exists_clause = ''
for c = 1 to num_cols
	col_name = upper(i_ds_rule.Describe("#" + string(c) + ".Name"))
	choose case col_name
		case "ID", "RULE_ID", "STATUS", "NOTES", "TIME_STAMP", "USER_ID", "BUDGET_RULE", "RANGE_BASED"
			//  Do nothing.
		case else
			col_name = '"' + col_name + '"'
			col_val = i_ds_rule.GetItemString(1, c)
			
			if col_val = "2" then // Anchor
				exists_clause = exists_clause + "a." + col_name + " = b." + col_name + " and "
			end if
			
			if col_val = "1" or col_val = "2" then // validate or anchor
				not_exists_clause = not_exists_clause + "a." + col_name + " = b." + col_name + " and "
				select_list = select_list + "a." + col_name + "||'-'||"
			end if
	end choose
next
select_list = mid(select_list,1,len(select_list) - 7)

exists_clause = '(select 1 from cr_validation_combos b where ' + exists_clause + ' rule_id = ' + string(a_rule_id) + ')'

not_exists_clause = '(select 1 from cr_validation_combos b where ' + not_exists_clause + ' rule_id = ' + string(a_rule_id) 
if i_effmn_cv = "YES" then 
	if i_effmn2_cv = "YES" then
		if i_effmn2_multi_rec_cv = "YES" then
			not_exists_clause = not_exists_clause + &
				" and a.month_number between b.effective_month_number (+) and b.effective_month_number2 (+)" + &
				" and b.status (+) = 1 and b.effective_month_number is null"
		else
			not_exists_clause = not_exists_clause + &
				" and (( a.month_number between b.effective_month_number and b.effective_month_number2 and b.status = 1) or " + &
				" (a.month_number not between b.effective_month_number and b.effective_month_number2 and b.status = 0))"
		end if
	else
		not_exists_clause = not_exists_clause + &
			" and ((a.month_number >=  b.effective_month_number and b.status = 1) or " + &
			" (a.month_number < b.effective_month_number and b.status = 0))"
	end if
end if
not_exists_clause = not_exists_clause + ')'

count_sqls = 'select ' + i_hint_str + ' count(*) from ' + a_table_name + ' a ' + &
	"where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and " + &
	"exists " + exists_clause + " and " + &
	"not exists  " + not_exists_clause

// ### 7983: JAK: 2011-07-07:  The single quotes around i hint string need to be double quotes here...
validate_sqls = "insert into cr_validations_invalid_ids2 (table_name, id, msg) " + &
	"(select " + i_hint_str + " '" + a_table_name + "', id, 'COMBOS (" + rule + ")' " + &				
	' from ' + a_table_name + ' a ' + &
	"where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and " + &
	"exists " + exists_clause + " and " + &
	"not exists  " + not_exists_clause

select_list = 'select  ' + i_hint_str + ' distinct ' + select_list + ' from ' + a_table_name + ' a ' + &
	"where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " and " + &
	"exists " + exists_clause + " and " + &
	"not exists  " + not_exists_clause

////// Update the SQL based on the system controls
if i_use_intermediate_tbl = 'YES' then 
	validate_sqls = f_replace_string(validate_sqls,'cr_validations_invalid_ids2','cr_validations_invalid_ids2a','all')
	validate_sqls  = validate_sqls + " ) "
else
	validate_sqls  = validate_sqls + " minus select table_name, id, msg from cr_validations_invalid_ids2 where table_name = '" + a_table_name + "' and msg = 'COMBOS (" + rule + ")') "
end if

//
//  LOG THE VALIDATION ERRORS:
//
i_ds_count.Reset()
count_sqls = uf_validate_combos_update_sql(count_sqls)
i_ds_count.SetSQLSelect(count_sqls)
i_ds_count.RETRIEVE()

ncount = i_ds_count.GetItemNumber(1, 1)

if ncount > 0 then
	f_pp_msgs("************  COMBOS (" + rule + "): THERE ARE " + &
		string(ncount) + " INVALID COMBINATIONS ... ")
	kickouts = true
	
	if i_log_combo_kickouts = 'YES' then
		i_ds_msg_one_col.Reset()
		select_list = uf_validate_combos_update_sql(select_list)
		i_ds_msg_one_col.SetSQLSelect(select_list)
		i_ds_msg_one_col.RETRIEVE()
		
		for i = 1 to i_ds_msg_one_col.rowcount()
			f_pp_msgs(i_ds_msg_one_col.getitemstring(i,1))
		next
	end if
	
	//
	//  LOG THE VALIDATION ERRORS:
	//
	validate_sqls = uf_validate_combos_update_sql(validate_sqls)
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("VALIDATE_SQLS = " + validate_sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	if i_use_intermediate_tbl = 'NO' then 
	else
		
		//	Insert into 2 from 2a here.  2a must also be cleared out before continuing on in case another
		//		function tries to insert into 2a.  This is needed since there isn't a minus anymore.
		insert into cr_validations_invalid_ids2 (table_name, id, msg)
		select table_name, id, msg from cr_validations_invalid_ids2a
		minus
		select table_name, id, msg from cr_validations_invalid_ids2;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Inserting into cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			// Always clear!
			sqlca.truncate_table('cr_validations_invalid_ids2a')
			return -1
		end if
		
		sqlca.truncate_table('cr_validations_invalid_ids2a')
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: Truncating cr_validations_invalid_ids2a !")
			f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	end if

end if

//	the truncate will fire a commit.  Go ahead and put one here to be explicit.
commit;

if kickouts then
	return -2
else
	return 1
end if


end function

public function longlong uf_validate_me_autoextend (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_me_autoextend
//
//  Return Codes :   1 = Success
//						  -1 = SQL Error
//
//*****************************************************************************************
string sqls, element, element_table, element_column, element_value, company_value
longlong 	source_id, i, element_id, max_months, values_by_company, j, old_month_number
longlong 	new_month_number

if mid(upper(a_table_name),1,14) = 'CR_ALL_DETAILS' then 
source_id = g_cr.uf_get_source_id("table_name",'CR_JOURNAL_LINES') //Autogenerated sql replace
else
source_id = g_cr.uf_get_source_id("table_name",f_replace_string(f_replace_string(f_replace_string(f_replace_string(f_replace_string(a_table_name,'CRB_','CR_','all'),'_STG3','','all'),'_STG2','','all'),'_STG1','','all'),'_STG','','all')) //Autogenerated sql replace
end if

if isnull(source_id) then return 1

sqls = 'select * from cr_master_element_autoextend where max_months > 0 and source_id = ' + string(source_id)
//uo_ds_top ds_extend
//ds_extend = create uo_ds_top
//f_create_dynamic_ds(i_ds_extend,"grid",sqls,sqlca,true)
i_ds_extend.Reset()
i_ds_extend.SetSQLSelect(sqls)
i_ds_extend.Retrieve()

uo_ds_top ds_me_values
ds_me_values = create uo_ds_top

for i = 1 to i_ds_extend.rowcount()
	element_id = i_ds_extend.getitemnumber(i,'element_id')
	max_months = i_ds_extend.getitemnumber(i,'max_months')

element = f_replace_string(f_replace_string(f_replace_string(upper(g_cr.uf_get_element_description(element_id)),'/','_','all'),'-','_','all'),' ','_','all') //Autogenerated sql replace
element_column = f_replace_string(f_replace_string(f_replace_string(upper(g_cr.uf_get_element_column(element_id)),'/','_','all'),'-','_','all'),' ','_','all') //Autogenerated sql replace
element_table = f_replace_string(f_replace_string(f_replace_string(upper(g_cr.uf_get_element_table(element_id)),'/','_','all'),'-','_','all'),' ','_','all') //Autogenerated sql replace
values_by_company = g_cr.uf_get_element_values_by_company(element_id) //Autogenerated sql replace
		
	if isnull(values_by_company) then values_by_company = 0
	
	// Extend the effective_month_number forward where the validation_status_id = 0 and the the effective_month_number is less than the current month
	//	but within the allowed tolerance.
	sqls = 'select a."' + element + '" element_value, b.effective_month_number old_month_number, max(a.month_number) + 1 new_month_number ' 
	if values_by_company = 1 then	 sqls = sqls + ', a."' + i_co_field + '" company_value '
	sqls = sqls + "from " + a_table_name + " a, " + element_table + " b " + &
		"where a.interface_batch_id = '" + a_ifb_id + "' " + " and " + "a." + i_add_where + &
		"and b.element_type = 'Actuals' " + &
		'and a."' + element + '" =  b."' + element_column + '" ' + &
		"and b.validation_status_id = 0 " + &
		"and a.month_number >= b.effective_month_number " + &
		"and a.month_number <  " + &
		"	case when substr(b.effective_month_number,5,2) > '12' then " + &
		"		substr(b.effective_month_number,1,4) || '00' + " + string(max_months) + " " + &
		"	when substr(b.effective_month_number + " + string(max_months) + ",5,2) > '12' then " + &
		"		b.effective_month_number + " + string(max_months) + " + 88 " + &
		"	else b.effective_month_number + " + string(max_months) + " end "
	if values_by_company = 1 then	 sqls = sqls + 'and a."' + i_co_field + '" = b."' + i_co_field + '" '
	sqls = sqls + 'group by a."' + element + '", b.effective_month_number'
	if values_by_company = 1 then	 sqls = sqls + ' ,a."' + i_co_field + '"'
	
	destroy(ds_me_values)
	ds_me_values = create uo_ds_top
	f_create_dynamic_ds(ds_me_values,'grid',sqls,sqlca,true)
	
	if trim(ds_me_values.i_sqlca_sqlerrtext) <> '' and not isnull(ds_me_values.i_sqlca_sqlerrtext) then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_ME_AUTOEXTEND: Retrieving values to autoextend!")
		f_pp_msgs("ERROR MESSAGE: " + ds_me_values.i_sqlca_sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	if ds_me_values.rowcount() = 0 then continue
	
	for j = 1 to ds_me_values.rowcount()
		if j = 1 then f_pp_msgs("The following " + element + " values were automatically extended:")
		element_value = ds_me_values.GetItemString(j,1)
		old_month_number = ds_me_values.GetItemNumber(j,2)
		new_month_number = ds_me_values.GetItemNumber(j,3)
		if values_by_company = 1 then	 
			company_value = ds_me_values.GetItemString(j,4)
			f_pp_msgs(company_value + '-' + element_value + '   (Extended to ' + string(new_month_number) + ' from ' + string(old_month_number) + ')')
		else
			f_pp_msgs(element_value + '   (Extended to ' + string(new_month_number) + ' from ' + string(old_month_number) + ')')
		end if
		
		if i_me_autoextend then
			if values_by_company = 1 then	 
				sqls = "update " + element_table + " set effective_month_number = " + string(new_month_number) + " " + &
					'where "' + element_column + '" = ' + "'" + element_value + "' and element_type = 'Actuals' " + &
					'and "' + i_co_field + '" = ' + "'" + company_value + "'"
			else
				sqls = "update " + element_table + " set effective_month_number = " + string(new_month_number) + " " + &
					'where "' + element_column + '" = ' + "'" + element_value + "' and element_type = 'Actuals'"
			end if
		
			
			execute immediate :sqls;
				
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR IN UF_VALIDATE_ME_AUTOEXTEND: Update master element table!")
				f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				f_pp_msgs(sqls)
				f_pp_msgs("  ")
				rollback;
				return -1
			end if
		end if
	next
next

return 1
end function

public function string uf_validate_combos_update_sql (string a_sqls);if pos(lower(a_sqls),'cr_journal_lines') > 0 then
	a_sqls = f_replace_string(a_sqls,'interface_batch_id','journal_id','all')
end if

if i_cv_control_or_combos = 'CONTROL' then 
	a_sqls = f_replace_string(a_sqls,'cr_validation_combos','cr_validation_control','all')
end if

return a_sqls
end function

public function longlong uf_validate_month_number (string a_table_name, string a_ifb_id, longlong a_source_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_month_number
//
//  Return Codes :   	1 = Success
//						  	-1 = SQL Error
//							-2 = Validation kickouts occurred
//
//  NOTE:  THIS FUNCTION DOES NOT HAVE THE NEW TECHNIQUE AGAINST THE 
//         CR_VALIDATIONS_INVALID_IDS2 TABLE SINCE RECORDS WITH INVALID MONTHS DO NOT
//         GET FLAGGED AS INVALID IDS.  SEE COMMENT BELOW.
//
//*****************************************************************************************
longlong counter, num_rows, source_id, i, src, mn
string  sqls, validate_sqls, co
boolean kickouts_mn, kickouts_ar

//  For Acct Range Validation:
longlong ncount, ele_id, excl_counter
string  count_sqls, element, main_sqls

//  Cannot validate the MN from the validator since there is no source_id.
if i_called_from_validator &
	or lower(a_table_name) = 'cr_all_details_combo_validate'  &
	or lower(a_table_name) = 'cr_all_details_orig_stg' &
	or lower(a_table_name) = 'cr_all_details_dr_stg' &
	then goto after_mn_validation

//
//  SETUP.
//
kickouts_mn = false
kickouts_ar = false

//  Need the source_id.
//  DMJ: 11/22/2005: 
//    At PNM they wanted to post many different instances of their Banner source into 
//    the same staging table.  Control requirements dictated that we have multiple 
//    interface exes and multiple CR staging tables.  The multiple tables were named
//    like the CR's traditional staging tables, but with numbers at the end (e.g. 
//    cr_banner_stg2).  The source_id lookup below has been modified so the MN 
//    validation will work properly.  Note that we have fundamentally set a limit of 4
//    staging tables with this code.
source_id = 0

if not isnull(a_source_id) and a_source_id <> 0 then
	// Kickouts (or derivations) Processing
	source_id = a_source_id
else
	source_id = g_cr.uf_get_source_id("table_name",f_replace_string(f_replace_string(f_replace_string(f_replace_string(f_replace_string(upper(a_table_name),'CRB_','CR_','all'),'_STG3','','all'),'_STG2','','all'),'_STG1','','all'),'_STG','','all')) //Autogenerated sql replace
											 
	if isnull(source_id) then source_id = 0
end if

// Derivations.  No such thing as month number validations
if source_id = -99 then return 1

if source_id = 0 or source_id = -1 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_MONTH_NUMBER: could not find a source_id (2) for " + &
		a_table_name)
	f_pp_msgs("  ")
	return -1
end if


//  Need the "company" field.
//  DMJ: 6/24/08: moved above to support acct range validations.


//
//  VALIDATE THE MONTH_NUMBER.
//
sqls = &
	"select " + i_co_field + ", " + string(source_id) + " as source_id, month_number " + &
	  "from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " " + &
	" minus " + &
	"select " + i_co_field + ", source_id, month_number from cr_open_month_number " + &
	" where status = 1"

f_create_dynamic_ds(i_ds_invalid_mn,"grid",sqls,sqlca,true)

num_rows = i_ds_invalid_mn.RowCount()

for i = 1 to num_rows
	if i = 1 then
		f_pp_msgs("************  THERE ARE TRANSACTIONS AGAINST CLOSED MONTHS  ************")
	end if
	co  = i_ds_invalid_mn.GetItemString(i, 1)
	src = i_ds_invalid_mn.GetItemNumber(i, 2)
	mn  = i_ds_invalid_mn.GetItemNumber(i, 3)
	f_pp_msgs("-- " + co + "  " + string(src) + "  " + string(mn))
	// SEK: 4/16/09: Changed kickouts to kickouts_mn... we want to be able to
	//		  tell the difference between Month Number and Account Range kickouts
	//		  at Southern because Month Number kickouts should not have suspense
	//		  accounting applied, but Account Range kickouts can go to suspense.
	kickouts_mn = true
next

	

//
//  FOR NOW, DO NOT INSERT INTO THE INVALID IDS TABLE.  IT DOES NOT LET THEM EDIT THE MN.
//
////
////  INSERT THE INVALID IDS:
////
//
//validate_sqls = "insert into cr_validations_invalid_ids (table_name, id) (" + &
//					 "select '" + a_table_name + "',id from " + a_table_name + &
//					 " where work_order = ' ' and account in (select account from cr_itc_account_wo) " + &
//					 "   and interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " " + &
//					 " minus select table_name, id from cr_validations_invalid_ids)"
//
//
//execute immediate :validate_sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR IN UF_VALIDATE_CUSTOM: inserting into cr_validations_invalid_ids !")
//	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	f_pp_msgs(validate_sqls)
//	f_pp_msgs("  ")
//	rollback;
//	DESTROY ds_invalid_mn
//	return -1
//end if


commit;


//DESTROY ds_invalid_mn


after_mn_validation:



//*****************************************************************************************
//
//  ACCT RANGE VALIDATION:
//    Placed here for ease of upgrade and to be less intrusive to stand-alone processes.
//
//*****************************************************************************************

////  Are acct range validations enabled ?
//setnull(cv)
//select upper(trim(control_value)) into :cv from cr_system_control
// where lower(control_name) = 'enable validations - acct range';
//if isnull(cv) or cv = "" then cv = "NO"

if i_cv_acct_range = "NO" then goto after_acct_range

//
//  SETUP.
//
//datastore ds_count
//ds_count = CREATE datastore

// Create the shell of this datastore.  It will be for the record counts.
//sqls = 'select count(*) from cr_elements where element_id = -1'
//f_create_dynamic_ds(i_ds_count, "grid", sqls, sqlca, true)
//
//  DO NOT reset the kickouts flag ... it is either false from above or true already
//  if there was a month_number kickout.

//  For the decode below.
//datastore ds_elements
//ds_elements = CREATE datastore
//sqls = "select * from cr_elements"
//f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)
//
//ds_elements.SetSort("element_id a")
//ds_elements.Sort()
//
//num_elements = ds_elements.RowCount()

validate_sqls = "insert into cr_validations_invalid_ids2 (table_name, id, msg) " + &
	"(select distinct '" + a_table_name + "', a.id, " + &
		"(select 'INVALID ACCT RANGE : ' || x.description ||' : '|| b.start_value ||' - '||b.end_value from cr_elements x where b.element_id = x.element_id) "

count_sqls = "select count(distinct a.id) "

main_sqls = "from " + a_table_name + " a, cr_validation_acct_range b " + &
	"where a.month_number = b.month_number and a." + i_co_field + " = b." + i_co_field + " " + &
	"and decode(b.element_id "

for i = 1 to i_num_elements
	ele_id  = i_ds_elements.GetItemNumber(i, "element_id")
	element = upper(i_ds_elements.GetItemString(i, "description"))
	element = f_cr_clean_string(element)
	main_sqls = main_sqls + ", " + string(ele_id) + ', a."' + element + '"'
next

main_sqls = main_sqls + &
	") between b.start_value and b.end_value and sysdate between b.start_date and b.end_date "

if lower(a_table_name) = "cr_journal_lines_stg" then
	main_sqls = main_sqls + " and a.journal_id = " + a_ifb_id + " and a." + i_add_where + " "
else
	main_sqls = main_sqls + " and a.interface_batch_id = '" + a_ifb_id + "' and a." + i_add_where + " "
end if

// Check to see if there are any GL Journal Category to exclude from the
// account range validations
select count(*) into :excl_counter from cr_validation_acct_range_excl;
if isnull(excl_counter) then excl_counter = 0

if excl_counter > 0 then
	main_sqls = main_sqls + " and not exists ( " + &
					"select 1 from cr_validation_acct_range_excl v " + &
					" where a.gl_journal_category = v.gl_journal_category) " 
end if

count_sqls = count_sqls + main_sqls
	//
	//  LOG THE VALIDATION ERRORS:
	//
	i_ds_count.Reset()
	i_ds_count.SetSQLSelect(count_sqls)
	i_ds_count.RETRIEVE()
	
	ncount = i_ds_count.GetItemNumber(1, 1)
		
	if ncount > 0 then
		
		f_pp_msgs("************  ACCT RANGE: " + string(ncount) + " invalid transaction(s) ... ")
		
		// SEK: 4/16/09: Changed kickouts to kickouts_ar... we want to be able to
		//		  tell the difference between Month Number and Account Range kickouts
		//		  at Southern because Month Number kickouts should not have suspense
		//		  accounting applied, but Account Range kickouts can go to suspense.
		kickouts_ar = true
		
	end if

//
//  INSERT THE INVALID IDS:
//
if ncount > 0 then
	validate_sqls = validate_sqls + main_sqls + "minus select table_name, id, msg from cr_validations_invalid_ids2)"
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_MONTH_NUMBER (ACCT RANGE): inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs(validate_sqls)
		f_pp_msgs("  ")
		rollback;
//		DESTROY ds_elements
//		DESTROY ds_count
		return -1
	end if
end if

insert into cr_validations_invalid_ids (table_name, id) (
	select distinct table_name, id from cr_validations_invalid_ids2
	 where table_name = :a_table_name
	minus
	select table_name, id from cr_validations_invalid_ids);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_MONTH_NUMBER (ACCT RANGE): inserting into cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_elements
//	DESTROY ds_count
	return -1
end if


//  DMJ: 12/27/05: This update has an inherent problem.  It does not restrict by interface_batch_id or journal_id.
//                 Since we coded around this in the Delete Line button of m_cr_journals ...  and since it really
//                 should not affect feeder sources, I am going to leave this alone.  I feel it is too dangerous
//                 to change.  If it causes any client problems we can change it later.
//  JAK: 11/13/06: Pinnacle hit a deadlock error a few times when many users were hammering on journal entries
//						 Added an interface_batch_id restriction to the below SQL to try and alleviate this.
//update cr_validations_invalid_ids
//set valid_online = 0
//where id in (
//	select id from cr_validations_invalid_ids2
//	where table_name = :a_table_name)
//and table_name = :a_table_name;
if lower(a_table_name) = "cr_journal_lines_stg" then
	sqls = 'update cr_validations_invalid_ids ' + &
		'set valid_online = 0 ' + &
		'where id in ( ' + &
		'select id from cr_validations_invalid_ids2 ' + &
		"where table_name = '" + a_table_name + "' " + &
		"and id in (select id from " + a_table_name + &		
		" where journal_id = '" + a_ifb_id + "' and " + i_add_where + ")) " + &
		"and table_name = '" + a_table_name + "'"
else
	sqls = 'update cr_validations_invalid_ids ' + &
		'set valid_online = 0 ' + &
		'where id in ( ' + &
		'select id from cr_validations_invalid_ids2 ' + &
		"where table_name = '" + a_table_name + "' " + &
		"and id in (select id from " + a_table_name + &
		" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + ")) " + &
		"and table_name = '" + a_table_name + "'"
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_MONTH_NUMBER: updating cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_count
	return -1
end if


commit;


//DESTROY ds_elements
//DESTROY ds_count
after_acct_range:
//*****************************************************************************************
//
//  END OF:
//  ACCT RANGE VALIDATION:
//    Placed here for ease of upgrade and to be less intrusive to stand-alone processes.
//
//*****************************************************************************************

// SEK: 4/16/09: Check for MN kickouts first because Southern can't have
//		  suspense accounts applied to invalid months regardless of whether
//		  there are Account Range kickouts.
if kickouts_mn then
	return -2
elseif kickouts_ar then
	return -3
else
	return 1
end if


end function

public function longlong uf_validate_month_number (string a_table_name, string a_ifb_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_month_number
//
//	Overloaded function to support the old arguments.  All logic to perform validations is in the function with 3 arguments
//
//*****************************************************************************************
return uf_validate_month_number(a_table_name, a_ifb_id, 0)
end function

public function longlong uf_runvalidations (string a_table_name, string a_ifb_id, longlong a_source_id);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_runValidations
//
//	 Note			  :  No other validation functions will delete from this table.  The delete
//						  must be called separately from within the interface before calling the
//						  other functions.
//
//*****************************************************************************************
string cv_validations, company_field, cv_memo_val, sqls
longlong lvalidate_rtn, num_rows, overall_rtn
boolean validation_kickouts, invalid_mn, skip_suspense

validation_kickouts = false
invalid_mn = false
skip_suspense = false

company_field = upper(trim(g_cr.uf_get_control_value('COMPANY FIELD'))) //Autogenerated sql replace

if trim(company_field) = '' or isnull(company_field) then
	f_pp_msgs("ERROR: Getting company field from CR System Control!")
	return -1
else
	company_field = f_cr_clean_string(company_field)
end if 

f_pp_msgs("Running Validations at " + string(now()))
f_pp_msgs(" -- Deleting from cr_validations_invalid_ids at " + string(now()))

lvalidate_rtn = uf_delete_invalid_ids(a_table_name, a_ifb_id)

if lvalidate_rtn <> 1 then
	return -1
end if

// Combo Validations
setnull(cv_validations)
cv_validations = upper(trim(g_cr.uf_get_control_value('ENABLE VALIDATIONS - COMBO'))) //Autogenerated sql replace
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then 
	f_pp_msgs(" -- Validating the Batch (Combos) at " + string(now()))
	//	 Which function is this client using?
	setnull(cv_validations)
cv_validations = upper(trim(g_cr.uf_get_control_value('VALIDATIONS - CONTROL OR COMBOS'))) //Autogenerated sql replace
	if isnull(cv_validations) then cv_validations = "CONTROL"
	
	if cv_validations = 'CONTROL' then
		lvalidate_rtn = uf_validate_control(a_table_name, a_ifb_id)
	else
		lvalidate_rtn = uf_validate_combos(a_table_name, a_ifb_id)
	end if
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate_(x).  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		num_rows = 0
		select count(*) into :num_rows
		  from cr_validations_invalid_ids
		 where lower(table_name)= :a_table_name;
		 
		if num_rows > 0 then validation_kickouts = true
	end if
end if

// Project Based
setnull(cv_validations)
cv_validations = upper(trim(g_cr.uf_get_control_value('ENABLE VALIDATIONS - PROJECTS BASED'))) //Autogenerated sql replace
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then
	f_pp_msgs(" -- Validating the Batch (Projects) at " + string(now()))
	lvalidate_rtn = uf_validate_projects(a_table_name, a_ifb_id)
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		num_rows = 0
		select count(*) into :num_rows
		  from cr_validations_invalid_ids
		 where lower(table_name)= :a_table_name;
		 
		if num_rows > 0 then validation_kickouts = true
	end if
end if

// Master Element Validations
setnull(cv_validations)
cv_validations = upper(trim(g_cr.uf_get_control_value('ENABLE VALIDATIONS - ME'))) //Autogenerated sql replace
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then 
	f_pp_msgs(" -- Validating the Batch (Element Values) at " + string(now()))
	lvalidate_rtn = uf_validate_me(a_table_name, a_ifb_id)
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		num_rows = 0
		select count(*) into :num_rows
		  from cr_validations_invalid_ids
		 where lower(table_name)= :a_table_name;
		 
		if num_rows > 0 then validation_kickouts = true
	end if
end if

// Month Number / Range Based Validations
setnull(cv_validations)
cv_validations = upper(trim(g_cr.uf_get_control_value('ENABLE VALIDATIONS - MONTH NUMBER'))) //Autogenerated sql replace
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then 
	f_pp_msgs(" -- Validating the Batch (Open Month Number) at " + string(now()))
	lvalidate_rtn = uf_validate_month_number(a_table_name, a_ifb_id)
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 then
		// Month Number is currently not inserting into the kickouts table.
		//  Validation Kickouts.
		
		// Build the records from the kickout tables here as it isn't done in the UO
		sqls = "insert into cr_validations_invalid_ids2 (table_name, id, msg) " + &
				 "select '" + a_table_name + "', id, 'MONTH IS NOT OPEN' " + &
				 "  from " + a_table_name + " " + &
				 " where interface_batch_id = '" + a_ifb_id + "' " + &
				 " and (" + company_field + ", month_number) in (" + &
				 "   select " + company_field + ", month_number  " + &
				 "     from " + a_table_name + " " + &
				 "    where interface_batch_id = '" + a_ifb_id + "' " + &
				 "   minus " + &
				 "   select " + company_field + ", month_number " + &
				 "     from cr_open_month_number " + &
				 "    where source_id = " + string(a_source_id) + ")"
		
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_validations_invalid_ids2: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
		insert into cr_validations_invalid_ids (table_name, id) (
		select distinct table_name, id from cr_validations_invalid_ids2
		 where table_name = :a_table_name
		minus
		select table_name, id from cr_validations_invalid_ids);
	
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_validations_invalid_ids: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
			
		validation_kickouts = true
		// Don't want to apply suspense accouting if there are invalid month numbers
		invalid_mn = true
	end if
	
	if lvalidate_rtn = -3 then
		// Account Range kickouts -- want these to go to suspense
		validation_kickouts = true
	end if
end if

// CUSTOM VALIDATIONS
// ### 7515: JAK: 2011-05-06:  Custom validations needs to check the system control.
setnull(cv_validations)
cv_validations = upper(trim(g_cr.uf_get_control_value('ENABLE VALIDATIONS - CUSTOM'))) //Autogenerated sql replace
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then
	f_pp_msgs(" -- Validating the Batch (Custom Validations) at " + string(now()))
	lvalidate_rtn = uf_validate_custom(a_table_name, a_ifb_id)

	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		validation_kickouts = true
	end if
end if

// CHECK FOR VALIDATION ERRORS
if validation_kickouts then
	cv_memo_val = ""
cv_memo_val = upper(trim(g_cr.uf_get_control_value('VALIDATIONS: MEMO VAL INTERFACES'))) //Autogenerated sql replace
	if isnull(cv_memo_val) or cv_memo_val = "" then cv_memo_val = "NO"
	
	if cv_memo_val = "NO" then		
		// Use suspense accounting?
		setnull(cv_validations)
cv_validations = upper(trim(g_cr.uf_get_control_value('VALIDATIONS - SUSPENSE ACCOUNTING'))) //Autogenerated sql replace
		if isnull(cv_validations) then cv_validations = "NO"
		
		if cv_validations = "YES" then 
			skip_suspense = true
			lvalidate_rtn = 2
		end if
		
		if invalid_mn then
			// Set the variable below to hold the batch value and skip the suspense function
			skip_suspense = true
			lvalidate_rtn = 2
		end if
		
		if not skip_suspense then
			lvalidate_rtn = uf_suspense(a_table_name, a_ifb_id)
			
			if lvalidate_rtn < 0 then
				// Error in uf_suspense 
				return -1
			end if
		end if
	
		if lvalidate_rtn = 2 then
			f_pp_msgs("  ")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE BATCH WILL NOT BE LOADED!  *****")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("  ")
			
			overall_rtn = -2
		else
			f_pp_msgs("  ")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  " + &
				"SUSPENSE ACCOUNTS HAVE BEEN APPLIED AND THE BATCH WILL BE POSTED!  *****")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("  ")
			
			overall_rtn = 1
		end if
	else
		f_pp_msgs("  ")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("   *****  MEMO VALIDATION KICKOUTS EXIST!  THE BATCH WILL BE LOADED!  *****")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("  ")
		
		overall_rtn = 1
		
		//  DO NOT KICK OUT --- MEMO VALIDATIONS ONLY !!!
		//  WITH MEMO VALIDATIONS, WE MUST FLUSH THE INVALID_IDS TABLES.
		lvalidate_rtn = uf_delete_invalid_ids(a_table_name, a_ifb_id)
		
		if lvalidate_rtn <> 1 then 
			return -1
		end if
	end if
else
	overall_rtn = 1
end if

f_pp_msgs("Validations complete at " + string(now()))


return overall_rtn
end function

public function longlong uf_validate_me_combined (string a_table_name, string a_ifb_id, boolean a_is_bdg_element);//*****************************************************************************************
//
//  User Object  :  uo_cr_validation
//
//  UO Function  :  uf_validate_me_combined
//
//  Return Codes :   1 = Success
//						 -1 = SQL Error
// 						 -2 = Validation kickouts occurred
//
//*****************************************************************************************
longlong i, num_elements, c, counter, element_id, exclude_element, hide, &
		  msg_count, m, values_by_co, rtn
string  sqls, validate_sqls, element, element_table, &
		  element_column, validate_sqls_for_msg, &
		  msg_val, elmnt_type, validate_sqls_from
boolean kickouts, called_from_kickouts = false

if i_cv_combo_vals = "YES" then goto skip_reset

//
//	MAKE THE ASSUMPTION IF THERE ARE RECORDS IN CR_VALIDATIONS_INVALID_IDS
//		FOR A_TABLE_NAME AND A_IFB THEN THIS USER OBJECT WAS CALLED FROM THE
//		KICKOUT WINDOW SINCE THE INTERFACE AND JE WINDOW WOULD HAVE CALLED
//		UF_DELETE_INVALID_IDS.
//

sqls = "select count(*) from cr_validations_invalid_ids where id in (" + &
	"select id from " + a_table_name + " where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " )"
	
f_create_dynamic_ds(i_ds_count,"grid",sqls,sqlca,true)

counter = i_ds_count.getitemnumber(1,1)

if isnull(counter) then counter = 0

if counter > 0 then
	called_from_kickouts = true
end if

//	Start off by marking everything valid and then make them invalid in the loop.
//	This method is preferred for a number of reasons.
//	1) Avoid using "not in" while touching as little existing code as possible.
//	2) If I have two rules and during the first rule everything passes so 
//		valid_online is set to one.  They fail the second rule but the first rule
//		marked them as valid so we would have to go back and unmark them.  It is
//		easier to just invalidate something that fails during a rule rather than
//		making it valid and then worrying about making it invalid in another rule.
sqls = "update cr_validations_invalid_ids set valid_online = 1 where " + &
	"(table_name,id) in (select '" + a_table_name + "',id from " + a_table_name + &
	" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " )"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: updating cr_validations_invalid_ids.valid_online !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("SQLS = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

skip_reset:

kickouts = false

// Call the autoextend function for actuals before running any validations...
//	Only need to call this function if they cannot delete ME values and effective month number
//	validations are turned on.
if a_is_bdg_element = false then
	if i_cv_delete_me = 'NO' and i_effmn_cv = 'YES' then
		rtn = uf_validate_me_autoextend(a_table_name, a_ifb_id)
		if rtn < 1 then
			rollback;
			return -1
		end if
	end if
end if

//
//  LOOP OVER EACH ELEMENT.
//
for i = 1 to i_num_elements
	
	// Initailizing ME specific variables for actuals or budget
	if a_is_bdg_element = false then
		element = upper(i_ds_elements.GetItemString(i, "description"))
		elmnt_type = 'Actuals'
	else
		element = upper(i_ds_elements.GetItemString(i, "budgeting_element"))
		elmnt_type = 'Budget'
	end if

	element_table  = i_ds_elements.GetItemString(i, "element_table")
	element_column = upper(i_ds_elements.GetItemString(i, "element_column"))
	element_id     = i_ds_elements.GetItemNumber(i, "element_id")
	values_by_co   = i_ds_elements.GetItemNumber(i, "values_by_company")
	hide           = i_ds_elements.GetItemNumber(i, "hide")
	if hide = 1 then continue

	element = f_cr_clean_string(element)
	element_column = f_cr_clean_string(element_column)

	exclude_element = 0
	select count(*) into :exclude_element from cr_validation_exclusion
	 where element_id = :element_id
		and (upper(table_name) = upper(:a_table_name) or table_name = '*');
	if isnull(exclude_element) then exclude_element = 0

	if exclude_element = 1 then continue
	if isnull(values_by_co) then values_by_co = 0
		
	// Insert invalid transactions into cr_validations_invalid_ids2
	// Only validate records for the interface batch in question and that meet i_add_where
	validate_sqls_from = &
		" from " + a_table_name + &
			" where interface_batch_id = '" + a_ifb_id + "'  and " + i_add_where + " "
		
	// Join to ME table.
	validate_sqls_from += &
		"and not exists (select 1 from " + element_table + " b " + &
		"where b.element_type = '" + elmnt_type + "' " + &
		'and b."' + element_column + '" = ' + a_table_name + '."' + element + '" '
		
	// Add the company join if it's a values by company
	if values_by_co = 1 then validate_sqls_from += &
		'and b."' + i_co_field + '" = ' + a_table_name + '."' + i_co_field + '" '
	
	// Now build the joins to find the valid transactions.
	if i_cv_delete_me = "YES" then /* No status or eff month */
		// No extra criteria
		validate_sqls_from += ") "
	elseif i_effmn_cv = "YES" then /* Both status and eff month */
		// Add month number and status criteria
		validate_sqls_from += &
			"and (" + a_table_name + ".month_number >= b.effective_month_number and b.validation_status_id = 1 " + &
			"or " + a_table_name + ".month_number < b.effective_month_number and b.validation_status_id = 0)) "
	else /* Has only status */
		validate_sqls_from += &
			"and b.validation_status_id = 1) "
	end if


	// Build SQL to insert into cr_validations_invalid_ids2
	validate_sqls = &
		"insert into cr_validations_invalid_ids2 (table_name, id, msg) " + &
			"select '" + a_table_name + "', id, 'INVALID " + element + "' " + validate_sqls_from
			
	//  Since more than one column could be invalid and currently we cannot insert the
	//  id more than once.
	//  ### JAK: 2014-01-06:  Change the minus to a not exists for performance
	validate_sqls += &
		"and not exists (select 1 from cr_validations_invalid_ids2 c where c.table_name = '" + a_table_name + "' and c.id = " + a_table_name + ".id and c.msg = 'INVALID " + element + "')"
	
	// Build SQL to log messages that are invalid
	validate_sqls_for_msg = &
		'select distinct nvl("' + element + '", ' + "'  ') "
	if values_by_co = 1 then validate_sqls_for_msg += "||'-'||" + 'nvl("' + i_co_field + '", ' + "'  ') "
	validate_sqls_for_msg += validate_sqls_from
		
	// Perform validation.
	execute immediate :validate_sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_ME: inserting into cr_validations_invalid_ids2 !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs(validate_sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	// If kickouts occured message invalid elements to user
	if sqlca.sqlnrows > 0 then
		kickouts = true
		f_pp_msgs("************  INVALID " + element + "(S) ... ")
		i_ds_msg_one_col.reset()
		i_ds_msg_one_col.SetSQLSelect(validate_sqls_for_msg)
		i_ds_msg_one_col.RETRIEVE()
		msg_count = i_ds_msg_one_col.RowCount()
		for m = 1 to msg_count
			msg_val = i_ds_msg_one_col.GetItemString(m, 1)
			f_pp_msgs(msg_val)
		next		
	end if
next  //  for i = 1 to num_rules ...

// If kickouts occured insert invalid MEs to cr_validations_invalid_ids
//  ### JAK: 2014-01-06:  Change the minus to a not exists for performance
if kickouts = true then
	insert into cr_validations_invalid_ids (table_name, id) 
		select distinct table_name, id from cr_validations_invalid_ids2 a
		 where table_name = :a_table_name
		 and not exists (select 1 from cr_validations_invalid_ids b where a.table_name = b.table_name and a.id = b.id);
		
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR IN UF_VALIDATE_ME: inserting into cr_validations_invalid_ids !")
		f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if

// Setting valid_online = 0 for impacted records
// Does this work for cr_journal_lines_stg?
sqls = 'update cr_validations_invalid_ids ' + &
	'set valid_online = 0 ' + &
	'where id in ( ' + &
	'select id from cr_validations_invalid_ids2 ' + &
	"where table_name = '" + a_table_name + "' " + &
	"and id in (select id from " + a_table_name + &
	" where interface_batch_id = '" + a_ifb_id + "' and " + i_add_where + " )) " + &
	"and table_name = '" + a_table_name + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN UF_VALIDATE_COMBOS: updating cr_validations_invalid_ids !")
	f_pp_msgs("ERROR MESSAGE: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

commit;

if kickouts then
	return -2
else
	return 1
end if

end function

on uo_cr_validation.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_validation.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string sqls

//  NEED THE COMPANY FIELD IF VALUES_BY_COMPANY IS TURNED ON FOR AN ACK ELEMENT.
setnull(i_co_field)
i_co_field = upper(trim(g_cr.uf_get_control_value('COMPANY FIELD'))) //Autogenerated sql replace
if isnull(i_co_field) then i_co_field = "NONE"  //  This will make it obvious
	//  that the company field is not set up properly.

// New variable to use the master element autoextend...in interfaces it is on by default; in the app
//	it is off by default.  The autoextend messages are still logged, but the ME table is not actually updated
if g_main_application then 
	i_me_autoextend = false
else
	i_me_autoextend = true
end if

// DMJ: 02/04/10: To Avoid Mem Leaks
i_ds_count = CREATE uo_ds_top
sqls = 'select count(*) count from cr_elements where element_id = -1'
f_create_dynamic_ds(i_ds_count, "grid", sqls, sqlca, true)

i_ds_rules = CREATE uo_ds_top
sqls = "select * from cr_validation_rules"
f_create_dynamic_ds(i_ds_rules, "grid", sqls, sqlca, true)
i_ds_rules.SetSort("rule a")
i_ds_rules.Sort()

i_ds_rules_proj = CREATE uo_ds_top
sqls = "select * from cr_validation_rules_projects where active = 'Y' order by rule_id"
f_create_dynamic_ds(i_ds_rules_proj, "grid", sqls, sqlca, true)
i_ds_rules_proj.SetSort("rule a")
i_ds_rules_proj.Sort()

i_ds_rule = CREATE uo_ds_top
sqls = "select * from cr_validation_rules where -1 = 0"
f_create_dynamic_ds(i_ds_rule, "grid", sqls, sqlca, true)

i_ds_control = CREATE uo_ds_top
sqls = "select * from cr_validation_control where rule_id = 0"
f_create_dynamic_ds(i_ds_control, "grid", sqls, sqlca, true)	

i_ds_elements = CREATE uo_ds_top
sqls = "select * from cr_elements"
f_create_dynamic_ds(i_ds_elements, "grid", sqls, sqlca, true)
i_ds_elements.SetSort("order a")
i_ds_elements.Sort()
i_num_elements = i_ds_elements.RowCount()

i_ds_msg_one_col = create uo_ds_top
sqls = "select rpad(' ',4000) from dual"
f_create_dynamic_ds(i_ds_msg_one_col, "grid", sqls, sqlca, true)

i_ds_msg_three_col = create uo_ds_top
sqls = "select rpad(' ',4000), 0, rpad(' ',4000) from dual"
f_create_dynamic_ds(i_ds_msg_three_col, "grid", sqls, sqlca, true)

i_ds_extend = create uo_ds_top
sqls = 'select * from cr_master_element_autoextend where -1 = 0'
f_create_dynamic_ds(i_ds_extend,"grid",sqls,sqlca,true)

i_ds_invalid_mn = create uo_ds_top
sqls = "select " + i_co_field + ", source_id, month_number from cr_cost_repository where -1 = 0 "
f_create_dynamic_ds(i_ds_invalid_mn,"grid",sqls,sqlca,true)

i_add_where = "amount_type <> 99"

select upper(control_value) 
	into :i_cv_control_or_combos
	from cr_System_control 
	where upper(Control_name) = 'VALIDATIONS - CONTROL OR COMBOS';
	
if i_cv_control_or_combos <> 'CONTROL' or isnull(i_cv_control_or_combos) then i_cv_control_or_combos = 'COMBOS'

//	DML: 080206: What technique do we use?  Includes all references to "i_use_intermediate_tbl" below.
i_use_intermediate_tbl = upper(g_cr.uf_get_control_value('VALIDATIONS - USE INTERMEDIATE TBL')) //Autogenerated sql replace

if isnull(i_use_intermediate_tbl) or i_use_intermediate_tbl <> "YES" then 
	i_use_intermediate_tbl = 'NO'
end if

//	JAK: 20101014: Do we write combo kickouts to the log
i_log_combo_kickouts = upper(g_cr.uf_get_control_value('Validations - Log Combo Kickouts')) //Autogenerated sql replace

if isnull(i_log_combo_kickouts) or i_log_combo_kickouts <> "YES" then 
	i_log_combo_kickouts = 'NO'
end if

// Read as many of the system controls up front as possible...
i_effmn_cv = upper(trim(g_cr.uf_get_control_value('validations: effective month number'))) //Autogenerated sql replace
if isnull(i_effmn_cv) or i_effmn_cv <> "YES" then i_effmn_cv = 'NO'

i_effmn2_cv = upper(trim(g_cr.uf_get_control_value('validation combos: eff mn 2'))) //Autogenerated sql replace
if isnull(i_effmn2_cv) or i_effmn2_cv <> "YES" then i_effmn2_cv = 'NO'

i_effmn2_multi_rec_cv = upper(trim(g_cr.uf_get_control_value('combos: effmn2: multiple records'))) //Autogenerated sql replace
if isnull(i_effmn2_multi_rec_cv) or i_effmn2_multi_rec_cv <> "YES" then i_effmn2_multi_rec_cv = 'NO'

//-------  System switch for the new structure values table.  -------
i_use_new_structures_table = upper(trim(g_cr.uf_get_control_value('USE NEW CR STRUCTURE VALUES TABLE'))) //Autogenerated sql replace
if isnull(i_use_new_structures_table) or i_use_new_structures_table <> 'YES' then i_use_new_structures_table = "NO"

//	CONTROL/COMBO VALIDATION IS CALLED FIRST...ONLY RESET VALID_ONLINE IF 
//	COMBO VALIDATION IS TURNED OFF OR I WILL OVERRIDE ANY VALIDATION THAT 
//	OCCURRED IN UF_VALIDATE_CONTROL OR UF_VALIDAT_COMBOS.
i_cv_combo_vals = upper(trim(g_cr.uf_get_control_value('ENABLE VALIDATIONS - COMBO'))) //Autogenerated sql replace
if isnull(i_cv_combo_vals) or i_cv_combo_vals <> 'YES' then i_cv_combo_vals = "NO"

i_cv_delete_me = upper(trim(g_cr.uf_get_control_value('DELETE MASTER ELEMENTS'))) //Autogenerated sql replace
if isnull(i_cv_delete_me) or i_cv_delete_me <> "YES" then i_cv_delete_me = 'NO'

// JAK:  20090709:  New control to use exists statements for include rules
i_cv_use_exists = upper(trim(g_cr.uf_get_control_value('VALIDATIONS - USE EXISTS CLAUSE'))) //Autogenerated sql replace
if isnull(i_cv_use_exists) or i_cv_use_exists <> "YES" then i_cv_use_exists = 'NO'

//  Are acct range validations enabled ?
i_cv_acct_range = upper(trim(g_cr.uf_get_control_value('enable validations - acct range'))) //Autogenerated sql replace
if isnull(i_cv_acct_range) or i_cv_acct_range <> "YES" then i_cv_acct_range = "NO"

//GET The work order num field
i_wo_field = upper(trim(g_cr.uf_get_control_value('WORK ORDER FIELD'))) //Autogenerated sql replace
if isnull(i_wo_field) or trim(i_wo_field) = '' then i_wo_field = 'NONE'
i_wo_field = f_cr_clean_string(i_wo_field)

//GET The powerplant company field
i_pp_co_field = upper(trim(g_cr.uf_get_control_value('POWERPLANT COMPANY FIELD'))) //Autogenerated sql replace
if isnull(i_pp_co_field) or trim(i_pp_co_field) = '' then i_pp_co_field = 'NONE'
i_pp_co_field = f_cr_clean_string(i_pp_co_field)

//  System switch to understand the default values that are invalid.
i_cv_req_obey_default = upper(g_cr.uf_get_control_value('VALIDATIONS-REQUIRED - OBEY DEFAULT')) //Autogenerated sql replace
if isnull(i_cv_req_obey_default) or i_cv_req_obey_default <> "YES" then i_cv_req_obey_default = 'NO'

i_uo_cr_validation_custom = create uo_cr_validation_custom
end event

