HA$PBExportHeader$uo_cr_cache.sru
forward
global type uo_cr_cache from nonvisualobject
end type
end forward

global type uo_cr_cache from nonvisualobject
end type
global uo_cr_cache uo_cr_cache

type variables
uo_ds_top i_ds_cr_elements
uo_ds_top i_ds_cr_elements_fields
uo_ds_top i_ds_cr_sources
uo_ds_top i_ds_cr_sources_fields
uo_ds_top i_ds_cr_system_control
uo_ds_top i_ds_cr_alloc_system_control
end variables

forward prototypes
public subroutine uf_refresh ()
public subroutine uf_refresh_cr_system_control ()
public subroutine uf_refresh_cr_alloc_system_control ()
public subroutine uf_refresh_cr_elements ()
public subroutine uf_refresh_cr_elements_fields ()
public subroutine uf_refresh_cr_sources ()
public subroutine uf_refresh_cr_sources_fields ()
public function datastore uf_get_cr_elements (string a_filter)
public function datastore uf_get_cr_elements_fields (string a_filter)
public function datastore uf_get_cr_sources (string a_filter)
public function datastore uf_get_cr_sources_fields (string a_filter)
public function longlong uf_get_source_id (string a_column, string a_value)
public function string uf_get_element_description (longlong a_element_id)
public function string uf_get_element_table (longlong a_element_id)
public function string uf_get_element_column (longlong a_element_id)
public function string uf_get_element_budgeting_element (longlong a_element_id)
public function string uf_get_element_default_value (longlong a_element_id)
public function string uf_get_control_value (string a_control_name)
public function longlong uf_get_element_id (string a_column, string a_value)
public function longlong uf_get_element_values_by_company (longlong a_element_id)
public function string uf_get_sources_batch_id_column (longlong a_source_id)
public function string uf_get_sources_description (longlong a_source_id)
public function string uf_get_sources_gl_journal_category (longlong a_source_id)
public function string uf_get_sources_table_name (longlong a_source_id)
public function string uf_get_sources_view_name (longlong a_source_id)
end prototypes

public subroutine uf_refresh ();uf_refresh_cr_elements()
uf_refresh_cr_elements_fields()
uf_refresh_cr_sources()
uf_refresh_cr_sources_fields()
uf_refresh_cr_system_control()
uf_refresh_cr_alloc_system_control() 
end subroutine

public subroutine uf_refresh_cr_system_control ();string sqls

if isvalid(i_ds_cr_system_control) then 
	destroy i_ds_cr_system_control
end if

i_ds_cr_system_control = create uo_ds_top
sqls = "select * from cr_system_control" 
f_create_dynamic_ds(i_ds_cr_system_control, "grid", sqls, sqlca, true)

if sqlca.sqlcode < 0 then
	if g_main_application then
		messagebox('Error Refreshing Cache - cr system control', sqlca.sqlerrtext)
	else
		f_pp_msgs('Error Refreshing Cache - cr system control')
	end if
end if
end subroutine

public subroutine uf_refresh_cr_alloc_system_control ();string sqls

if isvalid(i_ds_cr_alloc_system_control) then 
	destroy i_ds_cr_alloc_system_control
end if

i_ds_cr_alloc_system_control = create uo_ds_top
sqls = "select * from cr_alloc_system_control" 
f_create_dynamic_ds(i_ds_cr_alloc_system_control, "grid", sqls, sqlca, true)

if sqlca.sqlcode < 0 then
	if g_main_application then
		messagebox('Error Refreshing Cache - cr alloc system control', sqlca.sqlerrtext)
	else
		f_pp_msgs('Error Refreshing Cache - cr alloc system control')
	end if
end if
end subroutine

public subroutine uf_refresh_cr_elements ();string sqls

if isvalid(i_ds_cr_elements) then 
	destroy i_ds_cr_elements
end if

i_ds_cr_elements = create uo_ds_top
sqls = "select * from cr_elements" 
f_create_dynamic_ds(i_ds_cr_elements, "grid", sqls, sqlca, true)

if sqlca.sqlcode < 0 then
	if g_main_application then
		messagebox('Error Refreshing Cache - elements', sqlca.sqlerrtext)
	else
		f_pp_msgs('Error Refreshing Cache - elements')
	end if
end if
end subroutine

public subroutine uf_refresh_cr_elements_fields ();string sqls

if isvalid(i_ds_cr_elements_fields) then 
	destroy i_ds_cr_elements_fields
end if

i_ds_cr_elements_fields = create uo_ds_top
sqls = "select * from cr_elements_fields" 
f_create_dynamic_ds(i_ds_cr_elements_fields, "grid", sqls, sqlca, true)

if sqlca.sqlcode < 0 then
	if g_main_application then
		messagebox('Error Refreshing Cache - elements fields', sqlca.sqlerrtext)
	else
		f_pp_msgs('Error Refreshing Cache - elements fields')
	end if
end if
end subroutine

public subroutine uf_refresh_cr_sources ();string sqls

if isvalid(i_ds_cr_sources) then 
	destroy i_ds_cr_sources
end if

i_ds_cr_sources = create uo_ds_top
sqls = "select * from cr_sources" 
f_create_dynamic_ds(i_ds_cr_sources, "grid", sqls, sqlca, true)

if sqlca.sqlcode < 0 then
	if g_main_application then
		messagebox('Error Refreshing Cache - sources', sqlca.sqlerrtext)
	else
		f_pp_msgs('Error Refreshing Cache - sources')
	end if
end if
end subroutine

public subroutine uf_refresh_cr_sources_fields ();string sqls

if isvalid(i_ds_cr_sources_fields) then 
	destroy i_ds_cr_sources_fields
end if

i_ds_cr_sources_fields = create uo_ds_top
sqls = "select * from cr_sources_fields" 
f_create_dynamic_ds(i_ds_cr_sources_fields, "grid", sqls, sqlca, true)

if sqlca.sqlcode < 0 then
	if g_main_application then
		messagebox('Error Refreshing Cache - sources fields', sqlca.sqlerrtext)
	else
		f_pp_msgs('Error Refreshing Cache - sources fields')
	end if
end if
end subroutine

public function datastore uf_get_cr_elements (string a_filter);longlong rtn

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()

rtn = i_ds_cr_elements.setfilter( a_filter )
rtn = min(i_ds_cr_elements.filter( ), rtn)

if rtn < 0 then
	if g_main_application then
		messagebox("Error", "Error applying filter when retrieving cr cache - elements.")
	else
		f_pp_msgs( "Error applying filter when retrieving cr cache - elements.")
	end if
end if

return i_ds_cr_elements
end function

public function datastore uf_get_cr_elements_fields (string a_filter);longlong rtn

if not isvalid(i_ds_cr_elements_fields) then uf_refresh_cr_elements_fields()

rtn = i_ds_cr_elements_fields.setfilter( a_filter )
rtn = min(i_ds_cr_elements_fields.filter( ), rtn)

if rtn < 0 then
	if g_main_application then
		messagebox("Error", "Error applying filter when retrieving cr cache - elements fields.")
	else
		f_pp_msgs( "Error applying filter when retrieving cr cache - elements fields.")
	end if
end if

return i_ds_cr_elements_fields
end function

public function datastore uf_get_cr_sources (string a_filter);longlong rtn

if not isvalid(i_ds_cr_sources) then uf_refresh_cr_sources()

rtn = i_ds_cr_sources.setfilter( a_filter )
rtn = min(i_ds_cr_sources.filter( ), rtn)

if rtn < 0 then
	if g_main_application then
		messagebox("Error", "Error applying filter when retrieving cr cache - sources.")
	else
		f_pp_msgs( "Error applying filter when retrieving cr cache - sources.")
	end if
end if

return i_ds_cr_sources
end function

public function datastore uf_get_cr_sources_fields (string a_filter);longlong rtn

if not isvalid(i_ds_cr_sources_fields) then uf_refresh_cr_sources_fields()

rtn = i_ds_cr_sources_fields.setfilter( a_filter )
rtn = min(i_ds_cr_sources_fields.filter( ), rtn)

if rtn < 0 then
	if g_main_application then
		messagebox("Error", "Error applying filter when retrieving cr cache - sources fields.")
	else
		f_pp_msgs( "Error applying filter when retrieving cr cache - sources fields.")
	end if
end if

return i_ds_cr_sources_fields
end function

public function longlong uf_get_source_id (string a_column, string a_value);longlong row

if not isvalid(i_ds_cr_sources) then uf_refresh_cr_sources()

a_value = upper(trim(a_value))

row = i_ds_cr_sources.find( 'upper(trim('+a_column+')) = "'+a_value+'"', 0, i_ds_cr_sources.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - sources.')
	else
		f_pp_msgs('Error Accessing CR Cache - sources.')
	end if
	return -1
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified '+a_column+' was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified '+a_column+' was not found.')
	end if
	
	// Maybe a_value has CRB_, CRA_, _STG1, etc.
	if (mid(a_value,1,2) = 'CR' and mid(a_value,4,1) = '_') or mid(a_value,len(a_value)-4,4) = '_STG' or mid(a_value,len(a_value)-3,4) = '_STG' then 
		// Something like 'CRx_...'.  Change to 'CR_...' or Something like '..._STGx'.  Strip it off.
		
		// CRx_ 
		if (mid(a_value,1,2) = 'CR' and mid(a_value,4,1) = '_') then a_value = 'CR_' + mid(a_value,5)
		
		// _STGx 
		if mid(a_value,len(a_value)-4,4) = '_STG' then	a_value = mid(a_value,1,len(a_value) - 5)
		
		// _STG
		if mid(a_value,len(a_value)-3,4) = '_STG' then a_value = mid(a_value,1,len(a_value) - 4)
		
		return uf_get_source_id(a_column,a_value)
	else
		setnull(row)
		return row
	end if
else
	return i_ds_cr_sources.getitemnumber( row, 'source_id')
end if
end function

public function string uf_get_element_description (longlong a_element_id);longlong row

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()
if isnull(a_element_id) then a_element_id = -987

row = i_ds_cr_elements.find( 'element_id = '+string(a_element_id), 0, i_ds_cr_elements.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - elements.')
	else
		f_pp_msgs('Error Accessing CR Cache - elements.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified element id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified element id was not found.')
	end if
	return ''
else
	return i_ds_cr_elements.getitemstring( row, 'description')
end if
end function

public function string uf_get_element_table (longlong a_element_id);longlong row

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()
if isnull(a_element_id) then a_element_id = -987

row = i_ds_cr_elements.find( 'element_id = '+string(a_element_id), 0, i_ds_cr_elements.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - elements.')
	else
		f_pp_msgs('Error Accessing CR Cache - elements.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified element id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified element id was not found.')
	end if
	return ''
else
	return i_ds_cr_elements.getitemstring( row, 'element_table')
end if
end function

public function string uf_get_element_column (longlong a_element_id);longlong row

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()
if isnull(a_element_id) then a_element_id = -987

row = i_ds_cr_elements.find( 'element_id = '+string(a_element_id), 0, i_ds_cr_elements.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - elements.')
	else
		f_pp_msgs('Error Accessing CR Cache - elements.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified element id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified element id was not found.')
	end if
	return ''
else
	return i_ds_cr_elements.getitemstring( row, 'element_column')
end if
end function

public function string uf_get_element_budgeting_element (longlong a_element_id);longlong row

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()
if isnull(a_element_id) then a_element_id = -987

row = i_ds_cr_elements.find( 'element_id = '+string(a_element_id), 0, i_ds_cr_elements.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - elements.')
	else
		f_pp_msgs('Error Accessing CR Cache - elements.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified element id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified element id was not found.')
	end if
	return ''
else
	return i_ds_cr_elements.getitemstring( row, 'budgeting_element')
end if
end function

public function string uf_get_element_default_value (longlong a_element_id);longlong row

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()
if isnull(a_element_id) then a_element_id = -987

row = i_ds_cr_elements.find( 'element_id = '+string(a_element_id), 0, i_ds_cr_elements.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - elements.')
	else
		f_pp_msgs('Error Accessing CR Cache - elements.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified element id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified element id was not found.')
	end if
	return ''
else
	return i_ds_cr_elements.getitemstring( row, 'default_value')
end if
end function

public function string uf_get_control_value (string a_control_name);longlong row
string null_string

setnull(null_string)
if not isvalid(i_ds_cr_system_control) then uf_refresh_cr_system_control()
if not isvalid(i_ds_cr_alloc_system_control) then uf_refresh_cr_alloc_system_control()

row = i_ds_cr_system_control.find( 'trim(lower(control_name)) = "'+trim(lower(a_control_name))+'"', 0, i_ds_cr_system_control.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache.')
	else
		f_pp_msgs('Error Accessing CR Cache.')
	end if
	return null_string
elseif row = 0 then
	row = i_ds_cr_alloc_system_control.find( 'trim(lower(control_name)) = "'+trim(lower(a_control_name))+'"', 0, i_ds_cr_alloc_system_control.rowcount( ) )
	if row < 0 then
		if g_main_application then
			MessageBox('Error','Problem with the CR cache.')
		else
			f_pp_msgs('Error Accessing CR Cache.')
		end if
		return null_string
	elseif row = 0 then
		if g_main_application then
			//MessageBox('Error','The specified control name was not found.')
		else
			//f_pp_msgs('Error Accessing CR Cache: The specified control name was not found.')
		end if
		return null_string
	else
		return i_ds_cr_alloc_system_control.getitemstring( row, 'Control_Value')
	end if
else
	return i_ds_cr_system_control.getitemstring( row, 'Control_Value')
end if
end function

public function longlong uf_get_element_id (string a_column, string a_value);longlong row

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()

row = i_ds_cr_elements.find( 'upper(trim('+a_column+')) = upper(trim("'+a_value+'"))', 0, i_ds_cr_elements.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - elements.')
	else
		f_pp_msgs('Error Accessing CR Cache - elements.')
	end if
	return -1
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified '+a_column+' was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified '+a_column+' was not found.')
	end if
	setnull(row)
	return row
else
	return i_ds_cr_elements.getitemnumber( row, 'element_id')
end if
end function

public function longlong uf_get_element_values_by_company (longlong a_element_id);longlong row

if not isvalid(i_ds_cr_elements) then uf_refresh_cr_elements()
if isnull(a_element_id) then a_element_id = -987

row = i_ds_cr_elements.find( 'element_id = '+string(a_element_id), 0, i_ds_cr_elements.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - elements.')
	else
		f_pp_msgs('Error Accessing CR Cache - elements.')
	end if
	return 0
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified element id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified element id was not found.')
	end if
	return 0
else
	return i_ds_cr_elements.getitemnumber( row, 'values_by_company')
end if
end function

public function string uf_get_sources_batch_id_column (longlong a_source_id);longlong row

if not isvalid(i_ds_cr_sources) then uf_refresh_cr_sources()

row = i_ds_cr_sources.find( 'source_id = '+string(a_source_id), 0, i_ds_cr_sources.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - sources.')
	else
		f_pp_msgs('Error Accessing CR Cache - sources.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified source id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified source id was not found.')
	end if
	return ''
else
	return i_ds_cr_sources.getitemstring( row, 'batch_id_column')
end if
end function

public function string uf_get_sources_description (longlong a_source_id);longlong row

if not isvalid(i_ds_cr_sources) then uf_refresh_cr_sources()
if isnull(a_source_id) then a_source_id = -987

row = i_ds_cr_sources.find( 'source_id = '+string(a_source_id), 0, i_ds_cr_sources.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - sources.')
	else
		f_pp_msgs('Error Accessing CR Cache - sources.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified source id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified source id was not found.')
	end if
	return ''
else
	return i_ds_cr_sources.getitemstring( row, 'description')
end if
end function

public function string uf_get_sources_gl_journal_category (longlong a_source_id);longlong row

if not isvalid(i_ds_cr_sources) then uf_refresh_cr_sources()
if isnull(a_source_id) then a_source_id = -987

row = i_ds_cr_sources.find( 'source_id = '+string(a_source_id), 0, i_ds_cr_sources.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - sources.')
	else
		f_pp_msgs('Error Accessing CR Cache - sources.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified source id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified source id was not found.')
	end if
	return ''
else
	return i_ds_cr_sources.getitemstring( row, 'gl_journal_category')
end if
end function

public function string uf_get_sources_table_name (longlong a_source_id);longlong row

if not isvalid(i_ds_cr_sources) then uf_refresh_cr_sources()
if isnull(a_source_id) then a_source_id = -987

row = i_ds_cr_sources.find( 'source_id = '+string(a_source_id), 0, i_ds_cr_sources.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - sources.')
	else
		f_pp_msgs('Error Accessing CR Cache - sources.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified source id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified source id was not found.')
	end if
	return ''
else
	return i_ds_cr_sources.getitemstring( row, 'table_name')
end if
end function

public function string uf_get_sources_view_name (longlong a_source_id);longlong row

if not isvalid(i_ds_cr_sources) then uf_refresh_cr_sources()
if isnull(a_source_id) then a_source_id = -987

row = i_ds_cr_sources.find( 'source_id = '+string(a_source_id), 0, i_ds_cr_sources.rowcount( ) )
if row < 0 then
	if g_main_application then
		MessageBox('Error','Problem with the CR cache - sources.')
	else
		f_pp_msgs('Error Accessing CR Cache - sources.')
	end if
	return ''
elseif row = 0 then
	if g_main_application then
		//MessageBox('Error','The specified source id was not found.')
	else
		//f_pp_msgs('Error Accessing CR Cache: The specified source id was not found.')
	end if
	return ''
else
	return i_ds_cr_sources.getitemstring( row, 'view_name')
end if
end function

on uo_cr_cache.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_cache.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

