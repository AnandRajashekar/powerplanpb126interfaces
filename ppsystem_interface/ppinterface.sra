HA$PBExportHeader$ppinterface.sra
$PBExportComments$Standard Interface Object
forward
global type ppinterface from application
end type
global uo_sqlca sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
longlong g_process_id, g_occurrence_id, g_msg_order, g_rtn_code, g_interface_id, &
		   g_source_id, g_record_count, g_mn_to_process
string   g_log_file, g_batch_id, g_balancing_timestamp, g_command_line_args, g_debug
boolean  g_do_not_write_batch_id
datetime g_finished_at
u_prog_interface g_prog_interface
uo_sqlca_logs    g_sqlca_logs
uo_mail g_msmail
u_external_function uo_winapi
dec{2} g_amount, g_total_dollars, g_total_credits, g_total_debits

////	////	added for uo_log
boolean	g_of_log_add_time, g_of_log_use_ppmsg, g_of_log_use_writefile

////	////	added for uo_check_sql
boolean g_of_check_sql_write_success
boolean g_main_application

// Use this variable to be the OS return value in case we encounter an error 
// before we can connect to the database and establish the online logs
longlong g_rtn_failure = -1 

uo_client_interface g_uo_client
uo_ppinterface g_uo_ppint
boolean g_db_connected, g_db_logs_connected

nvo_runtimeerror g_rte
nvo_runtimeerror_app g_rte_app

nvo_stats g_stats


s_user_info                s_user_info
s_sys_info				s_sys_info
nvo_func_string		g_string_func
nvo_func_database	g_db_func
nvo_func_datastore	g_ds_func
nvo_pp_func_io			g_io_func
uo_messagebox                          g_msg  //Added by appeon (8/15/2012  )
uo_ds_top g_ds_messagebox_translate
s_file_info				g_exe_info   // ### 29892: JAK: global variable with exe attributes

// ### 29892: JAK: server side processing
boolean								g_ssp
string									g_ssp_user, g_ssp_process, g_ssp_return
longlong	g_ssp_process_identifier
s_ppbase_parm_arrays   			g_ssp_parms
s_ppbase_parm_arrays_labels	g_ssp_labels
nvo_server_side_request			g_ssp_nvo
uo_system_cache					g_cache
uo_cr_cache 						g_cr

// autogen je accounts
uo_ds_top  g_ds_cr_element_definitions
uo_autogen_je_account g_uo_autogen_je_account

//// Holder for PP Constants
nvo_pp_constants		g_pp_constants


string g_start
end variables

global type ppinterface from application
string appname = "ppinterface"
end type
global ppinterface ppinterface

type prototypes
Function uLong FindFirstFile(string lpFileName, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindFirstFileA;Ansi"
Function uLong FindNextFile(ulong hFindFile, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindNextFileA;Ansi"
Function uLong FindClose(ulong hFindFile) Library "kernel32.dll"

// To get environment variables
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long GetLastError() Library "kernel32.dll"

// addiitonal functions for system and user info
function long GetVersionExA( REF s_osversioninfoex OS) library "kernel32.dll" alias for "GetVersionExA;Ansi"
function long WNetGetUserA(long ptr,ref string user,ref long len) LIBRARY "mpr.dll" alias for "WNetGetUserA;Ansi"
function long gethostname(ref string host,long len) library "Ws2_32.dll" alias for "gethostname;Ansi"
function int WSAGetLastError ( ) library "Ws2_32.dll"
function int WSAStartup( uint UIVersionRequested, ref s_WSAData lpWSAData )library "Ws2_32.dll"

// to get UNC file names
//Function long WNetGetUniversalName(string lpLocalPath,long dwInfoLevel,ref s_universal_name_info lpBuffer,ref long lpBufferSize)   Library "mpr.DLL" alias for "WNetGetUniversalNameA;Ansi"
Function long WNetGetConnection(string lpLocalName, ref string lpRemoteName, ref long lpnLength)   Library "mpr.DLL" alias for "WNetGetConnectionA;Ansi"


end prototypes

forward prototypes
public subroutine uf_synch_rte ()
end prototypes

public subroutine uf_synch_rte ();g_rte.setmessage(g_rte_app.text)
g_rte.i_args							=	g_rte_app.i_args
g_rte.i_args_null					=	g_rte_app.i_args_null
g_rte.i_sqlca_sqlerrtext			=	g_rte_app.i_sqlca_sqlerrtext
g_rte.i_pp_error_code			=	g_rte_app.i_pp_error_code
g_rte.i_sqlca_sqlcode				=	g_rte_app.i_sqlca_sqlcode
g_rte.i_sqlca_sqldbcode			=	g_rte_app.i_sqlca_sqldbcode
g_rte.i_sqlca_sqlnrows			=	g_rte_app.i_sqlca_sqlnrows
g_rte.i_rtn							=	g_rte_app.i_rtn
// No need to synch validation messages -- they are logged seperately

end subroutine

on ppinterface.create
appname="ppinterface"
message=create message
sqlca=create uo_sqlca
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on ppinterface.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;//*****************************************************************************************
//
// Put all interface code inside of a try-catch block
//
// Interfaces should be batch jobs with no visual components or user interaction.
//
// The CATCH block at the bottom of this script will trap any exceptions or 
// runtime errors.  This will prevent PowerBuilder's "popup" messages from 
// displaying on the screen. 
//
// This means YOU CAN NOT USE GOTO/LABEL STATEMENTS inside of this code!!!
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong i, rtn

g_start = string(today(),"yyyymmdd_hhmmss")

TRY 
		
	// create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	g_cache = create uo_system_cache				
	g_cr = create uo_cr_cache	
	
	g_pp_constants = create nvo_pp_constants
	
	s_sys_info.dbms = 'oracle'
	
	//*****************************************************************************************
	//
	//  Create database connections, handle versioning, etc
	//
	//*****************************************************************************************	
	g_rtn_code = g_uo_ppint.uf_connect()

	if g_rtn_code > 0 then
		//*****************************************************************************************
		//
		//  Run the interface code
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Specific Code ********************')
		f_pp_msgs(' ')
		
		g_rtn_code = g_uo_client.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Specific Code ********************')
		f_pp_msgs(' ')
	end if

// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
	if isnull(nvo_e.i_rtn) then
		uf_synch_rte()
	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + nvo_e.i_sqlca_sqlerrtext
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_last_sql) then exception_msg += '~r~n   SQL: '         + nvo_e.i_sqlca_last_sql
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

// Log any validation messages that weren't logged yet.  If there are kickouts, we should return -2
rtn = g_rte.msg_val_log()
if rtn = -2 and g_rtn_code >= 0 then g_rtn_code = -2
rtn = g_rte_app.msg_val_log()
if rtn = -2 and g_rtn_code >= 0 then g_rtn_code = -2

// use the exception message to determine if we actually had an unhandled exception
if len(exception_msg) > 0 then 	
	g_rtn_code = g_rtn_failure
	
	// write the unhandled exception to the log file
	f_write_log(g_log_file, exception_msg)
	
	// if we never got connected to the database, write to log file and set the error code
	if g_db_connected then 
		f_pp_msgs(exception_msg)
	end if
end if

halt close
end event

event close;//***********************************************************************************
// You can immediately end the interface (and invoke this code) by 
// using the following statement in any PB script: 
//
//     halt close
//    
// Before issuing this statement you must have already:
//
//     1. Connected to the database using both sqlca & g_sqlca_logs.
//        This is generally taken care of in the application open event, but if
//        you are not connected errors may result in this script.
//
//     2. Set the global variable g_rtn_code.  The value in this variable should
//        correspond to a return_value in pp_processes_return_values table for
//        the current process_id.  If the os_return_value column is populated for that 
//        row, its value will be returned to the operating system or scheduler that ran 
//        the interface.  If the os_return_value is null, g_rtn_code will be returned. 
//**********************************************************************************
longlong rtn

//*****************************************************************************************
//
//  Disconnect and end
//
//*****************************************************************************************	
rtn = g_uo_ppint.uf_disconnect()

// ### 7598: JAK: 2011-05-17:  Don't ExitProcess if running from source.
if not g_uo_ppint.i_running_from_source then
	g_prog_interface.uf_exit(rtn)
end if

end event

