HA$PBExportHeader$nvo_web_crypto.sru
forward
global type nvo_web_crypto from nonvisualobject
end type
end forward

global type nvo_web_crypto from nonvisualobject
end type
global nvo_web_crypto nvo_web_crypto

type prototypes
function boolean CryptAcquireContextA (ref ulong hProv, ref string pszContainer, ref string pszProvider, ulong dwProvType, ulong dwFlags) LIBRARY "C:\Windows\System32\advapi32.dll"

function boolean CryptReleaseContext (ulong hProv, ulong dwFlags) LIBRARY "C:\Windows\System32\advapi32.dll"

function boolean CryptCreateHash (ulong hProv, uint Algid, ulong hKey, ulong dwFlags, ref ulong phHash) LIBRARY "C:\Windows\System32\advapi32.dll"

function boolean CryptHashData (ulong hHash,  ref string pbData, ulong dwDataLen, ulong dwFlags) LIBRARY "C:\Windows\System32\advapi32.dll"

function boolean CryptDestroyHash (ulong hHash) LIBRARY "C:\Windows\System32\advapi32.dll"

function boolean CryptGetHashParam (ulong hHash, ulong dwParam, ref blob pbData, ref ulong pdwDataLen, ulong dwFlags) LIBRARY "C:\Windows\System32\advapi32.dll"



function boolean CryptCreateKeyIdentifierFromCSP (ulong dwCertEncodingType, &
																	ulong pszPubKeyOID, &
																	ulong pPubKeyStruc, &
																	ulong cbPubKeyStruc, &
																	ulong dwFlags, &
																	ulong pvReserved, &
																	ref ulong pbHash, &
																	ref ulong pcbHash) library "C:\Windows\System32\crypt32.dll"

function boolean CryptEncrypt (	ulong hKey,&
											ulong hHash,&
											boolean Final,&
											ulong dwFlags,&
											ref blob pbData,&
											ref ulong pdwDataLen,& 
											ulong dwBufLen) library "C:\Windows\System32\advapi32.dll"
											
function boolean CryptDeriveKey (	ulong hProv,&
												ulong Algid,&
												ulong hBaseData,&
												ulong dwFlags,&
												ref ulong phKey) library "C:\Windows\System32\advapi32.dll"
												
function boolean CryptGenKey (ulong hProv,&
										uint Algid,&
										ulong dwFlags,&
										ref ulong phKey) library "C:\Windows\System32\advapi32.dll"
										
function ulong EncryptAES128 (	ref string aMsg,&
											ref string aSalt,&
											ref string aResult) library "PPCrypto.dll"


//Error handling
function Ulong GetLastError () Library "C:\Windows\System32\kernel32.dll"
end prototypes

type variables
//Algorithm IDS
private constant uint CALG_AES = 26129 //0x6611
private constant uint CALG_AES_128 = 26126 //0x660e
private constant uint CALG_AES_192 = 26127 //0x660f
private constant uint CALG_AES_256 = 26128 //0x6610

private constant uint CALG_MD2 = 32769//0x8001
private constant uint CALG_MD4 = 32770//0x8002
private constant uint CALG_MD5 = 32771//0x8003
private constant uint CALG_MAC = 32773//0x8005

private constant uint CALG_RC2 = 26114//0x00006602
private constant uint CALG_RC4 = 26625//0x00006801
private constant uint CALG_RC5 = 26125//0x0000660d

//Cryptographic Service Providers
private constant ulong PROV_RSA_FULL = 1
private constant ulong PROV_RSA_SIG = 2
private constant ulong PROV_RSA_SCHANNEL = 12
private constant ulong PROV_RSA_AES = 24


private constant ulong HP_HASHVAL = 2
private constant ulong CRYPT_VERIFYCONTEXT  =  4026531840 // 0xF0000000

private constant ulong X509_ASN_ENCODING = 1 			//0x00000001
private constant ulong PKCS_7_ASN_ENCODING = 65536 	//0x00010000 
end variables

forward prototypes
public function string uf_aes_128 (string a_message, string a_salt)
private function string uf_encrypt (string a_message, string a_salt, unsignedinteger a_method)
public function string uf_get_credentials ()
public subroutine uf_get_web_headers (ref string a_headers[])
private function string uf_encrypt_decrypt (string a_message, string a_salt, unsignedinteger a_method, boolean a_encrypt)
private function blob uf_hex_to_blob (string a_msg_hex)
private function string uf_decrypt (string a_message, string a_salt, unsignedinteger a_method)
public function string uf_rc2_decrypt (string a_message, string a_salt)
public function string uf_aes_128_decrypt (string a_message, string a_salt)
private subroutine uf_example_code ()
public function string uf_rc2_encrypt (string a_message, string a_salt)
private function string uf_blob_to_hex (blob a_msg_blob, longlong a_msg_len)
private function string uf_rc2 (string a_message, string a_salt, boolean a_encrypt)
end prototypes

public function string uf_aes_128 (string a_message, string a_salt);return uf_encrypt(a_message, a_salt, CALG_AES_128)
end function

private function string uf_encrypt (string a_message, string a_salt, unsignedinteger a_method);return uf_encrypt_decrypt(a_message, a_salt, a_method, true)
end function

public function string uf_get_credentials ();return uf_rc2_encrypt(sqlca.LogId + ":" + sqlca.LogPass, "P3pb3R?")
end function

public subroutine uf_get_web_headers (ref string a_headers[]);a_headers[upperbound(a_headers)+1] = "Pragma~tno-cache"
a_headers[upperbound(a_headers)+1] = "PPUNP~t" + uf_get_credentials()
a_headers[upperbound(a_headers)+1] = "HideShell~tYes"
a_headers[upperbound(a_headers)+1] = "Program~tPowerPlan"
a_headers[upperbound(a_headers)+1] = "Component~tRegulatory"
a_headers[upperbound(a_headers)+1] = "Authentication~tDIRECT-DB"
end subroutine

private function string uf_encrypt_decrypt (string a_message, string a_salt, unsignedinteger a_method, boolean a_encrypt);/*******************************************************************
*
*	Description: Encrypts a message with the given salt and method id
*	
*	Why is this private? It hasn't been tested with all encryption method ID's, 
*								so outside objects must use uf_aes_128, uf_rc2, etc...
*								More functions will be added as more methods are supported.
*
*********************************************************************/

ulong h_prov // provider handle
ulong h_hash // hash object handle
ulong h_key
ulong err_number
string result, s_null
ulong msg_len
ulong final
ulong dw_flags
ulong dw_buff_len
blob msg_blob, salty_blob

SetNull (s_null)

//Get handle to the crypto provider
if uo_winapi.uf_CryptAcquireContextA(h_prov, s_null, s_null, PROV_RSA_AES, CRYPT_VERIFYCONTEXT) = 0 then
	err_number = uo_winapi.uf_GetLastError()
	return 'acquire context failed ' + String (err_number)
end if

if isNull(a_salt) or a_salt = "" then
	// Generate the Key --CryptGenKey (NO SALT)
	if uo_winapi.uf_CryptGenKey(h_prov, a_method, 0, h_key) = 0 then
		err_number = uo_winapi.uf_GetLastError()
		uo_winapi.uf_CryptReleaseContext(h_prov, 0)
		return 'generate key failed ' + String (err_number)
	end if
else
	//Create a hash
	if uo_winapi.uf_CryptCreateHash(h_prov, CALG_MD5, 0, 0, h_hash) = 0 then
		err_number = uo_winapi.uf_GetLastError()
		uo_winapi.uf_CryptReleaseContext(h_prov, 0)
		uo_winapi.uf_CryptDestroyHash(h_hash)
		return 'create hash failed ' + String (err_number)
	end if
	
	//Salt the hash
	if uo_winapi.uf_CryptHashData(h_hash, a_salt, len(a_salt), 0) = 0 then
		err_number = uo_winapi.uf_GetLastError()
		uo_winapi.uf_CryptReleaseContext(h_prov, 0)
		uo_winapi.uf_CryptDestroyHash(h_hash)
		return 'hash data failed ' + String (err_number)
	end if
	
	// Generate the Key using salted hash --CryptDeriveKey (Yes SALT)
	salty_blob = blob(a_salt)
	if uo_winapi.uf_CryptDeriveKey(h_prov, a_method, h_hash, 0, h_key) = 0 then
		err_number = uo_winapi.uf_GetLastError()
		uo_winapi.uf_CryptReleaseContext(h_prov, 0)
		uo_winapi.uf_CryptDestroyHash(h_hash)
		return 'derive key failed ' + String (err_number)
	end if
end if

// Encrypt Data --CryptEncrypt
h_hash = 0
final = 1
dw_flags = 0

If a_Encrypt Then
	// Encrypt data.
	
	// Pad the message until it is divisible by 8
	do while mod(len(a_message),8) <> 0
		a_message += " "
	loop
	
	// Using unicode, so msg_len = message length * 2 bytes/char
	msg_len = len(a_message) * 2
	
	// Create a blob that is too big, then write the message to it
	dw_buff_len = 128 * (ceiling(msg_len/128) + 1)
	msg_blob = blob(space(dw_buff_len))
	blobedit(msg_blob, 1, a_message)
	
	
	// Encrypt.  msg_blob and msg_len will be altered by this function.
	if uo_winapi.uf_CryptEncrypt(h_key, h_hash, final, dw_flags, msg_blob, msg_len, dw_buff_len) = 0 then
		err_number = uo_winapi.uf_GetLastError()
		uo_winapi.uf_CryptReleaseContext(h_prov, 0)
		uo_winapi.uf_CryptDestroyHash(h_hash)
		return 'crypt encrypt failed ' + String (err_number)
	end if
	
	//// clean up and return
	uo_winapi.uf_CryptReleaseContext(h_prov, 0)
	uo_winapi.uf_CryptDestroyHash(h_hash)
	uo_winapi.uf_CryptDestroyKey(h_key)
	
	// Not sure why there are 8 extra bytes in the msg_len, but this just works
	result = uf_blob_to_hex(msg_blob, (msg_len - 8))
	
	return result
else
	// Convert the hex string, a_message, to a blob.
	msg_blob = uf_hex_to_blob(a_message)
	msg_len = len(msg_blob)
	
	// Decrypt data.
	if uo_winapi.uf_CryptDecrypt(h_key, 0, 1, 0, msg_blob, msg_len) = 0 then
		// Appears to throw false errors frequently.  Just commenting out errors.  Will silently fail this piece.
	//		err_number = uo_winapi.uf_GetLastError()
	//		if err_number <> 0 then
	//			uo_winapi.uf_CryptReleaseContext(h_prov, 0)
	//			uo_winapi.uf_CryptDestroyHash(h_hash)
	//			return 'crypt decrypt failed ' + String (err_number) + " (Return was: " + string(msg_blob) + ")"
	//		end if
	end if
	
	// Cleanup.
	uo_winapi.uf_CryptReleaseContext(h_prov, 0)
	uo_winapi.uf_CryptDestroyHash(h_hash)
	uo_winapi.uf_CryptDestroyKey(h_key)
	
	
	// Get the message length (the first 3 digits)
	result = trim(string(msg_blob))
	
	return result
end if
end function

private function blob uf_hex_to_blob (string a_msg_hex);blob msg_blob
string chars_hex_arr[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"}
string chars_hex1, chars_hex2
longlong i, j, length, num_bytes
byte b[]
int v1, v2

length = len(a_msg_hex)
num_bytes = length / 2

for i = 1 to num_bytes
	chars_hex1 = mid(a_msg_hex,i * 2 - 1,1)
	chars_hex2 = mid(a_msg_hex,i * 2,1)
	
	v1 = -1
	v2 = -1
	for j = 1 to 16
		if chars_hex_arr[j] = chars_hex1 then v1 = j - 1
		if chars_hex_arr[j] = chars_hex2 then v2 = j - 1
		if v1 >= 0 and v2 >= 0 then exit
	next
	
	b[i] = v1 * 16 + v2
next

msg_blob = Blob(b)

return msg_blob 
end function

private function string uf_decrypt (string a_message, string a_salt, unsignedinteger a_method);return uf_encrypt_decrypt(a_message, a_salt, a_method, false)
end function

public function string uf_rc2_decrypt (string a_message, string a_salt);return uf_decrypt(a_message, a_salt, CALG_RC2)
end function

public function string uf_aes_128_decrypt (string a_message, string a_salt);return uf_decrypt(a_message, a_salt, CALG_AES_128)
end function

private subroutine uf_example_code ();/*
 External Function Definitions
  FUNCTION Boolean CryptAcquireContextA (ref ulong hProv, &
      ref string pszContainer, &
      ref string pszProvider, ulong dwProvType, &
      ulong dwFlags) &
    LIBRARY "advapi32.dll"

 FUNCTION Boolean CryptReleaseContext (ulong hProv, ulong dwFlags) &
    LIBRARY "advapi32.dll"

 FUNCTION Boolean CryptCreateHash (ulong hProv, uint Algid, ulong hKey, &
      ulong dwFlags, ref ulong phHash) &
    LIBRARY "advapi32.dll"

 FUNCTION Boolean CryptHashData (ulong hHash,  ref string pbData, &
      ulong dwDataLen, ulong dwFlags) &
    LIBRARY "advapi32.dll"

 FUNCTION Boolean CryptDestroyHash (ulong hHash) &
    LIBRARY "advapi32.dll"

 FUNCTION Boolean CryptGetHashParam (ulong hHash, ulong dwParam, &
     ref blob pbData, &
     ref ulong pdwDataLen, ulong dwFlags) &
    LIBRARY "advapi32.dll"

 FUNCTION Ulong GetLastError () Library "kernel32.dll"
*/

/*
// Constants
CONSTANT ULONG PROV_RSA_FULL = 1
CONSTANT ULONG CRYPT_VERIFYCONTEXT  =  4026531840 // 0xF0000000
CONSTANT ULONG CALG_MD5 = 32771 // 4<<13 | 0 | 3
CONSTANT ULONG HP_HASHVAL = 2 //  0x0002


public function string of_md5 (string as_text);
  // Calculate the MD5 message digest hash of a string
  // Using the Windows Crypto API

  ulong MD5LEN = 16
  ulong hProv // provider handle
  ulong hHash // hash object handle
  ulong err_number
  String s_result, s_null
  Integer i, l, r, b
  Blob{16} bl_hash
  Blob{1} bl_byte

  SetNull (s_null)
  ulong cbHash = 0
  CHAR HexDigits[0 TO 15] = &
    {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'}

  //Get handle to the crypto provider
  IF NOT CryptAcquireContextA&
     (hProv, s_null, s_null, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) &
     THEN
     err_number = GetLastError()
     return 'acquire context failed ' + String (err_number)
  END IF

  // Create the hash object
  IF NOT CryptCreateHash(hProv, CALG_MD5, 0, 0, hHash) THEN
     err_number = GetLastError()
     CryptReleaseContext(hProv, 0)
     return 'create hash failed ' + String (err_number)
  END IF

  // Add the input to the hash
  IF NOT CryptHashData(hHash, as_text, Len(as_text), 0) THEN
     err_number = GetLastError()
     CryptDestroyHash(hHash)
     CryptReleaseContext(hProv, 0)
     return 'hashdata failed ' + String (err_number)
  END IF

  // Get the hash value and convert it to readable characters
  cbHash = MD5LEN
  IF (CryptGetHashParam(hHash, HP_HASHVAL, bl_hash, cbHash, 0)) THEN
     FOR i = 1 TO 16
       bl_byte = BlobMid (bl_hash, i, 1)
       b = Asc (String(bl_byte))
       r = Mod (b, 16) // right 4 bits
       l = b / 16      // left 4 bits
       s_result += HexDigits [l] + HexDigits [r]
     NEXT
  ELSE
     err_number = GetLastError()
     return 'gethashparam failed ' + String (err_number)
  END IF

  // clean up and return
  CryptDestroyHash(hHash)
  CryptReleaseContext(hProv, 0)

  return s_result
  */
end subroutine

public function string uf_rc2_encrypt (string a_message, string a_salt);return uf_encrypt(a_message, a_salt, CALG_RC2)

end function

private function string uf_blob_to_hex (blob a_msg_blob, longlong a_msg_len);string msg_hex = ""
string chars_hex_arr[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"}

longlong i, length
byte b
int v1, v2

if a_msg_len = 0 then
	length = len(a_msg_blob)
else
	length = a_msg_len
end if

for i = 1 to length
    GetByte(a_msg_blob, i, b)

    v1 = int(b / 16)
    v2 = mod(b, 16)
	 
	 msg_hex += chars_hex_arr[v1+1]
	 msg_hex += chars_hex_arr[v2+1]
next

return msg_hex 
end function

private function string uf_rc2 (string a_message, string a_salt, boolean a_encrypt);// Do not use
// Keeping this as a reference to OLEObjects
return ""

//string command, file_name, output
//time timeout
//boolean hasTimedOut = false
//longlong file_num, rtn
//OLEObject ole
//
////rtn_str = uf_encrypt(a_message, a_salt, CALG_RC2)
//
//// Require message
//if a_message = "" then
//	return ""
//end if
//
//ole = create OLEObject
//rtn = ole.ConnectToNewObject( "PowerPlan.Saturn.PBUtils.PBCryptoUtils" )
//choose case rtn
//	case 0
//		
//		if a_encrypt then
//			output = String(ole.Encrypt(a_message, a_salt))
//		else
//			output = String(ole.Decrypt(a_message, a_salt))
//		end if
//		ole.DisconnectObject()
//		destroy ole
//		
//	case -1
//		f_pp_msgs("Invalid Call: the argument is the Object property of a control")
//	case -2
//		f_pp_msgs("Class name not found")
//	case -3
//		f_pp_msgs("Object could not be created")
//	case -4
//		f_pp_msgs("Could not connect to object")
//	case -9
//		f_pp_msgs("Other error")
//	case -15
//		f_pp_msgs("COM+ is not loaded on this computer")
//	case -16
//		f_pp_msgs("Invalid Call: this function not applicable")
//end choose
//
//return output
end function

on nvo_web_crypto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_web_crypto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

