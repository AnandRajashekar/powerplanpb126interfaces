HA$PBExportHeader$nvo_sizeof.sru
$PBExportComments$Non visual user object that implements the SizeOf function present on other languages (C, Delphi, VB)
forward
global type nvo_sizeof from nonvisualobject
end type
end forward

global type nvo_sizeof from nonvisualobject autoinstantiate
end type

type variables
Private:
CONSTANT longlong SIZE_BOOLEAN = 1 // Boolean
CONSTANT longlong SIZE_CHAR = 1 // Char
CONSTANT longlong SIZE_INT = 2 // Signed long
CONSTANT longlong SIZE_UINT = 2 // Unsigned long
CONSTANT longlong SIZE_LONG  = 4 // Signed Long
CONSTANT longlong SIZE_ULONG  = 4 // Unsigned Long
CONSTANT longlong SIZE_STRING  = 4 // Assume as string


// Supported DataTypes
longlong long
uint  UINT
longlong LONG
ulong  ULONG
char  CHAR
string  STRING
boolean  BOOLEAN

end variables

forward prototypes
public function longlong sizeof (longlong data)
public function longlong sizeof (ulong data)
public function longlong sizeof (longlong data)
public function longlong sizeof (uint data)
public function longlong sizeof (character data)
public function longlong sizeof (string data)
public function longlong sizeof (any data[])
public function longlong sizeof (boolean data)
public function longlong sizeof (powerobject data)
private function longlong sizeof (variabledefinition vardef[])
end prototypes

public function longlong sizeof (longlong data);Return(SIZE_INT)
end function

public function longlong sizeof (ulong data);Return(SIZE_ULONG)
end function

public function longlong sizeof (uint data);Return(SIZE_UINT)
end function

public function longlong sizeof (character data);Return(SIZE_CHAR)
end function

public function longlong sizeof (string data);Return(SIZE_STRING)
end function

public function longlong sizeof (any data[]);
// Gives the dimension of an array
//
// Arguments: Data[] => Array to know the dimension
//
// Returns: Size of the array
//
// Notes:
//  1) Supports mixed type arrays (and variable sized strings within the array)
//  2) DOESN'T support multi-dimension arrays;
longlong ll_Index, ll_Count, ll_Size = 0

ll_Count = UpperBound(Data)

For ll_Index = 1 To ll_Count

 Choose Case ClassName(Data[ll_Index])
  Case "long"
   ll_Size += SizeOf(LONG)
  Case "unsignedlong","ulong"
   ll_Size += SizeOf(ULONG)
  Case "int","long"
   ll_Size += SizeOf(long)
  Case "uint","unsignedlong","unsignedint"
   ll_Size += SizeOf(UINT)
  Case "char", "character"
   ll_Size += SizeOf(CHAR)
  Case "string"
   ll_Size += SizeOf(CHAR) * SizeOf(String(Data[ll_Index]))
  Case "boolean"
   ll_Size += SizeOf(BOOLEAN)
 End Choose

Next

Return(ll_Size)
end function

public function longlong sizeof (boolean data);Return(SIZE_BOOLEAN)
end function

public function longlong sizeof (powerobject data);
// This function calculates the size of a structure
//
// The structure can contain simple datatypes (long,long, boolean), arrays or
// other structures within it
//
// Arguments: Data => Structure to know the size of..
//
// Returns: Size of the structure or -1 if error
//
// Notes:
//
// 1) Cannot calculate the size of a structure with strings (variable size), for fixed
//  sized strings use a char array;
// 2) CAN calculate the size of multi-dimension arrays within the structures
ClassDefinition ClassDef
VariableDefinition VarDef[]

ClassDef = Data.ClassDefinition
VarDef = ClassDef.VariableList

Return(SizeOf(VarDef))

end function

private function longlong sizeof (variabledefinition vardef[]);// Internal calculations for structure sizes
longlong ll_Index, ll_Count, ll_Size, ll_Array = 0
ClassDefinition TypeInfo,ClassDef
VariableDefinition VarList[]
VariableCardinalityDefinition VarCarDef

ArrayBounds ArrBounds[]

ll_Count = Upperbound(VarDef)

For ll_Index = 2 To ll_Count
 
 VarCarDef = VarDef[ll_Index].Cardinality
 ArrBounds = VarCarDef.ArrayDefinition
 
 If Upperbound(ArrBounds) > 0 Then
  ll_Array = ArrBounds[1].UpperBound
 Else
  ll_Array = 1
 End If

 Choose Case VarDef[ll_Index].TypeInfo.DataTypeOf
  Case "long"
   ll_Size += SizeOf(LONG) * ll_Array
  Case "ulong","unsignedlong"
   ll_Size += SizeOf(ULONG) * ll_Array
  Case "int","long"
   ll_Size += SizeOf(long) * ll_Array
  Case "uint","unsignedint","unsignedlong"
   ll_Size += SizeOf(UINT) * ll_Array
  Case "char","character"
   ll_Size += SizeOf(CHAR)  * ll_Array
  Case "string"
   ll_Size += SizeOf(STRING) * ll_Array
  Case "structure"
   TypeInfo = VarDef[ll_Index].TypeInfo
   VarList = TypeInfo.VariableList
   ll_Size += SizeOf(VarList) * ll_Array
  Case Else
   MessageBox("SizeOf error","Type is not supported,possibly variable sized or object type!",StopSign!,Ok!)
   Return(-1)
 End Choose
 
Next

Return(ll_Size)
end function

on nvo_sizeof.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_sizeof.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

