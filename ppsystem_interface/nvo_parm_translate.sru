HA$PBExportHeader$nvo_parm_translate.sru
forward
global type nvo_parm_translate from nonvisualobject
end type
end forward

global type nvo_parm_translate from nonvisualobject
end type
global nvo_parm_translate nvo_parm_translate

forward prototypes
public subroutine of_string_to_structs (string a_string, ref s_ppbase_parm_arrays a_values, ref s_ppbase_parm_arrays_labels a_labels)
public function string of_structs_to_string (s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels)
private function string of_array_to_string (any a_array[])
private function string of_javify_string (any a_arg)
private function any of_string_to_boolean_array (string a_string)
private function any of_string_to_date_array (string a_string)
private function any of_string_to_datetime_array (string a_string)
private function any of_string_to_decimal_array (string a_string)
private function any of_string_to_double_array (string a_string)
private function any of_string_to_int_array (string a_string)
private function any of_string_to_long_array (string a_string)
private function any of_string_to_string_array (string a_string)
private function string of_unjavify_string (string a_string)
end prototypes

public subroutine of_string_to_structs (string a_string, ref s_ppbase_parm_arrays a_values, ref s_ppbase_parm_arrays_labels a_labels);//
// PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
// Purpose : Translates a string back to a s_ppbase_parm_arrays structure (with labels in s_ppbase_parm_arrays_labels)
//
// Scope : global
//
// Arguments : a_string : string : the JSON string generated by this.of_structs_to_string
//             a_values : s_ppbase_parm_arrays : holds the return values
//             a_labels : s_ppbase_parm_arrays_labels : holds the labels for the return values
//
// Returns : none
//

s_ppbase_parm_arrays arrs
s_ppbase_parm_arrays_labels labs

string object
integer start, stop
start = pos(a_string, '{', pos(a_string, 'parms')) + 1
stop = pos(a_string, '}', start)
do while start > 1
	object = mid(a_string, start, stop - start)
	
	string aType, aValue, aLabel
	integer tStart, tStop, vStart, vStop, lStart, lStop
	tStart = pos(object, '"type":"')+8
	tStop = pos(object, '"', tStart)
	aType = mid(object, tStart, tStop - tStart)
	vStart = pos(object, '"value":"<')+10
	vStop = pos(object, '>",', vStart)
	aValue = mid(object, vStart, vStop - vStart)
	lStart = pos(object, '"label":"')+9
	lStop = pos(object, '"', lStart)
	aLabel = mid(object, lStart, lStop - lStart)
	
	longlong long_array[]
	string string_array[]
	integer int_array[]
	decimal dec_array[]
	double doub_array[]
	date date_array[]
	datetime dt_array[]
	boolean bool_array[]
	choose case aType
		case "long"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg = long_array
			labs.long_label = aLabel
		case "long2"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg2 = long_array
			labs.long_label2 = aLabel
		case "long3"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg3 = long_array
			labs.long_label3 = aLabel
		case "long4"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg4 = long_array
			labs.long_label4 = aLabel
		case "long5"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg5 = long_array
			labs.long_label5 = aLabel
		case "long6"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg6 = long_array
			labs.long_label6 = aLabel
		case "long7"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg7 = long_array
			labs.long_label7 = aLabel
		case "long8"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg8 = long_array
			labs.long_label8 = aLabel
		case "long9"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg9 = long_array
			labs.long_label9 = aLabel
		case "long10"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg10 = long_array
			labs.long_label10 = aLabel
		case "long11"
			long_array = of_string_to_long_array(aValue)
			arrs.long_arg11 = long_array
			labs.long_label11 = aLabel
		case "string"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg = string_array
			labs.string_label = aLabel
		case "string2"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg2 = string_array
			labs.string_label2 = aLabel
		case "string3"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg3 = string_array
			labs.string_label3 = aLabel
		case "string4"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg4 = string_array
			labs.string_label4 = aLabel
		case "string5"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg5 = string_array
			labs.string_label5 = aLabel
		case "string6"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg6 = string_array
			labs.string_label6 = aLabel
		case "string7"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg7 = string_array
			labs.string_label7 = aLabel
		case "string8"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg8 = string_array
			labs.string_label8 = aLabel
		case "string9"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg9 = string_array
			labs.string_label9 = aLabel
		case "string10"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg10 = string_array
			labs.string_label10 = aLabel
		case "string11"
			string_array = of_string_to_string_array(aValue)
			arrs.string_arg11 = string_array
			labs.string_label11 = aLabel
		case "integer"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg = int_array
			labs.integer_label = aLabel
		case "integer2"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg2 = int_array
			labs.integer_label2 = aLabel
		case "integer3"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg3 = int_array
			labs.integer_label3 = aLabel
		case "integer4"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg4 = int_array
			labs.integer_label4 = aLabel
		case "integer5"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg5 = int_array
			labs.integer_label5 = aLabel
		case "integer6"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg6 = int_array
			labs.integer_label6 = aLabel
		case "integer7"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg7 = int_array
			labs.integer_label7 = aLabel
		case "integer8"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg8 = int_array
			labs.integer_label8 = aLabel
		case "integer9"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg9 = int_array
			labs.integer_label9 = aLabel
		case "integer10"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg10 = int_array
			labs.integer_label10 = aLabel
		case "integer11"
			int_array = of_string_to_int_array(aValue)
			arrs.integer_arg11 = int_array
			labs.integer_label11 = aLabel
		case "decimal"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg = dec_array
			labs.decimal_label = aLabel
		case "decimal2"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg2 = dec_array
			labs.decimal_label2 = aLabel
		case "decimal3"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg3 = dec_array
			labs.decimal_label3 = aLabel
		case "decimal4"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg4 = dec_array
			labs.decimal_label4 = aLabel
		case "decimal5"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg5 = dec_array
			labs.decimal_label5 = aLabel
		case "decimal6"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg6 = dec_array
			labs.decimal_label6 = aLabel
		case "decimal7"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg7 = dec_array
			labs.decimal_label7 = aLabel
		case "decimal8"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg8 = dec_array
			labs.decimal_label8 = aLabel
		case "decimal9"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg9 = dec_array
			labs.decimal_label9 = aLabel
		case "decimal10"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg10 = dec_array
			labs.decimal_label10 = aLabel
		case "decimal11"
			dec_array = of_string_to_decimal_array(aValue)
			arrs.decimal_arg11 = dec_array
			labs.decimal_label11 = aLabel
		case "double"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg = doub_array
			labs.double_label = aLabel
		case "double2"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg2 = doub_array
			labs.double_label2 = aLabel
		case "double3"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg3 = doub_array
			labs.double_label3 = aLabel
		case "double4"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg4 = doub_array
			labs.double_label4 = aLabel
		case "double5"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg5 = doub_array
			labs.double_label5 = aLabel
		case "double6"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg6 = doub_array
			labs.double_label6 = aLabel
		case "double7"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg7 = doub_array
			labs.double_label7 = aLabel
		case "double8"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg8 = doub_array
			labs.double_label8 = aLabel
		case "double9"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg9 = doub_array
			labs.double_label9 = aLabel
		case "double10"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg10 = doub_array
			labs.double_label10 = aLabel
		case "double11"
			doub_array = of_string_to_double_array(aValue)
			arrs.double_arg11 = doub_array
			labs.double_label11 = aLabel
		case "date"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg = date_array
			labs.date_label = aLabel
		case "date2"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg2 = date_array
			labs.date_label2 = aLabel
		case "date3"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg3 = date_array
			labs.date_label3 = aLabel
		case "date4"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg4 = date_array
			labs.date_label4 = aLabel
		case "date5"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg5 = date_array
			labs.date_label5 = aLabel
		case "date6"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg6 = date_array
			labs.date_label6 = aLabel
		case "date7"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg7 = date_array
			labs.date_label7 = aLabel
		case "date8"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg8 = date_array
			labs.date_label8 = aLabel
		case "date9"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg9 = date_array
			labs.date_label9 = aLabel
		case "date10"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg10 = date_array
			labs.date_label10 = aLabel
		case "date11"
			date_array = of_string_to_date_array(aValue)
			arrs.date_arg11 = date_array
			labs.date_label11 = aLabel
		case "datetime"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg = dt_array
			labs.datetime_label = aLabel
		case "datetime2"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg2 = dt_array
			labs.datetime_label2 = aLabel
		case "datetime3"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg3 = dt_array
			labs.datetime_label3 = aLabel
		case "datetime4"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg4 = dt_array
			labs.datetime_label4 = aLabel
		case "datetime5"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg5 = dt_array
			labs.datetime_label5 = aLabel
		case "datetime6"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg6 = dt_array
			labs.datetime_label6 = aLabel
		case "datetime7"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg7 = dt_array
			labs.datetime_label7 = aLabel
		case "datetime8"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg8 = dt_array
			labs.datetime_label8 = aLabel
		case "datetime9"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg9 = dt_array
			labs.datetime_label9 = aLabel
		case "datetime10"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg10 = dt_array
			labs.datetime_label10 = aLabel
		case "datetime11"
			dt_array = of_string_to_datetime_array(aValue)
			arrs.datetime_arg11 = dt_array
			labs.datetime_label11 = aLabel
		case "boolean"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg = bool_array
			labs.boolean_label = aLabel
		case "boolean2"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg2 = bool_array
			labs.boolean_label2 = aLabel
		case "boolean3"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg3 = bool_array
			labs.boolean_label3 = aLabel
		case "boolean4"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg4 = bool_array
			labs.boolean_label4 = aLabel
		case "boolean5"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg5 = bool_array
			labs.boolean_label5 = aLabel
		case "boolean6"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg6 = bool_array
			labs.boolean_label6 = aLabel
		case "boolean7"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg7 = bool_array
			labs.boolean_label7 = aLabel
		case "boolean8"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg8 = bool_array
			labs.boolean_label8 = aLabel
		case "boolean9"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg9 = bool_array
			labs.boolean_label9 = aLabel
		case "boolean10"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg10 = bool_array
			labs.boolean_label10 = aLabel
		case "boolean11"
			bool_array = of_string_to_boolean_array(aValue)
			arrs.boolean_arg11 = bool_array
			labs.boolean_label11 = aLabel
	end choose
	
	start = pos(a_string, '{', stop) + 1
	stop = pos(a_string, '}', start)
loop

a_values = arrs
a_labels = labs
end subroutine

public function string of_structs_to_string (s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels);//
// PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
// Purpose : Translates a s_ppbase_parm_arrays structure (with labels in s_ppbase_parm_arrays_labels) into a valid JSON string
//
// Scope : global
//
// Arguments : a_values : s_ppbase_parm_arrays : the structure you want to translate
//             a_labels : s_ppbase_parm_arrays_labels : the labels for your structure
//
// Returns : string : a valid JSON string encoding the contents of these structures
//

string syntax

syntax = '{"parms":['

if upperbound(a_values.long_arg) > 0 then
	syntax += ',{"type":"long","value":"'+of_array_to_string(a_values.long_arg)+'","label":"'+a_labels.long_label+'"}'
end if
if upperbound(a_values.long_arg2) > 0 then
	syntax += ',{"type":"long2","value":"'+of_array_to_string(a_values.long_arg2)+'","label":"'+a_labels.long_label2+'"}'
end if
if upperbound(a_values.long_arg3) > 0 then
	syntax += ',{"type":"long3","value":"'+of_array_to_string(a_values.long_arg3)+'","label":"'+a_labels.long_label3+'"}'
end if
if upperbound(a_values.long_arg4) > 0 then
	syntax += ',{"type":"long4","value":"'+of_array_to_string(a_values.long_arg4)+'","label":"'+a_labels.long_label4+'"}'
end if
if upperbound(a_values.long_arg5) > 0 then
	syntax += ',{"type":"long5","value":"'+of_array_to_string(a_values.long_arg5)+'","label":"'+a_labels.long_label5+'"}'
end if
if upperbound(a_values.long_arg6) > 0 then
	syntax += ',{"type":"long6","value":"'+of_array_to_string(a_values.long_arg6)+'","label":"'+a_labels.long_label6+'"}'
end if
if upperbound(a_values.long_arg7) > 0 then
	syntax += ',{"type":"long7","value":"'+of_array_to_string(a_values.long_arg7)+'","label":"'+a_labels.long_label7+'"}'
end if
if upperbound(a_values.long_arg8) > 0 then
	syntax += ',{"type":"long8","value":"'+of_array_to_string(a_values.long_arg8)+'","label":"'+a_labels.long_label8+'"}'
end if
if upperbound(a_values.long_arg9) > 0 then
	syntax += ',{"type":"long9","value":"'+of_array_to_string(a_values.long_arg9)+'","label":"'+a_labels.long_label9+'"}'
end if
if upperbound(a_values.long_arg10) > 0 then
	syntax += ',{"type":"long10","value":"'+of_array_to_string(a_values.long_arg10)+'","label":"'+a_labels.long_label10+'"}'
end if
if upperbound(a_values.long_arg11) > 0 then
	syntax += ',{"type":"long11","value":"'+of_array_to_string(a_values.long_arg11)+'","label":"'+a_labels.long_label11+'"}'
end if
if upperbound(a_values.string_arg) > 0 then
	syntax += ',{"type":"string","value":"'+of_array_to_string(a_values.string_arg)+'","label":"'+a_labels.string_label+'"}'
end if
if upperbound(a_values.string_arg2) > 0 then
	syntax += ',{"type":"string2","value":"'+of_array_to_string(a_values.string_arg2)+'","label":"'+a_labels.string_label2+'"}'
end if
if upperbound(a_values.string_arg3) > 0 then
	syntax += ',{"type":"string3","value":"'+of_array_to_string(a_values.string_arg3)+'","label":"'+a_labels.string_label3+'"}'
end if
if upperbound(a_values.string_arg4) > 0 then
	syntax += ',{"type":"string4","value":"'+of_array_to_string(a_values.string_arg4)+'","label":"'+a_labels.string_label4+'"}'
end if
if upperbound(a_values.string_arg5) > 0 then
	syntax += ',{"type":"string5","value":"'+of_array_to_string(a_values.string_arg5)+'","label":"'+a_labels.string_label5+'"}'
end if
if upperbound(a_values.string_arg6) > 0 then
	syntax += ',{"type":"string6","value":"'+of_array_to_string(a_values.string_arg6)+'","label":"'+a_labels.string_label6+'"}'
end if
if upperbound(a_values.string_arg7) > 0 then
	syntax += ',{"type":"string7","value":"'+of_array_to_string(a_values.string_arg7)+'","label":"'+a_labels.string_label7+'"}'
end if
if upperbound(a_values.string_arg8) > 0 then
	syntax += ',{"type":"string8","value":"'+of_array_to_string(a_values.string_arg8)+'","label":"'+a_labels.string_label8+'"}'
end if
if upperbound(a_values.string_arg9) > 0 then
	syntax += ',{"type":"string9","value":"'+of_array_to_string(a_values.string_arg9)+'","label":"'+a_labels.string_label9+'"}'
end if
if upperbound(a_values.string_arg10) > 0 then
	syntax += ',{"type":"string10","value":"'+of_array_to_string(a_values.string_arg10)+'","label":"'+a_labels.string_label10+'"}'
end if
if upperbound(a_values.string_arg11) > 0 then
	syntax += ',{"type":"string11","value":"'+of_array_to_string(a_values.string_arg11)+'","label":"'+a_labels.string_label11+'"}'
end if
if upperbound(a_values.integer_arg) > 0 then
	syntax += ',{"type":"integer","value":"'+of_array_to_string(a_values.integer_arg)+'","label":"'+a_labels.integer_label+'"}'
end if
if upperbound(a_values.integer_arg2) > 0 then
	syntax += ',{"type":"integer2","value":"'+of_array_to_string(a_values.integer_arg2)+'","label":"'+a_labels.integer_label2+'"}'
end if
if upperbound(a_values.integer_arg3) > 0 then
	syntax += ',{"type":"integer3","value":"'+of_array_to_string(a_values.integer_arg3)+'","label":"'+a_labels.integer_label3+'"}'
end if
if upperbound(a_values.integer_arg4) > 0 then
	syntax += ',{"type":"integer4","value":"'+of_array_to_string(a_values.integer_arg4)+'","label":"'+a_labels.integer_label4+'"}'
end if
if upperbound(a_values.integer_arg5) > 0 then
	syntax += ',{"type":"integer5","value":"'+of_array_to_string(a_values.integer_arg5)+'","label":"'+a_labels.integer_label5+'"}'
end if
if upperbound(a_values.integer_arg6) > 0 then
	syntax += ',{"type":"integer6","value":"'+of_array_to_string(a_values.integer_arg6)+'","label":"'+a_labels.integer_label6+'"}'
end if
if upperbound(a_values.integer_arg7) > 0 then
	syntax += ',{"type":"integer7","value":"'+of_array_to_string(a_values.integer_arg7)+'","label":"'+a_labels.integer_label7+'"}'
end if
if upperbound(a_values.integer_arg8) > 0 then
	syntax += ',{"type":"integer8","value":"'+of_array_to_string(a_values.integer_arg8)+'","label":"'+a_labels.integer_label8+'"}'
end if
if upperbound(a_values.integer_arg9) > 0 then
	syntax += ',{"type":"integer9","value":"'+of_array_to_string(a_values.integer_arg9)+'","label":"'+a_labels.integer_label9+'"}'
end if
if upperbound(a_values.integer_arg10) > 0 then
	syntax += ',{"type":"integer10","value":"'+of_array_to_string(a_values.integer_arg10)+'","label":"'+a_labels.integer_label10+'"}'
end if
if upperbound(a_values.integer_arg11) > 0 then
	syntax += ',{"type":"integer11","value":"'+of_array_to_string(a_values.integer_arg11)+'","label":"'+a_labels.integer_label11+'"}'
end if
if upperbound(a_values.decimal_arg) > 0 then
	syntax += ',{"type":"decimal","value":"'+of_array_to_string(a_values.decimal_arg)+'","label":"'+a_labels.decimal_label+'"}'
end if
if upperbound(a_values.decimal_arg2) > 0 then
	syntax += ',{"type":"decimal2","value":"'+of_array_to_string(a_values.decimal_arg2)+'","label":"'+a_labels.decimal_label2+'"}'
end if
if upperbound(a_values.decimal_arg3) > 0 then
	syntax += ',{"type":"decimal3","value":"'+of_array_to_string(a_values.decimal_arg3)+'","label":"'+a_labels.decimal_label3+'"}'
end if
if upperbound(a_values.decimal_arg4) > 0 then
	syntax += ',{"type":"decimal4","value":"'+of_array_to_string(a_values.decimal_arg4)+'","label":"'+a_labels.decimal_label4+'"}'
end if
if upperbound(a_values.decimal_arg5) > 0 then
	syntax += ',{"type":"decimal5","value":"'+of_array_to_string(a_values.decimal_arg5)+'","label":"'+a_labels.decimal_label5+'"}'
end if
if upperbound(a_values.decimal_arg6) > 0 then
	syntax += ',{"type":"decimal6","value":"'+of_array_to_string(a_values.decimal_arg6)+'","label":"'+a_labels.decimal_label6+'"}'
end if
if upperbound(a_values.decimal_arg7) > 0 then
	syntax += ',{"type":"decimal7","value":"'+of_array_to_string(a_values.decimal_arg7)+'","label":"'+a_labels.decimal_label7+'"}'
end if
if upperbound(a_values.decimal_arg8) > 0 then
	syntax += ',{"type":"decimal8","value":"'+of_array_to_string(a_values.decimal_arg8)+'","label":"'+a_labels.decimal_label8+'"}'
end if
if upperbound(a_values.decimal_arg9) > 0 then
	syntax += ',{"type":"decimal9","value":"'+of_array_to_string(a_values.decimal_arg9)+'","label":"'+a_labels.decimal_label9+'"}'
end if
if upperbound(a_values.decimal_arg10) > 0 then
	syntax += ',{"type":"decimal10","value":"'+of_array_to_string(a_values.decimal_arg10)+'","label":"'+a_labels.decimal_label10+'"}'
end if
if upperbound(a_values.decimal_arg11) > 0 then
	syntax += ',{"type":"decimal11","value":"'+of_array_to_string(a_values.decimal_arg11)+'","label":"'+a_labels.decimal_label11+'"}'
end if
if upperbound(a_values.double_arg) > 0 then
	syntax += ',{"type":"double","value":"'+of_array_to_string(a_values.double_arg)+'","label":"'+a_labels.double_label+'"}'
end if
if upperbound(a_values.double_arg2) > 0 then
	syntax += ',{"type":"double2","value":"'+of_array_to_string(a_values.double_arg2)+'","label":"'+a_labels.double_label2+'"}'
end if
if upperbound(a_values.double_arg3) > 0 then
	syntax += ',{"type":"double3","value":"'+of_array_to_string(a_values.double_arg3)+'","label":"'+a_labels.double_label3+'"}'
end if
if upperbound(a_values.double_arg4) > 0 then
	syntax += ',{"type":"double4","value":"'+of_array_to_string(a_values.double_arg4)+'","label":"'+a_labels.double_label4+'"}'
end if
if upperbound(a_values.double_arg5) > 0 then
	syntax += ',{"type":"double5","value":"'+of_array_to_string(a_values.double_arg5)+'","label":"'+a_labels.double_label5+'"}'
end if
if upperbound(a_values.double_arg6) > 0 then
	syntax += ',{"type":"double6","value":"'+of_array_to_string(a_values.double_arg6)+'","label":"'+a_labels.double_label6+'"}'
end if
if upperbound(a_values.double_arg7) > 0 then
	syntax += ',{"type":"double7","value":"'+of_array_to_string(a_values.double_arg7)+'","label":"'+a_labels.double_label7+'"}'
end if
if upperbound(a_values.double_arg8) > 0 then
	syntax += ',{"type":"double8","value":"'+of_array_to_string(a_values.double_arg8)+'","label":"'+a_labels.double_label8+'"}'
end if
if upperbound(a_values.double_arg9) > 0 then
	syntax += ',{"type":"double9","value":"'+of_array_to_string(a_values.double_arg9)+'","label":"'+a_labels.double_label9+'"}'
end if
if upperbound(a_values.double_arg10) > 0 then
	syntax += ',{"type":"double10","value":"'+of_array_to_string(a_values.double_arg10)+'","label":"'+a_labels.double_label10+'"}'
end if
if upperbound(a_values.double_arg11) > 0 then
	syntax += ',{"type":"double11","value":"'+of_array_to_string(a_values.double_arg11)+'","label":"'+a_labels.double_label11+'"}'
end if
if upperbound(a_values.date_arg) > 0 then
	syntax += ',{"type":"date","value":"'+of_array_to_string(a_values.date_arg)+'","label":"'+a_labels.date_label+'"}'
end if
if upperbound(a_values.date_arg2) > 0 then
	syntax += ',{"type":"date2","value":"'+of_array_to_string(a_values.date_arg2)+'","label":"'+a_labels.date_label2+'"}'
end if
if upperbound(a_values.date_arg3) > 0 then
	syntax += ',{"type":"date3","value":"'+of_array_to_string(a_values.date_arg3)+'","label":"'+a_labels.date_label3+'"}'
end if
if upperbound(a_values.date_arg4) > 0 then
	syntax += ',{"type":"date4","value":"'+of_array_to_string(a_values.date_arg4)+'","label":"'+a_labels.date_label4+'"}'
end if
if upperbound(a_values.date_arg5) > 0 then
	syntax += ',{"type":"date5","value":"'+of_array_to_string(a_values.date_arg5)+'","label":"'+a_labels.date_label5+'"}'
end if
if upperbound(a_values.date_arg6) > 0 then
	syntax += ',{"type":"date6","value":"'+of_array_to_string(a_values.date_arg6)+'","label":"'+a_labels.date_label6+'"}'
end if
if upperbound(a_values.date_arg7) > 0 then
	syntax += ',{"type":"date7","value":"'+of_array_to_string(a_values.date_arg7)+'","label":"'+a_labels.date_label7+'"}'
end if
if upperbound(a_values.date_arg8) > 0 then
	syntax += ',{"type":"date8","value":"'+of_array_to_string(a_values.date_arg8)+'","label":"'+a_labels.date_label8+'"}'
end if
if upperbound(a_values.date_arg9) > 0 then
	syntax += ',{"type":"date9","value":"'+of_array_to_string(a_values.date_arg9)+'","label":"'+a_labels.date_label9+'"}'
end if
if upperbound(a_values.date_arg10) > 0 then
	syntax += ',{"type":"date10","value":"'+of_array_to_string(a_values.date_arg10)+'","label":"'+a_labels.date_label10+'"}'
end if
if upperbound(a_values.date_arg11) > 0 then
	syntax += ',{"type":"date11","value":"'+of_array_to_string(a_values.date_arg11)+'","label":"'+a_labels.date_label11+'"}'
end if
if upperbound(a_values.datetime_arg) > 0 then
	syntax += ',{"type":"datetime","value":"'+of_array_to_string(a_values.datetime_arg)+'","label":"'+a_labels.datetime_label+'"}'
end if
if upperbound(a_values.datetime_arg2) > 0 then
	syntax += ',{"type":"datetime2","value":"'+of_array_to_string(a_values.datetime_arg2)+'","label":"'+a_labels.datetime_label2+'"}'
end if
if upperbound(a_values.datetime_arg3) > 0 then
	syntax += ',{"type":"datetime3","value":"'+of_array_to_string(a_values.datetime_arg3)+'","label":"'+a_labels.datetime_label3+'"}'
end if
if upperbound(a_values.datetime_arg4) > 0 then
	syntax += ',{"type":"datetime4","value":"'+of_array_to_string(a_values.datetime_arg4)+'","label":"'+a_labels.datetime_label4+'"}'
end if
if upperbound(a_values.datetime_arg5) > 0 then
	syntax += ',{"type":"datetime5","value":"'+of_array_to_string(a_values.datetime_arg5)+'","label":"'+a_labels.datetime_label5+'"}'
end if
if upperbound(a_values.datetime_arg6) > 0 then
	syntax += ',{"type":"datetime6","value":"'+of_array_to_string(a_values.datetime_arg6)+'","label":"'+a_labels.datetime_label6+'"}'
end if
if upperbound(a_values.datetime_arg7) > 0 then
	syntax += ',{"type":"datetime7","value":"'+of_array_to_string(a_values.datetime_arg7)+'","label":"'+a_labels.datetime_label7+'"}'
end if
if upperbound(a_values.datetime_arg8) > 0 then
	syntax += ',{"type":"datetime8","value":"'+of_array_to_string(a_values.datetime_arg8)+'","label":"'+a_labels.datetime_label8+'"}'
end if
if upperbound(a_values.datetime_arg9) > 0 then
	syntax += ',{"type":"datetime9","value":"'+of_array_to_string(a_values.datetime_arg9)+'","label":"'+a_labels.datetime_label9+'"}'
end if
if upperbound(a_values.datetime_arg10) > 0 then
	syntax += ',{"type":"datetime10","value":"'+of_array_to_string(a_values.datetime_arg10)+'","label":"'+a_labels.datetime_label10+'"}'
end if
if upperbound(a_values.datetime_arg11) > 0 then
	syntax += ',{"type":"datetime11","value":"'+of_array_to_string(a_values.datetime_arg11)+'","label":"'+a_labels.datetime_label11+'"}'
end if
if upperbound(a_values.boolean_arg) > 0 then
	syntax += ',{"type":"boolean","value":"'+of_array_to_string(a_values.boolean_arg)+'","label":"'+a_labels.boolean_label+'"}'
end if
if upperbound(a_values.boolean_arg2) > 0 then
	syntax += ',{"type":"boolean2","value":"'+of_array_to_string(a_values.boolean_arg2)+'","label":"'+a_labels.boolean_label2+'"}'
end if
if upperbound(a_values.boolean_arg3) > 0 then
	syntax += ',{"type":"boolean3","value":"'+of_array_to_string(a_values.boolean_arg3)+'","label":"'+a_labels.boolean_label3+'"}'
end if
if upperbound(a_values.boolean_arg4) > 0 then
	syntax += ',{"type":"boolean4","value":"'+of_array_to_string(a_values.boolean_arg4)+'","label":"'+a_labels.boolean_label4+'"}'
end if
if upperbound(a_values.boolean_arg5) > 0 then
	syntax += ',{"type":"boolean5","value":"'+of_array_to_string(a_values.boolean_arg5)+'","label":"'+a_labels.boolean_label5+'"}'
end if
if upperbound(a_values.boolean_arg6) > 0 then
	syntax += ',{"type":"boolean6","value":"'+of_array_to_string(a_values.boolean_arg6)+'","label":"'+a_labels.boolean_label6+'"}'
end if
if upperbound(a_values.boolean_arg7) > 0 then
	syntax += ',{"type":"boolean7","value":"'+of_array_to_string(a_values.boolean_arg7)+'","label":"'+a_labels.boolean_label7+'"}'
end if
if upperbound(a_values.boolean_arg8) > 0 then
	syntax += ',{"type":"boolean8","value":"'+of_array_to_string(a_values.boolean_arg8)+'","label":"'+a_labels.boolean_label8+'"}'
end if
if upperbound(a_values.boolean_arg9) > 0 then
	syntax += ',{"type":"boolean9","value":"'+of_array_to_string(a_values.boolean_arg9)+'","label":"'+a_labels.boolean_label9+'"}'
end if
if upperbound(a_values.boolean_arg10) > 0 then
	syntax += ',{"type":"boolean10","value":"'+of_array_to_string(a_values.boolean_arg10)+'","label":"'+a_labels.boolean_label10+'"}'
end if
if upperbound(a_values.boolean_arg11) > 0 then
	syntax += ',{"type":"boolean11","value":"'+of_array_to_string(a_values.boolean_arg11)+'","label":"'+a_labels.boolean_label11+'"}'
end if

syntax += ']}'
integer idx 
idx = pos(syntax, '[,')
if idx > 0 then
	syntax = replace(syntax, idx, 2, '[')
end if
return syntax
end function

private function string of_array_to_string (any a_array[]);string val
integer i

val = '<'
val += of_javify_string(a_array[1])
for i = 2 to upperbound(a_array)
	val += (', ' + of_javify_string(a_array[i]))
next
val += '>'

return val
end function

private function string of_javify_string (any a_arg);string val
if isnull(a_arg) then
	val = '~~null~~'
else
	choose case lower(classname(a_arg))
		case 'datetime'
			val = string(a_arg,'mm/dd/yyyy hh:mm:ss')
		case 'date'
			val = string(a_arg,'mm/dd/yyyy')
		case else
			val = string(a_arg)
	end choose
end if

integer pos
pos = pos(val, '{')
do while pos > 0
	val = replace(val, pos, 1, '~~ob~~')
	pos = pos(val, '{', pos + 2)
loop
pos = pos(val, '}')
do while pos > 0
	val = replace(val, pos, 1, '~~cb~~')
	pos = pos(val, '}', pos + 2)
loop
pos = pos(val, ',')
do while pos > 0
	val = replace(val, pos, 1, '~~comma~~')
	pos = pos(val, ',', pos + 2)
loop
pos = pos(val, '\')
do while pos > 0
	val = replace(val, pos, 1, '~~slash~~')
	pos = pos(val, '\', pos + 2)
loop
pos = pos(val, '~"')
do while pos > 0
	val = replace(val, pos, 1, '\~"')
	pos = pos(val, '~"', pos + 2)
loop

return val
end function

private function any of_string_to_boolean_array (string a_string);boolean val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		if lower(unit) = "true" then
			val[i] = True
		else
			val[i] = False
		end if
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))
if unit = '~~null~~' then
	setnull(val[i])
elseif lower(unit) = "true" then
	val[i] = True
else
	val[i] = False
end if

return val
end function

private function any of_string_to_date_array (string a_string);date val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		select to_date(:unit,'mm/dd/yyyy') into :val[i] from dual;
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))

// Powerbuilder doesn't support masking when converting back to a date.  All dates are converted
//	to strings as mm/dd/yyyy.  Will use the DB functions to do this conversion.  A little clunky but the
//	only solution I can find.
//val[i] = date(unit)
if unit = '~~null~~' then
	setnull(val[i])
else
	select to_date(:unit,'mm/dd/yyyy') into :val[i] from dual;
end if

return val
end function

private function any of_string_to_datetime_array (string a_string);datetime val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		select to_date(:unit,'mm/dd/yyyy HH24:MI:SS') into :val[i] from dual;
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))

// Powerbuilder doesn't support masking when converting back to a date.  All dates are converted
//	to strings as mm/dd/yyyy.  Will use the DB functions to do this conversion.  A little clunky but the
//	only solution I can find.
//val[i] = datetime(unit)
if unit = '~~null~~' then
	setnull(val[i])
else
	select to_date(:unit,'mm/dd/yyyy HH24:MI:SS') into :val[i] from dual;
end if

return val
end function

private function any of_string_to_decimal_array (string a_string);decimal val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		val[i] = dec(unit)
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))
if unit = '~~null~~' then
	setnull(val[i])
else
	val[i] = dec(unit)
end if

return val
end function

private function any of_string_to_double_array (string a_string);double val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		val[i] = double(unit)
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))
if unit = '~~null~~' then
	setnull(val[i])
else
	val[i] = double(unit)
end if

return val
end function

private function any of_string_to_int_array (string a_string);integer val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		val[i] = integer(unit)
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))
if unit = '~~null~~' then
	setnull(val[i])
else
	val[i] = integer(unit)
end if

return val
end function

private function any of_string_to_long_array (string a_string);longlong val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		val[i] = longlong(unit)
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))
if unit = '~~null~~' then
	setnull(val[i])
else
	val[i] = longlong(unit)
end if

return val
end function

private function any of_string_to_string_array (string a_string);string val[]

string unit
integer i, start, stop
i = 1
start = 1
stop = pos(a_string, ',')
do while stop > 0
	unit = trim(mid(a_string, start, stop - start))
	if unit = '~~null~~' then
		setnull(val[i])
	else
		val[i] = of_unjavify_string(unit)
	end if
	start = stop + 1
	stop = pos(a_string, ',', start)
	i += 1
loop
unit = trim(mid(a_string, start))
if unit = '~~null~~' then
	setnull(val[i])
else
	val[i] = of_unjavify_string(unit)
end if

return val
end function

private function string of_unjavify_string (string a_string);string val
val = string(a_string)

integer pos
pos = pos(val, '\~"')
do while pos > 0
	val = replace(val, pos, 2, '~"')
	pos = pos(val, '\~"', pos + 1)
loop
pos = pos(val, '~~ob~~')
do while pos > 0
	val = replace(val, pos, 4, '{')
	pos = pos(val, '~~ob~~', pos + 1)
loop
pos = pos(val, '~~cb~~')
do while pos > 0
	val = replace(val, pos, 4, '}')
	pos = pos(val, '~~cb~~', pos + 1)
loop
pos = pos(val, '~~comma~~')
do while pos > 0
	val = replace(val, pos, 7, ',')
	pos = pos(val, '~~comma~~', pos + 1)
loop
pos = pos(val, '~~slash~~')
do while pos > 0
	val = replace(val, pos, 7, '\')
	pos = pos(val, '~~slash~~', pos + 1)
loop

return val
end function

on nvo_parm_translate.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_parm_translate.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

