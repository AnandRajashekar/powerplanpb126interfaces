HA$PBExportHeader$uo_ppbase_datawindow_dropdown.sru
forward
global type uo_ppbase_datawindow_dropdown from datawindow
end type
end forward

global type uo_ppbase_datawindow_dropdown from datawindow
integer width = 965
integer height = 76
event pp_hideheaders ( )
event pp_showheaders ( )
end type
global uo_ppbase_datawindow_dropdown uo_ppbase_datawindow_dropdown

type variables
////
////	Whether we will allow different right click functionalities and their defaults.
////
	boolean	i_allow_rc_showheaders = true
	string		i_dddw_null_display_value
////
////	Whether the dropdown should have scrollbars and what percent width it should open.  Default these.
////
	longlong	i_dropdown_width = 150
	boolean	i_horizontal_scroll = true
	boolean	i_vertical_scroll = true

////
////	Whether the headers are showing or not.
////
	private boolean	i_headers_showing = false

////
////	The height of the standard filter datawindow header band.
////
	constant longlong i_header_height = 72
end variables

forward prototypes
public subroutine of_adddddwwhere (string a_where_clause)
public subroutine of_addnullrow ()
public subroutine of_addnullrow (string a_display_value)
public subroutine of_addrow (longlong a_data_value, string a_display_value)
public subroutine of_addrow (string a_data_value, string a_display_value)
public subroutine of_cleardddwwhere ()
public subroutine of_definemenuitems (longlong a_xpos, longlong a_ypos, longlong a_row, ref dwobject a_dwo)
public subroutine of_filterdddw ()
public subroutine of_populatedropdown ()
public subroutine of_resetdddw ()
public function longlong of_rowcountdddw ()
public subroutine of_setfilterdddw (string a_filter_string, boolean a_apply_filter)
public subroutine of_setsortdddw (string a_sort_string, boolean a_apply_sort)
public subroutine of_sortdddw ()
end prototypes

event pp_hideheaders();
end event

event pp_showheaders();
end event

public subroutine of_adddddwwhere (string a_where_clause);
end subroutine

public subroutine of_addnullrow ();
end subroutine

public subroutine of_addnullrow (string a_display_value);
end subroutine

public subroutine of_addrow (longlong a_data_value, string a_display_value);
end subroutine

public subroutine of_addrow (string a_data_value, string a_display_value);
end subroutine

public subroutine of_cleardddwwhere ();
end subroutine

public subroutine of_definemenuitems (longlong a_xpos, longlong a_ypos, longlong a_row, ref dwobject a_dwo);
end subroutine

public subroutine of_filterdddw ();
end subroutine

public subroutine of_populatedropdown ();
end subroutine

public subroutine of_resetdddw ();
end subroutine

public function longlong of_rowcountdddw ();
return 0
end function

public subroutine of_setfilterdddw (string a_filter_string, boolean a_apply_filter);
end subroutine

public subroutine of_setsortdddw (string a_sort_string, boolean a_apply_sort);
end subroutine

public subroutine of_sortdddw ();
end subroutine

on uo_ppbase_datawindow_dropdown.create
end on

on uo_ppbase_datawindow_dropdown.destroy
end on

event constructor;// Stubbed object in interface repository so nvo_ppbase_sysoption_functions compiles
end event

