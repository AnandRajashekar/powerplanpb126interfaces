HA$PBExportHeader$f_wo_validation_control.srf
$PBExportComments$5167-check if any validations exist
global type f_wo_validation_control from function_object
end type

forward prototypes
global function longlong f_wo_validation_control (longlong a_validation_type, string a_args[20])
end prototypes

global function longlong f_wo_validation_control (longlong a_validation_type, string a_args[20]);longlong counter, company_id, funding_wo, validation_id, &
		hard_edit, run_sql, numrows, i, rtn, j, id, k, rtn2, rtn_sql, check, col_cnt
string syntax, ds_sql, check_ds, err_msg, modify_or_sql, object, window_name, &
		loop_window_name, cv, validation_type_desc, val
uo_ds_top ds_validation_cntl, ds_validation_run, ds_count, ds_company
graphicobject first_window, find_top, loop_gobject
window loop_window

ds_validation_cntl = create uo_ds_top
rtn = 1

// ### CDM - Maint 5167 - check if any validations exist for this type
select count(*) into :check
from wo_validation_control
where wo_validation_type_id = :a_validation_type
and status_id = 1;

if check = 0 then
	return rtn
end if

select find_company, description into :ds_sql, :validation_type_desc from wo_validation_type
where wo_validation_type_id = :a_validation_type;

if isnull(ds_sql) then
	return rtn
end if

for k = 1 to 20
	if isnull(a_args[k]) then
		ds_sql = f_replace_string(ds_sql, '<arg'+string(k)+'>', "''", 'all')
	else
		ds_sql = f_replace_string(ds_sql, '<arg'+string(k)+'>', "'"+a_args[k]+"'", 'all')
	end if
next

ds_company = create uo_ds_top
check_ds = f_create_dynamic_ds(ds_company, 'grid', ds_sql, sqlca, true)
if upper(check_ds) <> 'OK' or ds_company.rowcount() < 1 then
	return rtn
end if

company_id = ds_company.getItemNumber(1,1)

// SQL that retrieves the appropriate validations
ds_sql = ' select wo_validation_id, validation_message, hard_edit, run_sql, syntax, description '+&
			' from wo_validation_control where company_id = '+string(company_id) +' and '+&
			' 	status_id = 1 and '+&
			'	wo_validation_type_id = '+string(a_validation_type)+' union '+&
			' select wo_validation_id, validation_message, hard_edit, run_sql, syntax, description '+&
			' from wo_validation_control where company_id = -1 and '+&
			' 	status_id = 1 and '+&
			'	wo_validation_type_id = '+string(a_validation_type)+' and lower(trim(description)) in '+&
			'	(select lower(trim(description)) from wo_validation_control where company_id = -1 and '+&
			'		status_id = 1 and '+&
			'		wo_validation_type_id = '+string(a_validation_type)+' minus '+&
			'	select lower(trim(description)) from wo_validation_control where '+&
			'		company_id = '+string(company_id) +' and '+&
			'		status_id = 1 and '+&
			'		wo_validation_type_id = '+string(a_validation_type)+&
			'	) order by hard_edit, description'

check_ds = f_create_dynamic_ds(ds_validation_cntl, 'grid', ds_sql, sqlca, true)
if upper(check_ds) <> 'OK' then
	return rtn
end if

numrows = ds_validation_cntl.rowcount()

// Now loop through the different validations
for i = 1 to numrows
	ds_count = create uo_ds_top
	validation_id = ds_validation_cntl.getItemNumber(i, 'wo_validation_id')
	syntax = ds_validation_cntl.getItemString(i, 'syntax')
	
	for k = 1 to 20
		if isnull(a_args[k]) then
			syntax = f_replace_string(syntax, '<arg'+string(k)+'>', "''", 'all')			
		else
			syntax = f_replace_string(syntax, '<arg'+string(k)+'>', "'"+a_args[k]+"'", 'all')
		end if
	next
		
	check_ds = f_create_dynamic_ds(ds_count, 'grid', syntax, sqlca, true)
	// If the datastore has bad syntax then skip this validation
	if upper(check_ds) <> 'OK' then
		destroy ds_count
		continue
	end if
	
	if ds_count.rowCount() < 1 then
		counter = 0
	else
		counter = ds_count.getItemNumber(1, 1)
	end if
	
	// If this criteria is not met then skip this validation
	if counter = 0 then continue
	
	// Retrieve some variables
	hard_edit = ds_validation_cntl.getItemNumber(i, 'hard_edit')
	run_sql = ds_validation_cntl.getItemNumber(i, 'run_sql')
	err_msg = ds_validation_cntl.getItemString(i, 'validation_message')
	
	// ### CDM
	//   add functionality for giving feedback to the user
	//   based on the data retrieved in the validation
	col_cnt = long(ds_count.Describe("DataWindow.Column.Count"))
	for j = 2 to col_cnt
		setnull(val)
		val = ds_count.getitemstring(1,j)
		if not isnull(val) then
			err_msg = f_replace_string(err_msg, '<col'+string(j)+'>', val, 'all')
		end if
	next
	
	// Send the user a message if they entered one
	if err_msg <> '' and not isnull(err_msg) then
		if hard_edit < 2 then
			f_status_box(validation_type_desc, err_msg)
		elseif rtn <> -1 then // Only display the prompts if all other conditions are met
			rtn2 = messagebox(validation_type_desc, err_msg, Question!, YesNo!)
			if rtn2 <> 1 then // Hard Edit of 2 means warn with a 'Yes' to continue
				rtn = -1 		// They didn't say yes, so exit
				exit
			end if
		end if
	end if
	
	// If hard_edit then set the return value to -1
	if hard_edit = 1 then rtn = -1
	
	//If run_sql then create ds to check wo_validation_run
	if run_sql = 1 then
		ds_sql = ' select id, modify_or_sql, object, syntax from wo_validation_run '+&
					' where wo_validation_id = '+string(validation_id)+' and status_id = 1 order by id'
		
		ds_validation_run = create uo_ds_top
		
		check_ds = f_create_dynamic_ds(ds_validation_run, 'grid', ds_sql, sqlca, true)
		// If the datastore has bad syntax then skip this validation
		if upper(check_ds) = 'OK' then
			for j = 1 to ds_validation_run.rowcount()
				id = ds_validation_run.getItemNumber(j, 'id')
				modify_or_sql = ds_validation_run.getItemString(j, 'modify_or_sql')								
				syntax = ds_validation_run.getItemString(j, 'syntax')
				
				if lower(trim(modify_or_sql)) = 'sql' then
					cv = f_pp_system_control_company('Allow Dynamic Edits to Update', company_id)
					if upper(trim(cv)) = 'YES' then
						
						rtn_sql = f_wo_validation_sql(syntax, a_validation_type)
						
						if rtn_sql = -1 then
							continue
						end if
						
						for k = 1 to 20
							if isnull(a_args[k]) then
								syntax = f_replace_string(syntax, '<arg'+string(k)+'>', "''", 'all')			
							else
								syntax = f_replace_string(syntax, '<arg'+string(k)+'>', "'"+a_args[k]+"'", 'all')
							end if
						next
						
						execute immediate :syntax;
						if sqlca.SQLCode <> 0 then
							f_status_box(validation_type_desc, "Error running sql from wo_validation_run for id = '"+&
											string(id)+"~':" + sqlca.SQLErrText)
							rollback;
							return -1
						end if
					else
						continue
					end if
				elseif lower(trim(modify_or_sql)) = 'modify' then					
					cv = f_pp_system_control_company('Allow Dynamic Edits to Modify', company_id)
					if upper(trim(cv)) = 'YES' then
						object = ds_validation_run.getItemString(j, 'object')
						
						f_wo_validation_modify(validation_type_desc, object, syntax)
					else // System control is not set to 'YES'
						continue
					end if // Only proceed if system control is set to 'YES'
					
				end if
				
			next // Loop through ds_validation_run	
			
		end if // Check if ds_validation_run had good syntax
		
		destroy ds_validation_run
		
	end if // If run_sql = 1		
	
	destroy ds_count
next

destroy ds_validation_cntl
// ### SAT 9/17/2010 Maint 5402: destroy ds_company for memory issues
destroy ds_company
return rtn
			
end function

