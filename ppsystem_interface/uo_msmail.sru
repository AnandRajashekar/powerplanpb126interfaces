HA$PBExportHeader$uo_msmail.sru
$PBExportComments$v10.0.10 merge with standalone
forward
global type uo_msmail from uo_mail
end type
end forward

global type uo_msmail from uo_mail
end type
global uo_msmail uo_msmail

type variables
boolean i_passwd_flag = false
string i_userid
string i_passwd
string i_smtp_port
string i_smtp_timeout
end variables

forward prototypes
public function boolean uf_get_user (ref string a_user)
public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, boolean a_delete_files)
public function boolean uf_get_host (ref string a_server, ref string a_errmsg)
public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename, boolean a_delete_files)
end prototypes

public function boolean uf_get_user (ref string a_user);string mail_user
if pos(a_user,'@',1) = 0 then
	select mail_id  into :mail_user from pp_security_users where lower(rtrim(users)) = lower(rtrim(:a_user));
	if sqlca.sqlcode <> 0 then
		
		Return false
	end if
	if isnull(mail_user) then 
	   return false
   end if
	a_user = mail_user
end if

return true

end function

public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, boolean a_delete_files);
return sendFile(a_fromUser, a_password, a_subject, a_text, a_toUser, a_server, "", a_delete_files)
end function

public function boolean uf_get_host (ref string a_server, ref string a_errmsg);string ini_file,host_name
string flag

// Get the INI file
ini_file = f_get_ini_file() 

// If the host name was not passed as an argument, look it up from the INI file
if a_server = '' then
		host_name = ProfileString(ini_file, "Mail", "SMTP_HOST", "")
		
		if isnull(host_name) or host_name = '' then
			a_errMsg = "ERROR: Could not retrieve SMTP_HOST value from INI file!"
			return false
		else
			a_server = host_name
		end if
end if

// Look up the SMTP_Port from the INI file
i_smtp_port = ProfileString(ini_file, "Mail", "SMTP_PORT", "")

if isnull(i_smtp_port) or i_smtp_port = "" then
	a_errMsg = "ERROR: Could not retrieve SMTP_PORT value from INI file!"
	return false
end if

// Look up the SMTP_TimeOut from the INI file
i_smtp_timeout = ProfileString(ini_file, "Mail", "SMTP_TIMEOUT", "")

if isnull(i_smtp_timeout) or i_smtp_timeout = "" then
	a_errMsg = "ERROR: Could not retrieve SMTP_TIMEOUT value from INI file!"
	return false
end if

// Look up the SMTP_PASSWORD from the INI file
i_passwd = ProfileString(ini_file, "Mail", "SMTP_PASSWORD", "")

if isnull(i_passwd) or i_passwd = "" then
	a_errMsg = "ERROR: Could not retrieve SMTP_PASSWORD value from INI file!"
	return false
end if

// Look up the SMTP_USER from the INI file
i_userid = ProfileString(ini_file, "Mail", "SMTP_USER", "")

if isnull(i_userid) or i_userid = "" then
	a_errMsg = "ERROR: Could not retrieve SMTP_USER value from INI file!"
	return false
end if

return true
end function

public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename, boolean a_delete_files);string mail_user, userid, users[], touser, run_str, cmd_args, errMsg
longlong longResult, num, count, pos, i, rtn
OleObject wsh
environment env

getenvironment(env) 


count = 0
userid = lower(sqlca.userid)

touser = a_touser
if touser = '' then
   touser = userid
end if

if right(touser,1) = ";" then
	touser = left(touser,len(touser)-1)
end if

// Check to see if touser is a security group
select count(*) into :num from pp_security_groups where groups = lower(:touser);

if num > 0 then
	declare user_cur cursor for  select u.mail_id from pp_security_users u ,pp_security_users_groups g
					 where  rtrim(u.users) = rtrim(g.users) and g.groups = lower(:touser);
	open user_cur;
	fetch user_cur into :mail_user;
	do while  sqlca.sqlcode = 0 
		if isnull(mail_user) = false then
			count++
			users[count] = mail_user
		end if
		fetch user_cur into :mail_user;
	loop
	close user_cur;
	if count = 0 then
		if g_main_application then 
				 MessageBox ("Mail IDs", 'No Mail IDs exist for the group ' + touser )
		else
			f_pp_msgs('No Mail IDs exist for the group ' + touser)
		 end if
		 return false
	else
		touser = g_string_func.of_parsestringarrayintostring(users)
	end if
end if


// is this a real mail address
uf_get_user(a_fromuser)

if pos(touser,';') = 0 and pos(touser,',') = 0 then
   if uf_get_user(touser) = false then
   
	if g_main_application then 
      MessageBox ("Mail IDs", 'No Mail ID exist for ' + a_touser)
	else
		f_pp_msgs('No Mail ID exist for ' + a_touser)
	end if
      return false
   end if
end if

// Get the values from the INI file
if not uf_get_host(a_server, errMsg) then
	if g_main_application then
		messagebox("Send Mail", errMsg, StopSign!)
	else
		f_pp_msgs("  ")
		f_pp_msgs("Send Mail: " + errMsg)
		f_pp_msgs("  ")
	end if
	return false
end if

// Make sure PPSMTPMailer.exe exists in the expected location
if not fileExists(g_exe_info.file_path + 'PPSMTPMailer.exe') then
	if g_main_application then 
		messagebox("Send Mail","ERROR: Unable to find '" + g_exe_info.file_path + "PPSMTPMailer.exe'.", StopSign!)
	else
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Unable to find '" + g_exe_info.file_path + "PPSMTPMailer.exe'.")
		f_pp_msgs(" ")
	end if
	return false
end if

// Set up the run string
run_str = "cmd.exe /c PPSMTPMailer.exe "

cmd_args  = '/SMTPUsername="' + i_userid + '" '
cmd_args += '/SMTPPassword="' + i_passwd + '" '
cmd_args += '/SMPTServer="' + a_server + '" '
cmd_args += '/SMTPTimeout="' + i_smtp_timeout + '" '
cmd_args += '/SMTPPort="' + i_smtp_port + '" '
cmd_args += '/To="' + toUser + '" '
cmd_args += '/From="' + a_fromUser + '" '
cmd_args += '/Subject="' + a_subject + '" '
cmd_args += '/Body="' + a_text + '" '
if not isnull(a_fileName) and a_fileName <> "" then
	cmd_args += '/Attachment="' + a_fileName + '" '
end if
cmd_args += ' IsHTML="YES"'

run_str += cmd_args

wsh = create OleObject
wsh.ConnectToNewObject("WScript.Shell")
wsh.CurrentDirectory = g_exe_info.file_path
// Arguments to WScript.Shell Run method
// 1: strCommand - run_str = the process to be executed including command line arguments
// intWindowStyle: 0 = hide, 1 = show, but there are 11 options for this parameter
// bWaitOnReturn: true = wait for the command to finish, false = don't wait and return 0 automatically
rtn = wsh.Run(run_str, 0, true)

if rtn <> 0 then
	if g_main_application then
		messagebox("Send Mail", "ERROR: Calling PPSMTPMailer.exe", StopSign!)
	else
		f_pp_msgs("Send Mail: ERROR: Calling PPSMTPMailer.exe" + errMsg)
	end if
	return false
end if

return true

end function

on uo_msmail.create
call super::create
end on

on uo_msmail.destroy
call super::destroy
end on

