HA$PBExportHeader$nvo_exception.sru
forward
global type nvo_exception from exception
end type
end forward

global type nvo_exception from exception
end type
global nvo_exception nvo_exception

type variables
string i_args[]
end variables

on nvo_exception.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_exception.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

