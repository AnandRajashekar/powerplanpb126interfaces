HA$PBExportHeader$uo_ppinterface.sru
$PBExportComments$New base object to handle database logic.
forward
global type uo_ppinterface from nonvisualobject
end type
end forward

global type uo_ppinterface from nonvisualobject
end type
global uo_ppinterface uo_ppinterface

type variables
string i_exe_name, i_exe_version
boolean  i_running_from_source, i_running_from_ws
datetime i_started_at
longlong i_system_lock_enabled
boolean i_ssp_error = false
string i_ssp_process
boolean i_use_db_logs = false
end variables

forward prototypes
public function integer uf_getosversion (ref string a_ver)
public function longlong uf_connect ()
public function longlong uf_disconnect ()
public function integer uf_connect_logs ()
private function string uf_unc_translate (string a_path)
public function longlong uf_process_ldap_connection (string a_logid)
end prototypes

public function integer uf_getosversion (ref string a_ver);s_osversioninfoex os

os.szcsdversion= space(128)
longlong VER_PLATFORM_WIN32_NT = 2
longlong VER_PLATFORM_WIN32_WINDOWS = 1
longlong VER_PLATFORM_WIN32s = 0

longlong VER_NT_WORKSTATION  = 1
longlong VER_NT_DOMAIN_CONTROLLER = 2
longlong VER_NT_SERVER = 3
longlong VER_SERVER_NT                       =0
//longlong VER_WORKSTATION_NT                  0x40000000
longlong VER_SUITE_SMALLBUSINESS             =1
longlong VER_SUITE_ENTERPRISE                =2
longlong VER_SUITE_BACKOFFICE                =4
longlong VER_SUITE_COMMUNICATIONS            =8
longlong VER_SUITE_TERMINAL                  =16
longlong VER_SUITE_SMALLBUSINESS_RESTRICTED  =32
longlong VER_SUITE_EMBEDDEDNT                =64
longlong VER_SUITE_DATACENTER                =128
longlong VER_SUITE_SINGLEUSERTS              =256
longlong VER_SUITE_PERSONAL                  =512
longlong VER_SUITE_BLADE                     =1024
longlong VER_SUITE_EMBEDDED_RESTRICTED      = 2048
longlong status
//status = GetVersionExA(  OS)
// OSVERSIONINFOEX osvi;
longlong bOsVersionInfoEx
n_bit n_bit
string ver
//
//   // Try calling GetVersionEx using the OSVERSIONINFOEX structure.
//   // If that fails, try using the OSVERSIONINFO structure.
//
//   ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
   os.dwosversioninforsize =156
	bOsVersionInfoEx = GetVersionExA(os)
 
   if bOsVersionInfoEx =  0 then
 
      //osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
     // if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) 
         return 0
  
    end if
   choose case  os.dwPlatformId

      // Test for the Windows NT product family.
      case VER_PLATFORM_WIN32_NT

         // Test for the specific product family.
         if  os.dwMajorVersion = 5 and os.dwMinorVersion = 2 then
            ver = "Microsoft Windows .NET Server 2003 family "
		   end if
         if os.dwMajorVersion = 5 and os.dwMinorVersion = 1 then
            ver = "Microsoft Windows XP "
			end if
         if os.dwMajorVersion = 5 and os.dwMinorVersion = 0 then
            ver = "Microsoft Windows 2000 "
         end if
         if os.dwMajorVersion <= 4 then
             ver = "Microsoft Windows NT "
			end if
         // Test for specific product on Windows NT 4.0 SP6 and later.
         if bOsVersionInfoEx > 0 then
            // Test for the workstation type.
            if asc(os.wProductType) = VER_NT_WORKSTATION then        
               if os.dwMajorVersion = 4 then
                  ver = ver + " Workstation 4.0 "
               elseif n_bit.of_bitwiseand(os.wSuiteMask,  VER_SUITE_PERSONAL) > 0 then
                  ver =  ver + " Home Edition " 
               else
                  ver =  ver + " Professional " 
					end if
            // Test for the server type.
            elseif  asc(os.wProductType) = VER_NT_SERVER then

               if os.dwMajorVersion = 5 and os.dwMinorVersion = 2 then
              
                  if n_bit.of_bitwiseand(os.wSuiteMask , VER_SUITE_DATACENTER) > 0 then
                     ver = ver + "Datacenter Edition " 
                  elseif n_bit.of_bitwiseand(os.wSuiteMask, VER_SUITE_ENTERPRISE ) > 0 then
                     ver = ver + "Enterprise Edition " 
                  elseif n_bit.of_bitwiseand(os.wSuiteMask, VER_SUITE_BLADE ) > 0 then
                     ver = ver + "Web Edition "
                  else
                     ver = ver + "Standard Edition " 
						end if

               elseif os.dwMajorVersion = 5 and os.dwMinorVersion = 0 then
              
                  if n_bit.of_bitwiseand(os.wSuiteMask, VER_SUITE_DATACENTER) > 0 then
                      ver = ver + "Datacenter Server "
                  elseif n_bit.of_bitwiseand(os.wSuiteMask,VER_SUITE_ENTERPRISE ) > 0 then
                     ver = ver + "Advanced Server " 
                  else
                     ver = ver +  "Server " 
                  end if

               else  // Windows NT 4.0 
             
                  if n_bit.of_bitwiseand(os.wSuiteMask, VER_SUITE_ENTERPRISE ) > 0 then
                     ver = ver + "Server 4.0, Enterprise Edition " 
                  else
                     ver = ver + "Server 4.0 " 
						end if
					end if
            end if
         else  // Test for specific product on Windows NT 4.0 SP5 and earlier
       
            string hKey;
				longlong bufsize = 256
            string szProductType //bufsiz
            ulong dwBufLen=BUFSIZE
            longlong lRet
				longlong ERROR_SUCCESS = 0
              
//            lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
//               "SYSTEM\\CurrentControlSet\\Control\\ProductOptions",
//               0, KEY_QUERY_VALUE, &hKey );
            if lRet <> ERROR_SUCCESS then
               return -1
				end if
				 hkey = "SYSTEM\CurrentControlSet\Control\ProductOptions"
				 RegistryGet ( hkey, "ProductType", szProductType )


            if "WINNT"= szProductType then
                 ver = ver + "Workstation " 
				end if
            if  "LANMANNT"=  szProductType  then
                ver = ver + "Server " 
			   end if
            if "SERVERNT" =  szProductType then
                ver = ver + "Advanced Server "
            end if
            ver = ver + string(os.dwMajorVersion) + "." + string( os.dwMinorVersion )
         end if

      // Display service pack (if any) and build number.

         if os.dwMajorVersion = 4 and   os.szCSDVersion = "Service Pack 6" then
        
           

            // Test for SP6 versus SP6a.
            //lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
            //   "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Hotfix\\Q246009",
            //   0, KEY_QUERY_VALUE, &hKey );
				
				
            if lRet = ERROR_SUCCESS then
                ver= ver + " Service Pack 6a (Build "+ string(n_bit.of_bitwiseand(  os.dwBuildNumber  ,65535))         
            else // Windows NT 4.0 prior to SP6a
            
             ver= ver + os.szCSDVersion + " (Build " + string(n_bit.of_bitwiseand(  os.dwBuildNumber  ,65535))  + ")"
                
			   end if


       else // Windows NT 3.51 and earlier or Windows 2000 and later
      
           
              ver = ver +  os.szCSDVersion + " (Build " + string(n_bit.of_bitwiseand(os.dwBuildNumber ,65535)) + ")"
               
		 end if


        

      // Test for the Windows 95 product family.
      case VER_PLATFORM_WIN32_WINDOWS

         if os.dwMajorVersion = 4 and os.dwMinorVersion = 0 then
         
             ver = "Microsoft Windows 95 "
             if  mid(os.szCSDVersion,1,1) = 'C' or mid(os.szCSDVersion,1,1) = 'B' then
                ver = ver + " OSR2 " 
				end if
			end if

         if os.dwMajorVersion = 4 and os.dwMinorVersion = 10 then

             ver = "Microsoft Windows 98 "
             if  mid(os.szCSDVersion,1,1) = 'A' then
                ver = ver + "SE " 
				end if
         end if

         if os.dwMajorVersion = 4 and os.dwMinorVersion = 90 then
            ver ="Microsoft Windows Millennium Edition"
         end if
         

      case VER_PLATFORM_WIN32s

         ver ="Microsoft Win32s"
         
	end choose
a_ver = ver
return 0
end function

public function longlong uf_connect ();string sysdate, s_date, str, path, exe_version_orig, ini_file, cntrl_val, db_version, pp_version, pp_patch, comp_pp_version, &
	path_unc, log_file_unc, errMsg, custom_version
longlong	rtn, running_session_id, mod, status, allow_concurrent, system_lock, rRtn
date		ddate 
time 		ttime
datetime	file_created, file_written
environment env	
boolean version_pass
longlong start_pos, colon_pos, end_pos, i
string conn, conn_array[]
string conn_LogID, conn_LogPass, conn_UserID, conn_DBMS, conn_ServerName
longlong conn_SSPID, custom_PBD_count

//*****************************************************************************************
//
// Initialize global variables
//
//*****************************************************************************************
// get any command line arguments if used -- any parsing or use of this command line arg should be in the custom code
g_command_line_args 			= 	CommandParm()

g_of_log_add_time 				= 	true
g_of_log_use_ppmsg				=	true
g_of_log_use_writefile			=	true
g_of_check_sql_write_success 	= 	false
g_msg_order     					= 	1	
g_do_not_write_batch_id 		= 	false

g_rtn_code      						= 	0  		//	This is the code that gets sent back to auto sys
setnull(g_interface_id)						//	Interface Batch ID
g_record_count  					= 	0		

g_main_application 				= 	false
g_db_connected 					= 	false


//*****************************************************************************************
//
// Initialize local variables
//
//*****************************************************************************************
sysdate								= 	string(today(),"yyyymmdd_hhmmss")
s_date 								= 	string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  								= 	date(left(s_date, 10))
ttime  								=	time(right(s_date, 8))
i_started_at 						= 	datetime(ddate, ttime)
i_running_from_source 			= 	false
i_running_from_ws 				= 	false

// must instantiate uo_winapi before calling f_get_temp_dir() below
// this code was taken (and modified) from the PowerPlan application open() event
getenvironment(env) 
CHOOSE CASE env.OSType
CASE Windows!  
	 if env.win16 = true then 
		  uo_winapi = create u_external_function_winapi
	else
		  uo_winapi = create u_external_function_win32
	end if	
CASE WindowsNT!
	 if env.win16 = true then 
		 uo_winapi = create u_external_function_winapi
	 else	 
		 uo_winapi = create u_external_function_win32
	end if
END CHOOSE 

// ### 29892: JAK: Pull file information into global structure
//GET the last change time of this interface and write it to the log
nvo_func_file l_func_file
l_func_file = create nvo_func_file
g_exe_info = l_func_file.of_get_file_info()

i_exe_name = g_exe_info.file_name
path = g_exe_info.file_path + g_exe_info.file_name
i_exe_version = g_exe_info.file_version
file_written = g_exe_info.file_write_date

// PP-45359: Moved the create for g_msmail from ppinterface.open since we have to
// read from the INI file to look up the email option to determine which user 
// object to use to create g_msmail
ini_file   = f_get_ini_file()
if ProfileInt(ini_file, "Mail", "SMTPMAIL", 0) = 1 then
	g_msmail = create uo_smtpmail
else
	g_msmail = create uo_msmail
end if

//*****************************************************************************************
//
// Initialize some local variables
//
//*****************************************************************************************
//GET the last change time of this interface and write it to the log


// Moved the i_running_from_source check up so if the interface fails to connect it knows not to close PB
if (lower(i_exe_name) = 'pb110.exe' or lower(i_exe_name) = 'pb120.exe' or lower(i_exe_name) = 'pb126.exe') then
	i_running_from_source = true
elseif lower(i_exe_name) = 'w3wp.exe' then
	i_running_from_ws = true
end if

// If running from source or WS, get the exe name from g_uo_client.i_exe_name
if (i_running_from_source or i_running_from_ws) and (not isnull(g_uo_client.i_exe_name) and trim(g_uo_client.i_exe_name) <> '') then
	i_exe_name = g_uo_client.i_exe_name 
	
	if pos(i_exe_name,'|') > 0 then 
		i_exe_version = mid(i_exe_name,pos(i_exe_name,'|') + 1)
		i_exe_name = mid(i_exe_name,1,pos(i_exe_name,'|') - 1)
	end if 
end if	

exe_version_orig = i_exe_version

//*****************************************************************************************
//
// Get the location and name of the log file
//
//*****************************************************************************************
setNull(g_log_file)

// METHOD ONE: Use PPCSET Entry
g_prog_interface.uf_get_prog_params("")
g_log_file = trim(g_prog_interface.i_log_file)

// METHOD TWO: Look in the INI file for the TEMP dir
//             Be careful that the "Temp" value from the "Application" section of the 
//             pwrplant.ini file is setup correctly and points to a real folder
if isnull(g_log_file) or g_log_file = "" then
	ini_file   = f_get_ini_file()
	g_log_file = trim(ProfileString(ini_file, "Application", "Temp", "None"))
	
	// verify that a real value was returned
	if trim(g_log_file) = "" or g_log_file = "None" then
		setNull(g_log_file)
	else
		// append a '\' if needed, and use the {exe_name} & {sysdate} conventions
		if right(trim(g_log_file), 1) <> '\' then g_log_file += '\'
		g_log_file += '{exe_name}_{sysdate}.log'
	end if
end if

// METHOD THREE: Look up the windows TEMP environment variable
//               The f_get_temp_dir() function below calls the base function 
//               uo_ps_interface.uf_get_temp_dir(), which does a check similar to 
//               METHOD TWO above.  If you want this method to be used in determining
//               the location of the log file, you must either:
//                      (a) delete the "Temp" value from the "Application" section of pwrplant.ini OR
//                      (b) set this value to "None" (without quotes) in pwrplant.ini
if isnull(g_log_file) or g_log_file = "" then
	g_log_file = f_get_temp_dir()
	
	if not isnull(g_log_file) and trim(g_log_file) <> '' then
		if right(trim(g_log_file), 1) <> '\' then g_log_file += '\'
		g_log_file += '{exe_name}_{sysdate}.log'
	end if	
end if

// METHOD FOUR: Default to c:\temp
if isnull(g_log_file) or g_log_file = "" then
	g_log_file = 'C:\temp\{exe_name}_{sysdate}.log'
end if

g_log_file = f_replace_string(lower(g_log_file),'{exe_name}',f_replace_string(i_exe_name,'.exe','','all'),'all')
g_log_file = f_replace_string(lower(g_log_file),'{sysdate}',sysdate,'all')

if pos(g_log_file,'\') = 0 and pos(g_log_file,'/') = 0 then
	// Directory not explicit.  Explicitly set the same path as exe.
	g_log_file = g_exe_info.file_path + g_log_file
end if

//*****************************************************************************************
//
// Get more information about the user and machine running this process
//
//*****************************************************************************************
string os_version, user, hostname
longlong code, code2
long len
int UIVersionRequested = 2
//s_WSAData WSAData
	
// get the operating system version info
uf_getosversion(os_version)

// get the OS user name
len = 200
user = space(len)
status = WNetGetUserA(0,user,len)

// ### 7693: JAK: 2011-05-27:  Change the WSA calls to GetEnvironmentVariableA calls as they are more direct and less potential issues.
// get the machine (host) name
hostname = space(201)
code = GetEnvironmentVariableA('ComputerName',hostname,200)
if code = 0 then
	code = GetLastError()
	hostname = "Error code " + string(code) + " trying to get ComputerName when calling GetEnvironmentVariableA"
end if


//*****************************************************************************************
//
//  Are we doing Server Side Processing?
//		Command Line will be of the form "/ssp=userid:process_identifier" such as "/ssp=jking:4775"
// 		### 29892: JAK: server side processing
//
//			boolean								g_ssp
//			string									g_ssp_user
//			longlong	g_ssp_process_identifier
//			s_ppbase_parm_arrays   			g_ssp_parms
//			s_ppbase_parm_arrays_labels	g_ssp_labels
//			nvo_server_side_request			g_ssp_nvo
//
//*****************************************************************************************
i_ssp_process = ''
if pos(g_command_line_args,"/ssp=") > 0 then
	g_ssp = true
	start_pos = pos(g_command_line_args,"/ssp=")
	colon_pos = pos(g_command_line_args,":",start_pos)
	end_pos = pos(g_command_line_args + ' ',' ',start_pos)
	i_ssp_process = trim(left(g_command_line_args,(start_pos - 1)))
	if isnull(i_ssp_process) then i_ssp_process = ''
	
	if colon_pos <= 0 or end_pos <= 0 then
		// error
		g_ssp= false
		g_ssp_user = '???'
		g_ssp_process_identifier = 0
		f_write_log(g_log_file,'ERROR:  SSP parameter format is invalid.  Valid format is "/ssp=userid:process_identifier" ')
	else
		g_ssp_user = mid(g_command_line_args, start_pos + 5, colon_pos - (start_pos + 5))
		g_ssp_process_identifier = longlong(mid(g_command_line_args, colon_pos + 1, end_pos - (colon_pos + 1) + 1))
		
		// Log to a log directory, if it exists, otherwise the same directory with the process_identifier as the file name.
		if not DirectoryExists(g_exe_info.file_path + 'Logs\') then CreateDirectory(g_exe_info.file_path + 'Logs\')
		
		if DirectoryExists(g_exe_info.file_path + 'Logs\') then
			g_log_file = g_exe_info.file_path + 'Logs\' + string(g_ssp_process_identifier) + '.log'
		else
			g_log_file = g_exe_info.file_path + string(g_ssp_process_identifier) + '.log'
		end if
	end if
else
	g_ssp = false
end if

//*****************************************************************************************
//
//  Are we using command line login credentials.
//		Command Line will be of the form "/connection=encryptedconnectionstring" such as "/connection=436883EB6CAED91A"
// 		### 40102: JAK
//
//*****************************************************************************************
conn_SSPID = -11

if pos(g_command_line_args,"/connection=") > 0 then
	start_pos = pos(g_command_line_args,"/connection=") + len('/connection=')
	end_pos = pos(f_replace_string(g_command_line_args,'	',' ','all') + ' ',' ',start_pos)	
		
	if end_pos <= 0 then
		// error
	else
		// get the connection string
		conn = mid(g_command_line_args, start_pos, end_pos - start_pos)
		
		// remove all of the info from the logs
		g_command_line_args = f_replace_string(g_command_line_args,conn,'...','first')
		
		// decrypt the connection string.
		nvo_web_crypto web_crptyo
		web_crptyo = create nvo_web_crypto
		conn = web_crptyo.uf_rc2_decrypt(conn, 'b@n@n@password$plit')
		
		f_parsestringintostringarray(conn,';_',conn_array)
		
		for i = 1 to upperbound(conn_array)
			if mid(conn_array[i],1,4) = 'LID=' then conn_LogID = mid(conn_array[i],5)
			if mid(conn_array[i],1,3) = 'LP=' then conn_LogPass = mid(conn_array[i],4)
			if mid(conn_array[i],1,4) = 'UID=' then  conn_UserID = mid(conn_array[i],5)
			if mid(conn_array[i],1,5) = 'DBMS=' then  conn_DBMS = mid(conn_array[i],6)
			if mid(conn_array[i],1,3) = 'SN=' then  conn_ServerName = mid(conn_array[i],4)
			if mid(conn_array[i],1,6) = 'SSPID=' then  
				if isnumber(mid(TRIM(conn_array[i]),7)) then
					conn_SSPID = longlong(mid(TRIM(conn_array[i]),7))
				end if
			end if
		next

		// The conn_SSPID must match the g_ssp_process_identifier or we can't use these for now
		if conn_SSPID <> g_ssp_process_identifier then
			conn_LogID = ''
			conn_LogPass = ''
			conn_UserID = ''
			conn_DBMS = ''
			conn_ServerName = ''
		end if
	end if
end if

//*****************************************************************************************
//
//  Get the UNC Path for the log file and exe
//
//*****************************************************************************************
path_unc = uf_unc_translate(path)
log_file_unc = uf_unc_translate(g_log_file)


//*****************************************************************************************
//
//  Create the connection to the POWERPLAN instance.
//
//*****************************************************************************************
sqlca.ServerName = g_prog_interface.i_server
sqlca.DBMS       	= g_prog_interface.i_dbms
sqlca.LogID      	= g_prog_interface.i_username
sqlca.LogPass    	= g_prog_interface.i_password
sqlca.UserID 		= sqlca.LogID
sqlca.DBParm		= g_prog_interface.i_dbparm

if g_ssp and pos(lower(sqlca.DBparm),'disablebind') = 0 then
	// DisableBind hasn't been explicitly set in ppcset, then turn it off
	SQLCA.DBParm= SQLCA.DBParm + ",DisableBind=1"
end if

if trim(conn_LogID) <> '' then sqlca.LogID = conn_LogID
if trim(conn_LogPass) <> '' then sqlca.LogPass = conn_LogPass
if trim(conn_UserID) <> '' then sqlca.UserID = conn_UserID
if trim(conn_ServerName) <> '' then sqlca.ServerName = conn_ServerName
 if trim(sqlca.DBMS) = '' then sqlca.DBMS = conn_DBMS  // only use the DBMS if there wasn't an entry in ppcset at all.

if pos(sqlca.UserID,'[') > 0 then
	// trying to use SSO.
	// For login,    LogID has to be proxy[username] and UserID has to be proxy
	//	After login, it will be changed to UserID = proxy[username] and LogID = username
	sqlca.LogID = sqlca.UserID
	sqlca.UserID = mid(sqlca.LogID,1,pos(sqlca.LogID,'[') - 1)
end if

//// For SSP, use proxy functionality to make it run like the original user was running it.
//if g_ssp and lower(sqlca.logid) <> lower(g_ssp_user) then
//	sqlca.LogID = sqlca.LogID + '[' + g_ssp_user + ']'
//end if

if i_exe_name = "ssp_report_gen.exe" then
	uf_process_ldap_connection(sqlca.logid)
end if

connect using sqlca;
	
if sqlca.SQLCode = 0 then
	f_write_log(g_log_file,"************************************************************")
	f_write_log(g_log_file,"  ")
	f_write_log(g_log_file,"Connected to PowerPlan instance. (" + sqlca.ServerName + ")")
	if pos(sqlca.LogID,'[') > 0 then
		// using SSO.  Need to flip the user ids around.
		sqlca.UserID = sqlca.LogID
		sqlca.LogID = mid(sqlca.LogID,pos(sqlca.LogID,'[') + 1, len(sqlca.LogID) - pos(sqlca.LogID,'[') - 1)
	else
		sqlca.UserID = sqlca.LogID
	end if
	
	// ### 6451:  JAK: 2011-01-13:  Starting in Oracle 11g, must set the role even if it is the default role. 
	// SEB (37518) 4/2/14: For SSP, we need to call a different version of uf_set_role that will not try to validate that
	//		the DB user id is the same as the OS user id
	errMsg = ''
	rRtn = sqlca.uf_set_role('pwrplant_role_dev', true, errMsg)

	if isnull(rRtn) or rRtn <> 0 or errMsg <> '' then
		f_write_log(g_log_file, "ERROR: Setting user role: " + errMsg)
		// Set g_db_connected to false so that the interface will fail after logging all of the relevant information
		g_db_connected = false
	else
		g_db_connected = true
	end if
else
	f_write_log(g_log_file,"************************************************************")
	f_write_log(g_log_file,"  ")
	f_write_log(g_log_file, "Error connecting to PowerPlan instance: " + sqlca.SQLErrText)
	f_write_log(g_log_file, "    SQLCODE: " + string(sqlca.sqlcode))
	f_write_log(g_log_file, "    SQLDBCODE: " + string(sqlca.sqldbcode))
end if

f_write_log(g_log_file, "    Server: " + sqlca.ServerName)
f_write_log(g_log_file, "    DBMS:   " + sqlca.DBMS)
f_write_log(g_log_file, "    LogID:  " + sqlca.UserID)
f_write_log(g_log_file, "    DBParm: "+ sqlca.dbparm)
f_write_log(g_log_file, "    Code Last Modified:  " + string(file_written))
f_write_log(g_log_file, "    Interface Path: " + string(path))
if len(path_unc) > 0 then f_write_log(g_log_file, "    Interface Path UNC: " + string(path_unc))
f_write_log(g_log_file, "    Log File Path: " + string(g_log_file))
if len(log_file_unc) > 0 then f_write_log(g_log_file, "    Log File Path UNC: " + string(log_file_unc))
f_write_log(g_log_file, "    OS Version: " + string(os_version))
f_write_log(g_log_file, "    OS User: " + string(user))
f_write_log(g_log_file, "    OS Hostname: " + string(hostname))
if trim(g_command_line_args) <> '' then 	f_write_log(g_log_file, "    Command Line: " + string(g_command_line_args))
f_write_log(g_log_file, " ")		

// Errors connecting.  Stop here.
if not g_db_connected then return g_rtn_failure
	
s_user_info.user_id  = sqlca.logid
select userenv('sessionid') into :s_user_info.session_id from dual; 


//*****************************************************************************************
//
//  Create a separate connection to the POWERPLAN instance for the log.
//		Function gets the process_id and occurrence_id
//
//*****************************************************************************************
rtn = uf_connect_logs()
if rtn < 0 then return g_rtn_failure

// Errors connecting.  Stop here.
if not g_db_connected or not (g_db_logs_connected or i_use_db_logs) then
	f_write_log(g_log_file, "Failed to connect to database")
	f_write_log(g_log_file, "g_db_connected = " + string(g_db_connected) )
	f_write_log(g_log_file, "g_db_logs_connected = " + string(g_db_logs_connected) )
	f_write_log(g_log_file, "i_use_db_logs = " + string(i_use_db_logs) )
	return g_rtn_failure
end if

f_pp_msgs("************************************************************")
f_pp_msgs("  ")
f_pp_msgs("Connected to PowerPlan instance. (" + sqlca.ServerName + ")")
f_pp_msgs( "    Server: " + sqlca.ServerName)
f_pp_msgs( "    DBMS:   " + sqlca.DBMS)
f_pp_msgs( "    LogID:  " + sqlca.UserID)
f_pp_msgs( "    DBParm: "+ sqlca.dbparm) 
f_pp_msgs( "    Code Last Modified:  " + string(file_written))
f_pp_msgs( "    Interface Path: " + string(path))
if len(path_unc) > 0 then f_pp_msgs( "    Interface Path UNC: " + string(path_unc))
f_pp_msgs( "    Log File Path: " + string(g_log_file))
if len(log_file_unc) > 0 then f_pp_msgs( "    Log File Path UNC: " + string(log_file_unc))
f_pp_msgs( "    OS Version: " + string(os_version))
f_pp_msgs( "    OS User: " + string(user))
f_pp_msgs( "    OS Hostname: " + string(hostname))
if trim(g_command_line_args) <> '' then 	f_pp_msgs("    Command Line: " + string(g_command_line_args))
f_pp_msgs( " ")		
f_pp_msgs( "(LOGS) Connected to PowerPlan instance. (" + sqlca.ServerName + ")")
f_pp_msgs( " ")
f_pp_msgs("************************************************************")
f_pp_msgs(" ")


//*****************************************************************************************
//
//  Start the job if we are doing SSP.
//
//*****************************************************************************************
if g_ssp then
	// Remove the /ssp piece from g_command_line_args.
	g_command_line_args = f_replace_string(g_command_line_args,"/ssp=" + g_ssp_user + ":" + string(g_ssp_process_identifier),"","all")
	rtn = g_ssp_nvo.uf_start_job(g_ssp_process_identifier, g_ssp_process, g_ssp_parms, g_ssp_labels)
	if rtn < 0 then 
		commit;
		i_ssp_error = true
		return rtn
	else
		commit;
	end if
end if

// Remove the /connection=... from g_command_line_args.
g_command_line_args = f_replace_string(g_command_line_args,"/connection=...","","all")

//*****************************************************************************************
//
//  Check Running Session ID to see if the interface is already running
//
//*****************************************************************************************
// ### 5719:  Concurrent Processing
select allow_concurrent into :allow_concurrent
from pp_processes 
where process_id = :g_process_id using sqlca;
if isnull(allow_concurrent) or allow_concurrent <> 1 then allow_concurrent = 0

// ### 5719:  Skip this check if allow concurrent = 1
if allow_concurrent <> 1 then
	running_session_id = 0
	
	// ### 9722: JAK: 2012-06-19:  Protection for 2 processes running at the same time and both doing the select before the update.
	//		Lock immediately when the select is done.
	
	select running_session_id into :running_session_id from pp_processes
		where process_id = :g_process_id
		for update wait 60;

	if sqlca.SQLCode <> 0 then
		//  Disconnect and halt.
		f_pp_msgs(' ')
		f_pp_msgs('ERROR: Selecting running_session_id from pp_processes: ' + sqlca.SQLErrText)		
		f_pp_msgs(' ')
		rollback;		
		return g_rtn_failure
	elseif isnull(running_session_id) or running_session_id = 0 then
		//  NOT CURRENTLY RUNNING:  update the record with this session_id.
		update pp_processes 
		set running_session_id = :s_user_info.session_id
		where process_id = :g_process_id;
		
		if sqlca.SQLCode = 0 then
			commit;	
		else
			//  Disconnect and halt.
			f_pp_msgs(' ')
			f_pp_msgs('ERROR: updating pp_processes.running_session_id: ' + sqlca.SQLErrText)	
			f_pp_msgs(' ')	
			rollback;		
			return g_rtn_failure
		end if	
	else		
		//  CURRENTLY RUNNING: disconnect and halt.
		f_pp_msgs(' ')
		f_pp_msgs(' ')
		f_pp_msgs('Another session is currently running this process.')
		f_pp_msgs('  session_id = ' + string(running_session_id))
		f_pp_msgs(' ')
		f_pp_msgs(' ')
		
		//get the rtn value from the system control switch.  If not found default the value to g_rtn_failure
		setnull(str)
		str = f_pp_system_control_company('Interface Return - Running Session', -1)
	
		//Check to see that the rtn_value is a number
		if isnull(str) then str = 'null'
		if isnumber(str) then
			rtn = long(str)
		else
			rtn = g_rtn_failure
			f_pp_msgs(' ')
			f_pp_msgs(' ')
			f_pp_msgs(' Invalid return type - check system control "Interface Return - Running Session"')
			f_pp_msgs(' Defaulting return code to ' + string(g_rtn_failure))
		end if
		return rtn		
	end if
end if


//*****************************************************************************************
//
//  Check for system lock on the interface.
//
//*****************************************************************************************
select system_lock, system_lock_enabled into :system_lock, :i_system_lock_enabled
from pp_processes 
where process_id = :g_process_id using sqlca;
if isnull(system_lock) then system_lock = 0
if isnull(i_system_lock_enabled) then i_system_lock_enabled = 0

if system_lock <> 0 then
	//  The interface is currently locked and cannot be run again.
	f_pp_msgs(' ')
	f_pp_msgs(' ')
	f_pp_msgs('This process is currently locked due to a prior error which must be resolved prior to re-running the interface.')
	f_pp_msgs('  Occurrence ID = ' + string(system_lock))
	f_pp_msgs(' ')
	f_pp_msgs(' ')
	
	//get the rtn value from the system control switch.  If not found default the value to g_rtn_failure
	setnull(str)
	str = f_pp_system_control_company('Interface Return - Running Session', -1)

	//Check to see that the rtn_value is a number
	if isnull(str) then str = 'null'
	if isnumber(str) then
		rtn = long(str)
	else
		rtn = g_rtn_failure
		f_pp_msgs(' ')
		f_pp_msgs(' ')
		f_pp_msgs(' Invalid return type - check system control "Interface Return - Running Session"')
		f_pp_msgs(' Defaulting return code to ' + string(g_rtn_failure))
	end if
	return rtn		
end if


//*****************************************************************************************
//
//  Check the version 
//
//*****************************************************************************************
version_pass = true

//Retrieve the upper of this  system control into a variable 
cntrl_val = upper(f_pp_system_control_company('Interface Version Validation', -1))
if isnull(cntrl_val)  or cntrl_val <> 'NO' then cntrl_val = 'YES'

//Retrieve the Version for this interface from pp_processes interface
setNull(db_version)

select version
into :db_version
from pp_processes
where process_id = :g_process_id;

if isnull(db_version) then db_version = 'Null'
if isnull(i_exe_version) or i_exe_version = "" or i_exe_version = " " then i_exe_version = 'Null'

//Write the version validation system control switch to the online logs
f_pp_msgs('  Version validation system control:  ' + cntrl_val)
f_pp_msgs('')

//If this interface has customizations, verify the custom version(s)
//Check custom PBD version table for this process ID
//If no results, then do not attempt custom code version validation

select count(*)
into :custom_PBD_count
from pp_custom_pbd_versions
where process_id = :g_process_id;

//initialize variables needed in both if-else branches
string library_list
longlong index

if custom_PBD_count > 0 and upper(cntrl_val) = "YES" then
	f_pp_msgs("  There are customizations configured for this interface!")
	f_pp_msgs("  Beginning custom PBD version validation")
	f_pp_msgs("----------")
	
	//initialize necessary variables
	string sqls, str_rtn, db_custom_version, code_custom_version, pbd_name
	longlong length

	//Get the filenames and versions of the custom PBDs associated with this interface in the DB
	//Use a datastore in case this interface has more than one custom PBD returned by the SQL
	uo_ds_top ds_pbd_names
	ds_pbd_names = create uo_ds_top
	sqls ='select pbd_name pbd_name, version version from pp_custom_pbd_versions where process_id = ' + string(g_process_id)
	
	//Retrieve rows from pp_custom_pbd_versions into the datastore and check the return code
	//Set the version_pass to false if we have custom PBDs but can't pull the expected versions from the DB
	str_rtn = f_create_dynamic_ds(ds_pbd_names,'grid',sqls,sqlca,true) 
	if str_rtn <> 'OK' then
		f_pp_msgs('  Error: Failed to retrieve custom PBD list: ' + ds_pbd_names.i_sqlca_sqlerrtext)
		f_pp_msgs('     SQL command: ' + sqls)
		f_pp_msgs('     The interface will exit without running')
		version_pass = false
	end if
	
	//Pull PBD names from code using getLibraryList()
	library_list = getLibraryList()
	//library_list = library_list + ',test_pbd_2.pbd' //add filename for testing purposes
	//library_list = library_list + ',test_pbd_name.pbd' //add filename without adding and building a whole extra PBL to the target

	
	//trim the .exe filepath from the beginning of the library list, leaving us with just PBD filenames
	index = pos(library_list,',')
	library_list = replace(library_list,1,index,'')
	
	//Check PBD names from DB against PBD names from code
	//Pull each name from the datastore and see if it exists in library_list
	//Once checked, remove that PBD from library_list
	//If any names remain in library_list after the for() loop, then there's an unconfigured PBD running
	for i = 1 to ds_pbd_names.rowCount()
		
		//check that the expected PBD name shows up in the library of whats actually running
		if pos(library_list,ds_pbd_names.getItemString(i,1)) = 0 then
			f_pp_msgs('  Error: Failed library list check')
			f_pp_msgs('     Expected custom PBD file ' + ds_pbd_names.getItemString(i,1) + ' but it was not found in the running library for this interface')
			f_pp_msgs('     The interface will exit without running')
			version_pass = false 	
		else
			pbd_name = ds_pbd_names.getItemString(i,1)
			//trim the current PBD name out of the library list
			//remove trailing comma if necessary
			index = pos(library_list,pbd_name)
			if pos(library_list,pbd_name + ',') > 0 then
				library_list = replace(library_list,index,len(pbd_name) + 1,'')
			else
				library_list = replace(library_list,index,len(pbd_name),'')
			end if
			
			// Make sure the pbd is in the same directory
			if not fileexists(g_exe_info.file_path + pbd_name) then
				f_pp_msgs('  ERROR: ' + pbd_name + ' is missing from the following directory: ' + g_exe_info.file_path)
				f_pp_msgs('     The interface will exit without running')
				version_pass = false
			else
				//get the running and expected versions of this PBD and compare them			
				//pull the custom version of this PBD using the uf_getCustomVersion function of the interface
				code_custom_version = g_uo_client.dynamic uf_getcustomversion(pbd_name)
				
				//pull the expected version stored in the DB
				db_custom_version = ds_pbd_names.getItemString(i,2)
				
				if code_custom_version = '' then 
					//an empty string means retrieval failed 
					//either object is missing or a newly added PBD for this interface hasn't been added to the getCustomVersion() function
					f_pp_msgs('  Error: Could not retrieve version of custom PBD: ' + pbd_name + '!')
					f_pp_msgs('     ' + pbd_name + ' is both present and configured in the DB, but a version could not be found')
					f_pp_msgs('     Ensure the correct object containing the custom version is present in the extension PBL for this interface')
					f_pp_msgs('     The interface will exit without running')
					version_pass = false
				elseif db_custom_version <> code_custom_version then
					f_pp_msgs('  Error: Custom code version mismatch for ' + pbd_name +  '!')
					f_pp_msgs('      Expected custom version: ' + db_custom_version)
					f_pp_msgs('      Actual custom version: ' + code_custom_version)
					f_pp_msgs('     The interface will exit without running')
					version_pass = false
				else
					f_pp_msgs(' Version match for custom PBD: ' + pbd_name)
					f_pp_msgs("     Running (code) version: " + code_custom_version)
					f_pp_msgs("     Expected (DB) version: " + db_custom_version)
				end if
			end if
		end if
		
	next
	
	if len(library_list) > 0 then
		f_pp_msgs('  Error: Unconfigured PBD(s) detected!')
		f_pp_msgs('     This interface is running custom code that is not configured in pp_custom_pbd_names')
		f_pp_msgs('     Validation cannot be performed on the following PBDs: ' + library_list)
		f_pp_msgs('     The interface will exit without running')
		version_pass = false
	end if

	f_pp_msgs("  Custom PBD version validation complete")
	f_pp_msgs("----------")
elseif custom_PBD_count = 0 and upper(cntrl_val) = "YES" then
	//need to check if there are custom PBDs running but none configured in DB
	
	//get the library list and trim the exe filepath, leaving only custom PBD names
	//if there are no PBDs present, then index will = 0 and library_list will = the EXE filepath
	library_list = getLibraryList()
	index = pos(library_list,',')
	library_list = replace(library_list,1,index,'')
	
	//check if the trimmed list has anything left with the .PBD extension
	//if not, then library_list is just the exe filepath, so this is a normal interface without custom code
	if len(library_list) > 0 and pos(library_list,'.pbd') > 0 then
		f_pp_msgs('  Error: Unconfigured PBD(s) detected!')
		f_pp_msgs('     This interface is running custom code that is not configured in pp_custom_pbd_names')
		f_pp_msgs('     Validation cannot be performed on the following PBDs: ' + library_list)
		f_pp_msgs('     The interface will exit without running')
		version_pass = false
	end if
else 
	f_pp_msgs("  Custom version validation will not take place")
end if

if i_running_from_source then
	f_pp_msgs('************************************************************')
	f_pp_msgs('  The Interface is running from the Powerbuilder source code!')
	f_pp_msgs('  Validating Database Version with PowerPlan version only!')
	f_pp_msgs('************************************************************')	
	i_exe_version = db_version
end if

f_pp_msgs('  Interface version:  ' + i_exe_version)
f_pp_msgs('  Database version:  ' + db_version)
///Validate
if upper(cntrl_val) = 'YES' then
	if not db_version = i_exe_version or (db_version = 'Null' and i_exe_version = 'Null') then
		f_pp_msgs(' ')
		f_pp_msgs(' ')
		f_pp_msgs('************************************************************')
		f_pp_msgs('  The database version and Interface version do not match')
		f_pp_msgs('  The Interface will exit without running.')
		f_pp_msgs('************************************************************')
		f_pp_msgs(' ')
		f_pp_msgs(' ')
		
		version_pass = false
	else
		// ### 8853: JAK: 2011-10-30:  Version matches pp_processes version.  Need to cross validate to pp_version_exes
		//	to cross validate versions with the PowerPlan application
		//  1.  Check to see if this interface exists in pp_version_exes
		rtn = 0 
		select count(*)
			into :rtn
			from pp_version_exes
			where lower(trim(executable_file)) = lower(trim(:i_exe_name));
			
		if rtn > 0 then
			//  2.  Exe is controlled.  Need PowerPlan version to cross-reference
			//		strip off the "version" / "patch" syntax to just get to the number
			select trim(replace(replace(replace(upper(pp_version),'VERSION',''),'PATCH',''),':','')), trim(replace(replace(replace(upper(pp_patch),'VERSION',''),'PATCH',''),':','')) 
			into :pp_version, :pp_patch
			from pp_version;
			
			if mid(pp_patch,1,4) <> 'NONE' then pp_version = pp_patch
			if pos(pp_version,' ') > 0 then pp_version = mid(pp_version,1,pos(pp_version,' ') - 1)
			
			f_pp_msgs('  PowerPlan Version:  ' + pp_version)
			select executable_version
			into :comp_pp_version
			from pp_version_exes
			where lower(trim(executable_file)) = lower(trim(:i_exe_name))
			and pp_version = :pp_version;
			
			if SQLCA.SQLCode = 100 then
				// No rows found for pp_version
				f_pp_msgs(' ')
				f_pp_msgs(' ')
				f_pp_msgs('************************************************************')
				f_pp_msgs('  No record found in pp_version_exes for PowerPlan version: ' + pp_version)
				f_pp_msgs('  The interface version cannot be verified as compatible.')
				f_pp_msgs('  The Interface will exit without running.')
				f_pp_msgs('************************************************************')
				f_pp_msgs(' ')
				f_pp_msgs(' ')
				
				version_pass = false
			else
				f_pp_msgs('  Compatible Interface Version:  ' + comp_pp_version)
				//###CWB maint 9812 05-14-2012
				//allow exe version to have additional detail
				if len(i_exe_version) > len(comp_pp_version) and &
					(pos(i_exe_version,',') > 0 or pos(i_exe_version,' ') > 0 or pos(i_exe_version,';') > 0 or pos(i_exe_version,':') > 0 or pos(i_exe_version,'-') > 0) then
					i_exe_version = mid(i_exe_version,1,len(comp_pp_version))
				end if
				
				if not comp_pp_version = i_exe_version or (comp_pp_version = 'Null' and i_exe_version = 'Null') then
					f_pp_msgs(' ')
					f_pp_msgs(' ')
					f_pp_msgs('************************************************************')
					f_pp_msgs('  The interface version is not compatible with the current PowerPlan version.')
					f_pp_msgs('  The Interface will exit without running.')
					f_pp_msgs('************************************************************')
					f_pp_msgs(' ')
					f_pp_msgs(' ')
				
					version_pass = false
				end if
			end if
		end if
	end if
end if

if not version_pass then
	setnull(str)
	str = f_pp_system_control_company('Interface Return - Version Mismatch', -1)

	//Check to see that the rtn_value is a number
	if isnull(str) then str = 'null'
	if isnumber(str) then
		rtn = long(str)
	else
		rtn = g_rtn_failure
		f_pp_msgs(' ')
		f_pp_msgs(' ')
		f_pp_msgs(' Invalid return type - check system control "Interface Return - Version Mismatch"')
		f_pp_msgs(' Defaulting return code to ' + string(g_rtn_failure))
	end if
	return rtn
else
	f_pp_msgs('  Version Match Successful')
end if


//*****************************************************************************************
//
// Collect process statistics / attributes
//
//*****************************************************************************************
//g_stats.get_process_stat("Start")      // Disabled in 10.4 until future review
g_stats.set_met_start('Overall')

g_stats.set_attrib('Host', os_version, 'OS Version', 0)
g_stats.set_attrib('Host', user, 'OS User', 0)
g_stats.set_attrib('Host', hostname, 'OS Hostname', 0)

g_stats.set_attrib('Version', string(file_written), 'Code Last Modified', 0)
g_stats.set_attrib('Version', exe_version_orig, 'Version', 0)


return 1
end function

public function longlong uf_disconnect ();longlong os_return_value, rtn
string   s_date
datetime finished_at
date	ddate
time	ttime
string email_recipients, email_subject, email_body, from_user

if not g_db_connected then
	return g_rtn_failure
end if


//*****************************************************************************************
//
// Collect process statistics
//
//*****************************************************************************************
//g_stats.get_process_stat("End")       // Disabled in 10.4 until future review
//g_stats.get_db_stat()        // Disabled in 10.4 until future review
g_stats.set_met_volume('Overall',g_record_count)
g_stats.set_met_end('Overall')
g_stats.insert_to_db_stats()
g_stats.insert_to_db_met()

//*****************************************************************************************
//
//	Select the value that we should be returning to the Operating System from pp_processes_return_values
//
//*****************************************************************************************
setnull(os_return_value)
select os_return_value 
into :os_return_value
from pp_processes_return_values
where process_id = :g_process_id
and return_value = :g_rtn_code;
if isnull(os_return_value)  then os_return_value = g_rtn_code

//	### 8272: CWB: Write OS return to online logs
f_pp_msgs("OS Return Value: " + string(os_return_value))


//*****************************************************************************************
//
//  Complete the job if we are doing SSP.
//
//*****************************************************************************************
if g_ssp then
	if not i_ssp_error then // SSP Error only occurs if the start fails.
		if isnull(g_ssp_return) or trim(g_ssp_return) = '' then g_ssp_return = string(g_rtn_code)
		rtn = g_ssp_nvo.uf_complete_job(g_ssp_process_identifier, string(g_rtn_code), os_return_value)
		commit;
	end if
	
	datetime request_time, start_time, end_time
	string return_code, status, process_descr
	longlong notify_on_complete
	
	setnull(notify_on_complete) 
	g_ssp_nvo.uf_get_job_attribs(g_ssp_process_identifier, g_ssp_process, process_descr, request_time, start_time, end_time, return_code, status, notify_on_complete)
	
	// notify on complete will be driven by database if asynch. sych will be 0
	if notify_on_complete = 1 then
		select async_email_on_complete
		into :notify_on_complete
		from pp_processes
		where process_id = :g_process_id
		;
	end if
	
	if i_ssp_error then 
		status = 'X'
	end if
end if


//*****************************************************************************************
//
//  Update pp_processes to show this session is finished
//  make sure to only update pp_processes if this process is the current running process
//
//*****************************************************************************************
update pp_processes set running_session_id = null
where process_id = :g_process_id
and running_session_id = userenv('sessionid');

if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("ERROR: updating pp_processes.running_session_id: " + 	sqlca.SQLErrText)
	f_pp_msgs(" ")
	f_pp_msgs("The process DID RUN.")
	f_pp_msgs("The the pp_processes table must be updated by hand.")
	f_pp_msgs(" ")
	rollback;
end if





//*****************************************************************************************
//
//	update end time, batch_id, return_value, etc....
//
//*****************************************************************************************
// ### 7598: JAK: 2011-05-17:  After discussion with Jim Ogilivie, use the value in g_batch_id
//		if it is populated.  If it is not populated, overwrite it with the value in g_interface_id
// if process was successful, use this to populate the batch_id field on pp_processes_occurrences	
if isnull(g_batch_id) or trim(g_batch_id) = '' then g_batch_id = string(g_interface_id) 

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
finished_at   = datetime(ddate, ttime)
	
if i_use_db_logs then
//	sqlca.p_end_log(g_batch_id, g_rtn_code) //CBS - don't disconnect yet - wait until right before we disconnect the SQLCA object after the email below. 
else
	update pp_processes_occurrences 
		set end_time = sysdate, batch_id = :g_batch_id, return_value = :g_rtn_code
	 where process_id = :g_process_id and occurrence_id = :g_occurrence_id
	 using g_sqlca_logs;
		
	if g_sqlca_logs.SQLCode = 0 then
		commit using g_sqlca_logs;
	else
		rollback using g_sqlca_logs;
	end if
end if

//*****************************************************************************************
//
//	### 5644: JAK: Emails in standard interface shell
//
//*****************************************************************************************
select email_recipients, nvl(email_subject,description), nvl(email_body,long_description)
	into :email_recipients, :email_subject, :email_body
from pp_processes_return_values
where process_id = :g_process_id
and return_value = :g_rtn_code;

if not isnull(email_recipients) and trim(email_recipients) <> ''  or (g_ssp and notify_on_complete = 1) then
	f_pp_msgs("Sending email notifications at " + string(now()))

	// Need to send an email
	from_user = f_pp_system_control('Alert Email Address')
	
	g_msmail = create uo_smtpmail
	
	if not isnull(email_recipients) and trim(email_recipients) <> '' then
		if g_msmail.send(from_user, "", email_subject, email_body, email_recipients, "")  then
			f_pp_msgs("Email sent to Users(s): " + email_recipients)
		else
			f_pp_msgs("ERROR: sending email to: " + email_recipients)
		end if
	end if
	
	if g_ssp and notify_on_complete = 1 then		
		if isnull(process_descr) or trim(process_descr) = '' then process_descr = i_exe_name
		if isnull(status) or trim(status) = '' then status = 'unknown'
		if isnull(return_code) or trim(return_code) = '' then return_code = 'unknown'
		if isnull(g_process_id) then g_process_id = -1
		if isnull(g_occurrence_id) then g_occurrence_id = -1
		
		if status = 'D' then
			email_subject = 'PowerPlan Process Complete: ' + process_descr
		else
			email_subject = 'PowerPlan Process Error: ' + process_descr
		end if
		
		if status = 'X' then
			email_body = 'The PowerPlan Process ' + process_descr + ' failed to start.<br><br>' + &
				'Please review the online logs for more information.<br>' + &
				'Process ID: ' + string(g_process_id) + '<br>' + &
				'Occurrence ID: ' + string(g_occurrence_id)
		else
			email_body = 'The PowerPlan Process ' + process_descr + ' finished with the following information.<br>' + &
				'Status: ' + status + '<br>' + &
				'Return Code: ' + return_code + '<br>' + &
				'Request Time: ' + string(request_time) + '<br>' + &
				'Start Time: ' + string(start_time) + '<br>' + &
				'End Time: ' + string(end_time) + '<br><br>' + &
				'Please review the online logs for additional details.<br>' + &
				'Process ID: ' + string(g_process_id) + '<br>' + &
				'Occurrence ID: ' + string(g_occurrence_id)
		end if
		email_recipients = g_ssp_user

		if g_msmail.send(from_user, "", email_subject, email_body, email_recipients, "")  then
			f_pp_msgs("Email sent to Users(s): " + email_recipients)
		else
			f_pp_msgs("ERROR: sending email to: " + email_recipients)
		end if
	end if	
end if


//*****************************************************************************************
//
//  Disconnect from PowerPlan Instance ...
//
//*****************************************************************************************
if i_use_db_logs then
	// have to write the disconnect message before disconnecting.
	f_pp_msgs("Disconnecting from PowerPlan instance. (" + sqlca.ServerName + ")")
	sqlca.p_end_log(g_batch_id, g_rtn_code)
end if

disconnect using sqlca;

f_write_log(g_log_file,' ')

if sqlca.SQLCode = 0 then	
	if not i_use_db_logs then f_pp_msgs("Disconnected from PowerPlan instance. (" + sqlca.ServerName + ")")
	f_write_log(g_log_file, "Disconnected from PowerPlan instance. (" + sqlca.ServerName + ")")
else
	if not i_use_db_logs then f_pp_msgs("Error disconnecting from PowerPlan instance. (" + 	sqlca.ServerName + "): " + sqlca.SQLErrText)
	f_write_log(g_log_file, "Error disconnecting from PowerPlan instance. (" + 	sqlca.ServerName + "): " + sqlca.SQLErrText)
end if


//*****************************************************************************************
//
//	disconnect the logs connection
//
//*****************************************************************************************
if not i_use_db_logs then
	g_sqlca_logs.uf_disconnect()
	
	if g_sqlca_logs.SQLCode = 0 then
		f_write_log(g_log_file, "(LOGS) Disconnected from PowerPlan instance. (" + sqlca.ServerName + ")")
	else
		f_write_log(g_log_file, "(LOGS) Error disconnecting from PowerPlan instance. (" + sqlca.ServerName + "): " + g_sqlca_logs.SQLErrText)
	end if
end if

f_write_log(g_log_file, "  ")
f_write_log(g_log_file, "Finished:  " + s_date)
f_write_log(g_log_file, "************************************************************")
f_write_log(g_log_file, "  ")
f_write_log(g_log_file, "  ")

return os_return_value
end function

public function integer uf_connect_logs ();string errMsg
longlong do_counter, rRtn

// SSP_DEPR_CALC uses the logs in the DB.  Hardcode here for now.
if lower(g_uo_client.i_exe_name) = 'ssp_depr_calc.exe' then
	i_use_db_logs = true
	destroy(g_io_func)
	g_io_func = create nvo_pp_func_io_db
end if

	
//*****************************************************************************************
//
//  Find the process and occurrence
//
//*****************************************************************************************
//	Look up the process_id using the exe name
setNull(g_process_id)
select process_id into :g_process_id
from pp_processes 
where lower(trim(executable_file)) = lower(trim(:i_exe_name || ' ' || :i_ssp_process)) using sqlca;

if (isnull(g_process_id) or g_process_id = 0) and len(trim(i_ssp_process)) > 0 then
	// We didn't find a process with i_ssp_process, try the normal exe
	select process_id into :g_process_id
	from pp_processes  
	where lower(trim(executable_file)) = lower(trim(:i_exe_name)) using sqlca;
end if

// if we didn't find anything or what we found was null, then write to the log file and exit
if isnull(g_process_id) or sqlca.sqlcode = 100 then 
	f_write_log(g_log_file,"Could not determine the g_process_id")
	if sqlca.sqlcode = -1 then
		f_write_log(g_log_file, "   SQL Error: " + sqlca.sqlerrtext)
	else
		f_write_log(g_log_file, '   Make sure that a record exists in PP_PROCESSES for executable name "' + lower(trim(i_exe_name)) + '"')
	end if
	return -1
end if


if i_use_db_logs then
	// Start log and return Occurrence ID
	g_occurrence_id = sqlca.f_start_log(g_process_id)
	
	if sqlca.SQLCode = 0 then	
		return 1
	else
		f_write_log(g_log_file, " (LOGS) Error starting logs via PKG_PP_LOG.F_START_LOG: " + sqlca.SQLErrText)
		f_write_log(g_log_file, "    SQLCODE: " + string(sqlca.sqlcode))
		f_write_log(g_log_file, "    SQLDBCODE: " + string(sqlca.sqldbcode))
		return -1
	end if

else	
	//*****************************************************************************************
	//
	//  Create a separate connection to the POWERPLAN instance for the log.
	//
	//*****************************************************************************************
	g_sqlca_logs.uf_connect()
	
	if sqlca.SQLCode <> 0 then
		f_write_log(g_log_file, " (LOGS) Error connecting to PowerPlan instance: " + sqlca.SQLErrText)
		f_write_log(g_log_file, "    SQLCODE: " + string(sqlca.sqlcode))
		f_write_log(g_log_file, "    SQLDBCODE: " + string(sqlca.sqldbcode))
		f_write_log(g_log_file, "    Server: " + sqlca.ServerName)
		f_write_log(g_log_file, "    DBMS:   " + sqlca.DBMS)
		f_write_log(g_log_file, "    LogID:  " + sqlca.LogID)
		f_write_log(g_log_file, "    DBParm: "+ sqlca.dbparm)
	end if
	
	// ### 6451:  JAK: 2011-01-13:  Starting in Oracle 11g, must set the role even if it is the default role.  
	// SEB (37518) 4/2/14: For SSP, we need to call a different version of uf_set_role that will not try to validate that
	//		the DB user id is the same as the OS user id
	errMsg = ''
	rRtn = g_sqlca_logs.uf_set_role('pwrplant_role_dev', true, errMsg)
	
	if isnull(rRtn) or rRtn <> 0 or errMsg <> '' then
		f_write_log(g_log_file, "ERROR: Setting user role: ")
		f_write_log(g_log_file, "   " + errMsg)
		return -1
	end if
	
	// At this point, we have connected to the database via the sqlca object
	// and created a separate connection (g_sqlca_logs) for the online logs
	g_db_logs_connected = true
	
	if g_sqlca_logs.SQLCode = 0 then
		rRtn = g_io_func.of_pp_msgs_start_log(g_process_id, i_exe_version)
		
		if rRtn <> 0 then
			f_write_log(g_log_file, "  ")
			f_write_log(g_log_file, "ERROR: inserting into pp_processes_occurrences: " + g_sqlca_logs.sqlerrtext)
			f_write_log(g_log_file, "  ")
			rollback using g_sqlca_logs;
			return -1
		else
			return 1
		end if
	end if
end if

end function

private function string uf_unc_translate (string a_path);string path_unc, log_file_unc, local_name, remote_name
longlong code
long len = 1000
constant longlong ERROR_NOT_CONNECTED=2250, ERROR_BAD_DEVICE=1200

if Pos(a_path, ':') > 0 then
	// try to do a UNC lookup
	local_name = Upper(Left(a_path, pos(a_path, ':')))
	remote_name = space(len)
	
	code = WNetGetConnection(local_name, remote_name, len)
	
	if code = 0 then
		path_unc = remote_name + mid(a_path, pos(a_path, ':') + 1)
	elseif code = ERROR_NOT_CONNECTED or code = ERROR_BAD_DEVICE then 
		// this error most likely means that they are running from a local drive, not a network
//		path_unc = "N/A (local path, not a network resource)"
		path_unc = ''   // no need to log it if its a local path
	else 
		// 29888: JAK: 2013-05-01: Error code was causing confusion for users.  Really this is a warning. 
	//	path_unc = "Error code " + string(code) + " trying to get UNC path"
		path_unc = "Unable to determine a UNC path (Return Code: " + string(code) + ") "
	end if 
else
	path_unc = ''
end if

return path_unc
end function

public function longlong uf_process_ldap_connection (string a_logid);
string ls_sso_file = "single_sign_on.key"
string ls_current_dir, key_file
blob b
longlong unit, len


ls_current_dir = GetCurrentDirectory()

//see if the sso key file exists there. if not, return 0, else process it
key_file = ls_current_dir + "\" + ls_sso_file

f_pp_msgs("Checking for LDAP key file")

if FileExists(key_file) = false then
	f_pp_msgs("No LDAP key file - " + ls_sso_file + " not found in folder " + ls_current_dir + ". LDAP configuration will not be used for login" )
	return 0
end if

f_pp_msgs("LDAP key file - " + ls_sso_file + " found in folder " + ls_current_dir + ". LDAP configuration will be used for login" )

// Read Key File
unit = fileopen(key_file,StreamMode!,read!)
if unit = -1 then
   MessageBox("Error","Error opening key file " + key_file)
   return 0
end if
len = fileread(unit,b)
fileclose(unit)
n_encrypt n_encrypt

n_encrypt.uf_decrypt(b,len)

sqlca.dbpass = sqlca.logpass
sqlca.logid = n_encrypt.i_key_sso_logid + "[" + a_logid + "]"
sqlca.logpass = n_encrypt.i_key_sso_logpass 

longlong lpos
lpos = Pos(sqlca.servername, ".")

If lpos > 0 Then
	sqlca.servername = Left(sqlca.servername, lpos - 1)
End If

sqlca.database = sqlca.servername

return 1
end function

on uo_ppinterface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ppinterface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

