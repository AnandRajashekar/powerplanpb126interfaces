HA$PBExportHeader$nvo_pp_func_io.sru
forward
global type nvo_pp_func_io from nvo_func_io
end type
end forward

global type nvo_pp_func_io from nvo_func_io
end type
global nvo_pp_func_io nvo_pp_func_io

forward prototypes
public function longlong of_messagebox (string a_msg, string a_msg2[], icon i, button b) throws nvo_exception
end prototypes

public function longlong of_messagebox (string a_msg, string a_msg2[], icon i, button b) throws nvo_exception;longlong idx
string display_msg, msg

display_msg = a_msg2[1]
f_message_translate(a_msg,display_msg)

for idx = 2 to upperbound(a_msg2)
	msg = a_msg2[idx]
	f_message_translate(a_msg,msg)
	display_msg = display_msg + "; " + msg
next

return MessageBox(a_msg,display_msg,i,b,1)
end function

on nvo_pp_func_io.create
call super::create
end on

on nvo_pp_func_io.destroy
call super::destroy
end on

