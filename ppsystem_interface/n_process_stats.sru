HA$PBExportHeader$n_process_stats.sru
forward
global type n_process_stats from nonvisualobject
end type
type us_process_memory_counter from structure within n_process_stats
end type
end forward

type us_process_memory_counter from structure
	longlong	len
	longlong	pagefaultcount
	longlong	peakworkingsetsize
	longlong	workingsetsize
	longlong	quotapeakpagedpoolusage
	longlong	quotapagedpoolusage
	longlong	quotapeaknonpagedpoolusage
	longlong	quotanonpagedpoolusage
	longlong	pagefileusage
	longlong	peakpagefileusage
end type

global type n_process_stats from nonvisualobject
end type
global n_process_stats n_process_stats

type prototypes
function long NtQuerySystemInformation (long infoClass, long Pointer, long bufSize , ref long returnSize) library "ntdll.dll"
function long GetProcessMemoryInfo(long hprocess,ref  us_process_memory_counter mc ,long len  ) library "psapi.dll"
function long GetSystemInfo(long pSystemInfo ) library "kernel32.dll"
function long GetCurrentProcess()  library "kernel32.dll"
function long GetProcessTimes(long hProcess,ref longlong lpCreationTime,ref longlong lpExitTime,ref longlong lpKernelTime,ref longlong lpUserTime)  library "kernel32.dll"








end prototypes

forward prototypes
public function longlong uf_process_info ()
public function longlong uf_get_process_time (ref decimal a_kernel_time, ref decimal a_user_time)
public function longlong uf_get_memory_info (ref longlong a_pagefaultcount, ref longlong a_peakworkingsetsize, ref longlong a_workingsetsize, ref longlong a_quotapeakpagedpoolusage, ref longlong a_quotapagedpoolusage, ref longlong a_quotapeaknonpagedpoolusage, ref longlong a_quotanonpagedpoolusage, ref longlong a_pagefileusage, ref longlong a_peakpagefileusage)
end prototypes

public function longlong uf_process_info ();

return 0
end function

public function longlong uf_get_process_time (ref decimal a_kernel_time, ref decimal a_user_time);longlong status
longlong hProcess
longlong CreationTime
longlong ExitTime
longlong KernelTime
longlong UserTime

hProcess = GetCurrentProcess()


   
status =  GetProcessTimes(hProcess,ref CreationTime,ref ExitTime,ref KernelTime,ref UserTime)

a_kernel_time =  KernelTime / 10000000
a_user_time = UserTime / 10000000
	
return 0
end function

public function longlong uf_get_memory_info (ref longlong a_pagefaultcount, ref longlong a_peakworkingsetsize, ref longlong a_workingsetsize, ref longlong a_quotapeakpagedpoolusage, ref longlong a_quotapagedpoolusage, ref longlong a_quotapeaknonpagedpoolusage, ref longlong a_quotanonpagedpoolusage, ref longlong a_pagefileusage, ref longlong a_peakpagefileusage);longlong process
longlong status
us_process_memory_counter mc
longlong len
nvo_sizeof n_sizeof

len = n_sizeof.sizeof(mc)
process = GetCurrentProcess()
mc.len = len

status = GetProcessMemoryInfo(process,mc ,len  )

a_pagefaultcount = mc.pagefaultcount
a_peakworkingsetsize = mc.peakworkingsetsize
a_workingsetsize = mc. workingsetsize
a_quotapeakpagedpoolusage = mc.quotapeakpagedpoolusage
a_quotapagedpoolusage = mc.quotapagedpoolusage
a_quotapeaknonpagedpoolusage = mc.quotapeaknonpagedpoolusage
a_quotanonpagedpoolusage = mc.quotanonpagedpoolusage
a_pagefileusage = mc.pagefileusage
a_peakpagefileusage = mc.peakpagefileusage

return 0
end function

on n_process_stats.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_process_stats.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

