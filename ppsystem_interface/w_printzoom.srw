HA$PBExportHeader$w_printzoom.srw
$PBExportComments$for new report window
forward
global type w_printzoom from window
end type
type cbx_preview from checkbox within w_printzoom
end type
type em_custom from editmask within w_printzoom
end type
type cbx_rulers from checkbox within w_printzoom
end type
type st_percent from statictext within w_printzoom
end type
type cb_ok from commandbutton within w_printzoom
end type
type cb_cancel from commandbutton within w_printzoom
end type
type rb_custom from radiobutton within w_printzoom
end type
type rb_30 from radiobutton within w_printzoom
end type
type rb_65 from radiobutton within w_printzoom
end type
type rb_100 from radiobutton within w_printzoom
end type
type rb_200 from radiobutton within w_printzoom
end type
type gb_1 from groupbox within w_printzoom
end type
end forward

global type w_printzoom from window
integer x = 827
integer y = 528
integer width = 1211
integer height = 720
boolean titlebar = true
string title = "Print Preview"
windowtype windowtype = response!
long backcolor = 12632256
cbx_preview cbx_preview
em_custom em_custom
cbx_rulers cbx_rulers
st_percent st_percent
cb_ok cb_ok
cb_cancel cb_cancel
rb_custom rb_custom
rb_30 rb_30
rb_65 rb_65
rb_100 rb_100
rb_200 rb_200
gb_1 gb_1
end type
global w_printzoom w_printzoom

type variables
//int ii_zoom
datawindow idw_dw
end variables

event open;string tmp

idw_dw = message.powerObjectParm

tmp = idw_dw.describe( 'DataWindow.Print.Preview DataWindow.Print.Preview.rulers ' + &
								'DataWindow.Print.Preview.Zoom' )

//cbx_preview.checked = ( 'yes' = f_get_token( tmp, '~n' ) )

//Pre-click Print Preview in all the cases, otherwise use the line above
cbx_preview.checked = true
cbx_rulers.checked = ('yes' = f_get_token( tmp, '~n' ) )

choose case tmp
	case '200'
		rb_200.checked = true
		rb_200.triggerevent(clicked!)
	case '100'
		rb_100.checked = true
		rb_100.triggerevent(clicked!)
	case '65'
		rb_65.checked = true
		rb_65.triggerevent(clicked!)
	case '30'
		rb_30.checked = true
		rb_30.triggerevent(clicked!)
	case else
		rb_custom.checked = true
		em_custom.text = tmp
end choose
end event

on w_printzoom.create
this.cbx_preview=create cbx_preview
this.em_custom=create em_custom
this.cbx_rulers=create cbx_rulers
this.st_percent=create st_percent
this.cb_ok=create cb_ok
this.cb_cancel=create cb_cancel
this.rb_custom=create rb_custom
this.rb_30=create rb_30
this.rb_65=create rb_65
this.rb_100=create rb_100
this.rb_200=create rb_200
this.gb_1=create gb_1
this.Control[]={this.cbx_preview,&
this.em_custom,&
this.cbx_rulers,&
this.st_percent,&
this.cb_ok,&
this.cb_cancel,&
this.rb_custom,&
this.rb_30,&
this.rb_65,&
this.rb_100,&
this.rb_200,&
this.gb_1}
end on

on w_printzoom.destroy
destroy(this.cbx_preview)
destroy(this.em_custom)
destroy(this.cbx_rulers)
destroy(this.st_percent)
destroy(this.cb_ok)
destroy(this.cb_cancel)
destroy(this.rb_custom)
destroy(this.rb_30)
destroy(this.rb_65)
destroy(this.rb_100)
destroy(this.rb_200)
destroy(this.gb_1)
end on

type cbx_preview from checkbox within w_printzoom
integer x = 816
integer y = 438
integer width = 344
integer height = 70
integer taborder = 50
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Print Preview"
borderstyle borderstyle = stylelowered!
end type

type em_custom from editmask within w_printzoom
event spun pbm_enchange
integer x = 417
integer y = 454
integer width = 230
integer height = 70
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 16777215
string text = "50"
borderstyle borderstyle = stylelowered!
string mask = "####"
boolean spin = true
double increment = 5
string minmax = "1~~1000"
end type

on spun;rb_custom.checked = true

end on

type cbx_rulers from checkbox within w_printzoom
integer x = 816
integer y = 518
integer width = 344
integer height = 70
integer taborder = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Show Rulers"
borderstyle borderstyle = stylelowered!
end type

type st_percent from statictext within w_printzoom
integer x = 651
integer y = 464
integer width = 59
integer height = 70
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "%"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_printzoom
integer x = 845
integer y = 58
integer width = 282
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "OK"
boolean default = true
end type

event clicked;string tmp

setPointer( Hourglass! )

tmp = "DataWindow.Print.Preview.Zoom=" + em_custom.text + &
		" DataWindow.Print.Preview.rulers= "
if cbx_rulers.checked then 
	tmp = tmp + "yes"
else
	tmp = tmp + 'no'
end if
idw_dw.modify(tmp)

if cbx_preview.checked then
	idw_dw.object.datawindow.print.preview = true
elseif not cbx_preview.checked then
	idw_dw.object.datawindow.print.preview = false
end if
setPointer( Arrow! )
close ( parent )
end event

type cb_cancel from commandbutton within w_printzoom
integer x = 845
integer y = 173
integer width = 282
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Cancel"
boolean cancel = true
end type

on clicked;close ( parent)
end on

type rb_custom from radiobutton within w_printzoom
integer x = 128
integer y = 454
integer width = 274
integer height = 70
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "C&ustom"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

on clicked;em_custom.setfocus()
end on

type rb_30 from radiobutton within w_printzoom
integer x = 128
integer y = 368
integer width = 274
integer height = 70
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "  &30 %"
borderstyle borderstyle = stylelowered!
end type

on clicked;em_custom.text = '30'
end on

type rb_65 from radiobutton within w_printzoom
integer x = 128
integer y = 282
integer width = 274
integer height = 70
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "  &65 %"
borderstyle borderstyle = stylelowered!
end type

on clicked;em_custom.text = '65'
end on

type rb_100 from radiobutton within w_printzoom
integer x = 128
integer y = 195
integer width = 274
integer height = 70
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "&100 %"
borderstyle borderstyle = stylelowered!
end type

on clicked;em_custom.text = '100'
end on

type rb_200 from radiobutton within w_printzoom
integer x = 128
integer y = 109
integer width = 274
integer height = 70
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "&200 %"
borderstyle borderstyle = stylelowered!
end type

on clicked;em_custom.text = '200'

end on

type gb_1 from groupbox within w_printzoom
integer x = 40
integer y = 32
integer width = 728
integer height = 550
integer taborder = 10
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Magnification"
borderstyle borderstyle = stylelowered!
end type

