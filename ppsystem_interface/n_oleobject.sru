HA$PBExportHeader$n_oleobject.sru
$PBExportComments$1092
forward
global type n_oleobject from oleobject
end type
end forward

global type n_oleobject from oleobject
end type
global n_oleobject n_oleobject

type variables
string val
end variables

on n_oleobject.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_oleobject.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event externalexception;action = ExceptionIgnore! //ExceptionSubstituteReturnValue! 
val = ' '
returnvalue = val


end event

event error;
action = ExceptionSubstituteReturnValue! 
returnvalue = ' '
return

end event

