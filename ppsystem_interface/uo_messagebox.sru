HA$PBExportHeader$uo_messagebox.sru
forward
global type uo_messagebox from nonvisualobject
end type
end forward

global type uo_messagebox from nonvisualobject autoinstantiate
end type

forward prototypes
public function longlong messagebox (string as_title, string as_msg, icon i, button b)
public function longlong messagebox (string as_title, string as_msg, icon i)
public function longlong messagebox (string as_title, string as_msg)
public function longlong messagebox (string as_title)
public function longlong messagebox (string as_title, string as_msg, icon i, button b, longlong r)
end prototypes

public function longlong messagebox (string as_title, string as_msg, icon i, button b);return this.MessageBox(as_title,as_msg,i,b,1)
end function

public function longlong messagebox (string as_title, string as_msg, icon i);Return this.messagebox(as_title,as_msg,i,OK!)
end function

public function longlong messagebox (string as_title, string as_msg);Return this.messagebox(as_title,as_msg,information!)
end function

public function longlong messagebox (string as_title);return MessageBox(as_title,'',Information!,ok!,1)
end function

public function longlong messagebox (string as_title, string as_msg, icon i, button b, longlong r);return f_messagebox(as_title,as_msg,i,b,r)
end function

on uo_messagebox.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_messagebox.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

