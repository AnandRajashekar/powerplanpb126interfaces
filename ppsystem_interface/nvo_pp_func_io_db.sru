HA$PBExportHeader$nvo_pp_func_io_db.sru
forward
global type nvo_pp_func_io_db from nvo_pp_func_io
end type
end forward

global type nvo_pp_func_io_db from nvo_pp_func_io
end type
global nvo_pp_func_io_db nvo_pp_func_io_db

forward prototypes
public function longlong of_pp_msgs (string a_msg)
public function longlong of_pp_msgs (string a_msg, string a_pp_error_code, string a_sql_error_code)
public function longlong of_pp_msgs_end_log ()
public function longlong of_pp_msgs_start_log (longlong a_process_id)
public function longlong of_pp_msgs_start_log (longlong a_process_id, string a_exe_version)
end prototypes

public function longlong of_pp_msgs (string a_msg);// Override the of_pp_msgs for DB calls

//******************************************************************************************
//
//  Function     :  of_pp_msgs
//
//  Description  :  Write messages to pp_processes_messages
//
//	JAK 20100113:	Added logic so that if the message is greater than
//							2000 characters the message is still logged just on multiple 
//							lines rather than creating an SQL error and stopping there.
//
//******************************************************************************************
try
	sqlca.p_write_message(a_msg)
catch (throwable t)
	f_write_log(g_log_file,"An error occurred when trying to write a message to the database. Error text: " + t.text)
end try


return sqlca.SQLCode
end function

public function longlong of_pp_msgs (string a_msg, string a_pp_error_code, string a_sql_error_code);//******************************************************************************************
//
//  Function     :  f_pp_msgs

//  Description  :  Write messages to pp_processes_messages while capturing SQL codes and PP codes
//
//	JAK 20100113:	Added logic so that if the message is greater than
//							2000 characters the message is still logged just on multiple 
//							lines rather than creating an SQL error and stopping there.
//
//  C.Shilling 20131008 maint 33041 - change to use DB functions
//
//******************************************************************************************
sqlca.p_write_message(a_msg, a_pp_error_code, a_sql_error_code)

return sqlca.sqlcode
end function

public function longlong of_pp_msgs_end_log ();//******************************************************************************************
//
//  Function     	:	f_pp_msgs_end_log
//
//  Description  	:	End online process log
//
//  Argument(s)	:	None
//
//******************************************************************************************
sqlca.p_end_log()

return sqlca.sqlcode
end function

public function longlong of_pp_msgs_start_log (longlong a_process_id);//******************************************************************************************
//
//  Function     	:  f_pp_msgs_start_log
//
//  Description  	:  Initiate online process log
//
//  Argument(s)	:		a_process_id  -  Process ID to be initiated (Processes are defined
//															and created in table pp_processes)
//
//******************************************************************************************

g_process_id = a_process_id
g_occurrence_id = sqlca.f_start_log(g_process_id)

return sqlca.SQLCode
end function

public function longlong of_pp_msgs_start_log (longlong a_process_id, string a_exe_version);//******************************************************************************************
//
//  Function     	:  f_pp_msgs_start_log
//
//  Description  	:  Initiate online process log
//
//  Argument(s)	:		a_process_id  -  Process ID to be initiated (Processes are defined
//															and created in table pp_processes)
//
//******************************************************************************************

g_process_id = a_process_id
g_occurrence_id = sqlca.f_start_log(g_process_id)

return sqlca.SQLCode
end function

on nvo_pp_func_io_db.create
call super::create
end on

on nvo_pp_func_io_db.destroy
call super::destroy
end on

