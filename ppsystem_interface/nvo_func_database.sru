HA$PBExportHeader$nvo_func_database.sru
forward
global type nvo_func_database from nonvisualobject
end type
end forward

global type nvo_func_database from nonvisualobject
end type
global nvo_func_database nvo_func_database

forward prototypes
public function string of_create_foreign_key_syntax (string table_name, string constraint_name, string new_constraint_name, boolean enabled)
public function longlong of_get_column (ref any results[], string sql)
public function string of_create_index_syntax (string table_name, string index_name)
public function longlong of_get_primary_key_columns (string table_name, ref string column_names[])
public function string of_create_primary_key_syntax (string table_name)
public function string of_create_table_syntax (string table_name)
public function longlong of_create_table_syntax_all (string a_table_name, ref string a_sql[])
public function string of_clean_string (string a_string)
public function longlong of_topkey (string a_table)
end prototypes

public function string of_create_foreign_key_syntax (string table_name, string constraint_name, string new_constraint_name, boolean enabled);string syntax, ref_syntax, column_names[], ref_table_name, ref_column_name
longlong i
any results[]

syntax 		= ''
ref_syntax 	= ''

// get the columns of this table that reference another's PK
g_db_func.of_get_column(results,  	"  SELECT ALL_CONS_COLUMNS.COLUMN_NAME " + &
								"	  FROM ALL_CONSTRAINTS  , ALL_CONS_COLUMNS " + &
								"	 WHERE ALL_CONSTRAINTS.CONSTRAINT_NAME 	= 	ALL_CONS_COLUMNS.CONSTRAINT_NAME AND " + &
								"			 ALL_CONSTRAINTS.CONSTRAINT_NAME 	= 	'" + constraint_name + "'	 AND " + &
								"			 ALL_CONSTRAINTS.TABLE_NAME 		= 	'" + table_name + "' AND " + &
								"			 ALL_CONSTRAINTS.CONSTRAINT_TYPE 	= 	'R' " + &
								"ORDER BY ALL_CONS_COLUMNS.POSITION ")

column_names = results


// select the refs
for i = 1 to upperbound(column_names)			


  SELECT ALL_CONS_COLUMNS.TABLE_NAME,   
         ALL_CONS_COLUMNS.COLUMN_NAME
	 INTO :ref_table_name, :ref_column_name
    FROM ALL_CONS_COLUMNS,   
         ALL_CONSTRAINTS  
   WHERE ALL_CONSTRAINTS.CONSTRAINT_NAME 	= 	ALL_CONS_COLUMNS.CONSTRAINT_NAME AND  
         ALL_CONSTRAINTS.CONSTRAINT_TYPE 	= 	'P' AND  
//			ALL_CONS_COLUMNS.COLUMN_NAME		= 	:column_names[i]  AND
			ALL_CONS_COLUMNS.POSITION			= 	:i AND
         ALL_CONS_COLUMNS.CONSTRAINT_NAME = 
			(
			  SELECT R_CONSTRAINT_NAME  
				 FROM ALL_CONSTRAINTS  
				WHERE CONSTRAINT_NAME 	= 	:constraint_name AND  
						TABLE_NAME 			=	:table_name AND
						CONSTRAINT_TYPE 	= 	'R'
			)   
			;
			
			
	// if this is the first column, build the clause, else add the column name
	if i = 1 then
		if isNull(new_constraint_name) or len(new_constraint_name) = 0 then
			//CDM - add constraint_name
			syntax 	+= 'ALTER TABLE "' + table_name + '" ADD CONSTRAINT ' + constraint_name + ' FOREIGN KEY ("' + column_names[i] + '"'			
		else
			syntax 	+= 'ALTER TABLE "' + table_name + '" ADD CONSTRAINT ' + new_constraint_name + ' FOREIGN KEY ("' + column_names[i] + '"'
		end if
		ref_syntax 	+= 'REFERENCES "' + ref_table_name + '" ("' + ref_column_name + '"'
	else
		syntax 		+= '"' + column_names[i] + '"'
		ref_syntax 	+= '"' + ref_column_name + '"'
	end if


	// if this is the last column, then close paren, else comma
	if i = upperbound(column_names) then
		syntax 		+= ') '
		ref_syntax 	+= ') '
	else
		syntax 		+= ', '
		ref_syntax 	+= ', '
	end if

next

syntax += ref_syntax

if enabled = false then
	syntax += 'disable '
end if

return syntax
end function

public function longlong of_get_column (ref any results[], string sql);longlong count
any temp
any empty_array[]

results = empty_array

count = 0


PREPARE sqlsa from :sql;

DESCRIBE sqlsa INTO sqlda ;

DECLARE curs DYNAMIC CURSOR FOR sqlsa;

OPEN DYNAMIC curs USING DESCRIPTOR sqlda ;

DO

	FETCH curs USING DESCRIPTOR sqlda ;

	// check that we actually get results
	if upperbound(sqlda.outParmType) > 0 then 
	
		CHOOSE CASE sqlda.outParmType[1]
				CASE TypeString!
					temp = GetDynamicString(sqlda, 1)
				CASE TypeInteger!, TypeReal!, TypeLong!, TypeDecimal!, TypeDouble!, TypeReal!
					temp = GetDynamicNumber(sqlda, 1)
				CASE TypeDate!
					temp = GetDynamicDate(sqlda, 1)
				CASE TypeDateTime!
					temp = GetDynamicDateTime(sqlda, 1)
				CASE TypeTime!
					temp = GetDynamicTime(sqlda, 1)
		END CHOOSE
		
		if sqlca.sqlcode = 0 then
			count ++
			results[count] = temp
		end if

	end if
	
LOOP WHILE SQLCA.sqlcode = 0

CLOSE curs;

return count
end function

public function string of_create_index_syntax (string table_name, string index_name);string syntax, column_names[], ref_table_name, ref_column_name, tablespace, owner, temporary, sqls, column_position, column_name, column_expression, index_owner, uniqueness, partitioned
longlong i
any results[]

syntax 		= ''

table_name = upper(trim(table_name))
index_name = upper(trim(index_name))
syntax = ''
tablespace = ''
owner = ''

// If the table_name has the owner built into it, parse it out
if pos(table_name,'.') > 0 then
	owner = mid(table_name,1,pos(table_name,'.') - 1)
	table_name = mid(table_name,pos(table_name,'.') + 1, 100)
end if

// If they didn't provide the user, assume it's the current user
if isnull(owner) or trim(owner) = '' then	owner = upper(trim(SQLCA.UserID))
	
select user, tablespace_name, temporary
	into :owner, :tablespace, :temporary
	from all_tables 
	where owner = :owner 
	and table_name = :table_name;

// If we didn't find a table by that owner / table name, check for a public synonym
if sqlca.sqlnrows = 0 then
	select table_owner, table_name 
		into :owner, :table_name
		from all_synonyms 
		where owner = 'PUBLIC' 
		and synonym_name = :table_name;
	
	if sqlca.sqlnrows = 0 then
		// Can't find the table they are after...
		return 'ERROR: NO TABLE FOUND (' + trim(owner + ' ' + table_name) + ')'
	end if
	
	select user, tablespace_name, temporary
		into :owner, :tablespace, :temporary
		from all_tables 
		where owner = :owner 
		and table_name = :table_name;
		
	if sqlca.sqlnrows = 0 then
		// Can't find the table they are after...
		return 'ERROR: NO TABLE FOUND (' + trim(owner + ' ' + table_name) + ')'
	end if
end if
	
sqls = "select a.owner, a.index_name, a.index_type, a.partitioned, a.uniqueness, a.tablespace_name, b.column_position, b.column_name, e.column_expression "
sqls += "from all_indexes a, all_ind_columns b, all_ind_expressions e "
sqls += "where a.index_name = '" + index_name + "' "
sqls += "and a.owner = b.index_owner "
sqls += "and a.index_name = b.index_name "
sqls += "and a.table_owner = '" + owner + "' "
sqls += "and a.table_name = b.table_name "
sqls += "and a.table_name = '" + table_name + "' "
sqls += "and e.index_owner (+) = b.index_owner "
sqls += "and e.index_name (+) = b.index_name "
sqls += "and e.table_owner (+) = b.table_owner "
sqls += "and e.table_name (+) = b.table_name "
sqls += "and e.column_position (+) = b.column_position "
sqls += "order by a.index_name, column_position "

uo_ds_top ds_indexes
ds_indexes = create uo_ds_top
g_ds_func.of_create_dynamic_ds(ds_indexes,'grid',sqls,sqlca,true)

if ds_indexes.rowcount() = 0 then return "ERROR: NO INDEX FOUND (Table: " + trim(owner + ' ' + table_name) + "    Index: " + index_name + ")"

for i = 1 to ds_indexes.rowcount()
	column_position = ds_indexes.getitemstring(i,'column_position')
	column_name = ds_indexes.getitemstring(i,'column_name')
	column_expression = ds_indexes.getitemstring(i,'column_expression')
	
	if i = 1 then
		index_owner = ds_indexes.getitemstring(i,'owner')
		index_name = ds_indexes.getitemstring(i,'index_name')
		uniqueness = ds_indexes.getitemstring(i,'uniqueness')
		
		syntax = "CREATE "
		if uniqueness = "UNIQUE" then 
			syntax = "CREATE UNIQUE INDEX "
		else
			syntax = "CREATE INDEX "
		end if
		
		if not isnull(index_owner) and len(index_owner) > 0 then
			syntax += '"' + index_owner + '".'
		end if
		syntax += index_name + " on "
		
		if not isnull(owner) and len(owner) > 0 then
			syntax += '"' + owner + '".'
		end if
		syntax += table_name + " ("
	end if
	
	if i = ds_indexes.rowcount() then
		partitioned = ds_indexes.getitemstring(i,'partitioned')
		tablespace = ds_indexes.getitemstring(i,'tablespace_name')
		
		if not isnull(column_expression) and len(column_expression) > 0 then
			syntax += column_expression + ') '
		else
			syntax += '"' + column_name + '") '
		end if
		
		if not isnull(tablespace) and len(tablespace) > 0 then
			syntax += ' tablespace ' + tablespace
		end if
		if partitioned = 'YES' then
			syntax += ' local'
		end if
		
	else 
		if not isnull(column_expression) and len(column_expression) > 0 then
			syntax += column_expression + ', '
		else
			syntax += '"' + column_name + '", '
		end if
	end if
next

return syntax
end function

public function longlong of_get_primary_key_columns (string table_name, ref string column_names[]);longlong count
string sql
any results[]

// null out the array
column_names = results

// determine the PK columns
sql = 	"select column_name " + &
			"from all_cons_columns " + &
			"where constraint_name = ( " + &
			"		select constraint_name " + &
			"		from all_constraints where owner = 'PWRPLANT'  " + &
			"		and constraint_type = 'P' " + &
			"		and table_name = '" + table_name + "' " + &
			"		) " + &
			"and table_name = '" + table_name + "' " + &
			"and owner = 'PWRPLANT' "+ &
			"order by position "
			
count = g_db_func.of_get_column(results, sql)

if count > 0 then
	column_names = results
end if

return count
end function

public function string of_create_primary_key_syntax (string table_name);string syntax, col_names[], tablespace, owner, temporary, constraint_name, index_tablespace
longlong i, ret

table_name = upper(trim(table_name))
syntax = ''
tablespace = ''
owner = ''

// If the table_name has the owner built into it, parse it out
if pos(table_name,'.') > 0 then
	owner = mid(table_name,1,pos(table_name,'.') - 1)
	table_name = mid(table_name,pos(table_name,'.') + 1, 100)
end if

// If they didn't provide the user, assume it's the current user
if isnull(owner) or trim(owner) = '' then	owner = upper(trim(SQLCA.UserID))
	
select user, tablespace_name, temporary
	into :owner, :tablespace, :temporary
	from all_tables 
	where owner = :owner 
	and table_name = :table_name;

// If we didn't find a table by that owner / table name, check for a public synonym
if sqlca.sqlnrows = 0 then
	select table_owner, table_name 
		into :owner, :table_name
		from all_synonyms 
		where owner = 'PUBLIC' 
		and synonym_name = :table_name;
	
	if sqlca.sqlnrows = 0 then
		// Can't find the table they are after...
		return 'ERROR: NO TABLE FOUND (' + trim(owner + ' ' + table_name) + ')'
	end if
	
	select user, tablespace_name, temporary
		into :owner, :tablespace, :temporary
		from all_tables 
		where owner = :owner 
		and table_name = :table_name;
		
	if sqlca.sqlnrows = 0 then
		// Can't find the table they are after...
		return 'ERROR: NO TABLE FOUND (' + trim(owner + ' ' + table_name) + ')'
	end if
end if

select a.constraint_name, b.tablespace_name 
	into :constraint_name, :index_tablespace
	from all_constraints a, all_indexes b 
	where a.owner = :owner
	and a.table_name = :table_name
	and a.constraint_type = 'P'
	and b.table_name (+) = a.table_name
	and b.owner (+) = a.owner
	and b.index_name (+) = a.index_name;

if sqlca.sqlnrows = 0 then 
	// No primary key on the table ...
	return 'ERROR: NO PRIMARY KEY FOUND (' + trim(owner + ' ' + table_name) + ')'
end if

syntax = 'ALTER TABLE '

// check for a owner
if not isNull(owner) and len(owner) > 0 then
	syntax += '"' + owner + '".'
end if
syntax += '"' + table_name + '" '

if not isnull(constraint_name) and len(constraint_name) > 0 and mid(constraint_name,1,5) <> 'SYS_C' then
	syntax += ' ADD (CONSTRAINT ' + constraint_name + ' PRIMARY KEY ('
else
	syntax += ' ADD (PRIMARY KEY ('
end if

//alter table jak_temp add (primary key (control_id) using index tablespace pwrplant_idx);
//alter table jak_temp add (constraint jak_temp_pk primary key (control_id) using index tablespace pwrplant_idx);
ret = g_db_func.of_get_primary_key_columns(table_name, col_names)

for i = 1 to upperbound(col_names)
	syntax += '"' + col_names[i] + '"'
	
	if i = upperbound(col_names) then
		syntax += ') '
	else
		syntax += ', '
	end if
next

if isNull(index_tablespace) or len(index_tablespace) = 0 then
	syntax += ') '
else
	syntax += ' using index tablespace ' + index_tablespace + ') '
end if

return syntax
end function

public function string of_create_table_syntax (string table_name);string syntax, col_names[], tablespace, owner, data_type, nullable, temporary, duration
longlong j, ret, data_length, data_precision, data_scale
any results[]

table_name = upper(trim(table_name))
syntax = ''
tablespace = ''
owner = ''

// If the table_name has the owner built into it, parse it out
if pos(table_name,'.') > 0 then
	owner = mid(table_name,1,pos(table_name,'.') - 1)
	table_name = mid(table_name,pos(table_name,'.') + 1, 100)
end if

// If they didn't provide the user, assume it's the current user
if isnull(owner) or trim(owner) = '' then	owner = upper(trim(SQLCA.UserID))
	
// ### 7084: JAK: 2011-03-18:  For global temp tables also need to check the duration.
select user, tablespace_name, temporary, duration
	into :owner, :tablespace, :temporary, :duration
	from all_tables 
	where owner = :owner 
	and table_name = :table_name;

// If we didn't find a table by that owner / table name, check for a public synonym
if sqlca.sqlnrows = 0 then
	select table_owner, table_name 
		into :owner, :table_name
		from all_synonyms 
		where owner = 'PUBLIC' 
		and synonym_name = :table_name;
	
	if sqlca.sqlnrows = 0 then
		// Can't find the table they are after...
		return 'ERROR: NO TABLE FOUND (' + trim(owner + ' ' + table_name) + ')'
	end if
	
	// ### 7084: JAK: 2011-03-18:  For global temp tables also need to check the duration.
	select user, tablespace_name, temporary, duration
		into :owner, :tablespace, :temporary, :duration
		from all_tables 
		where owner = :owner 
		and table_name = :table_name;
		
	if sqlca.sqlnrows = 0 then
		// Can't find the table they are after...
		return 'ERROR: NO TABLE FOUND (' + trim(owner + ' ' + table_name) + ')'
	end if
end if
	
// begin the syntax
if temporary = 'Y' then 
	syntax += 'CREATE GLOBAL TEMPORARY TABLE ' 
else
	syntax += 'CREATE TABLE ' 
end if

// check for a tablespace name
if not isNull(owner) and len(owner) > 0 then
	syntax += '"' + owner + '".'
end if
syntax += '"' + table_name + '" '

// get the column list and loop over it
ret = g_db_func.of_get_column(results, "SELECT column_name FROM all_tab_columns WHERE owner = '" + owner + "' and table_name = '" + table_name + "' ORDER BY column_id")
col_names = results

for j = 1 to upperbound(col_names)
	
	// open paren
	if j = 1 then syntax += '('
	
	select data_type,  data_length,  data_precision,  data_scale,  nullable
	into  :data_type, :data_length, :data_precision, :data_scale, :nullable
	from   all_tab_columns
	where  owner = :owner
	and 	table_name  = :table_name
	and    column_name =  :col_names[j]
	;
	
	if sqlca.sqlcode = -1 then
		setNull(syntax)
		return syntax
	end if
		
	syntax += '"' + col_names[j] + '" '
	syntax += data_type
	
	
	// determine the column type
	if data_type = 'VARCHAR2' or data_type = 'VARCHAR' or data_type = 'CHAR' then
		syntax += '(' + string(data_length) + ')'
		
	elseif data_type = 'NUMBER' then
		if isNull(data_precision) or isNull(data_scale) then
			syntax += '(' + string(data_length) + ')'
		else
			syntax += '(' + string(data_precision) + ', ' + string(data_scale) + ')'
		end if
		
	elseif data_type = 'DATE' then
		syntax += ''
		
	elseif data_type = 'BLOB' then
		syntax += ''
		
	elseif data_type = 'LONG' then
		syntax += ''
		
	elseif data_type = 'RAW' then
		syntax += ''
		
	else
		
	end if
	
	
	if nullable = 'N' then
		syntax += ' NOT NULL'
	end if
	
	
	// close paren or comma
	if j = upperbound(col_names) then 
		syntax += ')'
	else
		syntax += ', '
	end if
	
next

// ### 7084: JAK: 2011-03-18:  For global temp tables also need to check the duration.
if temporary = 'Y' and duration = 'SYS$SESSION' then 
	syntax += ' on commit preserve rows '
end if

// check for a tablespace name
if not isNull(tablespace) and len(tablespace) > 0 then
	syntax += ' tablespace ' + tablespace
end if

return syntax
end function

public function longlong of_create_table_syntax_all (string a_table_name, ref string a_sql[]);string owner, table_name, null_array[], new_cons_name
longlong j, z, counter
any results[]

// Null out the existing array
a_sql = null_array

// Get the explicit table name / owner first.
a_table_name = upper(trim(a_table_name))

// If the a_table_name has the owner built into it, parse it out
if pos(a_table_name,'.') > 0 then
	owner = mid(a_table_name,1,pos(a_table_name,'.') - 1)
	table_name = mid(a_table_name,pos(a_table_name,'.') + 1, 100)
else
	owner = upper(trim(SQLCA.UserID))
	table_name = a_table_name
end if
	
select count(*)
	into :counter
	from all_tables 
	where owner = :owner 
	and table_name = :table_name;

// If we didn't find a table by that owner / table name, check for a public synonym
if counter = 0 then
	select table_owner, table_name 
		into :owner, :table_name
		from all_synonyms 
		where owner = 'PUBLIC' 
		and synonym_name = :table_name;
	
	if sqlca.sqlnrows = 0 then
		// Can't find the table they are after...
		return -1
	end if
	
	select count(*)
		into :counter
		from all_tables 
		where owner = :owner 
		and table_name = :table_name;
		
	if counter = 0 then
		// Can't find the table they are after...
		return -1
	end if
end if

// Table Definition
z = 1
a_sql[z] = g_db_func.of_create_table_syntax(owner + '.' + table_name )
if mid(a_sql[z],1,5) = 'ERROR' then
	return -1
end if

// Primary Key
z++
a_sql[z] = g_db_func.of_create_primary_key_syntax(owner + '.' + table_name)

if mid(a_sql[z],1,5) = 'ERROR' then
	if mid(a_sql[z],1,27) = 'ERROR: NO PRIMARY KEY FOUND' then
		// Don't stop for this error.
		setnull(a_sql[2])
		z = 1
	else
		return -1
	end if
end if

// Foreign Keys
g_db_func.of_get_column(results, "SELECT distinct CONSTRAINT_NAME FROM all_CONSTRAINTS WHERE OWNER = '" + owner + "' AND TABLE_NAME = '" + table_name + "' AND CONSTRAINT_TYPE = 'R'")
for j = 1 to upperbound(results)
	if mid(results[j],1,5) = 'SYS_C' then
		// System generated constraint name
		new_cons_name = ''
	else
		new_cons_name = results[j]
	end if 
	
	z++
	a_sql[z] = g_db_func.of_create_foreign_key_syntax(table_name, results[j], new_cons_name, true)
	
	if mid(a_sql[z],1,5) = 'ERROR' then
		return -1
	end if
next
	
	
// Indexes
g_db_func.of_get_column(results, "SELECT distinct index_name FROM all_indexes a WHERE OWNER = '" + owner + "' AND TABLE_NAME = '" + table_name + "'" + &
	"and not exists  " + &
	"	(select 1 from all_constraints c " + &
	"	where c.table_name = a.table_name " + &
	"	and c.owner = a.owner " + &
	"	and c.constraint_type = 'P' " + &
	"	and c.index_name = a.index_name) ")
	
for j = 1 to upperbound(results)	
	z++
	a_sql[z] = g_db_func.of_create_index_syntax(owner + '.' + table_name, results[j])
	
	if mid(a_sql[z],1,5) = 'ERROR' then
		return -1
	end if
next

return z
end function

public function string of_clean_string (string a_string);/************************************************************************************************************************************************************ 
 **
 ** of_clean_string
 ** Clean string to prevent database update/insert errors
 ** Parameters: a_string to clean
 ** Return: string - clean string with escape characters
 **
 ************************************************************************************************************************************************************/

string rtn

rtn = g_string_func.of_replace_string( a_string, "'", "''", "all" )

return rtn
end function

public function longlong of_topkey (string a_table);//Gets the next system id from a sequence that has the same name as a_table

string sqls
longlong newkey

DECLARE key_crsr  DYNAMIC CURSOR FOR SQLSA ;
sqls = "select sq_" + right(a_table, len(a_table) - 3 ) + ".nextval from dual"
PREPARE SQLSA FROM :sqls;
if sqlca.sqlcode <> 0 then
	//changeme
	messagebox('Sql Error',sqlca.sqlerrtext)
end if

OPEN DYNAMIC key_crsr ;
if sqlca.sqlcode <> 0 then
	//changeme
	messagebox('Sql Error',sqlca.sqlerrtext)
end if

FETCH key_crsr INTO :newkey ;
if sqlca.sqlcode <> 0 then
	//changeme
	messagebox('Sql Error',sqlca.sqlerrtext)
end if
CLOSE key_crsr ;

return newkey


end function

on nvo_func_database.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_func_database.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

