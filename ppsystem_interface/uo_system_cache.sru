HA$PBExportHeader$uo_system_cache.sru
forward
global type uo_system_cache from nonvisualobject
end type
end forward

global type uo_system_cache from nonvisualobject
end type
global uo_system_cache uo_system_cache

type variables
uo_ds_top i_ds_system_control_company
uo_ds_top i_ds_required_fields

////
////	The transaction object we'll use to update statistics table
////
		uo_sqlca_logs i_sqlsa
		
////
////	The datastore used to hold the session stats logging
////		
		datastore i_ds_stats_logging
		
		longlong i_session_log_seq, i_session_id
		string i_user_id
		
end variables

forward prototypes
public subroutine uf_retrieve_from_db ()
public function string uf_get_system_control (string a_control_name)
public function string uf_get_system_control_company (string a_control_name, longlong a_company_id)
public subroutine uf_refresh_cache ()
public function longlong uf_stats_log_start ()
public function integer uf_stats_log_insert ()
public function integer uf_stats_log_end ()
public function integer uf_stats_log (string a_object_name, string a_logged_event)
public function string uf_get_system_control_company_exact (string a_control_name, longlong a_company_id)
end prototypes

public subroutine uf_retrieve_from_db ();string sqls, rtn

////
////	Populate system control cache, if it doesn't exist yet
////
		if not isvalid(i_ds_system_control_company) then 
			//PP_SYSTEM_CONTROL_COMPANY
			i_ds_system_control_company = create uo_ds_top
			sqls = "select * from pp_system_control_company" 
			rtn = f_create_dynamic_ds(i_ds_system_control_company, "grid", sqls, sqlca, true)
			
			if rtn <> "OK" then
				messagebox('Error Creating Cache', "ERROR: Creating Cache: " + rtn)
				destroy i_ds_system_control_company
			end if
		end if
		
////
////	Populate required fields cache, if it doesn't exist yet
////
		if not isvalid(i_ds_required_fields) then 
			//PP_REQUIRED_TABLE_COLUMN
			i_ds_required_fields = create uo_ds_top
			sqls = "select objectpath, ' ' table_name, column_name, description, required_column_expression from pwrplant.pp_required_table_column"
			rtn = f_create_dynamic_ds(i_ds_required_fields, "grid", sqls, sqlca, true)
			
			if rtn <> "OK" then
				messagebox('Error Creating Cache', "ERROR: Creating Required Fields Cache: " + rtn)
				destroy i_ds_required_fields
			end if
		end if

end subroutine

public function string uf_get_system_control (string a_control_name);return uf_get_system_control_company(a_control_name, -1)
end function

public function string uf_get_system_control_company (string a_control_name, longlong a_company_id);return lower(uf_get_system_control_company_exact(a_control_name, a_company_id))
end function

public subroutine uf_refresh_cache ();destroy i_ds_system_control_company
uf_retrieve_from_db()
end subroutine

public function longlong uf_stats_log_start ();/********************************************************************************************
 **	
 **	uf_stats_log_start()
 **	
 **	1.  Set up the datastore for the logging to cache throughout the application's usage.  Do quick validation.
 **	2.  To match useage of existing function, not passing
 **	back an error message to be used by a messagebox.  Just opening the messagebox directly.
 **	
 **	Returns		:	integer		:	-1 	:	an error occurred
 **											1	:	everything worked
 **	
 ********************************************************************************************/

longlong rtn

////
////	Quick validations
////
		if isnull( s_user_info.user_id ) then
			Messagebox("Stats Log Start Error", "No user id available to select from system user structure!" ) 
			return -1 
		end if
		
		if isnull( s_user_info.session_id ) then
			Messagebox("Stats Log Start Error", "No session id available to select from system user structure!" ) 
			return -1 
		end if 
		
////
////	Set the session logging sequence to be 1, set the other i variables to be used throughout the logging.
////
		i_session_log_seq = 1
		i_user_id = s_user_info.user_id
		i_session_id = s_user_info.session_id
		
////
////	Set up the datastore used to hold the session's statistics logs.  Besure to set trans as the new trans object
////
		i_ds_stats_logging = create datastore
		rtn = i_ds_stats_logging.reset()
		i_ds_stats_logging.dataobject = trim( 'dw_pp_stats_logging' )
		rtn = i_ds_stats_logging.insertRow( 0 ) ///insert row at the end (row 1 at start).
		
		rtn = i_ds_stats_logging.setItem( i_session_log_seq, "users", i_user_id)
		rtn = i_ds_stats_logging.setItem( i_session_log_seq, "sessionid", i_session_id)
		rtn = i_ds_stats_logging.setItem( i_session_log_seq, "seq", i_session_log_seq)
		rtn = i_ds_stats_logging.setItem( i_session_log_seq, "object_name", 'uo_system_cache')
		rtn = i_ds_stats_logging.setItem( i_session_log_seq, "logged_event", 'Initialization of Logging')
		rtn = i_ds_stats_logging.setItem( i_session_log_seq, "logged_time", datetime(today(),now()) )
		
////
////	Everything was good, so return 1.
////
		return 1
		
end function

public function integer uf_stats_log_insert ();/********************************************************************************************
 **	
 **	uf_stats_log_insert()
 **	
 **	1.  Connect to the database
 **	2.  Insert the rows from logging datastore to the database
 **	3.  commit using the instantiated trans object.
 **	4.  reset the datastore
 **	5.  disconnect from database.
 **	
 **	Returns		:	integer		:	-1 	:	an error occurred
 **											1	:	everything worked good
 **	
 ********************************************************************************************/

/*
 *	Processing.
 */
	longlong	rtn, insert_err_check, i 
	string sqls, rtn_str

/*
 *	Role variables.
 */
 	longlong	code
	string	my_role, default_role
	string	key, roles, cmd, passwd
	string	dev_role, all_roles, rdonly_role

////
////	Make sure there is a current sequence (set in the "start" function).  If it cannot be found, the log function was
////	called without having the start function called first and this is not allowed.
////
		if isNull( i_session_log_seq ) then return 1

////
////	Connect with a different transaction object
////
		i_sqlsa.dbparm = sqlca.dbparm
		
		rtn = i_sqlsa.uf_connect()
			
		if rtn <> 0 then
			//Messagebox("Stats Log Insert Error", "Error connecting stats log transaction object to PowerPlant ( " + i_sqlsa.serverName + ").~n~nError Text: " + i_sqlsa.SQLErrText ) 
			return -1
		end if

////
////	Now, make sure the roles are set up correctly.
////
////
////	Get the default roles.
////
		roles = ""
		cmd = "getrole"
		dev_role = "pwrplant_role_dev"
		rdonly_role = "pwrplant_role_rdonly"
		
		DECLARE role_cur CURSOR FOR
			SELECT		granted_role, default_role
			FROM			user_role_privs
			WHERE		username <> 'PUBLIC'
				AND		granted_role <> upper( :dev_role )
			USING		i_sqlsa;
		
		OPEN role_cur;
		if i_sqlsa.SQLCode <> 0 then
			roles = ""
		else
			FETCH	role_cur
				INTO	:my_role, :default_role;
			
			do while	( i_sqlsa.SQLCode = 0 )
				if i_sqlsa.SQLCode = 0 and default_role = 'YES' then
					roles = roles + my_role + ","
				end if
				
				FETCH	role_cur
					INTO	:my_role, :default_role;
			loop
			
			CLOSE role_cur;
		end if
		
////
////	Now, set the roles.
////
		key = 'PWRPLANT42PPC'
		code = 0
		passwd = space(1024)
		code = i_sqlsa.pwrplant_admin(cmd, key, passwd, "", "", "", "")
		
		if i_sqlsa.SQLCode <> 0 or code <> 0 then
			//Messagebox("Stats Log Insert Error", "ORACLE error getting role information.~n~nError = " + i_sqlsa.SQLErrText + "~nError Code = " + string( i_sqlsa.SQLDBCode ) ) 
			code = -1
		else
			all_roles =  roles  + dev_role
			i_sqlsa.set_role( all_roles + " identified by " + passwd )
			if i_sqlsa.sqlcode <> 0 then
				//try the old way
				cmd = "begin dbms_session.set_role(~'" + all_roles + " identified by " + passwd + "~'); end;"
				execute immediate :cmd using i_sqlsa; 
				
				if i_sqlsa.sqlcode <> 0 then 
					if s_user_info.user_id <> 'PWRPLANT' and i_sqlsa.sqldbcode = 1924 then 
						//pwrplant_role_dev is not assigned to the user... try pwerplant_role_rdonly
						all_roles =  roles  + rdonly_role
						i_sqlsa.set_role( all_roles + " identified by " + passwd )
						if i_sqlsa.sqlcode <> 0 then
							//try the old way
							cmd = "begin dbms_session.set_role(~'" + all_roles + " identified by " + passwd + "~'); end;"
							execute immediate :cmd using i_sqlsa; 
							if i_sqlsa.sqlcode <> 0 then 
								//Messagebox("Stats Log Insert Error", "Error occurred while setting ORACLE roles.~n~nSet Role Error: " + i_sqlsa.SQLErrText + "~n~n" ) 
								return -1
							end if
						end if
					else	
						//Messagebox("Stats Log Insert Error", "Error occurred while setting ORACLE roles.~n~nSet Role Error: " + i_sqlsa.SQLErrText + "~n~n" ) 
						return -1
					end if 
				end if
			end if
		end if	

////
////	Set datastore to connect with new transaction object
////
		rtn = i_ds_stats_logging.setTransObject( i_sqlsa )
		
		if rtn < 1 then
			//Messagebox("Stats Log Insert Error", "Error connecting stats log transaction object to PowerPlant ( " + i_sqlsa.serverName + ").~n~nError Text: " + i_sqlsa.SQLErrText ) 
			return -1
		end if
			
////
////	Instead of looping through the datastore rows and doing an insert, just call the update function directly from the
////	datastore.  Its trans object is the i_sqlsa.
////
		rtn = i_ds_stats_logging.update()
		
		if rtn < 1 then
			//Messagebox("Stats Log Insert Error", "Error on updating statistics to database." ) 
			rollback using i_sqlsa;
			return -1
		else
			commit using i_sqlsa;
		end if 
	
////
////	Disconnect the trans object.
////
		rtn = i_sqlsa.uf_disconnect()
		
		if rtn <> 0 then
			//messagebox("Stats Log Insert Error", "Error disconnecting stats log transaction object from PowerPlant ( " + i_sqlsa.serverName + ").~n~nError text = " + i_sqlsa.SQLErrText)
			return -1
		end if
		
////
////	Reset the datastore
////
		i_ds_stats_logging.reset()
		
////
////	Everything worked fine, so clear out the error message and return 1.
////
		return 1
end function

public function integer uf_stats_log_end ();/********************************************************************************************
 **	
 **	uf_stats_log_end()
 **	
 **	1.  destroy the datastore used to hold the logging information
 **	2.  disconnect the trans object
 **	3.  reset the i variables
 **	
 **	Returns		:	integer		:	-1 	:	an error occurred
 **											1	:	everything worked
 **	
 ********************************************************************************************/

/*
 *	Processing.
 */
	longlong	rtn


////
////	Make sure there is a current sequence (set in the "start" function).  If it cannot be found, the log function was
////	called without having the start function called first and this is not allowed.
////
		if isNull( i_session_log_seq ) then return 1
		
////
////	increment the row id for the user's session
////
		i_session_log_seq++

////
////	log the end of the user session logging
////
		i_ds_stats_logging.insertRow( 0 ) ///insert row at the end
		
		i_ds_stats_logging.setItem( i_session_log_seq, "users", i_user_id)
		i_ds_stats_logging.setItem( i_session_log_seq, "sessionid", i_session_id)
		i_ds_stats_logging.setItem( i_session_log_seq, "seq", i_session_log_seq)
		i_ds_stats_logging.setItem( i_session_log_seq, "object_name", 'uo_system_cache')
		i_ds_stats_logging.setItem( i_session_log_seq, "logged_event", 'End of Session Logging')
		i_ds_stats_logging.setItem( i_session_log_seq, "logged_time", datetime(today(),now()) )
		
////
////	Insert what's in the datastore to the session logging table
////
		rtn = uf_stats_log_insert()
		
		if rtn < 1 then
			////messagebox of error happens in insert function
			return -1
		end if
			
////
////	destroy the datastore (instantiated in the start function)
////
		destroy i_ds_stats_logging

////
////	Reset the instance variables
////
		setNull( i_session_log_seq )
		setNull( i_session_id )
		setNull( i_user_id )

////
////	Everything worked fine, so clear out the error message and return 1.
////
		return 1
end function

public function integer uf_stats_log (string a_object_name, string a_logged_event);/********************************************************************************************
 **	
 **	uf_stats_log()
 **	
 **	1.  Add 1 to the sequence
 **	2.  append the datastore with information called from this
 **	3.  reset the datastore
 **	
 **	Parameters	:	string		: a_object_name  : the classname/object name of where this function is called.
 **						string		: a_logged_event  : the event where this function is called. 
 **
 **	Returns		:	integer		:	-1 	:	an error occurred
 **											1	:	everything worked good
 **	
 ********************************************************************************************/

/*
 *	Processing.
 */
	longlong	rtn
	longlong log_limit = 100000


////
////	Make sure there is a current sequence (set in the "start" function).  If it cannot be found, the log function was
////	called without having the start function called first and this is not allowed.
////
		if isNull( i_session_log_seq ) then return 1
		
////
////	Validation
////
		if isnull( a_object_name) then
			Messagebox("Stats Log Error", "No object name was passed into the stats logging logic!" ) 
			return -1 
		end if
		
		if isnull( a_logged_event) then
			Messagebox("Stats Log Error", "No event was passed into the stats logging logic!" ) 
			return -1 
		end if
				
////
////	If the datastore with the logs is greater than the log limit, insert into the table (which resets the datastore)
////
		if i_ds_stats_logging.rowcount() >= log_limit then
			
			rtn = uf_stats_log_insert()
			
			if rtn < 1 then
				////messagebox of error happens in insert function
				return -1
			end if
			
		end if
		
////
////	increment the row id for the user's session
////
		i_session_log_seq++

////
////	insert a new row for the logging of the event
////
		i_ds_stats_logging.insertRow( 0 ) ///insert row at the end
		
		i_ds_stats_logging.setItem( i_session_log_seq, "users", i_user_id)
		i_ds_stats_logging.setItem( i_session_log_seq, "sessionid", i_session_id)
		i_ds_stats_logging.setItem( i_session_log_seq, "seq", i_session_log_seq)
		i_ds_stats_logging.setItem( i_session_log_seq, "object_name", a_object_name)
		i_ds_stats_logging.setItem( i_session_log_seq, "logged_event", a_logged_event)
		i_ds_stats_logging.setItem( i_session_log_seq, "logged_time", datetime(today(),now()) )
		
////
////	Everything worked fine, so clear out the error message and return 1.
////
		return 1
end function

public function string uf_get_system_control_company_exact (string a_control_name, longlong a_company_id);string control_value, filter
longlong rtn

//select ltrim(rtrim(control_value)) into :control_value from pp_system_control_company 
//where ltrim(rtrim(lower(control_name))) = ltrim(rtrim(lower(:a_control_name))) and company_id = :a_company_id;

filter = 'trim(lower(control_name)) = "'+trim(lower(a_control_name))+'" and company_id = '+string(a_company_id)
rtn = i_ds_system_control_company.setfilter( filter )
rtn = min(i_ds_system_control_company.filter( ), rtn)

if rtn < 0 then
	if g_main_application then
		messagebox("Error", "Error applying filter when retrieving system controls.")
	else
		f_pp_msgs( "Error applying filter when retrieving system controls.")
	end if
	return ""
end if
	
if i_ds_system_control_company.rowcount( ) >= 1 then
	control_value = trim(i_ds_system_control_company.getitemstring( 1, 'control_value' ))
else
	if a_company_id <> -1 then
		control_value = uf_get_system_control_company_exact(a_control_name, -1)
	else
		control_value = ""
	end if
end if

return control_value
end function

on uo_system_cache.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_system_cache.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;////
////	Create the transaction object for the logs.
////
			i_sqlsa = create uo_sqlca_logs
			
////
////	Null out our instance variables.
////
			setNull(  i_session_log_seq )
			setNull(  i_session_id )
			setNull(  i_user_id )
end event

event destructor;////
////	Destroy the transaction object for the logs.
////
			destroy i_sqlsa
end event

