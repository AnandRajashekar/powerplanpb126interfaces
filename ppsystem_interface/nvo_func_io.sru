HA$PBExportHeader$nvo_func_io.sru
forward
global type nvo_func_io from nonvisualobject
end type
end forward

global type nvo_func_io from nonvisualobject
end type
global nvo_func_io nvo_func_io

forward prototypes
public subroutine of_messageboxexception (string a_msg, string a_msg2[], icon i, button b) throws nvo_exception
public subroutine of_messageboxexception (string a_msg, string a_msg2, icon i, button b) throws nvo_exception
public subroutine of_messageboxexception (string a_msg, string a_msg2, icon i) throws nvo_exception
public subroutine of_messageboxexception (string a_msg, string a_msg2) throws nvo_exception
public function boolean of_filewrite (string a_filename, string a_text, writemode a_writemode)
public function longlong of_pp_msgs (string a_msg)
public function longlong of_pp_msgs (longlong a_pct_complete)
public function longlong of_pp_msgs_start_log (longlong a_process_id)
public function longlong of_pp_msgs_start_log (longlong a_process_id, string a_exe_version)
public function longlong of_pp_msgs (string a_msg, string a_pp_error_code, string a_sql_error_code)
public function longlong of_pp_msgs_end_log ()
end prototypes

public subroutine of_messageboxexception (string a_msg, string a_msg2[], icon i, button b) throws nvo_exception;//Throws an excption with the message passed in.
//To be overridden in the child

nvo_exception e

e = create nvo_exception
//Better yet, use data dictioary here and pass the translated message 
e.setmessage( a_msg )
e.i_args = a_msg2
throw e




end subroutine

public subroutine of_messageboxexception (string a_msg, string a_msg2, icon i, button b) throws nvo_exception;//of_messageboxexception
//Throws an exception with the passed in message, based on child implementation. 

string msg[]

msg[1] = a_msg2
of_messageboxexception(a_msg,msg,i,b)
end subroutine

public subroutine of_messageboxexception (string a_msg, string a_msg2, icon i) throws nvo_exception;// Throws an exception with the passed in message, based on child's implementation.

of_messageboxexception(a_msg,a_msg2,i,OK!)
end subroutine

public subroutine of_messageboxexception (string a_msg, string a_msg2) throws nvo_exception;// Throws an exception with the passed in message, based on child implementation.
of_messageboxexception(a_msg,a_msg2,Information!,OK!)
end subroutine

public function boolean of_filewrite (string a_filename, string a_text, writemode a_writemode);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :  of_filewrite
//
//   Purpose  :	Writes the passed string to the passed filename.
//                Thhe string can be appended to the file 
//              
//
//   Scope    :   global
//
//   Arguments:  a_filename  : string      :  file to write to
//               a_text      : string      :  string to write
//               a_writemode : writemode   :  append! or replace! 
// 
//
//   Returns :    boolean ; Success or failure of the write
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    04-07-95    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

longlong rtn
string  txt
int fn

txt = a_text

fn = fileopen(a_filename,streammode!,write!,lockreadwrite!,a_writemode)
if fn = -1 then return false

//try_again:
do 
	rtn = filewrite(fn,txt) 
	
	if rtn = 32765 then
		txt = mid(txt,32766,len(txt))
		//goto try_again
		continue
	end if
	exit
loop while 1=1

if rtn = -1 then return false
if fileclose(fn) = -1 then return false
return true
end function

public function longlong of_pp_msgs (string a_msg);//******************************************************************************************
//
//  Function     :  of_pp_msgs
//
//  Description  :  Write messages to pp_processes_messages
//
//	JAK 20100113:	Added logic so that if the message is greater than
//							2000 characters the message is still logged just on multiple 
//							lines rather than creating an SQL error and stopping there.
//
//******************************************************************************************
string subbed_msg
do 
	subbed_msg = mid(a_msg,1,2000)
	
	insert into pp_processes_messages
		(process_id, occurrence_id, msg, msg_order)
	values
		(:g_process_id, :g_occurrence_id, :subbed_msg, :g_msg_order)
	using g_sqlca_logs;
	
	if g_sqlca_logs.SQLCode = 0 then
		commit using g_sqlca_logs;
		g_msg_order++
	else
		rollback using g_sqlca_logs;
	end if
	
	a_msg = mid(a_msg,2001)
loop while len(a_msg) > 0

return g_sqlca_logs.SQLCode
end function

public function longlong of_pp_msgs (longlong a_pct_complete);//******************************************************************************************
//
//  Function     :  of_pp_msgs
//
//  Description  :  Write percent_complete to pp_processes_occurrences
//
//******************************************************************************************
string subbed_msg

update pp_processes_occurrences 
set pct_complete = :a_pct_complete
where process_id = :g_process_id 
and occurrence_id = :g_occurrence_id 
using g_sqlca_logs;

if g_sqlca_logs.SQLCode = 0 then
	commit using g_sqlca_logs;
else
	rollback using g_sqlca_logs;
end if
	
return g_sqlca_logs.SQLCode
end function

public function longlong of_pp_msgs_start_log (longlong a_process_id);//******************************************************************************************
//
//  Function     	:  of_pp_msgs_start_log
//
//  Description  	:  Initiate online process log
//
//  Argument(s)	:		a_process_id  -  Process ID to be initiated (Processes are defined
//															and created in table pp_processes)
//	
//	Note:					Overloaded function without exe version.  Calls function with version and leaves it blank.
//
//******************************************************************************************
return of_pp_msgs_start_log(a_process_id,'')
end function

public function longlong of_pp_msgs_start_log (longlong a_process_id, string a_exe_version);//******************************************************************************************
//
//  Function     	:  of_pp_msgs_start_log
//
//  Description  	:  Initiate online process log
//
//  Argument(s)	:		a_process_id  -  Process ID to be initiated (Processes are defined
//															and created in table pp_processes)
//	
//	Note:					Overloaded function without exe version.  Calls function with version and leaves it blank.
//
//******************************************************************************************
string s_date
date ddate
time ttime
datetime started_at

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
started_at = datetime(ddate, ttime)

g_process_id    = a_process_id
g_occurrence_id = 0
g_msg_order     = 1

// Get the occurrence ID from the sequence.  No longer using a max(id)+1 construct
select pp_processes_ocurrences_seq.nextval into :g_occurrence_id from dual;

insert into pp_processes_occurrences
	(process_id, occurrence_id, start_time, version)
values
	(:g_process_id, :g_occurrence_id, sysdate, substr(:a_exe_version,1,35))
using g_sqlca_logs;

if g_sqlca_logs.SQLCode = 0 then
	commit using g_sqlca_logs;
end if

return g_sqlca_logs.SQLCode

end function

public function longlong of_pp_msgs (string a_msg, string a_pp_error_code, string a_sql_error_code);//******************************************************************************************
//
//  Function     :  of_pp_msgs

//  Description  :  Write messages to pp_processes_messages while capturing SQL codes and PP codes
//
//	JAK 20100113:	Added logic so that if the message is greater than
//							2000 characters the message is still logged just on multiple 
//							lines rather than creating an SQL error and stopping there.
//
//******************************************************************************************
string subbed_msg
do 
	subbed_msg = mid(a_msg,1,2000)
	
	insert into pp_processes_messages
		(process_id, occurrence_id, msg, msg_order, pp_error_code, sql_error_code)
	values
		(:g_process_id, :g_occurrence_id, :subbed_msg, :g_msg_order, :a_pp_error_code, :a_sql_error_code)
	using g_sqlca_logs;
	
	if g_sqlca_logs.SQLCode = 0 then
		commit using g_sqlca_logs;
		g_msg_order++
	else
		rollback using g_sqlca_logs;
	end if
	
	a_msg = mid(a_msg,2001)
loop while len(a_msg) > 0

return g_sqlca_logs.SQLCode
end function

public function longlong of_pp_msgs_end_log ();//******************************************************************************************
//
//  Function     	:	f_pp_msgs_end_log
//
//  Description  	:	End online process log
//
//  Argument(s)	:	None
//
//******************************************************************************************

string s_date
date ddate
time ttime
datetime finished_at

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
finished_at = datetime(ddate, ttime)

update pp_processes_occurrences set end_time = sysdate
 where process_id = :g_process_id and occurrence_id = :g_occurrence_id
 using g_sqlca_logs;

if g_sqlca_logs.sqlCode = 0 then
	commit using g_sqlca_logs;
else
	rollback using g_sqlca_logs;
	if g_main_application then
		messagebox("",g_sqlca_logs.sqlerrtext)
	else
		f_pp_msgs(g_sqlca_logs.sqlerrtext)
	end if
end if

return g_sqlca_logs.SQLCode
end function

on nvo_func_io.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_func_io.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

