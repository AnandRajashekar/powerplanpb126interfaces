HA$PBExportHeader$uo_ds_top.sru
$PBExportComments$v10.2.1.1 JAK:    System Objects for Cancelling Queries
forward
global type uo_ds_top from nvo_ds_top
end type
end forward

global type uo_ds_top from nvo_ds_top
end type
global uo_ds_top uo_ds_top

on uo_ds_top.create
call super::create
end on

on uo_ds_top.destroy
call super::destroy
end on

