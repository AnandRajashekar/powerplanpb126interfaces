HA$PBExportHeader$u_external_function.sru
$PBExportComments$v10.2 new function added
forward
global type u_external_function from nonvisualobject
end type
type s_time_zone_information from structure within u_external_function
end type
type s_string from structure within u_external_function
end type
end forward

type s_time_zone_information from structure
	longlong	bias
	character		standardname[64]
	s_system_time		standarddate
	longlong	standardbias
	character		daylightname[64]
	s_system_time		daylightdate
	longlong	daylightbias
end type

type s_string from structure
	string		name
end type

global type u_external_function from nonvisualobject
end type
global u_external_function u_external_function

type prototypes
//Notify Batch System
Function int NotifyBat(string host,string message2) library "notbt32.dll" alias for "NotifyBat;Ansi"
//
Function long LoadLibraryA(string libname) Library "KERNEL32.DLL" alias for "LoadLibraryA;Ansi"
//
FUNCTION long FindWindowA(string name, string title) library "user32.dll" alias for "FindWindowA;Ansi"
FUNCTION BOOLean ShowWindow(int hwnd, int nCmdShow) library "user32.dll"
// ### 11687: JAK: 2013-01-23: updated return datatypes to match Windows return types
private FUNCTION long SetCapture(long a)  LIBRARY "USER32.dll"
private FUNCTION boolean ReleaseCapture() LIBRARY "USER32.dll"
FUNCTION long GetClassNameA (LONG hWnd, REF STRING lpClassName, int nMaxCount ) LIBRARY "USER32.DLL" alias for "GetClassNameA;Ansi"

FUNCTION LONG GetLastActivePopup (LONG hWnd) LIBRARY "USER32.DLL"
FUNCTION BOOLEAN BringWindowToTop (LONG hWnd) LIBRARY "USER32.DLL"
FUNCTION BOOLEAN IsIconic (LONG hWnd) LIBRARY "USER32.DLL"


//playsound
Function boolean sndPlaySoundA (string SoundName, uint Flags) Library "WINMM.DLL" alias for "sndPlaySoundA;Ansi"
Function uint waveOutGetNumDevs () Library "WINMM.DLL"
// MCI
Function Long mciSendStringA ( String lpstrCommand, Ref String lpstrReturnString, int uReturnLength, Int hwndCallBack ) Library "winmm.dll" alias for "mciSendStringA;Ansi"
Function int mciGetErrorStringA ( long fdwError, Ref string lpszErrorText, int cchErrorText ) Library "winmm.dll" alias for "mciGetErrorStringA;Ansi"

//getsyscolor
Function ulong GetSysColor (int index) Library "USER32.DLL"
//get scrollbar thumb position
Function int GetScrollPos (LONG hWnd, int index) Library "USER32.DLL" 

//getsystemmetrics
Function int GetSystemMetrics (int index) Library "USER32.DLL"

//getfreememory
Subroutine GlobalMemoryStatus (ref str_memorystatus memorystatus ) Library "KERNEL32.DLL" alias for "GlobalMemoryStatus;Ansi"

//set and kill timer
Function Boolean KillTimer (long handle, uint id ) library "USER32.DLL"
Function uint SetTimer (long handle, uint id, uint time, long addr ) library "USER32.DLL"

//GetModuleHandle
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long  GetModuleFileName(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"

Function boolean FlashWindow (long handle, boolean flash) Library "USER32.DLL"
Function uint GetWindow (long handle,uint relationship) Library "USER32.DLL"
Function int GetWindowTextA(long handle, ref string wintext, int length) Library "USER32.DLL" alias for "GetWindowTextA;Ansi"
Function boolean IsWindowVisible (long handle) Library "USER32.DLL"
Function uint GetWindowsDirectoryA (ref string dirtext, uint textlen) library "KERNEL32.DLL" alias for "GetWindowsDirectoryA;Ansi"
Function uint GetSystemDirectoryA (ref string dirtext, uint textlen) library "KERNEL32.DLL" alias for "GetSystemDirectoryA;Ansi"
Function uint GetDriveTypeA (string drive) library "KERNEL32.DLL" alias for "GetDriveTypeA;Ansi"

Function boolean GetUserNameA (ref string name, ref ulong len) library "ADVAPI32.DLL" alias for "GetUserNameA;Ansi"
Function ulong GetTickCount ( ) Library "KERNEL32.DLL"
function long GetFileVersionInfoSizeA( string filename  ,ref long  size  ) library "version.dll" alias for "GetFileVersionInfoSizeA;Ansi"
function long GetFileVersionInfoA(string filename, long  dwHandle,long dwLen, ref blob  lpData) library "version.dll" alias for "GetFileVersionInfoA;Ansi"
function long VerQueryValueA( blob pBlock,string lpSubBlock,ref s_string lplpBuffer,ref long puLen ) library "version.dll" alias for "VerQueryValueA;Ansi"
function long GetUserDefaultLangID() library "kernel32.dll"
Function long  GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"

function unsignedlong SetSysColors(int nelements, int elements[],long RgbValues[])   library "user32.dll"
Function Boolean FileTimeToLocalFileTime(s_filedate lpFileTime,ref s_filedate lpLocalFileTime) library "kernel32.dll"

function long GetThreadLocale() library "Kernel32.dll"
function long GetLocaleInfo(ulong Locale, ulong LCType,  ref string lpLCData,long  cchData ) library "Kernel32.dll" alias for "GetLocaleInfoA;Ansi"    
function long sendmessage (long hwnd, uint msg, long wparam, string lparam) library "user32.dll" alias for "SendMessageA;Ansi"

// ### 8174: JAK: 2011-07-27:  new uf_create_directory function
function boolean CreateDirectoryA(ref string path, long attr) LIBRARY "kernel32.dll"

// ### 8250: JAK: 2011-11-01:  copyfile and find files functions
FUNCTION BOOLEAN CopyFile(string lpExistingFileName, string lpNewFileName, &
    boolean bFailIfExists) LIBRARY "Kernel32.dll" Alias for "CopyFileW"
	 
// ### 29892: JAK: 2013-05-20:  New functions to pull file information.
Function Long SystemTimeToTzSpecificLocalTime(s_TIME_ZONE_INFORMATION timezone, s_system_time utc_date, ref s_system_time local_date)  library "kernel32.dll" alias for "SystemTimeToTzSpecificLocalTime;Ansi"
Function Long GetTimeZoneInformation(ref s_TIME_ZONE_INFORMATION timezone)  library "kernel32.dll" alias for "GetTimeZoneInformation;Ansi"

// ### 30149 SRM from m_top
function longlong ShellExecute(longlong hwnd,string operation,string filename, &
		string parm,string dir,longlong showcmd) &
		library "shell32.dll" alias for "ShellExecuteA;Ansi"
		
Function long SetFileAttributes(string lpFileName, long dwFileAttributes) LIBRARY "kernel32.dll" &
		alias for "SetFileAttributesA;Ansi"

		
function long FindWindowExA( long hwndParent,long hwndChildAfter,string lpszClass,long lpszWindow) library "user32.dll" alias for "FindWindowExA;Ansi"
function long FindWindowA(string lpszClass,long lpszWindow)  alias for "FindWindowA;Ansi" library "user32.dll"
function long GetParent(long hwnd) library "user32.dll"
function long DestroyWindow(long hwnd) library "user32.dll"
function long CloseWindow(long hwnd) library "user32.dll"

// ### 30149 SRM from n_encrypt
FUNCTION long CryptAcquireContextA (ref ulong hProv, ref string pszContainer, ref string pszProvider, ulong dwProvType,  ulong dwFlags) LIBRARY "advapi32.dll" alias for "CryptAcquireContextA;Ansi"
FUNCTION long CryptReleaseContext (ulong hProv, ulong dwFlags) LIBRARY "advapi32.dll"
FUNCTION long CryptCreateHash (ulong hProv, uint Algid, ulong hKey, ulong dwFlags, ref ulong phHash) LIBRARY "advapi32.dll"
FUNCTION long CryptHashData (ulong hHash,  ref string pbData,ulong dwDataLen, ulong dwFlags) LIBRARY "advapi32.dll" alias for "CryptHashData;Ansi"
FUNCTION long CryptDestroyHash (ulong hHash) LIBRARY "advapi32.dll"
FUNCTION long CryptGetHashParam (ulong hHash, ulong dwParam, ref blob pbData, ref ulong pdwDataLen, ulong dwFlags) LIBRARY "advapi32.dll"
Function ulong CryptDeriveKey (ulong hProv, ulong Algid, ulong hBaseData, ulong dwFlags, Ref ulong phKey) Library "advapi32.dll" Alias For "CryptDeriveKey"
Function ulong CryptEncrypt (ulong hKey, ulong hHash, ulong Final, ulong dwFlags, Ref blob pbData, Ref ulong pdwDataLen, ulong dwBufLen) Library "advapi32.dll" Alias For "CryptEncrypt"
Function ulong CryptAcquireContext (Ref ulong phProv,long pszContainer, String pszProvider, ulong dwProvType, ulong dwFlags) Library "advapi32.dll" Alias For "CryptAcquireContextA;Ansi"
Function ulong CryptDestroyKey (ulong hKey) Library "advapi32.dll" Alias For "CryptDestroyKey"
Function ulong CryptDecrypt (ulong hKey, ulong hHash, ulong Final, ulong dwFlags, Ref blob pbData, Ref ulong pdwDataLen) Library "advapi32.dll" Alias For "CryptDecrypt"	
Function long FormatMessageA( long flag,long input,long err,long lang,ref string msg,long len,long arg) Library "kernel32.dll" alias for "FormatMessageA;ansi"
function long GetLastError()  library "kernel32.dll"
function long WNetGetUserA(long ptr,ref string user,ref long len) LIBRARY "mpr.dll" alias for "WNetGetUserA;Ansi"
function long CryptGenKey (ulong hProv, uint Algid, ulong dwFlags, ref ulong phKey) library "advapi32.dll"

// ### 30149 SRM from n_print_dialog
function long PrintDlgExA(   long lppd ) library "comdlg32.dll"
function long DisplayPrintPropertySheet(long hwnd,ref string lppd ) library "C:\work\printdlg\debug\printdlg.dll" alias for "DisplayPrintPropertySheet;Ansi"
function long PageSetupDlgA( ref s_print_setup_dlg lppsd ) library "comdlg32.dll" alias for "PageSetupDlgA;Ansi"
Subroutine   RtlMoveMemory(ref s_devmode nmhr, ulong ptr,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi" 
Subroutine   RtlMoveMemoryD(ref s_devname nmhr, ulong ptr,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi"
Subroutine   RtlMoveMemoryP(ulong ptr,  s_printer plld ,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi"
Subroutine   RtlMoveMemoryT(ulong ptr,  ulong pt,ulong len) library "kernel32.dll" alias for "RtlMoveMemory"
Subroutine   RtlMoveMemoryU(ref s_printer s_ptr,long ptr ,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi"
function long GlobalLock(long mem) library "kernel32.dll"
function long GlobalUnlock(long mem) library "kernel32.dll"
function long GlobalFree(long mem) library "kernel32.dll"
function long GlobalSize(long mem) library "kernel32.dll"
function long GlobalAlloc(long uFlags,long dwBytes) library "kernel32.dll"
function long LocalLock(long mem) library "kernel32.dll"
function long LocalUnlock(long mem) library "kernel32.dll"
function long LocalFree(long mem) library "kernel32.dll"
function long LocalSize(long mem) library "kernel32.dll"
function long LocalAlloc(long uFlags,long dwBytes) library "kernel32.dll"
function long SetPrinterA(long  hPrinter,  long  Level,ref long  devmode,  long Command ) library "winspool.drv"
function long OpenPrinterA(string  pPrinterName, ref long phPrinter,  long pDefault  ) library "winspool.drv" alias for "OpenPrinterA;Ansi"
function long ClosePrinter(long handle)  library "winspool.drv"
function long PrintDlg( long  lppd ) library "comdlg32.dll"  alias for "PrintDlgA;Ansi"
function long CommDlgExtendedError() library "comdlg32.dll"
Subroutine   RtlMoveMemoryPS(ulong ptr,  s_print_dlg  plld ,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi"
Subroutine   RtlMoveMemoryR(ulong ptr,  s_print_range plld ,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi"

// ### 30149 SRM from n_run
function long SetCurrentDirectory(string  PathName) library "Kernel32.dll" alias for "SetCurrentDirectoryW"
function long GetCurrentDirectory(long bufflen , ref string  PathName) library "Kernel32.dll" alias for "GetCurrentDirectoryW"

// ### 30149 SRM from n_tooltip
SubRoutine InitCommonControls() library "comctl32.dll"
Function long CreateWindowExA(ulong dwExStyle, string ClassName, long WindowName, ulong dwStyle, ulong X, ulong Y, ulong nWidth, ulong nHeight, ulong hWndParent, ulong hMenu, ulong hInstance, ulong lpParam) library "user32.dll" alias for "CreateWindowExA;Ansi"
Function integer ToolTipMsg(long hWnd, long uMsg, long wParam, REF s_TOOLINFO ToolInfo) library "user32.dll" Alias For "SendMessageA;Ansi"
Function integer RelayMsg(long hWnd, long uMsg, long wParam, REF s_MSG Msg) library "user32.dll" Alias For "SendMessageA;Ansi"
Function long lstrcpy(long Destination, string Source) library "kernel32.dll" alias for "lstrcpy;Ansi"
Function uLong SendMessageString( uLong hwnd, uLong Msg, uLong wParam, Ref String lpzString ) Library "user32.dll" Alias For "SendMessageA;Ansi"

// ### 30149 SRM from nca_folderbrowse
Function unsignedlong SHGetPathFromIDListA( unsignedlong pidl, ref string pszPath) Library 'shell32' alias for "SHGetPathFromIDListA;Ansi"
Function unsignedlong SHBrowseForFolderA( s_browseinfo lpbrowseinfo ) Library 'shell32' alias for "SHBrowseForFolderA;Ansi"
Subroutine CoTaskMemFree(ulong idlist) Library 'ole32'

// ### 30149 SRM from u_prog_interface
function long GetStdHandle( long word) library "kernel32.dll"
function long WriteFile( long hFile, string buf, long len,ref long lenw,long ptr) library "kernel32.dll" alias for "WriteFile;Ansi"
function long CreateFileA(string filename,long mode,long share,ref long ptr,long attr,long dw,long dw2)  library "kernel32.dll" alias for "CreateFileA;Ansi"
subroutine ExitProcess( long uExitCode)  library "kernel32.dll"
FUNCTION long GetEnvironmentVariableA( string lpName, ref string lpBuffer,long nSize)  library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
FUNCTION long  GetCurrentProcessId()  library "kernel32.dll" 

// ### 30149 SRM from u_trace_sql
function long SetOracleCallback(long handle,long a_hwnd,long a_evt) library "scintilla.dll"
Function long GetEnvironmentVariable(string var,ref string str,long len)  Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long SetEnvironmentVariable(string var,string val)  Library "kernel32.dll" alias for "SetEnvironmentVariableA;Ansi"
Function long GetModuleHandle(long modname) Library "KERNEL32.DLL"  alias for "GetModuleHandleA"
Function long LoadLibrary(string libname) Library "KERNEL32.DLL" alias for "LoadLibraryA;Ansi"
Function long FormatMessage( long flag,long input,long err,long lang,ref string msg,long len,long arg) Library "kernel32.dll" alias for "FormatMessageA;Ansi"
Function long GetProcAddress(long hmod ,string procname) Library "KERNEL32.DLL" alias for "GetProcAddress;Ansi"

// ### 30149 SRM from uo_encrypt
Function ulong GetDC ( ulong hWnd) Library "user32.dll"
Function ulong SelectObject ( ulong hdc, ulong hWnd ) Library "gdi32.dll"
Function boolean GetTextExtentPoint32 (ulong hdcr, string lpString, long nCount, Ref s_size size) Library "gdi32.dll" Alias For "GetTextExtentPoint32W"
Function long ReleaseDC (ulong hWnd, ulong hdcr ) Library "user32.dll"

// ### 30149 SRM from uo_ftp
Function long FtpGetFileA( long hFtpSession, string lpszRemoteFile,string lpszNewFile,long fFailIfExists, long dwLocalFlagsAndAttributes,long dwInternetFlags,long dwContext) library "wininet.dll" alias for "FtpGetFileA;Ansi"
function long InternetConnectA(long hInternetSession,string ServerName,long port,string user,string  password,long service,long Flags,long context) library "wininet.dll" alias for "InternetConnectA;Ansi"
function long FtpCommandA(long hFtpSession,long res,long flags,string cmd,long context,ref long ptr) library "wininet.dll" alias for "FtpCommandA;Ansi"
function long InternetGetLastResponseInfoA(ref long lpdwError,ref string lpszBuffer,ref long len) library "wininet.dll" alias for "InternetGetLastResponseInfoA;Ansi"
function long InternetOpenA(string lpszAgent,long AccessType,long proxynamex,long ProxyBypass,long dwFlags) library "wininet.dll" alias for "InternetOpenA;Ansi"
function long InternetCloseHandle(long hInet) library "wininet.dll"
function long FtpPutFileA(long hFtpSession,string filename,string remotefile,long Flags, long Context) library "wininet.dll" alias for "FtpPutFileA;Ansi"
Function long FtpSetCurrentDirectoryA(long hFtpSession,string Directory) library "wininet.dll" alias for "FtpSetCurrentDirectoryA;Ansi"
function long InternetWriteFile(long hfile,string buffer, long len,ref long retlen)  library "wininet.dll" alias for "InternetWriteFile;Ansi"
function long FtpOpenFileA(long hconnect,string filename,long ascess,long filetype,long context) library "wininet.dll" alias for "FtpOpenFileA;Ansi"

// ### 30149 SRM from uo_ftp_wrapper
function long FtpGetCurrentDirectoryA(long hFtpSession,ref string Directory, ref long numChars) library "wininet.dll" alias for "FtpGetCurrentDirectoryA;Ansi"
function long FtpRenameFileA(long hconnect, string existing, string new) library "wininet.dll" alias for "FtpRenameFileA;Ansi"
function long FtpDeleteFileA(long hconnect, string filename) library "wininet.dll" alias for "FtpDeleteFileA;Ansi"
function long FtpFindFirstFileA(long hconnect, string lpszSearchFile, ref s_finddata lpFindFileData, long dwFlags, long dwContext) library "wininet.dll" alias for "FtpFindFirstFileA;Ansi"
function long InternetQueryDataAvailable(long hconnect,ref long lpdwNumberOfBytesAvailable,long dwFlags,long dwContext) library "wininet.dll"
function long InternetReadFile(long hfile,ref string buffer, long len,ref long retlen) library "wininet.dll" alias for "InternetReadFile;Ansi"
function long InternetFindNextFileA(long hInternetSession, ref s_finddata lpvFileData) library "wininet.dll" alias for "InternetFindNextFileA;Ansi"
function long Sleep(long milliseconds) library "kernel32.dll"
function long ShellExecuteA(long hwnd,string verb,string filename,string parms, string dir, long x3) library "shell32.dll" alias for "ShellExecuteA;Ansi"
function long FileTimeToSystemTime(ref s_filetime lpFileTime, ref s_systemtime lpSystemTime) library "kernel32.dll" alias for "FileTimeToSystemTime;Ansi"

// ### 30149 SRM from uo_help
function boolean HtmlHelp( ulong hwnd, string pszFile, uint uCommand, ulong dwData ) library "Hhctrl.ocx" alias for "HtmlHelpA;Ansi"
function boolean HtmlHelp( ulong hwnd, string pszFile, uint uCommand, string dwData ) library "Hhctrl.ocx" alias for "HtmlHelpA;Ansi"

// ### 30149 SRM from uo_locale
function long SetThreadLocale ( long Locale ) library "Kernel32.dll"
function long LoadKeyboardLayout( ref string KLID, ulong Flags) library "Kernel32.dll" alias for "LoadKeyboardLayout;Ansi"
function long GetLocaleInfoA(ulong Locale, ulong LCType,  ref string lpLCData,long  cchData ) library "Kernel32.dll" alias for "GetLocaleInfoA;Ansi"    

// ### 30149 SRM from uo_menu2
FUNCTION int BitBlt(ulong hDC, int num, int num, int num, int num, ulong hDC, int num, int num, ulong lParam) LIBRARY "Gdi32.dll"
FUNCTION ulong CreateCompatibleDC(ulong hDC) LIBRARY "Gdi32.dll"
FUNCTION ulong LoadImageA( ulong hints, ref string lpszName,  UINT uType, int cxDesired,int cyDesired,UINT fuLoad ) library "user32.dll" alias for "LoadImageA;Ansi" 
FUNCTION ulong GetObjectBitmap( ulong  hgdiobj, int  cbBuffer, ref s_bitmap bm ) library "gdi32.dll" alias for GetObjectW
FUNCTION boolean DeleteObject ( ulong hgdiobj ) library "gdi32.dll" 
FUNCTION long StretchBlt(ulong hDCdest, int x1, int y1, int w1, int h1, ulong hDCsrc, int x2, int y2, int w2, int h2, ulong lParam) LIBRARY "Gdi32.dll"
FUNCTION long FindWindow(string name, long title) library "user32.dll" alias for "FindWindowW"
FUNCTION long FindWindowS(string name, string title) library "user32.dll" alias for "FindWindowW"
FUNCTION long FindWindowEx(long hwndParent,long hwndChildAfter,string lpszClass,long lpszWindow) library "user32.dll" alias for "FindWindowExW"
FUNCTION boolean SetMenuItemBitmaps(ulong hmenu,uint upos,uint flags,ulong handle_bm1,ulong handle_bm2)  LIBRARY "USER32.DLL"
FUNCTION ulong GetMenuItemID(ulong hMenu,uint uItem) LIBRARY "USER32.DLL"
FUNCTION int GetSubMenu(ulong hMenu,int pos) LIBRARY "USER32.DLL"
FUNCTION ulong GetMenu(ulong hWindow) LIBRARY "USER32.DLL"
FUNCTION boolean ModifyMenu(ulong  hMnu, ulong uPosition, ulong uFlags, ulong uIDNewItem, long lpNewI) alias for ModifyMenuW LIBRARY "USER32.DLL"
FUNCTION boolean SetMenuDefault(ulong hmenu,uint upos,uint flags)  LIBRARY "USER32.DLL"
FUNCTION ulong SetMenuInfo(ulong hmenu,s_menuinfo lpcmi)  LIBRARY "USER32.DLL" alias for "SetMenuInfo;Ansi"
FUNCTION ulong CreateSolidBrush( long crColor)  library "gdi32.dll"
FUNCTION ulong SetBkColor(long hdc,long  crColor )  LIBRARY "gdi32.dll"
function long FN_ToolBarSetFont(long unknown,string fontname,long fontsize,long unknown1,string str) library "pbvm100.dll" alias for "FN_ToolBarSetFont;Ansi" 
function long FN_ToolBarShowText(long unknown,long state) library "pbvm100.dll" 
function long FN_ToolItemSetColor(long unknown,long color,long unknown,long unknown2) library "pbvm100.dll" 
function long FN_ToolItemHwndFirst(long unknown,long unknown2, ref long unknown3) library "pbvm100.dll" 
function long FN_ToolGroupFirst(long unknown) library "pbvm100.dll" 
Function long SendMessageSS(long hWnd,long Msg,long wParam,ref s_colorschema lParam)library "user32.dll" ALIAS FOR "SendMessageW"
Function long SendMessage(long hWnd,long Msg,long wParam,long lParam)library "user32.dll" ALIAS FOR "SendMessageW"
Function long GetClientRect(long hWnd,ref s_rect lpRect) library "user32.dll" alias for "GetClientRect;Ansi"
function long FN_ToolBarSize(long unknown,ref s_rect lpRect) library "pbvm100.dll" alias for "FN_ToolBarSize;Ansi" 
function long FillRect(long hdc,ref s_rect rect ,long brush) LIBRARY "user32.dll" alias for "FillRect;Ansi"
FUNCTION long GetWindowLong (ulong hWnd, int nIndex) LIBRARY "USER32.DLL"  ALIAS FOR "GetWindowLongW"
FUNCTION long SetWindowLong (ulong hWnd, int nIndex, long dwNewLong) LIBRARY "USER32.DLL" ALIAS FOR "SetWindowLongW"
FUNCTION long SetLayeredWindowAttributes (long hWnd, Long crKey , char /*Byte*/ bAlpha , Long dwFlags) LIBRARY "USER32.DLL" alias for "SetLayeredWindowAttributes;Ansi" 
function long FN_ToolGroupReset(long unknown,long unknown) library "pbvm100.dll" 
function long FN_ToolGroupRemove(long unknown,long unknown) library "pbvm100.dll"

// ### 30149 SRM from uo_ps_interface
Function long PSInterface(string dir,string infile,string outfile,long typex, long orientation,ref s_search_info search_info) Library "psconvrt.dll" alias for "PSInterface;Ansi"
Function long CreateDCA( string lpszDriver, string lpszDevice, string lpszOutput, long data) Library "gdi32.dll" alias for "CreateDCA;Ansi"
Function long DeleteDC( long hdc) Library "gdi32.dll"
Function long GetDeviceCaps(long hdc,long x) Library "gdi32.dll"
Function long GetPrinterOrientation() Library "psconvrt.dll" 
Function long GetPrintDef(ref long orig ,ref long postscript) Library "psconvrt.dll" 
Function long GetPostscriptPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPostscriptPrinter;Ansi" 
Function long GetPrinterInfo( string printer,ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPrinterInfo;Ansi" 
Function long FindAddPostPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "FindAddPostPrinter;Ansi" 
Function long AddPrinterDriverA(long name, long Level, s_driver_info2 DriverInfo ) Library "winspool.drv" alias for "AddPrinterDriverA;Ansi"
Function long DeletePrinterDriverA(long name, long config, string driver) Library "winspool.drv" alias for "DeletePrinterDriverA;Ansi"
Function long AddPrinterA(long name, long Level, s_printer_info2_add DriverInfo ) Library "winspool.drv" alias for "AddPrinterA;Ansi"
Function long CopyFileA( string path,string  desination,long stat ) Library "KERNEL32.DLL" alias for "CopyFileA;Ansi"
Function long GetPrintDefPCL(ref long orig ,ref long postscript) Library "pclcnvrt.dll" 
Function long GetPCLPrinter(ref string name,ref string drv,ref string port) Library  "pclcnvrt.dll" alias for "GetPCLPrinter;Ansi" 
Function long PCLInterface(string dir,string infile,string outfile,long typex, long orientation,ref s_search_info search_info) Library "pclcnvrt.dll" alias for "PCLInterface;Ansi" 
Function long GetPrinterOrientationPCL() Library "pclcnvrt.dll" 
Function long SetDefPrinter(string pPrinterName)  Library "psconvrt.dll" alias for "SetDefPrinter;Ansi"
Function long GetDefPrinter(ref string pPrinterName,ref  long pdwBufferSize) Library "psconvrt.dll" alias for "GetDefPrinter;Ansi"
Function long GetPrinter(long hwnd,long ptype ,s_printer_info2  printer_info2 ,long num, ref long rnum) Library "KERNEL32.DLL" alias for "GetPrinter;Ansi"
Function long Add_Printer(long apost, string ptr ,string drv) Library "ppaddptr.dll" alias for "Add_Printer;Ansi"
Function long GetShortPathA(string longpath ,ref string shortpath,long buflen) Library "KERNEL32.DLL" alias for "GetShortPathA;Ansi"
Function long GetLongPathNameA(string shortpath ,ref string longpath,long buflen) Library "KERNEL32.DLL" alias for "GetLongPathNameA;Ansi"

// ### 30149 SRM from uo_scintilla
function long CreateWindowExA(long dwstyleex ,  string class ,  string name , long dwstyle , long x,long y,long width  , long height, long hwnd, long menu, long  inst, long extra ) library "user32.dll" alias for "CreateWindowExA;Ansi"
function LONG GetWindowLongA(long HWND, long nIndex) library "user32.dll"
Function long SendMessageS(long hWnd,long Msg,long wParam ,ref string  lParam)library "user32.dll" ALIAS FOR "SendMessageA;Ansi"
Function long SendMessageS2(long hWnd,long Msg,string wParam ,ref string  lParam)library "user32.dll" ALIAS FOR "SendMessageA;Ansi"
Function long SendMessageI(long hWnd,long Msg,long wParam,long lParam)library "user32.dll" ALIAS FOR "SendMessageA"
Function long PostMessageI(long hWnd,long Msg,long wParam,long lParam)library "user32.dll" ALIAS FOR "PostMessageA"
Function long SendMessageIR(long hWnd,long Msg,long wParam,ref long lParam)library "user32.dll" ALIAS FOR "SendMessageA"
Function string SendMessageIS(long hWnd,long Msg,long wParam, long lParam)library "user32.dll" ALIAS FOR "SendMessageA;Ansi"
Function long GetFontList(ref string fonts[] ,long count)library "scintilla.dll" alias for "GetFontList;Ansi" 
Function long GetMenu(long hinst)library "scintilla.dll" 
Function long GetModuleHandleA(long modname) Library "KERNEL32.DLL"
Subroutine   RtlMoveMemory(ref s_NotifyHeader nmhr, ulong ptr,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi" 
Function long MoveWindow( long hWnd,long  X,long  Y,long  nWidth,long  nHeight,long  bRepaint) library "user32.dll"
  
// ### 30149 SRM from uo_security
function long WNetGetUser(long ptr,ref string user,ref long len) LIBRARY "mpr.dll" alias for "WNetGetUserA;Ansi"
function long lstrcmpW(string str1,string str2) Library "kernel32.dll" 
function long HashData( string pbData,long cbData,ref ulong pbHash,long cbHash) library "shlwapi.dll" alias for "HashData;Ansi"

// ### 30149 SRM from uo_smtpmail
function long smtpmail(string fromuser,string subject,string text,string touser,string server) library "smtpmail.dll" alias for "smtpmail;Ansi"
function long smtpmailex(string fromuser,string subject,string text,string touser,string server,string filename,string err) library "smtpmail.dll" alias for "smtpmailex;Ansi"
function long smtpmailpasswd(string fromuser,string user,string passwd,string subject,string text,string touser,string server) library "smtpmail.dll" alias for "smtpmailpasswd;Ansi"
function long smtpmailpasswdex(string fromuser,string user,string passwd,string subject,string text,string touser,string server,string filename,string err) library "smtpmail.dll" alias for "smtpmailpasswdex;Ansi"

// ### 30149 SRM from uo_status_bar
Function long SendMessageA( long hWnd,long Msg, long wParam,long lParam ) library "user32.dll"
Function long CreateDCA( string display, long x,long y,long z) library "gdi32.dll" alias for "CreateDCA;Ansi"
Function long GetDC( long hWnd) library "user32.dll"
Function long GetWindowDC( long hWnd) library "user32.dll"
Function long ReleaseDC( long hdc,long hwnd) library "user32.dll"
Function long SetTextColor(long hdc,long col) library "gdi32.dll"
Function long TextOutA(long hdc, long x, long y, string text, long len)  library "gdi32.dll" alias for "TextOutA;Ansi"
Function long SetBkMode(long hdc, long mode)  library "gdi32.dll"
Function Long SetClassLongA(long hwnd,long index,long val) library "user32.dll"
Function Long SelectObject(long hdc,long brush) library "gdi32.dll"
Function Long SetWindowPos( long hWnd, long HWND2 ,long X,long Y, long cx,long cy,long flag) library "user32.dll"
Function Long ShowWindow( long hWnd, long HWND2) library "user32.dll"
Function Long RedrawWindow(long hWnd, long crect,long rect,long flags) library "user32.dll"
function integer GetFreeResources95 (integer WhichResource) library "rsrc32.dll" alias for "_MyGetFreeSystemResources32@4"

// ### 30149 SRM from uo_web
FUNCTION long FreeLibrary( LONG hModule) Library "kernel32.dll" 
Function int  GetKeyState( long code) Library "user32.dll"
Function long CompareString(long Locale, long dwCmpFlags,string object_data,long len,string object_data_db,long object_len_db)   library "kernel32.dll" alias for "CompareStringW"
Function long GetModuleHandle(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleW"
function long ShellExecute(long hwnd,string verb,string filename,long x1, long x2, long x3) library "shell32.dll" alias for "ShellExecuteW"
function long RegCreateKeyEx(long hkey , string path, long i,long j, long m, long key, long null2, ref long  regkey, ref long Disposition) Library "advapi32.dll" alias for "RegCreateKeyExW"
function long RegSetValueEx(long key, string name ,long i, long datatype, blob val,long len) Library "advapi32.dll" alias for "RegSetValueExW"
function long CreateWindowEx(long dwstyleex ,  string class ,  string name , long dwstyle , long x,long y,long width  , long height, long hwnd, long menu, long  inst, long extra ) library "user32.dll" ALIAS FOR "CreateWindowExW"
function long Initialize(long hwnd1,long hwnd) Library "ppcweb.dll" 
function long Start(long hwnd) Library "ppcweb.dll" 
function long Loadpage(long hwnd,string page) Library "ppcweb.dll"  alias for "Loadpage;ANSI" 
function long Loadpageframe(long hwnd,string page,string frame) Library "ppcweb.dll"  alias for "LoadpageFrame;ANSI" 
function long Updatepage(long hwnd) Library "ppcweb.dll" 
function long ChangeDir(string dir) Library "ppcweb.dll" alias for "ChangeDir;ANSI" 
function long ChangeDirW(string dir) Library "ppcweb.dll" alias for "ChangeDir" 
function long GetDir(string dir) Library "ppcweb.dll" alias for "GetDir" 
function long Register(long hwnd) Library "ppcweb.dll" alias for "Register" 
function long LoadHTML(long hwnd, string html) Library "ppcweb.dll" alias for "LoadHTML;ANSI" 
function long LoadHTMLFrame(long hwnd, string html,string frame) Library "ppcweb.dll" alias for "LoadHTMLFrame;ANSI" 
function long MouseEvent(long hwnd, long x1,long y1) Library "ppcweb.dll"
function long LoadFile(long hwnd, string file) Library "ppcweb.dll" alias for "LoadFile;ANSI" 
function long MousePressed(long hwnd, long button, long state, long x1, long y1) Library "ppcweb.dll"
function long KeyPressed(long hwnd,  long key1, long x1, long y1) Library "ppcweb.dll" 
function long Redraw( long hwnd, long updatescreen ) Library "ppcweb.dll" 
function long ExecJavascriptResults(long hwnd, string script,ref string results) Library "ppcweb.dll" 
function long ExecJavascript(long hwnd, string script) Library "ppcweb.dll" 
function long ExecJavascriptFrameResults(long hwnd, string script,ref string results, string frame) Library "ppcweb.dll" 
function long ExecJavascriptFrame(long hwnd, string script,string frame) Library "ppcweb.dll" 
function long CreateObject  (long hwnd, string object_name ) Library "ppcweb.dll" 
function long SetObjectProperty(long hwnd, string object_name, string prop_name, string  val) Library "ppcweb.dll" 
function long SetObjectCallback(long hwnd, string object_name, string  callback1) Library "ppcweb.dll" 
function long SetDBHandle(long dbhandle) Library "ppcweb.dll" 
function long Unload() Library "ppcweb.dll" 
function long Zoom(long hwnd, long pct) Library "ppcweb.dll" 
function long WebCut(long hwnd ) Library "ppcweb.dll" 
function long WebCopy(long hwnd ) Library "ppcweb.dll" 
function long WebPaste(long hwnd)Library "ppcweb.dll" 
function long WebSelectAll(long hwnd ) Library "ppcweb.dll" 
function long Print(long hwnd ) Library "ppcweb.dll" 
function long SaveAs(long hwnd, string file_name, string  a_file_type) Library "ppcweb.dll" 
function long Loadpageheader(long hwnd,string page, string headers[] ,long num) Library "ppcweb.dll"  alias for "Loadpageheader;ANSI" 
function long AddDataSource(long hwnd,string datasource_path , string datasource_name) Library "ppcweb.dll"  alias for "AddDataSource;ANSI" 

// ### 30149 SRM from w_attach_pic
FUNCTION BOOLEAN DeleteFile(string lpFileName) LIBRARY "Kernel32.dll" Alias for "DeleteFileA;Ansi"

// ### 30149 SRM from w_config
Function long ListPrinters(string name,ref string printers[], long pos,long max_printers) Library "PSCONVRT.DLL" alias for "ListPrinters;Ansi"
Function long GetFileVersion(string modname,ref string version) Library "PSCONVRT.DLL" alias for "GetFileVersion;Ansi"
Function long WNetGetUniversalName(string lpLocalPath,long dwInfoLevel,ref s_universal_name_info lpBuffer,ref long lpBufferSize)   Library "mpr.DLL" alias for "WNetGetUniversalNameA;Ansi"

// ### 30149 SRM from w_flex_names
function long GetPBDirectoryList(long hPboorca,string pblpath,ref string object_names[],ref string mod_dates[],ref string create_dates[],ref long object_types[],long len,long flag) library "psconvrt.dll" alias for "GetPBDirectoryList;Ansi"
subroutine PBORCA_SessionClose( long hORCASession )  library "psconvrt.dll"
function long PBORCA_SessionOpen( ) library "psconvrt.dll"
function long LoadPB() library "psconvrt.dll"
function long GetPBObjectDataInfo(long hPboorca,string pblpath,ref string object_names,ref string mod_date,long object_type) library "psconvrt.dll" alias for "GetPBObjectDataInfo;Ansi"

// ### 30149 SRM from w_login
function long OCIPasswordChange ( ref long svchp,ref long errcall,ref string userid, ulong userid_len,ref string passwd,ulong passwd_len,ref string passwd_new,long passwd_new_len,long mode) Library "oci.dll" alias for "OCIPasswordChange;Ansi"
function long ADsOpenObject(string lpszPathName, string lpszUserName, string lpszPassword, ulong dwReserved, s_refiid riid, ref any ole ) Library "activeds.dll" alias for "ADsOpenObject;Ansi"
function long ADSI_GetRoot(string adsi_path,string adsi_user,string adsi_passwd,ref long code) library "ppcadsi.dll" alias for "ADSI_GetRoot;Ansi"
function long ADSI_GetName(long pADSI,ref string name) library "ppcadsi.dll" alias for "ADSI_GetName;Ansi"
function long ADSI_GetClass(long pADSI,ref string name) library "ppcadsi.dll" alias for "ADSI_GetClass;Ansi"
function long ADSI_GetSchema(long pADSI,ref string name) library "ppcadsi.dll" alias for "ADSI_GetSchema;Ansi"
function long ADSI_GetADSIPath(long pADSI,ref string name) library "ppcadsi.dll" alias for "ADSI_GetADSIPath;Ansi"
function long ADSI_GetProperties(long pADSI,ref string names[]) library "ppcadsi.dll" alias for "ADSI_GetProperties;Ansi"
function long ADSI_GetChildren(long pADSI,ref long  listpADSI[]) library "ppcadsi.dll"
function long ADSI_GetAttribute(string cszAttribute,long piads, ref string attr) library "ppcadsi.dll" alias for "ADSI_GetAttribute;Ansi"
function long ADSI_GetAllProperties(long pADSI,ref string names[]) library "ppcadsi.dll" alias for "ADSI_GetAllProperties;Ansi"
function long ADSI_Free_Interface(long pInterface) library "ppcadsi.dll"
function long ADSI_Search(long pDSSearch,string adsi_filter,string adsi_attr_names,ref string names[]) library "ppcadsi.dll" alias for "ADSI_Search;Ansi"
function long ADSI_Search_Interface(string adsi_path,string adsi_user,string adsi_passwd,ref long code) library "ppcadsi.dll" alias for "ADSI_Search_Interface;Ansi"
function long ADSI_ChangePassword(string adsi_path,string adsi_user,string adsi_passwd,string oldpaswd,string newpasswd) library "ppcadsi.dll" alias for "ADSI_ChangePassword;Ansi"
function long ADSI_SetPassword(string adsi_path,string adsi_user,string adsi_passwd,string paswd) library "ppcadsi.dll" alias for "ADSI_SetPassword;Ansi"
function long ADSI_GetLastError(ref string buf,ref string sbuf) library "ppcadsi.dll" alias for "ADSI_GetLastError;Ansi"
function long NetWkstaGetInfo(long servername,long level,ref s_wksta_info_102 bufptr) library "Netapi32.dll" alias for "NetWkstaGetInfo;Ansi"
function long RegDeleteKey(long hKey,string lpSubKey) library "Advapi32.dll" alias for "RegDeleteKeyA;Ansi"

// ### 30149 SRM from w_pb_info
function long PPBORCA_SessionOpen( ) library "psconvrt.dll" alias for "PPBORCA_SessionOpen"
function long LoadPB(long ver) library "psconvrt.dll"

// ### 30149 SRM from w_post_password
function long RegCreateKeyExA(long hkey , string path, long i,long j, long m, long key, long null2, ref long  regkey, ref long Disposition) Library "advapi32.dll" alias for "RegCreateKeyExA;Ansi"
function long RegSetValueExA(long key, string name ,long i, long datatype, blob val,long len) Library "advapi32.dll" alias for "RegSetValueExA;Ansi"

// ### 30149 SRM from w_pp_about
Function long GetFileTime( long unit , ref s_filedate  create_time,  ref s_filedate last_access ,ref  s_filedate last_write) library "kernel32.dll" alias for "GetFileTime;Ansi"
Function Long FileTimeToSystemTime(s_filedate file_date ,ref  s_system_time system_date)  library "kernel32.dll" alias for "FileTimeToSystemTime;Ansi"
Function Long _lopen( string path ,long mode )  library "kernel32.dll" alias for "_lopen;Ansi"
Function Long _lclose( long unit )  library "kernel32.dll"

// ### 30149 SRM from w_pp_objects_copy
function long  PBCopyObject(string from_lib,string to_lib,string obj_name,long obj_type,long hwd) library "importpbl11.dll" alias for "PBCopyObject"

// ### 30149 SRM from w_ppc_web
function long wsprintfA( ref string szQueryStr, string formant,long langid,long usercode,string name) library "user32.dll" alias for "wsprintfA;Ansi"
function long WideCharToMultiByte(long CodePage, long dwFlags, string lpWideCharStr, long cchWideChar, ref string lpMultiByteStr, long cbMultiByte, long DefaultChar, ref long lpUsedDefaultChar ) library "kernel32.dll" alias for "WideCharToMultiByte;Ansi"
FUNCTION long SetEnvironmentVariableA(string lpName,string lpValue) library "kernel32.dll" alias for "SetEnvironmentVariableA;Ansi"

// ### 30149 SRM from w_report_dw_maint
Function long CompareStringA(long Locale, long dwCmpFlags,string object_data,long len,string object_data_db,long object_len_db)   library "kernel32.dll" alias for "CompareStringA;Ansi"

// ### 30149 SRM from w_security_ldap
FUNCTION long NetWkstaGetInfo(long server,long level,ref s_wksta_info_100 wksta_info)Library "Netapi32.dll" alias for "NetWkstaGetInfo;Ansi"
Function long NetGetDCName(long servername, long   servername ,ref string  bufptr) Library "Netapi32.dll" alias for "NetGetDCName;Ansi"

// ### 30149 SRM from w_security_sso
function long ShellExecuteA(long hwnd,string verb,string filename,long x1, long x2, long x3) library "shell32.dll" alias for "ShellExecuteA;Ansi"
function long GetVersionExA( REF s_osversioninfoex OS) library "kernel32.dll" alias for "GetVersionExA;Ansi"
function long gethostname(ref string host,long len) library "Ws2_32.dll" alias for "gethostname;Ansi"
function int WSACleanup() library "wsock32.dll"
function int WSAGetLastError ( ) library "wsock32.dll"
function long inet_addr(ref string addr) library "wsock32.dll" alias for "inet_addr;Ansi"
function long IcmpCloseHandle (long IcmpHandle) library "icmp.dll"
function long IcmpSendEcho ( long IcmpHandle, long DestinationAddress, string requestData, integer requestSize , long requestOption, ref s_icmp_echo_reply replyBuffer, long replySize, long timeout ) library "icmp.dll" alias for "IcmpSendEcho;Ansi"
function long IcmpCreateFile() library "icmp.dll"
Function long EnumProcesses(ref long lpidProcess[],long   cb,ref long cbNeeded) Library "psapi.dll" 
Function long EnumProcessModules(long hProcess,ref long lphModule[],long cb,ref long lpcbNeeded) Library "psapi.dll"
Function long GetModuleBaseName(long hProcess,long hModule,ref string lpBaseName,long nSize) Library "psapi.dll" alias for "GetModuleBaseNameA;Ansi"
Function long GetModuleFileNameEx(long hProcess,long hModule,ref string lpFilename,long nSize) Library "psapi.dll" alias for "GetModuleFileNameExA;Ansi"
Function long GetModuleInformation(long hProcess,long hModule,ref s_ws_moduleinfo lpmodinfo,long cb ) Library "psapi.dll" alias for "GetModuleInformation;Ansi"
Function long GetMappedFileName(long hProcess,ref long lpv,ref string lpFilename,long nSize) Library "psapi.dll" alias for "GetMappedFileNameA;Ansi"
Function long EnumDeviceDrivers(ref long lpImageBase,long cb,ref long lpcbNeeded) Library "psapi.dll"
Function long GetDeviceDriverBaseName(ref long ImageBase,ref string lpBaseName,long nSize) Library "psapi.dll" alias for "GetDeviceDriverBaseNameA;Ansi"
Function long GetDeviceDriverFileName(ref long ImageBase,ref string lpFilename,long nSize) Library "psapi.dll" alias for "GetDeviceDriverFileNameA;Ansi"
Function long GetWsChanges(long hProcess,ref s_ws_process_memory_counters lpWatchInfo,long cb) Library "psapi.dll" alias for "GetWsChanges;Ansi" 
Function long OpenProcess(long mode,long stat ,long pid) library "KERNEL32.DLL"
Function long CloseHandle(long handle) Library "kernel32.dll"
Function long TerminateProcess(long handle,long code) Library "kernel32.dll"
Function long OpenProcessToken(long ProcessHandle,long DesiredAccess,ref long  TokenHandle) Library "advapi32.dll"
Function long LookupPrivilegeValue(long lpSystemName,string lpName, ref s_ws_luid lpLuid) Library "advapi32.dll" alias for "LookupPrivilegeValueA;Ansi"
Function long AdjustTokenPrivileges(long TokenHandle,long DisableAllPrivileges,ref s_ws_token_privileges NewState,long BufferLength,long PreviousState,long ReturnLength) Library "advapi32.dll" alias for "AdjustTokenPrivileges;Ansi"
Function long GetCurrentProcess() Library "kernel32.dll"
function long GetFileAttributesEx(string  lpFileName,long  fInfoLevelId,ref s_ws_win32_file_attribute_data lpFileInformation) library "Kernel32.dll"  alias for "GetFileAttributesExA;Ansi"
function long GetDriveType( string path) library "Kernel32.dll" alias for "GetDriveTypeA;Ansi"
Function ulong CryptAcquireContext (Ref ulong phProv, String pszContainer, String pszProvider, ulong dwProvType, ulong dwFlags) Library "advapi32.dll" Alias For "CryptAcquireContextA;Ansi"

// ### 30149 SRM from w_show_environment
function s_hostent gethostbyname( string host) library "Ws2_32.dll" alias for "gethostbyname;Ansi"

// ### 30149 SRM from w_sort_columns
FUNCTION int SetHook(int state,int hwnd) library "moushook.dll"
FUNCTION int GetCurrentTask()  library "krnl386.exe"

// ### 30149 SRM from w_top
FUNCTION LONG GetSystemMenu(long hWnd,  int bRevert)  LIBRARY "USER32.DLL"
FUNCTION LONG InsertMenuItem(long menu_hwd,long pos ,int b,ref s_menu_info menuinfo)  LIBRARY "USER32.DLL" alias for "InsertMenuItem;Ansi"
FUNCTION LONG InsertMenuA(long menu_hwd,long pos,long flag,long id,string str)  LIBRARY "USER32.DLL" alias for "InsertMenuA;Ansi"
FUNCTION LONG ModifyMenuA(long hMnu,long pos,long flag,long id,string  text)  LIBRARY "USER32.DLL" alias for "ModifyMenuA;Ansi"
FUNCTION LONG CreatePopupMenu() LIBRARY "USER32.DLL" alias for "CreatePopupMenu"
FUNCTION LONG GetSubMenu( long menu_hwd,long pos ) LIBRARY "USER32.DLL"
FUNCTION LONG GetMenuString(long hMenu,long uIDItem,ref string menutext, long nMaxCount,long flag ) LIBRARY "USER32.DLL" alias for "GetMenuStringA;Ansi"
FUNCTION LONG DrawMenuBar( long hwd)   LIBRARY "USER32.DLL"
function ulong SetCapture(ulong a) library "user32.dll" 
Subroutine   RtlMoveMemoryFrom(ref s_minmaxinfo nmhr, ulong ptr,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi"
Subroutine   RtlMoveMemoryTo(ulong ptr , s_minmaxinfo nmhr,ulong len) library "kernel32.dll" alias for "RtlMoveMemory;Ansi"

// ### 30149 SRM from w_top_frame
Function Integer Shell_NotifyIconW(Long dwMessage, Any lpData) Library "shell32"
Function Long LoadImage (Long hInst, string lpsz, Long un1, Long n1, Long n2, Long un2) Library "user32" Alias  For "LoadImageA;Ansi" 
Function Long DestroyIcon (Long hIcon) Library "user32" Alias For "DestroyIcon"
Function Long SetForegroundWindow  (Long hwnd) Library "user32" Alias For "SetForegroundWindow"
Function Long OpenIcon  (Long hwnd) Library "user32" Alias For "OpenIcon"

// ### 30149 SRM from w_user_options
function long SetSysColors(long nelements, ref long elements,ref long RgbValues)   library "user32.dll" alias for  "SetSysColors;Ansi"
function long GetSysColors(long element)   library "user32.dll"
  
// ### 30149 SRM from n_units
function long CreateDC(string display ,long dev,long output ,long data ) library "gdi32.dll" alias for "CreateDCA;Ansi"

end prototypes

type variables
string i_ini_file = ''
string i_allow_capture
end variables

forward prototypes
public function longlong uf_playsound (string as_filename, longlong ai_option)
public function unsignedlong uf_getsyscolor (longlong ai_index)
public function longlong uf_getscreenwidth ()
public function longlong uf_getscreenheight ()
public function longlong uf_getmodulehandle (string as_modname)
public function uint uf_getsystemdirectory (ref string as_dir, uint aui_size)
public function uint uf_getdrivetype (int ai_drive)
public function unsignedlong uf_getwindowsdirectory (ref string as_dir, unsignedlong aui_size)
public function unsignedlong uf_getfreememory ()
public function longlong uf_get_logon_name (ref string as_name)
public function unsignedlong uf_get_logon_time ()
public function boolean uf_killtimer (longlong aui_handle, unsignedlong aui_id)
public function unsignedlong uf_settimer (longlong aui_handle, unsignedlong aui_id, unsignedlong aui_time)
public function boolean uf_flash_window (longlong aui_handle, boolean ab_flash)
public function unsignedlong uf_getwindow (longlong aui_handle, unsignedlong aui_relationship)
public function boolean uf_iswindowvisible (longlong aui_handle)
public function longlong uf_getwindowtext (longlong aui_handle, ref string as_text, longlong ai_max)
public function longlong uf_notify_batch (string host, string message)
public function longlong uf_setcapture (longlong hwnd)
public function boolean uf_releasecapture ()
public function longlong uf_getclassname (longlong hwnd, ref string classname, longlong maxcount)
public function boolean uf_isiconic (longlong hwnd)
public function longlong uf_getlastactivepopup (longlong hwnd)
public function longlong uf_findwindow (string classname, string title)
public function longlong uf_mcigeterrorstring (longlong err_hwnd, ref string err_str, longlong err_str_len)
public function longlong uf_mcisendstring (string cmd, ref string return_cmd, longlong return_cmd_len, longlong hwnd)
public function longlong uf_loadlibrary (string a_lib)
public function boolean uf_bringwindowtotop (longlong hwnd)
public function longlong uf_get_oracle_client_version ()
public function longlong uf_getmodulefilename (longlong a_mod, ref string a_path, longlong a_len)
public function longlong uf_setsyscolors (longlong a_num, longlong a_nelements[], longlong a_rgb[])
public function longlong uf_getfreesystemresources (longlong num)
public function longlong uf_sendmessage (longlong hwnd, unsignedlong msg, longlong wparam, string lparam)
public function longlong uf_getlocaleinfo (unsignedlong locale, unsignedlong lctype, string lplcdata, longlong cchdata)
public function longlong uf_getthreadlocale ()
public function boolean uf_filetimetolocalfiletime (s_filedate a_filetime, ref s_filedate a_localfiletime)
public function boolean uf_create_directory (string a_directory)
public function longlong uf_find_files (string a_filestring, ref string a_filenames[])
public function boolean uf_copyfile (string a_orig_filename, string a_new_filename, boolean a_overwrite)
public function longlong uf_shellexecute (longlong hwnd, string operation, string filename, string parm, string dir, longlong showcmd)
public function longlong uf_setfileattributes (string lpfilename, longlong dwfileattributes)
public function longlong uf_findwindowexa (longlong hwndparent, longlong hwndchildafter, string lpszclass, longlong lpszwindow)
public function longlong uf_findwindowa (string lpszclass, longlong lpszwindow)
public function longlong uf_getparent (longlong hwnd)
public function longlong uf_destroywindow (longlong hwnd)
public function longlong uf_closewindow (longlong hwnd)
public function longlong uf_cryptacquirecontexta (ref unsignedlong hprov, ref string pszcontainer, ref string pszprovider, unsignedlong dwprovtype, unsignedlong dwflags)
public function longlong uf_cryptreleasecontext (unsignedlong hprov, unsignedlong dwflags)
public function longlong uf_cryptcreatehash (unsignedlong hprov, unsignedinteger algid, unsignedlong hkey, unsignedlong dwflags, ref unsignedlong phhash)
public function longlong uf_crypthashdata (unsignedlong hhash, ref string pbdata, unsignedlong dwdatalen, unsignedlong dwflags)
public function longlong uf_cryptdestroyhash (unsignedlong hhash)
public function longlong uf_cryptgethashparam (unsignedlong hhash, unsignedlong dwparam, ref blob pbdata, ref unsignedlong pdwdatalen, unsignedlong dwflags)
public function unsignedlong uf_cryptderivekey (unsignedlong hprov, unsignedlong algid, unsignedlong hbasedata, unsignedlong dwflags, ref unsignedlong phkey)
public function unsignedlong uf_cryptencrypt (unsignedlong hkey, unsignedlong hhash, unsignedlong final, unsignedlong dwflags, ref blob pbdata, ref unsignedlong pdwdatalen, unsignedlong dwbuflen)
public function unsignedlong uf_cryptdestroykey (unsignedlong hkey)
public function unsignedlong uf_cryptdecrypt (unsignedlong hkey, unsignedlong hhash, unsignedlong final, unsignedlong dwflags, ref blob pbdata, ref unsignedlong pdwdatalen)
public function longlong uf_formatmessagea (longlong flag, longlong input, longlong err, longlong lang, ref string msg, longlong len, longlong arg)
public function longlong uf_getlasterror ()
public function longlong uf_wnetgetusera (longlong ptr, ref string user, ref longlong len)
public function longlong uf_printdlgexa (longlong lppd)
public function longlong uf_displayprintpropertysheet (longlong hwnd, ref string lppd)
public function longlong uf_pagesetupdlga (ref s_print_setup_dlg lppsd)
public subroutine uf_rtlmovememoryt (unsignedlong ptr, unsignedlong pt, unsignedlong len)
public function longlong uf_globallock (longlong mem)
public function longlong uf_globalunlock (longlong mem)
public function longlong uf_globalfree (longlong mem)
public function longlong uf_globalsize (longlong mem)
public function longlong uf_globalalloc (longlong uflags, longlong dwbytes)
public function longlong uf_locallock (longlong mem)
public function longlong uf_localunlock (longlong mem)
public function longlong uf_localfree (longlong mem)
public function longlong uf_localsize (longlong mem)
public function longlong uf_localalloc (longlong uflags, longlong dwbytes)
public function longlong uf_setprintera (longlong hprinter, longlong level, ref longlong devmode, longlong command)
public function longlong uf_openprintera (string pprintername, ref longlong phprinter, longlong pdefault)
public function longlong uf_closeprinter (longlong handle)
public subroutine uf_rtlmovememory (ref s_devmode nmhr, unsignedlong ptr, unsignedlong len)
public subroutine uf_rtlmovememoryd (ref s_devname nmhr, unsignedlong ptr, unsignedlong len)
public subroutine uf_rtlmovememoryp (unsignedlong ptr, s_printer plld, unsignedlong len)
public subroutine uf_rtlmovememoryu (ref s_printer s_ptr, longlong ptr, unsignedlong len)
public function longlong uf_setcurrentdirectory (string pathname)
public function longlong uf_getcurrentdirectory (longlong bufflen, ref string pathname)
public subroutine uf_initcommoncontrols ()
public function longlong uf_createwindowexa (unsignedlong dwexstyle, string classname, longlong windowname, unsignedlong dwstyle, unsignedlong x, unsignedlong y, unsignedlong nwidth, unsignedlong nheight, unsignedlong hwndparent, unsignedlong hmenu, unsignedlong hinstance, unsignedlong lpparam)
public function integer uf_tooltipmsg (longlong hwnd, longlong umsg, longlong wparam, ref s_toolinfo toolinfo)
public function integer uf_relaymsg (longlong hwnd, longlong umsg, longlong wparam, ref s_msg msg)
public function longlong uf_lstrcpy (longlong destination, string source)
public function unsignedlong uf_sendmessagestring (unsignedlong hwnd, unsignedlong msg, unsignedlong wparam, ref string lpzstring)
public function unsignedlong uf_shgetpathfromidlista (unsignedlong pidl, ref string pszpath)
public subroutine uf_cotaskmemfree (unsignedlong idlist)
public function unsignedlong uf_shbrowseforfoldera (s_browseinfo lpbrowseinfo)
public function longlong uf_getstdhandle (longlong word)
public function longlong uf_writefile (longlong hfile, string buf, longlong len, ref longlong lenw, longlong ptr)
public function longlong uf_createfilea (string filename, longlong mode, longlong share, ref longlong ptr, longlong attr, longlong dw, longlong dw2)
public subroutine uf_exitprocess (longlong uexitcode)
public function longlong uf_getenvironmentvariablea (string lpname, ref string lpbuffer, longlong nsize)
public function longlong uf_getcurrentprocessid ()
public function longlong uf_setoraclecallback (longlong handle, longlong a_hwnd, longlong a_evt)
public function longlong uf_getenvironmentvariable (string var, ref string str, longlong len)
public function longlong uf_setenvironmentvariable (string var, string val)
public function longlong uf_getmodulehandle (longlong modname)
public function longlong uf_formatmessage (longlong flag, longlong input, longlong err, longlong lang, ref string msg, longlong len, longlong arg)
public function longlong uf_getprocaddress (longlong hmod, string procname)
public function unsignedlong uf_getdc (unsignedlong hwnd)
public function unsignedlong uf_selectobject (unsignedlong hdc, unsignedlong hwnd)
public function boolean uf_gettextextentpoint32 (unsignedlong hdcr, string lpstring, longlong ncount, ref s_size size)
public function longlong uf_releasedc (unsignedlong hwnd, unsignedlong hdcr)
public function longlong uf_ftpgetfilea (longlong hftpsession, string lpszremotefile, string lpsznewfile, longlong ffailifexists, longlong dwlocalflagsandattributes, longlong dwinternetflags, longlong dwcontext)
public function longlong uf_internetconnecta (longlong hinternetsession, string servername, longlong port, string user, string password, longlong service, longlong flags, longlong context)
public function longlong uf_ftpcommanda (longlong hftpsession, longlong res, longlong flags, string cmd, longlong context, ref longlong ptr)
public function longlong uf_internetgetlastresponseinfoa (ref longlong lpdwerror, ref string lpszbuffer, ref longlong len)
public function longlong uf_internetopena (string lpszagent, longlong accesstype, longlong proxynamex, longlong proxybypass, longlong dwflags)
public function longlong uf_internetclosehandle (longlong hinet)
public function longlong uf_ftpputfilea (longlong hftpsession, string filename, string remotefile, longlong flags, longlong context)
public function longlong uf_ftpsetcurrentdirectorya (longlong hftpsession, string directory)
public function longlong uf_internetwritefile (longlong hfile, string buffer, longlong len, ref longlong retlen)
public function longlong uf_ftpopenfilea (longlong hconnect, string filename, longlong ascess, longlong filetype, longlong context)
public function longlong uf_ftpgetcurrentdirectorya (longlong hftpsession, ref string directory, ref longlong numchars)
public function longlong uf_ftprenamefilea (longlong hconnect, string existing, string new)
public function longlong uf_ftpdeletefilea (longlong hconnect, string filename)
public function longlong uf_ftpfindfirstfilea (longlong hconnect, string lpszsearchfile, ref s_finddata lpfindfiledata, longlong dwflags, longlong dwcontext)
public function longlong uf_internetquerydataavailable (longlong hconnect, ref longlong lpdwnumberofbytesavailable, longlong dwflags, longlong dwcontext)
public function longlong uf_internetreadfile (longlong hfile, ref string buffer, longlong len, ref longlong retlen)
public function longlong uf_internetfindnextfilea (longlong hinternetsession, ref s_finddata lpvfiledata)
public function longlong uf_sleep (longlong milliseconds)
public function longlong uf_shellexecutea (longlong hwnd, string verb, string filename, string parms, string dir, longlong x3)
public function longlong uf_filetimetosystemtime (ref s_filetime lpfiletime, ref s_systemtime lpsystemtime)
public function boolean uf_htmlhelp (unsignedlong hwnd, string pszfile, unsignedinteger ucommand, unsignedlong dwdata)
public function boolean uf_htmlhelp (unsignedlong hwnd, string pszfile, unsignedinteger ucommand, string dwdata)
public function longlong uf_setthreadlocale (longlong locale)
public function longlong uf_loadkeyboardlayout (ref string klid, unsignedlong flags)
public function longlong uf_getlocaleinfoa (unsignedlong locale, unsignedlong lctype, ref string lplcdata, longlong cchdata)
public function integer uf_bitblt (unsignedlong hdc, integer num, integer num, integer num, integer num, unsignedlong hdc, integer num, integer num, unsignedlong lparam)
public function unsignedlong uf_createcompatibledc (unsignedlong hdc)
public function unsignedlong uf_loadimagea (unsignedlong hints, ref string lpszname, unsignedinteger utype, integer cxdesired, integer cydesired, unsignedinteger fuload)
public function unsignedlong uf_getobjectbitmap (unsignedlong hgdiobj, integer cbbuffer, ref s_bitmap bm)
public function boolean uf_deleteobject (unsignedlong hgdiobj)
public function longlong uf_stretchblt (unsignedlong hdcdest, integer x1, integer y1, integer w1, integer h1, unsignedlong hdcsrc, integer x2, integer y2, integer w2, integer h2, unsignedlong lparam)
public function longlong uf_findwindow (string name, longlong title)
public function longlong uf_findwindows (string name, string title)
public function longlong uf_findwindowex (longlong hwndparent, longlong hwndchildafter, string lpszclass, longlong lpszwindow)
public function boolean uf_setmenuitembitmaps (unsignedlong hmenu, unsignedinteger upos, unsignedinteger flags, unsignedlong handle_bm1, unsignedlong handle_bm2)
public function unsignedlong uf_getmenuitemid (unsignedlong hmenu, unsignedinteger uitem)
public function integer uf_getsubmenu (unsignedlong hmenu, integer pos)
public function unsignedlong uf_getmenu (unsignedlong hwindow)
public function boolean uf_modifymenu (unsignedlong hmnu, unsignedlong uposition, unsignedlong uflags, unsignedlong uidnewitem, longlong lpnewi)
public function boolean uf_setmenudefault (unsignedlong hmenu, unsignedinteger upos, unsignedinteger flags)
public function unsignedlong uf_setmenuinfo (unsignedlong hmenu, s_menuinfo lpcmi)
public function unsignedlong uf_createsolidbrush (longlong crcolor)
public function unsignedlong uf_setbkcolor (longlong hdc, longlong crcolor)
public function longlong uf_fn_toolbarsetfont (longlong unknown, string fontname, longlong fontsize, longlong unknown1, string str)
public function longlong uf_fn_toolbarshowtext (longlong unknown, longlong state)
public function longlong uf_fn_toolitemsetcolor (longlong unknown, longlong color, longlong unknown, longlong unknown2)
public function longlong uf_fn_toolitemhwndfirst (longlong unknown, longlong unknown2, ref longlong unknown3)
public function longlong uf_fn_toolgroupfirst (longlong unknown)
public function longlong uf_sendmessagess (longlong hwnd, longlong msg, longlong wparam, ref s_colorschema lparam)
public function longlong uf_sendmessage (longlong hwnd, longlong msg, longlong wparam, longlong lparam)
public function longlong uf_getclientrect (longlong hwnd, ref s_rect lprect)
public function longlong uf_fn_toolbarsize (longlong unknown, ref s_rect lprect)
public function longlong uf_fillrect (longlong hdc, ref s_rect rect, longlong brush)
public function longlong uf_getwindowlong (unsignedlong hwnd, integer nindex)
public function longlong uf_setwindowlong (unsignedlong hwnd, integer nindex, longlong dwnewlong)
public function longlong uf_setlayeredwindowattributes (longlong hwnd, longlong crkey, character balpha, longlong dwflags)
public function longlong uf_fn_toolgroupreset (longlong unknown, longlong unknown)
public function longlong uf_fn_toolgroupremove (longlong unknown, longlong unknown)
public function longlong uf_psinterface (string dir, string infile, string outfile, longlong typex, longlong orientation, ref s_search_info search_info)
public function longlong uf_createdca (string lpszdriver, string lpszdevice, string lpszoutput, longlong data)
public function longlong uf_deletedc (longlong hdc)
public function longlong uf_getdevicecaps (longlong hdc, longlong x)
public function longlong uf_getprinterorientation ()
public function longlong uf_getprintdef (ref longlong orig, ref longlong postscript)
public function longlong uf_getpostscriptprinter (ref string name, ref string drv, ref string port)
public function longlong uf_getprinterinfo (string printer, ref string name, ref string drv, ref string port)
public function longlong uf_findaddpostprinter (ref string name, ref string drv, ref string port)
public function longlong uf_addprinterdrivera (longlong name, longlong level, s_driver_info2 driverinfo)
public function longlong uf_deleteprinterdrivera (longlong name, longlong config, string driver)
public function longlong uf_addprintera (longlong name, longlong level, s_printer_info2_add driverinfo)
public function longlong uf_copyfilea (string path, string desination, longlong stat)
public function longlong uf_getprintdefpcl (ref longlong orig, ref longlong postscript)
public function longlong uf_getpclprinter (ref string name, ref string drv, ref string port)
public function longlong uf_pclinterface (string dir, string infile, string outfile, longlong typex, longlong orientation, ref s_search_info search_info)
public function longlong uf_getprinterorientationpcl ()
public function longlong uf_setdefprinter (string pprintername)
public function longlong uf_getdefprinter (ref string pprintername, ref longlong pdwbuffersize)
public function longlong uf_getprinter (longlong hwnd, longlong ptype, s_printer_info2 printer_info2, longlong num, ref longlong rnum)
public function longlong uf_add_printer (longlong apost, string ptr, string drv)
public function longlong uf_getshortpatha (string longpath, ref string shortpath, longlong buflen)
public function longlong uf_getlongpathnamea (string shortpath, ref string longpath, longlong buflen)
public function longlong uf_createwindowexa (longlong dwstyleex, string class, string name, longlong dwstyle, longlong x, longlong y, longlong width, longlong height, longlong hwnd, longlong menu, longlong inst, longlong extra)
public function longlong uf_getwindowlonga (longlong hwnd, longlong nindex)
public function longlong uf_sendmessages (longlong hwnd, longlong msg, longlong wparam, ref string lparam)
public function longlong uf_sendmessages2 (longlong hwnd, longlong msg, string wparam, ref string lparam)
public function longlong uf_sendmessagei (longlong hwnd, longlong msg, longlong wparam, longlong lparam)
public function longlong uf_postmessagei (longlong hwnd, longlong msg, longlong wparam, longlong lparam)
public function longlong uf_sendmessageir (longlong hwnd, longlong msg, longlong wparam, ref longlong lparam)
public function string uf_sendmessageis (longlong hwnd, longlong msg, longlong wparam, longlong lparam)
public function longlong uf_getfontlist (ref string fonts[], longlong count)
public function longlong uf_getmenu (longlong hinst)
public function longlong uf_getmodulehandlea (longlong modname)
public subroutine uf_rtlmovememory (ref s_notifyheader nmhr, unsignedlong ptr, unsignedlong len)
public function longlong uf_movewindow (longlong hwnd, longlong x, longlong y, longlong nwidth, longlong nheight, longlong brepaint)
public function longlong uf_wnetgetuser (longlong ptr, ref string user, ref longlong len)
public function longlong uf_lstrcmpw (string str1, string str2)
public function longlong uf_hashdata (string pbdata, longlong cbdata, ref unsignedlong pbhash, longlong cbhash)
public function longlong uf_smtpmail (string fromuser, string subject, string text, string touser, string server)
public function longlong uf_smtpmailex (string fromuser, string subject, string text, string touser, string server, string filename, string err)
public function longlong uf_smtpmailpasswd (string fromuser, string user, string passwd, string subject, string text, string touser, string server)
public function longlong uf_smtpmailpasswdex (string fromuser, string user, string passwd, string subject, string text, string touser, string server, string filename, string err)
public function longlong uf_sendmessagea (longlong hwnd, longlong msg, longlong wparam, longlong lparam)
public function longlong uf_createdca (string display, longlong x, longlong y, longlong z)
public function longlong uf_getdc (longlong hwnd)
public function longlong uf_getwindowdc (longlong hwnd)
public function longlong uf_releasedc (longlong hdc, longlong hwnd)
public function longlong uf_settextcolor (longlong hdc, longlong col)
public function longlong uf_textouta (longlong hdc, longlong x, longlong y, string text, longlong len)
public function longlong uf_setbkmode (longlong hdc, longlong mode)
public function longlong uf_setclasslonga (longlong hwnd, longlong index, longlong val)
public function longlong uf_selectobject (longlong hdc, longlong brush)
public function longlong uf_setwindowpos (longlong hwnd, longlong hwnd2, longlong x, longlong y, longlong cx, longlong cy, longlong flag)
public function longlong uf_showwindow (longlong hwnd, longlong hwnd2)
public function longlong uf_redrawwindow (longlong hwnd, longlong crect, longlong rect, longlong flags)
public function integer uf_getfreeresources95 (integer whichresource)
public function longlong uf_freelibrary (longlong hmodule)
public function integer uf_getkeystate (longlong code)
public function longlong uf_comparestring (longlong locale, longlong dwcmpflags, string object_data, longlong len, string object_data_db, longlong object_len_db)
public function longlong uf_shellexecute (longlong hwnd, string verb, string filename, longlong x1, longlong x2, longlong x3)
public function longlong uf_regcreatekeyex (longlong hkey, string path, longlong i, longlong j, longlong m, longlong key, longlong null2, ref longlong regkey, ref longlong disposition)
public function longlong uf_regsetvalueex (longlong key, string name, longlong i, longlong datatype, blob val, longlong len)
public function longlong uf_createwindowex (longlong dwstyleex, string class, string name, longlong dwstyle, longlong x, longlong y, longlong width, longlong height, longlong hwnd, longlong menu, longlong inst, longlong extra)
public function longlong uf_initialize (longlong hwnd1, longlong hwnd)
public function longlong uf_start (longlong hwnd)
public function longlong uf_loadpage (longlong hwnd, string page)
public function longlong uf_loadpageframe (longlong hwnd, string page, string frame)
public function longlong uf_updatepage (longlong hwnd)
public function longlong uf_changedir (string dir)
public function longlong uf_changedirw (string dir)
public function longlong uf_getdir (string dir)
public function longlong uf_register (longlong hwnd)
public function longlong uf_loadhtml (longlong hwnd, string html)
public function longlong uf_loadhtmlframe (longlong hwnd, string html, string frame)
public function longlong uf_mouseevent (longlong hwnd, longlong x1, longlong y1)
public function longlong uf_loadfile (longlong hwnd, string file)
public function longlong uf_mousepressed (longlong hwnd, longlong button, longlong state, longlong x1, longlong y1)
public function longlong uf_keypressed (longlong hwnd, longlong key1, longlong x1, longlong y1)
public function longlong uf_redraw (longlong hwnd, longlong updatescreen)
public function longlong uf_execjavascriptresults (longlong hwnd, string script, ref string results)
public function longlong uf_execjavascript (longlong hwnd, string script)
public function longlong uf_execjavascriptframeresults (longlong hwnd, string script, ref string results, string frame)
public function longlong uf_execjavascriptframe (longlong hwnd, string script, string frame)
public function longlong uf_createobject (longlong hwnd, string object_name)
public function longlong uf_setobjectproperty (longlong hwnd, string object_name, string prop_name, string val)
public function longlong uf_setobjectcallback (longlong hwnd, string object_name, string callback1)
public function longlong uf_setdbhandle (longlong dbhandle)
public function longlong uf_unload ()
public function longlong uf_zoom (longlong hwnd, longlong pct)
public function longlong uf_webcut (longlong hwnd)
public function longlong uf_webcopy (longlong hwnd)
public function longlong uf_webpaste (longlong hwnd)
public function longlong uf_webselectall (longlong hwnd)
public function longlong uf_print (longlong hwnd)
public function longlong uf_saveas (longlong hwnd, string file_name, string a_file_type)
public function longlong uf_loadlibrarya (string a_lib)
public function longlong uf_getmodulehandlea (string modname)
public subroutine uf_globalmemorystatus (ref str_memorystatus memorystatus)
public function boolean uf_deletefile (string lpfilename)
public function longlong uf_listprinters (string name, ref string printers[], longlong pos, longlong max_printers)
public function longlong uf_getfileversion (string modname, ref string version)
public function longlong uf_wnetgetuniversalname (string lplocalpath, longlong dwinfolevel, ref s_universal_name_info lpbuffer, ref longlong lpbuffersize)
public function longlong uf_getpbdirectorylist (longlong hpboorca, string pblpath, ref string object_names[], ref string mod_dates[], ref string create_dates[], ref longlong object_types[], longlong len, longlong flag)
public subroutine uf_pborca_sessionclose (longlong horcasession)
public function longlong uf_pborca_sessionopen ()
public function longlong uf_loadpb ()
public function longlong uf_getpbobjectdatainfo (longlong hpboorca, string pblpath, ref string object_names, ref string mod_date, longlong object_type)
public function longlong uf_ocipasswordchange (ref longlong svchp, ref longlong errcall, ref string userid, unsignedlong userid_len, ref string passwd, unsignedlong passwd_len, ref string passwd_new, longlong passwd_new_len, longlong mode)
public function longlong uf_adsopenobject (string lpszpathname, string lpszusername, string lpszpassword, unsignedlong dwreserved, s_refiid riid, ref any ole)
public function longlong uf_adsi_getroot (string adsi_path, string adsi_user, string adsi_passwd, ref longlong code)
public function longlong uf_adsi_getname (longlong padsi, ref string name)
public function longlong uf_adsi_getclass (longlong padsi, ref string name)
public function longlong uf_adsi_getschema (longlong padsi, ref string name)
public function longlong uf_adsi_getadsipath (longlong padsi, ref string name)
public function longlong uf_adsi_getproperties (longlong padsi, ref string names[])
public function longlong uf_adsi_getchildren (longlong padsi, ref longlong listpadsi[])
public function longlong uf_adsi_getattribute (string cszattribute, longlong piads, ref string attr)
public function longlong uf_adsi_getallproperties (longlong padsi, ref string names[])
public function longlong uf_adsi_free_interface (longlong pinterface)
public function longlong uf_adsi_search (longlong pdssearch, string adsi_filter, string adsi_attr_names, ref string names[])
public function longlong uf_adsi_search_interface (string adsi_path, string adsi_user, string adsi_passwd, ref longlong code)
public function longlong uf_adsi_changepassword (string adsi_path, string adsi_user, string adsi_passwd, string oldpaswd, string newpasswd)
public function longlong uf_adsi_setpassword (string adsi_path, string adsi_user, string adsi_passwd, string paswd)
public function longlong uf_adsi_getlasterror (ref string buf, ref string sbuf)
public function longlong uf_netwkstagetinfo (longlong servername, longlong level, ref s_wksta_info_102 bufptr)
public function longlong uf_regdeletekey (longlong hkey, string lpsubkey)
public function longlong uf_ppborca_sessionopen ()
public function longlong uf_loadpb (longlong ver)
public function longlong uf_regcreatekeyexa (longlong hkey, string path, longlong i, longlong j, longlong m, longlong key, longlong null2, ref longlong regkey, ref longlong disposition)
public function longlong uf_regsetvalueexa (longlong key, string name, longlong i, longlong datatype, blob val, longlong len)
public function longlong uf_getfiletime (longlong unit, ref s_filedate create_time, ref s_filedate last_access, ref s_filedate last_write)
public function longlong uf_filetimetosystemtime (s_filedate file_date, ref s_system_time system_date)
public function longlong uf_lopen (string path, longlong mode)
public function longlong uf_lclose (longlong unit)
public function longlong uf_pbcopyobject (string from_lib, string to_lib, string obj_name, longlong obj_type, longlong hwd)
public function longlong uf_wsprintfa (ref string szquerystr, string formant, longlong langid, longlong usercode, string name)
public function longlong uf_widechartomultibyte (longlong codepage, longlong dwflags, string lpwidecharstr, longlong cchwidechar, ref string lpmultibytestr, longlong cbmultibyte, longlong defaultchar, ref longlong lpuseddefaultchar)
public function longlong uf_setenvironmentvariablea (string lpname, string lpvalue)
public function longlong uf_comparestringa (longlong locale, longlong dwcmpflags, string object_data, longlong len, string object_data_db, longlong object_len_db)
public function longlong uf_getuserdefaultlangid ()
public function longlong uf_getfileversioninfosizea (string filename, ref longlong size)
public function longlong uf_getfileversioninfoa (string filename, longlong dwhandle, longlong dwlen, ref blob lpdata)
public function longlong uf_verqueryvaluea (blob pblock, string lpsubblock, ref string lplpbuffer, ref longlong pulen)
public function longlong uf_getdrivetypea (string path)
public function unsignedlong uf_cryptacquirecontext (ref unsignedlong phprov, string pszcontainer, string pszprovider, unsignedlong dwprovtype, unsignedlong dwflags)
public function longlong uf_getversionexa (ref s_osversioninfoex os)
public function longlong uf_gethostname (ref string host, longlong len)
public function s_hostent uf_gethostbyname (string host)
public function longlong uf_icmpclosehandle (longlong icmphandle)
public function longlong uf_icmpsendecho (longlong icmphandle, longlong destinationaddress, string requestdata, integer requestsize, longlong requestoption, ref s_icmp_echo_reply replybuffer, longlong replysize, longlong timeout)
public function longlong uf_icmpcreatefile ()
public function longlong uf_enumprocesses (ref longlong lpidprocess[], longlong cb, ref longlong cbneeded)
public function longlong uf_enumprocessmodules (longlong hprocess, ref longlong lphmodule[], longlong cb, ref longlong lpcbneeded)
public function longlong uf_getmodulebasename (longlong hprocess, longlong hmodule, ref string lpbasename, longlong nsize)
public function longlong uf_getmodulefilenameex (longlong hprocess, longlong hmodule, ref string lpfilename, longlong nsize)
public function longlong uf_openprocess (longlong mode, longlong stat, longlong pid)
public function longlong uf_closehandle (longlong handle)
public function longlong uf_terminateprocess (longlong handle, longlong code)
public function longlong uf_openprocesstoken (longlong processhandle, longlong desiredaccess, ref longlong tokenhandle)
public function longlong uf_lookupprivilegevalue (longlong lpsystemname, string lpname, ref s_ws_luid lpluid)
public function longlong uf_adjusttokenprivileges (longlong tokenhandle, longlong disableallprivileges, ref s_ws_token_privileges newstate, longlong bufferlength, longlong previousstate, longlong returnlength)
public function longlong uf_getcurrentprocess ()
public function longlong uf_getfileattributesex (string lpfilename, longlong finfolevelid, ref s_ws_win32_file_attribute_data lpfileinformation)
public function longlong uf_getdrivetype (string path)
public function integer uf_sethook (integer state, integer hwnd)
public function integer uf_getcurrenttask ()
public function longlong uf_getsystemmenu (longlong hwnd, integer brevert)
public function longlong uf_insertmenuitem (longlong menu_hwd, longlong pos, integer b, ref s_menu_info menuinfo)
public function longlong uf_insertmenua (longlong menu_hwd, longlong pos, longlong flag, longlong id, string str)
public function longlong uf_modifymenua (longlong hmnu, longlong pos, longlong flag, longlong id, string text)
public function longlong uf_createpopupmenu ()
public function longlong uf_getsubmenu (longlong menu_hwd, longlong pos)
public function longlong uf_getmenustring (longlong hmenu, longlong uiditem, ref string menutext, longlong nmaxcount, longlong flag)
public function longlong uf_drawmenubar (longlong hwd)
public function unsignedlong uf_setcapture (unsignedlong a)
public subroutine uf_rtlmovememoryfrom (ref s_minmaxinfo nmhr, unsignedlong ptr, unsignedlong len)
public subroutine uf_rtlmovememoryto (unsignedlong ptr, s_minmaxinfo nmhr, unsignedlong len)
public function integer uf_shell_notifyiconw (longlong dwmessage, any lpdata)
public function longlong uf_loadimage (longlong hinst, string lpsz, longlong un1, longlong n1, longlong n2, longlong un2)
public function longlong uf_destroyicon (longlong hicon)
public function longlong uf_setforegroundwindow (longlong hwnd)
public function longlong uf_openicon (longlong hwnd)
public function longlong uf_setsyscolors (longlong nelements, ref longlong elements, ref longlong rgbvalues)
public function longlong uf_getsyscolors (longlong element)
public function string uf_getfileversion (string a_file)
public function datetime uf_getfiledate (string a_file, string a_date_type)
public function string uf_find_path (string a_name)
public function longlong uf_printdlg (longlong lppd)
public function longlong uf_commdlgextendederror ()
public subroutine uf_rtlmovememoryps (unsignedlong ptr, s_print_dlg plld, unsignedlong len)
public subroutine uf_rtlmovememoryr (unsignedlong ptr, s_print_range plld, unsignedlong len)
public function longlong uf_createdc (string display, longlong dev, longlong output, longlong data)
public function longlong uf_cryptgenkey (unsignedlong hprov, unsignedinteger algid, unsignedlong dwflags, ref unsignedlong phkey)
public function integer uf_getscrollpos (longlong hwnd, integer sb_vert)
public function longlong uf_gettimezoneinformation (ref s_time_zone_information a_time_zone)
end prototypes

public function longlong uf_playsound (string as_filename, longlong ai_option);//Options as defined in mmystem.h These may be or'd together.

//#define SND_SYNC            0x0000  /* play synchronously (default) */
//#define SND_ASYNC           0x0001  /* play asynchronously */
//#define SND_NODEFAULT       0x0002  /* don't use default sound */
//#define SND_MEMORY          0x0004  /* lpszSoundName points to a memory file */
//#define SND_LOOP            0x0008  /* loop the sound until next sndPlaySound */
//#define SND_NOSTOP          0x0010  /* don't stop any currently playing sound */    


uint lui_numdevs


lui_numdevs = WaveOutGetNumDevs() 
If lui_numdevs > 0 Then 
	sndPlaySoundA(as_filename, long(ai_option))
	return 1
Else
	return -1
End If
end function

public function unsignedlong uf_getsyscolor (longlong ai_index);//GetsysColor in win32
Return getsyscolor(long(ai_index))
end function

public function longlong uf_getscreenwidth ();Return GetSystemMetrics(0)
end function

public function longlong uf_getscreenheight ();Return GetSystemMetrics(1)
end function

public function longlong uf_getmodulehandle (string as_modname);Return GetModuleHandle(as_modname)
end function

public function uint uf_getsystemdirectory (ref string as_dir, uint aui_size);Return GetSystemDirectoryA(as_dir,aui_size)
end function

public function uint uf_getdrivetype (int ai_drive);//drive types
Return GetDriveTypeA(Char(ai_drive + Asc ('A')) + ":\")
end function

public function unsignedlong uf_getwindowsdirectory (ref string as_dir, unsignedlong aui_size);Return GetWindowsDirectoryA(as_dir,aui_size)
end function

public function unsignedlong uf_getfreememory ();//return # bytes free memory

str_memorystatus lstr_memory

//structure size is 8 ulong's or 8 * 4 bytes
lstr_memory.sul_dwlength = 32

GlobalMemoryStatus(lstr_memory)

//bytes of virtual memory available
Return (lstr_memory.sul_dwavailpagefile)
end function

public function longlong uf_get_logon_name (ref string as_name);//user win32 getusername

string ls_temp 
ulong lul_value =255
boolean lb_rc
ls_temp = string(255)
lb_rc = GetUserNameA(ls_temp,lul_value)
If lb_rc Then
	as_name = ls_temp
	Return 1
Else 
	Return -1
End If
end function

public function unsignedlong uf_get_logon_time ();//use win32 gettickcount() for time logged in

Return GetTickCount()
end function

public function boolean uf_killtimer (longlong aui_handle, unsignedlong aui_id);//win32 to kill timer
Return killtimer(long(aui_handle),aui_id)
end function

public function unsignedlong uf_settimer (longlong aui_handle, unsignedlong aui_id, unsignedlong aui_time);//win 32 call to create timer
Return settimer(long(aui_handle),aui_id,aui_time,0)

end function

public function boolean uf_flash_window (longlong aui_handle, boolean ab_flash);//function not found in descendent
Return flashwindow(long(aui_handle), ab_flash)
end function

public function unsignedlong uf_getwindow (longlong aui_handle, unsignedlong aui_relationship);//function not found
Return getwindow(long(aui_handle),aui_relationship)
end function

public function boolean uf_iswindowvisible (longlong aui_handle);Return iswindowvisible(long(aui_handle))
end function

public function longlong uf_getwindowtext (longlong aui_handle, ref string as_text, longlong ai_max);//function not found
Return getwindowtexta(long(aui_handle),as_text, long(ai_max))
end function

public function longlong uf_notify_batch (string host, string message);if fileexists('notbt32.dll')  then
 return notifybat(host,message)
end if
return 0

end function

public function longlong uf_setcapture (longlong hwnd);if i_allow_capture <> 'YES' and i_allow_capture <> 'NO' then
	i_allow_capture = upper(trim(ProfileString(i_ini_file, "Application", "EnableSetCapture", "yes")))
end if

if i_allow_capture = 'YES' then
	return setcapture(long(hwnd))
else
	return 0
end if
end function

public function boolean uf_releasecapture ();return releasecapture()
end function

public function longlong uf_getclassname (longlong hwnd, ref string classname, longlong maxcount);return getclassnamea(long(hwnd), ClassName, long(maxcount))
end function

public function boolean uf_isiconic (longlong hwnd);return isiconic(long(hwnd))
end function

public function longlong uf_getlastactivepopup (longlong hwnd);return getlastactivepopup(long(hwnd))
end function

public function longlong uf_findwindow (string classname, string title);return findwindowA(classname,title)
 
end function

public function longlong uf_mcigeterrorstring (longlong err_hwnd, ref string err_str, longlong err_str_len);return mcigeterrorstringa(long(err_hwnd), err_str, long(err_str_len))
end function

public function longlong uf_mcisendstring (string cmd, ref string return_cmd, longlong return_cmd_len, longlong hwnd);
return mciSendStringA ( cmd, return_cmd, long(return_cmd_len), long(hwnd))
end function

public function longlong uf_loadlibrary (string a_lib);return LoadLibrary(a_lib)
end function

public function boolean uf_bringwindowtotop (longlong hwnd);return bringwindowtotop(long(hwnd))
end function

public function longlong uf_get_oracle_client_version ();blob data
longlong CP_ACP = 0
longlong WC_SEPCHARS = 32
long len
string query_str = "\StringFileInfo\040904b0\"
string query_str2 = "\StringFileInfo\040904e4\"
long size
string szQueryStr
s_string s_str
string str,str2
longlong id,status
longlong mod
string path,prog_path
LoadLibraryA('ociw32.dll')
mod =  GetModuleHandleA('ociw32.dll')
if mod <> 0 then
	path = space(601)
	status = GetModuleFileNameA(mod,path,600)
	if status <> 0 then
		len = status		
	end if
else
	LoadLibraryA('oci.dll')
	mod =  GetModuleHandleA('oci.dll')
	if mod <> 0 then
		path = space(601)
		status = GetModuleFileNameA(mod,path,600)
		if status <> 0 then
			len = status		
		end if
	end if
end if


id = GetUserDefaultLangID()
len = GetFileVersionInfoSizeA(path, size  ) 

data = blob(space(len))
GetFileVersionInfoA(path, 0,len , data) 



szQueryStr = query_str + "ProductVersion"
s_str.name = space(100)
status = VerQueryValueA( data,szQueryStr,s_str,len )
if status = 0 then
	szQueryStr = query_str2 + "ProductVersion"
	s_str.name = space(100)
	status = VerQueryValueA( data,szQueryStr,s_str,len )
end if
if status = 0 then
   return -1
end if
if mid(s_str.name,1,3) = '8.1' then
	return 81
elseif  mid(s_str.name,1,3) = '8.0' then
	return 80
elseif  mid(s_str.name,1,3) = '7.3' then
	return 73
elseif mid(s_str.name,1,3) = '9.1' then
	return 91
elseif mid(s_str.name,1,3) = '9.2' then
	return 92
elseif mid(s_str.name,1,4) = '10.1' then
	return 101
elseif mid(s_str.name,1,4) = '10.2' then
	return 102
else
	return 0
end if	
	
//a_version = s_str.name
return 0
end function

public function longlong uf_getmodulefilename (longlong a_mod, ref string a_path, longlong a_len);longlong status
 
status = getmodulefilename(long(a_mod),a_path, long(a_len))


return status


end function

public function longlong uf_setsyscolors (longlong a_num, longlong a_nelements[], longlong a_rgb[]);longlong code

code = setsyscolors(a_num, a_nelements, a_rgb)

return code
end function

public function longlong uf_getfreesystemresources (longlong num);return 0
end function

public function longlong uf_sendmessage (longlong hwnd, unsignedlong msg, longlong wparam, string lparam);return sendmessage(long(hwnd),msg, long(wparam),lparam)


end function

public function longlong uf_getlocaleinfo (unsignedlong locale, unsignedlong lctype, string lplcdata, longlong cchdata);return getlocaleinfo(locale,lctype,lplcdata, long(cchdata))
end function

public function longlong uf_getthreadlocale ();return getthreadlocale()
end function

public function boolean uf_filetimetolocalfiletime (s_filedate a_filetime, ref s_filedate a_localfiletime);return FileTimeToLocalFileTime(a_FileTime,a_LocalFileTime) 
end function

public function boolean uf_create_directory (string a_directory);// ### 8174: JAK: 2011-07-27:  new uf_create_directory function

string tmp_dir
longlong i
boolean rc

//Need to build the directory one level at a time (look for backslashes to find each directory level)
for i = 1 to len(a_directory)
	tmp_dir = mid(a_directory,1,i)
	if right(a_directory, 1) = '\' then
		if not FileExists(tmp_dir) then
			rc = CreateDirectoryA(tmp_dir, 0)
			if not rc then
				return rc
			end if
		end if
	end if
next

if not FileExists(a_directory) then
	return CreateDirectoryA(a_directory, 0)
end if

return true
end function

public function longlong uf_find_files (string a_filestring, ref string a_filenames[]);// ### 8250: JAK: 2011-11-01:  copyfile and find files functions
longlong handle, i, count = 0, j, rtn
string errtext, filename
longlong ascii

s_finddata temp_data
s_finddata files[]

a_filestring = a_filestring + '*'

for i = 1 to upperbound(temp_data.cfilename)
	temp_data.cfilename[i] = ' '
next

if len(a_filestring) = 0 then 
	setNull(a_filestring)
end if

// get the first file's data
handle = FindFirstFile(a_filestring, temp_data)

if isNull(handle) or handle <= 0 then
	return 0
end if

count++

files[count] = temp_data

// loop through until there are no more files meeting the criteria
j = 0
rtn = 1
do while rtn = 1
	
	for i = 1 to upperbound(temp_data.cfilename)
		temp_data.cfilename[i] = ' '
	next
	
	rtn = FindNextFile(handle, temp_data)
	
	if rtn = 1 then
		count ++
		files[count] = temp_data
	end if
loop

// populate array with filenames
for i = 1 to count
	filename = ""
	j = 1
	ascii = asc(files[i].cfilename[j])
	do until ascii = 0 or j = 260
		filename += string(files[i].cfilename[j])
		j ++
		ascii = asc(files[i].cfilename[j])
	loop
	
	a_filenames[i] = trim(filename)

next

return count
end function

public function boolean uf_copyfile (string a_orig_filename, string a_new_filename, boolean a_overwrite);// ### 8250: JAK: 2011-11-01:  copyfile and find files functions
return CopyFile(a_orig_filename,a_new_filename,a_overwrite)
end function

public function longlong uf_shellexecute (longlong hwnd, string operation, string filename, string parm, string dir, longlong showcmd);return shellexecute(long(hwnd), operation, filename, parm, dir, long(showcmd))

end function

public function longlong uf_setfileattributes (string lpfilename, longlong dwfileattributes);return SetFileAttributes(lpFileName, long(dwfileattributes))
end function

public function longlong uf_findwindowexa (longlong hwndparent, longlong hwndchildafter, string lpszclass, longlong lpszwindow);return findwindowexa(long(hwndparent), long(hwndchildafter), lpszClass, long(lpszwindow)) 
end function

public function longlong uf_findwindowa (string lpszclass, longlong lpszwindow);return FindWindowA(lpszClass, long(lpszwindow))  
end function

public function longlong uf_getparent (longlong hwnd);return getparent(long(hwnd))
end function

public function longlong uf_destroywindow (longlong hwnd);return destroywindow(long(hwnd))
end function

public function longlong uf_closewindow (longlong hwnd);return closewindow(long(hwnd))
end function

public function longlong uf_cryptacquirecontexta (ref unsignedlong hprov, ref string pszcontainer, ref string pszprovider, unsignedlong dwprovtype, unsignedlong dwflags);return CryptAcquireContextA (hProv, pszContainer, pszProvider, dwProvType, dwFlags)
end function

public function longlong uf_cryptreleasecontext (unsignedlong hprov, unsignedlong dwflags);return CryptReleaseContext (hProv, dwFlags)
end function

public function longlong uf_cryptcreatehash (unsignedlong hprov, unsignedinteger algid, unsignedlong hkey, unsignedlong dwflags, ref unsignedlong phhash);return CryptCreateHash (hProv, Algid, hKey, dwFlags, phHash)
end function

public function longlong uf_crypthashdata (unsignedlong hhash, ref string pbdata, unsignedlong dwdatalen, unsignedlong dwflags);return CryptHashData (hHash, pbData, dwDataLen, dwFlags)
end function

public function longlong uf_cryptdestroyhash (unsignedlong hhash);return CryptDestroyHash (hHash)
end function

public function longlong uf_cryptgethashparam (unsignedlong hhash, unsignedlong dwparam, ref blob pbdata, ref unsignedlong pdwdatalen, unsignedlong dwflags);return CryptGetHashParam (hHash, dwParam, pbData, pdwDataLen, dwFlags)
end function

public function unsignedlong uf_cryptderivekey (unsignedlong hprov, unsignedlong algid, unsignedlong hbasedata, unsignedlong dwflags, ref unsignedlong phkey);return CryptDeriveKey (hProv, Algid, hBaseData, dwFlags, phKey)
end function

public function unsignedlong uf_cryptencrypt (unsignedlong hkey, unsignedlong hhash, unsignedlong final, unsignedlong dwflags, ref blob pbdata, ref unsignedlong pdwdatalen, unsignedlong dwbuflen);return CryptEncrypt (hKey, hHash, Final, dwFlags, pbData, pdwDataLen, dwBufLen)
end function

public function unsignedlong uf_cryptdestroykey (unsignedlong hkey);return CryptDestroyKey (hKey)
end function

public function unsignedlong uf_cryptdecrypt (unsignedlong hkey, unsignedlong hhash, unsignedlong final, unsignedlong dwflags, ref blob pbdata, ref unsignedlong pdwdatalen);return CryptDecrypt (hKey, hHash, Final, dwFlags, pbData, pdwDataLen)
end function

public function longlong uf_formatmessagea (longlong flag, longlong input, longlong err, longlong lang, ref string msg, longlong len, longlong arg);return formatmessagea(long(flag), long(input), long(err), long(lang), msg, long(len), long(arg))
end function

public function longlong uf_getlasterror ();return GetLastError()
end function

public function longlong uf_wnetgetusera (longlong ptr, ref string user, ref longlong len);
long long_len
longlong ret_val
long_len = long(len)
ret_val = wnetgetusera(long(ptr), user, long_len)
len = longlong(long_len)
return ret_val
end function

public function longlong uf_printdlgexa (longlong lppd);return printdlgexa(long(lppd))
end function

public function longlong uf_displayprintpropertysheet (longlong hwnd, ref string lppd);return displayprintpropertysheet(long(hwnd), lppd ) 
end function

public function longlong uf_pagesetupdlga (ref s_print_setup_dlg lppsd);
return PageSetupDlgA( lppsd ) 
end function

public subroutine uf_rtlmovememoryt (unsignedlong ptr, unsignedlong pt, unsignedlong len);
RtlMoveMemoryT( ptr,  pt, len)
end subroutine

public function longlong uf_globallock (longlong mem);return globallock(long(mem))
end function

public function longlong uf_globalunlock (longlong mem);return globalunlock(long(mem))
end function

public function longlong uf_globalfree (longlong mem);return globalfree(long(mem))
end function

public function longlong uf_globalsize (longlong mem);return globalsize(long(mem))
end function

public function longlong uf_globalalloc (longlong uflags, longlong dwbytes);return globalalloc(long(uflags), long(dwbytes))
end function

public function longlong uf_locallock (longlong mem);
return locallock(long(mem))
end function

public function longlong uf_localunlock (longlong mem);
return localunlock(long(mem))
end function

public function longlong uf_localfree (longlong mem);
return localfree(long(mem))
end function

public function longlong uf_localsize (longlong mem);
return localsize(long(mem))
end function

public function longlong uf_localalloc (longlong uflags, longlong dwbytes);
return localalloc(long(uflags), long(dwbytes))
end function

public function longlong uf_setprintera (longlong hprinter, longlong level, ref longlong devmode, longlong command);
long long_devmode
longlong ret_val
long_devmode = long(devmode)
ret_val = setprintera(long(hprinter), long(level), long_devmode, long(command)) 
devmode = longlong(long_devmode)
return ret_val 
end function

public function longlong uf_openprintera (string pprintername, ref longlong phprinter, longlong pdefault);
long long_phprinter
longlong ret_val
long_phprinter = long(phprinter)
ret_val = OpenPrinterA( pPrinterName, long_phprinter, long(pdefault))
phprinter = longlong(long_phprinter)
return ret_val 
end function

public function longlong uf_closeprinter (longlong handle);return closeprinter(long(handle)) 
end function

public subroutine uf_rtlmovememory (ref s_devmode nmhr, unsignedlong ptr, unsignedlong len);RtlMoveMemory( nmhr, ptr, len) 
end subroutine

public subroutine uf_rtlmovememoryd (ref s_devname nmhr, unsignedlong ptr, unsignedlong len);RtlMoveMemoryD( nmhr, ptr, len)
end subroutine

public subroutine uf_rtlmovememoryp (unsignedlong ptr, s_printer plld, unsignedlong len);RtlMoveMemoryP( ptr, plld, len)
end subroutine

public subroutine uf_rtlmovememoryu (ref s_printer s_ptr, longlong ptr, unsignedlong len);RtlMoveMemoryU( s_ptr, long(ptr), len)
end subroutine

public function longlong uf_setcurrentdirectory (string pathname);return SetCurrentDirectory(PathName)
end function

public function longlong uf_getcurrentdirectory (longlong bufflen, ref string pathname);return getcurrentdirectory(long(bufflen), PathName)
end function

public subroutine uf_initcommoncontrols ();InitCommonControls()
end subroutine

public function longlong uf_createwindowexa (unsignedlong dwexstyle, string classname, longlong windowname, unsignedlong dwstyle, unsignedlong x, unsignedlong y, unsignedlong nwidth, unsignedlong nheight, unsignedlong hwndparent, unsignedlong hmenu, unsignedlong hinstance, unsignedlong lpparam);return CreateWindowExA( dwExStyle,  ClassName, long(windowname),  dwStyle,  X,  Y,  nWidth,  nHeight,  hWndParent,  hMenu,  hInstance,  lpParam)
end function

public function integer uf_tooltipmsg (longlong hwnd, longlong umsg, longlong wparam, ref s_toolinfo toolinfo);
return tooltipmsg(long(hwnd), long(umsg), long(wparam), ToolInfo)
end function

public function integer uf_relaymsg (longlong hwnd, longlong umsg, longlong wparam, ref s_msg msg);
return relaymsg(long(hwnd), long(umsg), long(wparam),   Msg)
end function

public function longlong uf_lstrcpy (longlong destination, string source);return lstrcpy(long(destination),  Source)
end function

public function unsignedlong uf_sendmessagestring (unsignedlong hwnd, unsignedlong msg, unsignedlong wparam, ref string lpzstring);
return SendMessageString(  hwnd,  Msg,  wParam,  lpzString )
end function

public function unsignedlong uf_shgetpathfromidlista (unsignedlong pidl, ref string pszpath);
return SHGetPathFromIDListA(  pidl,  pszPath)
end function

public subroutine uf_cotaskmemfree (unsignedlong idlist);CoTaskMemFree(idlist)
end subroutine

public function unsignedlong uf_shbrowseforfoldera (s_browseinfo lpbrowseinfo);return SHBrowseForFolderA( lpbrowseinfo )

end function

public function longlong uf_getstdhandle (longlong word);return getstdhandle(long(word))

end function

public function longlong uf_writefile (longlong hfile, string buf, longlong len, ref longlong lenw, longlong ptr);
long long_lenw
longlong ret_val
long_lenw = long(lenw)
ret_val = writefile(long(hfile),  buf, long(len), long_lenw, long(ptr))
lenw = longlong(long_lenw)
return ret_val
end function

public function longlong uf_createfilea (string filename, longlong mode, longlong share, ref longlong ptr, longlong attr, longlong dw, longlong dw2);
long long_ptr
longlong ret_val
long_ptr = long(ptr)
ret_val = CreateFileA( filename, long(mode), long(share), long_ptr, long(attr), long(dw), long(dw2))
ptr = longlong(long_ptr)
return ret_val
end function

public subroutine uf_exitprocess (longlong uexitcode);exitprocess(long(uexitcode))

end subroutine

public function longlong uf_getenvironmentvariablea (string lpname, ref string lpbuffer, longlong nsize);return GetEnvironmentVariableA(  lpName,   lpBuffer, nSize)

end function

public function longlong uf_getcurrentprocessid ();return GetCurrentProcessId()

end function

public function longlong uf_setoraclecallback (longlong handle, longlong a_hwnd, longlong a_evt);return setoraclecallback(long(handle), long(a_hwnd), long(a_evt))

end function

public function longlong uf_getenvironmentvariable (string var, ref string str, longlong len);return GetEnvironmentVariable( var, str, long(len))

end function

public function longlong uf_setenvironmentvariable (string var, string val);return SetEnvironmentVariable( var, val)

end function

public function longlong uf_getmodulehandle (longlong modname);return getmodulehandle(long(modname))

end function

public function longlong uf_formatmessage (longlong flag, longlong input, longlong err, longlong lang, ref string msg, longlong len, longlong arg);return formatmessage(long(flag), long(input), long(err), long(lang),  msg, long(len), long(arg))

end function

public function longlong uf_getprocaddress (longlong hmod, string procname);return getprocaddress(long(hmod), procname)

end function

public function unsignedlong uf_getdc (unsignedlong hwnd);return GetDC(  hWnd) 

end function

public function unsignedlong uf_selectobject (unsignedlong hdc, unsignedlong hwnd);return SelectObject (  hdc,  hWnd )

end function

public function boolean uf_gettextextentpoint32 (unsignedlong hdcr, string lpstring, longlong ncount, ref s_size size);return GetTextExtentPoint32 ( hdcr,  lpString, long(ncount),   size)

end function

public function longlong uf_releasedc (unsignedlong hwnd, unsignedlong hdcr);return ReleaseDC ( hWnd,  hdcr )

end function

public function longlong uf_ftpgetfilea (longlong hftpsession, string lpszremotefile, string lpsznewfile, longlong ffailifexists, longlong dwlocalflagsandattributes, longlong dwinternetflags, longlong dwcontext);return ftpgetfilea(long(hftpsession),  lpszRemoteFile, lpszNewFile, long(ffailifexists), long(dwlocalflagsandattributes), long(dwinternetflags), long(dwcontext)) 

end function

public function longlong uf_internetconnecta (longlong hinternetsession, string servername, longlong port, string user, string password, longlong service, longlong flags, longlong context);return internetconnecta(long(hinternetsession), ServerName, long(port), user,  password, long(service), long(flags), long(context))

end function

public function longlong uf_ftpcommanda (longlong hftpsession, longlong res, longlong flags, string cmd, longlong context, ref longlong ptr);
long long_ptr
longlong ret_val
long_ptr = long(ptr)
ret_val = ftpcommanda(long(hftpsession), long(res), long(flags), cmd, long(context), long_ptr) 
ptr = longlong(long_ptr)
return ret_val
end function

public function longlong uf_internetgetlastresponseinfoa (ref longlong lpdwerror, ref string lpszbuffer, ref longlong len);
long long_lpdwerror, long_len
longlong ret_val
long_lpdwerror = long(lpdwerror)
long_len = long(len)
ret_val = internetgetlastresponseinfoa(long_lpdwerror,  lpszBuffer, long_len)
lpdwerror = longlong(lpdwerror)
len = longlong(len)
return ret_val
end function

public function longlong uf_internetopena (string lpszagent, longlong accesstype, longlong proxynamex, longlong proxybypass, longlong dwflags);return InternetOpenA( lpszAgent, long(accesstype), long(proxynamex), long(proxybypass), long(dwflags)) 

end function

public function longlong uf_internetclosehandle (longlong hinet);return internetclosehandle(long(hinet)) 

end function

public function longlong uf_ftpputfilea (longlong hftpsession, string filename, string remotefile, longlong flags, longlong context);return ftpputfilea(long(hftpsession), filename, remotefile, long(flags), long(context)) 

end function

public function longlong uf_ftpsetcurrentdirectorya (longlong hftpsession, string directory);return ftpsetcurrentdirectorya(long(hftpsession), Directory)

end function

public function longlong uf_internetwritefile (longlong hfile, string buffer, longlong len, ref longlong retlen);
long long_retlen
longlong ret_val
long_retlen = long(retlen)
ret_val = internetwritefile(long(hfile), buffer, long(len), long_retlen)  
retlen = longlong(long_retlen)
return ret_val 
end function

public function longlong uf_ftpopenfilea (longlong hconnect, string filename, longlong ascess, longlong filetype, longlong context);return ftpopenfilea(long(hconnect), filename, long(ascess), long(filetype), long(context))

end function

public function longlong uf_ftpgetcurrentdirectorya (longlong hftpsession, ref string directory, ref longlong numchars);
long long_numchars
longlong ret_val
long_numchars = long(numchars)
ret_val = ftpgetcurrentdirectorya(long(hftpsession),  Directory, long_numchars)
numchars = longlong(long_numchars)
return ret_val
end function

public function longlong uf_ftprenamefilea (longlong hconnect, string existing, string new);return ftprenamefilea(long(hconnect),  existing,  new)

end function

public function longlong uf_ftpdeletefilea (longlong hconnect, string filename);return ftpdeletefilea(long(hconnect),  filename)

end function

public function longlong uf_ftpfindfirstfilea (longlong hconnect, string lpszsearchfile, ref s_finddata lpfindfiledata, longlong dwflags, longlong dwcontext);return ftpfindfirstfilea(long(hconnect),  lpszSearchFile,   lpFindFileData, long(dwflags), long(dwcontext))

end function

public function longlong uf_internetquerydataavailable (longlong hconnect, ref longlong lpdwnumberofbytesavailable, longlong dwflags, longlong dwcontext);
long long_lpdwnumberofbytesavailable
longlong ret_val
long_lpdwnumberofbytesavailable = long(lpdwnumberofbytesavailable)
ret_val = internetquerydataavailable(long(hconnect), long_lpdwnumberofbytesavailable, long(dwflags), long(dwcontext))
lpdwnumberofbytesavailable = longlong(long_lpdwnumberofbytesavailable)
return ret_val
end function

public function longlong uf_internetreadfile (longlong hfile, ref string buffer, longlong len, ref longlong retlen);
long long_retlen
longlong ret_val
long_retlen = long(retlen)
ret_val = internetreadfile(long(hfile),  buffer, long(len), long_retlen)
retlen = longlong(long_retlen)
return ret_val
end function

public function longlong uf_internetfindnextfilea (longlong hinternetsession, ref s_finddata lpvfiledata);return internetfindnextfilea(long(hinternetsession),   lpvFileData)

end function

public function longlong uf_sleep (longlong milliseconds);return sleep(long(milliseconds))

end function

public function longlong uf_shellexecutea (longlong hwnd, string verb, string filename, string parms, string dir, longlong x3);return shellexecutea(long(hwnd), verb, filename, parms,  dir, long(x3))

end function

public function longlong uf_filetimetosystemtime (ref s_filetime lpfiletime, ref s_systemtime lpsystemtime);return FileTimeToSystemTime( lpFileTime, lpSystemTime) 

end function

public function boolean uf_htmlhelp (unsignedlong hwnd, string pszfile, unsignedinteger ucommand, unsignedlong dwdata);return HtmlHelp(  hwnd,  pszFile,  uCommand,  dwData )

end function

public function boolean uf_htmlhelp (unsignedlong hwnd, string pszfile, unsignedinteger ucommand, string dwdata);return HtmlHelp(  hwnd,  pszFile,  uCommand,  dwData )

end function

public function longlong uf_setthreadlocale (longlong locale);return setthreadlocale(long(locale))

end function

public function longlong uf_loadkeyboardlayout (ref string klid, unsignedlong flags);return LoadKeyboardLayout(   KLID,  Flags)

end function

public function longlong uf_getlocaleinfoa (unsignedlong locale, unsignedlong lctype, ref string lplcdata, longlong cchdata);return GetLocaleInfoA( Locale,  LCType,  lpLCData, long(cchdata)) 

end function

public function integer uf_bitblt (unsignedlong hdc, integer num, integer num, integer num, integer num, unsignedlong hdc, integer num, integer num, unsignedlong lparam);return BitBlt( hDC,  num,  num,  num,  num,  hDC,  num,  num,  lParam) 

end function

public function unsignedlong uf_createcompatibledc (unsignedlong hdc);return CreateCompatibleDC( hDC)

end function

public function unsignedlong uf_loadimagea (unsignedlong hints, ref string lpszname, unsignedinteger utype, integer cxdesired, integer cydesired, unsignedinteger fuload);return LoadImageA(  hints,   lpszName,   uType,  cxDesired, cyDesired, fuLoad ) 

end function

public function unsignedlong uf_getobjectbitmap (unsignedlong hgdiobj, integer cbbuffer, ref s_bitmap bm);return GetObjectBitmap(   hgdiobj,   cbBuffer,   bm )

end function

public function boolean uf_deleteobject (unsignedlong hgdiobj);return DeleteObject (  hgdiobj )

end function

public function longlong uf_stretchblt (unsignedlong hdcdest, integer x1, integer y1, integer w1, integer h1, unsignedlong hdcsrc, integer x2, integer y2, integer w2, integer h2, unsignedlong lparam);return StretchBlt( hDCdest,  x1,  y1,  w1,  h1,  hDCsrc,  x2,  y2,  w2,  h2,  lParam) 

end function

public function longlong uf_findwindow (string name, longlong title);return FindWindow( name, long(title))

end function

public function longlong uf_findwindows (string name, string title);return FindWindowS( name,  title) 

end function

public function longlong uf_findwindowex (longlong hwndparent, longlong hwndchildafter, string lpszclass, longlong lpszwindow);return findwindowex(long(hwndparent), long(hwndchildafter), lpszClass, long(lpszwindow)) 

end function

public function boolean uf_setmenuitembitmaps (unsignedlong hmenu, unsignedinteger upos, unsignedinteger flags, unsignedlong handle_bm1, unsignedlong handle_bm2);return SetMenuItemBitmaps( hmenu, upos, flags, handle_bm1, handle_bm2)

end function

public function unsignedlong uf_getmenuitemid (unsignedlong hmenu, unsignedinteger uitem);return GetMenuItemID( hMenu, uItem)

end function

public function integer uf_getsubmenu (unsignedlong hmenu, integer pos);return GetSubMenu( hMenu, pos) 

end function

public function unsignedlong uf_getmenu (unsignedlong hwindow);return GetMenu( hWindow)

end function

public function boolean uf_modifymenu (unsignedlong hmnu, unsignedlong uposition, unsignedlong uflags, unsignedlong uidnewitem, longlong lpnewi);return ModifyMenu(  hMnu,  uPosition,  uFlags,  uIDNewItem, long(lpnewi)) 

end function

public function boolean uf_setmenudefault (unsignedlong hmenu, unsignedinteger upos, unsignedinteger flags);return SetMenuDefault( hmenu, upos, flags)  

end function

public function unsignedlong uf_setmenuinfo (unsignedlong hmenu, s_menuinfo lpcmi);return SetMenuInfo( hmenu, lpcmi) 

end function

public function unsignedlong uf_createsolidbrush (longlong crcolor);return createsolidbrush(long(crcolor))

end function

public function unsignedlong uf_setbkcolor (longlong hdc, longlong crcolor);return setbkcolor(long(hdc), long(crcolor))

end function

public function longlong uf_fn_toolbarsetfont (longlong unknown, string fontname, longlong fontsize, longlong unknown1, string str);return fn_toolbarsetfont(long(unknown), fontname, long(fontsize), long(unknown1), str)

end function

public function longlong uf_fn_toolbarshowtext (longlong unknown, longlong state);return fn_toolbarshowtext(long(unknown), long(state))

end function

public function longlong uf_fn_toolitemsetcolor (longlong unknown, longlong color, longlong unknown, longlong unknown2);return fn_toolitemsetcolor(long(unknown), long(color), long(unknown), long(unknown2))

end function

public function longlong uf_fn_toolitemhwndfirst (longlong unknown, longlong unknown2, ref longlong unknown3);
long long_unknown3
longlong ret_val
long_unknown3 = long(unknown3)
ret_val = fn_toolitemhwndfirst(long(unknown), long(unknown2), long_unknown3)
unknown3 = longlong(long_unknown3)
return ret_val
end function

public function longlong uf_fn_toolgroupfirst (longlong unknown);return fn_toolgroupfirst(long(unknown))

end function

public function longlong uf_sendmessagess (longlong hwnd, longlong msg, longlong wparam, ref s_colorschema lparam);return sendmessagess(long(hwnd), long(msg), long(wparam),  lParam)

end function

public function longlong uf_sendmessage (longlong hwnd, longlong msg, longlong wparam, longlong lparam);return sendmessage(long(hwnd), long(msg), long(wparam), long(lparam))

end function

public function longlong uf_getclientrect (longlong hwnd, ref s_rect lprect);return getclientrect(long(hwnd),  lpRect) 

end function

public function longlong uf_fn_toolbarsize (longlong unknown, ref s_rect lprect);return fn_toolbarsize(long(unknown),  lpRect) 

end function

public function longlong uf_fillrect (longlong hdc, ref s_rect rect, longlong brush);return fillrect(long(hdc),  rect , long(brush))

end function

public function longlong uf_getwindowlong (unsignedlong hwnd, integer nindex);return GetWindowLong ( hWnd,  nIndex) 

end function

public function longlong uf_setwindowlong (unsignedlong hwnd, integer nindex, longlong dwnewlong);return SetWindowLong ( hWnd,  nIndex, long(dwnewlong))

end function

public function longlong uf_setlayeredwindowattributes (longlong hwnd, longlong crkey, character balpha, longlong dwflags);return setlayeredwindowattributes(long(hwnd), long(crkey),  bAlpha , long(dwflags))  

end function

public function longlong uf_fn_toolgroupreset (longlong unknown, longlong unknown);return fn_toolgroupreset(long(unknown), long(unknown)) 

end function

public function longlong uf_fn_toolgroupremove (longlong unknown, longlong unknown);return fn_toolgroupremove(long(unknown), long(unknown))

end function

public function longlong uf_psinterface (string dir, string infile, string outfile, longlong typex, longlong orientation, ref s_search_info search_info);return PSInterface( dir, infile, outfile, long(typex), long(orientation),  search_info) 

end function

public function longlong uf_createdca (string lpszdriver, string lpszdevice, string lpszoutput, longlong data);return CreateDCA(  lpszDriver,  lpszDevice,  lpszOutput, long(data))

end function

public function longlong uf_deletedc (longlong hdc);return deletedc(long(hdc))

end function

public function longlong uf_getdevicecaps (longlong hdc, longlong x);return getdevicecaps(long(hdc), long(x)) 

end function

public function longlong uf_getprinterorientation ();return GetPrinterOrientation()

end function

public function longlong uf_getprintdef (ref longlong orig, ref longlong postscript);
long long_orig, long_postscript
longlong ret_val
long_orig = long(orig)
long_postscript = long(postscript)
ret_val = getprintdef(long_orig, long_postscript) 
orig = longlong(long_orig)
postscript = longlong(long_postscript)
return ret_val
end function

public function longlong uf_getpostscriptprinter (ref string name, ref string drv, ref string port);return GetPostscriptPrinter(  name, drv, port)

end function

public function longlong uf_getprinterinfo (string printer, ref string name, ref string drv, ref string port);return GetPrinterInfo(  printer,  name,  drv,  port)

end function

public function longlong uf_findaddpostprinter (ref string name, ref string drv, ref string port);return FindAddPostPrinter( name, drv, port) 

end function

public function longlong uf_addprinterdrivera (longlong name, longlong level, s_driver_info2 driverinfo);return addprinterdrivera(long(name), long(level),  DriverInfo )

end function

public function longlong uf_deleteprinterdrivera (longlong name, longlong config, string driver);return deleteprinterdrivera(long(name), long(config),  driver)

end function

public function longlong uf_addprintera (longlong name, longlong level, s_printer_info2_add driverinfo);return addprintera(long(name), long(level),  DriverInfo )

end function

public function longlong uf_copyfilea (string path, string desination, longlong stat);return CopyFileA(  path,  desination, long(stat)) 

end function

public function longlong uf_getprintdefpcl (ref longlong orig, ref longlong postscript);
long long_orig, long_postscript
longlong ret_val
long_orig = long(orig)
long_postscript = long(postscript)
ret_val = getprintdefpcl(long_orig, long_postscript) 
orig = longlong(long_orig)
postscript = longlong(long_postscript)
return ret_val
end function

public function longlong uf_getpclprinter (ref string name, ref string drv, ref string port);return GetPCLPrinter(  name,  drv,  port)

end function

public function longlong uf_pclinterface (string dir, string infile, string outfile, longlong typex, longlong orientation, ref s_search_info search_info);return PCLInterface( dir, infile, outfile, long(typex), long(orientation),  search_info) 

end function

public function longlong uf_getprinterorientationpcl ();return GetPrinterOrientationPCL()

end function

public function longlong uf_setdefprinter (string pprintername);return SetDefPrinter( pPrinterName)  

end function

public function longlong uf_getdefprinter (ref string pprintername, ref longlong pdwbuffersize);
long long_pdwbuffersize
longlong ret_val
long_pdwbuffersize = long(pdwbuffersize)
ret_val = GetDefPrinter(  pPrinterName, long_pdwbuffersize) 
pdwbuffersize = longlong(long_pdwbuffersize)
return ret_val
end function

public function longlong uf_getprinter (longlong hwnd, longlong ptype, s_printer_info2 printer_info2, longlong num, ref longlong rnum);
long long_rnum
longlong ret_val
long_rnum = long(rnum)
ret_val = getprinter(long(hwnd), long(ptype),  printer_info2 , long(num), long_rnum) 
rnum = longlong(long_rnum)
return ret_val
end function

public function longlong uf_add_printer (longlong apost, string ptr, string drv);return add_printer(long(apost),  ptr , drv) 

end function

public function longlong uf_getshortpatha (string longpath, ref string shortpath, longlong buflen);return GetShortPathA( longpath ,  shortpath, long(buflen))

end function

public function longlong uf_getlongpathnamea (string shortpath, ref string longpath, longlong buflen);return GetLongPathNameA( shortpath ,  longpath, long(buflen))

end function

public function longlong uf_createwindowexa (longlong dwstyleex, string class, string name, longlong dwstyle, longlong x, longlong y, longlong width, longlong height, longlong hwnd, longlong menu, longlong inst, longlong extra);return createwindowexa(long(dwstyleex),   class ,   name , long(dwstyle), long(x), long(y), long(width), long(height), long(hwnd), long(menu), long(inst), long(extra))

end function

public function longlong uf_getwindowlonga (longlong hwnd, longlong nindex);return getwindowlonga(long(hwnd), long(nindex)) 

end function

public function longlong uf_sendmessages (longlong hwnd, longlong msg, longlong wparam, ref string lparam);return sendmessages(long(hwnd), long(msg), long(wparam),   lParam)

end function

public function longlong uf_sendmessages2 (longlong hwnd, longlong msg, string wparam, ref string lparam);return sendmessages2(long(hwnd), long(msg), wParam ,   lParam)

end function

public function longlong uf_sendmessagei (longlong hwnd, longlong msg, longlong wparam, longlong lparam);return sendmessagei(long(hwnd), long(msg), long(wparam), long(lparam))

end function

public function longlong uf_postmessagei (longlong hwnd, longlong msg, longlong wparam, longlong lparam);return postmessagei(long(hwnd), long(msg), long(wparam), long(lparam))

end function

public function longlong uf_sendmessageir (longlong hwnd, longlong msg, longlong wparam, ref longlong lparam);
long long_lparam
longlong ret_val
long_lparam = long(lparam)
ret_val = sendmessageir(long(hwnd), long(msg), long(wparam), long_lparam)
lparam = longlong(long_lparam)
return ret_val
end function

public function string uf_sendmessageis (longlong hwnd, longlong msg, longlong wparam, longlong lparam);return sendmessageis(long(hwnd), long(msg), long(wparam), long(lparam))

end function

public function longlong uf_getfontlist (ref string fonts[], longlong count);return GetFontList( fonts , long(count)) 

end function

public function longlong uf_getmenu (longlong hinst);return getmenu(long(hinst))

end function

public function longlong uf_getmodulehandlea (longlong modname);return getmodulehandlea(long(modname)) 

end function

public subroutine uf_rtlmovememory (ref s_notifyheader nmhr, unsignedlong ptr, unsignedlong len);RtlMoveMemory(  nmhr,  ptr, len) 

end subroutine

public function longlong uf_movewindow (longlong hwnd, longlong x, longlong y, longlong nwidth, longlong nheight, longlong brepaint);return movewindow(long(hwnd), long(x), long(y), long(nwidth), long(nheight), long(brepaint)) 

end function

public function longlong uf_wnetgetuser (longlong ptr, ref string user, ref longlong len);
long long_len
longlong ret_val
long_len = long(len)
ret_val = wnetgetuser(long(ptr), user, long_len)
len = longlong(long_len)
return ret_val
end function

public function longlong uf_lstrcmpw (string str1, string str2);return lstrcmpW( str1, str2) 

end function

public function longlong uf_hashdata (string pbdata, longlong cbdata, ref unsignedlong pbhash, longlong cbhash);return HashData(  pbData, long(cbdata),  pbHash, long(cbhash)) 

end function

public function longlong uf_smtpmail (string fromuser, string subject, string text, string touser, string server);return smtpmail( fromuser, subject, text, touser, server) 

end function

public function longlong uf_smtpmailex (string fromuser, string subject, string text, string touser, string server, string filename, string err);return smtpmailex( fromuser, subject, text, touser, server, filename, err)

end function

public function longlong uf_smtpmailpasswd (string fromuser, string user, string passwd, string subject, string text, string touser, string server);return smtpmailpasswd( fromuser, user, passwd, subject, text, touser, server)

end function

public function longlong uf_smtpmailpasswdex (string fromuser, string user, string passwd, string subject, string text, string touser, string server, string filename, string err);return smtpmailpasswdex( fromuser, user, passwd, subject, text, touser, server, filename, err) 

end function

public function longlong uf_sendmessagea (longlong hwnd, longlong msg, longlong wparam, longlong lparam);return sendmessagea(long(hwnd), long(msg), long(wparam), long(lparam)) 

end function

public function longlong uf_createdca (string display, longlong x, longlong y, longlong z);return CreateDCA(  display, long(x), long(y), long(z))

end function

public function longlong uf_getdc (longlong hwnd);return getdc(long(hwnd))

end function

public function longlong uf_getwindowdc (longlong hwnd);return getwindowdc(long(hwnd)) 

end function

public function longlong uf_releasedc (longlong hdc, longlong hwnd);return releasedc(long(hdc), long(hwnd)) 

end function

public function longlong uf_settextcolor (longlong hdc, longlong col);return settextcolor(long(hdc), long(col)) 

end function

public function longlong uf_textouta (longlong hdc, longlong x, longlong y, string text, longlong len);return textouta(long(hdc), long(x), long(y),  text, long(len))

end function

public function longlong uf_setbkmode (longlong hdc, longlong mode);return setbkmode(long(hdc), long(mode))

end function

public function longlong uf_setclasslonga (longlong hwnd, longlong index, longlong val);return setclasslonga(long(hwnd), long(index), long(val))

end function

public function longlong uf_selectobject (longlong hdc, longlong brush);return selectobject(long(hdc), long(brush)) 

end function

public function longlong uf_setwindowpos (longlong hwnd, longlong hwnd2, longlong x, longlong y, longlong cx, longlong cy, longlong flag);return setwindowpos(long(hwnd), long(hwnd2), long(x), long(y), long(cx), long(cy), long(flag))

end function

public function longlong uf_showwindow (longlong hwnd, longlong hwnd2);return showwindow(long(hwnd), long(hwnd2)) 

end function

public function longlong uf_redrawwindow (longlong hwnd, longlong crect, longlong rect, longlong flags);return redrawwindow(long(hwnd), long(crect), long(rect), long(flags))

end function

public function integer uf_getfreeresources95 (integer whichresource);return GetFreeResources95 ( WhichResource) 

end function

public function longlong uf_freelibrary (longlong hmodule);return freelibrary(long(hmodule)) 

end function

public function integer uf_getkeystate (longlong code);return getkeystate(long(code)) 

end function

public function longlong uf_comparestring (longlong locale, longlong dwcmpflags, string object_data, longlong len, string object_data_db, longlong object_len_db);return comparestring(long(locale), long(dwcmpflags), object_data, long(len), object_data_db, long(object_len_db))

end function

public function longlong uf_shellexecute (longlong hwnd, string verb, string filename, longlong x1, longlong x2, longlong x3);return shellexecute(long(hwnd), verb, filename, long(x1), long(x2), long(x3)) 

end function

public function longlong uf_regcreatekeyex (longlong hkey, string path, longlong i, longlong j, longlong m, longlong key, longlong null2, ref longlong regkey, ref longlong disposition);
long long_regkey, long_disposition
longlong ret_val
long_regkey = long(regkey)
long_disposition = long(disposition)
ret_val = regcreatekeyex(long(hkey),  path, long(i), long(j), long(m), long(key), long(null2), long_regkey, long_disposition) 
regkey = longlong(long_regkey)
disposition = longlong(long_disposition)
return ret_val
end function

public function longlong uf_regsetvalueex (longlong key, string name, longlong i, longlong datatype, blob val, longlong len);return regsetvalueex(long(key),  name , long(i), long(datatype),  val, long(len))

end function

public function longlong uf_createwindowex (longlong dwstyleex, string class, string name, longlong dwstyle, longlong x, longlong y, longlong width, longlong height, longlong hwnd, longlong menu, longlong inst, longlong extra);return createwindowex(long(dwstyleex),   class ,   name , long(dwstyle), long(x), long(y), long(width), long(height), long(hwnd), long(menu), long(inst), long(extra))

end function

public function longlong uf_initialize (longlong hwnd1, longlong hwnd);return initialize(long(hwnd1), long(hwnd))

end function

public function longlong uf_start (longlong hwnd);return start(long(hwnd))

end function

public function longlong uf_loadpage (longlong hwnd, string page);return loadpage(long(hwnd), page) 

end function

public function longlong uf_loadpageframe (longlong hwnd, string page, string frame);return loadpageframe(long(hwnd), page, frame)

end function

public function longlong uf_updatepage (longlong hwnd);return updatepage(long(hwnd))

end function

public function longlong uf_changedir (string dir);return ChangeDir( dir) 

end function

public function longlong uf_changedirw (string dir);return ChangeDirW( dir)

end function

public function longlong uf_getdir (string dir);return GetDir( dir)

end function

public function longlong uf_register (longlong hwnd);return register(long(hwnd))

end function

public function longlong uf_loadhtml (longlong hwnd, string html);return loadhtml(long(hwnd),  html)

end function

public function longlong uf_loadhtmlframe (longlong hwnd, string html, string frame);return loadhtmlframe(long(hwnd),  html,frame) 

end function

public function longlong uf_mouseevent (longlong hwnd, longlong x1, longlong y1);return mouseevent(long(hwnd), long(x1), long(y1))

end function

public function longlong uf_loadfile (longlong hwnd, string file);return loadfile(long(hwnd),  file)

end function

public function longlong uf_mousepressed (longlong hwnd, longlong button, longlong state, longlong x1, longlong y1);return mousepressed(long(hwnd), long(button), long(state), long(x1), long(y1)) 

end function

public function longlong uf_keypressed (longlong hwnd, longlong key1, longlong x1, longlong y1);return keypressed(long(hwnd), long(key1), long(x1), long(y1))

end function

public function longlong uf_redraw (longlong hwnd, longlong updatescreen);return redraw(long(hwnd), long(updatescreen))

end function

public function longlong uf_execjavascriptresults (longlong hwnd, string script, ref string results);return execjavascriptresults(long(hwnd),  script, results)

end function

public function longlong uf_execjavascript (longlong hwnd, string script);return execjavascript(long(hwnd),  script)

end function

public function longlong uf_execjavascriptframeresults (longlong hwnd, string script, ref string results, string frame);return execjavascriptframeresults(long(hwnd),  script, results,  frame)

end function

public function longlong uf_execjavascriptframe (longlong hwnd, string script, string frame);return execjavascriptframe(long(hwnd),  script, frame)

end function

public function longlong uf_createobject (longlong hwnd, string object_name);return createobject(long(hwnd),  object_name )

end function

public function longlong uf_setobjectproperty (longlong hwnd, string object_name, string prop_name, string val);return setobjectproperty(long(hwnd),  object_name,  prop_name,   val)

end function

public function longlong uf_setobjectcallback (longlong hwnd, string object_name, string callback1);return setobjectcallback(long(hwnd),  object_name,   callback1)

end function

public function longlong uf_setdbhandle (longlong dbhandle);return setdbhandle(long(dbhandle))

end function

public function longlong uf_unload ();return Unload()

end function

public function longlong uf_zoom (longlong hwnd, longlong pct);return zoom(long(hwnd), long(pct))

end function

public function longlong uf_webcut (longlong hwnd);return webcut(long(hwnd))

end function

public function longlong uf_webcopy (longlong hwnd);return webcopy(long(hwnd))

end function

public function longlong uf_webpaste (longlong hwnd);return webpaste(long(hwnd))

end function

public function longlong uf_webselectall (longlong hwnd);return webselectall(long(hwnd))

end function

public function longlong uf_print (longlong hwnd);return print(long(hwnd)) 

end function

public function longlong uf_saveas (longlong hwnd, string file_name, string a_file_type);return saveas(long(hwnd),  file_name,   a_file_type)

end function

public function longlong uf_loadlibrarya (string a_lib);return LoadLibraryA(a_lib)

end function

public function longlong uf_getmodulehandlea (string modname);return GetModuleHandleA(modname)

end function

public subroutine uf_globalmemorystatus (ref str_memorystatus memorystatus);GlobalMemoryStatus ( memorystatus ) 
end subroutine

public function boolean uf_deletefile (string lpfilename);return DeleteFile( lpFileName) 
end function

public function longlong uf_listprinters (string name, ref string printers[], longlong pos, longlong max_printers);return ListPrinters( name,  printers[], long(pos), long(max_printers)) 
end function

public function longlong uf_getfileversion (string modname, ref string version);return GetFileVersion( modname,  version)
end function

public function longlong uf_wnetgetuniversalname (string lplocalpath, longlong dwinfolevel, ref s_universal_name_info lpbuffer, ref longlong lpbuffersize);
long long_lpbuffersize
longlong ret_val
long_lpbuffersize = long(lpbuffersize)
ret_val = WNetGetUniversalName( lpLocalPath, long(dwinfolevel),  lpBuffer, long_lpbuffersize)
lpbuffersize = longlong(long_lpbuffersize)
return ret_val
end function

public function longlong uf_getpbdirectorylist (longlong hpboorca, string pblpath, ref string object_names[], ref string mod_dates[], ref string create_dates[], ref longlong object_types[], longlong len, longlong flag);
long long_object_types[], i
longlong ret_val
for i = 1 to upperbound(object_types)
	long_object_types[i] = long(object_types[i])
next
ret_val = getpbdirectorylist(long(hpboorca), pblpath,  object_names[],  mod_dates[],  create_dates[],  long_object_types[], long(len), long(flag))
for i = 1 to upperbound(long_object_types)
	object_types[i] = longlong(long_object_types[i])
next
return ret_val
end function

public subroutine uf_pborca_sessionclose (longlong horcasession);pborca_sessionclose(long(horcasession))
end subroutine

public function longlong uf_pborca_sessionopen ();return PBORCA_SessionOpen( )
end function

public function longlong uf_loadpb ();return LoadPB()
end function

public function longlong uf_getpbobjectdatainfo (longlong hpboorca, string pblpath, ref string object_names, ref string mod_date, longlong object_type);return getpbobjectdatainfo(long(hpboorca), pblpath,  object_names,  mod_date, long(object_type))
end function

public function longlong uf_ocipasswordchange (ref longlong svchp, ref longlong errcall, ref string userid, unsignedlong userid_len, ref string passwd, unsignedlong passwd_len, ref string passwd_new, longlong passwd_new_len, longlong mode);
long long_svchp, long_errcall
longlong ret_val
long_svchp = long(svchp)
long_errcall = long(errcall)
ret_val = ocipasswordchange(long_svchp, long_errcall,  userid,  userid_len,  passwd, passwd_len,  passwd_new, long(passwd_new_len), long(mode))
svchp = longlong(long_svchp)
errcall = longlong(long_errcall)
return ret_val
end function

public function longlong uf_adsopenobject (string lpszpathname, string lpszusername, string lpszpassword, unsignedlong dwreserved, s_refiid riid, ref any ole);return ADsOpenObject( lpszPathName,  lpszUserName,  lpszPassword,  dwReserved,  riid,   ole )
end function

public function longlong uf_adsi_getroot (string adsi_path, string adsi_user, string adsi_passwd, ref longlong code);
long long_code
longlong ret_val
long_code = long(code)
ret_val = ADSI_GetRoot( adsi_path, adsi_user, adsi_passwd, long_code)
code = longlong(long_code)
return ret_val
end function

public function longlong uf_adsi_getname (longlong padsi, ref string name);return adsi_getname(long(padsi),  name)
end function

public function longlong uf_adsi_getclass (longlong padsi, ref string name);return adsi_getclass(long(padsi),  name) 
end function

public function longlong uf_adsi_getschema (longlong padsi, ref string name);return adsi_getschema(long(padsi),  name)
end function

public function longlong uf_adsi_getadsipath (longlong padsi, ref string name);return adsi_getadsipath(long(padsi),  name)
end function

public function longlong uf_adsi_getproperties (longlong padsi, ref string names[]);return adsi_getproperties(long(padsi),  names[])
end function

public function longlong uf_adsi_getchildren (longlong padsi, ref longlong listpadsi[]);
long long_listpadsi[], i
longlong ret_val
for i = 1 to upperbound(listpadsi)
	long_listpadsi[i] = long(listpadsi[i])
next
ret_val = adsi_getchildren(long(padsi), long_listpadsi[])
for i = 1 to upperbound(long_listpadsi)
	listpadsi[i] = longlong(long_listpadsi[i])
next
return ret_val
end function

public function longlong uf_adsi_getattribute (string cszattribute, longlong piads, ref string attr);return ADSI_GetAttribute( cszAttribute, long(piads),   attr)
end function

public function longlong uf_adsi_getallproperties (longlong padsi, ref string names[]);return adsi_getallproperties(long(padsi),  names[])
end function

public function longlong uf_adsi_free_interface (longlong pinterface);return adsi_free_interface(long(pinterface))
end function

public function longlong uf_adsi_search (longlong pdssearch, string adsi_filter, string adsi_attr_names, ref string names[]);return adsi_search(long(pdssearch), adsi_filter, adsi_attr_names, names[])
end function

public function longlong uf_adsi_search_interface (string adsi_path, string adsi_user, string adsi_passwd, ref longlong code);
long long_code
longlong ret_val
long_code = long(code)
ret_val = ADSI_Search_Interface( adsi_path, adsi_user, adsi_passwd, long_code)
code = longlong(long_code)
return ret_val
end function

public function longlong uf_adsi_changepassword (string adsi_path, string adsi_user, string adsi_passwd, string oldpaswd, string newpasswd);return ADSI_ChangePassword( adsi_path, adsi_user, adsi_passwd, oldpaswd, newpasswd)
end function

public function longlong uf_adsi_setpassword (string adsi_path, string adsi_user, string adsi_passwd, string paswd);return ADSI_SetPassword( adsi_path, adsi_user, adsi_passwd, paswd)
end function

public function longlong uf_adsi_getlasterror (ref string buf, ref string sbuf);return ADSI_GetLastError(  buf,  sbuf)
end function

public function longlong uf_netwkstagetinfo (longlong servername, longlong level, ref s_wksta_info_102 bufptr);return netwkstagetinfo(long(servername), long(level),  bufptr)
end function

public function longlong uf_regdeletekey (longlong hkey, string lpsubkey);return regdeletekey(long(hkey), lpSubKey)
end function

public function longlong uf_ppborca_sessionopen ();return PPBORCA_SessionOpen( ) 
end function

public function longlong uf_loadpb (longlong ver);return loadpb(long(ver)) 
end function

public function longlong uf_regcreatekeyexa (longlong hkey, string path, longlong i, longlong j, longlong m, longlong key, longlong null2, ref longlong regkey, ref longlong disposition);
long long_regkey, long_disposition
longlong ret_val
long_regkey = long(regkey)
long_disposition = long(disposition)
ret_val = regcreatekeyexa(long(hkey),  path, long(i), long(j), long(m), long(key), long(null2), long_regkey, long_disposition) 
regkey = longlong(long_regkey)
disposition = longlong(long_disposition)
return ret_val
end function

public function longlong uf_regsetvalueexa (longlong key, string name, longlong i, longlong datatype, blob val, longlong len);return regsetvalueexa(long(key),  name , long(i), long(datatype),  val, long(len))
end function

public function longlong uf_getfiletime (longlong unit, ref s_filedate create_time, ref s_filedate last_access, ref s_filedate last_write);return getfiletime(long(unit),    create_time,    last_access ,   last_write) 
end function

public function longlong uf_filetimetosystemtime (s_filedate file_date, ref s_system_time system_date);return FileTimeToSystemTime( file_date ,   system_date) 
end function

public function longlong uf_lopen (string path, longlong mode);return _lopen(  path , long(mode)) 
end function

public function longlong uf_lclose (longlong unit);return _lclose(long(unit))
end function

public function longlong uf_pbcopyobject (string from_lib, string to_lib, string obj_name, longlong obj_type, longlong hwd);return PBCopyObject( from_lib, to_lib, obj_name, long(obj_type), long(hwd)) 
end function

public function longlong uf_wsprintfa (ref string szquerystr, string formant, longlong langid, longlong usercode, string name);return wsprintfA(   szQueryStr,  formant, long(langid), long(usercode), name) 
end function

public function longlong uf_widechartomultibyte (longlong codepage, longlong dwflags, string lpwidecharstr, longlong cchwidechar, ref string lpmultibytestr, longlong cbmultibyte, longlong defaultchar, ref longlong lpuseddefaultchar);
long long_lpuseddefaultchar
longlong ret_val
long_lpuseddefaultchar = long(lpuseddefaultchar)
ret_val = widechartomultibyte(long(codepage), long(dwflags),  lpWideCharStr, long(cchwidechar),   lpMultiByteStr, long(cbmultibyte), long(defaultchar), long_lpuseddefaultchar) 
lpuseddefaultchar = longlong(long_lpuseddefaultchar)
return ret_val
end function

public function longlong uf_setenvironmentvariablea (string lpname, string lpvalue);return SetEnvironmentVariableA( lpName, lpValue) 
end function

public function longlong uf_comparestringa (longlong locale, longlong dwcmpflags, string object_data, longlong len, string object_data_db, longlong object_len_db);return comparestringa(long(locale), long(dwcmpflags), object_data, long(len), object_data_db, long(object_len_db)) 
end function

public function longlong uf_getuserdefaultlangid ();return GetUserDefaultLangID()
end function

public function longlong uf_getfileversioninfosizea (string filename, ref longlong size);
long long_size
longlong ret_val
long_size = long(size)
ret_val = GetFileVersionInfoSizeA(  filename  , long_size)
size = longlong(long_size)
return ret_val
end function

public function longlong uf_getfileversioninfoa (string filename, longlong dwhandle, longlong dwlen, ref blob lpdata);return GetFileVersionInfoA( filename, long(dwhandle), long(dwlen),    lpData)
end function

public function longlong uf_verqueryvaluea (blob pblock, string lpsubblock, ref string lplpbuffer, ref longlong pulen);s_string s_str
long long_pulen
longlong ret_val
s_str.name = lplpBuffer
long_pulen = long(pulen)
ret_val = VerQueryValueA(  pBlock, lpSubBlock,  s_str, long_pulen)
pulen = longlong(long_pulen)
lplpBuffer = s_str.name
return ret_val
end function

public function longlong uf_getdrivetypea (string path);return GetDriveTypeA( path)
end function

public function unsignedlong uf_cryptacquirecontext (ref unsignedlong phprov, string pszcontainer, string pszprovider, unsignedlong dwprovtype, unsignedlong dwflags);return CryptAcquireContext (phProv, pszContainer, pszProvider, dwProvType, dwFlags)
end function

public function longlong uf_getversionexa (ref s_osversioninfoex os);return GetVersionExA( OS )
end function

public function longlong uf_gethostname (ref string host, longlong len);return gethostname(  host, long(len))
end function

public function s_hostent uf_gethostbyname (string host);return gethostbyname( host )
end function

public function longlong uf_icmpclosehandle (longlong icmphandle);return icmpclosehandle(long(icmphandle)) 
end function

public function longlong uf_icmpsendecho (longlong icmphandle, longlong destinationaddress, string requestdata, integer requestsize, longlong requestoption, ref s_icmp_echo_reply replybuffer, longlong replysize, longlong timeout);return icmpsendecho(long(icmphandle), long(destinationaddress),  requestData,  requestSize , long(requestoption),   replyBuffer, long(replysize), long(timeout))
end function

public function longlong uf_icmpcreatefile ();return IcmpCreateFile() 
end function

public function longlong uf_enumprocesses (ref longlong lpidprocess[], longlong cb, ref longlong cbneeded);
long long_lpidprocess[], long_cbneeded, i
longlong ret_val
for i = 1 to upperbound(lpidprocess)
	long_lpidprocess[i] = long(lpidprocess[i])
next
long_cbneeded = long(cbneeded)
ret_val = EnumProcesses(  long_lpidprocess[], long(cb), long_cbneeded)
for i = 1 to upperbound(long_lpidprocess)
	lpidprocess[i] = longlong(long_lpidprocess[i])
next
cbneeded = longlong(long_cbneeded)
return ret_val
end function

public function longlong uf_enumprocessmodules (longlong hprocess, ref longlong lphmodule[], longlong cb, ref longlong lpcbneeded);
long long_lphmodule[], long_lpcbneeded, i
longlong ret_val
for i = 1 to upperbound(lphmodule)
	long_lphmodule[i] = long(lphmodule[i])
next
long_lpcbneeded = long(lpcbneeded)
ret_val = enumprocessmodules(long(hprocess),  long_lphmodule[], long(cb), long_lpcbneeded)
for i = 1 to upperbound(long_lphmodule)
	lphmodule[i] = longlong(long_lphmodule[i])
next
lpcbneeded = longlong(long_lpcbneeded)
return ret_val
end function

public function longlong uf_getmodulebasename (longlong hprocess, longlong hmodule, ref string lpbasename, longlong nsize);return getmodulebasename(long(hprocess), long(hmodule),  lpBaseName, long(nsize)) 
end function

public function longlong uf_getmodulefilenameex (longlong hprocess, longlong hmodule, ref string lpfilename, longlong nsize);return getmodulefilenameex(long(hprocess), long(hmodule),  lpFilename, long(nsize))
end function

public function longlong uf_openprocess (longlong mode, longlong stat, longlong pid);return openprocess(long(mode), long(stat), long(pid))
end function

public function longlong uf_closehandle (longlong handle);return closehandle(long(handle)) 
end function

public function longlong uf_terminateprocess (longlong handle, longlong code);return terminateprocess(long(handle), long(code))
end function

public function longlong uf_openprocesstoken (longlong processhandle, longlong desiredaccess, ref longlong tokenhandle);
long long_tokenhandle
longlong ret_val
long_tokenhandle = long(tokenhandle)
ret_val = openprocesstoken(long(processhandle), long(desiredaccess), long_tokenhandle) 
tokenhandle = longlong(long_tokenhandle)
return ret_val
end function

public function longlong uf_lookupprivilegevalue (longlong lpsystemname, string lpname, ref s_ws_luid lpluid);return lookupprivilegevalue(long(lpsystemname), lpName,   lpLuid) 
end function

public function longlong uf_adjusttokenprivileges (longlong tokenhandle, longlong disableallprivileges, ref s_ws_token_privileges newstate, longlong bufferlength, longlong previousstate, longlong returnlength);return adjusttokenprivileges(long(tokenhandle), long(disableallprivileges),  NewState, long(bufferlength), long(previousstate), long(returnlength)) 
end function

public function longlong uf_getcurrentprocess ();return GetCurrentProcess() 
end function

public function longlong uf_getfileattributesex (string lpfilename, longlong finfolevelid, ref s_ws_win32_file_attribute_data lpfileinformation);return GetFileAttributesEx(  lpFileName, long(finfolevelid),  lpFileInformation) 
end function

public function longlong uf_getdrivetype (string path);return GetDriveType(  path) 
end function

public function integer uf_sethook (integer state, integer hwnd);return SetHook( state, hwnd)
end function

public function integer uf_getcurrenttask ();return GetCurrentTask()
end function

public function longlong uf_getsystemmenu (longlong hwnd, integer brevert);return getsystemmenu(long(hwnd),   bRevert)
end function

public function longlong uf_insertmenuitem (longlong menu_hwd, longlong pos, integer b, ref s_menu_info menuinfo);return insertmenuitem(long(menu_hwd), long(pos), b,  menuinfo) 
end function

public function longlong uf_insertmenua (longlong menu_hwd, longlong pos, longlong flag, longlong id, string str);return insertmenua(long(menu_hwd), long(pos), long(flag), long(id), str) 
end function

public function longlong uf_modifymenua (longlong hmnu, longlong pos, longlong flag, longlong id, string text);return modifymenua(long(hmnu), long(pos), long(flag), long(id),  text)
end function

public function longlong uf_createpopupmenu ();return CreatePopupMenu()
end function

public function longlong uf_getsubmenu (longlong menu_hwd, longlong pos);return getsubmenu(long(menu_hwd), long(pos)) 
end function

public function longlong uf_getmenustring (longlong hmenu, longlong uiditem, ref string menutext, longlong nmaxcount, longlong flag);return getmenustring(long(hmenu), long(uiditem),  menutext, long(nmaxcount), long(flag))
end function

public function longlong uf_drawmenubar (longlong hwd);return drawmenubar(long(hwd))
end function

public function unsignedlong uf_setcapture (unsignedlong a);return SetCapture( a) 
end function

public subroutine uf_rtlmovememoryfrom (ref s_minmaxinfo nmhr, unsignedlong ptr, unsignedlong len);RtlMoveMemoryFrom(  nmhr,  ptr, len) 
end subroutine

public subroutine uf_rtlmovememoryto (unsignedlong ptr, s_minmaxinfo nmhr, unsignedlong len);RtlMoveMemoryTo( ptr ,  nmhr, len)
end subroutine

public function integer uf_shell_notifyiconw (longlong dwmessage, any lpdata);return shell_notifyiconw(long(dwmessage),  lpData) 
end function

public function longlong uf_loadimage (longlong hinst, string lpsz, longlong un1, longlong n1, longlong n2, longlong un2);return loadimage(long(hinst),  lpsz, long(un1), long(n1), long(n2), long(un2)) 
end function

public function longlong uf_destroyicon (longlong hicon);return destroyicon(long(hicon)) 
end function

public function longlong uf_setforegroundwindow (longlong hwnd);return setforegroundwindow(long(hwnd)) 
end function

public function longlong uf_openicon (longlong hwnd);return openicon(long(hwnd))
end function

public function longlong uf_setsyscolors (longlong nelements, ref longlong elements, ref longlong rgbvalues);
long long_elements, long_rgbvalues
longlong ret_val
long_elements = long(elements)
long_rgbvalues = long(rgbvalues)
ret_val = setsyscolors(long(nelements), long_elements, long_rgbvalues) 
elements = long(long_elements)
rgbvalues = long(long_rgbvalues)
return ret_val
end function

public function longlong uf_getsyscolors (longlong element);return getsyscolors(long(element))
end function

public function string uf_getfileversion (string a_file);blob data
longlong CP_ACP = 0
longlong WC_SEPCHARS = 32
long len
string query_str = "\StringFileInfo\040904b0\"
string query_str2 = "\StringFileInfo\040904e4\"
long size
string szQueryStr
s_string s_str
string str2
longlong status
//longlong id

//id = GetUserDefaultLangID()
len = GetFileVersionInfoSizeA(a_file,size ) 

data = blob(space(len))
GetFileVersionInfoA(a_file, 0,len , data) 

szQueryStr = query_str + "FileVersion"
s_str.name = space(100)
status = VerQueryValueA( data,szQueryStr,s_str,len )
if status = 0 then
	szQueryStr = query_str2 + "FileVersion"
	s_str.name = space(100)
	status = VerQueryValueA( data,szQueryStr,s_str,len )
end if
if status = 0 then
	return ""
end if

return s_str.name

end function

public function datetime uf_getfiledate (string a_file, string a_date_type);// ************************************************************
//		Create / Write Date
// ************************************************************
s_filedate create_date, access_date, write_date, date_to_process
s_system_time system_time
s_time_zone_information time_zone
string str_date, str_time
longlong lunit, DST

// Get a "handle" for the file.
lunit = _lopen(a_file,0)
if lunit = -1 then
	datetime date_to_return
	setnull(date_to_return)
	return date_to_return
end if

// RJO - modifed to return dates in local time instead of UTC
DST = uf_GetTimeZoneInformation(time_zone)

// DST = 1: no daylight savings time
// DST = 2: use daylight savings time
if dst <> 2 then
	time_zone.Bias = time_zone.Bias   // we assume DST means a 60 minute difference
end if

// Write Datetime
GetFileTime(lunit,create_date,access_date,write_date)

choose case upper(trim(a_date_type))
	case 'CREATE','MODIFIED'
		date_to_process = create_date
	case 'ACCESS'
		date_to_process = access_date
	case 'WRITE'
		date_to_process = write_date
end choose		

// Close the "handle"
_lclose(lunit)

// Convert the date to local type
FileTimeToSystemTime(date_to_process, system_time)
SystemTimeToTzSpecificLocalTime(time_zone, system_time, system_time)

// Return the date
str_date = string(system_time.month) + '-' + string(system_time.day) + '-' + string(system_time.year)
str_time = string(system_time.hour) + ':' + string(system_time.min) + '-' + string(system_time.sec)
return DateTime(date(str_date), time(str_time))
end function

public function string uf_find_path (string a_name);longlong mod
string path,prog_path
longlong i,status,len
string str
setnull(str)
LoadLibraryA(a_name)
mod =  GetModuleHandleA(a_name)
if mod <> 0 then
	path = space(601)
	status = GetModuleFileNameA(mod,path,600)
	if status <> 0 then
		len = status
		for i = len to 1 step -1
			if mid(path,i,1) = '\' then
				prog_path = mid(path,1,i)
				return prog_path
			end if
		next
	end if
end if
return ""
end function

public function longlong uf_printdlg (longlong lppd);return printdlg(long(lppd))
end function

public function longlong uf_commdlgextendederror ();return CommDlgExtendedError()
end function

public subroutine uf_rtlmovememoryps (unsignedlong ptr, s_print_dlg plld, unsignedlong len);RtlMoveMemoryPS(ptr, plld , len)
end subroutine

public subroutine uf_rtlmovememoryr (unsignedlong ptr, s_print_range plld, unsignedlong len);RtlMoveMemoryR( ptr, plld , len)
end subroutine

public function longlong uf_createdc (string display, longlong dev, longlong output, longlong data);return CreateDC(display, long(dev), long(output), long(data))
end function

public function longlong uf_cryptgenkey (unsignedlong hprov, unsignedinteger algid, unsignedlong dwflags, ref unsignedlong phkey);return CryptGenKey(hProv, Algid, dwFlags, phKey)
end function

public function integer uf_getscrollpos (longlong hwnd, integer sb_vert);return GetScrollPos(long(hwnd), sb_vert)
end function

public function longlong uf_gettimezoneinformation (ref s_time_zone_information a_time_zone);return GetTimeZoneInformation(a_time_zone)
end function

on u_external_function.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_external_function.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

