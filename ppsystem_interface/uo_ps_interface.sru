HA$PBExportHeader$uo_ps_interface.sru
$PBExportComments$v10.2.1.6 maint 5378: Added option to define the default directory for the SaveAs Function.
forward
global type uo_ps_interface from nonvisualobject
end type
type s_object_loc from structure within uo_ps_interface
end type
type s_device_mode from structure within uo_ps_interface
end type
type s_printer_defaults from structure within uo_ps_interface
end type
end forward

type s_object_loc from structure
	string		name
	longlong	x
	longlong	y
	longlong	h
	longlong	w
	string		font
	string		weight
	string		height
	string		family
	string		pitch
end type

type s_device_mode from structure
	string		devicename
	unsignedlong		specversion
	unsignedlong		driverversion
	unsignedlong		size
	unsignedlong		driverextra
	unsignedlong		field
	unsignedlong		orientation
	unsignedlong		papersize
	unsignedlong		paperlength
	unsignedlong		paperwidth
	unsignedlong		scale
	unsignedlong		copies
	unsignedlong		defaultsource
	unsignedlong		printquality
	unsignedlong		color
	unsignedlong		duplex
	unsignedlong		yresolution
	unsignedlong		collate
	string		formname
	unsignedlong		logpixels
	unsignedlong		bitsperpel
	unsignedlong		pelswidth
	unsignedlong		pelsheight
	unsignedlong		displayflags
	unsignedlong		displayfrequency
	unsignedlong		cmmethod
	unsignedlong		cmintent
	unsignedlong		mediatype
	unsignedlong		dithertype
	unsignedlong		reserved1
	unsignedlong		reserved2
end type

type s_printer_defaults from structure
	string		datatype
	s_device_mode		devmode
	longlong	access
end type

global type uo_ps_interface from nonvisualobject
end type
global uo_ps_interface uo_ps_interface

type prototypes

end prototypes

type variables
s_search_info search_info
s_search search
longlong SEARCH_TYPE = 2
longlong TEXT_TYPE = 1
longlong PDF_TYPE = 3
longlong GSPDF_TYPE = 10
longlong VERYDOC_MERGE_TYPE = 11
longlong PRN_TYPE = 6
longlong FIX_TYPE = 5
longlong LANDSCAPE = 1
longlong PORTRAIT = 0
longlong POST_TYPE =4
longlong MDI_TYPE =7
longlong search_pos
string i_dir
boolean i_initialize_dll=false
boolean i_initialize_pcl=false
boolean i_initialize_ps=false
boolean i_initialize_print=false
boolean i_initialize_print_pcl = false
string i_print_file
s_object_loc i_object_loc[]
longlong i_orig
longlong LOGPIXELSX   = 88
longlong LOGPIXELSY =  90 
s_mail_print mail_arg
boolean redraw = true
int i_default_printer_orig
string i_path
boolean i_add_printer = false
boolean g_main_application = true
end variables

forward prototypes
public function longlong uf_saveas_text (string a_filename, string a_outfile)
public function longlong uf_saveas_pdf (string a_filename, string a_outfile)
public function longlong uf_load_library (string a_lib)
public function longlong uf_find_string (string a_filename, string a_str, ref longlong a_page, ref longlong a_x, ref longlong a_y, ref longlong a_h, ref longlong a_w)
public function longlong uf_find_next (datawindow a_dw)
public function longlong uf_get_objects (datawindow a_dw)
public function longlong uf_set_fonts (datawindow a_dw)
public function longlong uf_reset_fonts (datawindow a_dw)
public function longlong uf_highlight_line (datawindow a_dw, longlong a_page, longlong a_x, longlong a_y, longlong a_h, longlong a_w)
public function longlong uf_find (datawindow a_dw)
public function longlong uf_get_display_pixels (ref longlong a_xpixels, ref longlong a_ypixels)
public function longlong uf_find_path (ref string a_path)
public function longlong uf_saveas_prn_pcl (string a_filename, string a_outfile)
public function longlong uf_set_fonts_pcl (datawindow a_dw)
public function longlong uf_saveas_fix_pcl (string a_filename, string a_outfile)
public function longlong uf_get_print_defs ()
public function longlong uf_saveas_ascii (datawindow a_dw, string a_pathname)
public function longlong uf_reset_fonts_pcl (datawindow a_dw)
public function string uf_get_temp_dir ()
public function longlong uf_save_as (datawindow a_dw)
public function longlong uf_add_printer (longlong a_post, ref string a_ptr_name, ref string a_drv_name)
public function string uf_get_default_printer ()
public function longlong uf_set_default_printer (string a_printer, datawindow a_dw)
public function longlong uf_get_printer (string a_printer, ref string a_name, ref string a_device, ref string a_port, datawindow a_dw)
public function longlong uf_print_file (datawindow a_dw)
public function longlong uf_selected_printer (longlong a_printer_type, datawindow a_dw)
public function longlong uf_print_file_pcl (datawindow a_dw)
public function longlong uf_mail_print (datawindow a_dw, string a_report_name)
public function longlong uf_print_file_ps (datawindow a_dw, string a_filename)
public function longlong uf_merge_pdf (string a_outfile, string a_pdffiles)
end prototypes

public function longlong uf_saveas_text (string a_filename, string a_outfile);longlong status
search_pos = 1


status = uo_winapi.uf_PSInterface(i_dir,a_filename,a_outfile,TEXT_TYPE, i_orig ,search_info)


return status
end function

public function longlong uf_saveas_pdf (string a_filename, string a_outfile);longlong status
search_pos = 1
longlong orig

status = uo_winapi.uf_PSInterface(i_dir,a_filename,a_outfile,PDF_TYPE,i_orig ,search_info)


return status
end function

public function longlong uf_load_library (string a_lib);string ini_file,str
string path
string err
string exe_path
longlong ret
longlong i
longlong str_len
longlong not_found = 1
longlong gspdf 
environment env

getenvironment(env)

// 16 bit enironment not supported
if env.win16 = true then
	return 0
end if


//if i_initialize_dll = true then
//	return 1
//end if

ini_file = f_get_ini_file()
gspdf  = Profileint(ini_file, "Application", "GSPDF", 0)
if gspdf = 1 then
	PDF_TYPE = GSPDF_TYPE
end if

path = ProfileString(ini_file, "Application", "PostscriptConverter", "")
i_dir = path
if len(path) > 0 then
	path = path + '\\'
	goto end_of_check
end if
not_found = 0

end_of_check:


if not_found = 0 then
   //ret = messagebox("Warning","Warning no path defined for PowerPlant.\n  PowerPlant " + &
   //          "will search the default path for DLLs.",exclamation!,OkCancel!)
   //if ret = 2 then
   //   return -1
   //end if
   return -1
else
  uf_find_path(exe_path)
  str = path + a_lib
  
  if FileExists(exe_path + a_lib) = true then
	 str = a_lib
	  i_dir = exe_path
	  i_path = exe_path
  elseif FileExists(str) = true then
	  i_path = path
	 
  else
	 
	  if g_main_application then 
			MessageBox ("Warning","Warning the DLL library " + str + " does not exist.",exclamation!,Ok!)
		else
			f_pp_msgs("Warning the DLL library " + str + " does not exist.")
	  end if
	  return -1
  end if
  
  ret = uo_winapi.uf_loadLibrarya(str)
 
  choose case ret
        case 0
           	err = 'System was out of memory, executable file was corrupt, or relocations ' + &
                  'were invalid.' 
	     case 2
            err = 'File was not found.' 
        case 3	
            err = 'Path was not found.' 
        case 5	
            err = 'Attempt was made to dynamically link to a task, or there ' + &
                  'was a sharing or network-protection error.' 
        case 6	
            err = 'Library required separate data segments for each task.' 
        case 8	
            err = 'There was insufficient memory to start the application.' 
        case 10	
            err = 'Windows version was incorrect.' 
        case 11	
            err = 'Executable file was invalid. Either it was not a ' + &
                   'Windows application or there was an error in the .EXE image.' 
        case 12	
            err = 'Application was designed for a different operating system.' 
        case 13	
            err = 'Application was designed for MS-DOS 4.0.' 
        case 14	
            err = 'Type of executable file was unknown.' 
        case 15	
            err = 'Attempt was made to load a real-mode application (developed for an ' + &
                  'earlier version of Windows).' 
        case 16	
            err = 'Attempt was made to load a second instance of an executable file containing ' + &
                  'multiple data segments that were not marked read-only.' 
        case 19	
            err = 'Attempt was made to load a compressed executable file. The file must ' + &
                  'be decompressed before it can be loaded.' 
        case 20	
            err = 'Dynamic-link library (DLL) file was invalid. One of the DLLs required ' + &
                  'to run this application was corrupt.' 
        case 21	
            err = 'Application requires Microsoft Windows 32-bit extensions. '
        case is > 32 
			   i_initialize_dll=true
            return 1
        case is < 0
			   i_initialize_dll=true
            return 1 
        case else
            err = 'Unknown Error Code = ' + string(ret)
     end choose
 
if g_main_application then 
		MessageBox ( "Error",err)
	else
		f_pp_msgs(err)
	end if
return -1  
end if

end function

public function longlong uf_find_string (string a_filename, string a_str, ref longlong a_page, ref longlong a_x, ref longlong a_y, ref longlong a_h, ref longlong a_w);search_pos = 1
search_info.str = a_str
search_info.count = 0

uo_winapi.uf_PSInterface(i_dir,a_filename,"",SEARCH_TYPE, i_orig,search_info)
search = search_info.searchlist[search_pos]
a_page = search.page
a_x = search.x
a_y = search.y
a_h = search.h
a_w = search.w

return search_info.count
end function

public function longlong uf_find_next (datawindow a_dw);
longlong page, x1,y1,h1,w1

search_pos++

if search_pos <= search_info.count then
	search = search_info.searchlist[search_pos]
	page = search.page
	x1 = search.x
	y1 = search.y
	h1 = search.h
	w1 = search.w
else
	return -1
end if

uf_highlight_line(a_dw,page,x1,y1,h1,w1)

 
 if search_pos = search_info.count then
	return 0
 end if
 return 1







return search_info.count
end function

public function longlong uf_get_objects (datawindow a_dw);string objects
string name,edit_str
string object_type,str
longlong i,num,pos,start_pos
string object_list[]
longlong count = 0
s_object_loc object_null[]

i_object_loc =  object_null

str = a_dw.describe("DataWindow.syntax")

objects = a_dw.Object.DataWindow.Objects
pos = pos(objects,"~t")
do while pos > 1
	count++
	object_list[count] = mid(objects,start_pos,pos - start_pos)
	start_pos = pos + 1
	pos = pos(objects,"~t",start_pos)
loop

count++
object_list[count] = mid(objects,start_pos)
for i = 1 to count
	i_object_loc[i].x = long(a_dw.describe(object_list[i] + ".x"))
	i_object_loc[i].y = long(a_dw.describe(object_list[i] + ".x"))
	i_object_loc[i].w = long(a_dw.describe(object_list[i] + ".width"))
	i_object_loc[i].h = long(a_dw.describe(object_list[i] + ".height"))
	i_object_loc[i].name = object_list[i]
next

return upperbound(i_object_loc)

end function

public function longlong uf_set_fonts (datawindow a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
  uf_get_objects(a_dw)
else
	return 0
end if

for i = 1 to upperbound(i_object_loc)
	  i_object_loc[i].font = a_dw.describe(i_object_loc[i].name + ".Font.Face")
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='Helvetica'")   // 'Helvetica' 'MS Sans Serif'
next

return 1
end function

public function longlong uf_reset_fonts (datawindow a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
	return 0
end if

for i = 1 to upperbound(i_object_loc)
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='" + i_object_loc[i].font + "'")
next

return 1
end function

public function longlong uf_highlight_line (datawindow a_dw, longlong a_page, longlong a_x, longlong a_y, longlong a_h, longlong a_w);longlong xunits,yunits,pb_units
longlong adjustment = 70
longlong barheight = 200
longlong width,x
longlong left_margin,bottom_margin,i
string modstring,str
longlong count,count2,count3
 
if i_orig = PORTRAIT then
   width = 7500
 else
   width = 10500 
end if

if long(a_dw.object.datawindow.units) = 0 then // PowerBuilder Units
	// no. pixel per inch
	uf_get_display_pixels(xunits,yunits)
	pb_units= PixelsToUnits ( xunits, XPixelsToUnits!   )
	a_x = (pb_units * a_x) /1000
	  // no. pixel per inch
	pb_units = PixelsToUnits (yunits, YPixelsToUnits! )
	a_y = (pb_units * a_y ) /1000
	
	adjustment = (pb_units * adjustment ) /1000
	barheight = (pb_units * barheight ) /1000
	width = (pb_units *  width ) /1000
elseif long(a_dw.object.datawindow.units) = 1 then // Display Pixel Units
   uf_get_display_pixels(xunits,yunits)
	// no. pixel per inch
	a_x = (xunits * a_x) /1000
	  // no. pixel per inch	
	a_y = (yunits * a_y ) /1000
	
	adjustment = (yunits * adjustment ) /1000
	barheight = (yunits * barheight ) /1000
	width  = (yunits * width  ) /1000
elseif  long(a_dw.object.datawindow.units) = 2 then // 1/1000 inches Units
  // do nothing 
else  // 1/1000 centimeter Units

end if
 
 
 left_margin = 0 //long(a_dw.object.datawindow.print.margin.left)
 bottom_margin = 0
 x = long(a_dw.object.datawindow.print.margin.top)
 a_x = 0
 //x1  - left_margin
 a_y =  a_y  + adjustment
 //+ bottom_margin - 180
 a_h =  barheight
 
 a_w = width

a_dw.setredraw(false)
a_dw.scrolltorow(0)
count2 = 0

if a_page > 1 then
 for i = 2 to a_page
	count3 = long(a_dw.object.DataWindow.LastRowOnPage)
	a_dw.scrolltorow(count3 + 1)
 next
end if

a_dw.setredraw(true)


modstring = 'destroy rect1'
a_dw.Modify(modstring)
modstring = 'create rectangle(Band=foreground X="' + string(a_x) + '" Y="' + string(a_y) + &
            '" height="' + string(a_h) + '" width="' + string(a_w) + '" ' + &
            'brush.hatch="7" brush.color="12632256" pen.width="19"  pen.style="0"  ' + &
				'pen.color="255" background.mode="2" background.color="0" name=rect1 )'
str = a_dw.Modify(modstring)
a_dw.setredraw(true)
return 1
end function

public function longlong uf_find (datawindow a_dw);string search_str
longlong x1,y1,h1,w1,page
s_search_info search_info_null


if i_initialize_dll = false then
	return -1
end if



//i_orig = long(a_dw.Object.DataWindow.print.Orientation)	
//if i_orig = 1 then
//	i_orig = LANDSCAPE
//elseif i_orig = 2 then
//	i_orig = portrait
//else
//	i_orig = uf_get_print_defs()
//end if


open(w_ps_find)
setpointer(hourglass!)
search_str = message.stringparm
if search_str = '' then
	return -1
end if

if i_initialize_print = false then
	  uf_print_file(a_dw) 
end if
search_info = search_info_null
uf_find_string(i_print_file,search_str,page,x1,y1,h1,w1)



if search_info.count = 0 then
 
	if g_main_application then 
		MessageBox ("Information","Could not find string: " + search_str)
	else
		f_pp_msgs( "Could not find string: " + search_str)
	end if
	return 0
end if

uf_highlight_line(a_dw,page,x1,y1,h1,w1)

if search_info.count = 1 then
	return 0
end if
return search_info.count
end function

public function longlong uf_get_display_pixels (ref longlong a_xpixels, ref longlong a_ypixels); longlong hdc
 hdc = uo_winapi.uf_CreateDCA( "DISPLAY", "", "", 0)
 a_xpixels = uo_winapi.uf_GetDeviceCaps(hdc,LOGPIXELSX)
 a_ypixels = uo_winapi.uf_GetDeviceCaps(hdc,LOGPIXELSY)
 uo_winapi.uf_DeleteDC(hdc)
 return 1
end function

public function longlong uf_find_path (ref string a_path);longlong mod
string path
longlong i,status,len
string str
setnull(str)
mod =  uo_winapi.uf_GetModuleHandleA(str)
if mod <> 0 then
	path = space(601)
	status = uo_winapi.uf_GetModuleFileName(mod,path,600)
	if status <> 0 then
		len = status
		for i = len to 1 step -1
			if mid(path,i,1) = '\' then
				a_path = mid(path,1,i)
				return 1
			end if
		next
	end if
end if
return -1
end function

public function longlong uf_saveas_prn_pcl (string a_filename, string a_outfile);longlong status
string temp_dir
search_pos = 1

temp_dir = uf_get_temp_dir()
status = uo_winapi.uf_PCLInterface(temp_dir,a_filename,a_outfile, PRN_TYPE, i_orig ,search_info)


return status
end function

public function longlong uf_set_fonts_pcl (datawindow a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
  uf_get_objects(a_dw)
else
	return 0
end if

for i = 1 to upperbound(i_object_loc)
	  i_object_loc[i].font =   a_dw.describe(i_object_loc[i].name + ".Font.Face")
     i_object_loc[i].height = a_dw.describe(i_object_loc[i].name + ".Font.height")
	  i_object_loc[i].weight = a_dw.describe(i_object_loc[i].name + ".Font.weight")
	  i_object_loc[i].family = a_dw.describe(i_object_loc[i].name + ".Font.family")
	  i_object_loc[i].pitch =  a_dw.describe(i_object_loc[i].name + ".Font.pitch")
	  
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='LinePrinter'")   // 'Helvetica' 'MS Sans Serif'
     str = a_dw.modify(i_object_loc[i].name + ".font.height='-8'")
	  str = a_dw.modify(i_object_loc[i].name + ".font.weight='400'")
	  str = a_dw.modify(i_object_loc[i].name + ".font.family='1'")
	  str = a_dw.modify(i_object_loc[i].name + ".font.pitch='1'")
	  
	//  str = a_dw.modify(i_object_loc[i].name + ".Width=10")
next

return 1
end function

public function longlong uf_saveas_fix_pcl (string a_filename, string a_outfile);longlong status
string temp_dir
search_pos = 1

temp_dir = uf_get_temp_dir()
status = uo_winapi.uf_PCLInterface(temp_dir,a_filename,a_outfile, FIX_TYPE, 200 ,search_info)


return status
end function

public function longlong uf_get_print_defs ();longlong status
return  i_default_printer_orig


end function

public function longlong uf_saveas_ascii (datawindow a_dw, string a_pathname);f_saveas_ascii(a_dw, a_pathname)
return 1
end function

public function longlong uf_reset_fonts_pcl (datawindow a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
	return 0
end if

for i = 1 to upperbound(i_object_loc)
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='" + i_object_loc[i].font + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.height='" + i_object_loc[i].height + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.weight='" + i_object_loc[i].weight + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.family='" + i_object_loc[i].family + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.pitch='" + i_object_loc[i].pitch + "'")
next 

return 1
end function

public function string uf_get_temp_dir ();string str
longlong status 
string ini_file
string temp
string str1
longlong len 

str = space(200)
str1 = space(600)
status = uo_winapi.uf_GetEnvironmentVariableA('TEMP',str,200)
if status = 0 then
	status = uo_winapi.uf_GetLastError()
	return ""
end if

//ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")

ini_file = f_get_ini_file() 


temp = ProfileString(ini_file, "Application", "Temp", "None")
if temp <> 'None' and  temp <> '' then
	str = temp
end if
len = uo_winapi.uf_GetLongPathNameA(str,str1,600)
if len < 1 then
	return str
end if
return str1
end function

public function longlong uf_save_as (datawindow a_dw);string title,pathname,filename
string temp_file,temp_dir,str
longlong pos,i
string initdir,ini_file
title = "Select File Name and File Type"

ini_file = f_get_ini_file() 
initdir = ProfileString(ini_file, "Application", "SaveAsDirectory", "")


if i_initialize_pcl = true and i_initialize_ps = true then      // hp and postscript
	GetFileSaveName ( title, pathname, filename ,"PDF", &
	       " PDF Files (*.PDF),*.PDF,"+ &
		 " ASCII Files (*.ASC),*.ASC," + &
		 " HTML Files (*.HTM),*.HTM," + &
		 " Postscript Files (*.PS),*.PS," + &
		 " PDF Files (*.PDF),*.PDF,"+ &
		  " Microsoft Office Document Image (*.MDI),*.MDI," + &
	        " Microsoft XPS Document Writer (*.XPS),*.XPS," + &
		 " Extensible Markup Language (*.XML),*.XML," + &
		 " Extensible Stylesheet Language (*.XSL),*.XSL,",initdir) 
elseif i_initialize_pcl = false and i_initialize_ps = true then // only postscript
	GetFileSaveName ( title, pathname, filename ,"PS", &
	        " PDF Files (*.PDF), *.PDF"+ &
		 " Microsoft Office Document Image (*.MDI),*.MDI," + &
		 " Microsoft XPS Document Writer (*.XPS),*.XPS," + &
		  " ASCII Files (*.ASC),*.ASC," + &
		  " HTML Files (*.HTM),*.HTM," + &
		 " Postscript Files (*.PS),*.PS," + &	
		 " Extensible Markup Language (*.XML),*.XML," + &
		 " Extensible Stylesheet Language (*.XSL),*.XSL,",initdir) 
else                                                            // only hp
	GetFileSaveName ( title, pathname, filename ,"PRN", &
		 " Microsoft Office Document Image (*.MDI),*.MDI," + &
		 " Microsoft XPS Document Writer (*.XPS),*.XPS," + &
		 " HTML Files (*.HTM),*.HTM," + &
		 " ASCII Files (*.ASC),*.ASC,"  + &
		 " Extensible Markup Language (*.XML),*.XML," + &
		 " Extensible Stylesheet Language (*.XSL),*.XSL,",initdir) 
end if

	// " Printer Files (*.PRN),*.PRN," + &
	//	 " Fixed Files (*.FIX),*.FIX," + &
setpointer(hourglass!)
filename = lower(filename)
pos = pos(filename,'.')


if pos(filename,'txt',pos) > 0 then
  if i_initialize_print=false then
	   if uf_print_file(a_dw) = -1 then 
			return -1
		end if
   end if
   uf_saveas_text(i_print_file,pathname)
elseif pos(filename,'pdf',pos) > 0 then 
  if i_initialize_print=false then
	   if uf_print_file(a_dw) = -1 then 
			return -1
		end if
	end if
	uf_saveas_pdf(i_print_file,pathname)
elseif pos(filename,'xps',pos) > 0 then 
	a_dw.object.datawindow.print.filename = pathname
	a_dw.Object.DataWindow.Printer ="Microsoft XPS Document Writer"
     a_dw.print()
	i_initialize_print=false
	temp_file = pathname
	i_print_file =temp_file
	a_dw.object.datawindow.printer =""
elseif pos(filename,'ps',pos) > 0 then 
	a_dw.object.datawindow.print.filename = pathname
   a_dw.print()
	i_initialize_print=false
	temp_file = pathname
	i_print_file =temp_file
elseif pos(filename,'mdi',pos) > 0 then 
	a_dw.object.datawindow.print.filename = pathname
	a_dw.Object.DataWindow.Printer ="Microsoft Office Document Image Writer"
     a_dw.print()
	i_initialize_print=false
	temp_file = pathname
	i_print_file =temp_file
	a_dw.object.datawindow.printer =""

elseif pos(filename,'prn',pos) > 0  then 
	if i_initialize_print_pcl=false then
	    if uf_print_file_pcl(a_dw) = -1 then 
			return -1
		end if
	end if
	uf_saveas_prn_pcl(i_print_file,pathname)
elseif pos(filename,'fix',pos) > 0 then 
	if i_initialize_print_pcl=false then
	    if uf_print_file_pcl(a_dw) = -1 then 
			return -1
		end if
	end if
	uf_saveas_fix_pcl(i_print_file,pathname)
elseif pos(filename,'asc',pos) > 0 then 
	uf_saveas_ascii(a_dw,pathname)
elseif pos(filename,'htm',pos) > 0   then 
	a_dw.saveas(pathname,HTMLTable!,true)
elseif pos(filename,'xml',pos) > 0   then 
	a_dw.saveas(pathname,XML!,true)
elseif pos(filename,'xsl',pos) > 0   then 
	a_dw.saveas(pathname,XSLFO!,true)
else
 
	if g_main_application then 
		MessageBox ("Error","Unknown file type. (" + filename + ")")
	else
		f_pp_msgs("Unknown file type. (" + filename + ")")
	end if
	return -1
end if
return 0

end function

public function longlong uf_add_printer (longlong a_post, ref string a_ptr_name, ref string a_drv_name);if uf_load_library("ppaddptr.dll") <> 1 then
	return 0
end if
longlong status
string ptr_name ,drv_name
longlong posts
ptr_name = a_ptr_name
drv_name = a_drv_name
posts = a_post
status = uo_winapi.uf_Add_Printer(posts,ptr_name,drv_name)
return status


//s_driver_info2 s_driver_info2
//s_printer_info2_add s_printer_info2
//longlong status ,code
//string ptr_name = 'ppc'
//string dest_dir,sys_dir
//if i_add_printer = true then
//   a_ptr_name = ptr_name	
//	return -1
//end if
//s_driver_info2.version = 2
//s_driver_info2.name = 'ppc_drv8'
//
//environment env
//getenvironment(env)
//if env.OSMajorRevision	= 4 and env.OSType = Windows! then // WIndows 85
//	s_driver_info2.env = 'Windows 4.0' 
//	s_driver_info2.driver_path = i_path + 'pscript.drv'
//	s_driver_info2.data_file = i_path + 'Apple380.spd'
//	s_driver_info2.config_file = i_path + 'pscript.drv'
//	uo_winapi.uf_GetEnvironmentVariableA('winbootdir',sys_dir,200)
//	sys_dir = sys_dir + '\system\'
//	dest_dir = sys_dir + 'pscript.drv'
//	status = uo_winapi.uf_CopyFileA( s_driver_info2.driver_path, dest_dir,1)
//   if status = 0 then
//	   code = uo_winapi.uf_GetLastError()
//	   if code <> 80 then // file already exist
//         MessageBox('Error!','Copy file ' + dest_dir + ' code=' + string(code))
//         return -1
//	    end if
//	 end if
//	 dest_dir = sys_dir + 'Apple380.spd'
//	status = uo_winapi.uf_CopyFileA( s_driver_info2.driver_path, dest_dir,1)
//   if status = 0 then
//	   code = uo_winapi.uf_GetLastError()
//	   if code <> 80 then // file already exist
//         MessageBox('Error!','Copy file ' + dest_dir + ' code=' + string(code))
//         return -1
//	    end if
//	 end if
//else                                                         // WIndows NT
//	s_driver_info2.env = 'Windows NT x86' 
//	s_driver_info2.driver_path = i_path + 'pscript.dll'
//	s_driver_info2.data_file = i_path + 'Apple380.ppd'
//	s_driver_info2.config_file = i_path + 'PSCRPTUI.DLL'
//	sys_dir = space(200)
//	uo_winapi.uf_GetEnvironmentVariableA('systemroot',sys_dir,200)
//	sys_dir = sys_dir + '\system32\spool\drivers\w32x86\'
//	dest_dir = sys_dir + 'pscript.dll'
//	status = uo_winapi.uf_CopyFileA( s_driver_info2.driver_path, dest_dir,1)
//   if status = 0 then
//	   code = uo_winapi.uf_GetLastError()
//	   if code <> 80 then // file already exist
//         MessageBox('Error!','Copy file ' + dest_dir + ' code=' + string(code))
//         return -1
//	    end if
//	 end if
//	 dest_dir = sys_dir + 'Apple380.ppd'
//	status = uo_winapi.uf_CopyFileA( s_driver_info2.driver_path, dest_dir,1)
//   if status = 0 then
//	   code = uo_winapi.uf_GetLastError()
//	   if code <> 80 then // file already exist
//         MessageBox('Error!','Copy file ' + dest_dir + ' code=' + string(code))
//         return -1
//	    end if
//	 end if
//	dest_dir = sys_dir + 'PSCRPTUI.DLL'
//	status = uo_winapi.uf_CopyFileA( s_driver_info2.driver_path, dest_dir,1)
//   if status = 0 then
//	   code = uo_winapi.uf_GetLastError()
//	   if code <> 80 then // file already exist
//         MessageBox('Error!','Copy file ' + dest_dir + ' code=' + string(code))
//         return -1
//	    end if
//	 end if
//end if
//status = uo_winapi.uf_DeletePrinterDriverA(0,0,s_driver_info2.name)
////if status = 0  then
////	code = uo_winapi.uf_GetLastError()
////   MessageBox('Error!','Adding printer driver ' + string(code))
////  return -1
////end if
//status = uo_winapi.uf_AddPrinterDriverA(0,2,s_driver_info2)
//if status = 0  then
//	code = uo_winapi.uf_GetLastError()
//	if code <> 1795 then // Already exist
//	     MessageBox('Error!','Adding printer driver ' + string(code))
//		  return -1
//	end if
//end if
//
//s_printer_info2.servername = 0
//s_printer_info2.printername = ptr_name
//s_printer_info2.sharename = 0
//s_printer_info2.portname = 'LPT1:'
//s_printer_info2.drivername = s_driver_info2.name
//s_printer_info2.comment = 0
//s_printer_info2.location = 0
//s_printer_info2.devmode = 0
//s_printer_info2.sepfile = 0
//s_printer_info2.printprocessor = 'winprint'
//s_printer_info2.datatype = 'RAW'
//s_printer_info2.parameters = 0
//s_printer_info2.security =0
//s_printer_info2.attributes = 0
//s_printer_info2.priority = 1
//s_printer_info2.defaultpriority = 1
//s_printer_info2.starttime = 0
//s_printer_info2.uitltime = 0
//s_printer_info2.status = 0
//s_printer_info2.cjobs = 0
//s_printer_info2.averageppm = 8
//
//status = uo_winapi.uf_AddPrinterA(0,2,s_printer_info2)
//if status = 0 then
//	code = uo_winapi.uf_GetLastError()
//	if code <> 1802 then // Already exist
//	  MessageBox('Error!','Adding printer ' + string(code))
//	  return -1
//   end if
//end if
//a_ptr_name = ptr_name
//i_add_printer = true
//return 0
end function

public function string uf_get_default_printer ();string ptr
longlong status
longlong hmod
string funct
longlong buflen = 200

hmod =  uo_winapi.uf_GetModuleHandleA('psconvrt.dll')
funct = 'GetDefPrinter'
if uo_winapi.uf_GetProcAddress(hmod,funct) = 0 then
	ptr = ProfileString("win.ini","windows","device","")
else
	ptr = space(buflen)
   status = uo_winapi.uf_GetDefPrinter(ptr,buflen);
end if


return ptr
end function

public function longlong uf_set_default_printer (string a_printer, datawindow a_dw);
string ptr
longlong status = 1
longlong hmod
string funct
environment env
boolean crosstab_report = false

if long(a_dw.Object.DataWindow.Processing) = 4 then
	  crosstab_report = true
end if

getenvironment(env)
if env.PBMajorRevision	 > 8 then
	if crosstab_report = false then
	    	 a_dw.Object.DataWindow.Printer = a_printer
	else 
		status = uo_winapi.uf_SetDefPrinter(a_printer)
		if status = 0 then
			if g_main_application then 
					MessageBox ("Error","Failed to set default printer")
			 else
					f_pp_msgs( "Failed to set default printer")
			 end if
		end if
	end if


end if

return status
end function

public function longlong uf_get_printer (string a_printer, ref string a_name, ref string a_device, ref string a_port, datawindow a_dw);longlong hmod
string funct,ptr

a_name = space(200)
a_device = space(200)
a_port = space(200)
///num = 

hmod =  uo_winapi.uf_GetModuleHandleA('psconvrt.dll')
funct = 'GetPrinterInfo'
if uo_winapi.uf_GetProcAddress(hmod,funct) = 0 then
	return -1
end if

return uf_set_default_printer(a_printer,a_dw) 
//if uo_winapi.uf_GetPrinterInfo(a_printer,a_name,a_device,a_port) = 1 then
//	ptr = a_printer + ',' + a_device + ',' + a_port
//	SetProfileString ( "win.ini","windows","device", ptr )
//	return 1
//end if
//return -1
end function

public function longlong uf_print_file (datawindow a_dw);string temp_dir,str
string temp_file
longlong status,ret
longlong orig,postscript
string printer,drv,port
string ptr1,ptr2
string drv_name,name
boolean crosstab_report = false
string ini_file
s_object_loc i_object_loc_null[]
ptr2 = ''


if long(a_dw.Object.DataWindow.Processing) = 4 then
	  crosstab_report = true
end if

ptr1 = uf_get_default_printer()

ini_file = f_get_ini_file()
ptr2 = ProfileString(ini_file,"Application","Postscript_Printer","") 
if upper(ptr2) <> "ANY" and   upper(ptr2) <> "" then
	 uf_set_default_printer(ptr2,a_dw) 
else
	// Check for a postscipt printer
	// Does the user have s pre-selected printer from the ini file
	if uf_selected_printer(POST_TYPE,a_dw) = 1 then
		ptr2 = 'change'
	// Is the standard PowerPlant printer defined	
	elseif uf_get_printer('ppc_ps',name,drv_name,port,a_dw) = 1 then
		ptr2 = 'change'
		uf_set_default_printer('ppc_ps',a_dw) 
	else
		// Get the default printer and is it a postscript printer
		uo_winapi.uf_GetPrintDef(orig,postscript)
		if postscript <> 1 then
				 ptr2 = 'ppc_ps'
				 uf_set_default_printer('ppc_ps',a_dw) 
		end if
	end if
end if

temp_dir = uf_get_temp_dir()
if temp_dir = '' then
 
 if g_main_application then 
		MessageBox ("Error","Can't find the temp directory.")
 else
		f_pp_msgs( "Can't find the temp directory.")
 end if
  return -1
end if
temp_file = temp_dir + "\pstemp.ps"
a_dw.object.datawindow.print.filename = temp_file

status = a_dw.setredraw(false)
  
i_object_loc = i_object_loc_null
if crosstab_report = false then
   uf_set_fonts(a_dw)
end if


i_orig = long(a_dw.Object.DataWindow.print.Orientation)	
if i_orig = 0 then
	status = uo_winapi.uf_GetPrinterOrientation()
	if status = -1 then
		i_orig = landscape
		str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	elseif status = 1 then
		i_orig = portrait
	else
		i_orig = landscape
	end if
elseif i_orig = 2 then
	i_orig = portrait
end if

a_dw.print()

if crosstab_report = false then
    uf_reset_fonts(a_dw)
end if
// set printer back to the default
if ptr2 <> '' and len(trim(ptr1)) > 0 then
	uf_set_default_printer(ptr1,a_dw)
end if

a_dw.Modify("DataWindow.Print.Preview=yes")
if redraw <> false then
  a_dw.setredraw(true)
end if
a_dw.object.datawindow.print.filename = ''
i_initialize_print = true
i_print_file = temp_file
setpointer(hourglass!)
return 1
end function

public function longlong uf_selected_printer (longlong a_printer_type, datawindow a_dw);string name,printer,drv,port
string ptr2,funct
string ini_file
longlong hmod
longlong num,rnum,status
s_printer_info2 printer_info2
//ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")


ini_file = f_get_ini_file() 


if a_printer_type = POST_TYPE then
	printer = ProfileString(ini_file, "Application", "Postscript_Printer", "")
else
	printer = ProfileString(ini_file, "Application", "HP_Printer", "")
end if
if printer = "" then
	return -1
end if

return uf_get_printer(printer,name,drv,port,a_dw)

end function

public function longlong uf_print_file_pcl (datawindow a_dw);string temp_dir,str
string temp_file
longlong status,ret
longlong orig,pcl
string printer,drv,port
string ptr1,ptr2
string drv_name,name
string ini_file
s_object_loc i_object_loc_null[]
ptr2 = ''

boolean crosstab_report = false


if long(a_dw.Object.DataWindow.Processing) = 4 then
	  crosstab_report = true
end if

// Check for a PCL printer

ptr1 = uf_get_default_printer()
// Does the user have s pre-selected printer from the ini file
ini_file = f_get_ini_file()
ptr2 = ProfileString(ini_file,"Application","HP_Printer","") 

if upper(ptr2) <> "ANY" and  upper(ptr2) <> "" then
	 uf_set_default_printer(ptr2,a_dw) 
else
	if uf_selected_printer(TEXT_TYPE,a_dw) = 1 then
		ptr2 = 'change'
		uf_set_default_printer(ptr2,a_dw) 
	// Is the standard PowerPlant printer defined	
	elseif uf_get_printer('ppc_hp',name,drv_name,port,a_dw) = 1 then
		ptr2 = 'change'
		uf_set_default_printer('ppc_hp',a_dw) 
	else
		// Get the default printer and is it a postscript printer
		uo_winapi.uf_GetPrintDefPCL(orig,pcl)
		if pcl <> 1 then
				ptr2 = 'ppc_hp'
				uf_set_default_printer(ptr2,a_dw) 
		end if
	end if
end if

// this turn HPGL Mode for graphics 
environment env
getenvironment(env)
if env.OSMajorRevision	= 4 and env.OSType = Windows! then // WIndows 95
   SetProfileString ( "hp5si.ini",printer,"ManualPrintModel", "4" )
end if


temp_dir = uf_get_temp_dir()
if temp_dir = '' then
  	if g_main_application then 
		MessageBox ("Error","Can't find the temp directory.")
	else
		f_pp_msgs("Can't find the temp directory.")
	end if
  return -1
end if
temp_file = temp_dir + "\pcltemp.pcl"
a_dw.object.datawindow.print.filename = temp_file

status = a_dw.setredraw(false)
  
i_object_loc = i_object_loc_null
if crosstab_report = false then
    uf_set_fonts(a_dw)
end if

i_orig = long(a_dw.Object.DataWindow.print.Orientation)	
if i_orig = 0 then
	status = uo_winapi.uf_GetPrinterOrientationPCL()
	if status = -1 then
		i_orig = landscape
		str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	elseif status = 1 then
		i_orig = portrait
	else
		i_orig = landscape
	end if
elseif i_orig = 2 then
	i_orig = portrait
end if

a_dw.print()
if crosstab_report = false then
   uf_reset_fonts(a_dw)
end if
// set printer back to the default

if ptr2 <> '' and len(trim(ptr1)) > 0 then
	uf_set_default_printer(ptr1,a_dw)
end if
a_dw.Modify("DataWindow.Print.Preview=yes")
if redraw <> false then
  a_dw.setredraw(true)
end if
a_dw.object.datawindow.print.filename = ''
i_initialize_print_pcl = true
i_print_file = temp_file
setpointer(hourglass!)
return 1
end function

public function longlong uf_mail_print (datawindow a_dw, string a_report_name);string title,pathname,filename
string temp_file,temp_dir,str
string from_user,to_user
longlong pos,i



if mail_arg.from_user = '' then
	mail_arg.from_user = sqlca.logid
end if
mail_arg.msg = "Enclosed is a copy of the report " + a_report_name + "."
openwithparm(w_mail_print,mail_arg,'w_mail_print')
mail_arg = message.powerobjectparm


if not isvalid(mail_arg) then return -1 

if mail_arg.print_type = -1 then
	return -1
end if
from_user = mail_arg.from_user
to_user = mail_arg.to_user



filename = lower(filename)
pos = pos(filename,'.')

i_orig = long(a_dw.Object.DataWindow.print.Orientation)	
if i_orig = 1 then
	i_orig = LANDSCAPE
else
	i_orig = portrait
end if


if mail_arg.print_type = TEXT_TYPE  then
  if i_initialize_print=false then
	  uf_print_file(a_dw)
   end if 
	pathname = uf_get_temp_dir() + '\report.txt'
	uf_saveas_text(i_print_file,pathname)
elseif mail_arg.print_type = PDF_TYPE then 
  if i_initialize_print=false then
	    uf_print_file(a_dw)
	end if
	pathname = uf_get_temp_dir() + '\report.pdf'
	uf_saveas_pdf(i_print_file,pathname)
elseif mail_arg.print_type = PRN_TYPE  then 
	if i_initialize_print_pcl=false then
	    if uf_print_file_pcl(a_dw) = -1 then 
			return -1
		end if
	end if
	pathname = uf_get_temp_dir() + '\report.prn'
	uf_saveas_prn_pcl(i_print_file,pathname)
elseif mail_arg.print_type = FIX_TYPE  then 
	if i_initialize_print_pcl=false then
	    if uf_print_file_pcl(a_dw) = -1 then 
			return -1
		end if
	end if
	pathname = uf_get_temp_dir() + '\report.fix'
	uf_saveas_fix_pcl(i_print_file,pathname)
elseif mail_arg.print_type = MDI_TYPE then 
   pathname = uf_get_temp_dir() + '\report.mdi'
	a_dw.object.datawindow.print.filename = pathname
	a_dw.Object.DataWindow.Printer ="Microsoft Office Document Image Writer"
   a_dw.print()
	i_initialize_print=false
	temp_file = pathname
	i_print_file =temp_file
	a_dw.object.datawindow.printer =""
else
 
	if g_main_application then 
		MessageBox ("Error","Unknown file type.")
	else
		f_pp_msgs( "Unknown file type.")
	end if
	return -1
end if

string msg
//blob b = blob('')

msg = mail_arg.msg
g_msmail.sendfile(from_user,'',a_report_name,msg,to_user,'',pathname)

if mail_arg.copy_sender then
	g_msmail.sendfile(from_user,'',a_report_name,msg,from_user,'',pathname)
end if

return 0

end function

public function longlong uf_print_file_ps (datawindow a_dw, string a_filename);string temp_dir,str
string temp_file
longlong status,ret
longlong orig,postscript
string printer,drv,port
string ptr1,ptr2
string drv_name,name
string ini_file
s_object_loc i_object_loc_null[]
ptr2 = ''


// Check for a postscipt printer
ptr1 = uf_get_default_printer()
// Does the user have s pre-selected printer from the ini file
ini_file = f_get_ini_file()
ptr2 = ProfileString(ini_file,"Application","Postscript_Printer","") 
if upper(ptr2) <> "ANY" and  upper(ptr2) <> "" then
	 uf_set_default_printer(ptr2,a_dw) 
else
	// Check for a postscipt printer
	// Does the user have s pre-selected printer from the ini file
	if uf_selected_printer(POST_TYPE,a_dw) = 1 then
		ptr2 = 'change'
	// Is the standard PowerPlant printer defined	
	elseif uf_get_printer('ppc_ps',name,drv_name,port,a_dw) = 1 then
		ptr2 = 'change'
		uf_set_default_printer('ppc_ps',a_dw) 
	else
		// Get the default printer and is it a postscript printer
		uo_winapi.uf_GetPrintDef(orig,postscript)
		if postscript <> 1 then
				 ptr2 = 'ppc_ps'
				 uf_set_default_printer('ppc_ps',a_dw) 
		end if
	end if
end if
   

temp_file = a_filename
a_dw.object.datawindow.print.filename = temp_file


  
i_object_loc = i_object_loc_null
uf_set_fonts(a_dw)

i_orig = long(a_dw.Object.DataWindow.print.Orientation)   
if i_orig = 0 then
   //i_orig = landscape
   //str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
   status = uo_winapi.uf_GetPrinterOrientation()
   if status = -1 then
      i_orig = landscape
      str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
   elseif status = 1 then
      i_orig = portrait
   else
      i_orig = landscape
   end if
elseif i_orig = 2 then
   i_orig = portrait
end if


a_dw.print()
uf_reset_fonts(a_dw)
// set printer back to the default
if ptr2 <> '' and len(trim(ptr1)) > 0 then
	uf_set_default_printer(ptr1,a_dw)
end if

a_dw.Modify("DataWindow.Print.Preview=yes")
if redraw <> false then
  a_dw.setredraw(true)
end if
a_dw.object.datawindow.print.filename = ''
i_initialize_print = true
i_print_file = temp_file
setpointer(hourglass!)
return 1

end function

public function longlong uf_merge_pdf (string a_outfile, string a_pdffiles);longlong status
search_pos = 1
longlong orig

status = uo_winapi.uf_PSInterface(i_dir,a_pdffiles,a_outfile,VERYDOC_MERGE_TYPE,i_orig ,search_info)


return status
end function

on uo_ps_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ps_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;if uf_load_library("psconvrt.dll") = 1 then
	i_initialize_ps = true
end if

if uf_load_library("pclcnvrt.dll") = 1 then
	i_initialize_pcl = true
end if

if i_initialize_pcl = true or i_initialize_ps = true then
	i_initialize_dll = true
end if
end event

event destructor;boolean status 
status = FileDelete ( i_print_file)
return
end event

