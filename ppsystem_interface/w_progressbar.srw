HA$PBExportHeader$w_progressbar.srw
forward
global type w_progressbar from window
end type
type cb_1 from commandbutton within w_progressbar
end type
type st_msg from statictext within w_progressbar
end type
type hpb_1 from hprogressbar within w_progressbar
end type
end forward

global type w_progressbar from window
integer width = 1650
integer height = 468
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = child!
long backcolor = 67108864
cb_1 cb_1
st_msg st_msg
hpb_1 hpb_1
end type
global w_progressbar w_progressbar

on w_progressbar.create
this.cb_1=create cb_1
this.st_msg=create st_msg
this.hpb_1=create hpb_1
this.Control[]={this.cb_1,&
this.st_msg,&
this.hpb_1}
end on

on w_progressbar.destroy
destroy(this.cb_1)
destroy(this.st_msg)
destroy(this.hpb_1)
end on

event open;window  lw_father
integer li_x, li_y, li_max_width

integer first_comma, second_comma, third_comma
longlong max_range, position
string title, msg, param, picname

param = Message.StringParm
first_comma = pos(param,',') 
second_comma = pos(param, ',', first_comma + 1)
third_comma = pos(param, ',', second_comma + 1)

title = mid(param, 1, first_comma - 1)
msg = mid(param, first_comma + 1, second_comma - first_comma - 1)
max_range = long(mid(param, second_comma + 1, third_comma - second_comma - 1))
position = long(mid(param, third_comma + 1, len(param) - second_comma - 1))

this.title = title
st_msg.text = msg

hpb_1.maxPosition=MAX_RANGE
hpb_1.Position=position/max_range * 100


lw_father = parentwindow(this)

// Center the window on it's parent
li_x = lw_father.x + (lw_father.width - this.width)/2
if li_x < 10 then li_x = 10  // make sure it's on the screen
li_y = lw_father.y + (lw_father.height - this.height)/2
if li_y < 10 then li_y = 10     // make sure it's on the screen

move(this, li_x, li_y)


end event

type cb_1 from commandbutton within w_progressbar
integer x = 681
integer y = 252
integer width = 343
integer height = 84
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;close(w_progressbar)
end event

type st_msg from statictext within w_progressbar
integer x = 73
integer y = 76
integer width = 1490
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_progressbar
integer x = 73
integer y = 156
integer width = 1495
integer height = 52
unsignedinteger maxposition = 100
integer setstep = 10
boolean smoothscroll = true
end type

