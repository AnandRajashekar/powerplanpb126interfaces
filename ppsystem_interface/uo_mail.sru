HA$PBExportHeader$uo_mail.sru
$PBExportComments$v10.2 maint 2357: Added support for mail to send multiple attachments in an email message.
forward
global type uo_mail from nonvisualobject
end type
end forward

global type uo_mail from nonvisualobject
end type
global uo_mail uo_mail

type variables
mailSession		i_mSes
mailReturnCode		i_mRet
mailMessage		i_mMsg
mailFileDescription i_mFile
string i_file
string user_list[]
string cc_list[]
string attachment
string subject
string text
string i_server
end variables

forward prototypes
public subroutine initialize ()
public function boolean sendmessage ()
public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server)
public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename)
public function boolean add_attachment_file (string a_filename)
public function boolean add_attachment_blob (string a_attachment_name, blob a_blob)
public function boolean add_attachment_sql (string a_attachment_name, string a_sql)
public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename, boolean a_delete_files)
public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, boolean a_delete_files)
end prototypes

public subroutine initialize ();string del_list[]
user_list=del_list
cc_list=del_list

end subroutine

public function boolean sendmessage ();//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   uo_msmail.sendmessage
//
//   Purpose  :   Send a Microsoft mail message
//                
// 
//   Scope    :   global
//
//   Arguments:   
//
//   Returns :    success or falure     :  boolean     
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    05-15-94    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
int i,j,count
string mail_user
pointer oldpointer
oldpointer = SetPointer(HourGlass!)
mailfiledescription mFileNull

i_mRet = i_mSes.mailLogon ( mailNewSession! )
if attachment <> "" then
    i_mfile.PathName = attachment
    i_mfile.FileType = mailAttach!
    i_mfile.Position = 0 
    i_mMsg.AttachmentFile[1] = i_mFile
else
    i_mMsg.AttachmentFile[1] = mFileNull  
end if
  


if i_mRet <> mailReturnSuccess!  and i_mRet <>  mailReturnTooManySessions! then
 
	if g_main_application then 
		MessageBox ( "Mail Logon", 'Return Code <> mailReturnSuccess!' )
	else
		f_pp_msgs('Return Code <> mailReturnSuccess!' )
	end if
   SetPointer(oldpointer)
	Return false
end if

count = 0
for i = 1 to upperbound(user_list)
    select mail_id into :mail_user from pp_security_users where users = rtrim(:user_list[i]);
    if(sqlca.sqlcode = 0) then
      count = count + 1
      i_mMsg.Recipient[count].name = mail_user
      i_mMsg.Recipient[count].Recipienttype = mailTo!
    else
	 
	   	if g_main_application then 
			MessageBox ( "Mail ID", 'No Mail ID exist for ' + cc_list[j] )
		else
			f_pp_msgs( 'No Mail ID exist for ' + cc_list[j] )
		end if
    end if 
next

for j = 1 to upperbound(cc_list)
    select mail_id into :mail_user from pp_security_users where users = rtrim(:cc_list[j]);
    if(sqlca.sqlcode = 0) then
      count = count + 1
      i_mMsg.Recipient[count].name = mail_user
      i_mMsg.Recipient[count].Recipienttype = mailCC!
    else
	  
	  	if g_main_application then 
			MessageBox ("Mail ID", 'No Mail ID exist for ' + cc_list[j] )
		else
			f_pp_msgs('No Mail ID exist for ' + cc_list[j] )
		end if
    end if 
next


// Populate the mailFileDescription structure with information about 
// the message.

if subject = "" then
   SetPointer(oldpointer)
 
	if g_main_application then 
			MessageBox ("Mail Subject", 'No Mail Subject Defined' )
	else
			f_pp_msgs( 'No Mail Subject Defined' )
	end if
	return false
else
  i_mMsg.Subject = subject
end if 
 
if text = "" then 
   SetPointer(oldpointer)
 
	if g_main_application then 
		MessageBox ("Mail Text", 'No Mail Text Defined' ) 
	else
		f_pp_msgs('No Mail Text Defined' )  
	end if
	return false
else
   i_mMsg.NoteText = text
end if   

if upperbound(user_list) < 1 then 
  i_mRet = i_mSes.mailAddress ( i_mMsg ) 
  if i_mRet <> mailReturnSuccess! then
    SetPointer(oldpointer)
  
   	if g_main_application then 
		MessageBox ("Mail Address", 'Return Code <> mailReturnSuccess!' )
	else
		f_pp_msgs('Return Code <> mailReturnSuccess!' )
	end if
    return false
  end if
end if


i_mRet = i_mSes.mailSend ( i_mMsg )
if i_mRet <> mailReturnSuccess! then
   SetPointer(oldpointer)
 
	if g_main_application then 
		MessageBox ("Mail Send", 'Return Code <> mailReturnSuccess!' )
	else
		f_pp_msgs( 'Return Code <> mailReturnSuccess!' )
	end if
	return false
end if


//i_mSes.mailLogoff()
//destroy i_mSes
SetPointer(oldpointer)
return true
end function

public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server);return send(a_fromuser, a_password, a_subject, a_text, a_touser, a_server, true)
end function

public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename);return sendfile(a_fromuser, a_password, a_subject, a_text, a_touser, a_server, a_filename, true)
end function

public function boolean add_attachment_file (string a_filename);return false
end function

public function boolean add_attachment_blob (string a_attachment_name, blob a_blob);return false
end function

public function boolean add_attachment_sql (string a_attachment_name, string a_sql);
return false
end function

public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename, boolean a_delete_files);return true
end function

public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, boolean a_delete_files);return true
end function

on constructor;
// Create a mail session.

i_mSes = create mailSession
initialize()
end on

on uo_mail.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mail.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

