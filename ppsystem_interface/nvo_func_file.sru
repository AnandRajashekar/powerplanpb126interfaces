HA$PBExportHeader$nvo_func_file.sru
forward
global type nvo_func_file from nonvisualobject
end type
end forward

global type nvo_func_file from nonvisualobject
end type
global nvo_func_file nvo_func_file

type variables

end variables

forward prototypes
public function integer of_get_file_info (ref s_file_info a_file_info)
public function s_file_info of_get_file_info ()
end prototypes

public function integer of_get_file_info (ref s_file_info a_file_info);//  A_FILE_INFO should have FILE_NAME and FILE_PATH populated already.

// ************************************************************
//		Create / Write Date
// ************************************************************
a_file_info.file_write_date = uo_winapi.uf_getfiledate(a_file_info.file_path + a_file_info.file_name,'WRITE')
a_file_info.file_create_date = uo_winapi.uf_getfiledate(a_file_info.file_path + a_file_info.file_name,'CREATE')

// ************************************************************
//		File Version
// ************************************************************
a_file_info.file_version = uo_winapi.uf_getfileversion(a_file_info.file_path + a_file_info.file_name)

return 0
end function

public function s_file_info of_get_file_info ();s_file_info file_info
string str
longlong mod, status

// Get the file information for the current executable.
// Get the current exe.
setnull(str)
mod =  uo_winapi.GetModuleHandleA(str)
if mod <> 0 then
	file_info.file_name = space(601)
	status = uo_winapi.GetModuleFileNameA(mod,file_info.file_name,600)
else
	return file_info
end if

// The file_name has both the path and exe name.
// Parse them out.
do while pos(file_info.file_name,'\') > 0
	file_info.file_path += mid(file_info.file_name,1,pos(file_info.file_name,'\'))
	file_info.file_name = mid(file_info.file_name,pos(file_info.file_name,'\') + 1)
loop

of_get_file_info(file_info)
return file_info

end function

on nvo_func_file.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_func_file.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

