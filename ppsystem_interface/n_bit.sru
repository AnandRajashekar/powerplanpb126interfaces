HA$PBExportHeader$n_bit.sru
forward
global type n_bit from nonvisualobject
end type
end forward

global type n_bit from nonvisualobject autoinstantiate
end type

forward prototypes
public function string of_binary (longlong al_decimal)
public function longlong of_decimal (string as_binary)
public function boolean of_getbit (longlong al_decimal, unsignedlong aui_bit)
public function longlong of_bitwiseand (longlong al_decimala, longlong al_decimalb)
public function longlong of_bitwiseor (longlong al_decimala, longlong al_decimalb)
public function longlong of_setbit (longlong al_decimal, unsignedlong aui_bit)
public function longlong of_clearbit (longlong al_decimal, unsignedlong aui_bit)
public function longlong of_flipbit (longlong al_decimal, unsignedlong aui_bit)
public function longlong of_bitwisenot (longlong al_decimal)
public function longlong of_bitwisexor (longlong al_decimala, longlong al_decimalb)
public function longlong of_bitwiseand (longlong ai_decimala, longlong ai_decimalb)
public function longlong of_bitwiseor (longlong ai_decimala, longlong ai_decimalb)
public function int of_setbit (int ai_decimal, unsignedlong aui_bit)
public function int of_clearbit (int ai_decimal, unsignedlong aui_bit)
public function int of_flipbit (int ai_decimal, unsignedlong aui_bit)
public function longlong of_bitwisenot (longlong ai_decimal)
public function longlong of_bitwisexor (longlong ai_decimala, longlong ai_decimalb)
end prototypes

public function string of_binary (longlong al_decimal);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_Binary
//
// Access:	public
//
//	Arguments:
//		al_decimal		unsigned longlong to convert to bit string
//
//	Returns:  string
//
//	Description:	Convert al_decimal to a string of 32 1's and 0's in
//						big-endian format i.e with LSB at position 1.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

string 			ls_binary
unsignedint 	lui_cnt
longlong 				ll_remainder
unsignedlong	lul_decimal

lul_decimal = al_decimal

for lui_cnt = 1 to 32
	ll_remainder = mod(lul_decimal, 2)
	lul_decimal = lul_decimal / 2

	ls_binary = ls_binary + string(ll_remainder)
next

return ls_binary
end function

public function longlong of_decimal (string as_binary);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_Decimal
//
// Access:	public
//
//	Arguments:
//		as_binary	string to convert
//
//	Returns:	long
//
//	Description:	Convert an array of 1's and 0's in big-endian format
//						i.e. LSB at index 1 to a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

longlong ll_decimal
unsignedint lui_cnt

// Propogate the sign bit
as_binary += Fill(Right(as_binary, 1), 31)

// Calculate the value
for lui_cnt = 1 to 32
	ll_decimal = ll_decimal + (long(Mid(as_binary, lui_cnt, 1)) * (2 ^ (lui_cnt - 1)))
next

return ll_decimal
end function

public function boolean of_getbit (longlong al_decimal, unsignedlong aui_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_GetBit
//
// Access:	public
//
//	Arguments:
//		al_decimal		value
//		aui_bit			bit number to test
//
//	Returns:  boolean
//
//	Description:	Tests a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

unsignedlong	lul_decimal

lul_decimal	= al_decimal

if (Mod(long(lul_decimal /  2 ^(aui_bit - 1)), 2) > 0) then
	return true
end if

return false
end function

public function longlong of_bitwiseand (longlong ai_decimala, longlong ai_decimalb);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_BitwiseAND
//
// Access:	public
//
//	Arguments:
//		ai_decimala
//		ai_decimalb
//
//	Returns:  int
//
//	Description:	Logically AND 2 numbers
//
//		a	b	result
//		-- -- ------
//		0	0	0
//		0	1	0
//		1	0	0
//		1	1	1
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

unsignedint lui_bit
int			li_decimal

li_decimal = 0

for lui_bit = 1 to 16
	if this.of_GetBit(ai_decimala, lui_bit) and this.of_GetBit(ai_decimalb, lui_bit) then
		li_decimal = this.of_SetBit(li_decimal, lui_bit)
	end if
next

return li_decimal
end function

public function longlong of_bitwiseor (longlong ai_decimala, longlong ai_decimalb);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_BitwiseOR
//
// Access:	public
//
//	Arguments:
//		ai_decimala
//		ai_decimalb
//
//	Returns:  int
//
//	Description:	Logically OR 2 numbers
//
//		a	b	result
//		-- -- ------
//		0	0	0
//		0	1	1
//		1	0	1
//		1	1	1
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

unsignedint lui_bit
int			li_decimal

li_decimal = 0

for lui_bit = 1 to 16
	if this.of_GetBit(ai_decimala, lui_bit) or this.of_GetBit(ai_decimalb, lui_bit) then
		li_decimal = this.of_SetBit(li_decimal, lui_bit)
	end if
next

return li_decimal
end function

public function longlong of_setbit (longlong al_decimal, unsignedlong aui_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_SetBit
//
// Access:	public
//
//	Arguments:
//		al_decimal		value
//		aui_bit			bit number to set
//
//	Returns:  new value
//
//	Description:	Sets a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

if not this.of_GetBit(al_decimal, aui_bit) then
	return al_decimal + (2 ^ (aui_bit - 1))
end if

return al_decimal
end function

public function longlong of_clearbit (longlong al_decimal, unsignedlong aui_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_ClearBit
//
// Access:	public
//
//	Arguments:
//		al_decimal		value
//		aui_bit			bit number to clear
//
//	Returns:  new value
//
//	Description:	Clears a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

if of_GetBit(al_decimal, aui_bit) then
	return al_decimal - (2 ^ (aui_bit - 1))
end if

return al_decimal
end function

public function longlong of_flipbit (longlong al_decimal, unsignedlong aui_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_FlipBit
//
// Access:	public
//
//	Arguments:
//		al_decimal		value
//		aui_bit			bit number to flip
//
//	Returns:  new value
//
//	Description:	Flips a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

if not this.of_GetBit(al_decimal, aui_bit) then
	return this.of_SetBit(al_decimal, aui_bit)
else
	return this.of_ClearBit(al_decimal, aui_bit)
end if
end function

public function longlong of_bitwisenot (longlong ai_decimal);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_BitwiseNOT
//
// Access:	public
//
//	Arguments:
//		ai_decimal		value to not
//
//	Returns:	none
//
//	Description:	Logically NOT all bits in a number
//
//		a	result
//		-- ------
//		0	1
//		1	0
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

unsignedint lui_bit
int			li_decimal

li_decimal = 0

for lui_bit = 1 to 16
	if not this.of_GetBit(ai_decimal, lui_bit) then
		li_decimal = this.of_SetBit(li_decimal, lui_bit)
	end if
next

return li_decimal
end function

public function longlong of_bitwisexor (longlong ai_decimala, longlong ai_decimalb);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_BitwiseXOR
//
// Access:	public
//
//	Arguments:
//		ai_decimala
//		ai_decimalb
//
//	Returns:  int
//
//	Description:	Logically XOR 2 numbers
//
//		a	b	result
//		-- -- ------
//		0	0	0
//		0	1	1
//		1	0	1
//		1	1	0
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

unsignedint lui_bit
int			li_decimal

li_decimal = 0

for lui_bit = 1 to 16
	if this.of_GetBit(ai_decimala, lui_bit) <> this.of_GetBit(ai_decimalb, lui_bit) then
		li_decimal = this.of_SetBit(li_decimal, lui_bit)
	end if
next

return li_decimal
end function

public function int of_setbit (int ai_decimal, unsignedlong aui_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_SetBit
//
// Access:	public
//
//	Arguments:
//		ai_decimal		value
//		aui_bit			bit number to set
//
//	Returns:  new value
//
//	Description:	Sets a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

if not this.of_GetBit(ai_decimal, aui_bit) then
	return ai_decimal + (2 ^ (aui_bit - 1))
end if

return ai_decimal
end function

public function int of_clearbit (int ai_decimal, unsignedlong aui_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_ClearBit
//
// Access:	public
//
//	Arguments:
//		ai_decimal		value
//		aui_bit			bit number to clear
//
//	Returns:  new value
//
//	Description:	Clears a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

if of_GetBit(ai_decimal, aui_bit) then
	return ai_decimal - (2 ^ (aui_bit - 1))
end if

return ai_decimal
end function

public function int of_flipbit (int ai_decimal, unsignedlong aui_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_FlipBit
//
// Access:	public
//
//	Arguments:
//		ai_decimal		value
//		aui_bit			bit number to flip
//
//	Returns:  new value
//
//	Description:	Flips a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

if not this.of_GetBit(ai_decimal, aui_bit) then
	return this.of_SetBit(ai_decimal, aui_bit)
else
	return this.of_ClearBit(ai_decimal, aui_bit)
end if
end function

on n_bit.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_bit.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

