HA$PBExportHeader$w_mail_print.srw
$PBExportComments$v10.2.1.2 maint 3352: the message box for writing a brief message does not wrap.  also, if the user attempts to use the "Enter" key, it closes the window because both the "Cancel" and "Send" buttons are set as default buttons
forward
global type w_mail_print from window
end type
type cbx_cc from checkbox within w_mail_print
end type
type rb_fix from radiobutton within w_mail_print
end type
type rb_print from radiobutton within w_mail_print
end type
type st_3 from statictext within w_mail_print
end type
type mle_msg from multilineedit within w_mail_print
end type
type cb_cancel from commandbutton within w_mail_print
end type
type cb_send from commandbutton within w_mail_print
end type
type st_2 from statictext within w_mail_print
end type
type st_1 from statictext within w_mail_print
end type
type sle_fromuser from singlelineedit within w_mail_print
end type
type rb_pdf from radiobutton within w_mail_print
end type
type rb_mdi_format from radiobutton within w_mail_print
end type
type lb_user_list from listbox within w_mail_print
end type
type mle_to_users from multilineedit within w_mail_print
end type
type gb_1 from groupbox within w_mail_print
end type
end forward

global type w_mail_print from window
integer x = 1074
integer y = 476
integer width = 3342
integer height = 1136
string title = ""
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
long backcolor = 67108864
cbx_cc cbx_cc
rb_fix rb_fix
rb_print rb_print
st_3 st_3
mle_msg mle_msg
cb_cancel cb_cancel
cb_send cb_send
st_2 st_2
st_1 st_1
sle_fromuser sle_fromuser
rb_pdf rb_pdf
rb_mdi_format rb_mdi_format
lb_user_list lb_user_list
mle_to_users mle_to_users
gb_1 gb_1
end type
global w_mail_print w_mail_print

type variables
s_mail_print mail_arg
longlong TEXT_TYPE = 1
longlong PDF_TYPE = 3
longlong PRN_TYPE = 6
longlong FIX_TYPE = 5
longlong LANDSCAPE = 1
longlong PORTRAIT = 0
longlong POST_TYPE =4
longlong MDI_TYPE =7
end variables

on w_mail_print.create
int iCurrent
call super::create
this.cbx_cc=create cbx_cc
this.rb_fix=create rb_fix
this.rb_print=create rb_print
this.st_3=create st_3
this.mle_msg=create mle_msg
this.cb_cancel=create cb_cancel
this.cb_send=create cb_send
this.st_2=create st_2
this.st_1=create st_1
this.sle_fromuser=create sle_fromuser
this.rb_pdf=create rb_pdf
this.rb_mdi_format=create rb_mdi_format
this.lb_user_list=create lb_user_list
this.mle_to_users=create mle_to_users
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_cc
this.Control[iCurrent+2]=this.rb_fix
this.Control[iCurrent+3]=this.rb_print
this.Control[iCurrent+4]=this.st_3
this.Control[iCurrent+5]=this.mle_msg
this.Control[iCurrent+6]=this.cb_cancel
this.Control[iCurrent+7]=this.cb_send
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.sle_fromuser
this.Control[iCurrent+11]=this.rb_pdf
this.Control[iCurrent+12]=this.rb_mdi_format
this.Control[iCurrent+13]=this.lb_user_list
this.Control[iCurrent+14]=this.mle_to_users
this.Control[iCurrent+15]=this.gb_1
end on

on w_mail_print.destroy
call super::destroy
destroy(this.cbx_cc)
destroy(this.rb_fix)
destroy(this.rb_print)
destroy(this.st_3)
destroy(this.mle_msg)
destroy(this.cb_cancel)
destroy(this.cb_send)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.sle_fromuser)
destroy(this.rb_pdf)
destroy(this.rb_mdi_format)
destroy(this.lb_user_list)
destroy(this.mle_to_users)
destroy(this.gb_1)
end on

event open;call super::open;
string name, last_name, mail_id
mail_arg = message.powerobjectparm

sle_fromuser.text = mail_arg.from_user
mle_msg.text = mail_arg.msg
//declare mail_cur cursor for select mail_id from pp_security_users;
declare mail_cur cursor for select last_name||', '||first_name, last_name, mail_id 
									 from pp_security_users where mail_id is not null and
 users in (select users from pp_company_security ppsec, company where ppsec.company_id = company.company_id);



open mail_cur;

fetch mail_cur into :name, :last_name, :mail_id;
do while sqlca.sqlcode = 0
	if isnull(last_name) then name = mail_id
	lb_user_list.additem(name)
	
	fetch mail_cur into :name, :last_name, :mail_id;
loop
close mail_cur;

declare group_cur cursor for select groups from pp_security_groups;

open group_cur;

fetch group_cur into :name;
do while sqlca.sqlcode = 0

	lb_user_list.additem(name)
	fetch group_cur into :name;
loop
close group_cur;
end event

event close;call super::close;closewithreturn(this, mail_arg)
end event

type cbx_cc from checkbox within w_mail_print
integer x = 2208
integer y = 924
integer width = 741
integer height = 64
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Send a Copy to Sender"
end type

type rb_fix from radiobutton within w_mail_print
integer x = 91
integer y = 332
integer width = 434
integer height = 96
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "Fix Format"
boolean lefttext = true
end type

type rb_print from radiobutton within w_mail_print
integer x = 91
integer y = 212
integer width = 434
integer height = 96
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "Print Text"
boolean lefttext = true
end type

type st_3 from statictext within w_mail_print
integer x = 1093
integer y = 132
integer width = 613
integer height = 76
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "Message"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type mle_msg from multilineedit within w_mail_print
integer x = 718
integer y = 224
integer width = 1367
integer height = 668
integer taborder = 60
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 16777215
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_cancel from commandbutton within w_mail_print
integer x = 41
integer y = 912
integer width = 293
integer height = 108
integer taborder = 30
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Cancel"
end type

event clicked;mail_arg.print_type = -1
closewithreturn(parent,mail_arg)
end event

type cb_send from commandbutton within w_mail_print
integer x = 3017
integer y = 932
integer width = 293
integer height = 108
integer taborder = 40
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Send"
end type

event clicked;string user_name, mail_id, last_name, first_name
longlong	ck

//user_name = ddlb_to_user.text
//
//setnull(mail_id)
//ck = pos(user_name, ', ')
//if ck > 1 then 
//	last_name = mid(user_name, 1, ck - 1)
//	first_name = mid(user_name, ck + 2, 35)
//	
//	select mail_id into :mail_id
//	from pp_security_users
//	where last_name = :last_name
//	and	first_name = :first_name;
//end if
mail_id = mle_to_users.text
if isnull(mail_id) or mail_id = "" then mail_id = user_name

mail_arg.to_user = mail_id
//mail_arg.to_user = ddlb_to_user.text
mail_arg.from_user = sle_fromuser.text
mail_arg.msg = mle_msg.text
if rb_pdf.checked = true then
	mail_arg.print_type =  PDF_TYPE
elseif rb_fix.checked = true then
	mail_arg.print_type = fix_type
elseif rb_mdi_format.checked = true then
	mail_arg.print_type = mdi_type
else // print
	mail_arg.print_type = prn_type
end if

if cbx_cc.checked then
	mail_arg.copy_sender = true
else
	mail_arg.copy_sender = false
end if

closewithreturn(parent,mail_arg)
end event

type st_2 from statictext within w_mail_print
integer x = 1861
integer y = 28
integer width = 274
integer height = 76
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "To User:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_mail_print
integer x = 658
integer y = 40
integer width = 297
integer height = 76
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "From User:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_fromuser from singlelineedit within w_mail_print
integer x = 978
integer y = 24
integer width = 814
integer height = 84
integer taborder = 10
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 16777215
boolean enabled = false
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type rb_pdf from radiobutton within w_mail_print
integer x = 91
integer y = 92
integer width = 434
integer height = 96
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "PDF"
boolean checked = true
boolean lefttext = true
end type

type rb_mdi_format from radiobutton within w_mail_print
integer x = 91
integer y = 460
integer width = 434
integer height = 96
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "MDI Format"
boolean lefttext = true
end type

type lb_user_list from listbox within w_mail_print
integer x = 2213
integer y = 224
integer width = 1070
integer height = 668
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 16777215
boolean hscrollbar = true
boolean vscrollbar = true
boolean multiselect = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;longlong i,count,num,ck
string user,mail_user
string users[]
string last_name,first_name
count = 0
mle_to_users.text = ''
for i = 1 to this.totalitems()
	
	if this.state(i) = 1 then
	   user = this.text(i)
		if pos(user,'@',1) >  0 then
			count++
			users[count] = user
		else
			ck = pos(user, ', ')
			if ck > 1 then 
				last_name = mid(user, 1, ck - 1)
				first_name = mid(user, ck + 2, 35)			
				select mail_id into :mail_user
				from pp_security_users
				where last_name = :last_name
				and	first_name = :first_name;
				if sqlca.sqlcode = 0 then
					if isnull(mail_user) = false then
						count++
						users[count] = mail_user
					end if
				end if
				continue
         end if 
			select count(*) into :num from pp_security_groups where groups = lower(:user);
			if num > 0 then
				declare user_cur cursor for  select u.mail_id from pp_security_users u ,pp_security_users_groups g
								 where  rtrim(u.users) = rtrim(g.users) and g.groups = lower(:user);
				open user_cur;
				fetch user_cur into :mail_user;
				do while  sqlca.sqlcode = 0 
					if isnull(mail_user) = false then
						count++
						users[count] = mail_user
					end if
					fetch user_cur into :mail_user;
				loop
				close user_cur;
			end if
		end if
	end if
next
if count > 1 then
	for i = 1 to count - 1
		mle_to_users.text = mle_to_users.text + users[i] + ';'
	next 
	mle_to_users.text = mle_to_users.text + users[count] 
elseif count = 1 then 
	
	mle_to_users.text =  users[1] 
end if
end event

type mle_to_users from multilineedit within w_mail_print
integer x = 2213
integer y = 24
integer width = 1065
integer height = 184
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 16777215
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within w_mail_print
integer x = 18
integer y = 8
integer width = 581
integer height = 588
integer taborder = 50
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Send As"
end type

