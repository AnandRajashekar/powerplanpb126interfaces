HA$PBExportHeader$nvo_server_side_request.sru
forward
global type nvo_server_side_request from nonvisualobject
end type
end forward

global type nvo_server_side_request from nonvisualobject
end type
global nvo_server_side_request nvo_server_side_request

type variables
nvo_parm_translate i_parm_translate
end variables

forward prototypes
public function longlong uf_start_job (longlong a_process_identifier, ref string a_process, ref s_ppbase_parm_arrays a_values, ref s_ppbase_parm_arrays_labels a_labels)
public function longlong uf_request_job (string a_process, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels)
public function longlong uf_complete_job (longlong a_process_identifier, string a_return_code)
public function longlong uf_complete_job (longlong a_process_identifier)
public function longlong uf_request_job (string a_process, string a_process_descr, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels, boolean a_synchronous, boolean a_notify_on_complete)
public function longlong uf_request_job (string a_process, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels, boolean a_synchronous, boolean a_notify_on_complete)
public function longlong uf_get_job_attribs (longlong a_process_identifier, ref string a_process, ref string a_process_descr, ref datetime a_request_time, ref datetime a_start_time, ref datetime a_end_time, ref string a_return_code, ref string a_status, ref longlong a_notify_on_complete)
public function longlong uf_request_job (string a_process, string a_process_descr, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels, boolean a_synchronous, boolean a_notify_on_complete, boolean a_launch_local, boolean a_launch_cpm)
public function longlong uf_request_job (string a_process)
public subroutine uf_start_cpm (longlong a_process_identifier)
public subroutine uf_start_cpm (longlong a_process_id, longlong a_occurrence_id)
private function string uf_build_connection_string (longlong a_process_identifier)
public function longlong uf_complete_job (longlong a_process_identifier, string a_return_code, longlong a_os_return_value)
end prototypes

public function longlong uf_start_job (longlong a_process_identifier, ref string a_process, ref s_ppbase_parm_arrays a_values, ref s_ppbase_parm_arrays_labels a_labels);// **********************************************************
//		Mark the job as in progress along with the session id and occurrence id
//		 Lookup and return the process, values, and labels for a process.
//
//		If the process is already in progress or complete then error.
//
//		Return 0 on sucess (reference variables for other information)
//		Return -1 on error
// **********************************************************
string status
longlong counter, l_occ_id, l_proc_id, ll_pid, l_synch, l_stale_request
s_ppbase_parm_arrays null_parm_arrays
s_ppbase_parm_arrays_labels null_parm_labels_array
blob arguments

// **********************************************************
// Null out any values they passed in
// **********************************************************
setnull(a_process)
a_values = null_parm_arrays
a_labels = null_parm_labels_array

// **********************************************************
// Mark the job as in progress along with the session id and occurrence id
// **********************************************************
ll_pid = uo_winapi.uf_GetCurrentProcessId()
// ### 40354: JAK: 2014-10-16: Add "request_time >= (sysdate - .25)" so that jobs older than 6 hours old are not processed.
update PP_JOB_REQUEST set
	status = 'I',
	process_id = :g_process_id,
	occurrence_id = :g_occurrence_id, 
	start_time = nvl(start_time,sysdate),
	windows_process_id = :ll_pid
where process_identifier = :a_process_identifier
and status in ('L','N')
and request_time >= (sysdate - .25); 

if sqlca.SQLCode <> 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Updating PP_JOB_REQUEST to 'I': " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	return -1
elseif sqlca.SQLNRows = 0 then
	// Didn't find a row.  Could be the record doesn't exist at all, or that the status is not 'N'.
	select count(*), min(status), min(occurrence_id), min(process_id),
		min(case when request_time >= (sysdate - .25) then 0 else 1 end)
		into :counter, :status, :l_occ_id, :l_proc_id, :l_stale_request
	from PP_JOB_REQUEST
	where process_identifier = :a_process_identifier;
	
	if counter > 0 then
		// Record already in PP_JOB_REQUEST.  Is it complete already?
		if status = 'D' then
			if isnull(l_proc_id) then l_proc_id = 0
			if isnull(l_occ_id) then l_occ_id = 0
			
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: Request in PP_JOB_REQUEST has already been processed:  Status=" + status + &
				"; Process ID=" + string(l_proc_id) + &
				"; Occurrence ID=" + string(l_occ_id) + &
				".")
			f_pp_msgs(" ")
		elseif status = 'I' then
			if isnull(l_proc_id) then l_proc_id = 0
			if isnull(l_occ_id) then l_occ_id = 0
			
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: Request in PP_JOB_REQUEST is currently being processed:  Status=" + status + &
				"; Process ID=" + string(l_proc_id) + &
				"; Occurrence ID=" + string(l_occ_id) + &
				".")
			f_pp_msgs(" ")
		elseif l_stale_request = 1 then
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: Request in PP_JOB_REQUEST is too old to be processed.  Please re-submit the request.")
			f_pp_msgs(" ")
			
			update PP_JOB_REQUEST set
				status = 'D',
				start_time = sysdate, end_time = sysdate,
				return_code = 'Request in PP_JOB_REQUEST is too old to be processed'
			where process_identifier = :a_process_identifier
			and status in ('L','N');
		else
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: Generic error.  Please review job request logging for more information")
			f_pp_msgs("   Status of job request is " + status)
			f_pp_msgs(" ")
		end if
	elseif counter = 0 then
		// Record does not exist in PP_JOB_REQUEST.
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Request in PP_JOB_REQUEST does not exist for ID=" + string(a_process_identifier))
		f_pp_msgs(" ")
	end if
	
	return -1
end if;

// **********************************************************
//	 Lookup and return the process, values, and labels for a process.
// **********************************************************
selectblob arguments into :arguments
	from PP_JOB_REQUEST where process_identifier = :a_process_identifier;

if sqlca.SQLCode <> 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Selecting arguments from PP_JOB_REQUEST: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	return -1		
end if

select process, synchronous into :a_process, :l_synch
	from PP_JOB_REQUEST where process_identifier = :a_process_identifier;

if sqlca.SQLCode <> 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Selecting process from PP_JOB_REQUEST: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	return -1		
end if

if l_synch = 1 then
	update pp_processes_occurrences set suppress_indicator = 1 where process_id = :g_process_id and occurrence_id = :g_occurrence_id using g_sqlca_logs;
else
	update pp_processes_occurrences set suppress_indicator = 0 where process_id = :g_process_id and occurrence_id = :g_occurrence_id using g_sqlca_logs;
end if

// **********************************************************
// Convert arguments to arrays for the process.
// **********************************************************
i_parm_translate.of_string_to_structs(string(arguments), a_values, a_labels)

return 0
end function

public function longlong uf_request_job (string a_process, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels);// Overload without the synchronous option
return uf_request_job(a_process, a_process, a_values, a_labels, false, true)
end function

public function longlong uf_complete_job (longlong a_process_identifier, string a_return_code);longlong my_null_long
setnull(my_null_long)

// Overloaded function without os_return_value
return uf_complete_job(a_process_identifier,a_return_code,my_null_long)
end function

public function longlong uf_complete_job (longlong a_process_identifier);longlong my_null_long
setnull(my_null_long)

// Overloaded function without return code or os_return_value
return uf_complete_job(a_process_identifier,'',my_null_long)
end function

public function longlong uf_request_job (string a_process, string a_process_descr, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels, boolean a_synchronous, boolean a_notify_on_complete);string ini_file, LaunchLocal, LaunchCPM
boolean bool_LaunchLocal, bool_LaunchCPM

ini_file = f_get_ini_file() 
bool_LaunchLocal = true
// ### 34536: JAK: 2013-12-04:  Change the default such that launchlocal is yes.  This should remain in place until 10.5.
LaunchLocal = lower(ProfileString(ini_file, "SSP", "LaunchLocal", "yes"))
if trim(LaunchLocal) = 'no' then bool_LaunchLocal = false

bool_LaunchCPM = true
LaunchCPM = lower(ProfileString(ini_file, "SSP", "LaunchCPM", "yes"))
if isnull(LaunchCPM) or trim(LaunchCPM) <> 'yes' then bool_LaunchCPM = false

return uf_request_job(a_process, a_process_descr, a_values, a_labels, a_synchronous, a_notify_on_complete, bool_LaunchLocal, bool_LaunchCPM)
end function

public function longlong uf_request_job (string a_process, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels, boolean a_synchronous, boolean a_notify_on_complete);// Overload without the synchronous option
return uf_request_job(a_process, a_process, a_values, a_labels, a_synchronous, a_notify_on_complete)
end function

public function longlong uf_get_job_attribs (longlong a_process_identifier, ref string a_process, ref string a_process_descr, ref datetime a_request_time, ref datetime a_start_time, ref datetime a_end_time, ref string a_return_code, ref string a_status, ref longlong a_notify_on_complete);select request_time, process, process_descr, start_time, end_time, return_code, status, notify_on_complete
	into :a_request_time, :a_process, :a_process_descr, :a_start_time, :a_end_time, :a_return_code, :a_status, :a_notify_on_complete
	from pp_job_request
	where process_identifier = :a_process_identifier;

if sqlca.sqlcode < 0 then
	return -1
else 
	return 0
end if
	
end function

public function longlong uf_request_job (string a_process, string a_process_descr, s_ppbase_parm_arrays a_values, s_ppbase_parm_arrays_labels a_labels, boolean a_synchronous, boolean a_notify_on_complete, boolean a_launch_local, boolean a_launch_cpm);longlong process_identifier, process_id, occurrence_id, timeout, i, orig_timeout, notify_on_complete, synchronous
string user_id, status, file_name, arg
blob arguments

// **********************************************************
// Do we run locally or remote?
// **********************************************************

if a_launch_local then
	// Get the actual file name so we can validate it exists below
	if pos(a_process,' ') > 0 then  // space
		file_name = mid(a_process,1,pos(a_process,' ') - 1)
	else
		file_name = a_process
	end if
	
	if pos(file_name,'	') > 0 then  // tab
		file_name = mid(file_name,1,pos(file_name,'	') - 1)
	else
		file_name = file_name
	end if
	
	// Make sure the file exists
	if not fileexists(g_exe_info.file_path + file_name) then
		if g_main_application then 
			messagebox("Server Side Request","ERROR: Running Locally.  Unable to find '" + g_exe_info.file_path + file_name + "'.", StopSign!)
		else
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: Running Locally.  Unable to find '" + g_exe_info.file_path + file_name + "'.")
			f_pp_msgs(" ")
		end if
		return -1
	end if
end if


// **********************************************************
// Get Request ID
// **********************************************************
select pp_job_request_seq.nextval into :process_identifier from dual;

if sqlca.SQLCode <> 0 then
	if g_main_application then 
		messagebox("Server Side Request","ERROR: Retrieving an identifier for the job request: " + sqlca.sqlerrtext, StopSign!)
	else
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Retrieving an identifier for the job request: " + sqlca.sqlerrtext)
		f_pp_msgs(" ")
	end if
	
	return -1
end if

// **********************************************************
// Get Other Values for Job Request Table
// **********************************************************
user_id = sqlca.logid
status = 'N'
if a_launch_local then status = 'L'  // Keeps the monitor from picking up the job.
setnull(process_id)
setnull(occurrence_id)
notify_on_complete = 0
synchronous = 0
if a_notify_on_complete then notify_on_complete = 1
if a_synchronous then 
	synchronous = 1
	notify_on_complete = 0
end if
a_process += ' /ssp=' + user_id + ':' + string(process_identifier) + ' /connection=' + uf_build_connection_string(process_identifier)

// **********************************************************
// Convert arguments to CLOB for storage in PP_JOB_REQUEST table.
// **********************************************************
arguments = blob(i_parm_translate.of_structs_to_string(a_values, a_labels))

// **********************************************************
// Populate PP_JOB_REQUEST
// **********************************************************
insert into PP_JOB_REQUEST
	(process_identifier, requesting_user, process, arguments, process_descr, status, request_time, synchronous, notify_on_complete)
	values
	(:process_identifier, :user_id, :a_process, 'updated below but not nullable', nvl(trim(:a_process_descr), :a_process), :status, sysdate, :synchronous, :notify_on_complete);
	
if sqlca.SQLCode <> 0 then
	if g_main_application then 
		messagebox("Server Side Request","ERROR: Inserting into PP_JOB_REQUEST: " + sqlca.sqlerrtext, StopSign!)
	else
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Inserting into PP_JOB_REQUEST: " + sqlca.sqlerrtext)
		f_pp_msgs(" ")
	end if
	rollback;
	return -1
end if

// Will have to do an updateblob for the arguments field since it could get big.
updateblob PP_JOB_REQUEST 
		set arguments = :arguments
		where process_identifier = :process_identifier;
	
if sqlca.SQLCode <> 0 then
	if g_main_application then 
		messagebox("Server Side Request","ERROR: Saving arguments to PP_JOB_REQUEST: " + sqlca.sqlerrtext, StopSign!)
	else
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Saving arguments to PP_JOB_REQUEST: " + sqlca.sqlerrtext)
		f_pp_msgs(" ")
	end if
	rollback;
	return -1
end if
	

// **********************************************************
// Make Server Request
// **********************************************************
commit;
if a_launch_local then
	// Need to run locally
	// Make sure the file exists locally.
	
	// Validated the file exists above.
	run(g_exe_info.file_path + a_process)
else
	// Job Router will handle it
end if

// **********************************************************
// Start CPM with Request ID -- assume cpm.exe is in the same directory as app
// **********************************************************

// Start the process monitor. (used to be only for asynchronous processes, now we're using it for synchronous as well.)
if a_launch_cpm then
	uf_start_cpm(process_identifier)
end if
	
	if a_synchronous then
	
	// wait for the process to complete.  
	timeout = 60
	orig_timeout = timeout
	
	for i = 1 to timeout
		sleep(1)
		
		select status
			into :status
			from PP_JOB_REQUEST
			where process_identifier = :process_identifier;
		
		if status = 'N' then
			// Job has been requested but not started yet.
			continue
		elseif status = 'I' then
			// Job is in process.  Extend the timeout
			timeout = orig_timeout * 20 //SRM 33790 - I'm extending this as a safety measure now that the client process monitor gives the user the option to kill synchronous process. 
			continue
		elseif status = 'D' then
			// Job is complete.  Can return to the user.
			exit
		end if
	next 
end if

// **********************************************************
// Return Request ID
// **********************************************************
return process_identifier
end function

public function longlong uf_request_job (string a_process);s_ppbase_parm_arrays values_null
s_ppbase_parm_arrays_labels labels_null

// Overload without the synchronous option
return uf_request_job(a_process, a_process, values_null, labels_null, false, true)
end function

public subroutine uf_start_cpm (longlong a_process_identifier);string arg

// Start the process monitor. (used to be only for asynchronous processes, now we're using it for synchronous as well.)
if fileexists(g_exe_info.file_path + 'cpm.exe') then
	// Build the command to launch...
	//	cpm -s sqlca.servername -dbms sqlca.dbms -uid sqlca.logid -pass sqlca.logpass -ssp process_identifier
	arg = 'cpm.exe /connection=' + uf_build_connection_string(a_process_identifier) + ' /ssp=' + string(a_process_identifier)
	run(g_exe_info.file_path + arg)
end if
	
end subroutine

public subroutine uf_start_cpm (longlong a_process_id, longlong a_occurrence_id);string arg

// Start the process monitor. (used to be only for asynchronous processes, now we're using it for synchronous as well.)
if fileexists(g_exe_info.file_path + 'cpm.exe') then
	// Build the command to launch...
	//	cpm -s sqlca.servername -dbms sqlca.dbms -uid sqlca.logid -pass sqlca.logpass -ssp process_identifier
	arg = 'cpm.exe /connection=' + uf_build_connection_string(0) + ' /pid=' + string(a_process_id) + ' /oid=' + string(a_occurrence_id)
	run(g_exe_info.file_path + arg)
end if
	
end subroutine

private function string uf_build_connection_string (longlong a_process_identifier);string connection, dbms, msg, parms[], temp
longlong i, a, b

// **********************************************************
// 40102:  Build the connection string.
// **********************************************************
dbms = sqlca.DBMS

if pos(dbms,' ') > 0 then dbms = trim(mid(dbms,pos(dbms,' ')))

randomize(0)
parms[1] = 'LID=' + sqlca.LogID
parms[2] = 'LP=' + sqlca.LogPass
parms[3] = 'UID=' + sqlca.UserID
parms[4] = 'DBMS=' + dbms
parms[5] = 'SN=' + sqlca.Servername
parms[6] = 'SSPID=' + string(a_process_identifier)
parms[7] = 'RAND=' + string(rand(32767))

// Shuffle parameters into random order (swaps 2 random items 100 times)
for i = 1 to 100
	a = rand(upperbound(parms))
	b = rand(upperbound(parms))
	temp = parms[a]
	parms[a] = parms[b]
	parms[b] = temp
next

// Combine parms into one string
for i = 1 to upperbound(parms)
	connection += parms[i]
	if i < upperbound(parms) then
		connection += ";_"
	end if
next

// Encrypt the conection string
nvo_web_crypto web_crypto
web_crypto = create nvo_web_crypto

try
	connection = web_crypto.uf_rc2_encrypt(connection, 'b@n@n@password$plit')
catch (runTimeError rte)
	msg = rte.getMessage()
	
	if g_main_application then 
		messagebox("Server Side Request","ERROR: " + msg, StopSign!)
	else
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: " + msg)
		f_pp_msgs(" ")
	end if
end try

return connection
end function

public function longlong uf_complete_job (longlong a_process_identifier, string a_return_code, longlong a_os_return_value);// **********************************************************
//		Mark the job as complete along with an end time.
//
//		If the process is not in progress then error.
//
//		Return 0 on sucess (reference variables for other information)
//		Return -1 on error
// **********************************************************
string status
longlong counter, l_occ_id, l_proc_id


// **********************************************************
// Mark the job as in progress along with the session id and occurrence id
// **********************************************************
update PP_JOB_REQUEST set
	status = 'D',
	end_time = sysdate,
	return_code = substr(:a_return_code,1,255),
	os_return_value = :a_os_return_value
where process_identifier = :a_process_identifier
and status = 'I';

if sqlca.SQLCode <> 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Updating PP_JOB_REQUEST: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	return -1
elseif sqlca.SQLNRows = 0 then
	// Didn't find a row.  Could be the record doesn't exist at all, or that the status is not 'N'.
	select count(*), min(status), min(occurrence_id), min(process_id) into :counter, :status, :l_occ_id, :l_proc_id
	from PP_JOB_REQUEST
	where process_identifier = :a_process_identifier;
	
	if counter > 0 then
		// Record already in PP_JOB_REQUEST.
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Record in PP_JOB_REQUEST is not currently in process:  Status=" + status + &
			"; Process ID=" + string(l_proc_id) + &
			"; Occurrence ID=" + string(l_occ_id) + &
			".")
		f_pp_msgs(" ")
		return -1
	elseif counter = 0 then
		// Record does not exist in PP_JOB_REQUEST.
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Record in PP_JOB_REQUEST does not exist for process identifier " + string(a_process_identifier))
		f_pp_msgs(" ")
		return -1
	end if
end if;

return 0
end function

on nvo_server_side_request.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_server_side_request.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;i_parm_translate = create nvo_parm_translate
end event

