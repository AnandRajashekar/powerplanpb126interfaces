HA$PBExportHeader$uo_sqlca.sru
$PBExportComments$7986-Budget Reporting Construct
forward
global type uo_sqlca from transaction
end type
end forward

global type uo_sqlca from transaction
string dbparm = "CommitOnDisconnect=~'No~'"
end type
global uo_sqlca uo_sqlca

type prototypes

// Alerts Maint-xxxxx rkr
function longlong ALERT_REGISTER( string alert ) RPCFUNC ALIAS FOR "pp_alert_process.register"
function longlong ALERT_WAITONE(string alert , ref   string msg,ref longlong status  ,longlong timeout)  RPCFUNC ALIAS FOR "pp_alert_process.waitone"
function longlong ALERT_WAITANY(ref string alert ,ref string msg, ref longlong status , longlong timeout) RPCFUNC ALIAS FOR "pp_alert_process.waitany"
function longlong ALERT_SIGNAL( string  alert, string msg) RPCFUNC ALIAS FOR "pp_alert_process.signal"
function longlong ALERT_REMOVE( string alert) RPCFUNC ALIAS FOR "pp_alert_process.remove"
function longlong ALERT_REMOVEALL() RPCFUNC ALIAS FOR "pp_alert_process.removeall";
function longlong ALERT_SET_DEFAULTS(longlong sensitivity ) RPCFUNC ALIAS FOR "pp_alert_process.set_defaults";

subroutine truncate_table( string name) RPCFUNC ALIAS FOR "truncate_table"
function  longlong analyze_table(string name ) RPCFUNC ALIAS FOR "analyze_table"
function  longlong analyze_table_pct(string name,longlong pct ) RPCFUNC ALIAS FOR "analyze_table"
function longlong alter_table(string name,string cmd, string sql ) RPCFUNC ALIAS FOR "alter_table"
FUNCTION double fopen(string path,string filename,string mode) RPCFUNC ALIAS FOR "utl_file.fopen"
SUBROUTINE fclose(longlong unitx) RPCFUNC ALIAS FOR "utl_file.fclose"
SUBROUTINE  set_sql_trace_in_session(ref double sid, ref double serial,ref int state)  RPCFUNC ALIAS FOR "sys.dbms_system.set_sql_trace_in_session"
subroutine set_sqltrace_on(int sed,int serail)  RPCFUNC ALIAS FOR "system.set_sqltrace_on"
subroutine describe_procedure (string object_name ,string reserved1 , string reserved2 ,ref double overload[] ,ref double position[],ref double level[],ref string argument_name[] ,ref double datatype[],ref double default_value[],ref double n_out[],ref double length_out[],ref double precision[],ref double scale[],ref double radixx[],ref double spare[]) RPCFUNC ALIAS FOR "sys.dbms_describe.describe_procedure"
subroutine open_file(string path,string file,string mode,ref double unit) RPCFUNC ALIAS FOR "pp_file.open_file"
SUBROUTINE read_file(double unit, ref string buf)  RPCFUNC ALIAS FOR "pp_file.read_file"
SUBROUTINE close_file(double unit)  RPCFUNC ALIAS FOR "pp_file.close_file"
subroutine pack_message(string msg) rpcfunc alias for "dbms_pipe.pack_message"
subroutine pack_message(longlong msg) rpcfunc alias for "dbms_pipe.pack_message"
function longlong send_message(string msg,longlong timeout) rpcfunc alias for "dbms_pipe.send_message"
function string unique_session_name() rpcfunc alias for "dbms_pipe.unique_session_name"
function longlong receive_message(string msg,longlong timeout) rpcfunc alias for "dbms_pipe.receive_message"
subroutine unpack_message(ref string msg) rpcfunc alias for "dbms_pipe.unpack_message"
subroutine unpack_message(ref longlong status) rpcfunc alias for "dbms_pipe.unpack_message"
function longlong submit_tax( longlong version,ref longlong code,ref string err_msg) rpcfunc alias for "submit_tax"
subroutine submit(ref longlong jobid,string what,datetime startdate,string interval) rpcfunc alias for "dbms_job.submit"
subroutine remove( longlong jobid) rpcfunc alias for "dbms_job.remove"
subroutine user_export(longlong jobid,ref string what) rpcfunc alias for  "dbms_job.user_export"
subroutine run(longlong jobid) rpcfunc alias for  "dbms_job.run"
function double get_tax_version( ) rpcfunc alias for "archive_tax.get_version"
function longlong get_tax_vparameter( ) rpcfunc alias for "archive_tax.get_vparameter"
function longlong get_plant_vparameter( ) rpcfunc alias for "archive_plant.get_vparameter"
function double get_plant_version( ) rpcfunc alias for "archive_plant.get_version"
subroutine set_context (string namespace2, string attribute ,string value ,string client,string client_id) rpcfunc alias for "dbms_session.set_context"
subroutine set_module(string mod,string action) rpcfunc alias for "DBMS_APPLICATION_INFO.SET_MODULE";
subroutine SET_CLIENT_INFO (string info)  rpcfunc alias for "DBMS_APPLICATION_INFO.SET_CLIENT_INFO";
function longlong pwrplant_admin(string cmd ,string user, ref string passwd ,string tablespace, string temp_tablespace,string user_role , string dev_role)   rpcfunc alias for "pwrplant_admin";
subroutine set_role(string role) rpcfunc alias for "dbms_session.set_role";
function longlong drop_tax_partition(string table,longlong vers) rpcfunc alias for "drop_tax_partition";
function longlong SetContext(string parm ,string val) rpcfunc alias for "audit_table_pkg.SetContext";
function longlong SetWindowContext(string winparm ,string windowtitle, string auditparm ) rpcfunc alias for "audit_table_pkg.SetWindowContext";
function longlong CreatePackage(string a_name ) rpcfunc alias for "audit_table_pkg.CreatePackage";
function string GetErrorText(longlong code ) rpcfunc alias for "audit_table_pkg.GetErrorText";
function longlong exec_sql(string sql,ref string msg) rpcfunc alias for 'archive_tax.exec_sql'
function string pp_sso_login(string keystr ) rpcfunc alias for 'pp_sso_login'
function longlong pp_send_mail(string a_fromuser,string a_smtp_userid, string a_smtp_passwd, &
          string a_subject,string a_text,string a_user,string a_server,string a_filename) rpcfunc alias for "PP_SEND_MAIL";
function string pp_get_error_msg(longlong code) rpcfunc alias for 'pp_get_error_msg';
subroutine put_line(string outstr) rpcfunc alias for "dbms_output.put_line"
subroutine get_line(ref string buf,ref int status ) rpcfunc alias for "dbms_output.get_line"
subroutine enable(longlong buflen) rpcfunc alias for "dbms_output.enable"
subroutine disable() rpcfunc alias for "dbms_output.disable"
function string pp_ldap_login(string a_user,string a_passwd,string a_host,longlong a_port ) rpcfunc alias for "pp_ldap.login"
function longlong pp_ldap_search(string a_session ,string a_class ,string attrib ,string a_ldap_path, longlong a_level ) rpcfunc alias for "pp_ldap.search"
function longlong pp_ldap_search_list(string a_session ,string a_class ,string a_ldap_path, longlong a_level ) rpcfunc alias for "pp_ldap.search_list"
function longlong pp_ldap_logout(string session)rpcfunc alias for "pp_ldap.logout"
function longlong pp_ldap_update_ad_user(string user,string pw,string server,string dn ,string server_dn ,longlong port) rpcfunc alias for "pp_ldap.update_ad_user"
function string verify_user(string a_verify_user ,string  a_verify_passwd ) rpcfunc alias for "pp_verify_user"
function string pp_ldap_getpasswd( ) rpcfunc alias for "pp_ldap.getpasswd"
function string pp_ldap_profile_passwd(string a_name ) rpcfunc alias for "pp_ldap.profile_passwd"
function  string pp_ldap_update_profile( string a_name , string a_user , string a_pw , string a_server , string a_dn , string a_server_dn ,longlong a_portr) rpcfunc alias for "pp_ldap.update_profile" 
function  longlong pp_ldap_update_user(string a_session ,string a_user ,string a_server_dn ) rpcfunc alias for "pp_ldap.update_user"
function  string pp_set_process2(blob a_arg, blob a_arg2) rpcfunc alias for "pp_process_pkg.pp_set_process"
function  string pp_release_process2(blob a_arg  ) rpcfunc alias for "pp_process_pkg.pp_release_process"
function  string pp_check_process2(blob a_arg  ) rpcfunc alias for "pp_process_pkg.pp_check_process"
function  string pp_check_process_prefix2(blob a_arg  ) rpcfunc alias for "pp_process_pkg.pp_check_process_prefix"
function  string pp_reset_disconnected(string a_arg, longlong a_company_id  ) rpcfunc alias for "pp_process_pkg.pp_reset_disconnected"

function longlong f_get_asset_activity_id(longlong a_asset_id) rpcfunc alias for "pp_cpr_pkg.f_get_asset_activity_id"
function longlong f_get_associated_ret_units(longlong a_ret_unit_id, longlong a_company_id) rpcfunc alias for "pp_cpr_pkg.f_get_associated_ret_units"

//10.3 for Provision - Begin
//###PATCH (403) Begin
function longlong f_fas109_calc(	longlong a_calc_beg_bal, longlong a_update, ref double a_m_id[], ref double a_ta_norm_id[], &
										ref double a_gl_month[], ref double a_accum_diff_beg[], ref double a_accum_diff_end[], ref double a_accum_reg_dit_beg[], &
										ref double a_accum_reg_dit_end[], ref double a_fas109_liab_beg[], ref double a_fas109_liab_end[], &
										ref double a_fas109_increment_beg[], ref double a_fas109_increment_end[], ref double a_total_grossup_beg[], &
										ref double a_total_grossup_end[], ref double a_entity_grossup_beg[], ref double a_entity_grossup_end[], &
										ref double a_fas109_liab_current[], ref double a_entity_grossup_current[],double a_reg_ind[], &
										double a_rate_increment[], double a_rate_entity_grossup[], double a_rate_total_grossup[], &
										double a_rate_total_increment[], double a_m_beg_bal[], double a_m_end_bal[], double a_m_activity[], &
										double a_dt_beg_bal[],double a_dt_end_bal[],double a_dt_activity[],double a_first_month_ind[], double a_ytd_include[], &
										double a_do_not_update[], double a_process_option_id[],double a_calc_type[],double a_ytd_m_activity[], &
										double a_total_dt_activity[],double a_ytd_total_dt_activity[],double a_total_dt_beg_bal[], &
										double a_total_dt_end_bal[], double a_ytd_dt_activity[], double a_fas109_liab_ytd[], double a_fas109_increment_ytd[], &
										double a_total_grossup_ytd[], double a_entity_grossup_ytd[],ref string a_msg &
									) rpcfunc alias for 'tax_accrual_deftax_pkg.f_fas109_calc'
function longlong f_fas109_roll(	decimal a_gl_month,ref string a_msg) rpcfunc alias for 'tax_accrual_deftax_pkg.f_fas109_roll';
function longlong f_fas109_calc(string a_fas109_sqls, decimal{2} a_gl_month, string a_insert_rtp_sqls, string a_update_rtp_sqls, ref string a_msg) rpcfunc alias for 'tax_accrual_deftax_pkg.f_fas109_calc'
function longlong f_deftax_calc(	longlong a_update, ref double a_mid[], ref double a_ta_norm_id[], &
										ref double a_gl_month[], ref double a_beg_balance[], ref double a_end_balance[], &
										ref double a_amount_est[], ref double amount_act[], ref double a_amount_calc[], &
										ref double a_amount_manual[], double a_m_beg_bal[], double a_m_activity[], double a_rate[], &
										double a_first_month_ind[], double a_ytd_include[], double a_do_not_update[], double a_process_option_ind[], &
										double a_ytd_m_activity[], double a_ytd_m_activity_prov[], double a_ytd_dt_activity[], &
										double a_ytd_dt_activity_prov[], double a_aram_ind[], ref string a_msg &
									) rpcfunc alias for 'tax_accrual_deftax_pkg.f_deftax_calc'

function	longlong f_deftax_roll (decimal a_gl_month, ref string a_msg) rpcfunc alias for 'tax_accrual_deftax_pkg.f_fas109_roll'
function longlong f_deftax_calc(string a_deftax_sqls, decimal{2} a_gl_month, string a_insert_rtp_sqls, string a_update_rtp_sqls, ref string a_msg) rpcfunc alias for 'tax_accrual_deftax_pkg.f_deftax_calc'
subroutine set_ta_version_id(longlong a_ta_version_id) rpcfunc alias for 'tax_accrual_control_pkg.p_set_ta_version_id'
subroutine p_enable_debug() rpcfunc alias for 'tax_accrual_control_pkg.p_enable_debug'
subroutine p_disable_debug() rpcfunc alias for 'tax_accrual_control_pkg.p_disable_debug'
function longlong f_spread_ms(longlong a_ta_version_id, decimal a_gl_month, string a_spread_sql, string a_spread_ms, ref string a_msg) rpcfunc alias for 'tax_accrual_spread_pkg.f_spread_ms'
function longlong f_pop_monthly_spread_table(longlong a_ta_version_id, double a_monthly_spread_id[], double a_company_id[], double a_oper_ind[], double a_m_id[], double a_by_oper_ind[], &
															ref string a_msg) rpcfunc alias for 'tax_accrual_spread_pkg.f_pop_monthly_spread_table'
function longlong f_calc_spread_ms(double a_m_id[], double a_gl_month[], double a_annual_amount_est[], ref double a_amount_est[], double a_ytd_percent[], &
											double a_prior_ytd_est[], longlong a_update, ref string a_msg) rpcfunc alias for  'tax_accrual_spread_pkg.f_calc_spread_ms'
function longlong f_trueup_ms(longlong a_ta_version_id, decimal a_gl_month, string a_trueup_sql, string a_trueup_ms, string rtp_insert_sqls, string rtp_update_sqls, &
										ref string a_msg) rpcfunc alias for 'tax_accrual_spread_pkg.f_trueup_ms'
function longlong f_pop_trueup_table(longlong a_ta_version_id, double a_trueup_id[], double a_company_id[], double a_oper_ind[], double a_m_id[], double a_by_oper_ind[], &
															ref string a_msg) rpcfunc alias for 'tax_accrual_spread_pkg.f_pop_trueup_table'
//###PATCH(8048) Added trueup_includes_input
function longlong f_calc_trueup_ms(double a_m_id[], double a_gl_month[], double a_annual_amount_est[], double a_amount_est[], double a_ytd_percent[], &
											double a_ytd_est[], double a_prior_ytd_act[], ref double a_amount_calc[], ref double a_amount_manual[], ref double a_amount_act[], ref double a_amount_adj[], &
											ref double a_beg_balance[], ref double a_end_balance[], double a_trueup_option[], double do_not_update[], &
											double ytd_include[], double trueup_includes_input[], double a_num_remaining_trueup_months[], double a_spread_remaining_estimate[], double a_update, ref string a_msg) &
											rpcfunc alias for  'tax_accrual_spread_pkg.f_calc_trueup_ms'
											
function longlong f_spread_dt(longlong a_ta_version_id, decimal a_gl_month, string a_spread_ms, ref string a_msg) rpcfunc alias for 'tax_accrual_spread_pkg.f_spread_dt'
function longlong f_calc_spread_dt(	double a_m_id[], double a_ta_norm_id[], double a_gl_month[], double a_annual_amount_est[], &
											ref double a_amount_est[], double a_ytd_percent[], double a_prior_ytd_est[], longlong a_update, &
											ref string a_msg ) rpcfunc alias for  'tax_accrual_spread_pkg.f_calc_spread_dt'
//###PATCH(8048) Added trueup_includes_input
function longlong f_calc_trueup_dt(double a_m_id[], double a_ta_norm_id[], double a_gl_month[], double a_annual_amount_est[], double a_amount_est[], &
												double a_ytd_percent[], double a_ytd_est[], double a_prior_ytd_act[], ref double a_amount_calc[], &
												ref double a_amount_manual[], ref double a_amount_act[], ref double a_beg_balance[], &
												ref double a_end_balance[], double a_trueup_option[], double do_not_update[], &
												double ytd_include[], double trueup_includes_input[], double a_num_remaining_trueup_months[], &
												double a_spread_remaining_estimate[], longlong a_update, ref string a_msg) rpcfunc alias for  'tax_accrual_spread_pkg.f_calc_trueup_dt'
//10.3 for Provision - End

function string f_bv_temp_work_order(longlong a_report_id) rpcfunc alias for "F_BV_TEMP_WORK_ORDER";
function string f_bv_temp_budget(longlong a_report_id) rpcfunc alias for "F_BV_TEMP_BUDGET";

// ### 29297: JAK: 2013-02-21: DB functions for OCR
function longlong f_pp_ocr_asset_lookup(longlong a_company_id, longlong a_bus_segment_id, longlong a_utility_account_id, longlong a_sub_account_id, longlong a_property_group_id, &
	longlong a_retirement_unit_id, longlong a_asset_location_id, longlong a_gl_account_id,string a_serial_number, longlong a_vintage, double a_quantity, double a_amount, &
	ref double a_asset_id_array[], ref double a_quantity_array[], ref double a_amount_array[]) rpcfunc alias for 'cpr_retirement_pkg.f_pp_ocr_asset_lookup'
		
function longlong f_pp_ocr_asset_id_validate(longlong a_asset_id, ref double a_quantity, ref double a_amount) rpcfunc alias for 'cpr_retirement_pkg.f_pp_ocr_asset_id_validate'	

function longlong f_pp_ocr_mass_validate(longlong a_company_id, longlong a_bus_segment_id, longlong a_utility_account_id, longlong a_sub_account_id, longlong a_property_group_id, &
	longlong a_retirement_unit_id, longlong a_asset_location_id, longlong a_gl_account_id,double a_quantity) rpcfunc alias for 'cpr_retirement_pkg.f_pp_ocr_mass_validate'


// BSB $$$ 30747: global search
function longlong f_search(string a_searchString, string a_search_type, ref double a_ids[]) rpcfunc alias for "PKG_PP_SEARCH.F_SEARCH";

/******************* Tax Repairs  *************************/
subroutine p_populate_basis_buckets() rpcfunc alias for 'rpr_basis_pkg.p_populate_basis_buckets';
subroutine p_create_repair_book_summary() rpcfunc alias for 'rpr_basis_pkg.p_create_repair_book_summary';
subroutine p_get_basis_buckets(	ref longlong a_repair_book_summary_id, & 
											ref longlong a_retirement_book_summary_id, &                                  
											ref string a_add_qualified_sqls, &
											ref string a_retirement_sqls, &					
											ref string a_afudc_equity_sqls, &    
											ref string a_afudc_debt_sqls, &
											ref string a_other_book_basis_diffs_sqls, &
											ref string a_other_tax_basis_diffs_sqls, &
											ref string a_total_book_cost_sqls, &
											ref string a_reversal_basis_diffs_sqls, &											
											ref string a_add_qualified_str, &
											ref string a_retirement_str, &
											ref string a_reversal_basis_diffs_str, &											 
											ref string a_reversals_booked_sqls, &
											ref string a_activity_cost_sqls, &
											ref string a_charge_type_view_sqls, &
											ref string a_book_only_sqls, &
											ref string a_tax_only_basis_diffs_sqls &
										) rpcfunc alias for 'rpr_basis_pkg.p_get_basis_buckets';
function longlong f_get_next_repair_schema_id() rpcfunc alias for 'rpr_basis_pkg.f_get_next_repair_schema_id';

/******************* End Tax Repairs **********************/

/******************* PowerTax  *************************/
function string f_refresh_interface_cos( double a_co_ids[], string a_interface_object ) rpcfunc alias for 'pkg_tax_int_ret.f_refresh_interface_cos'
subroutine p_backfill_tbt_group() rpcfunc alias for 'pkg_tax_int_ret.p_backfill_tbt_group'
function string f_powertax_rets(decimal a_tax_year, longlong a_start_mo, longlong a_end_mo, longlong a_version, string a_interface_object) rpcfunc alias for 'pkg_tax_int_ret.f_powertax_rets'	
function string f_powertax_xfers(decimal a_tax_year, longlong a_start_mo, longlong a_end_mo, longlong a_version ) rpcfunc alias for 'pkg_tax_int_xfer.f_powertax_xfers'
function string f_powertax_xfer_load(decimal a_tax_year, longlong a_start_mo, longlong a_end_mo, longlong a_debug_yn ) rpcfunc alias for 'pkg_tax_int_xfer.f_powertax_xfer_load'
function longlong delete_table(string table_name, string owner_name, string where_clause, ref string err_msg ) rpcfunc alias for 'delete_table'
subroutine set_version_id(longlong a_version_id) rpcfunc alias for 'powertax_control_pkg.p_set_version_id'
function longlong f_renumber_trids( ) rpcfunc alias for 'powertax_control_pkg.f_renumber_trids'
function string f_tax_mlp_k1_gen(longlong a_version, longlong a_k1_export_run_id, longlong a_k1_export_definition_id) rpcfunc alias for 'tax_mlp.f_tax_mlp_k1_gen'
function string f_tax_mlp_k1_trc_backfill(longlong a_version, longlong a_k1_export_run_id) rpcfunc alias for 'tax_mlp.f_tax_mlp_k1_trc_backfill'
/******************* PowerTax  *************************/

/*
*	To call depreciation package
*/
subroutine p_stageMonthEndDepr(double a_co_ids[], date a_months[], longlong a_load_recurring) rpcfunc alias for "pp_depr_pkg.p_stageMonthEndDepr"
subroutine p_stageMonthEndDepr_BL(double a_co_ids[], date a_months[]) rpcfunc alias for "pp_depr_pkg.p_stageMonthEndDepr_BL"
subroutine p_stageFCSTDepr(longlong a_version, date a_months[]) rpcfunc alias for "pp_depr_pkg.p_fcstDeprStage"
subroutine p_stageIncrementalDepr(longlong a_version, double a_incProcessIds[], date a_months[]) rpcfunc alias for "pkg_depr_incremental.p_deprStage"
subroutine p_stageIncrementalCPRDepr(longlong a_version, double a_incProcessIds[], date a_months[]) rpcfunc alias for "pkg_depr_incremental.p_CPRdeprStage"
subroutine p_stageMonthEndCPRDepr(double a_co_ids[], date a_months[], longlong a_subledger) rpcfunc alias for "pkg_pp_cpr_depr.p_stageMonthEndDepr"
subroutine p_stageFCSTCPRDepr(date a_months[], longlong a_version) rpcfunc alias for "pkg_pp_cpr_depr.P_FCSTDEPRSTAGE"

subroutine p_calcDepr() rpcfunc alias for "pkg_depr_calc.p_deprCalc"
subroutine p_calcDeprRecurring(longlong a_recurring) rpcfunc alias for "pkg_depr_calc.p_deprCalc"
subroutine p_calcFCSTDepr(date a_month[], longlong a_recurring, longlong a_version, longlong a_calc_retire) rpcfunc alias for "pkg_depr_calc.p_deprCalc"
subroutine p_calcFCSTIncrementalDepr(date a_month[], longlong a_recurring, longlong a_version, longlong a_incremental) rpcfunc alias for "pkg_depr_calc.p_deprCalc"
subroutine p_calcCPRDepr(date a_month[]) rpcfunc alias for "pkg_depr_ind_calc.p_deprCalc"
subroutine p_calcFCSTCPRDepr(date a_month[], longlong a_version) rpcfunc alias for "pkg_depr_ind_calc.p_deprCalc"

subroutine p_archiveLease() rpcfunc alias for "pkg_pp_cpr_depr.p_archiveLease"
subroutine p_handleIncrementalResults(longlong a_version_id) rpcfunc alias for "pkg_depr_incremental.p_handleResults"
subroutine P_HANDLERESULTS_BL() rpcfunc alias for "pp_depr_pkg.P_HANDLERESULTS_BL"
/*
*	END DEPR
*/

/*
*	Server Side Processing Grant
*/
subroutine p_set_ssp_proxy(string a_proxy, string a_users[]) rpcfunc alias for "pkg_pp_common.p_set_ssp_proxy"


/*
* Logging 
*/
function longlong f_start_log(longlong a_process_id) rpcfunc alias for "pkg_pp_log.f_start_log"
subroutine p_write_message(string a_message) rpcfunc alias for "pkg_pp_log.p_write_message"
subroutine p_write_message(string a_message, string a_pp_error_code, string a_sql_error_code) rpcfunc alias for "pkg_pp_log.p_write_message"
subroutine p_end_log() rpcfunc alias for "pkg_pp_log.p_end_log"
subroutine p_end_log(string a_batch_id, longlong a_return_value) rpcfunc alias for "pkg_pp_log.p_end_log"
subroutine p_lock_process(longlong a_process_id) rpcfunc alias for "pkg_pp_log.p_lock_process"
function string f_msg_lookup(longlong a_process_id, string a_new_message, longlong a_sqldb_code) rpcfunc alias for "pkg_pp_log.f_msg_lookup"

/*Autonomous SQL*/
subroutine p_autonomous_sql(string a_sql) rpcfunc alias for "pkg_pp_common.p_autonomous_sql"


/*
* Lease package
*/
subroutine p_startILRBuild(double a_ilr_ids[], double a_revisions[], longlong a_sendJEs) rpcfunc alias for "pkg_lease_import.p_buildILRs"
function string f_process_residual(double a_ls_asset_ids[]) rpcfunc alias for "pkg_lease_calc.f_process_residual"
function longlong f_convert_forecasts(double a_ilr_ids[]) rpcfunc alias for "pkg_lease_ilr.f_convertforecasts"
function string f_delete_ls_assets(double a_asset_ids[]) rpcfunc alias for "pkg_lease_asset_post.f_delete_assets"
subroutine p_gettaxes(double a_asset_ids[]) rpcfunc alias for "pkg_lease_asset_post.p_gettaxes"
function longlong f_copyRevision(longlong a_ilr_id, longlong a_revision, longlong a_to_revision) rpcfunc alias for "pkg_lease_ilr.f_copyrevision"

/*
*	To check for client extensions implemented
*/
function longlong f_client_extension_exists(string a_function_name ) RPCFUNC ALIAS FOR "pkg_pp_common.f_client_extension_exists"

/*
*Reimbursables extension functions
*/
function longlong f_Client_reimb_detail_update_2(double a_billing_group_id[], double a_source_id[], double a_id[], double a_amt_type_id[]) rpcfunc alias for "F_CLIENT_REIMB_DETAIL_UPDATE_2" 

/****** UO Workflow Tools ******/
function longlong f_call_custom_fxn(string a_wf_name, string a_call_name) rpcfunc alias for "pkg_workflow_tools.f_call_custom_fxn"
function boolean f_check_level(string a_wf_name, longlong a_level) rpcfunc alias for "pkg_workflow_tools.f_check_level"
function longlong f_default_users(string a_wf_name, longlong a_workflow_type) rpcfunc alias for "pkg_workflow_tools.f_default_users"
function longlong f_delete_unnecessary_levels(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_delete_unnecessary_levels"
function longlong f_get_workflow(string a_wf_name, string a_subsystem, longlong a_company_id, string a_id_field1, string a_id_field2, string a_id_field3, string a_id_field4, string a_id_field5) rpcfunc alias for "pkg_workflow_tools.f_get_workflow"
subroutine f_msg(string a_wf_name, string msg, string str_error) rpcfunc alias for "pkg_workflow_tools.f_msg"
function longlong f_new_workflow(string a_wf_name, longlong a_workflow_type) rpcfunc alias for "pkg_workflow_tools.f_new_workflow"
function longlong f_new_workflow_detail(string a_wf_name, longlong a_workflow_type) rpcfunc alias for "pkg_workflow_tools.f_new_workflow_detail"
function longlong f_send_next_level(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_send_next_level"
function longlong f_user_approve(string a_wf_name, string a_users) rpcfunc alias for "pkg_workflow_tools.f_user_approve"
function longlong f_user_approve(string a_wf_name, string a_users, string a_approving_user) rpcfunc alias for "pkg_workflow_tools.f_user_approve"
function longlong f_user_approve(string a_wf_name, string a_users, string a_approving_user, string a_notes) rpcfunc alias for "pkg_workflow_tools.f_user_approve"
function longlong f_user_notes(string a_wf_name, string a_users, string a_notes) rpcfunc alias for "pkg_workflow_tools.f_user_notes"
function longlong f_user_reject(string a_wf_name, string a_users) rpcfunc alias for "pkg_workflow_tools.f_user_reject"
function longlong f_user_reject(string a_wf_name, string a_users, string a_approving_user) rpcfunc alias for "pkg_workflow_tools.f_user_reject"
function longlong f_user_reject(string a_wf_name, string a_users, string a_approving_user, string a_notes) rpcfunc alias for "pkg_workflow_tools.f_user_reject"
function longlong f_workflow_approve(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_workflow_approve"
function longlong f_workflow_reject(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_workflow_reject"
function longlong f_workflow_send(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_workflow_send"
function longlong f_workflow_unreject(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_workflow_unreject"
function longlong f_workflow_unsend(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_workflow_unsend"
function longlong f_workflow_validate(string a_wf_name, double id[], string users[], double required[], double authority_limit[], double workflow_rule_id[], string workflow_rule_desc[], ref double num_required[], ref double group_approval[], longlong a_workflow_id, string a_task) rpcfunc alias for "pkg_workflow_tools.f_workflow_validate"
function string f_get_instance_var(string a_wf_name) rpcfunc alias for "pkg_workflow_tools.f_get_instance_var"
subroutine f_set_instance_var(string a_wf_name, string a_json) rpcfunc alias for "pkg_workflow_tools.p_set_instance_var"
/**** End UO Workflow Tools ****/


/**** Client Budget Extensions ****/
function longlong f_client_budget_afudc(longlong a_call, string a_table_name, longlong a_actuals_month_number) rpcfunc alias for "f_client_budget_afudc"
function longlong f_client_budget_overheads(longlong a_call, string a_calling_window, string a_table_name) rpcfunc alias for "f_client_budget_overheads"
function longlong f_client_budget_update_with_act(longlong a_bv_id, string a_caller) rpcfunc alias for "f_client_budget_uwa"
function longlong f_client_budget_to_cr(longlong a_id, string a_insert_where_clause, string a_delete_where_clause, string a_cap_budget_version, string a_called_from, string a_cr_budget_version) rpcfunc alias for "f_client_budget_to_cr"
function longlong f_client_budget_revision(string a_function, longlong a_call, string a_wo_fp, double a_call_var_num[], string a_call_var_str[], longlong a_cst_rtn) rpcfunc alias for "f_client_budget_revision"
/**** End Client Budget Extensions ****/

/**** Oracle submit and schedule jobs ****/
function longlong pp_job_submit(string a_type  ,string a_funct ,longlong a_id ) RPCFUNC ALIAS FOR "pp_job.submit"
function longlong pp_job_submit(string a_type  ,string a_funct ) RPCFUNC ALIAS FOR "pp_job.submit"
function longlong pp_job_remove(longlong a_id ) RPCFUNC ALIAS FOR "pp_job.remove"
function longlong pp_job_create(string a_type  ,string a_funct ,longlong a_id ) RPCFUNC ALIAS FOR "pp_job.create_job"
function longlong pp_job_create(string a_type  ,string a_funct ) RPCFUNC ALIAS FOR "pp_job.create_job"
function longlong pp_job_drop(longlong a_id ) RPCFUNC ALIAS FOR "pp_job.drop_job"
/**** End Oracle submit and schedule jobs ****/

/**** PowerTax Plant Reconciliation package ****/
function longlong f_refresh_form_data(longlong a_job_no) rpcfunc alias for "pkg_tax_plant_recon.f_refresh_form_data"
function longlong f_calc_form_data(longlong a_job_no) rpcfunc alias for "pkg_tax_plant_recon.f_calc_form_data"
/**** End PowerTax Plant Reconciliation package ****/

/**** Provision TBBS ****/
function longlong tbbs_num_duplicate_accounts() rpcfunc alias for "pwrplant.pkg_tbbs_account.f_num_duplicate_accounts"
function longlong tbbs_num_accts_assigned_field(longlong field_id) rpcfunc alias for "pwrplant.pkg_tbbs_account.f_num_accounts_assigned_field"
function longlong tbbs_add_like_schema(longlong source_id, string new_description) rpcfunc alias for "pwrplant.pkg_tbbs_schema.f_add_like_schema"
//Using longlong array gives PLS-00418 "Array bind type must match PL/SQL table row type"
subroutine tbbs_remove_accts_assigned_fields(long field_id[]) rpcfunc alias for "pwrplant.pkg_tbbs_account.p_remove_accts_assigned_fields"
subroutine tbbs_load_book_accounts() rpcfunc alias for "pwrplant.pkg_tbbs_account.p_load_book_accounts"
subroutine tbbs_process_book_balance() rpcfunc alias for "pwrplant.pkg_tbbs_account.p_process_book_balances"
subroutine tbbs_clear_accounts() rpcfunc alias for "pwrplant.pkg_tbbs_account.p_clear_accounts"
subroutine tbbs_delete_child_accounts(string primary_accounts[]) rpcfunc alias for "pwrplant.pkg_tbbs_account.p_delete_child_accounts"
subroutine tbbs_generate_report_clobs() rpcfunc alias for "pwrplant.pkg_tbbs_report.p_generate_report_clobs"
subroutine tbbs_report_run(ref longlong ret) rpcfunc alias for "pwrplant.pkg_tbbs_report.p_report_run"

/**** End Provision TBBS ****/
end prototypes
type variables
string table_owner
window i_callback_window
string i_dev_role
string i_last_sql_statement, i_last_sql_statement_error, i_error_text
end variables

forward prototypes
public function longlong wf_create_truncate ()
public function string uf_get_default_roles (string a_dev_role)
public function longlong uf_set_role (string a_dev_role)
public function longlong uf_get_dn (ref string a_dn, ref string a_ad_server)
public function longlong uf_set_role (string a_dev_role, boolean ab_interface, ref string a_errmsg)
public function string pp_set_process (string a_arg1, string a_arg2)
public function string pp_release_process (string a_arg)
public function string pp_check_process (string a_arg)
public function string pp_check_process_prefix (string a_arg)
end prototypes

public function longlong wf_create_truncate ();string sql
		
sql =	     " create or replace procedure truncate_table(  " + &  
	     "      table_name   in varchar2) AS  " + &  
	     " rows integer;  " + &  
	     " counts number;  " + &  
	     " user_cursor integer;  " + &  
	     " sql_stmt long(4000);  " + &  
	     " status     long(2000);  " + &  
	     "   " + &  
	     "   begin  " + &  
	     " dbms_output.enable (200000);   /* enable the output package */  " + &  
	     " status := 'Enabled dbms_output.';  " + &  
	     " dbms_output.put_line (status);   " + &  
	     "   " + &  
	     " /* Truncate table */     " + &  
	     "   " + &  
	     " sql_stmt := 'truncate table ' ||  table_name ;  " + &  
	     " dbms_output.put_line (sql_stmt);   " + &      
	     " user_cursor := dbms_sql.open_cursor;  " + &  
        " dbms_sql.parse(user_cursor,sql_stmt,dbms_sql.v7);   " + &         
        " dbms_output.put_line ('execute');     " + &    
        " rows := dbms_sql.execute(user_cursor);  " + &  
        " dbms_sql.close_cursor(user_cursor);  " + &  
        " end;  " 
execute immediate :sql;

sql = 'grant execute on truncate_table to pwrplant_role_dev'
execute immediate :sql;

sql = 'create public synonym truncate_table for pwrplant.truncate_table'
execute immediate :sql;

return 0
end function

public function string uf_get_default_roles (string a_dev_role);string role,default_role,roles

roles = ''
if a_dev_role = '' then
	a_dev_role = 'PWRPLANT_ROLE_DEV'
end if
DECLARE role_cur  CURSOR  for SELECT granted_role,default_role 
           FROM user_role_privs where username <> 'PUBLIC' and granted_role <> upper(:a_dev_role) using this;
OPEN role_cur ;
if this.SQLCode <> 0 then
	return roles
end if
FETCH role_cur INTO :role,:default_role ;
do while (this.SQLCode = 0) 
  if(this.SQLCode = 0 and default_role = 'YES') then
			 roles = roles + role + ','
  end if
  FETCH role_cur INTO :role,:default_role ;
  //if(SQLCA.SQLCode = 0 and default_role = 'YES') then
	//		 roles = roles + ','
  //end if
loop
close role_cur;
return roles
end function

public function longlong uf_set_role (string a_dev_role);string errMsg

// Call the other version and assume this is not being called by an interface
return uf_set_role(a_dev_role, false, errMsg)


end function

public function longlong uf_get_dn (ref string a_dn, ref string a_ad_server);n_oleobject ole_ad
string site 
longlong result,i


ole_ad = CREATE n_oleobject

result = ole_ad.ConnectToNewObject("ADSystemInfo") 
if result = 0 then
			 site = ole_ad.SiteName
			 if isnull(ole_ad.UserName) then // No domain
		        a_dn = ' '
			        a_ad_server = ' '
				  return -1
			 end if
		 a_dn = ole_ad.UserName
		 a_ad_server = ole_ad.GetAnyDCName()
		 return 1	
end if
a_dn = ' '
return -1
end function

public function longlong uf_set_role (string a_dev_role, boolean ab_interface, ref string a_errmsg);string key,msg
string roles,cmd,schema,sqls
longlong code,errcode
string passwd = space(1024)
string all_roles
string tablespace = ''
string temp_tablespace = ''
string user_role = ''
string dev_role= ''
string output = space(1024)
string user_dn,ad_server
string ad_passwd

if ab_interface then
	cmd = 'getrolesp'
else
	cmd = 'getrole'
end if
roles = uf_get_default_roles(a_dev_role)
key = 'PWRPLANT42PPC'
code = 0
passwd = space(1024)
ad_server = ' '
user_dn = ' ' 

if  isnull(this.userid) or this.userid = '' then goto no_domain
 
 if  this.userid <> this.logid then
	 if uf_get_dn(user_dn,ad_server)  = -1 then
			 if g_main_application then 
				  MessageBox ("Error","Error, no domain found! " )
			else
				   f_pp_msgs( "Error, no domain found! " )
			end if
			
			a_errMsg = "Error, no domain found! "
			
			return -1
	 end if
end if
 
no_domain:


ad_passwd = this.dbpass
this.pwrplant_admin(cmd,key,passwd,ad_passwd,user_dn,ad_server,'')

if this.sqlcode <> 0 then
	  msg = "Error getting role password. " + this.sqlerrtext + " Code=" + string(this.sqldbcode)
	   if g_main_application then 
			MessageBox ("Error",msg )
	  else
			 f_pp_msgs(msg  )
	  end if
	  
	  a_errMsg = msg
	  
	 return -1
else
	 all_roles =  roles  + a_dev_role 
    	 this.set_role( all_roles + " identified by " + passwd )
	 if this.sqlcode <> 0 then
	      msg = 'Error setting Role. ' + this.sqlerrtext + " Code=" + string(this.sqldbcode)
		if g_main_application then 
			      MessageBox ("Error",msg )
		 else
				 f_pp_msgs(msg  )
		 end if
		 
		 a_errMsg = msg
		 
		 return -1
    	 end if
end if

return 0
end function

public function string pp_set_process (string a_arg1, string a_arg2);blob the_blob1, the_blob2

the_blob1 = blob(a_arg1)
the_blob2 = blob(a_arg2)

return this.pp_set_process2(the_blob1, the_blob2)
end function

public function string pp_release_process (string a_arg);blob the_blob

the_blob = blob(a_arg)

return this.pp_release_process2(the_blob)
end function

public function string pp_check_process (string a_arg);blob the_blob

the_blob = blob(a_arg)

return this.pp_check_process2(the_blob)
end function

public function string pp_check_process_prefix (string a_arg);blob the_blob

the_blob = blob(a_arg)

return this.pp_check_process_prefix2(the_blob)
end function

on uo_sqlca.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_sqlca.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event sqlpreview;i_last_sql_statement = sqlsyntax
end event

event dberror;i_last_sql_statement_error = i_last_sql_statement
i_error_text = sqlerrortext

end event

