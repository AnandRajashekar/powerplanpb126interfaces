HA$PBExportHeader$nvo_ppbase_sysoption_functions.sru
forward
global type nvo_ppbase_sysoption_functions from nonvisualobject
end type
end forward

global type nvo_ppbase_sysoption_functions from nonvisualobject
end type
global nvo_ppbase_sysoption_functions nvo_ppbase_sysoption_functions

type variables
////
////	The object identifying system options overridden by users in the user options table.
////
	private string i_sysopt_user_object = "System Option"

////
////	Constants for the different system option value types.
////
	constant string i_sysopt_value_type_freeform = "FREEFORM"
	constant string i_sysopt_value_type_dropdown = "DROPDOWN"
end variables

forward prototypes
public function string of_getsystemoption (string a_system_option_id)
public function string of_getsystemoptionvaluetype (string a_system_option_id)
public subroutine of_getsystemoptionvalues (string a_system_option_id, ref string a_option_value[], ref string a_description[])
public function string of_getsystemoptionuser (string a_system_option_id)
public function string of_getsystemoptionppdefault (string a_system_option_id)
public function string of_getsystemoptionclient (string a_system_option_id)
public function boolean of_savesystemoptionuser (string a_system_option_id, string a_option_value, ref string a_msg)
public function boolean of_savesystemoptionclient (string a_system_option_id, string a_option_value, ref string a_msg)
public function boolean of_issystemonly (string a_system_option_id)
public function string of_getuseroption (string a_object, string a_option_identifier)
public function boolean of_saveuseroption (string a_object, string a_option_identifier, string a_option_value, ref string a_msg)
public function longlong of_getoptioncountforworkspace (string a_workspace_identifier)
public function string of_getsystemoption (string a_system_option_id, longlong a_company_id)
public subroutine of_populatesystemoptionvalues (string a_system_option_id, ref uo_ppbase_datawindow_dropdown a_dddw, boolean a_populate_with_userval, boolean a_populate_with_clientval, boolean a_populate_with_defaultval)
public subroutine of_populatesystemoptionvalues (string a_system_option_id, ref uo_ppbase_datawindow_dropdown a_dddw, ref datawindow a_dw_company_grid, boolean a_populate_with_userval, boolean a_populate_with_clientval, boolean a_populate_with_defaultval)
end prototypes

public function string of_getsystemoption (string a_system_option_id);/************************************************************************************************************************************************************
 **	
 **	of_getSystemOption()
 **	
 **	Gets the system option value for the passed in system option.  The function first looks to the user options to find the value then the company system options then the default PowerPlan system option.  Null values
 **	in the database are returned as empty strings.
 **	
 **	Parameters	:	string		:	(a_system_option_id) The system option for which we're returning the value.
 **	
 **	Returns		:	string		:	''		:	the specified system option does not exist or a value could not be found
 **										else	:	the current value for this system option
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	string	sysval

////
////	Look for the system value in the user options table first if we should.
////
			sysval = this.of_getSystemOptionUser( a_system_option_id )
			if not isNull( sysval ) and sysval <> "" then return sysval

////
////	If we don't find it in the user options table grab it from the system options table first looking in the company values and then in the powerplan default values.  (We could call the two different functions to retrieve the client value then the system value,
////	but doing it here in a decode with one SQL statement is quicker.)
////
			setNull( sysval )
			
			select		decode( trim( ppbase_system_options.option_value ), null, ppbase_system_options.pp_default_value, '', ppbase_system_options.pp_default_value, ppbase_system_options.option_value )
			into		:sysval
			from		ppbase_system_options
			where	upper( trim( ppbase_system_options.system_option_id ) ) = upper( trim( :a_system_option_id ) );
			
			if isNull( sysval ) then sysval = ""
			
			return sysval
end function

public function string of_getsystemoptionvaluetype (string a_system_option_id);/************************************************************************************************************************************************************
 **	
 **	of_getSystemOptionValueType()
 **	
 **	Returns the entry style type (i.e., dropdown or freeform) for a given system option.  If a system option has values in the system options values table it is assumed to have an entry type of dropdown.  Otherwise,
 **	it is assumed to have an entry type of freeform.
 **	
 **	Parameters	:	string		:	(a_system_option_id) The system option for which we're returning the value type (i.e., whether its a dropdown or freeform).
 **	
 **	Returns		:	string		:	""					:	the specified system option does not exist.
 **										"DROPDOWN"	:	the specified system option uses a dropdown entry style with a list of valid values; call the of_getSystemOptionValue() function to get the list of valid values.  Callers
 **																should use the instance variables (i_sysopt_value_type_...) for checking the return values instead of hardcoding this value.
 **										"FREEFORM"	:	the specified system option uses a freeform entry style where the user can enter anything they like.  Callers should use the instance variables (i_sysopt_value_type_...)
 **																for checking the return values instead of hardcoding this value.
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	rows

////
////	Check to make sure the system option exists.
////
			setNull( rows )
			
			select		count('x')
			into		:rows
			from		ppbase_system_options
			where	system_option_id = :a_system_option_id;
			
			if isNull( rows ) or rows = 0 then return ""

////
////	If there are values in the system option values table then we're dealing with a dropdown, otherwise its a freeform.
////
			setNull( rows )
			
			select		count('x')
			into		:rows
			from		ppbase_system_options_values
			where	system_option_id = :a_system_option_id;
			
			if isNull( rows ) or rows = 0 then return i_sysopt_value_type_freeform
			return i_sysopt_value_type_dropdown
end function

public subroutine of_getsystemoptionvalues (string a_system_option_id, ref string a_option_value[], ref string a_description[]);/************************************************************************************************************************************************************
 **	
 **	of_getSystemOptionValue()
 **	
 **	Returns the list of valid values for a given system option (of the dropdown type) along with a corresponding description.  The results can be added to a DDDW for easy user selection.
 **	
 **	Parameters	:	string			:	(a_system_option_id) The system option for which we're returning the valid values.
 **						ref string		:	(a_option_value[]) The valid option values for this system option.
 **						ref string		:	(a_description[]) A corresponding description for each option value that can be displayed in a DDDW.  These values will only be different from the values in a_option_value[] if the
 **											system option values table includes a dropdown as containing valid values.
 **	
 **	Returns		:	(None)
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	i, maxi, j, k, maxk
	string	sqls, rtn_str
	string	optval, dddw_def[]
	string	empty_string_array[]

/*
 *	Misc objects.
 */
	uo_ds_top ds_dddw
	uo_ds_top ds_values

////
////	Make sure the arrays are empty.
////
			a_option_value[] = empty_string_array[]
			a_description[] = empty_string_array[]

////
////	Grab all of the legit values for this system option and stick them in our arrays.  If we come across something that is a DDDW then we have to retrieve the values from that DDDW.
////
			sqls =		"	select		option_value option_value " + &
						"	from		ppbase_system_options_values " + &
						"	where	upper( trim( system_option_id ) ) = '" + f_replace_string( upper( trim( a_system_option_id ) ), "'", "''", "all" ) + "' "
			
			ds_values = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_values, "grid", sqls, sqlca, true )
			
			if rtn_str <> "OK" then
				destroy ds_values
				return
			end if
			
			maxi = ds_values.rowCount()
			j = 1
			
			for i = 1 to maxi
				optval = ds_values.getItemString( i, 1 )
				
				if upper( left( trim( optval ), 4 ) ) = "DDDW" then
					f_parseStringIntoStringArray( mid( trim( optval ), 6, len( optval ) ), ";", dddw_def[] )
					if upperBound( dddw_def[] ) <> 3 then continue
					
					ds_dddw = create uo_ds_top
					ds_dddw.dataobject = dddw_def[1]
					ds_dddw.setTransObject( sqlca )
					ds_dddw.retrieve()
					
					maxk = ds_dddw.rowCount()
					
					for k = 1 to maxk
						choose case lower( left( ds_dddw.describe( dddw_def[2] + ".coltype" ), 4 ) )
							case	"char"
								a_option_value[j] = ds_dddw.getItemString( k, dddw_def[2] )
							case	"deci", "numb"
								a_option_value[j] = string( ds_dddw.getItemNumber( k, dddw_def[2] ) )
						end choose
						
						choose case lower( left( ds_dddw.describe( dddw_def[3] + ".coltype" ), 4 ) )
							case	"char"
								a_description[j] = ds_dddw.getItemString( k, dddw_def[3] )
							case	"deci", "numb"
								a_description[j] = string( ds_dddw.getItemNumber( k, dddw_def[3] ) )
						end choose
						
						j++
					next
					
					destroy ds_dddw
				else
					a_option_value[j] = optval
					a_description[j] = optval
					j++
				end if
			next
			
			destroy ds_values
end subroutine

public function string of_getsystemoptionuser (string a_system_option_id);/************************************************************************************************************************************************************
 **	
 **	of_getSystemOptionUser()
 **	
 **	Gets the current user's selected value for the passed in system option.  Null values in the database are returned as empty strings.
 **	
 **	Parameters	:	string		:	(a_system_option_id) The system option for which we're returning the user's current value.
 **	
 **	Returns		:	string		:	''		:	the specified system option does not exist or a value could not be found
 **										else	:	the current value for the current user for this system option
 **	
 ************************************************************************************************************************************************************/
////
////	Look for the system value in the user options table.
////
			return this.of_getUserOption( i_sysopt_user_object, a_system_option_id )
end function

public function string of_getsystemoptionppdefault (string a_system_option_id);/************************************************************************************************************************************************************
 **	
 **	of_getSystemOptionPPDefault()
 **	
 **	Gets the PowerPlan default value for the passed in system option.  Null values in the database are returned as empty strings.
 **	
 **	Parameters	:	string		:	(a_system_option_id) The system option for which we're returning the PowerPlan default value.
 **	
 **	Returns		:	string		:	''		:	the specified system option does not exist or a value could not be found
 **										else	:	the PowerPlan default for this system option
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	string	sysval

////
////	Look for the system value in the system options table.
////
			select		ppbase_system_options.pp_default_value
			into		:sysval
			from		ppbase_system_options
			where	upper( trim( ppbase_system_options.system_option_id ) ) = upper( trim( :a_system_option_id ) );
			
			if isNull( sysval ) then sysval = ""
			
			return sysval
end function

public function string of_getsystemoptionclient (string a_system_option_id);/************************************************************************************************************************************************************
 **	
 **	of_getSystemOptionClient()
 **	
 **	Gets the current client's selected value for the passed in system option.  Null values in the database are returned as empty strings.
 **	
 **	Parameters	:	string		:	(a_system_option_id) The system option for which we're returning the client's selected value.
 **	
 **	Returns		:	string		:	''		:	the specified system option does not exist or a value for this client could not be found
 **										else	:	the current client's selected value for this system option
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	string	sysval

////
////	Look for the system value in the system options table.
////
			select		ppbase_system_options.option_value
			into		:sysval
			from		ppbase_system_options
			where	upper( trim( ppbase_system_options.system_option_id ) ) = upper( trim( :a_system_option_id ) );
			
			if isNull( sysval ) then sysval = ""
			
			return sysval
end function

public function boolean of_savesystemoptionuser (string a_system_option_id, string a_option_value, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_saveSystemOptionUser()
 **	
 **	Updates the user option for a passed in system option id and option value.  If the option value is an empty string or null the system option value will be delete from this user's preferences in the user option table.
 **	Otherwise, the system option will be inserted or updated for this user in the user option table.
 **	
 **	Parameters	:	string			:	(a_system_option_id) The system option id we're saving.
 **						string			:	(a_option_value) The option value we're saving for this system option; if the value is null or an empty string ("") we'll delete any existing value for the user.
 **						ref string		:	(a_msg) I'll pass a friendly error message back using this parameter.  This error can be displayed straight to a message box.
 **	
 **	Returns		:	boolean		:	true	:	the save was successful
 **											false	:	the save failed; check the a_msg parameter for the specific error message
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	rows

////
////	Don't allow system-onlys to be updated.
////
			if this.of_isSystemOnly( a_system_option_id ) then
				a_msg = "The system option '" + a_system_option_id + "' is a system-only option meaning that it cannot be changed online.  System-only options store choices made during implementation and may require extra database or code work in order to change them."
				return false
			end if

////
////	Save the user option.
////
			return this.of_saveUserOption( i_sysopt_user_object, a_system_option_id, a_option_value, a_msg )
end function

public function boolean of_savesystemoptionclient (string a_system_option_id, string a_option_value, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_saveSystemOptionClient()
 **	
 **	Updates the option for a passed in system option id and option value in the ppbase system options table (as the default for the client).  If the option value is an empty string or null the system option value will be
 **	deleted for this client.  Otherwise, the system option will be updated for this client in the system options table.
 **	
 **	Parameters	:	string			:	(a_system_option_id) The system option id we're saving.
 **						string			:	(a_option_value) The option value we're saving for this system option; if the value is null or an empty string ("") we'll delete any existing value for the client.
 **						ref string		:	(a_msg) I'll pass a friendly error message back using this parameter.  This error can be displayed straight to a message box.
 **	
 **	Returns		:	boolean		:	true	:	the save was successful
 **											false	:	the save failed; check the a_msg parameter for the specific error message
 **	
 ************************************************************************************************************************************************************/

////
////	Don't allow system-onlys to be updated.
////
			if this.of_isSystemOnly( a_system_option_id ) then
				a_msg = "The system option '" + a_system_option_id + "' is a system-only option meaning that it cannot be changed online.  System-only options store choices made during implementation and may require extra database or code work in order to change them."
				return false
			end if

////
////	Treat any empty strings as nulls.
////
			if trim( a_option_value ) = "" then setNull( a_option_value )

////
////	Update the client choice in the system options table.
////
			update	ppbase_system_options
			set			ppbase_system_options.option_value = :a_option_value
			where	upper( trim( ppbase_system_options.system_option_id ) ) = upper( trim( :a_system_option_id ) );
			
			if sqlca.SQLCode <> 0 then
				a_msg = "Error occurred while updating the client option for '" + a_system_option_id + "' in the system options table.  The database returned the following error:~n~n" + sqlca.SQLErrText
				return false
			end if

////
////	Success.  Clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_issystemonly (string a_system_option_id);/************************************************************************************************************************************************************
 **	
 **	of_isSystemOnly()
 **	
 **	Returns whether a given system option is system-only or not.  (System-only options are set during implementation and cannot be changed online.)
 **	
 **	Parameters	:	string		:	(a_system_option_id) The system option for which we're checking the system-only status.
 **	
 **	Returns		:	boolean	:	true	:	the specified system option is system-only
 **										else	:	the specified system option is not system-only
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	system_only_indicator

////
////	Grab the system only indicator for this system option.
////
			setNull( system_only_indicator )
			
			select		system_only
			into		:system_only_indicator
			from		ppbase_system_options
			where	upper( trim( system_option_id ) ) = upper( trim( :a_system_option_id ) );
			
			if isNull( system_only_indicator ) then return true
			if system_only_indicator = 0 then return false
			return true
end function

public function string of_getuseroption (string a_object, string a_option_identifier);/************************************************************************************************************************************************************
 **	
 **	of_getUserOption()
 **	
 **	Gets the current user's value for the passed in user option.  Null values in the database are returned as empty strings.
 **	
 **	Parameters	:	string		:	(a_object) The object for which we're getting a user option.
 **						string		:	(a_option_identifier) The option identifier for the specific option we want for this object.
 **	
 **	Returns		:	string		:	''		:	the specified user option does not exist or a value could not be found
 **										else	:	the current value for the current user for this option
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	string	optval, optval_a, optval_b, optval_c, optval_d

////
////	Look for the value in the user options table.
////
			setNull( optval_a )
			setNull( optval_b )
			setNull( optval_c )
			setNull( optval_d )
			
			select		ppbase_user_options.user_option, ppbase_user_options.user_option_b, ppbase_user_options.user_option_c, ppbase_user_options.user_option_d
			into		:optval_a, :optval_b, :optval_c, :optval_d
			from		ppbase_user_options
			where	ppbase_user_options.object = :a_object
				and	upper( trim( ppbase_user_options.option_identifier ) ) = upper( trim( :a_option_identifier ) )
				and	upper( trim( ppbase_user_options.users ) ) = upper( trim( user ) );
			
			if isNull( optval_a ) then optval_a = ""
			if isNull( optval_b ) then optval_b = ""
			if isNull( optval_c ) then optval_c = ""
			if isNull( optval_d ) then optval_d = ""
			
			optval = optval_a + optval_b + optval_c + optval_d
			
			return optval
end function

public function boolean of_saveuseroption (string a_object, string a_option_identifier, string a_option_value, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_saveUserOption()
 **	
 **	Updates the user option value for a passed in object and option value.  If the option value is an empty string or null the user option value entry will be delete from this user's preferences in the user option table.
 **	Otherwise, the option value will be inserted or updated for this user in the user option table.
 **	
 **	Parameters	:	string			:	(a_object) The object for which we're saving a user option.
 **						string			:	(a_option_identifier) The option identifier for the specific option we want to save for this object.
 **						string			:	(a_option_value) The option value we're saving for this object and option identifier; if the value is null or an empty string ("") we'll delete any existing value for the user.
 **						ref string		:	(a_msg) I'll pass a friendly error message back using this parameter.  This error can be displayed straight to a message box.
 **	
 **	Returns		:	boolean		:	true	:	the save was successful
 **											false	:	the save failed; check the a_msg parameter for the specific error message
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	rows
	string	optval_a, optval_b, optval_c, optval_d

////
////	If the value is an empty string or null then we need to delete the row from ppbase user options.  Otherwise we're either updating or inserting based on whether the row currently exists or not.
////
			if isNull( a_option_value ) or trim( a_option_value ) = "" then
				delete from ppbase_user_options
					where	ppbase_user_options.object = :a_object
						and	upper( trim( ppbase_user_options.option_identifier ) ) = upper( trim( :a_option_identifier ) )
						and	upper( trim( ppbase_user_options.users ) ) = upper( trim( user ) );
				
				if sqlca.SQLCode <> 0 then
					a_msg = "Error occurred while deleting the existing user's option for '" + a_object + "." + a_option_identifier + "'.  The database returned the following error:~n~n" + sqlca.SQLErrText
					return false
				end if
			else
				optval_a = mid( a_option_value, 1, 4000 )
				optval_b = mid( a_option_value, 4001, 4000 )
				optval_c = mid( a_option_value, 8001, 4000 )
				optval_d = mid( a_option_value, 12001, 4000 )
				
				if optval_a = "" then setNull( optval_a )
				if optval_b = "" then setNull( optval_b )
				if optval_c = "" then setNull( optval_c )
				if optval_d = "" then setNull( optval_d )
				
				setNull( rows )
				select		count('x')
				into		:rows
				from		ppbase_user_options
				where	ppbase_user_options.object = :a_object
					and	upper( trim( ppbase_user_options.option_identifier ) ) = upper( trim( :a_option_identifier ) )
					and	upper( trim( ppbase_user_options.users ) ) = upper( trim( user ) );
				
				if isNull( rows ) or rows = 0 then
					insert into ppbase_user_options ( object, option_identifier, users, user_option, user_option_b, user_option_c, user_option_d )
						values ( :a_object, :a_option_identifier, lower( trim( user ) ), :optval_a, :optval_b, :optval_c, :optval_d );
					
					if sqlca.SQLCode <> 0 then
						a_msg = "Error occurred while saving the user's option for '" + a_object + "." + a_option_identifier + "'.  The database returned the following error:~n~n" + sqlca.SQLErrText
						return false
					end if
				else
					update	ppbase_user_options
					set			ppbase_user_options.user_option = :optval_a,
								ppbase_user_options.user_option_b = :optval_b,
								ppbase_user_options.user_option_c = :optval_c,
								ppbase_user_options.user_option_d = :optval_d
					where	ppbase_user_options.object = :a_object
						and	upper( trim( ppbase_user_options.option_identifier ) ) = upper( trim( :a_option_identifier ) )
						and	upper( trim( ppbase_user_options.users ) ) = upper( trim( user ) );
					
					if sqlca.SQLCode <> 0 then
						a_msg = "Error occurred while updating the user's option for '" + a_object + "." + a_option_identifier + "'.  The database returned the following error:~n~n" + sqlca.SQLErrText
						return false
					end if
				end if
			end if

////
////	Success.  Clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function longlong of_getoptioncountforworkspace (string a_workspace_identifier);/************************************************************************************************************************************************************
 **	
 **	of_getOptionCountForWorkspace()
 **	
 **	Gets the total number of (non system-only) system options for a workspace.
 **	
 **	Parameters	:	string		:	(a_workspace_identifier) The workspace for which we're returning the number of options.
 **	
 **	Returns		:	longlong	:	The total number of (non system-only) system options that apply to the given workspace.
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	rows

/*
 *	Misc objects.
 */
	uo_ds_top ds_optcount

////
////	Don't error.
////
			if isNull( a_workspace_identifier ) then return 0
			if trim( a_workspace_identifier ) = "" then return 0

////
////	We can use the existing grid from the preferences window to easily get the count for us.  Do that here.
////
			ds_optcount = create uo_ds_top
			
			ds_optcount.dataobject = "dw_ppbase_user_option_grid"
			ds_optcount.setTransObject( sqlca )
			ds_optcount.retrieve( a_workspace_identifier )
			rows = ds_optcount.rowCount()
			
			destroy ds_optcount

////
////	Return the row count.
////
			return rows
end function

public function string of_getsystemoption (string a_system_option_id, longlong a_company_id);/************************************************************************************************************************************************************
 **	
 **	of_getSystemOption()
 **	
 **	Gets the system option value for the passed in system option and company.  The function first looks to the system options by company. If not found, the function will check the values for all companies.
 **	Null values in the database are returned as empty strings.
 **	
 **	Parameters	:	string		:	(a_system_option_id) The system option for which we're returning the value.
 **						longlong	:	(a_company_id) The company identifier for the system option we are checking.
 **	
 **	Returns		:	string		:	''		:	the specified system option does not exist or a value could not be found
 **										else	:	the current value for this system option
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	string	sysval

////
////	If we don't find it in the user options table grab it from the system options table first looking in the company values and then in the powerplan default values.  (We could call the two different functions to retrieve the client value then the system value,
////	but doing it here in a decode with one SQL statement is quicker.)
////
			setNull( sysval )
			
			select		trim( ppbase_system_options_company.option_value )
			into		:sysval
			from		ppbase_system_options_company
			where	upper( trim( ppbase_system_options_company.system_option_id ) ) = upper( trim( :a_system_option_id ) )
				and	company_id = :a_company_id;
				
			if isNull( sysval ) then sysval = ""
			if sysval <> "" then return sysval
////
////	If we don't find the value for the specified company, check the sustem option value of all companies.
////
			sysval = this.of_getSystemOption( a_system_option_id )

			if isNull( sysval ) then sysval = ""
			
			return sysval
end function

public subroutine of_populatesystemoptionvalues (string a_system_option_id, ref uo_ppbase_datawindow_dropdown a_dddw, boolean a_populate_with_userval, boolean a_populate_with_clientval, boolean a_populate_with_defaultval);/************************************************************************************************************************************************************
 **	
 **	of_populateSystemOptionValues()
 **	
 **	Populates a dddw of type uo_ppbase_datawindow_dropdown with the "dddw_ppbase_system_option_value_external" dataobject with a list of valid option values for a given system option.  This function should
 **	only be used to system options that have the value display type of "DROPDOWN".  Using the last three parameters the datawindow can be set to the current option from the database (either the current user
 **	value, current system value, current pp default value or not set at all).
 **	
 **	Parameters	:	string												:	(a_system_option_id) The system option for which we're populating the dropdown.
 **						ref uo_ppbase_datawindow_dropdown	:	(a_dddw) The dropdown to populate.
 **						boolean											:	(a_populate_with_userval) TRUE if the datawindow should be set to the current user value from the database.  FALSE if not.
 **						boolean											:	(a_populate_with_clientval) TRUE if the datawindow should be set to the current client system value from the database.  FALSE if not.
 **						boolean											:	(a_populate_with_defaultval) TRUE if the datawindow should be set to the PowerPlan default value from the database.  FALSE if not.
 **	
 **	Returns		:	(None)
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	i, maxi
	string	null_str
	string	userval, clientval, ppval
	string	optval[], optdesc[]
	string	empty_string_array[]

////
////	Make sure to reset the clear out the current value for this datawindow and reset the kid datawindow.
////
			setNull( null_str )
			a_dddw.setItem( 1, 1, null_str )
			a_dddw.of_resetDDDW()

////
////	Grab the valid values for this system option.  If we don't have any, get out of here.
////
			optval[] = empty_string_array[]
			optdesc[] = empty_string_array[]
			this.of_getSystemOptionValues( a_system_option_id, optval[], optdesc[] )
			
			maxi = upperBound( optval[] )
			if maxi = 0 then return
						
			for i = 1 to maxi
				a_dddw.of_addRow( optval[i], optdesc[i] )
			next
			
			a_dddw.of_addNullRow()
			a_dddw.of_sortDDDW()

////
////	Set the current item from the database if the user wants us to.
////
			if a_populate_with_userval then
				userval = this.of_getSystemOptionUser( a_system_option_id )
				if trim( userval ) = "" then setNull( userval )
				a_dddw.setItem( 1, 1, userval )
			elseif a_populate_with_clientval then
				clientval = this.of_getSystemOptionClient( a_system_option_id )
				if trim( clientval ) = "" then setNull( clientval )
				a_dddw.setItem( 1, 1, clientval )
			elseif a_populate_with_defaultval then
				ppval = this.of_getSystemOptionPPDefault( a_system_option_id )
				if trim( ppval ) = "" then setNull( ppval )
				a_dddw.setItem( 1, 1, ppval )
			end if
end subroutine

public subroutine of_populatesystemoptionvalues (string a_system_option_id, ref uo_ppbase_datawindow_dropdown a_dddw, ref datawindow a_dw_company_grid, boolean a_populate_with_userval, boolean a_populate_with_clientval, boolean a_populate_with_defaultval);/************************************************************************************************************************************************************
 **	
 **	of_populateSystemOptionValues()
 **	
 **	Populates a dddw of type uo_ppbase_datawindow_dropdown with the "dddw_ppbase_system_option_value_external" dataobject with a list of valid option values for a given system option.  This function should
 **	only be used to system options that have the value display type of "DROPDOWN".  Using the last three parameters the datawindow can be set to the current option from the database (either the current user
 **	value, current system value, current pp default value or not set at all).
 **	
 **	Parameters	:	string												:	(a_system_option_id) The system option for which we're populating the dropdown.
 **						ref uo_ppbase_datawindow_dropdown	:	(a_dddw) The dropdown to populate.
 **						ref datawindow								:	(a_dw_company_grid) Company override grid with dropdowns to populate
 **						boolean											:	(a_populate_with_userval) TRUE if the datawindow should be set to the current user value from the database.  FALSE if not.
 **						boolean											:	(a_populate_with_clientval) TRUE if the datawindow should be set to the current client system value from the database.  FALSE if not.
 **						boolean											:	(a_populate_with_defaultval) TRUE if the datawindow should be set to the PowerPlan default value from the database.  FALSE if not.
 **	
 **	Returns		:	(None)
 **	
 ************************************************************************************************************************************************************/

longlong	rtn
datawindowchild dw_child_values, dw_child_values_company

			of_populateSystemOptionValues( a_system_option_id, a_dddw, a_populate_with_userval, a_populate_with_clientval, a_populate_with_defaultval )

			a_dw_company_grid.object.option_value.dddw.Name = a_dddw.object.option_value.dddw.Name
			a_dw_company_grid.object.option_value.dddw.DataColumn = a_dddw.object.option_value.dddw.DataColumn
			a_dw_company_grid.object.option_value.dddw.DisplayColumn = a_dddw.object.option_value.dddw.DisplayColumn			
			
			rtn = a_dddw.GetChild( "option_value", dw_child_values )
			rtn = a_dw_company_grid.GetChild( "option_value", dw_child_values_company )
			rtn = dw_child_values.shareData( dw_child_values_company )
			
			
end subroutine

on nvo_ppbase_sysoption_functions.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_ppbase_sysoption_functions.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

