HA$PBExportHeader$u_prog_interface.sru
$PBExportComments$v10.2.1.7 maint 6820: Standalone programs not reading the executable path correctly on Windows 2008.
forward
global type u_prog_interface from nonvisualobject
end type
end forward

global type u_prog_interface from nonvisualobject
end type
global u_prog_interface u_prog_interface

type prototypes

end prototypes

type variables
string i_username 
string i_password
string i_server
string i_mail_server
string i_mail_to
string i_mail_from
string i_debug
string i_disable_mail
string i_disable_log
string i_dbms
string i_log_file
string i_pbtrace_file
string i_pbtrace_dir
string i_prog_log
string i_dbparm
blob i_log_data
longlong i_log_id
longlong i_report_data_id
boolean i_local

end variables

forward prototypes
public function boolean uf_getbit (longlong a_num, unsignedlong a_bit)
public function longlong uf_setbit (longlong a_num, unsignedlong a_bit)
public subroutine uf_exit (longlong a_code)
public function longlong uf_encrypt (ref blob b)
public function longlong uf_decrypt (ref blob b, longlong len)
public function longlong uf_bitxor (longlong a_num1, longlong a_num2)
public function longlong uf_get_prog_params (string a_program_name)
public function longlong uf_set_role ()
public function boolean uf_write_log (string a_msg)
end prototypes

public function boolean uf_getbit (longlong a_num, unsignedlong a_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_GetBit
//
// Access:	public
//
//	Arguments:
//		al_decimal		value
//		aui_bit			bit number to test
//
//	Returns:  boolean
//
//	Description:	Tests a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

unsignedlong	lul_decimal

lul_decimal	= a_num

if (Mod(long(lul_decimal /  2 ^(a_bit - 1)), 2) > 0) then
	return true
end if

return false
end function

public function longlong uf_setbit (longlong a_num, unsignedlong a_bit);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_SetBit
//
// Access:	public
//
//	Arguments:
//		ai_decimal		value
//		aui_bit			bit number to set
//
//	Returns:  new value
//
//	Description:	Sets a given bit in a number.
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

if not this.uf_GetBit(a_num, a_bit) then
	return a_num + (2 ^ (a_bit - 1))
end if

return a_num
end function

public subroutine uf_exit (longlong a_code);uo_winapi.uf_ExitProcess(a_code)
end subroutine

public function longlong uf_encrypt (ref blob b);int i = 134
int len,j,k,m
char c,c2
int cn

blob b2
string a_password


a_password = string( b ,EncodingUTF16LE! )

b =  blob(a_password,EncodingANSI!)
len = len(a_password)
b2 = blob(space(len + 2))

m  =1
if len(a_password) > 0 then
	   len = len(a_password);
	   for j = 1 to len 
		    c = chara(mid(a_password,j,1))
			k = asca(c)
			i =  uf_bitxor(k,i)
			m = blobedit(b2,j,i)
		    i = asca(c)
		next 
		i = 0
end if
b = b2
return 0




return 0
end function

public function longlong uf_decrypt (ref blob b, longlong len);int i = 134
int j,k,l
int size
char c
string str
longlong strnums[]
environment env
getenvironment(env)


str = string(b,EncodingANSI!)
//b = blob(str,EncodingUTF16LE! )
size = len  //len(b)

for j = 1 to len
	strnums[j] = asca(string(blobmid(b,j,1),EncodingANSI!))
next

if size > 0 then     
     len = size
       for j = 1 to len
			
			  k = strnums[j]
			  i =  uf_bitxor(k,i)
			  BlobEdit (b, j, blob(chara(i)) )
	    next
end if

 str = string (b,EncodingANSI! )
  b =blob(str)

return 0
end function

public function longlong uf_bitxor (longlong a_num1, longlong a_num2);//		ai_decimala
//		ai_decimalb
//
//	Returns:  int
//
//	Description:	Logically XOR 2 numbers
//
//		a	b	result
//		-- -- ------
//		0	0	0
//		0	1	1
//		1	0	1
//		1	1	0
//
//////////////////////////////////////////////////////////////////////////////
//	
//	Revision History
//
//	Version
//	1.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Simon Harris (simon@addease.com.au).  All Rights Reserved.
//
//////////////////////////////////////////////////////////////////////////////

unsignedint lui_bit
int			li_decimal

li_decimal = 0

for lui_bit = 1 to 16
	if this.uf_GetBit(a_num1, lui_bit) <> this.uf_GetBit(a_num2, lui_bit) then
		li_decimal = this.uf_SetBit(li_decimal, lui_bit)
	end if
next

return li_decimal
end function

public function longlong uf_get_prog_params (string a_program_name);longlong hMod
longlong i,j,len2
longlong len
ulong ulen
blob b
ulong encrypt_type
string filename,path
string prog_log
n_encrypt n_encrypt 
if a_program_name = "" then
	filename = space(200)
	hMod = uo_winapi.uf_GetModuleHandleA("")
	uo_winapi.uf_GetModuleFileName(hMod,filename,200)
	filename = lower(filename)
else
	filename = a_program_name
end if

/* Get the path of the executable */
len =  len(filename)
for i = len to 1 step -1
		if mid(filename,i,1) = '\' then
			path = mid(filename,1,i)
			exit
		end if
next

blob b2
blob bsection
string val
string temp
temp = space(400)
uo_winapi.uf_GetEnvironmentVariableA("TEMP",temp,400)
i_prog_log = temp + '\ppc.log'
filedelete(i_prog_log)

if fileexists(path + 'ppcset.dat') = true then
	len = n_encrypt.uf_read_blob(path + 'ppcset.dat',b2)
	if len = -1 then
 			uf_write_log("Error reading from " + path + 'ppcset.dat' + " for " + filename )
			return -1
	end if
	if n_encrypt.uf_encrypt_decrypt(false,b2,len) <> 0 then
                  uf_write_log("Error decrypting " + path + 'ppcset.dat' + " for " + filename )
			return -1
	end if
	filename = trim(lower(filename))
	if n_encrypt.uf_find_section(b2,bsection,filename) = - 1 then		
	   if n_encrypt.uf_find_section(b2,bsection,filename + ' ') = - 1 then // add space for a bug on Windows 2008 
		if n_encrypt.uf_find_section(b2,bsection,'default') =	- 1 then
		      uf_write_log("Error getting infromation from " + path + 'ppcset.dat' + " for " + filename )
			return -1
		end if
	  end if
	end if
	n_encrypt.uf_find_line(bsection,'username',val)
	i_username = val
	n_encrypt.uf_find_line(bsection,'dbms',val)
	i_dbms =val
	n_encrypt.uf_find_line(bsection,'passwd',val)
	i_password = val
	n_encrypt.uf_find_line(bsection,'server',val)
	 i_server = val
	n_encrypt.uf_find_line(bsection,'dbms',val)
	n_encrypt.uf_find_line(bsection,'mail_server',val)
	 i_mail_server = val
	n_encrypt.uf_find_line(bsection,'mail_to',val)
	 i_mail_to = val
	n_encrypt.uf_find_line(bsection,'mail_from',val)
	i_mail_from = val
	n_encrypt.uf_find_line(bsection,'debug',val)
	 i_debug = val
	n_encrypt.uf_find_line(bsection,'disable_mail',val)
	i_disable_mail = val
	n_encrypt.uf_find_line(bsection,'disable_log',val)
	 i_disable_log = val
	n_encrypt.uf_find_line(bsection,'log_file',val)
	 i_log_file = val
	 n_encrypt.uf_find_line(bsection,'pbtrace_file',val)
	 i_pbtrace_file = val
	 n_encrypt.uf_find_line(bsection,'pbtrace_dir',val)
	 i_pbtrace_dir = val
	n_encrypt.uf_find_line(bsection,'dbparm',val)
      i_dbparm = val
else
	for j = 1 to len(filename)
		 if mid(filename,j,1) = '\' then
				filename = replace(filename,j,1,'/')
		 end if
	next	
	
	filename = trim(lower(filename))
	
	path = "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info\" + filename 
	if RegistryGet(path ,"username", i_username) = -1 then
		path = "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info\" + filename + ' ' // add space for a bug on Windows 2008 
		if RegistryGet(path ,"username", i_username) = -1 then 
			// must not exist, lets try the default
			path = "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info\default" 
			RegistryGet(path ,"username", i_username)
		end if
	end if
	RegistryGet(path ,"passwd", RegBinary!,b)
	RegistryGet(path ,"passwd_len", ReguLong!,ulen)
	if RegistryGet(path ,"encrypt_type", ReguLong!,encrypt_type) = -1 then
		encrypt_type = 0
	end if
	if encrypt_type = 0 then
		uf_decrypt(b,ulen)
	else
		len2 = ulen
		n_encrypt.uf_encrypt_decrypt(false,b,len2)
	end if
	i_password = string(b)
	//len = len(i_password)
	i_password = mid(i_password,1,ulen)
	
	RegistryGet(path ,"server", i_server)
	RegistryGet(path ,"dbms", i_dbms)
	RegistryGet(path ,"mail_server", i_mail_server)
	RegistryGet(path,"mail_to", i_mail_to)
	RegistryGet(path ,"mail_from", i_mail_from)
	RegistryGet(path,"debug", i_debug)
	RegistryGet(path,"disable_mail", i_disable_mail)
	RegistryGet(path,"disable_log", i_disable_log)
	RegistryGet(path,"log_file", i_log_file)
	RegistryGet(path,"pbtrace_file", i_pbtrace_file)
      RegistryGet(path,"pbtrace_dir", i_pbtrace_dir)
	RegistryGet(path,"dbparm", i_dbparm)
end if	
	
string hkey
environment env
getenvironment(env)
longlong id,hwnd,pos
hkey = 'HKEY_CURRENT_USER\Software\Sybase\PowerBuilder\' + string(env.PBMajorRevision	) + '.0\DBTrace' 
id = uo_winapi.uf_GetCurrentProcessId();
filename = lower(i_pbtrace_file)
pos = pos(filename,".log")
if pos > 0 then
	i_pbtrace_file = mid(filename,1,pos -1) + "_" + string(id) + "_" + string(datetime(today(),now()),'m-dd-yyyy_hmmss') + ".log"
end if
path =  i_pbtrace_dir + '\' +  i_pbtrace_file
RegistrySet(hkey,"ShowDialog",regulong!,0)
RegistrySet(hkey,"LogFileName",regstring!,path)
RegistrySet(hkey,"SqlTraceFile",regstring!,path)
	
return 0

end function

public function longlong uf_set_role ();


string role,default_role,roles,dev_role
longlong num
roles = ''
dev_role = 'PWRPLANT_ROLE_DEV'
// get the default roles

SELECT count(*) into :num 
           FROM user_role_privs where default_role = 'YES'
			  and username <> 'PUBLIC' and granted_role = upper(:dev_role);
if num > 0 then // if it is a default role then do nothing
	return 1
end if

DECLARE role_cur  CURSOR  for SELECT granted_role,default_role 
           FROM user_role_privs where username <> 'PUBLIC' and granted_role <> upper(:dev_role) ;
OPEN role_cur ;
if SQLCA.SQLCode <> 0 then
	return 1
end if
FETCH role_cur INTO :role,:default_role ;
do while (SQLCA.SQLCode = 0) 
  if(SQLCA.SQLCode = 0 and default_role = 'YES') then
			 roles = roles + role + ','
  end if
  FETCH role_cur INTO :role,:default_role ;
loop
close role_cur;


string key,msg
string cmd,schema,sqls
longlong code,errcode
string passwd = space(1024)
string all_roles
string tablespace = ''
string temp_tablespace = ''
string user_role = ''
string output = space(1024)
cmd = 'getrole'
key = 'PWRPLANT42PPC'
code = 0
passwd = space(1024)

// try calling the function to get the password
sqlca.pwrplant_admin(cmd,key,passwd,'','','','')
if sqlca.sqlcode <> 0 then
	  msg = sqlca.sqlerrtext + " Code=" + string(sqlca.sqldbcode)
	  code = -1
else
	 all_roles =  roles  + dev_role 
    sqlca.set_role( all_roles + " identified by " + passwd )
	 if sqlca.sqlcode <> 0 then
	     msg = sqlca.sqlerrtext + " Code=" + string(sqlca.sqldbcode)
	     code = -1
    end if
end if
if code <> 0 then // try it the old way
	all_roles =  roles  + dev_role 
	cmd = "begin dbms_session.set_role(~'" + all_roles + " identified by Tr9uI22k~'); end;"
	execute immediate :cmd;
	if sqlca.sqlcode = -1 then 
        //messagebox("ORACLE Error Setting Roles ", "Error dbms_session.set_role = " + sqlca.SQLErrText + &
		  //                       "~r~n ppc_set_role code=" + msg)
		  // 
		  return 0
	end if

	
end if


return 0
end function

public function boolean uf_write_log (string a_msg);longlong h,status,code
longlong STD_OUTPUT_HANDLE =  -11
longlong INVALID_HANDLE_VALUE = -1
longlong len,len_return

longlong rtn
string  txt
int fn



fn = fileopen(i_prog_log,streammode!,write!,lockreadwrite!)
if fn = -1 then return false

try_again:
rtn = filewrite(fn,a_msg) 

if rtn = 32765 then
	a_msg = mid(a_msg,32766,len(a_msg))
	goto try_again
end if

if rtn = -1 then return false
if fileclose(fn) = -1 then return false
return true
//// Get Console Output Handle
//h = uo_winapi.uf_GetStdHandle( STD_OUTPUT_HANDLE)
//if h = INVALID_HANDLE_VALUE then
//	code = uo_winapi.uf_GetLastError()
//   MessageBox("uf_write_log","Erorr Getting Output Handle.  Handle = " + string(h) + " code = " + string(code))
//end if
//
//
//// Try writing to the screen
//len = len(a_msg)
//status = uo_winapi.uf_WriteFile(h,a_msg,len,len_return,0)
//if status = 0 then
//	code = uo_winapi.uf_GetLastError()
//	MessageBox("uf_write_log","Error writing to console.  status = " + string(status) + " code = " + string(code) ) 
//end if


end function

on u_prog_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_prog_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

