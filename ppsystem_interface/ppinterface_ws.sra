HA$PBExportHeader$ppinterface_ws.sra
$PBExportComments$Web Service Object
forward
global type ppinterface_ws from application
end type
global uo_sqlca sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
longlong g_process_id, g_occurrence_id, g_msg_order, g_rtn_code, g_interface_id, &
		   g_source_id, g_record_count, g_mn_to_process
string   g_log_file, g_batch_id, g_balancing_timestamp, g_command_line_args, g_debug
boolean  g_do_not_write_batch_id
datetime g_finished_at
u_prog_interface g_prog_interface
uo_sqlca_logs    g_sqlca_logs
uo_mail g_msmail
u_external_function uo_winapi
dec{2} g_amount, g_total_dollars, g_total_credits, g_total_debits

////	////	added for uo_log
boolean	g_of_log_add_time, g_of_log_use_ppmsg, g_of_log_use_writefile

////	////	added for uo_check_sql
boolean g_of_check_sql_write_success
boolean g_main_application

// Use this variable to be the OS return value in case we encounter an error 
// before we can connect to the database and establish the online logs
longlong g_rtn_failure = -1 

uo_client_interface g_uo_client
uo_ppinterface g_uo_ppint
boolean g_db_connected, g_db_logs_connected

nvo_runtimeerror g_rte
nvo_runtimeerror_app g_rte_app

nvo_stats g_stats


s_user_info                s_user_info
s_sys_info				s_sys_info
nvo_func_string		g_string_func
nvo_func_database	g_db_func
nvo_func_datastore	g_ds_func
nvo_pp_func_io			g_io_func
uo_messagebox                          g_msg  //Added by appeon (8/15/2012  )
uo_ds_top g_ds_messagebox_translate
s_file_info				g_exe_info   // ### 29892: JAK: global variable with exe attributes

// ### 29892: JAK: server side processing
boolean								g_ssp
string									g_ssp_user, g_ssp_process, g_ssp_return
longlong	g_ssp_process_identifier
s_ppbase_parm_arrays   			g_ssp_parms
s_ppbase_parm_arrays_labels	g_ssp_labels
nvo_server_side_request			g_ssp_nvo
uo_system_cache					g_cache
uo_cr_cache 						g_cr

// autogen je accounts
uo_ds_top  g_ds_cr_element_definitions
uo_autogen_je_account g_uo_autogen_je_account

// Holder for PP Constants
nvo_pp_constants		g_pp_constants
end variables
global type ppinterface_ws from application
string appname = "ppinterface_ws"
end type
global ppinterface_ws ppinterface_ws

type prototypes
Function uLong FindFirstFile(string lpFileName, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindFirstFileA;Ansi"
Function uLong FindNextFile(ulong hFindFile, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindNextFileA;Ansi"
Function uLong FindClose(ulong hFindFile) Library "kernel32.dll"

// To get environment variables
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long GetLastError() Library "kernel32.dll"

// addiitonal functions for system and user info
function long GetVersionExA( REF s_osversioninfoex OS) library "kernel32.dll" alias for "GetVersionExA;Ansi"
function long WNetGetUserA(long ptr,ref string user,ref long len) LIBRARY "mpr.dll" alias for "WNetGetUserA;Ansi"
function long gethostname(ref string host,long len) library "Ws2_32.dll" alias for "gethostname;Ansi"
function int WSAGetLastError ( ) library "Ws2_32.dll"
function int WSAStartup( uint UIVersionRequested, ref s_WSAData lpWSAData )library "Ws2_32.dll"

// to get UNC file names
//Function long WNetGetUniversalName(string lpLocalPath,long dwInfoLevel,ref s_universal_name_info lpBuffer,ref long lpBufferSize)   Library "mpr.DLL" alias for "WNetGetUniversalNameA;Ansi"
Function long WNetGetConnection(string lpLocalName, ref string lpRemoteName, ref long lpnLength)   Library "mpr.DLL" alias for "WNetGetConnectionA;Ansi"



end prototypes

on ppinterface_ws.create
appname="ppinterface_ws"
message=create message
sqlca=create uo_sqlca
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on ppinterface_ws.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;// ****************************************************************************************
//
//	Web Services do not execute the application object code directly.  Thus, no code goes here.  All code goes into
//		the uo_client_interface_ws with the appropriate calls to uo_ppinterface.  
//
// ****************************************************************************************
end event

event close;// ****************************************************************************************
//
//	Web Services do not execute the application object code directly.  Thus, no code goes here.  All code goes into
//		the uo_client_interface_ws with the appropriate calls to uo_ppinterface.  
//
// ****************************************************************************************
end event

