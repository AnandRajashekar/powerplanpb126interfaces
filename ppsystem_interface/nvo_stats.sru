HA$PBExportHeader$nvo_stats.sru
forward
global type nvo_stats from nonvisualobject
end type
end forward

global type nvo_stats from nonvisualobject autoinstantiate
end type

type variables
string occ_attrib_name[], occ_attrib_cat[], occ_attrib_per[]
decimal occ_attrib_value[]
string occ_met_name[]
datetime occ_met_start[], occ_met_end[]
decimal occ_met_vol[] 
end variables

forward prototypes
public function longlong set_met_start (string name)
public function longlong get_met_idx (readonly string name)
public function longlong set_met (string name)
public function longlong set_met_end (string name)
public function longlong set_met_volume (string name, decimal volume)
public function longlong set_met_start (string name, datetime start_time)
public function longlong set_met_end (string name, datetime end_time)
public function integer set_met_default (string name)
public function longlong get_attrib_idx (readonly string category, string period, string name)
public function decimal get_attrib_value (string category, string period, string name)
public function string get_next_attrib_name (string category, string period, string name)
public function integer mod_attrib_value (string category, string period, string name, decimal new_value)
public function integer set_attrib (string category, string period, string name, decimal value)
public subroutine get_process_stat (string stage)
public function integer get_db_stat ()
public function integer insert_to_db_stats ()
public function integer insert_to_db_met ()
end prototypes

public function longlong set_met_start (string name);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   set_met_start

Purpose  :   If metric is already added, then, update the start time. If not, add a new metric with start time.

Scope    :   Instance

Arguments	:	name	:		string		:		attribute name

Returns 	:	longlong	:	Index of the attribute added
			:	NULL		: 	If Name is null

******************************************************************************************/

longlong next_idx, rtn

setNull(next_idx)

if not isnull(name) then
	next_idx = get_met_idx(name)
	if not isnull(next_idx) then
		occ_met_start[next_idx] = datetime(today(), now())
	else
		next_idx = upperbound(occ_met_name[]) + 1
		occ_met_name[next_idx] = name
		occ_met_start[next_idx] = datetime(today(), now())
	end if
	rtn = set_met_default(name)
end if

return next_idx
end function

public function longlong get_met_idx (readonly string name);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   get_met_idx

Purpose  :   Get the Index of the Array of the attribute name passed in

Scope    :   Instance

Arguments:  name		:		string		:		attribute name

Returns 	:	longlong	:	Returns the index of the attribute
			:	NULL		: 	If Name of attribute is not found

******************************************************************************************/

longlong i, idx
setNull(idx)
if not isnull(name) then
	for i = 1 to upperbound(occ_met_name) 
		if occ_met_name[i] = name then
			idx = i
			exit
		end if
	next
end if

return idx
end function

public function longlong set_met (string name);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   set_met_start

Purpose  :   If metric is already added, return index. If not, add a new metric return newly create index

Scope    :   Instance

Arguments	:	name	:		string		:		attribute name

Returns 	:	longlong	:	Index of the attribute added
			:	NULL		: 	If Name is null

******************************************************************************************/

longlong next_idx, rtn
decimal vol
datetime null_dt

setnull(vol)
setNull(null_dt)

setNull(next_idx)
if not isnull(name) then
	next_idx = get_met_idx(name)
	if isnull(next_idx) then
		next_idx = upperbound(occ_met_name[]) + 1
		occ_met_name[next_idx] = name
		rtn = set_met_default(name)
	end if
end if

return next_idx
end function

public function longlong set_met_end (string name);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   set_met_end

Purpose  :   If metric is already added, then, update the end time. If not, add a new metric with end time.

Scope    :   Instance

Arguments	:	name	:		string		:		attribute name

Returns 	:	longlong	:	Index of the attribute added
			:	NULL		: 	If Name is null

******************************************************************************************/

longlong next_idx, rtn

setNull(next_idx)

if not isnull(name) then
	next_idx = get_met_idx(name)
	if not isnull(next_idx) then
		occ_met_end[next_idx] = datetime(today(), now())
	else
		next_idx = upperbound(occ_met_name[]) + 1
		occ_met_name[next_idx] = name
		occ_met_end[next_idx] = datetime(today(), now())
	end if
	rtn = set_met_default(name)
end if

return next_idx
end function

public function longlong set_met_volume (string name, decimal volume);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   set_met_volume

Purpose  :   If metric is already added, then, update volume. If not, add a new metric with volume.

Scope    :   Instance

Arguments	:	name	:		string		:		attribute name

Returns 	:	longlong	:	Index of the attribute added
			:	NULL		: 	If Name is null

******************************************************************************************/

longlong next_idx, rtn

setNull(next_idx)

if not isnull(name) then
	next_idx = get_met_idx(name)
	if not isnull(next_idx) then
		occ_met_vol[next_idx] = volume
	else
		next_idx = upperbound(occ_met_name[]) + 1
		occ_met_name[next_idx] = name
		occ_met_vol[next_idx] = volume
	end if
	rtn = set_met_default(name)
end if

return next_idx
end function

public function longlong set_met_start (string name, datetime start_time);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   set_met_start

Purpose  :   If metric is already added, then, update the start time. If not, add a new metric with start time.

Scope    :   Instance

Arguments	:	name			:		string		:		attribute name
				:	start_time	:		datetime	:		Start Time
				
Returns 	:	longlong	:	Index of the attribute added
			:	NULL		: 	If Name is null

******************************************************************************************/

longlong next_idx, rtn

setNull(next_idx)

if not isnull(name) then
	next_idx = get_met_idx(name)
	if not isnull(next_idx) then
		occ_met_start[next_idx] = start_time
	else
		next_idx = upperbound(occ_met_name[]) + 1
		occ_met_name[next_idx] = name
		occ_met_start[next_idx] = start_time
	end if
	rtn = set_met_default(name)

end if

return next_idx
end function

public function longlong set_met_end (string name, datetime end_time);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   set_met_end

Purpose  :   If metric is already added, then, update the end time. If not, add a new metric with end time.

Scope    :   Instance

Arguments	:	name		:		string		:		attribute name
				:	end_time	:		datetime	:		End Time

Returns 	:	longlong	:	Index of the attribute added
			:	NULL		: 	If Name is null

******************************************************************************************/

longlong next_idx, rtn

setNull(next_idx)

if not isnull(name) then
	next_idx = get_met_idx(name)
	if not isnull(next_idx) then
		occ_met_end[next_idx] = end_time
	else
		next_idx = upperbound(occ_met_name[]) + 1
		occ_met_name[next_idx] = name
		occ_met_end[next_idx] = end_time
	end if
	rtn = set_met_default(name)
end if

return next_idx
end function

public function integer set_met_default (string name);decimal dec_null
dateTime dt_null
longlong idx

setnull(dec_null)
setnull(dt_null)

idx = get_met_idx(name)
if not isnull(idx) then
	if upperbound(occ_met_start) < idx then
		occ_met_start[idx] = dt_null
	end if
	
	if upperbound(occ_met_end) < idx then
		occ_met_end[idx] = dt_null
	end if
	
	if upperbound(occ_met_vol) < idx then
		occ_met_vol[idx] = dec_null
	end if
end if
return idx

end function

public function longlong get_attrib_idx (readonly string category, string period, string name);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   get_attrib_idx

Purpose  :   Get the Index of the Array of the attribute name passed in

Scope    :   Instance

Arguments:	category		:		string		:		attribute category - Example: DB, Systems
				   Period		:		string		:		attribute Period - Example: Start, End
				    name		:		string		:		attribute name

Returns 	:	longlong	:	Returns the index of the attribute
			:	NULL		: 	If Name of attribute is not found

******************************************************************************************/

longlong i, idx

if not isnull(name) and not isnull(period) and not isnull(category) then
	for i = 1 to upperbound(occ_attrib_name) 
		if occ_attrib_name[i] = name and occ_attrib_per[i] = period and occ_attrib_cat[i] = category then
			idx = i
			exit
		end if
	next
end if

return idx
end function

public function decimal get_attrib_value (string category, string period, string name);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   get_attrib_value

Purpose  :   Get the value of the attribute name passed in

Scope    :   Instance

Arguments:	category		:		string		:		attribute category - Example: DB, Systems
				   Period		:		string		:		attribute Period - Example: Start, End
				    name		:		string		:		attribute name

Returns 	:	Decimal		:	Returns the value of the attribute
			:	NULL			: 	If Name of attribute is not found

******************************************************************************************/

decimal value
longlong i, idx
setnull(value)
if not isnull(name) and not isnull(period) and not isnull(category) then
	for i = 1 to upperbound(occ_attrib_name) 
		if occ_attrib_name[i] = name and occ_attrib_per[i] = period and occ_attrib_cat[i] = category then
			idx = i
			exit
		end if
	next
	
	value = occ_attrib_value[idx]
end if

return value
end function

public function string get_next_attrib_name (string category, string period, string name);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   get_next_attrib_name

Purpose  :   Get the name of the attribute. If the attribute name exists tag "_#" to the end of the name

Scope    :   Instance

Arguments:	category		:		string		:		attribute category - Example: DB, Systems
				   Period		:		string		:		attribute Period - Example: Start, End
				    name		:		string		:		attribute name

Returns 	:	String		:	Returns the new name if exists
			:	String		: 	Returns name passed in if Name of attribute is not found
			:	String		: 	Returns Null if any of the variable is null

******************************************************************************************/
boolean not_found
longlong i, next_step = 0
string new_name 

setnull(new_name)

if isnull(name) or isnull(period) or isnull(category) then
	return new_name
end if

new_name = name


do 
	not_found = true
	for i = 1 to upperbound(OCC_attrib_name)
		if occ_attrib_name[i] = new_name and occ_attrib_per[i] = period and occ_attrib_cat[i] = category then
			not_found = false
			next_step++
			exit
		end if
	next 
	if not_found = false then
		if right(new_name, 2) = '_'+string(next_step - 1) then
			new_name = mid(new_name, 1, len(new_name)-2) + '_' + string(next_step)
		else
			new_name = new_name+ '_' + string(next_step)
		end if		
	end if
loop until not_found = true

return new_name
end function

public function integer mod_attrib_value (string category, string period, string name, decimal new_value);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   mod_attib_value

Purpose  :   Change the value of an attribute

Scope    :   Instance

Arguments	:	category		:		string		:		attribute category - Example: DB, Systems
				   	Period		:		string		:		attribute Period - Example: Start, End
					name			:		string		:		attribute name
					new_value	:		decimal	:		New Value

Returns 	:	longlong	:	1
			:	NULL		: 	If Name of attribute is not found

******************************************************************************************/
string value
longlong i, rtn

setNull(rtn)
setnull(value)

if not isnull(name) and not isnull(period) and not isnull(category) then
	for i = 1 to upperbound(occ_attrib_name) 
		if occ_attrib_name[i] = name and occ_attrib_per[i] = period and occ_attrib_cat[i] = category then
			occ_attrib_value[i] = new_value
			rtn = 1
			exit
		end if
	next
end if

return rtn
end function

public function integer set_attrib (string category, string period, string name, decimal value);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   set_attib_value

Purpose  :   Add a new attribute

Scope    :   Instance

Arguments	:	category		:		string		:		attribute category - Example: DB, Systems
				   	Period		:		string		:		attribute Period - Example: Start, End
					name			:		string		:		attribute name
					Value			:		decimal	:		Value

Returns 	:	longlong	:	Index of the attribute added
			:	NULL		: 	If Name is null

******************************************************************************************/

longlong next_idx

setNull(next_idx)
if not isnull(name) and not isnull(period) and not isnull(category) then
	name = get_next_attrib_name(category, period, name)
	next_idx = upperbound(occ_attrib_name[]) + 1
	
	occ_attrib_cat[next_idx] = category
	occ_attrib_per[next_idx] = period
	occ_attrib_name[next_idx] = name
	occ_attrib_value[next_idx] = value
end if

return next_idx
end function

public subroutine get_process_stat (string stage);/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   get_process_stat

Purpose  :   Get and Store server process statistics

Scope    :   Instance

Arguments	:	stage	:		string		:		The stage of the process where this stats are collected. 
														This name will be tag to the begining of the stats attribute.
														For example Use "Start" in the beginning of the interface
														or "End" for the end of the interface

Returns 	:	None		:	

******************************************************************************************/

n_process_stats n_process_stats 
decimal kernel_time
decimal user_time
longlong pagefaultcount
longlong peakworkingsetsize
longlong workingsetsize
longlong quotapeakpagedpoolusage
longlong quotapagedpoolusage
longlong quotapeaknonpagedpoolusage
longlong quotanonpagedpoolusage
longlong pagefileusage
longlong peakpagefileusage
String cat_process, cat_memory
cat_process = "System-Process time"
cat_memory = "System-Memory Info"

n_process_stats  = create n_process_stats 

n_process_stats.uf_get_process_time (kernel_time, user_time)
n_process_stats.uf_get_memory_info (pagefaultcount, peakworkingsetsize, workingsetsize, quotapeakpagedpoolusage, &
		quotapagedpoolusage, quotapeaknonpagedpoolusage, quotanonpagedpoolusage, pagefileusage, peakpagefileusage)
		
if isnull(stage) then stage = "End"

set_attrib(Cat_process, stage,"Kernel time", kernel_time)
set_attrib(Cat_process, stage,"User time", user_time)
set_attrib(cat_memory,stage,"Page fault count", pagefaultcount)
set_attrib(cat_memory,stage,"Peak working set size", peakworkingsetsize)
set_attrib(cat_memory,stage,"Working set size", workingsetsize)
set_attrib(cat_memory,stage,"Quota peak paged pool usage", quotapeakpagedpoolusage)
set_attrib(cat_memory,stage,"Quota paged pool usage", quotapagedpoolusage)
set_attrib(cat_memory,stage,"Quota peak non paged pool usage", quotapeaknonpagedpoolusage)
set_attrib(cat_memory,stage,"Quota non paged pool usage", quotanonpagedpoolusage)
set_attrib(cat_memory,stage,"Page file usage", pagefileusage)
set_attrib(cat_memory,stage,"Peak page file usage", peakpagefileusage)








end subroutine

public function integer get_db_stat ();/*****************************************************************************************
PROPRIETARY INFORMATION OF   
POWERPLAN INC. , 
ALL RIGHTS RESERVED

Subsystem:   system

Function :   get_attrib_value

Purpose  :   Get DB statistics on session. Should be ran at the end of interface

Scope    :   Instance

Arguments:	

Returns 	:	Integer		:	1 = Success, -1 = fail
******************************************************************************************/
string str_rtn, sqls, name, period, category
decimal value
longlong rtn, i

uo_ds_top ds_temp
ds_temp = create uo_ds_top

sqls = + &
" select 'DB-STAT' Category, a.name name, b.value " + &
" from v$statname a, v$mystat b " + &
" where a.statistic# = b.statistic# " + &
" union all " + &
" select 'DB-LATCH', name,  gets " + &
" from v$latch " + &
" union all " + &
" select 'DB-STAT','Elapsed Time', hsecs from v$timer " + &
" union all " + &
" select 'DB-STAT', stm.stat_name as statistic, trunc(stm.value/1000000,3) as seconds " + &
" from v$sess_time_model stm " + &
" where stm.stat_name <> 'DB time' " + &
" and stm.stat_name <> 'parse time elapsed' " + &
" and sid = (select distinct sid from  v$mystat) " + &
" and stm.value > 0 "


str_rtn = f_create_dynamic_ds(ds_temp, 'grid', sqls, sqlca,true)
if str_rtn <> 'OK' then
	f_pp_msgs('Warning - Capturing database statistics failed: ' + ds_temp.i_sqlca_sqlerrtext)
	return -1
end if
period = 'End'
for i = 1 to ds_temp.rowCount()
	setNull(category)
	setNull(name)
	setNull(value)
	category = ds_temp.getItemString(i,1)
	name = ds_temp.getItemString(i,2)
	value = ds_temp.getItemdecimal(i,3)
	
	rtn = set_attrib(category, period, name, value)
	if rtn < 1 then
		f_pp_msgs('Error - Setting Attrib failed')
		return -1
	end if
next


return 1
end function

public function integer insert_to_db_stats ();longlong i
string sqls
For i = 1 to upperbound(occ_attrib_name)
	sqls = "insert into pp_processes_occ_attribute (process_id, occurrence_id, category, period, attribute, value) " + & 
		"values ("+string(g_process_id)+","+string(g_occurrence_id)+",'"+occ_attrib_cat[i]+"','"+occ_attrib_per[i]+"','"+occ_attrib_name[i]+"',"+string(occ_attrib_value[i])+")"
	sqlca.p_autonomous_sql(sqls)
	if sqlca.sqlcode <> 0 then	
		f_pp_msgs("WARNING: Fail to log process statistics: " + 	sqlca.SQLErrText)
		f_pp_msgs("SQL Text: " + sqls)
		return -1
	end if
next
return 1
end function

public function integer insert_to_db_met ();longlong i
string sqls, str_start_date, str_end_date
For i = 1 to upperbound(occ_met_name)
	//convert the date into the correct format. computer's date format could be different
	str_start_date = string(occ_met_start[i], "mm/dd/yyyy")
	str_end_date = string(occ_met_end[i], "mm/dd/yyyy")
	
	sqls = "insert into pp_processes_occ_metric (process_id, occurrence_id, description, start_time, end_time, volume) " + &
		"values ("+string(g_process_id)+","+string(g_occurrence_id)+",'"+occ_met_name[i]+"',to_date('"+str_start_date+"','MM/DD/YYYY HH24:MI:SS')," + &
		"to_date('"+str_end_date +"','MM/DD/YYYY HH24:MI:SS'),"+string(occ_met_vol[i])+")"
	sqlca.p_autonomous_sql(sqls)
	if sqlca.sqlcode <> 0 then	
		f_pp_msgs("WARNING: Fail to log metric statistics: " + 	sqlca.SQLErrText)
		f_pp_msgs("SQL Text: " + sqls)
		return -1
	end if
next
return 1
end function

on nvo_stats.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_stats.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

