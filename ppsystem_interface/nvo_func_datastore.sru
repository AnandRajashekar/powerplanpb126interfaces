HA$PBExportHeader$nvo_func_datastore.sru
forward
global type nvo_func_datastore from nonvisualobject
end type
end forward

global type nvo_func_datastore from nonvisualobject
end type
global nvo_func_datastore nvo_func_datastore

forward prototypes
public function string of_build_ds_string (datastore object_ds)
public function string of_create_dynamic_ds (datastore a_ds, string a_type, string a_sql, transaction a_transobject, boolean a_retrieve)
public function string of_remove_table_name (any a_data, string a_type)
public function integer of_get_columns_ds (datastore a_dw, ref string a_cols[])
public function integer of_get_dbcolumns (datastore a_dw, ref string a_cols[], ref string a_dbname[], ref string a_col_types[])
public function longlong of_getcol_max_ds (datastore a_ds, string a_col)
public function longlong of_group_breaks_ds (datastore a_ds, ref longlong a_rows[])
public function integer of_transfer_rows_ds (datastore a_ds_source, longlong a_source_start, longlong a_source_end, datastore a_ds_target, longlong a_target_start, boolean a_new_rows)
public function string of_add_where_clause_ds (datastore a_dw, string a_colname, string a_where, boolean a_retrieve) throws nvo_exception
public function string of_add_where_clause_ds_1equal1 (datastore a_dw, string a_colname, string a_where, boolean a_retrieve) throws nvo_exception
public subroutine of_destroy_datastores (ref uo_ds_top a_ds[])
end prototypes

public function string of_build_ds_string (datastore object_ds);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   f_build_string
//
//   Purpose  :   builds a string of ID's from the selected rows in a datastore
//
//   Scope    :   global
//
//   Arguments:   object_ds : datawindow  : input datastore
// 
//
//   Returns :    return_string   :  string   : contains id's
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    10-08-97    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

longlong	k, j, num_rows
string	return_string

k = 0
num_rows = object_ds.rowcount()

if num_rows <> 0 then
	for j = 1 to num_rows
      k = k + 1            
		if k > 1 then  
			return_string = return_string + ','
		else
		end if
		if not Isnull(object_ds.getitemnumber(j,1)) then
			return_string = return_string + string(object_ds.getitemnumber(j,1))
		end if	
	next
else
	setnull(return_string)
end if

return(return_string)

end function

public function string of_create_dynamic_ds (datastore a_ds, string a_type, string a_sql, transaction a_transobject, boolean a_retrieve);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   f_create_dynamic_ds
//
//   Purpose  :   creates a dynamic datastore given the SQL SELECT statement.
//						Can retrieve the rows if requested  ...
// 
//   Scope    :   global
//
//   Arguments:   a_ds          :  datastore    :  datastore  control
//                a_type        :  string       :  'grid' or 'form'
//                a_sql         :  string       :  SQL Select statment for the datawindow     
//                a_transobject :  transaction  :  transaction opbject to use (e.g., sqlca)             
//                a_retrieve    :  boolean      :  retrieve the rows or not
//
//   Returns :    "OK"		:  if the function executes successfully
//				 :    dserror  :  if the CREATE statement failed
//				 :    "ERROR"  :  if the RETRIEVE statement failed
//
//******************************************************************************************
string dserror, create_parms
longlong rtn

create_parms =  "style(type="+a_type+")"     


 
a_ds.Create(syntaxfromsql(a_transobject, a_sql, create_parms, dserror))

if trim(dserror) <> "" then
	return dserror
end if
 
g_ds_func.of_remove_table_name(a_ds, "ds")
 
if a_retrieve = true then
   a_ds.SetTransObject(a_transobject)
   rtn = a_ds.RETRIEVE()
   if rtn = -1 then
      return "ERROR"
  end if
end if

return "OK"


end function

public function string of_remove_table_name (any a_data, string a_type);/* Remove table name from column names in dw */
/* Arguments: 	a_data - datawinfow or datastore
					a_type - 'dw' or 'ds' */


longlong col_count, i, pos, rtn
string col_name, new_col_name, col_db_name, table_name, rc, ret, dwsyntax, err, orig_table_name
datawindow dw
datastore ds

if a_type = "dw" then	
	dw= a_data
	col_count = long(dw.describe('datawindow.column.count'))
	dwsyntax = dw.Describe("Datawindow.Syntax")
			
	for i = 1 to col_count
			col_name = dw.describe('#' + string(i) + '.name')
			col_db_name = dw.describe('#' + string(i) + '.dbname')
			pos = pos(col_db_name, ".")
			new_col_name = mid(col_db_name, pos+1)
			
			if col_name <> new_col_name then
				dwsyntax = g_string_func.of_replace_string(dwsyntax, " name=" + col_name + " ", " name=" + new_col_name + " ", "first")
			end if
	next
	rtn = dw.Create(dwsyntax, err)
	
elseif a_type = "ds" then
	ds= a_data
	col_count = long(ds.describe('datawindow.column.count'))
	dwsyntax = ds.Describe("Datawindow.Syntax")
	
	for i = 1 to col_count
			col_name = ds.describe('#' + string(i) + '.name')
			col_db_name = ds.describe('#' + string(i) + '.dbname')
			pos = pos(col_db_name, ".")
			new_col_name = mid(col_db_name, pos+1)
			
			if col_name <> new_col_name then
				dwsyntax = g_string_func.of_replace_string(dwsyntax, " name=" + col_name + " ", " name=" + new_col_name + " ", "first")
			end if
	next
	rtn = ds.Create(dwsyntax, err)
end if

return err
end function

public function integer of_get_columns_ds (datastore a_dw, ref string a_cols[]);longlong num_cols
longlong i
longlong num = 0
longlong pos,start_pos
string objects,col_name

if a_dw.dataobject = '' or a_dw.dataobject = 'dw_none' then
	return 0
end if

objects = a_dw.describe("DataWindow.Objects")
if isnull(objects) then
    return 0
end if
pos = pos(objects,"~t")
do while pos > 1
	col_name = mid(objects,start_pos,pos - start_pos)
	if a_dw.describe(col_name + ".type") = 'column' then
		if  &
			a_dw.describe(col_name + ".visible") <> '0'  and &
				long(a_dw.describe(col_name + ".Protect")) <> 1 then
			num = num + 1
			a_cols[num] = col_name 
		end if
	end if
	start_pos = pos + 1
	pos = pos(objects,"~t",start_pos)
	if pos < 1 then
		col_name = mid(objects,start_pos)
		if a_dw.describe(col_name + ".type") = 'column' then
			if  &
				long(a_dw.describe(col_name + ".visible")) <> 0 and &
				long(a_dw.describe(col_name + ".Protect")) <> 1 then
				num = num + 1
				a_cols[num] = col_name 
			end if
	   end if
		exit
	end if
loop


return num


end function

public function integer of_get_dbcolumns (datastore a_dw, ref string a_cols[], ref string a_dbname[], ref string a_col_types[]);longlong num_cols
longlong i
longlong num = 0
longlong pos,start_pos
string objects,col_name
string name
longlong ppos
longlong col_count
string col_num
string rc

//###SRM - 7/25/2013 importing Alex's change from webmaint, below:
//###AYP: Maint 7229: The f_get_dbcolumns doesn't detect columns called "datawindow".  
// Rewrite the function using column numbers instead of referencing columns by name

rc = a_dw.Describe("DataWindow.Column.Count")
if rc = "!" or rc = "?" then
	return 0
else
	col_count = Long(rc)
end if

for i = 1 to col_count
	col_num = "#" + String(i)
	col_name = a_dw.Describe(col_num + ".Name")
	if a_dw.describe(col_num + ".type") = 'column' then
			a_cols[i] = col_name 
			a_dbname[i] =  a_dw.describe(col_num + ".dbname")
			a_col_types[i] =  a_dw.describe(col_num + ".coltype")
			 if pos(a_dbname[i],'.') > 0 then
					ppos =  pos(a_dbname[i],'.')
					name = mid(a_dbname[i],ppos +1)
					a_dbname[i] = name
			end if 	
	end if
next


return col_count


end function

public function longlong of_getcol_max_ds (datastore a_ds, string a_col);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   f_get_colmax_ds
//
//   Purpose  :   find max longlong value of a column
//						SAME AS f_getcol_max, EXCEPT THIS IS FOR DATASTORES!
//                     
//   Scope    :   global
//
//   Arguments:   a_ds      :  datastore  :  datastore
//                a_col     :  string     :  column name                   
//
//   Returns :    curr_max  : longlong : max value of column 
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    05-15-94    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************


// find max value of column

longlong curr_max, num_rows , new_val
integer i

num_rows = a_ds.rowcount()

if num_rows = 0 then

    return 0

end if

curr_max = a_ds.getitemnumber(1,a_col)

for i = 2 to num_rows

 new_val = a_ds.getitemnumber(i, a_col)

 if new_val > curr_max then

    curr_max = new_val

  end if

next 

return curr_max
end function

public function longlong of_group_breaks_ds (datastore a_ds, ref longlong a_rows[]);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   f_group_breaks_ds
//
//   Purpose  :   finds the first group breaks for a datawindow 
//                
// 
//   Scope    :   global
//
//   Arguments:   a_ds    :  datastore  :  datastore control
//                a_rows  :  longlong :  array containing the first row of each group
//
//   Returns :            :  longlong :  number of groups
//
//
//******************************************************************************************


longlong first_row, row, array_temp[], i, group_number

a_rows = array_temp

if a_ds.rowcount() = 0 then

   return 0;

end if

group_number = 1

first_row = 1

i = 2

row = 2

a_rows[1] = 1

if a_ds.rowcount() = 1 then

   return 1

end if

  

do while first_row >  0

   first_row = a_ds.findgroupchange(row,group_number)

   if first_row > 1 then

     a_rows[i] = first_row

     i = i + 1

     row = first_row + 1


   end if

loop 

return upperbound(a_rows)

 
end function

public function integer of_transfer_rows_ds (datastore a_ds_source, longlong a_source_start, longlong a_source_end, datastore a_ds_target, longlong a_target_start, boolean a_new_rows);//	*****************************************************
//
//	similar to powerbuilder's RowsCopy.  However, with
//	PB's RowsCopy() the datawindow columns must be an
//	exact match.  This function will copy even if the
//	columns aren't an exact match
//		
//		a_ds_source		:	ds			:	
//		a_source_start	:	longlong	:
//		a_source_end	:	longlong	:
//		a_ds_target		:	ds			:
//		a_target_start	:	longlong	:
//		a_new_rows		:	boolean	:
//
//
//	*****************************************************

longlong	row_count, source_rows, difference
integer	column, column2, no_columns
integer	indexes2[]


source_rows	=	a_source_end - a_source_start


//	********************************
//	set the array's upperbound
//	********************************
no_columns				=	Integer(a_ds_source.Object.DataWindow.Column.Count)
indexes2[no_columns]	=	0


if a_new_rows then
	
	if a_ds_target.RowCount()	=	0 then
		difference	=	source_rows + 1
	else
		difference	=	source_rows - a_ds_target.RowCount() 							&
											+ a_target_start + 1  
	end if
	
	for row_count=1 to difference
		a_ds_target.InsertRow(0)
	next

end if



//	********************************
//	locate the matching column within
//	the target datawindow
//	********************************
for column=1 to no_columns
	column2				=	Integer( a_ds_target.Describe(a_ds_source.Describe		&
										 ( "#" + String(column) + ".Name") + ".id"))
	indexes2[column]	=	column2
next



//	********************************
//	carry out the copy using direct 
//	access to the 2 datawindows
//	********************************
for row_count=0 to source_rows
	for column=1 to no_columns
		if indexes2[column]	<> 0 then
			a_ds_target.Object.Data[a_target_start + row_count, indexes2[column]]=	&
			a_ds_source.Object.Data[a_source_start + row_count, column]	
		end if
	next
next


return	source_rows + 1
end function

public function string of_add_where_clause_ds (datastore a_dw, string a_colname, string a_where, boolean a_retrieve) throws nvo_exception;//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   of_add_where_clause_ds
//
//   Purpose  :   adds a string to the end of the sql for a datastore , or dddw.
//                It can also do a retrieve.
//                
// 
//   Scope    :   global
//
//   Arguments:   a_dw       :  datastore   : datastore control
//                a_colname  :  atring      : If empty (""), adds the string to the main sql.
//                                            If not empty, adds the sql to the child dddw in
//                                            this column           
//                a_where    :  string      : string to be added to the sql for the datawindow
//                a_retrieve :  boolean     : if true, retrieve the rows.
//
//   Returns :   the changed sql
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    ----------------------------------------------
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

string sqlsa, rc, group_string
datawindowchild dw_child
int ret,group_start, where_end

	//choose case s_sys_info.dbms
	//case "sybase"
	//	 sqlsa = "datawindow.table.select = '"
	//case "oracle"
		 sqlsa = "datawindow.table.select = ~""
	//case else  // "mssql "
	//    sqlsa = "datawindow.table.select = ~""
	//end choose
	
	
	
	
	// get the sql from the datawindow or the child datawindow
	if a_colname = "" then
		sqlsa =  sqlsa + a_dw.describe("datawindow.table.select")
	else
		ret = a_dw.getchild(a_colname, dw_child)
		if ret = -1 then
		if g_main_application then
			g_io_func.of_messageboxexception("f_add_where_clause", a_colname + "is not a Child Datawindow")
		else
			g_io_func.of_pp_msgs(a_colname + "is not a Child Datawindow")
		end if
			return ""
		end if
		dw_child.settransobject(sqlca)
		sqlsa =  sqlsa + dw_child.describe("datawindow.table.select")
	end if
	
	
	
	 
	
	group_start = pos(upper(sqlsa),'GROUP BY',1)
	
	if group_start <> 0 then
	
		group_string = mid(sqlsa,group_start)
	
		where_end = group_start - 1
	
		sqlsa = left(sqlsa,where_end)
	
	end if 
	
	sqlsa = sqlsa + a_where
	
	if group_start <> 0 then
	
		sqlsa = sqlsa + ' ' + group_string
	
	end if 
	
	
	
	
	//choose case s_sys_info.dbms
	//			case "sybase"
	//			  	sqlsa = sqlsa + "  '"
	//			case "oracle"
					sqlsa = sqlsa + '  "'
	//			case else
	//            sqlsa = sqlsa + '  "'
	//			end choose
	
	
	  if a_colname = "" then
		  rc = a_dw.Modify(sqlsa)
		  a_dw.settransobject(sqlca)
		  if a_retrieve then
	
			  if rc = "" then
				 a_dw.retrieve()
			  else
				if g_main_application then 
						 g_io_func.of_messageboxexception("Status","dwModify Failed" + rc)
				else
					g_io_func.of_pp_msgs("dwModify Failed" + rc)
				end if
				 return ""
			  end if
			end if
	
	  else
		  rc = dw_child.Modify(sqlsa)
		  dw_child.settransobject(sqlca)
		 if a_retrieve then
	
			  if rc = "" then
				  dw_child.retrieve()
			 else
			if g_main_application then
				  g_io_func.of_messageboxexception("Status","dwModify Failed" + rc)
			else
			  g_io_func.of_pp_msgs("dwModify Failed" + rc)
			end if
				  return ""
			  end if
		  end if
	
	  end if
	
	 
	
	return sqlsa

	
end function

public function string of_add_where_clause_ds_1equal1 (datastore a_dw, string a_colname, string a_where, boolean a_retrieve) throws nvo_exception;//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   of_add_where_clause
//
//   Purpose  :   adds a string to the end of the sql for a datawindow , or dddw.
//                It can also do a retrieve.
//                
// 
//   Scope    :   global
//
//   Arguments:   a_dw       :  datawindow  : datawindow control
//                a_colname  :  atring      : If empty (""), adds the string to the main sql.
//                                            If not empty, adds the sql to the child dddw in
//                                            this column           
//                a_where    :  string      : string to be added to the sql for the datawindow
//                a_retrieve :  boolean     : if true, retrieve the rows.
//
//   Returns :   the changed sql
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    05-15-94    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

string sqlsa, rc, group_string
datawindowchild dw_child
int ret,group_start, where_end, nrows

//choose case s_sys_info.dbms
//case "sybase"
//	 sqlsa = "datawindow.table.select = '"
//case "oracle"
	 sqlsa = "datawindow.table.select = ~""
//case else  // "mssql "
//    sqlsa = "datawindow.table.select = ~""
//end choose




// get the sql from the datawindow or the child datawindow
if a_colname = "" then
   sqlsa =  sqlsa + a_dw.describe("datawindow.table.select")
else
   ret = a_dw.getchild(a_colname, dw_child)
   if ret = -1 then
      g_io_func.of_messageboxexception("f_add_where_clause", a_colname + "is not a Child Datawindow")
      return ""
   end if
   dw_child.settransobject(sqlca)
   sqlsa =  sqlsa + dw_child.describe("datawindow.table.select")
end if


sqlsa =  f_replace_string(sqlsa, '1=1', " ( " + a_where + " ) ", 'all')
sqlsa =  f_replace_string(sqlsa, '1 = 1', " ( " + a_where + " ) ",'all')



//choose case s_sys_info.dbms
//			case "sybase"
//			  	sqlsa = sqlsa + "  '"
//			case "oracle"
			  	sqlsa = sqlsa + '  "'
//			case else
//            sqlsa = sqlsa + '  "'
//			end choose


  if a_colname = "" then
     rc = a_dw.Modify(sqlsa)
     a_dw.settransobject(sqlca)
     if a_retrieve then

        if rc = "" then
          nrows = a_dw.retrieve()
        else
	        g_io_func.of_messageboxexception("Status","dwModify Failed" + rc)
	       return ""
        end if
      end if

  else
     rc = dw_child.Modify(sqlsa)
     dw_child.settransobject(sqlca)
    if a_retrieve then

        if rc = "" then
           nrows = dw_child.retrieve()
       else
	        g_io_func.of_messageboxexception("Status","dwModify Failed" + rc)
	        return ""
        end if
     end if

  end if

 

return sqlsa
end function

public subroutine of_destroy_datastores (ref uo_ds_top a_ds[]);longlong i

for i = 1 to upperBound(a_ds)
	destroy a_ds[i]
next
end subroutine

on nvo_func_datastore.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_func_datastore.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

