HA$PBExportHeader$uo_autogen_je_account.sru
$PBExportComments$Maint 31652
forward
global type uo_autogen_je_account from nonvisualobject
end type
end forward

global type uo_autogen_je_account from nonvisualobject
end type
global uo_autogen_je_account uo_autogen_je_account

type variables
longlong i_wip_comp, i_process_id

uo_ds_top		i_ds_cr_elements, i_ds_pp_journal_layouts, i_ds_pp_journal_keywords, i_ds_pp_jl_bind_arg_code
private uo_ds_top i_ds_gl_account, i_ds_depr_group

longlong	i_num_cr_elements

string i_elements[], i_defaults[]
longlong i_widths[]
end variables

forward prototypes
public subroutine uf_msg (string a_msg)
public function integer uf_sqlca (string a_msg, boolean a_rows)
public function longlong uf_load_pp_jl_temp (longlong a_co_id, longlong a_trans_type, longlong a_je_meth)
public function longlong uf_autogen_je_account (s_gl_trans_parms a_gl_trans_parms, longlong a_long_args[], longlong a_type, longlong a_je_meth, longlong a_company, longlong a_gl_acct)
public subroutine uf_retrieve_from_db ()
public function longlong uf_retrieve_pp_journal_layouts (longlong a_trans_type, longlong a_company_id, longlong a_je_method_id)
public function longlong uf_retrieve_pp_journal_keywords (string a_keyword)
public function longlong uf_retrieve_pp_jl_bind_arg_code (longlong a_bind_arg_code_id)
public function string uf_retrieve_depr_group (longlong a_id)
public function string uf_retrieve_gl_account (longlong a_id)
end prototypes

public subroutine uf_msg (string a_msg);

if i_process_id > 0 and not isnull(i_process_id) then
	f_pp_msgs(a_msg)
end if

f_wo_status_box('Generating GL Transactions',a_msg)
end subroutine

public function integer uf_sqlca (string a_msg, boolean a_rows);if sqlca.sqlcode < 0 then
	uf_msg('SQL ERROR: ' + a_msg + " " + sqlca.sqlerrtext)
	return -1
end if

if a_rows then
	uf_msg('  Rows Affected: ' + string(sqlca.sqlnrows) + " " + a_msg)
end if

return 1
end function

public function longlong uf_load_pp_jl_temp (longlong a_co_id, longlong a_trans_type, longlong a_je_meth);string sqls

delete from pp_journal_layouts_temp;

if uf_sqlca('Deleting PP Journal Layouts Temp', false) = -1 then return -1


sqls = "insert into pp_journal_layouts_temp "
sqls += "select * from pp_journal_layouts where company_id = " + string(a_co_id) 
sqls += " and  trans_type = " + string(a_trans_type) +  " and je_method_id = "  + string(a_je_meth)
execute immediate :sqls;

if uf_sqlca('Loading PP Journal Layouts Temp', false) = -1 then return -1


return 1
end function

public function longlong uf_autogen_je_account (s_gl_trans_parms a_gl_trans_parms, longlong a_long_args[], longlong a_type, longlong a_je_meth, longlong a_company, longlong a_gl_acct);string sqls, arg_str, sqls_1, sqls_2, ls_gl_je_code, ls_description, ls_source,ls_company, insert_sqls, sql, cv, bind_arg_str, bind_arg_id_in_clause, col_sql, sql_block
string gl_sql, insert_sql, outer_sql, bind_arg_sql, bind_arg_col_sqls, insert_col_sqls, element, elements[], sql_str, pk_type, ds_sql, arg_list, replace_str, gl_acct_str
string sqls_str, keyword_val, col_replace_str, is_keyword,update_sql, element_sql, defaults[], elements_sql[], element_str
longlong j, long_args[], ll_amount_type, ll_dr_cr_indicator, ll_gl_status, ll_month
longlong i, ll_element_count, col_count, num_elements, rowcount, index[], rtn, widths[]
longlong binding_arg1, binding_arg2, binding_arg3, binding_arg4, arg_count, k, binding_arg[]
datetime ld_month
s_gl_trans_parms lst_gl_trans_parms[]


rtn = uf_load_pp_jl_temp(a_company, a_type, a_je_meth)

if rtn <> 1 then return -1


//gl transaction fields which should be loaded from outside the function
ld_month = a_gl_trans_parms.a_month
ls_company  = a_gl_trans_parms.a_company
ls_description = a_gl_trans_parms.a_description
ll_dr_cr_indicator = a_gl_trans_parms.a_debit_credit_indicator
ls_gl_je_code= a_gl_trans_parms.a_gl_je_code
ll_gl_status = a_gl_trans_parms.a_gl_status_id
ls_source = a_gl_trans_parms.a_source
ll_amount_type = a_gl_trans_parms.a_amount_type
ll_month = long(string(ld_month, 'yyyymm'))


if not isValid(g_ds_cr_element_definitions) then
	g_ds_cr_element_definitions = create uo_ds_top
	g_ds_cr_element_definitions.dataobject  = "dw_cr_element_definitions"
	g_ds_cr_element_definitions.SetTransObject(sqlca)
	num_elements = g_ds_cr_element_definitions.RETRIEVE()
else
	if g_ds_cr_element_definitions.rowcount() = 0 then
		g_ds_cr_element_definitions.dataobject  = "dw_cr_element_definitions"
		g_ds_cr_element_definitions.SetTransObject(sqlca)
		num_elements = g_ds_cr_element_definitions.RETRIEVE()
	else
		num_elements = g_ds_cr_element_definitions.rowcount()
	end if
end if


for i = 1 to num_elements
	element = lower(g_ds_cr_element_definitions.GetItemString(i, "description"))
	element = f_cr_clean_string(element)
	elements[i]	= '"' + upper(element) + '"'
	widths[i] = g_ds_cr_element_definitions.GetItemNumber(i, "width")
	defaults[i] = g_ds_cr_element_definitions.GetItemString(i, "default_value")
next

//DJL - If my system control says to use GLJC in gl account
cv = upper(trim(g_cr.uf_get_control_value( 'PPTOCR: GLJC IN GL ACCOUNT' )))

if isnull(cv) or cv = "" then
	g_msg.messagebox('Warning', "Could not find the control value in cr cache for PPTOCR: GLJC IN GL ACCOUNT ~n~n" + &
		"Cannot continue !")
	return -1
end if

if cv = 'YES' then
	elements[num_elements + 1] = 'gl_journal_category'
	widths[num_elements + 1] = 35
	defaults[num_elements + 1] = '0'
	num_elements = num_elements + 1
end if


//Build  list of binding arguments
bind_arg_id_in_clause = ""
for i = 1 to num_elements
	
	
	SQL = "" 
	SQL = SQL + "SELECT nvl2(keyword, TO_CHAR(NVL(bind_arg_code_id1, 0) "
	SQL = SQL + "              || ', ' "
	SQL = SQL + "              || NVL(bind_arg_code_id2, 0) "
	SQL = SQL + "              || ', ' "
	SQL = SQL + "              || NVL(bind_arg_code_id3, 0) "
	SQL = SQL + "              || ', ' "
	SQL = SQL + "              || NVL(bind_arg_code_id4, 0)),'-99 ') "
	SQL = SQL + "FROM   pp_journal_keywords pjk, "
	SQL = SQL + "       pp_journal_layouts_temp pjl "
	SQL = SQL + "WHERE  pjk.keyword(+) = pjl." + elements[i]
	SQL = SQL + " AND    pjk.keyword_type(+) = 2"
	
	setnull(bind_arg_str)
	select pp_misc_pkg.dynamic_select(:sql)
	into :bind_arg_str
	from dual;
	
	if sqlca.sqlcode <> 0 then
			g_msg.messagebox('ERROR', 'Could not find the binding args for element ' + elements[i] + sqlca.sqlerrtext)
			return -1
	end if
	
	if i < num_elements then bind_arg_str += ', '
	
	bind_arg_id_in_clause = bind_arg_id_in_clause + bind_arg_str
	
next

bind_arg_id_in_clause = '(' + bind_arg_id_in_clause + ')'

ds_sql =  "select upper(variable_name) variable_name, bind_arg_code_id from pp_jl_bind_arg_code " + &
			"where keyword_type = 2 and bind_arg_code_id in " + bind_arg_id_in_clause + "order by bind_arg_code_id"

uo_ds_top ds_bind_arg
ds_bind_arg = create uo_ds_top												
sql_str = f_create_dynamic_ds(ds_bind_arg, "grid",ds_sql, sqlca, true)												
if sql_str <> 'OK' then
	g_msg.messagebox('Error', 'Creation of ds_bind_arg failed!' +ds_bind_arg. i_sqlca_sqlerrtext)
	return -1
end if


//Gather list of binding arg columns names
arg_list = ""											
rowcount = ds_bind_arg.rowcount()
for i = 1 to rowcount
	
	arg_list += ds_bind_arg.getitemString(i, 'variable_name')
	//Important to know the location of the binding arg
	index[i] = ds_bind_arg.getitemnumber(i, 'bind_arg_code_id')
	if i < rowcount then arg_list += ', '
	
next	


//The sql which queries the transaction attributes
//The SQL must have amount as one of the columns.
select nvl(sqls_1, ' '), nvl(sqls_2, ' '), key_type
into :sqls_1, :sqls_2, :pk_type
from pp_journal_data_sql
where trans_type = :a_type;

sqls = sqls_1 + sqls_2 

//Replace the arguments for the journal data sql
for j = 1 to upperBound(a_long_args)
	arg_str = '<arg' + string(j) + '>'
	sqls = f_replace_string(sqls, arg_str,string(a_long_args[j] ), "all")
next

//Query only those columns in the list of binding args
replace_str = 'select ' +  "'"  + pk_type +  "', "  + pk_type + ', amount, ' + arg_list  + ' from( select '
sqls = f_replace_string(sqls, 'select' , replace_str, "first")
sqls+= ')'

//Only need to load as many columns as we have binding args
for i = 1 to upperbound(index)
	col_sql+= 'column' + string(i) 
	if i < upperbound(index) then col_sql += ', '
next

insert_sqls+="insert into pp_gl_trans_temp"
insert_sqls+=  "(key_type, key_id, amount, " + col_sql + ")"
insert_sqls+=sqls

delete from pp_gl_trans_temp;
if uf_sqlca('Deleting PP GL Trans Data', false) = -1 then return -1

execute immediate :insert_sqls;
if uf_sqlca('Loading PP GL Trans Data', false) = -1 then return -1


// DJL - Blast from the old new f_autogen_je_account
// Build sql block from the keywords held in pp_journal_layouts_temp
element_sql = ""
for i = 1 to num_elements
	
	sql = ""
	sql = sql + "select " + elements[i]
	sql = sql + " from pp_journal_layouts_temp"
	
	setnull(keyword_val)
	select pp_misc_pkg.dynamic_select(:sql)
	into :keyword_val
	from dual;
	
	if sqlca.sqlcode <> 0 then
			g_msg.messagebox('ERROR', 'Could not find the pp journal layouts temp record for ' + elements[i] + sqlca.sqlerrtext)
			return -1
	end if
	
	//DJL - Check to see if my keyword is a hardcoded value
	setnull(is_keyword)
	select keyword 
	into :is_keyword
	from pp_journal_keywords 
	where keyword = :keyword_val 
	and keyword_type = 2;
	
	/* If I am hardcoding a value skip this code*/
	if  not isnull(is_keyword) and is_keyword <> '' then
	
		setnull(sqls_str); 
		setnull(binding_arg1); setnull(binding_arg2); setnull(binding_arg3); setnull(binding_arg4); 
		setnull(arg_count)
		select sqls, bind_arg_code_id1, bind_arg_code_id2, bind_arg_code_id3,
			 bind_arg_code_id4,
			 (nvl2(bind_arg_code_id1, 1, 0) + nvl2(bind_arg_code_id2, 1, 0) +
			  nvl2(bind_arg_code_id3, 1, 0) + nvl2(bind_arg_code_id4, 1, 0))
		 into :sqls_str, :binding_arg1, :binding_arg2, :binding_arg3, :binding_arg4,
				:arg_count
		 from pp_journal_keywords
		 where keyword = :keyword_val
			and keyword_type = 2;
		
		binding_arg[1] = binding_arg1;
		binding_arg[2] = binding_arg2;
		binding_arg[3] = binding_arg3;
		binding_arg[4] = binding_arg4;
		
		//One may specify defaults as a replacement argument
		sqls_str = f_replace_string(sqls_str, '<default>', defaults[i], "all")
		sqls_str = f_replace_string(sqls_str, '<a_type>', string(a_type), "all")
		sqls_str = f_replace_string(sqls_str, '<a_je_meth>', string(a_je_meth), "all")
		//One may use a_gl_acct, useful for gathering account
		sqls_str = f_replace_string(sqls_str, '<a_gl_acct>', string(a_gl_acct), "all")
		
		//This is key. You must know in which column the 
		// runtime binding arg values are located.
		for j= 1 to arg_count
			for k = 1 to upperbound(index)
				if binding_arg[j] = index[k] then
					col_replace_str = 'u.column' + string(k)
					exit
				end if
			next
			arg_str = '<arg' + string(j) + '>'
			sqls_str = f_replace_string(sqls_str, arg_str, col_replace_str, "all")
		next
		
		//Pad all the Accounting Key values
		element_sql+= elements[i] + " = rpad( (" + sqls_str + "), " + string(widths[i]) + ")"
		elements_sql[i] = sqls_str
		
	else
		//Pad all the Accounting Key values
		element_sql+= elements[i] + " = " +  "rpad( " +  "'" + keyword_val	+ "'"	+ ", "  +  string(widths[i]) + ")"
		elements_sql[i] = "'" + keyword_val	+ "'"
	end if
	if i < num_elements then element_sql += ', ' 
	
next


for i = 1 to num_elements
	element_str =  '<element' + string(i) + '>'
	element_sql = f_replace_string(element_sql, element_str, "(" + elements_sql[i] + ")", "all")
next

//DJL - Major performance benefits with the scalar select. No need for a where clause.
// We want the insert into gl transaction to fail if we have a null account block.
update_sql =  "update pp_gl_trans_temp u set " + element_sql
execute immediate :update_sql;

if uf_sqlca('Loading PP GL Trans Temp Account Block', false) = -1 then return -1

//Concatenate the cr elements
gl_acct_str = ""
for i = 1 to num_elements
	gl_acct_str += elements[i]
	if i < num_elements then gl_acct_str+= "||'-'||"
next

//Build insert sql. 
insert_sql = ""
insert_sql = insert_sql + "INSERT INTO gl_transaction " 
insert_sql = insert_sql +  " (gl_trans_id, MONTH, company_number, gl_account, debit_credit_indicator, "
insert_sql = insert_sql +  "  amount, gl_je_code, gl_status_id, description, source, comments) "

		
outer_sql = ""
outer_sql = outer_sql + " SELECT pwrplant1.nextval, to_date(" + string( ll_month) + ",'yyyymm')  MONTH , "
outer_sql = outer_sql + "'" + ls_company + "'" + " company_number, " + gl_acct_str + " gl_account, " + string(ll_dr_cr_indicator) + " debit_credit_indicator, amount, "
outer_sql = outer_sql + "'" + ls_gl_je_code + "'" +  " gl_je_code,  1 gl_status_id, "
outer_sql = outer_sql + "'" + ls_description + "'" + " description, " +  "'"  + ls_source + "'" + " source, key_type||' - '||key_id comments  from pp_gl_trans_temp"

gl_sql = insert_sql + outer_sql 


execute immediate :gl_sql;

if uf_sqlca('Creating GL Transactions', false) = -1 then return -1
			

return sqlca.sqlnrows		
end function

public subroutine uf_retrieve_from_db ();// ### 29592: JAK: 2013-03-25:  Retrieve values for key tables into memory for processing.
string sqls, cv
longlong i

if isvalid(i_ds_cr_elements) then 
	// already have the values
	return
end if

// CR Elements:
i_ds_cr_elements = create uo_ds_top
i_ds_cr_elements.dataobject  = "dw_cr_element_definitions"
i_ds_cr_elements.SetTransObject(sqlca)
i_num_cr_elements = i_ds_cr_elements.RETRIEVE()

for i = 1 to i_num_cr_elements
	i_elements[i]	= f_cr_clean_string(upper(trim(i_ds_cr_elements.GetItemString(i, "description"))))
	i_widths[i] = i_ds_cr_elements.GetItemNumber(i, "width")
	//Maint 31652:  do not use null values for this array
	if i_ds_cr_elements.GetItemString(i, "default_value") = '' or isnull( i_ds_cr_elements.GetItemString(i, "default_value")) then
		i_defaults[i] = ' '
	else
		i_defaults[i] = i_ds_cr_elements.GetItemString(i, "default_value")
	end if 
next

// Add GLJC if necessary to the "ACK".  Based on system control
cv = upper(trim(g_cr.uf_get_control_value( 'PPTOCR: GLJC IN GL ACCOUNT' )))

if isnull(cv) or cv <> 'YES' then cv = 'NO'

if cv = 'YES' then
	i_num_cr_elements++
	i_elements[i_num_cr_elements] = 'gl_journal_category'
	i_widths[i_num_cr_elements] = 35
	i_defaults[i_num_cr_elements] = '0'
end if

// PP Journal Layouts
i_ds_pp_journal_layouts = create uo_ds_top
sqls = "select * from PP_JOURNAL_LAYOUTS pp" 
f_create_dynamic_ds(i_ds_pp_journal_layouts, "grid", sqls, sqlca, true)

// PP Journal Keywords
i_ds_pp_journal_keywords = create uo_ds_top
sqls = "select * from pp_journal_keywords"
f_create_dynamic_ds(i_ds_pp_journal_keywords, "grid", sqls, sqlca, true)

// PP JL Bind Arg Code
i_ds_pp_jl_bind_arg_code = create uo_ds_top
sqls = "select * from pp_jl_bind_arg_code"
f_create_dynamic_ds(i_ds_pp_jl_bind_arg_code, "grid", sqls, sqlca, true)

// GL Accounts
i_ds_gl_account = create uo_ds_top 
sqls = "select * from gl_account"
f_create_dynamic_ds(i_ds_gl_account, "grid", sqls, sqlca, true)

// Depr Group
i_ds_depr_group = create uo_ds_top
sqls = "select * from depr_group"
f_create_dynamic_ds(i_ds_depr_group, "grid", sqls, sqlca, true)
end subroutine

public function longlong uf_retrieve_pp_journal_layouts (longlong a_trans_type, longlong a_company_id, longlong a_je_method_id);// Filter i_ds_pp_journal_layouts and find the records returned.
string filter_string
longlong rtn

filter_string = 'trans_type = ' + string(a_trans_type) + ' and company_id = ' + string(a_company_id) + ' and je_method_id = ' + string(a_je_method_id)

i_ds_pp_journal_layouts.setfilter(filter_string)
rtn = i_ds_pp_journal_layouts.Filter()

if rtn < 0 then 
	return rtn
else
	return i_ds_pp_journal_layouts.rowcount()
end if

end function

public function longlong uf_retrieve_pp_journal_keywords (string a_keyword);// Filter i_ds_pp_journal_layouts and find the records returned.
string filter_string
longlong rtn

//Maint 31652:  keyword = ' ' or null should both return 0
if trim(a_keyword) = '' or isnull(a_keyword) then
	return 0
end if 

filter_string = "keyword = '" + a_keyword + "' and keyword_type = 2"

i_ds_pp_journal_keywords.setfilter(filter_string)
rtn = i_ds_pp_journal_keywords.Filter()

if rtn < 0 then 
	return rtn
else
	return i_ds_pp_journal_keywords.rowcount()
end if

end function

public function longlong uf_retrieve_pp_jl_bind_arg_code (longlong a_bind_arg_code_id);// Filter i_ds_pp_journal_layouts and find the records returned.
string filter_string
longlong rtn

filter_string = "bind_arg_code_id = " + string(a_bind_arg_code_id) + " and keyword_type = 2"

i_ds_pp_jl_bind_arg_code.setfilter(filter_string)
rtn = i_ds_pp_jl_bind_arg_code.Filter()

if rtn < 0 then 
	return rtn
else
	return i_ds_pp_jl_bind_arg_code.rowcount()
end if

end function

public function string uf_retrieve_depr_group (longlong a_id);string filter, rtn

filter = 'depr_group_id = '+string(a_id)

i_ds_depr_group.setfilter( filter )
i_ds_depr_group.filter( )
if i_ds_depr_group.rowcount( ) = 1 then
	rtn = i_ds_depr_group.getitemstring( 1, 'description' )
end if

return rtn
end function

public function string uf_retrieve_gl_account (longlong a_id);string filter, rtn

filter = 'gl_account_id = '+string(a_id)

i_ds_gl_account.setfilter( filter )
i_ds_gl_account.filter( )
if i_ds_gl_account.rowcount( ) = 1 then
	rtn = i_ds_gl_account.getitemstring( 1, 'external_account_code' )
end if

return rtn
end function

on uo_autogen_je_account.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_autogen_je_account.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// ### DJL - Maint 8827 11.05.2011
// Depr journals by asset is slow.
end event

