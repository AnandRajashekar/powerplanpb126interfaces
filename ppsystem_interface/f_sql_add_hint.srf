HA$PBExportHeader$f_sql_add_hint.srf
$PBExportComments$v10.2.1.5 handle tabs
global type f_sql_add_hint from function_object
end type

forward prototypes
global function string f_sql_add_hint (string a_sql, string a_keyword)
end prototypes

global function string f_sql_add_hint (string a_sql, string a_keyword);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   f_sql_add_hint
//
//   Purpose  :   adds a oracle hint to an sql string based on table pp_datawindow_hints
//                
// 
//   Scope    :   global
//
//   Arguments:   a_sql      :  string      : the sql to be modified
//						a_keyword  :  string      : the I_D of the hint stored in the database
//                
//
//   Returns :   the changed sql
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    06-29-2007    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

longlong ll_select_number, ll_hint_pos, i
string ls_hint, ls_sql
boolean lb_add_hint

//sww - don't upper because that will interfer with any static values built into the SQL that need to be a certain case
ls_sql = a_sql

DECLARE dw_hint_cur CURSOR FOR SELECT hint, select_number
                                 FROM pp_datawindow_hints
                                WHERE lower(datawindow)=lower(:a_keyword)
                                ORDER BY select_number;
open dw_hint_cur;

do while sqlca.SQLCode = 0
	fetch dw_hint_cur into :ls_hint, :ll_select_number;
	if sqlca.SQLCode = 100 then
		exit
	end if
	
   ll_hint_pos = 1
	lb_add_hint = TRUE

    if pos(ls_sql, '.select' ) > 0 then ll_select_number = ll_select_number + 1 // the sql has datawindow.select in front of it

   // Check for Select number = 0 - This represents a hint that will be added to an
   // UPDATE, INSERT, or DELETE statement; check for spaces or tabs after the keyword; can't just check for keyword as it may be part of table or column name
   if ll_select_number = 0 then
      if pos( f_replace_string( upper( ls_sql ),"~t"," ", "all" ), "UPDATE ", 1 ) > 0 then
        ll_hint_pos = pos( f_replace_string( upper( ls_sql ),"~t"," ", "all" ), "UPDATE ", 1 )
        ll_hint_pos = ll_hint_pos + 6
      elseif pos( f_replace_string( upper( ls_sql ),"~t"," ", "all" ), "INSERT ", 1 ) > 0 then
        ll_hint_pos = pos( f_replace_string( upper( ls_sql ),"~t"," ", "all" ), "INSERT ", 1 )
        ll_hint_pos = ll_hint_pos + 6
      elseif pos( f_replace_string( upper( ls_sql ),"~t"," ", "all" ), "DELETE ", 1 ) > 0 then
        ll_hint_pos = pos( f_replace_string( upper( ls_sql ),"~t"," ", "all" ), "DELETE ", 1 )
        ll_hint_pos = ll_hint_pos + 6
      else
        lb_add_hint = FALSE
      end if
   else
      //loop until we reach the correct select
      //check for space or tabs after the SELECT; can't just check for SELECT by itself as it may be part of table or column name
      for i = 1 to ll_select_number
        ll_hint_pos = pos(f_replace_string(upper(ls_sql),"~t"," ", "all"), 'SELECT ', ll_hint_pos)
        if ll_hint_pos = 0 then
           lb_add_hint = FALSE
           exit
        end if
        ll_hint_pos = ll_hint_pos + 6
      next
   end if	
		
   if lb_add_hint then
      //add the hint
      ls_sql = left( ls_sql, ll_hint_pos ) + ' ' + ls_hint + ' ' + right( ls_sql, len( ls_sql ) - ll_hint_pos )
   end if		
loop

close dw_hint_cur;

return ls_sql
end function

