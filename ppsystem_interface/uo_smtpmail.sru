HA$PBExportHeader$uo_smtpmail.sru
$PBExportComments$v10.2.1.7 maint 6179: invalid sql; v10.2.1.6 maint 5598: Changed the mail object to use the f_get_ini_file
forward
global type uo_smtpmail from uo_mail
end type
end forward

global type uo_smtpmail from uo_mail
end type
global uo_smtpmail uo_smtpmail

type prototypes

end prototypes

type variables
boolean i_passwd_flag = false
string i_userid
string i_passwd
boolean i_database_mail = false

end variables

forward prototypes
public function boolean uf_get_user (ref string a_user)
public function boolean uf_get_host (ref string a_server)
public function longlong uf_load_library (string a_lib)
public function boolean add_attachment_file (string a_filename)
public function boolean add_attachment_blob (string a_attachment_name, blob a_blob)
public function boolean add_attachment_sql (string a_attachment_name, string a_sql)
public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, boolean a_delete_files)
public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename, boolean a_delete_files)
end prototypes

public function boolean uf_get_user (ref string a_user);string mail_user
if pos(a_user,'@',1) = 0 then
	select mail_id  into :mail_user from pp_security_users where lower(rtrim(users)) = lower(rtrim(:a_user));
	if sqlca.sqlcode <> 0 then
		
		Return false
	end if
	if isnull(mail_user) then 
	   return false
   end if
	a_user = mail_user
end if

return true

end function

public function boolean uf_get_host (ref string a_server);string ini_file,host_name
string flag
// host name was not given
if a_server = '' then
		ini_file = f_get_ini_file() 
		host_name = ProfileString(ini_file, "Mail", "SMTP_HOST", "")
		if host_name = '' then
			return false
		end if
		a_server = host_name
end if

ini_file = f_get_ini_file() 
flag = ProfileString(ini_file, "Mail", "SMTP_ORACLE", "")
if flag = '1' then
	i_database_mail = true
end if


i_passwd = ProfileString(ini_file, "Mail", "SMTP_PASSWORD", "")
if i_passwd = '' then
	i_passwd_flag = false
	return true
end if
i_userid = ProfileString(ini_file, "Mail", "SMTP_USER", "")
if i_userid = '' then
	i_passwd_flag = false
	return true
end if

return true
end function

public function longlong uf_load_library (string a_lib);string ini_file,str
string path,dll_path
string err
longlong ret
longlong i
longlong str_len
longlong not_found = 1

//get INI file path
ini_file = f_get_ini_file() 
str_len = len(ini_file)
for i = str_len to 1 step -1
    if mid(ini_file,i,1) = "\" then
         path = mid(ini_file,1,i)
         goto end_of_check
    end if  
next
not_found = 0

end_of_check:
str = path + a_lib
if FileExists(str) = true then
	//library exists in the same location as our INI file
	goto next_step
else
	//library does not exist in the same location as the INI file
	//	see if the location of the library exists within the INI file
	dll_path = ProfileString(ini_file, "Mail", "SMTP_DLL_PATH", "")
	str = dll_path + '\' + a_lib
	
	//CBS - error handling after "next_step" label below. 
//	if FileExists(str) = false then
//		f_pp_msgs("WARNING: No path defined in INI file for PowerPlan SMTP DLL or SMTP DLL could not be found at the specified path." )
//		f_pp_msgs("	Name of library: " + a_lib)
//		if not isnull(dll_path) and dll_path <> '' then 
//			f_pp_msgs("	SMTP_DLL_PATH setting from INI file: " + dll_path)
//		end if	
//		f_pp_msgs("	E-mail will not be sent." )
//		return -1
//	end if
end if
	
	
next_step:

	
   
	
  if FileExists(str) = true then
  elseif FileExists(a_lib) = true then
	  str = a_lib
  else
//g_msg.messagebox("Warning","Warning the DLL library " + str + " does not exist.",exclamation!,Ok!)
	   f_pp_msgs("ERROR:the DLL library " + str + " does not exist." )
	  return -1
  end if
  
   ret = uo_winapi.uf_loadLibrarya(str)
   choose case ret
        case 0
           	err = 'System was out of memory, executable file was corrupt, or relocations ' + &
                  'were invalid.' 
	     case 2
            err = 'File was not found.' 
        case 3	
            err = 'Path was not found.' 
        case 5	
            err = 'Attempt was made to dynamically link to a task, or there ' + &
                  'was a sharing or network-protection error.' 
        case 6	
            err = 'Library required separate data segments for each task.' 
        case 8	
            err = 'There was insufficient memory to start the application.' 
        case 10	
            err = 'Windows version was incorrect.' 
        case 11	
            err = 'Executable file was invalid. Either it was not a ' + &
                   'Windows application or there was an error in the .EXE image.' 
        case 12	
            err = 'Application was designed for a different operating system.' 
        case 13	
            err = 'Application was designed for MS-DOS 4.0.' 
        case 14	
            err = 'Type of executable file was unknown.' 
        case 15	
            err = 'Attempt was made to load a real-mode application (developed for an ' + &
                  'earlier version of Windows).' 
        case 16	
            err = 'Attempt was made to load a second instance of an executable file containing ' + &
                  'multiple data segments that were not marked read-only.' 
        case 19	
            err = 'Attempt was made to load a compressed executable file. The file must ' + &
                  'be decompressed before it can be loaded.' 
        case 20	
            err = 'Dynamic-link library (DLL) file was invalid. One of the DLLs required ' + &
                  'to run this application was corrupt.' 
        case 21	
            err = 'Application requires Microsoft Windows 32-bit extensions. ' 
        case is > 32 
            return 1
        case is < 0
            return 1 
        case else
            err = 'Unknown Error Code = ' + string(ret)
     end choose
//g_msg.messagebox("Error",err)

return -1  

end function

public function boolean add_attachment_file (string a_filename);blob data,data_temp
data =blob("")
blob filedata
longlong unit
longlong data_len
longlong bytes_read
longlong tot
longlong str_len
longlong i
string filename 
tot = 1

	bytes_read = 1
	unit = FileOpen(a_filename,StreamMode!,Read!)
	do while bytes_read > 0
		bytes_read = FileRead(unit,data_temp)
		if  bytes_read > 0 then
			 data = data + data_temp 
			 //BlobEdit(data, tot, data_temp)
			 tot = tot  + bytes_read 
		end if
	loop
	fileclose(unit)
    data_len = len(data)
	if isnull(data_len) then
		a_filename = ' '
		return false
	end if
	
//	if tot  < 32000 then
//	   data = data + blob(space(32000 - tot + 1 ),EncodingANSI! )	//needed to eliminate binary junk at the end of the attachment file when there isn't much data in the file
//	end if
	
	
	if pos(a_filename,'\',1) <> 0 then
		str_len = len(a_filename)
		for i = str_len to 1 step -1
			 if mid(a_filename,i,1) = "\" then
					filename = mid(a_filename,i + 1)
					exit
			 end if  
		next
	end if 
	
	if not g_main_application then

		f_check_sql_error_batch(sqlca,"Deleting from PP_MAIL_DATA")
	else
		f_check_sql_error(sqlca,"Deleting from PP_MAIL_DATA")
	end if
	
	delete from pp_mail_data where sessionid = userenv('SESSIONID');
	insert into pp_mail_data(sessionid,filename,datalen) values(userenv('SESSIONID'),:filename,:tot);
	
	if not g_main_application then
		f_check_sql_error_batch(sqlca,"Inserting into PP_MAIL_DATA")
	else
		f_check_sql_error(sqlca,"Inserting into PP_MAIL_DATA")
	end if
	
	updateblob pp_mail_data set filedata=:data where sessionid = userenv('SESSIONID') and filename = :filename;
	
		
	if not g_main_application then
		f_check_sql_error_batch(sqlca,"Updating into PP_MAIL_DATA")
	else
		f_check_sql_error(sqlca,"Updating into PP_MAIL_DATA")
	end if
	
	commit;
return true
end function

public function boolean add_attachment_blob (string a_attachment_name, blob a_blob);

longlong len
len = len(a_blob)

delete from pp_mail_data where sessionid = userenv('SESSIONID');
insert into pp_mail_data(sessionid,filename,datalen) values(userenv('SESSIONID'),:a_attachment_name,:len);
	
if not g_main_application then
	f_check_sql_error_batch(sqlca,"Inserting into PP_MAIL_DATA")
else
	f_check_sql_error(sqlca,"Inserting into PP_MAIL_DATA")
end if

updateblob pp_mail_data set filedata=:a_blob where sessionid = userenv('SESSIONID') and filename = :a_attachment_name;

if not g_main_application then
	f_check_sql_error_batch(sqlca,"updating  PP_MAIL_DATA")
else
	f_check_sql_error(sqlca,"updating into PP_MAIL_DATA")
end if	
if not g_main_application then
	f_check_sql_error_batch(sqlca,"Updating into PP_MAIL_DATA")
else
	f_check_sql_error(sqlca,"Updating into PP_MAIL_DATA")
end if
	
return true
end function

public function boolean add_attachment_sql (string a_attachment_name, string a_sql);string sql
longlong len
len = len(a_sql)
delete from pp_mail_data where sessionid = userenv('SESSIONID');
insert into pp_mail_data(sessionid,filename,datalen) values(userenv('SESSIONID'),:a_attachment_name,:len);
	
if not g_main_application then
	f_check_sql_error_batch(sqlca,"Inserting into PP_MAIL_DATA")
else
	f_check_sql_error(sqlca,"Inserting into PP_MAIL_DATA")
end if


sql = "update pp_mail_data set filedata= (" + a_sql + " ) where sessionid = userenv('SESSIONID') and filename = '" + a_attachment_name+ "'"
execute immediate :sql;
		
if not g_main_application then
	f_check_sql_error_batch(sqlca,"Updating into PP_MAIL_DATA")
else
	f_check_sql_error(sqlca,"Updating into PP_MAIL_DATA")
end if
	
	
return true
end function

public function boolean send (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, boolean a_delete_files);
return sendFile(a_fromUser, a_password, a_subject, a_text, a_toUser, a_server, "", a_delete_files)
end function

public function boolean sendfile (string a_fromuser, string a_password, string a_subject, string a_text, string a_touser, string a_server, string a_filename, boolean a_delete_files);//
//   a_fromuser   string - from mail id if null use sqlca.logid and lookup mail address   
//   a_password   string - not used
//   a_subject    string - subject of mail message
//   a_text       string - mail message
//   a_touser     string - to mail id  if touser does not contain a @ char, lookup mail address
//   a_server     string - smtp mail server if null get host name from SMTP_HOST in the pwrplant.ini file

//   a_filename   string - filename used on the attachment
//

longlong longResult
longlong data_len
int unit
string touser,fromuser,server
blob data,data_temp
environment env
string err
getenvironment(env) 
string mail_user
string users[]
string mail_id
longlong count,num

string userid
longlong pos
count = 0
string ls_email_list

userid = lower(sqlca.userid)

touser = trim(a_touser)
fromuser = a_fromuser

if touser = '' then
	touser = sqlca.logid
end if

count = 0 //JRD - moved from below, the setting to zero caused issue with the multiple users

//Multi Users
if pos(touser,';') > 0 then

   pos = pos(touser,';')
   do while  pos > 0 
      count++
      users[count] = mid(touser,1,pos - 1)
      touser = mid(touser,pos + 1)
      pos = pos(touser,';')
   loop
	if touser <> "" then //JRD 04-24-2008 add if
	   count++
	   users[count] = touser	
	end if
end if


// is this a real mail address

if uf_get_user(fromuser) = false then
     if g_main_application then
        MessageBox ("Mail ID", 'No Mail ID exist for ' + fromuser )
	  else
	     f_pp_msgs("No Mail ID exist for " + fromuser)
	end if 
   return false
end if

//count = 0 //JRD - moved above, the setting to zero caused issue with the multiple users

select count(*) into :num from pp_security_groups where groups = lower(:touser);
if num > 0 then
	declare user_cur cursor for  select u.mail_id from pp_security_users u ,pp_security_users_groups g
	             where  rtrim(u.users) = rtrim(g.users) and g.groups = lower(:touser);
	open user_cur;
	fetch user_cur into :mail_id;
	do while  sqlca.sqlcode = 0 
		if isnull(mail_id) = false then
			count++
		   users[count] = mail_id
		end if
		fetch user_cur into :mail_id;
	loop
	close user_cur;
	if count = 0 then
	      if g_main_application then
		    MessageBox ("Mail IDs", 'No Mail IDs exist for the group ' + touser )
		  else
		     f_pp_msgs('No Mail IDs exist for the group ' + touser)
		  end if
   	 return false
   end if
elseif uf_get_user(touser) = false and touser <> "" then //JRD 04-24-08 add and
	if g_main_application then
        MessageBox ("Mail ID", 'No Mail ID exist for ' + touser )
	 else
	    f_pp_msgs('No Mail ID exist for ' + touser )
	 end if
   return false
end if
server = a_server
uf_get_host(server)

if i_database_mail = false then 
   uf_load_library("smtpmail.dll")
end if

longlong tot,bytes_read
longlong str_len,i
string str
longlong code

// Read the file

if i_database_mail = true then // save the file to a table
	if a_fileName <> "" and not isNull(a_fileName) then
   	if add_attachment_file(a_filename) = false then
			select count(*) into :num from pp_mail_data where sessionid = userenv('SESSIONID') ;
			if  num > 0 then 
		   	a_filename = 'data '
	   	else
		   	a_filename = ' '
			end if
		end if
	end if
end if

if count = 0 then // its a single email id
  if i_database_mail = true then
      code = sqlca.pp_send_mail(fromuser, i_userid,  i_passwd,a_subject, a_text, touser,server, a_filename);	
		
			
		 if not g_main_application then

			f_check_sql_error_batch(sqlca,"Sending mail Message thru the Database")
		else
			f_check_sql_error(sqlca,"Sending mail Message thru the Database")
		end if
		delete pp_mail_data where sessionid = userenv('SESSIONID');
		
  else
		if i_passwd_flag = true then
			  longResult= uo_winapi.uf_smtpmailpasswdex(fromuser,i_userid,i_passwd,a_subject,a_text,users[i],server,a_filename,err)
		else
			  longResult= uo_winapi.uf_smtpmailex(fromuser,a_subject,a_text,touser,server,a_filename,err)
		end if 
 end if
	
else // multiple email ids. convert the array into string
	 if i_database_mail = true then
		ls_email_list = g_string_func.of_parsestringarrayintostring(users)
		ls_email_list = g_string_func.of_replace_string(ls_email_list, ",", ";", "all")
		
		sqlca.pp_send_mail(fromuser, i_userid,  i_passwd,a_subject, a_text, ls_email_list, server, a_filename);
	 else
		for i = 1 to upperbound(users)
			if i_passwd_flag = true then
				 longResult= uo_winapi.uf_smtpmailpasswdex(fromuser,i_userid,i_passwd,a_subject,a_text,users[i] ,server,a_filename,err)
			else
				 longResult= uo_winapi.uf_smtpmailex(fromuser,a_subject,a_text,users[i], server,a_filename,err)
			end if
		next
	end if

	 delete pp_mail_data where sessionid = userenv('SESSIONID');
	 
	 if sqlca.sqlcode < 0 then 
		 if g_main_application then
				 MessageBox ("Mail IDs", 'No Mail IDs exist for the group ' + touser )
		  else
				f_pp_msgs('No Mail IDs exist for the group ' + touser)
		  end if
	end if
end if
 
if a_delete_files then
	delete PP_MAIL_DATA where SESSIONID = USERENV('SESSIONID');
	commit;
end if

if	longResult=	0	then
	RETURN	true
else
	RETURN	false
end if



return true
end function

on uo_smtpmail.create
call super::create
end on

on uo_smtpmail.destroy
call super::destroy
end on

