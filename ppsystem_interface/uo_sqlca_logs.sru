HA$PBExportHeader$uo_sqlca_logs.sru
$PBExportComments$v10.3 Fixed single sign-on problem with creating online logs.
forward
global type uo_sqlca_logs from uo_sqlca
end type
end forward

global type uo_sqlca_logs from uo_sqlca
end type
global uo_sqlca_logs uo_sqlca_logs

forward prototypes
public function longlong uf_connect ()
public function longlong uf_disconnect ()
end prototypes

public function longlong uf_connect ();//*****************************************************************************************
//
//  Function  :  uf_connect
//
//
//  Args      :  None
//
//  Returns   :  SQLCODE 
//
//*****************************************************************************************



this.ServerName = sqlca.ServerName
this.DBMS       = sqlca.DBMS
if lower(mid(this.dbms,1,3)) = 'trs' then
	this.dbms = mid(this.dbms,5)
elseif 	 lower(mid(this.dbms,1,5)) = 'trace' then
	this.dbms = mid(this.dbms,7)
end if

this.LogId      = sqlca.LogId
this.LogPass    = sqlca.LogPass
this.dbparm    = sqlca.dbparm 

// ### 8056: JAK: 2011-08-11:  If password is expired, don't warn for the logs connection..
if pos(upper(this.dbparm),'PWDIALOG=1') > 0 then
	this.dbparm = mid(this.dbparm,1,pos(upper(this.dbparm),'PWDIALOG=1')-1) + 'PwDialog=0' + mid(this.dbparm,pos(upper(this.dbparm),'PWDIALOG=1')+10)
end if
if pos(upper(this.dbparm),'PWEXPDIALOG=1') > 0 then
	this.dbparm = mid(this.dbparm,1,pos(upper(this.dbparm),'PWEXPDIALOG=1')-1) + 'PwExpDialog=0' + mid(this.dbparm,pos(upper(this.dbparm),'PWEXPDIALOG=1')+13)
end if

// sso 

if sqlca.userid = '' or isnull(sqlca.userid) then
   this.logid = sqlca.logid
elseif lower(sqlca.UserID) <> lower(sqlca.logid) then
   logid = this.logid
   this.logid = sqlca.UserID

else
   this.logid = sqlca.logid
end if

this.logpass = sqlca.logpass


connect using this;
this.logid =  logid
//  FOR DEBUGGING ONLY ...
//if this.SQLCode = 0 then
//   messagebox("", "uo_sqlca_logs Connected to PowerPlant. (" + this.ServerName + ")")
//else
//   messagebox("", "uo_sqlca_logs Error connecting to PowerPlant. (" + &
//                            this.ServerName + ")" + this.SQLErrText)
//end if

return this.SQLCode

end function

public function longlong uf_disconnect ();//*****************************************************************************************
//
//  Function  :  uf_disconnect
//
//
//  Args      :  None
//
//  Returns   :  SQLCODE 
//
//*****************************************************************************************
disconnect using this;

//  FOR DEBUGGING ONLY ...
//if this.SQLCode = 0 then
//	messagebox("", "uo_sqlca_logs disconnected from PowerPlant. (" + this.ServerName + ")")
//else
//	messagebox("", "uo_sqlca_logs Error disconnecting from PowerPlant. (" + &
//	                         this.ServerName + ")" + this.SQLErrText)
//end if

return this.SQLCode
end function

on uo_sqlca_logs.create
call super::create
end on

on uo_sqlca_logs.destroy
call super::destroy
end on

