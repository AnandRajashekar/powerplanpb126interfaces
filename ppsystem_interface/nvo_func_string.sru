HA$PBExportHeader$nvo_func_string.sru
forward
global type nvo_func_string from nonvisualobject
end type
end forward

shared variables
////
////	Empty arrays, used by of_empty_array
////
		boolean		sv_empty_any_array[]
		boolean		sv_empty_boolean_array[]
		date			sv_empty_date_array[]
		datetime		sv_empty_datetime_array[]
		decimal		sv_empty_decimal_array[]
		double		sv_empty_double_array[]
		integer		sv_empty_integer_array[]
		longlong	sv_empty_long_array[]
		longlong		sv_empty_longlong_array[]
		string			sv_empty_string_array[]




end variables

global type nvo_func_string from nonvisualobject
end type
global nvo_func_string nvo_func_string

type variables


end variables

forward prototypes
public function string of_parsestringarrayintostring (any a_values[])
public function string of_parsenumarrayintostring (any a_values[])
public function string of_parsearrayby254notin (any a_values[], string a_type, string a_field)
public function string of_parsearrayby254 (any a_values[], string a_type, string a_field)
public function string of_parsearrayby1000 (any a_values[], string a_type, string a_field)
public function string of_parse_keycode_to_string (keycode a_key)
public function string of_lpad (string a_string, integer a_length, string a_chars)
public function any of_pp_nvl (any a_value, any a_replacement_value)
public function string of_replace_string (string a_old_string, string a_search_string, string a_replace_string, string a_how_many)
public function string of_rpad (string a_string, integer a_length, string a_chars)
public function decimal of_string_to_decimal (string a_string)
public function longlong of_count_string (string str1, string str2)
public function integer of_delete_from_array (ref longlong a_arr[], longlong a_index)
public function integer of_parsestringintonumarray (string stringtoparse, string separator, ref longlong array_to_hold[])
public function integer of_parsestringintostringarray (string stringtoparse, string separator, ref string array_to_hold[])
public subroutine of_split (string a_string_to_split, string a_string_to_split_on, ref string a_array_split_strings[])
public subroutine of_empty_array (ref any a_array[])
public subroutine of_empty_array (ref boolean a_array[])
public subroutine of_empty_array (ref date a_array[])
public subroutine of_empty_array (ref datetime a_array[])
public subroutine of_empty_array (ref decimal a_array[])
public subroutine of_empty_array (ref double a_array[])
public subroutine of_empty_array (ref integer a_array[])
public subroutine of_empty_array (ref longlong a_array[])
public subroutine of_empty_array (ref string a_array[])
public function integer of_sort_array_long (ref longlong a_long_array[], string a_asc_desc)
public function longlong of_delete_from_array (ref string a_arr[], longlong a_index)
public function longlong of_delete_from_array (boolean a_arr[], longlong a_index)
public function integer of_arrayunique (string as_sourcevalues[], ref string as_uniquevalues[], boolean ab_ignorecase)
end prototypes

public function string of_parsestringarrayintostring (any a_values[]);//******************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Function :   of_parsestringarrayintostring
//
//   Purpose  :   parses an array of strings into string delimited by commas
//
//	  Arguments:	a_values[]     :  array of values
//
//
//	  Return Codes:	 a_string = If successful, returns the comma sperated string
//
//		         	 
//   DATE		    NAME		     REVISION                     CHANGES
//  --------      --------     -----------   -----------------------------------------------
//  03-18-04      PowerPlan  	 Version 2.3   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*******************************************************************************************


longlong array_size, i, count
string a_string


array_size = UpperBound(a_values)

if array_size > 0 then // do not put the comma before the first elements
	a_string = String(a_values[1])
end if

for i = 2 to array_size
			/* matt - this next line protects against single quotations */
//			a_values[i] = f_replace_string(a_values[i],"'","''",'all')
			a_string = a_string + "," + a_values[i]
next

return a_string

end function

public function string of_parsenumarrayintostring (any a_values[]);//******************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Function :   of_parsenumarrayintostring
//
//   Purpose  :   parses an array of numbers into a comma seperated string
//
//	  Arguments:	a_values[]     :  array of values
//
//
//
//	  Return Codes:	 a_string = If successful, returns the constructed 'IN' clause
//
//		         	 
//   DATE		    NAME		     REVISION                     CHANGES
//  --------      --------     -----------   -----------------------------------------------
//  03-12-04      PowerPlan  	 Version 2.3   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*******************************************************************************************

longlong array_size, i, count
string a_string


array_size = UpperBound(a_values)

if array_size > 0 then // do not put the comma before the first elements
	a_string = a_string + String(a_values[1])
end if

for i = 2 to array_size
	a_string = a_string + "," + String(a_values[i])
next

return a_string
end function

public function string of_parsearrayby254notin (any a_values[], string a_type, string a_field);/************************************************************************************************************************************************************
 **	
 **	of_parseArrayBy254NotIn()
 **	
 **	Parses an array of values into a NOT IN string that won't violate 254 item limit of an SQL in/not in clause.  Works like the of_parseArrayBy254() function except this version creates a NOT IN
 **	clause as opposed to the IN clause created by the of_parseArrayBy254() function.
 **	
 **	Parameters	:	any		:	(a_values[]) Array of values to parse.
 **						string		:	(a_type) Type of value being passed: "N" = number, "C" = character, "D" = data, "M" = month number
 **						string		:	(a_field) Name of the field being qualified.
 **	
 **	Returns		:	string		:	contructed in clause for appending to an SQL statement.
 **	
 ************************************************************************************************************************************************************/

/*
 *	Processing.
 */
	longlong	i, maxi
	string	sqls

////
////	Don't error.
////
			if upperBound( a_values[] ) = 0 then return ""
			
			choose case upper( trim( a_type ) )
				case	"N", "C", "D", "M"
					// we're good
				case	else
					return ""
			end choose

////
////	Initialize the string.
////
			sqls = "( " + a_field + " not in ( "

////
////	Loop over the array and build our NOT IN clause.
////
			maxi = upperBound( a_values[] )
			
			for i = 1 to maxi
				choose case upper( trim( a_type ) )
					case	"N"
						sqls = sqls + string( a_values[i] )
					case	"C"
						a_values[i] = g_string_func.of_replace_string( a_values[i], "'", "''", "all" )
						sqls = sqls + "'" + a_values[i] + "'"
					case	"D"
						sqls = sqls + "to_date( '" + string( a_values[i], "yyyymmdd" ) + "', 'YYYYMMDD' )"
					case	"M"
						sqls = sqls + "to_date( '" + string( a_values[i], "yyyymm" ) + "', 'YYYYMM' )"
				end choose
				
				if i = maxi then
					sqls = sqls + " ) ) "
				else
					if mod( i, 254 ) = 0 then
						sqls = sqls + " ) and " + a_field + " not in ( "
					else
						sqls = sqls + ", "
					end if
				end if
			next

////
////	Return our result.
////
			return sqls
end function

public function string of_parsearrayby254 (any a_values[], string a_type, string a_field);//******************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Function :   of_parsearrayby254
//
//   Purpose  :   parses an array of values into string that won't violate 254 limit of 
//                SQL 'IN' clause.
//
//	  Arguments:	a_values[]     :  array of values
//
//						a_type   		:	Type of values:   N = Number; C = Character; D = Date
//
//						a_field			:	The name of the field being qualified.
//
//
//	  Return Codes:	 in_clause = If successful, returns the constructed 'IN' clause
//
//		         	 
//   DATE		    NAME		     REVISION                     CHANGES
//  --------      --------     -----------   -----------------------------------------------
//  01-22-99      PowerPlan  	 Version 2.3   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*******************************************************************************************

string in_clause
longlong array_size, i, count
boolean first_group, first_value

first_group = TRUE
first_value = TRUE
count = 0
array_size = UpperBound(a_values)

for i = 1 to array_size
	
	if count = 0 then 
		// check if this is first group, to open groupings with "("
		// if not close last group and paste the "or"
		if first_group then 
			in_clause = "("
			first_group = FALSE
		else
			in_clause = in_clause + ") or "
		end if
		
		in_clause = in_clause + a_field + " in ("
		
		first_value = TRUE
	end if
	
	if first_value then
		// check if this is first value of a group, if not paste ","
		first_value = FALSE
	else
		in_clause = in_clause + ","
	end if
	
	count++
	
	choose case a_type
		case "N"
			in_clause = in_clause + String(a_values[i])
		case "C"
			/* matt - this next line protects against single quotations */
			a_values[i] = g_string_func.of_replace_string(a_values[i],"'","''",'all')
			in_clause = in_clause + "'" + a_values[i] + "'"
		case "D"
			in_clause = in_clause + "to_date('" + String(a_values[i],"yyyymmdd") + "','YYYYMMDD')"
		case "M"
			in_clause = in_clause + "to_date('" + String(a_values[i],"yyyymm") + "','YYYYMM')"
	end choose
	
	if count = 254 then
		// last value for group, reset counter
		count = 0
	end if
	
next

// if anything build close groupings
if not (in_clause = "" or IsNull(in_clause)) then in_clause = in_clause + "))"

return in_clause
end function

public function string of_parsearrayby1000 (any a_values[], string a_type, string a_field);//******************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Function :   of_parsearrayby254
//
//   Purpose  :   parses an array of values into string that won't violate 254 limit of 
//                SQL 'IN' clause.
//
//	  Arguments:	a_values[]     :  array of values
//
//						a_type   		:	Type of values:   N = Number; C = Character; D = Date
//
//						a_field			:	The name of the field being qualified.
//
//
//	  Return Codes:	 in_clause = If successful, returns the constructed 'IN' clause
//
//		         	 
//   DATE		    NAME		     REVISION                     CHANGES
//  --------      --------     -----------   -----------------------------------------------
//  01-22-99      PowerPlan  	 Version 2.3   Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*******************************************************************************************

string in_clause
longlong array_size, i, count
boolean first_group, first_value

first_group = TRUE
first_value = TRUE
count = 0
array_size = UpperBound(a_values)

for i = 1 to array_size
	
	if count = 0 then 
		// check if this is first group, to open groupings with "("
		// if not close last group and paste the "or"
		if first_group then 
			in_clause = "("
			first_group = FALSE
		else
			in_clause = in_clause + ") or "
		end if
		
		in_clause = in_clause + a_field + " in ("
		
		first_value = TRUE
	end if
	
	if first_value then
		// check if this is first value of a group, if not paste ","
		first_value = FALSE
	else
		in_clause = in_clause + ","
	end if
	
	count++
	
	choose case a_type
		case "N"
			in_clause = in_clause + String(a_values[i])
		case "C"
			/* matt - this next line protects against single quotations */
			a_values[i] = g_string_func.of_replace_string(a_values[i],"'","''",'all')
			in_clause = in_clause + "'" + a_values[i] + "'"
		case "D"
			in_clause = in_clause + "to_date('" + String(a_values[i],"yyyymmdd") + "','YYYYMMDD')"
		case "M"
			in_clause = in_clause + "to_date('" + String(a_values[i],"yyyymm") + "','YYYYMM')"
	end choose
	
	if count = 1000 then
		// last value for group, reset counter
		count = 0
	end if
	
next

// if anything build close groupings
if not (in_clause = "" or IsNull(in_clause)) then in_clause = in_clause + "))"

return in_clause
end function

public function string of_parse_keycode_to_string (keycode a_key);
//   Function :   of_parse_keycode_to_string
//
//   Purpose  :   to return a string given a keycode 
//                     
//   Scope    :   global
//
//   Arguments:   a_key          : keycode
//
//   Returns :    ls_key          : string  the key pressed as a string
//
//	  Notes: THIS FUNCTION IS NOT CASE SENSITIVE, WILL ALWAYS RETURN A CAPITAL LETTER

string ls_key

ls_key = ""



CHOOSE CASE a_key

      // A-Z.

      CASE keyA!

          ls_Key = 'A'

      CASE keyB!

          ls_Key = 'B'

      CASE keyC!

          ls_Key = 'C'

      CASE keyD!

          ls_Key = 'D'

      CASE keyE!

          ls_Key = 'E'
		
		CASE keyF!

          ls_Key = 'F'

      CASE keyG!

          ls_Key = 'G'

      CASE keyH!

          ls_Key = 'H'

      CASE keyI!

          ls_Key = 'I'

      CASE keyJ!

          ls_Key = 'J'
		
		CASE keyK!

          ls_Key = 'K'

      CASE keyL!

          ls_Key = 'L'

      CASE keyM!

          ls_Key = 'M'

      CASE keyN!

          ls_Key = 'N'

      CASE keyO!

          ls_Key = 'O'
		
		CASE keyP!

          ls_Key = 'P'

      CASE keyQ!

          ls_Key = 'Q'

      CASE keyR!

          ls_Key = 'R'

      CASE keyS!

          ls_Key = 'S'

      CASE keyT!

          ls_Key = 'T'
			 
		CASE keyU!

          ls_Key = 'U'

      CASE keyV!

          ls_Key = 'V'

      CASE keyW!

          ls_Key = 'W'

      CASE keyX!

          ls_Key = 'X'

      CASE keyY!

          ls_Key = 'Y'
		
		CASE keyZ!

          ls_Key = 'Z'
			 
	
	//numbers
		CASE Key0!, KeyNumPad0!
			if keydown(KeyShift!) then
				ls_Key = ")"
			else
  			 ls_Key ="0"
			end if
		
		CASE Key1!
  			if keydown(KeyShift!) then
				ls_Key = "!"
			else
			 ls_Key ="1"
		end if
		CASE Key2!
  			if keydown(KeyShift!) then
				ls_Key = "@"
			else
  			 ls_Key ="2"
			end if
		CASE Key3!
  	       if keydown(KeyShift!) then
				ls_Key = "#"
			else
			 ls_Key ="3"
		end if
		CASE Key4!
  				if keydown(KeyShift!) then
				ls_Key = "$"
			else
  			 ls_Key ="4"
			end if
		CASE Key5!
        if keydown(KeyShift!) then
				ls_Key = "%"
			else
		    ls_Key ="5"
		end if
		CASE Key6!
		 if keydown(KeyShift!) then
				ls_Key = "^"
			else
		 	 ls_Key ="6"
		end if
		CASE Key7!
  if keydown(KeyShift!) then
				ls_Key = "&"
			else
  		    ls_Key ="7"
			end if
		CASE Key8!
  			if keydown(KeyShift!) then
				ls_Key = "*"
			else
			 ls_Key ="8"
		end if
		CASE Key9!
  		if keydown(KeyShift!) then
				ls_Key = "("
			else
		  	 ls_Key ="9"
			end if
			
	//numpad
	
	CASE KeyNumPad0!
  ls_Key ="0"
 CASE KeyNumPad1!
  ls_Key ="1"
 CASE KeyNumPad2!
  ls_Key ="2"
 CASE KeyNumPad3!
  ls_Key ="3"
 CASE KeyNumPad4!
  ls_Key ="4"
 CASE KeyNumPad5!
  ls_Key ="5"
 CASE KeyNumPad6!
  ls_Key ="6"
 CASE KeyNumPad7!
  ls_Key ="7"
 CASE KeyNumPad8!
  ls_Key ="8"
 CASE KeyNumPad9!
  ls_Key ="9"
  
//space

 CASE KeySpaceBar!
  ls_Key = " "

	//misc
	
		Case KeySlash!
			if keydown(KeyShift!) then
				ls_Key = "?"
			else
				
			 ls_Key = "/"
		end if
		Case KeyBackSlash! 
			if keydown(KeyShift!) then
				ls_Key = "|"
			else
			 ls_Key = "\"
			end if 
		Case KeyEqual!     
			if keydown(KeyShift!) then
				ls_Key = "+"
			else
			 ls_Key = "=" 
			 end if
		Case KeyComma!
			if keydown(KeyShift!) then
				ls_Key = "<"
			else
			ls_Key = ","
			end if
		Case KeyDash!
			if keydown(KeyShift!) then
				ls_Key = "_"
			else
			ls_Key = "-"
		end if
		Case KeyPeriod!    
			if keydown(KeyShift!) then
				ls_Key = ">"
			else
			ls_Key = "." 
			end if
		Case KeyBackQuote!    
			if keydown(KeyShift!) then
				ls_Key = "~~"
			else
			ls_Key = "`"
			end if
		Case KeyLeftBracket!    
			if keydown(KeyShift!) then
				ls_Key = "{"
			else
			ls_Key = "["
			end if
		Case KeyRightBracket!    
			if keydown(KeyShift!) then
				ls_Key = "}"
			else
			ls_Key = "]" 
			end if
		Case KeySemiColon!
			if keydown(KeyShift!) then
				ls_Key = ":"
			else
			ls_Key = ";" 
			end if
		Case KeyQuote!     
			if keydown(KeyShift!) then
				ls_Key = "~""
			else
			ls_Key = "'"
			end if
		Case KeyShift!
			ls_Key = ""
			
end choose
	
	return ls_key
end function

public function string of_lpad (string a_string, integer a_length, string a_chars);//	*****************************************************
//
//		similar to SQL's lpad().
//
//		ARGUMENTS:
//		a_string		:	string	:	starting string
//		a_length		:	integer	:	desired final length
//		a_chars		:	string	:	set of characters to insert before a_string
//
//		RETURN:
//		a_string		:	string	:	starting string w/ inserted characters
//
//	*****************************************************

return fill(a_chars, a_length - len(a_string)) + a_string
end function

public function any of_pp_nvl (any a_value, any a_replacement_value);/****************************************************************************************
 ****
 ****	of_pp_nvl						:	Similar to the SQL nvl function.  This function takes a value
 ****										and checks for nulls or empty strings and replaces it with
 ****										a the given replacement string.  The function assumes that 
 ****										the replacement string's type is the type of the value
 ****										being replaced.  Otherwise an error occur.  For instance, if
 ****										a longlong is null and you put the replacement value in quotes
 ****										when passing the argument, it will throw an error because
 ****										it interprets the replacement value as a string since it was
 ****										passed in quotes.
 ****
 ****	ARGUMENTS
 ****		a_value					:	any		:	The value you want to check for nulls
 ****
 ****		a_replacement_value	:	any		:	The value you want to replace the nulls with
 ****														for strings: of_pp_nvl( a_value,"a_replacement_value" ) - quotes
 ****														for numbers: of_pp_nvl( a_value, a_replacement_value ) - no quotes
 ****
 ****
 ****	RETURNS						:	any		:	Returns the any value of the replaced string
 ****														or null if an error occurs.
 ****		
 **** AUTHOR						:	Jason Crane
 ****
 **** DATE							:	10/11/2005
 ****
 ****************************************************************************************/
string	var_type

var_type = ClassName(a_replacement_value)

CHOOSE CASE var_type
	CASE "dec", "double", "integer", "long", "real"
		
		if isnull(a_value) then
				a_value = a_replacement_value
		end if
		return a_value
		
	CASE "string"
		
		if isnull(a_value) or a_value = '' then
			a_value = a_replacement_value
		end if
		return a_value
		
	CASE ELSE 
		setnull(a_value)
		return a_value
		
END CHOOSE
end function

public function string of_replace_string (string a_old_string, string a_search_string, string a_replace_string, string a_how_many);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:   system
//
//   Function :  	of_replace_string
//
//	  Purpose  :	replace all ocurrences of a substring in a string with another substring
//		             
//   Scope    :	global
//
//   Arguments:	a_old_string     : string  : original string 
//                a_search_string  : string  : substring to replace  
//                a_replace_string : string  : substring to replace a_search_string with  
//                a_how_many       : string  : 'first' or 'all' ocurrences
// 
//		
//   Returns :    a_old_string     : string  : changed string 
//
//
//     DATE		     NAME		REVISION                         CHANGES
//   ---------    --------   -----------    ----------------------------------------------- 
//    05-15-94    PowerPlan  	Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//****************************************************************************************** 

// replace all ocurrences of a string with another string
 
longlong start_pos=1
 
//find the first occurrence of a_search_string

start_pos = Pos(a_old_string, a_search_string, start_pos) 

//only enter the loop if you find a_search_string

do while start_pos > 0

	//replace a_search_string with a_replace_string

	a_old_string = Replace(a_old_string, start_pos, Len(a_search_string), a_replace_string)

	//find the next occurrence of a_search_string

   if a_how_many = 'first' then 

     exit
   
  else

	start_pos = Pos(a_old_string, a_search_string, start_pos+Len(a_replace_string))

  end if
 
loop

return a_old_string

end function

public function string of_rpad (string a_string, integer a_length, string a_chars);//	*****************************************************
//
//		similar to SQL's rpad().
//
//		ARGUMENTS:
//		a_string		:	string	:	starting string
//		a_length		:	integer	:	desired final length
//		a_chars		:	string	:	set of characters to append
//
//		RETURN:
//		a_string		:	string	:	starting string w/ appended characters
//
//	*****************************************************


return	a_string + fill(a_chars, a_length - len(a_string) )

end function

public function decimal of_string_to_decimal (string a_string);string str
dec {2} dec1
 

str = a_string
 

str= g_string_func.of_replace_string(str,"'", "", 'all')
str = g_string_func.of_replace_string(str, ")", "", 'all')
str = g_string_func.of_replace_string(str, "(", "-", 'first')
str = g_string_func.of_replace_string(str, "$", "", 'first')
dec1 = dec(str)

return dec1

 

end function

public function longlong of_count_string (string str1, string str2);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   of_count_string
//
//   Purpose  :   counts the number of occurances of str2 in str1
//
//   Scope    :   global
//
//   Arguments:   str1 : string  : input string
// 					str2 : string	: string to count within str1
//
//   Returns :    return_number   :  number   : number of str2 found in str1
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    02-28-97    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

longlong return_number, length1, length2, i


length1 = len(str1)
length2 = len(str2)

return_number = 0
for i = 1 to length1
	if (mid(str1,i,length2) = str2) then
		return_number = return_number + 1
	end if
next

return return_number


end function

public function integer of_delete_from_array (ref longlong a_arr[], longlong a_index);////////////////////
////
////	of_delete_from_array	:	deletes a node from an array, moving all other nodes
////									up one index
////
////	arguments				:	a_arr		:	array to delete a node from; type: long
////									a_index	:	node index to delete from the array
////
////	return					:	value delete from the array if it succeeds, if it
////									fails it returns a -1
////
////////////////////

longlong	i, j, max, rtn
longlong	temp_arr[]

setPointer( Hourglass! )

max	=	upperBound( a_arr[] )
i		=	1

if a_index > max or a_index < 1 then
	return -1
end if

rtn = a_arr[a_index]

for i = 1 to a_index - 1
	temp_arr[i] = a_arr[i]
next

j = upperBound( temp_arr[] ) + 1
for i = a_index + 1 to max
	temp_arr[j] = a_arr[i]
	j++
next

a_arr = temp_arr

return rtn
end function

public function integer of_parsestringintonumarray (string stringtoparse, string separator, ref longlong array_to_hold[]);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :  of_parsestringintoarray
//
//   Purpose  :This function will parse string  into an array of elements denoted
//					by the separator.  The number of elements parsed is the return value of
//					the function.  For example, if ~t were the separator, "red~twhite~tblue" 
//					would be parsed into an array containing red, white and blue.  The value
//					3 would be returned.
//
//
//   
//  Arguments :	stringtoparse ;		string			      : string to parse
//						separator	  ;		string			      : what to look for
//						array_to_hold : 		string array(by ref) : gets filled with values
//
//   
//   Returns : count    : int		:the number of elements returned into the array.
//											 If separator is not found then the original string is 
//                                placed in array_to_hold and 1 is returned
//
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    04-15-95    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

 
  

//###AYP: Maint 7506: Change INT to LONG
//int separator_pos, separator_len, count, start_pos=1
longlong separator_pos, separator_len, count, start_pos=1
string holder

separator_len = Len(separator)
separator_pos =  Pos(stringtoparse,separator,start_pos)
IF Trim(stringtoparse) = "" or IsNull(stringtoparse) THEN 
	RETURN 0
ELSEIF separator_pos = 0 THEN 
	array_to_hold[1] = longlong(stringtoparse)
	RETURN 1
END IF
DO WHILE separator_pos > 0
	holder = Mid(stringtoparse,start_pos,(separator_pos - start_pos))
	count++
	array_to_hold[count] = longlong(holder)
	start_pos = separator_pos + separator_len
	separator_pos =  Pos(stringtoparse,separator,start_pos)
LOOP
holder = Mid(stringtoparse,start_pos,Len(stringtoparse))
count = count + 1
array_to_hold[count] = longlong(holder)

RETURN count


end function

public function integer of_parsestringintostringarray (string stringtoparse, string separator, ref string array_to_hold[]);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :  of_parsestringintoarray
//
//   Purpose  :This function will parse string  into an array of elements denoted
//					by the separator.  The number of elements parsed is the return value of
//					the function.  For example, if ~t were the separator, "red~twhite~tblue" 
//					would be parsed into an array containing red, white and blue.  The value
//					3 would be returned.
//
//
//   
//  Arguments :	stringtoparse ;		string			      : string to parse
//						separator	  ;		string			      : what to look for
//						array_to_hold : 		string array(by ref) : gets filled with values
//
//   
//   Returns : count    : int		:the number of elements returned into the array.
//											 If separator is not found then the original string is 
//                                placed in array_to_hold and 1 is returned
//
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    04-15-95    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

 
  

//###AYP: Maint 7506: Change INT to LONG
//int separator_pos, separator_len, count, start_pos=1
longlong separator_pos, separator_len, count, start_pos=1
string holder

separator_len = Len(separator)
separator_pos =  Pos(stringtoparse,separator,start_pos)
IF Trim(stringtoparse) = "" or IsNull(stringtoparse) THEN 
	RETURN 0
ELSEIF separator_pos = 0 THEN 
	array_to_hold[1] = stringtoparse
	RETURN 1
END IF
DO WHILE separator_pos > 0
	holder = Mid(stringtoparse,start_pos,(separator_pos - start_pos))
	count++
	array_to_hold[count] = holder
	start_pos = separator_pos + separator_len
	separator_pos =  Pos(stringtoparse,separator,start_pos)	
LOOP
holder = Mid(stringtoparse,start_pos,Len(stringtoparse))
count = count + 1
array_to_hold[count] = holder

RETURN count


end function

public subroutine of_split (string a_string_to_split, string a_string_to_split_on, ref string a_array_split_strings[]);longlong end_pos 
string tail

end_pos = pos(a_string_to_split,a_string_to_split_on) 
if end_pos <= 0 then
	if not isnull(a_string_to_split) and a_string_to_split <>"" then
		a_array_split_strings[UpperBound(a_array_split_strings) + 1] = a_string_to_split
	end if
	return
else
	a_array_split_strings[UpperBound(a_array_split_strings) + 1] = left(a_string_to_split,end_pos - 1)
	tail = right(a_string_to_split,len(a_string_to_split) - end_pos)
	of_split(tail,a_string_to_split_on,ref a_array_split_strings)
end if

end subroutine

public subroutine of_empty_array (ref any a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref any		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_any_array[]
end subroutine

public subroutine of_empty_array (ref boolean a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref boolean		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_boolean_array[]
end subroutine

public subroutine of_empty_array (ref date a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref date		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_date_array[]
end subroutine

public subroutine of_empty_array (ref datetime a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref datetime		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_datetime_array[]
end subroutine

public subroutine of_empty_array (ref decimal a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref decimal		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_decimal_array[]
end subroutine

public subroutine of_empty_array (ref double a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref double		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_double_array[]
end subroutine

public subroutine of_empty_array (ref integer a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref integer		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_integer_array[]
end subroutine

public subroutine of_empty_array (ref longlong a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref longlong		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_longlong_array[]
end subroutine

public subroutine of_empty_array (ref string a_array[]);/************************************************************************************************************************************************************
 **	
 **	of_empty_array()
 **	
 **	Empty out passed in array
 **	
 **	Parameters	:	ref string		:	(a_array) an array to empty; returned as a reference variable
 **	
 **	Returns		:	None
 **	
 ************************************************************************************************************************************************************/

a_array[] = sv_empty_string_array[]
end subroutine

public function integer of_sort_array_long (ref longlong a_long_array[], string a_asc_desc);
longlong	num_array_elements, reset_array_long[]
string		sqls, rc


uo_ds_top	ds_sort_long_array
ds_sort_long_array = create uo_ds_top
//ds_sort_long_array.DataObject = 'dw_ls_sort_long_array'

sqls = "select 0 as long_col_1 from dual"
rc = g_ds_func.of_create_dynamic_ds( ds_sort_long_array, "grid", sqls, sqlca, false )
if rc = "ERROR" then return -1


ds_sort_long_array.Object.long_col_1.Primary	=	a_long_array		

if a_asc_desc = 'a' then
	ds_sort_long_array.SetSort('long_col_1 a')
else
	ds_sort_long_array.SetSort('long_col_1 d')
end if

ds_sort_long_array.Sort()
a_long_array = reset_array_long[]

a_long_array = ds_sort_long_array.Object.long_col_1.Primary
	
return 1	
end function

public function longlong of_delete_from_array (ref string a_arr[], longlong a_index);////////////////////
////
////	of_delete_from_array	:	deletes a node from an array, moving all other nodes
////									up one index
////
////	arguments				:	a_arr		:	array to delete a node from; type: long
////									a_index	:	node index to delete from the array
////
////	return					:	1 if it succeeds, if it
////									fails it returns a -1
////
////////////////////

longlong	i, j, max
string		temp_arr[]
string		rtn_str

setPointer( Hourglass! )

max	=	upperBound( a_arr[] )
i		=	1

if a_index > max or a_index < 1 then
	return -1
end if

rtn_str = a_arr[a_index]

for i = 1 to a_index - 1
	temp_arr[i] = a_arr[i]
next

j = upperBound( temp_arr[] ) + 1
for i = a_index + 1 to max
	temp_arr[j] = a_arr[i]
	j++
next

a_arr = temp_arr

return 1
end function

public function longlong of_delete_from_array (boolean a_arr[], longlong a_index);////////////////////
////
////	of_delete_from_array	:	deletes a node from an array, moving all other nodes
////									up one index
////
////	arguments				:	a_arr		:	array to delete a node from; type: long
////									a_index	:	node index to delete from the array
////
////	return					:	1 if it succeeds, if it
////									fails it returns a -1
////
////////////////////

longlong	i, j, max
boolean		temp_arr[]
boolean		rtn_b

setPointer( Hourglass! )

max	=	upperBound( a_arr[] )
i		=	1

if a_index > max or a_index < 1 then
	return -1
end if

rtn_b = a_arr[a_index]

for i = 1 to a_index - 1
	temp_arr[i] = a_arr[i]
next

j = upperBound( temp_arr[] ) + 1
for i = a_index + 1 to max
	temp_arr[j] = a_arr[i]
	j++
next

a_arr = temp_arr

return 1
end function

public function integer of_arrayunique (string as_sourcevalues[], ref string as_uniquevalues[], boolean ab_ignorecase);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   of_arrayunique
//
//   Purpose  :   takes an array and returns an array of strings and returns an array of the unique strings in the input
//
//   Scope    :   global
//
//   Arguments:   as_sourcevalues : string[]  : input strings
// 					as_uniquevalues : string[]	: array of unique strings in as_sourcevalues
//						ab_ignorecase	 : boolean  : whether to ignore case or not when testing for uniqueness
//
//   Returns :    return_number   :  number   : number of unique strings
//
//
//     DATE       NAME        REVISION                     CHANGES
//   ---------    --------   -----------    -----------------------------------------------
//    08-07-14    PowerPlan   Version 1.0   Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

int            li_pos,li_cnt,li_tcnt,li_cnt2
string      ls_empty[],ls_value
boolean      lb_skip


// Initialize Return Array
as_uniquevalues = ls_empty

// Loop Through Array Values
li_tcnt = upperbound(as_sourcevalues)
for li_cnt = 1 to li_tcnt

      // Get Value
      lb_skip = FALSE
      ls_value = as_sourcevalues[li_cnt]
      if ab_ignorecase then
            ls_value = trim(lower(ls_value))
            if ls_value = "" then continue
      end if

      // Determine if Unique
      for li_cnt2 = 1 to li_pos
            if ls_value = lower(as_uniquevalues[li_cnt2]) then
                  lb_skip = TRUE
                  exit
            end if
      next
      if lb_skip then continue

      // Include
      li_pos ++
      as_uniquevalues[li_pos] = ls_value

next

// Return
return(upperbound(as_uniquevalues))
end function

on nvo_func_string.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_func_string.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

