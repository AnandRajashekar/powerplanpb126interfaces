HA$PBExportHeader$w_ps_find.srw
$PBExportComments$Find String Window for the postscript interface object
forward
global type w_ps_find from Window
end type
type cb_3 from commandbutton within w_ps_find
end type
type cb_1 from commandbutton within w_ps_find
end type
type st_1 from statictext within w_ps_find
end type
type sle_1 from singlelineedit within w_ps_find
end type
end forward

global type w_ps_find from Window
int X=1075
int Y=477
int Width=1537
int Height=545
boolean TitleBar=true
string Title="Find String"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
cb_3 cb_3
cb_1 cb_1
st_1 st_1
sle_1 sle_1
end type
global w_ps_find w_ps_find

on w_ps_find.create
this.cb_3=create cb_3
this.cb_1=create cb_1
this.st_1=create st_1
this.sle_1=create sle_1
this.Control[]={ this.cb_3,&
this.cb_1,&
this.st_1,&
this.sle_1}
end on

on w_ps_find.destroy
destroy(this.cb_3)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.sle_1)
end on

event open;sle_1.setfocus()
end event

type cb_3 from commandbutton within w_ps_find
int X=55
int Y=333
int Width=279
int Height=97
int TabOrder=20
string Text="&Cancel"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;closewithreturn(parent,"")

end event

type cb_1 from commandbutton within w_ps_find
int X=1143
int Y=333
int Width=275
int Height=97
int TabOrder=30
string Text="Find"
boolean Default=true
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;closewithreturn(parent,sle_1.text)

end event

type st_1 from statictext within w_ps_find
int X=430
int Y=57
int Width=586
int Height=77
boolean Enabled=false
string Text="Search For:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_1 from singlelineedit within w_ps_find
int X=78
int Y=161
int Width=1299
int Height=89
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

