HA$PBExportHeader$nvo_ds_top.sru
forward
global type nvo_ds_top from datastore
end type
end forward

global type nvo_ds_top from datastore
end type
global nvo_ds_top nvo_ds_top

type variables
string          i_sqlca_sqlerrtext
longlong i_sqlca_sqlcode
string          i_sqlca_sqlsyntax

// JAK 20090218:  New for cancel query...allows us to 
//		get any errors back to the SQLCA variable
uo_sqlca 	  i_sqlca
end variables

event dberror;i_sqlca_sqlcode = sqldbcode
i_sqlca_sqlerrtext = SQLErrText
i_sqlca_sqlsyntax = sqlsyntax

// Always surpress the message
return 1
end event

on nvo_ds_top.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_ds_top.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

