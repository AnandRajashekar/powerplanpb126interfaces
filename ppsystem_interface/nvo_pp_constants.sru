HA$PBExportHeader$nvo_pp_constants.sru
forward
global type nvo_pp_constants from nonvisualobject
end type
end forward

global type nvo_pp_constants from nonvisualobject
end type
global nvo_pp_constants nvo_pp_constants

type variables
constant longlong COLOR_PP_BLUE 						= 11168256
constant longlong COLOR_PP_ORANGE						= 3450618
constant longlong COLOR_PP_GREY							= 10789281
constant longlong COLOR_PP_GREEN						= 6337382
constant longlong COLOR_PP_YELLOW						= 7062783
constant longlong COLOR_PP_SNOWWHITE				= 16777215
constant longlong COLOR_PP_REQUIRED					= 11795450
constant longlong COLOR_PP_REQUIRED_ERROR		= 11842815
constant longlong COLOR_DISABLEBCKGREY				= 15657449
constant longlong COLOR_DISABLETXTGREY				= 6908265

constant string FONT_PP_PRIMARY					= 'Avenir'
constant string FONT_PP_SECONDARY				= 'Arial'

constant longlong FONT_SIZE_PP_HEADER				= 	8
constant longlong FONT_WEIGHT_PP_HEADER			= 700

constant string	DW_PROCESSING_FORM_GROUP	= "0"	//(Default) Form, group, n-up, or tabular
constant string	DW_PROCESSING_GRID 				= "1"
constant string	DW_PROCESSING_LABEL 				= "2"
constant string	DW_PROCESSING_GRAPH 				= "3"
constant string	DW_PROCESSING_CROSSTAB 		= "4"
constant string	DW_PROCESSING_COMPOSITE 		= "5"
constant string	DW_PROCESSING_OLE 					= "6"
constant string	DW_PROCESSING_RICHTEXT 		= "7"
constant string	DW_PROCESSING_TREEVIEW 		= "8"
constant string	DW_PROCESSING_TREEVIEWGRID	= "9"


/**************************************************/
/*						DEPRECIATION CONSTANTS							*/
/**************************************************/
/**************************************************/
/*							DEPR ACTIVITY CODES								*/
/**************************************************/
constant longlong DEPR_EXP_ADJUST						= 1
constant longlong COST_OF_REMOVAL					= 2
constant longlong SALVAGE_CASH							= 3
constant longlong SALVAGE_RETURNS						= 4
constant longlong RESERVE_CREDITS						= 5
constant longlong RESERVE_TRANS_IN					= 6
constant longlong RESERVE_TRANS_OUT					= 7
constant longlong GAIN_LOSS								= 8
constant longlong RESERVE_ADJUSTMENT				= 9
constant longlong IMPAIRMENT_ACT						= 10
constant longlong COST_OF_REMOVAL_EXP_ADJUST	= 11
constant longlong COST_OF_REMOVAL_RES_ADJUST	= 12
constant longlong COST_OF_REMOVAL_TRANS_IN		= 13
constant longlong COST_OF_REMOVAL_TRANS_OUT	= 14
constant longlong SALVAGE_EXP_ADJUST					= 15
constant longlong IMPAIRMENT_ASSET_AMOUNT		= 16
constant longlong IMPAIRMENT_EXPENSE_AMOUNT	= 17

/**************************************************/
/*						END DEPRECIATION CONSTANTS						*/
/**************************************************/

//lease stuff
constant int	LSCNTR_BASIC_LEASE 				= 1
constant int	LSCNTR_BASIC_LESSOR				= 2
constant int	LSCNTR_BASIC_COMPANY			= 3
constant int LSCNTR_BASIC_LEASE_GROUP		= 4
constant int LSCNTR_BASIC_LEASE_TYPE		= 5

constant int	ASSETCNTR_BASIC_LEASE 				= 6
constant int	ASSETCNTR_BASIC_ASSET				= 7
constant int	ASSETCNTR_BASIC_COMPANY			= 8
constant int ASSETCNTR_BASIC_LEASE_GROUP	= 9
constant int ASSETCNTR_BASIC_ILR					= 10

constant int JECNTR_BASIC_MONTH = 11
constant int JECNTR_BASIC_COMPANY = 12
constant int JECNTR_BASIC_JE_TYPE = 13
constant int JECNTR_BASIC_LEASE_GROUP = 14
constant int JECNTR_BASIC_LEASE = 15
constant int JECNTR_BASIC_ASSET = 16
constant int JECNTR_BASIC_ILR = 17

constant int COMP_BASIC_PO = 18
constant int COMP_BASIC_DESCR = 19
constant int COMP_BASIC_LONG_DESCR = 20
constant int COMP_BASIC_SN = 21
constant int COMP_BASIC_ASSET = 22
constant int COMP_BASIC_INVOICE = 24

constant int ASSETCNTR_CLASS_CODE = 23

//selecttabs (continued list of constants from uo_ppbase_searchbox)
constant integer i_restrictby_lease = 75
constant integer i_restrictby_lease_group = 76
constant integer i_restrictby_lessor = 77
constant integer i_restrictby_ilr = 78
constant integer i_restrictby_asset = 79
constant integer i_restrictby_asset_location = 80
constant integer i_restrictby_state = 81
constant integer i_restrictby_tax_local = 82
constant integer i_restrictby_tax_distrcit = 83
constant integer i_restrictby_ua = 84
constant integer i_restrictby_bs = 85
end variables
on nvo_pp_constants.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_pp_constants.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

