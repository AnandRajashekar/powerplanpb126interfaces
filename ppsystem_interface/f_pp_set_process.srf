HA$PBExportHeader$f_pp_set_process.srf
global type f_pp_set_process from function_object
end type

forward prototypes
global function boolean f_pp_set_process (string a_process_name, ref string a_msg)
global function boolean f_pp_set_process (string a_process_name, string a_check, ref string a_msg)
end prototypes

global function boolean f_pp_set_process (string a_process_name, ref string a_msg);
	return f_pp_set_process(a_process_name, a_process_name, a_msg);
end function

global function boolean f_pp_set_process (string a_process_name, string a_check, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	f_pp_set_process()
 **
 **	This function serves as the entry point to the oracle package handling concurrent processing locks. It can be used to lock a process to prevent 
 **   others (in any session) from using it, to check whether a process is currently locked in any session, and to release a lock when the process is 
 **   complete.
 **
 **   To check if a process is running without making a new lock, call the function with a space (' ') in the process_name argument and the process
 **   you want to check in the check_string argument. There is another function f_pp_check_process() that will do this for you.
 **
 **   To release a process that is currently locked, call the function with a space (' ') in the check_string argument and the process you want to
 **   release in the process_name argument. There is another function f_pp_release_process() that will do this for you.
 **
 **   You can also check process prefixes by using a wildcard ('%') in the check_string argument.
 **
 **	Parameters	:	string			:	(a_process_name) Name of the process to lock.
 **                  string         :  (a_check_string) Name of the process, or processes to check. If more than one process, delimit with semicolons.
 **						ref string		:	(a_msg) Return from pp_set_process Oracle function, if successful, else error message.
 **	
 **	Returns		:	(boolean)	:	Retruns TRUE, if pp_set_process() ran successfully and returned OK, otherwise FALSE
 **
 **   Examples:
 **  		f_pp_set_process('afc_1','afc_1;afc_-1'); //Lock process afc_1, but first check to see if afc_1 or afc_-1 is already running. If so, fail. Returns OK.
 **  		f_pp_set_process('afc_2','afc_2;afc_-1'); //Lock process afc_2, but first check to see if afc_2 or afc_-1 is already running. If so, fail. Returns OK.
 **  		f_pp_set_process('afc_-1','afc_%'); //Lock process afc_-1, but first check to see if any afc process is already running. Returns Error.
 **  		f_pp_set_process('afc_1',' '); //Remove the lock on process afc_1. You can also use f_pp_release_process() for this.
 **  		f_pp_set_process('afc_2',' '); //Remove the lock on process afc_2. You can also use f_pp_release_process() for this.
 **  		f_pp_set_process('afc_-1','afc_%'); //Lock process afc_-1, but first check to see if any afc process is already running. Returns OK this time.
 **  		f_pp_set_process('afc_-1',' '); //Remove the lock on process afc_-1. You can also use f_pp_release_process() for this.
 **	
 ************************************************************************************************************************************************************/

		if a_process_name = '' then a_process_name = ' '
		
		a_msg = sqlca.pp_set_process(a_process_name, a_check)
		
		if sqlca.sqlcode <> 0 then	
			a_msg = "Error: " + sqlca.sqlerrtext
			return FALSE		
		elseif a_msg <> "OK" then	
			a_msg = "The process is in use by another session: " + a_msg
			return FALSE
		else
			return TRUE
		end if
		
		
		
		
end function

