HA$PBExportHeader$uo_ppbase_workspace.sru
forward
global type uo_ppbase_workspace from nonvisualobject
end type
end forward

global type uo_ppbase_workspace from nonvisualobject
end type
global uo_ppbase_workspace uo_ppbase_workspace

on uo_ppbase_workspace.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ppbase_workspace.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;/* Stubbed object */
end event

