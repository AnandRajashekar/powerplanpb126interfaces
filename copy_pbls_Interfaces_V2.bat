@echo off 

FOR /F %%i IN ('dir S:\Development\NightlyBuilds /b /ad-h /od') DO (SET newest=%%i)
FOR /F %%i IN ('dir S:\Development\NightlyBuilds /b /ad-h /od') DO (if %%i NEQ %newest% (SET newest2=%%i))
FOR /F %%i IN ('dir S:\Development\NightlyBuilds /b /ad-h /od') DO (if %%i NEQ %newest% (if %%i NEQ %newest2% (SET newest3=%%i)))

set option1=1. Newest (%newest%)
set option1_path=S:\Development\Powerplan\QA\%newest%\src
set option1_int_path=S:\Development\Powerplan\QA\%newest%\interfaces\src

set option2=2. 2nd Newest (%newest2%)
set option2_path=S:\Development\Powerplan\QA\%newest2%\src
set option2_int_path=S:\Development\Powerplan\QA\%newest2%\interfaces\src

set option3=3. 3rd Newest (%newest3%)
set option3_path=S:\Development\Powerplan\QA\%newest3%\src
set option3_int_path=S:\Development\Powerplan\QA\%newest3%\interfaces\src

set option4=4. Custom ( hardcoded folder path)
set option4_path=C:\Test\ppinterfaces-PB126
set option4_int_path=C:\Test\ppinterfaces-PB126



echo %option1%
echo %option2%
echo %option3%
echo %option4%
rem echo %option5%
echo 99. Quit

:try_again
set /p option_select=Select a option based on the list above (Enter "1", "2", etc): 

set network_dir=BLANK
if %option_select%==1 (set network_dir="%option1_path%")
if %option_select%==1 (set network_int_dir="%option1_int_path%")
if %option_select%==2 (set network_dir="%option2_path%")
if %option_select%==2 (set network_int_dir="%option2_int_path%")
if %option_select%==3 (set network_dir="%option3_path%")
if %option_select%==3 (set network_int_dir="%option3_int_path%")
if %option_select%==4 (set network_dir="%option4_path%")
if %option_select%==4 (set network_int_dir="%option4_int_path%")
if %option_select%==5 (set network_dir="%option5_path%")
if %option_select%==5 (set network_int_dir="%option5_int_path%")
if %option_select%==6 (set network_dir="%option6_path%")
if %option_select%==6 (set network_int_dir="%option6_int_path%")
if %option_select%==7 (set network_dir="%option7_path%")
if %option_select%==7 (set network_int_dir="%option7_int_path%")
if %option_select%==8 (set network_dir="%option8_path%")
if %option_select%==8 (set network_int_dir="%option8_int_path%")
if %option_select%==9 (set network_dir="%option9_path%")
if %option_select%==9 (set network_int_dir="%option9_int_path%")
if %option_select%==10 (set network_dir="%option10_path%")
if %option_select%==10 (set network_int_dir="%option10_int_path%")
if %option_select%==99 (goto :eof)
if %network_dir%==BLANK (goto try_again)
if %network_dir%== (goto try_again)


echo --- INTERFACES ---
set network_dir=%network_int_dir%
set network_dir=%network_dir:~1,-1%
echo Pulling files from: %network_dir%
set local_dir=C:\TestPB126Git
echo To: %local_dir%
pause

%local_dir:~0,2%
cd %local_dir%

for /R %%F in (*.pbg) DO (
     echo %%F
	if exist "%network_dir%\%%~nF\%%~nF.pbl" (
		echo %%~nF
		copy "%network_dir%\%%~nF\%%~nF.pbl" "%%~dF%%~pF"
     ) else (
		if exist "%network_dir%\%%~nF.pbl" (
			echo %%~nF
			copy "%network_dir%\%%~nF.pbl" "%%~dF%%~pF"
		)
     )
)
Pause
