HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_approve_accruals.exe'

nvo_wo_control i_nvo_wo_control
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
private function boolean uf_mainapproveaccruals ()
private function boolean uf_validate (longlong a_company_id, string a_company_descr)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// call the main approve accruals function and return success or failure
if uf_mainapproveaccruals( ) then
	commit;
	i_nvo_wo_control.of_releaseprocess( process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)
	return rtn_success
else
	rollback;
	// log the list of companies that failed to approve accruals
	i_nvo_wo_control.of_log_failed_companies("APPROVAL ACCRUALS")
	return rtn_failure
end if
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

private function boolean uf_mainapproveaccruals ();/************************************************************************************************************************************************************
 **
 **	uf_mainapproveaccruals()
 **	
 **	The main function for approve accruals.  This is called by uf_read and will call all other functions
 **
 **	INFORMATION:
 **		Parameters passed from PowerPlan and Job Server:
 **		g_ssp_parms.long_arg = Company IDs
 **		g_ssp_parms.date_arg[1] = Accrual Month 
 **	
 **	Parameters	:	none
 **
 **	Returns		:	boolean	:	true if successful
 **										false if not
 **
 ************************************************************************************************************************************************************/
longlong i, num_companies, month_number, rtn
string process_msg

i_nvo_wo_control.of_constructor()

// if any company in the loop do not approve accruals
// continue on to the next company but return a failure
boolean b_all_success
b_all_success = true

// reference data window
w_datawindows w_dw

// these parameters originate from the w_wo_control window
num_companies = upperBound( g_ssp_parms.long_arg )
i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_wo_control.i_month = datetime( g_ssp_parms.date_arg[1] )

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("Approve Accruals - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

// populate the company descs
rtn = i_nvo_wo_control.of_getDescriptionsFromids( i_nvo_wo_control.i_company_idx )
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if 

// loop over the companies and process accruals
f_pp_msgs("Processing Approve Accruals...")
for i = 1 to num_companies
	try
		// change the company in the NVO
		i_nvo_wo_control.of_companychanged( i , i_nvo_wo_control.i_month )
		f_pp_msgs("   Company_id: " + string( i_nvo_wo_control.i_company_idx[ i ] ) )
		
		// validate this company can run accruals.
		if uf_validate( i_nvo_wo_control.i_company_idx[ i ] , i_nvo_wo_control.i_company_descr[ i ] ) then
			// try to delete the accruals.  The function throws an exception if the SQL errors, so catch it.
			rtn = f_accrual_approval( i_nvo_wo_control.i_company_idx[ i ], i_nvo_wo_control.i_month )
			if rtn <> 1 then
				//f_accrual_approval logs the error and does the rollback
				b_all_success = false
				i_nvo_wo_control.of_add_to_failed_company_list(i)
				continue
			end if

		end if
		
		// update completion date on the window
		i_nvo_wo_control.i_ds_wo_control.setItem(1, 'accrual_approval', today())

		// commit by company
		i_nvo_wo_control.of_updatedw( )

	catch( exception e )
		f_pp_msgs( e.getMessage() )
		rollback;
		// keep track of all the companies that failed to approve accruals so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(i)
	end try
next

f_pp_msgs("Process ended")

if not b_all_success then
	f_pp_msgs("***********************************************************")
	f_pp_msgs("WARNING: AT LEAST ONE COMPANY HAD AN ERROR.  PLEASE REVIEW THE LOGS")
	f_pp_msgs("***********************************************************")
end if

return b_all_success
end function

private function boolean uf_validate (longlong a_company_id, string a_company_descr);/************************************************************************************************************************************************************
 **
 **	uf_validate()
 **	
 **	This function will validate that the approve accruals can process for the passed in company and month
 **
 **	
 **	Parameters	:	a_company_id		:	longlong	:	The company to check
 **
 **	Returns		:	boolean	:	true if company can process
 **										false if the company cannot process
 **
 ************************************************************************************************************************************************************/
string wo_accr_process_ans
datetime approved_date, calc_date

// check to see if this company is setup to process accruals.
wo_accr_process_ans = f_pp_system_control_company("Work Order Accrual Calculation",a_company_id)
if isnull(wo_accr_process_ans) or wo_accr_process_ans = "" then wo_accr_process_ans = 'no'
if wo_accr_process_ans = 'yes' then
	// check accrual approval
	approved_date = i_nvo_wo_control.i_ds_wo_control.getItemDateTime(1, 'accrual_approval')
	if not isnull(approved_date) then
		f_pp_msgs("Approve Accruals: Accruals have already been approved.")
		return false
	end if
	//check accrual calculation
	calc_date =  i_nvo_wo_control.i_ds_wo_control.getItemDateTime(1, 'accrual_calc')
	if isnull(calc_date) then 
		f_pp_msgs("Approve Accruals: The calculation must be performed first in order to be approved.")
		return false
	end if
	// Make sure the month isn't closed
	if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		return false
	end if
else
	f_pp_msgs( "System Control: 'Work Order Accrual Calculation' not set for Company: " + string(a_company_id) + " - " + a_company_descr )
	return false
end if

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppprojct_custom_version nvo_ppprojct_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version

choose case a_pbd_name
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version	
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

