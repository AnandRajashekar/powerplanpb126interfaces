HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_gl_charges_accruals from datawindow within w_datawindows
end type
type dw_workflow_type_active from datawindow within w_datawindows
end type
type dw_wo_to_unitize_101_late from datawindow within w_datawindows
end type
type dw_wo_to_unitize_101 from datawindow within w_datawindows
end type
type dw_required_class_codes from datawindow within w_datawindows
end type
type dw_ppbase_user_option_grid from datawindow within w_datawindows
end type
type dw_none from datawindow within w_datawindows
end type
type dw_messagebox_translate from datawindow within w_datawindows
end type
type dw_wo_process_control from datawindow within w_datawindows
end type
type dw_wo_process_afudc_ovh from datawindow within w_datawindows
end type
type dw_wo_interface_dates_all from datawindow within w_datawindows
end type
type dw_overhead_work_orders from datawindow within w_datawindows
end type
type dw_gl_transaction from datawindow within w_datawindows
end type
type dw_gl_charges_wip_comp from datawindow within w_datawindows
end type
type dw_gl_charges_oh from datawindow within w_datawindows
end type
type dw_gl_charges_cpi from datawindow within w_datawindows
end type
type dw_gl_charges_afc from datawindow within w_datawindows
end type
type dw_cpr_act_month from datawindow within w_datawindows
end type
type dw_cost_element from datawindow within w_datawindows
end type
type dw_clearing_wo_rate from datawindow within w_datawindows
end type
type dw_clearing_wo_control from datawindow within w_datawindows
end type
type dw_clearing_base from datawindow within w_datawindows
end type
type dw_cap_int_allo from datawindow within w_datawindows
end type
type dw_afudc_gl_trans from datawindow within w_datawindows
end type
type dw_afudc_calc_autogen_dw from datawindow within w_datawindows
end type
type dw_pp_verify_close from datawindow within w_datawindows
end type
type dw_pp_interface_dates_check from datawindow within w_datawindows
end type
type dw_cr_element_definitions from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
string tag = "w_datawindows"
integer width = 5559
integer height = 2224
boolean titlebar = true
string title = "w_datawindows"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_gl_charges_accruals dw_gl_charges_accruals
dw_workflow_type_active dw_workflow_type_active
dw_wo_to_unitize_101_late dw_wo_to_unitize_101_late
dw_wo_to_unitize_101 dw_wo_to_unitize_101
dw_required_class_codes dw_required_class_codes
dw_ppbase_user_option_grid dw_ppbase_user_option_grid
dw_none dw_none
dw_messagebox_translate dw_messagebox_translate
dw_wo_process_control dw_wo_process_control
dw_wo_process_afudc_ovh dw_wo_process_afudc_ovh
dw_wo_interface_dates_all dw_wo_interface_dates_all
dw_overhead_work_orders dw_overhead_work_orders
dw_gl_transaction dw_gl_transaction
dw_gl_charges_wip_comp dw_gl_charges_wip_comp
dw_gl_charges_oh dw_gl_charges_oh
dw_gl_charges_cpi dw_gl_charges_cpi
dw_gl_charges_afc dw_gl_charges_afc
dw_cpr_act_month dw_cpr_act_month
dw_cost_element dw_cost_element
dw_clearing_wo_rate dw_clearing_wo_rate
dw_clearing_wo_control dw_clearing_wo_control
dw_clearing_base dw_clearing_base
dw_cap_int_allo dw_cap_int_allo
dw_afudc_gl_trans dw_afudc_gl_trans
dw_afudc_calc_autogen_dw dw_afudc_calc_autogen_dw
dw_pp_verify_close dw_pp_verify_close
dw_pp_interface_dates_check dw_pp_interface_dates_check
dw_cr_element_definitions dw_cr_element_definitions
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_gl_charges_accruals=create dw_gl_charges_accruals
this.dw_workflow_type_active=create dw_workflow_type_active
this.dw_wo_to_unitize_101_late=create dw_wo_to_unitize_101_late
this.dw_wo_to_unitize_101=create dw_wo_to_unitize_101
this.dw_required_class_codes=create dw_required_class_codes
this.dw_ppbase_user_option_grid=create dw_ppbase_user_option_grid
this.dw_none=create dw_none
this.dw_messagebox_translate=create dw_messagebox_translate
this.dw_wo_process_control=create dw_wo_process_control
this.dw_wo_process_afudc_ovh=create dw_wo_process_afudc_ovh
this.dw_wo_interface_dates_all=create dw_wo_interface_dates_all
this.dw_overhead_work_orders=create dw_overhead_work_orders
this.dw_gl_transaction=create dw_gl_transaction
this.dw_gl_charges_wip_comp=create dw_gl_charges_wip_comp
this.dw_gl_charges_oh=create dw_gl_charges_oh
this.dw_gl_charges_cpi=create dw_gl_charges_cpi
this.dw_gl_charges_afc=create dw_gl_charges_afc
this.dw_cpr_act_month=create dw_cpr_act_month
this.dw_cost_element=create dw_cost_element
this.dw_clearing_wo_rate=create dw_clearing_wo_rate
this.dw_clearing_wo_control=create dw_clearing_wo_control
this.dw_clearing_base=create dw_clearing_base
this.dw_cap_int_allo=create dw_cap_int_allo
this.dw_afudc_gl_trans=create dw_afudc_gl_trans
this.dw_afudc_calc_autogen_dw=create dw_afudc_calc_autogen_dw
this.dw_pp_verify_close=create dw_pp_verify_close
this.dw_pp_interface_dates_check=create dw_pp_interface_dates_check
this.dw_cr_element_definitions=create dw_cr_element_definitions
this.Control[]={this.dw_gl_charges_accruals,&
this.dw_workflow_type_active,&
this.dw_wo_to_unitize_101_late,&
this.dw_wo_to_unitize_101,&
this.dw_required_class_codes,&
this.dw_ppbase_user_option_grid,&
this.dw_none,&
this.dw_messagebox_translate,&
this.dw_wo_process_control,&
this.dw_wo_process_afudc_ovh,&
this.dw_wo_interface_dates_all,&
this.dw_overhead_work_orders,&
this.dw_gl_transaction,&
this.dw_gl_charges_wip_comp,&
this.dw_gl_charges_oh,&
this.dw_gl_charges_cpi,&
this.dw_gl_charges_afc,&
this.dw_cpr_act_month,&
this.dw_cost_element,&
this.dw_clearing_wo_rate,&
this.dw_clearing_wo_control,&
this.dw_clearing_base,&
this.dw_cap_int_allo,&
this.dw_afudc_gl_trans,&
this.dw_afudc_calc_autogen_dw,&
this.dw_pp_verify_close,&
this.dw_pp_interface_dates_check,&
this.dw_cr_element_definitions}
end on

on w_datawindows.destroy
destroy(this.dw_gl_charges_accruals)
destroy(this.dw_workflow_type_active)
destroy(this.dw_wo_to_unitize_101_late)
destroy(this.dw_wo_to_unitize_101)
destroy(this.dw_required_class_codes)
destroy(this.dw_ppbase_user_option_grid)
destroy(this.dw_none)
destroy(this.dw_messagebox_translate)
destroy(this.dw_wo_process_control)
destroy(this.dw_wo_process_afudc_ovh)
destroy(this.dw_wo_interface_dates_all)
destroy(this.dw_overhead_work_orders)
destroy(this.dw_gl_transaction)
destroy(this.dw_gl_charges_wip_comp)
destroy(this.dw_gl_charges_oh)
destroy(this.dw_gl_charges_cpi)
destroy(this.dw_gl_charges_afc)
destroy(this.dw_cpr_act_month)
destroy(this.dw_cost_element)
destroy(this.dw_clearing_wo_rate)
destroy(this.dw_clearing_wo_control)
destroy(this.dw_clearing_base)
destroy(this.dw_cap_int_allo)
destroy(this.dw_afudc_gl_trans)
destroy(this.dw_afudc_calc_autogen_dw)
destroy(this.dw_pp_verify_close)
destroy(this.dw_pp_interface_dates_check)
destroy(this.dw_cr_element_definitions)
end on

type dw_gl_charges_accruals from datawindow within w_datawindows
integer x = 4626
integer y = 1434
integer width = 772
integer height = 451
integer taborder = 210
string title = "none"
string dataobject = "dw_gl_charges_accruals"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_workflow_type_active from datawindow within w_datawindows
integer x = 3888
integer y = 1437
integer width = 688
integer height = 400
integer taborder = 220
string title = "none"
string dataobject = "dw_workflow_type_active"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_to_unitize_101_late from datawindow within w_datawindows
integer x = 3138
integer y = 1443
integer width = 688
integer height = 400
integer taborder = 210
string title = "none"
string dataobject = "dw_wo_to_unitize_101_late"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_to_unitize_101 from datawindow within w_datawindows
integer x = 2417
integer y = 1456
integer width = 688
integer height = 400
integer taborder = 200
string title = "none"
string dataobject = "dw_wo_to_unitize_101"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_required_class_codes from datawindow within w_datawindows
integer x = 1668
integer y = 1450
integer width = 688
integer height = 400
integer taborder = 180
string title = "none"
string dataobject = "dw_required_class_codes"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_ppbase_user_option_grid from datawindow within w_datawindows
integer x = 874
integer y = 1456
integer width = 688
integer height = 400
integer taborder = 190
string title = "none"
string dataobject = "dw_ppbase_user_option_grid"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_none from datawindow within w_datawindows
integer x = 121
integer y = 1456
integer width = 688
integer height = 400
integer taborder = 180
string title = "none"
string dataobject = "dw_none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_messagebox_translate from datawindow within w_datawindows
integer x = 4630
integer y = 989
integer width = 688
integer height = 400
integer taborder = 150
string title = "none"
string dataobject = "dw_messagebox_translate"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_control from datawindow within w_datawindows
integer x = 3888
integer y = 1002
integer width = 688
integer height = 400
integer taborder = 190
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_afudc_ovh from datawindow within w_datawindows
integer x = 3127
integer y = 1005
integer width = 688
integer height = 400
integer taborder = 180
string title = "none"
string dataobject = "dw_wo_process_afudc_ovh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_interface_dates_all from datawindow within w_datawindows
integer x = 2373
integer y = 1011
integer width = 688
integer height = 400
integer taborder = 170
string title = "none"
string dataobject = "dw_wo_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_overhead_work_orders from datawindow within w_datawindows
integer x = 1605
integer y = 1002
integer width = 688
integer height = 400
integer taborder = 160
string title = "none"
string dataobject = "dw_overhead_work_orders"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_transaction from datawindow within w_datawindows
integer x = 863
integer y = 992
integer width = 688
integer height = 400
integer taborder = 150
string title = "none"
string dataobject = "dw_gl_transaction"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_wip_comp from datawindow within w_datawindows
integer x = 91
integer y = 986
integer width = 688
integer height = 400
integer taborder = 140
string title = "none"
string dataobject = "dw_gl_charges_wip_comp"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_oh from datawindow within w_datawindows
integer x = 4593
integer y = 541
integer width = 688
integer height = 400
integer taborder = 130
string title = "none"
string dataobject = "dw_gl_charges_oh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_cpi from datawindow within w_datawindows
integer x = 3880
integer y = 538
integer width = 688
integer height = 400
integer taborder = 120
string title = "none"
string dataobject = "dw_gl_charges_cpi"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_afc from datawindow within w_datawindows
integer x = 3123
integer y = 525
integer width = 688
integer height = 400
integer taborder = 110
string title = "none"
string dataobject = "dw_gl_charges_afc"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_act_month from datawindow within w_datawindows
integer x = 2355
integer y = 528
integer width = 688
integer height = 400
integer taborder = 110
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cost_element from datawindow within w_datawindows
integer x = 1569
integer y = 525
integer width = 688
integer height = 400
integer taborder = 100
string title = "none"
string dataobject = "dw_cost_element"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_wo_rate from datawindow within w_datawindows
integer x = 816
integer y = 509
integer width = 688
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_clearing_wo_rate"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_wo_control from datawindow within w_datawindows
integer x = 66
integer y = 509
integer width = 688
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "dw_clearing_wo_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_base from datawindow within w_datawindows
integer x = 4590
integer y = 99
integer width = 688
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_clearing_base"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cap_int_allo from datawindow within w_datawindows
integer x = 3789
integer y = 90
integer width = 688
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_cap_int_allo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_afudc_gl_trans from datawindow within w_datawindows
integer x = 3035
integer y = 67
integer width = 688
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_afudc_gl_trans"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_afudc_calc_autogen_dw from datawindow within w_datawindows
integer x = 2308
integer y = 58
integer width = 688
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_afudc_calc_autogen_dw"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_verify_close from datawindow within w_datawindows
integer x = 1540
integer y = 58
integer width = 688
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_pp_verify_close"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_interface_dates_check from datawindow within w_datawindows
integer x = 786
integer y = 58
integer width = 688
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cr_element_definitions from datawindow within w_datawindows
integer x = 48
integer y = 45
integer width = 688
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

