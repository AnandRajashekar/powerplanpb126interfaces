HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_depr_calc.exe'
longlong i_fcst_version_id
longlong i_calc_retire

nvo_cpr_control i_nvo_cpr_control
end variables
forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function longlong uf_individualresults ()
public function integer uf_individualcalc ()
public function integer uf_groupcalc ()
public function longlong uf_groupresults ()
public function longlong uf_rollfwd ()
public function longlong uf_rollfwd_ind ()
public function integer uf_fcstfp ()
public function longlong uf_fcstfpresults ()
public function integer uf_fcstdepr_group ()
public function longlong uf_fcstresults_group ()
public function integer uf_fcstresults_ind ()
public function integer uf_fcstdepr_ind ()
public function longlong uf_update_process_control (string a_depr_type)
public function boolean uf_all_types_finished ()
public subroutine uf_depr_reg_entries ()
public function longlong uf_set_run_date (string a_depr_type)
public subroutine uf_send_email (string a_process_description)
public function boolean uf_validations ()
public function longlong uf_handleerrors ()
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
//	
//	
//	
longlong rtn_success, rtn_failure, rtn, i, num
w_datawindows w_not_used
string process_description, subledger_description
boolean b_monthEnd = false

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE';

//Are we just doing the launcher? 
if upper(trim(g_ssp_parms.string_arg[1])) = 'LAUNCH' then 
	nvo_depr_calc_launcher my_launcher
	my_launcher = create nvo_depr_calc_launcher
	longlong process_control_group_id
	longlong process_identifiers[]
	try
		//launch other job requests 
		my_launcher.uf_launch_month_end_calcs(g_ssp_parms.long_arg, g_ssp_parms.date_arg[1], ref process_control_group_id, ref process_identifiers)
	catch (ex_depr_calc_launch ex)
		f_pp_msgs(ex.text)
		f_pp_msgs("Exiting depreciation calculation launcher")
		return rtn_failure
	end try
	
	try
		//now that the other jobs have been launched, we have to check for results. 
		my_launcher.uf_check_results(process_control_group_id, process_identifiers)
	catch (ex_depr_calc_launch ex2)
		f_pp_msgs(ex2.text)
		f_pp_msgs("Exiting depreciation calculation launcher")
		return rtn_failure
	end try	
	
	f_pp_msgs("All launched processes have completed successfully!")
	
	//no exceptions from either function call - we're done!
	return rtn_success 
end if

/*
*	string_arg[1] is the Depreciation TYPE
*	It will tell the function what type of event depreciation this is
*	MONTHEND, LESEE, FCST, REG are valid values.  
*/
choose case upper(g_ssp_parms.string_arg[1])
	case 'GROUP', 'INDIVIDUAL'
	f_pp_msgs("Processing Company IDs: ");
	num = upperBound( g_ssp_parms.long_arg )
	for i = 1 to num
		f_pp_msgs("   " +STRING(g_ssp_parms.long_arg[ i ] ) );
	next
end choose

// Determine if we are running a month end process
if upper(g_ssp_parms.string_arg[1]) = 'GROUP' then 
	b_monthEnd = true
elseif upper(g_ssp_parms.string_arg[1]) = 'INDIVIDUAL' then
	if upperbound(g_ssp_parms.long_arg2) > 0 then
		if g_ssp_parms.long_arg2[1] <> -100 then
			b_monthEnd = true
		end if
	end if
else
	b_monthEnd = false
end if

if b_monthEnd then
	// Construct nvo_cpr_control
	i_nvo_cpr_control.of_constructor()
	
	// Get the ssp parameters
	i_nvo_cpr_control.i_company_idx[]  = g_ssp_parms.long_arg[]
	i_nvo_cpr_control.i_month = datetime(g_ssp_parms.date_arg[1])
	i_nvo_cpr_control.i_original_month = datetime(g_ssp_parms.date_arg[1])
	i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])
	
	// Get the descriptions
	rtn = i_nvo_cpr_control.of_getDescriptionsFromIds(i_nvo_cpr_control.i_company_idx)
	if rtn <> 1 then
		// of_getDescriptionsFromIds logs the appropriate error
		return rtn_failure
	end if
	
	// Do concurrency check
	string process_msg
	if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
		 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
		 return rtn_failure
	end if
	
	// Perform same validations as w_cpr_control
	if not uf_validations() then
		// Release the concurrency lock
		i_nvo_cpr_control.of_releaseProcess(process_msg)
		f_pp_msgs("Release Process Status: " + process_msg)
		
		return rtn_failure
	end if
end if

choose case upper(g_ssp_parms.string_arg[1])
	case 'GROUP'	
		process_description = "Group Depreciation Calculation"
		f_pp_msgs("Processing " + process_description)
		rtn = uf_groupCalc( )
	case 'INDIVIDUAL'
		//get subledger description
		SELECT subledger_name
		INTO :subledger_description
		FROM subledger_control
		WHERE subledger_type_id = :g_ssp_parms.long_arg2[1];
		
		process_description = "Individual Depreciation Calculation for subledger " + subledger_description
		f_pp_msgs("Processing " + process_description)
		rtn = uf_individualCalc( )
	case 'FCST: GROUP'
		process_description = "Forecast Depreciation Calculation for Groups"
		f_pp_msgs("Processing " + process_description)
		rtn = uf_fcstDepr_group( )
	case 'FCST: INDIVIDUAL'
		process_description = "Forecast Depreciation Calculation for Individual"
		f_pp_msgs("Processing " + process_description)
		rtn = uf_fcstDepr_ind( )
	case 'INCREMENTAL'
		process_description = "Forecast Depreciation Calculation by Funding Project"
		f_pp_msgs("Processing " + process_description)
		rtn = uf_fcstFP( )
end choose

if rtn <> 1 then
	f_pp_msgs("	ERROR: " )
	if sqlca.sqlcode <> 0 then 
		f_pp_msgs("	SQL Error Code: " + string(sqlca.sqldbcode))
		f_pp_msgs("	SQL Error Message: " + sqlca.sqlerrtext)
	end if 
	rollback;
	//only send emails for normal depr, not fcst
	if upper(g_ssp_parms.string_arg[1]) = 'INDIVIDUAL' or upper(g_ssp_parms.string_arg[1]) = 'GROUP' then 
		uf_send_email(process_description)
	end if
	
	if b_monthEnd then
		// Release the concurrency lock
		i_nvo_cpr_control.of_releaseProcess(process_msg)
		f_pp_msgs("Release Process Status: " + process_msg)
	end if
	
	return rtn_failure
end if

if uf_handleErrors( ) = -1 then
	return rtn_failure
end if

f_pp_msgs("Saving calculation results")
choose case upper(g_ssp_parms.string_arg[1])
	case 'GROUP'
		rtn = uf_groupResults( )
	case 'INDIVIDUAL'
		rtn = uf_individualResults( )
	case 'FCST: GROUP'
		rtn = uf_fcstResults_group( )
	case 'FCST: INDIVIDUAL'
		rtn = uf_fcstResults_ind( )
	case 'INCREMENTAL'
		rtn = uf_fcstFPResults( )
end choose
	
if rtn = 1 then
	f_pp_msgs ("SUCCESS!!!!")
	
	if uf_all_types_finished() then
		uf_set_run_date(g_ssp_parms.string_arg[1])
		
		if left(upper(g_ssp_parms.string_arg[1]),4) <> 'FCST' then
			uf_depr_reg_entries()
		end if
	end if

	commit;
	//only send emails for normal depr, not fcst
	if upper(g_ssp_parms.string_arg[1]) = 'INDIVIDUAL' or upper(g_ssp_parms.string_arg[1]) = 'GROUP' then 
		uf_send_email(process_description)
	end if
	rtn = rtn_success
else
	f_pp_msgs("	ERROR: " )
	if sqlca.sqlcode <> 0 then 
		f_pp_msgs("	SQL Error Code: " + string(sqlca.sqldbcode))
		f_pp_msgs("	SQL Error Message: " + sqlca.sqlerrtext)
	end if 
	rollback;
	//only send emails for normal depr, not fcst
	if upper(g_ssp_parms.string_arg[1]) = 'INDIVIDUAL' or upper(g_ssp_parms.string_arg[1]) = 'GROUP' then 
		uf_send_email(process_description)
	end if 
	rtn = rtn_failure
end if

if b_monthEnd then
	// Release the concurrency lock
	i_nvo_cpr_control.of_releaseProcess(process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)
end if

return rtn
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = this
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function longlong uf_individualresults ();/*
*	@@ DESCRIPTION
*		This function loads the successful results to depr_ledger
*	@@PARAM:
*		NONE
*		
*	@@RETURN: number
*		1 or -1	
*/

/// we handled errors.
// now load cpr_depr with successes
update cpr_depr c
set
(
	depreciation_base, curr_depr_expense, month_rate, depr_exp_alloc_adjust, salvage_expense,
	salvage_exp_alloc_adjust, depr_reserve, 
	mid_period_conv, mid_period_method, depr_method_id,depr_group_id, true_up_cpr_depr,
        remaining_life, asset_Dollars, init_life
) =
(
	select nvl(s.depreciation_base,0), nvl(s.curr_depr_expense,0), nvl(s.month_rate,0), nvl(s.depr_exp_alloc_adjust,0) + nvl(s.trueup_adj,0) + nvl(s.nurv_adj,0), nvl(s.salvage_expense,0),
		nvl(s.salvage_exp_alloc_adjust,0), depr_reserve, 
		mid_period_conv, mid_period_method, depr_method_id, depr_group_id, true_up_cpr_depr,
                remaining_life, nvl(s.asset_dollars,0), init_life
	from cpr_depr_calc_stg s
	where s.depr_calc_status = 9
	and s.depr_group_id = c.depr_group_id
	and s.asset_id = c.asset_id
	and s.set_of_books_id = c.set_of_books_id
	and s.gl_posting_mo_yr = c.gl_posting_mo_yr
)
where exists
(
	select 1
	from cpr_depr_calc_stg s
	where s.depr_calc_status = 9
	and s.depr_group_id = c.depr_group_id
	and s.asset_id = c.asset_id
	and s.set_of_books_id = c.set_of_books_id
	and s.gl_posting_mo_yr = c.gl_posting_mo_yr
);
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating cpr_depr: " + sqlca.sqlErrtext)
	return -1
else 
	f_pp_msgs ("Successfully updated cpr_depr. Number of rows updated: " + string(sqlca.sqlnrows))
end if


// now load depr ledger with successes

update depr_ledger c set
(
	depreciation_base, depreciation_expense, depr_exp_alloc_adjust,
	salvage_expense, salvage_exp_alloc_adjust, end_reserve,
	end_Balance, begin_reserve, begin_balance,
	depr_exp_adjust, reserve_adjustments, salvage_exp_adjust
) = 
(
	select sum(s.depreciation_base), sum(s.curr_depr_expense), sum(s.depr_exp_alloc_adjust) + sum(s.trueup_adj) + sum(s.nurv_adj),
		sum(s.salvage_expense), sum(s.salvage_exp_alloc_adjust), sum(s.depr_reserve),
		sum(s.asset_dollars), sum(s.beg_reserve_month), sum(s.beg_asset_dollars),
		sum(s.depr_exp_adjust), sum(s.reserve_adjustment), sum(s.salvage_exp_adjust)
	from cpr_depr_calc_stg s
	where s.depr_calc_status = 9
	and s.depr_group_id = c.depr_group_id
	and s.set_of_books_id = c.set_of_books_id
	and s.gl_posting_mo_yr = c.gl_post_mo_yr
)
where exists
(
	select 1
	from cpr_depr_calc_stg s
	where s.depr_calc_status = 9
	and s.depr_group_id = c.depr_group_id
	and s.set_of_books_id = c.set_of_books_id
	and s.gl_posting_mo_yr = c.gl_post_mo_yr
)
;
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating depr_ledger: " + sqlca.sqlErrtext)
	return -1
else 
	f_pp_msgs ("Successfully updated depr_ledger. Number of rows updated: " + string(sqlca.sqlnrows))
end if

//TODO: this depr uop rollforward code is a best guess, old code should be debugged to confirm it's right
UPDATE depr_group_uop uop
SET (uop.uop_depr_expense, uop.uop_depr_expense_2, uop.rate_depr_expense, uop.ledger_depr_expense, uop.uop_exp_adj) = (
	SELECT sum(stg.curr_Depr_expense), sum(stg.curr_Depr_expense), sum(stg.curr_Depr_expense), sum(stg.curr_Depr_expense), sum(stg.depr_exp_adjust)
	FROM cpr_depr_calc_stg stg
	WHERE uop.depr_group_id = stg.depr_group_id
	AND uop.set_of_books_id = stg.set_of_books_id
	AND uop.gl_post_mo_yr = stg.gl_posting_mo_yr
	AND stg.depr_calc_status = 9
	and upper(stg.MID_PERIOD_METHOD) = 'UOPR'
	group by stg.DEPR_GROUP_ID, stg.SET_OF_BOOKS_ID, stg.GL_POSTING_MO_YR)
WHERE EXISTS (
	SELECT 1
	FROM cpr_depr_calc_stg stg
	WHERE uop.depr_group_id = stg.depr_group_id
	AND uop.set_of_books_id = stg.set_of_books_id
	AND uop.gl_post_mo_yr = stg.gl_posting_mo_yr
	AND upper(stg.MID_PERIOD_METHOD) = 'UOPR'
	AND stg.depr_calc_status = 9);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating current month's depr_group_uop: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_group_uop records updated for current month. Updating depr_method_uop from depr_calc_amort_stg for next month now...")

//update next month's depr_method_uop where it exists
UPDATE depr_method_uop uop
SET (estimated_production) = (
	SELECT distinct nvl(stg.estimated_production,0) - decode(stg.net_gross, 1, nvl(production,0), 0)
	FROM cpr_depr_calc_stg stg
	WHERE stg.depr_method_id = uop.depr_method_id
	AND stg.set_of_books_id = uop.set_of_books_id
	AND add_months(stg.gl_posting_mo_yr,1) = uop.gl_post_mo_yr /*add one month so that we are joining to the next month in depr_method_uop.*/
	AND upper(stg.mid_period_method) = 'UOPR'
	AND stg.depr_calc_status = 9)
WHERE EXISTS (
	SELECT 1 
	FROM cpr_depr_calc_stg stg
	WHERE stg.depr_method_id = uop.depr_method_id
	AND stg.set_of_books_id = uop.set_of_books_id
	AND add_months(stg.gl_posting_mo_yr,1) = uop.gl_post_mo_yr
	AND upper(stg.mid_period_method) = 'UOPR'
	AND stg.depr_calc_status = 9);
	
if sqlca.sqlcode < 0 then
	f_pp_msgs ("ERROR updating next month's depr_method_uop: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_method_uop records updated for next month. Inserting missing depr_method_uop records for next month now...")
	
//insert new depr_method_uop records where they do not exist
INSERT INTO depr_method_uop (depr_method_id, set_of_books_id, gl_post_mo_yr, production, estimated_production, uop_type_id, min_calc_option)
(SELECT DISTINCT uop.depr_method_id, uop.set_of_books_id, add_months(uop.gl_post_mo_yr, 1), uop.production, uop.estimated_production - decode(stg.net_gross, 1, uop.production, 0), uop.uop_type_id,
	uop.min_calc_option
FROM depr_method_uop uop, cpr_depr_calc_stg stg
WHERE uop.depr_method_id = stg.depr_method_id
AND uop.set_of_books_id = stg.set_of_books_id
AND uop.gl_post_mo_yr = stg.gl_posting_mo_yr
AND upper(stg.mid_period_method) = 'UOPR'
AND stg.depr_calc_status = 9
AND NOT EXISTS (
	SELECT 1 
	FROM depr_method_uop uop2
	WHERE uop.depr_method_id = uop2.depr_method_id
	AND uop.set_of_books_id = uop2.set_of_books_id
	AND add_months(uop.gl_post_mo_yr, 1) = uop2.gl_post_mo_yr)
); 
	
if sqlca.sqlcode < 0 then
	f_pp_msgs ("ERROR inserting next month's depr_method_uop records: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_method_uop records inserted for next month. Inserting missing depr_group_uop records for next month now...")
	
//insert new depr_group_uop records where they do not exist
INSERT INTO depr_group_uop (depr_group_id, set_of_books_id, gl_post_mo_yr, min_annual_expense, max_annual_expense, depr_method_id)
(SELECT DISTINCT uop.depr_group_id, uop.set_of_books_id, add_months(uop.gl_post_mo_yr, 1), uop.min_annual_expense, uop.max_annual_expense, uop.depr_method_id
FROM depr_group_uop uop, cpr_depr_calc_stg stg
WHERE uop.depr_group_id = stg.depr_group_id
AND uop.set_of_books_id = stg.set_of_books_id
AND uop.gl_post_mo_yr = stg.gl_posting_mo_yr
AND upper(stg.mid_period_method) = 'UOPR'
AND stg.depr_calc_status = 9
AND NOT EXISTS (
	SELECT 1 
	FROM depr_group_uop uop2
	WHERE uop.depr_group_id = uop2.depr_group_id
	AND uop.set_of_books_id = uop2.set_of_books_id
	AND add_months(uop.gl_post_mo_yr, 1) = uop2.gl_post_mo_yr)
); 
	
if sqlca.sqlcode < 0 then
	f_pp_msgs ("ERROR inserting next month's depr_group_uop records: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_group_uop records inserted for next month.")

if uf_rollfwd_ind() <> 1 then
	return -1
end if

if uf_update_process_control('INDIVIDUAL: '+string(g_ssp_parms.long_arg2[1])) <> 1 then return -1

f_pp_msgs("Exiting uf_individualresults().")
return 1

end function

public function integer uf_individualcalc ();/*
*	@@ DESCRIPTION
*		This function executes the Start month end depreciation procedure from the pp_depr package
*		It then executes the procedure, returning a 1 if successful and -1 if failure
*	@@PARAM:
*		g_ssp_parms.long_arg which is array of company_ids to be processed
*		g_ssp_parms.date_arg which is the gl month depreciation to be processed
*		str_ret is the return from execution that signifies success or failure
*		
*	@@RETURN: number
*		1 or -1	
*/

longlong l_check

sqlca.p_stageMonthEndCPRDepr(g_ssp_parms.long_arg, g_ssp_parms.date_arg, g_ssp_parms.long_arg2[1] )
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR Staging data: " + sqlca.sqlErrText)
	return -1
end if

sqlca.p_calcCPRDepr( g_ssp_parms.date_arg )
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR Calculating: " + sqlca.sqlErrText)
	return -1
end if

return 1
end function

public function integer uf_groupcalc ();/*
*	@@ DESCRIPTION
*		This function executes the Start month end group depreciation procedure from the pp_depr package
*		It then executes the procedure, returning a 1 if successful and -1 if failure
*	@@PARAM:
*		g_ssp_parms.long_arg which is array of company_ids to be processed
*		g_ssp_parms.date_arg[1] which is the gl month depreciation to be processed
*		str_ret is the return from execution that signifies success or failure
*		
*	@@RETURN: number
*		1 or -1	
*/

f_pp_msgs("Staging group depr calc with parameters " + string(g_ssp_parms.long_arg[1]) + " " + string(g_ssp_parms.date_arg[1]) + " " + string(1) )

sqlca.p_stageMonthEndDepr( g_ssp_parms.long_arg, g_ssp_parms.date_arg, 1)
if sqlca.sqlCode <> 0 then
	f_pp_msgs("sqlcode: " + string(sqlca.sqlcode))
	f_pp_msgs("sqldbcode: " + string(sqlca.sqldbcode))
	f_pp_msgs("sqlerrtext: " + sqlca.sqlerrtext)
	return -1
end if

f_pp_msgs("Starting group depr calc.")
// calc depr
// load recurring entries
sqlca.p_calcDepr()
if sqlca.sqlCode <> 0 then
	return -1
end if

return 1
end function

public function longlong uf_groupresults ();/*
*	@@ DESCRIPTION
*		This function loads the successful results to depr_ledger
*	@@PARAM:
*		NONE
*		
*	@@RETURN: number
*		1 or -1	
*/

/// we handled errors.
// now load depr ledger with successes

f_pp_msgs("Updating depr_ledger from depr_calc_stg now...")

update depr_ledger d set (end_balance, depreciation_base, depreciation_rate, depreciation_expense, depr_exp_alloc_adjust, salvage_base,
	salvage_rate, salvage_expense, salvage_exp_alloc_adjust, impairment_reserve_end, 
	reserve_bal_impairment, end_reserve, reserve_bal_provision,
	reserve_bal_cor, salvage_balance, reserve_bal_adjust, reserve_bal_retirements,
	reserve_bal_tran_in, reserve_bal_tran_out, reserve_bal_other_credits, reserve_bal_gain_loss,
	reserve_bal_salvage_exp, est_ann_net_adds, cor_end_reserve, cost_of_removal_rate, 
	cost_of_removal_base, cor_expense, cor_exp_alloc_adjust, 
	current_net_salvage_amort, RESERVE_BLENDING_TRANSFER, RESERVE_BLENDING_ADJUSTMENT,
	COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER, DEPR_EXP_ADJUST, COR_EXP_ADJUST, SALVAGE_EXP_ADJUST
	) = 
	(select s.end_balance, s.depreciation_base, s.depreciation_rate, s.depreciation_expense, 
	(s.depr_exp_alloc_adjust + s.retro_depr_adj + s.over_depr_adj + s.curve_trueup_adj + s.uop_exp_adj + s.combined_depr_adj) /*depr_exp_alloc_adjust*/, 
	s.salvage_base, s.salvage_rate, s.salvage_expense, 
	(s.salvage_exp_alloc_adjust + s.retro_salv_adj + s.over_depr_adj_salv + s.curve_trueup_adj_salv + s.combined_salv_adj) /*salv_exp_alloc_adjust*/, 
	s.impairment_reserve_end, 
	s.reserve_bal_impairment, s.end_reserve, s.reserve_bal_provision,
	s.reserve_bal_cor, s.salvage_balance, s.reserve_bal_adjust, s.reserve_bal_retirements,
	s.reserve_bal_tran_in, s.reserve_bal_tran_out, s.reserve_bal_other_credits, s.reserve_bal_gain_loss,
	s.reserve_bal_salvage_exp, s.est_ann_net_adds, s.cor_end_reserve, s.cost_of_removal_rate, 
	s.cost_of_removal_base, s.cor_expense, 
	(s.cor_exp_alloc_adjust + s.retro_cor_adj + s.over_depr_adj_cor + s.curve_trueup_adj_cor /*CBS TODO + s.uop_cor_adj*/+ s.combined_cor_adj) /*cor_exp_alloc_adjust*/,
	0, s.RESERVE_BLENDING_TRANSFER, s.RESERVE_BLENDING_ADJUSTMENT,
	s.COR_BLENDING_ADJUSTMENT, s.COR_BLENDING_TRANSFER, S.DEPR_EXP_ADJUST, S.COR_EXP_ADJUST, S.SALVAGE_EXP_ADJUST
	from depr_calc_stg s
	where d.depr_group_id = s.depr_group_id
	and d.set_of_books_id = s.set_of_books_id
	and d.gl_post_mo_yr = s.gl_post_mo_yr)
where exists
	(select 1
	from depr_calc_stg s
	where d.depr_group_id = s.depr_group_id
	and d.set_of_books_id = s.set_of_books_id
	and d.gl_post_mo_yr = s.gl_post_mo_yr
	and s.depr_calc_status = 9
	and d.depr_ledger_status <> 1);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating depr_ledger: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_ledger records updated. Updating depr_net_salvage_amor.")

//join to depr_calc_stg so we can check the status. 
update depr_net_salvage_amort a
set (cor_treatment, salvage_treatment, net_salvage_amort_life, cost_of_removal_bal, cost_of_removal_amort, cost_of_removal_reserve, salvage_bal, salvage_amort, salvage_reserve) = (
	select b.cor_treatment, b.salv_treatment, b.cor_life, b.cor_balance, b.cor_amort, b.cor_reserve, b.salv_balance, b.salv_amort, b.salv_reserve
	from depr_calc_amort_stg b, depr_calc_stg c
	where a.depr_group_id = b.depr_group_id
	and a.set_of_books_id = b.set_of_books_id
	and a.gl_post_mo_yr = b.gl_post_mo_yr
	and a.vintage = b.vintage
	and b.depr_group_id = c.depr_group_id
	and b.set_of_books_id = c.set_of_books_id
	and b.gl_post_mo_yr = c.gl_post_mo_yr
	and c.depr_calc_status = 9
)
where exists (
	select 1 
	from depr_calc_amort_stg b, depr_calc_stg c
	where a.depr_group_id = b.depr_group_id
	and a.set_of_books_id = b.set_of_books_id
	and a.gl_post_mo_yr = b.gl_post_mo_yr
	and a.vintage = b.vintage
	and b.depr_group_id = c.depr_group_id
	and b.set_of_books_id = c.set_of_books_id 
	and b.gl_post_mo_yr = c.gl_post_mo_yr
	and c.depr_calc_status = 9
);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating depr_net_salvage_amort: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_net_salvage_amort records updated. Inserting missing records into depr_net_salvage_amort.")

//insert into depr_net_salvage_amort where records do not exist. This can happen during january while doing monthly amortization. 
insert into depr_net_salvage_amort (set_of_books_id, depr_group_id, gl_post_mo_yr, vintage, cor_treatment, salvage_treatment, net_salvage_amort_life, 
	cost_of_removal_bal, cost_of_removal_amort, cost_of_removal_reserve, salvage_bal, salvage_amort, salvage_reserve)
select b.set_of_books_id, b.depr_group_id, b.gl_post_mo_yr, b.vintage, b.cor_treatment, b.salv_treatment, b.cor_life, 
	b.cor_balance, b.cor_amort, b.cor_reserve, b.salv_balance, b.salv_amort, b.salv_reserve
from depr_calc_amort_stg b, depr_calc_stg c
where b.depr_group_id = c.depr_group_id
and b.set_of_books_id = c.set_of_books_id
and b.gl_post_mo_yr = c.gl_post_mo_yr
and c.depr_calc_status = 9
and not exists (
	select 1 
	from depr_net_salvage_amort dnsa
	where dnsa.depr_group_id = b.depr_group_id
	and dnsa.set_of_books_id = b.set_of_books_id
	and dnsa.gl_post_mo_yr = b.gl_post_mo_yr
	and dnsa.vintage = b.vintage
);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR inserting missing records into depr_net_salvage_amort: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_net_salvage_amort records created. Updating depr_ledger_blending.")

//Move over depr_ledger_Blending
string sqls

sqls = 'merge into DEPR_LEDGER_BLENDING DLB '
sqls += 'using (select DEPR_GROUP_ID, SOURCE_SOB_ID, GL_POST_MO_YR, DEPR_LEDGER_STATUS, SOURCE_PERCENT, BEGIN_RESERVE_BL, END_RESERVE_BL,  '
sqls += '		RESERVE_BAL_PROVISION_BL, RESERVE_BAL_COR_BL, SALVAGE_BALANCE_BL, RESERVE_BAL_ADJUST_BL, RESERVE_BAL_RETIREMENTS_BL,  '
sqls += '		RESERVE_BAL_TRAN_IN_BL, RESERVE_BAL_TRAN_OUT_BL, RESERVE_BAL_OTHER_CREDITS_BL, RESERVE_BAL_GAIN_LOSS_BL, '
sqls += '		RESERVE_ALLOC_FACTOR_BL, BEGIN_BALANCE_BL, ADDITIONS_BL, RETIREMENTS_BL, TRANSFERS_IN_BL, '
sqls += '		TRANSFERS_OUT_BL, ADJUSTMENTS_BL, DEPRECIATION_BASE_BL, END_BALANCE_BL, DEPRECIATION_RATE_BL, '
sqls += '		DEPRECIATION_EXPENSE_BL, DEPR_EXP_ADJUST_BL, (DEPR_EXP_ALLOC_ADJUST_BL + RETRO_DEPR_ADJ_BL + OVER_DEPR_ADJ_BL) D_ALLOC, COST_OF_REMOVAL_BL, '
sqls += '		RESERVE_RETIREMENTS_BL, SALVAGE_RETURNS_BL, SALVAGE_CASH_BL, RESERVE_CREDITS_BL, '
sqls += '		RESERVE_ADJUSTMENTS_BL, RESERVE_TRAN_IN_BL, RESERVE_TRAN_OUT_BL, GAIN_LOSS_BL, '
sqls += '		VINTAGE_NET_SALVAGE_AMORT_BL, VINTAGE_NET_SALVAGE_RESERVE_BL, CURRENT_NET_SALVAGE_AMORT_BL, '
sqls += '		CURRENT_NET_SALVAGE_RESERVE_BL, IMPAIRMENT_RESERVE_BEG_BL, IMPAIRMENT_RESERVE_ACT_BL, '
sqls += '		IMPAIRMENT_RESERVE_END_BL, EST_ANN_NET_ADDS_BL, RWIP_ALLOCATION_BL, COR_BEG_RESERVE_BL, '
sqls += '		COR_EXPENSE_BL, COR_EXP_ADJUST_BL, (COR_EXP_ALLOC_ADJUST_BL + RETRO_COR_ADJ_BL + OVER_DEPR_ADJ_COR_BL) C_ALLOC, COR_RES_TRAN_IN_BL, COR_RES_TRAN_OUT_BL, '
sqls += '		COR_RES_ADJUST_BL, COR_END_RESERVE_BL, COST_OF_REMOVAL_RATE_BL, COST_OF_REMOVAL_BASE_BL, '
sqls += '		RWIP_COST_OF_REMOVAL_BL, RWIP_SALVAGE_CASH_BL, RWIP_SALVAGE_RETURNS_BL, RWIP_RESERVE_CREDITS_BL, '
sqls += '		SALVAGE_RATE_BL, SALVAGE_BASE_BL, SALVAGE_EXPENSE_BL, SALVAGE_EXP_ADJUST_BL, '
sqls += '		(SALVAGE_EXP_ALLOC_ADJUST_BL + RETRO_SALV_ADJ_BL + OVER_DEPR_ADJ_SALV_BL) S_ALLOC, RESERVE_BAL_SALVAGE_EXP_BL, BLENDING_ADJUSTMENT, '
sqls += '		RESERVE_BLENDING_ADJUSTMENT, COR_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER, '
sqls += '		COR_BLENDING_TRANSFER, IMPAIRMENT_ASSET_AMOUNT_BL, IMPAIRMENT_EXPENSE_AMOUNT_BL, '
sqls += '		RESERVE_BAL_IMPAIRMENT_BL'
sqls += '	from DEPR_LEDGER_BLENDING_STG DLBS) A '
sqls += 'on (DLB.DEPR_GROUP_ID = A.DEPR_GROUP_ID and DLB.SET_OF_BOOKS_ID = A.SOURCE_SOB_ID and DLB.GL_POST_MO_YR = A.GL_POST_MO_YR) '
sqls += 'when not matched then insert'
sqls += '	(DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, DEPR_LEDGER_STATUS, SOURCE_PERCENT, BEGIN_RESERVE_BL, END_RESERVE_BL,'
sqls += '	RESERVE_BAL_PROVISION_BL, RESERVE_BAL_COR_BL, SALVAGE_BALANCE_BL, RESERVE_BAL_ADJUST_BL, RESERVE_BAL_RETIREMENTS_BL,'
sqls += '	RESERVE_BAL_TRAN_IN_BL, RESERVE_BAL_TRAN_OUT_BL, RESERVE_BAL_OTHER_CREDITS_BL, RESERVE_BAL_GAIN_LOSS_BL,'
sqls += '	RESERVE_ALLOC_FACTOR_BL, BEGIN_BALANCE_BL, ADDITIONS_BL, RETIREMENTS_BL, TRANSFERS_IN_BL, '
sqls += '	TRANSFERS_OUT_BL, ADJUSTMENTS_BL, DEPRECIATION_BASE_BL, END_BALANCE_BL, DEPRECIATION_RATE_BL, '
sqls += '	DEPRECIATION_EXPENSE_BL, DEPR_EXP_ADJUST_BL, DEPR_EXP_ALLOC_ADJUST_BL, COST_OF_REMOVAL_BL, '
sqls += '	RESERVE_RETIREMENTS_BL, SALVAGE_RETURNS_BL, SALVAGE_CASH_BL, RESERVE_CREDITS_BL, '
sqls += '	RESERVE_ADJUSTMENTS_BL, RESERVE_TRAN_IN_BL, RESERVE_TRAN_OUT_BL, GAIN_LOSS_BL, '
sqls += '	VINTAGE_NET_SALVAGE_AMORT_BL, VINTAGE_NET_SALVAGE_RESERVE_BL, CURRENT_NET_SALVAGE_AMORT_BL, '
sqls += '	CURRENT_NET_SALVAGE_RESERVE_BL, IMPAIRMENT_RESERVE_BEG_BL, IMPAIRMENT_RESERVE_ACT_BL, '
sqls += '	IMPAIRMENT_RESERVE_END_BL, EST_ANN_NET_ADDS_BL, RWIP_ALLOCATION_BL, COR_BEG_RESERVE_BL, '
sqls += '	COR_EXPENSE_BL, COR_EXP_ADJUST_BL, COR_EXP_ALLOC_ADJUST_BL, COR_RES_TRAN_IN_BL, COR_RES_TRAN_OUT_BL, '
sqls += '	COR_RES_ADJUST_BL, COR_END_RESERVE_BL, COST_OF_REMOVAL_RATE_BL, COST_OF_REMOVAL_BASE_BL, '
sqls += '	RWIP_COST_OF_REMOVAL_BL, RWIP_SALVAGE_CASH_BL, RWIP_SALVAGE_RETURNS_BL, RWIP_RESERVE_CREDITS_BL, '
sqls += '	SALVAGE_RATE_BL, SALVAGE_BASE_BL, SALVAGE_EXPENSE_BL, SALVAGE_EXP_ADJUST_BL, '
sqls += '	SALVAGE_EXP_ALLOC_ADJUST_BL, RESERVE_BAL_SALVAGE_EXP_BL, BLENDING_ADJUSTMENT, '
sqls += '	RESERVE_BLENDING_ADJUSTMENT, COR_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER, '
sqls += '	COR_BLENDING_TRANSFER, IMPAIRMENT_ASSET_AMOUNT_BL, IMPAIRMENT_EXPENSE_AMOUNT_BL, '
sqls += '	RESERVE_BAL_IMPAIRMENT_BL) '
sqls += 'values'
sqls += '	(A.DEPR_GROUP_ID, A.SOURCE_SOB_ID, A.GL_POST_MO_YR, A.DEPR_LEDGER_STATUS, A.SOURCE_PERCENT, A.BEGIN_RESERVE_BL, A.END_RESERVE_BL,  '
sqls += '	A.RESERVE_BAL_PROVISION_BL, A.RESERVE_BAL_COR_BL, A.SALVAGE_BALANCE_BL, A.RESERVE_BAL_ADJUST_BL, A.RESERVE_BAL_RETIREMENTS_BL,  '
sqls += '	A.RESERVE_BAL_TRAN_IN_BL, A.RESERVE_BAL_TRAN_OUT_BL, A.RESERVE_BAL_OTHER_CREDITS_BL, A.RESERVE_BAL_GAIN_LOSS_BL, '
sqls += '	A.RESERVE_ALLOC_FACTOR_BL, A.BEGIN_BALANCE_BL, A.ADDITIONS_BL, A.RETIREMENTS_BL, A.TRANSFERS_IN_BL, '
sqls += '	A.TRANSFERS_OUT_BL, A.ADJUSTMENTS_BL, A.DEPRECIATION_BASE_BL, A.END_BALANCE_BL, A.DEPRECIATION_RATE_BL, '
sqls += '	A.DEPRECIATION_EXPENSE_BL, A.DEPR_EXP_ADJUST_BL, A.D_ALLOC, A.COST_OF_REMOVAL_BL, '
sqls += '	A.RESERVE_RETIREMENTS_BL, A.SALVAGE_RETURNS_BL, A.SALVAGE_CASH_BL, A.RESERVE_CREDITS_BL, '
sqls += '	A.RESERVE_ADJUSTMENTS_BL, A.RESERVE_TRAN_IN_BL, A.RESERVE_TRAN_OUT_BL, A.GAIN_LOSS_BL, '
sqls += '	A.VINTAGE_NET_SALVAGE_AMORT_BL, A.VINTAGE_NET_SALVAGE_RESERVE_BL, A.CURRENT_NET_SALVAGE_AMORT_BL, '
sqls += '	A.CURRENT_NET_SALVAGE_RESERVE_BL, A.IMPAIRMENT_RESERVE_BEG_BL, A.IMPAIRMENT_RESERVE_ACT_BL, '
sqls += '	A.IMPAIRMENT_RESERVE_END_BL, A.EST_ANN_NET_ADDS_BL, A.RWIP_ALLOCATION_BL, A.COR_BEG_RESERVE_BL, '
sqls += '	A.COR_EXPENSE_BL, A.COR_EXP_ADJUST_BL, A.C_ALLOC, A.COR_RES_TRAN_IN_BL, A.COR_RES_TRAN_OUT_BL, '
sqls += '	A.COR_RES_ADJUST_BL, A.COR_END_RESERVE_BL, A.COST_OF_REMOVAL_RATE_BL, A.COST_OF_REMOVAL_BASE_BL, '
sqls += '	A.RWIP_COST_OF_REMOVAL_BL, A.RWIP_SALVAGE_CASH_BL, A.RWIP_SALVAGE_RETURNS_BL, A.RWIP_RESERVE_CREDITS_BL, '
sqls += '	A.SALVAGE_RATE_BL, A.SALVAGE_BASE_BL, A.SALVAGE_EXPENSE_BL, A.SALVAGE_EXP_ADJUST_BL, '
sqls += '	A.S_ALLOC, A.RESERVE_BAL_SALVAGE_EXP_BL, A.BLENDING_ADJUSTMENT, '
sqls += '	A.RESERVE_BLENDING_ADJUSTMENT, A.COR_BLENDING_ADJUSTMENT, A.RESERVE_BLENDING_TRANSFER, '
sqls += '	A.COR_BLENDING_TRANSFER, A.IMPAIRMENT_ASSET_AMOUNT_BL, A.IMPAIRMENT_EXPENSE_AMOUNT_BL, '
sqls += '		A.RESERVE_BAL_IMPAIRMENT_BL) '
sqls += 'when matched then update '
sqls += 'set	DLB.BEGIN_RESERVE_BL = A.BEGIN_RESERVE_BL, '
sqls += '		DLB.END_RESERVE_BL = A.END_RESERVE_BL, '
sqls += '		DLB.RESERVE_BAL_PROVISION_BL = A.RESERVE_BAL_PROVISION_BL, '
sqls += '		DLB.RESERVE_BAL_COR_BL = A.RESERVE_BAL_COR_BL, '
sqls += '		DLB.SALVAGE_BALANCE_BL = A.SALVAGE_BALANCE_BL, '
sqls += '		DLB.RESERVE_BAL_ADJUST_BL = A.RESERVE_BAL_ADJUST_BL, '
sqls += '		DLB.RESERVE_BAL_RETIREMENTS_BL = A.RESERVE_BAL_RETIREMENTS_BL, '
sqls += '		DLB.RESERVE_BAL_TRAN_IN_BL = A.RESERVE_BAL_TRAN_IN_BL, '
sqls += '		DLB.RESERVE_BAL_TRAN_OUT_BL = A.RESERVE_BAL_TRAN_OUT_BL, '
sqls += '		DLB.RESERVE_BAL_OTHER_CREDITS_BL = A.RESERVE_BAL_OTHER_CREDITS_BL, '
sqls += '		DLB.RESERVE_BAL_GAIN_LOSS_BL = A.RESERVE_BAL_GAIN_LOSS_BL, '
sqls += '		DLB.RESERVE_ALLOC_FACTOR_BL = A.RESERVE_ALLOC_FACTOR_BL, '
sqls += '		DLB.BEGIN_BALANCE_BL = A.BEGIN_BALANCE_BL, '
sqls += '		DLB.ADDITIONS_BL = A.ADDITIONS_BL, '
sqls += '		DLB.RETIREMENTS_BL = A.RETIREMENTS_BL, '
sqls += '		DLB.TRANSFERS_IN_BL = A.TRANSFERS_IN_BL, '
sqls += '		DLB.TRANSFERS_OUT_BL = A.TRANSFERS_OUT_BL, '
sqls += '		DLB.ADJUSTMENTS_BL = A.ADJUSTMENTS_BL, '
sqls += '		DLB.DEPRECIATION_BASE_BL = A.DEPRECIATION_BASE_BL, '
sqls += '		DLB.END_BALANCE_BL = A.END_BALANCE_BL, '
sqls += '		DLB.DEPRECIATION_RATE_BL = A.DEPRECIATION_RATE_BL, '
sqls += '		DLB.DEPRECIATION_EXPENSE_BL = A.DEPRECIATION_EXPENSE_BL, '
sqls += '		DLB.DEPR_EXP_ADJUST_BL = A.DEPR_EXP_ADJUST_BL, '
sqls += '		DLB.DEPR_EXP_ALLOC_ADJUST_BL = A.D_ALLOC, '
sqls += '		DLB.COST_OF_REMOVAL_BL = A.COST_OF_REMOVAL_BL, '
sqls += '		DLB.RESERVE_RETIREMENTS_BL = A.RESERVE_RETIREMENTS_BL, '
sqls += '		DLB.SALVAGE_RETURNS_BL = A.SALVAGE_RETURNS_BL, '
sqls += '		DLB.SALVAGE_CASH_BL = A.SALVAGE_CASH_BL, '
sqls += '		DLB.RESERVE_CREDITS_BL = A.RESERVE_CREDITS_BL, '
sqls += '		DLB.RESERVE_ADJUSTMENTS_BL = A.RESERVE_ADJUSTMENTS_BL, '
sqls += '		DLB.RESERVE_TRAN_IN_BL = A.RESERVE_TRAN_IN_BL, '
sqls += '		DLB.RESERVE_TRAN_OUT_BL = A.RESERVE_TRAN_OUT_BL, '
sqls += '		DLB.GAIN_LOSS_BL = A.GAIN_LOSS_BL, '
sqls += '		DLB.VINTAGE_NET_SALVAGE_AMORT_BL = A.VINTAGE_NET_SALVAGE_AMORT_BL, '
sqls += '		DLB.VINTAGE_NET_SALVAGE_RESERVE_BL = A.VINTAGE_NET_SALVAGE_RESERVE_BL, '
sqls += '		DLB.CURRENT_NET_SALVAGE_AMORT_BL = A.CURRENT_NET_SALVAGE_AMORT_BL, '
sqls += '		DLB.CURRENT_NET_SALVAGE_RESERVE_BL = A.CURRENT_NET_SALVAGE_RESERVE_BL, '
sqls += '		DLB.IMPAIRMENT_RESERVE_BEG_BL = A.IMPAIRMENT_RESERVE_BEG_BL, '
sqls += '		DLB.IMPAIRMENT_RESERVE_ACT_BL = A.IMPAIRMENT_RESERVE_ACT_BL, '
sqls += '		DLB.IMPAIRMENT_RESERVE_END_BL = A.IMPAIRMENT_RESERVE_END_BL, '
sqls += '		DLB.EST_ANN_NET_ADDS_BL = A.EST_ANN_NET_ADDS_BL, '
sqls += '		DLB.RWIP_ALLOCATION_BL = A.RWIP_ALLOCATION_BL, '
sqls += '		DLB.COR_BEG_RESERVE_BL = A.COR_BEG_RESERVE_BL, '
sqls += '		DLB.COR_EXPENSE_BL = A.COR_EXPENSE_BL, '
sqls += '		DLB.COR_EXP_ADJUST_BL = A.COR_EXP_ADJUST_BL, '
sqls += '		DLB.COR_EXP_ALLOC_ADJUST_BL = A.C_ALLOC, '
sqls += '		DLB.COR_RES_TRAN_IN_BL = A.COR_RES_TRAN_IN_BL, '
sqls += '		DLB.COR_RES_TRAN_OUT_BL = A.COR_RES_TRAN_OUT_BL, '
sqls += '		DLB.COR_RES_ADJUST_BL = A.COR_RES_ADJUST_BL, '
sqls += '		DLB.COR_END_RESERVE_BL = A.COR_END_RESERVE_BL, '
sqls += '		DLB.COST_OF_REMOVAL_RATE_BL = A.COST_OF_REMOVAL_RATE_BL, '
sqls += '		DLB.COST_OF_REMOVAL_BASE_BL = A.COST_OF_REMOVAL_BASE_BL, '
sqls += '		DLB.RWIP_COST_OF_REMOVAL_BL = A.RWIP_COST_OF_REMOVAL_BL, '
sqls += '		DLB.RWIP_SALVAGE_CASH_BL = A.RWIP_SALVAGE_CASH_BL, '
sqls += '		DLB.RWIP_SALVAGE_RETURNS_BL = A.RWIP_SALVAGE_RETURNS_BL, '
sqls += '		DLB.RWIP_RESERVE_CREDITS_BL = A.RWIP_RESERVE_CREDITS_BL, '
sqls += '		DLB.SALVAGE_RATE_BL = A.SALVAGE_RATE_BL, '
sqls += '		DLB.SALVAGE_BASE_BL = A.SALVAGE_BASE_BL, '
sqls += '		DLB.SALVAGE_EXPENSE_BL = A.SALVAGE_EXPENSE_BL, '
sqls += '		DLB.SALVAGE_EXP_ADJUST_BL = A.SALVAGE_EXP_ADJUST_BL, '
sqls += '		DLB.SALVAGE_EXP_ALLOC_ADJUST_BL = A.S_ALLOC, '
sqls += '		DLB.RESERVE_BAL_SALVAGE_EXP_BL = A.RESERVE_BAL_SALVAGE_EXP_BL, '
sqls += '		DLB.BLENDING_ADJUSTMENT = A.BLENDING_ADJUSTMENT, '
sqls += '		DLB.RESERVE_BLENDING_ADJUSTMENT = A.RESERVE_BLENDING_ADJUSTMENT, '
sqls += '		DLB.COR_BLENDING_ADJUSTMENT = A.COR_BLENDING_ADJUSTMENT, '
sqls += '		DLB.RESERVE_BLENDING_TRANSFER = A.RESERVE_BLENDING_TRANSFER, '
sqls += '		DLB.COR_BLENDING_TRANSFER = A.COR_BLENDING_TRANSFER, '
sqls += '		DLB.IMPAIRMENT_ASSET_AMOUNT_BL = A.IMPAIRMENT_ASSET_AMOUNT_BL, '
sqls += '		DLB.IMPAIRMENT_EXPENSE_AMOUNT_BL = A.IMPAIRMENT_EXPENSE_AMOUNT_BL, '
sqls += '		DLB.RESERVE_BAL_IMPAIRMENT_BL = A.RESERVE_BAL_IMPAIRMENT_BL, '
sqls += '		DLB.SOURCE_PERCENT = A.SOURCE_PERCENT'

if not isNull(sqls) then
	execute immediate :sqls;
else
	f_pp_msgs("Error updating DEPR_LEDGER_BLENDING. Null string passed to execute immediate.")
	return -1
end if
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating DEPR_LEDGER_BLENDING: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_ledger_blending records updated. Updating depr_group_uop.")

UPDATE depr_group_uop uop
SET (uop.uop_depr_expense, uop.uop_depr_expense_2, uop.rate_depr_expense, uop.ledger_depr_expense, uop.uop_exp_adj) = (
	SELECT stg.curr_uop_exp, stg.curr_uop_exp_2, stg.curr_rate_exp, stg.depreciation_expense, stg.uop_exp_adj
	FROM depr_calc_stg stg
	WHERE uop.depr_group_id = stg.depr_group_id
	AND uop.set_of_books_id = stg.set_of_books_id
	AND uop.gl_post_mo_yr = stg.gl_post_mo_yr
	AND stg.mid_period_method = 'uop'
	AND stg.depr_calc_status = 9)
WHERE EXISTS (
	SELECT 1
	FROM depr_calc_stg stg
	WHERE uop.depr_group_id = stg.depr_group_id
	AND uop.set_of_books_id = stg.set_of_books_id
	AND uop.gl_post_mo_yr = stg.gl_post_mo_yr
	AND stg.mid_period_method = 'uop'
	AND stg.depr_calc_status = 9);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating current month's depr_group_uop: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_group_uop records updated for current month. Updating depr_method_uop.")

//update next month's depr_method_uop where it exists
UPDATE depr_method_uop uop
SET (estimated_production, estimated_production_2) = (
	SELECT distinct nvl(stg.estimated_production,0) - decode(stg.net_gross, 1, nvl(production,0), 0), nvl(stg.estimated_production_2,0) - decode(stg.net_gross, 1, nvl(production_2, 0), 0)
	FROM depr_calc_stg stg
	WHERE stg.depr_method_id = uop.depr_method_id
	AND stg.set_of_books_id = uop.set_of_books_id
	AND add_months(stg.gl_post_mo_yr,1) = uop.gl_post_mo_yr /*add one month so that we are joining to the next month in depr_method_uop.*/
	AND stg.mid_period_method = 'uop'
	AND stg.depr_calc_status = 9)
WHERE EXISTS (
	SELECT distinct 1 
	FROM depr_calc_stg stg
	WHERE stg.depr_method_id = uop.depr_method_id
	AND stg.set_of_books_id = uop.set_of_books_id
	AND add_months(stg.gl_post_mo_yr,1) = uop.gl_post_mo_yr
	AND stg.mid_period_method = 'uop'
	AND stg.depr_calc_status = 9);
	
if sqlca.sqlcode < 0 then
	f_pp_msgs ("ERROR updating next month's depr_method_uop: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_method_uop records updated for next month. Inserting missing depr_method_uop records for next month now...")
	
//insert new depr_method_uop records where they do not exist
INSERT INTO depr_method_uop (depr_method_id, set_of_books_id, gl_post_mo_yr, production, estimated_production, uop_type_id, production_2, estimated_production_2, uop_type_id2, min_calc_option)
(SELECT distinct uop.depr_method_id, uop.set_of_books_id, add_months(uop.gl_post_mo_yr, 1), uop.production, uop.estimated_production - decode(stg.net_gross, 1, uop.production, 0), uop.uop_type_id,
	uop.production_2, uop.estimated_production_2 - decode(stg.net_gross, 1, uop.production_2, 0), uop.uop_type_id2, uop.min_calc_option
FROM depr_method_uop uop, depr_calc_stg stg
WHERE uop.depr_method_id = stg.depr_method_id
AND uop.set_of_books_id = stg.set_of_books_id
AND uop.gl_post_mo_yr = stg.gl_post_mo_yr
AND stg.mid_period_method = 'uop'
AND stg.depr_calc_status = 9
AND NOT EXISTS (
	SELECT distinct 1 
	FROM depr_method_uop uop2
	WHERE uop.depr_method_id = uop2.depr_method_id
	AND uop.set_of_books_id = uop2.set_of_books_id
	AND add_months(uop.gl_post_mo_yr, 1) = uop2.gl_post_mo_yr)
); 
	
if sqlca.sqlcode < 0 then
	f_pp_msgs ("ERROR inserting next month's depr_method_uop records: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_method_uop records inserted for next month. Inserting missing depr_group_uop records for next month now...")
	
//insert new depr_group_uop records where they do not exist
INSERT INTO depr_group_uop (depr_group_id, set_of_books_id, gl_post_mo_yr, min_annual_expense, max_annual_expense, depr_method_id)
(SELECT uop.depr_group_id, uop.set_of_books_id, add_months(uop.gl_post_mo_yr, 1), uop.min_annual_expense, uop.max_annual_expense, uop.depr_method_id
FROM depr_group_uop uop, depr_calc_stg stg
WHERE uop.depr_group_id = stg.depr_group_id
AND uop.set_of_books_id = stg.set_of_books_id
AND uop.gl_post_mo_yr = stg.gl_post_mo_yr
AND stg.mid_period_method = 'uop'
AND stg.depr_calc_status = 9
AND NOT EXISTS (
	SELECT 1 
	FROM depr_group_uop uop2
	WHERE uop.depr_group_id = uop2.depr_group_id
	AND uop.set_of_books_id = uop2.set_of_books_id
	AND add_months(uop.gl_post_mo_yr, 1) = uop2.gl_post_mo_yr)
); 
	
if sqlca.sqlcode < 0 then
	f_pp_msgs ("ERROR inserting next month's depr_group_uop records: " + sqlca.sqlErrtext)
	return -1
end if

f_pp_msgs(string(sqlca.sqlnrows) + " depr_group_uop records inserted for next month.")

if uf_rollfwd() <> 1 then
	return -1
end if

if uf_update_process_control('GROUP') <> 1 then return -1

f_pp_msgs("Exiting uf_groupresults().")
return 1

end function

public function longlong uf_rollfwd ();update depr_ledger a 
set (begin_reserve, begin_balance, impairment_reserve_beg, 
		cor_beg_reserve, impairment_asset_begin_balance,
		RESERVE_BAL_PROVISION, RESERVE_BAL_COR, RESERVE_BAL_ADJUST,
		RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
		RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR,
		SALVAGE_BALANCE, RESERVE_BAL_IMPAIRMENT, IMPAIRMENT_RESERVE_END, RESERVE_BAL_SALVAGE_EXP) =
(select b.end_reserve, b.end_balance, impairment_reserve_end, 
		cor_end_reserve, impairment_asset_begin_balance + impairment_asset_activity_salv,
		RESERVE_BAL_PROVISION, RESERVE_BAL_COR, RESERVE_BAL_ADJUST,
		RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
		RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR,
		SALVAGE_BALANCE, RESERVE_BAL_IMPAIRMENT, IMPAIRMENT_RESERVE_END, RESERVE_BAL_SALVAGE_EXP
from depr_ledger b 
where a.set_of_books_id = b.set_of_books_id and
a.depr_group_id = b.depr_group_id and 
add_months(a.gl_post_mo_yr, -1) =b.gl_post_mo_yr
)
where exists
(select 1
from depr_calc_stg s
where a.depr_group_id = s.depr_group_id
and a.set_of_books_id = s.set_of_books_id
and add_months(a.gl_post_mo_yr, -1) = s.gl_post_mo_yr
and s.depr_calc_status = 9);

if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR rolling forward DEPR_LEDGER: " + sqlca.sqlErrtext)
	return -1
else 
	f_pp_msgs ("Successfully rolled forward DEPR_LEDGER. Number of rows updated: " + string(sqlca.sqlnrows))
end if

return 1
end function

public function longlong uf_rollfwd_ind ();update depr_ledger a 
set (begin_reserve, begin_balance, impairment_reserve_beg, cor_beg_reserve, impairment_asset_begin_balance) =
(select b.end_reserve, b.end_balance, impairment_reserve_end, cor_end_reserve, impairment_asset_begin_balance + impairment_asset_activity_salv
from depr_ledger b 
where a.set_of_books_id = b.set_of_books_id and
a.depr_group_id = b.depr_group_id and 
add_months(a.gl_post_mo_yr, -1) =b.gl_post_mo_yr
)
where exists
(select 1
from cpr_depr_calc_Stg s
where a.depr_group_id = s.depr_group_id
and a.set_of_books_id = s.set_of_books_id
and add_months(a.gl_post_mo_yr,-1) = s.gl_posting_mo_yr
and s.depr_calc_status = 9);
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR rolling forward DEPR_LEDGER: " + sqlca.sqlErrtext)
	return -1
else 
	f_pp_msgs ("Successfully rolled forward DEPR_LEDGER. Number of rows updated: " + string(sqlca.sqlnrows))
end if

update cpr_depr a 
set (beg_reserve_month,beg_asset_dollars, 
ytd_depr_expense, ytd_depr_exp_adjust,
beg_reserve_year, remaining_life,
impairment_asset_begin_balance
) = 
(select nvl(b.depr_reserve,0), nvl(b.asset_dollars,0), 
decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0,nvl(b.ytd_depr_expense,0)), 
decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0,nvl(b.ytd_depr_exp_adjust,0)), 
decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,nvl(b.depr_reserve,0),nvl(b.beg_reserve_year,0)), 
greatest(b.remaining_life - decode(nvl(d.init_life, -1),-1,
decode(b.init_life,b.remaining_life,b.mid_period_conv,1),1), 0),
b.impairment_asset_begin_balance + b.impairment_asset_activity_salv
from cpr_depr b, cpr_depr d
where a.asset_id = b.asset_id 
and a.set_of_books_id = b.set_of_books_id
and b.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1)
and b.asset_id = d.asset_id (+)
and b.set_of_books_id = d.set_of_books_id (+)
and add_months(b.gl_posting_mo_yr,-1) = d.gl_posting_Mo_yr (+)
)
where exists
(select 1
from cpr_depr_calc_Stg s
where a.depr_group_id = s.depr_group_id
and a.set_of_books_id = s.set_of_books_id
and a.asset_id = s.asset_id
and add_months(a.gl_posting_mo_yr, -1) = s.gl_posting_mo_yr
and s.depr_calc_status = 9);

if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating cpr_depr: " + sqlca.sqlErrtext)
	return -1
else 
	f_pp_msgs ("Successfully rolled forward cpr_depr. Number of rows updated: " + string(sqlca.sqlnrows))
end if

update cpr_depr a 
set (prior_ytd_depr_expense,prior_ytd_depr_exp_adjust) = 
(select nvl(b.ytd_depr_expense,0), nvl(b.ytd_depr_exp_adjust,0)
from cpr_depr b
where a.asset_id = b.asset_id 
and a.set_of_books_id = b.set_of_books_id
and b.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-12)
)
where exists
(select 1
from cpr_depr_calc_Stg s
where a.depr_group_id = s.depr_group_id
and a.set_of_books_id = s.set_of_books_id
and add_months(a.gl_posting_mo_yr, -1) = s.gl_posting_mo_yr
and a.asset_id = s.asset_id
and s.depr_calc_status = 9);
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating cpr_depr part 2: " + sqlca.sqlErrtext)
	return -1
end if

return 1
end function

public function integer uf_fcstfp ();/*
*	@@ DESCRIPTION
*		This function executes the depreciation forecast procedure by Funding Project
*	@@PARAM:
*		g_ssp_parms.long_arg[1] which is the Fcst Depr Version ID
*		g_ssp_parms.long_arg2 the array of incremental process ids (used to link back to funding project and revision)
*		g_ssp_parms.date_arg[1] is the start month
*		g_ssp_parms.date_arg[2] is the end month
*		
*	@@RETURN: number
*		1 (success) or -1	(failure)
*/
f_pp_msgs("Forecast Version: " + string(g_ssp_parms.long_arg[1]))
f_pp_msgs("Number of Incremental Process IDs: " + string(UPPERBOUND(g_ssp_parms.long_arg2)))
f_pp_msgs("Start Month: " + string(g_ssp_parms.date_arg[1], 'yyyymmdd'))
f_pp_msgs("End Month: " + string(g_ssp_parms.date_arg[ upperbound( g_ssp_parms.date_arg ) ], 'yyyymmdd'))

i_fcst_version_id = g_ssp_parms.long_arg[1]

sqlca.p_stageIncrementalDepr(i_fcst_version_id, g_ssp_parms.long_arg2, g_ssp_parms.date_arg)
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR: Calling p_stageIncrementalDepr")
	return -1
end if

delete from depr_fcst_group_stg;

insert into depr_fcst_group_stg
select a.fcst_depr_group_id
from fcst_depr_group_version a
where a.fcst_depr_version_id = :i_fcst_version_id
and a.subledger_type_id <> 0;

sqlca.p_stageIncrementalCPRDepr(i_fcst_version_id, g_ssp_parms.long_arg2, g_ssp_parms.date_arg)
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR: Calling p_stageIncrementalCPRDepr")
	return -1
end if

// Run the Depr Calc
sqlca.p_calcFCSTIncrementalDepr( g_ssp_parms.date_arg, 0, i_fcst_version_id, 1) 
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR: Calling p_calcFCSTIncrementalDepr")
	return -1
end if

// Run the Depr Calc
sqlca.p_calcFCSTCPRDepr( g_ssp_parms.date_arg, i_fcst_version_id) 
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR: Calling p_calcFCSTCPRDepr")
	return -1
end if

return 1
end function

public function longlong uf_fcstfpresults ();/*
*	@@ DESCRIPTION
*		Saves the results calculated into fcst_depr_ledger_fp
*	@@PARAM:
*		g_ssp_parms.long_arg[1] which is the Fcst Depr Version ID
*		g_ssp_parms.long_arg2 the array of funding projects
*		g_ssp_parms.long_arg3 The array of revisions
*		g_ssp_parms.date_arg[1] is the start month
*		g_ssp_parms.date_arg[2] is the end month
*		
*	@@RETURN: number
*		1 (success) or -1	(failure)
*/
sqlca.p_handleIncrementalResults(i_fcst_version_id)
if sqlca.sqlCode <> 0 then
	return -1
end if

return 1
end function

public function integer uf_fcstdepr_group ();/*
*	@@ DESCRIPTION
*		This function executes the depreciation forecast procedure from the pp_depr package
*	@@PARAM:
*		g_ssp_parms.long_arg[1] which is the Fcst Depr Version ID
*		g_ssp_parms.date_arg which is the array of gl months to be processed
*		
*	@@RETURN: number
*		1 (success) or -1	(failure)
*/
string sqls, in_clause
longlong num_rows

f_pp_msgs("   Forecast Version: " + string(g_ssp_parms.long_arg[1]))
f_pp_msgs("   Start Month: " + string(g_ssp_parms.date_arg[1], 'yyyymmdd'))
f_pp_msgs("   End Month: " + string(g_ssp_parms.date_arg[ upperbound(g_ssp_parms.date_arg) ], 'yyyymmdd'))


i_fcst_version_id = g_ssp_parms.long_arg[1]
i_calc_retire =  g_ssp_parms.long_arg4[1]

delete from depr_fcst_group_stg;
// if no records in fcst depr version arguments then entire version
if upperBound(g_ssp_parms.long_arg2) = 0 then
	// only load group depreciated
	insert into depr_fcst_group_stg
	select a.fcst_depr_group_id
	from fcst_depr_group_version a
	where a.fcst_depr_version_id = :i_fcst_version_id
	and a.subledger_type_id = 0
	;
else
	// only load group depreciated
	in_clause = f_parsearrayby254(g_ssp_parms.long_arg2, 'N', 'a.fcst_depr_group_id')
	sqls = &
		"insert into depr_fcst_group_stg" +&
		" select a.fcst_depr_group_id" +&
		" from fcst_depr_group_version a" +&
		" where " + in_clause +&
		" and a.fcst_depr_version_id = " + string(i_fcst_version_id) +&
		" and a.subledger_type_id = 0"
	execute immediate :sqls;
end if

num_rows = sqlca.sqlNRows
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR: Staging forecast depr groups")
	return -1
end if
if num_rows > 0 then
	// Stage the forecast data
	f_pp_msgs("Number of Forecast Groups to Process: " + string(num_rows))
	
	sqlca.p_stageFCSTDepr (i_fcst_version_id, g_ssp_parms.date_arg) 
	if sqlca.sqlCode <> 0 then
		f_pp_msgs("ERROR: Calling p_stageFCSTDepr")
		return -1
	end if
	
	// Run the Depr Calc
	sqlca.p_calcFCSTDepr( g_ssp_parms.date_arg, 0, i_fcst_version_id, i_calc_retire) 
	if sqlca.sqlCode <> 0 then
		f_pp_msgs("ERROR: Calling p_calcFCSTDepr")
		return -1
	end if
end if

return 1
end function

public function longlong uf_fcstresults_group ();/*
*	@@ DESCRIPTION
*		This function loads the successful results to fcst_depr_ledger
*	@@PARAM:
*		NONE
*		
*	@@RETURN: number
*		1 or -1	
*/

/// we handled errors.
// now load depr ledger with successes

update fcst_depr_ledger d set (
	begin_reserve, end_reserve, end_balance,
	depreciation_base, depreciation_rate, depreciation_expense, depr_exp_alloc_adjust,
	salvage_base, salvage_rate, salvage_expense, salvage_exp_alloc_adjust, salvage_balance,
	cost_of_removal_base, cost_of_removal_rate, cor_expense, cor_exp_alloc_adjust, cor_end_reserve, 
	impairment_reserve_end, reserve_bal_impairment,  reserve_bal_provision,
	reserve_bal_cor,  reserve_bal_adjust, reserve_bal_retirements,
	reserve_bal_tran_in, reserve_bal_tran_out, reserve_bal_gain_loss,
	reserve_bal_salvage_exp, est_ann_net_adds,
	cor_blending_adjustment, cor_blending_transfer, 
	impairment_asset_amount, impairment_expense_amount,
	reserve_blending_adjustment, reserve_blending_transfer,
	rwip_cost_of_removal, rwip_reserve_credits, rwip_salvage_cash, rwip_salvage_returns,
	salvage_exp_adjust,
	begin_balance, additions, retirements, cor_beg_reserve,
	depr_exp_adjust, cor_exp_adjust 
	 ) = 
	(  select s.begin_reserve, s.end_reserve, s.end_balance,
		s.depreciation_base, s.depreciation_rate, s.depreciation_expense, (s.depr_exp_alloc_adjust + s.over_depr_adj + s.retro_depr_adj + s.curve_trueup_adj),
		s.salvage_base, s.salvage_rate, s.salvage_expense, (s.salvage_exp_alloc_adjust + s.over_depr_adj_salv + s.retro_salv_adj + s.curve_trueup_adj_salv), s.salvage_balance,
		s.cost_of_removal_base, s.cost_of_removal_rate, s.cor_expense, (s.cor_exp_alloc_adjust + s.over_depr_Adj_cor + s.retro_cor_adj + s.curve_trueup_adj_Cor), s.cor_end_reserve, 
		s.impairment_reserve_end, s.reserve_bal_impairment,  s.reserve_bal_provision,
		s.reserve_bal_cor,  s.reserve_bal_adjust, s.reserve_bal_retirements,
		s.reserve_bal_tran_in, s.reserve_bal_tran_out, s.reserve_bal_gain_loss,
		s.reserve_bal_salvage_exp, s.est_ann_net_adds,
		s.cor_blending_adjustment, s.cor_blending_transfer, 
		s.impairment_asset_amount, s.impairment_expense_amount,
		s.reserve_blending_adjustment, s.reserve_blending_transfer,
		s.rwip_cost_of_removal, s.rwip_reserve_credits, s.rwip_salvage_cash, s.rwip_salvage_returns,
		s.salvage_exp_adjust,
		s.begin_balance, s.additions, s.retirements, s.cor_beg_reserve,
		s.depr_exp_adjust, s.cor_exp_adjust 
		from depr_calc_stg s
		where d.fcst_depr_group_id = s.depr_group_id
		and d.set_of_books_id = s.set_of_books_id
		and d.gl_post_mo_yr = s.gl_post_mo_yr
		and d.fcst_depr_version_id = s.fcst_depr_version_id
		and s.depr_calc_status = 9
	)
	where exists (
		select 1
		from depr_calc_stg s
		where d.fcst_depr_group_id = s.depr_group_id
		and d.set_of_books_id = s.set_of_books_id
		and d.gl_post_mo_yr = s.gl_post_mo_yr
		and d.fcst_depr_version_id = s.fcst_depr_version_id
		and s.depr_calc_status = 9
	);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating fcst_depr_ledger: " + sqlca.sqlErrtext)
	return -1
end if

//join to depr_calc_stg so we can check the status. 
update fcst_net_salvage_amort a
set (cor_treatment, salvage_treatment, net_salvage_amort_life, cost_of_removal_amort, cost_of_removal_reserve, salvage_amort, salvage_reserve) = (
	select b.cor_treatment, b.salv_treatment, b.cor_life, b.cor_amort, b.cor_reserve, b.salv_amort, b.salv_reserve
	from depr_calc_amort_stg b, depr_calc_stg c
	where a.fcst_depr_group_id = b.depr_group_id
	and a.set_of_books_id = b.set_of_books_id
	and a.gl_post_mo_yr = b.gl_post_mo_yr
	and a.vintage = b.vintage
	and b.depr_group_id = c.depr_group_id
	and b.set_of_books_id = c.set_of_books_id
	and b.gl_post_mo_yr = c.gl_post_mo_yr
	and c.fcst_depr_version_id = a.fcst_depr_version_id
	and c.depr_calc_status = 9
)
where exists (
	select 1 
	from depr_calc_amort_stg b, depr_calc_stg c
	where a.fcst_depr_group_id = b.depr_group_id
	and a.set_of_books_id = b.set_of_books_id
	and a.gl_post_mo_yr = b.gl_post_mo_yr
	and a.vintage = b.vintage
	and b.depr_group_id = c.depr_group_id
	and b.set_of_books_id = c.set_of_books_id 
	and b.gl_post_mo_yr = c.gl_post_mo_yr
	and c.fcst_depr_version_id = a.fcst_depr_version_id
	and c.depr_calc_status = 9
);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating fcst_net_salvage_amort: " + sqlca.sqlErrtext)
	return -1
end if

/*used for debugging*/
//int count 
//setnull(count)
//select count(1)
//into :count 
//from depr_calc_amort_stg;
//
//f_pp_msgs("	DEPR_CALC_AMORT_STG count: " + string(count));

insert into fcst_net_salvage_amort (
	fcst_depr_version_id, fcst_depr_group_id, set_of_books_id, gl_post_mo_yr, vintage,
	cor_treatment, salvage_treatment, net_salvage_amort_life, cost_of_removal_bal,
	cost_of_removal_amort, cost_of_removal_reserve, salvage_bal, salvage_amort,
	salvage_reserve)
select :i_fcst_version_id, a.depr_group_id, a.set_of_books_id, a.gl_post_mo_yr, a.vintage, 
	a.cor_treatment, a.salv_treatment, a.cor_life, a.cor_balance, 
	a.cor_amort, a.cor_reserve, a.salv_balance, a.salv_amort,
	a.salv_reserve
from depr_calc_amort_stg a, depr_calc_stg b
where a.depr_group_id = b.depr_group_id
and a.set_of_books_id = b.set_of_books_id
and a.gl_post_mo_yr = b.gl_post_mo_yr
and b.depr_calc_status = 9 
and not exists (
	select 1
	from fcst_net_salvage_amort c
	where :i_fcst_version_id = c.fcst_depr_version_id
	and a.depr_group_id = c.fcst_depr_group_id
	and a.set_of_books_id = c.set_of_books_id
	and a.gl_post_mo_yr = c.gl_post_mo_yr
	and a.vintage = c.vintage
);
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR inserting missing records into fcst_net_salvage_amort: " + sqlca.sqlErrtext)
	return -1
end if

string sqls

sqls = 'merge into FCST_DEPR_METHOD_UOP UOP '
sqls += 'using ( '
sqls += '	select DISTINCT STG.DEPR_METHOD_ID, STG.FCST_DEPR_VERSION_ID, STG.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR, STG.PRODUCTION, STG.ESTIMATED_PRODUCTION,  '
sqls += '		STG.MIN_CALC, STG.PRODUCTION_2, STG.ESTIMATED_PRODUCTION_2 '
sqls += '	from DEPR_CALC_STG STG '
sqls += '   where STG.MID_PERIOD_METHOD = ~'UOP~' and STG.DEPR_CALC_STATUS = 9) STG '
sqls += 'on (UOP.FCST_DEPR_METHOD_ID = STG.DEPR_METHOD_ID and  '
sqls += '	UOP.FCST_DEPR_VERSION_ID = STG.FCST_DEPR_VERSION_ID and  '
sqls += '	UOP.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID and  '
sqls += '	UOP.GL_POST_MO_YR = STG.GL_POST_MO_YR) '
sqls += 'when not matched then  '
sqls += '	insert (FCST_DEPR_METHOD_ID, FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, PRODUCTION, ESTIMATED_PRODUCTION, MIN_CALC_OPTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2) '
sqls += '	values (STG.DEPR_METHOD_ID, STG.FCST_DEPR_VERSION_ID, STG.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR, STG.PRODUCTION, STG.ESTIMATED_PRODUCTION, STG.MIN_CALC, STG.PRODUCTION_2, STG.ESTIMATED_PRODUCTION_2) '
sqls += 'when matched then  '
sqls += '	update  '
sqls += '	set UOP.PRODUCTION = STG.PRODUCTION, '
sqls += '		UOP.ESTIMATED_PRODUCTION = STG.ESTIMATED_PRODUCTION, '
sqls += '		UOP.MIN_CALC_OPTION = STG.MIN_CALC, '
sqls += '		UOP.PRODUCTION_2 = STG.PRODUCTION_2, '
sqls += '		UOP.ESTIMATED_PRODUCTION_2 = STG.ESTIMATED_PRODUCTION_2 '
		
if not isNull(sqls) then
	execute immediate :sqls;
else
	f_pp_msgs("Error updating FCST_DEPR_METHOD_UOP. Null string passed to execute immediate.")
	return -1
end if
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating FCST_DEPR_METHOD_UOP: " + sqlca.sqlErrtext)
	f_pp_msgs("SQL Used: " + sqls)
	return -1
end if

sqls = 'merge into FCST_DEPR_GROUP_UOP UOP '
sqls += 'using ( '
sqls += '	select DEPR_GROUP_ID, FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, DEPR_METHOD_ID, CURR_UOP_EXP,  '
sqls += '		CURR_RATE_EXP, DEPRECIATION_EXPENSE, MIN_ANNUAL_EXPENSE, MAX_ANNUAL_EXPENSE, CURR_UOP_EXP_2, UOP_EXP_ADJ '
sqls += '	from DEPR_CALC_STG) STG '
sqls += 'on (UOP.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID and  '
sqls += '	UOP.FCST_DEPR_VERSION_ID = STG.FCST_DEPR_VERSION_ID and  '
sqls += '	UOP.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID and  '
sqls += '	UOP.GL_POST_MO_YR = STG.GL_POST_MO_YR) '
sqls += 'when not matched then  '
sqls += '	insert (FCST_DEPR_GROUP_ID, FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, FCST_DEPR_METHOD_ID, UOP_DEPR_EXPENSE,  '
sqls += '		RATE_DEPR_EXPENSE, LEDGER_DEPR_EXPENSE, MIN_ANNUAL_EXPENSE, MAX_ANNUAL_EXPENSE, UOP_DEPR_EXPENSE_2, UOP_EXP_ADJ) '
sqls += '	values (STG.DEPR_GROUP_ID, STG.FCST_DEPR_VERSION_ID, STG.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR, STG.DEPR_METHOD_ID, STG.CURR_UOP_EXP,  '
sqls += '		STG.CURR_RATE_EXP, STG.DEPRECIATION_EXPENSE, STG.MIN_ANNUAL_EXPENSE, STG.MAX_ANNUAL_EXPENSE, STG.CURR_UOP_EXP_2, STG.UOP_EXP_ADJ) '
sqls += 'when matched then  '
sqls += '	update '
sqls += '	set UOP.FCST_DEPR_METHOD_ID = STG.DEPR_METHOD_ID, '
sqls += '		UOP.UOP_DEPR_EXPENSE = STG.CURR_UOP_EXP, '
sqls += '		UOP.RATE_DEPR_EXPENSE = STG.CURR_RATE_EXP, '
sqls += '		UOP.LEDGER_DEPR_EXPENSE = STG.DEPRECIATION_EXPENSE, '
sqls += '		UOP.UOP_DEPR_EXPENSE_2 = STG.CURR_UOP_EXP_2, '
sqls += '		UOP.UOP_EXP_ADJ = STG.UOP_EXP_ADJ '
		
if not isNull(sqls) then
	execute immediate :sqls;
else
	f_pp_msgs("Error updating FCST_DEPR_GROUP_UOP. Null string passed to execute immediate.")
	return -1
end if
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR updating FCST_DEPR_GROUP_UOP: " + sqlca.sqlErrtext)
	f_pp_msgs("SQL Used: " + sqls)
	return -1
end if

if uf_update_process_control('FCST: Group') <> 1 then return -1

return 1

end function

public function integer uf_fcstresults_ind ();/*
*	@@ DESCRIPTION
*		This function loads the successful results to depr_ledger
*	@@PARAM:
*		NONE
*		
*	@@RETURN: number
*		1 or -1	
*/

/// we handled errors.
// now load cpr_depr with successes
string sqls

sqls = "merge into fcst_cpr_depr c "
sqls += "using ( "
sqls += "	select ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, "+string(i_fcst_version_id)+" as FCST_DEPR_VERSION_ID, INIT_LIFE, "
sqls += "	REMAINING_LIFE, ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, "
sqls += "	RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH, "
sqls += "	SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN, "
sqls += "	RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS, "
sqls += "	DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, "
sqls += "	YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, "
sqls += "	PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID, "
sqls += "	MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID as FCST_DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST, "
sqls += "	DEPR_METHOD_ID as FCST_DEPR_METHOD_ID, IMPAIRMENT_ASSET_ACTIVITY_SALV, "
sqls += "	IMPAIRMENT_ASSET_BEGIN_BALANCE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, "
sqls += "	SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT"
sqls += "	from CPR_DEPR_CALC_STG "
sqls += ") s "
sqls += "on (s.asset_id = c.asset_id and s.set_of_books_id = c.set_of_books_id "
sqls += "  and s.gl_posting_mo_yr = c.gl_posting_mo_yr and S.FCST_DEPR_VERSION_ID = C.FCST_DEPR_VERSION_ID) "
sqls += "when not matched then insert "
sqls += "( "
sqls += "	C.ASSET_ID, C.SET_OF_BOOKS_ID, C.GL_POSTING_MO_YR, C.FCST_DEPR_VERSION_ID, C.INIT_LIFE, "
sqls += "	C.REMAINING_LIFE, C.ESTIMATED_SALVAGE, C.BEG_ASSET_DOLLARS, C.NET_ADDS_AND_ADJUST, "
sqls += "	C.RETIREMENTS, C.TRANSFERS_IN, C.TRANSFERS_OUT, C.ASSET_DOLLARS, C.BEG_RESERVE_MONTH, "
sqls += "	C.SALVAGE_DOLLARS, C.RESERVE_ADJUSTMENT, C.COST_OF_REMOVAL, C.RESERVE_TRANS_IN, "
sqls += "	C.RESERVE_TRANS_OUT, C.DEPR_EXP_ADJUST, C.OTHER_CREDITS_AND_ADJUST, C.GAIN_LOSS, "
sqls += "	C.DEPRECIATION_BASE, C.CURR_DEPR_EXPENSE, C.DEPR_RESERVE, C.BEG_RESERVE_YEAR, "
sqls += "	C.YTD_DEPR_EXPENSE, C.YTD_DEPR_EXP_ADJUST, C.PRIOR_YTD_DEPR_EXPENSE, "
sqls += "	C.PRIOR_YTD_DEPR_EXP_ADJUST, C.ACCT_DISTRIB, C.MONTH_RATE, C.COMPANY_ID, "
sqls += "	C.MID_PERIOD_METHOD, C.MID_PERIOD_CONV, C.FCST_DEPR_GROUP_ID, C.DEPR_EXP_ALLOC_ADJUST, "
sqls += "	C.FCST_DEPR_METHOD_ID, C.IMPAIRMENT_ASSET_ACTIVITY_SALV, "
sqls += "	C.IMPAIRMENT_ASSET_BEGIN_BALANCE, C.SALVAGE_EXPENSE, C.SALVAGE_EXP_ADJUST, "
sqls += "	C.SALVAGE_EXP_ALLOC_ADJUST, C.IMPAIRMENT_ASSET_AMOUNT, C.IMPAIRMENT_EXPENSE_AMOUNT"
sqls += ") values "
sqls += "( "
sqls += "	S.ASSET_ID, S.SET_OF_BOOKS_ID, S.GL_POSTING_MO_YR, S.FCST_DEPR_VERSION_ID, S.INIT_LIFE, "
sqls += "	S.REMAINING_LIFE, S.ESTIMATED_SALVAGE, S.BEG_ASSET_DOLLARS, S.NET_ADDS_AND_ADJUST, "
sqls += "	S.RETIREMENTS, S.TRANSFERS_IN, S.TRANSFERS_OUT, S.ASSET_DOLLARS, S.BEG_RESERVE_MONTH, "
sqls += "	S.SALVAGE_DOLLARS, S.RESERVE_ADJUSTMENT, S.COST_OF_REMOVAL, S.RESERVE_TRANS_IN, "
sqls += "	S.RESERVE_TRANS_OUT, S.DEPR_EXP_ADJUST, S.OTHER_CREDITS_AND_ADJUST, S.GAIN_LOSS, "
sqls += "	S.DEPRECIATION_BASE, S.CURR_DEPR_EXPENSE, S.DEPR_RESERVE, S.BEG_RESERVE_YEAR, "
sqls += "	S.YTD_DEPR_EXPENSE, S.YTD_DEPR_EXP_ADJUST, S.PRIOR_YTD_DEPR_EXPENSE, "
sqls += "	S.PRIOR_YTD_DEPR_EXP_ADJUST, S.ACCT_DISTRIB, S.MONTH_RATE, S.COMPANY_ID, "
sqls += "	S.MID_PERIOD_METHOD, S.MID_PERIOD_CONV, S.FCST_DEPR_GROUP_ID, S.DEPR_EXP_ALLOC_ADJUST, "
sqls += "	S.FCST_DEPR_METHOD_ID, S.IMPAIRMENT_ASSET_ACTIVITY_SALV, "
sqls += "	S.IMPAIRMENT_ASSET_BEGIN_BALANCE, S.SALVAGE_EXPENSE, S.SALVAGE_EXP_ADJUST, "
sqls += "	S.SALVAGE_EXP_ALLOC_ADJUST, S.IMPAIRMENT_ASSET_AMOUNT, S.IMPAIRMENT_EXPENSE_AMOUNT"
sqls += ") "
sqls += "when matched then update set "
sqls += "  c.depreciation_base = s.depreciation_base, "
sqls += "  c.curr_depr_expense = s.curr_depr_expense, " 
sqls += "  c.month_rate = s.month_rate, " 
sqls += "  c.depr_exp_alloc_adjust = s.depr_exp_alloc_adjust, "
sqls += "  c.salvage_expense = s.salvage_expense, "
sqls += "	c.salvage_exp_alloc_adjust = s.salvage_exp_alloc_adjust, " 
sqls += "  c.depr_reserve = s.depr_reserve, " 
sqls += "	c.mid_period_conv = s.mid_period_conv, " 
sqls += "  c.mid_period_method = s.mid_period_method, "
sqls += "  c.fcst_depr_method_id = s.fcst_depr_method_id, "
sqls += "  c.remaining_life = s.remaining_life,  "
sqls += "  c.asset_dollars = s.asset_dollars,  "
sqls += "  c.acct_distrib = s.acct_distrib,  "
sqls += "  c.init_life = s.init_life "

if not isNull(sqls) then 
	execute immediate :sqls;
else
	f_pp_msgs("Null string passed to execute immediate in uf_fcstresults_ind()")
	return -1
end if

if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating fcst_cpr_depr: " + sqlca.sqlErrtext)
	return -1
else 
	f_pp_msgs ("Successfully updated fcst_cpr_depr. Number of rows updated: " + string(sqlca.sqlnrows))
end if


// now load depr ledger with successes

update fcst_depr_ledger c set
(
	depreciation_base, depreciation_expense, depr_exp_alloc_adjust,
	salvage_expense, salvage_exp_alloc_adjust, end_reserve,
	end_Balance, begin_reserve, begin_balance,
	additions, retirements, transfers_in, transfers_out
) = 
(
	select sum(s.depreciation_base), sum(s.curr_depr_expense), sum(s.depr_exp_alloc_adjust) + sum(s.trueup_adj),
		sum(s.salvage_expense), sum(s.salvage_exp_alloc_adjust), sum(s.depr_reserve),
		sum(s.asset_dollars), sum(s.beg_reserve_month), sum(s.beg_asset_dollars),
		sum(s.net_adds_and_adjust), sum(s.retirements), sum(s.transfers_in), sum(s.transfers_out)
	from cpr_depr_calc_stg s
	where s.depr_group_id = c.fcst_depr_group_id
	and s.set_of_books_id = c.set_of_books_id
	and s.gl_posting_mo_yr = c.gl_post_mo_yr
)
where exists
(
	select 1
	from cpr_depr_calc_stg s
	where s.depr_calc_status = 9
	and s.depr_group_id = c.fcst_depr_group_id
	and s.set_of_books_id = c.set_of_books_id
	and s.gl_posting_mo_yr = c.gl_post_mo_yr
)
and c.fcst_depr_Version_id = :i_fcst_version_id
;
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating fcst_depr_ledger: " + sqlca.sqlErrtext)
	return -1
else 
	f_pp_msgs ("Successfully updated fcst_depr_ledger. Number of rows updated: " + string(sqlca.sqlnrows))
end if

if uf_update_process_control('FCST: Individual') <> 1 then return -1

f_pp_msgs("Exiting uf_fcstresults_ind().")
return 1

end function

public function integer uf_fcstdepr_ind ();/*
*	@@ DESCRIPTION
*		This function executes the depreciation forecast procedure from the pp_depr package
*	@@PARAM:
*		g_ssp_parms.long_arg[1] which is the Fcst Depr Version ID
*		g_ssp_parms.date_arg which is the array of gl months to be processed
*		
*	@@RETURN: number
*		1 (success) or -1	(failure)
*/
string sqls, in_clause
longlong num_rows

f_pp_msgs("   Forecast Version: " + string(g_ssp_parms.long_arg[1]))
f_pp_msgs("   Start Month: " + string(g_ssp_parms.date_arg[1], 'yyyymmdd'))
f_pp_msgs("   End Month: " + string(g_ssp_parms.date_arg[ upperbound(g_ssp_parms.date_arg) ], 'yyyymmdd'))

i_fcst_version_id = g_ssp_parms.long_arg[1]
delete from depr_fcst_group_stg;
// if no records in fcst depr version arguments then entire version
if upperBound(g_ssp_parms.long_arg2) = 0 then
	// only load group depreciated
	insert into depr_fcst_group_stg
	select a.fcst_depr_group_id
	from fcst_depr_group_version a
	where a.fcst_depr_version_id = :i_fcst_version_id
	and a.subledger_type_id <> 0
	;
else
	// only load group depreciated
	in_clause = f_parsearrayby254(g_ssp_parms.long_arg2, 'N', 'a.fcst_depr_group_id')
	sqls = &
		"insert into depr_fcst_group_stg" +&
		" select a.fcst_depr_group_id" +&
		" from fcst_depr_group_version a" +&
		" where " + in_clause +&
		" and a.fcst_depr_version_id = " + string(i_fcst_version_id) +&
		" and a.subledger_type_id <> 0"
	execute immediate :sqls;
end if

num_rows = sqlca.sqlNRows
if sqlca.sqlCode <> 0 then
	f_pp_msgs("ERROR: Staging forecast depr groups (ind)")
	return -1
end if
if num_rows > 0 then
	// Stage the forecast data
	f_pp_msgs("Number of Forecast Groups to Process: " + string(num_rows))
	
	sqlca.p_stageFCSTCPRDepr (g_ssp_parms.date_arg, i_fcst_version_id) 
	if sqlca.sqlCode <> 0 then
		f_pp_msgs("ERROR: Calling p_stageFCSTCPRDepr")
		return -1
	end if
	
	// Run the Depr Calc
	sqlca.p_calcFCSTCPRDepr( g_ssp_parms.date_arg, i_fcst_version_id) 
	if sqlca.sqlCode <> 0 then
		f_pp_msgs("ERROR: Calling p_calcFCSTCPRDepr")
		return -1
	end if
end if

return 1
end function

public function longlong uf_update_process_control (string a_depr_type);

update DEPR_CALC_PROCESS_CONTROL
set IS_PROCESSED = 1
where GROUP_ID = :g_ssp_parms.long_arg3[1]
and upper(TYPE) = upper(:a_depr_type);
if sqlca.sqlcode <> 0 then
	f_pp_msgs ("ERROR updating depr_calc_process_control: " + sqlca.sqlErrtext)
	return -1
end if

return 1
end function

public function boolean uf_all_types_finished ();longlong counter = -1


select count(*)
into :counter
from DEPR_CALC_PROCESS_CONTROL
where GROUP_ID = :g_ssp_parms.long_arg3[1]
and IS_PROCESSED = 0;

if counter = 0 then
	return TRUE
else
	return FALSE
end if
end function

public subroutine uf_depr_reg_entries ();longlong i

for i = 1 to Upperbound(g_ssp_parms.long_arg)
	f_reg_entries_calc(g_ssp_parms.long_arg[i], datetime(g_ssp_parms.date_arg[1]), 'DEPR')
next
end subroutine

public function longlong uf_set_run_date (string a_depr_type);longlong i, num

choose case LEFT(a_depr_type, 4)
	case 'FCST'
		update fcst_depr_version
		set last_calculate = sysdate
		where fcst_depr_version_id = :i_fcst_version_id
		;
	case 'GROU'
		num = upperBound(g_ssp_parms.long_arg)
		
		for i = 1 to num
			update cpr_control
			set depr_calculated = sysdate
			where accounting_month = :g_ssp_parms.date_arg[1]
			and company_id = :g_ssp_parms.long_arg[ i ]
			;
		next
	case 'INDI'
		if g_ssp_parms.long_arg2[1] = -100 then
			num = upperBound(g_ssp_parms.long_arg)
			for i = 1 to num
				update ls_process_control
				set depr_calc = sysdate
				where gl_posting_mo_yr = :g_ssp_parms.date_arg[1]
				and company_id = :g_ssp_parms.long_arg[ i ]
				;
			next
		else
			num = upperBound(g_ssp_parms.long_arg)

			for i = 1 to num
				update cpr_control
				set depr_calculated = sysdate
				where accounting_month = :g_ssp_parms.date_arg[1]
				and company_id = :g_ssp_parms.long_arg[ i ]
				;
			next
		end if
end choose


return 1
end function

public subroutine uf_send_email (string a_process_description);nvo_cpr_control n_cpr_control
uo_ds_top ds_users
boolean vfy_users
string sqls, find_str, user_id
longlong i

n_cpr_control.of_setupfromcompaniesandmonth(g_ssp_parms.long_arg, g_ssp_parms.date_arg[1])

if n_cpr_control.of_checksystemcontrol( 4 ) = true then
	ds_users = create uo_ds_top
	vfy_users = false
	select nvl(user_sql, '') into :sqls from pp_verify where lower(description) = 'email cpr close: depreciation calculation';
	if sqls <> "" then
		find_str = f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true)
		if f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true) = "OK" then
			if ds_users.RowCount() > 0 then
				vfy_users = true
			end if
		end if
	end if

	if vfy_users = true then
		for i=1 to ds_users.RowCount()
			user_id = ds_users.GetItemString( i, 1)
			f_send_mail( user_id, "Depreciation Calculation completed", &
			"Depreciation Calculation completed on " + String(Today(), "mm/dd/yyyy") + &
			" at " + String(Today(), "hh:mm:ss") + " for " + a_process_description, user_id)
		next
	else
		f_send_mail( s_user_info.user_id, "Depreciation Calculation completed", &
		"Depreciation Calculation completed on " + String(Today(), "mm/dd/yyyy") + &
		" at " + String(Today(), "hh:mm:ss") + " for " + a_process_description, s_user_info.user_id)
	end if
end if
end subroutine

public function boolean uf_validations ();integer i
datetime last_depr_approved

for i = 1 to upperbound(i_nvo_cpr_control.i_company_idx)
	if i_nvo_cpr_control.of_setupCompany(i) < 1 then 
		f_pp_msgs("Skipping depreciation calculation for "+ i_nvo_cpr_control.i_company_descr[i] +".")
		return false
	end if
	
	// was last months depreciation approved for this company?
	select depr_approved
	  into :last_depr_approved
	  from cpr_control
	 where company_id = :i_nvo_cpr_control.i_company
		and accounting_month = add_months(:i_nvo_cpr_control.i_month, -1);
	
	if isnull(last_depr_approved) then
		f_pp_msgs("Last month's depreciation results must be" &
					 + " approved before this month's may be calculated.")
		return false
	end if	
	
	if isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'depr_approved')) = false then
		f_pp_msgs("This month's depreciation results have already been approved.")
		return false
	end if
	
	// Make sure the month isn't closed
	if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		return false
	end if
next

return true
end function

public function longlong uf_handleerrors ();/*
*	@@ DESCRIPTION
*		This function handles the results from running the uf_monthenddepr
*		Finds depr groups that had errors
*		depr_calc_status 9 signifies success in depr_calc_stg
*		After handling errors, updates depr_ledger with values from depr_calc_stg with status = 9
*
*	@@PARAM:
*		ds_depr_error stores depr groups that had errors
*		
*	@@RETURN:
*		0 = success
*		-1 = failure
*/


uo_ds_top ds_depr_error
string sqls, ret
longlong i, num, rtn

// initialize to keep track of errors in group and/or cpr 
rtn = 0

sqls = "select distinct depr_group_id, depr_calc_message from depr_calc_stg where depr_calc_status not in (8,9)"

ds_depr_error = create uo_ds_top
ret = f_create_dynamic_ds(ds_depr_error, "grid", sqls, sqlca, TRUE)
if ret <> "OK" then
	// error... log it
	f_pp_msgs("Error retrieving der groups with invalid calculations: " + ret)
	return -1
end if

// get the number of errors
num = ds_depr_error.rowCount()
if num > 0 then
	f_pp_msgs("The following depr groups had errors in the calculation: ")
	rtn = rtn + 1
end if
for i = 1 to num
	f_pp_msgs("   " + string(ds_depr_error.getItemNumber(i, 1)) + ": " + ds_depr_error.getItemString(i, 2))
next

// handle cpr_depr records
sqls = "select distinct asset_id, depr_group_id, depr_calc_message from cpr_depr_calc_stg where depr_calc_status <> 9"
ret = f_create_dynamic_ds(ds_depr_error, "grid", sqls, sqlca, TRUE)
if ret <> "OK" then
	// error... log it
	f_pp_msgs("Error retrieving depr groups with invalid calculations: " + ret)
	return -1
end if

// get the number of errors
num = ds_depr_error.rowCount()
if num > 0 then
	f_pp_msgs("The following assets / depr groups had errors in the calculation: ")
	rtn = rtn + 1
end if
for i = 1 to num
	f_pp_msgs("   " + string(ds_depr_error.getItemNumber(i, 1)) + " / " +&
							 string(ds_depr_error.getItemNumber(i, 2)) + ": " + ds_depr_error.getItemString(i, 3))
next

if rtn > 0 then
	return -1
else
	return 0
end if
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

