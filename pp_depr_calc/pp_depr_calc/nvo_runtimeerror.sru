HA$PBExportHeader$nvo_runtimeerror.sru
$PBExportComments$CODE IN THIS OBJECT IS 100% IDENTICAL TO NVO_RUNTIMEERROR_APP EXCEPT IT IS INHERITED FROM ~r~nRUNTIMEERROR INSTEAD OF USEROBJECT .  THESE OBJECTS MUST BE KEPT IN SYNCH
forward
global type nvo_runtimeerror from runtimeerror
end type
end forward

global type nvo_runtimeerror from runtimeerror
string objectname = "nvo_runtimeerror"
string class = "nvo_runtimeerror"
string routinename = "create"
integer line = -1
end type
global nvo_runtimeerror nvo_runtimeerror

type variables
string 		i_args[], i_args_null[]
string          i_sqlca_sqlerrtext, i_pp_error_code, i_sqlca_last_sql
longlong i_sqlca_sqlcode, i_sqlca_sqldbcode, i_sqlca_sqlnrows, i_rtn

uo_ds_top	i_ds_val_messages
end variables

forward prototypes
public subroutine setmessage (string newmessage)
public subroutine msg (string a_newmessage)
public subroutine msg (longlong a_rtn, string a_newmessage, string a_args[])
public subroutine msg (longlong a_rtn, string a_newmessage)
public subroutine msg (string a_newmessage, string a_args[])
public subroutine msg_val (string a_newmessage, string a_args[])
public subroutine msg_val (string a_newmessage, string a_args)
public subroutine msg (longlong a_rtn, string a_newmessage, string a_args)
public subroutine msg (string a_newmessage, string a_args)
private function string msg_lookup (string a_newmessage)
private subroutine msg_val (string a_newmessage, string a_pp_error_code, string a_args[])
public function longlong msg_val_log ()
public function longlong msg_log ()
public function boolean check_sql (string a_err_msg, transaction a_sqlca)
public function boolean check_sql_no_rows (string a_err_msg, transaction a_sqlca)
public function boolean check_sql_no_rows (string a_err_msg)
public function boolean check_sql (string a_err_msg)
public function boolean check_sql (string a_err_msg, uo_ds_top a_ds)
public function boolean check_sql_no_rows (string a_err_msg, datastore a_ds)
end prototypes

public subroutine setmessage (string newmessage);// Capture other key information automatically when setmessage is called.
this.text = newmessage

// SQLCA Properties
i_sqlca_sqlerrtext = SQLCA.SQLErrText
i_sqlca_sqlcode = SQLCA.SQLCode
i_sqlca_sqldbcode = SQLCA.SQLDBCode
i_sqlca_sqlnrows = SQLCA.SQLNRows
i_sqlca_last_sql = SQLCA.i_last_sql_statement
end subroutine

public subroutine msg (string a_newmessage);// ***************************************************************
//		Error logging function:
//			string	a_newmessage		Primary message to be returned
//
//		Calls master msg(longlong a_rtn, string a_newmessage, string a_args[])
//			Assumes no additional information is needed and a_rtn = -1
//
// ***************************************************************
msg(-1,a_newmessage,i_args_null)
end subroutine

public subroutine msg (longlong a_rtn, string a_newmessage, string a_args[]);// ***************************************************************
//		Error logging function:
//			longlong	a_rtn					Interface return code
//			string	a_newmessage		Primary message to be returned
//			string	a_args[]				Additional information to be recorded in the logs
//
//		Standard interface shell automatically captures calling code information and
//		SQLCA information.
//
//		Example call:
//			if sqlca.sqlcode < 0 then 
//                g_rte.msg(-1,'ERROR: updating wo_estimate', {'Work Order ID: 1234','Revision: 2'})
//                throw g_rte
//			end if
//
// ***************************************************************
string pp_error_code

i_rtn = a_rtn
setmessage(a_newmessage)
i_args = a_args

pp_error_code = msg_lookup(a_newmessage)
i_pp_error_code = pp_error_code
end subroutine

public subroutine msg (longlong a_rtn, string a_newmessage);// ***************************************************************
//		Error logging function:
//			longlong	a_rtn					Interface return code
//			string	a_newmessage		Primary message to be returned
//
//		Calls master msg(longlong a_rtn, string a_newmessage, string a_args[])
//
// ***************************************************************
msg(a_rtn,a_newmessage,i_args_null)
end subroutine

public subroutine msg (string a_newmessage, string a_args[]);// ***************************************************************
//		Error logging function:
//			string	a_newmessage		Primary message to be returned
//			string	a_args[]				Additional information to be recorded in the logs
//
//		Calls master msg(longlong a_rtn, string a_newmessage, string a_args[])
//			Assumes a_rtn = -1
//
// ***************************************************************
msg(-1,a_newmessage,a_args)
end subroutine

public subroutine msg_val (string a_newmessage, string a_args[]);// ***************************************************************
//		Validation logging function:
//			string	a_newmessage		Primary message to be returned
//			string	a_args[]				Values failing the validation.
//
//		Example call:
//			g_rte.msg_val('Unable to translate External Department Code to Department', {'5543','2241'})
//
//		Calls master msg_val
//
// ***************************************************************
string pp_error_code

pp_error_code = msg_lookup(a_newmessage)
msg_val(a_newmessage, pp_error_code, a_args[])
end subroutine

public subroutine msg_val (string a_newmessage, string a_args);// ***************************************************************
//		Validation logging function:
//			string	a_newmessage		Primary message to be returned
//			string	a_args				Value failing the validation.
//
//		Example call:
//			g_rte.msg_val('Unable to translate External Department Code to Department', '5543')
//
//		Calls master msg_val
//
// ***************************************************************
string args[]
args[1] = a_args

msg_val(a_newmessage,args)
end subroutine

public subroutine msg (longlong a_rtn, string a_newmessage, string a_args);msg(a_rtn, a_newmessage, {a_args})
end subroutine

public subroutine msg (string a_newmessage, string a_args);msg(a_newmessage, {a_args})
end subroutine

private function string msg_lookup (string a_newmessage);return sqlca.f_msg_lookup(g_process_id, a_newmessage, i_sqlca_sqldbcode)
end function

private subroutine msg_val (string a_newmessage, string a_pp_error_code, string a_args[]);// ***************************************************************
//		Validation logging function:
//			string	a_newmessage		Error message.
//			longlong	a_pp_error_code	Error code.
//			string	a_args[]				Values failing the validation.
//
//		Registers the validation messages to be reported at the end of the interface by msg_val_log
//
// ***************************************************************
longlong i, rownum

if not isvalid(i_ds_val_messages) then //Change from isValid to not isValid. If it is valid why are we creating again... 
	i_ds_val_messages = create uo_ds_top
	f_create_dynamic_ds(i_ds_val_messages,'grid','select msg, pp_error_code, msg additional_information, 0 logged from pp_processes_messages where -1 = 0',sqlca,true)	
end if

if upperbound(a_args) = 0 then a_args[1] = ''

for i = 1 to upperbound(a_args)
	rownum = i_ds_val_messages.InsertRow(0)
	i_ds_val_messages.SetItem(rownum,'msg',a_newmessage)
	i_ds_val_messages.SetItem(rownum,'pp_error_code',a_pp_error_code)
	i_ds_val_messages.SetItem(rownum,'additional_information',a_args[i])
	i_ds_val_messages.SetItem(rownum,'logged',0)
next
end subroutine

public function longlong msg_val_log ();// ***************************************************************
//		Validation logging function:
//				
//		Takes the validation messages stored in i_ds_val_messages and reports them to the logs
//		
//		Columns are:
//		select msg, pp_error_code, msg additional_information from pp_processes_messages where -1 = 0
//
// ***************************************************************
longlong i, rownum
string msg, prior_msg, additional_information, pp_error_code

if not isvalid(i_ds_val_messages) then return 0 //If not isvalid, then return 0 if it is valid then continue
if i_ds_val_messages.rowcount() = 0 then return 0

i_ds_val_messages.SetSort('msg, additional_information')
i_ds_val_messages.Sort()
i_ds_val_messages.SetFilter('logged = 0')
i_ds_val_messages.Filter()

prior_msg = '-99765'

for i = 1 to i_ds_val_messages.rowcount()
	msg = i_ds_val_messages.GetItemString(i,'msg')
	pp_error_code = i_ds_val_messages.GetItemString(i,'pp_error_code')
	additional_information = i_ds_val_messages.GetItemString(i,'additional_information')
	
	if additional_information <> '' then 
		if msg <> prior_msg then
			f_pp_msgs(" ")
			f_pp_msgs_detail("The following values failed validations with the message '" + msg + "':",string(pp_error_code),'')
		end if
		
		f_pp_msgs_detail(additional_information,string(pp_error_code),'')
	else
		if msg <> prior_msg then
			f_pp_msgs(" ")
			f_pp_msgs_detail("The following validation issue was encountered: " + msg + ":",string(pp_error_code),'')
		end if
	end if
	
	i_ds_val_messages.SetItem(i,'logged',1)
next 

i_ds_val_messages.SetFilter('')
i_ds_val_messages.Filter()

if i_ds_val_messages.rowcount() > 0 then
	return -2
else 
	return 0
end if
end function

public function longlong msg_log ();// ***************************************************************
//		Message logging function:
//		SHOULD ONLY BE USED AS PART OF THE VISUAL APPLICATION.
//		INTERFACES SHOULD "THROW" THE EXCEPTION AND LET THE INTERFACE
//		HANDLE RETURNS AND ERRORS.
// ***************************************************************
string exception_msg_nvo, exception_msg
longlong i

if isNull(this.text) then this.text = ''
	
exception_msg_nvo  = 'ERROR: An error occurred with message "' + this.text + '"'
f_pp_msgs_detail(exception_msg_nvo,string(this.i_pp_error_code),string(this.i_sqlca_sqldbcode))
	
exception_msg = 'Additional Information:'
	
// Additional information
for i = 1 to upperbound(this.i_args)
	if not isNull(this.i_args[i]) and trim(this.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(this.i_args[i])
next
	
// SQL Information
if not isNull(this.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(this.i_sqlca_sqlcode)
if this.i_sqlca_sqlcode >= 0 and not isNull(this.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(this.i_sqlca_sqlnrows)
if this.i_sqlca_sqlcode < 0 and not isNull(this.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(this.i_sqlca_sqlerrtext)

f_pp_msgs(exception_msg)

// Rollback the transaction
rollback;

return this.i_rtn
end function

public function boolean check_sql (string a_err_msg, transaction a_sqlca);if a_sqlca.sqlcode < 0 then
	msg(-1, a_err_msg)
	return true
else
	return false
end if
end function

public function boolean check_sql_no_rows (string a_err_msg, transaction a_sqlca);if a_sqlca.sqlnrows = 0 then
	msg(-1, a_err_msg)
	return true
else
	return false
end if
end function

public function boolean check_sql_no_rows (string a_err_msg);return check_sql_no_rows(a_err_msg,sqlca)
end function

public function boolean check_sql (string a_err_msg);return check_sql(a_err_msg,sqlca)
end function

public function boolean check_sql (string a_err_msg, uo_ds_top a_ds);if a_ds.i_sqlca_sqlcode < 0 then
	msg(-1, a_err_msg)
	return true
else
	return false
end if
end function

public function boolean check_sql_no_rows (string a_err_msg, datastore a_ds);if a_ds.rowcount() = 0 then
	msg(-1, a_err_msg)
	return true
else
	return false
end if
end function

on nvo_runtimeerror.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_runtimeerror.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// **************************************************************************
//
//	CODE IN THIS OBJECT IS 100% IDENTICAL TO NVO_RUNTIMEERROR_APP EXCEPT IT IS INHERITED FROM 
//		RUNTIMEERROR INSTEAD OF USEROBJECT .  THESE OBJECTS MUST BE KEPT IN SYNCH
//
// **************************************************************************

setnull(i_sqlca_sqlerrtext)
setnull(i_sqlca_sqlcode)
setnull(i_sqlca_sqldbcode)
setnull(i_sqlca_sqlnrows)
setnull(i_rtn)
end event

