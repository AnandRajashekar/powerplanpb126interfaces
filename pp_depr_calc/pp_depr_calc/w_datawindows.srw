HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_5 from datawindow within w_datawindows
end type
type dw_4 from datawindow within w_datawindows
end type
type dw_3 from datawindow within w_datawindows
end type
type dw_2 from datawindow within w_datawindows
end type
type dw_1 from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer width = 4754
integer height = 1980
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_5 dw_5
dw_4 dw_4
dw_3 dw_3
dw_2 dw_2
dw_1 dw_1
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_5=create dw_5
this.dw_4=create dw_4
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_1=create dw_1
this.Control[]={this.dw_5,&
this.dw_4,&
this.dw_3,&
this.dw_2,&
this.dw_1}
end on

on w_datawindows.destroy
destroy(this.dw_5)
destroy(this.dw_4)
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_1)
end on

type dw_5 from datawindow within w_datawindows
integer x = 2098
integer y = 1160
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_cpr_company"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_4 from datawindow within w_datawindows
integer x = 2711
integer y = 1284
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_3 from datawindow within w_datawindows
integer x = 745
integer y = 1172
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_2 from datawindow within w_datawindows
integer x = 608
integer y = 416
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_datawindows
integer x = 2112
integer y = 488
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

