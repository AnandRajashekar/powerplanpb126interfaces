HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_close_charge_collection.exe'

nvo_wo_control i_nvo_wo_control


end variables

forward prototypes
public function longlong uf_read ()
private function boolean uf_close_charge_collection ()
private function boolean uf_check_charge_collection_closed (longlong a_company_idx, datetime a_month)
private function boolean uf_check_interfaces_ran (longlong a_company_idx, string a_company_description, datetime a_month)
private function boolean uf_set_charge_collection_close_date (longlong a_company_idx, string a_company_descr, datetime a_month)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  12-04-2012      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Process Close Charge Collection logic
if not uf_close_charge_collection() then
	return rtn_failure
end if

i_nvo_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

private function boolean uf_close_charge_collection ();//*********************************************************************************************************
//
//	uf_close_charge_collection()
//	
//	This function corresponds to w_wo_control.cb_close_chage_collection.clicked() event
// It is called from the ssp_wo_close_charge_collection ppinterface 
// application object and contains the main business logic for closing charge collection
// for a list of companies for a specific month
//	
//	Parameters	:	none
//	
//	Returns		:	boolean : true  : All provided companies closed for the provided accounting month
//                          false : All provided companies were NOT closed for the provided accounting month
//
//*********************************************************************************************************

longlong ndx, i, month_number, rtn
datetime    previous_month
uo_ds_top   ds_users
boolean     vfy_users
string      sqls,  user_id, process_msg

i_nvo_wo_control.of_constructor()

i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_wo_control.i_month = datetime(g_ssp_parms.date_arg[1])

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("Close Charge Collection - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

// Get the descriptions
rtn = i_nvo_wo_control.of_getDescriptionsFromIds(i_nvo_wo_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

f_pp_msgs("Attempting to close charge collection for: ")
f_pp_msgs("Month: " + string(i_nvo_wo_control.i_month, "mm/dd/yyyy"))
for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	f_pp_msgs("Company ID: " + string(i_nvo_wo_control.i_company_idx[ndx]) + ", Company Name: " + i_nvo_wo_control.i_company_descr[ndx])
next

// check if the multiple companies chosen can be processed  together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if

//
//	loop over the company list, and react to the selected rows
//
for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	
	// calculate the previous month's date
	select add_months(:i_nvo_wo_control.i_month, -1) into :previous_month from dual;

	// make sure the previous month was closed
	if not uf_check_charge_collection_closed( i_nvo_wo_control.i_company_idx[ndx], previous_month ) then
		f_pp_msgs("Close Charge Collection - " + &
					 "You must first close charge collection for the previous month: '" + string(previous_month, "mm/dd/yyyy") + "', for company: '" + i_nvo_wo_control.i_company_descr[ndx] + "'")
		return false
	end if
	
	// make sure all required interfaces have been ran for this company
	if not uf_check_interfaces_ran( i_nvo_wo_control.i_company_idx[ndx], i_nvo_wo_control.i_company_descr[ndx], i_nvo_wo_control.i_month ) then
		f_pp_msgs("Close Charge Collection - " + &
					 "You must first run all required interfaces for month: '" + string(i_nvo_wo_control.i_month, "mm/dd/yyyy") +"', for company: '"+ i_nvo_wo_control.i_company_descr[ndx] + "'")
		return false
	end if

	// if all the months have been closed and all the interfaces have been run
	// then close the month for the current company
	if not uf_set_charge_collection_close_date( i_nvo_wo_control.i_company_idx[ndx], i_nvo_wo_control.i_company_descr[ndx], i_nvo_wo_control.i_month) then
		return false
	end if
next

// email that companies have closed
i_nvo_wo_control.of_cleanup( 1, 'email wo close: close charge collection', 'Close Charge Collection')

return true
end function

private function boolean uf_check_charge_collection_closed (longlong a_company_idx, datetime a_month);//*********************************************************************************************************
//
//	uf_check_charge_collection_closed()
//	
//	This function corresponds to w_wo_control.cb_close_chage_collection.clicked() event
// It is called from the ssp_wo_close_charge_collection ppinterface 
// application object and is used to verify that charge collection has been 
// closed for the month requested
//	
//	Parameters	:	longlong : a_company_idx
//                datetime : a_month
//	
//	Returns		:	boolean	: true  : Month is Closed for Company and Month provided
//                           false : Month is Not Closed for Company and Month provided
//
//*********************************************************************************************************

datetime wo_charge_collection

setNull(wo_charge_collection)

//  Make sure that "Charge Collection" is closed for the month provided...
select wo_charge_collection into :wo_charge_collection
from wo_process_control
where ( company_id = :a_company_idx )
and  to_char( accounting_month, 'yyyymm' ) = to_char(:a_month, 'yyyymm' );

// if the wo_charge_collection for the last month for this company is null
// then the last month wasn't closed
if isnull(wo_charge_collection) then
	return false
end if

return true
end function

private function boolean uf_check_interfaces_ran (longlong a_company_idx, string a_company_description, datetime a_month);//*********************************************************************************************************
//
//	uf_check_interfaces_ran()
//	
//	This function corresponds to w_wo_control.cb_close_chage_collection.clicked() event
// It is called from the ssp_wo_close_charge_collection ppinterface 
// application object and is used to verify that all required interfaces
// have been run for a company (this is necessary to close charges for a month)
//	
//	Parameters	:	longlong : a_company_idx
//                string : a_company_description
//                date   : a_month
//	
//	Returns		:	boolean : true  : All needed interfaces ran for the provided company and month
//                          false : All needed interfaces have NOT been run for the provided company and month
//
//*********************************************************************************************************

longlong l_required_for_closing = 0

select count(1)
into :l_required_for_closing
from pp_interface pi, pp_interface_dates pid
where pi.interface_id = pid.interface_id
and pi.company_id = pid.company_id
and pi.required_for_closing = 1
and pid.last_run is null
and pi.subsystem = 'Project Management'
and pi.company_id = :a_company_idx
and pid.accounting_month = :a_month
;

if l_required_for_closing > 0 then
	return false			
end if

return true
end function

private function boolean uf_set_charge_collection_close_date (longlong a_company_idx, string a_company_descr, datetime a_month);//*********************************************************************************************************
//
//	uf_set_charge_collection()
//	
//	This function corresponds to w_wo_control.cb_close_chage_collection.clicked() event
// It is called from the ssp_wo_close_charge_collection ppinterface application object
// It will set the wo_charge_collection date column on the wo_process_control table
// for the company provided for the accounting month provided
//	
//	Parameters	:	longlong : a_company_idx : company id
//	               string : a_company_descr : company description
//                datetime   : a_month : accounting month for closing
//	
//	Returns		:	boolean : true  : Provided company closed for the provided accounting month
//                          false : Provided company NOT closed for the provided accounting month
//
//*********************************************************************************************************

longlong pp_stat_id

f_pp_msgs("Close Charge Collection started for company: '" + a_company_descr  +  "', month '" + string(a_month, "mm/dd/yyyy") + "' at '" + String(Today(), "hh:mm:ss") + "'")

// log performance statistics
pp_stat_id = f_pp_statistics_start("WO Monthly Close", "Close Charge Collection: " + a_company_descr)

if i_nvo_wo_control.i_ds_wo_control.retrieve(a_company_idx, a_month) = 1 then
	if (i_nvo_wo_control.i_ds_wo_control.setitem( 1, 'wo_charge_collection', today()) = 1) then
		f_pp_msgs("Charge Collection Closed for company: '" + a_company_descr  +  "', month: '" + string(a_month, "mm/dd/yyyy") + "' at '" + String(Today(), "hh:mm:ss") + "'")
	else
		f_pp_msgs("ERROR: Closing Charge Collection for company: '" + a_company_descr  +  "', month '" + string(a_month, "mm/dd/yyyy") + "'")
	end if

	i_nvo_wo_control.of_updateDW() 
else
	f_pp_msgs("ERROR: retrieving wo_process_control record for company: '" + a_company_descr  +  "', month '" + string(a_month, "mm/dd/yyyy") + "'")
end if

// stop capturing performance statistics
if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)

return true
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

