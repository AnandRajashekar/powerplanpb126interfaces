HA$PBExportHeader$w_datawindows.srw
$PBExportComments$must have a window that reference dw_wo_process_control~r~nin the project so that it will be included in the executable
forward
global type w_datawindows from window
end type
type dw_wo_process_control from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer width = 3168
integer height = 1320
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_wo_process_control dw_wo_process_control
end type
global w_datawindows w_datawindows

event open;// must have a window that reference dw_wo_process_control
// in the project so that it will be included in the executable
end event

on w_datawindows.create
this.dw_wo_process_control=create dw_wo_process_control
this.Control[]={this.dw_wo_process_control}
end on

on w_datawindows.destroy
destroy(this.dw_wo_process_control)
end on

type dw_wo_process_control from datawindow within w_datawindows
integer x = 114
integer y = 80
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

