HA$PBExportHeader$w_datawindows.srw
$PBExportComments$window to make sure that all the necessary DW objects get included in the build process
forward
global type w_datawindows from window
end type
type dw_3 from datawindow within w_datawindows
end type
type dw_2 from datawindow within w_datawindows
end type
type dw_1 from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer width = 4754
integer height = 1980
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_3 dw_3
dw_2 dw_2
dw_1 dw_1
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_1=create dw_1
this.Control[]={this.dw_3,&
this.dw_2,&
this.dw_1}
end on

on w_datawindows.destroy
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_1)
end on

type dw_3 from datawindow within w_datawindows
integer x = 297
integer y = 832
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_2 from datawindow within w_datawindows
integer x = 1074
integer y = 240
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_wo_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_datawindows
integer x = 251
integer y = 212
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

