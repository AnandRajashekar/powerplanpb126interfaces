HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_close_month.exe'

nvo_wo_control i_nvo_wo_control

boolean ib_check_oh[], ib_check_auto[]
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public subroutine uf_check_option_array (longlong a_company_ids[], ref boolean a_option_values[], longlong a_option_id, string a_option_description)
public function boolean uf_wo_close_month_main ()
public function longlong uf_afudc_je ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

if not uf_wo_close_month_main() then
	// log the list of companies that failed to perform auto unitization
	i_nvo_wo_control.of_log_failed_companies("CLOSE MONTH")
	return rtn_failure
end if

i_nvo_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public subroutine uf_check_option_array (longlong a_company_ids[], ref boolean a_option_values[], longlong a_option_id, string a_option_description);/************************************************************************************************************************************************************
**
**	uf_check_option_array()
**	
**	This function verfies whether a "Month End Option" was passed as a paramter and if not checks the pp_month_end_options table.
**	
**	Parameters	:	longlong : (a_company_ids)
**						boolean  : (a_option_values)
**						longlong : (a_option_id)
**						string   : (a_option_description)
**	
**	Returns		:	(none)
**
************************************************************************************************************************************************************/
boolean check_month_end_option
longlong i

for i = 1 to upperbound(a_company_ids)
	//make sure boolean_arg is populated
	if upperbound(a_option_values) < i then 
		check_month_end_option = true
	elseif isnull(a_option_values[i]) then //having this as a separate ELSEIF is important. If we used an OR in the IF clause, an array out of bounds exception occurs.
		check_month_end_option = true
	else 
		check_month_end_option = false
	end if
	
	if check_month_end_option = true then 
		f_pp_msgs("Checking month end option for '" + a_option_description + "'")	
		try
			if lower(f_pp_month_end_options(a_company_ids[i], g_process_id, a_option_id)) = 'yes' then 
				f_pp_msgs("'" + a_option_description + "' set to Yes.")	
				a_option_values[i] = false
			else 
				f_pp_msgs("'" + a_option_description + "' set to No.")	
				a_option_values[i] = true
			end if
		catch (exception e)
			f_pp_msgs("Could not find value for '" + a_option_description + "' month end option. Defaulting to 'No'.")
			a_option_values[i] = true	
		end try
	end if 
next
end subroutine

public function boolean uf_wo_close_month_main ();/************************************************************************************************************************************************************
**
**	uf_wo_close_month_main()
**	
**	This function corresponds to w_wo_control.cb_close_powerplant.event clicked().
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
longlong rtn, counter, num_months, month_number
////	added for company loop
longlong	c, num_companies, process_id, pp_stat_id, i, ret
string allow_pending, comp_descr, skip_afudc_check
string  sqls,  user_id, find_str, num_months_str, args[], process_msg
datetime max_month
datetime wo_charge_collection

// if any company in the loop has an Auto Unitization error,
// it continues on to the next company but returns a failure
// status to uf_read
boolean b_all_success
b_all_success = true

// Construct the nvo
i_nvo_wo_control.of_constructor()

// Get the ssp parameters
i_nvo_wo_control.i_company_idx[] = g_ssp_parms.long_arg[]
i_nvo_wo_control.i_month			= datetime(g_ssp_parms.date_arg[1])
i_nvo_wo_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])
ib_check_oh[]   						= g_ssp_parms.boolean_arg[]
ib_check_auto[] 						= g_ssp_parms.boolean_arg2[]

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("Close PowerPlant - Month: " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

// populate the company descs
rtn = i_nvo_wo_control.of_getDescriptionsFromIds(i_nvo_wo_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// Handle the month end options
uf_check_option_array(i_nvo_wo_control.i_company_idx,ib_check_oh,1,'Continue if Overheads and AFUDC have not been calculated?')
uf_check_option_array(i_nvo_wo_control.i_company_idx,ib_check_auto,2,'Continue if Auto Unitization has not been completed?')

// Verify the companies can be run together
if (i_nvo_wo_control.of_selectedCompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do " + &
				" not line up")
	return false
end if

//	loop over the companies
num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c = 1 to num_companies
	//	it's selected, update the rest of the window
	i_nvo_wo_control.of_companyChanged(c, i_nvo_wo_control.i_month)

	select description  into :comp_descr from company where company_id = :i_nvo_wo_control.i_company;

	f_pp_msgs("Close PowerPlant - Month: " + string(i_nvo_wo_control.i_month_number) + &
    " Company:  " + comp_descr + " Comp Id: " + string(i_nvo_wo_control.i_company)+ ' ' + string(now()))
	
   f_pp_msgs("Close PowerPlant Started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
	
	//
	//  They cannot click this button if there are any pending transactions remaining.
	//  The balances table that we normally embed in f_wo_control_close_month will
	//  end up out of balance.
	//
	counter = 0
	select count(*) into :counter from pend_transaction
	 where to_number(to_char(gl_posting_mo_yr,'yyyymm')) <= :i_nvo_wo_control.i_month_number and
			 company_id = :i_nvo_wo_control.i_company;
	if isnull(counter) then counter = 0
	 
	
	if counter > 0 then
		f_pp_msgs("Pending transactions exist.~n" + &
					 "They must be posted or deleted before running this process.")
		return false
	end if
 
	//###sjh 1/3/2006 added this to prevent closing the wrong month
 	// make sure the previous month is closed   	
	counter=0
	select count(*)   into :counter
	from wo_process_control
	where company_id = :i_nvo_wo_control.i_company and
	accounting_month = add_months(to_date(:i_nvo_wo_control.i_month_number,'yyyymm'), -1) and
	powerplant_closed  is null;

	if counter = 1 then
		f_pp_msgs("Information: You cannot close the month if the previous month is open.")
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(c)
		continue // next company
	end if 

 	// ensure Close Charge Collection has been ran for this company, for this month
	setNull(wo_charge_collection)

	//  Make sure that "Charge Collection" is closed for the month provided...
	select wo_charge_collection into :wo_charge_collection
	from wo_process_control
	where ( company_id = :i_nvo_wo_control.i_company )
	and  to_char( accounting_month, 'yyyymm' ) = to_char(:i_nvo_wo_control.i_month, 'yyyymm' );

	if isnull(wo_charge_collection) then
		f_pp_msgs("ERROR: Close Charge Collection has not been ran for Company: " + string(i_nvo_wo_control.i_company) + " - " + comp_descr + ", for selected Month: " + string(i_nvo_wo_control.i_month))
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(c)
		continue // next company
	end if 
	
	//
	//  There must be at least one open month ...
	//
	select max(accounting_month)
	  into :max_month
	  from wo_process_control
	 where company_id = :i_nvo_wo_control.i_company;
	
//	if i_nvo_wo_control.i_col_num = 1 then
	if max_month = i_nvo_wo_control.i_month then
		f_pp_msgs("There must be at least one open month ... Please add a new month before closing.")
		return false
	end if

	if isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "overhead_calculation")) then
		if ib_check_oh[c] then
			f_pp_msgs("Overheads and AFUDC have not yet been calculated (hard stop - Close Month process cannot continue)")
			return false
		else
			f_pp_msgs("Overheads and AFUDC have not yet been calculated (warning - Close Month process will continue)")
		end if
	end if

	if isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "auto_close")) then
		if ib_check_auto[c] then
			f_pp_msgs("Auto-Unitization has not yet been run (hard stop - Close Month process cannot continue)")
			return false
		else
			f_pp_msgs("Auto-Unitization has not yet been run (warning - Close Month process will continue)")
		end if
	end if

	if isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "powerplant_closed")) then
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'powerplant_closed',today()) 
	else
		f_pp_msgs("This month is already closed.")
		return false
	end if

	////*******************************************************
	////  Call f_cwip_balance which builds the cwip_balance
	////  table.  This table keeps CWIP and RWIP balances
	////  by work_order_id, company_id, charge_type_id,
	////  gl_account_id, and month_number.
	////*******************************************************
	f_pp_msgs("Build the cwip_balance table at " + String(Today(), "hh:mm:ss"))
		
	rtn = f_cwip_balance(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number)

	//  DO NOT error check and return out of the script ... the
	//  f_wo_control_close_month function MUST RUN, even if this
	//  function returns an error

	////*******************************************************
	////  Call f_metric which builds the metric data
	////  table.  This table keeps metric data
	////*******************************************************
	f_pp_msgs("Build the wo_metric_results table at " + String(Today(), "hh:mm:ss"))
		
	rtn = f_metric(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month)

	//  DO NOT error check and return out of the script ... the
	//  f_wo_control_close_month function MUST RUN, even if this
	//  function returns an error.

	//*******************************************************
	//  Call wf_afudc_je which updates the afudc_calc
	//  table for any charge JE's that affected AFUDC or CPI.
	//*******************************************************
	f_pp_msgs("Update  the afudc_calc table " + String(Today(), "hh:mm:ss"))
	
	rtn = uf_afudc_je()

	//  DO NOT error check and return out of the script ... the
	//  f_wo_control_close_month function MUST RUN, even if this
	//  function returns an error.

	//*******************************************************
	//  Call f_update_afudc_base_month which should be
	//  updates the base for any charges between the end of the month 
	//  and afudc calc, can be run multiple times, always incremental
	//*******************************************************
	f_pp_msgs("Update  the f_update_afudc_base_month table " + String(Today(), "hh:mm:ss"))

	skip_afudc_check = f_pp_system_control_company("WO CLOSE - Skip Late AFUDC Check", i_nvo_wo_control.i_company)
	
	if skip_afudc_check = 'no' or skip_afudc_check ='' then
		rtn = f_update_afudc_base_month(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number)
	end if

	if rtn <> 1 then
		f_pp_msgs("ERROR: in f_update_afudc_base_month: " + sqlca.SQLErrText)
		rollback;
		return false
	end if

	////*******************************************************
	////  Call f_wo_cwip_in_base_rollforward which rolls forward cwip in base amounts
	//JRD - 04-14-2008
	////*******************************************************
	f_pp_msgs("Rollforward CWIP in Base table at " + String(Today(), "hh:mm:ss"))
		
	rtn = f_wo_cwip_in_base_rollforward(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month)
	
	//  DO NOT error check and return out of the script ... the
	//  f_wo_control_close_month function MUST RUN, even if this
	//  function returns an error.

	//*******************************************************
	//  Call f_wo_control_close_month which should be
	//  customized for each client.  This function can be
	//  used for things like building customized reporting
	//  tables.
	//*******************************************************
	f_pp_msgs("call custom  f_wo_control_close_month function " + String(Today(), "hh:mm:ss"))

	rtn = f_wo_control_close_month()

	if rtn <> 1 then
		f_pp_msgs("ERROR: in f_wo_control_close_month: " + sqlca.SQLErrText)
		return false
	end if
	
	// Call Dynamic Validation option
	args[1] = string(i_nvo_wo_control.i_month)
	args[2] = string(i_nvo_wo_control.i_company)
	
	ret = f_wo_validation_control(1060,args)
	
	if ret < 0 then
		rollback;
		f_pp_msgs("Calling Custom Dynamic Validations to close the month Failed.")
		return false
	end if

	// Update wo_process_control
	i_nvo_wo_control.of_updateDW()
next

i_nvo_wo_control.of_cleanUp(9, 'email wo close: close month', "Close Month")

return b_all_success
end function

public function longlong uf_afudc_je ();/************************************************************************************************************************************************************
**
**	uf_afudc_je()
**	
**	This function corresponds to w_wo_control.wf_afudc_je().
**	
**	Parameters	:	(none)
**	
**	Returns		:	longlong value
**
************************************************************************************************************************************************************/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//update the afudc/cpi journals inserted into CWIP_CHARGE to the AFUDC_CALC table
//this function could be run more than once per month (close date removed from wo_process_control) so figure out incremental differences to update to AFUDC_CALC
//sum(amount) from cwip_charge, because there could be more than one afudc journal charge. 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong   num_rows, i, wo_id
string sqls, rtn_string
decimal {2} cwip_amount, afudc_calc_amount, amount 

uo_ds_top ds_afudc
ds_afudc = CREATE uo_ds_top

//--------------  AFUDC DEBT  ------------------------------------------------

f_pp_msgs("Charge JE: afudc_debt")

sqls = + &
	"select a.work_order_id, sum(a.amount) " + &
     "from cwip_charge a, afudc_control b, work_order_account c " + &
    "where a.cost_element_id = b.debt_cost_element and " + &
	       "a.work_order_id = c.work_order_id and " + &
		    "b.afudc_type_id = c.afudc_type_id and " + &
		    "a.status is null and month_number = " + string(i_nvo_wo_control.i_month_number) + &
	  " and a.company_id = " + string(i_nvo_wo_control.i_company) + &
		    " group by a.work_order_id "

rtn_string = f_create_dynamic_ds(ds_afudc, "grid", sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	f_pp_msgs( "ERROR: ERROR: unable to create ds_afudc for afudc_debt updates:  " + rtn_string)
	rollback;
	return -1
end if 

num_rows = ds_afudc.RowCount()

for i = 1 to num_rows
	
	if i = 1 or mod(i,100) = 0 then
		f_pp_msgs("Charge JE: afudc_debt: Row " + string(i) + " of " + string(num_rows))
	end if
	
	wo_id  = ds_afudc.GetItemNumber(i, 1)
	cwip_amount = ds_afudc.GetItemNumber(i, 2) 

	//Maint 41474: compare the afudc_je_debt already saved to the current cwip_charge je amount	
	afudc_calc_amount = 0
	select nvl(afudc_je_debt,0) into :afudc_calc_amount
	from afudc_calc 
	where work_order_id = :wo_id
	and to_number(to_char(month, 'yyyymm')) = :i_nvo_wo_control.i_month_number;
	
	amount = cwip_amount - afudc_calc_amount

	update afudc_calc 
	set    afudc_debt = nvl(afudc_debt,0) + :amount,
			 afudc_compound_amt = nvl(afudc_compound_amt,0) + :amount,
			 afudc_je_debt = nvl(afudc_je_debt,0) + :amount 
	where  work_order_id = :wo_id and to_number(to_char(month, 'yyyymm')) = :i_nvo_wo_control.i_month_number;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs( "ERROR: updating afudc_calc.afudc_debt~n~n" + &
				sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
next

//--------------  AFUDC EQUITY  ------------------------------------------------

f_pp_msgs("Charge JE: afudc_equity")

sqls = + &
	"select a.work_order_id, sum(a.amount) " + &
     "from cwip_charge a, afudc_control b, work_order_account c " + &
    "where a.cost_element_id = b.equity_cost_element and " + &
	       "a.work_order_id = c.work_order_id and " + &
		    "b.afudc_type_id = c.afudc_type_id and " + &
		    "a.status is null and month_number = " + string(i_nvo_wo_control.i_month_number) + &
	  " and a.company_id = " + string(i_nvo_wo_control.i_company) + &
		    " group by a.work_order_id "

rtn_string = f_create_dynamic_ds(ds_afudc, "grid", sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	f_pp_msgs( "ERROR: ERROR: unable to create ds_afudc for afudc_equity updates:  " + rtn_string)
	rollback;
	return -1
end if 

num_rows = ds_afudc.RowCount()

for i = 1 to num_rows
	
	if i = 1 or mod(i, 100) = 0 then
		f_pp_msgs("Charge JE: afudc_equity: Row " + string(i) + " of " + string(num_rows))
	end if
	
	wo_id  = ds_afudc.GetItemNumber(i, 1)
	cwip_amount = ds_afudc.GetItemNumber(i, 2)
	
	//Maint 41474: compare the afudc_je_debt already saved to the current cwip_charge je amount	
	afudc_calc_amount = 0
	select nvl(afudc_je_equity,0) into :afudc_calc_amount
	from afudc_calc 
	where work_order_id = :wo_id
	and to_number(to_char(month, 'yyyymm')) = :i_nvo_wo_control.i_month_number;
	
	amount = cwip_amount - afudc_calc_amount 
	
	update afudc_calc 
	set    afudc_equity = nvl(afudc_equity,0) + :amount,
			 afudc_compound_amt = nvl(afudc_compound_amt,0) + :amount,
			 afudc_je_equity = nvl(afudc_je_equity,0) + :amount 
	where  work_order_id = :wo_id and to_number(to_char(month, 'yyyymm')) = :i_nvo_wo_control.i_month_number;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs( "ERROR: updating afudc_calc.afudc_equity~n~n" + &
			sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
next

//--------------  CPI  ------------------------------------------------

f_pp_msgs("Charge JE: cpi")

sqls = + &
	"select a.work_order_id, sum(a.amount) " + &
     "from cwip_charge a, afudc_control b, work_order_account c " + &
    "where a.cost_element_id = b.cpi_cost_element and " + &
	       "a.work_order_id = c.work_order_id and " + &
		    "b.afudc_type_id = c.afudc_type_id and " + &
		    "a.status is null and month_number = " + string(i_nvo_wo_control.i_month_number) + &
	  " and a.company_id = " + string(i_nvo_wo_control.i_company) + &
		    " group by a.work_order_id "

rtn_string = f_create_dynamic_ds(ds_afudc, "grid", sqls, sqlca, true)

if upper(rtn_string) <> 'OK' then 
	f_pp_msgs( "ERROR: ERROR: unable to create ds_afudc for cpi updates:  " + rtn_string)
	rollback;
	return -1
end if 

num_rows = ds_afudc.RowCount()

for i = 1 to num_rows
	
	if i = 1 or mod(i, 100) = 0 then
		f_pp_msgs("Charge JE: cpi: Row " + string(i) + " of " + string(num_rows))
	end if
	
	wo_id  = ds_afudc.GetItemNumber(i, 1)
	cwip_amount = ds_afudc.GetItemNumber(i, 2)

	//Maint 41474: compare the afudc_je_debt already saved to the current cwip_charge je amount	
	afudc_calc_amount = 0
	select nvl(cpi_je,0) into :afudc_calc_amount
	from afudc_calc 
	where work_order_id = :wo_id
	and to_number(to_char(month, 'yyyymm')) = :i_nvo_wo_control.i_month_number;
	
	amount = cwip_amount - afudc_calc_amount 

	update afudc_calc 
	set    cpi = nvl(cpi,0) + :amount,
			 cpi_compound_amt = nvl(cpi_compound_amt,0) + :amount,
			 cpi_je = nvl(cpi_je,0) + :amount 
	where  work_order_id = :wo_id and to_number(to_char(month, 'yyyymm')) = :i_nvo_wo_control.i_month_number;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs( "ERROR: updating afudc_calc.cpi~n~n" + &
			sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
next

DESTROY ds_afudc


return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_wo_close_month_custom_version nvo_ssp_wo_close_month_custom_version

choose case a_pbd_name
	case 'ssp_wo_close_month_custom.pbd'
		return nvo_ssp_wo_close_month_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

