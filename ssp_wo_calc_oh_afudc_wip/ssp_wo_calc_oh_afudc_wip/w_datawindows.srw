HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_cap_int_allo from datawindow within w_datawindows
end type
type dw_cost_element from datawindow within w_datawindows
end type
type dw_overhead_work_orders from datawindow within w_datawindows
end type
type dw_clearing_wo_rate from datawindow within w_datawindows
end type
type dw_clearing_base from datawindow within w_datawindows
end type
type dw_cr_element_definitions from datawindow within w_datawindows
end type
type dw_wo_process_afudc_ovh from datawindow within w_datawindows
end type
type dw_afudc_calc_autogen_dw from datawindow within w_datawindows
end type
type dw_wo_interface_dates_all from datawindow within w_datawindows
end type
type dw_cpr_act_month from datawindow within w_datawindows
end type
type dw_clearing_wo_control from datawindow within w_datawindows
end type
type dw_wo_process_control from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
string tag = "w_datawindows"
integer width = 3168
integer height = 2344
boolean titlebar = true
string title = "w_datawindows"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_cap_int_allo dw_cap_int_allo
dw_cost_element dw_cost_element
dw_overhead_work_orders dw_overhead_work_orders
dw_clearing_wo_rate dw_clearing_wo_rate
dw_clearing_base dw_clearing_base
dw_cr_element_definitions dw_cr_element_definitions
dw_wo_process_afudc_ovh dw_wo_process_afudc_ovh
dw_afudc_calc_autogen_dw dw_afudc_calc_autogen_dw
dw_wo_interface_dates_all dw_wo_interface_dates_all
dw_cpr_act_month dw_cpr_act_month
dw_clearing_wo_control dw_clearing_wo_control
dw_wo_process_control dw_wo_process_control
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_cap_int_allo=create dw_cap_int_allo
this.dw_cost_element=create dw_cost_element
this.dw_overhead_work_orders=create dw_overhead_work_orders
this.dw_clearing_wo_rate=create dw_clearing_wo_rate
this.dw_clearing_base=create dw_clearing_base
this.dw_cr_element_definitions=create dw_cr_element_definitions
this.dw_wo_process_afudc_ovh=create dw_wo_process_afudc_ovh
this.dw_afudc_calc_autogen_dw=create dw_afudc_calc_autogen_dw
this.dw_wo_interface_dates_all=create dw_wo_interface_dates_all
this.dw_cpr_act_month=create dw_cpr_act_month
this.dw_clearing_wo_control=create dw_clearing_wo_control
this.dw_wo_process_control=create dw_wo_process_control
this.Control[]={this.dw_cap_int_allo,&
this.dw_cost_element,&
this.dw_overhead_work_orders,&
this.dw_clearing_wo_rate,&
this.dw_clearing_base,&
this.dw_cr_element_definitions,&
this.dw_wo_process_afudc_ovh,&
this.dw_afudc_calc_autogen_dw,&
this.dw_wo_interface_dates_all,&
this.dw_cpr_act_month,&
this.dw_clearing_wo_control,&
this.dw_wo_process_control}
end on

on w_datawindows.destroy
destroy(this.dw_cap_int_allo)
destroy(this.dw_cost_element)
destroy(this.dw_overhead_work_orders)
destroy(this.dw_clearing_wo_rate)
destroy(this.dw_clearing_base)
destroy(this.dw_cr_element_definitions)
destroy(this.dw_wo_process_afudc_ovh)
destroy(this.dw_afudc_calc_autogen_dw)
destroy(this.dw_wo_interface_dates_all)
destroy(this.dw_cpr_act_month)
destroy(this.dw_clearing_wo_control)
destroy(this.dw_wo_process_control)
end on

type dw_cap_int_allo from datawindow within w_datawindows
integer x = 2336
integer y = 976
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_cap_int_allo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cost_element from datawindow within w_datawindows
integer x = 1550
integer y = 960
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_cost_element"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_overhead_work_orders from datawindow within w_datawindows
integer x = 782
integer y = 940
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_overhead_work_orders"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_wo_rate from datawindow within w_datawindows
integer x = 41
integer y = 940
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_clearing_wo_rate"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_base from datawindow within w_datawindows
integer x = 2331
integer y = 492
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_clearing_base"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cr_element_definitions from datawindow within w_datawindows
integer x = 1545
integer y = 476
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_afudc_ovh from datawindow within w_datawindows
integer x = 768
integer y = 476
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_wo_process_afudc_ovh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_afudc_calc_autogen_dw from datawindow within w_datawindows
integer x = 32
integer y = 480
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_afudc_calc_autogen_dw"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_interface_dates_all from datawindow within w_datawindows
integer x = 2318
integer y = 40
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_wo_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_act_month from datawindow within w_datawindows
integer x = 1541
integer y = 28
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_wo_control from datawindow within w_datawindows
integer x = 763
integer y = 28
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_clearing_wo_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_control from datawindow within w_datawindows
integer x = 37
integer y = 32
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

