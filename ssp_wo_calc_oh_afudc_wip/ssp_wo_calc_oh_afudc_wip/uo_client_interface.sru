HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_calc_oh_afudc_wip.exe'

nvo_wo_control i_nvo_wo_control

string i_calc_ovh_before  
string i_calc_ovh_after   
string i_calc_afudc       
string i_appr_ovh_before   
string i_appr_ovh_after   
string i_appr_afudc

boolean i_debug=true
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function boolean uf_overhead_calc ()
public function boolean uf_calc_oh_afudc_wip ()
public function boolean uf_retrieve_dates ()
public function boolean uf_after_oh_calc (datetime a_month)
public function boolean uf_before_oh_calc (datetime a_month)
public function boolean uf_none_calc (datetime a_month)
public function boolean uf_afudc_real_calc (datetime a_month)
public function boolean uf_wo_afudc_ovh_control (boolean cbx_calc_ovh_before_checked, boolean cbx_calc_afudc_checked, boolean cbx_calc_ovh_after_checked, boolean cbx_calc_ovh_before_enabled, boolean cbx_calc_afudc_enabled, datetime a_month)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w
w_custom_dw w_custom
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Process Close Charge Collection logic
if not uf_calc_oh_afudc_wip() then
	return rtn_failure
end if

i_nvo_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function boolean uf_overhead_calc ();//**********************************************************************************************************************************************************
//
// uf_overhead_calc()
// 
// This function performs the overhead calculation
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string sql, filter_string, month_string, ans, id_num, ext_gl_acct, check_for_gl_acct
longlong i, priority, j, rtn, clear_rows, k, rate_rows, expend_id, oh_charge_type, clear_id, &
       clear_type, dept, cost_element, rows, acct, credit_acct, gl_acct,ce_rows, bus_seg, &
       cs_expend_id, account_type_id, ru_id, bus_seg_clear, dept_clear
decimal {8} rate
decimal {2} base, clearing_balance, oh_charge, total_cleared, diff, removal, &
            sum_abs_initial_clearing, sum_abs_remainder_clearing
// These declarations should be duplicates of those in the AFUDC calc button
string   description, dw_name, col_name, find_string, je_code, cwip_external_account_option 

integer  hours, quantity, num_cols, col
longlong wo_id, charge_id,  clear_wo_id, month_number, company_id, find_row,   proc_type
datetime gl_posting_mo_yr
boolean  ce_exists, acct_exists, dept_exists, rate_is_input, mult_expend, &
         task_exists, mult_expend_from, ru_exists, al_exists, sn_exists//JRD 10-11-04
//LLB 7/31/2007         
boolean   sa_exists
longlong sa_id

//JRD 05/13/08         
boolean   ref_num_exists
string ref_number

//JRD 10-11-04
longlong al
string sn, ovh_dept  

//JRD 1-14-09
string task

// ### LKK  external overheads 03/27/07 end 

//  Get pp_system_control variable to determine whether or not to calculate
//  overheads by utility account ...
ans = f_pp_system_control_company("Calc Overheads by Account",i_nvo_wo_control.i_company)

cwip_external_account_option = f_pp_system_control_company('AFUDC Calc-Use External GL Account', i_nvo_wo_control.i_company)

setnull(acct)

uo_ds_top dw_clearing_wo_control
dw_clearing_wo_control = CREATE uo_ds_top
dw_clearing_wo_control.DataObject = "dw_clearing_wo_control"

uo_ds_top dw_clearing_base
dw_clearing_base = CREATE uo_ds_top
dw_clearing_base.DataObject = "dw_clearing_base"

uo_ds_top dw_clearing_wo_rate
dw_clearing_wo_rate = CREATE uo_ds_top
dw_clearing_wo_rate.DataObject = "dw_clearing_wo_rate"

uo_ds_top dw_overhead_work_orders
dw_overhead_work_orders = CREATE uo_ds_top
dw_overhead_work_orders.DataObject = "dw_overhead_work_orders"

//  Datastore:  Retrieves all cost elements ...

uo_ds_top ds_cost_element
ds_cost_element = CREATE uo_ds_top
ds_cost_element.DataObject = "dw_cost_element"
ds_cost_element.SetTransObject(sqlca)
ce_rows = ds_cost_element.RETRIEVE()


dw_overhead_work_orders.SetTransObject(sqlca)
dw_clearing_wo_control.SetTransObject(sqlca)
dw_clearing_base.SetTransObject(sqlca)
dw_clearing_wo_rate.SetTransObject(sqlca)

// the i_ovh_type instance variable gets set in uf_before_oh_calc just before calling this function
choose case i_nvo_wo_control.i_ovh_type
   case 0 // after ovh
      f_add_where_clause_ds(dw_clearing_wo_control, "", " and before_afudc_indicator = 0 ",false)
      dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)
   case 1 // before ovh
      f_add_where_clause_ds(dw_clearing_wo_control, "", " and before_afudc_indicator = 1 ",false)
      dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)
   case 2  // external ovh
      f_add_where_clause_ds(dw_clearing_wo_control, "", " and before_afudc_indicator = 2 ",false)
      dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)
end choose      
 
if dw_clearing_wo_control.rowcount() = 0 then goto doReturn

month_string = string(month(date(i_nvo_wo_control.i_month))) + '/' + string(year(date(i_nvo_wo_control.i_month)))

if len(month_string) = 6 then month_string = '0' + month_string

gl_posting_mo_yr = i_nvo_wo_control.i_month

month_number = year(date(gl_posting_mo_yr)) * 100 + month(date(gl_posting_mo_yr))

hours    = 0
quantity = 0

cwip_external_account_option = f_pp_system_control_company('AFUDC Calc-Use External GL Account', i_nvo_wo_control.i_company)

// Find the unique priorities for the loop
sql = 'select distinct(processing_priority) from clearing_wo_control ' + &
      'order by processing_priority';

// dw_priority used to be on the w_wo_control window
// but now we're creating it as a datastore
uo_ds_top dw_priority
dw_priority = CREATE uo_ds_top
f_create_dynamic_ds(dw_priority, 'grid', sql, sqlca, true)

rows = dw_priority.rowcount()

for i = 1 to rows

   priority = dw_priority.getitemnumber(i,1)
   
   // Filter to all clearing overheads with the priority
   filter_string = 'processing_priority = ' + string(priority)

   dw_clearing_wo_control.Setfilter(filter_string)   
   dw_clearing_wo_control.Filter()   

  
   // Calculate the (or get the input) clearing rate for all of the clearing overheads
   for j = 1 to dw_clearing_wo_control.rowcount()
   
      wo_id       = dw_clearing_wo_control.getitemnumber(j,'work_order_id')
      clear_id    = dw_clearing_wo_control.getitemnumber(j,'clearing_id')
      
      dept        = dw_clearing_wo_control.getitemnumber(j,'department_id')
      dept_clear = dept
      clear_type  = dw_clearing_wo_control.getitemnumber(j,'clearing_indicator')
      credit_acct = dw_clearing_wo_control.getitemnumber(j,'credit_gl_account')
      description = dw_clearing_wo_control.getitemstring(j,'description')
      je_code      = dw_clearing_wo_control.getitemstring(j,'clearing_je')
      id_num      = string(clear_id)
      rate        = 0
      
      if i > 1 then
         //   we're past the first priority... rebuild the summaries for the current wo_id
         //   *****   wo_summary
         if not isnull(wo_id) then
            rtn   =   f_update_wo_summary_month_wo(wo_id, month_number)
            if rtn = - 1 then 
               f_pp_msgs("ERROR: updating wo_summary_wo in Overhead Calculation...~r~r"   &      
                        +   "Oracle Information: " + sqlca.sqlerrtext)
               rollback;
               goto doReturn
            else
               commit;
            end if
         end if
      end if
      
      
      account_type_id = 0
      select account_type_id into :account_type_id
        from gl_account where gl_account_id = :credit_acct;
      if isnull(account_type_id) then account_type_id = 0
      
   f_pp_msgs("Calc. Overheads and AFUDC - Clearing Overhead : " + description + " " + string(now()))

	if j = 1 or mod(j, 10) = 0 then
		f_pp_msgs(  " Clearing Overhead : " + string(i) + "/" + string(dw_clearing_wo_control.rowcount())  + " " + string(now())) 
	end if

      dw_name = dw_clearing_wo_control.getitemstring(j, 'dw_name')
      
      mult_expend       = false
      mult_expend_from  = false


      if not isnull(dw_name) then

         dw_clearing_base.DataObject = dw_name
      else 
         dw_clearing_base.DataObject = 'dw_clearing_base' 
      end if 

      dw_clearing_base.SetTransObject(sqlca)   

      clear_rows = dw_clearing_base.retrieve(i_nvo_wo_control.i_month_number, clear_id, i_nvo_wo_control.i_company)
      rate_rows =  dw_clearing_wo_rate.retrieve(month_string, clear_id)
      
		//maint 44301
		if clear_rows < 0 then 
			f_pp_msgs('ERROR: Error retrieving the datawindow for clearing_id = ' + string(clear_id) + '.' )
			f_pp_msgs('ERROR: UNABLE TO CALCULATE THIS OVERHEAD!!!' )
			rollback;
			goto doReturn
		end if 
		
		if clear_rows >= 0 then 
			if not isnull(dw_name) then
				f_pp_msgs(' dw_name = ' + dw_name)
			end if 
			f_pp_msgs(' retrieved ' + string(clear_rows) + ' work orders for the overhead.' )
		end if
	
		if clear_rows = 0 then 
			//nothing qualifying
			continue
		end if
   
      ce_exists = false
      acct_exists = false
      dept_exists = false
      task_exists = false
      ru_exists   = false
      //JRD 10-11-04
      al_exists = false
      sn_exists = false
      
       //LLB 7/31/2007
        sa_exists = false

      //JRD 05/13/08         
      ref_num_exists = false

   
      num_cols =   integer(dw_clearing_base.describe("datawindow.column.count"))

      for col  = 1 to num_cols
         col_name = dw_clearing_base.describe( "#"+string(col) + ".name")
          
         choose case col_name
               
            case 'cost_element_id'
               ce_exists = true
            case 'utility_account_id'
               acct_exists = true
            case 'department_id'
               dept_exists = true
            case 'job_task_id', 'job_task'
               task_exists = true
            case 'retirement_unit_id'
               ru_exists = true
         //JRD 10-11-04
            case 'asset_location_id'
               al_exists = true
            case 'serial_number'
               sn_exists = true
          //LLB 7/31/2007
                  case 'sub_account_id'
                     sa_exists = true

            //JRD 05/13/08         
                case 'reference_number'
                  ref_num_exists = true

         end choose
      next
   
      expend_id   = dw_clearing_base.getitemnumber(1,'expenditure_type_id')
      
      if expend_id <> dw_clearing_base.getitemnumber(1,'mult_expend') then
         mult_expend = true
      end if
      
      select bus_segment_id into :bus_seg 
      from work_order_control
      where work_order_id = :wo_id;
      
      
      //  clear_type = 1 indicates a 'clearing' overhead, where a work order will be 
      //                 cleared to others.
      //  clear_type = 2 indicates a 'loading' overhead, where charges are multiplied by 
      //                  an input rate to calculate another charge.

      //  The balance will have been rolled forward ...

      choose case account_type_id 
         case 3
            //  This is a removal overhead ...
            cs_expend_id = 2
         case 5, 6
            //  This is an expense overhead ...
            cs_expend_id = 3
         case 4
            //  This is a jobbing overhead ...
            cs_expend_id = 4
         case else
            //  This is a construction overhead ...
            cs_expend_id = 1
      end choose

      //jrh 20062509 - "Target Loadings" has same logic as "Loadings"
      if clear_type = 3 then clear_type = 2
   
      if clear_type = 1 then 
         if isnull(wo_id) then
               f_pp_msgs( 'There is no work order defined for the "' + description + &
                       '" overhead.~n~nThis overhead WILL NOT be calculated.')
            continue
         end if

			select count(expenditure_type_id) into :rtn from cwip_charge
			where month_number = :month_number and work_order_id = :wo_id ;

         if rtn > 1 then
            mult_expend_from = true             
         end if
			
			//Maint 8054:  stop using charge_summary.  use cwip_charge instead.
			clearing_balance = 0
			select sum(amount) into :clearing_balance 
			from cwip_charge
			where charge_type_id in (select charge_type_id from charge_type where processing_type_id not in (1,5) ) 
			and (closed_month_number is null or closed_month_number > :month_number)
			and month_number <= :month_number
			and work_order_id = :wo_id
			and expenditure_type_id = :cs_expend_id;
			
      end if


      if dw_clearing_wo_control.getitemnumber(j,'input_rate') = 1 then
         //  If dw_clearing_wo_rate did not return any rows, die gracefully.
         if rate_rows < 1 then
         	f_pp_msgs(  "Cannot find records in clearing_wo_rate for: " + description)
            goto doReturn
         end if
         rate_is_input = true
         rate =  dw_clearing_wo_rate.getitemdecimal(1,'input_rate')
         
         //  The rate may be 0 in some cases ... some clients user the OH logic
         //  to split an OH work order to other OH work orders based on an input
         //  rate (VA COOPS).  Sometimes the rate may look like 100% / 0%.
         //  Need to skip this OH if the rate is 0 (set the calc_rate field 1st).
         //  The dw_clearing_base DW is not smart enough to handle an input_rate = 0.
         //  In the DW, 0 means "calculated rate".
         if rate = 0 then
            dw_clearing_wo_rate.SetItem(1, "calc_rate", 0)
            continue
         end if
         
         dw_clearing_base.setitem(1,'input_rate',rate)
      else
         rate_is_input = false
         dw_clearing_wo_rate.insertrow(0)
         dw_clearing_wo_rate.setitem(1, 'accounting_month', i_nvo_wo_control.i_month)
         dw_clearing_wo_rate.setitem(1, 'clearing_id',      clear_id)
         dw_clearing_wo_rate.setitem(1, 'calc_rate',        rate)
         dw_clearing_base.setitem(1,    'clearing_balance', clearing_balance)
      end if

      //  Need to save the rate for the month ...
      if clear_type = 1 then
         dw_clearing_base.setitem(1,'clearing_balance',clearing_balance)
         rate = dw_clearing_base.getitemdecimal(1,'calc_rate')      
         dw_clearing_wo_rate.setitem(1,'calc_rate',rate)
      end if

      dw_clearing_base.GroupCalc()

      if rate = 0 then continue


      //  Apply the rate across the work orders which receive this overhead ...

      //  All of the Work Orders are in the dw_clearing_base table, the 'cleared_amount' 
      //  column ...
      //  holds the amount to enter as a charge in the cwip_charge table.
      //  The charge_type is dw_clearing_wo_control table, charge_type_id column
      //  A negative charge should be created for the clearing work order in the same 
      //  charge_type_id for a 'clearing' type of overhead.
      oh_charge_type = dw_clearing_wo_control.getitemnumber(j,'charge_type_id')
      cost_element   = dw_clearing_wo_control.getitemnumber(j,'cost_element_id')

      total_cleared  = 0
      diff           = 0

      for k = 1 to clear_rows
         
         clear_wo_id = dw_clearing_base.getitemnumber(k,'work_order_id')
         
         if mult_expend then
            expend_id = dw_clearing_base.getitemnumber(k,'expenditure_type_id')
         end if
         
         check_for_gl_acct = 'false'
         check_for_gl_acct = lower(dw_clearing_base.describe("use_gl_account_id.ColType"))
         if left(check_for_gl_acct,4) = 'deci' or left(check_for_gl_acct,4) = 'numb' then
            gl_acct = dw_clearing_base.getitemnumber(k,'use_gl_account_id')
         else
            
            if expend_id = 1 then
               select cwip_gl_account into :gl_acct 
                 from work_order_account where work_order_id = :clear_wo_id;
            end if
                  
            if expend_id = 2 then
               select removal_gl_account into :gl_acct 
                 from work_order_account where work_order_id = :clear_wo_id;
            end if
            
            if expend_id = 3 then
               select expense_gl_account into :gl_acct 
                 from work_order_account where work_order_id = :clear_wo_id;
            end if
            
            if expend_id = 4 then
               select jobbing_gl_account into :gl_acct 
                 from work_order_account where work_order_id = :clear_wo_id;
            end if
         end if

         if clear_type = 1 and rate_is_input then
            oh_charge = dw_clearing_base.GetItemNumber(k, "cleared_amount")
         else
            //  If the basis for all work orders is negative, the 
            //  cleared_amount_wo_negatives and remainder_clearing will all be 0.
            //  Use cleared_amount instead.
            sum_abs_initial_clearing   = &
               dw_clearing_base.GetItemNumber(k, "sum_abs_initial_clearing")
            sum_abs_remainder_clearing = &
               dw_clearing_base.GetItemNumber(k, "sum_abs_remainder_clearing")
            if sum_abs_initial_clearing = 0 and sum_abs_remainder_clearing = 0 then
               oh_charge = dw_clearing_base.GetItemNumber(k, "cleared_amount")
            else
               oh_charge = dw_clearing_base.GetItemNumber(k, "cleared_amount_wo_negatives") &
                         + dw_clearing_base.GetItemNumber(k, "remainder_clearing")
            end if
         end if
         
         //  If this is "clearing" overhead and using a calculated rate, keep track
         //  of the amount cleared so we can fix rounding later ...
         if clear_type = 1 and not rate_is_input then
            total_cleared = total_cleared + oh_charge
         end if
         
         if oh_charge <> 0 then

            charge_id = f_topkey_no_dw('cwip_charge','charge_id')
         
            if acct_exists then 
               acct = dw_clearing_base.getitemnumber(k,'utility_account_id')
            else
               setnull(acct)
            end if
            
         //LLB - 7/31/2007
            if sa_exists then 
               sa_id = dw_clearing_base.getitemnumber(k,'sub_account_id')
            else
               setnull(sa_id)
            end if            


            if ce_exists then 
               cost_element = dw_clearing_base.getitemnumber(k,'cost_element_id')
               
               //  Find Charge_type_id from Cost_element_ID:
    
               find_string = "cost_element_id = " + string(cost_element) 
               find_row    =  ds_cost_element.Find(find_string, 1,    ce_rows)
               oh_charge_type = ds_cost_element.getitemnumber(find_row,'charge_type_id')               
               
            end if

            proc_type = 0
         
            select processing_type_id into :proc_type from charge_type
            where charge_type_id = :oh_charge_type;

            if proc_type = 5 then gl_acct = 1
            
            
            ovh_dept = f_pp_system_control_company('Clearing Ovh-Dept from Clearing Wo', i_nvo_wo_control.i_company) 
            
            if ovh_dept = 'no' then
             
               if dept_exists then 
                  dept = dw_clearing_base.getitemnumber(k,'department_id')
               end if
               
            end if
            
            if task_exists then 
               task = dw_clearing_base.getitemstring(k,'job_task_id') 
            else
               setnull(task)
            end if
            //JRD 10-11-04
            if al_exists then 
               al = dw_clearing_base.getitemnumber(k,'asset_location_id')
            else
               setnull(al)
            end if
            if sn_exists then 
               sn = dw_clearing_base.getitemstring(k,'serial_number')
            else
               setnull(sn)
            end if
            
            if ru_exists then
               ru_id = dw_clearing_base.GetItemNumber(k, 'retirement_unit_id')
            else
               setnull(ru_id)
            end if


            //JRD 05/13/08         
            if ref_num_exists then
               ref_number = dw_clearing_base.getitemstring(k,'reference_number')
            else
               setnull(ref_number)
            end if

            setnull(ext_gl_acct)
			
				if cwip_external_account_option = 'yes'  then
					select external_account_code into :ext_gl_acct
					from gl_account
					where gl_account_id = :gl_acct;
				else
					 ext_gl_acct = f_autogen_je_account(dw_clearing_base, k, 5, 0, 1)  /*maint-39126*/
					//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
					if mid(ext_gl_acct,1,5) = 'ERROR' then
						f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + ext_gl_acct)
						rollback;
						destroy dw_clearing_wo_control
						destroy dw_clearing_base
						destroy dw_clearing_wo_rate
						destroy dw_overhead_work_orders
						destroy ds_cost_element
						destroy dw_priority
						return FALSE
					end if
					
				end if

            select bus_segment_id into :bus_seg_clear from work_order_control
             where work_order_id = :clear_wo_id;
            //JRD 10-11-04       //JRD 10-11-04; LLB 7/31/2007
            INSERT into cwip_charge (charge_id, expenditure_type_id, work_order_id, 
               charge_type_id, job_task_id, charge_mo_yr, description, quantity, amount, 
               hours, payment_date, department_id, cost_element_id, utility_account_id, 
               id_number, gl_account_id,   charge_audit_id, notes, month_number,
               company_id, external_gl_account, status, bus_segment_id, journal_code,
               retirement_unit_id, asset_location_Id, serial_number, sub_account_id, reference_number)  //JRD 05/13/08 - add ref number
            VALUES (:charge_id, :expend_id, :clear_wo_id, :oh_charge_type, :task, 
                    :gl_posting_mo_yr, :description, :quantity, :oh_charge, :hours, 
                    :gl_posting_mo_yr, :dept, :cost_element, :acct, :id_num, :gl_acct, 
                    :charge_id, 'Clearing Entry', :month_number, :i_nvo_wo_control.i_company,
                    :ext_gl_acct,:clear_id, :bus_seg_clear, :je_code, 
                    :ru_id, :al, :sn, :sa_id,:ref_number);  //JRD 05/13/08 - add ref number
            
            f_check_sql_error(sqlca, 'Error Creating Overhead Charges')
         
         end if  // end if oh_charge <> 0 then
         
      next  //  for k = 1 to clear_rows ...
      

      //  Check for rounding ...
      if clear_type = 1 and not rate_is_input then
            diff = clearing_balance - total_cleared
            
            if diff <> 0 then
               update cwip_charge set amount = amount + :diff
                where charge_id = :charge_id;
                
               if sqlca.SQLCode <> 0 then
                  f_pp_msgs( "Cannot adjust for rounding ... ~n" + &
                              sqlca.SQLErrText)
                  rollback;
                  goto doReturn
               end if
            end if
      end if

      if clear_type = 1 then

         charge_id = f_topkey_no_dw('cwip_charge','charge_id')
         oh_charge = -dw_clearing_base.getitemdecimal(1,'total_cleared_amount')
            
         //  Make sure the credit reflects the rounding adjustment ...
         if not rate_is_input then            
            oh_charge =  total_cleared + diff
            oh_charge = -oh_charge
         end if
      
         // utility account for the credit should be null ...
         setnull(acct)

         setnull(ext_gl_acct)
			
			if cwip_external_account_option = 'yes'  then
					select external_account_code into :ext_gl_acct
					from gl_account
					where gl_account_id = :credit_acct;
			else	
					ext_gl_acct = f_autogen_je_account(dw_clearing_wo_control, j, 5, 1, 1)  /*maint-39126*/
					//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
					if mid(ext_gl_acct,1,5) = 'ERROR' then
						f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + ext_gl_acct)
						rollback;
						destroy dw_clearing_wo_control
						destroy dw_clearing_base
						destroy dw_clearing_wo_rate
						destroy dw_overhead_work_orders
						destroy ds_cost_element
						destroy dw_priority
						return FALSE
					end if
			end if

         INSERT into cwip_charge 
            (charge_id, expenditure_type_id, work_order_id, charge_type_id, job_task_id,
            charge_mo_yr, description, quantity, amount, hours, payment_date, 
            department_id, cost_element_id, utility_account_id, id_number, gl_account_id,   
            charge_audit_id, month_number, company_id, external_gl_account, status, 
            bus_segment_id, journal_code) 
         VALUES 
            (:charge_id, :cs_expend_id, :wo_id, :oh_charge_type, :clear_id, 
             :gl_posting_mo_yr, :description, :quantity, :oh_charge, :hours, :gl_posting_mo_yr,
             :dept_clear, :cost_element, :acct, :id_num, :credit_acct, 
             :charge_id, :month_number, :i_nvo_wo_control.i_company, :ext_gl_acct, :clear_id, 
             :bus_seg, :je_code);
         
         f_check_sql_error(sqlca, 'Error Creating Overhead Charges')

      end if  //  if clear_type = 1 then ...


      rtn = dw_clearing_wo_rate.Update()
      
      if rtn = 1 then
         commit;
      else
         f_pp_msgs("Calc. Overheads and AFUDC - Unable to update the Clearing WO Rate table.~n" + &
                     dw_clearing_wo_rate.i_sqlca_sqlerrtext)   
				f_pp_msgs( "Cannot adjust for rounding ... ~n" + &
				sqlca.SQLErrText)
         rollback;
         goto doReturn
      end if
      dw_clearing_base.reset()
   
   next  // end of the j loop
   dw_clearing_wo_control.Setfilter("")   
   dw_clearing_wo_control.Filter()   
next // end of the i loop

destroy  dw_clearing_wo_control
destroy dw_clearing_base
destroy dw_clearing_wo_rate
destroy dw_overhead_work_orders
destroy ds_cost_element

// AMP FIX MEMORY LEAK
doReturn:

destroy dw_clearing_wo_control
destroy dw_clearing_base
destroy dw_clearing_wo_rate
destroy dw_overhead_work_orders
destroy ds_cost_element;
if isValid(dw_priority) then
   destroy dw_priority;
end if
return true



end function

public function boolean uf_calc_oh_afudc_wip ();//**********************************************************************************************************************************************************
//
// uf_calc_oh_afudc_wip()
// 
// The main workflow for the Interface.  Calls all the necessary functions to calculate Overheads,
//   AFUDC and WIP
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

longlong rowcount, row, ret, temp_service_month, temp_service_year, temp_month, &
		  temp_year, afudc_ind, cpi_ind, afc_elig_ind, cpi_elig_ind, &
		  afudc_triggers, cpi_triggers, m, last_compound_mn, last_mn, &
		  last_compound_mn_cpi, cap_int_rows, orig_mo_num, split_check, company, &
		  min_afc_months, min_cpi_months
longlong i, afudc_type_id, rows, clear_wo_id, wo_id, charge_type, charge_id, &
	     expenditure_type_id, month_number, equity_charge_type, debt_charge_type, &
		  cpi_charge_type, afudc_row, quantity, hours, equity_ce, debt_ce, cpi_ce, &
		  in_service_day, last_day, afudc_dept_id, bus_segment_id, gl_account_id, je_id, &
		  rtn, max_open_month, prior_debt_ce, prior_equity_ce, prior_cpi_ce, allow_neg_cpi, &
		  afudc_status, cpi_status, allow_neg_afudc
decimal{2} wo_amount, afudc_elig_amount, cpi_elig_amount, debt_charge, equity_charge, &
		     cpi_charge, last_month_afudc_base, current_afudc_base, entire_afudc_base, &
			  half_month_cpi, half_month_afudc, afudc_compound_amt, cpi_compound_amt, &
			  afudc_compound_base_amt, cpi_compound_base_amt, total_base, &
			  last_month_cpi_base, current_cpi_base, entire_cpi_base, debt_adj, equity_adj, &
			  cpi_adj, first_month_debt, first_month_equity, first_month_cpi, &
			  first_month_debt_adj, first_month_equity_adj, first_month_cpi_adj, &
			  input_afudc_debt, input_afudc_equity, input_cpi, input_base_adjust, &
			  afudc_base_used_in_calc, cpi_base_used_in_calc, &
			  half_mo_isd_adjust_eq, half_mo_isd_adjust_db, half_mo_isd_adjust_cpi, &
			  total_cap_int, total_int, cap_int_adjust, int_adjust, afudc_act_amt, min_afudc_amt, &
			  cpi_act_amt, min_cpi_amt, cpi_est_amt
datetime gl_posting_mo_yr, afudc_start_date, afudc_stop_date, payment_date, &
			approved_date,  months[], in_service_date, last_day_date, calc_date, orig_isd, check_closed
decimal{8} equity_rate, debt_rate, cpi_rate, half_month, percent_est_for_cpi
decimal{12} int_adjust_ratio, afudc_ratio, cpi_ratio
string description, find_str, month_str, column_str, ext_gl_acct, je_code, wo_process_ans, &
       estimate_cv, current_mo_str, isd_cv, text1, text2, estimate_req, comp_descr
boolean ignore_current_month, cap_int, parent_co, writelog
////	added for company loop
longlong	c, num_companies, num_array[], process_id, occurrence_id, company_array[], repair_schema_id_array_cwip[]
uo_ds_top ds_users
boolean vfy_users
string  sqls,  user_id, process_msg
//// $$$ mtc 20090804
longlong counter

longlong ndx
boolean cbx_calc_ovh_before_checked
boolean cbx_calc_afudc_checked
boolean cbx_calc_ovh_after_checked
boolean cbx_calc_ovh_before_enabled
boolean cbx_calc_afudc_enabled

i_nvo_wo_control.of_constructor()


// these parameters originate from the w_wo_control window
i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_wo_control.i_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_wo_control.i_month_number = year(date(g_ssp_parms.date_arg[1])) * 100 + month(date(g_ssp_parms.date_arg[1]))

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("Calc Overheads and AFUDC - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

// Get the descriptions
rtn = i_nvo_wo_control.of_getDescriptionsFromIds(i_nvo_wo_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

f_pp_msgs("Calc Overheads and AFUDC")
f_pp_msgs("Attempting to Calc Overheads and AFUDC for: ")
f_pp_msgs("Month: " + string(i_nvo_wo_control.i_month, "mm/dd/yyyy"))
for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	f_pp_msgs("Company ID: " + string(i_nvo_wo_control.i_company_idx[ndx]) + ", Company Name: " + i_nvo_wo_control.i_company_descr[ndx])
next

i_nvo_wo_control.i_afudc_error = false
i_nvo_wo_control.i_no_summary = false

// Check if last month is closed 
num_companies	=	upperbound(i_nvo_wo_control.i_company_idx)

for c=1 to num_companies
	i_nvo_wo_control.i_company = i_nvo_wo_control.i_company_idx[c]

	select powerplant_closed into :check_closed
	from wo_process_control 
	where accounting_month = add_months(:i_nvo_wo_control.i_month,-1) and
	company_id = :i_nvo_wo_control.i_company;
	
	if isnull(check_closed) then
		f_pp_msgs("Cannot calculate Overheads and AFUDC until the previous month has been closed on the Project side " + &
				" for all companies selected")
		return false
	end if
		
	split_check = 0
	select count(*) into :split_check
	from clearing_wo_control
	where  before_afudc_indicator  is null
	and company_id = :i_nvo_wo_control.i_company;
	
	if split_check > 0 then
		f_pp_msgs("The before_afudc_indicator in the clearing_wo_control table is not " + &
				  " set for " + string(i_nvo_wo_control.i_company))
		return false	
	end if
	
	// Make sure this month isn't closed
	setnull(check_closed)
	select powerplant_closed into :check_closed
	from wo_process_control 
	where accounting_month = :i_nvo_wo_control.i_month and
	company_id = :i_nvo_wo_control.i_company;
	
	if not isnull(check_closed) then
		f_pp_msgs("This month is closed.")
		return false
	end if
	
next

// check if the multiple companies chosen can be processed  together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if


// set cpr_act_month month again, in case they went to another window that changed cpr_act_month,
// and then came back 
i_nvo_wo_control.of_currentmonth(i_nvo_wo_control.i_month)

month_number = year(date(i_nvo_wo_control.i_month)) * 100 + month(date(i_nvo_wo_control.i_month))

if not i_nvo_wo_control.i_split_oh_afudc then
	
	// SEB: Moving loop
	num_companies = upperbound(i_nvo_wo_control.i_company_idx)
	for c=1 to num_companies
	
		i_nvo_wo_control.of_companychanged(c,i_nvo_wo_control.i_month)
		
		f_pp_msgs("Calculating Overheads and AFUDC for Company ID: " + string(i_nvo_wo_control.i_company) + ", Company Name: " + i_nvo_wo_control.i_company_descr[c])

		//  Get pp_system_control variable to determine whether or not to calculate
		//  Afudc, Overheads, or Both ...
	
		wo_process_ans = f_pp_system_control_company("Work Order Calculation",i_nvo_wo_control.i_company)
	
		if isnull(wo_process_ans) or wo_process_ans = "" then
			wo_process_ans = "afudc/overheads"
		end if
		
		CHOOSE CASE wo_process_ans
			CASE 'afudc'
				f_pp_msgs("Starting AFUDC calculation ... " + string(now()))
			CASE 'overheads'
				f_pp_msgs("Starting Overhead Calculation. " + string(now()))
			CASE 'none'
				f_pp_msgs("Skipping OH and AFUDC calculation. " + string(now()))
			CASE ELSE
				f_pp_msgs("Starting Overhead/AFUDC Calculation. " + string(now()))
		END CHOOSE
		
		if wo_process_ans = 'none' then
			uf_none_calc(i_nvo_wo_control.i_month)
			goto overhead_calc
		end if
		
		if (wo_process_ans =  'overheads' or wo_process_ans = "afudc/overheads") then
			uf_before_oh_calc(i_nvo_wo_control.i_month)
			if wo_process_ans ='overheads' then goto overhead_calc_after
			if i_nvo_wo_control.i_afudc_error then goto done
		end if
	
		uf_afudc_real_calc(i_nvo_wo_control.i_month)
		if i_nvo_wo_control.i_afudc_error then goto done
		
		overhead_calc_after:
		
		if (wo_process_ans =  'overheads' or wo_process_ans = "afudc/overheads") then
			uf_after_oh_calc(i_nvo_wo_control.i_month)
		end if
	  
		DONE:
		
		if not i_nvo_wo_control.i_no_summary then 
				i_nvo_wo_control.of_wo_summaries()
		end if
	
		overhead_calc:
		
		commit;

	next
else
	i_nvo_wo_control.i_calc  = true
	i_nvo_wo_control.i_approve = false
	
	// make sure PowerPlant passed in values corresponding to the check boxes
	// on the AFUDC window
	if upperbound(g_ssp_parms.boolean_arg) = 1 and &
		upperbound(g_ssp_parms.boolean_arg2) = 1 and &
		upperbound(g_ssp_parms.boolean_arg3) = 1 and &
		upperbound(g_ssp_parms.boolean_arg4) = 1 and &
		upperbound(g_ssp_parms.boolean_arg5) = 1 then
		
		// grab the checked values that originate from the w_wo_afudc_ovh_control window
		cbx_calc_ovh_before_checked = g_ssp_parms.boolean_arg[1]
		cbx_calc_afudc_checked = g_ssp_parms.boolean_arg2[1]
		cbx_calc_ovh_after_checked = g_ssp_parms.boolean_arg3[1]
		cbx_calc_ovh_before_enabled = g_ssp_parms.boolean_arg4[1]
		cbx_calc_afudc_enabled = g_ssp_parms.boolean_arg5[1]
		uf_wo_afudc_ovh_control(cbx_calc_ovh_before_checked, cbx_calc_afudc_checked, cbx_calc_ovh_after_checked, cbx_calc_ovh_before_enabled, cbx_calc_afudc_enabled, datetime(g_ssp_parms.date_arg[1]))
		if i_nvo_wo_control.i_afudc_error then return false
	else
		f_pp_msgs("ERROR: Not enough parameters passed into interface")
		return false
	end if
end if

i_nvo_wo_control.of_cleanup( 2, 'email wo close: calc overheads and afudc', "Calc Overheads and AFUDC")

if i_nvo_wo_control.i_afudc_error then
	return false
elseif i_nvo_wo_control.i_oh_error then
	return false
else
	return true
end if

end function

public function boolean uf_retrieve_dates ();//**********************************************************************************************************************************************************
//
// uf_retrieve_dates()
// 
// The function emulates the wf_retrieve_dates from the w_wo_afudc_ovh_control window
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

i_nvo_wo_control.i_ds_wo_process_afudc_ovh.retrieve(i_nvo_wo_control.i_company,i_nvo_wo_control.i_month_number)

i_calc_ovh_before = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'before_oh_calc'))
i_calc_ovh_after   = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'after_oh_calc'))
i_calc_afudc       = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'afudc_calc'))
i_appr_ovh_before  = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'before_oh_approval'))
i_appr_ovh_after   = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'after_oh_approval'))
i_appr_afudc       = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'afudc_approval'))

return true
end function

public function boolean uf_after_oh_calc (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_after_oh_calc()
// 
// This function must be executed after Overheads are calculated
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

longlong rowcount, row, ret, wo_accr_ct, check_count 
longlong i, rows, clear_wo_id, wo_id,rtn, max_open_month, month_number,m 
datetime		approved_date
  
longlong	num_companies

f_pp_msgs("Calc. Overheads and AFUDC - Calculating Overheads After AFUDC" + ' ' + string(now()))

approved_date = i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1, 'overhead_approval')
if not isnull(approved_date) then
	f_pp_msgs( "Overheads and AFUDC have already been approved.")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	goto doReturn
end if

approved_date = i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1, 'after_oh_approval')
if not isnull(approved_date) then
	f_pp_msgs("After Overheads have already been approved.")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	goto doReturn
end if

month_number = year(date(i_nvo_wo_control.i_month)) * 100 + month(date(i_nvo_wo_control.i_month))

// i_nvo_wo_control.i_company is set in i_nvo_wo_control.of_companychanged(c)
f_pp_msgs("Calc. Overheads after AFUDC Month: " + string(month_number) + &
 " Company ID: " + string(i_nvo_wo_control.i_company) + ' ' + string(now()))

//Maint 9754 ###SJH  3/27/2012 add validation for status column
check_count = 0
select count(*) into :check_count
from wip_computation 
where wip_computation_id in (select clearing_id from clearing_wo_control where before_afudc_indicator = 0)
and company_id_from = :i_nvo_wo_control.i_company;
			
if check_count > 0 then
	f_pp_msgs( "ERROR: After Overhead clearing_id numbers cannot be the same as wip_computation_id numbers!" + &
				sqlca.SQLErrText)
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
end if

check_count = 0
select count(*) into :check_count
from repair_schema 
where repair_schema_id in (select clearing_id from clearing_wo_control where before_afudc_indicator = 0);
			
if check_count > 0 then
	f_pp_msgs( "ERROR: After Overhead clearing_id numbers cannot be the same as repair_schema_id numbers!" + &
				sqlca.SQLErrText)
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
end if 

check_count = 0
select count(*) into :check_count
from clearing_wo_control 
where before_afudc_indicator = 0 and clearing_id in (1, 8, 9, -99, -100)
and nvl(company_id, -1) in (-1, :i_nvo_wo_control.i_company);
			
if check_count > 0 then
	f_pp_msgs( "ERROR: After Overhead clearing_id numbers cannot be 1, 8, 9, -99, -100!" + &
				sqlca.SQLErrText)
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
end if 

wo_accr_ct = long(f_pp_system_control_company("Work Order Accrual Charge Type",i_nvo_wo_control.i_company))	
delete from cwip_charge 
where month_number = :month_number and status in 
			(select clearing_id from clearing_wo_control
			 where before_afudc_indicator = 0)
and company_id = :i_nvo_wo_control.i_company
and charge_type_id <> :wo_accr_ct;
 
if sqlca.SQLCode < 0 then
	f_pp_msgs( "ERROR: deleting cwip_charge after OH (big delete) ...~n~n" + &
				sqlca.SQLErrText)
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
else
	commit;	
end if

f_pp_msgs("Calc. Overheads after AFUDC - Updating Work Order Summary Prior to After OH Calc"+ ' ' + string(now()))

rtn = f_update_wo_summary_month(i_nvo_wo_control.i_company, month_number)

if rtn = -1 then
	f_pp_msgs( "ERROR: updating wo_summary in cb_before_oh")
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
else
	commit;
end if
	
//  Rebuild wo_summary for clearing work orders ... 

f_pp_msgs("Calc. Overheads after AFUDC - Rebuilding Summaries for Clearing Wos"+ ' ' + string(now()))

uo_ds_top dw_clearing_wo_control1
dw_clearing_wo_control1 = CREATE uo_ds_top
dw_clearing_wo_control1.DataObject = "dw_clearing_wo_control"
dw_clearing_wo_control1.SetTransObject(sqlca)	
f_add_where_clause_ds(dw_clearing_wo_control1, "", " and before_afudc_indicator = 0 ",false)
dw_clearing_wo_control1.RETRIEVE(i_nvo_wo_control.i_company)
	
for i = 1 to dw_clearing_wo_control1.RowCount()
		
	clear_wo_id = dw_clearing_wo_control1.GetItemNumber(i, 'work_order_id')
	
	f_set_large_rollback_segment()
		
	if not isnull(clear_wo_id) then
		ret = f_update_wo_summary_month_wo(clear_wo_id, month_number) 
		
		if ret = -1 then
			f_pp_msgs( "ERROR: updating wo_summary for clearing work orders ")
			rollback;
			i_nvo_wo_control.i_afudc_error = true
			goto doReturn
		else
			commit;
		end if	
		
	end if	
next

// Begin the Overhead calculation.
f_pp_msgs("Calc. Overheads after AFUDC - Calculate Overheads After AFUDC. "+ ' ' + string(now()))

i_nvo_wo_control.i_oh_error = false
i_nvo_wo_control.i_ovh_type = 0
uf_overhead_calc()
if i_nvo_wo_control.i_oh_error then
	goto doReturn
end if

if not i_nvo_wo_control.i_split_oh_afudc then
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation',today()) 
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'after_oh_calc', today())	
	i_nvo_wo_control.of_updatedwnocommit( )
else
	//  date updated in the calling script
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'after_oh_calc', today())
	i_nvo_wo_control.of_updatedwnocommit( )
end if

//// AMP FIXED MEM LEAK 04/11/08
doReturn:
destroy dw_clearing_wo_control1;

return true
end function

public function boolean uf_before_oh_calc (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_before_oh_calc()
// 
// This function must be executed before Overheads are calculated
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************


longlong rowcount, row, ret, wo_accr_ct, check_count
longlong i, rows, clear_wo_id, wo_id,rtn, max_open_month, month_number,m 
datetime		approved_date
longlong num_companies

f_pp_msgs("Calc. Overheads and AFUDC")
f_pp_msgs("Calc. Overheads and AFUDC - Calculating Overheads Before AFUDC " + string(now()))

// START THE CALCULATION
approved_date = i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1, 'overhead_approval')
if not isnull(approved_date) then
	f_pp_msgs("Overheads and AFUDC have already been approved.")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	goto doReturn
end if

approved_date = i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1, 'before_oh_approval')
if not isnull(approved_date) then
	f_pp_msgs("Before Overheads have already been approved.")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	goto doReturn
end if


month_number = year(date(i_nvo_wo_control.i_month)) * 100 + month(date(i_nvo_wo_control.i_month))

f_pp_msgs("Calc. Overheads and AFUDC - Start OH Calc before AFUDC"+ ' ' + string(now()))

// i_nvo_wo_control.i_company was set by the call to i_nvo_wo_control.of_companychanged(c)
f_pp_msgs( "Calculating Overheads Before AFUDC  Month: " + string(month_number) + &
 " Company ID: " + string(i_nvo_wo_control.i_company) + ' ' + string(now()))


//Maint 9754 ###SJH  3/27/2012 add validation for status column
check_count = 0
select count(*) into :check_count
from wip_computation 
where wip_computation_id in (select clearing_id from clearing_wo_control where before_afudc_indicator = 1)
and company_id_from = :i_nvo_wo_control.i_company;
			
if check_count > 0 then

		f_pp_msgs( "ERROR: Overhead clearing_id numbers cannot be the same as wip_computation_id numbers!" + &
					sqlca.SQLErrText)

	rollback;

	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
end if 

check_count = 0
select count(*) into :check_count
from repair_schema 
where repair_schema_id in (select clearing_id from clearing_wo_control where before_afudc_indicator = 1);
			
if check_count > 0 then
	f_pp_msgs( "ERROR: Overhead clearing_id numbers cannot be the same as repair_schema_id numbers!" + &
					sqlca.SQLErrText)
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
end if 

check_count = 0
select count(*) into :check_count
from clearing_wo_control 
where before_afudc_indicator = 1 and clearing_id in (1, 8, 9, -99, -100)
and nvl(company_id, -1) in (-1, :i_nvo_wo_control.i_company);
			
if check_count > 0 then
	f_pp_msgs( "ERROR: Overhead clearing_id numbers cannot be 1, 8, 9, -99, -100!" + &
					sqlca.SQLErrText)
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
end if 

wo_accr_ct = long(f_pp_system_control_company("Work Order Accrual Charge Type", i_nvo_wo_control.i_company))	

//Maint 9754 ###SJH 3/27/2012  "status is not null" is bad
delete from cwip_charge 
where month_number = :month_number and status in 
			(select clearing_id from clearing_wo_control
			 where before_afudc_indicator = 1)
and company_id = :i_nvo_wo_control.i_company
and charge_type_id <> :wo_accr_ct;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: deleting cwip_charge overheads (big delete) ...~n~n" + &
					sqlca.SQLErrText)
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
else
	commit;	
end if
	
if i_nvo_wo_control.i_split_oh_afudc then
	f_pp_msgs("Calc. Overheads and AFUDC - Updating Work Order Summary Prior to Before OH Calc"+ ' ' + string(now()))
else
	f_pp_msgs("Calc. Overheads and AFUDC - Updating Work Order Summary Prior to OH Calc"+ ' ' + string(now()))
end if

f_pp_msgs("Calc. Overheads and AFUDC - Company ID: " + string(i_nvo_wo_control.i_company) + ' ' + string(now()))
	  
rtn = f_update_wo_summary_month(i_nvo_wo_control.i_company, month_number)
	
if rtn = -1 then
	f_pp_msgs("ERROR: updating wo_summary in cb_before_oh...")
	rollback;
	i_nvo_wo_control.i_afudc_error = true
	goto doReturn
else
	commit;
end if

//  Rebuild wo_summary for clearing work orders ...
f_pp_msgs("Calc. Overheads and AFUDC - Rebuilding Summaries  for Clearing Wos"+ ' ' + string(now()))

uo_ds_top dw_clearing_wo_control1
dw_clearing_wo_control1 = CREATE uo_ds_top
dw_clearing_wo_control1.DataObject = "dw_clearing_wo_control"
dw_clearing_wo_control1.SetTransObject(sqlca)	
f_add_where_clause_ds(dw_clearing_wo_control1, "", " and before_afudc_indicator = 1 ",false)
dw_clearing_wo_control1.RETRIEVE(i_nvo_wo_control.i_company)

for i = 1 to dw_clearing_wo_control1.RowCount()
		
	clear_wo_id = dw_clearing_wo_control1.GetItemNumber(i, 'work_order_id')
	
	f_set_large_rollback_segment()
		
	if not isnull(clear_wo_id) then
		ret = f_update_wo_summary_month_wo(clear_wo_id, month_number)
		if ret = -1 then
				f_pp_msgs("ERROR: updating wo_summary for clearing work orders ...")
			rollback;
			i_nvo_wo_control.i_afudc_error = true
			goto doReturn
		else
			commit;
		end if
	end if	
next

// Begin the Overhead calculation.
f_pp_msgs("Calculate Overheads Before AFUDC. "+ ' ' + string(now()))
i_nvo_wo_control.i_oh_error = false
i_nvo_wo_control.i_ovh_type = 1

// perform the actual overhead calcuation
uf_overhead_calc()

if i_nvo_wo_control.i_oh_error then
	 goto doReturn
end if

if not i_nvo_wo_control.i_split_oh_afudc then
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation',today()) 
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'before_oh_calc', today())
	i_nvo_wo_control.of_updatedwnocommit()
else
	// date updated in the calling script
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'before_oh_calc', today())
	i_nvo_wo_control.of_updatedwnocommit()
end if

// AMP FIXED MEM LEAK 04/11/08
doReturn:
destroy dw_clearing_wo_control1;

return true
end function

public function boolean uf_none_calc (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_none_calc()
// 
// This function is the none calc from w_wo_control
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string  company, je_code, month, where_str, acct, custom_afudc_credit
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
////	added for company loop
longlong	num_companies
	
if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1,"overhead_approval")) then
	f_pp_msgs("Approve Over. and AFUDC - You have already approved the overheads and AFUDC for this month.")
	return false
end if

if not i_nvo_wo_control.i_split_oh_afudc then
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation',today()) 

	i_nvo_wo_control.of_updatedwnocommit( )
		 
end if
	
return true

end function

public function boolean uf_afudc_real_calc (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_afudc_real_calc()
// 
// Calculates AFUDC.  Relies heavily on the uo_afudc_cpi_calc for the calculation
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

longlong i, have_wip, wip_rtn, month_number, num_companies, afc_rtn
datetime approved_date 


//Create UserObject for WIP Comp Calc 
uo_wip_computations uo_wip
uo_wip = create uo_wip_computations

//Create UserObject for Afudc and CPI Calc 
uo_afudc_cpi_calc uo_afudc_cpi_calc_instance
uo_afudc_cpi_calc_instance = CREATE uo_afudc_cpi_calc 
uo_afudc_cpi_calc_instance.i_afudc_error = false

month_number = year(date(i_nvo_wo_control.i_month)) * 100 + month(date(i_nvo_wo_control.i_month))

if not i_nvo_wo_control.i_split_oh_afudc then	
	
	setnull(approved_date)
	select overhead_approval into :approved_date
	from wo_process_control where company_id = :i_nvo_wo_control.i_company
	and accounting_month = :i_nvo_wo_control.i_month;
	
	if not isnull(approved_date) then
		f_pp_msgs("Overheads and AFUDC have already been approved.")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		if isvalid(uo_wip) then destroy uo_wip
		if isvalid(uo_afudc_cpi_calc_instance) then destroy uo_afudc_cpi_calc_instance
		return false
	end if
	
else 
	
	setnull(approved_date)
	select afudc_approval into :approved_date
	from wo_process_control where company_id = :i_nvo_wo_control.i_company
	and accounting_month = :i_nvo_wo_control.i_month;
	
	if not isnull(approved_date) then
		f_pp_msgs("Overheads and AFUDC have already been approved.")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true		 
		if isvalid(uo_wip) then destroy uo_wip
		if isvalid(uo_afudc_cpi_calc_instance) then destroy uo_afudc_cpi_calc_instance
		return false
	end if
end if

//run afudc/cpi calculation (there is a commit in this function call)
afc_rtn = uo_afudc_cpi_calc_instance.of__start_calc(i_nvo_wo_control.i_company, month_number, i_nvo_wo_control.i_month)

if afc_rtn < 0 then
	//error message would have already displayed in uo_afudc_cpi_calc
	rollback;
	
	//clear out the overhead_calculation date on wo_process_control
	 if not i_nvo_wo_control.i_split_oh_afudc then 		
		update wo_process_control 
		set overhead_calculation = null, afudc_calc = null
		where company_id = :i_nvo_wo_control.i_company
		and accounting_month = :i_nvo_wo_control.i_month;
	else
		update wo_process_control 
		set afudc_calc = null
		where company_id = :i_nvo_wo_control.i_company
		and accounting_month = :i_nvo_wo_control.i_month;
	end if 
				
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating wo_process_control.overhead_calculation and afudc_calc to null :  ~n~n" + &
					sqlca.SQLErrText)
		rollback;
	end if 
	
	i_nvo_wo_control.i_afudc_error = true 
	if isvalid(uo_wip) then destroy uo_wip 
	if isvalid(uo_afudc_cpi_calc_instance) then destroy uo_afudc_cpi_calc_instance
	return false
end if 
 
//check if the company has any wip_comps		
have_wip = 0
select count(*) into :have_wip
from   wip_computation where company_id_from = :i_nvo_wo_control.i_company and rownum = 1; 

if have_wip > 0 then
	
	wip_rtn = uo_wip.uf_wip_comp_calc(i_nvo_wo_control.i_company , i_nvo_wo_control.i_month_number)
	if wip_rtn = -1 then
		f_pp_msgs("ERROR: WIP Comp Calc  " + sqlca.sqlerrtext) 
		rollback;
		i_nvo_wo_control.i_afudc_error = true
		if isvalid(uo_wip) then destroy uo_wip 
		if isvalid(uo_afudc_cpi_calc_instance) then destroy uo_afudc_cpi_calc_instance
		return false
	else
		commit;
	end if
	
end if		

if not i_nvo_wo_control.i_split_oh_afudc then
	
	update wo_process_control 
	set overhead_calculation = sysdate, afudc_calc = sysdate
	where company_id = :i_nvo_wo_control.i_company
	and accounting_month = :i_nvo_wo_control.i_month;
			
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating wo_process_control.overhead_calculation and afudc_calc:  ~n~n" + &
					sqlca.SQLErrText)
		rollback;
		if isvalid(uo_wip) then destroy uo_wip 
		if isvalid(uo_afudc_cpi_calc_instance) then destroy uo_afudc_cpi_calc_instance
		return false	
	end if
	
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation',today()) 
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'afudc_calc', today())
	i_nvo_wo_control.of_updatedwnocommit( )
else
	
	update wo_process_control 
	set afudc_calc = sysdate
	where company_id = :i_nvo_wo_control.i_company
	and accounting_month = :i_nvo_wo_control.i_month;
			
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating wo_process_control.afudc_calc: ~n~n" + &
					sqlca.SQLErrText)
		rollback;
		if isvalid(uo_wip) then destroy uo_wip 
		if isvalid(uo_afudc_cpi_calc_instance) then destroy uo_afudc_cpi_calc_instance
		return false	
	end if
	
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'afudc_calc', today())
	i_nvo_wo_control.of_updatedwnocommit( )
end if 
		
if isvalid(uo_wip) then destroy uo_wip
if isvalid(uo_afudc_cpi_calc_instance) then destroy uo_afudc_cpi_calc_instance

return true

end function

public function boolean uf_wo_afudc_ovh_control (boolean cbx_calc_ovh_before_checked, boolean cbx_calc_afudc_checked, boolean cbx_calc_ovh_after_checked, boolean cbx_calc_ovh_before_enabled, boolean cbx_calc_afudc_enabled, datetime a_month);//**********************************************************************************************************************************************************
//
// uf_wo_afudc_ovh_control()
// 
// This function is the afudc overhead control
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

datetime process_date
longlong c, num_companies,company_id
string text2
longlong month_number

month_number = i_nvo_wo_control.i_month_number
 
if cbx_calc_ovh_before_checked = false and &
cbx_calc_afudc_checked = false and &
cbx_calc_ovh_after_checked = false then
  f_pp_msgs("Error in AFUDC Overhead Control - Please select a calculate option")
  return false
end if


uf_retrieve_dates( )

// SEB: Adding company loop here and removing the loop from each of the calc functions
num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c = 1 to num_companies
		
	//the company is selected, update the rest of the window
	i_nvo_wo_control.of_companychanged(c,a_month)
	f_pp_msgs("Calculating Overheads and AFUDC for Company ID: " + string(i_nvo_wo_control.i_company_idx[c]) + ", Company Name: " + i_nvo_wo_control.i_company_descr[c])
	if cbx_calc_ovh_before_checked = true then
		if cbx_calc_ovh_after_checked = false and not isnull(i_calc_ovh_after) and i_calc_ovh_after<> "" then
			f_pp_msgs("Processing Error - Can not process Before Overheads, "  + &
			"if After Overheads have been run and are not being run at this time."  + &
			" Delete After Overheads or Run both Before and After Overheads at"  + &
			" the same time.")
			return false
		end if
		if cbx_calc_afudc_checked = false and not isnull(i_calc_afudc) and i_calc_afudc<> ""then
			f_pp_msgs("Processing Error - Can not process Before Overheads, "  + &
			"if AFUDC have been run and are not being run at this time."  + &
			" Delete AFUDC or Run both Before Overheads and AFUDC at" + &
			" the same time.")
			return false
		end if
		 
		uf_before_oh_calc(a_month)
		
		if i_nvo_wo_control.i_afudc_error then goto done
	end if

	uf_retrieve_dates()

	if cbx_calc_afudc_checked = true then
		
		if (isnull(i_calc_ovh_before) or i_calc_ovh_before = "") and cbx_calc_ovh_before_enabled = true then
			f_pp_msgs("Error - Cannot Calc AFUDC if the Before Ovh has not been calculated")
			return false
		end if
		if cbx_calc_ovh_after_checked = false and not isnull(i_calc_ovh_after) and i_calc_ovh_after <> "" then
			f_pp_msgs("Processing Error - Can not process AFUDC, " + &
			"if After Overheads have been run and are not being run at this time." + &
			" Delete After Overheads or Run both AFUDC and After Overheads at"  +&
			" the same time.")
			return false
		end if
		
		uf_afudc_real_calc(a_month)
		if i_nvo_wo_control.i_afudc_error then goto done	
	end if

	uf_retrieve_dates()
	
	if cbx_calc_ovh_after_checked = true then
		
		if isnull(i_calc_ovh_before) and cbx_calc_ovh_before_enabled = true then
			f_pp_msgs("Error - Cannot Calc After Ovh if the Before Ovh has not been calculated")
			return false
		end if
		if isnull(i_calc_afudc) and cbx_calc_afudc_enabled = true then
			f_pp_msgs("Error - Cannot Calc After Ovh if AFUDC has not been calculated")
			return false
		end if
		uf_after_oh_calc(a_month)
			
	end if
	DONE:

	i_nvo_wo_control.of_wo_summaries( )

	// update the dates

	if cbx_calc_ovh_before_checked = true then
		
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'before_oh_calc', today())
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation',today()) 
		i_nvo_wo_control.of_updatedw( )
	end if
	
	if cbx_calc_afudc_checked = true then
		
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'afudc_calc', today())
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation', today()) 
		i_nvo_wo_control.of_updatedw( )
	end if
	
	if cbx_calc_ovh_after_checked = true then
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'after_oh_calc', today())
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation',today()) 
		i_nvo_wo_control.of_updatedw( )
	end if

next

/////////////////
cbx_calc_ovh_before_checked = false  
cbx_calc_afudc_checked = false  
cbx_calc_ovh_after_checked = false  

uf_retrieve_dates()
	
f_pp_msgs("Calc. Overheads and AFUDC - Done!")

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppprojct_custom_version nvo_ppprojct_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version

choose case a_pbd_name
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version	
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

