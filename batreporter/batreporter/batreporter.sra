HA$PBExportHeader$batreporter.sra
$PBExportComments$Generated Application Object
forward
global type batreporter from application
end type
global uo_sqlca sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
string g_os_type


longlong g_process_id, g_occurrence_id, g_msg_order, g_rtn_code, g_interface_id, &
         g_source_id, g_record_count
string   g_log_file, g_batch_id, g_balancing_timestamp, g_command_line_args
boolean  g_do_not_write_batch_id 
datetime g_finished_at
u_prog_interface g_prog_interface
uo_sqlca_logs    g_sqlca_logs
uo_mail g_msmail
u_external_function uo_winapi
dec{2} g_amount
uo_client_interface g_uo_client

////   ////   added for uo_log
boolean   g_of_log_add_time, g_of_log_use_ppmsg, g_of_log_use_writefile

////   ////   added for uo_check_sql
boolean g_of_check_sql_write_success
boolean g_main_application

// Use this variable to be the OS return value in case we encounter an error 
// before we can connect to the database and establish the online logs
longlong g_rtn_failure = -1 

boolean g_manual_batch
uo_ps_web g_ps_interface
uo_smtpmail g_smtpmail

uo_ppinterface g_uo_ppint
boolean g_db_connected, g_db_logs_connected

nvo_runtimeerror g_rte
nvo_runtimeerror_app g_rte_app
nvo_stats g_stats
s_user_info                s_user_info
s_sys_info				s_sys_info
nvo_func_string		g_string_func
nvo_func_database	g_db_func
nvo_func_datastore	g_ds_func
nvo_pp_func_io			g_io_func
uo_messagebox                          g_msg  //Added by appeon (8/15/2012  )
uo_ds_top g_ds_messagebox_translate
//----------------------- end from ppinterface

// ### 29892: JAK: server side processing
s_file_info				g_exe_info   // global variable with exe attributes
boolean								g_ssp
string									g_ssp_user, g_ssp_process, g_ssp_return
longlong	g_ssp_process_identifier
s_ppbase_parm_arrays   			g_ssp_parms
s_ppbase_parm_arrays_labels	g_ssp_labels
nvo_server_side_request			g_ssp_nvo
uo_system_cache					g_cache
uo_cr_cache 						g_cr

// autogen je accounts
uo_ds_top  g_ds_cr_element_definitions
uo_autogen_je_account g_uo_autogen_je_account

//Holder for PP Constants
nvo_pp_constants 		g_pp_constants
end variables

global type batreporter from application
string appname = "batreporter"
end type
global batreporter batreporter

type prototypes
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long GetLastError() Library "kernel32.dll"

Function long GetFileTime( long unit , ref s_filedate  create_time,  ref s_filedate last_access ,ref  s_filedate last_write) library "kernel32.dll" alias for "GetFileTime;Ansi"
Function Long FileTimeToSystemTime(s_filedate file_date ,ref  s_system_time system_date)  library "kernel32.dll" alias for "FileTimeToSystemTime;Ansi"
Function Long _lopen( string path ,long mode )  library "kernel32.dll" alias for "_lopen;Ansi"
Function Long _lclose( long unit )  library "kernel32.dll"
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
Function Long SystemTimeToTzSpecificLocalTime(s_TIME_ZONE_INFORMATION timezone, s_system_time utc_date, ref s_system_time local_date)  library "kernel32.dll" alias for "SystemTimeToTzSpecificLocalTime;Ansi"
Function Long GetTimeZoneInformation(ref s_TIME_ZONE_INFORMATION timezone)  library "kernel32.dll" alias for "GetTimeZoneInformation;Ansi"
Function uLong FindFirstFile(string lpFileName, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindFirstFileA;Ansi"
Function uLong FindNextFile(ulong hFindFile, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindNextFileA;Ansi"
Function uLong FindClose(ulong hFindFile) Library "kernel32.dll"

//File version functions
function long GetFileVersionInfoSizeA( string filename  ,ref long  size  ) library "version.dll" alias for "GetFileVersionInfoSizeA;Ansi"
function long GetFileVersionInfoA(string filename, long  dwHandle,long dwLen, ref blob  lpData) library "version.dll" alias for "GetFileVersionInfoA;Ansi"
function long VerQueryValueA( blob pBlock,string lpSubBlock,ref s_string lplpBuffer,ref long puLen ) library "version.dll" alias for "VerQueryValueA;Ansi"
function long GetUserDefaultLangID() library "kernel32.dll"
Function boolean SetCurrentDirectoryA(ref string cdir) LIBRARY "kernel32.dll" alias for "SetCurrentDirectoryA;Ansi" 



// addiitonal functions for system and user info
function long GetVersionExA( REF s_osversioninfoex OS) library "kernel32.dll" alias for "GetVersionExA;Ansi"
function long WNetGetUserA(long ptr,ref string user,ref long len) LIBRARY "mpr.dll" alias for "WNetGetUserA;Ansi"
function long gethostname(ref string host,long len) library "Ws2_32.dll" alias for "gethostname;Ansi"
function int WSAGetLastError ( ) library "wsock32.dll"
function int WSAStartup( uint UIVersionRequested, ref s_WSAData lpWSAData )library "wsock32.dll"

// to get UNC file names
//Function long WNetGetUniversalName(string lpLocalPath,long dwInfoLevel,ref s_universal_name_info lpBuffer,ref long lpBufferSize)   Library "mpr.DLL" alias for "WNetGetUniversalNameA;Ansi"
Function long WNetGetConnection(string lpLocalName, ref string lpRemoteName, ref long lpnLength)   Library "mpr.DLL" alias for "WNetGetConnectionA;Ansi"



end prototypes

type variables
blob i_log_data 
longlong i_debug

end variables

forward prototypes
public function longlong wf_getversion (string a_filename, ref string a_version)
end prototypes

public function longlong wf_getversion (string a_filename, ref string a_version);
return 0

end function

on batreporter.create
appname="batreporter"
message=create message
sqlca=create uo_sqlca
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on batreporter.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;//***********************************************************************************
//This is the code you should use to start from when creating non-CR transaction interface
//    
//
//**********************************************************************************

//Remaining to do
   ///Add a debug mode that will write every SQL that is done this logic should be imbeded uo_sqlca
   ///Get the debug from a passed in command arguement
   ///Attempt to get the failed auto sys code from the ini file
   
string    ini_file, s_date,  exe_name,  sqls, sysdate, &
path, str, cntrl_val, db_version, exe_path, write_date, create_date, exe_version, &
temp_path, str_rtn, exception_msg
longlong rtn, running_session_id, session_id, mod,status
date      ddate 
time       ttime
datetime   started_at, file_created, file_written
boolean  db_connected, running_from_source, running_from_ws
environment env
string arg[]
string   errmsg
longlong id

n_bat_reporter uo_client
uo_client = CREATE n_bat_reporter

   

   
//
// Put all interface code inside of a try-catch block
//
// Interfaces should be batch jobs with no visual components or user interaction.
//
// The CATCH block at the bottom of this script will trap any exceptions or 
// runtime errors.  This will prevent PowerBuilder's "popup" messages from 
// displaying on the screen. 
//
// This means YOU CAN NOT USE GOTO/LABEL STATEMENTS inside of this code!!!
//
TRY 
	
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	g_cache = create uo_system_cache				
	
	g_pp_constants = create nvo_pp_constants
	
	s_sys_info.dbms = 'oracle'
		
   //*****************************************************************************************
   //
   // these are global variables require definition for each exe
   //
   //*****************************************************************************************
   g_of_log_add_time       =    true
   g_of_log_use_ppmsg      =   true
   g_of_log_use_writefile   =   true
   g_of_check_sql_write_success = false
   g_msg_order              =    1   
   
   ///These return values should be setup in PP_PROCESSES_RETURN_VALUES
   g_rtn_code               =    0  ///This is the code that gets sent back to auto sys
   setNull(g_interface_id)         //   Interface Batch ID
   g_record_count           =    0
   // get any command line arguments if used
   // any parsing or use of this command line arg should be in the custom code
   g_command_line_args = CommandParm()
   //******************
   if not isnull(g_command_line_args) and  g_command_line_args <> '' then
      g_manual_batch = true
   else
      g_manual_batch = false
   end if
   

   //******************   

   //  This may be set to true in the case of certain fatal processing errors so the
   //  batch_id is not saved ...
   g_do_not_write_batch_id = false
   sysdate   = string(Today(),"yyyymmdd")+"_"+string(Now(),"hhmm")
   g_main_application = false
	db_connected = false	
	
	
	//*****************************************************************************************
	//
	// Initialize local variables
	//
	//*****************************************************************************************
	sysdate								= 	string(today(),"yyyymmdd_hhmmss")
	s_date 								= 	string(today(), "yyyy-mm-dd hh:mm:ss")
	ddate  								= 	date(left(s_date, 10))
	ttime  								=	time(right(s_date, 8))
	started_at 						= 	datetime(ddate, ttime)
	running_from_source 			= 	false
	running_from_ws 				= 	false
	
	// must instantiate uo_winapi before calling f_get_temp_dir() below
	// this code was taken (and modified) from the PowerPlant application open() event
	getenvironment(env) 
	CHOOSE CASE env.OSType
	CASE Windows!  
		 if env.win16 = true then 
			  uo_winapi = create u_external_function_winapi
		else
			  uo_winapi = create u_external_function_win32
		end if	
	CASE WindowsNT!
		 if env.win16 = true then 
			 uo_winapi = create u_external_function_winapi
		 else	 
			 uo_winapi = create u_external_function_win32
		end if
	END CHOOSE 
	
	// ### 29892: JAK: Pull file information into global structure
	//GET the last change time of this interface and write it to the log
	nvo_func_file l_func_file
	l_func_file = create nvo_func_file
	g_exe_info = l_func_file.of_get_file_info()
	
	exe_name = g_exe_info.file_name
	path = g_exe_info.file_path + g_exe_info.file_name
	exe_version = g_exe_info.file_version
	file_written = g_exe_info.file_write_date
	
	
	// PP-45359: Moved the create for g_msmail from ppinterface.open since we have to
	// read from the INI file to look up the email option to determine which user 
	// object to use to create g_msmail
	ini_file   = f_get_ini_file()
	if ProfileInt(ini_file, "Mail", "SMTPMAIL", 0) = 1 then
		g_msmail = create uo_smtpmail
	else
		g_msmail = create uo_msmail
	end if
	
	//*****************************************************************************************
	//
	// Get the location and name of the log file
	//
		//*****************************************************************************************
	setNull(g_log_file)
	
	// METHOD ONE: Use PPCSET Entry
	g_prog_interface.uf_get_prog_params("")
	g_log_file = trim(g_prog_interface.i_log_file)
	
	// METHOD TWO: Look in the INI file for the TEMP dir
	//             Be careful that the "Temp" value from the "Application" section of the 
	//             pwrplant.ini file is setup correctly and points to a real folder
	if isnull(g_log_file) or g_log_file = "" then
		ini_file   = f_get_ini_file()
		g_log_file = trim(ProfileString(ini_file, "Application", "Temp", "None"))
		
		// verify that a real value was returned
		if trim(g_log_file) = "" or g_log_file = "None" then
			setNull(g_log_file)
		else
			// append a '\' if needed, and use the {exe_name} & {sysdate} conventions
			if right(trim(g_log_file), 1) <> '\' then g_log_file += '\'
			g_log_file += '{exe_name}_{sysdate}.log'
		end if
	end if
	
	// METHOD THREE: Look up the windows TEMP environment variable
	//               The f_get_temp_dir() function below calls the base function 
	//               uo_ps_interface.uf_get_temp_dir(), which does a check similar to 
	//               METHOD TWO above.  If you want this method to be used in determining
	//               the location of the log file, you must either:
	//                      (a) delete the "Temp" value from the "Application" section of pwrplant.ini OR
	//                      (b) set this value to "None" (without quotes) in pwrplant.ini
	if isnull(g_log_file) or g_log_file = "" then
		g_log_file = f_get_temp_dir()
		
		if not isnull(g_log_file) and trim(g_log_file) <> '' then
			if right(trim(g_log_file), 1) <> '\' then g_log_file += '\'
			g_log_file += '{exe_name}_{sysdate}.log'
		end if	
	end if
	
	// METHOD FOUR: Default to c:\temp
	if isnull(g_log_file) or g_log_file = "" then
		g_log_file = 'C:\temp\{exe_name}_{sysdate}.log'
	end if
	
	g_log_file = f_replace_string(lower(g_log_file),'{exe_name}',f_replace_string(exe_name,'.exe','','all'),'all')
	g_log_file = f_replace_string(lower(g_log_file),'{sysdate}',sysdate,'all')
	
	if pos(g_log_file,'\') = 0 and pos(g_log_file,'/') = 0 then
		// Directory not explicit.  Explicitly set the same path as exe.
		g_log_file = g_exe_info.file_path + g_log_file
	end if   
   
   //*****************************************************************************************
   //
   //  Create the connection to the POWERPLANT instance.
   //
   //*****************************************************************************************
   s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
   ddate  = date(left(s_date, 10))
   ttime  = time(right(s_date, 8))
   started_at = datetime(ddate, ttime)

   g_prog_interface.i_local = false
   if g_manual_batch then
      rtn = f_parsestringintostringarray(g_command_line_args, ",", arg[])
      if rtn < 6 then
         f_write_log(g_log_file, "Error: Wrong number of arguments " + string(upperbound(arg)) + " g_command_line_args = " + g_command_line_args )
         halt
      end if
      sqlca.dbms = arg[5]   
      sqlca.servername = arg[4]
      sqlca.logid= arg[2]
      sqlca.logpass = arg[3]
      sqlca.dbparm=arg[6]
      id=long(arg[7])
      i_debug = long(arg[8])
      g_prog_interface.i_local = true
      if i_debug = 1 then
           MessageBox("DEBUG", &
                    "User " + sqlca.logid + " ~r~n" + &
                 "Password   " + sqlca.logpass + " ~r~n" + &
               "Server    " + sqlca.servername + " ~r~n" + &
               "dbms    " + sqlca.dbms + " ~r~n" + &
               "dbparm    " + sqlca.dbparm + " ~r~n" + &
               "id    " + string(id) + " ~r~n" )
      end if 
   else
      sqlca.ServerName = g_prog_interface.i_server
      sqlca.DBMS       = g_prog_interface.i_dbms
      sqlca.LogId      = g_prog_interface.i_username
      sqlca.LogPass    = g_prog_interface.i_password
      sqlca.dbparm='disablebind=1,staticbind=0'
   end if
   str = "C:\local_files\pwrpla9.0"
   SetCurrentDirectoryA(str)
   connect using sqlca;
   
   if sqlca.SQLCode = 0 then
      f_write_log(g_log_file, "Connected to PowerPlant instance. ")
      f_write_log(g_log_file, "    Server: " + sqlca.ServerName)
      f_write_log(g_log_file, "    DBMS:   " + sqlca.DBMS)
      f_write_log(g_log_file, "    LogId:  " + sqlca.LogId)
      f_write_log(g_log_file, "    DBParm: "+ sqlca.dbparm)
      f_write_log(g_log_file, "    SQLCODE: " + string(sqlca.sqlcode))
      f_write_log(g_log_file, "    SQLDBCODE: " + string(sqlca.sqldbcode))
      f_write_log(g_log_file, "    Code Last Modified:  " + string(file_written))
      f_write_log(g_log_file, "    Interface Path: " + string(path))
      f_write_log(g_log_file, " ")
   else
      f_write_log(g_log_file, "Error connecting to PowerPlant instance: " + sqlca.SQLErrText)
      f_write_log(g_log_file, "    Server: " + sqlca.ServerName)
      f_write_log(g_log_file, "    DBMS:   " + sqlca.DBMS)
      f_write_log(g_log_file, "    LogId:  " + sqlca.LogId)
      f_write_log(g_log_file, "    DBParm: "+ sqlca.dbparm)
      f_write_log(g_log_file, "    SQLCODE: " + string(sqlca.sqlcode))
      f_write_log(g_log_file, "    SQLDBCODE: " + string(sqlca.sqldbcode))
      f_write_log(g_log_file, "    Code Last Modified:  " + string(file_written))
      f_write_log(g_log_file, "    Interface Path: " + string(path))
      f_write_log(g_log_file, " ")
      //  Erroring out without running the close script of the application.
      g_prog_interface.uf_exit(g_rtn_failure)
      halt
   end if
   
   
   //*****************************************************************************************
   //
   //  Create a separate connection to the POWERPLANT instance for the log.
   //
   //*****************************************************************************************
   g_ps_interface = create uo_ps_web
   g_ps_interface.uf_load_library('psconvrt.dll',errmsg)   
   g_ps_interface.i_debug = i_debug


   // Before we do the lookup to get the pp_processes name, if we are running from the source code, get the real
   //   exe name from the project p_client_interface
   if lower(exe_name) = 'pb110.exe' and not isnull(uo_client.i_exe_name) and trim(uo_client.i_exe_name) <> '' then
      exe_name = uo_client.i_exe_name 
      running_from_source = true
   else
      running_from_source = false
   end if
   
   
   //   Look up the process_id using the exe name
   setNull(g_process_id)
   
   select process_id into :g_process_id
   from pp_processes 
   where lower(trim(executable_file)) = lower(trim(:exe_name)) using sqlca;
   
   // if we didn't find anything or what we found was null, then write to the log file and exit
   if isnull(g_process_id) or sqlca.sqlcode = 100 then 
      f_write_log(g_log_file,"Could not determine the g_process_id")
      if sqlca.sqlcode = -1 then
         f_write_log(g_log_file, "   SQL Error: " + sqlca.sqlerrtext)
      else
         f_write_log(g_log_file, '   Make sure that a record exists in PP_PROCESSES for executable name "' + lower(trim(exe_name)) + '"')
      end if      
      g_prog_interface.uf_exit(g_rtn_failure)
      halt
   end if
   
   // get the value for the interface_batch_id
   ///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ///THIS WILL CAUSE PROBLEMS FOR CR TRANSACTION INTERFACES.
   ///YOU NEED A NEW INTERFACE BATCH ID FOR EACH SOURCE (AP, PAYROLL, ETC.)
   select costrepository.nextval into :g_interface_id from dual using sqlca;
   if isnull(g_interface_id) then 
      f_write_log(g_log_file, "Could not determine the interface_id")
      if sqlca.sqlcode <> 0 then
         f_write_log(g_log_file, "   SQL Error: " + sqlca.sqlerrtext)
      end if      
      g_prog_interface.uf_exit(g_rtn_failure)
      halt
   end if
   
   g_sqlca_logs.uf_connect()
   
   if g_sqlca_logs.SQLCode = 0 then
   ////added information to the version column here.
	longlong rrtn 
	rrtn = g_io_func.of_pp_msgs_start_log(g_process_id, exe_version)
	
	if rrtn <> 0 then
		f_write_log(g_log_file, "  ")
		f_write_log(g_log_file, "ERROR: inserting into pp_processes_occurrences: " + g_sqlca_logs.sqlerrtext)
		f_write_log(g_log_file, "  ")
		rollback using g_sqlca_logs;
         g_prog_interface.uf_exit(g_rtn_failure)
         halt 
	end if
              
         
      f_pp_msgs("************************************************************")
      f_pp_msgs("  ")
   
      if sqlca.SQLCode = 0 then
         f_pp_msgs("Connected to PowerPlant instance. (" + sqlca.ServerName + ")")
         f_pp_msgs(exe_name + " INTERFACE")
         f_pp_msgs("Started:  " + string(today()) + ", " + string(now()))
         f_pp_msgs( "Connecting to PowerPlant instance: " + sqlca.SQLErrText)
         f_pp_msgs( "    Server: " + sqlca.ServerName)
         f_pp_msgs( "    DBMS:   " + sqlca.DBMS)
         f_pp_msgs( "    LogId:  " + sqlca.LogId)
         f_pp_msgs( "    DBParm: "+ sqlca.dbparm)
         f_pp_msgs( "    Code Last Modified:  " + string(file_written))
         f_pp_msgs( "    Interface Path: " + string(path))
         f_pp_msgs( "    Log File Path: "+string(g_log_file))
         f_pp_msgs( "    INI File Path: "+string(ini_file))
         f_pp_msgs( " ")
         
      end if
      
      f_write_log(g_log_file, "(LOGS) Connected to PowerPlant instance. (" + &
                                g_sqlca_logs.ServerName + ")")
      f_pp_msgs("(LOGS) Connected to PowerPlant instance. (" + g_sqlca_logs.ServerName + ")")
      f_pp_msgs(" ")
      f_write_log(g_log_file, " ")
   else
      f_write_log(g_log_file, "(LOGS) Error connecting to PowerPlant instance. (" + &
                        g_sqlca_logs.ServerName + ")" + g_sqlca_logs.SQLErrText)
      f_write_log(g_log_file,"    Server: " + sqlca.ServerName)
      f_write_log(g_log_file,"    DBMS:   " + sqlca.DBMS)
      f_write_log(g_log_file,"    LogId:  " + sqlca.LogId)
      f_write_log(g_log_file,"    DBParm: "+ sqlca.dbparm)
      f_write_log(g_log_file,"    SQLCODE: " + string(sqlca.sqlcode))
      f_write_log(g_log_file,"    SQLDBCODE: " + string(sqlca.sqldbcode))
      f_write_log(g_log_file,"    SQLErrText: "+sqlca.sqlerrtext)
      f_write_log(g_log_file,"    Code Last Modified:  " + string(file_written))
      f_write_log(g_log_file,"    Interface Path: " + string(path))
      f_write_log(g_log_file,"    INI File Path: " + string(ini_file))
      f_write_log(g_log_file," ")
      g_prog_interface.uf_exit(g_rtn_failure)
      halt
   end if
   
   f_pp_msgs("************************************************************")
   f_pp_msgs(" ")
   
   // At this point, we have connected to the database via the sqlca object
   // and created a separate connection (g_sqlca_logs) for the online logs
   db_connected = true
   

   
   //*****************************************************************************************
   //
   //  CheckRunning Session ID to see if the interface is already running
   //
   //*****************************************************************************************
   running_session_id = 0
   select running_session_id into :running_session_id from pp_processes
    where process_id = :g_process_id;
   
   if isnull(running_session_id) or running_session_id = 0 then
      //  NOT CURRENTLY RUNNING:  update the record with this session_id.
      select userenv('sessionid') into :session_id from dual;
      
      update pp_processes 
      set running_session_id = :session_id
      where process_id = :g_process_id;
      
      if sqlca.SQLCode = 0 then
         commit;
         
      else
         //  Disconnect and halt.
         f_write_log(g_log_file, '  ERROR: updating pp_processes.running_session_id: ' + &
            sqlca.SQLErrText)
         f_pp_msgs('  ERROR: updating pp_processes.running_session_id: ' + &
            sqlca.SQLErrText)      
         rollback;      
         
         // don't need the code commented out below - just use the close() event
         halt close
         
//         disconnect using sqlca;   
//         if sqlca.SQLCode = 0 then
//            f_write_log(g_log_file, "Disconnected from Database. (" + &
//                        sqlca.ServerName + ")")
//            f_pp_msgs("Disconnected from Database instance. (" + &
//                        sqlca.ServerName + ")")
//         else
//            f_write_log(g_log_file, "Error disconnecting from Database instance. (" + &
//                        sqlca.ServerName + ")" + sqlca.SQLErrText)
//            f_pp_msgs("Error disconnecting from Database instance. (" + &
//                        sqlca.ServerName + ")" + sqlca.SQLErrText)
//            
//         end if
//         
//         //  Log the time that the executable was terminated.
//         s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
//            
//         f_write_log(g_log_file, "  ")
//         f_write_log(g_log_file, "Finished:  " + s_date)
//         f_write_log(g_log_file, "************************************************************")
//         f_write_log(g_log_file, "  ")
//         f_write_log(g_log_file, "  ")
//         
//         f_pp_msgs("  ")
//         f_pp_msgs("Finished:  " + s_date)
//         f_pp_msgs("************************************************************")
//         f_pp_msgs("  ")
//         f_pp_msgs("  ")
//         g_prog_interface.uf_exit(g_rtn_failure)
//         halt
      end if
         
   else
      
      //  CURRENTLY RUNNING: disconnect and halt.
      f_write_log(g_log_file, ' ')
      f_write_log(g_log_file, ' ')
      f_write_log(g_log_file, 'Another session is currently running this process.')
      f_write_log(g_log_file, '  session_id = ' + string(running_session_id))
      f_write_log(g_log_file, ' ')
      f_write_log(g_log_file, ' ')
      
      f_pp_msgs(' ')
      f_pp_msgs(' ')
      f_pp_msgs('Another session is currently running this process.')
      f_pp_msgs('  session_id = ' + string(running_session_id))
      f_pp_msgs(' ')
      f_pp_msgs(' ')
      
      //get the rtn value from the system control switch.  If not found default the value to g_rtn_failure
      setnull(str_rtn)
      str_rtn = f_pp_system_control_company('Interface Return - Running Session', -1)
   
      //Check to see that the rtn_value is a number
      if isnull(str_rtn) then str_rtn = 'null'
      if isnumber(str_rtn) then
         rtn = long(str_rtn)
      else
         rtn = g_rtn_failure
         f_pp_msgs(' ')
         f_pp_msgs(' ')
         f_pp_msgs(' Invalid return type - check system control "Interface Return - Running Session"')
         f_pp_msgs(' Defaulting return code to ' + string(g_rtn_failure))
      end if
      g_rtn_code = rtn
      
      // exit via the close() event
      halt close
      
   end if
   
   //*****************************************************************************************
   //
   //  Check The version 
   //
   //*****************************************************************************************
       
   //Retrieve the upper of this  system control into a variable 
   cntrl_val = upper(f_pp_system_control_company('Interface Version Validation', -1))
   
   ///If it is null write to the online logs that you are defaulting the value to "YES"
   if isnull(cntrl_val)  or cntrl_val = '' then
      cntrl_val = 'YES'
   end if
   
   //Retrieve the Version for this interface from pp_processes interface
   select version
   into :db_version
   from pp_processes
   where process_id = :g_process_id;
   
   if isnull(db_version) then db_version = 'Null'
   if isnull(exe_version) or exe_version = "" or exe_version = " " then exe_version = 'Null'
   
   //Write the version validation system control switch to the online logs
   f_pp_msgs('  Version validation system control:  ' + cntrl_val)
   //Write the Interface version to the online logs (value in the version variable)
   f_pp_msgs('  Interface version:  ' + exe_version)
   //Write the database version to the on ling logs
   f_pp_msgs('  Database version:  ' + db_version)
   ///Validate
   if upper(cntrl_val) = 'YES' then
      if running_from_source then
         f_pp_msgs(' ')
         f_pp_msgs(' ')
         f_pp_msgs('************************************************************')
         f_pp_msgs('  The Interface is running from the Powerbuilder source code!')
         f_pp_msgs('  No version validation will be performed!')
         f_pp_msgs('************************************************************')
         f_pp_msgs(' ')
         f_pp_msgs(' ')
      else
         if not db_version = exe_version or (db_version = 'Null' and exe_version = 'Null') then
            f_pp_msgs(' ')
            f_pp_msgs(' ')
            f_pp_msgs('************************************************************')
            f_pp_msgs('  The database version and Interface version do not match')
            f_pp_msgs('  The Interface will exit without running')
            f_pp_msgs('************************************************************')
            f_pp_msgs(' ')
            f_pp_msgs(' ')
            
            setnull(str_rtn)
            str_rtn = f_pp_system_control_company('Interface Return - Version Mismatch', -1)
         
            //Check to see that the rtn_value is a number
            if isnull(str_rtn) then str_rtn = 'null'
            if isnumber(str_rtn) then
               rtn = long(str_rtn)
            else
               rtn = g_rtn_failure
               f_pp_msgs(' ')
               f_pp_msgs(' ')
               f_pp_msgs(' Invalid return type - check system control "Interface Return - Version Mismatch"')
               f_pp_msgs(' Defaulting return code to ' + string(g_rtn_failure))
            end if
            g_rtn_code = rtn
   
            // exit via the close() event 
            halt close   
         else
            f_pp_msgs('  Version Match Successful')
         end if
            
      end if
   end if
      
   //*****************************************************************************************
   //
   //  Run the interface code
   //
   //*****************************************************************************************   
   f_pp_msgs(' ')
   f_pp_msgs('******************** Begin Interface Custom Code ********************')
   f_pp_msgs(' ')
   
   g_rtn_code = uo_client.uf_generate_report()
   
   f_pp_msgs(' ')
   f_pp_msgs('******************** End Interface Custom Code ********************')
   f_pp_msgs(' ')
   

// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (Exception e)
   exception_msg  = 'An unhandled ' + upper(e.classname()) + 'Exception occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
   exception_msg  = 'An unhandled ' + upper(rte.classname()) + ' Run Time Error occurred with message "' + rte.getmessage() + '"'

   // append more RTE details
   if not isNull(rte.line)        then exception_msg += '~r~n   Line: '         + string(rte.line)
   if not isNull(rte.number)      then exception_msg += '~r~n   Number: '       + string(rte.number)
   if not isNull(rte.class)       then exception_msg += '~r~n   Class: '        + rte.class
   if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
   if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName

// this code will always get executed, regardless of an exception or not
finally
   
   // use the exception message to determine if we actually had an unhandled exception
   if len(exception_msg) > 0 then 
      
      // write the unhandled exception to the log file
      f_write_log(g_log_file, exception_msg)
      
      // if we never got connected to the database, write to log file and set the error code
      if db_connected then 
         f_pp_msgs(exception_msg)
         g_rtn_code = g_rtn_failure
      end if   
      
   end if
   
end try

// if we never got connected to the database, error out without running the close event of the application
if not db_connected then 
   g_prog_interface.uf_exit(g_rtn_failure)
   halt
end if

halt close


end event

event close;//***********************************************************************************
// You can immediately end the interface (and invoke this code) by 
// using the following statement in any PB script: 
//
//     halt close
//    
// Before issuing this statement you must have already:
//
//     1. Connected to the database using both sqlca & g_sqlca_logs.
//        This is generally taken care of in the application open event, but if
//        you are not connected errors may result in this script.
//
//     2. Set the global variable g_rtn_code.  The value in this variable should
//        correspond to a return_value in pp_processes_return_values table for
//        the current process_id.  If the os_return_value column is populated for that 
//        row, its value will be returned to the operating system or scheduler that ran 
//        the interface.  If the os_return_value is null, g_rtn_code will be returned. 
//**********************************************************************************

longlong os_return_value
string   s_date
datetime finished_at
date   ddate
time   ttime

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))

//  Update pp_processes to show this session is finished
//  make sure to only update pp_processes if this process is the current running process
update pp_processes set running_session_id = null
where process_id = :g_process_id
and running_session_id = userenv('sessionid')
;

if sqlca.SQLCode = 0 then
   commit;
else
   f_pp_msgs("ERROR: updating pp_processes.running_session_id: " + &
      sqlca.SQLErrText)
   f_pp_msgs(" ")
   f_pp_msgs("The process DID RUN.")
   f_pp_msgs("The the pp_processes table must be updated by hand.")
   f_pp_msgs(" ")
   rollback;
end if


///Select the value that we should be returning to the Operating System from pp_processes_return_values
SetNull(os_return_value)
select os_return_value 
into :os_return_value
from pp_processes_return_values
where process_id = :g_process_id
and return_value = :g_rtn_code;
   
if isnull(os_return_value)  then os_return_value = g_rtn_code
   


//*****************************************************************************************
//
//  Disconnect from PowerPlant Instance ...
//
//*****************************************************************************************

disconnect using sqlca;

f_pp_msgs('')
f_write_log(g_log_file,'')

if sqlca.SQLCode = 0 then
   f_write_log(g_log_file, "Disconnected from PowerPlant instance. (" + &
                            sqlca.ServerName + ")")
   f_pp_msgs("Disconnected from PowerPlant instance. (" + &
                            sqlca.ServerName + ")")
else
   f_write_log(g_log_file, "Error disconnecting from PowerPlant instance. (" + &
               sqlca.ServerName + ")" + sqlca.SQLErrText)
   f_pp_msgs("Error disconnecting from PowerPlant instance. (" + &
               sqlca.ServerName + ")" + sqlca.SQLErrText)
end if

//*****************************************************************************************
//
//   update end time, batch_id, return_value, etc....
//
//*****************************************************************************************



finished_at   = datetime(ddate, ttime)
g_finished_at = finished_at

// if process was successful, use this to populate  
// the batch_id field on pp_processes_occurrences                                                        
g_batch_id = string(g_interface_id) 

update pp_processes_occurrences 
   set end_time = :g_finished_at, batch_id = :g_batch_id, return_value = :g_rtn_code
 where process_id = :g_process_id and occurrence_id = :g_occurrence_id
 using g_sqlca_logs;
   
if g_sqlca_logs.SQLCode = 0 then
   commit using g_sqlca_logs;
else
   rollback using g_sqlca_logs;
end if


//*****************************************************************************************
//
//   disconnect the logs connection
//
//*****************************************************************************************

g_sqlca_logs.uf_disconnect()

if g_sqlca_logs.SQLCode = 0 then
   f_write_log(g_log_file, "(LOGS) Disconnected from PowerPlant instance. (" + &
                            g_sqlca_logs.ServerName + ")")
else
   f_write_log(g_log_file, "(LOGS) Error disconnecting from PowerPlant instance. (" + &
               g_sqlca_logs.ServerName + ")" + g_sqlca_logs.SQLErrText)
end if


f_write_log(g_log_file, "  ")
f_write_log(g_log_file, "Finished:  " + s_date)
f_write_log(g_log_file, "************************************************************")
f_write_log(g_log_file, "  ")
f_write_log(g_log_file, "  ")
destroy g_prog_interface

g_prog_interface.uf_exit(os_return_value)


end event

