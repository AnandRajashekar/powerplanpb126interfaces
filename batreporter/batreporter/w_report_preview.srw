HA$PBExportHeader$w_report_preview.srw
$PBExportComments$change 'c:' to temp directory
forward
global type w_report_preview from window
end type
type cb_close from commandbutton within w_report_preview
end type
type cb_npage from commandbutton within w_report_preview
end type
type cb_ppage from commandbutton within w_report_preview
end type
type cb_print from commandbutton within w_report_preview
end type
type dw_1 from datawindow within w_report_preview
end type
type cb_1 from commandbutton within w_report_preview
end type
type cb_zoom from commandbutton within w_report_preview
end type
type cb_save_as from commandbutton within w_report_preview
end type
type cb_find from commandbutton within w_report_preview
end type
type cb_next from commandbutton within w_report_preview
end type
type cb_set_title from commandbutton within w_report_preview
end type
type cb_mail from commandbutton within w_report_preview
end type
end forward

global type w_report_preview from window
integer x = 18
integer y = 138
integer width = 3529
integer height = 1933
string title = "Preview Report"
boolean resizable = true
long backcolor = 12632256
cb_close cb_close
cb_npage cb_npage
cb_ppage cb_ppage
cb_print cb_print
dw_1 dw_1
cb_1 cb_1
cb_zoom cb_zoom
cb_save_as cb_save_as
cb_find cb_find
cb_next cb_next
cb_set_title cb_set_title
cb_mail cb_mail
end type
global w_report_preview w_report_preview

type variables
int i_minheight
int i_minwidth

int i_height
int i_width
int i_buttonx = 3150
uo_ps_interface u_ps_interface

end variables

forward prototypes
public subroutine wf_resize (datawindow a_dw)
public function integer wf_retrieve_dwchild (datawindow a_dw)
public function integer wf_get_objects (string a_objects, ref string a_object_list[])
end prototypes

public subroutine wf_resize (datawindow a_dw);//Pass this function a datawindow

//This function will size the datawindow and window to the best size for the contents of
//the datawindow. This is done by searching the columns of the datawindow to find the
//right most width.  The height is determined by the size of the detail band and summary.

environment le_env
int	li_column_count, li_counter
int  li_column_x
int  li_column_y
int  li_column_width
int  li_column_height
int  li_new_width
int  li_new_height
int  li_max_x = 0 
int  li_max_y = 0
int li_screenwidth
int li_screenheight
int li_controls, li_index
int li_winheight,li_winwidth,li_dwheight,li_dwwidth
int	li_vscroll_width = 78
int	li_hscroll_height = 69
dragobject ldo_temp

//turn redraw off to help avoid flicker
a_dw.setredraw(false)

//fill sort column list box with column name as defined in the datawindow
li_column_count = Integer(a_dw.Describe("DataWindow.Column.Count"))

//loop through all of the columns in the datawindow
For li_counter =  1 To li_column_count  
//	Find max x	
	li_column_x = Integer(a_dw.Describe("#"+string(li_counter)+".X"))
	li_column_width = Integer(a_dw.Describe("#"+string(li_counter)+".Width"))
	If li_column_x + li_column_width > li_max_x Then
		li_max_x = li_column_x + li_column_width
	End If
Next 

//	Find max y -- get height of detail and summary band
li_max_y =  Integer(a_dw.Describe("Datawindow.Summary.Height"))
li_max_y = li_max_y + (Integer(a_dw.Describe("Datawindow.Detail.Height")) * 5)
li_max_y = li_max_y + Integer(a_dw.Describe("Datawindow.Header.Height"))
li_max_y = li_max_y + Integer(a_dw.Describe("Datawindow.Footer.Height"))
//calculate new height and width for datawindow	
li_new_width = a_dw.X + li_max_x + li_vscroll_width + 25
li_new_height = a_dw.Y + li_max_y + li_hscroll_height + 25

//min size of datawindow (contraints of the controls on the screen)
If li_new_width < i_minwidth Then li_new_width = i_minwidth
If li_new_height < i_minheight Then li_new_height = i_minheight


/* get screen measurements */

getenvironment(le_env)

li_screenheight = PixelsToUnits(le_env.screenheight,YPixelsToUnits!)
li_screenwidth = PixelsToUnits(le_env.screenwidth,XPixelsToUnits!)

//max size of datawindow (contraints of screen width)
If li_new_width >= li_screenwidth Then
	a_dw.hscrollbar = true
	li_dwwidth = li_screenwidth - 75
	li_winwidth = li_screenwidth
Else
	a_dw.hscrollbar = false
	li_dwwidth = li_new_width 
	li_winwidth = li_new_width + a_dw.X + 75
End If

//max size for vertical (contraint of screen)
If li_new_height >= li_screenheight Then
	li_dwheight = li_screenheight - 350
	li_winheight = li_screenheight
Else
	li_dwheight = li_new_height
	li_winheight = li_new_height + a_dw.Y + 350
End If

//shift buttons and text to match new window size
li_controls = upperbound( control[])
For li_index = 1 to li_controls
	If Typeof(this.control[li_index]) = CommandButton! Then
			ldo_temp = this.control[li_index]
			ldo_temp.Y = li_winheight - (125 + ldo_temp.height)
	ElseIf  Typeof(this.control[li_index]) = StaticText! Then
			ldo_temp = this.control[li_index]
			ldo_temp.Y = li_winheight - (225 +ldo_temp.height)
	End If
Next

//resize window to best fit
this.resize(li_winwidth,li_winheight)		
//move window to be centered based on new size
this.Move( (li_ScreenWidth - li_winwidth) / 2, (li_ScreenHeight - li_winheight) / 2 )

//resize datawindow to best fit
a_dw.resize(li_dwwidth,li_dwheight)

//set redraw back on
a_dw.setredraw(true)

end subroutine

public function integer wf_retrieve_dwchild (datawindow a_dw);string objects,report_objects
string name,edit_str
string object_type,str
longlong i,num,pos,start_pos,num_cols,j
string object_list[]
string report_object_list[]
longlong count = 0
DataWindowChild dwchild
datastore ds_report
ds_report = create datastore



str = a_dw.describe("DataWindow.syntax")
if isnull(str) or str = '' then
	return 0
end if

objects = a_dw.Object.DataWindow.Objects
count = wf_get_objects(objects,object_list)

for i = 1 to count
	//a_dw.modify(object_list[i] + ".Visible=1")
	object_type = a_dw.describe(object_list[i] + ".type")
	if object_type = 'column' then
		edit_str = a_dw.describe(object_list[i] + ".Edit.Style")
		if edit_str = 'dddw' then
			  a_dw.getchild(object_list[i],dwchild)
			  dwchild.settransobject(sqlca)	 
			  dwchild.retrieve()	  
		end if
	elseif object_type = 'report' or object_type = 'datawindow'then
		  a_dw.modify(object_list[i] + ".Visible=1")
		  a_dw.getchild(object_list[i],dwchild)
		  dwchild.settransobject(sqlca)	 
		  dwchild.retrieve()	  
		  name = a_dw.describe(object_list[i] + ".DataObject")
	     ds_report.dataObject = name
		  
//		  report_objects = ds_report.Object.DataWindow.Objects
//        report_count = wf_get_objects(report_objects,report_object_list)
//        for j = 1 to report_count
//			   a_dw.modify(report_object_list ".visible=1")
//		  next

		  num_cols =  integer(ds_report.describe("datawindow.column.count"))
	     for j = 1 to num_cols
			   object_type = ds_report.describe("#" + string(j) + ".type")
				edit_str = ds_report.describe("#" + string(j) + ".Edit.Style")
				if edit_str = 'dddw' then
					  ds_report.getchild(object_list[i],dwchild)
					  dwchild.settransobject(sqlca)	
					  dwchild.retrieve()	  
				end if
		  next
	end if
next

return 0

end function

public function integer wf_get_objects (string a_objects, ref string a_object_list[]);longlong start_pos,pos,count
string null_list[]

a_object_list = null_list
count = 0
start_pos = 1
pos = pos(a_objects,"~t")
do while pos > 1
	count++
	a_object_list[count] = mid(a_objects,start_pos,pos - start_pos)
	start_pos = pos + 1
	pos = pos(a_objects,"~t",start_pos)
loop
count++
a_object_list[count] = mid(a_objects,start_pos)
return count
end function

event open;call super::open;int id
blob data
longlong unit,loops,flen
longlong pos,tot,bytes_write
string dw_syntax,dw_data,err
longlong code,report_type
string filename,out_filename

u_ps_interface = create  uo_ps_interface
if u_ps_interface.i_initialize_dll = false then
	cb_find.visible = false
	cb_next.visible = false
	cb_save_as.visible = false
	cb_mail.visible = false
end if 
i_height = height
i_width = width

//id = message.doubleparm
//selectblob report_data into :data from pp_batch_reports_data where id = :id;
//// Get the file length, and open the file
//
//select report_type into :report_type from pp_batch_reports where id = :id;
//if report_type = 1 then
//	string filedir
//	//filedir = f_get_temp_dir()
//	filename = filedir + "\report.psr"
//	unit = FileOpen(filename, StreamMode!, write!, lockwrite! ,Replace!)
//	flen = len(data)
//	
//	// Write the file
//	pos = 1
//	tot = flen
//	do while tot > 0
//		bytes_write = FileWrite(unit,data)
//		tot = tot - bytes_write 
//		data = blobmid(data,bytes_write + 1)
//		
//	loop
//	FileClose(unit)
//	dw_1.dataobject = filename
//	FileDelete(filename)
//elseif report_type = 3 then
////	filename = "c:\report.ps"
////	out_filename = "c:\report.pdf"
////	unit = FileOpen(filename, StreamMode!, write!, lockwrite! ,Replace!)
////	flen = len(data)
////	
////	// Write the file
////	pos = 1
////	tot = flen
////	do while tot > 0
////		bytes_write = FileWrite(unit,data)
////		tot = tot - bytes_write 
////		data = blobmid(data,bytes_write + 1)
////		
////	loop
////	FileClose(unit)
////	u_ps_interface.i_orig = 1
////	u_ps_interface.uf_saveas_pdf(filename,out_filename) 
////	ole_acrobat.object.src = out_filename
//   
//	//FileDelete(filename)
//else
//	dw_syntax = string(data)
////	selectblob report_data into :data from pp_batch_report_data where
////		id = :id;
////	dw_data = string(data)
////	
////	//dw_1.settransobject(sqlca)
//	code = dw_1.create(dw_syntax + dw_data,err)
//	if code  <> 1 then
//		messagebox("Error Message", "Create failed: "+ err)
//		return 1
//	end if
//end if
////dw_1.show()
////
//
//err = dw_1.modify("DataWindow.Print.Preview = yes")
//if err <> "" THEN
//	
//end if
//
//u_ps_interface = create  uo_ps_interface
//if u_ps_interface.i_initialize_dll = false then
//	cb_find.visible = false
//	cb_next.visible = false
//	cb_save_as.visible = false
//	cb_mail.visible = false
//end if 
//
//wf_retrieve_dwchild(dw_1)
end event

on w_report_preview.create
this.cb_close=create cb_close
this.cb_npage=create cb_npage
this.cb_ppage=create cb_ppage
this.cb_print=create cb_print
this.dw_1=create dw_1
this.cb_1=create cb_1
this.cb_zoom=create cb_zoom
this.cb_save_as=create cb_save_as
this.cb_find=create cb_find
this.cb_next=create cb_next
this.cb_set_title=create cb_set_title
this.cb_mail=create cb_mail
this.Control[]={this.cb_close,&
this.cb_npage,&
this.cb_ppage,&
this.cb_print,&
this.dw_1,&
this.cb_1,&
this.cb_zoom,&
this.cb_save_as,&
this.cb_find,&
this.cb_next,&
this.cb_set_title,&
this.cb_mail}
end on

on w_report_preview.destroy
destroy(this.cb_close)
destroy(this.cb_npage)
destroy(this.cb_ppage)
destroy(this.cb_print)
destroy(this.dw_1)
destroy(this.cb_1)
destroy(this.cb_zoom)
destroy(this.cb_save_as)
destroy(this.cb_find)
destroy(this.cb_next)
destroy(this.cb_set_title)
destroy(this.cb_mail)
end on

event resize;longlong w,h
longlong i
h = newheight - i_height
w = newwidth - i_width
dw_1.width = 3063 + w
dw_1.height = 1748 + h
for i = 1 to upperbound(this.control[])
	graphicobject g
	g = this.control[i]
	if g.typeof() = commandbutton!  then
		commandbutton b
		b = g
		b.x = i_buttonx + w
	end if
	
next


end event

type cb_close from commandbutton within w_report_preview
integer x = 3149
integer y = 1693
integer width = 322
integer height = 83
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Close"
end type

on clicked;
close(parent)
end on

type cb_npage from commandbutton within w_report_preview
integer x = 3149
integer y = 749
integer width = 322
integer height = 83
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Next Page"
end type

on clicked;//Scroll datawindow forward a page

dw_1.scrollnextpage()
end on

type cb_ppage from commandbutton within w_report_preview
integer x = 3149
integer y = 621
integer width = 322
integer height = 83
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prev Page"
end type

on clicked;//scroll datawindow back a page

dw_1.scrollpriorpage()
end on

type cb_print from commandbutton within w_report_preview
integer x = 3149
integer y = 42
integer width = 322
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Print"
end type

on clicked;//standard print for a datawindow
dw_1.print( )
end on

type dw_1 from datawindow within w_report_preview
integer x = 26
integer y = 42
integer width = 3065
integer height = 1747
integer taborder = 40
boolean hscrollbar = true
boolean vscrollbar = true
end type

type cb_1 from commandbutton within w_report_preview
integer x = 3149
integer y = 282
integer width = 322
integer height = 83
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Save As"
end type

on clicked;dw_1.saveas()
end on

type cb_zoom from commandbutton within w_report_preview
integer x = 3149
integer y = 877
integer width = 322
integer height = 83
integer taborder = 100
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Zoom"
end type

event clicked;
openwithparm(w_printzoom,dw_1)
end event

type cb_save_as from commandbutton within w_report_preview
integer x = 3149
integer y = 1005
integer width = 322
integer height = 83
integer taborder = 140
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Save As"
end type

event clicked;setpointer(hourglass!)
u_ps_interface.uf_save_as(dw_1)
//string title,pathname,filename
//string temp_file,temp_dir
//longlong pos
//title = "Select File Name and File Type"
//GetFileSaveName ( title, pathname, filename ,"PS", &
//	 " Text Files (*.TXT),*.TXT," + &
//	 " Posscript Files (*.PS),*.PS," + &
//	 " PDF Files (*.PDF), *.PDF") 
//
//
//filename = lower(filename)
//pos = pos(filename,'.')
//
//
//if pos(filename,'txt',pos) > 0 then
//  temp_dir = u_ps_interface.uf_get_temp_dir()
// 
//  if temp_dir = '' then
//	  messagebox("Error","Can't find the temp directory.")
//	  return
//  end if
//  temp_file = temp_dir + "\pstemp.ps"
//  dw_1.object.datawindow.print.filename = temp_file
//  dw_1.print()
//  u_ps_interface.uf_saveas_text(temp_file,pathname)
//elseif pos(filename,'pdf',pos) > 0 then 
//  temp_dir = u_ps_interface.uf_get_temp_dir()
//  if temp_dir = '' then
//	  messagebox("Error","Can't find the temp directory.")
//	  return
//  end if
//  temp_file = temp_dir + "\\pstemp.ps"
//	dw_1.object.datawindow.print.filename = temp_file
//   dw_1.print()
//	u_ps_interface.uf_saveas_pdf(temp_file,pathname)
//elseif pos(filename,'ps',pos) > 0 then 
//	dw_1.object.datawindow.print.filename = pathname
//   dw_1.print()
//else
//	MessageBox("Error","Unknown file type. (" + filename + ")") 
//end if
//
end event

type cb_find from commandbutton within w_report_preview
integer x = 3149
integer y = 1133
integer width = 322
integer height = 83
integer taborder = 130
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Find"
end type

event clicked;setpointer(hourglass!)
if u_ps_interface.uf_find(dw_1) > 1 then
	cb_next.enabled=true
end if
end event

type cb_next from commandbutton within w_report_preview
integer x = 3149
integer y = 1261
integer width = 322
integer height = 83
integer taborder = 120
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
boolean enabled = false
string text = "Next"
end type

event clicked;setpointer(hourglass!)
if u_ps_interface.uf_find_next(dw_1) = 0 then
	this.enabled=false
end if
end event

type cb_set_title from commandbutton within w_report_preview
integer x = 3149
integer y = 147
integer width = 322
integer height = 83
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Title"
end type

event clicked;string txt
//openwithparm(w_title_response,txt)

txt = message.stringparm



dw_1.modify("user_title.text =  '" + txt + "'")
end event

type cb_mail from commandbutton within w_report_preview
integer x = 3152
integer y = 1389
integer width = 322
integer height = 83
integer taborder = 110
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Mail"
end type

event clicked;u_ps_interface.uf_mail_print(dw_1,parent.title)
end event

