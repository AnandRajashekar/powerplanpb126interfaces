HA$PBExportHeader$n_web_reporter.sru
$PBExportComments$Generated COM/MTS Component
forward
global type n_web_reporter from nonvisualobject
end type
type s_retarg from structure within n_web_reporter
end type
type s_arg from structure within n_web_reporter
end type
end forward

type s_retarg from structure
	longlong	count
	s_arg		args[]
end type

type s_arg from structure
	string		arg_type
	longlong	len
	decimal { 0 }		dec
	datetime		datetime
	longlong	id
	string		str
	string		strarray[]
	longlong	numarray[]
end type

global type n_web_reporter from nonvisualobject descriptor "PB_ObjectCodeAssistants" = "{BB0DD547-B36E-11D1-BB47-000086095DDA}" 
event activate pbm_component_activate
event deactivate pbm_component_deactivate
end type
global n_web_reporter n_web_reporter

type prototypes
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long CreateDCA( string lpszDriver, string lpszDevice, string lpszOutput, long data) Library "gdi32.dll" alias for "CreateDCA;Ansi"
Function long DeleteDC( long hdc) Library "gdi32.dll"
Function long GetDeviceCaps(long hdc,long x) Library "gdi32.dll"
Function long GetPrinterOrientation() Library "psconvrt.dll" 
Function long GetPrintDef(ref long orig ,ref long postscript) Library "psconvrt.dll" 
Function long GetPostscriptPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPostscriptPrinter;Ansi" 
Function long GetPrinterInfo( string printer,ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPrinterInfo;Ansi" 
Function long FindAddPostPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "FindAddPostPrinter;Ansi" 
Function long DeletePrinterDriverA(long name, long config, string driver) Library "winspool.drv" alias for "DeletePrinterDriverA;Ansi"
Function long GetLastError() Library "kernel32.dll"
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long  GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
Function long  CopyFileA( string path,string  desination,long stat ) Library "KERNEL32.DLL" alias for "CopyFileA;Ansi"
Function long GetPrintDefPCL(ref long orig ,ref long postscript) Library "pclcnvrt.dll" 
Function long GetPCLPrinter(ref string name,ref string drv,ref string port) Library  "pclcnvrt.dll" alias for "GetPCLPrinter;Ansi" 
 Function long GetPrinterOrientationPCL() Library "pclcnvrt.dll" 
Function long GetProcAddress(long hmod ,string procname) Library "KERNEL32.DLL" alias for "GetProcAddress;Ansi"
Function long GetWindowsDirectoryA(ref string var,long len) Library "kernel32.dll" alias for "GetWindowsDirectoryA;Ansi"
Function long LoadLibraryA(string libname) Library "KERNEL32.DLL" alias for "LoadLibraryA;Ansi"
end prototypes

type variables
protected:
s_option i_options[]
string i_report_type,i_special_note,i_time_option
string i_original_syntax,i_dw_name
longlong i_report_type_id,i_time_option_id,i_filter_id
longlong i_report_id
longlong i_debug
date i_start_date
date i_end_date
uo_datastore_template uo_dw
uo_datastore_template uo_dw_temp
uo_ps_web u_ps_interface
end variables

forward prototypes
public function integer uf_string_to_list (string a_str, ref string a_list[])
public function integer uf_string_to_num_list (string a_str, ref longlong a_list[])
public function integer uf_set_report_date (longlong a_report_type_id, datetime a_start_date, datetime a_end_date)
public function string uf_get_temp_dir ()
public function longlong uf_find_path (ref string a_path)
public function integer uf_wo_temp ()
private function s_option uf_get_option (string a_option_name)
public function longlong uf_save_report_to_db (string a_filename, longlong a_report_type, string a_name)
public function longlong uf_generate_report (string a_user, string a_password, string a_server, longlong a_report_type, string a_dw_name, longlong a_report_id, string a_options, string a_extra, string a_start_date, string a_end_date, longlong a_subsystem_id, ref string a_msg)
public function integer uf_modify_report (longlong a_report_id, ref string a_msg)
public function integer uf_retrieve_report (longlong a_subsystem_id)
public function boolean uf_check_option (string a_option_name)
private function s_retrieve_arg uf_parse_arg (string a_str)
public function integer uf_parse_options (string a_option_str)
public function integer uf_set_lib_list ()
public function longlong uf_get_debug ()
end prototypes

public function integer uf_string_to_list (string a_str, ref string a_list[]);longlong count = 0
longlong i
if len(a_str ) > 0 then
	count = 0	
	do while Pos( a_str, "~," ) > 0 
		i = Pos( a_str, "~," )
		count++
		a_list[count] = left( a_str, i - 1 )
		a_str = replace( a_str, 1, i, "" )
	loop	
	count++
	a_list[count] = a_str	
end if


return count
end function

public function integer uf_string_to_num_list (string a_str, ref longlong a_list[]);longlong count = 0
longlong i
if len(a_str ) > 0 then
	count = 0	
	do while Pos( a_str, "~," ) > 0 
		i = Pos( a_str, "~," )
		count++
		a_list[count] = long(left( a_str, i - 1 ))
		a_str = replace( a_str, 1, i, "" )
	loop	
	count++
	a_list[count] = long(a_str)	
end if


return count
end function

public function integer uf_set_report_date (longlong a_report_type_id, datetime a_start_date, datetime a_end_date);string	user_id, select_stmt, from_clause, sql_check, special_note
longlong	session_id, from_start, where_start, from_length
boolean	use_cpr_act_month



////
////	Set the report time different ways depending on the report type.
////
			SELECT	userenv( 'sessionid' )
			INTO		:session_id
			FROM		DUAL;

			choose case	a_report_type_id
				case	17			//	Detail - Asset
					////
					////	Delete the current user's report time from cpr_act_month.
					////
						DELETE FROM	cpr_act_month
							WHERE		ltrim( rtrim( upper( user_id ) ) )	=	upper( user )
								AND	session_id									=	userenv( 'sessionid' )
								AND	batch_report_id							=	0;
						COMMIT;
						
						//	delete old rows from cpr_act_month
						DELETE FROM	cpr_act_month
							WHERE	ltrim( rtrim( upper( user_id ) ) )	=	upper( user) and
							 to_char(time_stamp, 'yyyymmdd') < 	to_char(sysdate, 'yyyymmdd'); 
						COMMIT;

					////
					////	Insert the user's current time into cpr_act_month.
					////
						INSERT INTO	cpr_act_month (	month, user_id, batch_report_id, session_id	)
							VALUES	(	:a_start_date,			/*	month					*/
											user,				/*	user_id				*/
											0,							/*	batch_report_id	*/
											userenv( 'sessionid' )				/*	session_id			*/
										);   
						COMMIT;
				case	else
					////
					////	Delete the current user's report time from report_time.
					////
						DELETE FROM	report_time
							WHERE		ltrim( rtrim( upper( user_id ) ) )	=	user
								AND	session_id = userenv( 'sessionid' )	
								AND	batch_report_id	=	0;
						COMMIT;
						
					//	delete old rows from report_time
						DELETE FROM	report_time
							WHERE	ltrim( rtrim( upper( user_id ) ) )	=	user and
							to_char(time_stamp, 'yyyymmdd') < 	to_char(sysdate, 'yyyymmdd'); 
						COMMIT;

					////
					////	Update the current user's report time into report_time.
					////
						INSERT INTO	report_time (user_id, session_id, batch_report_id, start_month, end_month)
							VALUES	(	user,						/* user_id				*/
											userenv( 'sessionid' ),	/* session_id			*/
											0,									/* batch_report_id	*/
											:a_start_date,					/* start_month			*/
											:a_end_date						/* end_month			*/
											);
							COMMIT;

				
			
			end choose


return 1
end function

public function string uf_get_temp_dir ();string str
longlong status 
str = space(200)

status = GetEnvironmentVariableA('TEMP',str,200)
if status = 0 then
	status = GetLastError()
	return "c:\temp\"
end if

str = mid(str,1,status)
return str
end function

public function longlong uf_find_path (ref string a_path);longlong mod
string path
longlong i,status,len
string str
setnull(str)
mod =  GetModuleHandleA(str)
if mod <> 0 then
	path = space(601)
	status = GetModuleFileNameA(mod,path,600)
	if status <> 0 then
		len = status
		for i = len to 1 step -1
			if mid(path,i,1) = '\' then
				a_path = mid(path,1,i)
				return 1
			end if
		next
	end if
end if
return -1
end function

public function integer uf_wo_temp ();// delete old records from temp_work_order
delete from temp_work_order 
	where upper(user_id) = user and
		to_char(time_stamp, 'yyyymmdd') < to_char(sysdate, 'yyyymmdd');
if sqlca.sqlcode <> 0 then
   f_pp_msgs( "Error Deleting temp_work order Code = " + string(sqlca.sqlcode) + &
	       "~r~n Msg= " + sqlca.sqlerrtext)
end if
//f_check_sql_error(sqlca, "Error Deleting From temp_work_order")
commit;
if sqlca.sqlcode <> 0 then
   f_pp_msgs( "Error Commiting temp_work order Code = " + string(sqlca.sqlcode) + &
	       "~r~n Msg= " + sqlca.sqlerrtext)
end if
//  Delete the rows in temp_work_order for this session.
string temp_where_clause
string sqls
longlong i,id,rtn
temp_where_clause = &
	" upper(temp_work_order.user_id) = user and " + &
	" temp_work_order.session_id = userenv('sessionid') and " + &
	" temp_work_order.batch_report_id = 0 " 

sqls = "delete from temp_work_order where " + temp_where_clause

execute immediate :sqls;
if sqlca.sqlcode <> 0 then
   f_pp_msgs( "Error Deleting temp_work order Code = " + string(sqlca.sqlcode) + &
	       "~r~n Msg= " + sqlca.sqlerrtext)
end if
commit;
if sqlca.sqlcode <> 0 then
   f_pp_msgs( "Error Commiting temp_work order Code = " + string(sqlca.sqlcode) + &
	       "~r~n Msg= " + sqlca.sqlerrtext)
end if

	for i = 1 to uo_dw_temp.rowcount()

		//if uo_dw_temp.isselected(i) then
			 id = uo_dw_temp.getitemnumber(i,'work_order_id')
          insert into temp_work_order(user_id,session_id,batch_report_id,work_order_id)
			                     values(user,userenv('sessionid'),0,:id);

          if sqlca.sqlcode <> 0 then
				f_pp_msgs( "Error Inserting into temp_work order Code = " + string(sqlca.sqlcode) + &
						 "~r~n Msg= " + sqlca.sqlerrtext)
			 end if
		//end if

	next

	
   commit using sqlca;
	if sqlca.sqlcode <> 0 then
   	f_pp_msgs( "Error Commiting temp_work order Code = " + string(sqlca.sqlcode) + &
	       "~r~n Msg= " + sqlca.sqlerrtext)
	end if
	

	sqls = 'ANALYZE TABLE temp_work_order ESTIMATE STATISTICS' 

	execute immediate :sqls;
	return 0
end function

private function s_option uf_get_option (string a_option_name);s_option opt

longlong i
for i =1 to upperbound(i_options)
	if i_options[i].name = a_option_name then
		return i_options[i]
	end if
next 

return opt
end function

public function longlong uf_save_report_to_db (string a_filename, longlong a_report_type, string a_name);	// Get the file length, and open the file
	
longlong unit 
longlong id,len
longlong bytes_read 
blob data,indata
unit = FileOpen(a_filename, StreamMode!, Read!, Shared! )
	
	
	// Read the file
bytes_read = 1
len = 0
do while bytes_read > 0
	bytes_read = FileRead(unit,indata)
	if bytes_read > 0 then
	  data = data + indata
	  len += bytes_read
	end if
	
loop
FileClose(unit)
//FileDelete(filename)	  



// Save Report Data
select nvl(max(id),0) + 1 into :id from pp_batch_reports;

insert into pp_batch_reports(id,report_id,user_name,data_window,
                            report_type) values(:id,:i_report_id,user,:i_dw_name,:a_report_type);
if sqlca.sqlcode <> 0 then
		f_pp_msgs( "insert into pp_batch_report code=" + string(sqlca.sqlcode) + ' msg=' + sqlca.sqlerrtext)
end if
if i_debug = 1 then
		f_pp_msgs( 'Report len = ' + string(len))
end if
insert into pp_batch_reports_data(id,report_len) values(:id,:len);
if sqlca.sqlcode <> 0 then
		f_pp_msgs( "insert into pp_batch_report_data code=" + string(sqlca.sqlcode) + ' msg=' + sqlca.sqlerrtext)
end if

updateblob pp_batch_reports_data set report_data=:data where id=:id;
if sqlca.sqlcode <> 0 then
		f_pp_msgs( "updateblob pp_batch_report_data code=" + string(sqlca.sqlcode) + ' msg=' + sqlca.sqlerrtext)
end if

commit;
return id
end function

public function longlong uf_generate_report (string a_user, string a_password, string a_server, longlong a_report_type, string a_dw_name, longlong a_report_id, string a_options, string a_extra, string a_start_date, string a_end_date, longlong a_subsystem_id, ref string a_msg);

string passwd,title
integer i
longlong code 
string err,option,description
string dw_name
string dw_syntax 
string dw_data
longlong id,num
string syntax ,errors, sql, report_sql

string msg,filename,str
int unit
longlong bytes_read
longlong report_type
longlong rtncode
longlong len
longlong ids[]
string pp_report_id
longlong report_type_id
string pdf_file,temp_dir,fix_file
string dir
string report_number
string start_month
string report_filter = ""
string subdir = ""
longlong pos
s_retrieve_arg  s_retarg

//report_type = 5				
i_debug = uf_get_debug() 		


temp_dir = g_ps_interface.uf_get_temp_dir()

u_prog_interface = create  u_prog_interface
f_pp_msgs( "Starting Batch Reporter " + string(today()) + ' ' + string(now()) )


if i_debug = 1 then
	f_pp_msgs(  &
					"User        : " + a_user + '~r~n' + &
					"Password    : " + a_password + '~r~n' + &
					"Server      : " + a_server + '~r~n' + &
					"Report Type : " + string(a_report_type) + '~r~n' + &
					"Dw          : " + a_dw_name + '~r~n' + &
					"Report ID   : " + string(a_report_id) + '~r~n' + &
					"Options     : " + a_options + '~r~n' + &
					"Extra       : " + a_extra + '~r~n' + &
					"Start Date  : " + a_start_date + '~r~n' + &
					"End Date    : " + a_end_date + '~r~n' + &
					"SubSystem   : " + string(a_subsystem_id) + '~r~n' )
				
	
end if 
// Login to the database
sqlca.dbms ='o90'	
sqlca.dbparm="staticbind=0,disablebind=1"
if mid(a_server,1,1) = '@' then 
   sqlca.servername = a_server
else
	 sqlca.servername = '@' + a_server
end if
sqlca.logid= a_user
sqlca.logpass = a_password
connect;
if sqlca.sqlcode <> 0 then
	a_msg = 'Error Connecting Code = ' + string(sqlca.sqlcode) + &
	       ' Msg=' +   sqlca.sqlerrtext 
	f_pp_msgs( a_msg )
	return -1
end if

uf_set_lib_list()

// JAK 20081005:  Added a PP System Control to determine whether the file name is added onto
//		the end of the file name when saving a report.  If it is set to yes, the file will be saved as
//		Report_Name_mmyyyy.pdf otherwise it will be Report_Name.pdf etc.
string add_date 
add_date = f_pp_system_control_company('Batch Report - Add Month', -1)
if add_date <> 'no' then add_date = 'yes'

if i_debug=1 then
   f_pp_msgs( a_dw_name)
end if

select report_type_id ,datawindow into :report_type_id,:dw_name
                  from pp_reports where report_id = :a_report_id;
if sqlca.sqlcode <> 0 then
	a_msg = 'Error Selecting pp_reports Code = ' + string(sqlca.sqlcode) + &
	       ' Msg=' +   sqlca.sqlerrtext 
	f_pp_msgs( a_msg)
   return -1
end if

string rc
rc = GetLibraryList ( )
f_pp_msgs( "liblist=" + rc)
uo_dw.dataobject = dw_name
uo_dw.setTransObject( sqlca )

//if i_debug = 1 then 
//	rc = uo_dw.describe( "datawindow.syntax")
//	f_pp_msgs( "rc=" + rc)
//end if
num = uf_parse_options(a_options)
if num <> 0 then
	f_pp_msgs( a_options)
	if uf_modify_report(a_report_id,msg) = -1 then
		a_msg = "Modify " + msg
		f_pp_msgs( a_msg)
		
		return -1
	end if
end if

if a_start_date <> '' then
	i_start_date = date(a_start_date)
else
	setnull(i_start_date)
end if
if a_end_date <> '' then
	i_end_date = date(a_end_date)
else
	setnull(i_end_date)
end if

uf_set_report_date(report_type_id,datetime(date(a_start_date),time(0)) , &
                                  datetime(date(a_end_date),time(0)) )




// uf_retrieve_report
rtncode = uf_retrieve_report(a_subsystem_id)
//rtncode=uo_dw.retrieve()						
if rtncode = -1 then
	a_msg = 'Error Retrieve Code = ' + string(uo_dw.i_sqlca_sqlcode) + &
	       ' Msg=' +uo_dw.i_sqlca_sqlerrtext 
	f_pp_msgs( a_msg )
end if

if i_debug = 1 then
	num = uo_dw.rowcount()
   f_pp_msgs( 'Number of Rows = ' + string(num) )
end if
if i_debug = 1 then
   f_pp_msgs( 'Retrieve Finished')
end if


//uo_dw.modify( "company_name.text = '" + subtitle + "'")
//uo_dw.modify("report_number.text =  '" + report_number + "'")
	

report_type = a_report_type
if report_type = 1 then     // PSR File Type
	filename = temp_dir + "\" + "batrpt.psr"
	uo_dw.SaveAs(filename,psreport!,true)
	uf_save_report_to_db(filename,report_type,'')
	
elseif report_type = 2 then   // Print Directly
	if i_debug = 1 then
		msg = uo_dw.describe('datawindow.printer')
		f_pp_msgs( msg)
	end if

	//uf_set_fonts_pcl(uo_dw)
	filename = temp_dir + "\" +  title
	uo_dw.object.datawindow.print.filename = filename
	uo_dw.modify("DataWindow.print.Orientation = '1'")
	uo_dw.print()
   // Save Report Data
   uf_save_report_to_db(filename,report_type,'')

elseif report_type = 3 then // Save PostScript File
	
	filename = temp_dir + "\" + "batrpt.ps"
	pdf_file =  temp_dir + "\" + "batrpt.pdf"
	if i_debug = 1 then
		msg = uo_dw.describe('datawindow.printer')
		f_pp_msgs( msg)
	end if
	uo_dw.object.datawindow.print.filename = filename
	uo_dw.modify("DataWindow.print.Orientation = '1'")
	uo_dw.print()
   uf_save_report_to_db(filename,report_type,'')
	
elseif report_type = 4 then // Save Locally as a Fix file format
	u_ps_interface = create uo_ps_web	
	u_ps_interface.i_debug = i_debug
	if add_date = 'yes' then
		fix_file =  temp_dir + "\"  + report_number + string(i_start_date, 'mmyyyy') + '.FIX'
	else
		fix_file =  temp_dir + "\"  + report_number + '.FIX'
	end if
	if i_debug = 1 then
		f_pp_msgs( "Creating print file " + fix_file)
		msg = uo_dw.describe('datawindow.printer')
		f_pp_msgs( msg)
	end if
	if u_ps_interface.uf_print_file_pcl(uo_dw) <> "" then 
		   f_pp_msgs( "Error printing pcl file")
         return -1		
	end if
	filename = temp_dir + '\pcltemp.pcl'
	u_ps_interface.uf_saveas_fix_pcl(filename,fix_file)
	
	destroy u_ps_interface
	uf_save_report_to_db(fix_file,report_type,'')

elseif report_type = 5 then    // Save Locally as a PDF file format
	u_ps_interface = create uo_ps_web
	u_ps_interface.i_debug = i_debug
	if add_date = 'yes' then
		fix_file =  temp_dir + "\" + report_number + string(i_start_date, 'mmyyyy') + '.pdf'
	else
		fix_file =  temp_dir + "\" + report_number + '.pdf'
	end if	
	if i_debug = 1 then
		f_pp_msgs( "Creating print file " + fix_file)
		msg = uo_dw.describe('datawindow.printer')
		f_pp_msgs( msg)
	end if
	if u_ps_interface.uf_print_file(uo_dw) <> "" then 
      msg = 'Error on print file'
		f_pp_msgs( msg)
	end if
	filename =  temp_dir + '\pstemp.ps'
	if i_debug = 1 then f_pp_msgs( "Save AS PDF " + filename )
	u_ps_interface.uf_saveas_pdf(filename,fix_file)
	
	
	if i_debug = 1 then f_pp_msgs( "Save to DBF " + fix_file )
	id = uf_save_report_to_db(fix_file,report_type,'')
	
elseif report_type = 6 then // Save As Ascii
	// JAK  20081005:  Changed 'dir' variable to 'temp_dir' to be consistent with other file types
	//   'dir' variable was not populated above.
	if add_date = 'yes' then
		fix_file = temp_dir + '\' + report_number + string(i_start_date, 'mmyyyy') + '.asc'
	else
		fix_file = temp_dir + '\' + report_number + '.asc'
	end if	
	uo_dw.saveasascii(fix_file,i_report_type,'')
	if i_debug = 1 then f_pp_msgs( "Save to DBF " + fix_file )
	id = uf_save_report_to_db(fix_file,report_type,'')
else
	dw_data = uo_dw.Object.DataWindow.Syntax.Data
	blob data
	data = blob(dw_syntax + dw_data)
	updateblob pp_batch_reports_data set report_data=:data where id=:id;
	if sqlca.sqlcode <> 0 then
		 a_msg = 'UpdateBlob pp_batch_reports Error = ' + sqlca.sqlerrtext + ' ' + string(sqlca.sqlcode) 
		 return -1 
	end if		
end if
a_msg = 'OK'
return id 
////	
end function

public function integer uf_modify_report (longlong a_report_id, ref string a_msg);//
string where_clause

longlong rtn
string sql_stmt
s_option option
s_option option2
select report_type,special_note,time_option,report_type_id,pp_report_time_option_id,pp_report_filter_id,datawindow
      into :i_report_type,:i_special_note,:i_time_option,:i_report_type_id,:i_time_option_id,:i_filter_id,
		     :i_dw_name
            from pp_reports where report_id = :a_report_id;
				
string rc			
longlong i_funding_ind = 0

if sqlca.sqlcode <> 0 then
	a_msg = "Code = " + string(sqlca.sqlcode) + " Msg=" + sqlca.sqlerrtext
	return -1
end if

uo_dw_temp = create uo_datastore_template
//
//
//////
//////	For all reports.
//////
//	string	select_stmt	=	"",	&
//				where_stmt	=	" ",	&
//				dw_stmt		=	""
//	string	rtn_str, string_array[], replace_str
//	longlong	tmp_pos, i, max, null_long_array[]
//
//////
//////	For 'Main - Unit Catalog' reports.
//////
//	string	prop_where
//
//////
//////	For 'Detail - Asset' (CPR) reports.
//////
//	string	tabs_where, tabs_select, user, replace_string
//	longlong	tabs_pos, tabs_pos1, session_id, tabs_asset_id,	&
//				tmp_asset_id, tabs_act_status, asset_act_id
//
//////
//////	For 'Detail - Subledger' reports.
//////
//	string	subl_name, subl_sql, subl_temp, subl_table,	&
//				subl_where
//	string	subl_items[]
//	longlong	subl_id, subl_depr_ind, subl_count
//
//////
//////	For 'Main - Asset' and 'Main - Depreciation' reports.
//	date		closed_start, closed_end
//
//////
//////	For Budget subsystem and FP subsystem.
//////
//	longlong	bud_year, bud_month
//	string	bud_vers, bud_where, fp_where, bud_years_str
//
//////
//////	For 'Main - Depr Study' reports.
//////
//	string	depr_study_where
//
//////
//////	For batch to work.
//////
//	string	options, retrieve_array, retrieve_str
//
//
//
//////
//////	Make sure the datawindow has the original syntax.  (This is in case we've
//////	filtered it one way and now we're trying to filter it another way.  Otherwise,
//////	the old filter criteria would still be in the where clause.)
//////
//
//			
//
//      
//
////
////	Get the where clause.

 setnull(where_clause)
 
 
 //i_original_syntax = uo_dw_temp.object.datawindow.table.select
 
 // make sure that account summary reports have one set of books selected
			if i_filter_id = 2 and uf_check_option('account_summary') = false  then
				where_clause = ' and account_summary.set_of_books_id = 1'
			end if
			
		
			
//  // make sure that depr ledger  has one set of books selected
//     
			if i_filter_id = 5 and isnull(where_clause)  then
			
			   
			  if isnull(i_special_note) or &
			    ( not isnull(i_special_note) and &
				  (lower(i_special_note) <> 'no set of books') and lower(i_special_note) <> 'subledger') then
				   if i_special_note = 'DEPR_VINTAGE_SUMMARY' or i_special_note = 'DEPR_METHOD_RATES' then
						
			 	   where_clause = ' and ' + i_special_note  + '.set_of_books_id = 1'
						
				   else
						if pos(upper(i_original_syntax) , 'FCST_DEPR_LEDGER') > 0 then
			 	     		 where_clause = ' and fcst_depr_ledger.set_of_books_id = 1'
						elseif pos(upper(i_original_syntax) , 'FCST_TAX_DEPR') > 0 then
							 where_clause = ' and fcst_tax_depr.set_of_books_id = 1'
						else
							 where_clause = ' and depr_ledger.set_of_books_id = 1'
						end if
						
				   end if
			
		     end if
		
			end if
			
			if i_filter_id = 5 and uf_check_option('set_of_books_id') = false then 
				
				if isnull(i_special_note) or &
			      ( not isnull(i_special_note) and &
					(lower(i_special_note) <> 'no set of books' and lower(i_special_note) <> 'subledger') )  then
					 if i_special_note = 'DEPR_VINTAGE_SUMMARY' or i_special_note = 'DEPR_METHOD_RATES' then
					   where_clause =  where_clause + ' and ' + i_special_note  + '.set_of_books_id = 1'
				   else						
				      where_clause = where_clause + ' and depr_ledger.set_of_books_id = 1' 
				   end if
					 
				end if
			end if	
//			
////			
////	 
//		
//////
//			where_stmt = i_where_report_time +  where_clause
//
////////
////////	Perform different steps based on the subsystem and calling window.  The order is
////////	important here.  We first check the calling window and process the report based
////////	on the caller.  Then, if the calling window has no special instructions, we check
////////	the subsystem that we're working in to see if we need to do any special processing.
////////
			choose case	i_report_type_id
				case	 1		//	'Main - Unit Catalog'
					////
					////	Lets see what report we're retrieving.
					////
//						choose case	i_dw_name
//							case	'dw_prop_unit_report' 
//								
//								if i_props = 'all' then
//									if pos(upper(w_prop_unit_maint.i_dw_4_sqls), 'UTIL_ACCT_PROP_UNIT') > 0 then
//										prop_where = ' ,UTIL_ACCT_PROP_UNIT where ' + w_prop_unit_maint.i_dw_4_sqls
//									else
//										prop_where = ' where ' + w_prop_unit_maint.i_dw_4_sqls
//									end if
//								elseif i_props <> 'none' then
//									prop_where = ' where property_unit.property_unit_id in ( ' + &
//														i_props + ' )'
//								end if
//								
//								if pos( lower( prop_where ), 'retire_method', 1 ) > 0 then
//									dw_report.dataobject = i_dw_name + '_b'
//									dw_report.EVENT	pp_format()
//								end if
//								
//								f_add_where_clause( dw_report, '', prop_where, false ) 
//								i_props = ''
//								
//							case	'dw_retire_unit_prop_unit_report'
//								if i_props = 'all' then
//									prop_where = ' and ' + w_prop_unit_maint.i_dw_4_sqls
//								elseif i_props <> 'none' then
//									prop_where = ' and property_unit.property_unit_id in ( ' + &
//														i_props + ' )'
//								end if
//								
//								if pos( lower( prop_where ), 'retire_method', 1 ) > 0 then
//									dw_report.dataobject = i_dw_name + '_b'
//									dw_report.EVENT	pp_format()
//								end if
//								
//								f_add_where_clause( dw_report, '', prop_where, false )
//								i_props = ''
//						end choose
				
				case	2			//	'Main - Funding Project'
										
				case	3			//	'Main - Work Order'
				case	10			//	'Detail - Work Order'
					string sqls =''
					string view_sqls = ''
					string fp_view_sqls = ''
					fp_view_sqls = " and work_order_control.funding_wo_id in " + &
						"(select work_order_id from work_order_control where funding_wo_indicator = 1 "
						
					/********************************* Work Order No *****************************/
					if uf_check_option('Work Order') then
	
//						if g_work_info.funding_wo = 'yes'  &
//						or g_work_info.funding_wo = "budgetdrill" &
//						or (g_work_info.funding_wo <> 'yes' and g_work_info.caller = 'add')  &
//						or (g_work_info.funding_wo <> 'yes' and g_work_info.caller = 'summary')  then
//							dw_grid.DataObject = "dw_fp_grid_wo_id"
//							//  If retrieving funding projects, the i_select_sql variable
//							//  must be set again ... for work orders, it gets set correctly in
//							//  the open script.
//							i_select_sql_wo_id = "datawindow.table.select = ~"" + &
//														 dw_grid.Describe("datawindow.table.select")
//						else
							uo_dw_temp.DataObject = "dw_work_order_grid_wo_id"
							uo_dw_temp.settransobject(sqlca)
							i_original_syntax = "datawindow.table.select = ~"" + &
												 uo_dw_temp.Describe("datawindow.table.select")
//						end if	
						
	
						
					else
						
//						if g_work_info.funding_wo = 'yes'  &
//						or g_work_info.funding_wo = "budgetdrill" &
//						or (g_work_info.funding_wo <> 'yes' and g_work_info.caller = 'add')  &
//						or (g_work_info.funding_wo <> 'yes' and g_work_info.caller = 'summary')  then
//							dw_grid.DataObject = "dw_fp_grid"
							//  If retrieving funding projects, the i_select_sql variable
							//  must be set again ... for work orders, it gets set correctly in
							//  the open script.
							uo_dw_temp.DataObject = "dw_work_order_grid"
							uo_dw_temp.settransobject(sqlca)
							i_original_syntax = "datawindow.table.select = ~"" + &
												 uo_dw_temp.Describe("datawindow.table.select")
//						end if	
						
	
						
					end if
					/********************************** Company ********************************/
					if uf_check_option('Company') then	
					  option = uf_get_option('Company')
					  sqls      = sqls + ' and work_order_control.company_id in (' + &
	                                       option.str + ')'
	              view_sqls = view_sqls + ' and work_order_control.company_id in (' + &
	                          option.str + ')'
	              fp_view_sqls = fp_view_sqls + ' and work_order_control.company_id in (' + &
	                          option.str + ')'
				   end if
					
					/********************************** Funding Project ********************************/
					if uf_check_option('Funding Project') then
						      option = uf_get_option('Funding Project')
								sqls      = sqls + ' and work_order_control.funding_wo_id in (' + &
		                  								  option.str + ')'
								view_sqls = view_sqls + ' and work_order_control.funding_wo_id in (' + &
																  option.str + ')'
								fp_view_sqls = fp_view_sqls + ' and work_order_control.funding_wo_id in (' + &
																  option.str + ')'
					end if
					
					
					/********************************** Budget ********************************/
					if uf_check_option('Budget') then					
							option = uf_get_option('Budget')
							sqls      = sqls + ' and work_order_control.budget_id in (' + &
																option.str + ')'
							view_sqls = view_sqls + ' and work_order_control.budget_id in (' + &
															   option.str + ')'
							fp_view_sqls = fp_view_sqls + ' and work_order_control.budget_id in (' + &
										  						option.str + ')'
				   end if
					longlong sql_blocks,i,start_pos,end_pos
					longlong j
					string sql_block
				   /********************************** Location ********************************/
					if uf_check_option('Location') then		
						   option = uf_get_option('Location')	
							sql_blocks = ceiling(option.len/254)							
							if sql_blocks > 1 then
								sqls      = sqls + ' and ( '
								view_sqls = view_sqls + ' and ( '
								fp_view_sqls = fp_view_sqls + ' and ( '						
								for i = 1 to sql_blocks
									start_pos = ((i - 1) * 254) + 1								
									if i < sql_blocks then
										end_pos = start_pos + 253
									else
										end_pos = option.len
									end if
									
									sql_block = ""
									for j = start_pos to end_pos
										sql_block = sql_block + string(option.numarray[j])
										if j < end_pos then
											sql_block = sql_block + ','
										end if
									next
									
									sqls      = sqls + 'work_order_control.asset_location_id in (' + &
															  sql_block + ')'
									view_sqls = view_sqls + 'work_order_control.asset_location_id in (' + &
																	 sql_block + ')'
									fp_view_sqls = fp_view_sqls + 'work_order_control.asset_location_id in (' + &
																	 sql_block + ')'
										
									if i < sql_blocks then
										sqls      = sqls + ' or '
										view_sqls = view_sqls + ' or '
										fp_view_sqls = fp_view_sqls + ' or '
									end if 
								next
								
								sqls      = sqls + ')'
								view_sqls = view_sqls + ')'
								fp_view_sqls = fp_view_sqls + ') '
								
							else
								sqls      = sqls + ' and work_order_control.asset_location_id in (' + &
															option.str + ')'
								view_sqls = view_sqls + ' and work_order_control.asset_location_id in (' + &
															option.str +  + ')'
								fp_view_sqls = fp_view_sqls + ' and work_order_control.asset_location_id in (' + &
															option.str +  + ')'
							end if
								
					end if
					
					/********************************** Deparment ********************************/
					if uf_check_option('Deparment') then
						 option = uf_get_option('Deparment')
						 	if i_funding_ind = 1 then
							sqls      = sqls + " and work_order_control.work_order_id in (" + &
													 "select distinct work_order_id from wo_est_summary where " + &
													 "department_id in (" + option.str + '))'
							view_sqls = view_sqls + " and work_order_control.work_order_id in (" + &
													 "select distinct work_order_id from wo_est_summary where " + &
													 "department_id in (" + option.str + '))'
							fp_view_sqls = fp_view_sqls + " and work_order_control.work_order_id in (" + &
													 "select distinct work_order_id from wo_est_summary where " + &
													 "department_id in (" + option.str + '))'
					
						else	
							sqls      = sqls + " and work_order_control.work_order_id in (" + &
													 "select distinct work_order_id from " + &
													 "work_order_department where " + &
													 "department_id in (" + option.str + '))'
							view_sqls = view_sqls + " and work_order_control.work_order_id in (" + &
													 "select distinct work_order_id from " + &
													 "work_order_department where " + &
													 "department_id in (" + option.str + '))'
							fp_view_sqls = fp_view_sqls + " and work_order_control.work_order_id in (" + &
													 "select distinct work_order_id from " + &
													 "work_order_department where " + &
													 "department_id in (" + option.str + '))'
						end if 
				   end if
					
					/********************************** Work Group ********************************/
					if uf_check_option('Work Order Group') then		
						   option = uf_get_option('Work Order Group')
							sqls = sqls + ' and work_order_account.work_order_grp_id in (' + option.str + ')'
					end if
					/********************************** Work Order Type ********************************/
					if uf_check_option('Work Order Type') then		
						   option = uf_get_option('Work Order Type') 
					 		sqls      = sqls + ' and work_order_control.work_order_type_id in (' + &
	                     			option.str + ')'
							view_sqls = view_sqls + ' and work_order_control.work_order_type_id in (' + &
	                          		option.str + ')'
							fp_view_sqls = fp_view_sqls + ' and work_order_control.work_order_type_id in (' + &
	                          		option.str + ')'
					end if
					
               /********************************** Class Code ********************************/
					if uf_check_option('Class Code') then	
						option = uf_get_option('Class Code')
						option2 = uf_get_option('Class Code ID')
						string class_code_ids = ""
						sqls = sqls	+ ' and work_order_control.work_order_id in ' + & 
								'(select distinct work_order_id from ' + &
								'work_order_class_code where (work_order_class_code.class_code_id = ' + &
								option2.str + ' and work_order_class_code.value in ( ' + &
								option.str + ')))'
						view_sqls = view_sqls + ' and work_order_control.work_order_id in ' + & 
								'(select distinct work_order_id from ' + &
								'work_order_class_code where (work_order_class_code.class_code_id = ' + &
								option2.str + ' and work_order_class_code.value in ( ' + &
								option.str + ')))'
						fp_view_sqls = fp_view_sqls + ' and work_order_control.work_order_id in ' + & 
								'(select distinct work_order_id from ' + &
								'work_order_class_code where (work_order_class_code.class_code_id = ' + &
								option2.str + ' and work_order_class_code.value in ( ' + &
								option.str + ')))'
					end if

				//Status Code...
				if uf_check_option('Status Code') then	
						option = uf_get_option('Status Code')

						sqls      = sqls + ' and work_order_control.wo_status_id = ' + &
													option.str
						view_sqls = view_sqls + ' and work_order_control.wo_status_id = ' + &
														  option.str
						fp_view_sqls = fp_view_sqls + ' and work_order_control.wo_status_id = ' + &
														  option.str
				end if

				//ARO WOs
				if uf_check_option('ARO WOs') then	
						option = uf_get_option('ARO WOs')
					if option.numarray[1] = 1 then
						sqls = sqls + ' and work_order_control.work_order_id in (select work_order_id from aro_work_order) '
					elseif option.numarray[1]  = 2 then
						sqls = sqls + ' and work_order_control.work_order_id not in (select work_order_id from aro_work_order) '
					end if
				end if
				
				//Initiator...
				
				if uf_check_option('Initiator')  then
					option = uf_get_option('Initiator')
					sqls = sqls + ' and pp_security_users.description = ' + "'" +  &
										 option.str + "'"
				end if
				
				//Reimbursable Type...
				if uf_check_option('Reimbursable Type') then
					option = uf_get_option('reimbursable_type_id')
					sqls = sqls + ' and work_order_account.reimbursable_type_id = ' + &
										 option.str
				end if
				longlong view_pos
				string left_sql,right_sql,select_sql
				select_sql = i_original_syntax		
            if uo_dw_temp.DataObject = "dw_work_order_grid" or uo_dw_temp.DataObject = "dw_fp_grid" then
					view_pos  = pos(select_sql, "= charge_summary.work_order_id")
					view_pos  = view_pos + 34
			
					left_sql  = left(select_sql,view_pos)
					right_sql = right(select_sql, len(select_sql) - view_pos - 1)
			
					if uo_dw_temp.DataObject = "dw_fp_grid" then
						select_sql = left_sql + " " + fp_view_sqls + " " + right_sql
					else
						select_sql = left_sql + " " + view_sqls + " " + right_sql
					end if			
				end if
            sqls = select_sql + sqls
				rc = uo_dw_temp.modify( sqls + '"')
				if rc = "" then
					if uf_check_option('Work Order No') then
							option =uf_get_option('Work Order No')
							rtn = uo_dw_temp.RETRIEVE(option.numarray)
					else
							uo_dw_temp.RETRIEVE()
					end if
					if rtn = -1 then
						f_pp_msgs( "Error retrieving temp datawindow.")
						return -1
					end if
				else
					f_pp_msgs( "Error Modifing Temp Datawindow Msg = " + rc)
            end if
				uf_wo_temp()
				//**************************** Property Tax **********************************************/
			case 9      
				choose case i_filter_id
					case 7 // Property Tax Ledger
					
						sqls = ''
		
						if uf_check_option('Company') then	
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Company')
							  sqls = sqls + f_parseArrayBy254( option.numarray, "N", &
									"property_tax_ledger_control.company_id" )
						end if
	
						 if uf_check_option('State') and uf_check_option('Tax District') then	
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('State')
							  sqls      = sqls +  f_parseArrayBy254(option.strarray, "C", &
									"property_tax_ledger_control.state_id" )								
						 end if
	
						 if uf_check_option('County') and uf_check_option('Tax District') then
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('County')
							  sqls      = sqls +  f_parseArrayBy254( option.strarray, "C", &
									"property_tax_ledger_control.county_id" )								
						 end if
						 
						 
	
						  if uf_check_option('Tax District') then	
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Tax District')
							  sqls      = sqls + f_parseArrayBy254( option.numarray, "N", &
									"property_tax_ledger_control.tax_district_id" )								
						 end if
	
	
						 if uf_check_option('Property Tax Location') then
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Property Tax Location')	
							  sqls      = sqls +  f_parseArrayBy254( option.numarray, "N", &
									"property_tax_ledger_control.prop_tax_location_id" )								
						 end if
	
						 if uf_check_option('Method') then	
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Method')
							  
							  sqls      = sqls + ' and method_id in (' + &
																option.str + ')'					 
						 end if
	
						 if uf_check_option('Property Tax Type') then
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Property Tax Type')
							  sqls      = sqls + ' and property_tax_type_data.property_tax_type_id in (' + &
																option.str + ')'	
							 	sqls      = sqls +  f_parseArrayBy254( option.numarray, "N", &
									"property_tax_ledger_control.property_tax_type_id" )
						 end if
	
	
						 if uf_check_option('Property Tax Authority') then
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Property Tax Authority')
							  sqls      = sqls + ' and property_tax_auhtority.tax_authority_id in (' + &
																option.str + ')'					 
						 end if
	
	
						 if uf_check_option('Business Segment') then	
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Business Segment')
							 
							  sqls      = sqls +  f_parseArrayBy254( option.numarray, "N", &
									"property_tax_ledger_control.bus_segment_id" )
						 end if
	
						 if uf_check_option('Utility Account') then	
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('Utility Account')
								sqls      = sqls +  f_parseArrayBy254( option.numarray, "N", &
									"property_tax_ledger_control.utility_account_id" )								
						 end if
	
						  if uf_check_option('GL Account') then	
							  if sqls <> "" then sqls = sqls + " and "
							  option = uf_get_option('GL Account')
								sqls      = sqls +   f_parseArrayBy254(option.numarray, "N", &
									"property_tax_ledger_control.gl_account_id" )
						  end if
	
					      longlong null_vintage_count
		 					SELECT	count(*) into :null_vintage_count  FROM		property_tax_ledger_control 
							         where vintage is null;
		 					if uf_check_option('Vintage') then
								option = uf_get_option('Vintage')
								if null_vintage_count> 0  then
									if sqls <> "" then sqls =sqls + " and "								   
										sqls = sqls + "( " + f_parseArrayBy254( option.numarray, "N", &
										"property_tax_ledger_control.vintage" ) + " or " + &
										"property_tax_ledger_control.vintage is null ) "
								else
									if  sqls  <> "" then  sqls  =  sqls  + " and "
										sqls      = sqls + f_parseArrayBy254( option.numarray, "N", &
														"property_tax_ledger_control.vintage" )
									end if
							else
//								if null_vintage_count > 0 then
//									if sqls <> "" then sqls = sqls + " and "
//										sqls = sqls + "property_tax_ledger_control.vintage is null "
//								end if
							end if
						
						  
			////
//////	USER INPUT WHERE CLAUSE
//////
//			if not isNull( i_user_input ) then
//				if where_clause <> "" then where_clause = where_clause + " and "
//				where_clause = where_clause + "property_tax_ledger_control.user_input = " + &
//									string( i_user_input )
//			end if
               if i_debug = 1 then
						f_pp_msgs( 'Where = ' + sqls )
					end if
               sql_stmt = uo_dw.describe("Datawindow.table.select")
					if sqls <> '' then
					   sql_stmt = f_replace_string( sql_stmt, "5=5", sqls, "all" )
					end if
					sql_stmt = f_replace_string( sql_stmt, "'", "~~'", "all" )
					
					sql_stmt = "datawindow.table.select='" + sql_stmt + "'"
					if i_debug = 1 then
						f_pp_msgs( 'SQL = ' + sql_stmt )
					end if
					rc = uo_dw.modify( sql_stmt)
					if rc <> ''  then
						f_pp_msgs( 'Modify Error = ' + rc )
					end if
					case 8        // Property Tax Statement
					case 9        // Property Tax Parcel
				end choose
				
//				case	4, 16, 35, 36		//	'Main - Budget' or 'Detail - Budget'
//					
//					//  DMJ: Re-written ...
//					string bud_version
//					// ?bud_vers = f_build_string( tab_main.tabpage_selection.dw_budget )
//					
//					if i_report_type_id = 4 or i_report_type_id = 16 then
//						if not isNull( bud_vers ) and trim( bud_vers ) <> '' then
//							bud_where = " and budget_amounts.budget_version_id in (" + bud_vers + ") "
//						end if
//					else
//						if not isNull( bud_vers ) and trim( bud_vers ) <> '' then
//							bud_where = " and work_order_approval.budget_version_id in (" + bud_vers + ") "
//						end if
//					end if
//					
//					choose case	i_time_option_id
//						case	3	//	'Budget - Annual'
//							bud_years_str = f_build_lb_string( tab_main.tabpage_selection.lb_year )
//							
//							if i_report_type_id = 4 or i_report_type_id = 16 then
//								if not isNull( bud_years_str ) and bud_years_str <> '' then
//									bud_where = bud_where + " and budget_monthly_data.year in (" + bud_years_str + ") "
//								end if
//							else
//								if not isNull( bud_years_str ) and bud_years_str <> '' then
//									bud_where = bud_where + " and wo_est_monthly.year in (" + bud_years_str + ") "
//								end if
//							end if
//							
//							f_add_where_clause_unions( dw_report, '', bud_where, false )
//							
//						case	4	//	'Budget - Monthly'
//							bud_year = long( tab_main.tabpage_selection.lb_year.selectedItem() )
//							
//							if isNull( bud_year ) or bud_year < 1 then
//								messageBox( 'Error', 'This report requires a year to be selected.' )
//								return
//							end if
//							
//							bud_month = tab_main.tabpage_selection.lb_year.selectedIndex()
//							if bud_month < 1 then bud_month = 1
//							
//						case else
//							f_add_where_clause_unions( dw_report, '', bud_where, false )
//					end choose
//					
//				case	5			//	'Main - Asset'
//					//	elseif g_subsystem = 'Asset Management' then
//					////
//					////	Will we report on uncommited conversion assets?
//					////
//						if pos( upper(i_original_syntax), 'CPR_LEDGER' ) > 0 then
//
//							if f_pp_system_control( 'report conversion assets' ) <> 'yes' then
//								
//								f_add_where_clause( tab_main.tabpage_preview.dw_report, "", &
//															" and cpr_ledger.ledger_status < 100 ", false )
//							end if
//						end if
//	
//					////
//					////	Give a warning for summary reports on open months.
//					////
//						if pos( upper( i_original_syntax), 'ACCOUNT_SUMMARY' ) > 0 then 
//			
//							SELECT	cpr_closed
//							INTO		:closed_start 
//							FROM		cpr_control 
//							WHERE		to_char( accounting_month, 'mm/yyyy' ) = 
//														to_char( :i_start_date, 'mm/yyyy' )
//								AND	company_id = 1;
//		
//							SELECT	cpr_closed
//							INTO		:closed_end
//							FROM		cpr_control 
//							WHERE		to_char( accounting_month, 'mm/yyyy' ) = 
//														to_char( :i_end_date, 'mm/yyyy' )
//								AND	company_id = 1;
//							
//							choose case	i_time_option_id
//								case	1		//	Single Month
//									if isNull( closed_start ) then
//										messageBox( "Warning", "Summary Reports on Open Months " + &
//														"do not Include Final Balances." )						
//									end if
//								case	2		//	Span
//									if isNull( closed_start ) OR isNull( closed_end ) then
//										messageBox( "Warning", "Summary Reports on Open Months " + &
//														"do not Include Final Balances." )
//									end if
//							end choose
//						end if
//						
//					////
//					////	Check to see if the basis buckets need to be labeled.
//					////
//						if i_special_note = 'BASIS' then
//							tab_main.tabpage_preview.dw_basis_labels.setTransObject( sqlca )
//							tab_main.tabpage_preview.dw_basis_labels.retrieve()
//							f_label_buckets( tab_main.tabpage_preview.dw_report, &
//													tab_main.tabpage_preview.dw_basis_labels )
//						end if
//				
//				case	7			//	'Main - Depr Study'
//					//	elseif left( g_subsystem, 10 ) = 'Depr Study' then
//					if isValid( w_input_transactions ) then
//						depr_study_where = " and analysis_account.analysis_account_id = " + &
//											string( w_input_transactions.i_account_id ) + " and " + &
//											"analysis_version.analysis_version_id = " + string( &
//											w_input_transactions.i_version_id )
//						
//						f_add_where_clause( tab_main.tabpage_preview.dw_report, "", &
//									depr_study_where, false )
//					end if
//				
//				case	8			//	'Main - Depreciation'
//					//	elseif g_subsystem = 'Depreciation' then
//					////
//					////	Give a warning for summary reports on open months.
//					////
//						if pos( upper( tab_main.tabpage_preview.dw_report.describe( &
//												"datawindow.table.select" ) ), 'DEPR' ) > 0 then 
//							
//							SELECT	depr_calculated
//							INTO		:closed_start
//							FROM		cpr_control 
//							WHERE		to_char( accounting_month, 'mm/yyyy' ) = 
//													to_char( :i_start_date, 'mm/yyyy' )
//								AND	company_id = 1;
//							
//							SELECT	depr_calculated
//							INTO		:closed_end
//							FROM		cpr_control
//							WHERE		to_char( accounting_month, 'mm/yyyy' ) = 
//													to_char( :i_end_date, 'mm/yyyy' )
//								AND company_id = 1;
//							
//							choose case	i_time_option_id
//								case	1		//	Single Month
//									if isNull( closed_start ) then
//										messageBox( "Warning", "Depreciation Reports on Open " + &
//													"Months or Months where Depreciation has not " + &
//													"been Calculated do not Include Final Balances." )
//									end if
//								case	2		//	Span
//									if isNull( closed_start ) OR isNull( closed_end ) then
//										messageBox( "Warning", "Depreciation Reports on Open " + &
//													"Months or Months where Depreciation has not " + &
//													"been Calculated do not Include Final Balances." )
//									end if
//							end choose
//						end if
//				
//				case	17			//	'Detail - Asset'
//					////
//					////	There are some reports that we have to do some special stuff for,
//					////	so lets do that here.
//					////
//						choose case	i_dw_name 
//							case	'dw_tax_vintage_report'
//								if w_cpr_select_tabs.dw_grid.isSelected( 1 ) = false then
//									w_cpr_select_tabs.cb_select_all.triggerEvent( clicked! )
//								end if
//							case	'dw_grid_accum_cost_by_basis_report'
//								dw_basis_labels.setTransObject( sqlca )
//								dw_basis_labels.retrieve()
//								f_label_buckets( dw_report, dw_basis_labels )
//							case	'dw_cpr_activity_trail_report'
//								SELECT	upper( user ), userenv( 'sessionid' )
//								INTO		:user, :session_id
//								FROM		dual;
//								
//								tabs_select = "select asset_id from temp_asset where upper(user_id) = '" + &
//													user + "'" + ' and session_id = ' + string( session_id )
//								
//								f_create_dynamic_dw( dw_temp_dynamic, 'grid', tabs_select, sqlca, true )
//								max = dw_temp_dynamic.rowcount()
//								
//								for i = 1 to max
//									tmp_asset_id = dw_temp_dynamic.getItemNumber( i, 'asset_id' )
//									
//									SELECT	asset_activity_id
//									INTO		:asset_act_id
//									FROM		cpr_activity
//									WHERE		asset_id			=	:tmp_asset_id
//										AND	activity_code	=	'UTRT';
//									
//									tabs_act_status = 0
//									tabs_asset_id = tmp_asset_id
//									
//									do while tabs_act_status <> 1
//										// get FROM asset id stored in activity_status
//										SELECT	activity_status
//										INTO		:tabs_asset_id
//										FROM		cpr_activity
//										WHERE		asset_id				=	:tabs_asset_id
//											AND	asset_activity_id	=	:asset_act_id;
//										
//										INSERT INTO	temp_asset
//										(	user_id, session_id, asset_id,
//											batch_report_id, books_schema_id
//										) 
//										VALUES	(	:user, :session_id, :tabs_asset_id, 0, 
//														:tmp_asset_id
//													);
//		
//										UPDATE	temp_asset
//											SET	books_schema_id	=	:tmp_asset_id
//											WHERE	asset_id				=	:tmp_asset_id;
//			
//										tabs_act_status = 1
//			
//										// check if FROM asset was a transfer
//										SELECT	activity_status,
//													asset_activity_id
//										INTO		:tabs_act_status,
//													:asset_act_id
//										FROM		cpr_activity
//										WHERE		asset_id				=	:tabs_asset_id
//											AND	activity_status	<>	1 
//											AND	upper( rtrim( activity_code ) ) IN ('UTRT');
//									loop
//								next								
//								COMMIT;
//						end choose
//
//					////
//					////	We're going to add the temp asset stuff to the datawindow if its
//					////	not already in the thing.
//					////
//						tabs_where = ' and ' + w_cpr_select_tabs.i_temp_where_clause
//						tabs_select = dw_report.describe( 'datawindow.table.select' )
//						
//						tabs_pos = pos( upper( tabs_select ), "TEMP_ASSET" )
//						tabs_pos1 = pos( upper( tabs_select ), "USERENV" )
//						
//						if tabs_pos > 0 and tabs_pos1 = 0 then
//							tabs_select = of_add_where_clause( tabs_select, tabs_where )
//							dw_report.modify( tabs_select )
//						end if
//					
//					////
//					////	See if this report backs down activity.
//					////
//						tabs_select = dw_report.describe( 'datawindow.table.select' )
//						tabs_pos = pos( upper( tabs_select ), 'CPR_ACT_MONTH_TEMP_SIMPLE_VIEW' )
//						tabs_pos1 = pos( upper( i_dw_name ), 'NET_VALUE' )
//						
//						if tabs_pos > 0 or tabs_pos1 > 0 then
//							if messageBox( 'Warning', 'Have you selected ~'ALL~' assets ' + &
//												'(Active and Inactive)?', Question!, YesNo!, 2 ) = 2 then
//								messageBox( 'Error', 'Please use the ~'Misc~' tab to ' + &
//												'select ~'ALL~' assets' )
//								setMicroHelp( "Ready" )
//								return
//							end if
//						end if
//						
//				case	18			//	'Detail - Subledger'
//					subl_id			=	w_subledger_select_tabs.i_subledger
//					subl_depr_ind	=	w_subledger_select_tabs.i_depr_indicator
//					subl_where		=	w_subledger_select_tabs.i_sqls_in
//					
//					dw_subledger_labels.setTransObject( sqlca )
//					dw_subledger_labels.retrieve( subl_id )
//					subl_name = dw_subledger_labels.getItemString( 1, 'subledger_name' )
//					
//					////
//					////	Replace all of the generic subledger views and tables.
//					////
//						if subl_depr_ind <= 0 then
//							dw_report.dataobject = 'dw_std_subledger_report'
//						end if
//						
//						dw_report.setTransObject( sqlca )
//						subl_sql = dw_report.describe( 'datawindow.table.select' )
//						
//						if subl_depr_ind = 1 or subl_depr_ind = 2 then
//							subl_temp = 'DEPR_SUBLEDGER_TEMPLATE'
//							subl_table = upper( subl_name + '_DEPR' )
//						
//							i = 1
//							tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//							do until tmp_pos = 0
//								subl_sql = replace( subl_sql, tmp_pos, 23, subl_table )
//								i = i + len( subl_table )
//								tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//							loop
//						end if
//						
//						subl_temp = 'SUBLEDGER_TEMPLATE'
//						subl_table = upper( subl_name )
//						
//						i = 1
//						tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//						do until tmp_pos = 0
//							subl_sql = replace( subl_sql, tmp_pos, 18, subl_table )
//							i = i + len( subl_table )
//							tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//						loop
//						
//						subl_temp = 'TEMPLATE_ADDS_RET_VIEW'
//						subl_table = upper( subl_name ) + '_ADDS_RET_VIEW'
//						
//						i = 1
//						tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//						do until tmp_pos = 0
//							subl_sql = replace( subl_sql, tmp_pos, 22, subl_table )
//							i = i + len( subl_table )
//							tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//						loop
//						
//						subl_temp = 'TEMPLATE_ASSET_DOL_VIEW'
//						subl_table = upper( subl_name ) + '_ASSET_DOL_VIEW'
//						
//						i = 1
//						tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//						do until tmp_pos = 0
//							subl_sql = replace( subl_sql, tmp_pos, 23, subl_table )
//							i = i + len( subl_table )
//							tmp_pos = pos( upper( subl_sql ), subl_temp, i )
//						loop
//					
//					////
//					////	Prepare the sql.
//					////
//						dw_report.modify( 'datawindow.table.select="' + subl_sql + subl_where + '"' )
//					
//					////
//					////	Get the selected subledger items to add to the report.
//					////
//						if isValid( w_subledger_select_tabs ) then
//							subl_items[] = w_subledger_select_tabs.i_selected_items
//							
//							if upperBound( subl_items[] ) > 0 then
//								subl_sql = f_parsearrayby254( subl_items[], 'C', &
//												subl_name + ".asset_id || ' ' || " + &
//												subl_name + ".subledger_item_id" )
//								f_add_where_clause( dw_report, '', 'and' + subl_sql, false )
//							end if
//						end if
//					
//					////
//					////	Go ahead set-up and retrieve, we don't need to go any further.
//					////
//						dw_report.modify( "title.text = '" + subl_name + " Subledger Report" )
//						
//						if dw_report.dataobject = 'dw_depr_subledger_summary' then
//							subl_count = 0
//						else
//							subl_count = f_label_subledger( dw_report, dw_subledger_labels, &
//												subl_id )
//						end if
//						
//						if subl_count = 0 then
//							dw_report.EVENT	pp_format()
//						else
//							dw_report.modify( 'datawindow.detail.height=' + string( &
//													subl_count * 20 + 100 ) )
//						end if
//						
//						goto retrieve
//						
//			case 28, 34 // pending main and detail
//				
//					if  i_special_note =  'BASIS'  then
//						
//							tab_main.tabpage_preview.dw_basis_labels.setTransObject( sqlca )
//							tab_main.tabpage_preview.dw_basis_labels.retrieve()
//							f_label_buckets( tab_main.tabpage_preview.dw_report, &
//													tab_main.tabpage_preview.dw_basis_labels )
//					end if
//					
			end choose
			 

//
////  THE REPORT IS USING STRUCTURES:
////
////  1:  Open the structures window to allow the to pick the values.
////  2:  The 1st two retrieval arguments of the report must be the structure_id and
////      the value_id.  Another argument is allowed.  The value_id must be an array.
//if i_skip_cb_retrieve_struct_code then goto after_structure_window
//
//if i_structures then
//
//	SetPointer(HourGlass!)
//	
//	//  To fix the bug when running through cb_multiples and then running with 
//	//  ue_return_this_value.  The array would contain the old values for cells 2 through n.
//	i_value_id = null_long_array
//	
//	if isvalid(w_cr_structure_values) then close(w_cr_structure_values)
//	if isvalid(w_cr_structures)       then close(w_cr_structures)
//	
//	f_parsestringintostringarray(i_report_type, ",", string_array[])
//	
//	i_element_id_structures = long(string_array[upperbound(string_array)])
//	
//	if isnull(i_element_id_structures) then i_element_id_structures = 0
//	
//	OpenSheetWithParm(w_cr_structures, i_element_id_structures, &
//		"w_cr_structures", w_top_frame, 7, Original!)
//	
//	i_structure_id_structures = i_structure_id
//	
//	wf_structure_window()
//
//	//  We will trigger another run button.
//	return
//else
//	setnull(i_structure_id)
//	setnull(i_value_id[1])
//end if
//
//
//after_structure_window:
//
//
//i_skip_cb_retrieve_struct_code = false
//
//if i_structure_id = 0 then setnull(i_structure_id)
//if i_value_id[1]  = 0 then setnull(i_value_id[1])
//
//
//////
//////	Get the current select statement.
//////
//			select_stmt = dw_report.object.datawindow.table.select
//
//////
//////	Check if Prop_Tax_Dist_Pct table needs to be added to the from clause.
//////
//			tmp_pos = pos( upper( select_stmt ), 'PROP_TAX_DISTRI_PCT', 1 )
//			if tmp_pos > 0 then
//				////
//				////	Find if there is a second occurance.  If not then we need to add
//				////	it to the from clause.
//				////
//					tmp_pos = pos( upper( select_stmt ), 'PROP_TAX_DISTRI_PCT', tmp_pos + 1 )
//					if tmp_pos = 0 then
//						////
//						////	Add PROP_TAX_DISTRI_PCT to from clause.
//						////
//							tmp_pos = pos( select_stmt, 'FROM', 1 )
//							select_stmt = replace( upper( select_stmt ), tmp_pos + 4, 1, &
//									' PROP_TAX_DISTRI_PCT, ' )
//					end if
//			end if
//
//////
//////	Temp work order. The datawindow is supoosed to have the session id stuff
//////
////			tmp_pos = pos( upper( select_stmt ), "TEMP_WORK_ORDER", 1 )
////			if tmp_pos > 0 and isValid( w_project_select_tabs ) then
////				where_stmt = where_stmt + " and " + w_project_select_tabs.i_temp_where_clause
////			end if
////
//////
//////	Check the user's company security permissions.
//////
//			where_stmt = of_company_security( select_stmt, where_stmt )
//			if where_stmt = 'NO' then
//				setMicroHelp( "Ready" )				
//				return
//			end if
//
//////
//////	Add the where clause criteria (from the filter) to the datawindow select
//////	statement.  We add it before the 'group by' or 'order by' clause in the
//////	select statement (so that its in the where clause). If we find the string 1=1,
//////  we replace it with the where_stmt (this is to let us control exactly where it goes)
//////		
//      if pos(select_stmt, 'and 1=1') > 0 then
//		       replace_str = ' ' + where_stmt 
//			
//			    select_stmt= f_replace_string(select_stmt, 'and 1=1', replace_str, 'all' )
//			    dw_stmt = 'datawindow.table.select = "' + select_stmt +   ' "'
//			
//		else
//			  dw_stmt = of_add_where_clause( select_stmt, where_stmt )
//			
//		end if
//
//////
//////	Should we show assets with 0 dollars?
//////
//			if i_show_zero_assets then
//				dw_stmt = f_replace_string(  dw_stmt , ' and cpr_ledger.accum_cost <> 0', &
//					'', 'all' )
//				dw_stmt = f_replace_string(  dw_stmt , ' and CPR_LEDGER.ACCUM_COST <> 0', &
//					'', 'all' )	
//			end if
//
//
//////
//////	Do some special stuff for reports joining subledger tables.
//////
//			if upper( i_special_note ) = 'SUBLEDGER' then
//				subl_sql = dw_stmt
//
//				////
//				////	Get the subledger name and label the buckets.
//				////		
//					subl_id = i_retrieval_array[1]
//					subl_sql = upper( subl_sql )
//					
//					if i_dw_name <> 'dw_depr_group_subledger_summary' AND &
//						i_dw_name <> 'dw_depr_subledger_summary' then
//						f_label_subledger( dw_report, dw_subledger_labels, subl_id )
//					end if
//					
//					SELECT	upper( subledger_name )
//					INTO		:subl_name
//					FROM		subledger_control
//					WHERE		subledger_type_id = :subl_id;
//				
//				////
//				////	Replace 'DEPR_SUBLEDGER_TEMPLATE' w/ subledger_name + '_depr'.
//				////
//					subl_temp = "DEPR_SUBLEDGER_TEMPLATE"
//					subl_table = upper( subl_name ) + '_DEPR'
//					
//					i = 1
//					tmp_pos = pos( subl_sql, subl_temp, i )
//					do until tmp_pos = 0
//						subl_sql	=	replace( subl_sql, tmp_pos, 23, subl_table )
//						i			=	i + len( subl_table )
//						tmp_pos	=	pos( subl_sql, subl_temp, i )
//					loop
//				
//				////
//				////	Replace 'SUBLEDGER_TEMPLATE' w/ subledger_name.
//				////
//					subl_temp = "SUBLEDGER_TEMPLATE"
//					subl_table = upper( subl_name )
//					
//					i = 1
//					tmp_pos = pos( subl_sql, subl_temp, i )
//					do until tmp_pos = 0
//						subl_sql		=	replace( subl_sql, tmp_pos, 18, subl_table )		
//						i				=	i + len( subl_table )
//						tmp_pos		=	pos( subl_sql, subl_temp, i )
//					loop
//				
//				////
//				////	Replace view names.
//				////				
//					subl_sql = f_replace_string( subl_sql, 'TEMPLATE_ASSET_DOL_VIEW',	&
//								subl_name + '_ASSET_DOL_VIEW', 'all' )
//					
//					subl_sql = f_replace_string( subl_sql, 'TEMPLATE_ADDS_RET_VIEW',		&
//								subl_name + '_ADDS_RET_VIEW', 'all' )
//				
//				dw_stmt = subl_sql
//			end if
//
//////
//////	We need the datawindow syntax to be in all uppercase to avoid some
//////	errors that occur with lowercase syntax (for some reason PowerBuilder
//////	doesn't seem to like it).
//////
//
//			//  DMJ:  THIS IS GARBAGE.  If you have a problem with the syntax, fix the problem.
//			//        This breaks every report that has to have a string argument hardcoded in
//			//        the SQL.
////			dw_stmt	=	f_replace_string( dw_stmt, "lower(", "upper(", 'all' )
////			dw_stmt	=	f_replace_string( dw_stmt, "lower (", "upper (", 'all' )
////			dw_stmt	=	f_replace_string( dw_stmt, "lower  (", "upper  (", 'all' )
////			dw_stmt	=	upper( dw_stmt )
//			
////    For single basis reports, we have to replace a hardcoded 'basis_1' with 
////    basis_x where x is the input argument to the report
//
//     if pos(i_input_window, 'dw_book_summary_select') > 0  then
//		
//		  if upperbound(i_retrieval_args_array) = 1  then // one argument
//		  	subl_id = i_retrieval_array[1]
//		  else // two arguments
//			  if pos(i_input_window, 'dw_book_summary_select') = 1 then 
//				  // make sure they only pick one set of books
//				  subl_id = i_retrieval_array[1]
//				  if upperbound(i_retrieval_array) > 1 then
//					 messagebox("", "Please select only one Book Summary")
//					 return
//				  end if
//			  else
//				   subl_id = i_retrieval_array2[1]
//					if upperbound(i_retrieval_array2) > 1 then
//					 messagebox("", "Please select only one Book Summary")
//					 return
//				  end if
//			  end if
//			 
//		  end if
//		  
//		  	dw_stmt = f_replace_string(dw_stmt, 'basis_1', &
//		                'basis_' + string(subl_id) , 'all')
//		  	dw_stmt = f_replace_string(dw_stmt, 'BASIS_1', &
//		                'BASIS_' + string(subl_id) , 'all')					 
//	  end if
//
//
return 0
//				
end function

public function integer uf_retrieve_report (longlong a_subsystem_id);int rtncode
s_option option,option2

choose case a_subsystem_id
	case 10 //'Property Tax'
       choose case i_time_option_id
			case 6   // PT - No Tax Year - None
				  rtncode =uo_dw.retrieve()
			case 7   // PT - No Tax Year - Single Month
				  if not isnull(i_start_date) then
						rtncode =uo_dw.retrieve(i_start_date)
				  else
				    return -1
				  end if
			case 8   // PT - No Tax Year - Span
				if not isnull(i_start_date) and not isnull(i_end_date) then
						rtncode = uo_dw.retrieve(i_start_date,i_end_date)
				else
				    return -1
				end if
			case 9   // PT - Single Tax Year - None
				if uf_check_option('Tax Year1') then
						option = uf_get_option('Tax Year1')
						rtncode = uo_dw.retrieve(long(option.str))
				else
				    return -1
				end if
			case 10  // PT - Single Tax Year - Single Month
				if uf_check_option('Tax Year1') and not isnull(i_start_date) then
						option = uf_get_option('Tax Year1')
						rtncode = uo_dw.retrieve(long(option.str),i_start_date)
				else
				    return -1
				end if
			case 11  // PT - Single Tax Year - Span
				if uf_check_option('Tax Year1') and not isnull(i_start_date) and not isnull(i_end_date) then
						option = uf_get_option('Tax Year1')
						rtncode = uo_dw.retrieve(long(option.str),i_start_date,i_end_date)
				else
				    return -1
				end if
			case 12  // PT - Tax Year Compare - None
				if uf_check_option('Tax Year1') and  uf_check_option('Tax Year2') then
						option = uf_get_option('Tax Year1')
						option2 = uf_get_option('Tax Year2')
						if i_debug = 1 then
							f_pp_msgs( 'PT Retrieve 12 Time1= ' + &
							   string(option.numarray[1]) + ' time2=' + string(option.numarray[1]) )
						end if
						rtncode = uo_dw.retrieve(option.numarray[1],option2.numarray[1])
				else
				    return -1
				end if
			case 13  // PT - Tax Year Compare - Single Mont
				if uf_check_option('Tax Year1') and  uf_check_option('Tax Year2') and not isnull(i_start_date) then
						option = uf_get_option('Tax Year1')
						option2 = uf_get_option('Tax Year2')
						rtncode = uo_dw.retrieve(long(option.str),long(option2.str),i_start_date)
				else
				    return -1
				end if
			case 14  // PT - Tax Year Compare - Span
				if uf_check_option('Tax Year1') and  uf_check_option('Tax Year2') and &
				      not isnull(i_start_date)  and not isnull(i_end_date) then
						option = uf_get_option('Tax Year1')
						option2 = uf_get_option('Tax Year2')
						rtncode = uo_dw.retrieve(long(option.str),long(option2.str),i_start_date,i_end_date)
				else
				    return -1
				end if
			case 15  // PT - Multi Tax Year - None
				if uf_check_option('Tax Year1') then
						option = uf_get_option('Tax Year1')
						rtncode = uo_dw.retrieve(option.numarray)
				else
				    return -1
				end if
			case 16  // PT - Multi Tax Year - Single Month
				if uf_check_option('Tax Year1') and not isnull(i_start_date) then
						option = uf_get_option('Tax Year1')
						rtncode = uo_dw.retrieve(option.numarray,i_start_date)
				else
				    return -1
				end if
			case else
				 rtncode = uo_dw.retrieve()
		end choose
					
//		if uf_check_option('Tax Year1') and  uf_check_option('Tax Year2') then
//			option = uf_get_option('Tax Year1')
//			option2 = uf_get_option('Tax Year2')
//			rtncode = uo_dw.retrieve(option2.numarray[1],option2.numarray[1])
//		elseif uf_check_option('Tax Year1') then
//			option = uf_get_option('Tax Year1')
//			rtncode = uo_dw.retrieve(long(option.str))
//		else
//			if not isnull(i_start_date) and not isnull(i_end_date) then
//				rtncode = uo_dw.retrieve(i_start_date,i_end_date)
//			elseif not isnull(i_start_date) then
//				rtncode =uo_dw.retrieve(i_start_date)
//			else
//				rtncode =uo_dw.retrieve()
//			end if
//		end if
	case else
     rtncode = uo_dw.retrieve()
end choose
return rtncode

//if s_retarg.count > 0  then
//		if  s_retarg.count = 1 then // one input dw only
//		  if s_retarg.multi_retrieval_type = 'number' then // numeric args
//				if s_retarg.multi_retrieval then
//
//					if s_retarg.structure_id_null then
//						rtncode=uo_dw.retrieve( s_retarg.numarray )
//					else
//						rtncode=uo_dw.retrieve(s_retarg.structure_id, s_retarg.value_id, s_retarg.numarray )
//					end if
//				else
//					if s_retarg.structure_id_null then
//						rtncode=uo_dw.retrieve(  s_retarg.numarray[1] )
//					else
//						rtncode=uo_dw.retrieve(  s_retarg.structure_id,  s_retarg.value_id,  s_retarg.numarray[1] )
//					end if
//				end if
//			else // string args
//				if s_retarg.multi_retrieval then
//
//					if s_retarg.structure_id_null then
//						rtncode=uo_dw.retrieve( s_retarg.stringarray )
//					else
//						rtncode=uo_dw.retrieve( s_retarg.structure_id, s_retarg.value_id, s_retarg.stringarray )
//					end if
//				else
//	
//					if isnull(s_retarg.structure_id) then
//						rtncode=uo_dw.retrieve( s_retarg.stringarray[1] )
//					else
//						rtncode=uo_dw.retrieve( s_retarg.structure_id, s_retarg.value_id, s_retarg.stringarray[1] )
//					end if
//				end if
//			end if
//		else  // two input dws
//			choose case s_retarg.multi_retrieval_type
//				case 'numberlistnumberlist'
//					if s_retarg.structure_id_null then
//						rtncode=uo_dw.retrieve( s_retarg.numarray,s_retarg.numarray2)
//					else
//						rtncode=uo_dw.retrieve( s_retarg.structure_id, s_retarg.value_id, s_retarg.numarray,s_retarg.numarray2)
//					end if
//				case 'numberliststringlist'
//						if s_retarg.structure_id_null then
//						rtncode=uo_dw.retrieve( s_retarg.numarray,s_retarg.numarray2)
//					else
//						rtncode=uo_dw.retrieve( s_retarg.structure_id, s_retarg.value_id, s_retarg.numarray,s_retarg.numarray2)
//					end if
//				case 'stringlistnumberlist'
//						if s_retarg.structure_id_null then
//						rtncode=uo_dw.retrieve( s_retarg.numarray,s_retarg.numarray2)
//					else
//						rtncode=uo_dw.retrieve( s_retarg.structure_id, s_retarg.value_id, s_retarg.numarray,s_retarg.numarray2)
//					end if
//				case 'stringliststringlist'
//					if s_retarg.structure_id_null then
//						rtncode=uo_dw.retrieve( s_retarg.numarray,s_retarg.numarray2)
//					else
//						rtncode=uo_dw.retrieve( s_retarg.structure_id, s_retarg.value_id, s_retarg.numarray,s_retarg.numarray2)
//					end if
//				case else 
//					messagebox("", "Both input datawindow must be multi-select")
//			 end choose
//			
//			
//			///////////////////
//		end if
//	else
//		if s_retarg.time_option_id = 4 then
//			if s_retarg.structure_id_null then
//				rtncode=uo_dw.retrieve( s_retarg.year, s_retarg.month )
//			else
//				rtncode=uo_dw.retrieve(s_retarg.structure_id, s_retarg.value_id, s_retarg.year, s_retarg.month )
//			end if
//		else
//			if s_retarg.structure_id_null then
//				rtncode=uo_dw.retrieve()
//			else
//				rtncode=uo_dw.retrieve(s_retarg.structure_id, s_retarg.value_id)
//			end if
//		end if
//	end if

return 0
end function

public function boolean uf_check_option (string a_option_name);
longlong i
for i =1 to upperbound(i_options)
	if i_options[i].name = a_option_name then
		return true
	end if
next 
return false
end function

private function s_retrieve_arg uf_parse_arg (string a_str);s_retrieve_arg s_retarg
string arglist[]
longlong i,j,num
longlong count,pos
string val
string arraylist
longlong argpos
if len(a_str ) > 0 then
	count = 0	
	do while Pos( a_str, "~t" ) > 0 
		i = Pos( a_str, "~t" )
		count++
		arglist[count] = left( a_str, i - 1 )
		a_str = replace( a_str, 1, i, "" )
	loop	
	count++
	arglist[count] = a_str	
else
end if


if isnumber(arglist[1]) = true then
	s_retarg.count = long(arglist[1])
	if s_retarg.count = 0 then
			 return s_retarg
	end if
else
	return s_retarg
end if

if isnumber(arglist[2]) = false then
    // error wrong type of argument
	 return s_retarg
end if

if long(arglist[2]) = 1  then
	 s_retarg.structure_id_null = true
	 argpos = 3	
else
	 s_retarg.structure_id_null = false
	 s_retarg.structure_id = long(arglist[3])
	 num  = long(arglist[4])
	 if num = 0 then
		//s_retarg.value_id[1] = ''
		 argpos = 5
	 else
	   uf_string_to_num_list(arraylist,s_retarg.value_id)
      argpos = 5
	 end if
end if


if s_retarg.count = 1 then
	
	s_retarg.multi_retrieval_type = arglist[argpos]
	argpos++
	if long(arglist[argpos]) = 1 then
	    s_retarg.multi_retrieval = true
	else
		 s_retarg.multi_retrieval = false
	end if
	argpos++
	num = long(arglist[argpos])
				 
   if s_retarg.multi_retrieval = true then
		  if lower(s_retarg.multi_retrieval_type) = 'number' then
			    argpos++
			    uf_string_to_num_list(arglist[argpos],s_retarg.numarray)
				 
		 elseif s_retarg.multi_retrieval_type = 'string' then
			    argpos++
			    uf_string_to_list(arglist[argpos],s_retarg.stringarray)
		 end if
   else
		   if s_retarg.multi_retrieval_type = 'number' then
				 argpos++
			    s_retarg.numarray[1] =  long(arglist[argpos])
			elseif s_retarg.multi_retrieval_type = 'string' then
				 argpos++
			    s_retarg.stringarray[1]  =  arglist[argpos]	
		  end if
	end if
elseif s_retarg.count = 2 then
	s_retarg.multi_retrieval_type = arglist[argpos]
	argpos++
	if long(arglist[argpos]) = 1 then
	    s_retarg.multi_retrieval = true
	else
		 s_retarg.multi_retrieval = false
	end if
	
	
	choose case s_retarg.multi_retrieval_type
		case 'numberlistnumberlist'
			 argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray)
			 argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray2)
			 
			 
		case 'numberliststringlist'
          argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray)
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray2)
			 
		case 'stringlistnumberlist'
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray)
			 argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray2)
		case 'stringliststringlist'
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray)
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray2)
		case else 
			messagebox("", "Both input datawindow must be multi-select")
	 end choose
else
	s_retarg.time_option_id = long(arglist[argpos])
	argpos++
	s_retarg.year = long(arglist[argpos])
	argpos++
	s_retarg.month = long(arglist[argpos])

end if


return s_retarg
end function

public function integer uf_parse_options (string a_option_str);s_retrieve_arg s_retarg
string arglist[]
longlong i,j,num
longlong count,pos
string val
string arraylist
longlong argpos
longlong option_num

if len(a_option_str ) > 0 then
	count = 0	
	do while Pos( a_option_str, "~t" ) > 0 
		i = Pos( a_option_str, "~t" )
		count++
		arglist[count] = left( a_option_str, i - 1 )
		a_option_str = replace( a_option_str, 1, i, "" )
	loop	
	count++
	arglist[count] = a_option_str
else
end if


if isnumber(arglist[1]) = true then
	option_num = long(arglist[1])
	if count = 0 or option_num = 0 then
			 return 0
	end if
else
	return 0
end if
if (count - 1) <> (option_num * 4) then
	// We have problem
	
end if

j = 0
for i = 2 to count step 4
	j++
	i_options[j].name = arglist[i]
	i_options[j].option_type = arglist[i + 2]
	i_options[j].len = long(arglist[i + 1])
	val =  arglist[i + 3]
	i_options[j].str = val
	if lower(i_options[j].option_type) = 'number' then		
		uf_string_to_num_list(val,i_options[j].numarray)		
	else  //i_options[j].option_type = 'string'
		uf_string_to_list(val,i_options[j].strarray)	
	end if	
next 

return option_num
end function

public function integer uf_set_lib_list ();
string path
string lib_file_list  // //ProfileString(i_ini_file,  'Application', "Lib_File_List", "prompt")
string pathname,filename,str
longlong unit
longlong count,i
string i_pbl_list[]
longlong code
string dir
longlong status

dir = space(200)
status = GetWindowsDirectoryA(dir,200)
if status = 0 then
	status = GetLastError()
   f_pp_msgs( "Get System Directory Code=" + string(code) )
	return -1
end if
filename = dir + "\pwrplant.ini"
if fileexists(filename) = false then
     f_pp_msgs( "Error 4: Can't find the ini file " + filename)
	  return -1
end if



path = ProfileString(filename, "Application", "pbllist", "")

if len(path) > 0 then
	path = path 
else
	f_pp_msgs( "Error 5: Can't find the pbllist in the ini file " + filename)
end if



lib_file_list    = path 


unit = fileopen(lib_file_list,linemode!,read!)
if unit <> -1 then
	do while fileread(unit,str) > 0 
		  count = count + 1
		  i_pbl_list[count] = str			  
	loop
	fileclose(unit)
else
	f_pp_msgs( "Error - No file list found Filename=" + lib_file_list)
	return -1
end if


string lib_list
lib_list = GetLibraryList ( )
for i = 1 to count
	lib_list = lib_list + "," + i_pbl_list[i]
next
if i_debug = 1 then
  f_pp_msgs( "liblist=" + lib_list)
end if
SetLibraryList(lib_list)
return count + 1
end function

public function longlong uf_get_debug ();string path
string lib_file_list  // //ProfileString(i_ini_file,  'Application', "Lib_File_List", "prompt")
string pathname,filename,str
longlong unit
longlong count,i
string i_pbl_list[]
longlong code
string dir
longlong status

dir = space(200)
status = GetWindowsDirectoryA(dir,200)
if status = 0 then
	status = GetLastError()
   f_pp_msgs( "Get System Directory Code=" + string(code) )
	return -1
end if
filename = dir + "\pwrplant.ini"
if fileexists(filename) = false then
     f_pp_msgs( "Error 6: Can't find the ini file " + filename)
	  return -1
end if

string debug

debug = ProfileString(filename, "Application", "debug", "0")

if debug = "0" then
	return 0
end if


return 1
end function

on n_web_reporter.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_web_reporter.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;uo_dw = create uo_datastore_template
end event

event destructor;disconnect;

end event

