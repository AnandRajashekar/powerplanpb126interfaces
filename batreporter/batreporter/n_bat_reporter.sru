HA$PBExportHeader$n_bat_reporter.sru
$PBExportComments$v10.2.1.4 maint 3459: various fixes, enhancements
forward
global type n_bat_reporter from nonvisualobject
end type
type s_retarg from structure within n_bat_reporter
end type
type s_arg from structure within n_bat_reporter
end type
end forward

type s_retarg from structure
	longlong	count
	s_arg		args[]
end type

type s_arg from structure
	string		arg_type
	longlong	len
	decimal { 0 }		dec
	datetime		datetime
	longlong	id
	string		str
	string		strarray[]
	longlong	numarray[]
end type

global type n_bat_reporter from nonvisualobject descriptor "PB_ObjectCodeAssistants" = "{BB0DD547-B36E-11D1-BB47-000086095DDA}" 
event activate pbm_component_activate
event deactivate pbm_component_deactivate
end type
global n_bat_reporter n_bat_reporter

type prototypes
Function long SetEnvironmentVariable(string var,string val)  Library "kernel32.dll" alias for "SetEnvironmentVariableA;Ansi"
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long CreateDCA( string lpszDriver, string lpszDevice, string lpszOutput, long data) Library "gdi32.dll" alias for "CreateDCA;Ansi"
Function long DeleteDC( long hdc) Library "gdi32.dll"
Function long GetDeviceCaps(long hdc,long x) Library "gdi32.dll"
Function long GetPrinterOrientation() Library "psconvrt.dll" 
Function long GetPrintDef(ref long orig ,ref long postscript) Library "psconvrt.dll" 
Function long GetPostscriptPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPostscriptPrinter;Ansi" 
Function long GetPrinterInfo( string printer,ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPrinterInfo;Ansi" 
Function long FindAddPostPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "FindAddPostPrinter;Ansi" 
Function long DeletePrinterDriverA(long name, long config, string driver) Library "winspool.drv" alias for "DeletePrinterDriverA;Ansi"
Function long GetLastError() Library "kernel32.dll"
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long  GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
Function long  CopyFileA( string path,string  desination,long stat ) Library "KERNEL32.DLL" alias for "CopyFileA;Ansi"
Function long GetPrintDefPCL(ref long orig ,ref long postscript) Library "pclcnvrt.dll" 
Function long GetPCLPrinter(ref string name,ref string drv,ref string port) Library  "pclcnvrt.dll" alias for "GetPCLPrinter;Ansi" 
 Function long GetPrinterOrientationPCL() Library "pclcnvrt.dll" 
Function long GetProcAddress(long hmod ,string procname) Library "KERNEL32.DLL" alias for "GetProcAddress;Ansi"
Function long GetWindowsDirectoryA(ref string var,long len) Library "kernel32.dll" alias for "GetWindowsDirectoryA;Ansi"
Function long LoadLibraryA(string libname) Library "KERNEL32.DLL" alias for "LoadLibraryA;Ansi"
//Function boolean SetCurrentDirectoryA(ref string cdir) LIBRARY "kernel32.dll" alias for "SetCurrentDirectoryA;Ansi" 
Function long GetLongPathNameA(string shortpath ,ref string longpath,long buflen) Library "KERNEL32.DLL" alias for "GetLongPathNameA;Ansi"
end prototypes

type variables
protected:
s_option i_options[]
string i_report_type,i_special_note,i_time_option
string i_original_syntax,i_dw_name
longlong i_report_type_id,i_time_option_id,i_filter_id
longlong i_report_id
longlong i_debug
date i_start_date
date i_end_date
uo_datastore_template uo_dw
uo_datastore_template uo_dw_temp
uo_ps_web u_ps_interface
string	i_add_date
public:
string i_exe_name = "batreporter.exe"


end variables

forward prototypes
public function integer uf_string_to_list (string a_str, ref string a_list[])
public function integer uf_string_to_num_list (string a_str, ref longlong a_list[])
private function s_option uf_get_option (string a_option_name)
private function s_retrieve_arg uf_parse_arg (string a_str)
public function integer uf_parse_options (string a_option_str)
public function integer uf_get_argument (string a_type, longlong a_id, ref any a_val)
public function integer uf_retrieve_report (datastore a_ds, longlong a_num_args, string a_arg1_type, any a_arg1_val, string a_arg2_type, any a_arg2_val, string a_arg3_type, any a_arg3_val, string a_arg4_type, any a_arg4_val)
public function integer uf_set_report_date (longlong a_report_type_id, datetime a_start_date, datetime a_end_date)
public function longlong uf_save_report_to_db (string a_filename, longlong a_batch_report_id, boolean a_save_data)
public function integer uf_save_blob (string a_filename, blob a_data)
public function integer uf_set_lib_list ()
public function longlong uf_get_debug ()
public function string uf_get_temp_dir ()
public function integer uf_retrieve_report_dw (datawindow a_dw, longlong a_num_args, string a_arg1_type, any a_arg1_val, string a_arg2_type, any a_arg2_val, string a_arg3_type, any a_arg3_val, string a_arg4_type, any a_arg4_val)
public function longlong uf_find_path (ref string a_path)
public function longlong uf_generate_report ()
public function longlong uf_add_schedule_reports ()
public function longlong uf_send_mail_distri_list (longlong a_id, datastore a_ds, blob a_data)
end prototypes

public function integer uf_string_to_list (string a_str, ref string a_list[]);longlong count = 0
longlong i
if len(a_str ) > 0 then
	count = 0	
	do while Pos( a_str, "~," ) > 0 
		i = Pos( a_str, "~," )
		count++
		a_list[count] = left( a_str, i - 1 )
		a_str = replace( a_str, 1, i, "" )
	loop	
	count++
	a_list[count] = a_str	
end if


return count
end function

public function integer uf_string_to_num_list (string a_str, ref longlong a_list[]);longlong count = 0
longlong i
if len(a_str ) > 0 then
	count = 0	
	do while Pos( a_str, "~," ) > 0 
		i = Pos( a_str, "~," )
		count++
		a_list[count] = long(left( a_str, i - 1 ))
		a_str = replace( a_str, 1, i, "" )
	loop	
	count++
	a_list[count] = long(a_str)	
end if


return count
end function

private function s_option uf_get_option (string a_option_name);s_option opt

longlong i
for i =1 to upperbound(i_options)
	if i_options[i].name = a_option_name then
		return i_options[i]
	end if
next 

return opt
end function

private function s_retrieve_arg uf_parse_arg (string a_str);s_retrieve_arg s_retarg
string arglist[]
longlong i,j,num
longlong count,pos
string val
string arraylist
longlong argpos
if len(a_str ) > 0 then
	count = 0	
	do while Pos( a_str, "~t" ) > 0 
		i = Pos( a_str, "~t" )
		count++
		arglist[count] = left( a_str, i - 1 )
		a_str = replace( a_str, 1, i, "" )
	loop	
	count++
	arglist[count] = a_str	
else
end if


if isnumber(arglist[1]) = true then
	s_retarg.count = long(arglist[1])
	if s_retarg.count = 0 then
			 return s_retarg
	end if
else
	return s_retarg
end if

if isnumber(arglist[2]) = false then
    // error wrong type of argument
	 return s_retarg
end if

if long(arglist[2]) = 1  then
	 s_retarg.structure_id_null = true
	 argpos = 3	
else
	 s_retarg.structure_id_null = false
	 s_retarg.structure_id = long(arglist[3])
	 num  = long(arglist[4])
	 if num = 0 then
		//s_retarg.value_id[1] = ''
		 argpos = 5
	 else
	   uf_string_to_num_list(arraylist,s_retarg.value_id)
      argpos = 5
	 end if
end if


if s_retarg.count = 1 then
	
	s_retarg.multi_retrieval_type = arglist[argpos]
	argpos++
	if long(arglist[argpos]) = 1 then
	    s_retarg.multi_retrieval = true
	else
		 s_retarg.multi_retrieval = false
	end if
	argpos++
	num = long(arglist[argpos])
				 
   if s_retarg.multi_retrieval = true then
		  if lower(s_retarg.multi_retrieval_type) = 'number' then
			    argpos++
			    uf_string_to_num_list(arglist[argpos],s_retarg.numarray)
				 
		 elseif s_retarg.multi_retrieval_type = 'string' then
			    argpos++
			    uf_string_to_list(arglist[argpos],s_retarg.stringarray)
		 end if
   else
		   if s_retarg.multi_retrieval_type = 'number' then
				 argpos++
			    s_retarg.numarray[1] =  long(arglist[argpos])
			elseif s_retarg.multi_retrieval_type = 'string' then
				 argpos++
			    s_retarg.stringarray[1]  =  arglist[argpos]	
		  end if
	end if
elseif s_retarg.count = 2 then
	s_retarg.multi_retrieval_type = arglist[argpos]
	argpos++
	if long(arglist[argpos]) = 1 then
	    s_retarg.multi_retrieval = true
	else
		 s_retarg.multi_retrieval = false
	end if
	
	
	choose case s_retarg.multi_retrieval_type
		case 'numberlistnumberlist'
			 argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray)
			 argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray2)
			 
			 
		case 'numberliststringlist'
          argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray)
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray2)
			 
		case 'stringlistnumberlist'
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray)
			 argpos++
			 argpos++
			 uf_string_to_num_list(arglist[argpos],s_retarg.numarray2)
		case 'stringliststringlist'
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray)
			 argpos++
			 argpos++
			 uf_string_to_list(arglist[argpos],s_retarg.stringarray2)
		case else 
			messagebox("", "Both input datawindow must be multi-select")
	 end choose
else
	s_retarg.time_option_id = long(arglist[argpos])
	argpos++
	s_retarg.year = long(arglist[argpos])
	argpos++
	s_retarg.month = long(arglist[argpos])

end if


return s_retarg
end function

public function integer uf_parse_options (string a_option_str);s_retrieve_arg s_retarg
string arglist[]
longlong i,j,num
longlong count,pos
string val
string arraylist
longlong argpos
longlong option_num

if len(a_option_str ) > 0 then
	count = 0	
	do while Pos( a_option_str, "~t" ) > 0 
		i = Pos( a_option_str, "~t" )
		count++
		arglist[count] = left( a_option_str, i - 1 )
		a_option_str = replace( a_option_str, 1, i, "" )
	loop	
	count++
	arglist[count] = a_option_str
else
end if


if isnumber(arglist[1]) = true then
	option_num = long(arglist[1])
	if count = 0 or option_num = 0 then
			 return 0
	end if
else
	return 0
end if
if (count - 1) <> (option_num * 4) then
	// We have problem
	
end if

j = 0
for i = 2 to count step 4
	j++
	i_options[j].name = arglist[i]
	i_options[j].option_type = arglist[i + 2]
	i_options[j].len = long(arglist[i + 1])
	val =  arglist[i + 3]
	i_options[j].str = val
	if lower(i_options[j].option_type) = 'number' then		
		uf_string_to_num_list(val,i_options[j].numarray)		
	else  //i_options[j].option_type = 'string'
		uf_string_to_list(val,i_options[j].strarray)	
	end if	
next 

return option_num
end function

public function integer uf_get_argument (string a_type, longlong a_id, ref any a_val);date date
string str
decimal num
string strarray[]
decimal numarray[]
date datearray[]
longlong count

choose case a_type
	case 'string'
		select arg_string into :strarray[1] from pp_batch_report_args  where id = :a_id ;
		a_val = strarray[1]
	case 'number'
		select arg_number into :numarray[1] from pp_batch_report_args  where id = :a_id ;
		a_val = numarray[1]
	case 'date'
		select arg_date into :datearray[1] from pp_batch_report_args  where id = :a_id ;
	   a_val = datearray[1]
  case 'array string'
		declare arg_str_cur cursor for select arg_string from pp_batch_report_args where id = :a_id order by pos;
		open arg_str_cur;
		fetch  arg_str_cur into :str;
		count = 0
		do while sqlca.sqlcode = 0
			count++
			strarray[count] = str;
			fetch  arg_str_cur into :str;
		loop
		close arg_str_cur;
		a_val = strarray
	case 'array number'
		declare arg_num_cur cursor for select arg_number from pp_batch_report_args where id = :a_id order by pos;
		open arg_num_cur;
		fetch  arg_num_cur into :num;
		count = 0
		do while sqlca.sqlcode = 0
			count++
			numarray[count] = num;
			fetch  arg_num_cur into :num;
		loop
		close arg_num_cur;
		a_val = numarray
	case 'array date'
		declare arg_date_cur cursor for select arg_date from pp_batch_report_args where id = :a_id order by pos;
		open arg_date_cur;
		fetch  arg_date_cur into :date;
		count = 0
		do while sqlca.sqlcode = 0
			count++
			datearray[count] = date;
			fetch  arg_date_cur into :date;
		loop
		close arg_date_cur;
		a_val = datearray
	case else
end choose

return 0
end function

public function integer uf_retrieve_report (datastore a_ds, longlong a_num_args, string a_arg1_type, any a_arg1_val, string a_arg2_type, any a_arg2_val, string a_arg3_type, any a_arg3_val, string a_arg4_type, any a_arg4_val);longlong code 
choose case a_num_args
	case 0
		code = a_ds.retrieve()
	case 1
		code = a_ds.retrieve(a_arg1_val)
	case 2
		code = a_ds.retrieve(a_arg1_val,a_arg2_val)
	case 3
		code = a_ds.retrieve(a_arg1_val,a_arg2_val,a_arg3_val)
	case 4
		code = a_ds.retrieve(a_arg1_val,a_arg2_val,a_arg3_val,a_arg4_val)
	case else
end choose
return code 
end function

public function integer uf_set_report_date (longlong a_report_type_id, datetime a_start_date, datetime a_end_date);string	user_id, select_stmt, from_clause, sql_check, special_note
longlong	session_id, from_start, where_start, from_length
boolean	use_cpr_act_month



////
////	Set the report time different ways depending on the report type.
////
			SELECT	userenv( 'sessionid' )
			INTO		:session_id
			FROM		DUAL;

			choose case	a_report_type_id
				case	17			//	Detail - Asset
					////
					////	Delete the current user's report time from cpr_act_month.
					////
						DELETE FROM	cpr_act_month
							WHERE		ltrim( rtrim( upper( user_id ) ) )	=	upper( user )
								AND	session_id									=	userenv( 'sessionid' )
								AND	batch_report_id							=	0;
						commit;
						
						//	delete old rows from cpr_act_month
						DELETE FROM	cpr_act_month
							WHERE	ltrim( rtrim( upper( user_id ) ) )	=	upper( user) and
							 to_char(time_stamp, 'yyyymmdd') < 	to_char(sysdate, 'yyyymmdd'); 
						commit;

					////
					////	Insert the user's current time into cpr_act_month.
					////
						INSERT INTO	cpr_act_month (	month, user_id, batch_report_id, session_id	)
							VALUES	(	:a_start_date,			/*	month					*/
											user,				/*	user_id				*/
											0,							/*	batch_report_id	*/
											userenv( 'sessionid' )				/*	session_id			*/
										);   
						commit;
				case	else
					////
					////	Delete the current user's report time from report_time.
					////
						DELETE FROM	report_time
							WHERE		ltrim( rtrim( upper( user_id ) ) )	=	user
								AND	session_id = userenv( 'sessionid' )	
								AND	batch_report_id	=	0;
						commit;
						
					//	delete old rows from report_time
						DELETE FROM	report_time
							WHERE	ltrim( rtrim( upper( user_id ) ) )	=	user and
							to_char(time_stamp, 'yyyymmdd') < 	to_char(sysdate, 'yyyymmdd'); 
						commit;

					////
					////	Update the current user's report time into report_time.
					////
						INSERT INTO	report_time (user_id, session_id, batch_report_id, start_month, end_month)
							VALUES	(	user,						/* user_id				*/
											userenv( 'sessionid' ),	/* session_id			*/
											0,									/* batch_report_id	*/
											:a_start_date,					/* start_month			*/
											:a_end_date						/* end_month			*/
											);
							commit;

				
			
			end choose


return 1
end function

public function longlong uf_save_report_to_db (string a_filename, longlong a_batch_report_id, boolean a_save_data);	// Get the file length, and open the file
	
longlong unit 
longlong id,len
longlong log_id
longlong bytes_read 
blob data,indata                

//FileDelete(filename)	  

string title,mailto,format,output,directory,filename 

// Save Report Data
select nvl(max(id),0) + 1 into :id from pp_batch_reports_data;
select title,mailto,format,output,directory into
         :title,:mailto,:format,:output,:directory
			from pp_batch_reports where id = :a_batch_report_id;
if sqlca.sqlcode <> 0 then
	f_pp_msgs( "selecting from  pp_batch_reports code=" + string(sqlca.sqldbcode) + ' msg=' + sqlca.sqlerrtext)
end if
	
 // Get Log id 
select nvl(max(id),0) + 1 into :log_id from pp_batch_reports_log;


insert into pp_batch_reports_data(id,batch_report_id ,log_id,
                                   title,
											  run_date,
											  mailto,
											  format,
											  output,
											  directory,
											  filename,
											  start_date,
											  end_date)
											 values(:id,:a_batch_report_id,:log_id,
											 :title,
											 sysdate,
											 :mailto,
											 :format,
											 :output,
											 :directory,
											 :a_filename,
											 :i_start_date,
											 :i_end_date);
if sqlca.sqlcode <> 0 then
	f_pp_msgs( "insert into pp_batch_report_data code=" + string(sqlca.sqldbcode) + ' msg=' + sqlca.sqlerrtext)
end if

// save print data
if a_save_data then
	  unit = FileOpen(a_filename, StreamMode!, Read!, Shared! )	
	  // Read the file
		bytes_read = 1
		data = blob('')
		len = 0
		do while bytes_read > 0
			bytes_read = FileRead(unit,indata)
			if bytes_read > 0 then
			  data = data + indata
			  len += bytes_read
			end if	
		loop
		FileClose(unit)
		updateblob  pp_batch_reports_data set data=:data where id = :id;
		if sqlca.sqlcode <> 0 then
		    f_pp_msgs( "updateblob pp_batch_report_data code=" + string(sqlca.sqldbcode) + ' msg=' + sqlca.sqlerrtext)
      end if
end if 


// Save Log Data 
insert into pp_batch_reports_log(id,report_id,report_data_id) values(:log_id,:a_batch_report_id,:id);
if sqlca.sqlcode <> 0 then
		f_pp_msgs( "insert into pp_batch_report_log code=" + string(sqlca.sqldbcode) + ' msg=' + sqlca.sqlerrtext)
end if

if isnull(g_prog_interface.i_log_data) = false and string(g_prog_interface.i_log_data) <> "" then
	updateblob pp_batch_reports_log set data=:g_prog_interface.i_log_data where id = :log_id;
	if sqlca.sqlcode <> 0 then
			f_pp_msgs( "insert into pp_batch_report_log code=" + string(sqlca.sqldbcode) + ' msg=' + sqlca.sqlerrtext)
	end if
	commit;
end if
//clear the log data
g_prog_interface.i_log_data=blob('')
return id
end function

public function integer uf_save_blob (string a_filename, blob a_data);integer unit 
longlong pos
longlong bytes



unit = FileOpen(a_filename, StreamMode!, Write!, Shared!,Replace!)

pos = 1
Do While len(a_data) > pos 
	  bytes = FileWrite( unit, BlobMid(a_data, pos, 32765))
     pos += bytes
Loop

FileClose(unit)

return 0

end function

public function integer uf_set_lib_list ();
string path
string lib_file_list  // //ProfileString(i_ini_file,  'Application', "Lib_File_List", "prompt")
string pathname,filename,str
longlong unit
longlong count,i
string i_pbl_list[]
longlong code
string dir
longlong status

dir = space(200)
status = GetWindowsDirectoryA(dir,200)
if status = 0 then
	status = GetLastError()
   f_pp_msgs( "Get System Directory Code=" + string(code) )
	return -1
end if
filename = dir + "\pwrplant.ini"
if fileexists(filename) = false then
     f_pp_msgs( "Error 1: Can't find the ini file " + filename)
	  return -1
end if



path = ProfileString(filename, "Application", "pbllist", "")

if len(path) > 0 then
	path = path 
else
	f_pp_msgs( "Error 2: Can't find the pbllist in the ini file " + filename)
end if



lib_file_list    = path 


unit = fileopen(lib_file_list,linemode!,read!)
if unit <> -1 then
	do while fileread(unit,str) > 0 
		  count = count + 1
		  i_pbl_list[count] = str			  
	loop
	fileclose(unit)
else
	f_pp_msgs( "Error - No file list found Filename=" + lib_file_list)
	return -1
end if


string lib_list
lib_list = GetLibraryList ( )
for i = 1 to count
	lib_list = lib_list + "," + i_pbl_list[i]
next
if i_debug = 1 then
  f_pp_msgs( "liblist=" + lib_list)
end if
SetLibraryList(lib_list)
return count + 1
end function

public function longlong uf_get_debug ();string path
string lib_file_list  // //ProfileString(i_ini_file,  'Application', "Lib_File_List", "prompt")
string pathname,filename,str
longlong unit
longlong count,i
string i_pbl_list[]
longlong code
string dir
longlong status

filename  = ProfileString("win.ini", "PowerPlan", "ini_file", "")

if fileexists(filename) = false then
     f_pp_msgs( "Error 3: Can't find the ini file " + filename)
	  return -1
end if

string debug

debug = ProfileString(filename, "Application", "debug", "0")

if debug = "0" then
	return 0
end if


return 1
end function

public function string uf_get_temp_dir ();string str,str1
longlong status ,len
str = space(200)

status = GetEnvironmentVariableA('TEMP',str,200)
if status = 0 then
	status = GetLastError()
	return "c:\temp\"
end if

str1 = space(600)
len = GetLongPathNameA(str,str1,600)
if len < 1 then
	return str
end if

str = mid(str,1,status)
return str1
end function

public function integer uf_retrieve_report_dw (datawindow a_dw, longlong a_num_args, string a_arg1_type, any a_arg1_val, string a_arg2_type, any a_arg2_val, string a_arg3_type, any a_arg3_val, string a_arg4_type, any a_arg4_val);longlong code 
choose case a_num_args
	case 0
		code = a_dw.retrieve()
	case 1
		code = a_dw.retrieve(a_arg1_val)
	case 2
		code = a_dw.retrieve(a_arg1_val,a_arg2_val)
	case 3
		code = a_dw.retrieve(a_arg1_val,a_arg2_val,a_arg3_val)
	case 4
		code = a_dw.retrieve(a_arg1_val,a_arg2_val,a_arg3_val,a_arg4_val)
	case else
end choose
return code 
end function

public function longlong uf_find_path (ref string a_path);longlong mod
string path
longlong i,status,len
string str
setnull(str)
mod =  GetModuleHandleA(str)
if mod <> 0 then
	path = space(601)
	status = GetModuleFileNameA(mod,path,600)
	if status <> 0 then
		len = status
		for i = len to 1 step -1
			if mid(path,i,1) = '\' then
				a_path = mid(path,1,i)
				return 1
			end if
		next
	end if
end if
return -1
end function

public function longlong uf_generate_report ();


string passwd,title 
longlong i, rtn
longlong code 
string err,option,description, str_rtn
string dw_name,errmsg
string dw_syntax 
string dw_data
string report_desc
string drv_name,port,name
longlong id,num
string syntax ,errors, sql, report_sql 
uo_ds_top ds
string msg,filename,str
longlong unit
longlong bytes_read
string user_name
string report_name
string class
longlong rtncode
longlong len
string mailto,printer
longlong ids[]
string output_file
longlong report_type_id
string temp_dir,fix_file
string directory,dir
string report_number
longlong pos
longlong arg_num
string output,format
string long_desc
string arg1_type,arg2_type,arg3_type,arg4_type
longlong arg1_id,arg2_id,arg3_id,arg4_id
string cmd//, version
string	server
string arg[]
longlong count
longlong local 

//version = 'v10.1.2' 

SetEnvironmentVariable("ORA_OCI_UCBPKG","")  
g_smtpmail = create uo_smtpmail
g_smtpmail.i_database_mail = true		//Always send through database
g_ps_interface.uf_get_host(server)

//s_retrieve_arg  s_retarg
//u_prog_interface  = create  u_prog_interface 
//f_pp_msgs("Starting Batch Reporter " + " version " + version + "  " + string(today()) + ' ' + string(now()) )


//cmd = Trim(CommandParm())	
////cmd = 'Local pwrplant pwrplant xcel9 o84 disablebind=1,staticbind=0,pwdialog=1 19 1'
//if  Len(cmd ) <> 0 then
//	count = 1
//   DO WHILE Len(cmd ) > 0
//		// Find the first blank
//		i = Pos( cmd, " " )
//		if i = 0 then i = Len(cmd) + 1
//		arg[count] = Left( cmd, i - 1 )
//		count += 1
//		cmd = replace( cmd, 1, i, "" )
//   LOOP	
//end if

//u_ps_interface = create uo_ps_web
//g_ps_interface.uf_load_library('psconvrt.dll',errmsg)

if g_manual_batch = false then g_ps_interface.i_debug = uf_get_debug()       
temp_dir = g_ps_interface.uf_get_temp_dir()


 
////scheduler_date = datetime(date(str ),time(0))
//u_prog_interface.i_local = false
//if upperbound(arg) > 0 then
//	if arg[1] = 'Local' then
//		if upperbound(arg) < 6 then
//			MessageBox("Error","Wrong number of arguments " + string(upperbound(arg)) + " cmd=" + cmd)
//			return -1
//		end if
//		sqlca.dbms = arg[5]	
//		sqlca.servername = arg[4]
//		sqlca.logid= arg[2]
//		sqlca.logpass = arg[3]
//		sqlca.dbparm=arg[6]
//		id=long(arg[7])
//		i_debug = long(arg[8])
//		local = 1
//		u_prog_interface.i_local = true
//		if i_debug = 1 then
//	        MessageBox("Information", &
//			            "User " + sqlca.logid + " ~r~n" + &
//				         "Password   " + sqlca.logpass + " ~r~n" + &
//					      "Server    " + sqlca.servername + " ~r~n" + &
//							"dbms    " + sqlca.dbms + " ~r~n" + &
//							"dbparm    " + sqlca.dbparm + " ~r~n" + &
//							"id    " + string(id) + " ~r~n" )
//								 
//      end if 
//		goto skip_parms
//	end if
//end if
//   local = 0
//	u_prog_interface.uf_get_prog_params("")
//	sqlca.dbms = u_prog_interface.i_dbms	
//	sqlca.servername = u_prog_interface.i_server
//	sqlca.logid= u_prog_interface.i_username
//	sqlca.logpass = u_prog_interface.i_password
//	sqlca.dbparm='disablebind=1,staticbind=0'
//    g_log_file		  = u_prog_interface.i_log_file
//skip_parms:
//str = "C:\local_files\pwrpla9.0"
//SetCurrentDirectoryA(str)
//connect;
//if local = 0 then
//  f_pp_msgs("Connected " + string(sqlca.sqlcode) )
//  f_write_log(g_log_file, "Connected " + string(sqlca.sqlcode) ) 
//   f_write_log(g_log_file, "See ppc.log at " +temp_dir)
//end if
//
//if sqlca.sqlcode <> 0 then
//	errmsg = 'Error Connecting Code = ' + string(sqlca.sqldbcode) + &
//	       ' Msg=' +   sqlca.sqlerrtext 
//	f_pp_msgs(errmsg )
//	f_write_log(g_log_file, errmsg )
//	return -1
//end if

// JAK 20081005:  Added a PP System Control to determine whether the file name is added onto
//		the end of the file name when saving a report.  If it is set to yes, the file will be saved as
//		Report_Name_mmyyyy.pdf otherwise it will be Report_Name.pdf etc.
i_add_date = f_pp_system_control_company('Batch Report - Add Month', -1)
if i_add_date <> 'no' then i_add_date = 'yes'

if g_prog_interface.i_local = false then
	
		/*START: Add Scheduled Reports*/

		rtn = uf_add_schedule_reports()
		if rtn <> 1 then
			errmsg = 'Error Adding Scheduled Reports = ' + string(sqlca.sqldbcode) + &
					 ' Msg=' +   sqlca.sqlerrtext 
			f_pp_msgs(errmsg )
		end if
		
		/*END: Add Scheduled Reports*/
		
		
	// get all available reports to run
	declare rep_cur cursor for select id from pp_batch_reports where status = 1;
	open rep_cur;
	fetch rep_cur into :id;
	count = 0
	do while sqlca.sqlcode = 0
		count++
		ids[count] = id
		fetch rep_cur into :id;
	loop
	close rep_cur;
else

	count = 1
	ids[count] = id
end if
	

if g_ps_interface.i_debug = 1 or i_debug = 1 then
	f_pp_msgs( &
					"User        : " + sqlca.logid + '~r~n' + &
					"Password    : " + sqlca.logpass + '~r~n' + &
					"Server      : " + sqlca.servername + '~r~n' )
end if 
longlong report_id
date report_date
datetime start_date,end_date
decimal vers
for i = 1 to upperbound(ids)
	 id = ids[i]
	 g_prog_interface.i_log_data=blob('')
	 select report_id,
	        class,
			  report_name,
			  report_date, 
			  user_name,
			  title,
			  description,
			  start_date,
     		  end_date,
			  version,
			  format,
			  output,
			  directory,
			  mailto,
			  printer,
			  debug,
			  arg_num,
			  arg1_type,
			  arg1_id,
			  arg2_type,
			  arg2_id,
			  arg3_type,
			  arg3_id,
			  arg4_type,
			  arg4_id into 
			  :report_id,
	        :class,
			  :report_name,
			  :report_date,
			  :user_name,
			  :title,
			  :long_desc,
			  :start_date,
     		  :end_date,
			  :vers,		  
			  :format,
			  :output,
			  :directory,
			  :mailto,
			  :printer,
			  :g_ps_interface.i_debug,
			  :arg_num,
			  :arg1_type,
			  :arg1_id,
			  :arg2_type,
			  :arg2_id,
			  :arg3_type,
			  :arg3_id,
			  :arg4_type,
			  :arg4_id  
			  from pp_batch_reports where id = :id;
	
	if sqlca.sqlcode <> 0 then
			errmsg = 'Error Selecting pp_batch_reports Code = ' + string(sqlca.sqldbcode) + ' Msg=' +   sqlca.sqlerrtext 
			f_pp_msgs(errmsg)
			return -1
	end if
	//g_ps_interface.i_debug = i_debug
   if g_ps_interface.i_debug = 1 or i_debug = 1 then
            f_pp_msgs( &   
				   "ID          : " + string(id) + '~r~n' + &
				   "Report Type : " + string(class) + '~r~n' + &
					"Report ID   : " + string(report_id) + '~r~n' + &
					"Start Date  : " + string(start_date) + '~r~n' + &
					"End Date    : " + string(end_date)  + '~r~n' )
	end if
	blob b
	ds = create uo_ds_top
	selectblob dw_data into :b from pp_batch_reports where id = :id;
	if sqlca.sqlcode <> 0 then
			errmsg = 'Error Selecting the syntax from the pp_batch_reports table Code = ' + string(sqlca.sqldbcode) + ' Msg=' +   sqlca.sqlerrtext 
			f_pp_msgs(errmsg)
			return -1
	end if
	   syntax = string(b)
	 
	   ds.settransobject(sqlca)
//	   code = ds.Create(syntax, errmsg)
//		if code <> 1 and local = 1 then
//				f_pp_msgs(syntax)
//				MessageBox("Error","Error creating report datastore. msg = " + err)
//				return 0
//			end if 
	   code = ds.Create(syntax, errmsg)
		if code <> 1 then
				f_pp_msgs("Error creating report datastore. msg = " + errmsg)
				f_pp_msgs(syntax)
				if g_prog_interface.i_local = true then f_pp_msgs("Error creating report datastore. msg = " + errmsg)
				return 0
			end if 
	   ds.settransobject(sqlca)

		if not isnull(start_date) then
			i_start_date = date(start_date)
		else
			setnull(i_start_date)
		end if
		if not isnull(end_date) then
			i_end_date = date(end_date)
		else
			setnull(end_date)
		end if
		select description,report_type_id into :report_desc,:report_type_id from pp_reports where report_id = :report_id;
		if sqlca.sqlcode <> 0 then
			errmsg = 'Error Selecting pp_reports Code = ' + string(sqlca.sqldbcode) + ' Msg=' +   sqlca.sqlerrtext 
			f_pp_msgs(errmsg)
			return -1
	   end if
		uf_set_report_date(report_type_id,datetime(date(i_start_date),time(0)) , &
		                                  datetime(date(i_end_date),time(0)) )
		any val1,val2,val3,val4

		if arg_num > 0 then
			uf_get_argument(arg1_type,arg1_id,val1)
		end if
		if arg_num > 1 then
			uf_get_argument(arg2_type,arg2_id,val2)
		end if
		if arg_num > 2 then
			uf_get_argument(arg3_type,arg3_id,val3)
		end if
		if arg_num > 3 then
			uf_get_argument(arg4_type,arg4_id,val4)
		end if
		if not isnull(title) and title <> '' then
		      ds.modify("user_title.text ='" +title + "'")
	   end if
      if g_prog_interface.i_local = true then
   	   		open(w_report_preview)
			dw_syntax = ds.describe('datawindow.syntax')
			w_report_preview.dw_1.settransobject(sqlca)
			code = w_report_preview.dw_1.create(dw_syntax ,err)
			w_report_preview.dw_1.settransobject(sqlca)
		   code =uf_retrieve_report_dw(w_report_preview.dw_1,arg_num,arg1_type,val1,arg2_type,val2,arg3_type,val3,arg4_type,val4)
//		   if code <> 0 then
//				MessageBox("Error","Error retrieving report datawindow. code = " + string(code))
//				return 0
//			end if 
			delete from pp_batch_reports_log where id = :id;
			if sqlca.sqlcode <> 0 then
				errmsg = 'Error Selecting the deleting pp_batch_reports_log table Code = ' + string(sqlca.sqldbcode) + ' Msg=' +   sqlca.sqlerrtext 
				f_pp_msgs(errmsg)
			end if
			delete from pp_batch_reports_data where id = :id;
			if sqlca.sqlcode <> 0 then
				errmsg = 'Error Selecting the deleting pp_batch_reports_data table Code = ' + string(sqlca.sqldbcode) + ' Msg=' +   sqlca.sqlerrtext 
				f_pp_msgs(errmsg)
			end if
			delete from pp_batch_reports where id = :id;
			if sqlca.sqlcode <> 0 then
				errmsg = 'Error Selecting the deleting pp_batch_reports table Code = ' + string(sqlca.sqldbcode) + ' Msg=' +   sqlca.sqlerrtext 
				f_pp_msgs(errmsg)
			end if

	   else
		   code =uf_retrieve_report(ds,arg_num,arg1_type,val1,arg2_type,val2,arg3_type,val3,arg4_type,val4)
		end if
		report_number = report_name
		do while pos(report_number,' ') > 0 
			report_number = replace(report_number,pos(report_number,' '),1,'_')
		loop
		do while pos(report_number,'/') > 0 
			report_number = replace(report_number,pos(report_number,'/'),1,'_')
		loop
		do while pos(report_number,'\') > 0 
			report_number = replace(report_number,pos(report_number,'\'),1,'_')
		loop
		if isnull(long_desc) then long_desc = ''
		choose case format
			case 'PDF' 
			
			if i_add_date = 'yes' then 
				output_file =  temp_dir + "\" + report_number + string(i_start_date, 'mmyyyy') + '.pdf'
			else
				output_file =  temp_dir + "\" + report_number + '.pdf'
			end if 
			
	         filename =  temp_dir + '\pstemp.ps'
				g_ps_interface.uf_print_file(ds)
           		 if g_ps_interface.i_debug = 1 or i_debug = 1 then f_pp_msgs("Save AS PDF " + filename )
				
				choose case output
					case 'Database'					
                  			g_ps_interface.uf_saveas_pdf(filename,output_file)
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						if i_add_date = 'yes' then 
							output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.pdf'
						else
							output_file =  directory + "\" + report_number + '.pdf'
						end if 
                  			g_ps_interface.uf_saveas_pdf(filename,output_file)
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						g_ps_interface.uf_saveas_pdf(filename,output_file)
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						//uf_save_report_to_db(output_file,id,false)
						//Save the report to the database anyway, in case the user wants to see the report later from the history tab
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
							ds.print()
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							uf_save_report_to_db(output_file,id,true)
						end if
						uf_save_report_to_db(output_file,id,false)
				end choose 
			case 'PRN' 
			
				if i_add_date = 'yes' then 
					output_file =  temp_dir + "\" + report_number + string(i_start_date, 'mmyyyy') + '.pdf'
				else
					output_file =  temp_dir + "\" + report_number + '.pdf'
				end if 
	      	   filename =  temp_dir + '\pcltemp.pcl'
				g_ps_interface.uf_print_file_pcl2(ds)
            		if g_ps_interface.i_debug = 1 or i_debug = 1 then f_pp_msgs("Save AS PRN " + filename )
				
				choose case output
					case 'Database'					
						g_ps_interface.uf_saveas_prn_pcl(filename,output_file)
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						if i_add_date = 'yes' then 
							output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.pdf'
						else
							output_file =  directory + "\" + report_number + '.pdf'
						end if 
						g_ps_interface.uf_saveas_prn_pcl(filename,output_file)
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						g_ps_interface.uf_saveas_prn_pcl(filename,output_file)
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
						   ds.print()
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							uf_save_report_to_db(output_file,id,true)
						end if
						uf_save_report_to_db(output_file,id,false)
				end choose 
			case 'PS' 	
				if i_add_date = 'yes' then 
					output_file =   directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.ps'
				else
					output_file =   directory + "\" + report_number + '.ps'
				end if 	
				g_ps_interface.uf_print_file_ps(ds,output_file)
				//g_ps_interface.uf_printer_file_ps(ds,
            		if g_ps_interface.i_debug = 1 or i_debug = 1 then f_pp_msgs("Save AS PS " + filename )
				
				choose case output
					case 'Database'					
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
						   ds.print()
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							uf_save_report_to_db(output_file,id,true)
						end if
						uf_save_report_to_db(output_file,id,false)
				end choose 
         case 'HTML' 
				if i_add_date = 'yes' then 
					output_file =  temp_dir + "\" + report_number + string(i_start_date, 'mmyyyy') + '.htm'	
				else
					output_file =  temp_dir + "\" + report_number + '.htm'	
				end if 	
				
				choose case output
					case 'Database'	
						ds.saveas(output_file,HTMLTable!,true)
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						if i_add_date = 'yes' then 
							output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.htm'
						else
							output_file =  directory + "\" + report_number + '.htm'
						end if 	
						ds.saveas(output_file,HTMLTable!,true)
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						ds.saveas(output_file,HTMLTable!,true)
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
						   ds.print()
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							uf_save_report_to_db(output_file,id,true)
						end if
						ds.saveas(output_file,HTMLTable!,true)
						uf_save_report_to_db(output_file,id,false)
				end choose 
			case 'Syntax'
				blob data
				dw_data = ds.Object.DataWindow.Syntax.Data
				data = blob(syntax + dw_data)
				if i_add_date = 'yes' then 
					output_file =  temp_dir + "\" + report_number + string(i_start_date, 'mmyyyy') + '.syntax'
				else
					output_file =  temp_dir + "\" + report_number + '.syntax'
				end if 					
				choose case output
					case 'Database'
						uf_save_blob(output_file,data)
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						if i_add_date = 'yes' then 
							output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.syntax'
						else
							output_file =  directory + "\" + report_number + '.syntax'
						end if 					
						uf_save_blob(output_file,data)
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						uf_save_blob(output_file,data)
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						// not valid
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
						   ds.print()
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							uf_save_blob(output_file,data)
							uf_save_report_to_db(output_file,id,true)
						end if  
						uf_save_report_to_db(output_file,id,false)
				end choose 
			case 'TEXT', 'Text'
				if i_add_date = 'yes' then 
					output_file =  temp_dir + "\" + report_number + string(i_start_date, 'mmyyyy') + '.txt'
				else
					output_file =  temp_dir + "\" + report_number + '.txt'
				end if 		
				
				choose case output
					case 'Database'	
						ds.saveas(output_file,HTMLTable!,true)
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						if i_add_date = 'yes' then 
							output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.txt'
						else
							output_file =  directory + "\" + report_number + '.txt'
						end if 		
						ds.saveas(output_file,HTMLTable!,true)
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						ds.saveas(output_file,HTMLTable!,true)
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
						   ds.print()
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							uf_save_report_to_db(output_file,id,true)
						end if
						ds.saveas(output_file,HTMLTable!,true)
						uf_save_report_to_db(output_file,id,false)
				end choose 
//				g_ps_interface.uf_saveas_prn(output_file,HTMLTable!,true)
			case 'Ascii'
				if i_add_date = 'yes' then 
					output_file = temp_dir + '\' + report_number + string(i_start_date, 'mmyyyy') + '.asc'
				else
					output_file = temp_dir + '\' + report_number + '.asc'
				end if 	
				choose case output
					case 'Database'	
						ds.saveasascii(output_file)
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						if i_add_date = 'yes' then 
							output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.asc'
						else
							output_file =  directory + "\" + report_number + '.asc'
						end if 		
						ds.saveasascii(output_file)
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						ds.saveasascii(output_file)
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
						   ds.print()
							uf_save_report_to_db(output_file,id,false)
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							ds.saveasascii(output_file,class,'')
							uf_save_report_to_db(output_file,id,true)
						end if				
				end choose 
		   	case 'Excel'
					if i_add_date = 'yes' then 
						output_file = temp_dir + '\' + report_number + string(i_start_date, 'mmyyyy') + '.asc'
					else
						output_file = temp_dir + '\' + report_number + '.asc'
					end if    
					choose case output
						case 'Database'   
							ds.saveas(output_file,Excel!,true)
							uf_save_report_to_db(output_file,id,true)
						case 'File'
							if i_add_date = 'yes' then 
								output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.xls'
							else
								output_file =  directory + "\" + report_number + '.xls'
							end if       
							ds.saveas(output_file,Excel!,true)
							uf_save_report_to_db(output_file,id,false)
						case 'Mail'
							ds.saveas(output_file,Excel!,true)
							//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
							//uf_send_mail_distri_list() is used later
							//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
							//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
							uf_save_report_to_db(output_file,id,true)
						case 'Printer'         
								f_pp_msgs(" Printer Option is not supported for Excel Files. Will save printer output to database.")
								ds.saveas(output_file,Excel!,true)
								uf_save_report_to_db(output_file,id,true)         
					end choose 
				case 'XML'
					if i_add_date = 'yes' then 
						output_file = temp_dir + '\' + report_number + string(i_start_date, 'mmyyyy') + '.xml'
					else
						output_file = temp_dir + '\' + report_number + '.xml'
					end if    
					choose case output
						case 'Database'   
							ds.saveas(output_file,xml!,true)
							uf_save_report_to_db(output_file,id,true)
						case 'File'
							if i_add_date = 'yes' then 
								output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.xml'
							else
								output_file =  directory + "\" + report_number + '.xml'
							end if       
							ds.saveas(output_file,xml!,true)
							uf_save_report_to_db(output_file,id,false)
						case 'Mail'
							ds.saveas(output_file,xml!,true)
							//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
							//uf_send_mail_distri_list() is used later
							//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
							//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
							uf_save_report_to_db(output_file,id,true)
						case 'Printer'         
								f_pp_msgs(" Printer Option is not supported for XMLl Files. Will save printer output to database.")
								ds.saveas(output_file,xml!,true)
								uf_save_report_to_db(output_file,id,true)
					end choose 
				case 'Text'
					if i_add_date = 'yes' then 
						output_file = temp_dir + '\' + report_number + string(i_start_date, 'mmyyyy') + '.txt'
					else
						output_file = temp_dir + '\' + report_number + '.txt'
					end if    
					choose case output
						case 'Database'   
							ds.saveas(output_file,text!,true)
							uf_save_report_to_db(output_file,id,true)
						case 'File'
							if i_add_date = 'yes' then 
								output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.txt'
							else
								output_file =  directory + "\" + report_number + '.txt'
							end if       
							ds.saveas(output_file,text!,true)
							uf_save_report_to_db(output_file,id,false)
						case 'Mail'
							ds.saveas(output_file,text!,true)
							//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
							//uf_send_mail_distri_list() is used later
							//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
							//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
							uf_save_report_to_db(output_file,id,true)
						case 'Printer'         
								f_pp_msgs(" Printer Option is not supported for Text Files. Will save printer output to database.")
								ds.saveas(output_file,text!,true)
								uf_save_report_to_db(output_file,id,true)
					end choose 
			  
			 case 'PSR' 
				if i_add_date = 'yes' then 
					output_file =  temp_dir + "\" + report_number + string(i_start_date, 'mmyyyy') + '.psr'	
				else
					output_file =  temp_dir + "\" + report_number + '.psr'	
				end if 	
				choose case output
					case 'Database'	
						ds.saveas(output_file,PSReport!	,true)
						uf_save_report_to_db(output_file,id,true)
					case 'File'
						if i_add_date = 'yes' then 
							output_file =  directory + "\" + report_number + string(i_start_date, 'mmyyyy') + '.psr'
						else
							output_file =  directory + "\" + report_number + '.psr'
						end if 								
						ds.saveas(output_file,PSReport!	,true)
						uf_save_report_to_db(output_file,id,false)
					case 'Mail'
						ds.saveas(output_file,PSReport!	,true)
						//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
						//uf_send_mail_distri_list() is used later
						//f_pp_msgs("Sending email '"+ title + "' to " + mailto)
						//g_smtpmail.sendfile(user_name,' ', title,long_desc,mailto, server, output_file)
						uf_save_report_to_db(output_file,id,true)
					case 'Printer'
						if g_ps_interface.uf_get_printer(printer,name,drv_name,port,ds) = 1 then
						   ds.print()
						else
							f_pp_msgs("Can't Find Printer " + printer  + '. Will save printer output to database.')
							uf_save_report_to_db(output_file,id,true)
						end if
						ds.saveas(output_file,PSReport!	,true)
						uf_save_report_to_db(output_file,id,false)
				end choose 
		   case else
				
				
		end choose
	
	// Send an email even when batreporter is rum manually (with input arguments)
   //if g_prog_interface.i_local = false then
		/*START: SEND MAIL FROM DISTRIBUTION LIST*/
		rtn = uf_send_mail_distri_list(id, ds, data)
		if rtn <> 1 then
			f_pp_msgs('--Error Send mail to distribution list: ' + string(id))
		end if
	//end if
		
	update pp_batch_reports set status = 3 where id=:id;
	commit;
	//Update last run date
	update pp_batch_reports set last_run = sysdate where id = :id;
	if sqlca.sqlcode <> 0 then
		rollback;
		f_pp_msgs("Cannot update pp_batch_reports.last_run: " + sqlca.sqlerrtext )
	else
		commit;
	end if
next
				
if rtncode = -1 then
	errmsg = 'Error Retrieve Code = ' + string(uo_dw.i_sqlca_sqlcode) + &
	       ' Msg=' +uo_dw.i_sqlca_sqlerrtext 
	f_pp_msgs(errmsg )
end if

if g_ps_interface.i_debug = 1 or i_debug = 1 then
	num = uo_dw.rowcount()
   f_pp_msgs('Number of Rows = ' + string(num) )
end if
if g_ps_interface.i_debug = 1 or i_debug = 1 then
   f_pp_msgs('Retrieve Finished')
end if

// get all batch reports
declare rep_cur2 cursor for select id from pp_batch_reports;
open rep_cur2;
fetch rep_cur2 into :id;
count = 0
do while sqlca.sqlcode = 0
	count++
	ids[count] = id
	fetch rep_cur2 into :id;
loop
close rep_cur2;

// Clean up data and log files
longlong release_date
for i = 1  to upperbound(ids)
	id = ids[i]
	
	 str_rtn = f_pp_system_control_company('Purge Unviewed Batch Reports', -1)

	select release_report into :release_date from pp_batch_reports where id = :id;
	// remove external files 
	
	   
   if str_rtn = 'no' then

		select count(*) into :rtn
		from pp_batch_rpts_viewed
		where date_viewed is null
		and batch_report_id = :id;
      	/*If it is not viewed and it is one month after the release date then purge it anyway*/
		if count > 0 then 
			select sign(add_months(run_date + :release_date, 1) - sysdate) into :count
			from pp_batch_reports_data where batch_report_id = :id;
			if count >= 0 then
				continue
			end if 
		end if
	end if

	
	declare file_cur cursor for select directory,filename from pp_batch_reports_data where output = 'File' and sysdate - run_date  > :release_date;
	open file_cur;
	fetch file_cur into :dir,:filename;
	do while sqlca.sqlcode = 0 
		dir = dir + '\' + filename
		filedelete(dir)
		fetch file_cur into :dir,:filename;
	loop 
	
	delete from pp_batch_report_args where id in ( select log_id from pp_batch_reports_data where sysdate - run_date  > :release_date) ;
	delete from pp_batch_reports_log where id in ( select log_id from pp_batch_reports_data where sysdate - run_date  > :release_date) ;
	delete from pp_batch_reports_data where sysdate - run_date  > :release_date ;
	
next

destroy g_prog_interface
destroy g_ps_interface
return 0

////	
end function

public function longlong uf_add_schedule_reports ();longlong i, j, rtn, id, mypp_time_id, check, next_id
string sqls, report_name, report_class, str
dateTime Start_date, End_date
blob dw_data
boolean success = true
uo_ds_top ds_report

f_pp_msgs( '--Enter Add Report')

/* Get All reports that is a recurring entry and has a report time period */
sqls =    ' select id, mypp_time_id from pp_batch_reports ' + &
         ' where recurring_entry = 1 ' + &
         ' and mypp_time_id is not null '
         
ds_report = create uo_ds_top
f_create_dynamic_ds(ds_report, "grid", sqls, sqlca, true)

if ds_report.rowcount() < 1 then
   f_pp_msgs( '--No Recurring Report Found')
   success = true
   goto the_end
end if

for i = 1 to ds_report.rowcount()
   id = ds_report.getItemNumber(i,1)
   mypp_time_id = ds_report.getItemNumber(i,2)
   
   select count(*) into :rtn
   from pp_batch_rpts_schedule
   where trunc(schedule_date) = trunc(sysdate)
   and batch_report_id = :id;
   
   if rtn < 1 then
      continue
   end if
   
   /* Set Start Date/End Date base on mypp_time_id */
   setnull(start_date)
   setnull(end_date)
   Choose case mypp_time_id
      case 0   //None
         setnull(start_date)
         setnull(end_date)
      case 1   //Current Month
         select max(accounting_month) into :start_date
         from cpr_control
         where cpr_closed is null;
      case 2   //Prior Month
         select add_months(max(accounting_month), -1) into :start_date
         from cpr_control
         where cpr_closed is null;
      case 3   //Last Closed Month
         select max(accounting_month)  into :start_date
         from cpr_control
         where cpr_closed is not null;
      case 4   //Year to Date
         select to_date(to_char(max(accounting_month),'YYYY')||'01','YYYYMM'),max(accounting_month) 
         into :start_date, :end_date
         from cpr_control
         where cpr_closed is null;
      case 5   //Last 12 Months
         select add_months(max(accounting_month),-12),max(accounting_month) 
         into :start_date, :end_date
         from cpr_control
         where cpr_closed is null;
      case 6   //Last 13 Months
         select add_months(max(accounting_month),-13),max(accounting_month) 
         into :start_date, :end_date
         from cpr_control
         where cpr_closed is null;
      case 7   //Prior Fiscal Year
         check = 0  
         select count(*) into :check 
         from pp_calendar 
         where month in (
         select max(accounting_month)
         from cpr_control, company
         where cpr_closed is null
         and cpr_control.company_id = company.company_id
         );
         
         if check > 0 then  //not using calander year
            select min(month), max(month)
            into :start_date, :end_date
            from pp_calendar
            where fiscal_year in 
            (
               select fiscal_year - 1
               from pp_calendar 
               where month in 
               (
                  select max(accounting_month)
                  from cpr_control, company
                  where cpr_closed is null
                  and cpr_control.company_id = company.company_id
               )
            );         
         else 
            
            select to_date('01-'||year,'MM-YYYY'), to_date('12-'||year,'MM-YYYY')
            into :start_date, :end_date
            from 
            (
               select to_number(to_char(max(accounting_month),'YYYY')) - 1 year
               from cpr_control, company
               where cpr_closed is null
               and cpr_control.company_id = company.company_id
            );
         end if
               
      case 8   //Prior Fiscal Year - 2 years
         check = 0  
         select count(*) into :check 
         from pp_calendar 
         where month in (
         select max(accounting_month)
         from cpr_control, company
         where cpr_closed is null
         and cpr_control.company_id = company.company_id
         );
         
         if check > 0 then//not using calander year
            select min(month), max(month)
            into :start_date, :end_date
            from pp_calendar
            where fiscal_year in 
            (
               select fiscal_year - 2
               from pp_calendar 
               where month in 
               (
                  select max(accounting_month)
                  from cpr_control, company
                  where cpr_closed is null
                  and cpr_control.company_id = company.company_id
               )
            );         
         else 
            
            select to_date('01-'||year,'MM-YYYY'), to_date('12-'||year,'MM-YYYY')
            into :start_date, :end_date
            from 
            (
               select to_number(to_char(max(accounting_month),'YYYY')) - 2 year
               from cpr_control, company
               where cpr_closed is null
               and cpr_control.company_id = company.company_id
            );
         end if
      case 9   //Restore Only - User Selects Months
         select start_date, end_date
         into :start_date, :end_date
         from pp_batch_reports
         where id = :id;
   end choose
   
   /* Insert Report into pp_batch_reports */
   select nvl(max(id),0) + 1 into :next_id from pp_batch_reports;   
   
   insert into pp_batch_reports
   (
   id, report_id, user_name, report_name, status, class, report_date, title, 
   description, directory, printer, mailto, format, release_report, output, 
   end_date, start_date, debug, version, arg_num, arg1_type, arg1_id, 
   arg2_type, arg2_id, arg3_type, arg3_id, arg4_type, arg4_id, recurring_entry, mypp_time_id, notify_email
   )
   select :next_id, report_id, user_name, 
   report_name, 1, class, report_date, title, 
   description, directory, printer, mailto, format, release_report, 
   output, :end_date, :start_date, debug, version, arg_num, 
   arg1_type, arg1_id, 
   arg2_type, arg2_id, 
   arg3_type, arg3_id, 
   arg4_type, arg4_id, 
   0, mypp_time_id, notify_email
   from pp_batch_reports
   where id = :ID;
   if sqlca.sqlcode < 0 then 
      f_pp_msgs( "SQL Errror - Insert into PP Batch Report Failed! ~r~n SQL Error : " + sqlca.sqlerrtext)
      success = false
      goto the_end
   End if
   
   /* Insert Report into pp_batch_report_args */   
   insert into pp_batch_report_args
   (id, pos, arg_string, arg_number, arg_date )
   select :next_id, pos, arg_string, arg_number, arg_date 
   from pp_batch_report_args
   where id = :ID;
   if sqlca.sqlcode < 0 then 
      f_pp_msgs( "SQL Errror - Insert into PP Batch Report Args Failed! ~r~n SQL Error : " + sqlca.sqlerrtext)
      success = false
      goto the_end
   End if

	// JAK 20081001:  This doesn't work if the syntax is over 32k characters...
//   select dw_data into :str
//   from pp_batch_reports
//   where id = :id;
//   
//   dw_data = blob(str)
   
   selectblob dw_data into :dw_data
   from pp_batch_reports
   where id = :id;
   
   updateblob pp_batch_reports 
   set dw_data = :dw_data
   where id = :next_id;
   if sqlca.sqlcode < 0 then 
      f_pp_msgs( "SQL Errror - Update PP Batch Report dw_data Failed! ~r~n SQL Error : " + sqlca.sqlerrtext)
      success = false
      goto the_end
   End if
   
   /* Insert Distribution Groups*/
   insert into pp_batch_rpts_distr_grps(batch_report_id, group_id)
   (
   select :next_id, group_id from pp_batch_rpts_distr_grps
   where batch_report_id = :ID
   minus
   select batch_report_id, group_id from pp_batch_rpts_distr_grps
   ); 
   if sqlca.sqlcode < 0 then 
      f_pp_msgs( "SQL Errror - Insert into Reports Distr Groups Failed! ~r~n SQL Error : " + sqlca.sqlerrtext)
      success = false
      goto the_end
   End if
next

goto the_end
the_end:
if success then
   commit;
   f_pp_msgs( '--Report Added')
   return 1
else
   f_pp_msgs( '--Add Report Failed')
   rollback;
   return -1
end if

end function

public function longlong uf_send_mail_distri_list (longlong a_id, datastore a_ds, blob a_data);longlong rtn, report_id, i, notify_email
string title,long_desc,output,directory,printer,class,report_name, sqls, mmsg, email_link_name, ppt_link
string first_name, mail_id, output_file, format, temp_dir, filename, errmsg, trigger_name, nullstr, users, user_name
boolean success = true
uo_ds_top ds_temp
string server

g_ps_interface.uf_get_host(server)
//g_ps_interface.uf_send_mail(user_name,title,long_desc,mailto,output_file,errmsg)
setnull(nullstr)
select report_id,class,report_name,format,
       title,description,output,directory,printer, notify_email, user_name
into     :report_id,:class,:report_name,:format,
         :title,:long_desc,:output,:directory,:printer, :notify_email, :user_name
from pp_batch_reports where id = :a_id;

temp_dir = g_ps_interface.uf_get_temp_dir()

sqls =    ' select mail_id, first_name, c.users users ' + &
         ' from pp_batch_rpts_distr_grps a, cr_distrib_users_groups b, pp_security_users c ' + &
         ' where a.group_id = b.group_id ' + &
         ' and lower(trim(b.users)) = lower(trim(c.users)) ' + &
         ' and a.batch_report_id = ' + string(a_id)
         
ds_temp = create uo_ds_top
f_create_dynamic_ds(ds_temp, "grid", sqls, sqlca, true)

if ds_temp.rowcount() < 1 then
   f_pp_msgs( '--No Recurring user Found for batch report id: ' + string(a_id))
   goto the_end
end if

do while pos(report_name,' ') > 0 
	report_name = replace(report_name,pos(report_name,' '),1,'_')
loop
do while pos(report_name,'/') > 0 
	report_name = replace(report_name,pos(report_name,'/'),1,'_')
loop
do while pos(report_name,'\') > 0 
	report_name = replace(report_name,pos(report_name,'\'),1,'_')
loop


//setnull(trigger_name)
//
//select trigger_name into :trigger_name from sys.all_triggers
//where upper(trim(table_name)) = 'PP_BATCH_RPTS_VIEWED';
//
//if not isnull(trigger_name) then
//   ALTER TRIGGER :trigger_name disable;
//end if
//
for i = 1 to ds_temp.rowcount()
   mail_id = ds_temp.getItemstring(i,1)
   First_name = ds_temp.getItemstring(i,2)
   users = ds_temp.getItemstring(i,3)
   if i_add_date = 'yes' then 
		output_file =  temp_dir + "\" + report_name + string(i_start_date, 'mmyyyy')
	else
		output_file =  temp_dir + "\" + report_name
	end if 

	
	choose case format
      case 'PDF' 
         output_file = output_file + '.pdf'
      case 'HTML' 
          output_file = output_file + '.htm'   
      case 'Syntax'
          output_file = output_file + '.syntax'
      case 'TEXT', 'Text'
		output_file = output_file + '.txt'
      case 'Ascii'
		output_file = output_file + '.asc'
       case 'PSR' 
          output_file = output_file + '.psr'   
	end choose
	filename =  temp_dir + '\pstemp.ps'

   
   email_link_name = f_pp_system_control_company("EMAIL LINK NAME", -1)
   if isnull(email_link_name) or email_link_name = '' then email_link_name = 'Login To PowerPlant'
   
   ppt_link = f_pp_system_control_company("POWERPLANT EMAIL LINK", -1)
   if isnull(ppt_link) or ppt_link = '' then 
      ppt_link = ' '
   else
      ppt_link = '<br /><br /><A href="'+ppt_link+'">'+email_link_name+'</A>'
   end if
   
   choose case output
      case 'Database'               
         mmsg = first_name + ', <br />The following report is ready for your review. Please Log on to Powerplant to review report ' + &
               '<br />-- Report Classification: ' + class + &
               '<br />-- Report Name: ' + Report_Name + '(Report ID: ' + string(report_id) + ')' + &
               '<br />-- Title: ' + Title + &
               '<br />-- Description: ' + long_desc + &
               ppt_link

      case 'File'
         mmsg = first_name + ', <br />The following report is ready for your review. ' + &
               '<br />-- Report Classification: ' + class + &
               '<br />-- Report Name: ' + Report_Name + '(Report ID: ' + string(report_id) + ')' + &
               '<br />-- Title: ' + Title + &
               '<br />-- Description: ' + long_desc + &
               '<br /> ** File can be access at ' + output_file

      case 'Mail'
		choose case format
			case 'PDF' 
				g_ps_interface.uf_saveas_pdf(filename,output_file)
			case 'HTML' 
				a_ds.saveas(output_file,HTMLTable!, true)
			case 'Syntax'
				uf_save_blob(output_file,a_data)
			case 'TEXT', 'Text'
				a_ds.saveas(output_file, text!, true)
			case 'Ascii'
				a_ds.saveasascii(output_file)          
			 case 'PSR' 
				a_ds.saveas(output_file,PSReport!, true)
		end choose
			
         //g_ps_interface.uf_saveas_pdf(filename,output_file)
         //g_ps_interface.uf_send_mail('pwrplant',title,long_desc,mail_id,output_file,errmsg)
		f_pp_msgs("Sending email '"+ title + "' to " + mail_id)
		//g_smtpmail.sendfile('pwrplant',' ', title,long_desc,mail_id, server, output_file)
		g_msmail.sendfile(user_name,' ', title,long_desc,mail_id, server, output_file)
         uf_save_report_to_db(output_file,a_id,false)
         continue
         
      case 'Printer'
         mmsg = first_name + ', <br />The following report is ready for your review. ' + &
               '<br />-- Report Classification: ' + class + &
               '<br />-- Report Name: ' + Report_Name + '(Report ID: ' + string(report_id) + ')' + &
               '<br />-- Title: ' + Title + &
               '<br />-- Description: ' + long_desc + &
               '<br /> ** The Printout is send to printer ' + printer
   end choose 
   
   setnull(output_file)
   mmsg = '<HTML><BODY><DIV> ' + mmsg + ' </DIV></BODY></HTML>' 
   if notify_email = 1 then
      //g_ps_interface.uf_send_mail('pwrplant',title,mmsg,mail_id,output_file,errmsg)
		//g_smtpmail.send('pwrplant',' ', title,long_desc,mail_id, server)	
		g_msmail.send(user_name,' ', title,long_desc,mail_id, server)	
   end if
   
   insert into pp_batch_rpts_viewed
   (batch_report_id, users, date_viewed) values
   (:a_id, :users, :nullstr);
   
   
next
goto the_end
the_end:
//if not isnull(trigger_name) then
//   ALTER TRIGGER :trigger_name enable;
//end if
if Success then
   return 1
else 
   return -1
End if

end function

on n_bat_reporter.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_bat_reporter.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;uo_dw = create uo_datastore_template
end event

event destructor;disconnect;

end event

