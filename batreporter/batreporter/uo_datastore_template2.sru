HA$PBExportHeader$uo_datastore_template2.sru
forward
global type uo_datastore_template2 from datastore
end type
end forward

global type uo_datastore_template2 from datastore
end type
global uo_datastore_template2 uo_datastore_template2

type variables
string          i_sqlca_sqlerrtext
longlong i_sqlca_sqlcode
string          i_sqlca_sqlsyntax
end variables

on uo_datastore_template2.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_datastore_template2.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event dberror;i_sqlca_sqlcode = sqldbcode
i_sqlca_sqlerrtext = SQLErrText
i_sqlca_sqlsyntax = sqlsyntax
return 1
end event

event error;i_sqlca_sqlcode = errornumber
i_sqlca_sqlerrtext = errortext
i_sqlca_sqlsyntax = errorscript
end event

