HA$PBExportHeader$uo_ps_web.sru
$PBExportComments$Postscript Driver Interface
forward
global type uo_ps_web from nonvisualobject
end type
type s_search from structure within uo_ps_web
end type
type s_search_info from structure within uo_ps_web
end type
type s_object_loc from structure within uo_ps_web
end type
type s_device_mode from structure within uo_ps_web
end type
type s_printer_defaults from structure within uo_ps_web
end type
type s_printer_info2 from structure within uo_ps_web
end type
type s_driver_info2 from structure within uo_ps_web
end type
type s_printer_info2_add from structure within uo_ps_web
end type
type s_printer_list from structure within uo_ps_web
end type
end forward

type s_search from structure
	longlong	page
	longlong	x
	longlong	y
	longlong	h
	longlong	w
end type

type s_search_info from structure
	longlong	count
	string		str
	longlong	option
	s_search		searchlist[200]
end type

type s_object_loc from structure
	string		name
	longlong	x
	longlong	y
	longlong	h
	longlong	w
	string		font
	string		weight
	string		height
	string		family
	string		pitch
end type

type s_device_mode from structure
	string		devicename
	unsignedinteger		specversion
	unsignedinteger		driverversion
	unsignedinteger		size
	unsignedinteger		driverextra
	unsignedlong		field
	unsignedinteger		orientation
	unsignedinteger		papersize
	unsignedinteger		paperlength
	unsignedinteger		paperwidth
	unsignedinteger		scale
	unsignedinteger		copies
	unsignedinteger		defaultsource
	unsignedinteger		printquality
	unsignedinteger		color
	unsignedinteger		duplex
	unsignedinteger		yresolution
	unsignedinteger		collate
	string		formname
	unsignedinteger		logpixels
	unsignedlong		bitsperpel
	unsignedlong		pelswidth
	unsignedlong		pelsheight
	unsignedlong		displayflags
	unsignedlong		displayfrequency
	unsignedlong		cmmethod
	unsignedlong		cmintent
	unsignedlong		mediatype
	unsignedlong		dithertype
	unsignedlong		reserved1
	unsignedlong		reserved2
end type

type s_printer_defaults from structure
	string		datatype
	s_device_mode		devmode
	longlong	access
end type

type s_printer_info2 from structure
	string		ServerName
	string		Printername
	string		sharename
	string		portname
	string		drivername
	string		comment
	string		location
	string		devmode
	string		sepfile
	string		printprocessor
	string		datatype
	string		parameters
	string		security
	string		attributes
	string		priority
	string		defaultpriority
	string		starttime
	string		uitltime
	string		status
	string		cjobs
	string		averageppm
end type

type s_driver_info2 from structure
	longlong	version
	string		name
	string		env
	string		driver_path
	string		data_file
	string		config_file
end type

type s_printer_info2_add from structure
	longlong	servername
	string		printername
	longlong	sharename
	string		portname
	string		drivername
	longlong	comment
	longlong	location
	longlong	devmode
	longlong	sepfile
	string		printprocessor
	string		datatype
	longlong	parameters
	longlong	security
	longlong	attributes
	longlong	priority
	longlong	defaultpriority
	longlong	starttime
	longlong	uitltime
	longlong	status
	longlong	cjobs
	longlong	averageppm
end type

type s_printer_list from structure
	string		printers[100]
end type

global type uo_ps_web from nonvisualobject
end type
global uo_ps_web uo_ps_web

type prototypes
Function long PSInterface(string dir,string infile,string outfile,long typex, long orientation,ref s_search_info search_info) Library "psconvrt.dll" alias for "PSInterface;Ansi"
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long CreateDCA( string lpszDriver, string lpszDevice, string lpszOutput, long data) Library "gdi32.dll" alias for "CreateDCA;Ansi"
Function long DeleteDC( long hdc) Library "gdi32.dll"
Function long GetDeviceCaps(long hdc,long x) Library "gdi32.dll"
Function long GetPrinterOrientation() Library "psconvrt.dll" 
Function long GetPrintDef(ref long orig ,ref long postscript) Library "psconvrt.dll" 
Function long GetPostscriptPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPostscriptPrinter;Ansi" 
Function long GetPrinterInfo( string printer,ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "GetPrinterInfo;Ansi" 
Function long FindAddPostPrinter(ref string name,ref string drv,ref string port) Library "psconvrt.dll" alias for "FindAddPostPrinter;Ansi" 
Function long AddPrinterDriverA(long name, long Level, s_driver_info2 DriverInfo ) Library "winspool.drv" alias for "AddPrinterDriverA;Ansi"
Function long DeletePrinterDriverA(long name, long config, string driver) Library "winspool.drv" alias for "DeletePrinterDriverA;Ansi"
Function long AddPrinterA(long name, long Level, s_printer_info2_add DriverInfo ) Library "winspool.drv" alias for "AddPrinterA;Ansi"
Function long GetLastError() Library "kernel32.dll"
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long  GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
Function long  CopyFileA( string path,string  desination,long stat ) Library "KERNEL32.DLL" alias for "CopyFileA;Ansi"
Function long GetPrintDefPCL(ref long orig ,ref long postscript) Library "pclcnvrt.dll" 
Function long GetPCLPrinter(ref string name,ref string drv,ref string port) Library  "pclcnvrt.dll" alias for "GetPCLPrinter;Ansi" 
Function long PCLInterface(string dir,string infile,string outfile,long typex, long orientation,ref s_search_info search_info) Library "pclcnvrt.dll" alias for "PCLInterface;Ansi" 
Function long GetPrinterOrientationPCL() Library "pclcnvrt.dll" 
Function long SetDefPrinter(string pPrinterName)  Library "psconvrt.dll" alias for "SetDefPrinter;Ansi"
Function long GetDefPrinter(ref string pPrinterName,ref  long pdwBufferSize) Library "psconvrt.dll" alias for "GetDefPrinter;Ansi"
Function long GetPrinter(long hwnd,long ptype ,s_printer_info2  printer_info2 ,long num, ref long rnum) Library "KERNEL32.DLL" alias for "GetPrinter;Ansi"
Function long GetProcAddress(long hmod ,string procname) Library "KERNEL32.DLL" alias for "GetProcAddress;Ansi"
Function long Add_Printer(long apost, string ptr ,string drv) Library "ppaddptr.dll" alias for "Add_Printer;Ansi"
Function long GetShortPathA(string longpath ,ref string shortpath,long buflen) Library "KERNEL32.DLL" alias for "GetShortPathA;Ansi"
Function long GetLongPathNameA(string shortpath ,ref string longpath,long buflen) Library "KERNEL32.DLL" alias for "GetLongPathNameA;Ansi"
Function long LoadLibraryA(string shortpath ) Library "KERNEL32.DLL" alias for "LoadLibraryA;Ansi"

function long smtpmail(string fromuser,string subject,string text,string touser,string server) library "smtpmail.dll"
function long smtpmailex(string fromuser,string subject,string text,string touser,string server,string filename,string err) library "smtpmail.dll"
Function long GetWindowsDirectoryA(ref string var,long len) Library "kernel32.dll"  alias for "GetWindowsDirectoryA;Ansi" 
Function long ListPrinters(long name, ref s_printer_list printer_list ,long pos,long max_printers) Library "psconvrt.dll" 
Function long gzopen (string path, string mode ) Library "zlib.dll"  alias for "gzopen;Ansi"
Function long gzwrite (long file, blob buf, ulong len) Library "zlib.dll"    alias for "gzwrite;Ansi" 
Function long gzclose (long file) Library "zlib.dll"   alias for "gzclose;Ansi" 

end prototypes

type variables
public:
uo_sqlca u_sqlca
longlong i_debug = 0
string i_dir
private:
s_search_info search_info
s_search search
longlong SEARCH_TYPE = 2
longlong TEXT_TYPE = 1
longlong PDF_TYPE = 3
longlong PRN_TYPE = 6
longlong FIX_TYPE = 5
longlong LANDSCAPE = 1
longlong PORTRAIT = 0
longlong POST_TYPE =4
longlong search_pos

boolean i_initialize_dll=false
boolean i_initialize_zlib=false
boolean i_initialize_pcl=false
boolean i_initialize_ps=false
boolean i_initialize_print=false
boolean i_initialize_print_pcl = false
string i_print_file
s_object_loc i_object_loc[]
longlong i_orig
longlong LOGPIXELSX   = 88
longlong LOGPIXELSY =  90 
boolean redraw = true
int i_default_printer_orig
string i_path
boolean i_add_printer = false

end variables

forward prototypes
public function integer uf_highlight_line (datawindow a_dw, longlong a_page, longlong a_x, longlong a_y, longlong a_h, longlong a_w)
public function integer uf_reset_fonts_pcl (datastore a_dw)
public function longlong uf_find_string (string a_filename, string a_str, ref longlong a_page, ref longlong a_x, ref longlong a_y, ref longlong a_h, ref longlong a_w)
public function integer uf_get_display_pixels (ref longlong a_xpixels, ref longlong a_ypixels)
private function integer uf_find_path (ref string a_path)
public function longlong uf_saveas_fix_pcl (string a_filename, string a_outfile)
public function longlong uf_saveas_text (string a_filename, string a_outfile)
public function integer uf_get_print_defs ()
public function longlong uf_saveas_prn_pcl (string a_filename, string a_outfile)
public function string uf_get_printer_list ()
public function string uf_call_function (string a_func, string a_args, uo_sqlca a_sqlca)
public function string uf_get_mail_list ()
public function string uf_get_user_mailid ()
public function integer uf_reset_fonts (datastore a_dw)
public function string uf_print_file_pcl (datastore a_dw)
public function integer uf_set_fonts_pcl (datastore a_dw)
public function integer uf_set_fonts (datastore a_dw)
private function integer uf_get_objects (datastore a_dw)
public function string uf_get_temp_dir ()
public function longlong uf_saveas_pdf (string a_filename, string a_outfile)
public function string uf_export_file (datastore a_ds, string a_type, string a_session_id)
public function string uf_mail_print (uo_datastore_template a_dw, string a_session_id, string a_report_name, string a_type, string a_touser, string a_fromuser, string a_msg)
public function integer uf_load_library (string a_lib, ref string a_err)
public function string uf_get_host (ref string a_server)
private function integer uf_add_printer (longlong a_post, ref string a_ptr_name, ref string a_drv_name)
public function boolean uf_get_user (ref string a_user)
public function boolean uf_send_mail (string a_fromuser, string a_subject, string a_text, string a_touser, string a_filename, ref string a_err_msg)
public function string uf_print_file_pcl2 (datastore a_dw)
public function string uf_print_file_ps (datastore a_dw, string a_filename)
public function integer uf_get_printer (string a_printer, ref string a_name, ref string a_device, ref string a_port, datastore a_ds)
public function integer uf_set_default_printer (string a_printer, datastore a_ds)
public function integer uf_selected_printer (longlong a_printer_type, datastore a_ds)
public function string uf_print_file (datastore a_dw)
public function string uf_get_default_printer ()
end prototypes

public function integer uf_highlight_line (datawindow a_dw, longlong a_page, longlong a_x, longlong a_y, longlong a_h, longlong a_w);longlong xunits,yunits,pb_units
longlong adjustment = 70
longlong barheight = 200
longlong width,x
longlong left_margin,bottom_margin,i
string modstring,str
longlong count,count2,count3
 
if i_orig = PORTRAIT then
   width = 7500
 else
   width = 10500 
end if

if long(a_dw.object.datawindow.units) = 0 then // PowerBuilder Units
	// no. pixel per inch
	uf_get_display_pixels(xunits,yunits)
	pb_units= PixelsToUnits ( xunits, XPixelsToUnits!   )
	a_x = (pb_units * a_x) /1000
	  // no. pixel per inch
	pb_units = PixelsToUnits (yunits, YPixelsToUnits! )
	a_y = (pb_units * a_y ) /1000
	
	adjustment = (pb_units * adjustment ) /1000
	barheight = (pb_units * barheight ) /1000
	width = (pb_units *  width ) /1000
elseif long(a_dw.object.datawindow.units) = 1 then // Display Pixel Units
   uf_get_display_pixels(xunits,yunits)
	// no. pixel per inch
	a_x = (xunits * a_x) /1000
	  // no. pixel per inch	
	a_y = (yunits * a_y ) /1000
	
	adjustment = (yunits * adjustment ) /1000
	barheight = (yunits * barheight ) /1000
	width  = (yunits * width  ) /1000
elseif  long(a_dw.object.datawindow.units) = 2 then // 1/1000 inches Units
  // do nothing 
else  // 1/1000 centimeter Units

end if
 
 
 left_margin = 0 //long(a_dw.object.datawindow.print.margin.left)
 bottom_margin = 0
 x = long(a_dw.object.datawindow.print.margin.top)
 a_x = 0
 //x1  - left_margin
 a_y =  a_y  + adjustment
 //+ bottom_margin - 180
 a_h =  barheight
 
 a_w = width

a_dw.setredraw(false)
a_dw.scrolltorow(0)
count2 = 0

if a_page > 1 then
 for i = 2 to a_page
	count3 = long(a_dw.object.DataWindow.LastRowOnPage)
	a_dw.scrolltorow(count3 + 1)
 next
end if

a_dw.setredraw(true)


modstring = 'destroy rect1'
a_dw.Modify(modstring)
modstring = 'create rectangle(Band=foreground X="' + string(a_x) + '" Y="' + string(a_y) + &
            '" height="' + string(a_h) + '" width="' + string(a_w) + '" ' + &
            'brush.hatch="7" brush.color="12632256" pen.width="19"  pen.style="0"  ' + &
				'pen.color="255" background.mode="2" background.color="0" name=rect1 )'
str = a_dw.Modify(modstring)
a_dw.setredraw(true)
return 1
end function

public function integer uf_reset_fonts_pcl (datastore a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
	return 0
end if

for i = 1 to upperbound(i_object_loc)
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='" + i_object_loc[i].font + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.height='" + i_object_loc[i].height + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.weight='" + i_object_loc[i].weight + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.family='" + i_object_loc[i].family + "'")
	  str = a_dw.modify(i_object_loc[i].name + ".Font.pitch='" + i_object_loc[i].pitch + "'")
next 

return 1
end function

public function longlong uf_find_string (string a_filename, string a_str, ref longlong a_page, ref longlong a_x, ref longlong a_y, ref longlong a_h, ref longlong a_w);search_pos = 1
search_info.str = a_str
search_info.count = 0

PSInterface(i_dir,a_filename,"",SEARCH_TYPE, i_orig,search_info)
search = search_info.searchlist[search_pos]
a_page = search.page
a_x = search.x
a_y = search.y
a_h = search.h
a_w = search.w

return search_info.count
end function

public function integer uf_get_display_pixels (ref longlong a_xpixels, ref longlong a_ypixels); longlong hdc
 hdc = CreateDCA( "DISPLAY", "", "", 0)
 a_xpixels = GetDeviceCaps(hdc,LOGPIXELSX)
 a_ypixels = GetDeviceCaps(hdc,LOGPIXELSY)
 DeleteDC(hdc)
 return 1
end function

private function integer uf_find_path (ref string a_path);longlong mod
string path
longlong i,status,len
string str
setnull(str)
mod =  GetModuleHandleA(str)
if mod <> 0 then
	path = space(601)
	status = GetModuleFileNameA(mod,path,600)
	if status <> 0 then
		len = status
		for i = len to 1 step -1
			if mid(path,i,1) = '\' then
				a_path = mid(path,1,i)
				return 1
			end if
		next
	end if
end if
return -1
end function

public function longlong uf_saveas_fix_pcl (string a_filename, string a_outfile);longlong status
string temp_dir
search_pos = 1


temp_dir = g_ps_interface.uf_get_temp_dir()
status = PCLInterface(temp_dir,a_filename,a_outfile, FIX_TYPE, 200 ,search_info)


return status
end function

public function longlong uf_saveas_text (string a_filename, string a_outfile);longlong status
search_pos = 1


status = PSInterface(i_dir,a_filename,a_outfile,TEXT_TYPE, i_orig ,search_info)


return status
end function

public function integer uf_get_print_defs ();longlong status
return  i_default_printer_orig


end function

public function longlong uf_saveas_prn_pcl (string a_filename, string a_outfile);longlong status
string temp_dir
search_pos = 1

temp_dir = g_ps_interface.uf_get_temp_dir()
status = PCLInterface(temp_dir,a_filename,a_outfile, PRN_TYPE, i_orig ,search_info)


return status
end function

public function string uf_get_printer_list ();string list
string printers[100]
s_printer_list printer_list
longlong i,num
for i = 1 to upperbound(printer_list.printers)
   printer_list.printers[i] = space(400)
next
num = ListPrinters(0,printer_list ,0,100)
if num < 1 then
  return "Error"
end if
list = ""
for i = 1 to num
   list = list + trim(printer_list.printers[i]) 
   if i <> num then
     list = list + ','
   end if
next
return list
end function

public function string uf_call_function (string a_func, string a_args, uo_sqlca a_sqlca);u_sqlca = a_sqlca
choose case a_func
	case "uf_get_printer_list"
	   return this.uf_get_printer_list()
   case "uf_get_mail_list" 
		return this.uf_get_mail_list()
	case 'uf_get_user_mailid'	
   	return this.uf_get_user_mailid()
end choose

return "Error"
end function

public function string uf_get_mail_list ();string user_list
//declare user_cur cursor for  select u.mail_id from pp_security_users u ,pp_security_users_groups g
//	             where  rtrim(u.users) = rtrim(g.users) and g.groups = lower(:touser);
//	open user_cur;
//	fetch user_cur into :mail_id;
//	do while  sqlca.sqlcode = 0 
//		if isnull(mail_id) = false then
//			count++
//		   users[count] = mail_id
//		end if
//		fetch user_cur into :mail_id;
//	loop
//	close user_cur;
//	if count = 0 then
//		 a_err_msg = 'No Mail IDs exist for the group ' + touser  
//   	 return false
//   end if
//elseif uf_get_user(touser) = false then
//   a_err_msg =   'No Mail ID exist for ' + touser 
//   return false
string user_id,mail_id,full_name
declare user_cur cursor for  select users,mail_id,
         nvl(first_name,'') || ' ' || nvl(last_name,'') 
	from pp_security_users where mail_id is not null using u_sqlca;
open user_cur;
user_list = ''
fetch user_cur into :user_id,:mail_id,:full_name;

do while u_sqlca.sqlcode = 0 
	user_list = user_list + user_id + ','  + mail_id + ',' + full_name + '~t'
	fetch user_cur into :user_id,:mail_id,:full_name;
loop

close user_cur;
return user_list
end function

public function string uf_get_user_mailid ();string mail_user

	select mail_id  into :mail_user from pp_security_users where lower(rtrim(users)) = lower(rtrim(user)) using u_sqlca;
	if u_sqlca.sqlcode <> 0 then		
		Return ''
	end if
	if isnull(mail_user) then 
	   return ''
   end if



return mail_user
end function

public function integer uf_reset_fonts (datastore a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
	return 0
end if

for i = 1 to upperbound(i_object_loc)
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='" + i_object_loc[i].font + "'")
next

return 1
end function

public function string uf_print_file_pcl (datastore a_dw);string temp_dir,str
string temp_file
longlong status,ret
long orig,pcl
string printer,drv,port
string ptr1,ptr2
string drv_name,name
s_object_loc i_object_loc_null[]
ptr2 = ''


// Check for a PCL printer
ptr1 = ProfileString("win.ini","windows","device","")
if uf_selected_printer(TEXT_TYPE,a_dw) = 1 then
	ptr2 = 'change'
elseif uf_get_printer('ppc_hp',name,drv_name,port,a_dw) = 1 then
	ptr2 = 'change'
else
	GetPrintDefPCL(orig,pcl)
	if pcl <> 1 then
		printer = space(200)
		drv = space(200)
		port = space(100)
		if GetPCLPrinter(printer,drv,port) = -1 then
			string ptr_name
			//ret = MessageBox("Information","Can't find a HP printer, do you want to add a printer",Question!,YesNo! )		
			//if ret = 2 then return -1
			ptr_name = 'ppc_hp'
			drv_name = 'HP LaserJet 4MP'
			if uf_add_printer(0,ptr_name,drv_name) = -1 then return "Error adding Printer " + ptr_name 
			ptr1 = ProfileString("win.ini","windows","device","")
			ptr2 = ptr_name + ',' + 'winprint' + ',' + 'LPT1:'
			SetProfileString ( "win.ini","windows","device", ptr2 )
		else
			ptr1 = ProfileString("win.ini","windows","device","")
			ptr2 = printer + ',' + 'winprint' +  ',' + port
			SetProfileString ( "win.ini","windows","device", ptr2 )
		end if
	end if
end if

// this turn HPGL Mode for graphics 
environment env
getenvironment(env)
if env.OSMajorRevision	= 4 and env.OSType = Windows! then // WIndows 95
   SetProfileString ( "hp5si.ini",printer,"ManualPrintModel", "4" )
end if


temp_file = i_print_file

a_dw.object.datawindow.print.filename = temp_file


  
i_object_loc = i_object_loc_null
uf_set_fonts_pcl(a_dw)

i_orig = long(a_dw.describe("DataWindow.print.Orientation"))	
if i_orig = 0 then
	//i_orig = landscape
	//str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	status = GetPrinterOrientationPCL()
	if status = -1 then
		i_orig = landscape
		str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	elseif status = 1 then
		i_orig = portrait
	else
		i_orig = landscape
	end if
elseif i_orig = 2 then
	i_orig = portrait
end if
//status = GetPrinterOrientation()
//if status = -1 then
//	i_default_printer_orig = landscape
//elseif status = 1 then
//	i_default_printer_orig = portrait
//else
//   i_default_printer_orig = landscape
//end if

a_dw.print()
uf_reset_fonts_pcl(a_dw)
// set printer back to the default

if ptr2 <> '' and len(trim(ptr1)) > 0  then
	SetProfileString ( "win.ini","windows","device", ptr1 )
end if

a_dw.Modify("DataWindow.Print.Preview=yes")

a_dw.Modify("datawindow.print.filename = ''")


setpointer(hourglass!)
return ""
end function

public function integer uf_set_fonts_pcl (datastore a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
  uf_get_objects(a_dw)
else
	return 0
end if

for i = 1 to upperbound(i_object_loc)
	  i_object_loc[i].font =   a_dw.describe(i_object_loc[i].name + ".Font.Face")
     i_object_loc[i].height = a_dw.describe(i_object_loc[i].name + ".Font.height")
	  i_object_loc[i].weight = a_dw.describe(i_object_loc[i].name + ".Font.weight")
	  i_object_loc[i].family = a_dw.describe(i_object_loc[i].name + ".Font.family")
	  i_object_loc[i].pitch =  a_dw.describe(i_object_loc[i].name + ".Font.pitch")
	  
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='LinePrinter'")   // 'Helvetica' 'MS Sans Serif'
     str = a_dw.modify(i_object_loc[i].name + ".font.height='-8'")
	  str = a_dw.modify(i_object_loc[i].name + ".font.weight='400'")
	  str = a_dw.modify(i_object_loc[i].name + ".font.family='1'")
	  str = a_dw.modify(i_object_loc[i].name + ".font.pitch='1'")
	  
	//  str = a_dw.modify(i_object_loc[i].name + ".Width=10")
next

return 1
end function

public function integer uf_set_fonts (datastore a_dw);string str
longlong i

if upperbound(i_object_loc) < 1 then
  uf_get_objects(a_dw)
else
	return 0
end if

for i = 1 to upperbound(i_object_loc)
	  i_object_loc[i].font = a_dw.describe(i_object_loc[i].name + ".Font.Face")
     str = a_dw.modify(i_object_loc[i].name + ".Font.Face='Helvetica'")   // 'Helvetica' 'MS Sans Serif'
next

return 1
end function

private function integer uf_get_objects (datastore a_dw);string objects
string name,edit_str
string object_type,str
longlong i,num,pos,start_pos
string object_list[]
longlong count = 0
s_object_loc object_null[]

i_object_loc =  object_null

str = a_dw.describe("DataWindow.syntax")
//if i_debug = 1 then  f_pp_msgs( "DataWindow Syntax"  + str)
objects = a_dw.describe("DataWindow.Objects")
if i_debug = 1 then  f_pp_msgs( "DataWindow Objects"  )
pos = pos(objects,"~t")
do while pos > 1
	count++
	object_list[count] = mid(objects,start_pos,pos - start_pos)
	start_pos = pos + 1
	pos = pos(objects,"~t",start_pos)
loop

count++
object_list[count] = mid(objects,start_pos)
for i = 1 to count
	i_object_loc[i].x = long(a_dw.describe(object_list[i] + ".x"))
	i_object_loc[i].y = long(a_dw.describe(object_list[i] + ".x"))
	i_object_loc[i].w = long(a_dw.describe(object_list[i] + ".width"))
	i_object_loc[i].h = long(a_dw.describe(object_list[i] + ".height"))
	i_object_loc[i].name = object_list[i]
next

return upperbound(i_object_loc)

end function

public function string uf_get_temp_dir ();string str
longlong status 
string ini_file
string temp
string str1
longlong len 

str = space(200)
str1 = space(600)
status = GetEnvironmentVariableA('TEMP',str,200)
if status = 0 then
	status = GetLastError()
	return ""
end if


ini_file = f_get_ini_file() 

temp = ProfileString(ini_file, "Application", "Temp", "None")
if temp <> 'None'  and  temp <> '' then
	str = temp
end if
len = GetLongPathNameA(str,str1,600)
if len < 1 then
	return str
end if
return str1
end function

public function longlong uf_saveas_pdf (string a_filename, string a_outfile);longlong status
search_pos = 1
longlong orig
if i_debug = 1  then
   f_pp_msgs( "Creating PDF file " + a_filename + &
	         "dir =" + i_dir  + " outfile=" + a_outfile)
end if
status = PSInterface(i_dir,a_filename,a_outfile,PDF_TYPE,i_orig ,search_info)

if i_debug = 1  then
   f_pp_msgs( "PSInterface status =" + string(status))
end if
return status
end function

public function string uf_export_file (datastore a_ds, string a_type, string a_session_id);string title ,filename
string temp_file,temp_dir,str
string gzfilename
longlong pos,i
longlong gzunit,unit
blob sblob
longlong len,wlen

uo_datastore_template ds
title = "Select File Name and File Type"

string err
temp_dir = g_ps_interface.uf_get_temp_dir()
ds = a_ds
filename = temp_dir + "\" + a_session_id + "." + a_type 

choose case a_type
	case 'xls'
 		ds.saveas(filename,Excel5!,true)
	case 'txt'
		ds.saveas(filename,Text!,true)
	case 'csv'	
		ds.saveas(filename,CSV!,true)
	case 'wks'	
		ds.saveas(filename,WKS!,true)
	case 'sql'	
		ds.saveas(filename,SQLInsert!,true)
	case 'psr'	
		ds.saveas(filename,PSReport!,true)
	case 'htm'	
		ds.saveas(filename,HTMLTable!,true)
	case 'wmf'	
		ds.saveas(filename,wmf!,true)
end choose



return filename
end function

public function string uf_mail_print (uo_datastore_template a_dw, string a_session_id, string a_report_name, string a_type, string a_touser, string a_fromuser, string a_msg);string title,pathname,filename
string temp_file,temp_dir,str
longlong pos,i
string err_msg
uo_datastore_template ds


//ds = a_dw.ids_datastore




temp_dir = g_ps_interface.uf_get_temp_dir()

pathname = temp_dir + "\" + a_session_id + "." + a_type 
i_print_file = temp_dir + "\" + a_session_id + ".prt" 

i_orig = long(ds.Object.DataWindow.print.Orientation)	
if i_orig = 1 then
	i_orig = LANDSCAPE
else
	i_orig = portrait
end if


if a_type = "txt"  then
  
	err_msg = uf_print_file(ds)
   if err_msg <> "" then
     return ""
   end if
	pathname = g_ps_interface.uf_get_temp_dir() + '\report.txt'
	uf_saveas_text(i_print_file,pathname)
elseif a_type = "pdf" then 
   err_msg = uf_print_file(ds)
   if err_msg <> "" then
     return ""
   end if
	uf_saveas_pdf(i_print_file,pathname)
elseif a_type = "prn"  then 
	err_msg = uf_print_file_pcl(ds)
   if err_msg <> "" then
     return ""
   end if
	uf_saveas_prn_pcl(i_print_file,pathname)
elseif a_type = "fix"  then 
	err_msg = uf_print_file_pcl(ds)
   if err_msg <> "" then
     return ""
   end if
	uf_saveas_fix_pcl(i_print_file,pathname)
else
	//MessageBox("Error","Unknown file type.")
	return "Unknown file type: " + a_type
end if

string msg
//blob b = blob('')

msg = a_msg
//from_user = sqlca.logid
if uf_send_mail(a_fromuser,a_report_name,msg,a_touser,pathname,err_msg) = false then
  return err_msg
end if

filedelete(pathname)
filedelete(i_print_file)          

return ""

end function

public function integer uf_load_library (string a_lib, ref string a_err);string ini_file,str
string path
string err
string exe_path
longlong ret
integer i
integer str_len
integer not_found = 1
environment env

getenvironment(env)

// 16 bit enironment not supported
if env.win16 = true then
	return 0
end if



string dir
longlong status
string filename
//dir = space(200)
//status = GetWindowsDirectoryA(dir,200)
//if status = 0 then
//	status = GetLastError()
//   u_prog_interface.uf_write_log("Error getting system directory, code = " + string(status))
//	return -1
//end if
//filename = dir + "\pwrplant.ini"
//if fileexists(filename) = false then
//     u_prog_interface.uf_write_log("Can't find the ini file " + filename)
//	  return -1
//end if

filename = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")



path = ProfileString(filename, "Application", "PostscriptConverter", "")


if FileExists(path + '\' + a_lib) = true then
	i_dir = path
	str = path + '\' + a_lib
else
  uf_find_path(exe_path)
  i_dir = exe_path
  str = exe_path + a_lib  
  if FileExists(str) = true then

  else
	  //messagebox("Warning","Warning the DLL library " + str + " does not exist.",exclamation!,Ok!)
     a_err = "Warning the DLL library " + str + " does not exist."
	  return -1
  end if
end if
  
 // if i_debug  = 1 then
    f_pp_msgs( "Load Lib: " + str)
 // end if

  ret = LoadLibraryA(str)
 
  choose case ret
        case 0
			  return 1
           	err = 'System was out of memory, executable file was corrupt, or relocations ' + &
                  'were invalid.' 
	     case 2
            err = 'File was not found.' 
        case 3	
            err = 'Path was not found.' 
        case 5	
            err = 'Attempt was made to dynamically link to a task, or there ' + &
                  'was a sharing or network-protection error.' 
        case 6	
            err = 'Library required separate data segments for each task.' 
        case 8	
            err = 'There was insufficient memory to start the application.' 
        case 10	
            err = 'Windows version was incorrect.' 
        case 11	
            err = 'Executable file was invalid. Either it was not a ' + &
                   'Windows application or there was an error in the .EXE image.' 
        case 12	
            err = 'Application was designed for a different operating system.' 
        case 13	
            err = 'Application was designed for MS-DOS 4.0.' 
        case 14	
            err = 'Type of executable file was unknown.' 
        case 15	
            err = 'Attempt was made to load a real-mode application (developed for an ' + &
                  'earlier version of Windows).' 
        case 16	
            err = 'Attempt was made to load a second instance of an executable file containing ' + &
                  'multiple data segments that were not marked read-only.' 
        case 19	
            err = 'Attempt was made to load a compressed executable file. The file must ' + &
                  'be decompressed before it can be loaded.' 
        case 20	
            err = 'Dynamic-link library (DLL) file was invalid. One of the DLLs required ' + &
                  'to run this application was corrupt.' 
        case 21	
            err = 'Application requires Microsoft Windows 32-bit extensions. '
        case is > 32 
			   i_initialize_dll=true
            return 1
        case is < 0
			   i_initialize_dll=true
            return 1 
        case else
            err = 'Unknown Error Code = ' + string(ret)
     end choose
//MessageBox("Error",err)
a_err = err
return -1  


end function

public function string uf_get_host (ref string a_server);string ini_file,host_name
string dir,filename
longlong status
// host name was not given
if a_server = '' then
		dir = space(200)
		filename = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")
		host_name = ProfileString(filename, "Mail", "SMTP_HOST", "")
		if host_name = '' then
			return "NO Host Name"
		end if
		a_server = host_name
end if
return ""
end function

private function integer uf_add_printer (longlong a_post, ref string a_ptr_name, ref string a_drv_name);//string title ,filename
//string temp_file,temp_dir,str
//string gzfilename
//longlong pos,i
//longlong gzunit,unit
//blob sblob
//longlong len,wlen
//
//uo_datastore_template ds
//title = "Select File Name and File Type"
//
//string err
//temp_dir = uf_get_temp_dir()
//ds = a_ds
//filename = temp_dir + "\" + a_session_id + "." + a_print_type 
//i_print_file = temp_dir + "\" + a_session_id + ".prt" 
//gzfilename = temp_dir + "\" + a_session_id + ".gz" 
//
//if a_print_type = 'txt'  then
//      err = uf_print_file(ds)
//	   if  err <> "" then 
//			return err
//		end if
//  
//      uf_saveas_text(i_print_file,filename)
//elseif a_print_type = 'pdf'  then 
//      err = uf_print_file(ds)
//	   if  err <> "" then 
//			return err
//		end if
//	   uf_saveas_pdf(i_print_file,filename)
//elseif a_print_type = 'asc' then 
//	    
//	//ds.saveasascii(filename,"~t",'"',"~r~n")
////	datastore ds2
////	ds2 = create datastore 
////	ds2.DataObject = ds.DataObject
////	ds.RowsCopy(1, ds.RowCount(), Primary!, ds2, 1, Primary!)
////	uo_scroll_grid u_scroll_grid
////	u_scroll_grid = create uo_scroll_grid
////	s_col_object col_object[]
////	u_scroll_grid.i_ds = ds
////	u_scroll_grid.uf_get_table_info(col_object)
//	
//	
//	ds.modify("DataWindow.HTMLGen.Browser=''")
//	ds.modify("DataWindow.HTMLGen.ClientComputedFields=''")
//	ds.modify("DataWindow.HTMLGen.ClientEvents='0'")
//	ds.modify("DataWindow.HTMLGen.ClientFormatting='0'")
//	ds.modify("DataWindow.HTMLGen.ClientScriptable='0'")
//	ds.modify("DataWindow.HTMLGen.ClientValidation='0'")
//	ds.modify("DataWindow.HTMLGen.GenerateJavaScript='0'")
//	ds.modify("DataWindow.HTMLGen.HTMLVersion='0'")
//	ds.modify("DataWindow.HTMLGen.PageSize='0'")	
//	ds.modify("DataWindow.HTMLDW='no'") 
//	ds.saveasascii(filename)
//elseif a_print_type = 'prn'  then 
//	
//	err =  uf_print_file_pcl(ds)  
//	if  err <> "" then 
//			return err
//	end if
//	uf_saveas_prn_pcl(i_print_file,filename)
//elseif a_print_type = 'fix' then 
//	err =  uf_print_file_pcl(ds)  
//	if  err <> "" then 
//			return err
//	end if
//	uf_saveas_fix_pcl(i_print_file,filename)
//else
//	return "Error Unknown file type. (" + a_print_type + ")"
//end if
//return filename
////gzunit = gzopen(gzfilename,"wb")
////unit = fileopen(filename,StreamMode!,Read! )
////len = 0
////do while len > -1
////   len = fileread(unit,sblob)
////   pos = 1
////   do while len > 0
////       wlen = gzwrite(gzunit,blobmid(sblob,pos),len)
////       len = len - wlen
////       pos = pos + wlen
////   loop
////loop
////gzclose(gzunit)
////fileclose(unit)
return 1//gzfilename
//
end function

public function boolean uf_get_user (ref string a_user);string mail_user
if pos(a_user,'@',1) = 0 then
	select mail_id  into :mail_user from pp_security_users where lower(rtrim(users)) = lower(rtrim(:a_user));
	if sqlca.sqlcode <> 0 then
		
		Return false
	end if
	if isnull(mail_user) then 
	   return false
   end if
	a_user = mail_user
end if

return true
end function

public function boolean uf_send_mail (string a_fromuser, string a_subject, string a_text, string a_touser, string a_filename, ref string a_err_msg);//
//   a_fromuser   string - from mail id if null use sqlca.logid and lookup mail address   
//   a_subject    string - subject of mail message
//   a_text       string - mail message
//   a_touser     string - to mail id  if touser does not contain a @ char, lookup mail address
//   a_filename   string - filename used on the attachment
//

longlong longResult
longlong data_len
int unit
string touser,fromuser,server
blob data,data_temp
environment env
string err
getenvironment(env) 
string mail_user
string users[]
string mail_id
longlong count,num

touser = a_touser
fromuser = a_fromuser

if touser = '' then
	touser = sqlca.logid
end if

// is this a real mail address

if uf_get_user(fromuser) = false then
   a_err_msg = "No Mail ID exist for " + fromuser  
   //return false //try using it any way
end if

count = 0

select count(*) into :num from pp_security_groups where lower(groups) = lower(:touser);
if num > 0 then
	declare user_cur cursor for  select u.mail_id from pp_security_users u ,pp_security_users_groups g
	             where  rtrim(u.users) = rtrim(g.users) and g.groups = lower(:touser);
	open user_cur;
	fetch user_cur into :mail_id;
	do while  sqlca.sqlcode = 0 
		if isnull(mail_id) = false then
			count++
		   users[count] = mail_id
		end if
		fetch user_cur into :mail_id;
	loop
	close user_cur;
	if count = 0 then
		 a_err_msg = 'No Mail IDs exist for the group ' + touser  
   	 return false
   end if
//elseif uf_get_user(touser) = false then
//   a_err_msg =   'No Mail ID exist for ' + touser 
//   return false
end if

uf_get_host(server)

//if uf_load_library("smtpmail.dll",a_err_msg) = -1 then
//   return false
//end if

// save print data
longlong bytes_read,len
blob indata
delete pp_mail_data where sessionid = userenv('SESSIONID');	

insert into  pp_mail_data(sessionid) values(userenv('SESSIONID'));

if sqlca.sqlcode <> 0 then 
  a_err_msg = "insert into pp_batch_report_data code=" + string(sqlca.sqldbcode) + ' msg=' + sqlca.sqlerrtext
  return false
end if

unit = FileOpen(a_filename, StreamMode!, Read!, Shared! )	
  // Read the file
	bytes_read = 1
	data = blob('')
	len = 0
	do while bytes_read > 0
		bytes_read = FileRead(unit,indata)
		if bytes_read > 0 then
		  data = data + indata
		  len += bytes_read
		end if	
	loop
FileClose(unit)
updateblob  pp_mail_data set data=:data where sessionid = userenv('SESSIONID');

if sqlca.sqlcode <> 0 then
   a_err_msg = "updateblob pp_batch_report_data code=" + string(sqlca.sqldbcode) + ' msg=' + sqlca.sqlerrtext
	return false
end if



longlong i
if count = 0 then 
	  sqlca.pp_send_mail(fromuser, touser,  ' ',a_subject, a_text, touser,server, a_filename);
	  delete pp_mail_data where sessionid = userenv('SESSIONID');	
	  //longResult= smtpmailex(fromuser,a_subject,a_text,touser,server,a_filename,err)
else
	for i = 1 to count 
		sqlca.pp_send_mail(fromuser, touser,  ' ',a_subject, a_text, users[i],server, a_filename);
	   //longResult= smtpmailex(fromuser,a_subject,a_text,users[i],server,a_filename,err)
	next
end if
return true

end function

public function string uf_print_file_pcl2 (datastore a_dw);string temp_dir,str
string temp_file
longlong status,ret
long orig,postscript
string printer,drv,port
string ptr1,ptr2
string drv_name,name
s_object_loc i_object_loc_null[]
ptr2 = ''


// Check for a postscipt printer
ptr1 = ProfileString("win.ini","windows","device","")
if i_debug = 1 then  f_pp_msgs( "Printer " + ptr1)

// Does the user have s pre-selected printer from the ini file
if uf_selected_printer(PRN_TYPE,a_dw) = 1 then
	ptr2 = 'change'
	if i_debug = 1 then  f_pp_msgs( "Printer  PCL_TYPE" )
end if

// Is the standard PowerPlant printer defined	

if uf_get_printer('ppc_hp',name,drv_name,port,a_dw) = 1 then
	ptr2 = 'change'
	if i_debug = 1 then  f_pp_msgs( "Printer  ppc_hp" )
else
	// Get the default printer and is it a postscript printer
	if i_debug = 1 then  f_pp_msgs( "Get Printer Default " )
	GetPrintDef(orig,postscript)
	ptr1 = ProfileString("win.ini","windows","device","")
	ptr2 = printer + ',' +'winprint' + ',' + port
	SetProfileString ( "win.ini","windows","device", ptr2 )

end if
   
i_print_file =  g_ps_interface.uf_get_temp_dir() + '\pcltemp.pcl'
if i_debug = 1 then  f_pp_msgs( "Printer File " + i_print_file )

string rc
rc = a_dw.modify("DataWindow.Print.Filename='" + i_print_file + "'") 
if i_debug = 1 then  f_pp_msgs( "Modify =   " + rc)

rc = a_dw.describe("DataWindow.Print.Filename" )
if i_debug = 1 then  f_pp_msgs( "datawindow.print.filename=" + rc)

if i_debug = 1 then  f_pp_msgs( "Set Fonts  " )
i_object_loc = i_object_loc_null
uf_set_fonts(a_dw)

i_orig = long(a_dw.describe("DataWindow.print.Orientation"))	
if i_orig = 0 then
	//i_orig = landscape
	//str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	if i_debug = 1 then  f_pp_msgs( "Get Printer Orientation  " )
	status = GetPrinterOrientation()
	if status = -1 then
		i_orig = landscape
		str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	elseif status = 1 then
		i_orig = portrait
	else
		i_orig = landscape
	end if
elseif i_orig = 2 then
	i_orig = portrait
end if
//status = GetPrinterOrientation()
//if status = -1 then
//	i_default_printer_orig = landscape
//elseif status = 1 then
//	i_default_printer_orig = portrait
//else
//   i_default_printer_orig = landscape
//end if
if i_debug = 1 then  f_pp_msgs( "Print File  " )
status = a_dw.print(false)

if i_debug = 1 then  f_pp_msgs( "Finished Printing File Status = "+ string(status) )
longlong job

//job = printopen("Report")
//PrintDataWindow(job,a_dw)
//PrintClose(job)
uf_reset_fonts(a_dw)


// set printer back to the default
if ptr2 <> '' and len(trim(ptr1)) > 0 then
	if i_debug = 1 then  f_pp_msgs( "Set Printing "  + ptr1 )
	SetProfileString ( "win.ini","windows","device", ptr1 )
end if
if i_debug = 1 then  f_pp_msgs( "DataWindow.Print.Preview  " )
a_dw.Modify("DataWindow.Print.Preview=yes")

a_dw.Modify("datawindow.print.filename=''") 

setpointer(hourglass!)
return ""
end function

public function string uf_print_file_ps (datastore a_dw, string a_filename);string temp_dir,str
string temp_file
longlong status,ret
long orig,postscript
string printer,drv,port
string ptr1,ptr2
string drv_name,name
s_object_loc i_object_loc_null[]
ptr2 = ''


// Check for a postscipt printer
ptr1 = ProfileString("win.ini","windows","device","")
if i_debug = 1 then  f_pp_msgs( "Printer " + ptr1)

// Does the user have s pre-selected printer from the ini file
if uf_selected_printer(POST_TYPE,a_dw) = 1 then
	ptr2 = 'change'
	if i_debug = 1 then  f_pp_msgs( "Printer  POST_TYPE" )
end if

// Is the standard PowerPlant printer defined	

if uf_get_printer('ppc_ps',name,drv_name,port,a_dw) = 1 then
	ptr2 = 'change'
	if i_debug = 1 then  f_pp_msgs( "Printer  ppc_ps" )
else
	// Get the default printer and is it a postscript printer
	if i_debug = 1 then  f_pp_msgs( "Get Printer Default " )
	GetPrintDef(orig,postscript)
	if postscript <> 1 then
		printer = space(200)
		drv = space(200)
		port = space(100)
		// Find a postscript printer 
		if i_debug = 1 then  f_pp_msgs( "Get Post Printer Default " )

		if GetPostscriptPrinter(printer,drv,port) = -1 then
			string ptr_name
			 f_pp_msgs( "Can't find a postscript printer " )
			//ret = MessageBox("Information","Can't find a postscript printer, do you want to add a printer",Question!,YesNo! )
			//if ret = 2 then return -1
			drv_name = 'ppc_drv5'
			ptr_name = 'ppc_ps'
			// Add the PowerPlant standard printer to the system
			if uf_add_printer(1,ptr_name,drv_name) = -1 then return "Error adding Printer " + ptr_name 
			ptr1 = ProfileString("win.ini","windows","device","")
			ptr2 = ptr_name + ',' + 'winprint'+ ',' + 'LPT1:'
			 f_pp_msgs( "Add a postscript printer " )
			SetProfileString ( "win.ini","windows","device", ptr2 )
			//return -1
		else
			ptr1 = ProfileString("win.ini","windows","device","")
			ptr2 = printer + ',' +'winprint' + ',' + port
			SetProfileString ( "win.ini","windows","device", ptr2 )
		end if
	end if
end if
   
i_print_file =  a_filename
if i_debug = 1 then  f_pp_msgs( "Printer File " + i_print_file )

string rc
rc = a_dw.modify("DataWindow.Print.Filename='" + i_print_file + "'") 
if i_debug = 1 then  f_pp_msgs( "Modify =   " + rc)

rc = a_dw.describe("DataWindow.Print.Filename" )
if i_debug = 1 then  f_pp_msgs( "datawindow.print.filename=" + rc)

if i_debug = 1 then  f_pp_msgs( "Set Fonts  " )
i_object_loc = i_object_loc_null
uf_set_fonts(a_dw)

i_orig = long(a_dw.describe("DataWindow.print.Orientation"))	
if i_orig = 0 then
	//i_orig = landscape
	//str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	if i_debug = 1 then  f_pp_msgs( "Get Printer Orientation  " )
	status = GetPrinterOrientation()
	if status = -1 then
		i_orig = landscape
		str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	elseif status = 1 then
		i_orig = portrait
	else
		i_orig = landscape
	end if
elseif i_orig = 2 then
	i_orig = portrait
end if
//status = GetPrinterOrientation()
//if status = -1 then
//	i_default_printer_orig = landscape
//elseif status = 1 then
//	i_default_printer_orig = portrait
//else
//   i_default_printer_orig = landscape
//end if
if i_debug = 1 then  f_pp_msgs( "Print File  " )
status = a_dw.print(false)

if i_debug = 1 then  f_pp_msgs( "Finished Printing File Status = "+ string(status) )
longlong job

//job = printopen("Report")
//PrintDataWindow(job,a_dw)
//PrintClose(job)
uf_reset_fonts(a_dw)


// set printer back to the default
if ptr2 <> '' and len(trim(ptr1)) > 0 then
	if i_debug = 1 then  f_pp_msgs( "Set Printing "  + ptr1 )
	SetProfileString ( "win.ini","windows","device", ptr1 )
end if
if i_debug = 1 then  f_pp_msgs( "DataWindow.Print.Preview  " )
a_dw.Modify("DataWindow.Print.Preview=yes")

a_dw.Modify("datawindow.print.filename=''") 

setpointer(hourglass!)
return ""
end function

public function integer uf_get_printer (string a_printer, ref string a_name, ref string a_device, ref string a_port, datastore a_ds);longlong hmod
string funct,ptr

a_name = space(200)
a_device = space(200)
a_port = space(200)
///num = 

hmod =  GetModuleHandleA('psconvrt.dll')
funct = 'GetPrinterInfo'
if GetProcAddress(hmod,funct) = 0 then
	return -1
end if

return uf_set_default_printer(a_printer,a_ds) 
end function

public function integer uf_set_default_printer (string a_printer, datastore a_ds);string ptr
longlong status = 1
longlong hmod
string funct
environment env

getenvironment(env)
if env.PBMajorRevision	 > 8 then
	a_ds.Object.DataWindow.Printer = a_printer
end if

return status
end function

public function integer uf_selected_printer (longlong a_printer_type, datastore a_ds);string name,printer,drv,port
string ptr2,funct
string ini_file
longlong hmod
longlong num,rnum,status
s_printer_info2 printer_info2
//ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")


ini_file = f_get_ini_file() 


if a_printer_type = POST_TYPE then
	printer = ProfileString(ini_file, "Application", "Postscript_Printer", "")
else
	printer = ProfileString(ini_file, "Application", "HP_Printer", "")
end if
if printer = "" then
	return -1
end if
return uf_get_printer(printer,name,drv,port,a_ds)

end function

public function string uf_print_file (datastore a_dw);string temp_dir,str
string temp_file
longlong status,ret
long orig,postscript
string printer,drv,port
string ptr1,ptr2
string drv_name,name
boolean crosstab_report = false
string ini_file
s_object_loc i_object_loc_null[]
ptr2 = ''


if long(a_dw.Object.DataWindow.Processing) = 4 then
	  crosstab_report = true
end if

ptr1 = uf_get_default_printer()

ini_file = f_get_ini_file()
ptr2 = ProfileString(ini_file,"Application","Postscript_Printer","") 
if upper(ptr2) <> "ANY" and   upper(ptr2) <> "" then
	 uf_set_default_printer(ptr2,a_dw) 
else
	// Check for a postscipt printer
	// Does the user have s pre-selected printer from the ini file
	if uf_selected_printer(POST_TYPE,a_dw) = 1 then
		ptr2 = 'change'
	// Is the standard PowerPlant printer defined	
	elseif uf_get_printer('ppc_ps',name,drv_name,port,a_dw) = 1 then
		ptr2 = 'change'
		uf_set_default_printer('ppc_ps',a_dw) 
	else
		// Get the default printer and is it a postscript printer
		GetPrintDef(orig,postscript)
		if postscript <> 1 then
				 ptr2 = 'ppc_ps'
				 uf_set_default_printer('ppc_ps',a_dw) 
		end if
	end if
end if

temp_dir = g_ps_interface.uf_get_temp_dir()
if temp_dir = '' then 
			 f_pp_msgs( "Can't find the temp directory.")
      return  "Can't find the temp directory."
end if
temp_file = temp_dir + "\pstemp.ps"
a_dw.object.datawindow.print.filename = temp_file


  
i_object_loc = i_object_loc_null
if crosstab_report = false then
   uf_set_fonts(a_dw)
end if


i_orig = long(a_dw.Object.DataWindow.print.Orientation)	
if i_orig = 0 then
	status = GetPrinterOrientation()
	if status = -1 then
		i_orig = landscape
		str = a_dw.modify("DataWindow.print.Orientation = '" + string(landscape) + "'")
	elseif status = 1 then
		i_orig = portrait
	else
		i_orig = landscape
	end if
elseif i_orig = 2 then
	i_orig = portrait
end if

a_dw.print()

if crosstab_report = false then
    uf_reset_fonts(a_dw)
end if
// set printer back to the default
if ptr2 <> '' and len(trim(ptr1)) > 0 then
	uf_set_default_printer(ptr1,a_dw)
end if


a_dw.object.datawindow.print.filename = ''
i_initialize_print = true
i_print_file = temp_file

return ''
end function

public function string uf_get_default_printer ();string ptr
longlong status
longlong hmod
string funct
long buflen = 200

hmod =  GetModuleHandleA('psconvrt.dll')
funct = 'GetDefPrinter'
if GetProcAddress(hmod,funct) = 0 then
	ptr = ProfileString("win.ini","windows","device","")
else
	ptr = space(buflen)
   status = GetDefPrinter(ptr,buflen);
end if


return ptr
end function

on uo_ps_web.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ps_web.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string err_msg
if uf_load_library("psconvrt.dll",err_msg) = 1 then
	i_initialize_ps = true
else
	f_pp_msgs( err_msg)
end if

if uf_load_library("pclcnvrt.dll",err_msg) = 1 then
	i_initialize_pcl = true
else
	f_pp_msgs( err_msg)
end if
//if uf_load_library("zlib.dll",err_msg) = 1 then
//	i_initialize_zlib = true
//end if
if i_initialize_pcl = true or i_initialize_ps = true then
	i_initialize_dll = true
end if
end event

event destructor;boolean status 
//status = FileDelete ( i_print_file)
return
end event

