HA$PBExportHeader$uo_datastore_template.sru
forward
global type uo_datastore_template from n_datastore
end type
end forward

global type uo_datastore_template from n_datastore
end type
global uo_datastore_template uo_datastore_template

type variables
string          i_sqlca_sqlerrtext
longlong i_sqlca_sqlcode
string          i_sqlca_sqlsyntax
end variables

on uo_datastore_template.create
call super::create
end on

on uo_datastore_template.destroy
call super::destroy
end on

event dberror;call super::dberror;i_sqlca_sqlcode = sqldbcode
i_sqlca_sqlerrtext = SQLErrText
i_sqlca_sqlsyntax = sqlsyntax
return 1
end event

event error;call super::error;i_sqlca_sqlcode = errornumber
i_sqlca_sqlerrtext = errortext
i_sqlca_sqlsyntax = errorscript
end event

