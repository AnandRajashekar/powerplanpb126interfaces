HA$PBExportHeader$uo_depr_processing.sru
$PBExportComments$48299: theo batching
forward
global type uo_depr_processing from nonvisualobject
end type
end forward

global type uo_depr_processing from nonvisualobject
end type
global uo_depr_processing uo_depr_processing

type variables
uo_ds_top ds_depr_activity, ds_depr_groups, ds_depr_trans_set
boolean		i_reverse_recurring
dec {2}     i_reserve_recurring_amount   
end variables

forward prototypes
public function longlong uf_build_depr_factors ()
public function longlong uf_allo_combined_reserve_theo (datastore a_ds_name)
public function longlong uf_allo_reserve (datastore ds_allo_depr_reserve)
public function longlong uf_allo_reserve_theo (datastore a_ds_name, longlong a_dg_list[])
public function longlong uf_allo_combined_reserve (datastore a_ds_name)
public function longlong uf_build_combined_depr_factors ()
public function longlong uf_depr_ledger_balances ()
public function longlong uf_cpr_depr_balances ()
public function longlong uf_subledger_balances ()
public function integer uf_depr_ledger_blending_balances ()
public function boolean uf_check_blending (longlong a_dg_id, longlong a_sob_id, datetime a_moyr)
public function integer uf_add_missing_cpr_act_depr_groups ()
public function integer uf_cpr_attach_depr_groups (string optional_and_clause)
public function integer uf_assign_depr_group_to_asset (longlong a_co_id, longlong a_bs_id, longlong a_ua_id, string a_cpr_filter)
public function integer uf_build_depr_vintage_summary ()
public function string uf_allo_reserve_co (longlong a_company, datetime a_month)
public function integer uf_allo_reserve_subl (uo_ds_top a_ds)
end prototypes

public function longlong uf_build_depr_factors ();string month_string, sql_mod_string, company_string, original_sql, sqls, template, modret, subledger, replacestring, rwip, rtn_string
longlong factor_count, rc, i, empty_array[], rows, subl, curs, tablepos, ret, null_array[], rtn
decimal{8} factor
datetime moyr, p_gl_post_mo_yr
longlong p_company_id, process_rows, p, p_depr_group_id, p_set_of_books_id, depr_ind, subledger_type_id, dg_list[]

uo_ds_top ds_depr_processing, ds_depr_res_allo_factors, ds_allo_depr_reserve, ds_allo_depr_reserve_theo, ds_allo_subl_reserve

ds_depr_processing = CREATE uo_ds_top

//NEED TO LOOP THROUGH DEPR_PROCESSING_TEMP TABLE  (do not exclude depr groups that are combined depr groups)
ds_depr_processing.DataObject = "dw_temp_dynamic"
	
sqls = "select depr_group.company_id, depr_group.subledger_type_id,  " + &
			" depr_process_temp.depr_group_id, depr_process_temp.set_of_books_id, depr_process_temp.gl_post_mo_yr " + &
			" from depr_process_temp, depr_group  " + &
			" where depr_process_temp.depr_group_id = depr_group.depr_group_id  " + &
			" order by depr_process_temp.depr_group_id, depr_process_temp.set_of_books_id, depr_process_temp.gl_post_mo_yr "
f_create_dynamic_ds(ds_depr_processing,"grid",sqls,sqlca,true)

process_rows = ds_depr_processing.rowcount()

if process_rows > 0 then
	f_status_box("Information", String(Now(),"hh:mm:ss") + ": Starting depr factors rebuild...")
end if

for p = 1 to process_rows
	
	p_depr_group_id = ds_depr_processing.getitemnumber(p, 'depr_group_id')
	p_set_of_books_id = ds_depr_processing.getitemnumber(p, 'set_of_books_id')
	p_gl_post_mo_yr = ds_depr_processing.getitemdatetime(p, 'gl_post_mo_yr')
	p_company_id = ds_depr_processing.getitemnumber(p, 'company_id')
	subledger_type_id = ds_depr_processing.getitemnumber(p, 'subledger_type_id') 
	
	month_string =  String(p_gl_post_mo_yr, "MM/YYYY")  
	
	f_status_box("Information",  '              depr factors (depr_group_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(month_string) + ' sob_id = ' + string(p_set_of_books_id) + ')')
	
	//setup temp tables...
	
	delete from cpr_act_month 
	where session_id = USERENV('SESSIONID') ; 	
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: delete from cpr_act_month failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 

	insert into  CPR_ACT_MONTH(USER_ID, BATCH_REPORT_ID, SESSIOn_id, MONTH)
	VALUES(user, 0, userenv('sessionid'), :p_gl_post_mo_yr);
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: insert into cpr_act_month failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
		
	delete from cpr_company
	where session_id = USERENV('SESSIONID') ;
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: delete from cpr_company failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
	
	insert into  CPR_COMPANY(USER_ID, BATCH_REPORT_ID, session_id, COMPANY_ID )
	VALUES(user, 0, userenv('sessionid'), :p_company_id);
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: inset into  cpr_company failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
	
	//Start depr_res_allo_factors rebuild...
	
	delete from depr_res_allo_factors 
	where month = :p_gl_post_mo_yr
	and depr_group_id = :p_depr_group_id 
	and set_of_books_id = :p_set_of_books_id;
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: delete from depr_res_allo_factors failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
	
     sql_mod_string = " and depr_ledger.depr_group_id = "  + string(p_depr_group_id) + " and depr_ledger.set_of_books_id = " + string(p_set_of_books_id)  //joined to cpr_act_month for month filter 
	
	SetNull(rwip)
	rwip = Upper(f_pp_system_control_company('Include RWIP in Allocation Factors', p_company_id))	
	if IsNull(rwip) then rwip = 'NO'		

	ds_depr_res_allo_factors = CREATE uo_ds_top
	ds_depr_res_allo_factors.dataobject = 'dw_depr_res_allo_factors'	
	ds_depr_res_allo_factors.settransobject(sqlca)
	ds_depr_res_allo_factors.reset()
	
	select depreciation_indicator into :depr_ind
	from subledger_control
	where subledger_type_id = :subledger_type_id ;
	
	if IsNull(depr_ind) then depr_ind = 0
	
	choose case depr_ind 
		case 1
			// need code for subledger factors
			 f_status_box('Information', 'Error: unable to process subledger depr groups: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
			rollback;
			DESTROY ds_depr_processing
			return -1
		case 3
			//cpr depr factors
			insert into depr_res_allo_factors 
			(set_of_books_id, depr_group_id, vintage, month, factor, theo_factor, remaining_life,
			 life_factor, cor_factor)
			(select a.set_of_books_id, b.depr_group_id, 
			to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :p_gl_post_mo_yr, 
			decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 
			decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)),
			avg(remaining_life / decode(init_life, 0, 1, init_life)),
			decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 0
			from cpr_depr a, cpr_ledger b	
			where a.asset_id = b.asset_id
			and a.gl_posting_mo_yr = :p_gl_post_mo_yr 
			and a.depr_group_id = :p_depr_group_id 
			and a.set_of_books_id = :p_set_of_books_id 
			group by a.set_of_books_id, b.depr_group_id, to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :p_gl_post_mo_yr);
	
			if sqlca.sqlcode = -1 then
				 f_status_box('Information', 'Error: cpr_depr insert into depr_res_allo_factors failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
									' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
				rollback;
				DESTROY ds_depr_processing
				return -1
			end if
			
		case else
		
			rc = 7 // do all allocations (4 + 2 + 1)
			if Upper(f_pp_system_control_company("Custom Reserve Allocation", p_company_id)) = "YES" then
				//rc = f_allo_reserve_custom(dw_allo_depr_reserve, i_company, i_date1)
				//CANNOT DO THIS  YET
				f_status_box("Information", String(Now()) + ":Cannot do custom reserve allocations during posting.  Please rebuild factors manually.")
				DESTROY ds_depr_processing
				rollback;
				return -1
			end if
				
			if rc >= 4 then
				// group depr so use theo and dollar-age
			
				ds_allo_depr_reserve_theo = CREATE uo_ds_top
				if rwip = 'YES' then
					ds_allo_depr_reserve_theo.DataObject = "dw_allo_depr_resrwip_theo_list"
				else
					ds_allo_depr_reserve_theo.DataObject = "dw_allo_depr_reserve_theo_list"
				end if
				ds_allo_depr_reserve_theo.SetTransObject(sqlca)
				
				// Theoretical factors
//				f_status_box("Information", String(Now()) + ":Calculating allo factors - Step 1...")
				
				dg_list[1] = p_depr_group_id
				
				rtn = uf_allo_reserve_theo(ds_allo_depr_reserve_theo, dg_list[]) 
				
				if rtn < 0 then
					f_status_box("Information", String(Now()) + ":Error in uf_allo_reserve_theo.  Please rebuild factors manually.")
					rollback;
					DESTROY ds_allo_depr_reserve_theo
					DESTROY ds_depr_processing
					return -1
				end if 
				
				rows = ds_allo_depr_reserve_theo.RowCount()
				for i = 1 to rows
					
					if ds_allo_depr_reserve_theo.getitemnumber(i,'set_of_books_id') = p_set_of_books_id then
						ds_depr_res_allo_factors.insertrow(1)
						
						ds_depr_res_allo_factors.setitem(1,'set_of_books_id', &
							  ds_allo_depr_reserve_theo.getitemnumber(i,'set_of_books_id'))
				
						ds_depr_res_allo_factors.setitem(1,'depr_group_id', &
							  ds_allo_depr_reserve_theo.getitemnumber(i,'depr_group_id'))
				
						 ds_depr_res_allo_factors.setitem(1,'vintage', &
							  ds_allo_depr_reserve_theo.getitemnumber(i,'vintage'))
				
						ds_depr_res_allo_factors.setitem(1,'month', &
							  ds_allo_depr_reserve_theo.getitemdatetime(i,'curr_month'))
				
						 factor = ds_allo_depr_reserve_theo.getitemnumber(i,'factor')
						 if isnull(factor) then factor = 0
						 ds_depr_res_allo_factors.setitem(1,'factor', factor)
				
						 ds_depr_res_allo_factors.setitem(1,'theo_factor',&
							 ds_allo_depr_reserve_theo.getitemnumber(i,'theo_factor'))
				
						ds_depr_res_allo_factors.setitem(1,'remaining_life',&
							 ds_allo_depr_reserve_theo.getitemnumber(i,'current_rem_life'))
				
						ds_depr_res_allo_factors.setitem(1,'life_factor',&
							 ds_allo_depr_reserve_theo.getitemnumber(i,'life_factor'))
				
						 ds_depr_res_allo_factors.setitem(1,'cor_factor',&
							 ds_allo_depr_reserve_theo.getitemnumber(i,'cor_factor'))

					end if //if ds_allo_depr_reserve_theo.getitemnumber(i,'set_of_books_id') = p_set_of_books_id then
				next
					
				rc -= 4
			end if
		
			if rc >= 2 then
				// Dollar Age factors
//				f_status_box("Information", String(Now(),"hh:mm:ss") + ": Calculating allo factors - Step 2...") 				
				
				ds_allo_depr_reserve = CREATE uo_ds_top
				if rwip = 'YES' then
					ds_allo_depr_reserve.DataObject = "dw_allo_depr_resrwip"
				else
					ds_allo_depr_reserve.DataObject = "dw_allo_depr_reserve"
				end if
				ds_allo_depr_reserve.Settransobject(sqlca) 
				
				rtn_string = f_add_where_clause_ds(ds_allo_depr_reserve,"", sql_mod_string, false)
				
				if rtn_string = '' then  
					f_status_box("Information", "ERROR: f_add_where_clause_ds failed(1): "   + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
					rollback;
					DESTROY ds_allo_depr_reserve_theo
					DESTROY ds_allo_depr_reserve
					DESTROY ds_depr_processing
					return -1	  
				end if
				
				rtn =  uf_allo_reserve(ds_allo_depr_reserve) 
				
				if rtn < 0 then
					f_status_box("Information", String(Now()) + ":Error in uf_allo_reserve.  Please rebuild factors manually.")
					rollback;
					DESTROY ds_allo_depr_reserve_theo
					DESTROY ds_allo_depr_reserve
					DESTROY ds_depr_processing
					return -1
				end if 

				rows = ds_allo_depr_reserve.rowcount()
				for i = 1 to rows 
					 
					 ds_depr_res_allo_factors.insertrow(1)
				
					 ds_depr_res_allo_factors.setitem(1,'set_of_books_id', &
							  ds_allo_depr_reserve.getitemnumber(i,'set_of_books_id'))
				
					 ds_depr_res_allo_factors.setitem(1,'depr_group_id', &
							  ds_allo_depr_reserve.getitemnumber(i,'depr_group_id'))
				
					 ds_depr_res_allo_factors.setitem(1,'vintage', &
							  ds_allo_depr_reserve.getitemnumber(i,'vintage'))
				
					 ds_depr_res_allo_factors.setitem(1,'month', &
							  ds_allo_depr_reserve.getitemdatetime(i,'curr_month'))
				
					 factor = ds_allo_depr_reserve.getitemnumber(i,'factor')
					 if isnull(factor) then 
						factor = 0
					end if
					 ds_depr_res_allo_factors.setitem(1,'factor', factor)
					 ds_depr_res_allo_factors.setitem(1,'theo_factor',factor)
							
					 ds_depr_res_allo_factors.setitem(1,'life_factor',&
							 ds_allo_depr_reserve.getitemnumber(i,'life_factor'))
				
					 ds_depr_res_allo_factors.setitem(1,'cor_factor',&
							 ds_allo_depr_reserve.getitemnumber(i,'cor_factor'))
				
				next
			
				rc -= 2
			end if
	
			ret = ds_depr_res_allo_factors.update()
			
			if ret <> 1 then
				f_status_box("Error","Error updating ds_depr_res_allo_factors. SQLInfo: " + ds_depr_res_allo_factors.i_sqlca_sqlerrtext)
				rollback;
				DESTROY ds_allo_depr_reserve_theo
				DESTROY ds_allo_depr_reserve
				DESTROY ds_depr_processing
				return -1
			end if  
			
	end choose
	
	DESTROY ds_depr_res_allo_factors  
	DESTROY ds_allo_depr_reserve  
	DESTROY ds_allo_depr_reserve_theo  
next  //for p = 1 to process_rows (depr group, sob, month)

DESTROY ds_depr_processing 

return 1

end function

public function longlong uf_allo_combined_reserve_theo (datastore a_ds_name);decimal {2} total_reserve_to_allo, value, total_dollar_age, age, total_allo_reserve, &
				dollar_age, calc_res, book_amount, total_allo, original_allo, last_reserve, &
				total_depr_base, total_theo
decimal {2} total_cor_to_allo, total_cor_depr_base, total_cor_theo, total_allo_cor, life_res, cor_res
longlong num_groups, depr_groups[], depr_group, rows_left, books, last_row, method, &
      reserve_ratio_id, check_curve, curve, adj_sign
datetime effdate
longlong i, rtn, dg, calc_rows, loops, f
string sqls, trans, descr, where_str
decimal {8} salvage_pct, avg_life, check_life, weight, k_factor, theo_ratio, net_salvage, over_check, cor_pct, ratio
boolean copied_rows

//f_status_box("Information",String(Now(),"hh:mm:ss") + ": Setting up theoretical reserve ratios...") 

where_str = ""

rtn = f_load_reserve_ratios(where_str)

if rtn <> 1 then
	g_msg.messagebox("Error", "f_load_reserve_ratios() failed.")
	rollback;
	return -1 
end if

// retrieve groups for theo allocation
//f_status_box("Information",String(Now(),"hh:mm:ss") + ": Processing theoretical combined reserve allocation...")
rtn = a_ds_name.retrieve()

if rtn < 0 then
	return -1
end if

a_ds_name.GroupCalc()
a_ds_name.GroupCalc()
num_groups = g_ds_func.of_group_breaks_ds(a_ds_name,depr_groups)

uo_ds_top ds_theo_calc
ds_theo_calc = CREATE uo_ds_top
ds_theo_calc.DataObject = "dw_allo_combined_depr_reserve_theo"

for i = 1 to num_groups
	f_setmicrohelp_w_top(String(i) + " of " + String(num_groups))
	if i = num_groups then
		last_row = a_ds_name.rowcount()
	else
		last_row = depr_groups[(i+1)] - 1
	end if
	 
	depr_group = a_ds_name.getitemnumber(depr_groups[i],'combined_depr_group_id')
	books      = a_ds_name.getitemnumber(depr_groups[i],'set_of_books_id')
	 
	copied_rows = false

	  /////////////////////////////
	  /// ALLOCATE LIFE RESERVE ///
	 /////////////////////////////
	 total_reserve_to_allo = a_ds_name.getitemnumber(depr_groups[i],'reserve')
	 total_depr_base = a_ds_name.getitemnumber(depr_groups[i],'total_life_cost')
	 total_theo = a_ds_name.getitemnumber(depr_groups[i],'total_theo')
	 original_allo = total_reserve_to_allo
	 
	 if (total_reserve_to_allo > total_theo and total_depr_base <> total_theo and total_depr_base > total_reserve_to_allo) &
	    or (total_reserve_to_allo < total_theo and total_reserve_to_allo <> 0) then
		// use weighting [R(1-R)] loop until k <= 1
		adj_sign = Sign(total_reserve_to_allo - total_theo)
		ds_theo_calc.Reset()
		a_ds_name.RowsCopy(depr_groups[i], last_row, Primary!, ds_theo_calc, 1, Primary!)
		calc_rows = ds_theo_calc.RowCount()
		ds_theo_calc.GroupCalc()
		ds_theo_calc.GroupCalc()
		copied_rows = true
		k_factor = ds_theo_calc.getitemnumber(1,'k_factor')
		over_check = ds_theo_calc.getitemnumber(1,'over_check')
		net_salvage = ds_theo_calc.getitemnumber(1,'net_salvage_pct')
		loops = 0
//		do until over_check * k_factor <= 1 - net_salvage
		do until 0 < k_factor and k_factor <= 1
			loops ++
			if loops > 100 then 
				f_status_box("Error","looped 100 times for group: " + String(depr_group))
//				exit
				return 0 
			end if			
			for dg = 1 to calc_rows
				theo_ratio = ds_theo_calc.getitemnumber(dg,'curr_month_weight')
				weight = (theo_ratio^2 * (1 - theo_ratio)) * adj_sign
				//###AJS: 14-JAN-2010: fix rounding issue if theo_ratio > 1 
				if sign(weight) <> adj_sign and sign(weight) <> 0 then
					weight = 1.0 - theo_ratio
				end if
				ds_theo_calc.SetItem(dg,'weight', ds_theo_calc.getitemnumber(dg,'weight') + weight)
			next
			ds_theo_calc.GroupCalc()
			k_factor = ds_theo_calc.getitemnumber(1,'k_factor')
			over_check = ds_theo_calc.getitemnumber(1,'over_check')
		loop 
		if loops > 0 then
			for dg = 1 to calc_rows
				weight = ds_theo_calc.GetItemNumber(dg, "weight")
				a_ds_name.SetItem(depr_groups[i] + dg - 1, "weight", weight)
			next
		end if
	 end if	
	 
      ////////////////////////////
	  /// ALLOCATE COR RESERVE ///
	 ////////////////////////////
    total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i],'cor_reserve')
	 total_cor_depr_base = a_ds_name.getitemnumber(depr_groups[i],'total_cor_cost')
	 total_cor_theo = a_ds_name.getitemnumber(depr_groups[i],'total_cor_theo')
	 cor_pct = a_ds_name.GetItemNumber(depr_groups[i],'cost_of_removal_pct')
	 
	if cor_pct = 0 then 
		/// COR was allocated with Life and we need to split life reserve into pieces
		total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i], 'total_cor_reserve')

		if original_allo = 0 then
			ratio = 0
		else
			ratio = total_cor_to_allo / original_allo 
		end if

		for f = depr_groups[i] to last_row
			life_res = a_ds_name.getitemnumber(f, 'allo_reserve')
			
			cor_res = round(life_res * ratio,2)

			a_ds_name.setitem(f, 'allo_error', cor_res * -1)
			
			a_ds_name.setitem(f, 'cor_allo_error', cor_res)
		next
		
		continue // next group
	end if
	 
	 if (total_cor_to_allo > total_cor_theo and total_cor_depr_base <> total_cor_theo and &
	    total_cor_depr_base > total_cor_to_allo) or (total_cor_to_allo < total_cor_theo and total_cor_to_allo <> 0) then
		 
		adj_sign = Sign(total_cor_to_allo - total_cor_theo)
		// use weighting [R(1-R)] loop until k <= 1
		if not copied_rows then
			ds_theo_calc.Reset()
			a_ds_name.RowsCopy(depr_groups[i], last_row, Primary!, ds_theo_calc, 1, Primary!)
			calc_rows = ds_theo_calc.RowCount()
			ds_theo_calc.GroupCalc()
			ds_theo_calc.GroupCalc()
		end if
		k_factor = ds_theo_calc.getitemnumber(1,'cor_k_factor')
		loops = 0
		do until 0 < k_factor and k_factor <= 1
			loops ++
			if loops > 100 then 
				f_status_box("Error","looped 100 times (COR) for group: " + String(depr_group))
//				exit
				return 0 
			end if			
			for dg = 1 to calc_rows
				theo_ratio = ds_theo_calc.getitemnumber(dg,'cor_curr_month_weight')
				weight = (theo_ratio^2 * (1 - theo_ratio)) * adj_sign
				//###AJS: 14-JAN-2010: fix rounding issue if theo_ratio > 1 
				if sign(weight) <> adj_sign and sign(weight) <> 0 then
					weight = 1.0 - theo_ratio
				end if
				ds_theo_calc.SetItem(dg,'cor_weight', ds_theo_calc.getitemnumber(dg,'cor_weight') + weight)
			next
			ds_theo_calc.GroupCalc()
			k_factor = ds_theo_calc.getitemnumber(1,'cor_k_factor')
		loop 
		if loops > 0 then
			for dg = 1 to calc_rows
				weight = ds_theo_calc.GetItemNumber(dg, "cor_weight")
				a_ds_name.SetItem(depr_groups[i] + dg - 1, "cor_weight", weight)
			next
		end if
	 end if	
next

// loop over again to check rounding 
a_ds_name.GroupCalc()
a_ds_name.GroupCalc()
for i = 1 to num_groups
	f_setmicrohelp_w_top(String(i) + " of " + String(num_groups))
	if i = num_groups then
		last_row = a_ds_name.rowcount()
	else
		last_row = depr_groups[(i+1)] - 1
	end if
	 
   total_reserve_to_allo = a_ds_name.getitemnumber(depr_groups[i],'reserve')
	if cor_pct = 0 then 
		total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i], 'total_cor_reserve')
		total_reserve_to_allo -= total_cor_to_allo
	else
    	total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i],'cor_reserve')
 	end if
	 
	total_allo_reserve = a_ds_name.getitemnumber(depr_groups[i],'total_allo_reserve')
	 
	if total_reserve_to_allo <> total_allo_reserve then
		a_ds_name.SetItem(depr_groups[i], 'allo_error', total_reserve_to_allo - total_allo_reserve + a_ds_name.GetItemNumber(depr_groups[i], 'allo_error') )
	end if	
	 
	cor_pct = a_ds_name.GetItemNumber(depr_groups[i],'cost_of_removal_pct')
	 
	total_allo_cor = a_ds_name.getitemnumber(depr_groups[i],'total_allo_cor')
	 
	if total_cor_to_allo <> total_allo_cor then
		a_ds_name.SetItem(depr_groups[i], 'cor_allo_error', total_cor_to_allo - total_allo_cor + a_ds_name.GetItemNumber(depr_groups[i], 'cor_allo_error') )
	end if	
next
a_ds_name.GroupCalc()
a_ds_name.GroupCalc()

//f_status_box("Information",String(Now(),"hh:mm:ss") + ": Finished theoretical reserve allocation.")
DESTROY ds_theo_calc
return 1
end function

public function longlong uf_allo_reserve (datastore ds_allo_depr_reserve);decimal {2} total_reserve_to_allo, value, total_dollar_age, age, row_age, accum_cost, &
				dollar_age, calc_res, book_amount, total_allo, original_allo, over_reserve_to_allo
decimal {2} total_cor_res_to_allo, calc_cor, total_cor, original_cor, life_res, cor_res
longlong num_groups, depr_groups[], depr_group, rows_left, books, last_row
longlong j, k, i, rtn, f
string sqls, trans, descr
decimal {8} salvage_pct, factor, cor_pct, ratio
boolean over_allo, special_allo

ds_allo_depr_reserve.settransobject(sqlca)
rtn = ds_allo_depr_reserve.retrieve()

if rtn < 0 then
	return -1
end if

num_groups = g_ds_func.of_group_breaks_ds(ds_allo_depr_reserve,depr_groups) 

for i = 1 to num_groups
//	f_status_box("status","Depr Group " + String(i) + " of " + String(num_groups))
	
	if i = num_groups then
		
		last_row = ds_allo_depr_reserve.rowcount()
	else
		
		last_row = depr_groups[(i+1)] - 1
	end if
	 
   depr_group = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'depr_group_id')

   books      = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'set_of_books_id') 

   total_reserve_to_allo = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'reserve')
   total_cor_res_to_allo = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'cor_reserve')

	original_allo = total_reserve_to_allo
	original_cor = total_cor_res_to_allo

	total_dollar_age = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'total_dollar_age')
	 
	total_allo = 0
	over_allo = FALSE
	special_allo = FALSE

   book_amount = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'total_book_cost')

   salvage_pct = ds_allo_depr_reserve.getitemdecimal(depr_groups[i],'net_salvage_pct')
	cor_pct = ds_allo_depr_reserve.getitemdecimal(depr_groups[i],'cost_of_removal_pct')

//	 k = 1

  //CBS 2014/11/13 maint-40961 - the f_status_box portion is already commented out, which essentially means that these IF statements do nothing. 
  //			Comment out all together. This object is now used in the depr_approval interface, which doesn't have a w_status_box. 
//	if total_reserve_to_allo < 0 and w_status_box.enabled = true then
//		
//		descr = ds_allo_depr_reserve.getitemstring(depr_groups[i],'description')
//		
////		f_status_box("Information","Depreciation Group : " + descr &
////						+ " has a negative reserve.")
//	end if
//	if total_cor_res_to_allo < 0 and w_status_box.enabled = true then
//		
//		descr = ds_allo_depr_reserve.getitemstring(depr_groups[i],'description')
//		
////		f_status_box("Information","Depreciation Group : " + descr &
////						+ " has a negative COR reserve.")
//	end if

	// CHECK FOR OVER-RESERVED FOR LIFE 
	if (total_reserve_to_allo > (1 - salvage_pct) * book_amount and &
			sign(book_amount) = 1 and &
			sign(total_reserve_to_allo) = 1 ) &
		or &
		(total_reserve_to_allo < (1 - salvage_pct) * book_amount and &
			sign(book_amount) = -1 and &
			sign(total_reserve_to_allo) = -1 ) 		then
	
		salvage_pct = 0
		
		ds_allo_depr_reserve.setitem(depr_groups[i],'net_salvage_pct',0)
		
		// see if still over depreciated w/o net salvage
		if (total_reserve_to_allo > book_amount and &
				sign(book_amount) = 1 and &
				sign(total_reserve_to_allo) = 1 ) &
			or &
			(total_reserve_to_allo <  book_amount and &
				sign(book_amount) = -1 and &
				sign(total_reserve_to_allo) = -1 ) 		then
				
			over_allo = TRUE
		end if
		
	end if

	if sign(total_reserve_to_allo) <> sign(book_amount) then special_allo = TRUE

	  ///////////////////////////////
	 /// LIFE RESERVE ALLOCATION ///
	///////////////////////////////
	
	if not (over_allo or special_allo) then
    // GET THE YOUNGEST AGE WHERE THE RESERVE IS OVER ALLOCATED.
   	age = ds_allo_depr_reserve.getitemdecimal(depr_groups[i],'total_age')

		for f = depr_groups[i] to last_row
			row_age = ds_allo_depr_reserve.getitemdecimal(f,'age')
			
			dollar_age = ds_allo_depr_reserve.getitemnumber(f,'dollar_age')
			
			if row_age >= age then
				// fully reserved
				accum_cost = ds_allo_depr_reserve.getitemdecimal(f,'accum_cost')
				
				calc_res = accum_cost * ( 1 - salvage_pct)
				
				ds_allo_depr_reserve.Setitem(f,'calc_reserve', calc_res)
				  
				total_reserve_to_allo -= calc_res
				total_dollar_age -= dollar_age
				
			else
	// NOW, MUST RE-ALLOCATE THE RESERVE AND STEP THROUGH THE YEARS UNTIL NOT OVER ALLOCATED 
	// ANY MORE.
				total_allo = 0
				
				if f = last_row then
					// no more vintages after this -- initialize calc_reserve to zero
					rtn = ds_allo_depr_reserve.setitem(last_row,'calc_reserve', 0)
				else
						
					if total_dollar_age <> 0 then
						calc_res = round( dollar_age / total_dollar_age * total_reserve_to_allo, 2)
					else
						calc_res = 0
					end if
				
					ds_allo_depr_reserve.Setitem(f,'calc_reserve', calc_res)

				// NOW CHECK TO SEE IF THIS YEAR IS OVER ALLOCATED. IF, SO CORRECT AND GO TO NEXT YEAR.
				// IF NOT, RESERVE ALLOCATION IS COMPLETE
				
					value = ds_allo_depr_reserve.getitemnumber(f,'accum_cost') * (1 - salvage_pct)
				
				//       if ds_allo_depr_reserve.getitemnumber(k,'calc_reserve') &
				//       <= value then goto next_depr_group
				
					if value <> 0 then
						factor = calc_res / value
					else 
						factor = 10
					end if
						
					if -1 <= factor and factor <= 1 then 
						total_allo += calc_res
						
						// allocate remaining vintages now that it is under 1
						for j = f + 1 to last_row
					
							dollar_age = ds_allo_depr_reserve.getitemnumber(j,'dollar_age')
						
							if total_dollar_age <> 0 then
								calc_res = round(dollar_age / total_dollar_age * total_reserve_to_allo, 2)
							else
								calc_res = 0
							end if
						
							ds_allo_depr_reserve.Setitem(j,'calc_reserve', calc_res)
							  
							total_allo += calc_res
						next
				
						goto life_done
					end if // within 1/-1 limit
						 
					// multiply value by sign of reserve and sign of total dollar age
					value = value * Sign(total_reserve_to_allo) * Sign(total_dollar_age)
					 
					ds_allo_depr_reserve.Setitem(f,'calc_reserve', value )
					 
					total_reserve_to_allo -= value
					total_dollar_age -= dollar_age
					
				end if // f=last_row

			end if
		next // f

	else
		// special scenarios -- do simple weighted allocation (all years over depr)
		if over_allo then
			if ds_allo_depr_reserve.rowcount() = 0 then goto life_done
		
			total_dollar_age = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'total_dollar_age')
		
//			rows_left = ds_allo_depr_reserve.rowcount()
			
			// all vintages will be at least factor=1 so only spread over reserve
			over_reserve_to_allo = total_reserve_to_allo - ((1 - salvage_pct) * book_amount)
		 
			total_allo = 0
		
			for j = depr_groups[i] to last_row
	
				dollar_age = ds_allo_depr_reserve.getitemnumber(j,'dollar_age')

				value = ds_allo_depr_reserve.getitemnumber(j,'accum_cost') * (1 - salvage_pct)
							 
				if total_dollar_age <> 0 then
					calc_res = round( value + (dollar_age / total_dollar_age * over_reserve_to_allo) , 2)
				else
					calc_res = 0
				end if
		
				ds_allo_depr_reserve.Setitem(j,'calc_reserve', calc_res)
				total_allo += calc_res
			next
				 
		else //special (screwy) scenarios - straight dollar-age allocation
			
			if ds_allo_depr_reserve.rowcount() = 0 then goto life_done
		
			total_dollar_age = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'total_dollar_age')
		
			total_allo = 0
	
			for j = depr_groups[i] to last_row
	
				dollar_age = ds_allo_depr_reserve.getitemnumber(j,'dollar_age')

				if total_dollar_age <> 0 then
					calc_res = round( dollar_age / total_dollar_age * total_reserve_to_allo, 2)
				else
					calc_res = 0
				end if
		
				ds_allo_depr_reserve.Setitem(j,'calc_reserve', calc_res)
				total_allo += calc_res
			next

		end if // over v. special

	end if // not(over or special)
	life_done:
	// update calc_reserve of the last group with calc_reserve + (original_allo-total_allo)

	if total_reserve_to_allo - total_allo <> 0 then
		
		rtn = ds_allo_depr_reserve.setitem(last_row,'calc_reserve',&
											ds_allo_depr_reserve.getitemdecimal(last_row,'calc_reserve') &
										 + total_reserve_to_allo - total_allo)	

	end if

	  //////////////////////////////
	 /// COR RESERVE ALLOCATION ///
	//////////////////////////////
	total_dollar_age = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'total_dollar_age')
	if cor_pct = 0 then 
		/// COR was allocated with Life and we need to split life reserve into pieces
		total_cor_res_to_allo = ds_allo_depr_reserve.getitemnumber(depr_groups[i], 'total_cor_res')

		if original_allo - total_cor_res_to_allo = 0 or original_allo = 0 then
			if total_cor_res_to_allo = 0 then 
				ratio = 0
			else
				ratio = 1
			end if
		else
			ratio = total_cor_res_to_allo / original_allo
		end if

		for f = depr_groups[i] to last_row
			life_res = ds_allo_depr_reserve.getitemnumber(f, 'calc_reserve')
			
			cor_res = round(life_res * ratio,2)
			life_res -= cor_res
			ds_allo_depr_reserve.setitem(f, 'calc_reserve', life_res)
			
			ds_allo_depr_reserve.setitem(f, 'calc_cor_res', cor_res)
		next
		
		continue // next group
	end if
	 
	over_allo = FALSE
	special_allo = FALSE

	if (total_cor_res_to_allo > cor_pct * book_amount and &
			sign(book_amount) = 1 and &
			sign(total_cor_res_to_allo) = 1 ) &
		or &
		(total_cor_res_to_allo < cor_pct * book_amount and &
			sign(book_amount) = -1 and &
			sign(total_cor_res_to_allo) = -1 ) 		then
	
		over_allo = TRUE
	end if

	if sign(total_cor_res_to_allo) <> sign(book_amount) then special_allo = TRUE

	if not (over_allo or special_allo) then
    // GET THE YOUNGEST AGE WHERE THE RESERVE IS OVER ALLOCATED.
   	age = ds_allo_depr_reserve.getitemdecimal(depr_groups[i],'total_age_cor')

		for f = depr_groups[i] to last_row
			row_age = ds_allo_depr_reserve.getitemdecimal(f,'age')
			
			dollar_age = ds_allo_depr_reserve.getitemnumber(f,'dollar_age')
			
			if row_age >= age then
				// fully reserved
				accum_cost = ds_allo_depr_reserve.getitemdecimal(f,'accum_cost')
				
				calc_cor = accum_cost * cor_pct
				
				ds_allo_depr_reserve.Setitem(f,'calc_cor_res', calc_cor)

				total_cor_res_to_allo -= calc_cor
				total_dollar_age -= dollar_age
			else
	// NOW, MUST RE-ALLOCATE THE RESERVE AND STEP THROUGH THE YEARS UNTIL NOT OVER ALLOCATED 
	// ANY MORE.
				total_cor = 0
				
				if f = last_row then
					// no more vintages after this -- initialize calc_reserve to zero
					rtn = ds_allo_depr_reserve.setitem(last_row,'calc_cor_res', 0)
				else
					if total_dollar_age <> 0 then
						calc_cor = round( dollar_age / total_dollar_age * total_cor_res_to_allo, 2)
					else
						calc_cor = 0
					end if
				
					ds_allo_depr_reserve.Setitem(f,'calc_cor_res', calc_cor)
						  
				// NOW CHECK TO SEE IF THIS YEAR IS OVER ALLOCATED. IF, SO CORRECT AND GO TO NEXT YEAR.
				// IF NOT, RESERVE ALLOCATION IS COMPLETE
				
					value = ds_allo_depr_reserve.getitemnumber(f,'accum_cost') * cor_pct
				
				//       if ds_allo_depr_reserve.getitemnumber(k,'calc_reserve') &
				//       <= value then goto next_depr_group
				
					if value <> 0 then
						factor = calc_cor / value
					else 
						factor = 10
					end if
						
					if -1 <= factor and factor <= 1 then 
						total_cor += calc_cor
						
						// allocate remaining vintages now that it is under 1
						for j = f + 1 to last_row
					
							dollar_age = ds_allo_depr_reserve.getitemnumber(j,'dollar_age')
						
							if total_dollar_age <> 0 then
								calc_cor = round( dollar_age / total_dollar_age * total_cor_res_to_allo, 2)
							else
								calc_cor = 0
							end if
						
//							ds_allo_depr_reserve.Setitem(f,'calc_cor_res', calc_cor) //$$$sjh
							ds_allo_depr_reserve.Setitem(j,'calc_cor_res', calc_cor)
							  
							total_cor += calc_cor
						next
				
						goto cor_done
					end if // within 1/-1 limit
						 
					// multiply value by sign of reserve and sign of total dollar age
					value = value * Sign(total_cor_res_to_allo) * Sign(total_dollar_age)
					 
					ds_allo_depr_reserve.Setitem(f,'calc_cor_res', value )
					 
					total_cor_res_to_allo -= value
					total_dollar_age -= dollar_age
				end if // f=last_row

			end if
		next // f

	else
		// special scenarios -- do simple weighted allocation (all years over depr)
		if over_allo then
			if ds_allo_depr_reserve.rowcount() = 0 then goto cor_done
		
			total_dollar_age = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'total_dollar_age')
		
//			rows_left = ds_allo_depr_reserve.rowcount()
			
			// all vintages will be at least factor=1 so only spread over reserve
			over_reserve_to_allo = total_cor_res_to_allo - (cor_pct * book_amount)
		 
			total_cor = 0
		
			for j = depr_groups[i] to last_row
	
				dollar_age = ds_allo_depr_reserve.getitemnumber(j,'dollar_age')

				value = ds_allo_depr_reserve.getitemnumber(j,'accum_cost') * cor_pct
							 
				if total_dollar_age <> 0 then
					calc_cor = round( value + (dollar_age / total_dollar_age * over_reserve_to_allo) , 2)
				else
					calc_cor = 0
				end if
		
				ds_allo_depr_reserve.Setitem(j,'calc_cor_res', calc_cor)
				  
				total_cor += calc_cor
			next
				 
		else //special (screwy) scenarios - straight dollar-age allocation
			
			if ds_allo_depr_reserve.rowcount() = 0 then goto cor_done
		
			total_dollar_age = ds_allo_depr_reserve.getitemnumber(depr_groups[i],'total_dollar_age')
		
			total_cor = 0
	
			for j = depr_groups[i] to last_row
	
				dollar_age = ds_allo_depr_reserve.getitemnumber(j,'dollar_age')

				if total_dollar_age <> 0 then
					calc_cor = round( dollar_age / total_dollar_age * total_cor_res_to_allo, 2)
				else
					calc_cor = 0
				end if
		
				ds_allo_depr_reserve.Setitem(j,'calc_cor_res', calc_cor)
				total_cor += calc_cor
			next

		end if // over v. special

	end if // not(over or special)
	cor_done:


	// update calc_reserve of the last group with calc_reserve + (original_allo-total_allo)
	if total_cor_res_to_allo - total_cor <> 0 then
		
		rtn = ds_allo_depr_reserve.setitem(last_row,'calc_cor_res',&
											ds_allo_depr_reserve.getitemdecimal(last_row,'calc_cor_res') &
										 + total_cor_res_to_allo - total_cor)	

	end if


next

ds_allo_depr_reserve.groupcalc()

return 1
end function

public function longlong uf_allo_reserve_theo (datastore a_ds_name, longlong a_dg_list[]);decimal {2} total_reserve_to_allo, value, total_dollar_age, age, total_allo_reserve, &
				dollar_age, calc_res, book_amount, total_allo, original_allo, last_reserve, &
				total_depr_base, total_theo, theo, full_theo, big_res
decimal {2} total_cor_to_allo, total_cor_depr_base, total_cor_theo, total_allo_cor, life_res, cor_res
longlong num_groups, depr_groups[], depr_group, rows_left, books, last_row, method, &
      reserve_ratio_id, check_curve, curve, adj_sign, allo_total_flag, special_allo_flag
datetime effdate
longlong i, rtn, dg, calc_rows, loops, f, big_vint[]
string sqls, trans, descr, where_str
decimal {8} salvage_pct, avg_life, check_life, weight, k_factor, theo_ratio, net_salvage, over_check, cor_pct
double ratio
boolean copied_rows
longlong max_loops,co_id, last_co_id

//f_status_box("Information",String(Now(),"hh:mm:ss") + ": Setting up theoretical reserve ratios...")

if UpperBound(a_dg_list[]) > 0 then
	where_str = " and " + f_parsearrayby254(a_dg_list[],"N","depr_group.depr_group_id")
else
	where_str = ""
end if

rtn = f_load_reserve_ratios(where_str)

if rtn <> 1 then
	g_msg.messagebox("Error", "f_load_reserve_ratios() failed.")
	return -1 
end if

// retrieve groups for theo allocation
uo_ds_top ds_theo_calc
ds_theo_calc = CREATE uo_ds_top
ds_theo_calc.DataObject = a_ds_name.DataObject

//f_status_box("Information",String(Now(),"hh:mm:ss") + ": Processing theoretical reserve allocation...")
if UpperBound(a_dg_list) > 0 then
	rtn = a_ds_name.retrieve(a_dg_list)
else
	rtn = a_ds_name.retrieve()
end if

if rtn < 0 then
	return -1
end if

a_ds_name.GroupCalc()
a_ds_name.GroupCalc()
num_groups = g_ds_func.of_group_breaks_ds(a_ds_name,depr_groups)


for i = 1 to num_groups
	f_setmicrohelp_w_top(String(i) + " of " + String(num_groups))
	 if i = num_groups then
		 last_row = a_ds_name.rowcount()
	 else
		 last_row = depr_groups[(i+1)] - 1
	 end if
	 
	 depr_group = a_ds_name.getitemnumber(depr_groups[i],'depr_group_id')
	 descr = a_ds_name.GetItemString(depr_groups[i],"description")
    books      = a_ds_name.getitemnumber(depr_groups[i],'set_of_books_id')
	 
	 copied_rows = false


	/// !!AJS: maint 7656: moved this sys control lookup outside of loop for performance (remember Alex E should never code)
	///// ###ARE 1-12-2008 make max loops variable a system control, and replace compare to loops variable in two places below plus error_msg
	co_id = 0
	select company_id into :co_id from depr_group where depr_group_id = :depr_group;
	if last_co_id <> co_id then
		max_loops = long(f_pp_system_control_company('DEPR - Reserve Allo Max Loops', co_id))
		if isnull(max_loops) or max_loops = 0 then
			max_loops = 100
		end if
		last_co_id = co_id
	end if
	
      /////////////////////////////
	  /// ALLOCATE LIFE RESERVE ///
	 /////////////////////////////
    total_reserve_to_allo = a_ds_name.getitemnumber(depr_groups[i],'reserve')
	 total_depr_base = a_ds_name.getitemnumber(depr_groups[i],'total_depr_base')
	 total_theo = a_ds_name.getitemnumber(depr_groups[i],'total_theo')
	 original_allo = total_reserve_to_allo
	 
    total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i],'cor_reserve')
	 total_cor_depr_base = a_ds_name.getitemnumber(depr_groups[i],'total_cor_depr_base')
	 total_cor_theo = a_ds_name.getitemnumber(depr_groups[i],'total_cor_theo')
	 
	 special_allo_flag = a_ds_name.GetItemNumber(depr_groups[i],"special_allo_flag")
	 allo_total_flag = a_ds_name.GetItemNumber(depr_groups[i],"allo_total_flag")
	 
	if allo_total_flag = 1 then
		total_depr_base = total_depr_base + total_cor_depr_base
		total_theo = total_theo + total_cor_theo
	end if
	
	if special_allo_flag = 1 then
		f_status_box("Error","Group: " + descr + " Using Dollar-Age Allocation due to sign mismatch (id=" + String(depr_group) &
					+ "; book=" + String(books) + ") Actual Reserve = " + String(total_reserve_to_allo,"#,##0.00")&
					+ " v. Theo Reserve = " + String(total_theo,"#,##0.00") )
	end if
	 
	 if special_allo_flag = 0 and &
	    ((total_reserve_to_allo > total_theo and total_depr_base <> total_theo and total_depr_base >= total_reserve_to_allo) &
	    or (total_reserve_to_allo < total_theo and total_reserve_to_allo <> 0)) then
		// use weighting [R(1-R)] loop until k <= 1
		adj_sign = Sign(total_reserve_to_allo - total_theo)
		ds_theo_calc.Reset()
		a_ds_name.RowsCopy(depr_groups[i], last_row, Primary!, ds_theo_calc, 1, Primary!)
		calc_rows = ds_theo_calc.RowCount()
		ds_theo_calc.GroupCalc()
		ds_theo_calc.GroupCalc()
		copied_rows = true
		k_factor = ds_theo_calc.getitemnumber(1,'k_factor')
		over_check = ds_theo_calc.getitemnumber(1,'over_check')
//		net_salvage = ds_theo_calc.getitemnumber(1,'net_salvage_pct')
		loops = 0
//		do until over_check * k_factor <= 1 - net_salvage
	  do until 0 < k_factor and k_factor <= 1
         loops ++
         if loops > max_loops then 
            f_status_box("Error","Group: " + descr + " looped " + string(max_loops) + " times (id=" + String(depr_group)+")")
            exit
         end if      
         theo = ds_theo_calc.getitemnumber(1,'total_weighted_theo')
         adj_sign = Sign(total_reserve_to_allo - theo)
         full_theo = ds_theo_calc.getitemnumber(1,'full_theo')
         for dg = 1 to calc_rows
            theo_ratio = ds_theo_calc.getitemnumber(dg,'curr_month_weight')
            if total_reserve_to_allo < full_theo and theo_ratio = 1 then theo_ratio = 0.99
            weight = (theo_ratio^2 * (1 - theo_ratio)) * adj_sign
            ds_theo_calc.SetItem(dg,'weight', ds_theo_calc.getitemnumber(dg,'weight') + weight)
         next
         ds_theo_calc.GroupCalc()
         k_factor = ds_theo_calc.getitemnumber(1,'k_factor')
         over_check = ds_theo_calc.getitemnumber(1,'over_check')
      loop 

		if loops > 0 and loops <= max_loops then
			for dg = 1 to calc_rows
				weight = ds_theo_calc.GetItemNumber(dg, "weight")
				a_ds_name.SetItem(depr_groups[i] + dg - 1, "weight", weight)
			next
		end if
	 end if	
	 
a_ds_name.GroupCalc()
a_ds_name.GroupCalc()

      ////////////////////////////
	  /// ALLOCATE COR RESERVE ///
	 ////////////////////////////
    total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i],'cor_reserve')
	 total_cor_depr_base = a_ds_name.getitemnumber(depr_groups[i],'total_cor_depr_base')
	 total_cor_theo = a_ds_name.getitemnumber(depr_groups[i],'total_cor_theo')
	 cor_pct = a_ds_name.GetItemNumber(depr_groups[i],'cost_of_removal_pct')
	 
	big_res = 0
	big_vint[i] = depr_groups[i]

	if allo_total_flag = 1 then 
		/// COR was allocated with Life and we need to split life reserve into pieces
		total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i], 'total_cor_reserve')

		if original_allo - total_cor_to_allo = 0 or original_allo = 0  then
			if total_cor_to_allo = 0 then 
				ratio = 0
			else
				ratio = 1
			end if
		else
			ratio = total_cor_to_allo / original_allo				
		end if
		
		for f = depr_groups[i] to last_row
			life_res = a_ds_name.getitemnumber(f, 'allo_reserve')
			
			if abs(life_res) > abs(big_res) then 
				big_res = life_res 
				big_vint[i] = f
			end if
			
			cor_res = round(life_res * ratio,2)

			a_ds_name.setitem(f, 'allo_error', cor_res * -1)
			
			a_ds_name.setitem(f, 'cor_allo_error', cor_res)
		next
		
		continue // next group
	else
		////just find "big" vintage
		for f = depr_groups[i] to last_row
			life_res = a_ds_name.getitemnumber(f, 'allo_reserve')
			
			if abs(life_res) > abs(big_res) then 
				big_res = life_res 
				big_vint[i] = f
			end if
		next		
	end if
	 
	 if (total_cor_to_allo > total_cor_theo and total_cor_depr_base <> total_cor_theo and &
	    total_cor_depr_base >= total_cor_to_allo) or (total_cor_to_allo < total_cor_theo and total_cor_to_allo <> 0) then
		 
		adj_sign = Sign(total_cor_to_allo - total_cor_theo)
		// use weighting [R(1-R)] loop until k <= 1
		if not copied_rows then
			ds_theo_calc.Reset()
			a_ds_name.RowsCopy(depr_groups[i], last_row, Primary!, ds_theo_calc, 1, Primary!)
			calc_rows = ds_theo_calc.RowCount()
			ds_theo_calc.GroupCalc()
			ds_theo_calc.GroupCalc()
		end if
		k_factor = ds_theo_calc.getitemnumber(1,'cor_k_factor')
		loops = 0
		do until 0 < k_factor and k_factor <= 1
			loops ++
			if loops > max_loops then 
				f_status_box("Error","Group: " + descr + " looped " + string(max_loops) + " times (COR) (id=" + String(depr_group)+"; book=" + String(books) + ")")
				exit 
			end if			
			theo = ds_theo_calc.getitemnumber(1,'total_cor_weighted_theo')
			adj_sign = Sign(total_cor_to_allo - theo)
			full_theo = ds_theo_calc.getitemnumber(1,'total_cor_full')
			for dg = 1 to calc_rows
				theo_ratio = ds_theo_calc.getitemnumber(dg,'cor_curr_month_weight')
				if total_cor_to_allo < full_theo and theo_ratio = 1 then theo_ratio = 0.99
				weight = (theo_ratio^2 * (1 - theo_ratio)) * adj_sign
				ds_theo_calc.SetItem(dg,'cor_weight', ds_theo_calc.getitemnumber(dg,'cor_weight') + weight)
			next
			ds_theo_calc.GroupCalc()
			k_factor = ds_theo_calc.getitemnumber(1,'cor_k_factor')
		loop 
		if loops > 0 and loops <= max_loops then
			for dg = 1 to calc_rows
				weight = ds_theo_calc.GetItemNumber(dg, "cor_weight")
				a_ds_name.SetItem(depr_groups[i] + dg - 1, "cor_weight", weight)
			next
		end if
	 end if	
next

// loop over again to check rounding 
a_ds_name.GroupCalc()
a_ds_name.GroupCalc()
for i = 1 to num_groups
	f_setmicrohelp_w_top(String(i) + " of " + String(num_groups))
	if i = num_groups then
		last_row = a_ds_name.rowcount()
	else
		last_row = depr_groups[(i+1)] - 1
	end if
	 
	cor_pct = a_ds_name.GetItemNumber(depr_groups[i],'cost_of_removal_pct')
	 allo_total_flag = a_ds_name.GetItemNumber(depr_groups[i],"allo_total_flag")
	 
   total_reserve_to_allo = a_ds_name.getitemnumber(depr_groups[i],'reserve')
	
	if allo_total_flag = 1 then 
		total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i], 'total_cor_reserve')
		total_reserve_to_allo -= total_cor_to_allo
	else
    	total_cor_to_allo = a_ds_name.getitemnumber(depr_groups[i],'cor_reserve')
 	end if
	 
	total_allo_reserve = a_ds_name.getitemnumber(depr_groups[i],'total_allo_reserve')
	 
	if total_reserve_to_allo <> total_allo_reserve then
		a_ds_name.SetItem(big_vint[i], 'allo_error', total_reserve_to_allo - total_allo_reserve + a_ds_name.GetItemNumber(big_vint[i], 'allo_error') )
	end if	
	 
	total_allo_cor = a_ds_name.getitemnumber(depr_groups[i],'total_allo_cor')
	 
	if total_cor_to_allo <> total_allo_cor then
		a_ds_name.SetItem(big_vint[i], 'cor_allo_error', total_cor_to_allo - total_allo_cor + a_ds_name.GetItemNumber(big_vint[i], 'cor_allo_error') )
	end if	
next
a_ds_name.GroupCalc()
a_ds_name.GroupCalc()

//f_status_box("Information",String(Now(),"hh:mm:ss") + ": Finished theoretical reserve allocation.")
DESTROY ds_theo_calc
return 1
end function

public function longlong uf_allo_combined_reserve (datastore a_ds_name);decimal {2} total_reserve_to_allo, value, total_dollar_age, age, row_age, accum_cost, &
				dollar_age, calc_res, book_amount, total_allo, original_allo, over_reserve_to_allo
decimal {2} total_cor_res_to_allo, calc_cor, total_cor, original_cor, life_res, cor_res, life_cost, cor_cost
longlong num_groups, depr_groups[], depr_group, rows_left, books, last_row
longlong j, k, i, rtn, f
string sqls, trans, descr
decimal {8} salvage_pct, factor, cor_pct, ratio
boolean over_allo, special_allo


rtn = a_ds_name.retrieve()
if rtn < 0 then
	return -1
end if 

num_groups = g_ds_func.of_group_breaks_ds(a_ds_name,depr_groups)
 
for i = 1 to num_groups 
	
	if i = num_groups then
		
		last_row = a_ds_name.rowcount()
	else
		
		last_row = depr_groups[(i+1)] - 1
	end if
	 
   depr_group = a_ds_name.getitemnumber(depr_groups[i],'combined_depr_group_id')

   books      = a_ds_name.getitemnumber(depr_groups[i],'set_of_books_id')

   total_reserve_to_allo = a_ds_name.getitemnumber(depr_groups[i],'reserve')
   total_cor_res_to_allo = a_ds_name.getitemnumber(depr_groups[i],'cor_reserve')

	original_allo = total_reserve_to_allo
	original_cor = total_cor_res_to_allo

	total_dollar_age = a_ds_name.getitemnumber(depr_groups[i],'total_dollar_age')
	 
	total_allo = 0
	over_allo = FALSE
	special_allo = FALSE

   book_amount = a_ds_name.getitemnumber(depr_groups[i],'total_book_cost')

   life_cost =  a_ds_name.getitemdecimal(depr_groups[i],'total_life_cost')
	cor_cost = a_ds_name.getitemdecimal(depr_groups[i],'total_cor_cost')

	if book_amount = 0 then
		salvage_pct = 0
		cor_pct = 0
	else
		salvage_pct = ( book_amount - life_cost) / book_amount
		cor_pct = cor_cost / book_amount
	end if
	

//	 k = 1

	if total_reserve_to_allo < 0 and f_is_status_box_enabled() = true then
		
		descr = a_ds_name.getitemstring(depr_groups[i],'description')
		
		f_status_box("Information","Depreciation Group : " + descr &
						+ " has a negative reserve.")
	end if
	if total_cor_res_to_allo < 0 and f_is_status_box_enabled() = true then
		
		descr = a_ds_name.getitemstring(depr_groups[i],'description')
		
		f_status_box("Information","Depreciation Group : " + descr &
						+ " has a negative COR reserve.")
	end if

	// CHECK FOR OVER-RESERVED FOR LIFE 
	if (total_reserve_to_allo > (1 - salvage_pct) * book_amount and &
			sign(book_amount) = 1 and &
			sign(total_reserve_to_allo) = 1 ) &
		or &
		(total_reserve_to_allo < (1 - salvage_pct) * book_amount and &
			sign(book_amount) = -1 and &
			sign(total_reserve_to_allo) = -1 ) 		then
	
		salvage_pct = 0
		
		// see if still over depreciated w/o net salvage
		if (total_reserve_to_allo > book_amount and &
				sign(book_amount) = 1 and &
				sign(total_reserve_to_allo) = 1 ) &
			or &
			(total_reserve_to_allo <  book_amount and &
				sign(book_amount) = -1 and &
				sign(total_reserve_to_allo) = -1 ) 		then
				
			over_allo = TRUE
		end if
		
	end if

	if sign(total_reserve_to_allo) <> sign(book_amount) then special_allo = TRUE

	  ///////////////////////////////
	 /// LIFE RESERVE ALLOCATION ///
	///////////////////////////////
	
	if not (over_allo or special_allo) then
    // GET THE YOUNGEST AGE WHERE THE RESERVE IS OVER ALLOCATED.
   	age = a_ds_name.getitemdecimal(depr_groups[i],'total_age')

		for f = depr_groups[i] to last_row
			row_age = a_ds_name.getitemdecimal(f,'age')
			
			dollar_age = a_ds_name.getitemnumber(f,'dollar_age')
			
			if row_age >= age then
				// fully reserved
				accum_cost = a_ds_name.getitemdecimal(f,'accum_cost')
				
				calc_res = accum_cost * ( 1 - salvage_pct)
				
				a_ds_name.Setitem(f,'calc_reserve', calc_res)
				  
				total_reserve_to_allo -= calc_res
				total_dollar_age -= dollar_age
				
			else
	// NOW, MUST RE-ALLOCATE THE RESERVE AND STEP THROUGH THE YEARS UNTIL NOT OVER ALLOCATED 
	// ANY MORE.
				total_allo = 0
				
				if f = last_row then
					// no more vintages after this -- initialize calc_reserve to zero
					rtn = a_ds_name.setitem(last_row,'calc_reserve', 0)
				else
						
					if total_dollar_age <> 0 then
						calc_res = round( dollar_age / total_dollar_age * total_reserve_to_allo, 2)
					else
						calc_res = 0
					end if
				
					a_ds_name.Setitem(f,'calc_reserve', calc_res)

				// NOW CHECK TO SEE IF THIS YEAR IS OVER ALLOCATED. IF, SO CORRECT AND GO TO NEXT YEAR.
				// IF NOT, RESERVE ALLOCATION IS COMPLETE
				
					value = a_ds_name.getitemnumber(f,'accum_cost') * (1 - salvage_pct)
				
				//       if a_ds_name.getitemnumber(k,'calc_reserve') &
				//       <= value then goto next_depr_group
				
					if value <> 0 then
						factor = calc_res / value
					else 
						factor = 10
					end if
						
					if -1 <= factor and factor <= 1 then 
						total_allo += calc_res
						
						// allocate remaining vintages now that it is under 1
						for j = f + 1 to last_row
					
							dollar_age = a_ds_name.getitemnumber(j,'dollar_age')
						
							if total_dollar_age <> 0 then
								calc_res = round(dollar_age / total_dollar_age * total_reserve_to_allo, 2)
							else
								calc_res = 0
							end if
						
							a_ds_name.Setitem(j,'calc_reserve', calc_res)
							  
							total_allo += calc_res
						next
				
						goto life_done
					end if // within 1/-1 limit
						 
					// multiply value by sign of reserve and sign of total dollar age
					value = value * Sign(total_reserve_to_allo) * Sign(total_dollar_age)
					 
					a_ds_name.Setitem(f,'calc_reserve', value )
					 
					total_reserve_to_allo -= value
					total_dollar_age -= dollar_age
					
				end if // f=last_row

			end if
		next // f

	else
		// special scenarios -- do simple weighted allocation (all years over depr)
		if over_allo then
			if a_ds_name.rowcount() = 0 then goto life_done
		
			total_dollar_age = a_ds_name.getitemnumber(depr_groups[i],'total_dollar_age')
		
//			rows_left = a_ds_name.rowcount()
			
			// all vintages will be at least factor=1 so only spread over reserve
			over_reserve_to_allo = total_reserve_to_allo - ((1 - salvage_pct) * book_amount)
		 
			total_allo = 0
		
			for j = depr_groups[i] to last_row
	
				dollar_age = a_ds_name.getitemnumber(j,'dollar_age')

				value = a_ds_name.getitemnumber(j,'accum_cost') * (1 - salvage_pct)
							 
				if total_dollar_age <> 0 then
					calc_res = round( value + (dollar_age / total_dollar_age * over_reserve_to_allo) , 2)
				else
					calc_res = 0
				end if
		
				a_ds_name.Setitem(j,'calc_reserve', calc_res)
				total_allo += calc_res
			next
				 
		else //special (screwy) scenarios - straight dollar-age allocation
			
			if a_ds_name.rowcount() = 0 then goto life_done
		
			total_dollar_age = a_ds_name.getitemnumber(depr_groups[i],'total_dollar_age')
		
			total_allo = 0
	
			for j = depr_groups[i] to last_row
	
				dollar_age = a_ds_name.getitemnumber(j,'dollar_age')

				if total_dollar_age <> 0 then
					calc_res = round( dollar_age / total_dollar_age * total_reserve_to_allo, 2)
				else
					calc_res = 0
				end if
		
				a_ds_name.Setitem(j,'calc_reserve', calc_res)
				total_allo += calc_res
			next

		end if // over v. special

	end if // not(over or special)
	life_done:
	// update calc_reserve of the last group with calc_reserve + (original_allo-total_allo)

	if total_reserve_to_allo - total_allo <> 0 then
		
		rtn = a_ds_name.setitem(last_row,'calc_reserve',&
											a_ds_name.getitemdecimal(last_row,'calc_reserve') &
										 + total_reserve_to_allo - total_allo)	

	end if

	  //////////////////////////////
	 /// COR RESERVE ALLOCATION ///
	//////////////////////////////
	total_dollar_age = a_ds_name.getitemnumber(depr_groups[i],'total_dollar_age')
	if cor_pct = 0 then 
		/// COR was allocated with Life and we need to split life reserve into pieces
		total_cor_res_to_allo = a_ds_name.getitemnumber(depr_groups[i], 'total_cor_res')

		if original_allo = 0 then
			ratio = 0
		else
			ratio = total_cor_res_to_allo / original_allo 
		end if

		for f = depr_groups[i] to last_row
			life_res = a_ds_name.getitemnumber(f, 'calc_reserve')
			
			cor_res = round(life_res * ratio,2)
			life_res -= cor_res
			a_ds_name.setitem(f, 'calc_reserve', life_res)
			
			a_ds_name.setitem(f, 'calc_cor_res', cor_res)
		next
		
		continue // next group
	end if
	 
	over_allo = FALSE
	special_allo = FALSE

	if (total_cor_res_to_allo > cor_pct * book_amount and &
			sign(book_amount) = 1 and &
			sign(total_cor_res_to_allo) = 1 ) &
		or &
		(total_cor_res_to_allo < cor_pct * book_amount and &
			sign(book_amount) = -1 and &
			sign(total_cor_res_to_allo) = -1 ) 		then
	
		over_allo = TRUE
	end if

	if sign(total_cor_res_to_allo) <> sign(book_amount) then special_allo = TRUE

	if not (over_allo or special_allo) then
    // GET THE YOUNGEST AGE WHERE THE RESERVE IS OVER ALLOCATED.
   	age = a_ds_name.getitemdecimal(depr_groups[i],'total_age_cor')

		for f = depr_groups[i] to last_row
			row_age = a_ds_name.getitemdecimal(f,'age')
			
			dollar_age = a_ds_name.getitemnumber(f,'dollar_age')
			
			if row_age >= age then
				// fully reserved
				accum_cost = a_ds_name.getitemdecimal(f,'accum_cost')
				
				calc_cor = accum_cost * cor_pct
				
				a_ds_name.Setitem(f,'calc_cor_res', calc_cor)

				total_cor_res_to_allo -= calc_cor
				total_dollar_age -= dollar_age
			else
	// NOW, MUST RE-ALLOCATE THE RESERVE AND STEP THROUGH THE YEARS UNTIL NOT OVER ALLOCATED 
	// ANY MORE.
				total_cor = 0
				
				if f = last_row then
					// no more vintages after this -- initialize calc_reserve to zero
					rtn = a_ds_name.setitem(last_row,'calc_cor_res', 0)
				else
					if total_dollar_age <> 0 then
						calc_cor = round( dollar_age / total_dollar_age * total_cor_res_to_allo, 2)
					else
						calc_cor = 0
					end if
				
					a_ds_name.Setitem(f,'calc_cor_res', calc_cor)
						  
				// NOW CHECK TO SEE IF THIS YEAR IS OVER ALLOCATED. IF, SO CORRECT AND GO TO NEXT YEAR.
				// IF NOT, RESERVE ALLOCATION IS COMPLETE
				
					value = a_ds_name.getitemnumber(f,'accum_cost') * cor_pct
				
				//       if a_ds_name.getitemnumber(k,'calc_reserve') &
				//       <= value then goto next_depr_group
				
					if value <> 0 then
						factor = calc_cor / value
					else 
						factor = 10
					end if
						
					if -1 <= factor and factor <= 1 then 
						total_cor += calc_cor
						
						// allocate remaining vintages now that it is under 1
						for j = f + 1 to last_row
					
							dollar_age = a_ds_name.getitemnumber(j,'dollar_age')
						
							if total_dollar_age <> 0 then
								calc_cor = round( dollar_age / total_dollar_age * total_cor_res_to_allo, 2)
							else
								calc_cor = 0
							end if
						
//							a_ds_name.Setitem(f,'calc_cor_res', calc_cor) //$$$sjh
							a_ds_name.Setitem(j,'calc_cor_res', calc_cor)
							  
							total_cor += calc_cor
						next
				
						goto cor_done
					end if // within 1/-1 limit
						 
					// multiply value by sign of reserve and sign of total dollar age
					value = value * Sign(total_cor_res_to_allo) * Sign(total_dollar_age)
					 
					a_ds_name.Setitem(f,'calc_cor_res', value )
					 
					total_cor_res_to_allo -= value
					total_dollar_age -= dollar_age
				end if // f=last_row

			end if
		next // f

	else
		// special scenarios -- do simple weighted allocation (all years over depr)
		if over_allo then
			if a_ds_name.rowcount() = 0 then goto cor_done
		
			total_dollar_age = a_ds_name.getitemnumber(depr_groups[i],'total_dollar_age')
		
//			rows_left = a_ds_name.rowcount()
			
			// all vintages will be at least factor=1 so only spread over reserve
			over_reserve_to_allo = total_cor_res_to_allo - (cor_pct * book_amount)
		 
			total_cor = 0
		
			for j = depr_groups[i] to last_row
	
				dollar_age = a_ds_name.getitemnumber(j,'dollar_age')

				value = a_ds_name.getitemnumber(j,'accum_cost') * cor_pct
							 
				if total_dollar_age <> 0 then
					calc_cor = round( value + (dollar_age / total_dollar_age * over_reserve_to_allo) , 2)
				else
					calc_cor = 0
				end if
		
				a_ds_name.Setitem(j,'calc_cor_res', calc_cor)
				  
				total_cor += calc_cor
			next
				 
		else //special (screwy) scenarios - straight dollar-age allocation
			
			if a_ds_name.rowcount() = 0 then goto cor_done
		
			total_dollar_age = a_ds_name.getitemnumber(depr_groups[i],'total_dollar_age')
		
			total_cor = 0
	
			for j = depr_groups[i] to last_row
	
				dollar_age = a_ds_name.getitemnumber(j,'dollar_age')

				if total_dollar_age <> 0 then
					calc_cor = round( dollar_age / total_dollar_age * total_cor_res_to_allo, 2)
				else
					calc_cor = 0
				end if
		
				a_ds_name.Setitem(j,'calc_cor_res', calc_cor)
				total_cor += calc_cor
			next

		end if // over v. special

	end if // not(over or special)
	cor_done:


	// update calc_reserve of the last group with calc_reserve + (original_allo-total_allo)
	if total_cor_res_to_allo - total_cor <> 0 then
		
		rtn = a_ds_name.setitem(last_row,'calc_cor_res',&
											a_ds_name.getitemdecimal(last_row,'calc_cor_res') &
										 + total_cor_res_to_allo - total_cor)	

	end if


next

a_ds_name.groupcalc() 

return 1
end function

public function longlong uf_build_combined_depr_factors ();string month_string, sql_mod_string, company_string, original_sql, sqls, template, modret, subledger, replacestring, rwip, rtn_string
longlong factor_count, rc, i, rows, subl, curs, tablepos, ret, p_combined_depr_group_id, rtn
decimal{8} factor
datetime moyr, p_gl_post_mo_yr
longlong p_company_id, process_rows, p, p_set_of_books_id 

uo_ds_top ds_depr_processing, ds_depr_res_allo_factors, ds_combined_depr_res_allo_factors, ds_allo_depr_reserve,ds_allo_depr_reserve_theo

ds_depr_processing = CREATE uo_ds_top

//NEED TO LOOP THROUGH DEPR_PROCESSING_TEMP TABLE  
ds_depr_processing.DataObject = "dw_temp_dynamic"
	
sqls = "select distinct depr_group.company_id, depr_group.combined_depr_group_id, " + &
			" depr_process_temp.set_of_books_id, depr_process_temp.gl_post_mo_yr " + &
			" from depr_process_temp, depr_group  " + &
			" where depr_process_temp.depr_group_id = depr_group.depr_group_id and depr_group.combined_depr_group_id is not null " + &
			" order by depr_process_temp.set_of_books_id, depr_process_temp.gl_post_mo_yr "
f_create_dynamic_ds(ds_depr_processing,"grid",sqls,sqlca,true)

process_rows = ds_depr_processing.rowcount()

if process_rows > 0 then
	f_status_box("Information", String(Now(),"hh:mm:ss") + ":    Starting combined depr factors rebuild...")
end if

for p = 1 to process_rows 
	
	p_set_of_books_id = ds_depr_processing.getitemnumber(p, 'set_of_books_id')
	p_gl_post_mo_yr = ds_depr_processing.getitemdatetime(p, 'gl_post_mo_yr')
	p_company_id = ds_depr_processing.getitemnumber(p, 'company_id') 
	p_combined_depr_group_id = ds_depr_processing.getitemnumber(p, 'combined_depr_group_id')
	
	month_string =  String(p_gl_post_mo_yr, "MM/YYYY") 
	
	//setup temp tables...
	
	delete from cpr_act_month 
	where session_id = USERENV('SESSIONID') ; 	
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: delete from cpr_act_month failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 

	insert into  CPR_ACT_MONTH(USER_ID, BATCH_REPORT_ID, SESSIOn_id, MONTH)
	VALUES(user, 0, userenv('sessionid'), :p_gl_post_mo_yr);
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: insert into cpr_act_month failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
		
	delete from cpr_company
	where session_id = USERENV('SESSIONID') ;
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: delete from cpr_company failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
	
	insert into  CPR_COMPANY(USER_ID, BATCH_REPORT_ID, session_id, COMPANY_ID )
	VALUES(user, 0, userenv('sessionid'), :p_company_id);
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: inset into  cpr_company failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	    
	
	if not isnull(p_combined_depr_group_id) then
		
		delete from combined_depr_res_allo_factors 
		where month = :p_gl_post_mo_yr 
		and combined_depr_group_id =:p_combined_depr_group_id;
	
		if sqlca.sqlcode = -1 then
			 f_status_box('Information', 'Error: delete from combined_depr_res_allo_factors failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
								' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
			rollback;
			DESTROY ds_depr_processing
			DESTROY ds_depr_res_allo_factors
			return -1
		end if	 
		
		sql_mod_string = " cdg_ledger.combined_depr_group_id = " + string(p_combined_depr_group_id) + " and cdg_ledger.set_of_books_id = " + string(p_set_of_books_id)  //joined to cpr_act_month for month filter 		
	
		SetNull(rwip)
		rwip = Upper(f_pp_system_control_company('Include RWIP in Allocation Factors', p_company_id))	
		if IsNull(rwip) then rwip = 'NO'	
		
		ds_combined_depr_res_allo_factors = CREATE uo_ds_top 
		ds_combined_depr_res_allo_factors.SetTransObject(sqlca) 
		ds_combined_depr_res_allo_factors.DATAOBJECT  = 'dw_combined_depr_res_allo_factors'
		ds_combined_depr_res_allo_factors.reset() 
	
		// Theoretical factors
//		f_status_box("Information", String(Now(),"hh:mm:ss") + ": Calculating combined allo factors - Step 1...") 
		
		ds_allo_depr_reserve_theo = CREATE uo_ds_top
		if rwip = 'YES' then
			ds_allo_depr_reserve_theo.DataObject = "dw_allo_combined_depr_resrwip_theo"
		else
			ds_allo_depr_reserve_theo.DataObject = "dw_allo_combined_depr_reserve_theo"
		end if
		ds_allo_depr_reserve_theo.Settransobject(sqlca)
		rtn_string = f_add_where_clause_ds_1equal1(ds_allo_depr_reserve_theo,"", sql_mod_string, false) 
				
		if rtn_string = '' then  
			f_status_box("Information", "ERROR: f_add_where_clause_ds_1equal1 failed(2): "   + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
			rollback;
			DESTROY ds_combined_depr_res_allo_factors
			DESTROY ds_allo_depr_reserve_theo
			DESTROY ds_depr_processing
			return -1	  
		end if
		
		rtn = uf_allo_combined_reserve_theo(ds_allo_depr_reserve_theo)
			
		if rtn < 0 then
			f_status_box("Information", String(Now()) + ":Error in uf_allo_combined_reserve_theo.  Please rebuild factors manually.")
			rollback;
			DESTROY ds_combined_depr_res_allo_factors
			DESTROY ds_allo_depr_reserve_theo
			DESTROY ds_depr_processing
			return -1
		end if 
		
		rows = ds_allo_depr_reserve_theo.rowcount()
		
		for i = 1 to rows 
			 
			 ds_combined_depr_res_allo_factors.insertrow(1)
		
			 ds_combined_depr_res_allo_factors.setitem(1,'set_of_books_id', &
					  ds_allo_depr_reserve_theo.getitemnumber(i,'set_of_books_id'))
		
			 ds_combined_depr_res_allo_factors.setitem(1,'combined_depr_group_id', &
					  ds_allo_depr_reserve_theo.getitemnumber(i,'combined_depr_group_id'))
		
			 ds_combined_depr_res_allo_factors.setitem(1,'vintage', &
					  ds_allo_depr_reserve_theo.getitemnumber(i,'vintage'))
		
			 ds_combined_depr_res_allo_factors.setitem(1,'month', &
					  ds_allo_depr_reserve_theo.getitemdatetime(i,'accounting_month'))
		
			 factor = ds_allo_depr_reserve_theo.getitemnumber(i,'factor')
			 if isnull(factor) then factor = 0
			 ds_combined_depr_res_allo_factors.setitem(1,'factor', factor)
		
			 ds_combined_depr_res_allo_factors.setitem(1,'theo_factor',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'theo_factor'))
		
			 ds_combined_depr_res_allo_factors.setitem(1,'remaining_life',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'current_rem_life'))
		
			 ds_combined_depr_res_allo_factors.setitem(1,'life_factor',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'life_factor'))
		
			 ds_combined_depr_res_allo_factors.setitem(1,'cor_factor',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'cor_factor'))
		
		next
	end if
	
	// Dollar Age factors
//	f_status_box("Information", String(Now(),"hh:mm:ss") + ": Calculating combined allo factors - Step 2...") 
	
	ds_allo_depr_reserve = CREATE uo_ds_top
	if rwip = 'YES' then
		ds_allo_depr_reserve.DATAOBJECT  = 'dw_allo_combined_depr_resrwip'
	else
		ds_allo_depr_reserve.DATAOBJECT  = 'dw_allo_combined_depr_reserve'
	end if				
	ds_allo_depr_reserve.Settransobject(sqlca)		
	
	rtn_string = f_add_where_clause_ds_1equal1(ds_allo_depr_reserve,"", sql_mod_string, false)
				
	if rtn_string = '' then  
		f_status_box("Information", "ERROR: f_add_where_clause_ds_1equal1 failed(3): "   + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_combined_depr_res_allo_factors
		DESTROY ds_allo_depr_reserve_theo
		DESTROY ds_allo_depr_reserve
		DESTROY ds_depr_processing
		return -1	  
	end if
	
	rtn = uf_allo_combined_reserve(ds_allo_depr_reserve)
			
	if rtn < 0 then
		f_status_box("Information", String(Now()) + ":Error in uf_allo_combined_reserve.  Please rebuild factors manually.")		
		rollback;
		DESTROY ds_combined_depr_res_allo_factors
		DESTROY ds_allo_depr_reserve_theo
		DESTROY ds_allo_depr_reserve
		DESTROY ds_depr_processing
		return -1
	end if 
	
	rows = ds_allo_depr_reserve.rowcount()
	for i = 1 to rows 
		 ds_combined_depr_res_allo_factors.insertrow(1)
	
		 ds_combined_depr_res_allo_factors.setitem(1,'set_of_books_id', &
				  ds_allo_depr_reserve.getitemnumber(i,'set_of_books_id'))
	
		 ds_combined_depr_res_allo_factors.setitem(1,'combined_depr_group_id', &
				  ds_allo_depr_reserve.getitemnumber(i,'combined_depr_group_id'))
	
		 ds_combined_depr_res_allo_factors.setitem(1,'vintage', &
				  ds_allo_depr_reserve.getitemnumber(i,'vintage'))
	
		 ds_combined_depr_res_allo_factors.setitem(1,'month', &
				  ds_allo_depr_reserve.getitemdatetime(i,'accounting_month'))
	
		 factor = ds_allo_depr_reserve.getitemnumber(i,'factor')
		 if isnull(factor) then 
			factor = 0
		end if
		 ds_combined_depr_res_allo_factors.setitem(1,'factor', factor)
		 ds_combined_depr_res_allo_factors.setitem(1,'theo_factor',factor)
				
		 ds_combined_depr_res_allo_factors.setitem(1,'life_factor',&
		       ds_allo_depr_reserve.getitemnumber(i,'life_factor'))
	
		 ds_combined_depr_res_allo_factors.setitem(1,'cor_factor',&
		       ds_allo_depr_reserve.getitemnumber(i,'cor_factor'))
	
	next
	
	rtn = ds_combined_depr_res_allo_factors.update()
	
	if rtn < 0 then
		f_status_box("Error","Error updating Combined Depreciation Reserve Allocation Factors. SQLInfo: " + ds_depr_res_allo_factors.i_sqlca_sqlerrtext)
		rollback;
		DESTROY ds_combined_depr_res_allo_factors
		DESTROY ds_allo_depr_reserve_theo
		DESTROY ds_allo_depr_reserve
		DESTROY ds_depr_processing 
		return -1
	end if 
	
	insert into combined_depr_res_allo_factors 
	(set_of_books_id, combined_depr_group_id, vintage, month, factor, theo_factor, remaining_life,
	 life_factor, cor_factor)
	(select a.set_of_books_id, d.combined_depr_group_id, 
	to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :p_gl_post_mo_yr, 
	decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 
	decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)),
	avg(decode(init_life,0,null,remaining_life / init_life)),
	decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 0
	from cpr_depr a, cpr_ledger b, depr_group d
	where a.asset_id = b.asset_id
	and a.gl_posting_mo_yr = :p_gl_post_mo_yr
	and a.company_id = :p_company_id
	and b.depr_group_id = d.depr_group_id
	and d.subledger_Type_id <> 0
	and d.combined_depr_group_id = :p_combined_depr_group_id
	and a.set_of_books_id = :p_set_of_books_id
	group by a.set_of_books_id, d.combined_depr_group_id, to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :p_gl_post_mo_yr)	;
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: insert into combined_depr_res_allo_factors combined_depr_res_allo_factors for cpr_depr failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		
		rollback;
		DESTROY ds_combined_depr_res_allo_factors
		DESTROY ds_allo_depr_reserve_theo
		DESTROY ds_allo_depr_reserve
		DESTROY ds_depr_processing 
		return -1
	end if	 
	
//	f_status_box("Information", String(Now(), "hh:mm:ss") + ": Pushing combined reserve allocation down to member groups. Step 1")
	DELETE FROM depr_res_allo_factors
	WHERE month = :p_gl_post_mo_yr 
		AND (depr_group_id, set_of_books_id) IN
		(
			SELECT dg.depr_group_id, dcgb.set_of_books_id
			FROM depr_combined_group_books dcgb, depr_group dg
			WHERE dcgb.combined_depr_group_id = dg.combined_depr_group_id
				AND dg.company_id = :p_company_id 
				AND dg.combined_depr_group_id = :p_combined_depr_group_id
				AND dcgb.set_of_books_id = :p_set_of_books_id
				AND NVL(dcgb.combined_reserve_allocation, 0) = 1
		);
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error:Pushing combined reserve allocation  Step 1 failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		
		rollback;
		DESTROY ds_combined_depr_res_allo_factors
		DESTROY ds_allo_depr_reserve_theo
		DESTROY ds_allo_depr_reserve
		DESTROY ds_depr_processing 
		return -1
	end if	 
	
//	f_status_box("Information", String(Now(), "hh:mm:ss") + ": Pushing combined reserve allocation down to member groups. Step 2")
	INSERT INTO depr_res_allo_factors (set_of_books_id, depr_group_id, vintage, month, factor, 
		theo_factor, remaining_life, life_factor, cor_factor)
	SELECT dcgb.set_of_books_id, dg.depr_group_id, cdraf.vintage, cdraf.month, cdraf.factor,
		cdraf.theo_factor, cdraf.remaining_life, cdraf.life_factor, cdraf.cor_factor
	FROM depr_combined_group_books dcgb, depr_group dg, combined_depr_res_allo_factors cdraf
		WHERE dcgb.combined_depr_group_id = dg.combined_depr_group_id
			AND dcgb.combined_depr_group_id = cdraf.combined_depr_group_id
			AND dcgb.set_of_books_id = cdraf.set_of_books_id
			AND dg.company_id = :p_company_id
			AND dg.combined_depr_group_id =  :p_combined_depr_group_id
			AND cdraf.set_of_books_id = :p_set_of_books_id
			AND NVL(dcgb.combined_reserve_allocation, 0) = 1
			AND cdraf.month = :p_gl_post_mo_yr	;		
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error:Pushing combined reserve allocation  Step 2 failed: ' + sqlca.SQLErrText + ' (cdg_id = ' + string(p_combined_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		
		rollback;
		DESTROY ds_combined_depr_res_allo_factors
		DESTROY ds_allo_depr_reserve_theo
		DESTROY ds_allo_depr_reserve
		DESTROY ds_depr_processing 
		return -1
	end if	 
	
next  //for p = 1 to process_rows (combined depr group, sob, month)

DESTROY ds_depr_processing   

return 1

end function

public function longlong uf_depr_ledger_balances ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This function foots the depr_ledger ending balance and rolls forward to next month, including updates to cpr_depr and subledger tables as needed
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong ret, i, rows, depr, curs, tablepos, mo, yr, company_id, check_dl_count
longlong p, process_rows, rtn, subledger_type_id , p_depr_group_id, p_set_of_books_id, depr_indicator
string sqls, sub_string, template, replacestring, subledger, sql_dollar, next_month_number, month_string
decimal{2} beg_bal, beg_res, end_bal, end_res 
datetime moyr, p_gl_post_mo_yr

//Whatever is going to be calling this function should be sure not to insert into DEPR_PROCESSING_TEMP table where nvl(subledger_type_id,0) < 0 (lease depr groups)
	
//NEED TO LOOP THROUGH DEPR_PROCESSING_TEMP TABLE 
uo_ds_top ds_depr_processing
ds_depr_processing = CREATE uo_ds_top
ds_depr_processing.DataObject = "dw_temp_dynamic"
	
sqls = "select depr_group_id, set_of_books_id, gl_post_mo_yr from depr_process_temp order by depr_group_id, set_of_books_id, gl_post_mo_yr "
f_create_dynamic_ds(ds_depr_processing,"grid",sqls,sqlca,true)

process_rows = ds_depr_processing.rowcount() 

if process_rows > 0 then
	f_status_box("Information", String(Now(),"hh:mm:ss") + ": Starting depr balance updates...")
end if

for p = 1 to process_rows
	
	p_depr_group_id = ds_depr_processing.getitemnumber(p, 'depr_group_id')
	p_set_of_books_id = ds_depr_processing.getitemnumber(p, 'set_of_books_id')
	p_gl_post_mo_yr = ds_depr_processing.getitemdatetime(p, 'gl_post_mo_yr')
	
	check_dl_count = 0 
	select count(*) into :check_dl_count
	from depr_ledger
	where depr_group_id = :p_depr_group_id
	and set_of_books_id = :p_set_of_books_id
	and  gl_post_mo_yr = :p_gl_post_mo_yr;
	
	if check_dl_count <= 0 then
		 f_status_box('Information', 'Error: no depr_ledger data for : ' + sqlca.SQLErrText + ' (depr_group_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if 
	
	month_string =  String(p_gl_post_mo_yr, "MM/YYYY")  
	
	f_status_box("Information",  '              depr ledger (depr_group_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(month_string) + ' sob_id = ' + string(p_set_of_books_id) + ')')
	
	//Depr_ledger_status needs to be updated elsewhere (ie called from w_cpr_control's month end)

	// Foot ending balances on depr ledger
	update depr_ledger u
	set (end_balance, end_reserve, reserve_bal_cor, reserve_bal_adjust, 
		  reserve_bal_retirements, reserve_bal_tran_in, reserve_bal_tran_out,
		  reserve_bal_gain_loss, reserve_bal_other_credits, current_net_salvage_reserve,
		  cor_end_reserve, reserve_bal_salvage_exp, salvage_balance, reserve_bal_impairment ) =
		(select  c.begin_balance + c.additions + c.retirements + c.transfers_in 
					+ c.transfers_out + c.adjustments + c.impairment_asset_amount
				 , c.begin_reserve + c.retirements + c.gain_loss + c.salvage_cash 
					+ c.salvage_returns + c.reserve_credits + c.reserve_tran_in + c.reserve_tran_out 
					+ c.reserve_adjustments + c.depr_exp_alloc_adjust + c.depr_exp_adjust + c.depreciation_expense
					- nvl(c.current_net_salvage_amort,0) + decode(nvl(subledger_type_id,0), 0, 0, c.cost_of_removal)
					+ c.salvage_expense + c.salvage_exp_adjust + c.salvage_exp_alloc_adjust  + c.reserve_blending_transfer
					+ c.impairment_expense_amount + c.impairment_asset_amount
				 , nvl(p.reserve_bal_cor,0) + c.cost_of_removal
				 , nvl(p.reserve_bal_adjust,0) + c.reserve_adjustments + c.reserve_blending_adjustment + c.reserve_blending_transfer
				 , nvl(p.reserve_bal_retirements,0) + c.retirements
				 , nvl(p.reserve_bal_tran_in,0) + c.reserve_tran_in
				 , nvl(p.reserve_bal_tran_out,0) + c.reserve_tran_out
				 , nvl(p.reserve_bal_gain_loss,0) + c.gain_loss
				 , nvl(p.reserve_bal_other_credits,0) + c.reserve_credits
				 , nvl(p.current_net_salvage_reserve,0) + nvl(c.current_net_salvage_amort,0)
				 , decode(nvl(subledger_type_id,0), 0, c.cor_beg_reserve + c.cor_expense + c.cor_exp_adjust + c.cost_of_removal
					+ c.cor_res_tran_in + c.cor_res_tran_out + c.cor_res_adjust + c.cor_exp_alloc_adjust  + c.cor_blending_transfer, 0)
				 , nvl(p.reserve_bal_salvage_exp, 0) + c.salvage_expense + c.salvage_exp_adjust + c.salvage_exp_alloc_adjust  
				 , nvl(p.salvage_balance, 0) + c.salvage_cash + c.salvage_returns
				 , (nvl(p.reserve_bal_impairment, 0) + c.impairment_expense_amount + c.impairment_asset_amount)
		 from  depr_ledger p, depr_ledger c, depr_group g
		 where p.depr_group_id (+) = c.depr_group_id
		 and   p.set_of_books_id (+) = c.set_of_books_id
		 and   c.depr_group_id = g.depr_group_id
		 and   to_char(p.gl_post_mo_yr (+), 'mm/yyyy') = to_char(add_months(c.gl_post_mo_yr,-1), 'mm/yyyy' ) 
		 and   u.depr_group_id = c.depr_group_id
		 and   u.set_of_books_id = c.set_of_books_id
		 and   u.gl_post_mo_yr = c.gl_post_mo_yr
		)
	where u.gl_post_mo_yr = :p_gl_post_mo_yr
	and	u.depr_group_id  = :p_depr_group_id
	and	u.set_of_books_id  = :p_set_of_books_id; 
	
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: update depr_ledger ending failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
	
	// if no row exists to update begin balance then don't do balancing check
	if sqlca.sqlnrows <> 0 then 
		// Roll forward to beginning balances on depr ledger
		update depr_ledger a  
		set (begin_reserve, begin_balance, impairment_reserve_beg, cor_beg_reserve) =
			(select b.end_reserve, b.end_balance, impairment_reserve_end, cor_end_reserve
			 from  depr_ledger b 
			 where a.set_of_books_id = b.set_of_books_id and
					 a.depr_group_id   = b.depr_group_id   and 
					 add_months(a.gl_post_mo_yr, -1) = b.gl_post_mo_yr
			)
		where a.gl_post_mo_yr = add_months(:p_gl_post_mo_yr,1) 
		and	a.depr_group_id  = :p_depr_group_id
		and	a.set_of_books_id  = :p_set_of_books_id;
		
		if sqlca.sqlcode = -1 then
			 f_status_box('Information', 'Error: update depr_ledger beginning failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
								' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
			rollback;
			DESTROY ds_depr_processing
			return -1
		end if	 
		
		// make sure beg bal = end bal
		select round(sum(begin_balance),2), round(sum(begin_reserve),2)  into :beg_bal, :beg_res
		from depr_ledger 
		where gl_post_mo_yr = add_months(:p_gl_post_mo_yr, 1)
		and depr_group_id  = :p_depr_group_id
		
		and set_of_books_id = :p_set_of_books_id;
		select round(sum(end_balance),2),round(sum(end_reserve),2) into :end_bal, :end_res
		from depr_ledger 
		where  gl_post_mo_yr = :p_gl_post_mo_yr
		and depr_ledger.depr_group_id  = :p_depr_group_id
		and depr_ledger.set_of_books_id = :p_set_of_books_id;
		
		if beg_bal <> end_bal then
			f_status_box("Information","Begin Plant Balance : " + string(beg_bal) + " not equal to " + &
			" End Plant Balance " + string(end_bal))
			rollback;
			DESTROY ds_depr_processing
			return -1
		end if
		
		if beg_res <> end_res then
			f_status_box("Information","Begin Reserve Balance: " + string(beg_res) + " not equal to " + &
			" End Reserve Balance " + string(end_res))
			rollback;
			DESTROY ds_depr_processing
			return -1
		end if	
	end if
	
	//Process depr groups setup for Indivual Asset Depr	
	select nvl(subledger_type_id, 0) into :subledger_type_id
	from depr_group where depr_group_id = :p_depr_group_id;	 
	
	if subledger_type_id <> 0 then				
		
		//Right now, this part is not coded, no prior month activities should be posting for IAD depr groups	  
		f_status_box("Information","Unable to process depr groups setup for individual asset depr.")
		rollback;
		DESTROY ds_depr_processing
		return -1
		
		depr_indicator = 0
		select depreciation_indicator into :depr_indicator
		from subledger_control where subledger_type_id = :subledger_type_id; 
	
		if depr_indicator = 1 then
			rtn = uf_subledger_balances()			 
				
			if rtn = -1 then
				rollback;
				DESTROY ds_depr_processing
				return -1 
			end if			 		
		end if 
		
		if depr_indicator = 3 then
			rtn = uf_cpr_depr_balances()	
				
			if rtn = -1 then
				rollback;
				DESTROY ds_depr_processing
				return -1 
			end if			 			 
		end if     
	end if

next //process row (depr_group_id, sob_id, gl_mo)
 
DESTROY ds_depr_processing
return 1
end function

public function longlong uf_cpr_depr_balances ();//Not yet coded for prior period postings on cpr_depr depr groups

return 1
end function

public function longlong uf_subledger_balances ();//Not yet coded for prior period postings on cpr_depr depr groups

//	f_status_box("Information", "Update Balances on all Depreciation Subledgers...")

////Use for subledger updates
//uo_ds_top ds_subledger
//ds_subledger = CREATE uo_ds_top
//ds_subledger.DataObject = "dw_temp_dynamic"
	
//sqls = "select subledger_name, depreciation_indicator from subledger_control order by subledger_name "
//f_create_dynamic_ds(ds_subledger,"grid",sqls,sqlca,true)
//
//rows = ds_subledger.rowcount()
//DESTROY ds_subledger
//
//
////	mo = month(date(moyr))	
//	yr = year(date(moyr))
//	
//	if mo = 12 then
//		mo = 1
//		yr = yr + 1
//	else
//		mo = mo + 1
//	end if
//	
//	if mo >= 10 then
//		next_month_number = string(yr) + string(mo) 
//	else
//		next_month_number = string(yr) + '0' + string(mo) 
//	end if
//	
//	sub_string = &
//	"update depr_subledger_template a  set (beg_reserve,beg_asset_dollars) = " &
//		+ "(select b.depr_reserve, b.asset_dollars from  depr_subledger_template b " &
//				+ "where a.asset_id = b.asset_id and " &
//				+ "   a.subledger_item_id   = b.subledger_item_id   and  " &
//				+ "   a.subledger_type_id   = b.subledger_type_id   and  " &
//				+ "   to_char(b.cpr_posting_mo_yr, 'mm/yyyy') = " &
//				+ " '" + String(moyr, 'mm/yyyy') + "'"  &
//				+ "  and to_char(a.cpr_posting_mo_yr,'yyyymm') = " + next_month_number + " ) " & 
//				+ "  where to_char(a.cpr_posting_mo_yr,'yyyymm') = " + next_month_number  &
//				+ "  and a.asset_id in (select asset_id from cpr_ledger where " &
//				+ "  company_id = " + string(company_id) + " and nvl(subledger_indicator,0) > 0 )"

// subledger = lower(trim(ds_subledger.getitemstring(i,'subledger_name'))) 
//
// //$$$sjh: added 9/30/2004 for footing subledger ending balances before rollfwd
// sql_dollar =  "update " + subledger + "_depr " + & 
//	" set asset_dollars = nvl(beg_asset_dollars,0) + nvl(net_adds_and_adjust,0) + nvl(retirements,0)," + &
//	" depr_reserve =  nvl(beg_reserve,0) +  nvl(cost_of_removal,0) +  nvl(retirements,0) +  nvl(salvage_dollars,0) +" + &
//	" nvl(gain_loss,0) +  nvl(curr_depr_expense,0) +  nvl(other_credits_and_adjust ,0) " + &
//	" where to_char(cpr_posting_mo_yr, 'mm/yyyy') = " + &
//	" '" + String(moyr, 'mm/yyyy') + "'" + &
//	" and asset_id in (select asset_id from cpr_ledger where " + &
//	 "  company_id = " + string(company_id) + " and nvl(subledger_indicator,0) > 0) " 
//
//  execute immediate :sql_dollar using sqlca;	
//
//  ret = f_check_sql_error(sqlca,"Error trying to (foot ending) update "+ subledger + " subledger.")
//
// sqls = sub_string
//
////Find tablename throughout SQL and replace; Check DBMS for necessary case changes
//
//template = 'depr_subledger_template'
//
//choose case s_sys_info.dbms
//	case "sybase"
//	template = lower(template)
//	replacestring = lower(subledger+'_depr')
//	case "oracle"
//	template = lower(template)
//	replacestring = lower(subledger+'_depr')
//case else
//end choose
//
//curs = 1
//tablepos = pos(sqls, template, curs)
//do until tablepos = 0
//	sqls = replace(sqls, tablepos, 23, replacestring)		
//	curs = curs + len(replacestring)
//	tablepos = pos(sqls, template, curs)
//loop
//
//
//
//// Execute the sql update of the depreciation subledger
//
////f_status_box("Information", "Update " + subledger  + " Depreciation Ledger...") 
// 
//execute immediate :sqls using sqlca;
// 
//ret = f_check_sql_error(sqlca,"Error trying to roll foward "+ subledger + " subledger.")
// 

return 1
end function

public function integer uf_depr_ledger_blending_balances ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This function foots the depr_ledger_blending ending balance and rolls forward to next month
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longlong ret, i, rows, depr, curs, tablepos, mo, yr, company_id, check_dl_count, dl_status
longlong p, process_rows, rtn, subledger_type_id , p_depr_group_id, p_set_of_books_id, depr_indicator
string sqls, sub_string, template, replacestring, subledger, sql_dollar, next_month_number, month_string
decimal{2} beg_bal, beg_res, end_bal, end_res 
datetime moyr, p_gl_post_mo_yr
boolean blending

//Whatever is going to be calling this function should be sure not to insert into DEPR_PROCESSING_TEMP table where nvl(subledger_type_id,0) < 0 (lease depr groups)
			
//NEED TO LOOP THROUGH DEPR_PROCESSING_TEMP TABLE 
uo_ds_top ds_depr_processing
ds_depr_processing = CREATE uo_ds_top
ds_depr_processing.DataObject = "dw_temp_dynamic"
	
sqls = "select depr_group_id, set_of_books_id, gl_post_mo_yr from depr_process_temp order by depr_group_id, set_of_books_id, gl_post_mo_yr "
f_create_dynamic_ds(ds_depr_processing,"grid",sqls,sqlca,true)

process_rows = ds_depr_processing.rowcount() 

if process_rows > 0 then
	f_status_box("Information", String(Now(),"hh:mm:ss") + ": Starting depr balance updates...")
end if

for p = 1 to process_rows
	
	p_depr_group_id = ds_depr_processing.getitemnumber(p, 'depr_group_id')
	p_set_of_books_id = ds_depr_processing.getitemnumber(p, 'set_of_books_id')
	p_gl_post_mo_yr = ds_depr_processing.getitemdatetime(p, 'gl_post_mo_yr')
	
	blending = uf_check_blending(p_depr_group_id, p_set_of_books_id, p_gl_post_mo_yr)
	
	if not blending then continue
	
	check_dl_count = 0 
	select count(*) into :check_dl_count
	from depr_ledger_blending
	where depr_group_id = :p_depr_group_id
	and set_of_books_id = :p_set_of_books_id
	and  gl_post_mo_yr = :p_gl_post_mo_yr;
	
	if check_dl_count <= 0 then
		// if the current month (status = 8) then no row is ok b/c the calc will take create it
		SetNull(dl_status)
		select depr_ledger_status into :dl_status
		from depr_ledger
		where depr_group_id = :p_depr_group_id
		and set_of_books_id = :p_set_of_books_id
		and  gl_post_mo_yr = :p_gl_post_mo_yr;
		
		if dl_status = 8 or IsNull(dl_status) then  continue
		
		 f_status_box('Information', 'Error: no depr_ledger_blending data for : ' + sqlca.SQLErrText + ' (depr_group_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if 
	
	month_string =  String(p_gl_post_mo_yr, "MM/YYYY")  
	
	f_status_box("Information",  '              depr ledger (depr_group_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(month_string) + ' sob_id = ' + string(p_set_of_books_id) + ')')
	
	//Depr_ledger_status needs to be updated elsewhere (ie called from w_cpr_control's month end)

	// Foot ending balances on depr ledger
	update depr_ledger_blending u
	set (end_balance_bl, end_reserve_bl, cor_end_reserve_bl ) =
		(select  c.begin_balance_bl + c.additions_bl + c.retirements_bl + c.transfers_in_bl 
					+ c.transfers_out_bl + c.adjustments_bl + c.impairment_asset_amount_bl
				 , c.begin_reserve_bl + c.retirements_bl + c.gain_loss_bl + c.salvage_cash_bl 
					+ c.salvage_returns_bl + c.reserve_credits_bl + c.reserve_tran_in_bl + c.reserve_tran_out_bl 
					+ c.reserve_adjustments_bl + c.depr_exp_alloc_adjust_bl + c.depr_exp_adjust_bl + c.depreciation_expense_bl
					- nvl(c.current_net_salvage_amort_bl,0) + decode(nvl(subledger_type_id,0), 0, 0, c.cost_of_removal_bl)
					+ c.salvage_expense_bl + c.salvage_exp_adjust_bl + c.salvage_exp_alloc_adjust_bl  + c.reserve_blending_transfer
					+ c.impairment_expense_amount_bl + c.impairment_asset_amount_bl
				 , decode(nvl(subledger_type_id,0), 0, c.cor_beg_reserve_bl + c.cor_expense_bl + c.cor_exp_adjust_bl + c.cost_of_removal_bl
					+ c.cor_res_tran_in_bl + c.cor_res_tran_out_bl + c.cor_res_adjust_bl + c.cor_exp_alloc_adjust_bl  + c.cor_blending_transfer, 0)
		 from  depr_ledger_blending p, depr_ledger_blending c, depr_group g
		 where p.depr_group_id (+) = c.depr_group_id
		 and   p.set_of_books_id (+) = c.set_of_books_id
		 and   c.depr_group_id = g.depr_group_id
		 and   to_char(p.gl_post_mo_yr (+), 'mm/yyyy') = to_char(add_months(c.gl_post_mo_yr,-1), 'mm/yyyy' ) 
		 and   u.depr_group_id = c.depr_group_id
		 and   u.set_of_books_id = c.set_of_books_id
		 and   u.gl_post_mo_yr = c.gl_post_mo_yr
		)
	where u.gl_post_mo_yr = :p_gl_post_mo_yr
	and	u.depr_group_id  = :p_depr_group_id
	and	u.set_of_books_id  = :p_set_of_books_id; 
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: update depr_ledger_blending ending failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
	
	// Roll forward to beginning balances on depr ledger
	update depr_ledger_blending a  
	set (begin_reserve_bl, begin_balance_bl, impairment_reserve_beg_bl, cor_beg_reserve_bl) =
		(select b.end_reserve_bl, b.end_balance_bl, impairment_reserve_end_bl, cor_end_reserve_bl
		 from  depr_ledger_blending b 
		 where a.set_of_books_id = b.set_of_books_id and
				 a.depr_group_id   = b.depr_group_id   and 
				 add_months(a.gl_post_mo_yr, -1) = b.gl_post_mo_yr
		)
	where a.gl_post_mo_yr = add_months(:p_gl_post_mo_yr,1) 
	and	a.depr_group_id  = :p_depr_group_id
	and	a.set_of_books_id  = :p_set_of_books_id;
	
	if sqlca.sqlcode = -1 then
		 f_status_box('Information', 'Error: update depr_ledger_blending beginning failed: ' + sqlca.SQLErrText + ' (dg_id = ' + string(p_depr_group_id) + &
		 					' mo = ' + string(p_gl_post_mo_yr) + ' sob_id = ' + string(p_set_of_books_id) + ')')
		rollback;
		DESTROY ds_depr_processing
		return -1
	end if	 
	
	if sqlca.sqlnrows <> 0 then 
		// make sure beg bal = end bal
		select round(sum(begin_balance_bl),2), round(sum(begin_reserve_bl),2)  into :beg_bal, :beg_res
		from depr_ledger_blending 
		where gl_post_mo_yr = add_months(:p_gl_post_mo_yr   , 1)
		and depr_group_id  = :p_depr_group_id
		and set_of_books_id = :p_set_of_books_id;
		
		select round(sum(end_balance_bl),2),round(sum(end_reserve_bl),2) into :end_bal, :end_res
		from depr_ledger_blending 
		where  gl_post_mo_yr = :p_gl_post_mo_yr
		and depr_group_id  = :p_depr_group_id
		and set_of_books_id = :p_set_of_books_id;
		
		if beg_bal <> end_bal then
			f_status_box("Information","Begin Plant Balance : " + string(beg_bal) + " not equal to " + &
			" End Plant Balance " + string(end_bal))
			rollback;
			DESTROY ds_depr_processing
			return -1
		end if
		
		if beg_res <> end_res then
			f_status_box("Information","Begin Reserve Balance: " + string(beg_res) + " not equal to " + &
			" End Reserve Balance " + string(end_res))
			rollback;
			DESTROY ds_depr_processing
			return -1
		end if	
	end if
	
next //process row (depr_group_id, sob_id, gl_mo)
 
DESTROY ds_depr_processing
return 1
end function

public function boolean uf_check_blending (longlong a_dg_id, longlong a_sob_id, datetime a_moyr);////////////////////////////
// maint 8952: new function to update DEPR_LEDGER_BLENDING for prior period adjustments
///////////////////////////
longlong blending

// find out if this group has blending.
// this  is needed if this set of books a source book to another blended book
blending = 0
select count(*) into :blending
from depr_Method_rates r, depr_group g
where r.depr_method_id = g.depr_method_id
and r.effective_date =
	(select max(x.effective_date) from depr_method_rates x
		where x.depr_method_id = r.depr_method_id
		and x.effective_date <= :a_moyr
	)
and r.set_of_books_id <> :a_sob_id
and g.depr_group_id = :a_dg_id
and r.rate_used_code = 3
;

if blending = 0 then return false


return true


end function

public function integer uf_add_missing_cpr_act_depr_groups ();insert into cpr_act_depr_group (asset_id, gl_posting_mo_yr, depr_group_id)
select distinct a.asset_id, b.gl_posting_mo_yr, a.depr_group_id
from cpr_ledger a, cpr_activity b
where a.asset_id = b.asset_id
and a.depr_group_id is not null
and (b.asset_id, b.gl_posting_mo_yr) in (
	select asset_id, gl_posting_mo_yr from cpr_activity
	minus
	select asset_id, gl_posting_mo_yr from cpr_act_depr_group
);

if sqlca.sqlcode <> 0 then
	 f_status_box('Information', 'Error: Insert Into CPR Act Depr Groups failed: ' + sqlca.SQLErrText)
	rollback;
	return -1
end if	

return 1
end function

public function integer uf_cpr_attach_depr_groups (string optional_and_clause);/******************************************************
Description: 
	Attached Depr Groups to CPR Assets
	Use depr Process temp table to get company, bs ua from depr group id.
Arg:
	String: 	optional_and_clause 
				Should not start with "AND", "OR", or "WHERE"
				Should refer to CPR_LEDGER as "CPR" 
				for example "CPR.DEPR_GROUP_ID IS NULL"
		
Returns: 
	-1 - Abnormal End
	 0 - Nothing to Process
	 1 - Success
	
******************************************************/
boolean has_filter
longlong rtn, i
string str_rtn, sqls

longlong co_id, bs_id, ua_id

uo_ds_top ds_temp

select count(1) into :rtn 
from depr_process_temp;
if rtn < 1 then
	return 0
end if

////Create DataStore
ds_temp = create uo_ds_top

////Handle where clause
if isNull(optional_and_clause) or trim(optional_and_clause) = "" then
	has_filter = false
else 
	////Cleean up the "And" Clause
	////Just to be sure remove the first AND or WHERE.
	optional_and_clause = upper(optional_and_clause)
	has_filter = true
	rtn = pos(optional_and_clause,"AND", 1)
	if rtn > 0 then
		optional_and_clause = f_replace_string(optional_and_clause, "AND","", "first")
	else
		rtn = pos(optional_and_clause,"WHERE", 1)
		if rtn > 0 then
			optional_and_clause = f_replace_string(optional_and_clause, "WHERE","", "first")
		end if
	end if
end if

sqls = + &
"select distinct c.company_id, a.bus_segment_id, a.utility_account_id " + "~r~n" + &
"from depr_group_control a, depr_process_temp b, depr_group c " + "~r~n" + &
"where a.depr_group_id  = b.depr_group_id " + "~r~n" + &
"and a.depr_group_id = c.depr_group_id "

str_rtn = f_create_dynamic_ds(ds_temp, 'grid', sqls, sqlca,true)
if str_rtn <> "OK" then
	messageBox("Assign Depr Groups to Asset", "Error: Create DS Failed:~r~n" + str_rtn,stopSign!)
	destroy ds_temp
	return -1
end if

FOR i = 1 to ds_temp.rowCount()
	co_id = ds_temp.getItemNumber(i,1)
	bs_id = ds_temp.getItemNumber(i,2)
	ua_id = ds_temp.getItemNumber(i,3)
	////Call function, pass in company, bus_segment_id, utility account_id and where_clause
	rtn = uf_assign_depr_group_to_asset(co_id, bs_id, ua_id, optional_and_clause)
	if rtn <> 1 then
		destroy ds_temp
		return -1
	end if
NEXT

destroy ds_temp
return 1
end function

public function integer uf_assign_depr_group_to_asset (longlong a_co_id, longlong a_bs_id, longlong a_ua_id, string a_cpr_filter);//Maint 41333:  this code is very similiar to the find depr code in w_depr_ledger_attach and w_depr_ledger_maint
longlong sa_flag, gl_flag, ml_flag, al_flag, lt_flag, sl_flag, bv_flag, pu_flag, ru_flag, ccid_flag, ccval_flag
longlong sa_id, gl_id, ml_id, al_id, lt_id, sl_id, yyyy, pu_id, ru_id, cc_id
longlong idx
boolean first_cpr
string sqls, from_clause, where_clause, fields, update_sqls, cc_val, str_rtn
longlong rows,rows2, depr_group, updated, i, num_co, co, old_depr_group, status
longlong ua_rows, ua
string logfile, dg_error_msg, msg
decimal{2} total_cost

longlong dgc_gl_id, dgc_al_id, dgc_ml_id, dgc_lt_id, dgc_sa_id, dgc_sl_id, dgc_cc_id, dgc_pu_id, dgc_ru_id, dgc_yyyy, dg_cc_id
string dgc_cc_val

uo_ds_top ds_assets
ds_assets = CREATE uo_ds_top

sa_flag = 0
gl_flag = 0
ml_flag = 0
al_flag = 0
sl_flag = 0
lt_flag = 0
bv_flag = 0
pu_flag = 0
ru_flag = 0
ccid_flag = 0
ccval_flag = 0

SetNull(sa_id )
SetNull(gl_id )
SetNull(ml_id )
SetNull(al_id )
SetNull(sl_id )
SetNull(lt_id )
SetNull(yyyy )
SetNull(pu_id )
SetNull(ru_id )
SetNull(cc_id )
SetNull(cc_val )


// get fields used
select nvl(sum(sub_account_id),0),
		 nvl(sum(gl_account_id),0),
		 nvl(sum(major_location_id),0),
		 nvl(sum(asset_location_id),0),
		 nvl(sum(c.subledger_type_id),0),
		 nvl(sum(location_type_id),0),
		 nvl(sum(property_unit_id),0),
		 nvl(sum(retirement_unit_id),0),
		 nvl(sum(to_number(to_char(book_vintage,'YYYY'))),0),
		 nvl(sum(class_code_id),0),
		 nvl(sum(decode(trim(cc_value), null, 0, '', 0, 1)) ,0) 
into   :sa_flag, :gl_flag, :ml_flag, :al_flag, :sl_flag, :lt_flag, :pu_flag, :ru_flag, :bv_flag, :ccid_flag, :ccval_flag
from   depr_group_control c, depr_group g
where  c.bus_segment_id = :a_bs_id
and    utility_account_id = :a_ua_id
and    c.depr_group_id = g.depr_group_id
and    company_id = :a_co_id
;

// Build CPR Retrieve SQL
sqls   = "" 
from_clause = " from	 cpr_ledger cpr " 
where_clause = " where cpr.depr_group_id is null and cpr.company_id = " + String(a_co_id) &
					+ " and cpr.bus_segment_id = " + String(a_bs_id) &
					+ " and cpr.utility_account_id = " + String(a_ua_id)

fields = " cpr.company_id, cpr.bus_segment_id, cpr.utility_account_id, cpr.depr_group_id " 
idx = 5
if sa_flag <> 0 then	
	fields = fields + ", cpr.sub_account_id"
	sa_flag = idx
	idx ++
end if
if gl_flag <> 0 then	
	fields = fields + ", cpr.gl_account_id"
	gl_flag = idx
	idx ++
end if
if lt_flag <> 0 then	
	fields = fields + ", m.location_type_id"
	lt_flag = idx
	idx ++
	
	from_clause = from_clause + ", asset_location l, major_location m "
	where_clause = where_clause &
		 + " and cpr.asset_location_id = l.asset_location_id " &
		 + " and m.major_location_id = l.major_location_id " 
end if
if ml_flag <> 0 then	
	fields = fields + ", l.major_location_id"
	ml_flag = idx
	idx ++
	
	if lt_flag = 0 then
		from_clause = from_clause + ", asset_location l "
		where_clause = where_clause + " and cpr.asset_location_id = l.asset_location_id " 
	end if			
end if
if al_flag <> 0 then	
	fields = fields + ", l.asset_location_id"
	al_flag = idx
	idx ++
	
	if lt_flag = 0 and ml_flag = 0 then
		from_clause = from_clause + ", asset_location l "
		where_clause = where_clause + " and cpr.asset_location_id = l.asset_location_id " 
	end if			
end if
if sl_flag <> 0 then	
	fields = fields + ", cpr.subledger_indicator"
	sl_flag = idx
	idx ++
end if
if bv_flag <> 0 then
	fields = fields + ", to_number(to_char(cpr.eng_in_service_year,'yyyy'))"
	bv_flag = idx
	idx ++
end if
if pu_flag <> 0 then	
	// ### 29114: JAK: 2013-01-23:  Updated field name from "property_unit" to "property_unit_id"
	fields = fields + ", r.property_unit_id"
	pu_flag = idx
	idx ++
	
	from_clause = from_clause + ", retirement_unit r "
	where_clause = where_clause &
		 + " and cpr.retirement_unit_id = r.retirement_unit_id " 	
end if
if ru_flag <> 0 then	
	fields = fields + ", cpr.retirement_unit_id"
	ru_flag = idx
	idx ++
end if

dg_cc_id = f_get_dg_cc_id()
if dg_cc_id > 0 then
	if ccid_flag <> 0 and ccval_flag <> 0 then
		fields = fields + ", cc.class_code_id"
		ccid_flag = idx
		idx ++
		fields = fields + ", cc.value"
		ccval_flag = idx
		idx ++
		
		from_clause = from_clause + ", class_code_cpr_ledger cc"
		where_clause = where_clause &
			+ " and cpr.asset_id = cc.asset_id" &
			+ " and cc.class_code_id = " + string(dg_cc_id) + " " 
	end if
end if 


sqls = 'select ' + fields + ', sum(cpr.accum_cost) ' + from_clause + where_clause + ' group by ' + fields

str_rtn = f_create_dynamic_ds(ds_assets, "grid", sqls, sqlca, False)
if str_rtn <> 'OK' then
	destroy ds_assets
	return -1
end if

ds_assets.SetTransObject(sqlca)
rows = ds_assets.Retrieve()
f_status_box("Info","  " + String(a_co_id) + "/" + String(a_bs_id) + "/" + String(a_ua_id) &
						+ ": " + String(rows) + " CPR combinations to process")
////
//// loop over cpr rows
////
first_cpr = true
for i = 1 to rows
	
	if sa_flag <> 0 then	
		sa_id = ds_assets.GetItemNumber(i, sa_flag)
		dgc_sa_id = sa_id
	end if
	if gl_flag <> 0 then
		gl_id = ds_assets.GetItemNumber(i, gl_flag)
		dgc_gl_id = gl_id
	end if
	if ml_flag <> 0 then	
		ml_id = ds_assets.GetItemNumber(i, ml_flag)
		dgc_ml_id = ml_id
	end if
	if al_flag <> 0 then	
		al_id = ds_assets.GetItemNumber(i, al_flag)
		dgc_al_id = al_id
	end if
	if sl_flag <> 0 then	
		sl_id = ds_assets.GetItemNumber(i, sl_flag)
		dgc_sl_id = sl_id
	end if
	if lt_flag <> 0 then	
		lt_id = ds_assets.GetItemNumber(i, lt_flag)
		dgc_lt_id = lt_id
	end if
	if bv_flag <> 0 then	
		yyyy  = ds_assets.GetItemNumber(i, bv_flag)
		dgc_yyyy = yyyy
	end if
	if pu_flag <> 0 then	
		pu_id  = ds_assets.GetItemNumber(i, pu_flag)
		dgc_pu_id = pu_id
	end if
	if ru_flag <> 0 then	
		ru_id  = ds_assets.GetItemNumber(i, ru_flag)
		dgc_ru_id = ru_id
	end if
	if ccid_flag <> 0 and ccval_flag <> 0 then	
		cc_id  = ds_assets.GetItemNumber(i, ccid_flag)
		cc_val = ds_assets.getitemstring(i, ccval_flag)
		
		dgc_cc_id = cc_id
		dgc_cc_val = cc_val
	end if

	old_depr_group = ds_assets.GetItemNumber(i, 4)
	total_cost = ds_assets.GetItemNumber(i, idx)
	
	//Maint 29994:  use oracle package to determine depr_group_id
	
	//set variables to 0 if null.  the oracle package needs non-null values for the attributes listed below.	
	if isnull(dgc_gl_id) then dgc_gl_id = 0
	if isnull(dgc_al_id) then dgc_al_id = 0
	if isnull(dgc_ml_id) then dgc_ml_id = 0
	if isnull(dgc_lt_id) then dgc_lt_id = 0
	if isnull(dgc_sa_id) then dgc_sa_id = 0
	if isnull(dgc_sl_id) then dgc_sl_id = 0
	if isnull(dgc_cc_id) then dgc_cc_id = 0
	if isnull(dgc_pu_id) then dgc_pu_id = 0
	if isnull(dgc_ru_id) then dgc_ru_id = 0
	if isnull(dgc_yyyy) then dgc_yyyy = 0 
	
	SELECT pp_depr_pkg.f_find_depr_group
				 (:a_co_id, :dgc_gl_id, :dgc_ml_id, :a_ua_id, :a_bs_id,
				  :dgc_sa_id, :dgc_sl_id, :dgc_yyyy, :dgc_al_id, :dgc_lt_id, :dgc_ru_id, :dgc_pu_id, :dgc_cc_id, :dgc_cc_val)
	INTO :depr_group		  
	FROM dual;   
	
	if depr_group = -9 then
		dg_error_msg = "Depr Group is 'Invalid': "
	end if 
	if depr_group = -1 then
		dg_error_msg = "NO Depr Group found:"
	end if 
	
	if depr_group = -9 or depr_group =-1 then
		msg = "co(" + String(a_co_id) + "); bs(" + String(a_bs_id) + "); ua(" + String(a_ua_id) + ")"
		if not IsNull(gl_id) then msg = msg + "; gl(" + String(gl_id) + ")"
		if not IsNull(sa_id) then msg = msg + "; sa(" + String(sa_id) + ")"
		if not IsNull(lt_id) then msg = msg + "; lt(" + String(lt_id) + ")"
		if not IsNull(ml_id) then msg = msg + "; ml(" + String(ml_id) + ")"
		if not IsNull(al_id) then msg = msg + "; al(" + String(al_id) + ")"
		if not IsNull(sl_id) then msg = msg + "; sl(" + String(sl_id) + ")"
		if not IsNull(yyyy) then msg = msg + "; vint(" + String(yyyy) + ")"
		if not IsNull(pu_id) then msg = msg + "; pu(" + String(pu_id) + ")"
		if not IsNull(ru_id) then msg = msg + "; ru(" + String(ru_id) + ")"
		if not IsNull(cc_id) then msg = msg + "; ccid(" + String(cc_id) + ")"
		if not IsNull(cc_val) then msg = msg + "; ccval(" + cc_val + ")"

		f_status_box("Info", dg_error_msg + msg + " with total_cost=" + String(total_cost,"#,##0.00") )
		continue //next combo		
	end if
	
	first_cpr = false 
		
	//Update combo
	update_sqls = "update cpr_ledger cpr set depr_group_id = " + String(depr_group) &
					+ " where cpr.depr_group_id is null and cpr.company_id = " + String(a_co_id) &
					+ " and cpr.bus_segment_id =" + String(a_bs_id) &
					+ " and cpr.utility_account_id = " + String(a_ua_id)   
					
	if trim(a_cpr_filter) <> "" and not isNull(a_cpr_filter) then
		update_sqls = update_sqls + ' and ' + a_cpr_filter
	end if
					
	if not IsNull(gl_id) then update_sqls = update_sqls + " and cpr.gl_account_id = " + String(gl_id)
	if not IsNull(sa_id) then update_sqls = update_sqls + " and cpr.sub_account_id = " + String(sa_id)
	if not IsNull(sl_id) then update_sqls = update_sqls + " and cpr.subledger_indicator = " + String(sl_id)
	if not IsNull(al_id) then update_sqls = update_sqls + " and cpr.asset_location_id = " + String(al_id)
	if not IsNull(yyyy) then update_sqls = update_sqls + " and to_number(to_char(cpr.eng_in_service_year,'yyyy')) = " + String(yyyy)
	
	if not IsNull(ml_id) then 
		if isnull (al_id) then
			update_sqls = update_sqls + " and cpr.asset_location_id in (select asset_location_id " &
											  + " from asset_location where major_location_id = " + String(ml_id) + ")"
		end if
	end if
	if not IsNull(lt_id) then 
		if isnull(al_id) and isnull(ml_id) then
			update_sqls = update_sqls + " and cpr.asset_location_id in (select asset_location_id " &
											  + " from asset_location where major_location_id in (select Major_location_id " &
											  + " from major_location where location_type_id = " + String(lt_id) + "))"
		end if
	end if

	if not IsNull(ru_id) then update_sqls = update_sqls + " and cpr.retirement_unit_id = " + String(ru_id)
	if not IsNull(pu_id) then 
		if IsNull(ru_id) then
			update_sqls = update_sqls + " and cpr.retirement_unit_id in (select retirement_unit_id " &
											  + " from retirement_unit where property_unit_id = " + String(pu_id) + ")"
		end if
	end if
	if not IsNull(cc_id) and not isnull(cc_val) then
		update_sqls = update_sqls + " and exists ( select 1 from class_code_cpr_ledger cc " &
			+ " where cc.asset_id = cpr.asset_id and cc.class_code_id = " + string(cc_id) &
			+ " and cc.value = '" + cc_val + "' ) "
	end if
	
	
	execute immediate :update_sqls ;
	
	If sqlca.SQLCode <> 0 then 
		MessageBox("Error", "Error: Update cpr_ledger failed:~n~r" + sqlca.SQLErrText)
		rollback;
		destroy ds_assets
		return -1
	end if
next // combo

DESTROY ds_assets

return 1
end function

public function integer uf_build_depr_vintage_summary ();longlong i, k, m, o, rtn
string sqls, str_rtn, dg_where_clause
longlong company_ids[], depr_group_ids[], empty_lng_array[]
datetime gl_posting_mo_yrs[], empty_dt_array[], dt_rtn
boolean is_found
uo_ds_top ds_temp
ds_temp = create uo_ds_top

sqls = + &
"select b.company_id, a.gl_post_mo_yr, a.depr_group_id " + "~r~n" + &
"from depr_process_temp a, depr_group b " + "~r~n" + &
"where a.depr_group_id = b.depr_group_id " + "~r~n" + &
"order by company_id, gl_post_mo_yr, depr_group_id"

str_rtn = f_create_dynamic_ds(ds_temp, 'grid', sqls, sqlca, true)
if str_rtn <> "OK" then
	messagebox("Build Vintage Summary","Error Creating DS: ~r~nDetails:~r~n" + str_rtn + "~r~nSQLS:~r~n" + sqls, stopsign!)
	destroy ds_temp
	return -1
end if

m = 0
for i = 1 to ds_temp.rowCount()
	//// Get Company ID
	rtn = ds_temp.getitemNumber(i,1)
	//// For each item in company_ids check 
	//// if company is already there
	is_found = false
	for k = 1 to upperbound(company_ids) 
		if rtn = company_ids[k] then
			is_found = true
			exit
		end if
	next
	
	if not is_found then
		m++
		company_ids[m] = rtn
	end if
Next

///// For each company 
for i = 1 to upperbound(company_ids[])
	////Clear Filter
	ds_temp.setFilter("")
	ds_temp.filter()
	////Filter to current company 
	ds_temp.setFilter("company_id=" + string(company_ids[i]))
	ds_temp.filter()
	
	////
	//// Get the Months to Process
	////
	////Clear out months
	gl_posting_mo_yrs[] = empty_dt_array[]	
	////Loop through months to process	
	for k = 1 to ds_temp.rowCount()		
		////Get Gl Post Mo Yr
		dt_rtn = ds_temp.getItemDateTime(k,2)
		is_found = false
		for m = 1 to upperbound(gl_posting_mo_yrs)
			if dt_rtn = gl_posting_mo_yrs[m] then
				is_found = true
				exit
			end if
		next		
		if not is_Found then
			o++
			gl_posting_mo_yrs[o] = dt_rtn
		end if
	Next
	
	////Clear out Depr Group Array
	depr_group_ids[] = empty_lng_array[]
	////
	////For each month in array get Depr Groups
	////
	for k = 1 to upperbound(gl_posting_mo_yrs)
		////Clear Filter
		ds_temp.setFilter("")
		ds_temp.filter()
		////Filter to current company 
		ds_temp.setFilter("company_id=" + string(company_ids[i]) + " and gl_post_mo_yr=" + string(gl_posting_mo_yrs[k]) )
		ds_temp.filter()
		for m = 1 to ds_temp.rowcount()
			rtn = ds_temp.getItemNumber(m,3)
			depr_group_ids[m] = rtn
		next 
		
		str_rtn = " and " + f_parsearrayby254(depr_group_ids, "N", "depr_group.depr_group_id")
		rtn = f_build_depr_Vintage_summary(gl_posting_mo_yrs[k], company_ids[i], str_rtn)
		if rtn <> 1 then
			messageBox("Build Vintage Summary","Error Building depr_vintage_summary", stopsign!)
			return -1
		end if
	next
Next


return 1
end function

public function string uf_allo_reserve_co (longlong a_company, datetime a_month);string month_string, company_string, original_sql, sqls, template, modret, subledger, replacestring, rwip, loop_allo
longlong factor_count, rc, i, empty_array[], rows, subl, curs, tablepos, ret, null_array[], min_id, max_id, ii, dg_array[]
decimal{8} factor

uo_ds_top ds_depr_res_allo_factors, ds_combined_depr_res_allo_factors, ds_allo_depr_reserve, ds_allo_depr_reserve_theo, &
			 ds_allo_subl_reserve, ds_hide, ds_dg_groups
			 
ds_depr_res_allo_factors = CREATE uo_ds_top
ds_combined_depr_res_allo_factors = CREATE uo_ds_top
ds_allo_depr_reserve = CREATE uo_ds_top
ds_allo_depr_reserve_theo = CREATE uo_ds_top
ds_allo_subl_reserve = CREATE uo_ds_top
ds_hide = CREATE uo_ds_top
ds_dg_groups = CREATE uo_ds_top

month_string =  String(a_month, "MM/YYYY")

f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Deleting  reserve allo factor for " + string(month_string))

delete from depr_res_allo_factors where to_char(month,'mm/yyyy') = :month_string
and depr_group_id in (
select depr_group_id from depr_group where company_id = :a_company);

if f_check_sql_error(sqlca, "Cannot Delete depr_res_allo_factors") = 1 then
	goto next_company
end if

SetNull(rwip)
rwip = Upper(f_pp_system_control_company('Include RWIP in Allocation Factors', a_company))

if IsNull(rwip) then rwip = 'NO'

if rwip = 'YES' then f_status_box("Information", String(Now(),"hh:mm:ss") + ": Including RWIP in Reserve Allocation Factors.")

ds_depr_res_allo_factors.DataObject = "dw_depr_res_allo_factors"
ds_depr_res_allo_factors.setTransObject(sqlca)

ds_combined_depr_res_allo_factors.DataObject = "dw_combined_depr_res_allo_factors"
ds_combined_depr_res_allo_factors.setTransObject(sqlca)

if rwip = 'YES' then
	ds_allo_depr_reserve.DataObject = "dw_allo_depr_resrwip"
else
	ds_allo_depr_reserve.DataObject = "dw_allo_depr_reserve"
end if
ds_allo_depr_reserve.setTransObject(sqlca)

if rwip = 'YES' then
	ds_allo_depr_reserve_theo.DataObject = "dw_allo_depr_resrwip_theo"
else
	ds_allo_depr_reserve_theo.DataObject = "dw_allo_depr_reserve_theo"
end if
ds_allo_depr_reserve_theo.setTransObject(sqlca)

ds_allo_subl_reserve.DataObject = "dw_allo_subl_reserve"
ds_allo_subl_reserve.setTransObject(sqlca)

ds_depr_res_allo_factors.Reset()
f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Loading new allo factors...")

//Copied here from if rc >= 2
ds_depr_res_allo_factors.SetTransObject(sqlca)
ds_allo_depr_reserve.Settransobject(sqlca)
ds_allo_depr_reserve_theo.Settransobject(sqlca)			

rc = 7 // do all allocations (4 + 2 + 1)
if Upper(f_pp_system_control_company("Custom Reserve Allocation", a_company)) = "YES" then

	rc = f_allo_reserve_custom(ds_allo_depr_reserve, a_company, a_month)
	if rc < 0 then
		// error
	end if
	
	// Call Dynamic Validation option
	string args[]
	args[1] = string(a_month)
	args[2] = string(a_company)
	args[3] = ds_allo_depr_reserve.Describe("datawindow.table.select")
	
	ret = f_wo_validation_control(1008,args)
	
	if ret < 0 then
		rollback;
		f_status_box("Information","(Dynamic Validations) Call for f_allo_reserve_custom Failed.")
		// Don't exit since the call to the custom function doesn't do anything in the event of an error
	end if
	
	factor_count = ds_allo_depr_reserve.rowcount()
	for i = 1 to factor_count
		 ds_depr_res_allo_factors.insertrow(1)
	
		 ds_depr_res_allo_factors.setitem(1,'set_of_books_id', &
				  ds_allo_depr_reserve.getitemnumber(i,'set_of_books_id'))
	
		 ds_depr_res_allo_factors.setitem(1,'depr_group_id', &
				  ds_allo_depr_reserve.getitemnumber(i,'depr_group_id'))
	
		 ds_depr_res_allo_factors.setitem(1,'vintage', &
				  ds_allo_depr_reserve.getitemnumber(i,'vintage'))
	
		 ds_depr_res_allo_factors.setitem(1,'month', &
				  ds_allo_depr_reserve.getitemdatetime(i,'curr_month'))
	
		 factor = Round(ds_allo_depr_reserve.getitemnumber(i,'factor'), 8)
		 if isnull(factor) then factor = 0
		 ds_depr_res_allo_factors.setitem(1,'factor', factor)
		 ds_depr_res_allo_factors.setitem(1,'theo_factor',factor)
		 
		 ds_depr_res_allo_factors.setitem(1,'life_factor',&
				 ds_allo_depr_reserve.getitemnumber(i,'life_factor'))
	
		 ds_depr_res_allo_factors.setitem(1,'cor_factor',&
				 ds_allo_depr_reserve.getitemnumber(i,'cor_factor'))
	
	
	next

end if

if rc >= 4 then
	// Theoretical factors
	f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Calculating allo factors - Step 1...")
	
	//Performance enhancement. PP-48299
		
	loop_allo = f_pp_system_control_company('CPR CLOSE MONTH LOOP ALLO RES', a_company)
		
	if loop_allo  = '' or loop_allo = '0' or isnull(loop_allo) then	
			uf_allo_reserve_theo(ds_allo_depr_reserve_theo, empty_array)
			min_id = 1
			max_id = 1
		else
			select min(id) min_mod, max(id) max_mod
				into :min_id, :max_id
				from 
					(	select depr_group_id, mod(depr_group_id, :loop_allo) id
						from depr_group
						where company_id = :a_company
						and subledger_type_id = 0
					);
					
			if	f_check_sql_error(sqlca, "Error: Determining Theoretical Allo Process Batches") = 1 then
				goto next_company
			end if
			
			if rwip = 'YES' then
				ds_allo_depr_reserve_theo.DataObject = "dw_allo_depr_resrwip_theo_list"
			else
				ds_allo_depr_reserve_theo.DataObject = "dw_allo_depr_reserve_theo_list"
			end if
	
		ds_allo_depr_reserve_theo.Settransobject(sqlca)
	
	end if

	for ii = min_id to max_id
				
		if min_id = 1 and max_id = 1 then
			/// Min ID = Max ID = 1 when not looping
		else	
			f_status_box("Information","Theoretical Allo Factors Batching... " + string(ii + 1) + " of " + string(max_id + 1))
			//CAT/RAH: added filters.
			sqls = 'select depr_group_id from depr_group where mod(depr_group_id, ' + string(loop_allo) + ' ) = ' + string(ii) + ' and company_id = ' + string(a_company) + ' and subledger_type_id = 0' 
			f_create_dynamic_ds(ds_dg_groups, 'grid', sqls, sqlca, true)
			
			if sqlca.SQLCode < 0 then
				f_status_box("Information","Error - Building Theoretical Allo Factors Batch #" + String(ii+1) + "... " +  sqlca.SQLErrText )
				rollback;
				return "next company"
			end if
			
			dg_array = ds_dg_groups.object.depr_group_id.primary
			uf_allo_reserve_theo(ds_allo_depr_reserve_theo, dg_array)
		end if
	
		factor_count += ds_allo_depr_reserve_theo.rowcount()
		
		for i = 1 to ds_allo_depr_reserve_theo.rowcount()	 
			 ds_depr_res_allo_factors.insertrow(1)
		
			 ds_depr_res_allo_factors.setitem(1,'set_of_books_id', &
					  ds_allo_depr_reserve_theo.getitemnumber(i,'set_of_books_id'))
		
			 ds_depr_res_allo_factors.setitem(1,'depr_group_id', &
					  ds_allo_depr_reserve_theo.getitemnumber(i,'depr_group_id'))
		
			 ds_depr_res_allo_factors.setitem(1,'vintage', &
					  ds_allo_depr_reserve_theo.getitemnumber(i,'vintage'))
		
			 ds_depr_res_allo_factors.setitem(1,'month', &
					  ds_allo_depr_reserve_theo.getitemdatetime(i,'curr_month'))
		
			 factor = Round(ds_allo_depr_reserve_theo.getitemnumber(i,'factor'), 8)
	
			 ds_depr_res_allo_factors.setitem(1,'factor', factor)
	
			 ds_depr_res_allo_factors.setitem(1,'theo_factor',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'theo_factor'))
		
			 ds_depr_res_allo_factors.setitem(1,'remaining_life',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'current_rem_life'))
		
			 ds_depr_res_allo_factors.setitem(1,'life_factor',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'life_factor'))
		
			 ds_depr_res_allo_factors.setitem(1,'cor_factor',&
					 ds_allo_depr_reserve_theo.getitemnumber(i,'cor_factor'))
		
		next
	next


	rc -= 4
end if
if rc >= 2 then
	// Dollar Age factors
	f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Calculating allo factors - Step 2...")
	
	ds_depr_res_allo_factors.SetTransObject(sqlca)
	if rwip = 'YES' then
		ds_allo_depr_reserve.DataObject = "dw_allo_depr_resrwip"
	else
		ds_allo_depr_reserve.DataObject = "dw_allo_depr_reserve"
	end if
	ds_allo_depr_reserve.Settransobject(sqlca)
	ds_allo_depr_reserve_theo.Settransobject(sqlca)
	
	company_string = " and  company_id = "  + string(a_company)
	
	f_add_where_clause_ds(ds_allo_depr_reserve,"",company_string,false)
	
	uf_allo_reserve(ds_allo_depr_reserve)

	factor_count += ds_allo_depr_reserve.rowcount()
	
	for i = 1 to ds_allo_depr_reserve.rowcount()		 
		 ds_depr_res_allo_factors.insertrow(1)
	   
		 ds_depr_res_allo_factors.setitem(1,'set_of_books_id', &
				  ds_allo_depr_reserve.getitemnumber(i,'set_of_books_id'))
	
		 ds_depr_res_allo_factors.setitem(1,'depr_group_id', &
				  ds_allo_depr_reserve.getitemnumber(i,'depr_group_id'))
	
		 ds_depr_res_allo_factors.setitem(1,'vintage', &
				  ds_allo_depr_reserve.getitemnumber(i,'vintage'))
	
		 ds_depr_res_allo_factors.setitem(1,'month', &
				  ds_allo_depr_reserve.getitemdatetime(i,'curr_month'))
	
		 factor = Round(ds_allo_depr_reserve.getitemnumber(i,'factor'), 8)
		 if isnull(factor) then factor = 0
		 ds_depr_res_allo_factors.setitem(1,'factor', factor)
		 ds_depr_res_allo_factors.setitem(1,'theo_factor',factor)
		 
		 ds_depr_res_allo_factors.setitem(1,'life_factor',&
				 ds_allo_depr_reserve.getitemnumber(i,'life_factor'))
	
		 ds_depr_res_allo_factors.setitem(1,'cor_factor',&
				 ds_allo_depr_reserve.getitemnumber(i,'cor_factor'))
	
	next

	rc -= 2 
end if

if rc >= 1 then
	// replace DEPR_SUBLEDGER_TEMPLATE is dw
	// Subledger factors
	f_status_box("Information","Information: " + String(Now(), "hh:mm:ss")+" : Calculating allo factors - Step 3...")
	original_sql = ds_allo_subl_reserve.Describe("DataWindow.Table.Select")
	
	sqls = "select subledger_name from subledger_control where depreciation_indicator = 1 "
	f_create_dynamic_ds(ds_hide,'grid',sqls,sqlca,true)
	rows = ds_hide.rowcount()
	
	for subl = 1 to rows
	
		 subledger = upper(trim(ds_hide.getitemstring(subl,1)))

		 f_status_box("Information","Information: " + String(Now(), "hh:mm:ss")+" : "+subledger + " Subledger...")

		 sqls = original_sql

		//Find tablename throughout SQL and replace

		template = 'DEPR_SUBLEDGER_TEMPLATE'
		replacestring = upper(subledger+'_DEPR')

		curs = 1
		tablepos = pos(sqls, template, curs)
		do until tablepos = 0
			sqls = replace(sqls, tablepos, 23, replacestring)		
			curs = curs + len(replacestring)
			tablepos = pos(sqls, template, curs)
		loop

		//Prepare SQL for Retrieve
		sqls = 'datawindow.table.select = "' + sqls  +  '"'
		modret = ds_allo_subl_reserve.Modify(sqls)
		
		if modret = "" then

			ds_allo_subl_reserve.settransobject(sqlca)
	
			uf_allo_reserve_subl(ds_allo_subl_reserve)
		
			factor_count += ds_allo_subl_reserve.rowcount()
			
			for i = 1 to ds_allo_subl_reserve.rowcount()
		 
				 ds_depr_res_allo_factors.insertrow(1)
			
				 ds_depr_res_allo_factors.setitem(1,'set_of_books_id', &
						  ds_allo_subl_reserve.getitemnumber(i,'set_of_books_id'))
			
				 ds_depr_res_allo_factors.setitem(1,'depr_group_id', &
						  ds_allo_subl_reserve.getitemnumber(i,'depr_group_id'))
			
				 ds_depr_res_allo_factors.setitem(1,'vintage', &
						  ds_allo_subl_reserve.getitemnumber(i,'vintage'))
			
				 ds_depr_res_allo_factors.setitem(1,'month', &
						  ds_allo_subl_reserve.getitemdatetime(i,'curr_month'))
			
				 factor = Round(ds_allo_subl_reserve.getitemnumber(i,'factor'), 8)
				 
				 if isnull(factor) then factor = 0
				 
				 ds_depr_res_allo_factors.setitem(1,'factor', factor)
				 ds_depr_res_allo_factors.setitem(1,'theo_factor', factor)
				 ds_depr_res_allo_factors.setitem(1,'life_factor', factor)
				 ds_depr_res_allo_factors.setitem(1,'cor_factor', 0)
			
			next
		else
			//modify error
			f_status_box("Information","Error setting up dw_allo_subl_reserve for " + subledger +":~n" + modret)
			rollback;
			goto next_company	
		end if
		sqls = 'datawindow.table.select = "' + original_sql + '"'
		modret = ds_allo_subl_reserve.Modify(sqls)
		ds_allo_subl_reserve.Reset()
	next // subledger
	
	//Update Depr_res_allo_factors for CPR DEPR Groups
	insert into depr_res_allo_factors 
	(set_of_books_id, depr_group_id, vintage, month, factor, theo_factor, remaining_life,
	 life_factor, cor_factor)
	(select a.set_of_books_id, b.depr_group_id, 
	to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :a_month, 
	decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 
	decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)),
	avg(remaining_life / decode(init_life, 0, 1, init_life)),
	decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 0
	from cpr_depr a, cpr_ledger b	
	where a.asset_id = b.asset_id
	and a.gl_posting_mo_yr = :a_month
	and a.company_id = :a_company
	group by a.set_of_books_id, b.depr_group_id, to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :a_month)
	;

	if	f_check_sql_error(sqlca, "Error Updating Depr Res Allo Factors for CPR DEPR") = 1 then
		goto next_company
	end if
	
	factor_count += sqlca.sqlnrows

end if

f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Update Allocation factors ...") 

ret = ds_depr_res_allo_factors.update()

if ret <> 1 then

	rollback using sqlca;
	f_status_box("Information","Information: Error updating Depreciation Reserve Allocation Factors.")
	goto next_company	
end if
	

f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Deleting combined reserve allo factor for " + string(month_string))

delete from combined_depr_res_allo_factors 
where month = :a_month 
and combined_depr_group_id in (select combined_depr_group_id from depr_group where
	company_id = :a_company);
 
if f_check_sql_error(sqlca,"Error trying to delete Combined Depr Res Allo Factors table. ") <> -1 then 
	goto next_company
end if
	
ds_combined_depr_res_allo_factors.SetTransObject(sqlca)

//resets datawindow (in case already run)
ds_allo_depr_reserve.Reset()

if rwip = 'YES' then
	ds_allo_depr_reserve.DATAOBJECT  = 'dw_allo_combined_depr_resrwip'
else
	ds_allo_depr_reserve.DATAOBJECT  = 'dw_allo_combined_depr_reserve'
end if

ds_allo_depr_reserve.Settransobject(sqlca)
ds_allo_depr_reserve_theo.Settransobject(sqlca)

if rwip = 'YES' then
	ds_allo_depr_reserve_theo.DataObject = "dw_allo_combined_depr_resrwip_theo"
else
	ds_allo_depr_reserve_theo.DataObject = "dw_allo_combined_depr_reserve_theo"
end if

// Theoretical factors
f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Calculating combined allo factors - Step 1...")

ds_allo_depr_reserve_theo.SetTransObject(sqlca)

uf_allo_combined_reserve_theo(ds_allo_depr_reserve_theo)

rows = ds_allo_depr_reserve_theo.rowcount()

for i = 1 to rows
	 
	 ds_combined_depr_res_allo_factors.insertrow(1)

	 ds_combined_depr_res_allo_factors.setitem(1,'set_of_books_id', &
			  ds_allo_depr_reserve_theo.getitemnumber(i,'set_of_books_id'))

	 ds_combined_depr_res_allo_factors.setitem(1,'combined_depr_group_id', &
			  ds_allo_depr_reserve_theo.getitemnumber(i,'combined_depr_group_id'))

	 ds_combined_depr_res_allo_factors.setitem(1,'vintage', &
			  ds_allo_depr_reserve_theo.getitemnumber(i,'vintage'))

	 ds_combined_depr_res_allo_factors.setitem(1,'month', &
			  ds_allo_depr_reserve_theo.getitemdatetime(i,'accounting_month'))

	 factor = ds_allo_depr_reserve_theo.getitemnumber(i,'factor')
	 if isnull(factor) then factor = 0
	 ds_combined_depr_res_allo_factors.setitem(1,'factor', factor)

	 ds_combined_depr_res_allo_factors.setitem(1,'theo_factor',&
			 ds_allo_depr_reserve_theo.getitemnumber(i,'theo_factor'))

	 ds_combined_depr_res_allo_factors.setitem(1,'remaining_life',&
			 ds_allo_depr_reserve_theo.getitemnumber(i,'current_rem_life'))

	 ds_combined_depr_res_allo_factors.setitem(1,'life_factor',&
			 ds_allo_depr_reserve_theo.getitemnumber(i,'life_factor'))

	 ds_combined_depr_res_allo_factors.setitem(1,'cor_factor',&
			 ds_allo_depr_reserve_theo.getitemnumber(i,'cor_factor'))

next

// Dollar Age factors
f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Calculating combined allo factors - Step 2...")
uf_allo_combined_reserve(ds_allo_depr_reserve)

rows = ds_allo_depr_reserve.rowcount()
for i = 1 to rows
	 
	ds_combined_depr_res_allo_factors.insertrow(1)

	ds_combined_depr_res_allo_factors.setitem(1,'set_of_books_id', &
			  ds_allo_depr_reserve.getitemnumber(i,'set_of_books_id'))

	ds_combined_depr_res_allo_factors.setitem(1,'combined_depr_group_id', &
			  ds_allo_depr_reserve.getitemnumber(i,'combined_depr_group_id'))

	ds_combined_depr_res_allo_factors.setitem(1,'vintage', &
			  ds_allo_depr_reserve.getitemnumber(i,'vintage'))

	ds_combined_depr_res_allo_factors.setitem(1,'month', &
			  ds_allo_depr_reserve.getitemdatetime(i,'accounting_month'))

	factor = ds_allo_depr_reserve.getitemnumber(i,'factor')
	if isnull(factor) then 
		factor = 0
	end if
	ds_combined_depr_res_allo_factors.setitem(1,'factor', factor)
	ds_combined_depr_res_allo_factors.setitem(1,'theo_factor',factor)
			
	ds_combined_depr_res_allo_factors.setitem(1,'life_factor',&
			 ds_allo_depr_reserve.getitemnumber(i,'life_factor'))

	ds_combined_depr_res_allo_factors.setitem(1,'cor_factor',&
			 ds_allo_depr_reserve.getitemnumber(i,'cor_factor'))

next


//Update Depr_res_allo_factors for CPR DEPR Groups
insert into combined_depr_res_allo_factors 
(set_of_books_id, combined_depr_group_id, vintage, month, factor, theo_factor, remaining_life,
 life_factor, cor_factor)
(select a.set_of_books_id, d.combined_depr_group_id, 
to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :a_month, 
decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 
decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)),
avg(remaining_life / decode(init_life, 0, 1, init_life)),
decode(sum(a.asset_dollars),0,0,sum(a.depr_reserve)/sum(a.asset_dollars)), 0
from cpr_depr a, cpr_ledger b, depr_group d
where a.asset_id = b.asset_id
and a.gl_posting_mo_yr = :a_month
and a.company_id = :a_company
and b.depr_group_id = d.depr_group_id
and d.subledger_Type_id <> 0
and d.combined_depr_group_id is not null
group by a.set_of_books_id, d.combined_depr_group_id, to_number(to_char(b.ENG_IN_SERVICE_YEAR, 'YYYY')), :a_month)
;

if	f_check_sql_error(sqlca, "Error Update Combined Depr Res Allo Factors for CPR DEPR") = 1 then
	f_status_box("Information","Error Update Combined Depr Res Allo Factors for CPR DEPR. SQLInfo: " + sqlca.sqlerrtext)
	rollback using sqlca;
	goto next_company
end if
	
f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Update Combined Allocation factors ...") 

ret = ds_combined_depr_res_allo_factors.update()

if ret <> 1 then

	f_status_box("Information","Error updating Combined Depreciation Reserve Allocation Factors. SQLInfo: " + sqlca.sqlerrtext)
	rollback using sqlca;
	goto next_company

end if

f_status_box("Information","Information: " + String(Now(), "hh:mm:ss") + ": Pushing combined reserve allocation down to member groups. Step 1")
DELETE FROM depr_res_allo_factors
WHERE month = :a_month 
	AND (depr_group_id, set_of_books_id) IN
	(
		SELECT dg.depr_group_id, dcgb.set_of_books_id
		FROM depr_combined_group_books dcgb, depr_group dg
		WHERE dcgb.combined_depr_group_id = dg.combined_depr_group_id
			AND dg.company_id = :a_company 
			AND NVL(dcgb.combined_reserve_allocation, 0) = 1
	);
if	f_check_sql_error(sqlca, "Error updating reserve allocation on member groups using combined reserve allocation. (Delete from depr_res_allo_factors).") = 1 then
	f_status_box("Information","Error updating reserve allocation on member groups using combined reserve allocation. (Delete from depr_res_allo_factors). SQLInfo: " + sqlca.sqlerrtext)
	rollback using sqlca;
	goto next_company
end if

f_status_box("Information","Information: " + String(Now(), "hh:mm:ss") + ": Pushing combined reserve allocation down to member groups. Step 2")
INSERT INTO depr_res_allo_factors (set_of_books_id, depr_group_id, vintage, month, factor, 
	theo_factor, remaining_life, life_factor, cor_factor)
SELECT dcgb.set_of_books_id, dg.depr_group_id, cdraf.vintage, cdraf.month, cdraf.factor,
	cdraf.theo_factor, cdraf.remaining_life, cdraf.life_factor, cdraf.cor_factor
FROM depr_combined_group_books dcgb, depr_group dg, combined_depr_res_allo_factors cdraf
	WHERE dcgb.combined_depr_group_id = dg.combined_depr_group_id
		AND dcgb.combined_depr_group_id = cdraf.combined_depr_group_id
		AND dcgb.set_of_books_id = cdraf.set_of_books_id
		AND dg.company_id = :a_company
		AND NVL(dcgb.combined_reserve_allocation, 0) = 1
		AND cdraf.month = :a_month
;
if	f_check_sql_error(sqlca, "Error updating reserve allocation on member groups using combined reserve allocation. (insert into depr_res_allo_factors).") = 1 then
	f_status_box("Information","Error updating reserve allocation on member groups using combined reserve allocation. (insert into depr_res_allo_factors). SQLInfo: " + sqlca.sqlerrtext)
	rollback using sqlca;
	goto next_company
end if


DESTROY ds_depr_res_allo_factors
DESTROY ds_combined_depr_res_allo_factors
DESTROY ds_allo_depr_reserve
DESTROY ds_allo_depr_reserve_theo
DESTROY ds_allo_subl_reserve
destroy ds_dg_groups

return String(factor_count)

next_company:
DESTROY ds_depr_res_allo_factors
DESTROY ds_combined_depr_res_allo_factors
DESTROY ds_allo_depr_reserve
DESTROY ds_allo_depr_reserve_theo
DESTROY ds_allo_subl_reserve
destroy ds_dg_groups

return "next company"

end function

public function integer uf_allo_reserve_subl (uo_ds_top a_ds);// retrieve groups for theo allocation
f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Processing subledger reserve allocation...")
a_ds.retrieve()
a_ds.GroupCalc()
a_ds.GroupCalc()
f_status_box("Information","Information: " + String(Now(),"hh:mm:ss") + ": Finished subledger reserve allocation.")
return 1
end function

on uo_depr_processing.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_depr_processing.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

