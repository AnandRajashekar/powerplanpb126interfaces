HA$PBExportHeader$uo_depr_activity_post.sru
$PBExportComments$34663
forward
global type uo_depr_activity_post from nonvisualobject
end type
end forward

global type uo_depr_activity_post from nonvisualobject
end type
global uo_depr_activity_post uo_depr_activity_post

type variables
uo_ds_top i_ds_depr_activity, i_ds_depr_groups, i_ds_depr_trans_set
boolean		i_reverse_recurring, i_prior_period = false, i_blending
dec {2}     i_reserve_recurring_amount 
string i_error_msg
longlong i_depr_trans_set_id
end variables

forward prototypes
public function integer uf_subl_depr_post (longlong row_num)
public function integer uf_depr_activity_post (longlong row_num)
public function longlong uf_depr_activity_recur (longlong row_num)
public function integer uf_depr_recur_post (longlong row_num)
public function integer uf_cpr_depr_post (longlong row_num)
public function integer uf_depr_post ()
public function longlong uf_gl_transaction_post (longlong a_row)
public function longlong uf_gl_transaction_post (longlong a_row, string a_source, longlong a_je_method)
public function integer uf_depr_ledger_post (longlong a_row_num)
public function longlong uf_depr_trans_set_post (longlong a_depr_trans_set_id, boolean a_prior_period)
public function longlong uf_depr_activity_status_update (boolean a_all)
public function longlong uf_update_prior_period_tables ()
public function integer uf_depr_ledger_blending_post (longlong a_row_num)
public function boolean uf_check_blending (longlong a_row_num)
end prototypes

public function integer uf_subl_depr_post (longlong row_num);longlong dg_id, sob_id, subledger_item_id, asset_id
decimal amount
string act_code, subledger_name, sqls
datetime moyr

f_status_box("Information", "    Updating cpr_depr...")

dg_id = i_ds_depr_activity.getitemnumber(row_num,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(row_num,'set_of_books_id')
amount = i_ds_depr_activity.getitemdecimal(row_num,'amount')
act_code = i_ds_depr_activity.getitemstring(row_num,'description')
moyr = i_ds_depr_activity.getitemDateTime(row_num,'gl_post_mo_yr')
asset_id = i_ds_depr_activity.getItemNumber(row_num,'asset_id')
subledger_item_id = i_ds_depr_activity.getItemNumber(row_num,'subledger_item_id')
i_error_msg = ''

if sob_id <> 1 then return 0

select subledger_name into :subledger_name 
from subledger_control sc, cpr_ledger cl
where subledger_type_id = subledger_indicator
and asset_id = :asset_id;

//Send to CPR_DEPR
	CHOOSE CASE act_code
		CASE 'COST_OF_REMOVAL'
			sqls = "update " + subledger_name + "_depr " &
				  + " set cost_of_removal = cost_of_removal + " + String(amount) &
				  + " where asset_id = " + string(asset_id) &
				  + " and cpr_posting_mo_yr = to_date(" + string(moyr,'yyyymm') + ",'yyyymm')" &
				  + " and subledger_item_id = " + String(subledger_item_id )
			execute immediate :sqls;
		CASE 'SALVAGE_CASH', 'SALVAGE_RETURNS'
			sqls = "update " + subledger_name + "_depr " &
				  + " set salvage_dollars = salvage_dollars + " + String(amount) &
				  + " where asset_id = " + string(asset_id) &
				  + " and cpr_posting_mo_yr = to_date(" + string(moyr,'yyyymm') + ",'yyyymm')" &
				  + " and subledger_item_id = " + String(subledger_item_id )
			execute immediate :sqls; 
		CASE 'GAIN_LOSS'
			sqls = "update " + subledger_name + "_depr " &
				  + " set gain_loss = gain_loss + " + String(amount) &
				  + " where asset_id = " + string(asset_id) &
				  + " and cpr_posting_mo_yr = to_date(" + string(moyr,'yyyymm') + ",'yyyymm')" &
				  + " and subledger_item_id = " + String(subledger_item_id )
			execute immediate :sqls; 
		CASE 'RESERVE_ADJUSTMENT'
			sqls = "update " + subledger_name + "_depr " &
				  + " set other_credits_and_adjust = other_credits_and_adjust + " + String(amount) &
				  + " where asset_id = " + string(asset_id) &
				  + " and cpr_posting_mo_yr = to_date(" + string(moyr,'yyyymm') + ",'yyyymm')" &
				  + " and subledger_item_id = " + String(subledger_item_id )
			execute immediate :sqls; 
		CASE 'SALVAGE_EXP_ADJUST'
			//invalid
			return -2
		case else
			return -2
	END CHOOSE

if sqlca.sqlcode = -1 then	
	i_error_msg = sqlca.sqlerrtext
	rollback;
	return -1
end if

if sqlca.sqlnrows = 0 then
	 i_error_msg =  'No data found in subledger_depr table for this activity'
	rollback;
	return -3
end if	
	
return 1
end function

public function integer uf_depr_activity_post (longlong row_num);longlong activity_id 

i_error_msg = ''
activity_id = i_ds_depr_activity.getitemnumber(row_num,'depr_activity_id')	

/* ### AJS: 22-OCT-2010: maint 5706: copy USER_ID1 */

insert into depr_activity
(DEPR_ACTIVITY_ID, DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, user_id, USER_ID1,
USER_ID2, RELATED_ACTIVITY_ID, DEPR_ACTIVITY_CODE_ID,	AMOUNT, DESCRIPTION, LONG_DESCRIPTION,
GL_JE_CODE,	GL_POST_FLAG,CREDIT_ACCOUNT,DEBIT_ACCOUNT, POST_PERIOD_MONTHS, ASSET_ID,
SUBLEDGER_ITEM_ID, depr_trans_set_id, depr_trans_set_activity_id, current_mo_yr)
(Select DEPR_ACTIVITY_ID, DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, user_id, USER_ID1,
USER_ID2, RELATED_ACTIVITY_ID, DEPR_ACTIVITY_CODE_ID,	AMOUNT, DESCRIPTION, LONG_DESCRIPTION,
GL_JE_CODE,	GL_POST_FLAG,CREDIT_ACCOUNT,DEBIT_ACCOUNT, POST_PERIOD_MONTHS, ASSET_ID,
SUBLEDGER_ITEM_ID , depr_trans_set_id, depr_trans_set_activity_id, current_mo_yr
from pend_depr_activity
where depr_activity_id = :activity_id);
										
if sqlca.sqlcode = -1 then 
	i_error_msg = sqlca.sqlerrtext
	rollback;
	return -1
end if	

return 1
end function

public function longlong uf_depr_activity_recur (longlong row_num);longlong activity_id 
datetime moyr

activity_id = i_ds_depr_activity.getitemnumber(row_num,'recurring_id')
moyr = i_ds_depr_activity.getitemDateTime(row_num,'gl_post_mo_yr')

/* ### AJS: 22-OCT-2010: maint 5706: copy USER_ID1 */

insert into depr_activity
(DEPR_ACTIVITY_ID, DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, user_id, USER_ID1,
USER_ID2, RELATED_ACTIVITY_ID, DEPR_ACTIVITY_CODE_ID,	AMOUNT, DESCRIPTION, LONG_DESCRIPTION,
GL_JE_CODE,	GL_POST_FLAG,CREDIT_ACCOUNT,DEBIT_ACCOUNT, POST_PERIOD_MONTHS, ASSET_ID, SUBLEDGER_ITEM_ID)
(Select pwrplant1.nextval, DEPR_GROUP_ID, SET_OF_BOOKS_ID, :moyr, user_id, USER_ID1,
USER_ID2, RELATED_ACTIVITY_ID, DEPR_ACTIVITY_CODE_ID,	AMOUNT, DESCRIPTION, LONG_DESCRIPTION,
GL_JE_CODE,	GL_POST_FLAG,CREDIT_ACCOUNT,DEBIT_ACCOUNT, POST_PERIOD_MONTHS, ASSET_ID, SUBLEDGER_ITEM_ID
from depr_activity_recurring
where recurring_id = :activity_id);
										
if sqlca.sqlcode = -1 then
	rollback;
	return -1
end if	
	
return 1
end function

public function integer uf_depr_recur_post (longlong row_num);datetime enddate
longlong months,activity_id, overwrite_check

f_status_box("Information", "    Updating depr_activity_recurring...")

	enddate = i_ds_depr_activity.getitemdatetime(row_num,'gl_post_mo_yr_end')
	months = i_ds_depr_activity.getitemnumber(row_num,'post_period_months')
	activity_id = i_ds_depr_activity.getitemnumber(row_num,'depr_activity_id')
	
	select count(*) into :overwrite_check from depr_activity_recurring
	where recurring_id = :activity_id;
	
	if overwrite_check > 0 then
		//Already exists so overwrite!
		delete from depr_activity_recurring
		where recurring_id = :activity_id;
		if sqlca.sqlcode = -1 then
			rollback;
			return -1
		end if
	end if

	/* ### AJS: 22-OCT-2010: maint 5706: copy USER_ID1 */

	insert into depr_activity_recurring
	(RECURRING_ID, DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, GL_POST_MO_YR_END, POST_PERIOD_MONTHS,USER_ID, USER_ID1,
	USER_ID2, RELATED_ACTIVITY_ID, DEPR_ACTIVITY_CODE_ID,	AMOUNT, DESCRIPTION, LONG_DESCRIPTION,
	GL_JE_CODE,	GL_POST_FLAG,CREDIT_ACCOUNT,DEBIT_ACCOUNT, STATUS_CODE_ID, ASSET_ID, SUBLEDGER_ITEM_ID)
	(Select DEPR_ACTIVITY_ID, DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, :enddate, :months, USER_ID, USER_ID1,
	USER_ID2, RELATED_ACTIVITY_ID, DEPR_ACTIVITY_CODE_ID,	AMOUNT, DESCRIPTION, LONG_DESCRIPTION,
	GL_JE_CODE,	GL_POST_FLAG,CREDIT_ACCOUNT,DEBIT_ACCOUNT, 1, ASSET_ID, SUBLEDGER_ITEM_ID 
	from pend_depr_activity
	where depr_activity_id = :activity_id);
	
	if sqlca.sqlcode = -1 then
		rollback;
		return -1
	end if
return 1

end function

public function integer uf_cpr_depr_post (longlong row_num);longlong dg_id, sob_id, asset_id
decimal amount
string act_code, subledger_name, sqls
datetime moyr

f_status_box("Information", "    Updating cpr_depr...")

dg_id = i_ds_depr_activity.getitemnumber(row_num,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(row_num,'set_of_books_id')
amount = i_ds_depr_activity.getitemdecimal(row_num,'amount')
act_code = i_ds_depr_activity.getitemstring(row_num,'description')
moyr = i_ds_depr_activity.getitemDateTime(row_num,'gl_post_mo_yr')
asset_id = i_ds_depr_activity.getItemNumber(row_num,'asset_id')
i_error_msg = ''

//Send to CPR_DEPR
CHOOSE CASE act_code
	CASE 'DEPR_EXP_ADJUST'
		update cpr_depr
		set depr_exp_adjust = depr_exp_adjust + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'COST_OF_REMOVAL'
		update cpr_depr
		set cost_of_removal = cost_of_removal + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'SALVAGE_CASH', 'SALVAGE_RETURNS'
		update cpr_depr
		set salvage_dollars = salvage_dollars + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'RESERVE_CREDITS'
		update cpr_depr
		set other_credits_and_adjust = other_credits_and_adjust + :amount 
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'RESERVE_TRANS_IN'
		update cpr_depr
		set reserve_trans_in = reserve_trans_in + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'RESERVE_TRANS_OUT'
		update cpr_depr
		set reserve_trans_out = reserve_trans_out + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'GAIN_LOSS'
		update cpr_depr
		set gain_loss = gain_loss + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'RESERVE_ADJUSTMENT'
		update cpr_depr
		set reserve_adjustment = reserve_adjustment + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	CASE 'SALVAGE_EXP_ADJUST'
		update cpr_depr
		set salvage_exp_adjust = salvage_exp_adjust + :amount
		where set_of_books_id = :sob_id
		and gl_posting_mo_yr = :moyr
		and asset_id = :asset_id; 
	case else
		return -2
END CHOOSE

if sqlca.sqlcode = -1 then
	 i_error_msg =  sqlca.sqlerrtext
	 rollback;
	return -1
end if	

if sqlca.sqlnrows = 0 then
	 i_error_msg =  'No data found in cpr_depr table for this activity'
	 rollback;
	return -3
end if	

return 1
end function

public function integer uf_depr_post ();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////This is called from the depr reserve approval window, which expects a return code of 1 (successful) or 2 (failed)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
longlong trans_set_num_rows, t,  depr_trans_set_id, rtn, process_rtn, status_rtn
boolean recur_to_act, trans_set_error_found 
string depr_trans_set_descr

trans_set_error_found = false

//uo_ds_top ds_depr_trans_set
i_ds_depr_trans_set = CREATE uo_ds_top
i_ds_depr_trans_set.dataobject = "dw_depr_trans_set_for_post"
i_ds_depr_trans_set.SetTransObject(sqlca)

rtn = i_ds_depr_trans_set.retrieve() 

if rtn < 0 then
	//a fatal error occurred: need to halt processing and update depr_activity_staus to get any unprocessed activities out of status = 2
	status_rtn = uf_depr_activity_status_update(true)
	if status_rtn = 1 then
		commit;
		f_status_box("Information", "              failed status saved.")
	end if
	DESTROY i_ds_depr_trans_set  
	f_status_box("Information", "ERROR: Halt posting!")
	return 2
end if 

trans_set_num_rows= 0
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////Maint 8604:  First process the current month activities to be able to commit database updates by depr group (including transfers)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rtn = i_ds_depr_trans_set.SetFilter("isnull(current_mo_yr)")
rtn = i_ds_depr_trans_set.Filter()

if rtn < 0 then 
	f_status_box("Information", "ERROR: Unable to filter dw_depr_trans_set_for_post for isnull(current_mo_yr)." )
	
	//a fatal error occurred: need to halt processing and update depr_activity_staus to get any unprocessed activities out of status = 2
	status_rtn = uf_depr_activity_status_update(true)
	if status_rtn = 1 then
		commit;
		f_status_box("Information", "              failed status saved.")
	end if
	DESTROY i_ds_depr_trans_set  
	f_status_box("Information", "ERROR: Halt posting!")
	return 2
end if
	
trans_set_num_rows = i_ds_depr_trans_set.rowcount() 

//Loop through the current month activities 
for t =1 to trans_set_num_rows
	
	depr_trans_set_id = i_ds_depr_trans_set.getitemnumber(t,"depr_trans_set_id") 	
	i_depr_trans_set_id = depr_trans_set_id 
	
	if isnull(depr_trans_set_id) then
		f_status_box("Information", String(Now(),"hh:mm:ss") + ": Starting Depr Postings...")
	else		
		select description into :depr_trans_set_descr from depr_trans_set where depr_trans_set_id = :depr_trans_set_id;
		f_status_box("Information", String(Now(),"hh:mm:ss") + ': Starting Depr Posting for "' + depr_trans_set_descr + '" Trans Set...')
	end if
	
	//Maint 8605 Call function to process the activities group by Trans Sets
	rtn = uf_depr_trans_set_post(depr_trans_set_id, false)
	
	if rtn < 0 then 
		//appropriate rollbacks and commits have already occured in the uf_depr_trans_set_post 
		//need to update depr_activity_staus to get any unprocessed activities out of status = 2
		status_rtn = uf_depr_activity_status_update(false)
		if status_rtn = 1 then
			commit;
			f_status_box("Information", "              failed status saved.")
		end if
	end if
	
	if rtn < 0 or status_rtn < 0 then
		//a fatal error occurred in uf_depr_trans_set_post: need to halt processing 
		//appropriate rollbacks have occurred
		DESTROY i_ds_depr_trans_set  
		f_status_box("Information", "ERROR: Halt posting!")
		return 2 
	end if 
	
next //current month Trans Sets 

trans_set_num_rows = 0
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////Maint 8604: Next process the prior period month activities to be able to commit database updates by depr group (including transfers)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rtn = i_ds_depr_trans_set.SetFilter("not isnull(current_mo_yr)")
rtn = i_ds_depr_trans_set.Filter()

if rtn < 0 then 
	f_status_box("Information", "ERROR: Unable to filter dw_depr_trans_set_for_post for not isnull(current_mo_yr).")
	DESTROY i_ds_depr_trans_set
	
	//a fatal error occurred: need to halt processing and update depr_activity_staus to get any unprocessed activities out of status = 2
	status_rtn = uf_depr_activity_status_update(false)
	if status_rtn = 1 then
		commit;
		f_status_box("Information", "              failed status saved.")
	end if
	DESTROY i_ds_depr_trans_set  
	f_status_box("Information", "ERROR: Halt posting!")
	return 2
end if
	
trans_set_num_rows = i_ds_depr_trans_set.rowcount()

//Loop through the prior period months activities by Trans Set, including depr_trans_set_id = NULL
if trans_set_num_rows > 0 then	
	
	for t =1 to trans_set_num_rows
		
		depr_trans_set_id = i_ds_depr_trans_set.getitemnumber(t,"depr_trans_set_id") 	 
		i_depr_trans_set_id = depr_trans_set_id  
	
		if isnull(depr_trans_set_id) then
			f_status_box("Information", String(Now(),"hh:mm:ss") + ": Starting Prior Period Depr Postings...")
		else		
			select description into :depr_trans_set_descr from depr_trans_set where depr_trans_set_id = :depr_trans_set_id;
			f_status_box("Information", String(Now(),"hh:mm:ss") + ': Starting Prior Period Depr Posting for "' + depr_trans_set_descr + '" Trans Set...')
		end if  
		
		//Call function to process the activities by Trans Sets
		rtn = uf_depr_trans_set_post(depr_trans_set_id, true)
		
		if rtn < 0 then
			//appropriate rollbacks and commits have already occured in the uf_depr_trans_set_post 
			//need to update depr_activity_staus to get any unprocessed activities out of status = 2
			status_rtn = uf_depr_activity_status_update(false)
			if status_rtn = 1 then
				commit;				
				f_status_box("Information", "              failed status saved.")
			end if 
		end if	//the if check for "rtn = uf_depr_trans_set_post(depr_trans_set_id, true)"		
			
		if rtn < 0 or status_rtn < 0 then
			//a fatal error occurred in uf_depr_trans_set_post: need to halt processing 
			DESTROY i_ds_depr_trans_set  
			f_status_box("Information", "ERROR: Halt posting!")
			return 2 
		end if 
	next //Prior period months by Trans Sets 	 	

	DESTROY uo_depr_processing
end if   //"if trans_set_num_rows > 0" then  for Prior Period activities

DESTROY i_ds_depr_trans_set

f_status_box("Information", String(Now(),"hh:mm:ss") + ": Finished!")

return 1

	
end function

public function longlong uf_gl_transaction_post (longlong a_row);longlong depr_act_id, dg_id, sob_id, je_rows, co_id, rtn, je, je_method
longlong book_count, book_include
string je_descr
datetime moyr
decimal amount

depr_act_id = i_ds_depr_activity.GetItemNumber(a_row, "depr_activity_code_id")
dg_id = i_ds_depr_activity.getitemnumber(a_row,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(a_row,'set_of_books_id')
moyr = i_ds_depr_activity.getitemDateTime(a_row,'gl_post_mo_yr')
amount = i_ds_depr_activity.getitemdecimal(a_row,'amount')

if amount = 0 then return 1

/// we need to find all the JE mehtods using this set of books for reserve posting (18 or 19)
uo_ds_top ds_je_methods
ds_je_methods = CREATE uo_ds_top
ds_je_methods.DataObject="dw_depr_txn_je_method_by_book"
ds_je_methods.SetTransObject(sqlca)
je_rows = ds_je_methods.Retrieve(sob_id, dg_id)

if je_rows < 0 then
	i_error_msg = "Retrieve failed:  ds_je_methods.Retrieve(sob_id, dg_id)"
	rollback;
	return -1
end if

/// for each JE method make sure all books included are valid for company
setnull(co_id)
select company_id into :co_id
from depr_group
where depr_group_id = :dg_id;

if sqlca.sqlcode <> 0 then 
	i_error_msg =  sqlca.sqlerrtext
	rollback;
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Error getting Company ID: " + i_error_msg)
	return -4
end if
if IsNull(co_id) then  
	rollback;
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Company ID not found for Depr Group: " + String(dg_id))
	return -4
end if		

for je = 1 to je_rows
	je_method = ds_je_methods.GetItemNumber(je, "je_method_id")	
	
	select count(*), sum(nvl(include_indicator,0)), je.description
	into :book_count, :book_include, :je_descr
	from je_method_set_of_books j, company_set_of_books c, je_method je
	where j.set_of_books_id = c.set_of_books_id (+)
	and j.je_method_id = :je_method
	and c.company_id = :co_id
	and j.je_method_id = je.je_method_id
	group by je.description
	;

	if book_count <> book_include then
		f_status_box("Information","ERROR: Depr JE Method Books do not match.  THIS JE METHOD WILL BE SKIPPED for this activity." )
		f_status_box("Information","ERROR: Method " + je_descr +" has " + String(book_count) + " but company uses " + String(book_include) )
		continue
	else
		rtn = uf_gl_transaction_post(a_row, 'DEPR MANUAL', je_method)
	end if			
	
	if rtn < 0 then
		return rtn
	end if
next

return rtn
end function

public function longlong uf_gl_transaction_post (longlong a_row, string a_source, longlong a_je_method);longlong depr_act_id, rtn, trans_id, dg_id, sob_id,company_id, amt_type, conv
string company, gl_account_str
datetime moyr
boolean post_trf=false
decimal amount

depr_act_id = i_ds_depr_activity.GetItemNumber(a_row, "depr_activity_code_id")
dg_id = i_ds_depr_activity.getitemnumber(a_row,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(a_row,'set_of_books_id')
moyr = i_ds_depr_activity.getitemDateTime(a_row,'gl_post_mo_yr')
amount = i_ds_depr_activity.getitemdecimal(a_row,'amount')

//Maint 8604
if i_prior_period then
	//make the journals for expense (need to use an instance variable instead of modifying the function call, because other things call this function (depr approval)
else
	// !!AJS: 12-JUN-2011: ignore salvage exp adj (submitted w/ 7636)
	if depr_act_id = g_pp_constants.depr_exp_adjust &
	or depr_act_id = g_pp_constants.cost_of_removal_exp_adjust &
	or depr_act_id = g_pp_constants.salvage_exp_adjust then	
		return 0
	end if
end if

select gl_company_no, dg.company_id into :company , :company_id
from company co, depr_group dg
where co.company_id = dg.company_id
and depr_group_id = :dg_id
;
if sqlca.sqlcode <> 0 then
	i_error_msg =  sqlca.sqlerrtext
	rollback; 
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Error getting Company Number: " + i_error_msg)
	return -4
end if
if IsNull(company) then 
	rollback; 
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Company Number not found for Depr Group ID: " + String(dg_id))
	return -4
end if		

//Maint 8604
if i_prior_period then
	//make the journals for transfers (need to use an instance variable instead of modifying the function call, because other things call this function (depr approval)
else
	if depr_act_id = g_pp_constants.reserve_trans_in &
	or depr_act_id = g_pp_constants.reserve_trans_out &
	or depr_act_id = g_pp_constants.cost_of_removal_trans_in &
	or depr_act_id = g_pp_constants.cost_of_removal_trans_out then
		//Maint 34663:  always perform depr reserve transfer journals in this uo object
		post_trf = true
	end if
end if

// get the amount type and convention
isnull(amt_type)
select amount_type into :amt_type
from je_method
where je_method_id = :a_je_method;

if sqlca.sqlcode <> 0 then
	i_error_msg =  sqlca.sqlerrtext
	rollback;  
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Error getting Amount Type: " + i_error_msg)
	return -4
end if
if IsNull(amt_type) then 
	rollback; 
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Amount Type not found for JE Method ID: " + String(a_je_method))
	return -4
end if		

isnull(conv)
select reversal_convention into :conv
from je_method_set_of_books
where je_method_id = :a_je_method
and set_of_books_id = :sob_id;

if sqlca.sqlcode <> 0 then
	i_error_msg =  sqlca.sqlerrtext
	rollback;   
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Error getting Reversal Convention: " + i_error_msg)
	return -4
end if
if IsNull(conv) then 
	rollback; 
	i_ds_depr_activity.SetItem(a_row, "posting_error", "ERROR: Reversal Convention not found for JE Method ID " + String(a_je_method) + " and Book " + String(sob_id) )
	return -4
end if		

f_status_box("Information", "              updating gl_transactions: $" + string(amount) )

uo_ds_top ds_gl_trans
ds_gl_trans = CREATE uo_ds_top
ds_gl_trans.DataObject = "dw_gl_transaction"
ds_gl_trans.SetTransObject(sqlca)

///// $$$ARE 2/11/2003 changed base to put depr_group_id: in front of description, allowing allocation of gl_transactions
///// 	by depr_group where needed (e.g. Southern) at lines 52 and 78
if Trim(i_ds_depr_activity.getitemstring(a_row,'debit_account')) <> 'Intra-Company Transfer' then
	
	ds_gl_trans.insertrow(1)
	
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
	ds_gl_trans.setitem(1,'gl_trans_id',trans_id)
	ds_gl_trans.setitem(1,'month',moyr)
	ds_gl_trans.setitem(1,'company_number',company)
	///// ###ARE 1/31/2006 sign is backwards on JEs for trf, flip * -1 to else side
	if depr_act_id = 7 or depr_act_id = 14 then
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * -1 * conv )
	else
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * conv )
	end if
	ds_gl_trans.setitem(1,'description', & 
										mid(string(dg_id) + ':' + &
										string(i_ds_depr_activity.getitemnumber(a_row,'depr_activity_id'))+":"+i_ds_depr_activity.GetItemString(a_row,"description"),1,35))
	gl_account_str = f_autogen_je_account(i_ds_depr_activity, a_row, 18, long(i_ds_depr_activity.getitemstring(a_row,'debit_account')), a_je_method )
	if Left(gl_account_str,5) = "ERROR" then
		f_status_box("Information","f_autogen_je_account() error for offset (debit): " + gl_account_str)
		destroy ds_gl_trans 
		rollback;
		return -2
	end if
		
	ds_gl_trans.setitem(1,'gl_account', gl_account_str)
	ds_gl_trans.setitem(1,'debit_credit_indicator',1)
	ds_gl_trans.setitem(1,'gl_je_code',i_ds_depr_activity.getitemstring(a_row,'gl_je_code'))
	ds_gl_trans.setitem(1,'gl_status_id',1)
	ds_gl_trans.setitem(1,'source', a_source)
	ds_gl_trans.setitem(1,'je_method_id', a_je_method)
	ds_gl_trans.setitem(1,'amount_type', amt_type)
	ds_gl_trans.setitem(1,'trans_type', 18)
end if

if Trim(i_ds_depr_activity.getitemstring(a_row,'credit_account')) <> 'Intra-Company Transfer' then
	
	ds_gl_trans.insertrow(1)
	
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
	ds_gl_trans.setitem(1,'gl_trans_id',trans_id)
	ds_gl_trans.setitem(1,'month',moyr)
	ds_gl_trans.setitem(1,'company_number',company)
	///// ###ARE 3/6/2006 sign is backwards on JEs for trf, flip * -1 to else side
	if depr_act_id = 7 or depr_act_id = 14 then
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * -1 * conv )
	else
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * conv )
	end if
	ds_gl_trans.setitem(1,'description', & 
										mid(string(dg_id) + ':' + &
										string(i_ds_depr_activity.getitemnumber(a_row,'depr_activity_id'))+":"+i_ds_depr_activity.GetItemString(a_row,"description"),1,35))
	gl_account_str = f_autogen_je_account(i_ds_depr_activity, a_row, 19, long(i_ds_depr_activity.getitemstring(a_row,'credit_account')), a_je_method )
	if Left(gl_account_str,5) = "ERROR" then
		f_status_box("Information","f_autogen_je_account() error for reserve (credit): " + gl_account_str)
		destroy ds_gl_trans
		rollback;
		return -3
	end if
		
	ds_gl_trans.setitem(1,'gl_account', gl_account_str)
	ds_gl_trans.setitem(1,'debit_credit_indicator',0)
	ds_gl_trans.setitem(1,'gl_je_code',i_ds_depr_activity.getitemstring(a_row,'gl_je_code'))
	ds_gl_trans.setitem(1,'gl_status_id',1)
	ds_gl_trans.setitem(1,'source',a_source)
	ds_gl_trans.setitem(1,'je_method_id', a_je_method)
	ds_gl_trans.setitem(1,'amount_type', amt_type)
	ds_gl_trans.setitem(1,'trans_type', 19)
end if

rtn = ds_gl_trans.Update()

if rtn < 0 then
	i_error_msg =  ds_gl_trans.i_sqlca_sqlerrtext
	rollback;   
	f_status_box("Information","Error: ds_gl_trans.Update() failed " + i_error_msg)
	destroy ds_gl_trans 
	return -1
end if 

DESTROY ds_gl_trans

return rtn
end function

public function integer uf_depr_ledger_post (longlong a_row_num);longlong dg_id, sob_id, subledger_item_id, asset_id, return_code 
decimal amount
string act_code, subledger_name, sqls
datetime moyr 
	
dg_id = i_ds_depr_activity.getitemnumber(a_row_num,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(a_row_num,'set_of_books_id')
amount = i_ds_depr_activity.getitemdecimal(a_row_num,'amount')
act_code = i_ds_depr_activity.getitemstring(a_row_num,'description')
moyr = i_ds_depr_activity.getitemDateTime(a_row_num,'gl_post_mo_yr')
asset_id = i_ds_depr_activity.getItemNumber(a_row_num,'asset_id')
subledger_item_id = i_ds_depr_activity.getItemNumber(a_row_num,'subledger_item_id') 

//i_reverse_recurring never gets used, but leave it here just in case
if i_reverse_recurring then
	amount = -i_reserve_recurring_amount
	goto update_depr_ledger
end if

if amount = 0 then return 1

i_error_msg = ''

//CALL CPR_DEPR FUNCTION if asset _id is populated
if not isnull(asset_id) then
	if isnull(subledger_item_id) then
		return_code = uf_cpr_depr_post(a_row_num)
		
		//if uf_cpr_depr_post fails, need to rollback here to be able to save error messages later
		if return_code = -2 then 
			i_ds_depr_activity.SetItem(a_row_num, "posting_error", "ERROR: UPDATING to individual asset: Invalid Activity Code: ")
			return -4
		elseif return_code = - 1 then 
			i_ds_depr_activity.SetItem(a_row_num, "posting_error", "ERROR: UPDATING to individual asset: DB Problem: " + i_error_msg)
			return -4
		elseif return_code = - 3 then 
			i_ds_depr_activity.SetItem(a_row_num, "posting_error", "ERROR: UPDATING to individual asset: " + i_error_msg)
			return -4
		end if
	else
		return_code = uf_subl_depr_post(a_row_num)
		
		//if uf_cpr_depr_post fails, need to rollback here to be able to save error messages later
		if return_code = -2 then 
			i_ds_depr_activity.SetItem(a_row_num, "posting_error", "ERROR: UPDATING to subledger: Invalid Activity Code: ")
			return -5
		elseif return_code = - 1 then 
			i_ds_depr_activity.SetItem(a_row_num, "posting_error", "ERROR: UPDATING to subledger: DB Problem: " + i_error_msg) 
			return -5
		elseif return_code = - 3 then 
			i_ds_depr_activity.SetItem(a_row_num, "posting_error", "ERROR: UPDATING to subledger: " + i_error_msg) 
			return -5
		end if
	end if // subl v. cpr_depr
end if

update_depr_ledger:
//Send to DEPR_LEDGER
CHOOSE CASE act_code
	CASE 'DEPR_EXP_ADJUST'
		update depr_ledger
		set depr_exp_adjust = depr_exp_adjust + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		; 
	CASE 'COST_OF_REMOVAL'
		update depr_ledger
		set cost_of_removal = cost_of_removal + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'SALVAGE_CASH'
		update depr_ledger
		set salvage_cash = salvage_cash + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;   
	CASE 'SALVAGE_RETURNS'
		update depr_ledger
		set salvage_returns = salvage_returns + :amount 
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_CREDITS'
		update depr_ledger
		set reserve_credits = reserve_credits + :amount 
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_TRANS_IN'
		update depr_ledger
		set reserve_tran_in = reserve_tran_in + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_TRANS_OUT'
		update depr_ledger
		set reserve_tran_out = reserve_tran_out + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_TRANS_OUT'
		update depr_ledger
		set cor_res_tran_out = cor_res_tran_out + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_TRANS_IN'
		update depr_ledger
		set cor_res_tran_in = cor_res_tran_in + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_RES_ADJUST'
		update depr_ledger
		set cor_res_adjust = cor_res_adjust + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_EXP_ADJUST'
		update depr_ledger
		set cor_exp_adjust = cor_exp_adjust + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'GAIN_LOSS'
		update depr_ledger
		set gain_loss = gain_loss + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_ADJUSTMENT'
		update depr_ledger
		set reserve_adjustments = reserve_adjustments + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'IMPAIRMENT_ACT'
		update depr_ledger
		set impairment_reserve_act = impairment_reserve_act + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'SALVAGE_EXP_ADJUST'
		update depr_ledger
		set salvage_exp_adjust = salvage_exp_adjust + :amount
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		; 
	case else
		return -2
END CHOOSE

if sqlca.sqlcode = -1 then
	i_error_msg = sqlca.sqlerrtext
	rollback;
	return -1
end if	

//Maint 8604
if sqlca.sqlnrows = 0 then
	 i_error_msg =  'No data found in depr_ledger table for this activity for moyr = ' + string(moyr)
	rollback;
	return -3
end if	

return 1
end function

public function longlong uf_depr_trans_set_post (longlong a_depr_trans_set_id, boolean a_prior_period);longlong depr_group_num_rows, num_activity_rows, i,j, depr_group_id, company_id , rtn, act_dg_id
longlong return_code, check_count, ret1, activity_id, sob_id
boolean recur_to_act, insert_error = false, trans_set_total_error = false
datetime depr_calc, moyr 
string activity_description, sql_mod_string, rtn_string
longlong status_rtn

//check_count useful for debug
select count(*) into :check_count
from depr_post_activity_temp;

i_reverse_recurring = false  //<--this variable never gets used, will leave it for now
trans_set_total_error = false
insert_error = false

///Maint 8604:  Reset depr_process_temp table at the beginning of the Trans Set
if a_prior_period then
	delete from depr_process_temp;
		
	if sqlca.sqlcode = -1 then
		f_status_box("Information", "ERROR: Unable to reset depr_process_temp: " + sqlca.sqlerrtext)  
		rollback;
		//a fatal error occurred: need to halt processing and update depr_activity_staus to get any unprocessed activities out of status = 2
		status_rtn = uf_depr_activity_status_update(false)
		if status_rtn = 1 then
			commit;
			f_status_box("Information", "              failed status saved.")
		end if  
		f_status_box("Information", "ERROR: Halt posting!")
		return -1
	end if 	
end if 

//uo_ds_top i_ds_depr_groups
i_ds_depr_groups = CREATE uo_ds_top
i_ds_depr_groups.dataobject = "dw_depr_groups_for_post"

//uo_ds_top i_ds_depr_activity
i_ds_depr_activity = CREATE uo_ds_top
i_ds_depr_activity.dataobject = "dw_depr_activity_grid_post"

i_ds_depr_groups.SetTransObject(sqlca)
i_ds_depr_activity.SetTransObject(sqlca) 

///Maint 8604 
if a_prior_period then
	i_prior_period =  true
	sql_mod_string = " and pend_depr_activity.depr_activity_id in (select depr_post_activity_temp.depr_activity_id from depr_post_activity_temp) " + &
			" and current_mo_yr is not null "
else
	i_prior_period =  false
	sql_mod_string = " and pend_depr_activity.depr_activity_id in (select depr_post_activity_temp.depr_activity_id from depr_post_activity_temp) " + &
			" and current_mo_yr is null "
end if

///Maint 8605
if isnull(a_depr_trans_set_id) then
	sql_mod_string = sql_mod_string + " and depr_trans_set_id is null " 
else	
	sql_mod_string = sql_mod_string + " and depr_trans_set_id = " +string(a_depr_trans_set_id)
end if  

rtn_string =	f_add_where_clause_ds(i_ds_depr_groups,"", sql_mod_string + " order by pend_depr_activity.depr_group_id " , false)

if rtn_string = '' then 
	f_status_box("Information", "ERROR: f_add_where_clause_ds failed for i_ds_depr_groups: "   + i_ds_depr_groups.i_sqlca_sqlerrtext )
	DESTROY i_ds_depr_groups
	DESTROY i_ds_depr_activity 
	rollback;
	return -1	 //Fatal error and need to halt all processing
end if 

rtn_string =	f_add_where_clause_ds(i_ds_depr_activity,"", sql_mod_string, false)

if rtn_string = '' then 
	f_status_box("Information", "ERROR: f_add_where_clause_ds failed for i_ds_depr_activity: "  + i_ds_depr_activity.i_sqlca_sqlerrtext )
	DESTROY i_ds_depr_groups
	DESTROY i_ds_depr_activity 
	rollback;
	return -1	 //Fatal error and need to halt all processing
end if 

depr_group_num_rows = i_ds_depr_groups.retrieve()  //this is also the rowcount 

if depr_group_num_rows < 0 then
	f_status_box("Information", "ERROR:i_ds_depr_groups failed retrieve "   + i_ds_depr_groups.i_sqlca_sqlerrtext )
	DESTROY i_ds_depr_groups
	DESTROY i_ds_depr_activity 
	rollback;
	return -1	 //Fatal error and need to halt all processing	
end if  

for i = 1 to depr_group_num_rows
	
	depr_group_id = i_ds_depr_groups.getitemnumber(i,"depr_group_id") //<--Depr Group being processed
	
	// !!AJS: 08-JUN-2011: Maint 7636: this dw now retrieves all activities and related activities (eg transfers) for the depr group 
	num_activity_rows = i_ds_depr_activity.retrieve(depr_group_id)  //this is also the rowcount  
	
	if num_activity_rows < 0 then
		f_status_box("Information", "ERROR:i_ds_depr_activity failed retrieve "   + i_ds_depr_activity.i_sqlca_sqlerrtext )
		DESTROY i_ds_depr_groups
		DESTROY i_ds_depr_activity 
		rollback;
		return -1	 //Fatal error and need to halt all processing	
	end if  
	
	if num_activity_rows = 0 then continue
	
//	f_status_box("Information", String(Now(),"hh:mm:ss") + ": testing: depr loop: dpg = " + string(depr_group_id) + " : num_activity_rows = " + string(num_activity_rows))
	
	return_code = 0
	insert_error = false
	////###sjh 6/16/2011 Maint 7866 replace i variable with 1 value
	company_id = i_ds_depr_activity.getitemnumber(1,'company_id')
	moyr = i_ds_depr_activity.getitemDateTime(1,'gl_post_mo_yr')
	
	select depr_calculated into :depr_calc from cpr_control
	where company_id = :company_id
	and accounting_month = :moyr;
	
	If isnull(depr_calc) then
		recur_to_act = false
	else		
		if a_prior_period or not isnull(a_depr_trans_set_id) then
			//Prior Period and Trans Sets not allowed to be setup as recurring
			recur_to_act = false
		else
			recur_to_act = true
		end if		
	end if 
	
	for j = 1 to num_activity_rows		 		 
		
		activity_id = i_ds_depr_activity.getitemnumber(j,"depr_activity_id") 
		activity_description = i_ds_depr_activity.getitemstring(j,"description")
		act_dg_id = i_ds_depr_activity.getitemnumber(j,"depr_group_id")
		sob_id = i_ds_depr_activity.getitemnumber(j,"set_of_books_id")
	
		// !!AJS: 08-JUN-2011: Maint 7636: if marked as error skip it (this would be if included in diff depr group due to transfer)
		if i_ds_depr_activity.GetItemNumber(j, "depr_activity_status") = 3 then continue		
	
		f_status_box("Information", String(Now(),"hh:mm:ss") + ": Processing " + activity_description + " for depr_group_id " + string(act_dg_id) + " sob_id " + string(sob_id)) 
		  
		If (isnull(i_ds_depr_activity.getItemNumber(j,'post_period_months')))  then
			
			//CALL DEPR_LEDGER FUNCTION (this calls cpr_depr or subl if necessary)
			return_code = uf_depr_ledger_post(j)
			if return_code < 0 then
				choose case return_code
					case -2  
						i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: UPDATING to DEPR_LEDGER: Invalid Activity Code ")
					case -1  
						i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: UPDATING to DEPR_LEDGER: DB Problem: " + i_error_msg) 
					case -3  
						i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: UPDATING to DEPR_LEDGER: " + i_error_msg) 
					case -4, -5
						// message written in uf_depr_ledger_post() for cpr_depr and subledger updates.  rollback also already done, do not do rollback again because the posting error message will not save						
				end choose
				insert_error = true  
				continue
			end if

			//check if this group/book needs to update blending
			i_blending = uf_check_blending(j)
			
			if i_blending and a_prior_period then
				return_code = uf_depr_ledger_blending_post(j)
				if return_code < 0 then
					choose case return_code
						case -2  
							i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: UPDATING to DEPR_LEDGER_BLENDING: Invalid Activity Code ")
						case -1  
							i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: UPDATING to DEPR_LEDGER_BLENDING: DB Problem: " + i_error_msg) 
						case -3  
							i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: UPDATING to DEPR_LEDGER_BLENDING: " + i_error_msg) 
						case -4, -5
							// message written in uf_depr_ledger_post() for cpr_depr and subledger updates.  rollback also already done, do not do rollback again because the posting error message will not save						
					end choose
					insert_error = true  
					continue
				end if
			end if
			
			//CALL DEPR_ACTIVITY FUNCTION
			return_code = uf_depr_activity_post(j)
			if return_code = -1 then 
				i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: Insert to DEPR_ACTIVITY: " + i_error_msg)
				insert_error = true 
				continue
			end if
			
			if IsNull(i_ds_depr_activity.GetItemNumber(j,"post_period_months")) and i_ds_depr_activity.GetItemNumber(j,"gl_post_flag") = 1 then
				// CALL GL_TRANSACTION FUNCTION
				return_code = uf_gl_transaction_post(j)
				if return_code < 0 then
					choose case return_code
						case -1 
							i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: processing GL_TRANSACTION: " + i_error_msg)
						case -2 
							i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: creating debit account string")
						case -3 
							i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: creating credit account string")
						case -4							
						// message written in uf_gl_transaction_post().  rollback also already done, do not do rollback again because the posting error message will not save
					end choose
					insert_error = true 
					continue
				end if				
			end if
			
		end if  //If (isnull(i_ds_depr_activity.getItemNumber(j,'post_period_months')))  then
		
 		//recurring_fuction:
		if Not (isnull(i_ds_depr_activity.getItemNumber(j,'post_period_months'))) then
			//CALL RECURRING FUNCTION
			return_code = uf_depr_recur_post(j)
			if return_code = -1 then
				rollback;
				insert_error = true
				i_ds_depr_activity.SetItem(j, "posting_error", "ERROR: UPDATE TO RECURRING TABLE FAILED") 
				continue
			end if
		end if
		
		if recur_to_act then
			ret1 = f_load_depr_input(company_id, moyr)
			if ret1 <> 1 then
				rollback;
				insert_error = true
				i_ds_depr_activity.SetItem(j,"posting_error","ERROR: RELOADING RECURRING DEPR ACTIVITIES") 
				continue
			end if
		end if
		
		if a_prior_period and insert_error = false then
//			f_status_box("Information", String(Now(),"hh:mm:ss") + ": testing: insert depr_process_temp activity_id= " + string(activity_id) )
			
			//Need to insert into depr_process_temp table for the depr_group_id, sob, and each month between moyr and current_mo_yr to foot and rollforward 		
			if isnull(a_depr_trans_set_id) then 
				insert into depr_process_temp (depr_group_id, gl_post_mo_yr, set_of_books_id)
				select b.depr_group_id, b.gl_post_mo_yr, b.set_of_books_id 
				from pend_depr_activity a, depr_ledger b
				where depr_activity_status = 2 
				and amount <> 0
				and current_mo_yr is not null
				and a.depr_activity_id = :activity_id
				and a.depr_group_id = b.depr_group_id
				and a.set_of_books_id = b.set_of_books_id
				and b.gl_post_mo_yr between a.gl_post_mo_yr and a.current_mo_yr
				and a.depr_trans_set_id is null 
				and a.depr_activity_id in (select d.depr_activity_id from depr_post_activity_temp d)
				minus
				select c.depr_group_id, c.gl_post_mo_yr, c.set_of_books_id 
				from	 depr_process_temp c;
			else
				insert into depr_process_temp (depr_group_id, gl_post_mo_yr, set_of_books_id)
				select b.depr_group_id, b.gl_post_mo_yr, b.set_of_books_id 
				from pend_depr_activity a, depr_ledger b
				where depr_activity_status = 2 
				and amount <> 0
				and current_mo_yr is not null
				and a.depr_activity_id = :activity_id
				and a.depr_group_id = b.depr_group_id
				and a.set_of_books_id = b.set_of_books_id
				and b.gl_post_mo_yr between a.gl_post_mo_yr and a.current_mo_yr
				and a.depr_trans_set_id = :a_depr_trans_set_id
				and a.depr_activity_id in (select d.depr_activity_id from depr_post_activity_temp d)				
				minus
				select c.depr_group_id, c.gl_post_mo_yr, c.set_of_books_id 
				from depr_process_temp c;
			end if
			
			if sqlca.sqlcode = -1 then 
				f_status_box("Information", "ERROR: Unable to insert into depr_process_temp: " + sqlca.sqlerrtext)
				rollback;
				DESTROY i_ds_depr_groups
				DESTROY i_ds_depr_activity 
				return -1	 //Fatal error and need to halt all processing
			end if	 
			
			rtn = sqlca.sqlnrows
		end if	//if a_prior_period and insert_error = false then

	next  //for j = 1 to num_activity_rows
	
	if insert_error = true then
		
		//rollback any data updates that might have occured at this point from the activities loop: gl trans, depr_ledger, insert into depr_activity
		//this rollback is particuarly important to have when posting activities in a Transfer Pair or activities in a Trans Set
		rollback;
		
		if not isnull(a_depr_trans_set_id) then
			trans_set_total_error = true 
		end if
		 
		for j = 1 to num_activity_rows 
			i_ds_depr_activity.SetItem(j, "depr_activity_status",3)
		next
		
		 f_status_box("Information", "              posting failed: check posting error messages on the failed activities.")
	else			 
		//activity successfully posted 	
		for j = 1 to num_activity_rows 
			i_ds_depr_activity.DeleteRow(1)
		next 
		
	end if  		

	rtn = i_ds_depr_activity.update() 
	
	if rtn < 0 then 
		f_status_box("Information", "ERROR: Update for i_ds_depr_activity failed: "  + i_ds_depr_activity.i_sqlca_sqlerrtext)
		rollback; 
		DESTROY i_ds_depr_groups
		DESTROY i_ds_depr_activity 
		return -1	 //Fatal error and need to halt all processing
	end if

	if isnull(a_depr_trans_set_id) then		
		if a_prior_period and insert_error = false then				
			//need to update depr_ledger and rebuild factors for the Non Trans Set activites
			//uf_update_prior_period_tables contains its own rollbacks and commits
			rtn = uf_update_prior_period_tables()
			
			if rtn < 0 then
				return -1	 //Fatal error and need to halt all processing
			end if
		end if 			
		
		if insert_error then
			f_status_box("Information", "              error message saved (other updates not saved).")
		else				
			f_status_box("Information", "              posting saved.")
		end if 
		//commit database updates for successful postings OR for saving error messages when the activity is not part of a Trans Set 
		commit;
	else
		if trans_set_total_error and not isnull(a_depr_trans_set_id) then
			//need to fail the entire Trans Set instead of processing other activities in the Trans Set 
			goto the_end
		end if 
	end if 		
	
next  //by depr group within the Trans Set	

DESTROY i_ds_depr_groups
DESTROY i_ds_depr_activity 

the_end:
//Need to update activities in a Trans Set
if not isnull(a_depr_trans_set_id) then	
	if a_prior_period and trans_set_total_error = false then	
		//Update depr_ledger and rebuild factors for the Trans Set if no errors occurred 
		//uf_update_prior_period_tables contains its own rollbacks and commits
		rtn = uf_update_prior_period_tables()
		
		if rtn < 0 then
			return -1	 //Fatal error and need to halt all processing
		end if
	end if  
	
	if trans_set_total_error then
		//need to fail the entire Trans Set and update depr_activity_staus to 3 on the other activities in the Trans Set
		status_rtn = uf_depr_activity_status_update(false)
		if status_rtn <> 1 then
			f_status_box("Information", "ERROR: Halt posting!")
			return -1
		end if  
		f_status_box("Information", "              Trans Set: error message saved (other updates not saved).")
	else				
		f_status_box("Information", "              Trans Set: posting saved.")
	end if
	
	commit;
	
end if  //if not isnull(a_depr_trans_set_id) then	

return 1 
	
	
	
end function

public function longlong uf_depr_activity_status_update (boolean a_all);//Need to change the depr_activity_status from a 4 to a 3 for errors encountered during processing
//Leaving the status = 4 keeps the depr activities in limbo

//a_all = true: update all pend_depr_activity data not yet successfully processed (successful process deletes out of the table
//a_all = false: update by depr_trans_set_id

if a_all then
	update pend_depr_activity a set a.depr_activity_status  = 3 
	where a.depr_activity_id in (select d.depr_activity_id from depr_post_activity_temp d);
else
	
	if isnull(i_depr_trans_set_id) then
		update pend_depr_activity a set a.depr_activity_status  = 3 
		where a.depr_activity_id in (select d.depr_activity_id from depr_post_activity_temp d) 
		and a.depr_trans_set_id is null;
	else
		update pend_depr_activity a set a.depr_activity_status  = 3 
		where a.depr_activity_id in (select d.depr_activity_id from depr_post_activity_temp d) 
		and a.depr_trans_set_id = :i_depr_trans_set_id;		
	end if
	
end if

if sqlca.sqlcode = -1 then 
	f_status_box("Information", "ERROR: Unable to pend_depr_activity.depr_activity_status = 3: " + sqlca.sqlerrtext)
	rollback; 
	return -1	 //Fatal error and need to halt all processing
end if	 			

return 1
end function

public function longlong uf_update_prior_period_tables ();longlong status_rtn, process_rtn

uo_depr_processing uo_process
uo_process = CREATE uo_depr_processing	

//Need to call the function that does the depr table updates for footing and rolling foward balances
//DEPR_PROCESS_TEMP table has data loaded in from uf_depr_trans_set_post() for prior period postings  
process_rtn = uo_process.uf_depr_ledger_balances()

if process_rtn = 1 then
	process_rtn = uo_process.uf_depr_ledger_blending_balances()
end if

if process_rtn = 1 then  				
 
	//Allow commit here since important depr_ledger updates for the Trans Set have been successful
	f_status_box("Information", "              depr ledger updates saved.")
	commit;  
	
	//Need to call function to rebuild depr factors
	process_rtn = uo_process.uf_build_depr_factors()
	if process_rtn < 0 then		 
		f_status_box("Information", "ERROR: Unable to build depr factors for prior months!!  Rebuild factors manually!")
		//depr factors have no impact and depr calculations and balances, output error message but activity posted 
	end if 
	
	//Need to call function to rebuild combined depr factors
	process_rtn = uo_process.uf_build_combined_depr_factors()
	if process_rtn < 0 then		 
		f_status_box("Information", "ERROR: Unable to build combined depr factors for prior months!!  Rebuild combined factors manually!")
		//depr factors have no impact and depr calculations and balances, output error message but activity posted
	end if 				
	
	//Reset depr_process_temp table after updating the prior period data for the activities
	delete from depr_process_temp;
			
	if sqlca.sqlcode = -1 then
		f_status_box("Information", "ERROR: Unable to reset depr_process_temp after successful posting: " + sqlca.sqlerrtext)  
		rollback;
		//a fatal error occurred: need to halt processing and update depr_activity_staus to get any unprocessed activities out of status = 2
		status_rtn = uf_depr_activity_status_update(false)
		if status_rtn = 1 then
			commit;
			f_status_box("Information", "            failed status saved.")
		end if  
		f_status_box("Information", "ERROR: Halt posting!")
		return -1 
	end if 	
	
	//Allow commit to save factor data updates
	commit;
	f_status_box("Information", "              depr factors rebuild saved.")	 

else
	
	//a rollback would have been called in uf_depr_ledger_balances if the code is inside this if statement	
	if isnull(i_depr_trans_set_id) then
		update pend_depr_activity set pend_depr_activity.depr_activity_status = 3, pend_depr_activity.posting_error = 'Unable to update depr_ledger balances for prior periods' 
		where pend_depr_activity.depr_activity_status = 2 and pend_depr_activity.depr_trans_set_id is null 
		and pend_depr_activity.depr_activity_id in 
				(select depr_post_activity_temp.depr_activity_id from depr_post_activity_temp)
		and (pend_depr_activity.depr_group_id, pend_depr_activity.gl_post_mo_yr, pend_depr_activity.set_of_books_id) in 
				(select depr_process_temp.depr_group_id, depr_process_temp.gl_post_mo_yr, depr_process_temp.set_of_books_id from depr_process_temp);
	else
		update pend_depr_activity set pend_depr_activity.depr_activity_status = 3, pend_depr_activity.posting_error = 'Unable to update depr_ledger balances for prior periods' 
		where pend_depr_activity.depr_activity_status = 2 and pend_depr_activity.depr_trans_set_id  = :i_depr_trans_set_id 
		and pend_depr_activity.depr_activity_id in 
				(select depr_post_activity_temp.depr_activity_id from depr_post_activity_temp)
		and (pend_depr_activity.depr_group_id, pend_depr_activity.gl_post_mo_yr, pend_depr_activity.set_of_books_id) in 
				(select depr_process_temp.depr_group_id, depr_process_temp.gl_post_mo_yr, depr_process_temp.set_of_books_id from depr_process_temp);
	end if		
		
	if sqlca.sqlcode = -1 then
		f_status_box("Information", "ERROR: Unable to update failed status: " + sqlca.sqlerrtext) 
		rollback;
		 
		//a fatal error occurred: need to halt processing and update depr_activity_staus to get any unprocessed activities out of status = 2
		status_rtn = uf_depr_activity_status_update(false)
		if status_rtn = 1 then
			commit;
			f_status_box("Information", "            failed status saved.")
		end if 
		DESTROY uo_depr_processing
		f_status_box("Information", "ERROR: Halt posting!")
		return -1

	end if 
end if 

DESTROY uo_depr_processing	
return 1
end function

public function integer uf_depr_ledger_blending_post (longlong a_row_num);////////////////////////////
// maint 8952: new function to update DEPR_LEDGER_BLENDING for prior period adjustments
///////////////////////////
longlong dg_id, sob_id, subledger_item_id, asset_id, return_code, blending
decimal{2} amount
decimal{8} blending_pct
string act_code, subledger_name, sqls
datetime moyr 
	
dg_id = i_ds_depr_activity.getitemnumber(a_row_num,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(a_row_num,'set_of_books_id')
amount = i_ds_depr_activity.getitemdecimal(a_row_num,'amount')
act_code = i_ds_depr_activity.getitemstring(a_row_num,'description')
moyr = i_ds_depr_activity.getitemDateTime(a_row_num,'gl_post_mo_yr')
asset_id = i_ds_depr_activity.getItemNumber(a_row_num,'asset_id')
subledger_item_id = i_ds_depr_activity.getItemNumber(a_row_num,'subledger_item_id') 

i_error_msg = ''

SetNull(blending_pct)
select source_percent into :blending_pct
from depr_method_blending b, depr_group g
where b.depr_method_id = g.depr_method_id
and b.effective_date =
	(select max(x.effective_date) from depr_method_blending x
		where x.depr_method_id = b.depr_method_id
		and x.source_set_of_books_id = b.source_set_of_books_id
		and x.effective_date <= :moyr
	)
and b.source_set_of_books_id = :sob_id
and g.depr_group_id = :dg_id
;

if blending_pct = 0 or IsNull(blending_pct) then return 1

//Send to DEPR_LEDGER_BLENDING
CHOOSE CASE act_code
	CASE 'DEPR_EXP_ADJUST'
		update depr_ledger_blending
		set depr_exp_adjust_bl = depr_exp_adjust_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		; 
	CASE 'COST_OF_REMOVAL'
		update depr_ledger_blending
		set cost_of_removal_bl = cost_of_removal_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'SALVAGE_CASH'
		update depr_ledger_blending
		set salvage_cash_bl = salvage_cash_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;   
	CASE 'SALVAGE_RETURNS'
		update depr_ledger_blending
		set salvage_returns_bl = salvage_returns_bl + (:amount * :blending_pct) 
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_CREDITS'
		update depr_ledger_blending
		set reserve_credits_bl = reserve_credits_bl + (:amount * :blending_pct) 
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_TRANS_IN'
		update depr_ledger_blending
		set reserve_tran_in_bl = reserve_tran_in_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_TRANS_OUT'
		update depr_ledger_blending
		set reserve_tran_out_bl = reserve_tran_out_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_TRANS_OUT'
		update depr_ledger_blending
		set cor_res_tran_out_bl = cor_res_tran_out_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_TRANS_IN'
		update depr_ledger_blending
		set cor_res_tran_in_bl = cor_res_tran_in_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_RES_ADJUST'
		update depr_ledger_blending
		set cor_res_adjust_bl = cor_res_adjust_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'COST_OF_REMOVAL_EXP_ADJUST'
		update depr_ledger_blending
		set cor_exp_adjust_bl = cor_exp_adjust_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'GAIN_LOSS'
		update depr_ledger_blending
		set gain_loss_bl = gain_loss_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'RESERVE_ADJUSTMENT'
		update depr_ledger_blending
		set reserve_adjustments_bl = reserve_adjustments_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'IMPAIRMENT_ACT'
		update depr_ledger_blending
		set impairment_reserve_act_bl = impairment_reserve_act_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		;  
	CASE 'SALVAGE_EXP_ADJUST'
		update depr_ledger_blending
		set salvage_exp_adjust_bl = salvage_exp_adjust_bl + (:amount * :blending_pct)
		where depr_group_id = :dg_id
		and set_of_books_id = :sob_id
		and gl_post_mo_yr = :moyr		; 
	case else
		return -2
END CHOOSE

if sqlca.sqlcode = -1 then
	i_error_msg = sqlca.sqlerrtext
	rollback;
	return -1
end if	

//Maint 8604
if sqlca.sqlnrows = 0 then
	 i_error_msg =  'No data found in depr_ledger_blending table for this activity for moyr = ' + string(moyr)
	rollback;
	return -3
end if	

return 1
end function

public function boolean uf_check_blending (longlong a_row_num);////////////////////////////
// maint 8952: new function to update DEPR_LEDGER_BLENDING for prior period adjustments
///////////////////////////
longlong dg_id, sob_id, blending
datetime moyr 
	
dg_id = i_ds_depr_activity.getitemnumber(a_row_num,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(a_row_num,'set_of_books_id')
moyr = i_ds_depr_activity.getitemDateTime(a_row_num,'gl_post_mo_yr')

i_error_msg = ''

// find out if this group has blending.
// this  is needed if this set of books a source book to another blended book
blending = 0
select count(*) into :blending
from depr_Method_rates r, depr_group g
where r.depr_method_id = g.depr_method_id
and r.effective_date =
	(select max(x.effective_date) from depr_method_rates x
		where x.depr_method_id = r.depr_method_id
		and x.effective_date <= :moyr
	)
and r.set_of_books_id <> :sob_id
and g.depr_group_id = :dg_id
and r.rate_used_code = 3
;

if blending = 0 then return false

return true


end function

on uo_depr_activity_post.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_depr_activity_post.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

