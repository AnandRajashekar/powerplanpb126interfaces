HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string	i_exe_name = 'cr_balances.exe'

string			  i_use_new_structures_table, i_amount_types
uo_ds_top	i_ds_elements
longlong	i_num_elements
end variables

forward prototypes
public function longlong uf_step1 (longlong a_month)
public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field)
public function longlong uf_step2 (longlong a_month)
public function longlong uf_parse (string a_stringtoparse, string a_separator, ref string a_array_to_hold[])
public function longlong uf_read ()
public function longlong uf_step3 (longlong a_month)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_step1 (longlong a_month);//*********************************************************************************************
//
//  Step 1  :  Insert the records into balances_id
//
//  NOTES   :  This script is best performed on conversion with a company/MN index and
//					using where clauses in the code below.
//
//*********************************************************************************************
longlong i, max_id, include
string sqls, element_string, element

f_pp_msgs(" ")
f_pp_msgs("---Starting uf_step1 (Maintaining balances_id) at: " + string(now()))


//**********************************************************
//  SETUP:
//**********************************************************
f_pp_msgs("------Updating balances_id to 0 where balances_id is NULL at " + string(now()))

update cr_cost_repository set balances_id = 0 
 where balances_id is NULL and month_number = :a_month;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: updating balances_id on cr_cost_repository: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if

max_id = 0
select nvl(max(balances_id),0) into :max_id from cr_balances_id;

//**********************************************************
//  INSERT SQL:
//**********************************************************
f_pp_msgs("------Inserting into cr_balances_id at " + string(now()))

sqls           = "insert into cr_balances_id (balances_id,"
element_string = ""

for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element  = upper(i_ds_elements.GetItemString(i, "description"))
	include  = i_ds_elements.GetItemNumber(i, 'include_cr_balances')
	element  = f_cr_clean_string(element)
	
	if include = 1 then
		element_string = element_string + '"' + element + '", '
	end if
	
next

element_string = left(element_string, len(element_string) - 2)

sqls = sqls + element_string


//**********************************************************
//  SELECT SQL:
//**********************************************************
sqls = sqls + ") (" + &
	"select " + string(max_id) + " + rownum as balances_id, " + element_string + " from (" + &
	"select " + element_string + &
	 " from cr_cost_repository " + &
	 "where balances_id = 0 and month_number = " + string(a_month) + " minus " + &
	"select " + element_string + " from cr_balances_id ))"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: " + sqlca.SQLErrText)
	f_pp_msgs("------sqls : " + sqls)
	rollback;
	return -1
end if

commit;

f_pp_msgs("---uf_step1 finished at: " + string(now()))

return 1
end function

public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field);//******************************************************************************************
//
//  Window Function  :  wf_structure_value
//
//  Description      :  Builds the piece of the where clause for hierarchical queries
//								that use the cr_structures.
//
//******************************************************************************************
longlong comma_pos, start_pos, counter, the_last_comma_pos, string_length, i, status
string rollup_value_id, where_clause, description_edit_values[], rollup_value_id_list


where_clause         = " "
rollup_value_id_list = " ("


//-------  Parse out the rollup values into an array, if the user selected more  -----------
//-------  than 1 rollup value.                                                  -----------
uf_parse(a_description_edit, "{", description_edit_values)


for i = 1 to upperbound(description_edit_values)
	
	//-------  First, must parse out the value_id from the chosen element_value  ------------
	start_pos = 1
	counter   = 0

	if i_use_new_structures_table = "YES" then
		//  These need to be harcoded to 0 in case the element_value contains a comma.
		comma_pos = 0
		the_last_comma_pos = 0
	else
		do while true
		
			counter++
		
			//  Endless loop protection ...
			if counter > 1000 then
				messagebox("!!!", "Endless loop in wf_structure_value.", Exclamation!)
				exit
			end if
		
			comma_pos = pos(description_edit_values[i], ",", start_pos)
		
			if comma_pos = 0 then exit
		
			the_last_comma_pos = comma_pos
			start_pos          = comma_pos + 1
		
		loop
	end if

	string_length = len(description_edit_values[i])
	
	if i_use_new_structures_table = "YES" then
		//  "{CWIP}" goes to "CWIP}"
		rollup_value_id = right(description_edit_values[i], (string_length - the_last_comma_pos) - 1)
	else
		//  "{CWIP, 15}" goes to " 15}"
		rollup_value_id = right(description_edit_values[i], string_length - the_last_comma_pos)
	end if

	rollup_value_id = trim(left(rollup_value_id, len(rollup_value_id) - 1))
	
	status = 0
	if i_use_new_structures_table = "YES" then
		select status into :status from cr_structure_values2
		 where structure_id = :a_structure_id and element_value = :rollup_value_id;
	else
		select status into :status from cr_structure_values
		 where structure_id = :a_structure_id and value_id = :rollup_value_id;
	end if
		
	if isnull(status) or status = 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: the value_id (" + rollup_value_id + ") is INACTIVE on " + &
			" structure_id (" + string(a_structure_id) + ")")
		f_pp_msgs(" ")
		return "ERROR"
	end if
	
	if i_use_new_structures_table = "YES" then
		rollup_value_id = "'" + rollup_value_id + "'"
	end if

	if i = upperbound(description_edit_values) then
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ") "
	else
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ","
	end if


	//-------  Build the piece of the where clause to be passed back to the  ---------------- 
	//-------  calling script                                                ----------------
	if i = upperbound(description_edit_values) then
		
		if a_not_field = 0 then
			//  The user clicked the "NOT" checkbox.
			where_clause = where_clause + " " + a_description + " not in ("
		else
			where_clause = where_clause + " " + a_description + " in ("
		end if
		
		if i = 1 then
			if i_use_new_structures_table = "YES" then
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value = " + rollup_value_id + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id = " + rollup_value_id + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		else
			if i_use_new_structures_table = "YES" then
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value in " + rollup_value_id_list + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id in " + rollup_value_id_list + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		end if
	end if

next

return where_clause
end function

public function longlong uf_step2 (longlong a_month);//*********************************************************************************************
//
//  Step 2  :  Update the balances_id field on cr_cost_repository
//
//  NOTES   :  This script is best performed on conversion with a company/MN index and
//					using where clauses in the code below.  And in fact, while script 1 might run
//					without the indexes, this one will probably grind to a halt.
//
//*********************************************************************************************
longlong num_elements, i, include
string sqls, element

f_pp_msgs(" ")
f_pp_msgs("---Starting uf_step2 (Backfill balances_id into cr_cost_repository) at: " + string(now()))

f_pp_msgs("------Analyzing table cr_balances_id at " + string(now()))

sqlca.analyze_table('cr_balances_id');

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: analyzing cr_balances_id: " + sqlca.SQLErrText)
	rollback;
	return -1
end if


f_pp_msgs("------Updating cr_cost_repository at " + string(now()))

sqls = "update cr_cost_repository a set balances_id = ( select b.balances_id from cr_balances_id b " + &
	"where  a.balances_id = 0 and "

for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element  = upper(i_ds_elements.GetItemString(i, "description"))
	element  = f_cr_clean_string(element)
	include  = i_ds_elements.GetItemNumber(i, "include_cr_balances")
	
	if include = 1 then
		sqls = sqls + 'a."' + upper(element) + '" = b."' + upper(element) + '" and '
	end if
		
next

sqls = left(sqls,len(sqls) - 4)

sqls = sqls + ") where a.balances_id = 0 and month_number = " + string(a_month)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: updating cr_cost_repository.balances_id:  " + sqlca.SQLErrText)
	f_pp_msgs("------sqls = " + sqls)
	rollback;
	return -1
end if

commit;

f_pp_msgs("---uf_step2 finished at: " + string(now()))

return 1
end function

public function longlong uf_parse (string a_stringtoparse, string a_separator, ref string a_array_to_hold[]);//*****************************************************************************************
//
//  Window Function  :  uf_parse
//
//  Description      :  This function will parse string  into an array of elements denoted
//								by the separator.  The number of elements parsed is the return 
//								value of the function.  For example, if ~t were the separator, 
//								"red~twhite~tblue" would be parsed into an array containing red, 
//								white and blue.  The value 3 would be returned.
//
//  Notes            :  A MODIFIED VERSION OF:  f_parsestringintoarray from ppsystem.
//								This functions MUST INCLUDE the "separator" in the string that is
//								placed in the array (The "{").
//
//  Arguments :	a_stringtoparse :		string			      : string to parse
//						a_separator	    :		string			      : what to look for
//						a_array_to_hold : 	string array(by ref) : gets filled with values
//
//   
//  Returns   :  longlong :  The number of elements returned into the array.
//								  If the separator is not found then the original string is 
//                        placed in array_to_hold and 1 is returned.
//
//******************************************************************************************
longlong separator_pos, separator_len, count, start_pos
string holder

start_pos     = 1
separator_len = Len(a_separator)
separator_pos = Pos(a_stringtoparse, a_separator, start_pos)
IF Trim(a_stringtoparse) = "" or IsNull(a_stringtoparse) THEN 
	RETURN 0
ELSEIF separator_pos = 0 THEN 
	a_array_to_hold[1] = trim(a_stringtoparse)
	RETURN 1
END IF
DO WHILE separator_pos > 0
	if count = 0 then  //  Modified
		count++         //  Modified
		goto bottom		 //  Modified
	end if				 //  Modified
	holder = Mid(a_stringtoparse, start_pos - 1, (separator_pos - start_pos) + 1)  //  Modified
	a_array_to_hold[count] = trim(holder)
	count++  //  Moved below the previous line
	bottom:  //  Modified
	start_pos = separator_pos + separator_len
	separator_pos =  Pos(a_stringtoparse, a_separator, start_pos)	
LOOP
holder = Mid(a_stringtoparse, start_pos - 1, Len(a_stringtoparse))
//  NOT IN THIS WINDOW FUNCTION ... //count = count + 1
a_array_to_hold[count] = trim(holder)

RETURN count


end function

public function longlong uf_read ();longlong rtn
string   s_date
date     ddate
time     ttime
longlong	start_month, end_month, month, num_rows, i, counter, cr_bal_month
string	sqls, s_mon_s, e_mon_s
boolean	valid_month
datastore ds_months

i_ds_elements = CREATE uo_ds_top
sqls = 'select * from cr_elements order by "ORDER" '
f_create_dynamic_ds(i_ds_elements, "grid", sqls, sqlca, true)
i_num_elements = i_ds_elements.RowCount()


//*****************************************************************************************
//   ### 6202: Amount Type System Control
//*****************************************************************************************
select control_value
	into :i_amount_types
	from cr_system_control 
	where upper(control_name) = 'CR BALANCES: AMOUNT TYPES';
if isnull(i_amount_types) or i_amount_types = '' then i_amount_types = '1,3'

//*****************************************************************************************
//
//  Get the Structures Variable:  New variable to determine which structure values
//                                table is being used.
//
//*****************************************************************************************
setnull(i_use_new_structures_table)
select upper(rtrim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(rtrim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"

//************************************************************************
//  Call step 0:  Updating cr_month_number  (need this in step 0 below)
//************************************************************************
f_pp_msgs("Updating cr_month_number at " + string(now()))
insert into cr_month_number (month_number) (
	select distinct month_number from cr_cost_repository
	minus
	select month_number from cr_month_number);

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: inserting into cr_month_number: " + sqlca.SQLErrText)
	rollback;
	return -1 
else
	commit;
end if

ds_months = create datastore
sqls = "select month_number From cr_month_number order by month_number"
f_create_dynamic_ds(ds_months,'grid',sqls,sqlca,true)
num_rows = ds_months.rowcount()

//************************************************************************
//  Call step 0.5:  Check command line argument to make sure it is valid.
//************************************************************************
//  g_command_line_args has to be blank, or in the 'YYYYMM' 
//		or 'YYYYMM,YYYYYMM' formats
g_command_line_args = trim(g_command_line_args)

choose case len(g_command_line_args)
	case 0
		// Pull value from cr_alloc_system_control and interpret this as the same as a command line
		//		entry.  That way they can enter a range of months.
		select control_value into :g_command_line_args
			from cr_alloc_system_control 
			where upper(control_name) = 'CR BALANCES MONTH NUMBER';
		
		choose case len(g_command_line_args)
			case 6
				// Should be 'YYYYMM'.  Fine if its a valid date
				e_mon_s = mid(g_command_line_args,1,6)
				
				if not isnumber(e_mon_s) then
					// Error in SQL.  arg is not a valid date.
					f_pp_msgs("ERROR: Argument (" + g_command_line_args + ") is not valid.  Valid entries are:")
					f_pp_msgs(" ")
					f_pp_msgs("YYYYMM:         Updates CR Balances up to the input month.")
					f_pp_msgs("YYYYMM,YYYYMM:  Updates CR Balances for specified range of months.")
					f_pp_msgs(" ")
					return -1
				end if
		
			case 13
				// Should be 'YYYYMM,YYYYMM'.  Fine if they are both dates
				s_mon_s = mid(g_command_line_args,1,6)
				e_mon_s = mid(g_command_line_args,8,6)
				
				if isnumber(s_mon_s) and isnumber(e_mon_s) then
					// Converted to a valid date, looks like we are good.
					
					if long(s_mon_s) > long(e_mon_s) then
						// Switch the variables
						start_month = long(e_mon_s)
						end_month = long(s_mon_s)
					else
						start_month = long(s_mon_s)
						end_month = long(e_mon_s)
					end if
				else
					// Error in SQL.  arg is not a valid date.
					f_pp_msgs("ERROR: Argument (" + g_command_line_args + ") is not valid.  Valid entries are:")
					f_pp_msgs(" ")
					f_pp_msgs("YYYYMM:         Updates CR Balances up to the input month.")
					f_pp_msgs("YYYYMM,YYYYMM:  Updates CR Balances for specified range of months.")
					f_pp_msgs(" ")
					return -1
				end if
					
			case else
				f_pp_msgs("ERROR: Argument (" + g_command_line_args + ") is not valid.  Valid entries are:")
				f_pp_msgs(" ")
				f_pp_msgs("YYYYMM:         Updates CR Balances up to the input month.")
				f_pp_msgs("YYYYMM,YYYYMM:  Updates CR Balances for specified range of months.")
				f_pp_msgs(" ")
				return -1
		end choose


	case 6
		// Should be 'YYYYMM'.  Fine if its a valid date
		e_mon_s = mid(g_command_line_args,1,6)
		
		if not isnumber(e_mon_s) then
			// Error in SQL.  Command line arg is not a valid date.
			f_pp_msgs("ERROR: Command line argument (" + g_command_line_args + ") is not valid.  Valid entries are:")
			f_pp_msgs(" ")
			f_pp_msgs("Blank:          Updates CR Balances up to month from cr_alloc_system_control")
			f_pp_msgs("YYYYMM:         Updates CR Balances up to the input month.")
			f_pp_msgs("YYYYMM,YYYYMM:  Updates CR Balances for specified range of months.")
			f_pp_msgs(" ")
			return -1
		end if

	case 13
		// Should be 'YYYYMM,YYYYMM'.  Fine if they are both dates
		s_mon_s = mid(g_command_line_args,1,6)
		e_mon_s = mid(g_command_line_args,8,6)
		
		if isnumber(s_mon_s) and isnumber(e_mon_s) then
			// Converted to a valid date, looks like we are good.
			
			if long(s_mon_s) > long(e_mon_s) then
				// Switch the variables
				start_month = long(e_mon_s)
				end_month = long(s_mon_s)
			else
				start_month = long(s_mon_s)
				end_month = long(e_mon_s)
			end if
		else
			// Error in SQL.  Command line arg is not a valid date.
			f_pp_msgs("ERROR: Command line argument (" + g_command_line_args + ") is not valid.  Valid entries are:")
			f_pp_msgs(" ")
			f_pp_msgs("Blank:          Updates CR Balances up to month from cr_alloc_system_control")
			f_pp_msgs("YYYYMM:         Updates CR Balances up to the input month.")
			f_pp_msgs("YYYYMM,YYYYMM:  Updates CR Balances for specified range of months.")
			f_pp_msgs(" ")
			return -1
		end if
			
	case else
		f_pp_msgs("ERROR: Command line argument (" + g_command_line_args + ") is not valid.  Valid entries are:")
		f_pp_msgs(" ")
		f_pp_msgs("Blank:          Updates CR Balances up to month from cr_alloc_system_control")
		f_pp_msgs("YYYYMM:         Updates CR Balances up to the input month.")
		f_pp_msgs("YYYYMM,YYYYMM:  Updates CR Balances for specified range of months.")
		f_pp_msgs(" ")
		return -1
end choose

if trim(s_mon_s) = '' or isnull(s_mon_s) then
	// Using the blank, or one month option.  In this case, will do all months
	//		from max month in CR balances to selected month
	end_month = long(e_mon_s)

	// find the start month
	// Grab the largest month_number in CR_BALANCES
	select max(month_number)
	into :cr_bal_month
	from cr_balances;
	
	if isnull(cr_bal_month) then cr_bal_month = end_month
	
	if cr_bal_month >= end_month then
		// CR Balances has already been generated for months greater
		//		than what we are trying to generate now.  Only do one month.
		start_month = end_month
	else
		// Find the next month_number
		for i = 1 to num_rows
			if i = num_rows then 
				// JAK 05/06/06: cr_bal_month is the same as the max month
				//		in cr_month_number or is not in cr_month_number.
				//		Just start at cr_bal_month + 1, and let the valid month 
				//		check below prevent bad data from being built.
				start_month = cr_bal_month + 1
			elseif cr_bal_month = ds_months.GetItemNumber(i,1) then 
				// JAK 05/06/06: cr_bal_month is in cr_month_number.  Start with
				//		the next month in cr_month_number unless the next month is
				//		in the following year, then start with the 00 period.
				if mid(string(ds_months.GetItemNumber(i + 1,1)),1,4) <> mid(string(ds_months.GetItemNumber(i,1)),1,4) then
					start_month = long(mid(string(ds_months.GetItemNumber(i + 1,1)),1,4) + '00')
				else
					start_month = ds_months.GetItemNumber(i + 1,1)
				end if
				
				exit
			end if
		next
	end if
end if

if start_month >= end_month then
	// If cr_month_number doesn't have the end_month they want balances
	//		for this is possible.
	start_month = end_month
	setnull(e_mon_s)
end if


//************************************************************************
//  Call step 0.75:  Start interface
//************************************************************************			
if isnull(e_mon_s) then
	// Only processing one month
	f_pp_msgs("CR_Balances will be generated / regenerated for " + string(start_month))
else
	// Processing for a range of months
	f_pp_msgs("CR_Balances will be generated / regenerated for " + &
		string(start_month) + " to " + string(end_month))
end if

f_pp_msgs("  ")

for month = start_month to end_month
	
	valid_month = true
	
	if start_month <> end_month then
		// If they are doing a range of months, only run processing if month is in cr_month_number
		// JAK 05/06/06:  Also run for '00' months.
		valid_month = false
		for i = 1 to num_rows
			if month = ds_months.GetItemNumber(i,1) or mid(string(month),5,2) = '00' then 
				valid_month = true
				exit
			end if
		next
	end if
		
	if not valid_month then continue
			
	f_pp_msgs("_____________________________________________________________________________")
	f_pp_msgs("Starting cr_balances processes for " + string(month) + " at " + string(now()))	

	// JAK 09/09/2005 - No longer using cr_id, instead using balances_id.  Since the whole code
	//		block doesn't have to be used anymore
	
	counter = 0 
	select count(*) into :counter 
		from cr_cost_repository
		where month_number = :month
		and 	(balances_id is null or balances_id = 0);
		
	if sqlca.SQLCode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Checking summary table for records with blank balances_ids: " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		rollback;
		return -1 
	end if
	
	if counter = 0 then

		// Skip Steps 1 and 2
		f_pp_msgs(" ")
		f_pp_msgs("---No need for Steps 1 and 2 for month " + string(month) + ".")


	else
		
		//************************************************************************
		//  Call step 1:  Maintain cr_balances_id.
		//************************************************************************
		
		rtn = uf_step1(month)
		
		if rtn <> 1 then return -1
		
		
		//************************************************************************
		//  Call step 2:  Update the balances_id field on cr_cost_repository.
		//************************************************************************
		
		rtn = uf_step2(month)
		
		if rtn <> 1 then return -1
	
	end if
	
	//************************************************************************
	//  Call step 3:  Building cr_balances
	//************************************************************************
	
	rtn = uf_step3(month)
	
	if rtn <> 1 then return -1

next

//************************************************************************
//  Delete from cr_balances where month_number > end month.  This data
//	 	won't be accurate anymore so to avoid confusion just get rid of it
//************************************************************************
f_pp_msgs(" ")
f_pp_msgs("Deleting future data from cr_balances (to prevent invalid " + &
	"data from being displayed) at " + string(now()))
	
delete from cr_balances where month_number > :end_month;

if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Deleting future months in CR Balances: " + sqlca.SQLErrText)
	f_pp_msgs(" ")
	rollback;
	return -1 
end if

//************************************************************************
//  Log the finishing time.
//************************************************************************
s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))

g_finished_at = datetime(ddate, ttime)

f_pp_msgs(" ")
f_pp_msgs("CR_balances update complete at " + string(now()))

return 1
end function

public function longlong uf_step3 (longlong a_month);//*********************************************************************************************
//
//  Step 3  :  Building cr_balances for current month
//
//  NOTES   :  Steps are: 	1)  Clear current month from cr_balances.  
//									2)  Roll forward previous month from cr_balances.
//									3)  Insert missing BALANCES_IDs for current month.
//									4)  Updating balances from cr_cost_repository.
//
//*********************************************************************************************
string sqls, element_string, element, where_sqls, left_paren, col_name, operator, &
	value1, value2, between_and, right_paren, and_or, structure_where, update_sqls
longlong num_rows, i, prev_month, include, structure_id, not_field, first_month
uo_ds_top ds_where_clause,  ds_months, ds_income_accounts

f_pp_msgs(" ")
f_pp_msgs("---Building cr_balances for current month at: " + string(now()))


element_string = ' '
for i = 1 to i_num_elements
	//  Create the Accounting Key string ...
	element  = upper(i_ds_elements.GetItemString(i, "description"))
	include  = i_ds_elements.GetItemNumber(i, "include_cr_balances")
	element  = f_cr_clean_string(element)
	
	if include = 1 then
		element_string = element_string + '"' + upper(element) + '", '
	end if
next

element_string = left(element_string, len(element_string) - 2)

//*****************************************************************
//  1:  Clearing current month
//*****************************************************************
f_pp_msgs("------Clearing cr_balances for month at " + string(now()))

delete from cr_balances where month_number = :a_month;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: Clearing current month from cr_balances: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if

//*****************************************************************
//  2:  Rolling previous month forward
//*****************************************************************
f_pp_msgs("------Rolling forward balances from previous month at " + string(now()))

// JAK 05/06/2006:  Since the way it chooses the months to build, we always know the previous
//		month has been built in cr_balances.  Thus, we'll find our previous month just by looking 
//		in cr_balances

prev_month = 0

select max(month_number) into :prev_month from cr_balances where month_number < :a_month;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: Finding previous month from cr_balances: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	if isnull(prev_month) or prev_month = 0 then
		// First month in cr_month_number so no historical data to roll forward.
		goto insert_missing
	end if
end if

sqls = 'insert into cr_balances ("BALANCES_ID", "MONTH_NUMBER", "AMOUNT_TYPE", "QUANTITY", "AMOUNT", ' + &
	element_string + ') select "BALANCES_ID", ' + string(a_month) + ', "AMOUNT_TYPE", "QUANTITY", "AMOUNT", ' + &
	element_string + ' from cr_balances where month_number = ' + string(prev_month)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: Rolling forward previous month: " + sqlca.SQLErrText)
	f_pp_msgs("------SQL: " + sqls)
	rollback;
	return -1
end if


//*****************************************************************
//  3:  Insert missing Balances_ids
//*****************************************************************
insert_missing:
f_pp_msgs("------Insert missing balances_ids at " + string(now()))

sqls = 'insert into cr_balances ("BALANCES_ID", "MONTH_NUMBER", "AMOUNT_TYPE", "QUANTITY", "AMOUNT", ' + &
	element_string + ') select "BALANCES_ID", ' + string(a_month) + ', "AMOUNT_TYPE", 0, 0, ' + element_string
	
sqls = sqls + ' from cr_cost_repository where balances_id > 0 and amount_type in (' + i_amount_types + ') and month_number = ' + string(a_month)

sqls = sqls + ' minus select "BALANCES_ID", ' + string(a_month) + &
	', "AMOUNT_TYPE", 0, 0, ' + element_string
	
sqls = sqls + ' from cr_balances where month_number = ' + string(a_month)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: Inserting missing BALANCES_IDs: " + sqlca.SQLErrText)
	f_pp_msgs("------SQL: " + sqls)
	rollback;
	return -1
end if
	
	
//*****************************************************************
//  4:  Checking for Balances on Income Accounts
//*****************************************************************
select upper(trim(control_value)) 
	into :first_month
	from cr_alloc_system_control 
	where upper(trim(control_name)) = upper(trim('Initial Month of Fiscal Year'));

// If it's 1 then zero out balances in period 0. 
if isnull(first_month) or first_month <= 1 or first_month > 12 then first_month = 0

if long(mid(string(a_month),5,2)) = first_month then
	
	f_pp_msgs("------Performing year-end rollforward at " + string(now()))
	
	sqls = "select * from cr_balances_ye_close order by row_id"
	ds_where_clause = create uo_ds_top
	f_create_dynamic_ds(ds_where_clause, "grid", sqls, sqlca, true)
	num_rows = ds_where_clause.RowCount()
	
	if num_rows = 0 then
		f_pp_msgs(' ')
		f_pp_msgs('ERROR:  No Income Account criteria is in the system!')
		f_pp_msgs(' ')
	end if 
	
	for i = 1 to num_rows
		
		if i = 1 then
			where_sqls = ' month_number = ' + string(a_month) + ' and ('
		end if
		
		left_paren  = upper(trim(ds_where_clause.GetItemString(i, "left_paren")))
		col_name    = upper(trim(ds_where_clause.GetItemString(i, "column_name")))
		operator    = upper(trim(ds_where_clause.GetItemString(i, "operator")))
		between_and = upper(trim(ds_where_clause.GetItemString(i, "between_and")))
		right_paren = upper(trim(ds_where_clause.GetItemString(i, "right_paren")))
		and_or      = upper(trim(ds_where_clause.GetItemString(i, "and_or")))
		structure_id = ds_where_clause.GetItemNumber(i, "structure_id")		
		value1 = ds_where_clause.GetItemString(i, "value1")
				
		if value1 = " " then
			//  Single-space is OK.
		else
			if (i_use_new_structures_table = "YES" and left(trim(value1), 1) = "{") then
				value1 = trim(value1)
			else
				value1 = upper(trim(value1))
			end if
		end if
		
		value2 = ds_where_clause.GetItemString(i, "value2")
		
		if value2 = " " then
			//  Single-space is OK.
		else
			if (i_use_new_structures_table = "YES" and left(trim(value2), 1) = "{") then
				value2 = trim(value2)
			else
				value2 = upper(trim(value2))
			end if
		end if
				
		if isnull(left_paren)  then left_paren  = ""
		if isnull(col_name)    then col_name    = ""
		if isnull(operator)    then operator    = ""
		if isnull(value1)      then value1      = ""
		if isnull(between_and) then between_and = ""
		if isnull(value2)      then value2      = ""
		if isnull(right_paren) then right_paren = ""
		if isnull(and_or)      then and_or      = ""
		if isnull(structure_id) then structure_id = 0
		
		col_name = f_cr_clean_string(col_name)
		
		if left(trim(value1), 1) = "{" then
			//  The user selected a structure value ...
			choose case operator
				case "=", "IN"
					//  EQUAL TO the structure value ...
					not_field = 1
				case "<>", "NOT IN"
					//  NOT EQUAL TO the structure value ...
					not_field = 0
				case else
			end choose
			
			structure_where = uf_structure_value(structure_id, col_name, value1, not_field)
			
			where_sqls = where_sqls + left_paren + structure_where + right_paren + " " + and_or + " "
			goto bottom
		end if
		
		if between_and = "" then
			if operator = "IN" or operator = "NOT IN" then
				where_sqls = where_sqls + &
					left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
					value1 + " " + &
					right_paren + " " + and_or + " "
			else
				where_sqls = where_sqls + &
					left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
					value1 + "' " + &
					right_paren + " " + and_or + " "
			end if
		else
			where_sqls = where_sqls + &
				left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
				value1 + "' " + between_and + " '" + value2 + "' " + &
				right_paren + " " + and_or + " "
		end if
		
		bottom:
		
		if i = num_rows then
			where_sqls = where_sqls + " )"
		end if
		
	next
	
	sqls = 'select count(*) count from (select ' + element_string + &
		' from cr_balances where ' + where_sqls + ' group by ' + element_string + &
		' having sum(amount) <> 0 or sum(quantity) <> 0)'
	update_sqls = 'update cr_balances set amount = 0, quantity = 0 where ' + where_sqls
	
	ds_income_accounts = create uo_ds_top
	f_create_dynamic_ds(ds_income_accounts, "grid", sqls, sqlca, false)
	ds_income_accounts.SetTransObject(sqlca)
	num_rows = ds_income_accounts.retrieve()
	
	if ds_income_accounts.i_sqlca_sqlcode < 0 or num_rows < 0 then
		f_pp_msgs("------ERROR: Overriding income account balances: " + ds_income_accounts.i_sqlca_sqlerrtext)
		rollback;
		return -1
	end if
	
	num_rows = ds_income_accounts.GetItemNumber(1,'count')
	
	if num_rows > 0 then
		// There are income accounts with a none zero balance on them!  This isn't allowed in period 00.
		f_pp_msgs("------Updating income accounts to 0 at " + string(now()))
		
		execute immediate :update_sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("------ERROR: Overriding income account balances: " + sqlca.SQLErrText)
			rollback;
			return -1
		end if
	end if
end if


//*****************************************************************
//  5:  Updating balances
//*****************************************************************
f_pp_msgs("------Updating quantities and amounts at " + string(now()))

sqlca.analyze_table('cr_balances');
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("WARNING: the analyze_table function failed on cr_balances.")
	f_pp_msgs("The process WILL continue.")
	f_pp_msgs("  ")
end if

update cr_balances a 
	set amount = amount +
		(select nvl(sum(amount),0)
		from cr_cost_repository b 
		where month_number = :a_month
		and b.balances_id = a.balances_id
		and a.amount_type = b.amount_type), 
	quantity = quantity + 
		(select nvl(sum(quantity),0)
		from cr_cost_repository b 
		where month_number = :a_month
		 and b.balances_id = a.balances_id
		 and a.amount_type = b.amount_type) 
	where a.month_number = :a_month
	and exists 
		(select id
		from cr_cost_repository b 
		where month_number = :a_month
		 and b.balances_id = a.balances_id
		 and a.amount_type = b.amount_type);

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: Updating amounts/quantities in cr_balances: " + sqlca.SQLErrText)
	f_pp_msgs("------SQL: " + sqls)
	rollback;
	return -1
end if


//*****************************************************************
//  6:  Clearing zero balances entries
//*****************************************************************
f_pp_msgs("------Clearing zero balances entries at " + string(now()))

delete from cr_balances where quantity = 0 and amount = 0 and month_number = :a_month;

if sqlca.SQLCode < 0 then
	f_pp_msgs("------ERROR: Clearing zero balances entries: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

f_pp_msgs("---uf_step3 finished at: " + string(now()))

commit;

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_balances_custom_version nvo_cr_balances_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_balances_custom.pbd'
		return nvo_cr_balances_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

