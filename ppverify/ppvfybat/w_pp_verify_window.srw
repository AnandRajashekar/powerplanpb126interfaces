HA$PBExportHeader$w_pp_verify_window.srw
forward
global type w_pp_verify_window from window
end type
type dw_subledger_control_choose from datawindow within w_pp_verify_window
end type
type dw_pp_task_list from datawindow within w_pp_verify_window
end type
type dw_email from datawindow within w_pp_verify_window
end type
type d3 from datawindow within w_pp_verify_window
end type
type dw_vfy_wo_in_service_date from datawindow within w_pp_verify_window
end type
type dw_vfy_wo_summ_to_charge_summ from datawindow within w_pp_verify_window
end type
type st_3 from statictext within w_pp_verify_window
end type
type st_2 from statictext within w_pp_verify_window
end type
type st_1 from statictext within w_pp_verify_window
end type
type dw_pp_verify_batch_auto from datawindow within w_pp_verify_window
end type
type dw_pp_verify_batch_manual from datawindow within w_pp_verify_window
end type
type dw_pp_verify_results from datawindow within w_pp_verify_window
end type
end forward

global type w_pp_verify_window from window
boolean visible = false
integer width = 2533
integer height = 1408
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
dw_subledger_control_choose dw_subledger_control_choose
dw_pp_task_list dw_pp_task_list
dw_email dw_email
d3 d3
dw_vfy_wo_in_service_date dw_vfy_wo_in_service_date
dw_vfy_wo_summ_to_charge_summ dw_vfy_wo_summ_to_charge_summ
st_3 st_3
st_2 st_2
st_1 st_1
dw_pp_verify_batch_auto dw_pp_verify_batch_auto
dw_pp_verify_batch_manual dw_pp_verify_batch_manual
dw_pp_verify_results dw_pp_verify_results
end type
global w_pp_verify_window w_pp_verify_window

on w_pp_verify_window.create
this.dw_subledger_control_choose=create dw_subledger_control_choose
this.dw_pp_task_list=create dw_pp_task_list
this.dw_email=create dw_email
this.d3=create d3
this.dw_vfy_wo_in_service_date=create dw_vfy_wo_in_service_date
this.dw_vfy_wo_summ_to_charge_summ=create dw_vfy_wo_summ_to_charge_summ
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.dw_pp_verify_batch_auto=create dw_pp_verify_batch_auto
this.dw_pp_verify_batch_manual=create dw_pp_verify_batch_manual
this.dw_pp_verify_results=create dw_pp_verify_results
this.Control[]={this.dw_subledger_control_choose,&
this.dw_pp_task_list,&
this.dw_email,&
this.d3,&
this.dw_vfy_wo_in_service_date,&
this.dw_vfy_wo_summ_to_charge_summ,&
this.st_3,&
this.st_2,&
this.st_1,&
this.dw_pp_verify_batch_auto,&
this.dw_pp_verify_batch_manual,&
this.dw_pp_verify_results}
end on

on w_pp_verify_window.destroy
destroy(this.dw_subledger_control_choose)
destroy(this.dw_pp_task_list)
destroy(this.dw_email)
destroy(this.d3)
destroy(this.dw_vfy_wo_in_service_date)
destroy(this.dw_vfy_wo_summ_to_charge_summ)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_pp_verify_batch_auto)
destroy(this.dw_pp_verify_batch_manual)
destroy(this.dw_pp_verify_results)
end on

event open;longlong rtn

//*****************************************************************************************
//
//  INTERFACE code ...
//
//*****************************************************************************************
uo_pp_verify    u_uo_pp_verify
u_uo_pp_verify    = CREATE uo_pp_verify
rtn = u_uo_pp_verify.uf_read() 
DESTROY uo_pp_verify

if rtn = 1 then
	g_rtn_code = 0 
	f_pp_msgs("PowerPlant Verify finished.")
elseif rtn = 0 then
	g_rtn_code = 0
	f_pp_msgs("PowerPlant Verify  finished.")
else
	g_rtn_code = -100	
	f_pp_msgs("PowerPlant Verify failed. Please correct the errors and run it again.")
end if


//*****************************************************************************************
//
//  HALT the application ...
//
//*****************************************************************************************
//if g_rtn_code = 0 then
//	f_pp_msgs("PowerPlant Verify ran successfully.")
//end if

halt close
end event

type dw_subledger_control_choose from datawindow within w_pp_verify_window
integer x = 1216
integer y = 764
integer width = 1093
integer height = 108
integer taborder = 50
boolean titlebar = true
string title = "dw_subledger_control_choose"
string dataobject = "dw_subledger_control_choose"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_task_list from datawindow within w_pp_verify_window
integer x = 73
integer y = 404
integer width = 1088
integer height = 96
integer taborder = 40
boolean titlebar = true
string title = "dw_pp_task_list"
string dataobject = "dw_pp_task_list"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_email from datawindow within w_pp_verify_window
integer x = 1216
integer y = 304
integer width = 1088
integer height = 96
integer taborder = 40
boolean titlebar = true
string title = "dw_email"
string dataobject = "dw_email"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type d3 from datawindow within w_pp_verify_window
integer x = 78
integer y = 720
integer width = 1088
integer height = 96
integer taborder = 40
boolean titlebar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_vfy_wo_in_service_date from datawindow within w_pp_verify_window
integer x = 78
integer y = 620
integer width = 1088
integer height = 96
integer taborder = 10
boolean titlebar = true
string title = "dw_vfy_wo_in_service_date"
string dataobject = "dw_vfy_wo_in_service_date"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_vfy_wo_summ_to_charge_summ from datawindow within w_pp_verify_window
integer x = 1216
integer y = 620
integer width = 1088
integer height = 96
integer taborder = 50
boolean titlebar = true
string title = "dw_vfy_wo_summ_to_charge_summ"
string dataobject = "dw_vfy_wo_summ_to_charge_summ"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_pp_verify_window
integer x = 101
integer y = 556
integer width = 567
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Verify Datawindows:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_pp_verify_window
integer x = 1001
integer y = 20
integer width = 1431
integer height = 108
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Leave these datawindows here!!!   The need to be here so Powerbuilder will include them in the standalone exe."
boolean focusrectangle = false
end type

type st_1 from statictext within w_pp_verify_window
integer x = 101
integer y = 140
integer width = 567
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "System Datawindows:"
boolean focusrectangle = false
end type

type dw_pp_verify_batch_auto from datawindow within w_pp_verify_window
integer x = 1216
integer y = 204
integer width = 1088
integer height = 96
integer taborder = 50
boolean titlebar = true
string title = "dw_pp_verify_batch_auto"
string dataobject = "dw_pp_verify_batch_auto"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_verify_batch_manual from datawindow within w_pp_verify_window
integer x = 73
integer y = 204
integer width = 1088
integer height = 96
integer taborder = 40
boolean titlebar = true
string title = "dw_pp_verify_batch_manual"
string dataobject = "dw_pp_verify_batch_manual"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_verify_results from datawindow within w_pp_verify_window
integer x = 73
integer y = 304
integer width = 1088
integer height = 96
integer taborder = 30
boolean titlebar = true
string title = "dw_pp_verify_results"
string dataobject = "dw_pp_verify_results"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

