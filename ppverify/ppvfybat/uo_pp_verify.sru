HA$PBExportHeader$uo_pp_verify.sru
$PBExportComments$Maint 8655: change PP_VERIFY_USER_ COMMENTS id from max_id+1 to pwrplant1.nextval
forward
global type uo_pp_verify from nonvisualobject
end type
end forward

global type uo_pp_verify from nonvisualobject
end type
global uo_pp_verify uo_pp_verify

type variables
uo_ds_top i_ds_gl_trans_joint_interco

longlong i_records_processed, i_records_to_process, i_process_limit, &
         i_min_month_number, i_max_month_number, i_company[]
string   i_comp 
longlong 		i_start_record, i_month_number

datetime i_month

longlong i_cr_recs
longlong i_ap_recs
longlong i_mat_recs

//u_prog_interface g_prog_interface
string i_filename
string i_exe_name = "ppverify.exe"
end variables

forward prototypes
public function longlong uf_x_process_subledger_dw (longlong a_company[], string a_sqls, longlong a_verify_id)
public function longlong uf_read ()
end prototypes

public function longlong uf_x_process_subledger_dw (longlong a_company[], string a_sqls, longlong a_verify_id);string modified_sqls, rc , subledger_name[], sqls, company_str, started_at, finished_at, pp_msg, sqls_lock
longlong subledger_count, i, depr_indicator[], nrows, total_rows, nrows1, ret, j, rtn,num_criteria

total_rows = 0
company_str = f_parsearrayby254(a_company, 'N', 'company_id')

datastore ds_temp_dynamic
ds_temp_dynamic = CREATE datastore
ds_temp_dynamic.DataObject = "dw_temp_dynamic"
ds_temp_dynamic.SetTransObject(sqlca)

datastore ds_subledger_control_choose
ds_subledger_control_choose = CREATE datastore
ds_subledger_control_choose.DataObject = "dw_subledger_control_choose"
ds_subledger_control_choose.SetTransObject(sqlca)

uo_ds_top ds_pp_verify_results
ds_pp_verify_results   = CREATE uo_ds_top
ds_pp_verify_results.DataObject = "dw_pp_verify_results"
ds_pp_verify_results.SetTransObject(sqlca)


 
ds_subledger_control_choose.SetTransObject(sqlca)
ds_subledger_control_choose.Retrieve()
subledger_count = ds_subledger_control_choose.rowcount()

for i = 1 to subledger_count 
  subledger_name[i] = ds_subledger_control_choose.GetItemString(i, 'subledger_name')
  depr_indicator[i] = ds_subledger_control_choose.GetItemnumber(i, 'depreciation_indicator')   
next

//Delete previous results ..
		sqls = 'delete from pp_verify_results where verify_id = ' + string(a_verify_id ) +  &
		 '  and (' + company_str + ' or company_id = 0)'
		 execute immediate :sqls;

		if sqlca.sqlcode < 0 then
			messagebox("", "cannot delete previous results." + ' ' + sqlca.sqlerrtext)
			return -1
		end if

for i = 1 to subledger_count

	modified_sqls = a_sqls //reset
	
	modified_sqls = f_replace_string(modified_sqls,'TEMPLATE_DEPR',& 
							 UPPER(subledger_name[i]) + '_DEPR' ,'all')
							 
	modified_sqls = f_replace_string(modified_sqls,'TEMPLATE_BASIS',& 
							 UPPER(subledger_name[i]) + '_BASIS' ,'all')	
							 
	modified_sqls = f_replace_string(modified_sqls,'TEMPLATE_NAME',& 
							 UPPER(subledger_name[i])  ,'all')	
							 
	modified_sqls = f_replace_string(modified_sqls,'SUBLEDGER_TEMPLATE',& 
							 UPPER(subledger_name[i])  ,'all')								 
				 
	modified_sqls = 'datawindow.table.select = "' + modified_sqls + '"' 
		
	rc = ds_temp_dynamic.Modify(modified_sqls)
	
	started_at = string(today()) + ' ' + string(now())
	
	pp_msg = '--' + subledger_name[i] + ': Started at ' +  started_at
	f_pp_msgs(pp_msg) 
	 
	
	
	if rc <>  "" then
		f_pp_msgs( "Error modifying sql. " + rc)
		return -1
	else
		f_pp_msgs("   Processing " + subledger_name[i] + ' ...')
	end if  
	
	ds_temp_dynamic.settransobject(sqlca)
	nrows = ds_temp_dynamic.retrieve(a_company) 
	
	for j = 1 to nrows 
		total_rows = total_rows + 1 
		ds_temp_dynamic.setitem(j, 'verify_id', a_verify_id)
		ds_temp_dynamic.setitem(j, 'dummy_id', total_rows) 
	next

	ds_pp_verify_results.settransobject(sqlca)

	if nrows = 0 then 
		pp_msg = '--' + subledger_name[i] + ': Passed validation with no errors'
		f_pp_msgs(pp_msg) 
		 
	else
		pp_msg = '--' + subledger_name[i] + ': Failed validation with '+ string(nrows) +' errors'
		f_pp_msgs(pp_msg) 
		 
					
 	//	dw_temp_dynamic.saveas(i_filename, text!, false)
	//	nrows1 = dw_pp_verify_results.importfile(i_filename)
	
		if nrows1 <> nrows then
			messagebox("", "cannot read the  results."+ ' ' + sqlca.sqlerrtext) 
			return -1
		end if
	
		

		ret = ds_pp_verify_results.update()
		if ret = 1 then
			commit;
			sqls_lock = "lock table ppverify_lock in exclusive mode nowait"
			execute immediate :sqls_lock;
			
			if sqlca.SQLCode < 0 then
				/* PPVERIFY is already running */
				f_write_log(g_log_file, "PPVERIFY is already running: " + sqlca.SQLErrText)
				g_prog_interface.uf_Exit(-1)
				halt
			end if
		else
			messagebox("", "cannot save the  results." + ' ' + sqlca.sqlerrtext) 
			return -1
		end if  
	 
	end if //if nrows <> 0 then  
	
	finished_at = string(today()) + ' ' + string(now())
	pp_msg = '--' + subledger_name[i] + ': Finished at ' +  started_at
	f_pp_msgs(pp_msg)
	 
	
next
		
  return 1
end function

public function longlong uf_read ();//************************************************************************************************
// PowerPlant Alerts / Verification System
//
//	uo_pp_verify(uf_read)
//
// Purpose:  Stand-alone executable that is called from a batch moniter.  It loops over 
//					all alerts/verifys that have been set up in pp_verify_batch and executes
//					them based on their frequency parameter.  Also can run a manual batch when 
//					called from w_pp_verify2.
//
//	Actions:	 If alert is enabled, an email will be sent to sepcified user(s) for each error
//					condition.  All error conditions found will be archived to pp_verify_results.
//
//	7/1/03 kab
//************************************************************************************************
longlong rtn, i, j, k, l, m, n, o, verify_id, nrows, nrows2, nrows3, company[], find_depr, all_companies[], task_list_id
longlong company_id, user_alert, ncols, seq_no, frequency, next_notification, result_id, result_row, attach_row
longlong status, subsystem, version_id , i_subl, subledger_count, nrows_subl,msg_size
string datawindow, pp_msg, sqls, verify_desc, new_sql, rc, ret, col[], null_string_array[]
string sql, started_at, finished_at, user, company_str, user_sql, alert_message, find_str
string email_subject, from_user, to_user, to_user_email, ini_file, mail_server
string col_type, col_name, all_companies_str, attach_file, file_directory, str,sql_subl
string subledger_name[], modified_sqls, vfy_descr, comp_descr, msg_size_str, email_link_name, ppt_link, mobile_link, mobile_ip, oracle_mail_flag
dec {8} tolerance
datetime last_sent, run_time, first_sent
boolean send_email, new_result, mail_rtn, mobile_alert
uo_ds_top ds_temp, ds_pp_verify, ds_pp_verify_results, ds_users, ds_email, ds_pp_task_list, ds_temp_subl, ds_email_excel

longlong COMMENT_ALERT, COMMENT_REQUIRED, MAX_ID
STRING PRIMARY_KEY_FIELD
string col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15
string col16,col17,col18,col19,col20,col21,col22,col23,col24,col25,col26,col27,col28,col29,col30
string sql1, sql2, sql3, sql4, sql5

// ### CDM
longlong attach_excel
string sqls2
// end CDM
string	str_filter, sqls_lock, sqls2_user, str_value, order_by_clause, order_by_array[], order_column
longlong	ncols_excel, pos
string html_alert_message


//********************************************************************************************
//
//  Setup ...
//
//********************************************************************************************
// Setup datastores
ds_temp					= CREATE uo_ds_top
ds_temp_subl			= CREATE uo_ds_top
ds_pp_verify  			= CREATE uo_ds_top
ds_pp_verify_results = CREATE uo_ds_top
ds_users					= CREATE uo_ds_top
ds_email					= CREATE uo_ds_top
ds_pp_task_list		= CREATE uo_ds_top
ds_pp_verify_results.DataObject = "dw_pp_verify_results"
ds_pp_task_list.DataObject = "dw_pp_task_list"
ds_pp_verify_results.SetTransObject(sqlca)
ds_pp_task_list.SetTransObject(sqlca)

uo_ds_top ds_subledger_control 
ds_subledger_control = CREATE uo_ds_top
ds_subledger_control.DataObject = "dw_subledger_control_choose"
ds_subledger_control.SetTransObject(sqlca)

ds_email_excel			= CREATE uo_ds_top

// ### CDM
uo_ds_top ds_attach_excel
ds_attach_excel = CREATE uo_ds_top
// end CDM


// Save off current time & date
select sysdate into :run_time from dual;

// Get the smpt mail server
ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")
mail_server = ProfileString(ini_file, "Mail", "SMTP_HOST", "")
oracle_mail_flag = ProfileString(ini_file, "Mail", "SMTP_ORACLE", "")

if oracle_mail_flag = '1' then
	f_pp_msgs ("Mail will be sent through Oracle.")
elseif isnull(mail_server) or trim(mail_server) = "" then
	f_pp_msgs ("Cannot find the mail server ('SMTP_HOST') in pwrplant.ini: " + ini_file)
	return -1
end if

// PP-45359: read from the INI file to look up the email option to determine which user 
// object to use to create g_msmail
if ProfileInt(ini_file, "Mail", "SMTPMAIL", 0) = 1 then
	g_msmail = create uo_smtpmail
else
	g_msmail = create uo_msmail
end if

//ayp: we get it again later, no need to do it here
// Defaults
//select Control_value into :email_subject
//from pp_system_control_company
//where trim(upper(control_name)) = 'ALERT EMAIL SUBJECT'
//and company_id = :company_id;
//
//if isnull(email_subject) or email_subject = '' then 
//	select Control_value into :email_subject
//	from pp_system_control_company
//	where trim(upper(control_name)) = 'ALERT EMAIL SUBJECT'
//	and company_id = -1;
//end if
//
//if isnull(email_subject) or email_subject = '' then 
//	email_subject = 'PowerPlant Alerts' 
//else
//	// Replace <db> variable in the email subject line with the database name
//	email_subject = f_replace_string( f_replace_string( email_subject, "<db>", sqlca.servername, "all"), "<DB>", sqlca.servername, "all")
//end if

ds_email.dataobject = "dw_email"

select control_value into :from_user 
from pp_system_control where lower(control_name) = 'alert email address';

if isnull(from_user) or from_user = '' then
	from_user = 'pwrplant@pwrplan.com'
end if


// Get max result_id from pp_verify_results
select max(result_id) into :result_id from pp_verify_results;
if isnull(result_id) or result_id < 0 then result_id = 0

// Get max task_list_id from pp_task_list
select max(task_list_id) into :task_list_id from pp_task_list;
if isnull(task_list_id) or task_list_id < 0 then task_list_id = 0



select nvl(max(id),0) into :MAX_ID from PP_VERIFY_USER_COMMENTS;




//********************************************************************************************
//
//  Setup the all companies array and string...
//
//********************************************************************************************
sqls = "select company_id from company_setup"
f_create_dynamic_ds(ds_temp,'grid',sqls,sqlca,true)
all_companies = ds_temp.object.company_id.primary
all_companies_str = string(ds_temp.getitemnumber(1,1))
for i = 2 to ds_temp.rowcount()
	all_companies_str = all_companies_str + "," + string(ds_temp.getitemnumber(i,1))
next


//********************************************************************************************
//
//  Get list of alerts/verifys-companies to be run ...
//
//********************************************************************************************
if g_manual_batch then 
	// "dw_pp_verify_batch_auto" selects all MANUAL alerts/verifys
	ds_pp_verify.DataObject = "dw_pp_verify_batch_manual"
	f_pp_msgs ( upper(g_manual_batch_type) +  " PROCESS")
	f_pp_msgs ("")
else
	// "dw_pp_verify_batch_auto" selects all alerts/verifys which meet the following criteria:
	//		1. pp_verify.enable_disable = 1 (enabled)
	//		2. pp_verify_batch.manual_batch = 0 (auto batch)
	//		3.	pp_verify_batch.frequency >= 0 (not inactive)
	//		4. pp_verify_batch.next_run < sysdate (the current time has surpassed the next_run
	//										parameter that is automatically set at the end of this 
	//										executable or at the time the batch was set up)
	ds_pp_verify.DataObject = "dw_pp_verify_batch_auto"
	f_pp_msgs ("Auto Batch")
	f_pp_msgs ("")
end if
ds_pp_verify.SetTransObject(sqlca)
nrows = ds_pp_verify.retrieve() 

if nrows < 0 then
	f_pp_msgs("ERROR: in pp_verify retrieve: " + ds_pp_verify.i_sqlca_sqlerrtext )
	return -1
elseif nrows = 0 then
	f_pp_msgs("Retrieve: no verify/alerts rows found to process in pp_verify table .")
	return 1
end if


//********************************************************************************************
//
//  If we are running a manual batch, we can now delete those rows from pp_verify_batch 
//   bescause we already have them in the datawindow buffer ...
//
//********************************************************************************************
if g_manual_batch then
	delete from pp_verify_batch where manual_batch = 1;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs(string(today(), 'mm/dd/yy hh:mm:ss') + "Error deleting manual batch: " + sqlca.sqlerrtext)
		rollback;
		return -1
	else
		commit;
		sqls_lock = "lock table ppverify_lock in exclusive mode nowait"
		execute immediate :sqls_lock;
		
		if sqlca.SQLCode < 0 then
			/* PPVERIFY is already running */
			f_write_log(g_log_file, "PPVERIFY is already running: " + sqlca.SQLErrText)
			g_prog_interface.uf_exit(-1)
			halt
		end if
	end if
end if


//********************************************************************************************
//
//  Loop through Alerts/Verifys-companies ...
//
//********************************************************************************************
for i = 1 to nrows
	
	/************** Save Alert/Verify info to variables **************/
	verify_id   	= ds_pp_verify.getitemnumber(i, 	'verify_id')
	company_id		= ds_pp_verify.getitemnumber(i, 	'company_id')
	
	email_link_name = f_pp_system_control_company("EMAIL LINK NAME", company_id)
	if isnull(email_link_name) or email_link_name = '' then email_link_name = 'Login To PowerPlant'
	
	ppt_link = f_pp_system_control_company("POWERPLANT EMAIL LINK", company_id)
	if isnull(ppt_link) or ppt_link = '' then 
		ppt_link = ' '
	else
		ppt_link = '<br /><br /><A href="'+ppt_link+'">'+email_link_name+'</A>'
	end if
	
	//create the link to mobile approvals
	mobile_link = f_pp_system_control_company("Mobile Approvals IP", company_id)
	if isnull(mobile_link) or mobile_link = '' then
		mobile_link = ' '
	else
		mobile_link = '<br /><br /><A href="'+mobile_link+'">'+'Login to PowerPlan Mobile Approvals'+'</A>'
	end if
	
	sqls2 = ""			
	attach_excel = 0
	attach_excel = ds_pp_verify.getitemnumber(i, 'attach_as_excel')
	if isnull(attach_excel) then attach_excel = 0

	
	select description into :vfy_descr
	from pp_verify where verify_id = :verify_id;
	
	if company_id <> 0 then
		select description into :comp_descr from
		company where company_id = :company_id;
	else
		comp_descr = 'All companies'
	end if
	
	//f_pp_msgs(string(today()) + ' ' + string(now()) + ' Processing alert: '   + &
 	//vfy_descr + ' (id: ' +  string(verify_id) + ') for company: ' + comp_descr  + &
 	//' ( company_id: ' + string(company_id) + ') ...' )
	f_pp_msgs( string(now()) + ' Alert id: ' +  string(verify_id) + ' for comp: ' +  string(company_id) )
	verify_desc		= ds_pp_verify.getitemstring(i, 	'description')
	datawindow  	= ds_pp_verify.getitemstring(i, 	'dw')
	tolerance	   = ds_pp_verify.getitemdecimal(i, 'tolerance')
	subsystem	   = ds_pp_verify.getitemdecimal(i, 'subsystem')
	version_id     = ds_pp_verify.getitemdecimal(i, 'version')
	
	COMMENT_ALERT  	= 0
	COMMENT_REQUIRED	= 0
	PRIMARY_KEY_FIELD = ''
	
	SELECT COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD
	INTO :COMMENT_ALERT, :COMMENT_REQUIRED, :PRIMARY_KEY_FIELD
	FROM PP_VERIFY WHERE VERIFY_ID = :verify_id
	AND COMMENT_ALERT = 1;
	
	//if subsystem = 1 then // PowerTax 
		//select version into :version_id from pp_verify_batch
		//where verify_id = :verify_id and
		//company_id = :company_id;
	//	f_pp_msgs("version " +  string(version_id))
	
	//end if
	
	if isnull(tolerance) then tolerance = 0
	sql1 = ""
	sql2 = ""
	sql3 = ""
	sql4 = ""
	sql5 = ""
	sql1 				= ds_pp_verify.getitemstring(i,  'sql')
	sql2 				= ds_pp_verify.getitemstring(i,  'sql2')
	sql3 				= ds_pp_verify.getitemstring(i,  'sql3')
	sql4 				= ds_pp_verify.getitemstring(i,  'sql4')
	sql5 				= ds_pp_verify.getitemstring(i,  'sql5')
	if isnull(sql1) then sql1 = ""
	if isnull(sql2) then sql2 = ""
	if isnull(sql3) then sql3 = ""
	if isnull(sql4) then sql4 = ""
	if isnull(sql5) then sql5 = ""
	sql = sql1+ sql2 + sql3 + sql4 + sql5
	pos = pos(lower(sql), "order by ")
	if pos > 0 then
		order_by_clause = mid(sql, pos + 9)
		f_parsestringintostringarray( order_by_clause, ',', order_by_array) 
	end if

	/************** create company_str and company[] **************/
	//messagebox("", "company id" + string(company_id))
	if company_id = 0 then
		company = all_companies
		company_str = all_companies_str
	else
		company[1]	= company_id
		company_str = string(company_id)
	end if
  
	//  messagebox("", "company_str " + company_str )
	// messagebox("", "comp upperbound" + string( upperbound(company)))

	// ayp: set up a datastore for users <-> messages, when excel option is turned on
	if attach_excel = 1 then
		if lower(left(sql, 6)) <> "select" then
			f_pp_msgs("Error: Invalid SLECT statement.")
		end if
		sqls2_user = "SELECT '' ~"ZZZ_ALERT_USER~"," + mid(sql, 7, len(sql) - 6)
		sqls2_user = f_replace_string(sqls2_user,"<company_id>", company_str, "all")
		if f_create_dynamic_ds(ds_email_excel,'grid',sqls2_user,sqlca,false) <> "OK" then
			f_pp_msgs("Error in creating ds_alert_message_excel datastore.")
		end if
	end if
			
	user_alert		= ds_pp_verify.getitemnumber(i,  'user_alert')
	if g_manual_batch then
		setnull(next_notification)
		setnull(frequency)
		select next_notification, frequency into :next_notification, :frequency from pp_verify_batch
			where verify_id = :verify_id and company_id = :company_id and manual_batch = 0;
	else
		next_notification	= ds_pp_verify.getitemnumber(i,  'next_notification')
		frequency			= ds_pp_verify.getitemnumber(i,  'frequency')
	end if
	
	// next_notification has to have an integer value, because it's passed into relativedate() function that takes in an integer argument
	if next_notification > 32700 then next_notification = 32700

	
	
	/************** Reset email datastore, subject **************/
	ds_email.reset()
	ds_email_excel.reset()
	/*here*/
	setnull(email_subject)
//	email_subject = f_pp_system_control_company("ALERT EMAIL SUBJECT", company_id)

	select Control_value into :email_subject
	from pp_system_control_company
	where trim(upper(control_name)) = 'ALERT EMAIL SUBJECT'
	and company_id = :company_id;
	
	if isnull(email_subject) or email_subject = '' then 
		select Control_value into :email_subject
		from pp_system_control_company
		where trim(upper(control_name)) = 'ALERT EMAIL SUBJECT'
		and company_id = -1;
	end if
	
	if isnull(email_subject) or email_subject = '' then 
		email_subject = 'PowerPlant Alerts' 
	else
		// Replace <db> variable in the email subject line with the database name
		email_subject = f_replace_string( f_replace_string( email_subject, "<db>", sqlca.servername, "all"), "<DB>", sqlca.servername, "all")
	end if
	
	email_subject = email_subject + ' - ' + verify_desc
	
	// if SQL has update/insert then just run it
		
	if left(lower(trim(sql)), 6) = 'insert' or left(lower(trim(sql)), 6) = 'update' or &
		left(lower(trim(sql)), 6) = 'delete' then	
		
		if pos(lower(sql), 'work_order_class_code') = 0 and  pos(lower(sql), 'class_code_cpr_ledger') = 0 then
			f_pp_msgs(string(today(), 'mm/dd/yy hh:mm:ss') + "Can only update work_order_class_code and " + &
			" class_code_cpr_ledger with user SQL : " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
				
		execute immediate :sql;
		if sqlca.sqlcode < 0 then
			f_pp_msgs(string(today(), 'mm/dd/yy hh:mm:ss') + "Error executing user SQL : " + sqlca.sqlerrtext)
			rollback;
			return -1
		else
			commit;
			sqls_lock = "lock table ppverify_lock in exclusive mode nowait"
			execute immediate :sqls_lock;
			
			if sqlca.SQLCode < 0 then
				/* PPVERIFY is already running */
				f_write_log(g_log_file, "PPVERIFY is already running: " + sqlca.SQLErrText)
				g_prog_interface.uf_exit(-1)
				halt
			end if
			f_pp_msgs(string(today()) + ' ' + string(now()) + ' Executed User SQL: ' + string(verify_id) + ', company_id: ' + string(company_id) + ' ...~r~n')

		nrows2 = 0
		goto check_results
		end if
								
	end if
	
	/************** Are we running from a dw or sql? ... Retrieve ... **************/
		if isnull(datawindow) then
	 
			if left(lower(trim(sql)), 6) = 'select' then 
 

				sql = f_replace_string(sql,"<company_id>", company_str, "all")
				ret = f_create_dynamic_ds(ds_temp, 'grid', sql, sqlca, true)
				if ret <> "OK" then
					f_pp_msgs('Error retrieving verify_id: ' + string(verify_id) + ', company_id: ' + string(company_id) + ' ...')
					continue
				end if
			else	
				f_pp_msgs("Error: SQL does not begin with 'select'")
			end if
	else
		ds_temp.dataobject = datawindow
		ds_temp.settransobject(sqlca)
		
		sql_subl = upper(ds_temp.DESCRIBE('datawindow.table.select'))
		
		find_depr = pos(sql_subl, 'TEMPLATE_DEPR') 
		
		if find_depr > 0 then   // subledger found 
					
			subledger_count = ds_subledger_control.retrieve()

			for i_subl = 1 to subledger_count 
  				subledger_name[i_subl] = ds_subledger_control.GetItemString(i_subl, 'subledger_name')
  			next
			  
			for i_subl = 1 to subledger_count 
				
				ds_temp_subl.dataobject = datawindow
				
				f_pp_msgs("--- Processing subledger " + subledger_name[i_subl] )
				
				modified_sqls = sql_subl //reset
	
				modified_sqls = f_replace_string(modified_sqls,'TEMPLATE_DEPR',& 
							 UPPER(subledger_name[i_subl]) + '_DEPR' ,'all')
							 
				modified_sqls = f_replace_string(modified_sqls,'TEMPLATE_BASIS',& 
							 UPPER(subledger_name[i_subl]) + '_BASIS' ,'all')	
							 
				modified_sqls = f_replace_string(modified_sqls,'TEMPLATE_NAME',& 
							 UPPER(subledger_name[i_subl])  ,'all')	
							 
				modified_sqls = f_replace_string(modified_sqls,'SUBLEDGER_TEMPLATE',& 
							 UPPER(subledger_name[i_subl])  ,'all')								 
				 
				modified_sqls = 'datawindow.table.select = "' + modified_sqls + '"' 
		
				rc = ds_temp_subl.Modify(modified_sqls)
				ds_temp_subl.settransobject(sqlca)
				
				if rc <> '' then
					f_pp_msgs("Cannot modify the subledger SQL")
					continue
					
				end if
				
				
				nrows_subl = ds_temp_subl.retrieve(company)
				
				f_pp_msgs("--- Subledger rows Found: " + string(nrows_subl)) 
				
			   ds_temp_subl.RowsCopy(ds_temp_subl.GetRow(), ds_temp_subl.RowCount(), &
				  Primary!, ds_temp, 1, Primary!)
				
				 
			next 

         nrows2 = ds_temp.rowcount()
			goto check_results
			
		else
			if subsystem =  1 then // PowerTax'  
				rtn = ds_temp.retrieve(company, version_id, tolerance) 
				if company_id <>  0 then
					f_pp_msgs('company  ' + string(company_id) + ' version ' + string(version_id) + ' tolerance ' + string(tolerance))
			   else
					f_pp_msgs('company  ' + 'all companies' + ' version ' + string(version_id) + ' tolerance ' + string(tolerance))
			  
				end if
				
			else
				rtn = ds_temp.retrieve(company)
			end if
			
			if rtn < 0 then
				f_pp_msgs('Error retrieving verify_id: ' + string(verify_id) + ', company_id: ' + string(company_id) + ' ...')
				continue
			end if
		
	   end if
		
	end if
	
	/************** Did we find any results? **************/
   nrows2 = ds_temp.rowcount()
	check_results:
	
	//if g_manual_batch = false then
	//	f_pp_msgs("--- Result Rows Found " + string(nrows2 ))
	//else
	//	f_pp_msgs("---Problem Rows Found " + string(nrows2 ))
	//end if
	
	if nrows2 = 0 then
		// If verify/alert DID NOT find anything then delete any previous results (and user task list entries) ...
		delete from pp_task_list where result_id in 
			(select result_id from pp_verify_results where verify_id = :verify_id and company_id = :company_id);
	
		if sqlca.sqlcode < 0 then
			
			f_pp_msgs( "Error deleting previous results from pp_task_list where verify_id = " + string(verify_id) + &
				" and company_id = " + string(company_id) + ": " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if 
		
		delete from pp_verify_results where verify_id = :verify_id and company_id = :company_id;
	
		if sqlca.sqlcode < 0 then
			
			f_pp_msgs( "Error deleting previous results from pp_verify_results where verify_id = " + string(verify_id) + &
				" and company_id = " + string(company_id) + ": " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if 
		
	  //   update cpr_control  
	  
	  if lower(g_manual_batch_type) = 'close' then
		
			update cpr_control set cpr_balanced = sysdate
			where company_id = :company_id  and to_char(accounting_month,'yyyymm') = :g_manual_month;
		 
		 
	  end if
	 
	else
	
		// If verify/alert DID find something then loop through results to:
		//		1. Save result
		//		2. If result alread existed, save alert last_sent date
		//		3. If alert is active, check to see if alert should be sent
		//		4. Send alert (provided all conditions are met)
		//		5. Add result to user task list
		//		6. Delete previous results
		
		if g_manual_batch_type = 'close' then
		
			update cpr_control set cpr_balanced = sysdate
			where company_id = :company_id  and to_char(accounting_month,'yyyymm') = :g_manual_month;
		 
		 
	  end if

		col = null_string_array
		ncols = integer(ds_temp.describe("datawindow.column.count"))
		ds_pp_verify_results.retrieve(verify_id, company_id)

		// Get the pp_verify_results sequence number for this verify-company
		select max(sequence_number) + 1 into :seq_no from pp_verify_results
			where verify_id = :verify_id and company_id = :company_id;
		if isnull(seq_no) then seq_no = 1

		/******* Loop through results rows and archive results to pp_verify_results *******/
		for j = 1 to nrows2
			ds_pp_verify_results.insertrow(1)
			ds_pp_verify_results.setitem(1, 'verify_id', verify_id)
			ds_pp_verify_results.setitem(1, 'company_id', company_id)
			ds_pp_verify_results.setitem(1, 'dummy_id', j)
			ds_pp_verify_results.setitem(1, 'sequence_number', seq_no)
			find_str = "verify_id = " + string(verify_id) + " and company_id = " + &
					string(company_id) + " and sequence_number < " + string(seq_no)
					
			if isnull(datawindow) then
				for k = 1 to ncols	// SQL
					col_name = ds_temp.describe("#" + string(k) + ".Name")
					col_type = ds_temp.describe(col_name + ".ColType")
					if mid(col_type,1,4) = 'char' then
						col[k] = ds_temp.getitemstring(j,k)
					elseif mid(col_type,1,7) = 'decimal' then
						col[k] = string(ds_temp.getitemdecimal(j,k))
					elseif mid(col_type,1,6) = 'number' then
						col[k] = string(ds_temp.getitemnumber(j,k))
					elseif mid(col_type,1,4) = 'date' then
						col[k] = string(ds_temp.getitemdatetime(j,k))
					end if
										               
					//Make sure null values are not written
					if isnull(col[k]) then col[k] = 'Null'

					ds_pp_verify_results.setitem(1, 'col' + string(k), col[k])
					find_str = find_str + " and col" + string(k) + " = '" + f_replace_string(col[k],"'","~~'","all") + "'"
				next
			else
				for k = 4 to ncols  // Datawindow
					col_name = ds_temp.describe("#" + string(k) + ".Name")
					col_type = ds_temp.describe(col_name + ".ColType")
					if mid(col_type,1,4) = 'char' then
						col[k - 3] = ds_temp.getitemstring(j,k)
					elseif mid(col_type,1,7) = 'decimal' then
						col[k - 3] = string(ds_temp.getitemdecimal(j,k))
					elseif mid(col_type,1,6) = 'number' then
						col[k - 3] = string(ds_temp.getitemnumber(j,k))
					elseif mid(col_type,1,4) = 'date' then
						col[k - 3] = string(ds_temp.getitemdatetime(j,k))
					end if
					ds_pp_verify_results.setitem(1, 'col' + string(k - 3), col[k - 3])
					
					
					if isnull( col[k - 3])  then 
						col[k - 3] = 'Null'
					end if
					
					find_str = find_str + " and col" + string(k - 3) + " = '" + f_replace_string(col[k - 3],"'","~~'","all") + "'"
				next
			end if
			
			// If this result has already been saved?
			setnull(last_sent)
			rtn = ds_pp_verify_results.find(find_str, 1, ds_pp_verify_results.rowcount())
			if rtn <= 1 then
				// this is a new result
				new_result = true
				
				// give it a result_id
				result_id ++
				ds_pp_verify_results.setitem(1, 'result_id', result_id)
				
				// the result is in row 1 in the datawindow
				result_row = 1
				
			else
				// we've already seen this result
				new_result = false
				
				// delete row 1 (we already have the result saved)
				ds_pp_verify_results.deleterow(1)
				
				// since we just deleted a row, what row is our result on?
				result_row = ds_pp_verify_results.find(find_str, 1, ds_pp_verify_results.rowcount())
				
				// update the sequence num so it doesn't get deleted
				ds_pp_verify_results.setitem(result_row, 'sequence_number', seq_no)
				
				// when was the last time an email alert was sent?
				last_sent = ds_pp_verify_results.getitemdatetime(result_row, 'last_sent')
				
				// when was the first time an email alert was sent?
				setnull(first_sent)
				first_sent = ds_pp_verify_results.getitemdatetime(result_row, 'first_sent')
			end if
			
	
			/************** Do we want to send alerts? **************/
			//  We will send an alert if alerts have been avtivated ( user_alert = 1 ) and
			//  one of the following three conditions holds true:
			//		1.	This alert has never been sent ( isnull(last_sent) )
			//		2.	This is a manual batch and there is no auto batch ( isnull(next_notification) )
			// or 3. It has been at least 'next_notification' days since the last 
			//			time this alert was sent
			if user_alert = 1 and (isnull(last_sent) or isnull(next_notification) or &
				RelativeDate(date(last_sent),next_notification) <= date(run_time) ) then
				
				// we want to send mail
				send_email = true
				ds_pp_verify_results.setitem(result_row, 'last_sent', run_time)

				if isnull(first_sent) then
					// record this as the first time it was sent
					ds_pp_verify_results.setitem(result_row, 'first_sent', run_time)
				end if
			else
				// don't send mail
				send_email = false
			end if

			/***** Do we want to process the users for this alert? *****/
			//  We want to process the users if:
			//		1.	The user_sql is not null and
			// 	2.	We are saving the users to the task list (new_result = true) or
			//			we are send an alert email to the users (send_email = true)
			user_sql			= ds_pp_verify.getitemstring(i,  'user_sql')
			alert_message	= ds_pp_verify.getitemstring(i,  'alert_message')
			
			if not (isnull(user_sql) or user_sql = "") and (new_result or send_email) then
				
				if isnull(datawindow) then
					for k = 1 to ncols	// SQL
						alert_message = f_replace_string(alert_message, "<col"  + string(k) + ">", col[k], "all")
						user_sql = f_replace_string(user_sql, "<col"  + string(k) + ">", "'" + col[k] + "'", "all")
					next
				else
					for k = 1 to ncols - 3	// Datawindow
						alert_message = f_replace_string(alert_message, "<col"  + string(k) + ">", col[k], "all")
						user_sql = f_replace_string(user_sql, "<col"  + string(k) + ">", "'" + col[k] + "'", "all")
					next
				end if
				
				ret = f_create_dynamic_ds(ds_users,'grid',user_sql,sqlca,true)
				if ret <> "OK" then
					f_pp_msgs(string(today(), 'mm/dd/yy hh:mm:ss') + "Error in ds_users create: " + ret + "~r~nVerify ID = " + string(verify_id))
					return -1
				end if
				nrows3 = ds_users.rowcount()

				IF COMMENT_ALERT <> 1 THEN
					ds_pp_verify_results.setitem(1, 'alert_message', alert_message)
					for k = 1 to nrows3
						to_user = trim(ds_users.getitemstring(k,1))
						if isnull(to_user) or to_user = '' then continue
						 
						if new_result then
							// Save result to task list
							task_list_id ++
							ds_pp_task_list.insertrow(1)
							ds_pp_task_list.setitem(1,'task_list_id',task_list_id)
							ds_pp_task_list.setitem(1,'users',to_user)
							ds_pp_task_list.setitem(1,'result_id',result_id)
						end if
	
						if send_email then
							// Store a row for each user (Emails will be sent a little later)
							if attach_excel = 1 then
								ds_email_excel.InsertRow(1)
								ds_email_excel.SetItem(1, 1, to_user)
								if isnull(datawindow) then
									for o = 1 to ncols	// SQL
										//ds_email_excel.SetItem(1, o+1, col[o])
										col_name = ds_temp.describe("#" + string(o) + ".Name")
										col_type = ds_temp.describe(col_name + ".ColType")
										if mid(col_type,1,4) = 'char' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemstring(j,o))
										elseif mid(col_type,1,7) = 'decimal' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdecimal(j,o))
										elseif mid(col_type,1,6) = 'number' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemnumber(j,o))
										elseif mid(col_type,1,4) = 'date' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdatetime(j,o))
										end if

									next
								else
									for o = 1 to ncols - 3	// Datawindow
										//ds_email_excel.SetItem(1, o+1, col[o])
										col_name = ds_temp.describe("#" + string(o+3) + ".Name")
										col_type = ds_temp.describe(col_name + ".ColType")
										if mid(col_type,1,4) = 'char' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemstring(j,o+3))
										elseif mid(col_type,1,7) = 'decimal' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdecimal(j,o+3))
										elseif mid(col_type,1,6) = 'number' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemnumber(j,o+3))
										elseif mid(col_type,1,4) = 'date' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdatetime(j,o+3))
										end if
									next
								end if								
							else
								ds_email.insertrow(1)
								ds_email.object.user_id[1] = to_user
								ds_email.object.alert_message[1] = alert_message
							end if
						end if
					next
				ELSE //We want to save it off to pp_verify_user_comments
					for k = 1 to nrows3
						to_user = trim(ds_users.getitemstring(k,1))
						if isnull(to_user) or to_user = '' then continue
						 
						if new_result then
							// Save result to task list
							task_list_id ++
							ds_pp_task_list.insertrow(1)
							ds_pp_task_list.setitem(1,'task_list_id',task_list_id)
							ds_pp_task_list.setitem(1,'users',to_user)
							ds_pp_task_list.setitem(1,'result_id',result_id)
							
							//###AYP: Maint 8655: PP_VERIFY_USER_ COMMENTS id collides with the records inserted in uo_user_comment.uf_update(), which uses the sequence
							//max_id++
							select pwrplant1.nextval into :max_id from dual; 
							if SQLCA.SQLCode = -1 then
	 							f_write_log(g_log_file, "SQL error: " + SQLCA.SQLErrText)
    								rollback;
							end if
							col1	=	''
							col2	=	''
							col3	=	''
							col4	=	''
							col5	=	''
							col6	=	''
							col7	=	''
							col8	=	''
							col9	=	''
							col10	=	''
							col11	=	''
							col12	=	''
							col13	=	''
							col14	=	''
							col15	=	''
							col16	=	''
							col17	=	''
							col18	=	''
							col19	=	''
							col20	=	''
							col21	=	''
							col22	=	''
							col23	=	''
							col24	=	''
							col25	=	''
							col26	=	''
							col27	=	''
							col28	=	''
							col29	=	''
							col30	=	''

							col1	=	mid(ds_pp_verify_results.getItemString(1,'col1'),1,254)
							col2	=	mid(ds_pp_verify_results.getItemString(1,'col2'),1,254)
							col3	=	mid(ds_pp_verify_results.getItemString(1,'col3'),1,254)
							col4	=	mid(ds_pp_verify_results.getItemString(1,'col4'),1,254)
							col5	=	mid(ds_pp_verify_results.getItemString(1,'col5'),1,254)
							col6	=	mid(ds_pp_verify_results.getItemString(1,'col6'),1,254)
							col7	=	mid(ds_pp_verify_results.getItemString(1,'col7'),1,254)
							col8	=	mid(ds_pp_verify_results.getItemString(1,'col8'),1,254)
							col9	=	mid(ds_pp_verify_results.getItemString(1,'col9'),1,254)
							col10	=	mid(ds_pp_verify_results.getItemString(1,'col10'),1,254)
							col11	=	mid(ds_pp_verify_results.getItemString(1,'col11'),1,254)
							col12	=	mid(ds_pp_verify_results.getItemString(1,'col12'),1,254)
							col13	=	mid(ds_pp_verify_results.getItemString(1,'col13'),1,254)
							col14	=	mid(ds_pp_verify_results.getItemString(1,'col14'),1,254)
							col15	=	mid(ds_pp_verify_results.getItemString(1,'col15'),1,254)
							col16	=	mid(ds_pp_verify_results.getItemString(1,'col16'),1,254)
							col17	=	mid(ds_pp_verify_results.getItemString(1,'col17'),1,254)
							col18	=	mid(ds_pp_verify_results.getItemString(1,'col18'),1,254)
							col19	=	mid(ds_pp_verify_results.getItemString(1,'col19'),1,254)
							col20	=	mid(ds_pp_verify_results.getItemString(1,'col20'),1,254)
							col21	=	mid(ds_pp_verify_results.getItemString(1,'col21'),1,254)
							col22	=	mid(ds_pp_verify_results.getItemString(1,'col22'),1,254)
							col23	=	mid(ds_pp_verify_results.getItemString(1,'col23'),1,254)
							col24	=	mid(ds_pp_verify_results.getItemString(1,'col24'),1,254)
							col25	=	mid(ds_pp_verify_results.getItemString(1,'col25'),1,254)
							col26	=	mid(ds_pp_verify_results.getItemString(1,'col26'),1,254)
							col27	=	mid(ds_pp_verify_results.getItemString(1,'col27'),1,254)
							col28	=	mid(ds_pp_verify_results.getItemString(1,'col28'),1,254)
							col29	=	mid(ds_pp_verify_results.getItemString(1,'col29'),1,254)
							col30	=	mid(ds_pp_verify_results.getItemString(1,'col30'),1,254)

							insert into pp_verify_user_comments (
							 id, result_id, verify_id, company_id, primary_key_field, primary_key_value,
							status, users, assigned_date, col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,
							col11,col12,col13,col14,col15,col16,col17,col18,col19,col20,
							col21,col22,col23,col24,col25,col26,col27,col28,col29,col30, alert_message
							)(
							SELECT :max_id, :result_id, :verify_id, :company_id, :primary_key_field, nvl(:col1,'0'),
							decode(:comment_required, 1, 'Requires Comment', 'Optional Comment'), 
							:to_user, sysdate, nvl(:col1,'0'),nvl(:col2,'0'),nvl(:col3,'0'),nvl(:col4,'0'),nvl(:col5,'0'),nvl(:col6,'0'),nvl(:col7,'0'),nvl(:col8,'0'),nvl(:col9,'0'),nvl(:col10,'0'),
							nvl(:col11,'0'),nvl(:col12,'0'),nvl(:col13,'0'),nvl(:col14,'0'),nvl(:col15,'0'),nvl(:col16,'0'),nvl(:col17,'0'),nvl(:col18,'0'),nvl(:col19,'0'),nvl(:col20,'0'),
							nvl(:col21,'0'),nvl(:col22,'0'),nvl(:col23,'0'),nvl(:col24,'0'),nvl(:col25,'0'),nvl(:col26,'0'),nvl(:col27,'0'),nvl(:col28,'0'),nvl(:col29,'0'),nvl(:col30,'0'), :alert_message
							FROM dual
							);
							if sqlca.sqlcode < 0 then
								f_pp_msgs( "Error Inserting into pp_verify_user_comments where verify_id = " + string(verify_id) + &
									" and company_id = " + string(company_id) + ": " + sqlca.sqlerrtext)
								rollback;
								return -1
							end if 
						end if
	
						if send_email then
							if attach_excel = 1 then
								ds_email_excel.InsertRow(1)
								ds_email_excel.SetItem(1, 1, to_user)
								if isnull(datawindow) then
									for o = 1 to ncols	// SQL
										//ds_email_excel.SetItem(1, o+1, col[o])
										col_name = ds_temp.describe("#" + string(o) + ".Name")
										col_type = ds_temp.describe(col_name + ".ColType")
										if mid(col_type,1,4) = 'char' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemstring(j,o))
										elseif mid(col_type,1,7) = 'decimal' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdecimal(j,o))
										elseif mid(col_type,1,6) = 'number' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemnumber(j,o))
										elseif mid(col_type,1,4) = 'date' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdatetime(j,o))
										end if
									next
								else
									for o = 1 to ncols - 3	// Datawindow
										//ds_email_excel.SetItem(1, o+1, col[o])
										col_name = ds_temp.describe("#" + string(o+3) + ".Name")
										col_type = ds_temp.describe(col_name + ".ColType")
										if mid(col_type,1,4) = 'char' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemstring(j,o+3))
										elseif mid(col_type,1,7) = 'decimal' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdecimal(j,o+3))
										elseif mid(col_type,1,6) = 'number' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemnumber(j,o+3))
										elseif mid(col_type,1,4) = 'date' then
											ds_email_excel.SetItem(1, o+1, ds_temp.getitemdatetime(j,o+3))
										end if
									next
								end if								
							else
								// Store a row for each user (Emails will be sent a little later)
								ds_email.insertrow(1)
								ds_email.object.user_id[1] = to_user
								ds_email.object.alert_message[1] = alert_message
							end if
						end if
					next
				end if
			end if
		next
		

		/************** Save the results Datawindow **************/
		rtn = ds_pp_verify_results.update()
		
		//f_pp_msgs("--- Result Rows Saved " + string(ds_pp_verify_results.rowcount() ))
		if rtn <> 1 then
			
			f_pp_msgs( "Error updating ds_pp_verify_results where verify_id = " + string(verify_id) + &
				" and company_id = " + string(company_id) + ": " + ds_pp_verify_results.i_sqlca_sqlerrtext )
			rollback;
			return -1
		end if


		/************** Save the task list Datawindow **************/
		rtn = ds_pp_task_list.update()
		if rtn <> 1 then
			
			f_pp_msgs( "Error updating ds_pp_task_list where verify_id = " + string(verify_id) + &
				" and company_id = " + string(company_id) + ": " + ds_pp_task_list.i_sqlca_sqlerrtext )
				ds_pp_task_list.saveas(replace(g_log_file,len(g_log_file) - 3, 3, 'xls'), Excel!, true)
				f_pp_msgs('File save as ' + replace(g_log_file,len(g_log_file) - 4, 4, 'a.xls'))
			rollback;
			return -1
		end if


		/************** Delete out of date results for this alert-company **************/
		delete from pp_task_list where result_id in 
			(select result_id from pp_verify_results where verify_id = :verify_id and company_id = :company_id
			and sequence_number < :seq_no);
	
		if sqlca.sqlcode < 0 then
			
			f_pp_msgs( "Error deleting previous results from pp_task_list where verify_id = " + string(verify_id) + &
				" and company_id = " + string(company_id) + ": " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if 
		
		delete from pp_verify_results where verify_id = :verify_id and company_id = :company_id
			and sequence_number < :seq_no;
		
		if sqlca.sqlcode < 0 then
			
			f_pp_msgs( "Error deleting previous results from pp_verify_results where verify_id = " + string(verify_id) + &
				" and company_id = " + string(company_id) + ": " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if 


		/************** Send off emails now **************/
		//// ATH 2/18/04: Added code so alert will be sent to user as attachment
		////
		alert_message = ""
		attach_file = ""
		file_directory = ""
		
		str = space(200)

		//status = GetEnvironmentVariableA('TEMP',str,200)
		str = f_get_temp_dir()
					
		//if status = 0 then // temp variable not found
		if str = '' then // temp variable not found
		
			// get file directory from pp_system_control
			file_directory = ''
			select control_value into :file_directory
			from pp_system_control
			where lower( control_name ) = 'save data directory';
		 
		
			if isnull( file_directory ) or trim( file_directory ) = "" then
			
				f_pp_msgs(' ')
				f_pp_msgs( "WARNING: no Save Data Directory found in pp_system_control." )
				f_pp_msgs( "Cannot send e-mail attachments with alert messages!" )
			
				return -1
			
			end if
			
		else
			
			file_directory = str
			
		end if
		

//		// ### CDM  4/12/2006 - Attach file as excel file based on user selection
//		//ayp: move it up
//		attach_excel = 0
//		sqls2 = ""		
//		
//		attach_excel = ds_pp_verify.getitemnumber(i, 'attach_as_excel')
		
		if attach_excel = 1 then
			//sqls2 = ds_pp_verify.getitemstring(i, 'sql')
				sql1 = ""
				sql2 = ""
				sql3 = ""
				sql4 = ""
				sql5 = ""
				sql1 				= ds_pp_verify.getitemstring(i,  'sql')
				sql2 				= ds_pp_verify.getitemstring(i,  'sql2')
				sql3 				= ds_pp_verify.getitemstring(i,  'sql3')
				sql4 				= ds_pp_verify.getitemstring(i,  'sql4')
				sql5 				= ds_pp_verify.getitemstring(i,  'sql5')
				if isnull(sql1) then sql1 = ""
				if isnull(sql2) then sql2 = ""
				if isnull(sql3) then sql3 = ""
				if isnull(sql4) then sql4 = ""
				if isnull(sql5) then sql5 = ""
				sqls2 = sql1+ sql2 + sql3 + sql4 + sql5
			//f_pp_msgs(sqls2)
			sqls2 = f_replace_string(sqls2,"<company_id>", company_str, "all")
			if f_create_dynamic_ds(ds_attach_excel,'grid',sqls2,sqlca,false) <> "OK" then
				f_pp_msgs("Error in creating ds_attach_excel datastore.")
			end if
		end if
		// ### end CDM
		
		
		if right( file_directory, 1 ) = "\" then
			
			// ### CDM
			//attach_file = file_directory + 'ppverify.' + string(verify_id) + ".txt"

			if attach_excel = 1 then
				
				attach_file = file_directory + 'ppverify.' + string(verify_id) + ".xls"
				
			else
				
				attach_file = file_directory + 'ppverify.' + string(verify_id) + ".txt"
				
			end if
			// end CDM
			
		else
		
			// ### CDM
			//attach_file = file_directory + "\" + string(verify_id) + ".txt"

			if attach_excel = 1 then

				attach_file = file_directory + "\" + string(verify_id) + ".xls"
				
			else
				
				attach_file = file_directory + "\" + string(verify_id) + ".txt"
				
			end if
			// end CDM
			
		end if
		
		
		//ayp: Sort by user_id. Loop through all email records and package them into seperate emails for each user_id
		if attach_excel = 1 then
			//if order by clause contains numeric columns, increment them by 1 to account for the user column ZZZ_ALERT_USER
			for n = 1 to upperbound(order_by_array[])
				pos = pos(trim(order_by_array[n]), " ", 1)
				if pos > 1 then
					order_column = left(trim(order_by_array[n]), pos -1)
					if isnumber( order_column ) then 
						order_by_array[n] = "#" + string(long(order_column) + 1) + mid( trim(order_by_array[n]), pos )
					end if
				else
					order_column = trim(order_by_array[n])
					if isnumber( order_column ) then 
						order_by_array[n] = "#" + string(long(order_column) + 1)
					end if
				end if
			next
			order_by_clause = f_parsestringarrayintostring( order_by_array )
			order_by_clause = f_replace_string(order_by_clause,",", ", ", "all")
			rtn = ds_email_excel.SetSort("ZZZ_ALERT_USER A" + ", " + order_by_clause)
			if rtn <> 1 then 
				//if there was an error or ORDER BY is not used, sort just by user
				rtn = ds_email_excel.SetSort("ZZZ_ALERT_USER A")
			end if
			ds_email_excel.sort()
			nrows3 = ds_email_excel.rowcount()
		else
			ds_email.SetSort("user_id A, alert_message A")
			ds_email.sort()
			nrows3 = ds_email.rowcount()
		end if
		
		//check if this alert displays on mobile, setup boolean so that mobile link can be included
		longlong vfy_id
		select verify_id into :vfy_id
		from mobile_alerts where verify_id = :verify_id;
		
		if vfy_id = verify_id then
			mobile_alert = true
		else
			mobile_alert = false
		end if
		
		attach_row = 0
		for j = 1 to nrows3
			if attach_excel = 1 then
				//attach_row++
				if j < nrows3 then
					if ds_email_excel.object.ZZZ_ALERT_USER[j] = ds_email_excel.object.ZZZ_ALERT_USER[j + 1] then
						//alert_message = alert_message + ds_email.object.alert_message[j] + "~r~n~r~n"
						ncols_excel = long(ds_email_excel.describe("datawindow.column.count"))
						attach_row = ds_attach_excel.InsertRow(0)
						// Copy the row into the datawindow that will be emailed to the user
						for n = 2	to ncols_excel
							col_name = ds_email_excel.describe("#" + string(n) + ".Name")
							col_type = ds_email_excel.describe(col_name + ".ColType")
							if mid(col_type,1,4) = 'char' then
								ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemstring(j,n))
							elseif mid(col_type,1,7) = 'decimal' then
								ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemdecimal(j,n))
							elseif mid(col_type,1,6) = 'number' then
								ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemnumber(j,n))
							elseif mid(col_type,1,4) = 'date' then
								ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemdatetime(j,n))
							end if
						next						
						continue
					end if
				end if
				to_user = ds_email_excel.object.ZZZ_ALERT_USER[j]
				ncols_excel = long(ds_email_excel.describe("datawindow.column.count"))
				attach_row = ds_attach_excel.InsertRow(0)
				// Copy the row into the datawindow that will be emailed to the user
				for n = 2	to ncols_excel
					col_name = ds_email_excel.describe("#" + string(n) + ".Name")
					col_type = ds_email_excel.describe(col_name + ".ColType")
					if mid(col_type,1,4) = 'char' then
						ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemstring(j,n))
					elseif mid(col_type,1,7) = 'decimal' then
						ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemdecimal(j,n))
					elseif mid(col_type,1,6) = 'number' then
						ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemnumber(j,n))
					elseif mid(col_type,1,4) = 'date' then
						ds_attach_excel.SetItem(attach_row, n - 1, ds_email_excel.getitemdatetime(j,n))
					end if
				next						
				attach_row = 0
			
			else	// Text attachment	
				if j < nrows3 then
					if ds_email.object.user_id[j] = ds_email.object.user_id[j + 1] then
						//// Maint#: 2131
						if isnull(ds_email.object.alert_message[j]) then
							alert_message = alert_message + " NULL " + "<br><br>~r~n~r~n"
						else
							alert_message = alert_message + ds_email.object.alert_message[j] + "<br><br>~r~n~r~n"
						end if
						continue
					end if
				end if
				to_user = ds_email.object.user_id[j]
				//// Maint#: 2131
				if isnull(ds_email.object.alert_message[j]) then
					alert_message = alert_message + " NULL "
				else			
					alert_message = alert_message + ds_email.object.alert_message[j]
				end if
				html_alert_message = alert_message
				alert_message = f_replace_string(alert_message, "<br><br>", "", "all")
			end if
		
			// Check the size  - Only create an attachment if too large
			
			msg_size_str = f_pp_system_control_company('Size Before Alert Email Attachment', company_id)
			if msg_size_str = '' then
				msg_size = 1000000
			else
				msg_size = long(msg_size_str)
			end if
			
			//ayp: should not go in here if user wants to receive output as an excel file
			if len(alert_message) <= msg_size and attach_excel <> 1 then 
				//f_pp_msgs("Mail length is " + string(len(alert_message)))
				
			else
				// ### CDM
				
				//f_pp_msgs("Mail is being sent as an attachment")
				//
				//if f_write_file( attach_file, alert_message, replace! ) = false then
				//	
				//	f_pp_msgs( "Error writing alert to attachment file " + attach_file)
				//	f_pp_msgs( "Alert Message: " + alert_message )
				//	
				//	alert_message = ""
				//	
				//	continue
				//	
				//end if
				
				if attach_excel = 1 then

					f_pp_msgs("Mail is being sent as an excel attachment")
			
					if ds_attach_excel.saveas(attach_file, Excel!, TRUE) <> 1 then
						
						f_pp_msgs( "Error writing alert to excel file " + attach_file)
						//f_pp_msgs( "Alert Message: " + alert_message )
						
						//alert_message = ""
						
						continue
						
					end if
					
				else
					
					f_pp_msgs("Mail is being sent as a text attachment")
			
					if pos(lower(alert_message), "<div>") > 0 or pos(lower(alert_message), "<tr>") > 0 then
						alert_message = '<HTML><BODY><DIV> ' + alert_message
						if mobile_alert then //add mobile link if necessary
							alert_message = alert_message + mobile_link
						end if
						alert_message = alert_message + ' </DIV></BODY></HTML>'
						attach_file = left(attach_file, len(attach_file) - 3) + "html"
					end if	
						
					if f_write_file( attach_file, alert_message, replace! ) = false then
						
						f_pp_msgs( "Error writing alert to attachment file " + attach_file)
						f_pp_msgs( "Alert Message: " + alert_message )
						
						alert_message = ""
						
						continue
						
					end if					
				end if
				// end CDM
				
			end if
			
			setnull(to_user_email)
			
      if match(to_user,'@') = true then 
         to_user_email = to_user 
      else
   		select mail_id into :to_user_email from pp_security_users
			 where trim(users) = trim(:to_user);
      end if 
		
			if isnull(to_user_email) or to_user_email = '' then
				f_pp_msgs("No email address exists for user ID: " + to_user + "~r~nMessage = " + alert_message + "~r~n~r~n")
			else
				
				if len(alert_message) <= msg_size and attach_excel <> 1 then
					html_alert_message = '<HTML><BODY><DIV> ' + html_alert_message + ppt_link
					if mobile_alert then //add the mobile link if necessary
						html_alert_message = html_alert_message + mobile_link + ' </DIV></BODY></HTML>'
					else
						html_alert_message = html_alert_message + ' </DIV></BODY></HTML>'
					end if
					mail_rtn = g_msmail.send(from_user, " ", email_subject, html_alert_message, to_user_email, mail_server) 
				else
					html_alert_message = '<HTML><BODY><DIV> See attached file.'+ ppt_link
					if mobile_alert then //add the mobile link if necessary
						html_alert_message = html_alert_message + mobile_link + ' </DIV></BODY></HTML>'
					else
						html_alert_message = html_alert_message + ' </DIV></BODY></HTML>'
					end if
					mail_rtn =  g_msmail.sendfile( from_user, " ", email_subject, html_alert_message, to_user_email, &
											  mail_server, attach_file ) 
				end if
											  
				if mail_rtn = false then
					f_pp_msgs("Error sending email to: " + to_user_email + "~r~nMessage = " + alert_message + "~r~n~r~n")
				else
					f_pp_msgs("Alert sent to user ID: " + to_user)
				end if
			end if
			alert_message = ""
			ds_attach_excel.reset()
		next
	//next
		
	end if // if nrows2 = 0 
	


	/************ Update the Last_Run time for this alert-company **********/
	//Needed for the batch scheduler to decide when the next run is.
	update pp_verify_batch set last_run = sysdate
	 where verify_id = :verify_id and company_id = :company_id;
	if sqlca.sqlcode < 0 then
		f_pp_msgs(string(today(), 'mm/dd/yy hh:mm:ss') + "Error updating last_run: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	// Displays last run date on the results
	update pp_verify_results set last_run = sysdate
	 where verify_id = :verify_id and company_id = :company_id;
	if sqlca.sqlcode < 0 then
		f_pp_msgs(string(today(), 'mm/dd/yy hh:mm:ss') + "Error updating last_run: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if


	/************ Finally, we need to set the Next_Run time for this alert-company **********/
	if not g_manual_batch then // If we are running a manual batch then don't update next_run
	
		// If an alert has been turned off, or the batch process does not run for extended amounts of time
		// this method of updating dates may not work correctly
		
		choose case frequency
			case 4 // annually
				update pp_verify_batch set next_run = to_date(to_char(to_number(to_char(next_run,'yyyy')) + 1) || 
				to_char(next_run,'mmdd'),'yyyymmdd')
				 where verify_id = :verify_id and company_id = :company_id and manual_batch = 0;
			case 3 // monthly
//				update pp_verify_batch set next_run = greatest(add_months(next_run,1), to_date(to_char(sysdate,'yyyymm') || 
//				 DECODE(lpad(to_char(frequency_info),2,'0'),'00',NULL,lpad(to_char(frequency_info),2,'0')),'yyyymmdd hh'))
//				 where verify_id = :verify_id and company_id = :company_id and manual_batch = 0;
				update pp_verify_batch set next_run = greatest(add_months(next_run,1), to_date(to_char(sysdate,'yyyymm') || lpad(to_char(frequency_info),2,'0'),'yyyymmdd hh24'))
				 where verify_id = :verify_id and company_id = :company_id and manual_batch = 0;

			case 2 // weekly
				update pp_verify_batch set next_run = next_run + 7
				 where verify_id = :verify_id and company_id = :company_id and manual_batch = 0;
			case 1 // daily
				/* PC-45445 */
				UPDATE pp_verify_batch
				SET next_run = GREATEST(next_run + 1, TO_DATE(TO_CHAR(SYSDATE,'yyyymmdd ') || lpad(TO_CHAR(frequency_info),2,'0'),'yyyymmdd hh24'))
				WHERE verify_id = :verify_id
				AND company_id = :company_id
				AND manual_batch = 0;
			case 0 // always
				// leave next run set at 1/1/1900 and it will always be triggered
			case else
		end choose
	
		if sqlca.sqlcode < 0 then
			f_pp_msgs(string(today(), 'mm/dd/yy hh:mm:ss') + "Error updating next_run: " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
	end if

	commit;
	sqls_lock = "lock table ppverify_lock in exclusive mode nowait"
	execute immediate :sqls_lock;
	
	if sqlca.SQLCode < 0 then
		/* PPVERIFY is already running */
		f_write_log(g_log_file, "PPVERIFY is already running: " + sqlca.SQLErrText)
		g_prog_interface.uf_exit(-1)
		halt
	end if
	
	if company_id <> 0 then
//		f_pp_msgs(string(today()) + ' ' + string(now()) + ' Finished with verify_id: ' + &
//	   string(verify_id) + ', company_id: ' + string(company_id) + ' ...~r~n')
		f_pp_msgs(string(now()) + ' Done~r~n')
	
	else
		//f_pp_msgs(string(today()) + ' ' + string(now()) + ' Finished with verify_id: ' + &
	   //string(verify_id) + ' for All Companies ...~r~n')
		f_pp_msgs(string(now()) + ' Done~r~n')
	end if
	
	f_pp_msgs( "")
	
next
	 

// Destroy variables
Destroy g_msmail
Destroy uo_winapi
Destroy ds_temp
Destroy ds_pp_verify
Destroy ds_pp_verify_results
Destroy ds_users
Destroy ds_email
Destroy ds_pp_task_list
// ### CDM
destroy ds_attach_excel
// end CDM
  
f_pp_msgs( "Done.")
f_pp_msgs( "")

return 1










end function

on uo_pp_verify.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_pp_verify.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

