HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'cr_flatten_structures.exe'

string 					i_use_new_structures_table, i_co_field

uo_ds_top 				i_ds_structure
uo_ds_top 				i_ds_points
uo_ds_top				i_ds_levels
uo_ds_top				i_ds_descr
end variables

forward prototypes
public function integer uf_flatten_one (longlong a_structure_id)
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function integer uf_flatten_one (longlong a_structure_id);//*******************************************************************
//
//		  UF_FLATTEN_ONE
//			Flatten an single structure, a_structure_id
//			Return 1 on success, -1 on failure
//			No commits or rollbacks in this function
//
//*******************************************************************
longlong the_row, rtn, num_rows, i, value_id, detail_point_seq_num, &
		 rollup_value_id, num_levels, ii, element_id, sequence_number, status, &
		 values_by_co, counter
string structure_description, sqls, detail_point, element_description, element_table, &
       element_column, detail_point_description, structure_type, element_value, &
		 parent_value, orig_detail_point, company_value
boolean inactive_summary_point


//*******************************************************************
//
//		  Retrieve some structure information
//
//*******************************************************************
select description, element_id, structure_type, company_value
	into :structure_description, :element_id, :structure_type, :company_value
	from cr_structures
	where structure_id = :a_structure_id;
	
if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving structure information: " + sqlca.SQLErrText)
	f_pp_msgs(" ")
	return -1
end if

//************************************************************
//
//  Reset the flattened datastore before each new tree
//		so that it doesn't contain any other data
//
//************************************************************
i_ds_structure.Reset()


//***************************************************************
//
//  Get the element table/column/description
//
//***************************************************************
select description, element_table, trim(element_column), nvl(values_by_company,0)
  into :element_description, :element_table, :element_column, :values_by_co
  from cr_elements
 where element_id = :element_id;
 
 
// NOTE: We need to know whether the master element tables have values by company
//			so that we don't try to insert duplicate values.
if values_by_co = 1 and (isnull(company_value) or company_value = "") then
	f_pp_msgs("ERROR: There is no company value set up for structure_id " + string(a_structure_id) + ".")
	rollback;
	return -1
end if


//************************************************************
//
//  Key Audits:
//		If the structure has the detail point in multiple places, it cannot be flattened!
//
//************************************************************
if i_use_new_structures_table = "YES" then
	select count(*)
		into :counter
		from 
			(select trim(substr(element_value, 1, decode(instr(element_value, ':'),0,length(element_value), instr(element_value, ':') - 1)))
			from cr_structure_values2
			where structure_id = :a_structure_id
			and detail_budget = 1 and status = 1
			group by  trim(substr(element_value, 1, decode(instr(element_value, ':'),0,length(element_value), instr(element_value, ':') - 1)))
			having count(*) > 1);
else
	select count(*)
		into :counter
		from 
			(select trim(substr(element_value, 1, decode(instr(element_value, ':'),0,length(element_value), instr(element_value, ':') - 1)))
			from cr_structure_values
			where structure_id = :a_structure_id
			and detail_budget = 1 and status = 1
			group by  trim(substr(element_value, 1, decode(instr(element_value, ':'),0,length(element_value), instr(element_value, ':') - 1)))
			having count(*) > 1);
end if

if counter > 0 then
	f_pp_msgs(" ")
	f_pp_msgs("Unable to flatten the structure due to detail points that exist in multiple locations in the structure.")
	f_pp_msgs("Please audit the structure and remove the values to allow the structure to be flattened.")
	f_pp_msgs(" ")
	return 1
end if
	

//************************************************************
//
//  Get all the detail points for the structure.
//
//************************************************************
f_pp_msgs("Retrieving the structure detail points at " + string(now()))

if i_use_new_structures_table = "YES" then
	sqls = "select * from cr_structure_values2 where structure_id = " + string(a_structure_id) + &
		" and detail_budget = 1 and status = 1"
else
	sqls = "select * from cr_structure_values where structure_id = " + string(a_structure_id) + &
		" and detail_budget = 1 and status = 1"
end if

i_ds_points.Reset()
i_ds_points.SetSQLSelect(sqls)
num_rows = i_ds_points.Retrieve()


//******************************************************************************
//
//  Loop over each detail point, build an array for the number of levels, and
//  insert into i_ds_structure.
//
//******************************************************************************
f_pp_msgs("Building the flattened structure at " + string(now()))
for i = 1 to num_rows
	
	if mod(i, 1000) = 0 then
		f_pp_msgs("Finished " + string(i) + " of " + string(num_rows) + " records at " + string(now()))
	end if
	
	if i_use_new_structures_table = "YES" then
		//  No such field in the new table.
	else
		value_id          = i_ds_points.GetItemNumber(i, "value_id")
	end if
	detail_point         = trim(i_ds_points.GetItemString(i, "element_value"))
	
	//  1/24/06:  The detail points in the flattened structure needs to be pristine.  Strip
	//            out everything to the right of the ":".  Two thoughts on this ... 1) We
	//            already have the point description on the table ... 2) Clients are writing
	//            custom reports against this table.  It is unreasonable to expect them to
	//            remember to perform the "decode/instr/length/instr" in their joins.
	//            We are trimming in case of spaces preceding the ":".
	//
	//            We save the original for the i_ds_levels.RETRIEVE()
	orig_detail_point = detail_point
	
	if pos(detail_point, ":") > 0 then
		detail_point = trim(left(detail_point, pos(detail_point, ":") - 1))
	end if
	
	setnull(detail_point_description)
			
	sqls = &
		"select description from " + element_table + &
		' where "' + upper(element_column) + '" = ' + "'" + detail_point + "'" + &
		  " and element_type = 'Actuals'"
		  
	if values_by_co = 1 then
		sqls = sqls + &
			    " and " + i_co_field + " = '" + company_value + "' "
	end if	  
	
	i_ds_descr.SetSQLSelect(sqls)
	i_ds_descr.RETRIEVE()
	
	if i_ds_descr.RowCount() > 0 then
		detail_point_description = i_ds_descr.GetItemString(1, 1)
	end if
	
	if isnull(detail_point_description) or trim(detail_point_description) = "" then
		detail_point_description = " "
	end if
	
	detail_point_seq_num = i_ds_points.GetItemNumber(i, "sequence_number")
	if i_use_new_structures_table = "YES" then
		parent_value      = i_ds_points.GetItemString(i, "parent_value")
	else
		rollup_value_id   = i_ds_points.GetItemNumber(i, "rollup_value_id")
	end if
	
	if isnull(detail_point_seq_num) then detail_point_seq_num = 0
	
	if i_use_new_structures_table = "YES" then
		
		sqls = " select * "
		sqls += "from cr_structure_values2  "
		sqls += "where structure_id = " + string(a_structure_id)
		sqls += "start with structure_id = " + string(a_structure_id) + " and element_value = '" + orig_detail_point + "' "
		sqls += "connect by element_value = prior parent_value and structure_id = " + string(a_structure_id) + " "
		sqls += "order by level desc "
		i_ds_levels.SetSQLSelect(sqls)
		
		num_levels = i_ds_levels.RETRIEVE()
	else
		// ### 9665: JAK: 2012-03-07:  Reference to cr_structure_values2 changed to cr_structure_values
		sqls = " select * "
		sqls += "from cr_structure_values  "
		sqls += "where structure_id = " + string(a_structure_id)
		sqls += "start with structure_id = " + string(a_structure_id) + " and value_id = '" + string(value_id) + "' "
		sqls += "connect by value_id = prior rollup_value_id and structure_id = " + string(a_structure_id) + " "
		sqls += "order by level desc "
		i_ds_levels.SetSQLSelect(sqls)
		
		num_levels = i_ds_levels.RETRIEVE()
	end if
	
	if num_levels >= 20 then
		//  Flag an error ... we have gone beyond the capacity of the
		//  cr_structures_flattened table.
		
		//	 For the standalone, we still want to flatten the other structures
		//		We will log the error but return a 1 indicating success
		f_pp_msgs(" ")
		f_pp_msgs("WARNING: Level 20 of the structure was reached.  " + &
			"The structure contains too many levels and cannot be flattened (max = 20 levels).")
		f_pp_msgs(" ")
		return 1
	end if
	
	//  If any parent status values are NULL or 0 then this point is under an inactive
	//  summary point.  Do not export to the flattened table.
	inactive_summary_point = false
	for ii = 1 to num_levels
		status = i_ds_levels.GetItemNumber(ii, "status")
		if isnull(status) or status <> 1 then
			inactive_summary_point = true
			exit  //  We already know we need to skip this detail point ...
		end if
	next
	
	if inactive_summary_point then continue
	
	for ii = 1 to num_levels
		
		if ii = num_levels then continue  //  The detail point has been dealt with already.
		
		if ii = 1 then
			
			i_ds_structure.InsertRow(1)
			
			i_ds_structure.SetItem(1, "structure_id",          a_structure_id)
			i_ds_structure.SetItem(1, "structure_description", structure_description)
			i_ds_structure.SetItem(1, "element_id",            element_id)
			i_ds_structure.SetItem(1, "element_description",   element_description)
			i_ds_structure.SetItem(1, "structure_type",        structure_type)
			if i_use_new_structures_table = "YES" then
				//  No such field in the new table.
			else
				i_ds_structure.SetItem(1, "value_id",              value_id)
			end if
			i_ds_structure.SetItem(1, "detail_point",          detail_point)
			i_ds_structure.SetItem(1, "detail_point_description", detail_point_description)
			i_ds_structure.SetItem(1, "detail_point_seq_num",  detail_point_seq_num)
			

		end if
		
		
		//  Fill in the rollup values.
		element_value   = i_ds_levels.GetItemString(ii, "element_value")
		sequence_number = i_ds_levels.GetItemNumber(ii, "sequence_number")

		i_ds_structure.SetItem(1, "summary_point_" + string(21 - ii), element_value)
		i_ds_structure.SetItem(1, "summary_point_" + string(21 - ii) + "_seq_num", sequence_number)

		
	next  //  for ii = 1 to num_levels ...

next  //  for i = 1 to num_rows ...


//*******************************************************************
//
//		  Save the newly flattened structure.
//
//*******************************************************************
f_pp_msgs("Saving the flattened structure at " + string(now()))

rtn = i_ds_structure.Update()

if rtn <> 1 then
	messagebox("Flatten One", "ERROR: saving the structure: " + &
		i_ds_structure.i_sqlca_sqlerrtext, Exclamation!)
	return -1
end if

return 1
end function

public function longlong uf_read ();longlong rtn, i, num_structures, structure_id
string sqls, description

//*******************************************************************
//
//		  System switch for the new structure values table.  
//
//*******************************************************************
setnull(i_use_new_structures_table)
select upper(rtrim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(rtrim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"

// Look up the company field
setnull(i_co_field)
select upper(trim(control_value)) into :i_co_field from cr_system_control
 where upper(trim(control_name)) = 'COMPANY FIELD';
if isnull(i_co_field) or i_co_field = "" then
	f_pp_msgs("ERROR: could not find the COMPANY FIELD in cr_system_control.")
	rollback;
	return -1
end if
i_co_field = f_cr_clean_string(i_co_field)



//*******************************************************************
//
//  First: truncate the table ... 
//		that will keep it from growing excessively.
//
//*******************************************************************
f_pp_msgs(" ")
f_pp_msgs("Truncating flattened table at " + string(now()))
if i_use_new_structures_table = "YES" then
	sqlca.truncate_table('cr_structures_flattened2');
else
	sqlca.truncate_table('cr_structures_flattened');
end if

if sqlca.SQLCode < 0 then
	if i_use_new_structures_table = "YES" then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Truncating cr_structures_flattened2: " + &
			sqlca.SQLErrText)
		f_pp_msgs(" ")
	else
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Truncating from cr_structures_flattened: " + &
			sqlca.SQLErrText)
		f_pp_msgs(" ")
	end if
	
	rollback;
	return -1
end if


//***************************************************************
//
//  Build datastore in the shape of the flattened table.
//
//***************************************************************
f_pp_msgs("Initializing the structure datastore at " + string(now()))

i_ds_structure = CREATE uo_ds_top

if i_use_new_structures_table = "YES" then
	sqls = "select * from cr_structures_flattened2 where structure_id = -1 and detail_point = '-1isthefakeone'"
else
	sqls = "select * from cr_structures_flattened where structure_id = -1 and value_id = -1"
end if

f_create_dynamic_ds(i_ds_structure, "grid", sqls, sqlca, true)


//************************************************************
//
//  Build datastore in the shape of the values table.
//
//************************************************************
f_pp_msgs("Initializing the structure detail points datastore at " + string(now()))

i_ds_points = CREATE uo_ds_top

if i_use_new_structures_table = "YES" then
	sqls = "select * from cr_structure_values2 where structure_id = -1 and detail_budget = -1 and status = -1"
else
	sqls = "select * from cr_structure_values where structure_id = -1 and detail_budget = -1 and status = -1"
end if

f_create_dynamic_ds(i_ds_points, "grid", sqls, sqlca, true)


//************************************************************
//
//  Build datastore for the levels of the structure
//
//************************************************************
f_pp_msgs("Initializing the levels datastore at " + string(now()))

i_ds_levels = CREATE uo_ds_top

if i_use_new_structures_table = "YES" then
	sqls = " select * "
	sqls += "from cr_structure_values2  "
	sqls += "where structure_id = -1 "
	sqls += "start with structure_id = -1 and element_value = '-1' "
	sqls += "connect by element_value = prior parent_value and structure_id = -1 "
	sqls += "order by level desc "

	f_create_dynamic_ds(i_ds_levels,'grid',sqls,sqlca,true)
//	i_ds_levels.DataObject = "dw_cr_structures_flattened_levels2"
else
	sqls = " select * "
	sqls += "from cr_structure_values  "
	sqls += "where structure_id = -1 "
	sqls += "start with structure_id = -1 and value_id = -1 "
	sqls += "connect by value_id = prior rollup_value_id and structure_id = -1 "
	sqls += "order by level desc "

	f_create_dynamic_ds(i_ds_levels,'grid',sqls,sqlca,true)
//	i_ds_levels.DataObject = "dw_cr_structures_flattened_levels"
end if

i_ds_levels.SetTransObject(sqlca)


//************************************************************
//
//  Build datastore for the levels of the structure
//
//************************************************************
f_pp_msgs("Initializing the description datastore at " + string(now()))

i_ds_descr = CREATE uo_ds_top

sqls = "select rpad(' ', 254) from dual"

f_create_dynamic_ds(i_ds_descr, "grid", sqls, sqlca, true)


//*******************************************************************
//
//  Build a datastore of the structures and loop through them
//		flattening them one by one
//
//*******************************************************************
f_pp_msgs("Retrieving structures to be flattened at " + string(now()))

//sqls = 'select structure_id, description from cr_structures order by description' - replace by Cathy for JIRA#PP-30296 (06/25/2013)
if isnull(g_command_line_args) or g_command_line_args = "" then
	sqls = 'select structure_id, description from cr_structures order by description'
else
	sqls = 'select structure_id, description from cr_structures where structure_id in (' + g_command_line_args + ') order by description'
end if
	
uo_ds_top ds_structures
ds_structures = create uo_ds_top

f_create_dynamic_ds(ds_structures,'grid',sqls,sqlca,true)

num_structures = ds_structures.rowcount()

for i = 1 to num_structures
	structure_id = ds_structures.getitemnumber(i,1)
	description  = ds_structures.getitemstring(i,2)
	description  = description + ' (' + string(structure_id) + ')'
	
	f_pp_msgs(" ")
	f_pp_msgs("----------------------------------------------------------------------------------------")
	f_pp_msgs("   ******  Starting " + description + " at " + string(now()) + "  *****")
	f_pp_msgs(" ")
	rtn = uf_flatten_one(structure_id)
	f_pp_msgs(" ")	
	f_pp_msgs("   ******  End of " + description + " at " + string(now()) + "  *****")
	f_pp_msgs("-----------------------------------------------------------------------------------------")
		
	if rtn < 0 then 
		rollback;
		return -1
	else
		// Will commit structure by structure for volume
		commit;
	end if
next


//*******************************************************************
//
//  Analyze the table now that all structures have been
//		flattened
//
//*******************************************************************
f_pp_msgs(" ")
f_pp_msgs("Analyzing flattened table at " + string(now()))
f_pp_msgs(" ")

if i_use_new_structures_table = "YES" then
	sqlca.analyze_table('cr_structures_flattened2');
else
	sqlca.analyze_table('cr_structures_flattened');
end if

if sqlca.SQLCode < 0 then
	if i_use_new_structures_table = "YES" then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Analyzing cr_structures_flattened2: " + &
			sqlca.SQLErrText)
		f_pp_msgs("The table was successfully built!")
		f_pp_msgs(" ")
	else
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Analyzing from cr_structures_flattened: " + &
			sqlca.SQLErrText)
		f_pp_msgs("The table was successfully built!")
		f_pp_msgs(" ")
	end if
end if

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_flatten_structures_custom_version nvo_cr_flatten_structures_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_flatten_structures_custom.pbd'
		return nvo_cr_flatten_structures_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

