HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Stub for the junky ppsystem_interface.pbl
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong 			i_bd_queue_count, i_source_id, i_max_id_processed, i_sort_order
string 		i_type, i_co, i_company_field, i_detail_table, i_stg_table, i_wo_field, i_proj_field, &
				i_mn_string, i_ce_field, i_ce_me_table, i_ce_me_table_field, i_posting_approval
uo_ds_top	i_ds_bd_queue, i_ds_counter, i_ds_ifb_id, i_ds_max_id, i_ds_interface_dates, i_ds_distinct_source_id, &
				i_ds_sum_amount, i_cr_deriver_type_sources, i_ds_null_rollup
uo_cr_derivation i_uo_cr_derivation
string			i_basis_mn, i_posting_mn
longlong	i_posted_source_ids[]
string			i_type_array[], i_co_array[]

string			i_exe_name = 'cr_batch_derivation.exe'
longlong	i_ce_me_by_company
longlong	i_derivation_type_on_stg
uo_cr_interco_balancing i_uo_cr_interco_balancing
uo_ds_top i_ds_months
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_txn ()
public function longlong uf_checkoob ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//*****************************************************************************************
//
//  Object      :  uo_cr_batch_derivation
//  UO Function :  uf_read
//
//	 Returns     :  1 if successful
// 					-1 otherwise
//
//*****************************************************************************************
longlong s, i, rtn, lvalidate_rtn, validation_counter, max_id, c, sequence
string sqls, detail_table, cv_validations, s_date, null_string_array[], co_val, &
		 co_ifb_array[], rc
boolean this_source_has_results, validation_kickouts, re_processing_a_prior_batch, overall_kickouts = false
date ddate
time ttime
datetime finished_at


//*****************************************************************************************
//
//  Get the interface_id from cr_interfaces ...
//    DO NOT perform an upper on the select value ... 
//      This must be before the check for invalid ids !
//
//*****************************************************************************************
f_pp_msgs("Retrieve interface_id for CR BATCH DERIVATION")

g_interface_id = 0
select interface_id into :g_interface_id from cr_interfaces
 where lower(description) = 'cr batch derivation';
if isnull(g_interface_id) then g_interface_id = 0
 
if g_interface_id = 0 then
	f_write_log(g_log_file, &
		"ERROR: No (CR Batch Derivation) entry exists in cr_interfaces ... Cannot run the " + &
		"CR Batch Derivation process!")
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: No (CR Batch Derivation) entry exists in cr_interfaces ... Cannot run the " + &
				 "CR Batch Derivation process!")
	f_pp_msgs("  ")
	g_do_not_write_batch_id = true
	return -1
end if

//*****************************************************************************************
//
//  ALL CONNECTED.  CHECK THE DEBUG VARIABLE.
//
//*****************************************************************************************
setnull(g_debug)
select upper(trim(control_value)) into :g_debug from cr_system_control
 where upper(trim(control_name)) = 'CR BATCH DERIVATION - DEBUG';
if g_debug <> "YES" then g_debug = "NO"


//*****************************************************************************************
//
//  Setup:
//
//*****************************************************************************************
f_pp_msgs("Performing setup")
//  Get the company field.
i_company_field = ""
select upper(trim(control_value)) into :i_company_field from cr_system_control
 where upper(trim(control_name)) = 'COMPANY FIELD';
i_company_field = f_cr_clean_string(i_company_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_company_field = " + i_company_field)
if isnull(i_company_field) or i_company_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COMPANY FIELD in cr_system_control!")
	f_pp_msgs("  ")
	return -1
end if

//  Get the work_order field.
i_wo_field = ""
select upper(trim(control_value)) into :i_wo_field from cr_system_control
 where upper(trim(control_name)) = 'WORK ORDER FIELD';
i_wo_field = f_cr_clean_string(i_wo_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_wo_field = " + i_wo_field)
if isnull(i_wo_field) or i_wo_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the WORK ORDER FIELD in cr_system_control!")
	f_pp_msgs("  ")
	return -1
end if

//  Get the funding_project field.
i_proj_field = ""
select upper(trim(control_value)) into :i_proj_field from cr_system_control
 where upper(trim(control_name)) = 'FUNDING PROJECT FIELD';
i_proj_field = f_cr_clean_string(i_proj_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_proj_field = " + i_proj_field)
if isnull(i_proj_field) or i_proj_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the FUNDING PROJECT FIELD in cr_system_control!")
	f_pp_msgs("  ")
	return -1
end if

//  Get the cost_element field.
i_ce_field = ""
select upper(trim(control_value)) into :i_ce_field from cr_system_control
 where upper(trim(control_name)) = 'COST ELEMENT FIELD';
i_ce_field = f_cr_clean_string(i_ce_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_field = " + i_ce_field)
if isnull(i_ce_field) or i_ce_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT FIELD in cr_system_control!")
	f_pp_msgs("  ")
	return -1
end if

// ### 7924:  JAK: 2011-06-27:  Values by company logic.
i_ce_me_table = ""
i_ce_me_table_field = ""
i_ce_me_by_company = 0
select upper(trim(element_table)), upper(trim(element_column)), values_by_company
  into :i_ce_me_table, :i_ce_me_table_field, :i_ce_me_by_company
  from cr_elements
 where upper(replace(replace(replace(description,' ','_'),'-','_'),'/','_')) = :i_ce_field;
if i_ce_me_by_company <> 1 or isnull(i_ce_me_by_company) then i_ce_me_by_company = 0
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_me_table = " + i_ce_me_table)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_me_table_field = " + i_ce_me_table_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_me_by_company = " + string(i_ce_me_by_company))
if isnull(i_ce_me_table) or i_ce_me_table = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT MASTER ELEMENT TABLE in cr_elements!")
	f_pp_msgs("  ")
	return -1
end if
if isnull(i_ce_me_table_field) or i_ce_me_table_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT MASTER ELEMENT TABLE in cr_elements!")
	f_pp_msgs("  ")
	return -1
end if

//  Check if we are using posting approvals
i_posting_approval = ""
select upper(trim(control_value)) into :i_posting_approval from cr_system_control
 where upper(trim(control_name)) = 'CR POSTING - OBEY APPROVAL';
if isnull(i_posting_approval) or i_posting_approval <> 'YES' then i_posting_approval = 'NO'
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_posting_approval = " + i_posting_approval)
if isnull(i_ce_field) or i_ce_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT FIELD in cr_system_control!")
	f_pp_msgs("  ")
	return -1
end if


//  Get the basis month_number ... I'm having some debate with myself about a single value for
//  this concept, or a month number range.  PROS:  1) If they miss something historically (due to
//  not passing the basis, having a wo exclusion, etc.), we would be able to catch it up ...
//  2) others?  CONS:  1) Performance could be whacked ... 2) Non-trivial, we'd have to summarize
//  away month number (or something like that) to avoid creating a zillion records in the current
//  period ... 3) I'm not wild about building in life-to-date logic in this process - I want the
//  batch derivations to essentially behave as derivations would if they were called from within
//  transaction interface - I also don't want to duplicate the fundamental logic that already
//  exists in the trueup (we have enough stuff kludged up in PP that does the same fundamental thing.
//  I don't relish the thought of adding another).
i_basis_mn = ''
select control_value into :i_basis_mn from cr_alloc_system_control
 where upper(trim(control_name)) = 'CR BATCH DERIVATION BASIS MN';
if isnull(i_basis_mn) or trim(i_basis_mn) = '' or trim(i_basis_mn) = '0' then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Basis Month Number not found in cr_alloc_system_control !")
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  For the distinct sources that we will process.  Just a "union" since
//  we want the distinct list.
i_ds_distinct_source_id = CREATE uo_ds_top
sqls = &
	"select distinct source_id from cr_batch_derivation_control where source_id not in (-3) " + &
	"union " + &
	"select distinct source_id from cr_batch_derivation_queue"
f_create_dynamic_ds(i_ds_distinct_source_id, "grid", sqls, sqlca, true)

//  For counting in uf_txn().
i_ds_counter = CREATE uo_ds_top
sqls = "select count(*) from cr_cost_repository where 1 = 0"
f_create_dynamic_ds(i_ds_counter, "grid", sqls, sqlca, true)

//  For determining the ifb_id when there are validation kickouts.
i_ds_ifb_id = CREATE uo_ds_top
sqls = "select min(interface_batch_id) from cr_allocations_stg where 1 = 0"
f_create_dynamic_ds(i_ds_ifb_id, "grid", sqls, sqlca, true)

//  For determining the max(id) in the stg table during derivations.
i_ds_max_id = CREATE uo_ds_top
sqls = "select max(id) from cr_allocations_stg where 1 = 0"
f_create_dynamic_ds(i_ds_max_id, "grid", sqls, sqlca, true)

//  For balancing the results to the original transactions.
i_ds_sum_amount = CREATE uo_ds_top
sqls = "select sum(amount) from cr_allocations_stg where 1 = 0"
f_create_dynamic_ds(i_ds_sum_amount, "grid", sqls, sqlca, true)

//  For the update derivations.
i_cr_deriver_type_sources = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources where source_id = 2122789"
f_create_dynamic_ds(i_cr_deriver_type_sources, "grid", sqls, sqlca, true)

i_ds_interface_dates = CREATE uo_ds_top
sqls = &
	"select month_number, month_period, sum(amount), " + &
			 "count(*), sum(decode(sign(amount),1,amount,0)), " + &
			 "sum(decode(sign(amount),-1,amount,0)) " + &
	  "from cr_cost_repository " + &
	 "where id = -678 " + &
	 "group by month_number, month_period"
f_create_dynamic_ds(i_ds_interface_dates, "grid", sqls, sqlca, true)

//  For checking for NULL values in the cr_derivation_rollup field.
i_ds_null_rollup = CREATE uo_ds_top
sqls = 'select distinct "' + i_ce_field + '" from cr_cost_repository where 1 = 2'
f_create_dynamic_ds(i_ds_null_rollup, "grid", sqls, sqlca, true)

// SEK 080910: Changed datastore to uo_ds_top
uo_ds_top ds_co
ds_co = CREATE uo_ds_top
sqls = "select distinct " + i_company_field + " from cr_cost_repository where 1 = 2"
f_create_dynamic_ds(ds_co, "grid", sqls, sqlca, true)

i_uo_cr_derivation = CREATE uo_cr_derivation
i_uo_cr_interco_balancing = CREATE uo_cr_interco_balancing

uo_cr_validation l_uo_cr_validation
l_uo_cr_validation = CREATE uo_cr_validation


// ### 10600: JAK: 2012-06-27:  RAC and Sequence Caching:  
//  If the database is using RAC and sequences are cached, each server is allocated it's own cache.  
//  This can cause an issue with batch derivations as the sequence can generate IDs lower than the IDs used by the other server.
select pp_sequence_clear_cache('crdetail') into :sequence
	from dual;
	
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Clearing any prior kickouts (cr_validations_invalid_ids2): " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
elseif sequence = -1 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: CRDETAIL Sequence increment by value is invalid.  Please contact the DBA to modify the sequence to increment by 1.")
	f_pp_msgs("  ")
	rollback;
	return -1
elseif g_debug = "YES" then 
	if sequence = 0 then f_pp_msgs("***DEBUG*** Non-RAC environment.  No sequence numbers were consumed.")
	if sequence > 0 then f_pp_msgs("***DEBUG*** RAC environment.  Cached sequences values were consumed.  Current sequence value = " + string(sequence))
end if
	

//*****************************************************************************************
//
//  Analyze some tables.
//
//*****************************************************************************************
sqlca.analyze_table('cr_batch_derivation_control')
sqlca.analyze_table('cr_batch_derivation_company')
sqlca.analyze_table('cr_batch_derivation_exclusion')
sqlca.analyze_table('cr_batch_derivation_excl_wo')



//*****************************************************************************************
//
//  There is a CR Alloc System Control value that determines the posting month number
//  for these transactions.  Since this process is looping over CR sources, and
//  validations will fire for each source, we will be alerted later if this MN
//  is closed for any of the company/source combinations.
//
//*****************************************************************************************
f_pp_msgs("Retrieving month number")

i_posting_mn = '0'
select control_value into :i_posting_mn from cr_alloc_system_control
 where upper(trim(control_name)) = 'CR BATCH DERIVATION POSTING MN';
if isnull(i_posting_mn) or trim(i_posting_mn) = '' or trim(i_posting_mn) = '0' then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Month Number not found in cr_alloc_system_control !")
	f_pp_msgs("  ")
	rollback;
	return -1
end if

f_pp_msgs("The basis month number is: " + i_basis_mn)
f_pp_msgs("Transactions will post to the following month number: " + i_posting_mn)


//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:  Just in case we need to mess around really early (like
//                         fiddling with cr_deriver_control or something).
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom("way up-front", 0, " ", "too early for a table name")

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if


//*****************************************************************************************
//
//  OUTERMOST LOOP:  Over the list of source_ids to process.  We know that this is the
//                   master list of sources configured for batch derivations.  Below, we
//                   will worry about "Cycle" vs. "On Demand" for each source.
//
//*****************************************************************************************
	FOR s = 1 TO i_ds_distinct_source_id.RowCount()
	
	setnull(g_batch_id)
	
	i_source_id = i_ds_distinct_source_id.GetItemNumber(s, 1)
	
	select lower(table_name) into :i_detail_table 
	  from cr_sources
	 where source_id = :i_source_id;
	
	i_stg_table = i_detail_table + "_stg"
	i_stg_table = left(i_stg_table, 2) + "b" + right(i_stg_table, len(i_stg_table) - 2)
	
	f_pp_msgs("  ")
	f_pp_msgs("*************************************************************************************")
	f_pp_msgs("Source Id: " + string(i_source_id))
	f_pp_msgs("DTL Table: " + i_detail_table)
	f_pp_msgs("*************************************************************************************")
	f_pp_msgs("  ")
	
	// ### 9992: JAK: 2012-06-21:  Check for CR_DERIVATION_TYPE up front.
	i_derivation_type_on_stg = 0
	select count(*) into :i_derivation_type_on_stg from all_tab_columns
	 where table_name = upper(:i_stg_table) and column_name = 'CR_DERIVATION_TYPE';
	if isnull(i_derivation_type_on_stg) then i_derivation_type_on_stg = 0

	//
	//  CLEAR OUT ANY DERIVATION RECORDS THAT ARE LEFT OVER FROM PRIOR VALIDATION KICKOUTS
	//  OR FROM AN OOB CONDITION.
	//
	//  We must also fire code similar to uf_delete_invalid_ids in case the TARGET or OFFSET
	//  txns kicked in validations.  Otherwise we will strand these records in the invalid
	//  ids table which would cause problems later.
	//
	//  FOR CRB DERIVATIONS:  SINCE THE CRB STAGING TABLES ARE ONLY USED FOR THIS PROCESS,
	//  AND SINCE WE ARE ABOUT TO TRUNCATE THE TABLES, WE'LL OMIT ANY WHERE CLAUSES REFERENCING
	//  IDS AND SUCH (LIKE WE HAVE IN TRANSACTION INTERFACES).
	//
	f_pp_msgs("Clearing any prior kickouts (cr_validations_invalid_ids2)")
	
	sqls = &
		"delete from cr_validations_invalid_ids2 " + &
		 "where lower(table_name) = '" + i_stg_table + "' "
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Clearing any prior kickouts (cr_validations_invalid_ids2): " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	
	f_pp_msgs("Clearing any prior kickouts (cr_validations_invalid_ids)")
	
	sqls = &
		"delete from cr_validations_invalid_ids " + &
		 "where lower(table_name) = '" + i_stg_table + "'"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Deleting any prior kickouts (cr_validations_invalid_ids): " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	//  Trunc the crb staging table.
	sqlca.truncate_table(i_stg_table)
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: truncating " + i_stg_table + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	//	 "If this interface ran previously but stopped b/c of validation kickouts, I don't want 
	//  to get any new data until the previous run has been loaded."
	//
	//  Were there validation kikcouts or other problems with the prior run ?  If so, there will
	//  be records left in the CR detail table that are marked as 'I'.  If that is the case,
	//  re-process them.  This functions just as an interface would and makes the logic consistent
	//  with transaction interface controls.
	sqls = &
		"select count(*) from " + i_detail_table + &
		" where month_number in (" + i_basis_mn + ") " + &
		  " and cr_derivation_status = 'I'"
	i_ds_counter.SetSQLSelect(sqls)
	i_ds_counter.RETRIEVE()
	
	if i_ds_counter.RowCount() <= 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: count(*) where cr_derivation_status = 'I' is <= 0 for: " + i_detail_table)
		f_pp_msgs("  ")
		rollback;
		return -1
	else
		if i_ds_counter.GetItemNumber(1, 1) > 0 then
			//  We are reprocessing a prior batch.  Do not mark any new transactions.
			//  with an 'I'.
			f_pp_msgs("*****  RE-PROCESSING A PRIOR BATCH  *****")
			f_pp_msgs("  ")
			re_processing_a_prior_batch = true
		else
			//  We used to have the "update to 'I'" code here, but it would hit companies
			//  that should not be part of this run.
			re_processing_a_prior_batch = false
		end if
	end if
	
	
	
	
	
	//*****************************************************************************************
	//
	//  Determine the Batch Derivation Queue:
	//
	//  The queue is a combination of those types/sources that whose timing
	//  indicator = 'Cycle', plus, and 'On Demand' combinations that have been submitted
	//  since the last run.  For the 'Cycle' records, the cr_batch_derivation_company table
	//  determines which companies are processed.  Note: not "union all" since we want a
	//  distinct list.
	//
	//  The PK on cr_batch_derivation_control is simply (derivation) TYPE and SOURCE_ID.
	//  Thus, if a source is defined as 'Cycle' it must cycle for all companies.  Otherwise,
	//  define it as 'On Demand' and each company must submit their "run" of the batch
	//  derivations individually.
	//
	//*****************************************************************************************
	i_ds_bd_queue = CREATE uo_ds_top
	
	sqls = &
		'select a.sort_order, a."TYPE", b.company ' + &
		  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
		 "where a.timing = 'Cycle' and a.source_id = " + string(i_source_id) + &
		" union " + &
		'select sort_order, "TYPE", company ' + &
		  "from cr_batch_derivation_queue where source_id = " + string(i_source_id)
	
	f_create_dynamic_ds(i_ds_bd_queue, "grid", sqls, sqlca, true)
	
	i_ds_bd_queue.SetSort("sort_order a, type a, company a")
	i_ds_bd_queue.Sort()
	
	i_bd_queue_count = i_ds_bd_queue.RowCount()
	
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_bd_queue_count = " + string(i_bd_queue_count))
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ds_bd_queue sqls = " + sqls)
	
	if i_bd_queue_count <= 0 then
		f_pp_msgs("No records returned from the queue.  There are no batch derivations to be processed.")
		f_pp_msgs("  ")
		continue  //  On to the next source.
	end if
	
	
	
	//*****************************************************************************************
	//
	//  Loop over the queue and process each "TYPE", company that has been
	//  submitted for processing.
	//
	//*****************************************************************************************
	f_pp_msgs("Starting the Batch Derivation process")
	f_pp_msgs("  ")
	
	if g_debug = "YES" then i_uo_cr_derivation.i_debug = "YES"
	
	this_source_has_results = false
	
	i_type_array = null_string_array
	i_co_array   = null_string_array
	
	for i = 1 to i_bd_queue_count
		
		i_sort_order = i_ds_bd_queue.GetItemNumber(i, "sort_order")
		i_type       = i_ds_bd_queue.GetItemString(i, "type")
		i_co         = i_ds_bd_queue.GetItemString(i, "company")
		
		i_type_array[upperbound(i_type_array) + 1] = i_type
		i_co_array[upperbound(i_co_array) + 1]     = i_co
		
		f_pp_msgs("-------------------------------------------------------------------------------------")
		f_pp_msgs("Derivation Type: " + i_type)
		f_pp_msgs(i_company_field + ": " + i_co)
		f_pp_msgs("-------------------------------------------------------------------------------------")
		
		if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_detail_table = " + i_detail_table)
		if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_stg_table = " + i_stg_table)
		
		//  Mark the records in the CR detail table to be processed.  This was originally in code above,
		//  but would hit companies that should not be in this run.
		//
		//  If we ARE NOT re-processing a prior batch, we need to mark the cr_derivation_status
		//  to 'I' at this point.
		// ### 30099: JAK: 2013-05-28: Only apply these during the first pass through the interface so we don't flag records 
		//		on the 2nd pass when it should have gone through the first derivation.
		// ### 36542: JAK: 2013-02-21: Change in 30099 doesn't work when multiple companies are used.  Removing the company restrictions below
		//		from the 3 SQL statements.
		if not re_processing_a_prior_batch and i = 1 then
			
			//  1: Apply GL Journal Category exclusions.  Mark with the "negative" of the GL Journal Category
			//  value.  That will allow us to null out those records if a client ever incorrectly enters
			//  a GL JCAT value in the exclusion table.
			f_pp_msgs("Applying GL Journal Category exclusions")
			sqls = "update " + i_detail_table + " set cr_derivation_status = '-'||substr(gl_journal_category,1,34) " + &
				"where month_number in (" + i_basis_mn + ") and cr_derivation_status is null " + &
				  "and gl_journal_category in (select gl_journal_category from cr_batch_derivation_exclusion)"
			execute immediate :sqls;
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: Applying GL Journal Category exclusions: " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				rollback;
				return -1
			end if
			if g_debug = "YES" then f_pp_msgs("***DEBUG*** gl journal category exclusion sql = " + sqls)
			
			//  2: Apply Work Order exclusions.  Mark with the "negative" of the Work Order
			//  value.  That will allow us to null out those records if a client ever incorrectly enters
			//  a WO value in the exclusion table.
			f_pp_msgs("Applying Work Order exclusions")
			sqls = "update " + i_detail_table + " set cr_derivation_status = '-'||substr(" + i_wo_field + ",1,34) " + &
				"where month_number in (" + i_basis_mn + ") and cr_derivation_status is null " + &
				  "and " + i_wo_field + " in (select work_order_number from cr_batch_derivation_excl_wo)"
			execute immediate :sqls;
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: Applying Work Order exclusions: " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				rollback;
				return -1
			end if
			
			if g_debug = "YES" then f_pp_msgs("***DEBUG*** WO exclusion sql = " + sqls)
			
			//  3: Update CR Derivation Status to 'I'.
			//	JAK 2010-11-30: Only do the posting check if approvals are turned on.  This is a costly check
			//		if approvals aren't being used.  Also, added month number to the link for performance
			//  SEK 042010: Only mark records with 'I' if this data has been posted to the GL, if
			//		the data has been approved to be posted to the GL (batch_id = 999999999999999), or
			//		if the data came from the GL and won't be posted... batch_id is not null
			sqls = "update " + i_detail_table + " set cr_derivation_status = 'I' " + &
				"where month_number in (" + i_basis_mn + ") and cr_derivation_status is null "
				 
			if i_posting_approval = 'YES' then
				sqls += "and exists ( select 1 from cr_cost_repository cr " + &
					" where cr.drilldown_key = " + i_detail_table + ".drilldown_key " + &
					" and cr.month_number = " + i_detail_table + ".month_number " + &
					" and cr.batch_id is not null)"
			end if
			
			execute immediate :sqls;
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_status to 'I': " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				rollback;
				return -1
			end if
			
			if g_debug = "YES" then f_pp_msgs("***DEBUG*** set derivation status to I = " + sqls)
			
		end if
		
		//  Call the "transaction interface" logic.
		rtn = uf_txn()
		
		f_pp_msgs("  ")
		
		if rtn = 2 then continue  //  No TARGET records generated.  Nothing to do.
		
		if rtn <> 1 then
			//  Messages handled in uf_txn().
			rollback;
			return -1
		end if
		
		this_source_has_results = true  //  If we're here, then we posted some results.
		
	next  //  for i = 1 to i_bd_queue_count ... types and companies ...
	
	//*****************************************************************************************
	//
	//  THE FOLLOWING DOES NOT MAKE SENSE IF THERE WERE NO RESULTS:
	//
	//*****************************************************************************************
	f_pp_msgs("Completing the derivation logic for source_id = " + string(i_source_id))
	f_pp_msgs("This source has results = " + string(this_source_has_results))
	
	if this_source_has_results then
		//  Continue on.
	else
		goto after_summarization
	end if
	
	
	
	//*****************************************************************************************
	//
	//  CUSTOM FUNCTION CALL:
	//
	//*****************************************************************************************
	rc = f_cr_batch_derivation_custom("before validations", 0, " ", i_stg_table)
	
	if rc = "ERROR" then
		//  Put the messages in the custom function.
		rollback;
		return -1
	end if
	
	
	//*****************************************************************************************
	//
	//	 VALIDATIONS:  Call the new validations user object.  It is assumed that combos have
	//						already been created.
	//
	//  For this process, we will simply update the interface_batch_id to -1 since we
	//  may not want to burn the nextval at this point.
	//
	//*****************************************************************************************
	// ### 9992: JAK: 2012-06-21:  Interface Batch ID already set to 0 -- don't need to reset it now.
//	f_pp_msgs("Updating interface_batch_id to -1")
//	
//	sqls = "update " + i_stg_table + " set interface_batch_id = -1"
//	
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating " + i_stg_table + ".interface_batch_id to -1: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		rollback;
//		return -1
//	end if
	
	f_pp_msgs("Analyzing stg and other tables")
	sqlca.analyze_table(i_stg_table)
	
	sqlca.analyze_table('cr_validations_invalid_ids')
	
	f_pp_msgs("Running validations")
	
	//  MOVED ABOVE DUE TO THE LOOPS.
	//uo_cr_validation l_uo_cr_validation
	//l_uo_cr_validation = CREATE uo_cr_validation
	
	f_pp_msgs(" -- Deleting from cr_validations_invalid_ids")
	
	lvalidate_rtn = l_uo_cr_validation.uf_delete_invalid_ids(i_stg_table, "0")
	
	if lvalidate_rtn <> 1 then return -1
	
	validation_kickouts = false
	
	setnull(cv_validations)
	select upper(trim(control_value)) into :cv_validations from cr_system_control 
	 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - COMBO';
	if isnull(cv_validations) then cv_validations = "NO"
	
	if cv_validations <> "YES" then goto after_combo_validations
	
	
	f_pp_msgs(" -- Validating the Batch (Combos)")
	
	//	 Which function is this client using?
	setnull(cv_validations)
	select upper(trim(control_value)) into :cv_validations from cr_system_control 
	 where upper(trim(control_name)) = 'VALIDATIONS - CONTROL OR COMBOS';
	if isnull(cv_validations) then cv_validations = "CONTROL"
	
	if cv_validations = 'CONTROL' then
		lvalidate_rtn = l_uo_cr_validation.uf_validate_control(i_stg_table, "0")
	else
		lvalidate_rtn = l_uo_cr_validation.uf_validate_combos(i_stg_table, "0")
	end if
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate_(x).  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 then
		//  Validation Kickouts.
		validation_counter = 0
		select count(*) into :validation_counter	from cr_validations_invalid_ids
		 where lower(table_name) = :i_stg_table;
		if validation_counter = 0 then
		else
			validation_kickouts = true
		end if
	end if
	
	after_combo_validations:
	
	
	setnull(cv_validations)
	select upper(trim(control_value)) into :cv_validations from cr_system_control 
	 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
	if isnull(cv_validations) then cv_validations = "NO"
	
	if cv_validations <> "YES" then goto after_project_validations
	
	f_pp_msgs(" -- Validating the Batch (Projects)")
	
	lvalidate_rtn = l_uo_cr_validation.uf_validate_projects(i_stg_table, "0")
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 then
		//  Validation Kickouts.
		validation_counter = 0
		select count(*) into :validation_counter	from cr_validations_invalid_ids
		 where lower(table_name) = :i_stg_table;
		if validation_counter = 0 then
		else
			validation_kickouts = true
		end if
	end if
	
	after_project_validations:
	
	
	setnull(cv_validations)
	select upper(trim(control_value)) into :cv_validations from cr_system_control 
	 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - ME';
	if isnull(cv_validations) then cv_validations = "NO"
	
	if cv_validations <> "YES" then goto after_me_validations
	
	f_pp_msgs(" -- Validating the Batch (Element Values)")
	
	lvalidate_rtn = l_uo_cr_validation.uf_validate_me(i_stg_table, "0")
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 then
		//  Validation Kickouts.
		validation_counter = 0
		select count(*) into :validation_counter	from cr_validations_invalid_ids
		 where lower(table_name) = :i_stg_table;
		if validation_counter = 0 then
		else
			validation_kickouts = true
		end if
	end if
	
	after_me_validations:
	
	
	setnull(cv_validations)
	select upper(trim(control_value)) into :cv_validations from cr_system_control 
	 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MONTH NUMBER';
	if isnull(cv_validations) then cv_validations = "NO"
	
	if cv_validations <> "YES" then goto after_mn_validations
	
	f_pp_msgs(" -- Validating the Batch (Open Month Number)")
	
	lvalidate_rtn = l_uo_cr_validation.uf_validate_month_number(i_stg_table, "0")
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 or lvalidate_rtn = -3 then
		//CURRENTLY NOT INSERT INTO THE INVALID IDS TABLE.
		//  Validation Kickouts.
	//	validation_counter = 0
	//	select count(*) into :validation_counter	from cr_validations_invalid_ids
	//	 where lower(table_name) = 'cr_gas_master_stg';
	//	if validation_counter = 0 then
	//	else
			validation_kickouts = true
	//	end if
	end if
	
	after_mn_validations:
	
	
	f_pp_msgs(" -- Validating the Batch (Custom Validations)")
	
	lvalidate_rtn = l_uo_cr_validation.uf_validate_custom(i_stg_table, "0")
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 then
		//  Validation Kickouts.
		validation_counter = 0
		select count(*) into :validation_counter	from cr_validations_invalid_ids
		 where lower(table_name) = :i_stg_table;
		if validation_counter = 0 then
			//  Historical accident from interfaces at another client ... not sure why I would ever
			//  want to continue on and post the txns if there is a validation kickout!  Especially
			//  with the custom function since it is easy to code SQL in the insert of that fxn that
			//  may fail.
			validation_kickouts = true
		else
			validation_kickouts = true
		end if
	end if
	
	
	
	///////////////////////////////////////////  PROCESS SUSPENSE  ///////////////////////////////////////////
	
	if validation_kickouts then
		
		
		// N/A FOR THIS PROCESS AT THE MOMENT ... THIS IS AN EXAMPLE OF HOW TO HOLD TRANSACTIONS IN THE STAGING
		// TABLE IF CERTAIN CONDITIONS SHOULD NOT TRIGGER SUSPENSE ACCOUNTING.  FOR NOW, SUSPENSE
		// WILL ALWAYS BE TRIGGERED AT SOUTHERN.
		//	//  For PNW, hold the batch if there were charge numbers that did not exist in
		//	//  cr_deriver_control. That condition was detected above, but we want to run the full
		//	//  validation processing before terminating the interface.  This condition will prevent
		//	//  suspense accounting and will hold the batch to be reviewed in the kickouts window.
		//	if cn_invalid or invalid_mn or missing_fp then
		//		//  Set the variable below to the "hold the batch" value and skip the suspense function.
		//		lvalidate_rtn = 2
		//		goto after_suspense_function
		//	end if
		
		
		//  Call the suspense accounting function.  It will return a value of 2 if 
		//  suspense accounting is turned off in cr_system_control.
		lvalidate_rtn = l_uo_cr_validation.uf_suspense(i_stg_table, "0")
		
		if lvalidate_rtn < 0 then
			//  Error in uf_validate.  Logged by the function.
			return -1
		end if
		
		
		//	after_suspense_function:  // N/A FOR THIS PROCESS AT THE MOMENT ... SEE ABOVE.
		
		
		if lvalidate_rtn = 2 then
		
			f_pp_msgs("  ")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE BATCH WILL NOT BE LOADED!  *****") 
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("  ")
			
			// ### 8142: JAK: 2011-11-01:  Consistent return codes
			overall_kickouts = true
		else
			
			f_pp_msgs("  ")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  " + &
				"SUSPENSE ACCOUNTS HAVE BEEN APPLIED AND THE BATCH WILL BE POSTED!  *****")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"-------------------------------------------------------------")
			f_pp_msgs("  ")
			
		end if
		
		if lvalidate_rtn = 2 then
			//  No suspense accounting ... hold the batch.  Goto a spot down below that will avoid
			//  the code that posts into the detail and summary tables.
			goto after_posting  //  return -1 (this is what an interface has)
		end if
	end if
	
	
	
	///////////////////////////////////////  END OF: PROCESS SUSPENSE  ///////////////////////////////////////
	
	
	
	f_pp_msgs("Validations complete")
	
	
	
	//
	//  PLACEHOLDER FOR FUTURE INTERCO PROCESSING ... NOT NEEDED INITIALLY AT SOUTHERN
	//
	//f_pp_msgs("NULLing out drilldown_key on INTERCO")
	//
	//update cr_gas_master_stg
	//	set drilldown_key = NULL
	// where drilldown_key = 'INTERCO';
	//
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("(GM) ERROR: NULLing out drilldown_key on INTERCO: " + &
	//		sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	rollback;
	//	return -1
	//end if
	
	
	//after_validations:
	
	
	//*****************************************************************************************
	//
	//	 UPDATE the interface_batch_id ... we do this here, so we can be at the latest
	//  possible spot where we know we have some results (so we don't burn nextvals for
	//  no good reason --- since we will be truncating the stg table and re-running in the
	//  event of kickouts).
	//
	//*****************************************************************************************
	f_pp_msgs("Updating " + i_stg_table + ".interface_batch_id")
	
	//  Get the distinct list of companies ... for ifb_id by company.
	sqls = "select distinct " + i_company_field + " from " + i_stg_table
	ds_co.SetSQLSelect(sqls)
	ds_co.RETRIEVE()
	
	co_ifb_array = null_string_array
	
	for c = 1 to ds_co.RowCount()
		
		co_val = ds_co.GetItemString(c, 1)
		
		f_pp_msgs("--" + i_company_field + " = " + co_val)
		
		setnull(g_batch_id) // added in this loop to trigger one per company
		
		if isnull(g_batch_id) then
			//  This is the first source in this run to post any transactions.  Get the
			//  nextval here.
			select costrepository.nextval into :g_batch_id from dual;
			if isnull(g_batch_id) or g_batch_id = "" then g_batch_id = '0'
			
			if g_batch_id = '0' then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: Could not get the interface_batch_id from costrepository.nextval")
				f_pp_msgs("  ")
				rollback;
				return -1
			end if
		end if
		
		co_ifb_array[c] = g_batch_id
		
		sqls = "update " + i_stg_table + " set interface_batch_id = '" + g_batch_id + "' " + &
				  "where " + i_company_field + " = '" + co_val + "'"
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating " + i_stg_table + ".interface_batch_id: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
	
	next
	
	
	//*****************************************************************************************
	//
	//	 INSERT FROM i_stg_table TO i_detail_table.
	//
	//*****************************************************************************************
	f_pp_msgs("Inserting into " + i_detail_table)
	
	sqls = "insert into " + i_detail_table + " select * from " + i_stg_table
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Inserting into " + i_detail_table + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	
	
	//*****************************************************************************************
	//
	//  UPDATE cr_cost_repository ...
	//
	//*****************************************************************************************
	f_pp_msgs("Updating cr_cost_repository")
	
	uo_cr_cost_repository uo_cr
	uo_cr = CREATE uo_cr_cost_repository
	
	for c = 1 to ds_co.RowCount()
		
		co_val = ds_co.GetItemString(c, 1)
		
		f_pp_msgs("--" + i_company_field + " = " + co_val)
		
		g_batch_id = co_ifb_array[c]
		
		//pass the source_id and the batch
		rtn = uo_cr.uf_insert_to_cr_batch_id(i_source_id, string(g_batch_id))
		
		if rtn = 1 then
		else
			choose case rtn
				case -1
					f_pp_msgs("ERROR: inserting into cr_temp_cr: " + sqlca.SQLErrText)
				case -2
					f_pp_msgs("ERROR: cr_temp_cr.id UPDATE failed: " + sqlca.SQLErrText)			
				case -3
					f_pp_msgs("ERROR: cr_temp_cr.drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
				case -4
					f_pp_msgs("ERROR: " + i_detail_table + ".drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
				case -5
					f_pp_msgs("ERROR: inserting into cr_cost_repository: " + sqlca.SQLErrText)			
				case -6
					f_pp_msgs("ERROR: deleting from cr_temp_cr UPDATE failed: " + sqlca.SQLErrText)			
				case else
					f_pp_msgs("ERROR: unknown error in uo_cr_cost_repository: " + sqlca.SQLErrText)			
			end choose
			rollback;
			return -1		
		end if
	
	next
	
	
	
	//*****************************************************************************************
	//
	//  CUSTOM FUNCTION CALL:  For SAP-like clients you'd want to mark the cwip_charge_status
	//                         to -id on the offsets (assuming that the "original" txns were
	//                         marked in the same manner.  This is a good place to do that.
	//
	//*****************************************************************************************
	rc = f_cr_batch_derivation_custom("just after summarization", 0, " ", "too late for a table name")
	
	if rc = "ERROR" then
		//  Put the messages in the custom function.
		rollback;
		return -1
	end if
	
	
	after_summarization:
	
	
	
	//*****************************************************************************************
	//
	//  TRUNCATE i_stg_table ...
	//
	//*****************************************************************************************
	f_pp_msgs("Deleting all records from " + i_stg_table)
	
	sqlca.truncate_table(i_stg_table)
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Truncating " + i_stg_table + ": " + sqlca.SQLErrtext)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	
	
	//*****************************************************************************************
	//
	//  PREP FOR LATER ... NOT CLEAR IF WE WILL KEEP THIS CODE.
	//
	//*****************************************************************************************
	max_id = 0
	sqls = "select max(id) from " + i_detail_table + " where cr_derivation_status = 'I'"
	i_ds_max_id.SetSQLSelect(sqls)
	i_ds_max_id.RETRIEVE()
	if i_ds_max_id.RowCount() <= 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: selecting the max(id) from " + i_detail_table + " (no rows returned): " + sqlca.SQLErrtext)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	max_id = i_ds_max_id.GetItemNumber(1, 1)
	if isnull(max_id) then max_id = 0
	
	
	
	//*****************************************************************************************
	//
	//  MARK THE BASIS RECORDS WITH A 'P' FOR PROCESSED.
	//  CHANGE: MARK THEM WITH THE IFB ID SO THEY CAN BE UN-MARKED IF A USER DELETES
	//          THE INTERFACE BATCH.
	//
	//    We still want to do this when there were no results generated so the
	//    process does not keep spinning through the same records over and over again.
	//    
	//    Note: the statement directly above means that transactions where we do not find
	//    a match in CDC.STRING will also be marked and will not re-process.  This works
	//    the same as a transaction interface where the txns do not get any derivation
	//    results and the trueup must perform the "initial" split at some point.
	//    A "default" derivation type can aid with this (e.g. to prevent $$$ from being
	//    stranded in a clearing account).
	//
	//*****************************************************************************************
	f_pp_msgs("Updating cr_derivation_status with Interface Batch Id")
	
	if this_source_has_results then
		
		for c = 1 to ds_co.RowCount()
			
			co_val = ds_co.GetItemString(c, 1)
			
			f_pp_msgs("--" + i_company_field + " = " + co_val)
			
			g_batch_id = co_ifb_array[c]
			
			//  Update ifb_id with the g_batch_id variable we got above.
			sqls = "update " + i_detail_table + " set cr_derivation_status = '" + g_batch_id + "' " + &
					  "where month_number in (" + i_basis_mn + ") and cr_derivation_status = 'I' " + &
						 "and " + i_company_field + " = '" + co_val + "'"
			
			if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
			
			execute immediate :sqls;
			
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_status with interface batch id: " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				f_pp_msgs("sqls = " + sqls)
				f_pp_msgs("  ")
				rollback;
				return -1
			end if
		next
	end if

	// JAK: always run this update.  Anything remaining means it didn't have results so flag it as such so a 2nd
	//	pass isn't needed.
	
	//  No results means no interface batch, but we still want to mark the processed records so
	//  they don't continue to be processed over and over again.  Let's just use negative id.
	//  This will give us the ability to gracefully null these out at some point if they decide
	//  that these records really should have created results (e.g. perhaps due to missing CDC
	//  records at the time this ran).
	sqls = "update " + i_detail_table + " set cr_derivation_status = -id " + &
			  "where month_number in (" + i_basis_mn + ") and cr_derivation_status = 'I'"
	
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_status with interface batch id: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
		
	//*****************************************************************************************
	//
	//  INSERT the batch derivation results ...
	//
	//    We still want to do this when there were no results generated so the
	//    max_id_processed gets incremented.
	//
	//*****************************************************************************************
	f_pp_msgs("Inserting batch derivation results")
	
	s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
	ddate  = date(left(s_date, 10))
	ttime  = time(right(s_date, 8))
	
	finished_at = datetime(ddate, ttime)
	g_finished_at = finished_at
	
	for i = 1 to upperbound(i_type_array)
		
		if this_source_has_results then
			for c = 1 to ds_co.RowCount()
				if i_co_array[i] = ds_co.GetItemString(c, 1) then
					g_batch_id = co_ifb_array[c]
				end if
			next
		else
			setnull(g_batch_id)
		end if
		
		insert into cr_batch_derivation_results
			(sort_order, "TYPE", source_id, company, process_date, max_id_processed, interface_batch_id)
		values
			(:i_sort_order, :i_type_array[i], :i_source_id, :i_co_array[i], :g_finished_at, :max_id, :g_batch_id);
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_batch_derivation_results: " + sqlca.SQLErrtext)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
		//  This seems like the proper place to flush records from the "on demand" queue
		//  if they exist.
		delete from cr_batch_derivation_queue
		 where "TYPE" = :i_type_array[i] and source_id = :i_source_id and company = :i_co_array[i];
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: deleting from cr_batch_derivation_queue: " + sqlca.SQLErrtext)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	next
	
	
	
	//*****************************************************************************************
	//
	//  COMMIT HERE ... don't want to kill the interface without loading if the only
	//	 thing that fails is the insert into cr_interface_dates below or the 
	//	 archiving.  Both of which are easy to do by hand and can be left alone as
	//	 longlong as they are done before the next run of the interface.
	//
	//  As we do in a txn interface ... s/b OK since we've posted and logged the max_id
	//  and such.
	//
	//*****************************************************************************************
	
	i_posted_source_ids[upperbound(i_posted_source_ids) + 1] = i_source_id
	
	commit;
	
	
	
	//*****************************************************************************************
	//
	//  INSERT INTO cr_interface_dates ...
	//
	//    Note: if there are multiple types and companies for a source, it will already
	//    be in the arrays.
	//
	//*****************************************************************************************
	f_pp_msgs("Capturing cr_interface_dates statistics")
	
	//  If no records posted yet, then no point in firing this.
	if isnull(g_batch_id) then goto after_interface_dates
	
	for c = 1 to ds_co.RowCount()
		
		co_val = ds_co.GetItemString(c, 1)
		
		f_pp_msgs("--" + i_company_field + " = " + co_val)
		
		g_batch_id = co_ifb_array[c]
		
		sqls = &
			"insert into cr_interface_dates " + &
				"(company, interface_id, month_number, month_period, process_date, " + &
				 "total_dollars, total_records, feeder_system_id, total_debits, total_credits) (" + &
			"select '" + co_val + "'," + string(g_interface_id) + ", month_number, month_period, sysdate, sum(amount), " + &
					 "count(*), interface_batch_id, sum(decode(sign(amount),1,amount,0)), " + &
					 "sum(decode(sign(amount),-1,amount,0)) " + &
			  "from " + i_detail_table + " " + &
			 "where interface_batch_id = '" + g_batch_id + "' " + &
			 "group by month_number, month_period, interface_batch_id)"
		
		if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_interface_dates: " + sqlca.SQLErrtext)
			f_pp_msgs("  ")
			f_pp_msgs("sqls = " + sqls)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	next
	
	after_interface_dates:
	
	
	
	//*****************************************************************************************
	//
	//  JUMP OFF POINT IN THE EVENT OF VALIDATION KICKOUTS:
	//
	//    To prevent posting the transactions and flushing the stg table.
	//
	//*****************************************************************************************
	after_posting:
	
	
	commit;
	
	
	
	//*****************************************************************************************
	//
	//  END OF OUTERMOST LOOP:  Over the list of source_ids to process.
	//
	//*****************************************************************************************
NEXT  //  FOR s = 1 TO i_ds_distinct_source_id.RowCount() ...



DESTROY i_uo_cr_derivation
DESTROY l_uo_cr_validation


// ### 8142: JAK: 2011-11-01:  Consistent return codes
if overall_kickouts then
	return -2
else
	return 1
end if

end function

public function longlong uf_txn ();//*****************************************************************************************
//
//  Object      :  uo_cr_batch_derivation
//  UO Function :  uf_txn
//  Description :  Behaves like a transaction interface, booking the batch derivation
//                 results to the appropriate detail table(s).
//
//	 Returns     :  1 if successful
// 					-1 on error
//						 2 if no targets generated (nothing to do)
//
//*****************************************************************************************
longlong counter, rtn, lvalidate_rtn, validation_counter, i, ix, j, &
		 update_derivations, num_types, t, update_cr_derivation_rollup, &
		 update_rollup_on_detail_table, no_source_transactions, orig_max_id, &
		 id_balancing,override2_derivations, intercompany_accounting, num_months
string sqls, basis, offset_basis, offset_type, gl_jcat, mn_string, cv_validations, &
		 s_date, filter_string, the_type, update_basis, delimiter, join_fields[], &
		 basis_2nd_arg, basis_6th_arg, rc, ce_val, id_balancing_basis, split_quantities, null_str_array[],&
		 analyze_option, basis_no_company, id_balancing_basis_no_comp
boolean update_orig, validation_kickouts
decimal {2} total_orig, total_targ, total_offs


//*****************************************************************************************
//
//  Need the original max(id) so I don't overwrite the id field on those transactions
//  if we have a loop of more than 1 derivation type.  Otherwise, on types 2 to n, the
//  cr_derivation_id will not explain the id it came from.
//
//*****************************************************************************************
orig_max_id = 0
sqls = "select max(id) from " + i_stg_table // No need for company in this where clause
i_ds_max_id.SetSQLSelect(sqls)
i_ds_max_id.RETRIEVE()
if i_ds_max_id.RowCount() > 0 then
	orig_max_id = i_ds_max_id.GetItemNumber(1, 1)
end if
if isnull(orig_max_id) then orig_max_id = 0

if g_debug = "YES" then f_pp_msgs("***DEBUG*** orig_max_id = " + string(orig_max_id))


//*****************************************************************************************
//
//  Get the interface_id from cr_interfaces ...
//    DO NOT perform an upper on the select value ... 
//      This must be before the check for invalid ids !
//
//*****************************************************************************************

//
//  CLEAR OUT ANY INTERCOMPANY RECORDS THAT ARE LEFT OVER FROM PRIOR VALIDATION KICKOUTS.
//
f_pp_msgs("Deleting any prior intercompany transactions")

sqls = "delete from " + i_stg_table + " where drilldown_key = 'INTERCO' and interface_batch_id <> '0'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting any prior intercompany transactions in " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//*****************************************************************************************
//
//  GET CONTROL VALUES FROM CR_BATCH_DERIVATION_CONTROL:
//		### 9360: JAK: 2012-06-27:  Added analyze option
//
//*****************************************************************************************
setnull(basis)
setnull(offset_basis)
setnull(offset_type)
setnull(gl_jcat)
setnull(filter_string)
setnull(update_derivations)
setnull(update_cr_derivation_rollup)
setnull(update_rollup_on_detail_table)
setnull(no_source_transactions)
setnull(id_balancing)
setnull(split_quantities)
setnull(analyze_option)
setnull(override2_derivations)
setnull(intercompany_accounting)
select trim(basis), trim(offset_basis), trim(offset_type), trim(gl_journal_category),
		 trim(filter_string), update_derivations, update_cr_derivation_rollup, 
		 update_rollup_on_detail_table, no_source_transactions, id_balancing,
		 split_quantities, analyze_option, override2_derivations, intercompany_accounting
  into :basis, :offset_basis, :offset_type, :gl_jcat,
  		 :filter_string, :update_derivations, :update_cr_derivation_rollup,
		 :update_rollup_on_detail_table, :no_source_transactions, :id_balancing,
		 :split_quantities, :analyze_option, :override2_derivations, :intercompany_accounting
  from cr_batch_derivation_control
 where "TYPE" = :i_type and source_id = :i_source_id;
if isnull(basis) or basis = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.basis is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if
if isnull(offset_basis) or offset_basis = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.offset_basis is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if
if isnull(offset_type) or offset_type = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.offset_type is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if
if isnull(gl_jcat) or gl_jcat = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.gl_jcat is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


if isnull(filter_string) then
	filter_string = ""  //  This field is optional.
else
	//### Maint 8966 - JCC - 11-07-2011: added the else to avoid invalid criteria clause if filter string is empty
	if mid(trim(lower(filter_string)),1,5) = 'where' then filter_string = mid(trim(filter_string),6)
	if mid(trim(lower(filter_string)),1,3) <> 'and' then filter_string = 'and ' + filter_string
end if

if isnull(update_derivations) then					update_derivations = 0
if isnull(update_cr_derivation_rollup) then		update_cr_derivation_rollup = 0
if isnull(update_rollup_on_detail_table) then		update_rollup_on_detail_table = 0
if isnull(no_source_transactions) then				no_source_transactions = 0
if isnull(id_balancing) then							id_balancing = 0
if isnull(split_quantities) then						split_quantities = "None"
if isnull(analyze_option) then 						analyze_option = "NONE"
if isnull(override2_derivations) then				override2_derivations = 0


if g_debug = "YES" then f_pp_msgs("***DEBUG*** basis = " + basis)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** offset_basis = " + offset_basis)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** offset_type = " + offset_type)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** gl_jcat = " + gl_jcat)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** update_derivations = " + string(update_derivations))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** update_cr_derivation_rollup = " + string(update_cr_derivation_rollup))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** update_rollup_on_detail_table = " + string(update_rollup_on_detail_table))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** no_source_transactions = " + string(no_source_transactions))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** id_balancing = " + string(id_balancing))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** split_quantities = " + string(split_quantities))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** analyze_option = " + string(analyze_option))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** override2_derivations = " + string(override2_derivations))




//*****************************************************************************************
//
//  PERFORM THE INSERT FROM THE DETAIL TABLE TO THE STAGING TABLE.
//
//  These are the "ORIGINAL" records that will be used as the basis for the derivations.
//  
//  Notes:
//  ------
//    1)  The id can be inserted as-is since these records will be deleted later.
//    2)  Month number will be updated later on the results to the current period.
//    3)  IFB Id will be updated later, after the derivations have run, and we know if
//        there are any derivation results.  We don't want to burn nextvals for
//        no good reason.
//
//*****************************************************************************************

if no_source_transactions = 0 then
	//  Insert the records that need to be processed.
	f_pp_msgs("Inserting from " + i_detail_table + " to " + i_stg_table)
	
	sqls = &
		"insert into " + i_stg_table + " " + &
		"select * from " + i_detail_table + &
		" where month_number in (" + i_basis_mn + ") " + &
		  " and cr_derivation_status = 'I' " + &
		  " and " + i_company_field + " = '" + i_co + "'"
	
	if filter_string <> "" then
		f_pp_msgs("*** filter_string = " + filter_string + " ***")
	end if
	
	sqls = sqls + " " + filter_string
	
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: inserting into " + i_stg_table + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	//  DETERMINE IF THERE IS ANYTHING TO DO.
	//		### 10595: JAK: 2012-06-21:  Should only do this check if we are loading source transactions.
	// 		No need to do this check twice.  Just do the insert and count the records then
	if SQLCA.SQLNRows = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("There are no eligible transactions.  Nothing to do.")
		return 2
	else
		f_pp_msgs(string(SQLCA.SQLNRows) + " records are eligible to be processed")
	end if
	
	//  Initialize the "derivation" fields ... note that these fields are "ORIGINAL" and
	//  the rate and id need to be NULLed out in the event that these transactions were
	//  the result of prior derivations.
	f_pp_msgs("Updating " + i_stg_table + ".cr_txn_type")
	sqls = &
		"update " + i_stg_table + &
		  " set cr_txn_type = 'ORIGINAL', cr_derivation_rate = NULL, cr_derivation_id = NULL " + &
		 "where " + i_company_field + " = '" + i_co + "' and cr_derivation_status = 'I'" // The "I" is
				// super-critical in this where clause !!!  Otherwise, we'll be clobbering any existing
				// target and offset records already in the crb table !!!  This could occur if 2 or more
				// derivation types are being run against this source !!!  The cr_derivation_status on
				// results is set to 'P' below so they are avoided by this update !!!
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_txn_type: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	//	### 9360: JAK: 2012-06-27:  Added analyze option
	if pos(upper(trim(analyze_option)),'ORIG') > 0 then
		f_pp_msgs("Analyzing " + i_stg_table)
		sqlca.analyze_table(i_stg_table)
	end if
end if //	no_source_transactions = 0


//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:
//    Right after we've inserted into the "crb" staging table.  You might put code here
//    like that in the SAP transaction interfaces where we:  perform an initial ferc
//    account update, audit retire_je_code values against CDC, etc.
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom("right after insert to crb_stg", 0, " ", i_stg_table)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if



//*****************************************************************************************
//
//  Run the Insert Derivations:
//
//    "Basis" is the "where clause" for the derivations.  For example, it might be:
//    "cr_allocations_stg.work_order = cr_deriver_control.string and target_credit = 'TARGET'"
//    for the target side of the derivation and:
//    "cr_allocations_stg.work_order = cr_deriver_control.string and cr_txn_type = 'TARGET'"
//    for the offset side.
//
//    Note: if you want to restrict the amount types, you have to include something in
//    the basis where clause.  E.G. " and cr.amount_type = 1 " would restrict to actuals.
//
//    CR TXN TYPE --- for speed, we're assuming that field is on all the transaction
//    tables.  If that ends up bad in the future, we'll add a system switch and evaluate
//    it before we get to this code.
//
//    CR DERIVATION ROLLUP --- If you plan on setting the update_cr_derivation_rollup flag
//    to Yes (and plan on having it in the CDC.STRING) then the field MUST BE on all the
//    transaction tables ... or else you will get a rude surprise in the logs.
//
//*****************************************************************************************
f_pp_msgs("Starting the derivations")


//  CUSTOM FUNCTION CALL:
//    Just before the derivations fire.  This is almost a duplicate of the one above,
//    but it plays into the logic that was built into interfaces at Pepco (where some
//    of their derivation audits were by retire_je_code).  This call would be a good
//    place to add audits like "Single Record For Default Type", or to call
//    uf_deriver_create_offset.
rc = f_cr_batch_derivation_custom("just before derivations fire", 0, " ", i_stg_table)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if


//  -----------------------------------  TARGET SECTION:  -----------------------------------

//  Update the cr_derivation_rollup field (generally a rollup of cost elements
//  kept in the cdc.string.  If a client is using this process and not updating
//  the cr_derivation_rollup field in any transaction interfaces, it may be appropriate
//  to update the detail table too.  This would keep that field consistent on all
//  records, and aid in reconciling queries.
if update_cr_derivation_rollup = 1 then
	
	if update_rollup_on_detail_table = 1 then
		// ### 7924:  JAK: 2011-06-27:  Values by company logic.
		f_pp_msgs("Updating cr_derivation_rollup on detail table")
		sqls = "update " + i_detail_table + " a set cr_derivation_rollup = (" + &
			"select cr_derivation_rollup from " + i_ce_me_table + " b " + &
			 'where a."' + i_ce_field + '" = b."' + i_ce_me_table_field + '"'
			 
		if i_ce_me_by_company = 1 then sqls += ' and a."' + i_company_field + '" = b."' + i_company_field + '"'
		
		sqls += " and b.element_type= 'Actuals') " + &
		"where month_number in (" + i_basis_mn + ") " + &
	    " and cr_derivation_status = 'I' " + &
	    " and " + i_company_field + " = '" + i_co + "'"
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_rollup: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		//  It's not obvious that we should hang up the process with a NULL check at this point.  If the
		//  "cost elements" with a NULL are important, they should kick on the check below against the
		//  staging table.
	end if
	
	// ### 7924:  JAK: 2011-06-27:  Values by company logic.
	f_pp_msgs("Updating cr_derivation_rollup on staging table")
	sqls = "update " + i_stg_table + " a set cr_derivation_rollup = (" + &
		"select cr_derivation_rollup from " + i_ce_me_table + " b " + &
		 'where a."' + i_ce_field + '" = b."' + i_ce_me_table_field + '"'
		
	if i_ce_me_by_company = 1 then sqls += ' and a."' + i_company_field + '" = b."' + i_company_field + '"'
	
	sqls += " and b.element_type= 'Actuals') " + &
	"where " + i_company_field + " = '" + i_co + "'"
	execute immediate :sqls;
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_derivation_rollup: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	sqls = 'select distinct "' + i_ce_field + '" from ' + i_stg_table + ' ' + &
	"where cr_derivation_rollup is null and " + i_company_field + " = '" + i_co + "'"
	i_ds_null_rollup.SetSQLSelect(sqls)
	i_ds_null_rollup.RETRIEVE()
	if i_ds_null_rollup.RowCount() > 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: NULL cr_derivation_rollup values detected for the following " + i_ce_field + " values:")
		f_pp_msgs("---------------------------------------------------------------------------------------------------")
		for i = 1 to i_ds_null_rollup.RowCount()
			ce_val = i_ds_null_rollup.GetItemString(i, 1)
			f_pp_msgs(ce_val)
		next
		f_pp_msgs("  ")
		f_pp_msgs("The process cannot continue.")
		rollback;
		return -1
	end if
	
end if

//  The basis will almost certainly have "cr." references for the detail table.  Change that here
//  to the staging_table.
basis = f_replace_string(basis, "cr.", i_stg_table + ".", "all")

//  Need to include the company in the basis string since uf_txn() is being called in a company
//  loop.  Otherwise we run the risk of perfoming derivations over and over again on companies
//  that come earlier in the loop.
basis_no_company = basis
basis = basis + " and " + i_stg_table + "." + i_company_field + " = '" + i_co + "'"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** basis = " + basis)

// Use the defaults logic now rather than backfilling after the fact.
i_uo_cr_derivation.uf_deriver_defaults_reset()
i_uo_cr_derivation.uf_deriver_defaults_add('cr_txn_type','TARGET',1)
i_uo_cr_derivation.uf_deriver_defaults_add('cr_derivation_status','P',1)
i_uo_cr_derivation.uf_deriver_defaults_add('interface_batch_id','-1',1)
i_uo_cr_derivation.uf_deriver_defaults_add('cr_derivation_type',i_type,1)
i_uo_cr_derivation.uf_deriver_defaults_add('gl_journal_category',gl_jcat,1)
i_uo_cr_derivation.uf_deriver_defaults_add('month_period','0',1)
if i_posting_mn = '*' then
	i_uo_cr_derivation.uf_deriver_defaults_add('month_number','cr.month_number',0)
else
	i_uo_cr_derivation.uf_deriver_defaults_add('month_number',i_posting_mn,1)
end if

//  Call the indicated derivation type.
f_pp_msgs("Inserting derivation target results")

rtn = i_uo_cr_derivation.uf_deriver(i_type, i_stg_table, basis)

if rtn <> 1 then
	//  Assume that messages were logged by uf_deriver().
	rollback;
	return -1
else
	f_pp_msgs(string(sqlca.SQLNRows) + " rows inserted.")
end if

// ### 9992: JAK: 2012-06-21:  No need to do a datastore count here.  The update above would have hit the same
//	records so we can just check the sqlca.sqlnrows.
//  If there are no target records, log a message and return to the caller which will
//  loop through to the next value.

if SQLCA.SQLNRows = 0 then
	f_pp_msgs("  ")
	f_pp_msgs("No TARGET records generated.  Nothing to do.")
	
	//  This has to be done here, since we won't get to the code below.
	f_pp_msgs("Deleting ORIGINAL transactions")
	sqls = "delete from " + i_stg_table + " where cr_txn_type = 'ORIGINAL'"
	execute immediate :sqls;
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Deleting ORIGINAL transactions from " + i_stg_table + ": " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	return 2
else
	//	### 9360: JAK: 2012-06-27:  Added analyze option
	if pos(upper(trim(analyze_option)),'TARGET') > 0 then
		f_pp_msgs("Analyzing " + i_stg_table)
		sqlca.analyze_table(i_stg_table)
	end if
end if


//  -----------------------------------  OFFSET SECTION:  -----------------------------------


f_pp_msgs("Inserting derivation offset results")

//  The basis will almost certainly have "cr." references for the detail table.  Change that here
//  to the staging_table.
offset_basis = f_replace_string(offset_basis, "cr.", i_stg_table + ".", "all")

//  Need to include the company in the basis string since uf_txn() is being called in a company
//  loop.  Otherwise we run the risk of perfoming derivations over and over again on companies
//  that come earlier in the loop.
offset_basis = offset_basis + " and " + i_stg_table + "." + i_company_field + " = '" + i_co + "'"

//  The " id in (select nvl(cr_derivation_id,0) from ... " piece of the
//  argument was added to avoid the following situation.  If a "STRING" in CDC only
//  had a record for the "offset" derivation type, then that "STRING"'s transactions
//  would get and offset record here, but no target record above.  Then, the default
//  target logic below WOULD NOT insert a default target records because it already
//  had an " id not in (select nvl(cr_derivation_id,0) from ..." piece in its argument.
//  I.E. the stranded offset was providing the cr_derivation_id that kept this "STRING"
//  from going through the default logic.  Thus, we ended up OOB.  I want to avoid BOTH
//  targets and credits if the CDC data is not whole for this "STRING" and let it just
//  go through the default logic below (since I can't prevent someone from manually
//  entering bad data in CDC).

// ### 9992: JAK: 2012-06-21:  Change to exist and add an ID restriction for speed.
offset_basis = offset_basis + " and exists " + &
	" (select 1 from " + i_stg_table + " b " + &
		"where b.cr_txn_type = 'TARGET' and "
		
// ### 9992: JAK: 2012-06-21:  Check for this field once
if i_derivation_type_on_stg > 0 then offset_basis += " b.cr_derivation_type = '" + i_type + "' and "

offset_basis += " b.id > " + string(orig_max_id) + " and " + &
		i_stg_table + ".id = b.cr_derivation_id)"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** offset_basis = " + offset_basis)

// Use the defaults logic now rather than backfilling after the fact.
i_uo_cr_derivation.uf_deriver_defaults_reset()
i_uo_cr_derivation.uf_deriver_defaults_add('cr_txn_type','OFFSET',1)
i_uo_cr_derivation.uf_deriver_defaults_add('cr_derivation_status','P',1)
i_uo_cr_derivation.uf_deriver_defaults_add('interface_batch_id','-1',1)
i_uo_cr_derivation.uf_deriver_defaults_add('cr_derivation_type',i_type,1)
i_uo_cr_derivation.uf_deriver_defaults_add('gl_journal_category',gl_jcat,1)
i_uo_cr_derivation.uf_deriver_defaults_add('month_period','0',1)
if i_posting_mn = '*' then
	i_uo_cr_derivation.uf_deriver_defaults_add('month_number','cr.month_number',0)
else
	i_uo_cr_derivation.uf_deriver_defaults_add('month_number',i_posting_mn,1)
end if
if not (split_quantities = "CR Derivation Rollup" or split_quantities = "Split All") then i_uo_cr_derivation.uf_deriver_defaults_add('quantity','-1 * cr.quantity',0) 

//  Call the indicated offset derivation type.
rtn = i_uo_cr_derivation.uf_deriver(offset_type, i_stg_table, offset_basis)

if rtn <> 1 then
	//  Assume that messages were logged by uf_deriver().
	rollback;
	return -1
else
	f_pp_msgs(string(sqlca.SQLNRows) + " rows inserted.")
end if

//	### 9360: JAK: 2012-06-27:  Added analyze option
if pos(upper(trim(analyze_option)),'OFFSET') > 0 then
	f_pp_msgs("Analyzing " + i_stg_table)
	sqlca.analyze_table(i_stg_table)
end if

//  ----------------------------------  ROUNDING SECTION:  ----------------------------------

//  CUSTOM FUNCTION CALL:
//    Just after the derivations fire.  This call would be a good place to add logic
//    for "default" derivation types.  Base code for the defaults is not obvious since
//    some clients (like Pepco) had multiple default types (by retire_je_code).
rc = f_cr_batch_derivation_custom("just after derivations fire", 0, " ", i_stg_table)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if


//  CLEANUP:
//  Fix any rounding error.  Rounding must be called for each of the a_type/a_join
//  values passed to uf_deriver.
//
//  And example of the basis variable at this point would be:
//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup = cr_deriver_control.string
//  and crb_accounts_payable_stg.cr_txn_type = 'ORIGINAL' 
//  and crb_accounts_payable_stg.amount_type = 1 
//  and crb_accounts_payable_stg.company = 'CECO'
//
//  NOTE: The last arg (a_table_name_field) is assuming that the "join" field in the stg table
//  is the first part of the basis from cr_batch_derivation_control (so we can strip off
//  everything to the left of the "=" sign.  For example:
//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup
//
//  NOTE: balancing uses either the traditional technique or the id_balancing technique.
//  See comments in the UO function.
//

f_pp_msgs("Performing rounding adjustment")

basis_2nd_arg = f_replace_string(basis, "cr_txn_type = 'ORIGINAL'", "cr_txn_type = 'TARGET'", "all")

basis_6th_arg = left(basis, pos(basis, "=") - 1)

// ### 9992: JAK: 2012-06-21:  Check for this field once
id_balancing_basis_no_comp = "cr_txn_type = 'TARGET' and id > " + string(orig_max_id)
id_balancing_basis = "cr_txn_type = 'TARGET' and " + i_company_field + " = '" + i_co + "' and id > " + string(orig_max_id)
if i_derivation_type_on_stg > 0 then 
	id_balancing_basis_no_comp += " and cr_derivation_type = '" + i_type + "' "
	id_balancing_basis += " and cr_derivation_type = '" + i_type + "' "
end if


// We already assume the following columns are on the table:
//		CR Derivation Status, CR Txn Type, CR Derivation ID, CR Derivation Rate, CR Derivation Type
if id_balancing = 1 then
	// ### 9992: JAK: 2012-06-21:  Check for this field once
	
	offset_basis = " and exists " + &
		" (select 1 from " + i_stg_table + " b " + &
		"where b.cr_txn_type = 'TARGET' and "
		
	if i_derivation_type_on_stg > 0 then offset_basis += " b.cr_derivation_type = '" + i_type + "' and "

	offset_basis += "b.id > " + string(orig_max_id) + " and " + &
		i_stg_table + ".id = b.cr_derivation_id)"
		
	rtn = i_uo_cr_derivation.uf_deriver_rounding_error_ids( &
		i_type, i_stg_table, &
		basis_no_company + offset_basis, &
		id_balancing_basis_no_comp, &
		id_balancing_basis, &
		basis_6th_arg)
else
	//### Maint 8966 - JCC - 11-07-2011: need to decode the posting mn in case a * is used
	rtn = i_uo_cr_derivation.uf_deriver_rounding_error( &
		i_type, i_stg_table, &
		basis, &
		basis_2nd_arg + " and " + i_stg_table + ".month_number = decode('" + i_posting_mn + "','*',month_number,'" + i_posting_mn + "')", &
		i_stg_table + ".cr_txn_type = 'TARGET' and " + i_stg_table + ".month_number = decode('" + i_posting_mn + "','*',month_number,'" + i_posting_mn + "')" , &
		basis_6th_arg)	
end if

if rtn <> 1 then
	//  No msgs ... uf_deriver_rounding_error handles its own.
	rollback;
	return -1
end if


//*****************************************************************************************
//
//	 COMMIT.
//
//    This is really the first place we could have a problem.  So I'm firing a commit
//    here to save the records in case we need to audit something.  They are about to
//    get committed anyways before validations run.  The beginning of the source loop
//    in uf_read truncates this table, so there should be no danger.
//
//*****************************************************************************************
commit;



//*******************************************************************************************
//
//  BALANCE:  Verify that the targets and offsets = the original amount.
//
//*******************************************************************************************
// ### - SEB - 8791 - 100611: Fixing spelling error
//f_pp_msgs("Balancing TARGET and OFFEST results against ORIGINAL transactions")
f_pp_msgs("Balancing TARGET and OFFSET results against ORIGINAL transactions")

// ### - SEB - 8791 - 100611: Need to include cr_derivation_type in the where clause
// JAK: removed the cr_txn_type = 'ORIGINAL' criteria for the case of derivation1 being the source for derivation2.
//		In that case the "ORIGINAL" transaction have cr_txn_type = 'TARGET' or 'OFFSET'.  Can only key off of the fact that 
//		the original records have an id link to the new records.
sqls = &
	"select sum(amount) from " + i_stg_table + " a " + &
	" where exists (select 1 from " + i_stg_table + " b where b.id > " + string(orig_max_id) + &
	" and a.id = b.cr_derivation_id "

// ### 9992: JAK: 2012-06-21:  Check for cr_derivation_type
if i_derivation_type_on_stg > 0 then sqls += " and cr_derivation_type = '" + i_type + "'"
sqls += ")"

i_ds_sum_amount.SetSQLSelect(sqls)
i_ds_sum_amount.RETRIEVE()

if i_ds_sum_amount.RowCount() <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: i_ds_sum_amount.RETRIEVE() returned no records for ORIGINAL !")
	f_pp_msgs("*** sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	total_orig = i_ds_sum_amount.GetItemDecimal(1, 1)
end if

sqls = &
	"select sum(amount) from " + i_stg_table + " a " + &
	" where cr_txn_type = 'TARGET' " + &
	" and id > " + string(orig_max_id)
// ### 9992: JAK: 2012-06-21:  Check for cr_derivation_type
if i_derivation_type_on_stg > 0 then sqls += " and cr_derivation_type = '" + i_type + "'"
	
i_ds_sum_amount.SetSQLSelect(sqls)
i_ds_sum_amount.RETRIEVE()

if i_ds_sum_amount.RowCount() <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: i_ds_sum_amount.RETRIEVE() returned no records for TARGET !")
	f_pp_msgs("*** sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	total_targ = i_ds_sum_amount.GetItemDecimal(1, 1)
end if

sqls = &
	"select sum(amount) from " + i_stg_table + " a " + &
	" where cr_txn_type = 'OFFSET' " + &
	" and id > " + string(orig_max_id)
// ### 9992: JAK: 2012-06-21:  Check for cr_derivation_type
if i_derivation_type_on_stg > 0 then sqls += " and cr_derivation_type = '" + i_type + "'"

i_ds_sum_amount.SetSQLSelect(sqls)
i_ds_sum_amount.RETRIEVE()

if i_ds_sum_amount.RowCount() <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: i_ds_sum_amount.RETRIEVE() returned no records for OFFSET !")
	f_pp_msgs("*** sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	total_offs = -i_ds_sum_amount.GetItemDecimal(1, 1)
end if

if isnull(total_offs) then total_offs = 0
if isnull(total_targ) then total_targ = 0
if isnull(total_orig) then total_orig = 0

if total_orig <> total_targ or total_orig <> total_offs then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: The TARGET / OFFSET results do not equal the ORIGINAL transactions !")
	f_pp_msgs("  ")
	f_pp_msgs("TARGETS:  " + string(total_targ))
	f_pp_msgs("ORIGINAL: " + string(total_orig))
	f_pp_msgs("OFFSETS:  " + string(total_offs))
	f_pp_msgs("  ")
	commit;
	return -1
end if


//
//  "UPDATE" DERIVATIONS WOULD GO HERE.  E.G. WHAT IF THEY "SPLIT" SOME THINGS WITH THE
//  INSERT DERIVATIONS THAT POSTED TO OTHER DERIVATION KEYS?  WE WOULD NEED TO LOOP OVER
//  CR_DERIVER_TYPE_SOURCES FOR THIS SOURCE_ID AND FIRE THE UPDATE DERIVATIONS.  I SUSPECT
//  I WILL NEED A FLAG ON CR_BATCH_DERIVATION_CONTROL TO TURN THIS ON OR OFF, IN CASE
//  DIFFERENT CLIENTS DO/DON'T WANT TO FIRE THESE.
//

// ### 9992: JAK: 2012-06-21:  Interface Batch ID set above for target and credit.  Need to set the original records
//	for the processes below.
sqls = "update " + i_stg_table + " a set interface_batch_id = '-1' " + &
	" where exists (select 1 from " + i_stg_table + " b where a.id = b.cr_derivation_id and b.interface_batch_id = '-1')"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ".interface_batch_id to -1: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//*******************************************************************************************
//
//  Split Quantities:  
//
//    While we're here with a filled in ifb_id, might as well split the quantities.
//
//*******************************************************************************************
if split_quantities = "CR Derivation Rollup" then
	f_pp_msgs("Splitting quantities")
	
	// ### 7838: JAK: 2011-10-26:  Split quantites WC
	i_uo_cr_derivation.i_split_quantities_wc = " and id > " + string(orig_max_id) + " "
	
	rtn = i_uo_cr_derivation.uf_split_quantities(i_stg_table, "-1")
	
	if rtn <> 1 then
		//  Assume that messages were logged by uf_deriver().
		rollback;
		return -1
	else
		f_pp_msgs(string(sqlca.SQLNRows) + " rows inserted.")
	end if
	
end if

if split_quantities = "Split All" then
	f_pp_msgs("Splitting quantities")
	
	i_uo_cr_derivation.i_split_quantities_all_wc = " and id > " + string(orig_max_id) + " "
	
	rtn = i_uo_cr_derivation.uf_split_quantities_all(i_stg_table, "-1")
	
	if rtn <> 1 then
		//  Assume that messages were logged by uf_deriver().
		rollback;
		return -1
	else
		f_pp_msgs(string(sqlca.SQLNRows) + " rows updated.")
	end if
	
end if


//*******************************************************************************************
//
//  GET RID OF THE ORIGINAL RECORDS:
//
//    Very important here since we may end up processing another derivation type in
//    the source_id loop and it will end up inserting its own 'ORIGINAL' records.
//
//*******************************************************************************************
f_pp_msgs("Deleting ORIGINAL transactions")

sqls = "delete from " + i_stg_table + " where cr_txn_type = 'ORIGINAL'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting ORIGINAL transactions from " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if



//*******************************************************************************************
//
//  Other Updates:
//
//    Like gl_journal_category
//
//*******************************************************************************************
// ### 9992: JAK: 2012-06-21: ID already set from the nextval during the derivation process.  
//		Resetting it here would burn IDs unnecessarily.
//f_pp_msgs("Performing other updates (id)")
//
//sqls = "update " + i_stg_table + " set id = crdetail.nextval where id > " + string(orig_max_id)
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".id: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if

// ### 9992: JAK: 2012-06-21: Merging gl_journal_category and dr_cr_id into one statement.
f_pp_msgs("Performing other updates")
sqls = "update " + i_stg_table + " set dr_cr_id = decode(sign(amount), -1, -1, 1) where id > " + string(orig_max_id)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating dr_cr_id: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//*****************************************************************************************
//
//  UPDATE DERIVATIONS.
//
//    -4 is the source_id in cr_deriver_type_sources.  We did a dedicate source_id
//    instead of the source_id values for each detail table because of the conflict
//    between interfaces and ADJ JE's.  They both hit the same detail tables, but may
//    need to be configured differently.  However, it is unlikely that you'd want them
//    to be configured differently for this process.  Thus, this process gets its own
//    source_id and its own derivation type config.
//   
//*****************************************************************************************
if update_derivations = 1 then
	f_pp_msgs("Performing update derivations: Analyzing table " + i_stg_table)
	sqlca.analyze_table(i_stg_table)  //  s/b/ fine since we've already committed anyways
	
	
	f_pp_msgs("Performing update derivations")
	
	sqls = &
		"select * from cr_deriver_type_sources " + &
		 "where source_id = -4 and upper(trim(insert_or_update))  ='UPDATE'"
	i_cr_deriver_type_sources.SetSQLSelect(sqls)
	i_cr_deriver_type_sources.RETRIEVE()
	i_cr_deriver_type_sources.SetSort("run_order a")
	i_cr_deriver_type_sources.Sort()
	num_types = i_cr_deriver_type_sources.RowCount()
	
	if num_types <= 0 then
		if g_debug = "YES" then f_pp_msgs("DERIVATIONS: No derivations defined for: " + i_stg_table)
	end if
	
	for t = 1 to num_types
		
		the_type            = trim(i_cr_deriver_type_sources.GetItemString(t, "type"))
		update_basis        = trim(i_cr_deriver_type_sources.GetItemString(t, "basis"))
		delimiter           = i_cr_deriver_type_sources.GetItemString(t, "delimiter")
		
		if isnull(the_type)  or the_type = "" then continue // useless record
		if isnull(basis)     or basis    = "" then continue // useless record
		if isnull(delimiter)                  then delimiter    = ""
		
		f_pp_msgs("Performing update derivations: " + the_type)
		
		if g_debug = "YES" then
			f_pp_msgs("type = " + the_type)
			f_pp_msgs("basis = " + basis)
			f_pp_msgs("delimiter = " + delimiter)
		end if
		
		//  Parse the basis value (e.g. "company,work_order") into an array for the update function.
		join_fields = null_str_array[]
		f_parsestringintostringarray(update_basis, ",", join_fields)
		
		rtn = i_uo_cr_derivation.uf_deriver_update_mult( &
			the_type, &
			i_stg_table, &
			join_fields, &
			"-1", &
			false, &
			delimiter)
		
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: in uf_deriver_update_mult(): see message above")
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	next
end if


//*******************************************************************************************
//
//  Derivation Overrides:  #2 table
//
//    Re-using variables in this section from above.
//
// ### BSB: Maint 10143
//
//*******************************************************************************************
if override2_derivations = 1 then 
	f_pp_msgs("Calling override2 derivations")
	rtn = i_uo_cr_derivation.uf_deriver_override()
	
	if rtn < 1 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: in uf_deriver_override(): see message above")
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if
// ### BSB: END MAINT 10143

//*****************************************************************************************
//
//	 PERFORM INTERCOMPANY BALANCING:  Before the validations.  Must loop over the months
//                                   since interco balancing is by month.
//
//  Note:  the month_period is unique for each month_number.
//
//*****************************************************************************************
intercompany_accounting = 0
string cross_company_column
select upper(trim(control_value)) into :cross_company_column from cr_system_control
 where upper(trim(control_name)) = 'CROSS CHARGE COMPANY COLUMN NAME';

if not isNull(cross_company_column) and cross_company_column <> '' then
	//check that we actually have the field
	select count(1) into :counter
	from ALL_TAB_COLUMNS
	where table_name = 'CR_DERIVER_CONTROL'
	and column_name = :cross_company_column;
	
	if isNull(counter) then counter = 0
	
	if counter > 0 then
		//see if the companies are out of balance
		counter = uf_checkOOB()
		if counter > 0 then intercompany_accounting = 1
	end if
end if

if intercompany_accounting = 1 then
	f_pp_msgs("Performing intercompany balancing")
	
	sqls = "select distinct month_number, month_period from " + i_stg_table
	if isvalid(i_ds_months) then
		i_ds_months.SetSQLSelect(sqls)
		i_ds_months.Retrieve()
	else
		i_ds_months = create uo_ds_top
		f_create_dynamic_ds(i_ds_months, "grid", sqls, sqlca, true)
	end if
	
	num_months = i_ds_months.RowCount()
	
	for i = 1 to num_months
		f_pp_msgs("-- Month " + string(i) + " of " + string(num_months))
		
		rtn = i_uo_cr_interco_balancing.uf_interco_balancing_x_charge(upper(i_stg_table), "interface_batch_id", "-1", &
			i_ds_months.GetItemNumber(i, 1), i_ds_months.GetItemNumber(i, 2))
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: in uf_interco_balancing: rtn = " + string(rtn))
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
	next
end if


//*******************************************************************************************
//
//  Derivations need a real interface_batch_id.  Change it back so nothing else
//  has a problem.
//
//*******************************************************************************************
// ### 9992: JAK: 2012-06-21: Mark interface_batch_id to 0 here so it can be reused in the next step rather than having to re-update 
//	it there.
sqls = "update " + i_stg_table + " set interface_batch_id = '0' where interface_batch_id = '-1' "

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ".interface_batch_id to NULL: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

if g_debug = "YES" then
	f_pp_msgs("Update derivations complete")
end if


//*****************************************************************************************
//
//	 ENSURE THAT THE BATCH BALANCES BY COMPANY.
//
//    This is really the first place we could have a problem.  So I'm firing a commit
//    here to save the records in case we need to audit something.  They are about to
//    get committed anyways before validations run.  The beginning of the source loop
//    in uf_read truncates this table, so there should be no danger.
//
//*****************************************************************************************
counter = 0
counter = uf_checkOOB()

if counter > 0 then
	//  OOB !
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: " + i_stg_table + " is out of balance by: " + i_company_field + &
		", gl_journal_category, amount_type, and month_number !")
	f_pp_msgs("  ")
	commit; // save the records for review
	return -1
end if

return 1

end function

public function longlong uf_checkoob ();string sqls
longlong counter

counter = 0
sqls = &
	"select count(*) from (" + &
		"select " + i_company_field + ", gl_journal_category, amount_type, month_number, sum(amount) " + &
		  "from " + i_stg_table + " " + &
		 "group by " + i_company_field + ", gl_journal_category, amount_type, month_number having sum(amount) <> 0)"
i_ds_counter.SetSQLSelect(sqls)
i_ds_counter.RETRIEVE()
if i_ds_counter.RowCount() > 0 then
	counter = i_ds_counter.GetItemNumber(1, 1)
end if
if isnull(counter) then counter = 0

return counter
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_batch_derivation_custom_version nvo_cr_batch_derivation_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_batch_derivation_custom.pbd'
		return nvo_cr_batch_derivation_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

