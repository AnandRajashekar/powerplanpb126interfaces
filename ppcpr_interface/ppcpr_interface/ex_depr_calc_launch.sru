HA$PBExportHeader$ex_depr_calc_launch.sru
forward
global type ex_depr_calc_launch from exception
end type
end forward

global type ex_depr_calc_launch from exception
string text = "An exception occurred in the depreciation calculation launching process. "
end type
global ex_depr_calc_launch ex_depr_calc_launch

on ex_depr_calc_launch.create
call super::create
TriggerEvent( this, "constructor" )
end on

on ex_depr_calc_launch.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

