HA$PBExportHeader$nvo_depr_calc_launcher.sru
forward
global type nvo_depr_calc_launcher from nonvisualobject
end type
end forward

global type nvo_depr_calc_launcher from nonvisualobject
end type
global nvo_depr_calc_launcher nvo_depr_calc_launcher

forward prototypes
public subroutine uf_validate_processes () throws ex_depr_calc_launch
public subroutine uf_check_results (longlong a_process_control_group_id, longlong a_process_identifiers[]) throws ex_depr_calc_launch
public subroutine uf_launch_month_end_calcs (longlong a_company_ids[], date a_month, ref longlong a_group_id, ref longlong a_process_identifiers[]) throws ex_depr_calc_launch
public subroutine uf_launch_month_end_calcs (longlong a_company_ids[], date a_month) throws ex_depr_calc_launch
end prototypes

public subroutine uf_validate_processes () throws ex_depr_calc_launch;ex_depr_calc_launch my_exception
my_exception = create ex_depr_calc_launch

longlong process_count

//Validate process for group depreciation 
SELECT count(1)
INTO :process_count
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_depr_calc.exe group';

if sqlca.sqlcode < 0 then 
	my_exception.text = "A SQL Error occurred while validing that a PP_PROCESSES record exists for group depreciation. SQL Error: " + sqlca.sqlerrtext
	throw my_exception
elseif process_count = 0 then 
	my_exception.text = "No PP_PROCESSES record found for 'ssp_depr_calc.exe GROUP'. Please add a record to the PP_PROCESSES table."
	throw my_exception
elseif process_count > 1 then 
	my_exception.text = "There were multiple process found for 'ssp_depr_calc.exe GROUP'. The EXECUTABLE_FILE coulmn on PP_PROCESSES should be unique." 
	throw my_exception
end if

//Validate processes for individual depreciation (not including lease)
string sqls, str_rtn
sqls = "SELECT subledger_type_id FROM subledger_control WHERE depreciation_indicator = 3 AND subledger_type_id <> -100"
uo_ds_top ds_subledger_types
ds_subledger_types = create uo_ds_top
str_rtn = f_create_dynamic_ds(ds_subledger_types, 'grid', sqls, sqlca, true)

//Possible returns from f_create_dynamic_ds are "OK", "Error", or a dserror 
if str_rtn = 'ERROR' then 
	my_exception.text = "Error while retrieving subledger types."
	throw my_exception
elseif str_rtn <> 'OK' then 
	my_exception.text = "A datastore error occured while retrieving subledger types: " + str_rtn
	throw my_exception
end if 

//loop over subledgers and validate that each one is in PP_PROCESSES
longlong i, rowcount, subledger_type_id
string subledger_exe
rowcount = ds_subledger_types.rowcount()
for i = 1 to rowcount
	subledger_type_id = ds_subledger_types.getitemnumber(i,1)
	subledger_exe = 'ssp_depr_calc.exe ind:' + string(subledger_type_id)
	
	//Validate process for this subledger 
	SELECT count(1)
	INTO :process_count
	FROM pp_processes
	WHERE lower(trim(executable_file)) = :subledger_exe;
	
	if sqlca.sqlcode < 0 then 
		my_exception.text = "A SQL Error occurred while validing that a PP_PROCESSES record exists for subledger_type_id " + string(subledger_type_id) + ". SQL Error: " + sqlca.sqlerrtext
		throw my_exception
	elseif process_count = 0 then 
		my_exception.text = "No PP_PROCESSES record found for 'ssp_depr_calc.exe IND:" + string(subledger_type_id) +  "'. Please add a record to the PP_PROCESSES table."
		throw my_exception
	elseif process_count > 1 then 
		my_exception.text = "There were multiple process found for 'ssp_depr_calc.exe IND:" + string(subledger_type_id) +  "'. The EXECUTABLE_FILE coulmn on PP_PROCESSES should be unique." 
		throw my_exception
	end if
next 
end subroutine

public subroutine uf_check_results (longlong a_process_control_group_id, longlong a_process_identifiers[]) throws ex_depr_calc_launch;ex_depr_calc_launch my_exception
my_exception = create ex_depr_calc_launch

datetime start_time, current_time
decimal{2} time_difference

start_time = datetime(today(), now())

f_pp_msgs("Begin polling DEPR_CALC_PROCESS_CONTROL to make sure all launched processes complete successfully at " + string(start_time) + ".")

//we exit by either throwing an exception or returning, so do while true is OK. 
	
longlong process_count, finished_process_count
do while true 
	f_pp_msgs("	...")
	
	//initialize our variables 
	process_count = 0
	finished_process_count =0 
	
	//first check to see if all the processes in the process control group are finished
	SELECT count(1)
	INTO :process_count
	FROM depr_calc_process_control
	WHERE group_id = :a_process_control_group_id;
	
	if sqlca.sqlcode < 0 then 
		my_exception.text = "A SQL error occurred while counting the number of processes in DEPR_CALC_PROCESS_CONTROL for group_id " + &
			string(a_process_control_group_id) + ". SQL error text: " + sqlca.sqlerrtext
		throw my_exception
	end if 
	
	SELECT count(1)
	INTO :finished_process_count
	FROM depr_calc_process_control
	WHERE group_id = :a_process_control_group_id
	AND is_processed = 1;
	
	if sqlca.sqlcode < 0 then 
		my_exception.text = "A SQL error occurred while counting the number of finished processes in DEPR_CALC_PROCESS_CONTROL for group_id " + &
			string(a_process_control_group_id) + ". SQL error text: " + sqlca.sqlerrtext
		throw my_exception
	end if 
	
	if process_count = finished_process_count then 
		//we are done!
		return
	end if
	
	//if all the processes in the process control group are NOT finished, check to see if they're still running or if they errored out
	longlong i, done_count
	string job_status, process_description
	
	//initialize done_count to zero before we loop over the identifiers. 
	done_count = 0
	
	for i = 1 to upperbound(a_process_identifiers) 
		//possible values for status on PP_JOB_REQUEST are 'N' (new), 'I' (in process), 'D' (done), 'E' (error), and 'L' (new local request).
		//	Error is bad - exit right away
		//	In process and Done are good - keep looping
		// 	N and L are kind of inbetween - they may be like that for awhile, but eventually they should change to something else. 
		current_time = datetime(today(),now())
		SELECT upper(trim(status)), process_descr, round((:current_time - :start_time)*60*24,2)
		INTO :job_status, :process_description, :time_difference
		FROM pp_job_request
		WHERE process_identifier = :a_process_identifiers[i];
	
		if sqlca.sqlcode < 0 then 
			my_exception.text = "A SQL error occurred while retrieving the job status from PP_JOB_REQUEST for process_identifier " + string(a_process_identifiers[i])
			throw my_exception
		elseif job_status = 'D' then 
			//Jobs that are started correctly but exit with a "FAILURE" return code still get set to done, so
			//	we need to keep track of how many "Done" items there are so that we can compare them 
			// 	to the number of processes in DEPR_CALC_PROCESS_CONTROL
			done_count++
			
			if done_count = process_count then 
				//we have as many processes that are done as we do in DEPR_CALC_PROCESS_CONTROL. 
				//	Double check the "finished_process_count" to make sure the last one didn't finish
				//	between the count above (prior to this loop) and here. 
				SELECT count(1)
				INTO :finished_process_count
				FROM depr_calc_process_control
				WHERE group_id = :a_process_control_group_id
				AND is_processed = 1;
				
				if sqlca.sqlcode < 0 then 
					my_exception.text = "A SQL error occurred while counting the number of finished processes in DEPR_CALC_PROCESS_CONTROL for group_id " + &
						string(a_process_control_group_id) + ". SQL error text: " + sqlca.sqlerrtext
					throw my_exception
				end if 
				
				if finished_process_count < done_count then 
					//our done count is greater than the finished_process_count, so one of them must have errorred out. throw exception
					my_exception.text = "One or more of the depreciation calculations completed processing, but did not update its DEPR_CALC_PROCESS_CONTROL record. Please review the logs for each process for details."
					throw my_exception
				end if
			end if
		elseif sqlca.sqlcode = 100 then 
			my_exception.text = "Could not find a PP_JOB_REQUEST record for the process_identifier " + string(a_process_identifiers[i])
			throw my_exception
		end if 
		
		if job_status = 'E' then
			my_exception.text = "An error occurred in process " + process_description + ". Please see the logs for that process for error details." 
			throw my_exception
		elseif (job_status = 'N' or job_status = 'L') and time_difference > 5 then
			//it's been over 5 minutes since we requested the job and it hasn't started yet - throw exception and leave 
			my_exception.text = "The job request for " + process_description + " was submitted over five minutes ago, but has not yet started." 
			throw my_exception
		end if
	next 
	
	//we aren't done yet - sleep for a little while and check again
	sleep(10)
loop
end subroutine

public subroutine uf_launch_month_end_calcs (longlong a_company_ids[], date a_month, ref longlong a_group_id, ref longlong a_process_identifiers[]) throws ex_depr_calc_launch;/************************************************************************************************************************************************************
**
** uf_launch_calcs()
** 
** Handles the logic for inserting into the DEPR_CALC_PROCESS_CONTROL table and launching the server side depr calcs. This object is synced between the application
**		and interface repository so that this logic will only need to be maintained in once place. 
** 
** Parameters : 	longlong 	: (a_company_ids[]) Array of company IDs that we need to launch the depreciation calculation for
**                       date		: (a_month) The month to launch the depreciation calculations for
** 
** Returns 	: (None) - Errors will be returned using exceptions
**
** Throws	: ex_depr_calc_launch
**
************************************************************************************************************************************************************/
longlong num_rows, i, subledger_type_id
string job_description, rtn_str, sqls, subledger_name, process_string
uo_ds_top ds_subledgers
s_ppbase_parm_arrays s_parms
s_ppbase_parm_arrays_labels s_labels

//validate that PP_PROCESSES records exist for all subledger
uf_validate_processes()

//declare and initialize exception in case we need it
ex_depr_calc_launch my_exception
my_exception = create ex_depr_calc_launch

//init the labels
s_labels.long_label = 'Companies'
s_labels.date_label = 'Month'
s_labels.string_label = 'Depreciation Type'
s_labels.long_label3 = 'Process Control Group'

// set the date and companies in s_parms
s_parms.date_arg[1] = a_month
s_parms.long_arg = a_company_ids

//get depr_calc_process_group_id
select DEPR_CALC_PROCESS_CONTROL_SEQ.nextval
into :a_group_id
from dual;

//set group_id in s_parms
s_parms.long_arg3[1] = a_group_id

// stage the group depr into the process
job_description = "Month End Depreciation - Group"
s_parms.string_arg[1] = "GROUP"
insert into DEPR_CALC_PROCESS_CONTROL (GROUP_ID, TYPE, IS_PROCESSED)
values (:a_group_id, :s_parms.string_arg[1], 0);

//Check return value
if sqlca.sqlcode <> 0 then 
	my_exception.text = "An error occurred inserting into DEPR_CALC_PROCESS_CONTROL for group depreciation. SQL Code: " + string(sqlca.sqldbcode) + " Error Text: " + sqlca.sqlerrtext
	throw my_exception
end if 

//get ds of non-lease individually depreciated subledgers so that we can loop over them. 
ds_subledgers = create uo_ds_top
sqls = 'select subledger_type_id, subledger_name from subledger_control where depreciation_indicator = 3 and subledger_type_id > 0'
rtn_str = f_create_dynamic_ds(ds_subledgers, 'grid', sqls, sqlca, true)

if rtn_str <> "OK" then	
	if rtn_str <> "ERROR" then
		my_exception.text = "An error occured while creating the datastore for subledgers. " + &
						"~n~nError: " + rtn_str
		throw my_exception
	else
		my_exception.text = "An error occured while retrieving the rows of the datastore for subledgers. No changes made."
		throw my_exception
	end if
end if

num_rows = ds_subledgers.RowCount()
// stage the subledgers
for i = 1 to num_rows
		
	//set our subledger type parameter from the datastore 
	subledger_type_id = ds_subledgers.getItemNumber(i, "subledger_type_id")

	insert into DEPR_CALC_PROCESS_CONTROL (GROUP_ID, TYPE, IS_PROCESSED)
	values (:a_group_id, 'INDIVIDUAL: ' ||  to_char(:subledger_type_id), 0);
next

commit;

//create nvo_ss
nvo_server_side_request nvo_ss
nvo_ss = create nvo_server_side_request 

// set the depreciation type
s_parms.string_arg[1] = 'GROUP'

// request the group job.
if g_main_application then 
	a_process_identifiers[1] = nvo_ss.uf_request_job ("ssp_depr_calc.exe GROUP", job_description, s_parms, s_labels, false, true)
else
	a_process_identifiers[1] = nvo_ss.uf_request_job ("ssp_depr_calc.exe GROUP", job_description, s_parms, s_labels, false, true, true, false)
end if

if a_process_identifiers[1] < 0 then 
	my_exception.text = "An error occurred while submitting the job request for group depreciation."
	throw my_exception
end if

f_status_box("Information","The job request for " + job_description + " was submitted successfully.")


/*
*
*	loop over individually depreciated subledgers and submit a job request for each one. 
*
*/	
//modify request for each depreciation
//set depreciation type - will be the same for all individually depreciated subledgers
s_parms.string_arg[1] = 'INDIVIDUAL'

//need to add subledger type as an argument
s_labels.long_label2 = 'Subledger Type'

for i = 1 to num_rows
	//get subledger name for the job description
	subledger_name = ds_subledgers.getitemstring(i, "subledger_name")
	job_description = "Month End Depreciation - " + subledger_name
	
	//set our subledger type parameter from the datastore 
	s_parms.long_arg2[1] = ds_subledgers.getItemNumber(i, "subledger_type_id")
	process_string = s_parms.string_arg[1] +": "+string(s_parms.long_arg2[1])
	
	//submit job task - all labels and other inputs will be the same for all individually depreciated subledgers
	//	use i + 1 because we already populated the first element with the process_identifier for the group job
	if g_main_application then 
		a_process_identifiers[i + 1] = nvo_ss.uf_request_job ("ssp_depr_calc.exe IND:" + trim(string(s_parms.long_arg2[1])), job_description, s_parms, s_labels, false, true)
	else 
		//when not running from the main application (which means we're running from Job Server), hardcode the last two inputs (run local, CPM) to true, false
		//	so that they launch immediately and do not create a CPM process
		a_process_identifiers[i + 1] = nvo_ss.uf_request_job ("ssp_depr_calc.exe IND:" + trim(string(s_parms.long_arg2[1])), job_description, s_parms, s_labels, false, true, true, false) 
	end if 
	f_status_box("Information","The job request for " + job_description + " was submitted successfully.")
next


return
end subroutine

public subroutine uf_launch_month_end_calcs (longlong a_company_ids[], date a_month) throws ex_depr_calc_launch;longlong not_used, not_used_array[]

uf_launch_month_end_calcs(a_company_ids, a_month, ref not_used, ref not_used_array)

return 
end subroutine

on nvo_depr_calc_launcher.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_depr_calc_launcher.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

