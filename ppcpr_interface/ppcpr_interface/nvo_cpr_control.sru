HA$PBExportHeader$nvo_cpr_control.sru
forward
global type nvo_cpr_control from nonvisualobject
end type
end forward

global type nvo_cpr_control from nonvisualobject autoinstantiate
end type

type variables


uo_ds_top i_ds_pp_interface_dates_check
uo_ds_top i_ds_interface_dates_all
uo_ds_top i_ds_cpr_control //this replaces i_dw
uo_ds_top i_ds_cpr_act_month
uo_ds_top i_ds_cpr_company

longlong i_month_number

string i_ProcessString
integer i_array_position_of_failed_companies[]

string i_company_descr[]

boolean i_bal_depreciation
boolean i_bal_summary
boolean i_cpr_balanced
boolean i_depr_balanced
string i_dw_title
string i_dw_object
boolean i_bal_act_actbasis
boolean i_bal_ledger_ldgbasis
boolean i_bal_ledger_act
boolean i_bal_ledger_subl	
boolean i_bal_quantities
boolean i_bal_subl
boolean i_bal_depr_subl
boolean i_balanced_cancelled
longlong i_row
datetime  i_month
datetime  i_months[4]
datetime i_auto_month
longlong i_company
longlong i_color
int             i_col_num
string i_where_act_mo
string i_where_cpr_company 
boolean i_return
boolean i_show_list
longlong i_company_idx[]
datetime i_original_month
integer i_original_col_num
longlong i_process_id
longlong i_num_basis

end variables

forward prototypes
public function integer of_selectedcompanies (ref longlong a_companies[])
public subroutine of_setupmonth (integer a_month_ind)
public function integer of_updatedw ()
public function boolean of_checksystemcontrol (integer a_button_number)
public function longlong of_setupcompany (longlong a_company_index)
public function integer of_retrievecompany (longlong a_company_index)
public function boolean of_cleanup (longlong a_button_number, string a_verify_descr, string a_msg)
public subroutine of_constructor ()
public function longlong of_getdescriptionsfromids (longlong a_company_ids[])
public function longlong of_setcolumnfromdate (date a_date)
public function string of_getcompanylist ()
public function integer of_currentmonth (datetime current_month)
public function longlong of_setupfromcompaniesandmonth (longlong a_company_ids[], date a_month)
public function string of_buildprocessstring (string a_exe_name, longlong a_company_id[], longlong a_month_number)
public function string of_buildcheckstring (string a_exe_name, string a_processstring)
public function boolean of_lockprocess (string a_exe_name, longlong a_company_id[], longlong a_month_number, ref string a_msg)
public function boolean of_releaseprocess (ref string a_msg)
public subroutine of_log_failed_companies (string a_log_header)
public function integer of_updatedw (boolean a_commit_flag)
end prototypes

public function integer of_selectedcompanies (ref longlong a_companies[]);/************************************************************************************************************************************************************
**
**	of_selectedCompanies()
**	
**	This function corresponds to w_cpr_control.wf_selected_companies(ref a_companies[]) and has been modified to 
**	determine whether the companies in a_companies[] can be processed together.
**	
**	Parameters	:	longlong	:	(ref a_companies[]) The selected companies
**	
**	Returns		:	integer	: Success: The number of companies selected; Failure: -1 
**
************************************************************************************************************************************************************/
string text
longlong max_open_mo, max_closed_mo, open_mo, closed_mo
longlong i, num_companies, company_id, index, empty[]

// loop over companies in a_companies
for index = 1 to upperbound(a_companies)

	select max(to_char(accounting_month, 'yyyymm'))
	into :max_open_mo
	from cpr_control 
	where company_id = :a_companies[index]
	and powerplant_closed is null;
	
	if isnull(max_open_mo) then max_open_mo = 0
	
	select max(to_char(accounting_month, 'yyyymm'))
	into :max_closed_mo
	from cpr_control  
	where company_id = :a_companies[index]
	and powerplant_closed is not null;
	
	if isnull(max_closed_mo) then max_closed_mo = 0 
		
	// if the first company selected the just set open and closed month
	if index = 1 then
		open_mo = max_open_mo
		closed_mo = max_closed_mo
	elseif open_mo <> max_open_mo  or closed_mo <>  max_closed_mo then				
		return -1
	end if // end if first index
next 

return index
end function

public subroutine of_setupmonth (integer a_month_ind);/************************************************************************************************************************************************************
**
**	of_setupMonth()
**	
**	This function corresponds to w_cpr_control.wf_setup_month(a_month_ind). Sets some instance variables for the current month and retrieves
**	i_ds_cpr_control for the current company/month.
**	
**	Parameters	:	longlong	:	(a_month_ind) Current index in i_months being processed
**	
**	Returns		:	(none)
**
************************************************************************************************************************************************************/
i_month = i_months[a_month_ind]
i_col_num = a_month_ind

i_ds_cpr_control.retrieve(i_company, i_month)

of_currentMonth(i_month)
end subroutine

public function integer of_updatedw ();/************************************************************************************************************************************************************
**
**	of_updateDW()
**	
**	This function just calls of_updatedw(boolean a_commit_flag) with a hardcoded true value. This was the original function where we always committed, so to avoid having to change everywhere to the new function, 
**		just overload it with the new function here. 
**	
**	Parameters	:	(none)
**	
**	Returns		:	integer	: Success: 1; Failure: -1
**
************************************************************************************************************************************************************/
return of_updatedw(true)
end function

public function boolean of_checksystemcontrol (integer a_button_number);/************************************************************************************************************************************************************
**
**	of_checkSystemControl()
**	
**	This function corresponds to w_cpr_control.wf_check_system_control(a_button_number).  This determines whether emails should be sent based on the
**	selected companies.
**	
**	Parameters	:	longlong	:	(a_button_number) Corresponds to the various buttons on w_cpr_control
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
// If a_system_control is turned on for at least one selected company, then return TRUE.
// If at least one company is not set, but "all companies" is turned on, then return TRUE.
// Otherwise return FALSE

integer	num_companies, c, rc
string	control_value
boolean	hasNull

longlong	start_pos, end_pos

start_pos = 1
end_pos	= 1
hasNull = FALSE
num_companies = upperbound(i_company_idx)
for c=1 to num_companies
	control_value = upper(f_pp_system_control_company("CPR MONTHLY CLOSE EMAILS", i_company_idx[c]))
	// There is an entry for the given company
	if isnull( control_value ) = false then
		// emails enabled for this process and this company
		do while end_pos > 0
			end_pos = pos(control_value, ",", start_pos)
			if end_pos > 0 then
				if Integer(mid(control_value, start_pos, end_pos - start_pos)) = a_button_number then return TRUE
			elseif end_pos = 0 then
				if Integer(mid(control_value, start_pos, len(control_value) - start_pos + 1)) = a_button_number then return TRUE
			else
				return FALSE
			end if
			start_pos = end_pos + 1
		loop
//		if pos(control_value, String( a_button_number )) > 0 then
//			return TRUE
//		end if
	else
		hasNULL = TRUE
	end if
next

// If for all the selected companies had emails disabled
if hasNULL = false then 
	return FALSE
else
	//Some companies had no value (neither disabled nor enabled), check if it's enabled for all the companies
	control_value = upper(f_pp_system_control_company("CPR MONTHLY CLOSE EMAILS", -1))
	if pos(control_value, String( a_button_number )) > 0 then
		return TRUE
	end if
end if


//All the values are set to "NO" or the system control is not set
return FALSE
end function

public function longlong of_setupcompany (longlong a_company_index);/************************************************************************************************************************************************************
**
**	of_setupCompany()
**	
**	This function corresponds to w_cpr_control.wf_setup_company(a_company_row) and has been modified to 
**	determine whether the companies in a_companies[] can be processed together.
**	
**	Parameters	:	longlong	:	(a_company_id) Company id
**	
**	Returns		:	longlong	: Success: The number of companies selected; Failure: -1 
**
************************************************************************************************************************************************************/
string company_descr
longlong m, rc, check

f_pp_msgs("Beginning of_setupCompany(). a_company_index = " + string(a_company_index))

rc = of_retrieveCompany(a_company_index)

if rc < 0 then
	f_pp_msgs("Error (" + String(rc) + ") Retrieving Company '" + i_company_descr[a_company_index] + "'")
	return -1
end if

f_pp_msgs("===========================")
f_pp_msgs("                           ")
f_pp_msgs("===========================")
f_pp_msgs("Processing Company " + String(i_company_idx[a_company_index]) &
				+ "-'" + i_company_descr[a_company_index] + "'")
				
f_pp_msgs("Upperbound of i_months: " + string(upperbound(i_months)))
					
//ddlb_company.selectionchanged causes month 1 to be picked, check if it should be 2-4
for m = 1 to UpperBound(i_months) 
	if i_months[m] = i_original_month then
		if m <> i_original_col_num then
			f_pp_msgs("Skipping company because Month " + String(i_original_month, 'mm/yyyy') &
				+ " is in column " + String(m) + " not column " + String(i_original_col_num))
			return -1
		end if
		if m > 1 then of_setupMonth(m)
		exit
	end if
next
if m > UpperBound(i_months) then
	f_pp_msgs("Skipping company because Month " + String(i_original_month, 'mm/yyyy') &
				+ " was not found in the Control Window.")
	return -1
end if

return i_company_idx[a_company_index]
end function

public function integer of_retrievecompany (longlong a_company_index);/************************************************************************************************************************************************************
**
**	of_retrieveCompany()
**	
**	This function corresponds to w_cpr_control.wf_retrieve_company(a_company_row). Performs some validations and sets some instance variables for the 
**	current company.
**	
**	Parameters	:	longlong	:	(a_company_index) Current index in i_company_idx being processed
**	
**	Returns		:	integer	: Success: 1; Failure: -1 or -2
**
************************************************************************************************************************************************************/
int i, rows, nrows, owned, check
string text 
longlong color, company_id, check_count, interface_id, rtn
datetime month, max_cpr_month, max_pp_month, min_cpr_month
boolean skip

i_company = i_company_idx[a_company_index]

// make sure the company_set_of_books is defined
select count(*) into :check
from company_set_of_books where company_id  = :i_company;

if check = 0 then 
	f_pp_msgs("Company " + i_company_descr[a_company_index] + " must be defined in the Company/Set of Books table")
	return -1
end if

////////////////
//compare max months in pp_interface_dates and cpr_control and fix pp_interface_dates as needed 
setnull(max_cpr_month)
check_count = 0
select max(accounting_month), count(*) into :max_cpr_month , :check_count
from cpr_control where company_id = :i_company;

// lkk: if all months are missing, this company is not being closed, so no message
if isnull(max_cpr_month) then 
	return -2
end if

if check_count < 4 then
	f_pp_msgs('Call PPC. Some Months are missing in the CPR Control table.')
	return -2
end if

nrows = i_ds_pp_interface_dates_check.retrieve(i_company,'Asset Management')
	
select add_months (:max_cpr_month,   -3) into :min_cpr_month from dual; 

if nrows = 0 then
	//no pp_interface_dates were found
	
	check_count = 0
	select count(*) into :check_count from pp_interface
	where subsystem = 'Asset Management' and needed_for_closing = 1;
	
	if check_count = 0 then
		 // insert dummy row in pp_interface;
		insert into pp_interface (company_id, interface_id, subsystem, window, description,
		needed_for_closing)
		values (:i_company, 999, 'Asset Management', 'w_close_interface', 'Not in Use', 1); 
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("Cannot insert missing record into pp_interface." + sqlca.sqlerrtext)
			return -2
		end if 
	end if

	//  insert new rows in pp_interface_dates to sync with cpr_control 

	insert into pp_interface_dates(company_id, interface_id,accounting_month) 
	( select a.company_id, a.interface_id, b.accounting_month
	from pp_interface a, cpr_control b
	where a.company_id = b.company_id
	and a.subsystem = 'Asset Management' 
	and b.company_id = :i_company and needed_for_closing = 1 
	and b.accounting_month >=  :min_cpr_month 
	minus
	select company_id, interface_id, accounting_month
	from pp_interface_dates
	where interface_id in (select interface_id from pp_interface
		where subsystem = 'Asset Management' and company_id = :i_company
		and needed_for_closing = 1 ) 
	and company_id = :i_company 
	and accounting_month >=   :min_cpr_month  ); 

	if sqlca.sqlcode < 0 then
		f_pp_msgs("Cannot insert missing records into pp_interface_dates." + sqlca.sqlerrtext)
		return -2
	end if 
	
else

	for i = 1 to nrows
		
		max_pp_month = i_ds_pp_interface_dates_check.getitemdatetime(i, 'max_month')
		interface_id = i_ds_pp_interface_dates_check.getitemnumber(i, 'interface_id')
		check_count =  i_ds_pp_interface_dates_check.getitemnumber(i, 'count_check')
		
		if max_cpr_month = max_pp_month then
			if check_count < 4 then
				f_pp_msgs('Call PPC. Some Months are missing in the Interface Dates table for interface id ' + string(interface_id))
				return -2 
			end if
			continue
		end if 
	
		if max_cpr_month < max_pp_month then
			//very bad - need to fix by hand  
			f_pp_msgs('Call PPC. Some Interface Dates for interface id ' + string(interface_id) + &
			' are missing in the CPR Control table')
			exit
		end if 
	
		if max_cpr_month > max_pp_month then 
			  //   insert new rows in pp_interface_dates only to sync with cpr_control  
				
				insert into pp_interface_dates(company_id, interface_id,accounting_month) 
				(select a.company_id, a.interface_id, b.accounting_month
				from pp_interface a, cpr_control b
				where a.company_id = b.company_id
				and a.subsystem = 'Asset Management' 
				and b.company_id = :i_company and a.interface_id = :interface_id
				and needed_for_closing = 1 
				and b.accounting_month >=   :min_cpr_month 
				minus
				select company_id, interface_id, accounting_month
				from pp_interface_dates
				where interface_id in (select interface_id from pp_interface
					where subsystem = 'Asset Management' 
					and company_id = :i_company
					and interface_id = :interface_id
					and needed_for_closing = 1 ) 
				and company_id = :i_company 
				and accounting_month >=  :min_cpr_month);

				if sqlca.sqlcode < 0 then
					f_pp_msgs("Cannot insert missing records into pp_interface_dates." + sqlca.sqlerrtext)
					return -2
				end if 
		
		 end if
	  
	next 
	 
end if

rtn = i_ds_interface_dates_all.retrieve(i_company)

if sqlca.sqlcode <> 0 then
	f_pp_msgs("SQL Error retrieving i_ds_interface_dates for company_id " + string(i_company) + ". SQL Code: " + string(sqlca.sqlcode) + ". SQL Error text: " + sqlca.sqlerrtext)
end if

rows = i_ds_interface_dates_all.rowcount() 

skip = true
if rows = 0 or skip = false then 
	skip = true
	f_pp_msgs("if rows = 0 or skip = false then")
	return -2
end if

if rows > 4 then rows = 4

for i = 1 to rows

   month = i_ds_interface_dates_all.getitemdatetime(i,'accounting_month')
    
   i_months[i] = month
	 
	if i_original_month = month then
		if isnull(i_original_col_num) or i_original_col_num = 0 then
			i_original_col_num = i
		end if
	end if
next

of_setupMonth(1)

if i_original_col_num > 1 then
	of_setupMonth(i_original_col_num)
end if

string sqls

i_ds_cpr_company.Settransobject(sqlca)

i_where_cpr_company =  " upper(cpr_company.user_id) = '" & 
					+ trim(upper(s_user_info.user_id)) + "'" &
               + " and cpr_company.session_id = " + string(s_user_info.session_id) &
               + " and cpr_company.batch_report_id = " + string(0)

sqls = "delete from cpr_company  where " +  i_where_cpr_company
execute immediate :sqls;
if sqlca.sqlcode <> 0 then	return -3
commit;

i_ds_cpr_company.insertrow(1)

i_ds_cpr_company.setitem(1,'user_id',upper(s_user_info.user_id))
i_ds_cpr_company.setitem(1,'session_id',s_user_info.session_id)
i_ds_cpr_company.setitem(1,'batch_report_id',0)
i_ds_cpr_company.setitem(1,'company_id',i_company)

rtn = i_ds_cpr_company.update()

if rtn = 1 then

   commit using sqlca;

   i_ds_cpr_company.reset()
   i_ds_cpr_company.resetupdate()

else

   rollback using sqlca;

//     g_msg.messagebox("Update Error ", "Error while updating the cpr company table.")

   return -4

end if

return 1
end function

public function boolean of_cleanup (longlong a_button_number, string a_verify_descr, string a_msg);/************************************************************************************************************************************************************
**
**	of_cleanup()
**	
**	This function will send emails to notify the appropriate users that a process has completed.
**	
**	Parameters	:	longlong	:	(a_button_number) Corresponds to the various buttons on w_cpr_control
**						string   :  (a_verify_descr) The description from pp_verify
**						string   :  (a_msg) The process name that has finished
**	
**	Returns		:	integer	: Success: The number of companies selected; Failure: -1 
**
************************************************************************************************************************************************************/
longlong i
string	find_str, user_id, sqls
boolean	vfy_users
uo_ds_top ds_users

if of_checkSystemControl( a_button_number ) = true then
	ds_users = create uo_ds_top
	vfy_users = false
	select nvl(user_sql, '') into :sqls from pp_verify where lower(description) = :a_verify_descr;
	if sqls <> "" then
		find_str = f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true)
		if f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true) = "OK" then
			if ds_users.RowCount() > 0 then
				vfy_users = true
			end if
		end if
	end if

	if vfy_users = true then
		for i=1 to ds_users.RowCount()
			user_id = ds_users.GetItemString( i, 1)
			f_send_mail( user_id, a_msg + " completed", &
			a_msg + " completed   on " + String(Today(), "mm/dd/yyyy") + &
			" at " + String(Today(), "hh:mm:ss"), user_id)
		next
	else
		f_send_mail( s_user_info.user_id, a_msg + " completed", &
		a_msg + " completed   on " + String(Today(), "mm/dd/yyyy") + &
		" at " + String(Today(), "hh:mm:ss"), s_user_info.user_id)
	end if
end if

return true
end function

public subroutine of_constructor ();
// Set up all of the datastores
i_ds_pp_interface_dates_check = CREATE uo_ds_top
i_ds_pp_interface_dates_check.dataObject = 'dw_pp_interface_dates_check'
i_ds_pp_interface_dates_check.setTransObject(SQLCA)

i_ds_interface_dates_all = CREATE uo_ds_top
i_ds_interface_dates_all.dataObject = 'dw_interface_dates_all'
i_ds_interface_dates_all.setTransObject(SQLCA)

i_ds_cpr_control = CREATE uo_ds_top
i_ds_cpr_control.dataObject = 'dw_cpr_control'
i_ds_cpr_control.setTransObject(SQLCA)

i_ds_cpr_act_month = CREATE uo_ds_top
i_ds_cpr_act_month.dataObject = 'dw_cpr_act_month'
i_ds_cpr_act_month.setTransObject(SQLCA)

i_ds_cpr_company = CREATE uo_ds_top
i_ds_cpr_company.dataObject = 'dw_cpr_company'
i_ds_cpr_company.setTransObject(SQLCA)

end subroutine

public function longlong of_getdescriptionsfromids (longlong a_company_ids[]);/************************************************************************************************************************************************************
**
**	of_getDescriptionsFromIDs()
**	
**	This function populates the i_company_descr array based on the company ids passed in
**	
**	Parameters	:	longlong	:	(a_company_ids[]) The selected companies_ids
**	
**	Returns		:	integer	: Success: 1; Failure: -1 
**
************************************************************************************************************************************************************/
longlong index

for index = 1 to upperbound(a_company_ids)
	select description
	into :i_company_descr[index]
	from company
	where company_id = :a_company_ids[index];
	
	if sqlca.sqlcode = 100 then
		f_pp_msgs("ERROR: Could not find company_id " + string(a_company_ids[index]) + " in company view.")
		return -1
	elseif sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: SQL Error getting company description from company table for company_id " + string(a_company_ids[index]) + ". SQL Error: " + sqlca.sqlerrtext)
		return -1
	end if
next

return 1
end function

public function longlong of_setcolumnfromdate (date a_date);/*****************************************************************************************************
**
**	of_setColumnFromDate()
**	 
**	This function sets the i_original_col_num from a date passed in 
**	
**	Parameters	:	date		:	(a_date) The date to find the column for
**	
**	Returns		:	integer	: Success: 1; Failure: -1 
**
******************************************************************************************************/
longlong index

for index = 1 to upperbound(i_months)
	if i_months[index] = datetime(a_date) then 
		i_original_col_num = index
	end if 
next

return 1

end function

public function string of_getcompanylist ();string list_str

list_str = f_parseNumArrayIntoString(i_company_idx)

return list_str
end function

public function integer of_currentmonth (datetime current_month);string sqls
int i, rtn


i_ds_cpr_act_month.DataObject = 'dw_cpr_act_month'
i_ds_cpr_act_month.Settransobject(sqlca)

//rtn = dw_cpr_act_month.retrieve(s_user_info.user_id, 0, s_user_info.session_id)
//
i_where_act_mo =  "  upper(cpr_act_month.user_id) = '" & 
					+ trim(upper(s_user_info.user_id)) + "'" &
               + " and  cpr_act_month.session_id = " + string(s_user_info.session_id) &
               + " and  cpr_act_month.batch_report_id = " + string(0)

//if rtn < 1 then dw_cpr_act_month.insertrow(0)

sqls = "delete from cpr_act_month  where " +  i_where_act_mo
execute immediate :sqls;
f_check_sql_error(sqlca, "Error deleting cpr_act_month")

commit;

i_ds_cpr_act_month.insertrow(1)

//
i_ds_cpr_act_month.setitem(1,'user_id', upper(s_user_info.user_id))
i_ds_cpr_act_month.setitem(1,'session_id',s_user_info.session_id)
i_ds_cpr_act_month.setitem(1,'batch_report_id',0)
i_ds_cpr_act_month.setitem(1,'month',current_month)

//g_msg.messagebox("", "current month " + string(current_month))

rtn = i_ds_cpr_act_month.update()

if rtn = 1 then

   commit using sqlca;

   i_ds_cpr_act_month.reset()
   i_ds_cpr_act_month.resetupdate()

else

   rollback using sqlca;

     g_msg.messagebox("Update Error ", "Error while updating the cpr activity month table.")

   return -1

end if

return 1
end function

public function longlong of_setupfromcompaniesandmonth (longlong a_company_ids[], date a_month);longlong rc, rtn

//setup i_nvo_cpr_control
this.i_month = datetime(a_month)
this.i_original_month = datetime(a_month)
this.i_company_idx = a_company_ids
//now that we set the i_company_idx array, we can call function to populate descriptions
rtn = this.of_getDescriptionsFromIDs(a_company_ids)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return -1
end if

rc = this.of_retrieveCompany(1) // retrieve first company to get i_months setup correctly.

if rc < 0 then
	f_pp_msgs("Error (" + String(rc) + ") Retrieving Company '" + this.i_company_descr[1] + "'")
	return -1
end if

this.of_setcolumnfromdate(a_month)

return 1
end function

public function string of_buildprocessstring (string a_exe_name, longlong a_company_id[], longlong a_month_number);/************************************************************************************************************************************************************
**
**	of_buildProcessString()
**	
**	This function should build the process string that will be used to lock the given process.
**		
**	Parameters	:	string a_exe_name: The exe name for the process currently running, e.g. ssp_wo_new_month.exe
**						longlong a_company_id[]: The array of company ids to process
**						longlong a_month_number: The month being processed in YYYYMM format
**
**	Returns		:	string	: Returns the process string
**
************************************************************************************************************************************************************/
string processkeyword, processstring
longlong num_Companies, c, i

//Determine the process key word based on a_exe_name
choose case a_exe_name
	case "ssp_cpr_new_month.exe"
		processKeyWord = 'CPR_NEW_MONTH'
	case "ssp_cpr_close_month.exe"
		processKeyWord = 'CPR_CLOSE_MONTH'
	case "ssp_aro_calc.exe"
		processKeyWord = 'CPR_ARO_CALC'
	case "ssp_aro_approve.exe"
		processKeyWord = 'CPR_ARO_APPR'
	case "ssp_depr_calc.exe"
		processKeyWord = 'CPR_DEPR_CALC'
	case "ssp_depr_approval.exe"
		processKeyWord = 'CPR_DEPR_APPR'
	case "ssp_release_je.exe"
		processKeyWord = 'CPR_RJE'
	case "ssp_gl_recon.exe"
		processKeyWord = 'CPR_RECON'
	case "ssp_close_powerplant.exe"
		processKeyWord = 'CPR_CLOSE_PP'
	case "ssp_cpr_balance_pp.exe"
		processKeyWord = 'CPR_BAL_PP'
	case "ssp_preview_je.exe"
		processKeyWord = 'PREVIEW_JE_CPR'
	case else
		return "ERROR"
end choose
 
for c = 1 to upperbound(a_company_id)
	processString =  processString + processKeyWord + "|" + string(a_company_id[c]) + "|" + string(a_month_number) + ";"
next

return processString
end function

public function string of_buildcheckstring (string a_exe_name, string a_processstring);/************************************************************************************************************************************************************
**
**	of_buildCheckstring()
**	
**	This function will build the check string that will be used to check for related processes for this exe.
** If a process such as a calculation process is passed in, then we also want to pass back the correlating approval processes to check
**		
**	Parameters	:	string a_exe_name: The exe name for the process currently running, e.g. ssp_wo_calc_oh_afudc_wip.exe
**						string a_processString
**
**	Returns		:	string	: Returns the check string
**
************************************************************************************************************************************************************/
string checkString, processKeyWord

checkString = a_processstring

choose case a_exe_name
	case "ssp_cpr_new_month.exe"
		
	case "ssp_cpr_close_month.exe"
		
	case "ssp_aro_calc.exe"
		// Calculation and Approval cannot run at the same time
		checkString = checkString + f_replace_string(checkString, 'CPR_ARO_CALC', 'CPR_ARO_APPR','all')
	case "ssp_aro_approve.exe"
		// Calculation and Approval cannot run at the same time
		checkString = checkString + f_replace_string(checkString, 'CPR_ARO_APPR', 'CPR_ARO_CALC','all')
	case "ssp_depr_calc.exe"
		// Calculation and Approval cannot run at the same time
		// The depr calc can run concurrently with itself, just not the approval
		checkString = f_replace_string(checkString, 'CPR_DEPR_CALC', 'CPR_DEPR_APPR','all')
	case "ssp_depr_approval.exe"
		// Calculation and Approval cannot run at the same time
		checkString = checkString + f_replace_string(checkString, 'CPR_DEPR_APPR', 'CPR_DEPR_CALC','all')
	case "ssp_release_je.exe"
		
	case "ssp_gl_recon.exe"
		
	case "ssp_close_powerplant.exe"
		
	case "ssp_cpr_balance_pp.exe"
		
	case "ssp_preview_je.exe"
		
	case else
		return "ERROR"
end choose

return checkString
end function

public function boolean of_lockprocess (string a_exe_name, longlong a_company_id[], longlong a_month_number, ref string a_msg);/************************************************************************************************************************************************************
**
**	of_lockprocess()
**	
**	This function will lock the process for the executable name being passed in.
**		
**	Parameters	:	string a_exe_name: The exe name for the process currently running, e.g. ssp_wo_new_month.exe
**						longlong a_company_id[]: The array of company ids to process
**						longlong a_month_number: The month being processed in YYYYMM format
**						ref string a_msg: Passes any error messages back to the caller
**
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
//Variables Declaration
string lockString, checkString
boolean rtn

//Call of_buildProcessString() to build the process string or lock string
lockString = of_buildProcessString(a_exe_name, a_company_id[], a_month_number)
//check processString for error
if lockString = "ERROR" then
	f_pp_msgs("Error occurred in building the lock string.")
	f_pp_msgs("Executable name: " + a_exe_name)
	return false
end if

//Based on the a_exe_name, determine whether anything else needs to be added to the check string
//e.g. if a calc is being processed, need to add the corresponding approval to the check string
checkString = of_buildCheckString(a_exe_name, lockString)
if checkString = "Error" then
	f_pp_msgs("Error occurred in building the check string.")
	f_pp_msgs("Executable name: " + a_exe_name)
	f_pp_msgs("Check string returned: " + checkString)
	return false
end if

// make sure process is not currently running
rtn = f_pp_check_process(checkString, a_msg)
if rtn = true then
	f_pp_msgs("The check for any concurrent process is successful.")
	f_pp_msgs("No other process is running, preparing to lock the process.")
else
	f_pp_msgs("Error occurred in f_pp_check_process.")
	f_pp_msgs("Detected process with executable name: " + a_exe_name + " is currently running")
	f_pp_msgs("Error Message: " + a_msg)
	return false
end if

// lock the process
rtn = f_pp_set_process(lockString, checkString, a_msg)

//If successful, set i_processString = process string
if rtn = true then
	i_processString = lockString
else
   f_pp_msgs("The process is currently being used in another session")	
	return false
end if

end function

public function boolean of_releaseprocess (ref string a_msg);/************************************************************************************************************************************************************
**
**	of_releaseProcess()
**	
**	This function will release the process for the executable name being passed in.
**		
**	Parameters	:	ref string a_msg: Passes any error messages back to the caller
**
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/

return f_pp_release_process(i_processString, a_msg)
end function

public subroutine of_log_failed_companies (string a_log_header);//**********************************************************************************************************************************************************
//
// of_log_failed_companies()
// 
// log the list of companies that failed at whatever they were doing in their company loop
// 
// Parameters : String: a_log_header - log entry header, like "CALCULATE ACCRUALS"
// 
// Returns : Nothing
//
//**********************************************************************************************************************************************************

int num_failed_companies, c, company_array_position

num_failed_companies = upperbound(i_array_position_of_failed_companies)
for c=1 to num_failed_companies
	company_array_position = i_array_position_of_failed_companies[c]
	f_pp_msgs("FAILED TO " + a_log_header + " FOR : Company ID: " + String(i_company_idx[company_array_position]) + ", Company Name: " + i_company_descr[company_array_position] )
next
end subroutine

public function integer of_updatedw (boolean a_commit_flag);/************************************************************************************************************************************************************
**
**	of_updateDW()
**	
**	This function corresponds to w_cpr_control.wf_update_dw(). Updates cpr_control and commits conditionally depending on the commit flag. 
**	
**	Parameters	:	(none)
**	
**	Returns		:	integer	: Success: 1; Failure: -1
**
************************************************************************************************************************************************************/
int rtn


rtn = i_ds_cpr_control.update()

if rtn = 1 then

	if a_commit_flag then
		commit using sqlca;
	end if
  return 1

else

   rollback using sqlca;

   f_pp_msgs("Cannot update the CPR Control table.")
   return -1

end if 
end function

on nvo_cpr_control.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_cpr_control.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;
DESTROY i_ds_pp_interface_dates_check
DESTROY i_ds_interface_dates_all
DESTROY i_ds_cpr_control
DESTROY i_ds_cpr_act_month
DESTROY i_ds_cpr_company

string sqls

// clean out temp tables 
sqls = "delete from cpr_act_month where " + i_where_act_mo;
execute immediate :sqls;


sqls = "delete from cpr_company where " + i_where_cpr_company
execute immediate :sqls;

commit;
end event

event constructor;of_constructor()
end event

