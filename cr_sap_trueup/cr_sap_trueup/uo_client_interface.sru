HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong	 i_num_elements, i_source_id, i_min_open_month, i_num_types, i_num_virt, &
			 i_elig_accts_count
string	 i_co, i_derivation_type, i_derived_ack_fields, i_stg_table, i_detail_table, &
			 i_company_field, i_join_fields, i_join_fields_array[], i_fp_field, &
			 i_rollup_element, i_rollup_element_table, i_rollup_element_column, &
			 i_wo_field, i_account_field, i_gl_journal_category, i_ignore_rule, &
			 i_pp_external_company, i_rate_field
datetime	 i_datetime_last_processed, i_upper_datetime_limit
decimal {4} i_pos_neg_ratio
uo_ds_top i_ds_all_tab_cols, i_ds_elements, i_ds_counter, i_ds_cr_txn_types, &
			 i_ds_balancing, i_ds_virt, i_ds_open_month
uo_cr_derivation i_uo_deriver
boolean i_warnings

string	i_exe_name = 'cr_sap_trueup.exe'
longlong	i_rollup_element_by_company
end variables

forward prototypes
public function longlong uf_insert_original ()
public function longlong uf_insert_basis ()
public function longlong uf_re_derive ()
public function longlong uf_insert_trueup ()
public function longlong uf_insert_trueup_final ()
public function longlong uf_delete ()
public function longlong uf_update_cr_derivation_rollup ()
public function longlong uf_ignore_rule ()
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_insert_original ();//*****************************************************************************************
//
//  Function  :  uf_insert_original
//
//  Description  :  Inserts the "original" transactions containing the current "answer" in
//                  the CR.  These transactions will be compared to the transactions
//                  generated through the re-derivation to determine which ACK
//                  distributions need to be trued up.  These transactions DO HAVE THE
//                  "derived" values (e.g. they do contain the ferc account if that is one
//                  of the derived fields).  The transactions generated in uf_insert_basis
//                  DO NOT contain the derived fields.
//
//  Notes  :  This script is called from within a master "TYPE" loop.  The instance
//            variable for the "TYPE" is i_derivation_type.
//            
//            We have to loop over i_join_fields_array since the original insert will have
//            a different where clause for every "join" we are performing in this
//            derivation type.
//
//            Due to potential overlap with the i_join_fields (i.e. one type of "STRING"
//            being an overriding subset of another "STRING" in that same "TYPE"), we must
//            insert individual transactions first (using the id field as a restriction
//            for subsequent inserts so we don't double-count, AND THEN summarize to the
//            higher level of detail).  In this function, summarizing everything by the
//            entire ACK ... in uf_insert_basis, summarizing everything beyond the
//            derived fields in the ACK.
//
//            The offshoot of these individual inserts by join, and the fact that we want
//            to allow for different joins within a "TYPE" is that certain "joins" may
//            end up going through the trueup logic when their cr_deriver_control records
//            have not changed.  For example, if a work order based record changes, then
//            even the work order||"RESOURCE" based records will end up in this table.
//            Since the selective inserts are simply a performance consideration, this is
//            OK, and the rounding threshold will ensure that we don't book inappropriate
//            trueup entries.
//
//            The offshoot of THIS (above paragraph) is that ultimately, the re-derivation
//            is going to potentially fire multiple times for a given "TYPE".  THUS, IT
//            IS CRITICAL THAT THE JOIN_FIELDS ARE ENTERED IN THE PROPER ORDER IN
//            CR_SAP_TRUEUP_CONTROL.
//            |
//            |--> SUBSETS MUST BE FIRST IN THE COMMA-SEPARATED LIST !!!
//                 E.G. work_order||"RESOURCE",work_order
//
//            You would have to set up different derivation "TYPES" if the derived fields
//            are not the same.
//
//            Since CR_TXN_TYPE is holding the join_field value, you might need to make
//            it bigger than varchar2(35) depending on the join_field values need at
//            each client.  CR_TXN_TYPE is a required field in the detailed attributes.
//
//*****************************************************************************************
longlong num_cols, j, i, id, rtn
string sqls, col, join_field, sqls_pk_off, sqls_pk_on, outer_sqls, where_filter


sqls_pk_off = "alter table " + i_stg_table + " disable primary key"
sqls_pk_on  = "alter table " + i_stg_table + "  enable primary key"


//  There is an instance DS against all_tab_columns so we can build the insert.  Since we
//  want to summarize life-to-date balances, we are simply going to get the $$$ from
//  cr_cost_repository.
sqls = &
	"select upper(column_name), column_id from all_tab_columns " + &
	 "where table_name = 'CR_COST_REPOSITORY' and owner = 'PWRPLANT'"
i_ds_all_tab_cols.Reset()
i_ds_all_tab_cols.SetSQLSelect(sqls)
num_cols = i_ds_all_tab_cols.RETRIEVE()
i_ds_all_tab_cols.SetSort("column_id a")
i_ds_all_tab_cols.Sort()




//  THIS IS THE OUTER LOOP OVER JOIN FIELDS.  Loop and execute 1 insert command
//  per join (with the appropariate where clause).
//
//  It's a pain to build a sum() and then an outer select to handle the PK field using
//  the crdetail sequence.  Simply insert the transactions with the id from
//  cr_cost_repository and update id with crdetail.nextval later after we have summarized
//  the transactions to the higher level of detail.
for j = 1 to upperbound(i_join_fields_array)

join_field = i_join_fields_array[j]




//  Start the insert command.
sqls = "insert into " + i_stg_table + " ( "


//  Build the columns for the insert portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
	end choose
	
	sqls = sqls + '"' + col + '",'
	
next


//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)


// JAK -- changed the cr_deriver_control piece to an exists so we don't need a distinct
//  Close the "insert" and start the "select".
sqls = sqls + ", cr_txn_type) (select "


//  Loop again and build the select portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  *** 
	//  *** REMOVED THE DERIVED_POS CODE HERE THAT CONTROLS THE SUMMARIZATION TO REMOVE
	//  *** THE DERIVED FIELDS IN UF_INSERT_ORIGINAL.
	//  ***
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
		case "ID"
			//  Nothing.  I'm just making it obvious that we are inserting indivual
			//  transactions first.
		case "DR_CR_ID" // We'll update this later.
			sqls = sqls + "1,"
			continue
		case "QUANTITY" // We don't split quantities in the first place.
			//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
//			sqls = sqls + "0,"
//			continue
		case "AMOUNT"
			//  Nothing.  I'm just making it obvious that we are inserting indivual
			//  transactions first.
		case "MONTH_NUMBER"
			sqls = sqls + string(i_min_open_month) + ","
			continue
		case "MONTH_PERIOD"
			sqls = sqls + "0,"
			continue
		case "DRILLDOWN_KEY"
			//  Using this as a helper field.  This can be the same as uf_insert_basis since we
			//  will overwrite it below.
			sqls = sqls + "'NEEDS SUMMARIZATION',"
			continue
		case "GL_JOURNAL_CATEGORY"
			sqls = sqls + "'" + i_gl_journal_category + "',"
			continue
	end choose
	
	if '"' + col + '"' = i_rollup_element_column then
		//  This column needs a prefix since we are joining in its ME table below.
		//  Otherwise we will get an "ambiguous column" error.
		sqls = sqls + 'cr."' + col + '",'
	else
		sqls = sqls + 'cr."' + col + '",'
	end if
	
next


//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)

// JAK -- changed the cr_deriver_control piece to an exists so we don't need a distinct
//  From and where.
sqls = sqls + ", " + join_field + &
	" from cr_cost_repository cr, " + i_rollup_element_table + " me " + &
	" where "


//  The join between .cr and .me tables.
sqls = sqls + "cr." + i_rollup_element + " = me." + i_rollup_element_column + &
	" and me.element_type = 'Actuals' and "
	
// ### 7924:  JAK: 2011-06-27:  Values by company logic.
if i_rollup_element_by_company = 1 then sqls += "cr." + i_company_field + " = me." + i_company_field + " and "


//  Restrict by company.
sqls = sqls + 'cr."' + i_company_field + '" = ' + "'" + i_co + "'"


//  Restrict by month_number.
sqls = sqls + " and cr.month_number <= " + string(i_min_open_month)


//  Restrict by join_fields.
//
//  Also add the time_stamp restriction so we don't constantly slam everything
//  through the trueup logic.  i_datetime_last_processed is a > since it was
//  already processed the last time this process ran (due to <= against the
//  i_upper_datetime_limit).
//
//  DMJ: 11/7/07: PERFORMANCE CHANGES:
//  1) Added cr. in front of the "id not in (select..." ... also added it in front of
//     "gl_journal_category not in ...".
//  2) Added cr. above in front of the i_company_field and month_number SQL.
//  3) Added "cr_deriver_control cdc" to the "from" above.
//  4) Added "cdc." in front of the time_stamps in the where clause.
//  5) Commented out lines below related to the "join_field in" clause since CDC
//     will now be joined in directly.
//  6) Added 6 lines below the commented out lines to accomplish the join and
//     other restrictions on cr_deriver_control.
//  7) Added "cr." to all columns in the "select" above since the ACK will now
//     be on both cr_cost_repository and cr_deriver_control.
//  8) Added a "distinct" above in the "select" ... since cr_deriver_control could have
//     many records for these wo||cdr combinations (the splits) ... hopefully this
//     cure will not be worse than the disease.
//
//  OTHER CHANGES:
//  1) 2/2/09: Not clear why we had yyyy in the date format.  It was failing on my laptop
//     with the date as a yy construct.  Changed here to yy.  Note: in testing, the yy format
//     worked with BOTH yyyy and yy dates, while the yyyy format only worked with a yyyy date.
//     And then modified to format the string() PB function instead of changing the Oracle format.
//     That seemed more in line with the original code.
//
// JAK: Changed to exists 
sqls = sqls + &
	" and not exists (select gl_journal_category from cr_sap_trueup_exclusion z where z.gl_journal_category = cr.gl_journal_category )"
//sqls = sqls + &
//	" and cr.gl_journal_category not in (select gl_journal_category from cr_sap_trueup_exclusion)"
	
//sqls = sqls + " and " + join_field + " in ("
//sqls = sqls + 'select "STRING" from cr_deriver_control '
//sqls = sqls + 'where "TYPE" = ' + "'" + i_derivation_type + "' "
//sqls = sqls + "and cdc.time_stamp >  to_date('" + &
//	string(i_datetime_last_processed, 'mm/dd/yyyy hh:mm:ss') + "', 'mm/dd/yyyy hh24:mi:ss') "
//sqls = sqls + "and cdc.time_stamp <= to_date('" + &
//	string(i_upper_datetime_limit, 'mm/dd/yyyy hh:mm:ss')    + "', 'mm/dd/yyyy hh24:mi:ss')) "


// JAK: Changed to exists 
sqls = sqls + " and exists (select id from cr_deriver_control cdc "
sqls = sqls + "where " + join_field + ' = cdc."STRING" '
sqls = sqls + 'and cdc."TYPE" = ' + "'" + i_derivation_type + "' "
sqls = sqls + "and cdc.time_stamp >  to_date('" + &
	string(i_datetime_last_processed, 'mm/dd/yyyy hh:mm:ss') + "', 'mm/dd/yyyy hh24:mi:ss') "
sqls = sqls + "and cdc.time_stamp <= to_date('" + &
	string(i_upper_datetime_limit, 'mm/dd/yyyy hh:mm:ss')    + "', 'mm/dd/yyyy hh24:mi:ss')) "
	
	
// JAK: Changed to exists 
sqls = sqls + "and not exists (select id from " + i_stg_table + " z where z.id = cr.id) "

//  DMJ: 2/6/08: Added a table for work order exclusions.  One example might be a conversion
//  disaster where statuses and dates don't get filled in well on the WO Hdrs.
sqls = sqls + " and " + &
	"not exists ( " + &
		"select 1 from cr_sap_trueup_exclusion_wo zzz " + &
		 "where zzz.work_order_number = cr." + i_wo_field + ")"

if i_elig_accts_count > 0 then
// JAK: Changed to exists 
// ### 8122: JAK: 2011-07-20:  Bad join in the SQL below will never work.
	sqls = sqls + ' and exists (select account from cr_sap_trueup_eligible_accts zzz where zzz.account = cr."' + i_account_field + '") '
//	sqls = sqls + ' and "' + i_account_field + '" in (select account from cr_sap_trueup_eligible_accts) '
end if

//  DMJ: 12/30/2008: Entergy was going to have 2 derivation types and needed separate
//  filter code for their blanket/non-blanket types.  Added i_derivation_type as the
//  a_addl_arg (2nd arg).
where_filter = f_cr_sap_trueup_custom("uf_insert_original_filter", i_derivation_type, "cr_cost_repository")

if isnull(where_filter) then where_filter = ""

sqls = sqls + " " + where_filter


//  Close the "select".
sqls = sqls + ")"

if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_original: inserting into " + i_stg_table )
	f_pp_msgs("***DEBUG*** sqls = " + sqls)
end if

//  Insert the records.
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting into " + i_stg_table + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if




next  //  for j = 1 to upperbound(i_join_fields_array) ...


//  SUMMARIZE:
//  ----------
//  At this point we've got individual records from cr_cost_repository (now in the STG
//  table) that need to be processed through the trueup logic.  Summarize these
//  transactions first, so we are at the correct level of detail for the re-derivation.
//
//  We need this to be after the series of potential inserts above so we don't interfere
//  with the gathering of appropriate transactions based on the join_fields.
//
//  Remember that we are within a master "TYPE" loop here, so we don't need to worry
//  about summarizing transactions for multiple types together.

//  Start the insert command.
sqls = "insert into " + i_stg_table + " ( "
outer_sqls = "select "


//  Build the columns for the insert portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They are useless.
			continue
	end choose
	
	sqls = sqls + '"' + col + '",'
	
	if col = "ID" then
		outer_sqls = outer_sqls + "crdetail.nextval,"
	else
		outer_sqls = outer_sqls + '"' + col + '",'
	end if
	
next



//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)
outer_sqls = left(outer_sqls, len(outer_sqls) - 1)


outer_sqls = outer_sqls + ", cr_txn_type "
outer_sqls = outer_sqls + "from ( "


//  Close the "insert" and start the "select".
sqls = sqls + ", cr_txn_type) (" + outer_sqls + "select "


//  Loop again and build the select portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  We do not need derived DERIVED ACK FIELD code here since we populated those fields
	//  with single spaces above.
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They are useless.
			continue
		case "ID"  //  We'll update this later.
			sqls = sqls + "0 as id,"
			continue
		case "DR_CR_ID" // We'll update this later.
			sqls = sqls + "1 as dr_cr_id,"
			continue
		case "QUANTITY" // We don't split quantities in the first place.
			//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
//			sqls = sqls + "0 as quantity,"
			sqls = sqls + "sum(quantity) as quantity,"
			continue
		case "AMOUNT"
			sqls = sqls + "sum(amount) as amount,"
			continue
		case "DRILLDOWN_KEY"
			//  Using this as a helper field.
			sqls = sqls + "'ORIGINAL RECORD' as drilldown_key,"
			continue
	end choose
		
	sqls = sqls + '"' + col + '",'
	
next


//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)


//  From and where.
sqls = sqls + ", cr_txn_type from " + i_stg_table + " where "


//  The where is just based on drilldown_key.
sqls = sqls + "drilldown_key = 'NEEDS SUMMARIZATION' "


//  Group by.
sqls = sqls + " group by "


//  One more loop to build the group by.  We skip anything we skipped above and anything
//  that was hardcoded.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  We do not need derived DERIVED ACK FIELD code here since we populated those fields
	//  with single spaces above.
	
	choose case col
		case "TIME_STAMP", "USER_ID", "DRILLDOWN_KEY", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "ID", "DR_CR_ID", "QUANTITY", "AMOUNT", &
			  "SOURCE_ID"
			//  Leave these out of the sqls.  They are useless or they were hardcoded above.
			continue
	end choose
		
	sqls = sqls + '"' + col + '",'
	
next

//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)


//  Close the "select".
// JAK add having here so delete below isn't needed
sqls = sqls + ", cr_txn_type having sum(amount) <> 0))" // 2 ")" due to the outer_select.


////
////  8/18/07: POOR DECISION ... IT'S NOT CLEAR THAT ALL DBA'S WILL GIVE THE ID
////           PRIVILEGES TO EXECUTE THIS COMMAND ... WE NOW SELECT CRDETAIL.NEXTVAL
////           ABOVE.
////
////  Since we hardcoded id to 0, disable the PK.
//execute immediate :sqls_pk_off;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: disabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_original: inserting original records into " + i_stg_table )
	f_pp_msgs("***DEBUG*** sqls = " + sqls)
end if

//  Insert the ORIGINAL records.
execute immediate :sqls;

// JAK:  Return the record count below so we know if there are really any records to process...we can skip to the next company if not
rtn = sqlca.sqlnrows

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting original records into " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	////  SEE 8/18/07 COMMENT ABOVE.
	////  Need to re-enable PK to avoid an error the next time through.
	//execute immediate :sqls_pk_on;
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	rollback;
	//end if
	return -1
end if


//  Toss the "NEEDS SUMMARIZATION" records now that they have been summarized to
//  the appropriate level of detail.
sqls = "delete from " + i_stg_table + " where drilldown_key = 'NEEDS SUMMARIZATION'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting 'NEEDS SUMMARIZATION' from " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	//  At this point, we've got records (ORIGINAL RECORD) that will crash the PK.
	//  Trunc the table.
	sqlca.truncate_table(i_stg_table)
	////  SEE 8/18/07 COMMENT ABOVE.
	////  Need to re-enable PK to avoid an error the next time through.
	//execute immediate :sqls_pk_on;
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	rollback;
	//end if
	return -1
end if


//
//  SEE 8/18/07 COMMENT ABOVE.
//
//  Update PK (just the newly insert records).  Technically, we are not very interested in
//  having crdetail.nextval on the ORIGINAL RECORD transactions, since they will never be
//  inserted into a CR Detail table.  However, the STG table requires unique id values, so
//  the easiest thing to do is use the sequence ... keeping in mind that rownum and max()
//  techniques are not valid since the trueup records will later be inserted into this STG
//  table and MUST have crdetail.nextval as their PK to avoid any unintended consequences in
//  the CR.  We really don't care any longer since we've proven in Oracle that sequences will
//  go well into the trillions (we got bored once we hit the tens of trillions in testing),
//  and since id is number(22,0).  Burning sequence numbers is the least of our problems and
//  the decision has been made that the CR will happily allow that.  Any end user issues
//  with buring sequence numbers is not a valid complaint.
//sqls = &
//	"update " + i_stg_table + " set id = crdetail.nextval " + &
//	 "where drilldown_key = 'ORIGINAL RECORD'"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".id with crdetail.nextval: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
	////  SEE 8/18/07 COMMENT ABOVE.
	////  Need to re-enable PK to avoid an error the next time through.
	//execute immediate :sqls_pk_on;
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	rollback;
	//end if
//	return -1
//end if


////  SEE 8/18/07 COMMENT ABOVE.
////  Enable the PK.
//execute immediate :sqls_pk_on;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


//
//  This was an OK idea, just in the wrong place.  By putting this code here, it prevented
//  the MTL, NON-MTL, etc. transactions (the non-virtual cost elements) from being trued up
//  to an answer that would prevent the virtual derivations from creating large debits and
//  credits.  For example, with a 107 = 100, 108 = -50 and CDC records of .80/.20 the trueup
//  would create 107 = -60, 108 = -60 transactions that would shift the base for the virtual
//  trueup to reasonable numbers (i.e. no negatives).  Instead of forcing the trueup to deal
//  with transactions that might be negative (gl jcats that are excluded?), just let the virtual
//  derivations create whatever transactions they want, and use the positive_negative_ratio
//  to audit for yucky transactions before validating and final posting.  Then if the user
//  wants to book some JE to change the underlying basis, they can.  Or they can bump the ratio
//  higher in cr_sap_trueup_control and re-run this process to accept the yucky transactions.
//  The only other thing we need to add is a check for 0.00 in the summarized records so we
//  cannot get a divide-by-zero SQL error.  We will simply set those to .01 to emphasize
//  the bad net amount.  Furthermore, with regards to the 0.00 check, the trueup of the "regular"
//  cost elements based on CDC is of no help since the net amount on the work order is 0.00
//  The trueup of the "regular" will produce no result at all in this case.
//
////////  For Virtual Derivations:
////////  Delete any 'ORIGINAL RECORD' records that do not pass the positive_negative_ratio.  This
////////  threshold prevents wild answers when the denominator of the derivation approaches 0.00
////////  relative to the numerators (i.e. 107/108 balances of 1,000,000 and -999,999).  The default
////////  ratio is 2.00 which would allow an amount being trued-up to "double up" to the postive
////////  and negative side.  This also prevents divide-by-zero errors in the SQL for virtual
////////  derivations (the decode to .000001 would force even a penny to a ratio of 10,000 which
////////  should be plenty to guarantee that a net 0.00 always gets deleted).
////////  
////////  Note: this is a forced check by company, work order, and account.  If in the future,
////////  other fields need to be added, we would add some code that calls the extension function
////////  to build the "select" and "group by" portions of the SQL.
////////
////////  This function deletes ALL a work order's records if ANY account's ratio for the work order
////////  is outside the threshold.
//////sqls = &
//////	"delete from " + i_stg_table + " where " + i_wo_field + " in ( " + &
//////		"select " + i_wo_field + " from ( " + &
//////			"select " + i_company_field + ", " + i_wo_field + ", " + i_account_field + ", drilldown_key, " + &
//////					  "sum(amount) " + &
//////					  "/ " + &
//////					  "(select decode(sum(amount), 0, .000001, sum(amount)) " + &
//////						  "from " + i_stg_table + " aa " + &
//////						 "where aa.company = a.company and aa.work_order = a.work_order " + &
//////						 	"and drilldown_key = 'ORIGINAL RECORD') as amount " + &
//////			  "from " + i_stg_table + " a " + &
//////			 "where drilldown_key = 'ORIGINAL RECORD' " + &
//////			 "group by " + i_company_field + ", " + i_wo_field + ", " + i_account_field + ", drilldown_key) " + &
//////		 "where (amount < " + string(-i_pos_neg_ratio) + " or amount > " + string(i_pos_neg_ratio) + "))"
//////
//////execute immediate :sqls;
//////
//////if sqlca.SQLCode < 0 then
//////	f_pp_msgs("  ")
//////	f_pp_msgs("ERROR: deleting from " + i_stg_table + " (positive_neg_ratio): " + sqlca.SQLErrText)
//////	f_pp_msgs("  ")
//////	rollback;
//////	return -1
//////end if

// JAK : Having clause above now
////  DMJ: 11/7/07:
////  Delete the ORIGINAL RECORD records that are 0.00 ... there won't be any trueup on
////  these, but they could get the rounding plug, throwing is OOB by cost element.
////  When testing at Pepco, the custom balancing was kicking on these records.
//sqls = "delete from " + i_stg_table + " where drilldown_key = 'ORIGINAL RECORD' " + &
//	"and amount = 0"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: deleting 'ORIGINAL RECORD' where 0.00 from " + i_stg_table + ": " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	//  At this point, we've got records (ORIGINAL RECORD) that will crash the PK.
//	//  Trunc the table.
//	sqlca.truncate_table(i_stg_table)
//	return -1
//end if


commit;


return rtn

end function

public function longlong uf_insert_basis ();//*****************************************************************************************
//
//  Function  :  uf_insert_basis
//
//  Description  :  Inserts the "original" basis for transactions that will be considered
//                  for a trueup.  For example, if the ferc account is being derived,
//                  this function would insert summarized transactions into the STG table
//                  that have that field summarized away.  The resulting transactions
//                  generated by this function will be the basis for the "re-derivation"
//                  whose results will be compared to the current state of affairs.
//
//  Notes  :  This script is called from within a master "TYPE" loop.  The instance
//            variable for the "TYPE" is i_derivation_type.
//            
//            We have to loop over i_join_fields_array since the basis insert will have
//            a different where clause for every "join" we are performing in this
//            derivation type.
//
//            Due to potential overlap with the i_join_fields (i.e. one type of "STRING"
//            being an overriding subset of another "STRING" in that same "TYPE"), we must
//            insert individual transactions first (using the id field as a restriction
//            for subsequent inserts so we don't double-count, AND THEN summarize to the
//            higher level of detail).
//
//            The offshoot of these individual inserts by join, and the fact that we want
//            to allow for different joins within a "TYPE" is that certain "joins" may
//            end up going through the trueup logic when their cr_deriver_control records
//            have not changed.  For example, if a work order based record changes, then
//            even the work order||"RESOURCE" based records will end up in this table.
//            Since the selective inserts are simply a performance consideration, this is
//            OK, and the rounding threshold will ensure that we don't book inappropriate
//            trueup entries.
//
//            The offshoot of THIS (above paragraph) is that ultimately, the re-derivation
//            is going to potentially fire multiple times for a given "TYPE".  THUS, IT
//            IS CRITICAL THAT THE JOIN_FIELDS ARE ENTERED IN THE PROPER ORDER IN
//            CR_SAP_TRUEUP_CONTROL.
//            |
//            |--> SUBSETS MUST BE FIRST IN THE COMMA-SEPARATED LIST !!!
//                 E.G. work_order||"RESOURCE",work_order
//
//            You would have to set up different derivation "TYPES" if the derived fields
//            are not the same.
//
//            Since CR_TXN_TYPE is holding the join_field value, you might need to make
//            it bigger than varchar2(35) depending on the join_field values need at
//            each client.  CR_TXN_TYPE is a required field in the detailed attributes.
//
//*****************************************************************************************
longlong num_cols, j, i, derived_pos, id, rtn
string sqls, col, join_field, sqls_pk_off, sqls_pk_on, outer_sqls, where_filter


sqls_pk_off = "alter table " + i_stg_table + " disable primary key"
sqls_pk_on  = "alter table " + i_stg_table + "  enable primary key"


//  There is an instance DS against all_tab_columns so we can build the insert.  Since we
//  want to summarize life-to-date balances, we are simply going to get the $$$ from
//  cr_cost_repository.
//
//  The call to uf_insert_basis is now after the call to uf_insert_original.  Thus, we
//  will use the i_stg_table results from uf_insert_original as the basis for this
//  function's insert.  Regardless, we can still get column names from cr_cost_repository
//  since we are only interested in the ACK and CR common fields.
sqls = &
	"select upper(column_name), column_id from all_tab_columns " + &
	 "where table_name = 'CR_COST_REPOSITORY' and owner = 'PWRPLANT'"
i_ds_all_tab_cols.Reset()
i_ds_all_tab_cols.SetSQLSelect(sqls)
num_cols = i_ds_all_tab_cols.RETRIEVE()
i_ds_all_tab_cols.SetSort("column_id a")
i_ds_all_tab_cols.Sort()




//  THIS IS THE OUTER LOOP OVER JOIN FIELDS.  Loop and execute 1 insert command
//  per join (with the appropariate where clause).
//
//  It's a pain to build a sum() and then an outer select to handle the PK field using
//  the crdetail sequence.  Simply insert the transactions with the id from
//  cr_cost_repository (now from i_stg_table since we've added uf_insert_original)
//  and update id with crdetail.nextval later after we have summarized
//  the transactions to the higher level of detail.
for j = 1 to upperbound(i_join_fields_array)

join_field = i_join_fields_array[j]

join_field = f_replace_string(join_field, "cr.", "", "all")
join_field = f_replace_string(join_field, "me.", "", "all") // This function is now
	// working against the detail stg table which will have the cr_derivation_rollup
	// field.  However, the join_field value from cr_sap_trueup_control will contain
	// something like cr.work_order||me.cr_derivation_rollup since uf_insert_original
	// works against cr_cost_repository.  The prefixing allows us to join in the ME
	// table in uf_insert_original and roll the CR-Summary transactions up to the
	// cr_derivation_rollup level of detail.  We will generally be stripping off
	// the cr. and me. in all subsequent functions that work against the detail
	// stg table.




//  Start the insert command.
sqls = "insert into " + i_stg_table + " ( "


//  Build the columns for the insert portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
	end choose
	
	sqls = sqls + '"' + col + '",'
	
next


//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)


//  Close the "insert" and start the "select".
sqls = sqls + ", cr_txn_type) (select "


//  Loop again and build the select portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  If this is a DERIVED ACK FIELD, hardcode the column to a single-space so we
	//  summarize it away.  The transactions with the single-spaces will never be posted
	//  so validations are not a concern.
	// PP-43882
	derived_pos = pos(',' + upper(i_derived_ack_fields), "," + col + ",")
	if derived_pos <> 0 then
		sqls = sqls + "' ',"
		continue
	end if
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
		case "ID"
			//  Since we have inserted records and applied crdetail.nextval in uf_insert_original
			//  we need to take care that we don't bomb the PK on i_stg_table.  Flip the sign
			//  on the id so this insert will fire.  The crdetail.nextval is applied later
			//  after we summarize the transactions generated by this insert.
			sqls = sqls + "-id,"
			continue
		case "DR_CR_ID" // We'll update this later.
			sqls = sqls + "1,"
			continue
		case "QUANTITY" // We don't split quantities in the first place.
			//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
//			sqls = sqls + "0,"
//			continue
		case "AMOUNT"
			//  Nothing.  I'm just making it obvious that we are inserting indivual
			//  transactions first.
		case "MONTH_NUMBER"
			sqls = sqls + string(i_min_open_month) + ","
			continue
		case "MONTH_PERIOD"
			sqls = sqls + "0,"
			continue
		case "DRILLDOWN_KEY"
			//  Using this as a helper field.
			sqls = sqls + "'NEEDS SUMMARIZATION',"
			continue
	end choose
		
	sqls = sqls + '"' + col + '",'
	
next


//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)


//  From and where.
//  DMJ: 10/03/07: Testing at Pepco found that this needs to be the value from the
//  ORIGINAL RECORD records (the value looks like 600796NON-MTL and so forth).  This
//  is the result of the "join fields" on each record.  The join_fields variable looks
//  something like "work_order||cost_element" which is not what we want here in the 
//  cr_txn_type.  The actual value is needed for the threshold_limit logic later in
//  the trueup.
//sqls = sqls + ", '" + join_field + "' from " + i_stg_table + " where "
sqls = sqls + ", cr_txn_type from " + i_stg_table + " where "


//  From and where.
sqls = sqls + " drilldown_key = 'ORIGINAL RECORD' "


//  Restrict by company.  Still need this in this function.  Even though we are hitting
//  i_stg_table, we still want to restrict by company.
sqls = sqls + 'and "' + i_company_field + '" = ' + "'" + i_co + "'"


//  Restrict by month_number.  Still seems OK in this function even though we are
//  hitting i_stg_table.
sqls = sqls + " and month_number <= " + string(i_min_open_month)


//
// ** WE STILL NEED A RESTRICTION OF THIS FORM FOR THE BASIS INSERT.  THE JOIN FIELD
// ** PART IS REDUNDANT, BUT THE ID RESTRICTION IS VERY IMPORTANT SINCE ... IF THERE
// ** ARE MULTIPLE JOINS IN A "TYPE", THE MULTIPLE INSERTS WOULD GRAB SOME OF THE
// ** RECORDS MORE THAN ONCE.  FOR EXAMPLE, THE WORK_ORDER||"RESOURCE" INSERT WOULD
// ** HAVE ALREADY INSERTED SOME OF THE RECORDS THAT THE WORK ORDER-ONLY INSERT WOULD
// ** WOULD GRAB IF NOT RESTRICTED.  NOTE: --- THE -ID PORTION CONTROLS THIS.
//
//  Restrict by join_fields.
//
//  Also add the time_stamp restriction so we don't constantly slam everything
//  through the trueup logic.  i_datetime_last_processed is a > since it was
//  already processed the last time this process ran (due to <= against the
//  i_upper_datetime_limit).
//
//  2/2/09: Changed yyyy format to yy (see comments in uf_insert_original)
//     And then modified to format the string() PB function instead of changing the Oracle format.
//     That seemed more in line with the original code.
//
sqls = sqls + " and " + join_field + " in ("
sqls = sqls + 'select "STRING" from cr_deriver_control '
sqls = sqls + 'where "TYPE" = ' + "'" + i_derivation_type + "' "
sqls = sqls + "and time_stamp >  to_date('" + &
	string(i_datetime_last_processed, 'mm/dd/yyyy hh:mm:ss') + "', 'mm/dd/yyyy hh24:mi:ss') "
// ###: 9071: JAK: 2012-01-07: Remove the upperbound limit.  This can cause problems if the estimates
//	for an eligible work order are updated while the process is running ultimately causing an OOB condition.
sqls = sqls + ") "
//sqls = sqls + "and time_stamp <= to_date('" + &
//	string(i_upper_datetime_limit, 'mm/dd/yyyy hh:mm:ss')    + "', 'mm/dd/yyyy hh24:mi:ss')) "
sqls = sqls + "and -id not in (select id from " + i_stg_table + ")"


//  JAK 20100714:  If they have multiple types that have the same join field, this causes problems.
//  Add a call to the custom function to fine tune it as needed.
where_filter = f_cr_sap_trueup_custom("uf_insert_basis_filter", i_derivation_type, i_stg_table)

if isnull(where_filter) then where_filter = ""

sqls = sqls + " " + where_filter

//
//  4/9/09:  BILLIABLE AFUDC, OH, ETC. THAT YOU DO NOT WANT TO GO THROUGH THE TRUEUP, BUT
//           DO WANT TO BE INCLUDED IN THE VIRTUAL BASIS.
//
//  I thought about putting code here that would filter these things out after allowing
//  uf_insert_original to include those records (by removing the gl_journal_category values
//  from cr_sap_trueup_exclusion).
//
//  I decided against this because it seems grossly unnecessary and I'm very concerned about
//  performance.  Instead, they can still be removed from cr_sap_trueup_exclusion and if we
//  simply refrain from putting their records in cr_deriver_control, then those $$$ will be
//  in the staging table (and available to uf_virtual - with some other code changes in
//  uf_re_deriver), but will not get a re-derivation and thus, no trueup.
//
//  Actually wrong since uf_insert_original joins in cdc.string, and there is also a Case C
//  in the trueup that will still try to grab the AFUDC.  Still, I'm shifting gears away
//  from this function.
//


//  Close the "select".
sqls = sqls + ")"

if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_basis: inserting into " + i_stg_table )
	f_pp_msgs("***DEBUG*** sqls = " + sqls)
end if


//  Insert the records.
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting into " + i_stg_table + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if




next  //  for j = 1 to upperbound(i_join_fields_array) ...


//  SUMMARIZE:
//  ----------
//  At this point we've got individual records from cr_cost_repository (now in the STG
//  table) that need to be processed through the trueup logic.  Summarize these
//  transactions first, so we are at the correct level of detail for the re-derivation.
//
//  We need this to be after the series of potential inserts above so we don't interfere
//  with the gathering of appropriate transactions based on the join_fields.
//
//  Remember that we are within a master "TYPE" loop here, so we don't need to worry
//  about summarizing transactions for multiple types together.

//  Start the insert command.
sqls = "insert into " + i_stg_table + " ( "
outer_sqls = "select "


//  Build the columns for the insert portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They are useless.
			continue
	end choose
	
	sqls = sqls + '"' + col + '",'
	
	if col = "ID" then
		outer_sqls = outer_sqls + "crdetail.nextval,"
	else
		outer_sqls = outer_sqls + '"' + col + '",'
	end if
	
next


//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)
outer_sqls = left(outer_sqls, len(outer_sqls) - 1)


outer_sqls = outer_sqls + ", cr_txn_type "
outer_sqls = outer_sqls + "from ( "


//  Close the "insert" and start the "select".
sqls = sqls + ", cr_txn_type) (" + outer_sqls + "select "


//  Loop again and build the select portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  We do not need derived DERIVED ACK FIELD code here since we populated those fields
	//  with single spaces above.
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They are useless.
			continue
		case "ID"  //  We'll update this later.
			sqls = sqls + "0,"
			continue
		case "DR_CR_ID" // We'll update this later.
			sqls = sqls + "1 as dr_cr_id,"
			continue
		case "QUANTITY" // We don't split quantities in the first place.
			//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
//			sqls = sqls + "0 as quantity,"
			sqls = sqls + "sum(quantity) as quantity,"
			continue
		case "AMOUNT"
			sqls = sqls + "sum(amount) as amount,"
			continue
		case "DRILLDOWN_KEY"
			//  Using this as a helper field.
			sqls = sqls + "'SUMMARIZED RECORD' as drilldown_key,"
			continue
	end choose
		
	sqls = sqls + '"' + col + '",'
	
next


//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)


//  From and where.
sqls = sqls + ", cr_txn_type from " + i_stg_table + " where "


//  The where is just based on drilldown_key.
sqls = sqls + "drilldown_key = 'NEEDS SUMMARIZATION' "


//  Group by.
sqls = sqls + " group by "


//  One more loop to build the group by.  We skip anything we skipped above and anything
//  that was hardcoded.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  We do not need derived DERIVED ACK FIELD code here since we populated those fields
	//  with single spaces above.
	
	choose case col
		case "TIME_STAMP", "USER_ID", "DRILLDOWN_KEY", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "ID", "DR_CR_ID", "QUANTITY", "AMOUNT", &
			  "SOURCE_ID"
			//  Leave these out of the sqls.  They are useless or they were hardcoded above.
			continue
	end choose
		
	sqls = sqls + '"' + col + '",'
	
next

//  Trim the trailing comma.
sqls = left(sqls, len(sqls) - 1)


//  Close the "select".
sqls = sqls + ", cr_txn_type))" // 2 ")" due to the outer_select.


////
////  8/18/07: POOR DECISION ... IT'S NOT CLEAR THAT ALL DBA'S WILL GIVE THE ID
////           PRIVILEGES TO EXECUTE THIS COMMAND ... WE NOW SELECT CRDETAIL.NEXTVAL
////           ABOVE.
////
////  Since we hardcoded id to 0, disable the PK.
//execute immediate :sqls_pk_off;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: disabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if

if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_basis: inserting summarized records into " + i_stg_table )
	f_pp_msgs("***DEBUG*** sqls = " + sqls)
end if

//  Insert the SUMMARIZED records.
execute immediate :sqls;

// JAK:  Return the record count below so we know if there are really any records to process...we can skip to the next company if not
rtn = sqlca.sqlnrows

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting summarized records into " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	////  SEE 8/18/07 COMMENT ABOVE.
	////  Need to re-enable PK to avoid an error the next time through.
	//execute immediate :sqls_pk_on;
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	rollback;
	//end if
	return -1
end if


//  Toss the "NEEDS SUMMARIZATION" records now that they have been summarized to
//  the appropriate level of detail.
sqls = "delete from " + i_stg_table + " where drilldown_key = 'NEEDS SUMMARIZATION'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting 'NEEDS SUMMARIZATION' from " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	//  At this point, we've got records (SUMMARIZED RECORD) that will crash the PK.
	//  Trunc the table.
	sqlca.truncate_table(i_stg_table)
	////  SEE 8/18/07 COMMENT ABOVE.
	////  Need to re-enable PK to avoid an error the next time through.
	//execute immediate :sqls_pk_on;
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	rollback;
	//end if
	return -1
end if


//  Update PK (just the newly insert records).
sqls = &
	"update " + i_stg_table + " set id = crdetail.nextval " + &
	 "where drilldown_key = 'SUMMARIZED RECORD'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ".id with crdetail.nextval: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	////  SEE 8/18/07 COMMENT ABOVE.
	////  Need to re-enable PK to avoid an error the next time through.
	//execute immediate :sqls_pk_on;
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	rollback;
	//end if
	return -1
end if


////  SEE 8/18/07 COMMENT ABOVE.
////  Enable the PK.
//execute immediate :sqls_pk_on;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: enabling " + i_stg_table + " PK: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if

commit;


return rtn

end function

public function longlong uf_re_derive ();//*****************************************************************************************
//
//  Function  :  uf_re_derive
//
//  Description  :  Runs the derivations against i_stg_table.
//
//  Notes  :  This script is called from within a master "TYPE" loop.  The instance
//            variable for the "TYPE" is i_derivation_type.
//            
//            The i_stg_table contains summarized transacitons at this point which contain
//            the basis for re-derivation that will determine what the correct answer
//            should be.
//            
//            We have to loop over i_join_fields_array since those values imply different
//            a_join values for the call to uf_deriver.  A custom function that accepts
//            the i_join_fields_array values will return the a_join.  This is consistent
//            with our interface methodology since the developer "types" the a_join
//            syntax into the uf_deriver call in those scripts.  Also, there is no
//            guarantee that the a_join in an interface will be exactly the same as the
//            a_join in this trueup logic.  Thus we cannot table-drive a_join.  Also #2,
//            derivation types can support multiple a_join values by making separate calls
//            to uf_deriver with the appropriate where clause in each a_join to keep the
//            disparate calls away from each other's data.
//
//            This is a MONUMENTALLY IMPORTANT comment from uf_insert_basis:
//            -- The 1st paragraph about the insert does not apply in this
//               script and is for informational purposes only.
//            -- The 2nd and 3rd paragraphs need to be committed to memory
//               or YOU WILL be cleaning up a mess.
//            ----------------------------------------------------------------
//            Due to potential overlap with the i_join_fields (i.e. one type of "STRING"
//            being an overriding subset of another "STRING" in that same "TYPE"), we must
//            insert individual transactions first (using the id field as a restriction
//            for subsequent inserts so we don't double-count, AND THEN summarize to the
//            higher level of detail).
//
//            The offshoot of these individual inserts by join, and the fact that we want
//            to allow for different joins within a "TYPE" is that certain "joins" may
//            end up going through the trueup logic when their cr_deriver_control records
//            have not changed.  For example, if a work order based record changes, then
//            even the work order||"RESOURCE" based records will end up in this table.
//            Since the selective inserts are simply a performance consideration, this is
//            OK, and the rounding threshold will ensure that we don't book inappropriate
//            trueup entries.
//
//            The offshoot of THIS (above paragraph) is that ultimately, the re-derivation
//            is going to potentially fire multiple times for a given "TYPE".  THUS, IT
//            IS CRITICAL THAT THE JOIN_FIELDS ARE ENTERED IN THE PROPER ORDER IN
//            CR_SAP_TRUEUP_CONTROL.
//            |
//            |--> SUBSETS MUST BE FIRST IN THE COMMA-SEPARATED LIST !!!
//                 E.G. work_order||"RESOURCE",work_order
//            ----------------------------------------------------------------
//            End of: The MONUMENTALLY IMPORTANT comment from uf_insert_basis:
//            ----------------------------------------------------------------
//
//            You would have to set up different derivation "TYPES" if the derived fields
//            are not the same.
//
//            It is a bit of overkill to call the setup audits every time through the
//            loop, but I will, just in case someone is monkeying around with
//            cr_deriver_control while this process runs.
//
//*****************************************************************************************
longlong j, deriver_rtn, v, comma_pos, at_rtn, counter
string  join_field, the_a_join_value, sqls, the_a_join_value2, virt_cdr, &
		  uf_deriver_virtual_third_arg, sort_of_like_third_arg, rc
boolean failed_audits



//  THIS IS THE OUTER LOOP OVER JOIN FIELDS.  Loop and call the deriver functions.
for j = 1 to upperbound(i_join_fields_array)

join_field = i_join_fields_array[j]

join_field = f_replace_string(join_field, "cr.", "", "all")
join_field = f_replace_string(join_field, "me.", "", "all") // This function is now
	// working against the detail stg table which will have the cr_derivation_rollup
	// field.  However, the join_field value from cr_sap_trueup_control will contain
	// something like cr.work_order||me.cr_derivation_rollup since uf_insert_original
	// works against cr_cost_repository.  The prefixing allows us to join in the ME
	// table in uf_insert_original and roll the CR-Summary transactions up to the
	// cr_derivation_rollup level of detail.  We will generally be stripping off
	// the cr. and me. in all subsequent functions that work against the detail
	// stg table.




//  SETUP:
//  Since "default" types do not apply in the trueup (we only trueup those transactions
//  that have official cr_deriver_control records), we don't need to call
//  uf_deriver_audits("Single Record For Default Type").


//  SETUP:
//  Run the audit to ensure no negative percents (would destroy normalization).
//////f_pp_msgs("Derivations: Auditing for negative percents at " + string(now()))
deriver_rtn = i_uo_deriver.uf_deriver_audits( &
	i_derivation_type, "Disallow Negative Percents")
if deriver_rtn <> 1 then
	f_pp_msgs("  ")
	f_pp_msgs("FAILED AUDIT: There are negative percentages in cr_deriver_control for " + &
		"derivation type: " + i_derivation_type)
	f_pp_msgs("The process cannot run in this state.")
	f_pp_msgs("  ")
	rollback;
	failed_audits = true
end if


//  SETUP:
//  This re-derivation is completely one-sided.  Thus, we don't need to call
//  uf_deriver_audits("Single Record For Offsets")


//  SETUP:
//  Scram if any of the audits failed.
if failed_audits then return -1


//  SETUP:
//  This re-derivation is completely one-sided.  Thus, we don't need to call
//  uf_deriver_create_offset()


//  TARGETS:
//  Call the custom function that will return the a_join we want to use in
//  uf_deriver.
the_a_join_value = ""
the_a_join_value = f_cr_sap_trueup_custom("a_join", join_field, i_stg_table)


//  TARGETS:
//  The "join value" needs to have the company restriction since we are in a
//  company loop.  I don't expect the custom function to try and handle this.
//  For Virtual Derivations: keep the cr_derivation_rollup values that are
//  defined as virutal out of the regular re-derive logic.
the_a_join_value = the_a_join_value + &
	'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "' " + &
	'and ' + i_stg_table + ".cr_derivation_rollup not in " + &
		"(select cr_derivation_rollup from cr_derivation_rollup " + &
		  "where virtual_derivation = 1) "


//  DMJ: 2/2/09:
//  At Entergy, performance was suffering on the call below.  However, SCE & Pepco
//  (amongst others) were OK, so I don't want to load up on a bunch of extra calls
//  to analyze_table() if it is not necessary (will just slow the other clients down).
//  Thus, I'm creating an extension call (separate from the original one) to call
//  analyze_table() in other strategic places.
at_rtn = f_cr_sap_trueup_analyze_table("uf_re_derive_before_1st_uf_deriver", "", i_stg_table)
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: calling analyze_table (uf_re_derive_before_1st_uf_deriver): " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  TARGETS:
//  Perform the split ... Since the source and target table is the same, we 
//  can just call uf_deriver.
deriver_rtn = i_uo_deriver.uf_deriver(i_derivation_type, i_stg_table, the_a_join_value)
if deriver_rtn <> 1 then
	f_pp_msgs("  ")
	f_pp_msgs("TARGETS ERROR: in uf_deriver: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  DMJ: 11/7/07:
//  At Pepco, performance was suffering on the virtual insert below.
f_pp_msgs("Calling analyze_table for " + i_stg_table + " at " + string(now()))
sqlca.analyze_table(i_stg_table)
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: calling analyze_table: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  DMJ: 11/7/07:
//  At Pepco, work orders with virtual cr_derivation_rollup records only would
//  create an OOB error in uf_insert_trueup.
//
//  The idea is that even the "virtual" cdr's like CIAC get a record in cr_deriver_control.
//  This triggers the code in uf_insert_original to insert the starting records for the WO
//  even though there will end up being no basis for RE_DERIVATION records (since there are
//  no non-virtual charges on this WO.  Once ORIGINAL records get inserted, the insert to
//  cdc_virtual gets nothing for this WO and the uf_insert_trueup logic is fooled into thinking
//  this work order has a Case C.  Of course this is not really a Case C that would end up 
//  having a DR and CR, so basically, a 1-sided entry is booked to the CIAC.  THIS CAN ONLY
//  HAPPEN TO VIRTUALS !  If non-virtuals get into the ORIGINAL transactions, it's because
//  they indeed have those CDC records and thus, a RE-DERIVATION will get booked for them.
//
//  This is probably more of a conversion problem, rather than an ongoing issue (unless of course
//  manual JE's are used to "create" something like CIAC).
//
//  The delete below completely wipes out any work orders that have ONLY virtual
//  CR Derivation Rollups in their transactions.
// ### 8026: JAK: 20110712:  This delete needs to be by company and exclude true-up final.
sqls = &
	"delete from " + i_stg_table + " where " + i_wo_field + " in ( " + &
		"select a." + i_wo_field + " " + &
		"from " + i_stg_table + " a, cr_derivation_rollup b " + &
		"where a.cr_derivation_rollup = b.cr_derivation_rollup " + &
		'and a."' + i_company_field + '" = ' + "'" + i_co + "' " + &  
		"and drilldown_key <> 'TRUEUP-FINAL' " + &
	"group by " + i_wo_field + " having sum(decode(b.virtual_derivation,1,0,1000)) = 0) " + &
	'and "' + i_company_field + '" = ' + "'" + i_co + "' " + &  
	"and drilldown_key <> 'TRUEUP-FINAL' "

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting virtual-only records: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


f_pp_msgs("Performing re-derivation (uf_deriver_virtual) at " + string(now()))

FOR v = 1 TO i_num_virt

virt_cdr = i_ds_virt.GetItemString(v, 1)

//  VIRTUAL DERIVATIONS:
//  Need to flip the 1/0 in the_a_join_value so we fire the virtual function
//  against the right values.  This is a bit of a shortcut and if the join string
//  above ever changes enough to conflict with the virtual derivations, we'll
//  have to completely rebuild the_a_join_value here.  We also add in the reference
//  to the virt_cdr variable in case they have more that one virtually derived
//  cr_derivation_rollup value (the whole point of this loop).  We also replace
//  cr_deriver_control. with cr_deriver_control_virtual.
the_a_join_value = f_replace_string(the_a_join_value, &
	"virtual_derivation = 1", "virtual_derivation = 0", "all")

the_a_join_value = the_a_join_value + " and cr_derivation_rollup = '" + virt_cdr + "' "

the_a_join_value = f_replace_string(the_a_join_value, &
	"cr_deriver_control.", "cr_deriver_control_virtual.", "all")


//  VIRTUAL DERIVATIONS: TARGETS:
//  Perform the split for virtual derivation rollups.
//  This is an example of what the arguments look like:
//     deriver_rtn = i_uo_deriver.uf_deriver_virtual('SAP Ferc Derivation', 'cr_accounts_payable_stg', &
//	      "work_order||'" + virt_cdr + "'", "ferc", &
//	      "cr_txn_type = 'TARGET' and " + &
//   	   "cr_derivation_rollup in " + &
//	      "(select cr_derivation_rollup from cr_derivation_rollup where basis_for_virtual_derivation = 1)", &
//	      "aa.work_order = a.work_order", &
//	      'SAP Ferc Derivation - Offset', &
//	      'cr_accounts_payable_stg.work_order||cr_accounts_payable_stg.cr_derivation_rollup = cr_deriver_control_virtual.string and ' + &
//	      "cr_accounts_payable_stg.cr_txn_type = 'SAP'")
//  Note: i_derived_ack_fields will always have a comma at the end.  Thus, the pos()
//  Note: the a_string arg (3rd arg) will almost always be the work order field concatenated with the
//        virt_cdr value (e.g. 'CIAC').  Force that here.  We will add an extension call above later
//        if we ever need a_string to be different.
//  Note: for the a_filter1 arg (5th arg) I'm thinking that the only records at this point in the STG
//        table with a null dd_key will be the results of uf_deriver above.  Those records are the
//        "current" answer for other derivation rollup types, so they contain the relative %'s that
//        I want in the virtual derivations.  This is the "cr_txn_type = 'TARGET' and " argument in
//        the example above, which would be correct if running in an interface.  This arg also includes
//        the basis where clause. ALSO: 5th ARG: we filter out any work orders where the sum(amount)
//        would go to 0.00 (divide-by-zero) protection for building cr_deriver_control_virtual.  This is
//        only any issue when we actually have records summing to 0.00.  If there are no records on a WO
//        (other than something like an up-front billing), then nothing will happen here since there will
//        be nothing in the basis for the insert to CDC_virtual (thus no div-by-0 error).
//  Note: the a_join1 arg (6th arg) will usually be a join on the aa. and a. work order field.  For now
//        this is forced.  We can add another extension call above if it ever needs to be something other
//        than the work order field.  Any extension would need to reference the aa and a table alias.
//  Note: we don't want the offsets to fire, so we pass a bogus argument in the a_type_offset arg
//        (the 7th arg).  This is the 'SAP Ferc Derivation - Offset' above, which would be correct
//        if running in an intefrace.
//  DMJ: 11/7/07: PERFORMANCE CHANGES:
//  1) i_wo_field + " not in " + &
//			"(select " + i_wo_field + " from " + i_stg_table + " where drilldown_key is null " + &
//			  "group by work_order having sum(amount) = 0)", &
//     to an in-minus construct.
//  DMJ: 2/2/09: "i_wo_field + "||'" + virt_cdr + "'"" is too hardcoded.  Added a variable and a call
//     to the extension to get the value.  We'll default to the hardcoded value if nothing is returned
//     by the function (for existing clients).
//  
setnull(uf_deriver_virtual_third_arg)

uf_deriver_virtual_third_arg = f_cr_sap_trueup_custom("uf_deriver_virtual_third_arg", virt_cdr + '///' + i_derivation_type, i_stg_table) // Note that
	// the a_addl_arg is the virt_cdr values (kind of important).

if isnull(uf_deriver_virtual_third_arg) or trim(uf_deriver_virtual_third_arg) = "" then
	uf_deriver_virtual_third_arg = i_wo_field + "||'" + virt_cdr + "'" // This was the 3rd
		// argument when it was hardcoded before 2/2/09.
end if



//  HARDCODED TEST FOR ENTERGY BILLABLE AFUDC:
//  Would need to consider the time_stamp logic from uf_insert_original --- perhaps determine the columns that
//  are important and add a where clause like: and business_unit||project in (select business_unit||project
//  from cr_sap_trueup_stg).
//
//  This piece from uf_insert_original is a little curious: sqls = sqls + "and cr.id not in (select id from " + i_stg_table + ") "
//  That prevents duplicate inserts when there are multiple derivation types.  I probably need to take this into account.
//  *** After looking at the code again, the "id not in" could not possibly be doing anything (it's actually the join to CDC
//      that is the limiting factor for each type.  I still need to take this issue into account somehow ***
//
//  If needed, I could get this to work at Entergy by making a custom function call and inserting their appropriate records.
//  I could then worry about base-izing it later, once I have additional examples from other clients.  Prior clients may not have
//  hit this (OH's were included in the trueup @ Pepco and AFUDC may not have been billable ... SCE had a strage methodology for
//  CIAC anyway by charging it all to a liability until unitization).
rc = f_cr_sap_trueup_custom("uf_re_derive_pre_virtual", "", i_stg_table)

if rc = "ERROR" then
	rollback;
	return -1
end if


//insert into cr_sap_trueup_stg (
//	id,providing_area,target_area,"RESOURCE",task,sid,work_order,cost_code,
//	location_id,company,ferc,future_use_1,future_use_2,dr_cr_id,ledger_sign,quantity,amount,
//	month_number,month_period,gl_journal_category, amount_type, drilldown_key, cr_derivation_rollup) (
//	select crdetail.nextval,providing_area,target_area,"RESOURCE",task,sid,work_order,cost_code,
//	location_id,company,ferc,future_use_1,future_use_2,dr_cr_id,ledger_sign,quantity,amount,
//	200611 month_number,0 month_period,'AX' gl_journal_category, amount_type, NULL drilldown_key, 'NON-MTL'
//	from cr_cost_repository where work_order = 'B1350' and "RESOURCE" = 'AF');

// ### 8422: JAK: 2012-06-12: When filtering work orders, need to look for work orders where the basis_for_virtual_derivation 
//	has a non-zero sum.
deriver_rtn = i_uo_deriver.uf_deriver_virtual(i_derivation_type, i_stg_table, &
	uf_deriver_virtual_third_arg, left(i_derived_ack_fields, len(i_derived_ack_fields) - 1), &
	"drilldown_key is null and " + &
	"cr_derivation_rollup in " + &
	   "(select cr_derivation_rollup from cr_derivation_rollup where basis_for_virtual_derivation = 1) and " + &
	i_wo_field + " in " + &
		"(select " + i_wo_field + " from " + i_stg_table + " where drilldown_key is null " + &
		" and cr_derivation_rollup in (select cr_derivation_rollup from cr_derivation_rollup where basis_for_virtual_derivation = 1) " + &
		  "group by " + i_wo_field + " having sum(amount) <> 0)", &
	'aa."' + upper(i_wo_field) + '" = a."' + upper(i_wo_field) + '"', &
	'a bogus argument', &
	the_a_join_value)

if deriver_rtn <> 1 then
	f_pp_msgs("  ")
	f_pp_msgs("TARGETS ERROR: in uf_deriver_virtual: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


rc = f_cr_sap_trueup_custom("uf_re_derive_post_virtual", "", i_stg_table)

if rc = "ERROR" then
	rollback;
	return -1
end if
//delete from cr_sap_trueup_stg where work_order = 'B1350' and "RESOURCE" = 'AF';
//  END OF:
//  HARDCODED TEST FOR ENTERGY BILLABLE AFUDC:




//  DMJ: 2/5/08: If we have a work order with CIAC only - for example - and there are no "non-virtual"
//  charges, or the "non-virtual" charges sum to 0.00 ... then cr_deriver_control_virtual will have no
//  records for this "STRING".  Thus, no RE-DERIVATION records for this "STRING".  That will end up
//  triggering case C in the trueup (ORIGINAL records with no RE-DERIVATION) and will end up booking
//  an erroneous trueup, which by the way, will throw this "STRING" out of balance, triggering the
//  balance by company error.
//
//  Solution: if a "STRING" has no records in CDC_VIRT, set it's ORIGINAL RECORD(s) to
//  'ORIGINAL RECORD-NO VIRTUAL BASIS'.  That will remove it from the trueup since there will be
//  no RE-DERIVATION records AND no ORIGINAL RECORD records (bypassing cases A-C in the trueup).
//
//  SQL of the form:
//     update cr_sap_trueup_stg set drilldown_key = drilldown_key||'-NO VIRTUAL BASIS'
//      where drilldown_key = 'ORIGINAL RECORD'
//        and not exists (
//           select 1 from cr_deriver_control_virtual cdc 
//            where cdc."STRING" = cr_sap_trueup_stg.work_order||cr_derivation_rollup
//              and cr_sap_trueup_stg.cr_derivation_rollup = 'CIAC')
//        and cr_derivation_rollup = 'CIAC';
//
//  DMJ: 2/4/09: Needed something very similar to "third arg" above.  Basically, WO and CDR
//  were hardcoded before.  At Entergy, this did not work since BU was also in the string.
//  The original where clause looked like:
//     'where cdc."STRING" = ' + i_stg_table + '.' + i_wo_field + '||cr_derivation_rollup ' + &
//
sort_of_like_third_arg = f_Cr_sap_trueup_custom("sort_of_like_third_arg", virt_cdr + "///" + i_derivation_type, i_stg_table) // Note that
	// the a_addl_arg is the virt_cdr values (kind of important).

if isnull(sort_of_like_third_arg) or trim(sort_of_like_third_arg) = "" then
	sort_of_like_third_arg = i_stg_table + '.' + i_wo_field + '||cr_derivation_rollup ' // This was the 3rd
		// argument when it was hardcoded before 2/2/09.
end if

sqls = &
	"update " + i_stg_table + " set drilldown_key = drilldown_key||'-NO VIRTUAL BASIS' " + &
	 "where drilldown_key = 'ORIGINAL RECORD' " + &
	   "and not exists ( " + &
         "select 1 from cr_deriver_control_virtual cdc " + &
			 'where cdc."STRING" = ' + sort_of_like_third_arg + " " + &
			   "and " + i_stg_table + ".cr_derivation_rollup = '" + virt_cdr + "') " + &
	   "and cr_derivation_rollup = '" + virt_cdr + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR IN VIRTUAL LOOP: Updating ORIGINAL RECORD(s) with no CDC_VIRT record(s): " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


NEXT  //  FOR v = 1 TO i_num_virt ...


//  OFFSETS:
//  Not applicable.  This re-derivation is completely one-sided.


//  DEFAULTS TARGETS:
//  DEFAULTS OFFSETS:
//  Not applicable.  The "default" types do not apply in the trueup (we only
//  trueup those transactions that have official cr_deriver_control records).


//  CLEANUP:
//  Update the drilldown_key on the re-derivation records with 'RE-DERIVATION'.
sqls = &
	"update " + i_stg_table + " set drilldown_key = 'RE-DERIVATION' " + &
	 "where drilldown_key is null " + &
	   'and "' + i_company_field + '" = ' + "'" + i_co + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + " drilldown_key to 'RE-DERIVATION' " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  CLEANUP:
//  Call the custom function that will return the a_join we want to use in
//  uf_deriver.
the_a_join_value  = ""
the_a_join_value2 = ""
the_a_join_value  = f_cr_sap_trueup_custom("a_join", join_field + ':RE-DERIVE1', i_stg_table)
the_a_join_value2 = f_cr_sap_trueup_custom("a_join", join_field + ':RE-DERIVE2', i_stg_table)


//  CLEANUP:
//  The "join values" needs to have the company restriction since we are in a
//  company loop.  I don't expect the custom function to try and handle this.
//  DMJ: 2/5/08: not clear why I was limiting the rounding check to non-virtual CDR's.
//  This created an OOB condition at Pepco.
//  DMJ: 2/5/08: also added the exists clause or else the rounding function will throw an
//  error on any virtuals that get no RE-DERIVATION records.  I should only need this clause
//  for the top of the minus SQL.
//the_a_join_value = the_a_join_value + &
//	'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "' " + &
//	'and ' + i_stg_table + ".cr_derivation_rollup not in " + &
//		"(select cr_derivation_rollup from cr_derivation_rollup " + &
//		  "where virtual_derivation = 1) "

//the_a_join_value2 = the_a_join_value2 + &
//	'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "' " + &
//	'and ' + i_stg_table + ".cr_derivation_rollup not in " + &
//		"(select cr_derivation_rollup from cr_derivation_rollup " + &
//		  "where virtual_derivation = 1) "

//  CLEANUP:
//  Fix any rounding error.
f_pp_msgs("Derivations: Adjusting for rounding at " + string(now()))
// ### 6853: JAK: Use the "ids" rounding if possible
select count(*) into :counter
	from all_tab_columns
	where table_name = upper(trim(:i_stg_table))
	and owner = 'PWRPLANT'
	and column_name = 
		(select upper(trim(control_value))
		from cr_system_control
		where upper(control_name) = 'DERIVATIONS - ID ATTRIBUTE');

if counter > 0 then
	// Use id rounding
	the_a_join_value = the_a_join_value + &
		" and " + i_stg_table + ".drilldown_key = 'SUMMARIZED RECORD' " + &
		"and exists (select 1 from " + i_stg_table + " a where a.cr_derivation_id = " + i_stg_table + ".id)" + &
		'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "' "
	
	the_a_join_value2 = &
		i_stg_table + ".drilldown_key = 'RE-DERIVATION' " + &
		"and exists (select 1 from " + i_stg_table + " a where a.id = " + i_stg_table + ".cr_derivation_id)" + &
		'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "' "
	
	deriver_rtn = i_uo_deriver.uf_deriver_rounding_error_ids( &
		i_derivation_type, i_stg_table, &
		the_a_join_value, &
		the_a_join_value2, &
		the_a_join_value2, &
		join_field)
else
	// Use original method
	
	the_a_join_value = the_a_join_value + &
		'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "' " + &
		"and exists (select 1 from " + i_stg_table + " r " + &
			"where r." + i_wo_field + " = " + i_stg_table + "." + i_wo_field + &
			 " and r.cr_derivation_rollup = " + i_stg_table + ".cr_derivation_rollup " + &
			 " and r.drilldown_key = 'RE-DERIVATION') "
	
	the_a_join_value2 = the_a_join_value2 + &
		'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "' "
	
	deriver_rtn = i_uo_deriver.uf_deriver_rounding_error( &
		i_derivation_type, i_stg_table, &
		the_a_join_value, &
		the_a_join_value2, &
		i_stg_table + ".drilldown_key = 'RE-DERIVATION' " + &
		'and ' + i_stg_table + '."' + i_company_field + '" = ' + "'" + i_co + "'", &
		join_field)
end if

if deriver_rtn <> 1 then
	//  No msgs ... uf_deriver_rounding_error handles its own.
	rollback;
	return -1
end if

//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
f_pp_msgs("Splitting quantities at " + string(now()))
deriver_rtn = i_uo_deriver.uf_split_quantities_trueup(i_stg_table)
if deriver_rtn <> 1 then
	//  No msgs ... uf_deriver_rounding_error handles its own.
	rollback;
	return -1
end if

next  //  for j = 1 to upperbound(i_join_fields_array) ...


commit;


return 1
end function

public function longlong uf_insert_trueup ();//*****************************************************************************************
//
//  Function  :  uf_insert_trueup
//
//  Description  :  Inserts all "trueup" transactions containing the difference between the
//                  current balance and the re-derivation.  These transactions DO HAVE THE
//                  "derived" values (e.g. they do contain the ferc account if that is one
//                  of the derived fields).  The transactions generated in uf_insert_basis
//                  DO NOT contain the derived fields.
//
//  Notes  :  This script is called one time from OUTSIDE THE "TYPE" loop.  We are still
//            in the company loop however.
//            
//            The transactions generated by this function will contain the "join field"
//            values in the cr_txn_type.  Thus, the cr_txn_type on the SAP Trueup CR
//            Detail table will need to be larger than on all the other detail tables.
//            It only needs to be 10 to support derivations in interfaces.  You can choose
//            however to make the field bigger in all tables (e.g. character 35).
//
//            Right after this function call, the caller will initiate a "TYPE" loop and
//            call uf_insert_trueup_final to delete the TRUEUP-DELETE records that do not
//            pass the threshold limit for the "TYPE" and use the remaining records as
//            the basis for the TRUEUP-FINAL records.
//
//            Case A = "In both original txns and re-derivation txns" (inner join)
//            Case B = "In re-derivation txns not in original txns"   (outer join (+))
//            Case C = "In original txns not in re-derivation txns"   ((+) outer join)
//
//*****************************************************************************************
longlong num_cols, i, counter
string sqls, col, re_derivation_from, txns_from, re_der_from_group_by, txns_from_group_by, &
		 where_sqls, where_sqls_case_b, sqls_case_b, orig_sqls, where_sqls_case_c, sqls_case_c



//*****************************************************
//
//  Insert the "raw" TRUEUP records ... not taking
//  into account the threshold limit.
//
//*****************************************************



//  There is an instance DS against all_tab_columns so we can build the insert.  Since we
//  want to summarize life-to-date balances, we are simply going to get the $$$ from
//  cr_cost_repository.
sqls = &
	"select upper(column_name), column_id from all_tab_columns " + &
	 "where table_name = 'CR_COST_REPOSITORY' and owner = 'PWRPLANT'"
i_ds_all_tab_cols.Reset()
i_ds_all_tab_cols.SetSQLSelect(sqls)
num_cols = i_ds_all_tab_cols.RETRIEVE()
i_ds_all_tab_cols.SetSort("column_id a")
i_ds_all_tab_cols.Sort()



// ** REMOVED THE LOOP OVER UPPERBOUND(I_JOIN_FIELDS_ARRAY) SINCE WE SIMPLY WANT TO
// ** INSERT THE TRUEUP RECORDS FOR ALL "TYPES" THAT CURRENTLY HAVE RECORDS IN THE
// ** STG TABLE.  ALL OF THOSE "TYPES" ALREADY HAVE "ORIGINAL" AND "RE-DERIVATION"
// ** RECORDS IN THE STG TABLE, SO THE "TRUEUP" IS A SINGLE BIG-BANG.



//  Start the insert command.
sqls = "insert into " + i_stg_table + " ( "


//  Build the columns for the insert portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
	end choose
	
	sqls = sqls + '"' + col + '",'
		
next


//  Trim the trailing comma.  Not on the "froms" due to the sum(amount).
sqls = left(sqls, len(sqls) - 1)


//  Close the "insert" and start the "select".
sqls = sqls + ", cr_txn_type"

if i_rate_field <> "***" then
	sqls = sqls + "," + i_rate_field
end if

sqls = sqls + ") (select "

re_derivation_from = " (select "
txns_from          = " (select "

re_der_from_group_by = ""
txns_from_group_by   = ""
where_sqls           = " where "
where_sqls_case_b    = " where "
where_sqls_case_c    = " where "


//  Loop again and build the select portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  *** 
	//  *** REMOVED THE DERIVED_POS CODE HERE THAT CONTROLS THE SUMMARIZATION TO REMOVE
	//  *** THE DERIVED FIELDS IN UF_INSERT_ORIGINAL.
	//  ***
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
		case "ID"
			//  This is the outer select which is not summarized, so add in crdetail.nextval.
			sqls = sqls + "crdetail.nextval,"
			continue
		case "DR_CR_ID" // We'll update this later.
			sqls = sqls + "1,"
			continue
		case "QUANTITY" // We don't split quantities in the first place.
			//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
			sqls = sqls + "nvl(re_derivationzzz.quantity, 0) - nvl(txns.quantity, 0),"
//			sqls = sqls + "0,"
			continue
		case "AMOUNT"
			//  The diff.  The nvl's are for Cases B and C (since we are sharing the
			//  original sqls).  The "zzz" is to avoid the f_replace_string on "re_derivation."
			//  when we are building the SQL for case C below.  Once we've performed the
			//  f_replace_string, we will call another f_replace_string to 86 the "zzz".
			sqls = sqls + "nvl(re_derivationzzz.amount, 0) - nvl(txns.amount, 0),"
			continue
		case "MONTH_NUMBER"
			sqls = sqls + string(i_min_open_month) + ","
			continue
		case "MONTH_PERIOD"
			sqls = sqls + "0,"
			continue
		case "DRILLDOWN_KEY"
			//  Use the appropriate value here.
			sqls = sqls + "'TRUEUP',"
			continue
	end choose
		
	sqls = sqls + 're_derivation."' + col + '",'
		
	//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
	//  Amount was needed for the outer select above, but needs to be omitted from the SQL
	//  below since "sum(amount)" is hardcoded later.
	if col = "AMOUNT" or col = "QUANTITY" then
		continue
	end if	
	
	re_derivation_from = re_derivation_from + '"' + col + '",'
	txns_from          = txns_from          + '"' + col + '",'
	re_der_from_group_by = re_der_from_group_by + '"' + col + '",'
	txns_from_group_by   = txns_from_group_by   + '"' + col + '",'
	
	where_sqls = where_sqls + "re_derivation." + '"' + col + '" = '
	where_sqls = where_sqls + "txns."          + '"' + col + '" and '
	
	where_sqls_case_b = where_sqls_case_b + "re_derivation." + '"' + col + '" = '
	where_sqls_case_b = where_sqls_case_b + "txns."          + '"' + col + '" (+) and '
	
	where_sqls_case_c = where_sqls_case_c + "re_derivation." + '"' + col + '" (+) = '
	where_sqls_case_c = where_sqls_case_c + "txns."          + '"' + col + '" and '
	
next


//  Trim the trailing comma.  Not on the "froms" due to the sum(amount).  Trim the
//  trailing "and " on where_sqls
sqls = left(sqls, len(sqls) - 1)

re_der_from_group_by = left(re_der_from_group_by, len(re_der_from_group_by) - 1)
txns_from_group_by   = left(txns_from_group_by,   len(txns_from_group_by)   - 1)

where_sqls        = left(where_sqls,        len(where_sqls)        - 4)
where_sqls_case_b = left(where_sqls_case_b, len(where_sqls_case_b) - 4)
where_sqls_case_c = left(where_sqls_case_c, len(where_sqls_case_c) - 4)


//  From and where.
sqls = sqls + ", re_derivation.cr_txn_type"
if i_rate_field <> "***" then
	sqls = sqls + ", re_derivationzzz." + i_rate_field
end if
sqls = sqls + " from "


//  Add to the "froms".
//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
if i_rate_field <> "***" then
	re_derivation_from = re_derivation_from + &
		"cr_txn_type, " + i_rate_field + ", sum(quantity) quantity, sum(amount) amount from " + i_stg_table + " where drilldown_key = 'RE-DERIVATION' " + &
		"group by " + re_der_from_group_by + ", cr_txn_type, " + i_rate_field + ") re_derivation,"
else
	re_derivation_from = re_derivation_from + &
		"cr_txn_type, sum(quantity) quantity, sum(amount) amount from " + i_stg_table + " where drilldown_key = 'RE-DERIVATION' " + &
		"group by " + re_der_from_group_by + ", cr_txn_type) re_derivation,"
end if

txns_from          = txns_from + &
	"cr_txn_type, sum(quantity) quantity, sum(amount) amount from " + i_stg_table + " where drilldown_key = 'ORIGINAL RECORD' " + &
	"group by " + txns_from_group_by   + ", cr_txn_type) txns "


//  Restrict by company.
//
//  Note that Case C has addl where clause to filter out ORIGINAL
//  records that hit the clearing account (net 0 records actually).  They creep in due
//  to the outer join, and the insert would end up booking them as 0.00 transactions
//  for TRUEUP-FINAL.
where_sqls = where_sqls + &
	' and re_derivation."' + i_company_field + '" = ' + "'" + i_co + "'"

where_sqls_case_b = where_sqls_case_b + &
	' and re_derivation."' + i_company_field + '" = ' + "'" + i_co + "'" + &
	" and txns.amount is null"

//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
where_sqls_case_c = where_sqls_case_c + &
	' and txns."' + i_company_field + '" = ' + "'" + i_co + "'" + &
	" and re_derivation.amount is null" + &
	" and (nvl(re_derivation.amount, 0) - nvl(txns.amount, 0) <> 0 or nvl(re_derivation.quantity, 0) - nvl(txns.quantity, 0) <> 0)"


//  ** Removed the "Restrict by month_number".  This basically irrelevant and will bomb
//  ** the SQL.


//  ** Restrict by join_fields has been removed from this function.  This function is
//  ** called from outside the "TYPE" loop, so the "TYPE"/join_field where clause is
//  ** irrelevant.


//  Build the entire insert.  All 3 cases need to be built.  Save the original sqls
//  value 1st since it can be used for the start of all 3 cases.  Case C needs to
//  have the "re_derivation." changed to "txns." (the prefix on the select columns).
//
//  To avoid messing up the diff formula with the f_replace_string in case C, we
//  hardcoded re_derivationzzz.amount above.  All 3 SQL's need that changed.
orig_sqls   = sqls
sqls        = sqls      + re_derivation_from + txns_from + where_sqls        + ")"
sqls_case_b = orig_sqls + re_derivation_from + txns_from + where_sqls_case_b + ")"
sqls_case_c = f_replace_string(orig_sqls, "re_derivation.", "txns.", "all") + &
	re_derivation_from + txns_from + where_sqls_case_c + ")"

sqls        = f_replace_string(sqls,        "re_derivationzzz.", "re_derivation.", "all")
sqls_case_b = f_replace_string(sqls_case_b, "re_derivationzzz.", "re_derivation.", "all")
sqls_case_c = f_replace_string(sqls_case_c, "re_derivationzzz.", "re_derivation.", "all")


//  ** REMOVED: SUMMARIZE:
//  **          ----------
//  ** This section is not needed since the TRUEUP records are effectively summarized
//  ** in the original insert.


//  ** REMOVED: "disable the PK."  The PK is correct on the original insert of the
//  **          TRUEUP records.


if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_truep: inserting TRUEUP records (CASE A) into " + i_stg_table )
	f_pp_msgs("***DEBUG*** sqls = " + sqls)
end if

//  Insert the TRUEUP records: CASE A.
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting TRUEUP records (CASE A) into " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	//  ** REMOVED: "Need to re-enable PK to avoid an error the next time through."  The
	//  **          PK is correct from the original insert.
	return -1
end if


if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_truep: inserting TRUEUP records (CASE B) into " + i_stg_table )
	f_pp_msgs("***DEBUG*** sqls = " + sqls_case_b)
end if

//  Insert the TRUEUP records: CASE B.
execute immediate :sqls_case_b;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting TRUEUP records (CASE B) into " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	//  ** REMOVED: "Need to re-enable PK to avoid an error the next time through."  The
	//  **          PK is correct from the original insert.
	return -1
end if

if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_truep: inserting TRUEUP records (CASE C) into " + i_stg_table )
	f_pp_msgs("***DEBUG*** sqls = " + sqls_case_c)
end if

//  Insert the TRUEUP records: CASE C.
execute immediate :sqls_case_c;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting TRUEUP records (CASE C) into " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	//  ** REMOVED: "Need to re-enable PK to avoid an error the next time through."  The
	//  **          PK is correct from the original insert.
	return -1
end if


//  ** REMOVED: "Toss the "NEEDS SUMMARIZATION" records".  There were no such records
//              inserted by this function.


//  ** REMOVED: "Update PK and drilldown_key".  The PK and drilldown_key are correct on
//              the original insert of the TRUEUP records.


//  ** REMOVED: "Enable the PK."  The PK is correct on the original insert of the
//  **          TRUEUP records.


//  Commit here before the next steps in the trueup.
commit;



//*****************************************************
//
//  At this point, TRUEUP records have been inserted
//  into the STG table ... with no regard to the
//  threshold limit.
//
//*****************************************************



//  Right away, we had better balance by company.  If we've made a mistake, now is a good
//  time to get lost and let someone troubleshoot the problem.
sqls = &
	"select count(*) from (" + &
		'select "' + i_company_field + '", sum(amount) from ' + i_stg_table + " " + &
		 "where drilldown_key = 'TRUEUP' " + &
		 'group by "' + i_company_field + '" having sum(amount) <> 0)'

i_ds_counter.Reset()
i_ds_counter.SetSQLSelect(sqls)
i_ds_counter.RETRIEVE()

if i_ds_counter.RowCount() > 0 then
	counter = i_ds_counter.GetItemNumber(1, 1)
	if counter <> 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: balancing by company (1): TRUEUP records are out of balance.")
		f_pp_msgs("  ")
		return -1
	end if
else
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: balancing by company (1): i_ds_counter.RowCount() <= 0")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	return -1
end if


// JAK:  The TRUEUP-DELETE method never panned out and a different method is used now...these
//		records just waste time...

////*****************************************************
////
////  We will now delete some records based on the
////  threshold limit.  If any "STRING" values have an
////  amount outside the threshold, then all the records
////  for those "STRINGS" will be kept.  All others
////  will be deleted.  The leftover records labelled
////  TRUEUP-DELETE will then be the basis for our
////  TRUEUP-FINAL insert.  We do this to avoid hitting
////  cr_deriver_control excessively in where clauses.
////
////  This is just the insert of the TRUEUP-DELETE
////  records.  The delete is in uf_insert_trueup_final.
////
////*****************************************************
//
//
//// ** IT IS NOT CLEAR IF I SHOULD DELETE ALL OTHER RECORDS FROM THE STG TABLE
//// ** EXCEPT FOR THE 'TRUEUP' RECORDS ... FOR PERFORMANCE.
//
//
////  Start the insert command.
//sqls = "insert into " + i_stg_table + " ( "
//
//
////  Build the columns for the insert portion.
//for i = 1 to num_cols
//	
//	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
//	
//	choose case col
//		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
//			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
//			//  Leave these out of the sqls.  They will force us into a lower level of
//			//  detail than desired.
//			continue
//	end choose
//	
//	sqls = sqls + '"' + col + '",'
//		
//next
//
//
////  Trim the trailing comma.  Not on the "froms" due to the sum(amount).
//sqls = left(sqls, len(sqls) - 1)
//
//
////  Close the "insert" and start the "select".
//sqls = sqls + ", cr_txn_type"
//if i_rate_field <> "***" then
//	sqls = sqls + ", " + i_rate_field
//end if
//sqls = sqls + ") (select "
//
//
////  Loop again and build the select portion.
//for i = 1 to num_cols
//	
//	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
//	
//	//  *** 
//	//  *** REMOVED THE DERIVED_POS CODE HERE THAT CONTROLS THE SUMMARIZATION TO REMOVE
//	//  *** THE DERIVED FIELDS IN UF_INSERT_ORIGINAL.
//	//  ***
//	
//	choose case col
//		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
//			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
//			//  Leave these out of the sqls.  They will force us into a lower level of
//			//  detail than desired.
//			continue
//		case "ID"
//			//  Use negative id here.  It is part of the where clause as we loop over types.
//			//  The sequence will be applied after the insert.
//			sqls = sqls + "-id,"
//			continue
//		case "DR_CR_ID" // We'll update this later.
//			sqls = sqls + "1,"
//			continue
//		case "QUANTITY" // We don't split quantities in the first place.
//			sqls = sqls + "0,"
//			continue
//		case "AMOUNT"
//			//  Nothing.  Take the amount as-is.
//		case "MONTH_NUMBER"
//			//  Nothing.  Take the amount as-is.
//		case "MONTH_PERIOD"
//			sqls = sqls + "0,"
//			continue
//		case "DRILLDOWN_KEY"
//			//  Using this as a helper field.  This can be the same as uf_insert_basis since we
//			//  will overwrite it below.
//			sqls = sqls + "'TRUEUP-DELETE',"
//			continue
//	end choose
//		
//	sqls = sqls + '"' + col + '",'
//		
//next
//
//
////  Trim the trailing comma.  Not on the "froms" due to the sum(amount).  Trim the
////  trailing "and " on where_sqls
//sqls = left(sqls, len(sqls) - 1)
//
//
////  From and where.
//sqls = sqls + ", cr_txn_type"
//if i_rate_field <> "***" then
//	sqls = sqls + ", " + i_rate_field
//end if
//sqls = sqls + " from " + i_stg_table
//
//sqls = sqls + " where drilldown_key = 'TRUEUP' "
//
//sqls = sqls + ' and "' + i_company_field + '" = ' + "'" + i_co + "'"
//
//sqls = sqls + ")"
//
//
//if g_debug = "YES" then
//	f_pp_msgs("*** uf_insert_truep: inserting TRUEUP-DELETE records into " + i_stg_table )
//	f_pp_msgs("***DEBUG*** sqls = " + sqls)
//end if
//
////  Insert the TRUEUP-DELETE records.
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: inserting TRUEUP-DELETE records into " + i_stg_table + ": " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


//  Commit here for troubleshooting.
commit;


return 1

end function

public function longlong uf_insert_trueup_final ();//*****************************************************************************************
//
//  Function  :  uf_insert_trueup_final
//
//  Description  :  Deletes all the TRUEUP-DELETE records that do not pass the threshold
//                  limit for the "TYPE".  Then uses the remaining records as the basis
//                  for the TRUEUP-FINAL records.
//
//  Notes  :  This script is called one time from OUTSIDE THE "TYPE" loop.  We are still
//            in the company loop however.
//            
//*****************************************************************************************
longlong i, num_rows, num_cols, c, counter
string sqls, cr_txn_type, col
decimal {2} t_limit, t_limit_negative


//  SETUP:
//  There is an instance DS against all_tab_columns so we can build the insert.  Since we
//  want to summarize life-to-date balances, we are simply going to get the $$$ from
//  cr_cost_repository.
sqls = &
	"select upper(column_name), column_id from all_tab_columns " + &
	 "where table_name = 'CR_COST_REPOSITORY' and owner = 'PWRPLANT'"
i_ds_all_tab_cols.Reset()
i_ds_all_tab_cols.SetSQLSelect(sqls)
num_cols = i_ds_all_tab_cols.RETRIEVE()
i_ds_all_tab_cols.SetSort("column_id a")
i_ds_all_tab_cols.Sort()


//  Get the threshold limit.
t_limit = 0
t_limit_negative = 0
select threshold_limit into :t_limit from cr_sap_trueup_control
 where "TYPE" = :i_derivation_type;
if isnull(t_limit) then t_limit = 0

if t_limit = 0 then
	t_limit_negative = 0
else
	t_limit_negative = -t_limit
end if


//  Start the insert command.
sqls = "insert into " + i_stg_table + " ( "


//  Build the columns for the insert portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
	end choose
	
	sqls += '"' + col + '",'
		
next
	

//  Trim the trailing comma.  Not on the "froms" due to the sum(amount).
sqls = left(sqls, len(sqls) - 1)


//  Close the "insert" and start the "select".
sqls += ", cr_txn_type"
if i_rate_field <> "***" then
	sqls += ", " + i_rate_field
end if
sqls += ") (select "

	
//  Loop again and build the select portion.
for i = 1 to num_cols
	
	col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
	
	//  *** 
	//  *** REMOVED THE DERIVED_POS CODE HERE THAT CONTROLS THE SUMMARIZATION TO REMOVE
	//  *** THE DERIVED FIELDS IN UF_INSERT_ORIGINAL.
	//  ***
	
	choose case col
		case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
			  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
			//  Leave these out of the sqls.  They will force us into a lower level of
			//  detail than desired.
			continue
		case "ID"
			//  This is the outer select which is not summarized, so add in crdetail.nextval.
			sqls += "crdetail.nextval,"
			continue
		case "DR_CR_ID" // We'll update this later.
			sqls += "1,"
			continue
		case "QUANTITY" // We don't split quantities in the first place.
			//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
//			sqls += "0,"
//			continue
		case "AMOUNT"
			//  Allow this to go through.  The amount is the trueup amount.
		case "MONTH_NUMBER"
			sqls += string(i_min_open_month) + ","
			continue
		case "MONTH_PERIOD"
			sqls += "0,"
			continue
		case "DRILLDOWN_KEY"
			//  Use the appropriate value here.
			sqls += "'TRUEUP-FINAL',"
			continue
	end choose
		
	sqls += '"' + col + '",'
	
next


//  Trim the trailing comma.  Not on the "froms" due to the sum(amount).  Trim the
//  trailing "and " on where_sqls
sqls = left(sqls, len(sqls) - 1)


//  From and where.
sqls += ", cr_txn_type"
if i_rate_field <> "***" then
	sqls += ", " + i_rate_field
end if
sqls += " from " + i_stg_table + " cr  "
sqls += "where drilldown_key = 'TRUEUP' "
sqls += 'and "' + i_company_field + '" = ' + "'" + i_co + "'"
sqls += ")"

if isnull(sqls) then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: sqls is NULL in uf_insert_trueup_final (2) c=" + string(c) + " !" + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

if g_debug = "YES" then
	f_pp_msgs("*** uf_insert_trueup_final (2):")
	f_pp_msgs("***DEBUG*** sqls = " + sqls)
end if
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting TRUEUP-FINAL records into " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  Apply the threshold limit.
//  Note that the inner SQL is a "keep these" list of cr_txn_types.
if t_limit > 0 then
	f_pp_msgs("Deleting based on threshold limit at " + string(now()))
	sqls = &
		"delete from " + i_stg_table + " cr " + &
		 "where (cr_txn_type) in " + &
		 		"(select cr_txn_type from " + &
					"(select " + i_derived_ack_fields + " cr_txn_type, sum(amount) amount " + &
					"from " + i_stg_table + " z " + &
					"where drilldown_key = 'TRUEUP-FINAL' " + &
					'and "' + i_company_field + '" = ' + "'" + i_co + "' " + &
					"group by " + i_derived_ack_fields + " cr_txn_type) " + &
				"group by cr_txn_type " + &
				"having max(abs(amount)) <= " + string(t_limit) + ") " + &
			"and drilldown_key = 'TRUEUP-FINAL' " + &
			'and "' + i_company_field + '" = ' + "'" + i_co + "'"
			
//	sqls = &
//		"delete from " + i_stg_table + " cr " + &
//		 "where (cr_txn_type) not in " + &
//				"(select cr_txn_type from " + i_stg_table + " z " + &
//				 "where drilldown_key = 'TRUEUP-FINAL' " + &
//					'and "' + i_company_field + '" = ' + "'" + i_co + "' " + &
//				 "group by " + i_derived_ack_fields + " cr_txn_type " + &
//				"having sum(amount) not between " + string(t_limit_negative) + " and " + string(t_limit) + ") " + &
//			"and drilldown_key = 'TRUEUP-FINAL' " + &
//			'and "' + i_company_field + '" = ' + "'" + i_co + "'"
	
	if isnull(sqls) then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: sqls is NULL in uf_insert_trueup_final (1) !" + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if

	if g_debug = "YES" then
		f_pp_msgs("*** uf_insert_trueup_final: t_limit:")
		f_pp_msgs("***DEBUG*** sqls = " + sqls)
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: deleting TRUEUP-FINAL records for threshold_limit: " + &
			sqlca.SQLErrText)
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if

//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
//  Since the threshold check is at the cr_derivation_rollup level of detail, you could get
//  0.00 records if some cost elements within the rollup had no diffs.  Delete those here.
sqls = "delete from " + i_stg_table + " where quantity = 0 and amount = 0 and drilldown_key = 'TRUEUP-FINAL' "

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting 0.00 TRUEUP-FINAL records: " + &
		sqlca.SQLErrText)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  Commit here before the next steps in the trueup.
commit;


//  Right away, we had better balance by company.  If we've made a mistake, now is a good
//  time to get lost and let someone troubleshoot the problem.
sqls = &
	"select count(*) from (" + &
		'select "' + i_company_field + '", sum(amount) from ' + i_stg_table + " " + &
		 "where drilldown_key = 'TRUEUP-FINAL' " + &
		 'group by "' + i_company_field + '" having sum(amount) <> 0)'

i_ds_counter.Reset()
i_ds_counter.SetSQLSelect(sqls)
i_ds_counter.RETRIEVE()

if i_ds_counter.RowCount() > 0 then
	counter = i_ds_counter.GetItemNumber(1, 1)
	if counter <> 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: balancing by company (2): TRUEUP-FINAL records are out of balance.")
		f_pp_msgs("  ")
		return -1
	end if
else
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: balancing by company (2): i_ds_counter.RowCount() <= 0")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	return -1
end if


return 1




////*****************************************************************************************
////
////  Function  :  uf_insert_trueup_final
////
////  Description  :  Deletes all the TRUEUP-DELETE records that do not pass the threshold
////                  limit for the "TYPE".  Then uses the remaining records as the basis
////                  for the TRUEUP-FINAL records.
////
////  Notes  :  This script is called one time from OUTSIDE THE "TYPE" loop.  We are still
////            in the company loop however.
////            
////*****************************************************************************************
//longlong i, num_rows, num_cols, c, counter
//string sqls, cr_txn_type, col
//decimal {2} t_limit, t_limit_negative
//
//
////  SETUP:
////  There is an instance DS against all_tab_columns so we can build the insert.  Since we
////  want to summarize life-to-date balances, we are simply going to get the $$$ from
////  cr_cost_repository.
//sqls = &
//	"select upper(column_name), column_id from all_tab_columns " + &
//	 "where table_name = 'CR_COST_REPOSITORY' and owner = 'PWRPLANT'"
//i_ds_all_tab_cols.Reset()
//i_ds_all_tab_cols.SetSQLSelect(sqls)
//num_cols = i_ds_all_tab_cols.RETRIEVE()
//i_ds_all_tab_cols.SetSort("column_id a")
//i_ds_all_tab_cols.Sort()
//
//
////  Get the threshold limit.
//t_limit = 0
//t_limit_negative = 0
//select threshold_limit into :t_limit from cr_sap_trueup_control
// where "TYPE" = :i_derivation_type;
//if isnull(t_limit) then t_limit = 0
//
//if t_limit = 0 then
//	t_limit_negative = 0
//else
//	t_limit_negative = -t_limit
//end if
//
//
////
////  DMJ: 10/03/07: This did not work with the TRUEUP-FINAL SQL below.  Also, after
////  thinking it through it became obvious that the delete was too simplified and really
////  did not delete the intended records.  The records to be deleted by the t_limit should
////  be those records where the SUM OF THE AMOUNTS by derived_ack_fields and cr_txn_type
////  are within the threshold.  In other words, if there are 3 MTL cost elements that get
////  hit with trueup amounts and their $$$ are $2, $3, and $4 ... and if the t_limit is
////  5 bucks, then these SHOULD NOT get deleted since the total MTL trueup = 9 bucks.
////  Code below against the TRUEUP-FINAL records will apply the t_limit.
////
//////  Delete the TRUEUP-DELETE records that do not pass the threshold limit.
////sqls = &
////	"delete from " + i_stg_table + " where drilldown_key = 'TRUEUP-DELETE' " + &
////	   "and amount between " + string(t_limit_negative) + " and " + string(t_limit) + " " + &
////		'and "' + i_company_field + '" = ' + "'" + i_co + "'"
////
////if isnull(sqls) then
////	f_pp_msgs("  ")
////	f_pp_msgs("ERROR: sqls is NULL in uf_insert_trueup_final (1) !" + &
////		sqlca.SQLErrText)
////	f_pp_msgs("  ")
////	rollback;
////	return -1
////end if
////
////execute immediate :sqls;
////
////if sqlca.SQLCode < 0 then
////	f_pp_msgs("  ")
////	f_pp_msgs("ERROR: deleting TRUEUP-DELETE records for threshold_limit: " + &
////		sqlca.SQLErrText)
////	f_pp_msgs("  ")
////	rollback;
////	return -1
////end if
//
//
////  Get the list of distinct cr_txn_type values.  These are going to be the join_fields
////  from cr_sap_trueup_control that were saved in the records inserted by previous
////  functions.  They are saved and used here since it makes the SQL a zillion times
////  easier with the values saved in a field in the STG table ... rather than looping
////  over i_join_fields_array[], getting each join_field value, and having to use that
////  as a where clause sub-select against cr_deriver_control.
//sqls = &
//	"select distinct cr_txn_type from " + i_stg_table + " " + &
//	 "where drilldown_key = 'TRUEUP-DELETE'"
//
//i_ds_cr_txn_types.Reset()
//i_ds_cr_txn_types.SetSQLSelect(sqls)
//num_rows = i_ds_cr_txn_types.RETRIEVE()
//
//
////  Loop over the distinct cr_txn_type values.  Then insert the correct records into
////  the STG table as TRUEUP-FINAL.  The methodology is:
////
////  -- Get ALL of the TRUEUP records with this cr_txn_type (these are the total debits
////     and credits)
////  -- Where the "join field" (work_order, work_order||"RESOURCE", etc.) is in the STG
////     table in the TRUEUP-DELETE records (the ones where cr_txn_type = the one in the
////     loop).  We have the right values in these records since we deleted any that did
////     not pass the t_limit.
////
////  We know the "join field" because that is the value saved in cr_txn_type.
////
////  DMJ: 11/7/07:
////  This loop is now useless since the delete above has been commented out.  In the
////  interest of time and to avoid bugs, I'm leaving this code here and placing an
////  index on cr_sap_trueup_stg.cr_txn_type.  That index improved performance to around
////  2.25 minutes on ~13,700 distinct values in the loop.  That seems acceptable for now.
//for c = 1 to num_rows
//	
//	if mod(c, 2500) = 0 then
//		f_pp_msgs("Finished " + string(c) + " of " + string(num_rows) + " at " + string(now()))
//	end if
//	
//	cr_txn_type = i_ds_cr_txn_types.GetItemString(c, 1)
//	
//	//  Start the insert command.
//	sqls = "insert into " + i_stg_table + " ( "
//	
//	
//	//  Build the columns for the insert portion.
//	for i = 1 to num_cols
//		
//		col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
//		
//		choose case col
//			case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
//				  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
//				//  Leave these out of the sqls.  They will force us into a lower level of
//				//  detail than desired.
//				continue
//		end choose
//		
//		sqls += '"' + col + '",'
//			
//	next
//	
//	
//	//  Trim the trailing comma.  Not on the "froms" due to the sum(amount).
//	sqls = left(sqls, len(sqls) - 1)
//	
//	
//	//  Close the "insert" and start the "select".
//	sqls += ", cr_txn_type"
//	if g_rate_field <> "***" then
//		sqls += ", " + g_rate_field
//	end if
//	sqls += ") (select "
//	
//	
//	//  Loop again and build the select portion.
//	for i = 1 to num_cols
//		
//		col = upper(trim(i_ds_all_tab_cols.GetItemString(i, 1)))
//		
//		//  *** 
//		//  *** REMOVED THE DERIVED_POS CODE HERE THAT CONTROLS THE SUMMARIZATION TO REMOVE
//		//  *** THE DERIVED FIELDS IN UF_INSERT_ORIGINAL.
//		//  ***
//		
//		choose case col
//			case "TIME_STAMP", "USER_ID", "CWIP_CHARGE_STATUS", "GL_ID", &
//				  "BATCH_ID", "CR_ID", "BALANCES_ID", "SOURCE_ID"
//				//  Leave these out of the sqls.  They will force us into a lower level of
//				//  detail than desired.
//				continue
//			case "ID"
//				//  This is the outer select which is not summarized, so add in crdetail.nextval.
//				sqls += "crdetail.nextval,"
//				continue
//			case "DR_CR_ID" // We'll update this later.
//				sqls += "1,"
//				continue
//			case "QUANTITY" // We don't split quantities in the first place.
//				sqls += "0,"
//				continue
//			case "AMOUNT"
//				//  Allow this to go through.  The amount is the trueup amount.
//			case "MONTH_NUMBER"
//				sqls += string(i_min_open_month) + ","
//				continue
//			case "MONTH_PERIOD"
//				sqls += "0,"
//				continue
//			case "DRILLDOWN_KEY"
//				//  Use the appropriate value here.
//				sqls += "'TRUEUP-FINAL',"
//				continue
//		end choose
//			
//		sqls += '"' + col + '",'
//		
//	next
//	
//	
//	//  Trim the trailing comma.  Not on the "froms" due to the sum(amount).  Trim the
//	//  trailing "and " on where_sqls
//	sqls = left(sqls, len(sqls) - 1)
//	
//	
//	//  From and where.
//	sqls += ", cr_txn_type"
//	if g_rate_field <> "***" then
//		sqls += ", " + g_rate_field
//	end if
//	sqls += " from " + i_stg_table + " where "
//	
//	
//	//  The where is based on this cr_txn_type.
//	sqls += " cr_txn_type in (" // DMJ: should this be using the cr_txn_type field instead?
//	sqls += "select cr_txn_type from " + i_stg_table + " "  // DMJ: ditto; instead of cr_txn_type.
//		// DMJ: actually, I think this whole in-clause is irrelevant since 2 lines below we have
//		//      "and cr_txn_type = '" + cr_txn_type + "' " which is doing the same thing.
//	sqls += "where drilldown_key = 'TRUEUP-DELETE' "
//	sqls += "and cr_txn_type = '" + cr_txn_type + "' "
//	sqls += 'and "' + i_company_field + '" = ' + "'" + i_co + "') "
//	sqls += "and drilldown_key = 'TRUEUP' and cr_txn_type = '" + cr_txn_type + "' "
//	sqls += 'and "' + i_company_field + '" = ' + "'" + i_co + "'"
//	sqls += ")"
//	
//	if isnull(sqls) then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: sqls is NULL in uf_insert_trueup_final (2) c=" + string(c) + " !" + &
//			sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		rollback;
//		return -1
//	end if
//	
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: inserting TRUEUP-FINAL records into " + i_stg_table + ": " + &
//			sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		rollback;
//		return -1
//	end if
//	
//next
//
//
////  Apply the threshold limit.
////  Note that the inner SQL is a "keep these" list of cr_txn_types.
//f_pp_msgs("Deleting based on threshold limit at " + string(now()))
//
////  DMJ: 11/7/07: PERFORMANCE CHANGES:
////  1) Changed the "not in" to a minus construct.
//
////sqls = &
////	"delete from " + i_stg_table + " " + &
////	 "where cr_txn_type not in ( " + &
////			"select cr_txn_type " + &
////			  "from " + i_stg_table + " where drilldown_key = 'TRUEUP-FINAL' " + &
////			   'and "' + i_company_field + '" = ' + "'" + i_co + "' " + &
////			 "group by " + i_derived_ack_fields + " cr_txn_type " + &
////			"having sum(amount) < " + string(t_limit_negative) + " or sum(amount) > " + string(t_limit) + ") " + &
////		"and drilldown_key = 'TRUEUP-FINAL' " + &
////		'and "' + i_company_field + '" = ' + "'" + i_co + "'"
//
//sqls = &
//	"delete from " + i_stg_table + " " + &
//	 "where cr_txn_type in ( " + &
//	 		"select cr_txn_type from " + i_stg_table + " " + &
//			 "where drilldown_key = 'TRUEUP-FINAL' " + &
//			   'and "' + i_company_field + '" = ' + "'" + i_co + "' " + &
//			 "minus " + &
//			"select cr_txn_type from " + i_stg_table + " " + &
//			 "where drilldown_key = 'TRUEUP-FINAL' " + &
//			   'and "' + i_company_field + '" = ' + "'" + i_co + "' " + &
//			 "group by " + i_derived_ack_fields + " cr_txn_type " + &
//			"having sum(amount) < " + string(t_limit_negative) + " or sum(amount) > " + string(t_limit) + ") " + &
//		"and drilldown_key = 'TRUEUP-FINAL' " + &
//		'and "' + i_company_field + '" = ' + "'" + i_co + "'"
//
//if isnull(sqls) then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: sqls is NULL in uf_insert_trueup_final (1) !" + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if
//
//if g_debug = "YES" then
//	f_pp_msgs("*** uf_insert_trueup_final: t_limit:")
//	f_pp_msgs("***DEBUG*** sqls = " + sqls)
//end if
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: deleting TRUEUP-FINAL records for threshold_limit: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if
//
//
////  Since the threshold check is at the cr_derivation_rollup level of detail, you could get
////  0.00 records if some cost elements within the rollup had no diffs.  Delete those here.
//sqls = "delete from " + i_stg_table + " where amount = 0"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: deleting 0.00 TRUEUP-FINAL records: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if
//
//
////  Commit here before the next steps in the trueup.
//commit;
//
//
////  Right away, we had better balance by company.  If we've made a mistake, now is a good
////  time to get lost and let someone troubleshoot the problem.
//sqls = &
//	"select count(*) from (" + &
//		'select "' + i_company_field + '", sum(amount) from ' + i_stg_table + " " + &
//		 "where drilldown_key = 'TRUEUP-FINAL' " + &
//		 'group by "' + i_company_field + '" having sum(amount) <> 0)'
//
//i_ds_counter.Reset()
//i_ds_counter.SetSQLSelect(sqls)
//i_ds_counter.RETRIEVE()
//
//if i_ds_counter.RowCount() > 0 then
//	counter = i_ds_counter.GetItemNumber(1, 1)
//	if counter <> 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: balancing by company (2): TRUEUP-FINAL records are out of balance.")
//		f_pp_msgs("  ")
//		return -1
//	end if
//else
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: balancing by company (2): i_ds_counter.RowCount() <= 0")
//	f_pp_msgs("sqls = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//
//return 1
end function

public function longlong uf_delete ();//*****************************************************************************************
//
//  Function  :  uf_delete
//
//  Description  :  Deletes all the records from the intermediate steps in the trueup
//                  logic.  All records except the TRUEUP-FINAL records.
//
//  Notes  :  This script is called one time from OUTSIDE THE "TYPE" loop.  We are still
//            in the company loop however.
//            
//*****************************************************************************************
string sqls

//f_pp_msgs()

sqls = &
	"delete from " + i_stg_table + &
	" where drilldown_key <> 'TRUEUP-FINAL'" + &
	" and " + '"' + i_company_field + '" = ' + "'" + i_co + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting non-TRUEUP-FINAL records from " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


commit;


return 1
end function

public function longlong uf_update_cr_derivation_rollup ();//*****************************************************************************************
//
//  Function  :  uf_update_cr_derivation_rollup
//
//  Description  :  Updates the cr_derivation_rollup field on the i_stg_table so the
//                  re-derivation can key off of the values.
//
//  Notes  :  This script is called from within a master "TYPE" loop just before the call
//            to uf_re_derive().  This update is probably very similar to the update
//            in the transaction interface.
//
//*****************************************************************************************
longlong num_cols, j, i
string sqls


// ### 7924:  JAK: 2011-06-27:  Values by company logic.
sqls = "update " + i_stg_table + " a set cr_derivation_rollup = (" + &
	"select cr_derivation_rollup from " + i_rollup_element_table + " b " + &
	"where a." + i_rollup_element + " = b." + i_rollup_element_column 
	
if i_rollup_element_by_company = 1 then sqls += " and a." + i_company_field + " = b." + i_company_field

sqls += " and element_type = 'Actuals') " + &
	'where "' + i_company_field + '" = ' + "'" + i_co + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_derivation_rollup: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  ### 6328: JAK: 2010-01-14:  True-up of quantities.
sqls = "update " + i_stg_table + " a set quantity = 0 " + &
	"where not exists (select 1 from cr_derivation_rollup b where a.cr_derivation_rollup = b.cr_derivation_rollup and b.split_quantities = 1) " + &
	'and "' + i_company_field + '" = ' + "'" + i_co + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_derivation_rollup: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

commit;


return 1

end function

public function longlong uf_ignore_rule ();//*****************************************************************************************
//
//  Function  :  uf_ignore_rule
//
//  Description  :  We don't necessarily want to process all work orders through the
//                  trueup.  For example, if we unitized and closed a work order last year,
//                  it may not make sense to trueup the actuals if someone fiddles with
//                  the WO's estimate.
//
//                  "Rules" will be entered in the cr_sap_trueup_control.ignore_rule field
//                  as a comma-separated list (e.g. '1,2,3').  Those rule numbers will be
//                  hardcoded for certain scenarios in uf_ignore_rule.  That function will
//                  parse out the list and apply the correct rules.  E.G. if rule 3 is 
//                  "ignore WO's whose wo_status_id = 7" and we want that rule to be
//                  applied, then the number 3 needs to be in the comma-separated list.
//
//  Rules  :  RULE 1: DELETE WORK ORDERS WITH NO HEADERS IN PROJECTS.
//            RULE 2: DELETE CANCELLED WORK ORDERS.
//            RULE 3: DELETE CLOSED/POSTED WORK ORDERS.
//            RULE 4: DELETE UNITIZED WORK ORDERS.
//            RULE 5: DELETE SUSPENDED WORK ORDERS.
//				  RULE 6: DELETE WORK ORDERS THAT SHOULD HAVE BEEN POSTED BY NOW.
//
//*****************************************************************************************
longlong i
string  ignore_array[], rule_number, sqls
boolean exec_sql

//  No need to go any further if the cr_sap_trueup_control.ignore_rule field
//  was not filled in.
if isnull(i_ignore_rule) or trim(i_ignore_rule) = "" then return 1

//  Almost certainly critical to have fresh stats for the SQL below.
f_pp_msgs("--Analyzing " + i_stg_table + " table at: " + string(now()))
sqlca.analyze_table(i_stg_table)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** analyze SQLCode = " + string(sqlca.SQLCode))

//  Parse the cr_sap_trueup_control.ignore_rule value into an array.  The i_ignore_rule
//  variable was populated in uf_read.
f_parsestringintostringarray(i_ignore_rule, ",", ignore_array)

//  No rules selected ... just return.
if upperbound(ignore_array) = 0 then
	return 1
end if

//  Loop over the rules now that they are in each cell of the array.  Based on what
//  was entered, fire the appropriate rule.
for i = 1 to upperbound(ignore_array)
	
	//  Reset this.
	exec_sql = true
	
	//  Get the rule number ... trim since there could be spaces ... use a string
	//  variable so we don't bomb if they enter a letter.
	rule_number = trim(ignore_array[i])
	
	choose case rule_number
		case "1"
			//  RULE 1: DELETE WORK ORDERS WITH NO HEADERS IN PROJECTS.
			f_pp_msgs("--Applying rule 1 (no headers) at " + string(now()))
			sqls = &
				"delete from " + i_stg_table + " " + &
				 "where not exists " + &
					 "(select * from work_order_control w, company_setup c " + &
					   "where w.company_id = c.company_id and w.funding_wo_indicator = 0 " + &
						  "and " + i_stg_table + '."' + i_wo_field + '" = w.work_order_number ' + &
						  'and ' + i_stg_table + '."' + i_company_field + '" = c.' + i_pp_external_company + ")"
		case "2"
			//  RULE 2: DELETE CANCELLED WORK ORDERS.
			f_pp_msgs("--Applying rule 2 (cancelled) at " + string(now()))
			sqls = &
				"delete from " + i_stg_table + " " + &
				 "where exists " + &
					 "(select * from work_order_control w, company_setup c " + &
						"where w.company_id = c.company_id and w.funding_wo_indicator = 0 " + &
						  "and " + i_stg_table + '."' + i_wo_field + '" = w.work_order_number ' + &
						  'and ' + i_stg_table + '."' + i_company_field + '" = c.' + i_pp_external_company + " " + &
						  "and w.wo_status_id = 8)"
		case "3"
			//  RULE 3: DELETE CLOSED/POSTED WORK ORDERS.
			f_pp_msgs("--Applying rule 3 (closed) at " + string(now()))
			sqls = &
				"delete from " + i_stg_table + " " + &
				 "where exists " + &
					 "(select * from work_order_control w, company_setup c " + &
						"where w.company_id = c.company_id and w.funding_wo_indicator = 0 " + &
						  "and " + i_stg_table + '."' + i_wo_field + '" = w.work_order_number ' + &
						  'and ' + i_stg_table + '."' + i_company_field + '" = c.' + i_pp_external_company + " " + &
						  "and w.wo_status_id = 7)"
		case "4"
			//  RULE 4: DELETE UNITIZED WORK ORDERS.
			f_pp_msgs("--Applying rule 4 (unitized) at " + string(now()))
			sqls = &
				"delete from " + i_stg_table + " " + &
				 "where exists " + &
					 "(select * from work_order_control w, company_setup c " + &
						"where w.company_id = c.company_id and w.funding_wo_indicator = 0 " + &
						  "and " + i_stg_table + '."' + i_wo_field + '" = w.work_order_number ' + &
						  'and ' + i_stg_table + '."' + i_company_field + '" = c.' + i_pp_external_company + " " + &
						  "and w.wo_status_id = 6)"
		case "5"
			//  RULE 5: DELETE SUSPENDED WORK ORDERS.
			f_pp_msgs("--Applying rule 5 (suspended) at " + string(now()))
			sqls = &
				"delete from " + i_stg_table + " " + &
				 "where exists " + &
					 "(select * from work_order_control w, company_setup c " + &
						"where w.company_id = c.company_id and w.funding_wo_indicator = 0 " + &
						  "and " + i_stg_table + '."' + i_wo_field + '" = w.work_order_number ' + &
						  'and ' + i_stg_table + '."' + i_company_field + '" = c.' + i_pp_external_company + " " + &
						  "and w.wo_status_id = 3)"
		case "6"
			//  RULE 6: DELETE WORK ORDERS THAT SHOULD HAVE BEEN POSTED BY NOW
			//    This rule will probably work in conjunction with removing rules 3 and 4.  The idea here
			//    is to allow the trueup to run against work orders thay may have been unitized this month.
			//    This will be necessary for clients who's process is to unitize during the month before the
			//    trueup runs.  They may still have some trailing charges or the estimate might still have
			//    been modified as part of unitization.  A client unitizing after the trueup runs might have
			//    rules 3 and 4 in effect and not this one (although even that could be questionable depending
			//    on their ability to shut down charging in the feeders).  This rule works with wo_status_id
			//    hardcoded to 7 --- meaning that until the work order has had its unitization transactions
			//    posted, it will always go through the trueup.  This will give us the ability to run the
			//    trueup on work orders that may keep kicking out of unitization.
			f_pp_msgs("--Applying rule 6 (closed: apply late chg wait period) at " + string(now()))
			sqls = &
				"delete from " + i_stg_table + " " + &
				 "where exists " + &
					 "(select * from work_order_control w, company_setup c " + &
						"where w.company_id = c.company_id and w.funding_wo_indicator = 0 " + &
						  "and " + i_stg_table + '."' + i_wo_field + '" = w.work_order_number ' + &
						  'and ' + i_stg_table + '."' + i_company_field + '" = c.' + i_pp_external_company + " " + &
						  "and w.wo_status_id = 7 " + &
						  "and to_char(add_months(w.completion_date, w.late_chg_wait_period + 1),'yyyymm') <= '" + string(i_min_open_month) + "')"
		case else
			//  Unknown rule.  Don't execute the sqls.
			exec_sql = false
	end choose
	
	if exec_sql then
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: in uf_ignore_rule (" + rule_number + "): " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
	end if
	
next

commit;

return 1

end function

public function longlong uf_read ();//*****************************************************************************************
//
//  Object      :  uo_cr_sap_trueup
//  UO Function :  uf_read
//
//	 Returns     :  1 if successful
// 					-1 otherwise
//
//  Detailed Attribute Notes :
//  1) The cr_derivation_id will always be NULL on the results since it is meaningless
//     for the trueup.  There is no single record in the CR that is the basis for a
//     trueup record.  Rather, it is the current sum by account (generally) for some
//     level of detail that is the starting basis for the trueup.
//  2) The cr_derivation_rate will be populated with the pecent value from the
//     cr_deriver_control table (as it is in interfaces).  This value first appears in
//     the "RE-DERIVATION" records generated in uf_re_derive by calling the
//     uo_cr_derivation object.  This field is then carried through to all subsequent
//     intermediate transactions and the final results.
//
//*****************************************************************************************
longlong counter, num_companies, i, t, rtn, num_rows, lvalidate_rtn, validation_counter, overall_rtn, max_days
string  companies, sqls, company_where_clause, str_source_id, rc, cv_validations, s_date, to_user
boolean validation_kickouts = false
date    ddate
time    ttime
datetime finished_at


//*****************************************************************************************
//
//  ALL CONNECTED.  CHECK THE DEBUG VARIABLE.
//
//*****************************************************************************************
setnull(g_debug)
select upper(trim(control_value)) into :g_debug from cr_system_control
 where upper(trim(control_name)) = 'SAP TRUEUP - DEBUG';
if isnull(g_debug) or g_debug = "" then g_debug = "NO"


//*****************************************************************************************
//
//  Get the interface_id from cr_interfaces ...
//    DO NOT perform an upper on the select value ... 
//      This must be before the check for invalid ids !
//
//*****************************************************************************************
f_pp_msgs("Retrieve interface_id for SAP TRUEUP at " + string(now()))

g_interface_id = 0
select interface_id into :g_interface_id from cr_interfaces
 where lower(description) = 'sap trueup' or lower(description) = 'cr derivation trueup';
if isnull(g_interface_id) then g_interface_id = 0
 
if g_interface_id = 0 then
	f_write_log(g_log_file, &
		"ERROR: No (SAP Trueup) entry exists in cr_interfaces ... Cannot run the " + &
		"SAP Trueup process!")
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: No (SAP Trueup) entry exists in cr_interfaces ... Cannot run the " + &
				 "SAP Trueup process!")
	f_pp_msgs("  ")
	g_do_not_write_batch_id = true
	overall_rtn = -1
	goto exit_and_return
end if


//*****************************************************************************************
//
//  Setup
//
//*****************************************************************************************
f_pp_msgs("Perform setup at " + string(now()))

company_where_clause = ""

i_company_field = ""
select upper(trim(control_value)) into :i_company_field from cr_system_control
 where upper(trim(control_name)) = 'COMPANY FIELD';
i_company_field = f_cr_clean_string(i_company_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_company_field = " + i_company_field)
if isnull(i_company_field) then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COMPANY FIELD in cr_system_control!")
	f_pp_msgs("  ")
	overall_rtn = -1
	goto exit_and_return
end if

i_wo_field = ""
select upper(trim(control_value)) into :i_wo_field from cr_system_control
 where upper(trim(control_name)) = 'WORK ORDER FIELD';
i_wo_field = f_cr_clean_string(i_wo_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_wo_field = " + i_wo_field)
if isnull(i_company_field) then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the WORK ORDER FIELD in cr_system_control!")
	f_pp_msgs("  ")
	overall_rtn = -1
	goto exit_and_return
end if

i_account_field = ""
select upper(trim(control_value)) into :i_account_field from cr_system_control
 where upper(trim(control_name)) = 'FERC ACCOUNT FIELD';
i_account_field = f_cr_clean_string(i_account_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_account_field = " + i_account_field)
if isnull(i_account_field) then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the FERC ACCOUNT FIELD in cr_system_control!")
	f_pp_msgs("  ")
	overall_rtn = -1
	goto exit_and_return
end if

//	 Look up the "percent attribute" field from cr_system_control.  This will be retained
//  as a detailed attribute in the final results.
i_rate_field = ""
select upper(trim(control_value)) into :i_rate_field from cr_system_control
 where upper(control_name) = 'DERIVATIONS - PERCENT ATTRIBUTE';
if isnull(i_rate_field) then
	i_rate_field = "***"
else
	i_rate_field = f_cr_clean_string(i_rate_field)
end if
if g_debug = "YES" then	f_pp_msgs("***DEBUG*** i_rate_field = " + i_rate_field)

//  For uf_ignore_rule.
i_pp_external_company = ""
select upper(trim(control_value)) into :i_pp_external_company from cr_system_control
 where upper(trim(control_name)) = 'PP EXTERNAL COMPANY';
i_pp_external_company = f_cr_clean_string(i_pp_external_company)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_pp_external_company = " + i_pp_external_company)
if isnull(i_pp_external_company) then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the PP EXTERNAL COMPANY in cr_system_control!")
	f_pp_msgs("  ")
	overall_rtn = -1
	goto exit_and_return
end if

i_ds_all_tab_cols = CREATE uo_ds_top
sqls = &
	"select upper(column_name), column_id from all_tab_columns " + &
	 "where table_name = ''"
f_create_dynamic_ds(i_ds_all_tab_cols, "grid", sqls, sqlca, true)

//i_ds_elements = CREATE uo_ds_top
//i_ds_elements.DataObject = "dw_cr_element_definitions"
//i_ds_elements.SetTransObject(sqlca)
//i_num_elements = i_ds_elements.RETRIEVE()

i_ds_elements = CREATE uo_ds_top
sqls = 'select * from cr_elements order by "ORDER" '
f_create_dynamic_ds(i_ds_elements, "grid", sqls, sqlca, true)
i_num_elements = i_ds_elements.RowCount()

i_ds_counter = CREATE uo_ds_top
sqls = "select count(*) from dual where 1 = 2"
f_create_dynamic_ds(i_ds_counter, "grid", sqls, sqlca, true)

i_ds_cr_txn_types = CREATE uo_ds_top
sqls = "select rpad(' ', 2000) from dual where 1 = 2"
f_create_dynamic_ds(i_ds_cr_txn_types, "grid", sqls, sqlca, true)

uo_ds_top ds_derivation_types
ds_derivation_types = CREATE uo_ds_top
sqls = "select * from cr_sap_trueup_control"
f_create_dynamic_ds(ds_derivation_types, "grid", sqls, sqlca, true)
i_num_types = ds_derivation_types.RowCount()

if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_num_types = " + string(i_num_types))

if i_num_types <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: There are no records in cr_sap_trueup_control.  Cannot process.")
	f_pp_msgs("  ")
	overall_rtn = -1
	goto exit_and_return
end if

i_uo_deriver = CREATE uo_cr_derivation

i_ds_virt = CREATE datastore
sqls = "select distinct cr_derivation_rollup from cr_derivation_rollup " + &
	"where virtual_derivation = 1"
f_create_dynamic_ds(i_ds_virt, "grid", sqls, sqlca, true)
i_num_virt = i_ds_virt.RowCount()

i_ds_open_month = CREATE datastore
sqls = "select min(month_number) from cr_open_month_number where source_id = 0"
f_create_dynamic_ds(i_ds_open_month, "grid", sqls, sqlca, true)

i_elig_accts_count = 0
select count(*) into :i_elig_accts_count from cr_sap_trueup_eligible_accts where rownum = 1;
if isnull(i_elig_accts_count) then i_elig_accts_count = 0


if g_debug = "YES" then
	i_uo_deriver.i_debug = "YES"
end if


//*****************************************************************************************
//
//  What is the source_id for the "SAP Trueup" transaction table ?  This is the CR-Detail
//  table where trueup transaction will post.
//
//*****************************************************************************************
f_pp_msgs("Determining SAP Trueup Source ID at " + string(now()))

i_source_id = 0
setnull(str_source_id)
select trim(control_value) into :str_source_id from cr_system_control
 where upper(trim(control_name)) = 'SAP TRUEUP SOURCE ID';
if isnull(str_source_id) then str_source_id = ""
i_source_id = long(str_source_id)
if isnull(i_source_id) or i_source_id = 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: SAP TRUEUP SOURCE ID not found in cr_system_control.")
	f_pp_msgs("  ")
	overall_rtn = -1
	goto exit_and_return
end if

if g_debug = "YES" then f_pp_msgs("***DEBUG*** str_source_id = " + str_source_id)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_source_id = " + string(i_source_id))

setnull(i_stg_table)
setnull(i_detail_table)
select upper(trim(table_name)), upper(trim(table_name))||'_STG'
  into :i_detail_table, :i_stg_table
  from cr_sources
 where source_id = :i_source_id;
if isnull(i_stg_table) or i_stg_table = "" or &
   isnull(i_detail_table) or i_detail_table = "" &
then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Could not find the table name in cr_sources !")
	f_pp_msgs("  ")
	overall_rtn = -1
	goto exit_and_return
end if

if g_debug = "YES" then
	f_pp_msgs("***DEBUG*** i_stg_table = " + i_stg_table)
	f_pp_msgs("***DEBUG*** i_detail_table = " + i_detail_table)
end if


//*****************************************************************************************
//
//  Custom Function Call:  way up-front.
//
//  For things like audits or maintenance of cr_deriver_control.  Messages need to
//  be handled in the function.
//
//*****************************************************************************************
rc = f_cr_sap_trueup_custom("way up-front", "", i_stg_table)

if rc = "ERROR" then
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if


//*****************************************************************************************
//
//  Additional Setup:  That depends on variables from above.
//
//*****************************************************************************************
f_pp_msgs("Perform additional setup at " + string(now()))

i_ds_balancing = CREATE uo_ds_top
sqls = &
	"select " + i_company_field + ", sum(amount) from " + i_stg_table + &
	" where id = -678 group by " + i_company_field
rc = f_create_dynamic_ds(i_ds_balancing, "grid", sqls, sqlca, true)


//  If we are simply re-running, use the existing interface_batch_id.  Otherwise, we
//  will get the sequence nextval below.
setnull(g_batch_id)

datastore ds_batch_id
ds_batch_id = CREATE datastore
sqls = "select min(interface_batch_id) from " + i_stg_table
f_create_dynamic_ds(ds_batch_id, "grid", sqls, sqlca, true)

if ds_batch_id.RowCount() <> 0 then
	g_batch_id = trim(ds_batch_id.GetItemString(1, 1))
end if

if g_debug = "YES" and not isnull(g_batch_id) then
	f_pp_msgs("***DEBUG*** g_batch_id = " + g_batch_id)
end if


//*****************************************************************************************
//
//  NULL time_stamp values in cr_deriver_control would be bad to say the least.  Check
//  for that condition here.
//
//*****************************************************************************************
f_pp_msgs("Checking for NULL time_stamp values in cr_deriver_control at " + string(now()))

sqls = "select count(*) from cr_deriver_control where time_stamp is null"

i_ds_counter.Reset()
i_ds_counter.SetSQLSelect(sqls)
num_rows = i_ds_counter.RETRIEVE()

//  On error ...
if num_rows < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("WARNING: Error returned by i_ds_counter.Retrieve() !")
	f_pp_msgs("The process WILL continue")
	f_pp_msgs("  ")
	i_warnings = true
else
	//  On no rows returned ...
	if num_rows = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("WARNING: No rows returned by i_ds_counter !")
		f_pp_msgs("The process WILL continue")
		f_pp_msgs("  ")
		i_warnings = true
	else
		//  The null check ...
		if i_ds_counter.GetItemNumber(1, 1) > 0 then
			f_pp_msgs("  ")
			f_pp_msgs("WARNING: There are NULL time_stamp values in cr_deriver_control !")
			f_pp_msgs("These records will never be processed by the trueup logic !")
			f_pp_msgs("The process WILL continue")
			f_pp_msgs("  ")
			i_warnings = true
		end if
	end if
end if


//*****************************************************************************************
//
//  Determine which companies need to be processed.
//
//*****************************************************************************************
f_pp_msgs("Determining companies to process at " + string(now()))

//  If there are no records in the control table, assume that we are processing
//  all companies.
counter = 0
select count(*) into :counter from cr_sap_trueup_company;
if isnull(counter) then counter = 0

if counter = 0 then
	companies = "cr_company"
	goto process_companies
end if

//  Records exist in cr_sap_trueup_company.  If any of these are "ALL", assume that
//  we are processing all companies.
counter = 0
select count(*) into :counter from cr_sap_trueup_company
 where upper(trim(company)) = 'ALL';
if isnull(counter) then counter = 0

if counter > 0 then
	companies = "cr_company"
	goto process_companies
end if

//  If we get to this point, there are records in cr_sap_trueup_company and none of
//  them are "ALL".  We will need to loop over this table to build a company where clause.
//  In list of the form ('ACO','BCO','CCO')
companies = "cr_sap_trueup_company"

process_companies:

uo_ds_top ds_company
ds_company = CREATE uo_ds_top
sqls = "select * from " + companies
if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls for ds_company = " + sqls)
f_create_dynamic_ds(ds_company, "grid", sqls, sqlca, true)

if companies = "cr_company" then
	ds_company.SetSort("external_company_id a")
else
	ds_company.SetSort("company a")
end if

ds_company.Sort()

num_companies = ds_company.RowCount()

//  The company_where_clause variable is not currently used, but I've decided to leave
//  the code here in case a future where clause needs it.
for i = 1 to num_companies
	if companies = "cr_company" then
		//  Trim since Run For All Companies sometimes has a space in front.
		i_co = "'" + upper(trim(ds_company.GetItemString(i, "external_company_id"))) + "'"
	else
		i_co = "'" + upper(ds_company.GetItemString(i, "company")) + "'"
	end if
	if i_co = "'RUN FOR ALL COMPANIES'" then continue
	if i = num_companies then
		company_where_clause = company_where_clause + i_co
	else
		company_where_clause = company_where_clause + i_co + ","
	end if
next

company_where_clause = "(" + company_where_clause + ")"


//*****************************************************************************************
//
//  We are about to start processing by inserting records in the STG table.  By
//  definition, we must truncate the STG table first.
//  **  This is appropriate since the trueup is all records in the CR since the
//      datetime_last_processed.  In the case of an error and a subsequent run, we
//      want to make sure to grab any records that posted into the CR in the interim.
//      This is the correct spot, since we will not necessarily truncate the STG upon
//      certain errors (so we can effectively audit the problem).
//
//*****************************************************************************************
sqlca.truncate_table(i_stg_table)

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: truncating " + i_stg_table + ": " + &
		sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

//
//  Since we've truncated the stg table, we don't want to leave any validation kickouts
//  hanging around ... or else they will get stranded.
//
delete from cr_validations_invalid_ids2
 where table_name = :i_stg_table;
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_validations_invalid_ids2 for " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

delete from cr_validations_invalid_ids
 where table_name = :i_stg_table;
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_validations_invalid_ids for " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if


//  Before processing we need to get the upper limit for the cr_deriver_control
//  timestamps.
//  12-DEC-2007: AJS: This was just before the insert original function inside the company & type loops
//                    so the value was creeping forward.  So, records updated while this true-up 
//							 is running could be lost.
setnull(i_upper_datetime_limit)

select control_value 
	into :max_days
	from cr_system_control 
	where upper(control_name) = 'DERIVATION TRUEUP MAX DAYS';
	
if isnull(max_days) or max_days <= 0 or max_days > 1000 then
	select sysdate
		into :i_upper_datetime_limit
		from dual;
else
	select least(sysdate, min(datetime_last_processed) + :max_days)
		into :i_upper_datetime_limit
		from cr_sap_trueup_control;
end if

if g_debug = "YES" then
	f_pp_msgs("***DEBUG*** i_upper_datetime_limit = " + string(i_upper_datetime_limit))
end if


//*****************************************************************************************
//
//  Loop and process each company.  An inner loop will loop over each derivation type
//  and process each "TYPE" individually.
//
//*****************************************************************************************
f_pp_msgs("Starting the trueup process at " + string(now()))

for i = 1 to num_companies

	
	//  Get the company value in the i_co variable.
	if companies = "cr_company" then
		//  Trim since Run For All Companies sometimes has a space in front.
		i_co = upper(trim(ds_company.GetItemString(i, "external_company_id")))
	else
		i_co = upper(ds_company.GetItemString(i, "company"))
	end if
	
	
	if i_co = "RUN FOR ALL COMPANIES" then continue
	
	
	f_pp_msgs("  ")
	f_pp_msgs(i_company_field + ": " + i_co)
	f_pp_msgs("------------------------------------------------------------")
	
	
	rc = f_cr_sap_trueup_custom("beginning_of_co_loop", i_co, i_stg_table)
	
	if rc <> "" then continue
	
	
	//  There must be an open month for the "SAP Trueup" source_id.  If many are open, we
	//  will book the transactions to the min open month.  The base for the trueup
	//  will only be the sum of the transactions where month_number <= i_min_open_month.  Any
	//  transactions already posted to future periods will not be considered.
	f_pp_msgs("Retrieving valid open month at " + string(now()))
	i_min_open_month = 0
	sqls = &
		"select min(month_number) from cr_open_month_number " + &
		 "where source_id = " + string(i_source_id) + " and " + i_company_field + " = '" + i_co + "' and status = 1"
	i_ds_open_month.SetSQLSelect(sqls)
	i_ds_open_month.RETRIEVE()
	if i_ds_open_month.RowCount() > 0 then
		i_min_open_month = i_ds_open_month.GetItemNumber(1, 1)
	end if
	if isnull(i_min_open_month) then i_min_open_month = 0
	
	
	if i_min_open_month = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("WARNING: There are no open months for company " + i_co + &
			".  This company will not be processed.")
		f_pp_msgs("  ")
		i_warnings = true
		continue
	end if
	
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_min_open_month = " + string(i_min_open_month))
	
	
	//  LOOP: Over derivation types that have been entered into cr_sap_trueup_control.
	for t = 1 to i_num_types
		
		
		i_derivation_type = trim(ds_derivation_types.GetItemString(t, "type"))
		i_pos_neg_ratio   = ds_derivation_types.GetItemNumber(t, "positive_negative_ratio")
		i_ignore_rule     = ds_derivation_types.GetItemString(t, "ignore_rule")
		
		if isnull(i_pos_neg_ratio) then i_pos_neg_ratio = 2
		
		f_pp_msgs('PROCESSING "TYPE": ' + i_derivation_type)
		
		
		//  We will restrict the trueup processing using i_datetime_last_processed.  If it is
		//  null in cr_sap_trueup_control, use the beginning of time to reprocess everything.
		f_pp_msgs("Retrieving control values at " + string(now()))
		setnull(i_datetime_last_processed)
		i_datetime_last_processed = ds_derivation_types.GetItemDateTime(t, "datetime_last_processed")
		if isnull(i_datetime_last_processed) then
			i_datetime_last_processed = datetime(date("01/01/1900"))
		end if
		
		f_pp_msgs("Processing as of (i_datetime_last_processed): " + string(i_datetime_last_processed))
		
		
		//  The i_derived_ack_fields value should be of the form:
		//  ferc,resource_code,location_id, (a comma-separated list).
		//  This will be used in uf_insert_basis to determine which ACK fields we need to
		//  summarize away to get a total basis.  PLACEMENT OF THE COMMAS IS CRITICAL!  No
		//  spaces, and a comma after every field including the last one.  The f_replace_string
		//  function will be looking for "value," in i_derived_ack_fields.
		setnull(i_derived_ack_fields)		
		i_derived_ack_fields = trim(ds_derivation_types.GetItemString(t, "derived_ack_fields"))		
		if isnull(i_derived_ack_fields) or i_derived_ack_fields = "" then
			f_pp_msgs("  ")
			f_pp_msgs('ERROR: "TYPE" ' + "'" + i_derivation_type + &
				"' does not have any derived_ack_fields in cr_sap_trueup_control !")
			f_pp_msgs("  ")
			overall_rtn = -1
			goto exit_and_return
		end if
		
		// remove the spaces to make future parsing easier.
		i_derived_ack_fields = f_replace_string(i_derived_ack_fields,' ','','all')
		
		if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_derived_ack_fields = " + i_derived_ack_fields)
				
		
		//  The join_fields value should be of the form:
		//
		//  work_order||"RESOURCE",work_order (in case you haven't read the comment in
		//                                     uf_insert_basis, subsets MUST be first)
		//
		//  Where the comma denotes different types of "joins" being used in this derivation
		//  type.  For example, most derivations might simply be by work order, but work order
		//  along with labor may have a different distribution to ferc accounts.  Allowing for
		//  a mix of "joins" within a single derivation type gives us flexibility and also
		//  allows for greater performance in the transaction interfaces.  The upper case and
		//  double-quotes are not critical except in cases where the ACK field is an Oracle
		//  reserved key word.
		i_join_fields = trim(ds_derivation_types.GetItemString(t, "join_fields"))
		if isnull(i_join_fields) or i_join_fields = "" then
			f_pp_msgs("  ")
			f_pp_msgs('ERROR: "TYPE" ' + "'" + i_derivation_type + &
				"' does not have any join_fields in cr_sap_trueup_control !")
			f_pp_msgs("  ")
			overall_rtn = -1
			goto exit_and_return
		end if
		
		f_parsestringintostringarray(i_join_fields, ";", i_join_fields_array)
		
		if g_debug = "YES" then
			f_pp_msgs("***DEBUG*** i_join_fields = " + i_join_fields)
			f_pp_msgs("***DEBUG*** num array cells = " + string(upperbound(i_join_fields_array)))
		end if
		
		
		//  The cr_derivation_rollup_field value tels us which ACK field is rolling up to
		//  the discrete values we using for the derivation rates (outside of work order).
		//  For example, a cost_element field might roll up to cr_derivation_rollup values
		//  of MTL, NON-MTL, SALVAGE, and CIAC in the cost_element's master element table.
		//  Get that value here and set the i vars for the element table and element column.
		i_rollup_element = trim(ds_derivation_types.GetItemString(t, "cr_derivation_rollup_field"))
		
		if isnull(i_rollup_element) or i_rollup_element = "" then
			f_pp_msgs("  ")
			f_pp_msgs('ERROR: "TYPE" ' + "'" + i_derivation_type + &
				"' does not have a cr_derivation_rollup_field value in cr_sap_trueup_control !")
			f_pp_msgs("  ")
			overall_rtn = -1
			goto exit_and_return
		end if
		
		// ### 7924:  JAK: 2011-06-27:  Values by company logic.
		setnull(i_rollup_element_table)
		setnull(i_rollup_element_column)
		i_rollup_element_by_company = 0
		select element_table, element_column, values_by_company 
		  into :i_rollup_element_table, :i_rollup_element_column, :i_rollup_element_by_company
		  from cr_elements
		 where upper(trim(replace(description,' ','_'))) = upper(:i_rollup_element); // NOTE: do not
		 	 // enter the cr_derivation_rollup_field value in cr_sap_trueup_control with double-quotes.
			 // It will foul up this lookup.  We'll tack on the double quotes below.
		if isnull(i_rollup_element_table)  or i_rollup_element_table  = "" then
			i_rollup_element_table  = "none_found"
		end if
		if isnull(i_rollup_element_column) or i_rollup_element_column = "" then
			i_rollup_element_column = "none_found"
		end if
		if i_rollup_element_by_company <> 1 or isnull(i_rollup_element_by_company) then i_rollup_element_by_company = 0
		
		if i_rollup_element_table = "none_found" then
			f_pp_msgs("  ")
			f_pp_msgs('ERROR: "TYPE" ' + "'" + i_derivation_type + &
				"': could not determine an i_rollup_element_table value !")
			f_pp_msgs("  ")
			overall_rtn = -1
			goto exit_and_return
		end if
		
		if i_rollup_element_column = "none_found" then
			f_pp_msgs("  ")
			f_pp_msgs('ERROR: "TYPE" ' + "'" + i_derivation_type + &
				"': could not determine an i_rollup_element_column value !")
			f_pp_msgs("  ")
			overall_rtn = -1
			goto exit_and_return
		end if
		
		//  In case these are reserved words in Oracle (e.g. resource).
		i_rollup_element        = '"' + upper(i_rollup_element) + '"'
		i_rollup_element_column = '"' + upper(i_rollup_element_column) + '"'
		
		
		//  Get the GL Journal Category value to be used in the trueup process.
		i_gl_journal_category = trim(ds_derivation_types.GetItemString(t, "gl_journal_category"))
		
		if isnull(i_gl_journal_category) or i_gl_journal_category = "" then
			f_pp_msgs("  ")
			f_pp_msgs('ERROR: "TYPE" ' + "'" + i_derivation_type + &
				"' does not have a gl_journal_category value in cr_sap_trueup_control !")
			f_pp_msgs("  ")
			overall_rtn = -1
			goto exit_and_return
		end if
		
		
		//  Insert the "total" answer including the ferc account.  These records are the
		//  current summarized balances in the CR.
		//
		//  These balances will be compared later to the re-derivation records to determine
		//  if we need to trueup any "STRING" values.
		//
		//  These records will also be the basis for uf_insert_basis so we don't have to hit
		//  cr_cost_repository on a life-to-date basis more than once.
		f_pp_msgs("Inserting current balances (uf_insert_original) at " + string(now()))
		rtn = uf_insert_original()
		// JAK:  Return the record count so we know if there are really any records to process...
		//		< 0 means error
		//		0 means no records to process
		if rtn < 0 then return rtn
		if rtn = 0 then
			f_pp_msgs(" ")
			f_pp_msgs("*** No Trueup Transactions To Process for this Company / Derivation Type ***")
			f_pp_msgs(" ")
			continue
		end if
		
		
		//  We don't necessarily want to process all work orders through the trueup.
		//  For example, if we unitized and closed a work order last year, it may not
		//  make sense to trueup the actuals if someone fiddles with the WO's estimate.
		//
		//  "Rules" will be entered in the cr_sap_trueup_control.ignore_rule field
		//  as a comma-separated list (e.g. '1,2,3').  Those rule numbers will be
		//  hardcoded for certain scenarios in uf_ignore_rule.  That function will parse
		//  out the list and apply the correct rules.  E.G. if rule 3 is "ignore WO's
		//  whose wo_status_id = 7" and we want that rule to be applied, then the number 3
		//  needs to be in the comma-separated list.
		f_pp_msgs("Applying ignore rules (uf_ignore_rule) at " + string(now()))
		rtn = uf_ignore_rule()
		if rtn <> 1 then return rtn
		
		
		//  See comment for the last call to uf_update_cr_derivation_rollup() in this loop.
		//  The history of this code was such that the cr_derivation_rollup field came after
		//  the trueup logic was written.  Instead of mangling the SQL in many sub-functions,
		//  I simply added a bunch of calls to uf_update_cr_derivation_rollup() where needed
		//  The performance impact should be minimal and it ensures I don't introduce bugs
		//  into the complex sqls builds in the sub-functions.
		f_pp_msgs("Updating cr_derivation_rollup at " + string(now()))
		rtn = uf_update_cr_derivation_rollup()
		if rtn <> 1 then return rtn
		
		
		//  Insert the "total" answer excluding the ferc account.  Essentially we are putting
		//  the dollar amounts back to the original account.
		//
		//  This is the basis that we will re-run derivations against to determine if we need
		//  to trueup any "STRING" values.
		f_pp_msgs("Inserting basis for re-derivation (uf_insert_basis) at " + string(now()))
		rtn = uf_insert_basis()
		// JAK:  Return the record count so we know if there are really any records to process...
		//		< 0 means error
		//		0 means no records to process
		if rtn < 0 then return rtn
		if rtn = 0 then
			f_pp_msgs(" ")
			f_pp_msgs("*** No Trueup Transactions To Process for this Company / Derivation Type ***")
			f_pp_msgs(" ")
			continue
		end if
		
		
		//  Before we can successfully perform the re-derivation, we have to update the
		//  cr_derivation_rollup detailed attribute.  This will generally be a rollup of
		//  "cost elements".  For now I'm requiring this detailed attribute for the trueup
		//  (it is not necessarily required for derivations in interfaces) ... because I
		//  want to avoid an excessive amount of extension function work in this exe.  If
		//  this gets in the way, I'll system switch this function call at a later date.
		f_pp_msgs("Updating cr_derivation_rollup at " + string(now()))
		rtn = uf_update_cr_derivation_rollup()
		if rtn <> 1 then return rtn
		
		
		//  Re-derive what the answer should be.
		f_pp_msgs("Performing re-derivation (uf_re_derive) at " + string(now()))
		rtn = uf_re_derive()
		if rtn <> 1 then return rtn
		
		
	next  //  for t = 1 to num_types ...
	
	
	f_pp_msgs('Finished "TYPE" loop at ' + string(now()))
	
	
	//  Book the trueup transactions.  Since the original and re-derivation records were
	//  inserted correctly by "TYPE" in the loop above, we shouldn't have to book the
	//  trueup transactions by "TYPE".  The variance is essentially cooked into the records
	//  above regardless of what "TYPE" generated them.  Besides, getting the inserts for
	//  the trueup transactions correct by "TYPE" is much more difficult than just slamming
	//  them into the STG table.  I can anticipate however, someone wanting to apply a
	//  different threshold by "TYPE".  We'll deal with that below.
	f_pp_msgs("Performing initial trueup (uf_trueup) at " + string(now()))
	rtn = uf_insert_trueup()
	if rtn <> 1 then return rtn
	
	
	//  The 2nd "TYPE" loop to deal with the TRUEUP-DELETE records and create the final
	//  TRUEUP-FINAL records.
	f_pp_msgs("Performing final trueup with threshold (uf_trueup_final) at " + string(now()))
	rtn = uf_insert_trueup_final()
	if rtn <> 1 then return rtn
	
	
	//  Clean up my mess.  Get rid of everything for this company except the TRUEUP-FINAL
	//  records.
	f_pp_msgs("Deleting intermediate records (uf_delete) at " + string(now()))
	if g_debug = "YES" then
		//  Do not delete.
	else
		rtn = uf_delete()
		if rtn <> 1 then return rtn
	end if
	
	
	//  One more time to get this value onto the final answer.  It's easier to do it this
	//  way than to try and carry it all the way through.  See comments above.
	f_pp_msgs("Updating cr_derivation_rollup at " + string(now()))
	rtn = uf_update_cr_derivation_rollup()
	if rtn <> 1 then return rtn
	
	
next  //  for i = 1 to num_companies ...


DESTROY i_ds_elements
DESTROY ds_company


//*****************************************************************************************
//
//	 DEBUG: I've decided that debugging will commit all the records above to the STG table
//         and scram.  It is far more important to be able to interrogate and audit all
//         of these intermediate records, than to continue to anything below.  Everything
//         below is easy to audit or debug by hand.
//
//*****************************************************************************************
if g_debug = "YES" then
	f_pp_msgs("  ")
	f_pp_msgs("***DEBUG***")
	f_pp_msgs("  Debugging is turned on.  This process will terminate")
	f_pp_msgs("  without posting any results.  Intermediate records have been")
	f_pp_msgs("  saved in " + i_stg_table)
	f_pp_msgs("***DEBUG***")
	f_pp_msgs("  ")
	commit;
	return 1
end if


//*****************************************************************************************
//
//	 Interface Batch Id:  New only ... On a new run, we need to get the ifb_id from the
//  sequence.  We need this before the 0 record check below since we will still be
//  inserting into cr_interface_dates, and the PK on that table includes feeder_system_id.
//
//  THIS MUST BE AHEAD OF THE "NO TRANSACTIONS TO POST" SECTION EVEN THOUGH IT CAN BURN
//  BATCH IDS.  We still log a record into cr_interface_dates when there is nothing to
//  post, and without the g_batch_id value, we can bomb on a unique constraint.
//
//*****************************************************************************************
f_pp_msgs("Selecting new batch id at " + string(now()))

if isnull(g_batch_id) or g_batch_id = "" or g_batch_id = "0" then
	setnull(g_batch_id)
	select costrepository.nextval into :g_batch_id from dual;
	if isnull(g_batch_id) then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: could not determine the g_batch_id.")
		f_pp_msgs("The process cannot run.")
		f_pp_msgs("  ")
		overall_rtn = -1
			goto exit_and_return
	end if
end if

if g_debug = "YES" then f_pp_msgs("***DEBUG*** g_batch_id = " + g_batch_id)


//*****************************************************************************************
//
//  We've got the answer now in the STG table (drilldown_key = 'TRUEUP-FINAL' and
//  cr_txn_type = the join field values).  All the intermediate records have been
//  deleted at this point.
//
//  Check that we really have some transactions to post.  If not, skip to the bottom
//  of this script.  I have purposely skipped the datetime_last_processed section here
//  as a bit of insurance.  In general, there should never be much stacking up in this
//  condition (i.e. 6 months of processing without posting any trueup transactions).
//
//  Then ... Validate and post the transactions.
//
//*****************************************************************************************

sqls = "select count(*) from " + i_stg_table + " where drilldown_key = 'TRUEUP-FINAL'"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** count sqls = " + sqls)

i_ds_counter.Reset()
i_ds_counter.SetSQLSelect(sqls)
i_ds_counter.RETRIEVE()

if i_ds_counter.RowCount() <> 0 then
	counter = i_ds_counter.GetItemNumber(1, 1)
	if counter = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("*** No Trueup Transactions To Post ***")
		f_pp_msgs("  ")
		goto interface_dates
	end if
end if



// *** -----------------  TRADITIONAL INTERFACE CODE BELOW  ----------------- **//



//*****************************************************************************************
//
//	 BALANCE THE TRANSACTIONS IN staging table.
//
//*****************************************************************************************
f_pp_msgs("Balancing the trueup transactions at " + string(now()))

sqls = &
	"select " + i_company_field + ", sum(amount) from " + i_stg_table + &
	" group by " + i_company_field + " having sum(amount) <> 0"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** balancing sqls = " + sqls)

i_ds_balancing.Reset()
i_ds_balancing.SetSQLSelect(sqls)
num_rows = i_ds_balancing.RETRIEVE()

if num_rows < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: balancing " + i_stg_table + &
		".  i_ds_balancing returned error code: " + string(num_rows) + " !")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("Transactions WILL NOT be posted !")
	f_pp_msgs("  ")
	rollback;  //  Note this rolls back all intermediate inserts and updates so at square 1.
	overall_rtn = -1
	goto exit_and_return
end if

if num_rows > 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: " + i_stg_table + " is out of balance by " + i_company_field + " !")
	f_pp_msgs("Transactions WILL NOT be posted !")
	for i = 1 to i_ds_balancing.RowCount()
		if i = 1 then f_pp_msgs("--------------------------------------------------")
		f_pp_msgs(i_ds_balancing.GetItemString(i, 1) + "   " + &
			string(i_ds_balancing.GetItemDecimal(i, 2)))
	next
	f_pp_msgs("  ")
	rollback;  //  Note this rolls back all intermediate inserts and updates so at square 1.
	overall_rtn = -1
	goto exit_and_return
end if


//*****************************************************************************************
//
//  It is almost certainly important to analyze the table before any of the derivations
//  below.
//
//*****************************************************************************************
f_pp_msgs("Analyzing " + i_stg_table + " table at: " + string(now()))
sqlca.analyze_table(i_stg_table)

if g_debug = "YES" then f_pp_msgs("***DEBUG*** analyze SQLCode = " + string(sqlca.SQLCode))

//  Another important table that could change often and needs fresh stats.
f_pp_msgs("Analyzing cr_sap_trueup_exclusion_wo table at: " + string(now()))
sqlca.analyze_table('cr_sap_trueup_exclusion_wo')

if g_debug = "YES" then f_pp_msgs("***DEBUG*** analyze SQLCode = " + string(sqlca.SQLCode))


//*****************************************************************************************
//
//  MONTH_PERIOD:
//
//*****************************************************************************************
f_pp_msgs("Updating cr_interface_month_period (existing months) at " + string(now()))

//	 Increment the month_period (existing months) on cr_interface_month_period.
sqls = &
	"update cr_interface_month_period set month_period = month_period + 1 " + &
	 "where source_id = " + string(i_source_id) + " " + &
		"and month_number in ( " + &
			"select distinct month_number from " + i_stg_table + ")"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** MP sqls (1) = " + sqls)

execute immediate :sqls;
	
if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_interface_month_period (existing months) : " + &
		sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

f_pp_msgs("Updating " + i_stg_table + ".month_period (existing months) at " + string(now()))

//  Update the month period (existing months) on the STG table.
sqls = &
	"update " + i_stg_table + " a " + &
	   "set month_period = ( " + &
			"select b.month_period " + &
		     "from cr_interface_month_period b " + &
		    "where a.month_number = b.month_number " + &
		      "and b.source_id = " + string(i_source_id) + ") " + &
    "where month_number in ( " + &
		"select month_number from cr_interface_month_period " + &
	    "where source_id = " + string(i_source_id) + ")"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** MP sqls (2) = " + sqls)

execute immediate :sqls;
	
if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating " + i_stg_table + ".month_period " + &
		"(existing months) : " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

f_pp_msgs("Inserting into cr_interface_month_period (new months) at " + string(now()))

//  Insert new month numbers and their corresponding month periods.
sqls = &
	"insert into cr_interface_month_period (source_id, month_number, month_period) " + &
		"select distinct " + string(i_source_id) + ", month_number, 1 " + &
	     "from " + i_stg_table + " where month_period = 0"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** MP sqls (3) = " + sqls)

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into cr_interface_month_period (new months): " + &
		sqlca.sqlerrtext)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if  

f_pp_msgs("Updating " + i_stg_table+ ".month_period (new months) at " + string(now()))

//  Update the month period (new months) on the STG table.
sqls = &
	"update " + i_stg_table + " a " + &
		"set month_period = ( " + &
			"select b.month_period " + &
		     "from cr_interface_month_period b " + &
		    "where a.month_number = b.month_number " + &
		      "and b.source_id = " + string(i_source_id) + " " + &
		      "and a.month_period = 0) " + &
	 "where a.month_period = 0"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** MP sqls (4) = " + sqls)

execute immediate :sqls;
	
if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating " + i_stg_table + ".month_period (new months): " + &
		sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

commit;


//*****************************************************************************************
//
//	 Derive the FUNDING PROJECT.  I am doing this before validations, so any projects that
//  are left as blank will fail the validations.
//
//  I have left the interface_batch_id out of the where clause since we trunc the STG
//  table before running.  It should be irrelevant and it has not been updated yet.
//
//*****************************************************************************************

//  Get the control values for the FP and WO fields in the accounting key.
setnull(i_fp_field)
select upper(trim(control_value)) into :i_fp_field from cr_system_control
 where upper(trim(control_name)) = 'FUNDING PROJECT FIELD';
if isnull(i_fp_field) or i_fp_field = "" then i_fp_field = "NONE"
i_fp_field   = f_cr_clean_string(i_fp_field)

if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_fp_field = " + i_fp_field)

if i_fp_field = "NONE" then goto after_fp_derivation

if i_wo_field = "NONE" then goto after_fp_derivation

if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_fp_field = " + i_fp_field)


//  First, update to single-spaces.  This is probably not needed, but is the technique used
//  in interfaces, so we will stay consistent here.
f_pp_msgs("Updating " + i_stg_table + ".funding_project to ' ' at " + string(now()))

sqls = &
	"update " + i_stg_table + " a set " + i_fp_field + " = ' ' " + &
	 "where " + i_wo_field + " <> ' '"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** FP sqls (1) = " + sqls)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating " + i_stg_table + ".funding_project to ' ': " + &
		sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
else 
	//	No commit...
end if


//  Derive the FP.
f_pp_msgs("Updating " + i_stg_table + ".funding_project at " + string(now()))

sqls = &
	"update " + i_stg_table + " a " + &
	   "set " + i_fp_field + " = " + &
			"(select fp.work_order_number " + &
			   "from work_order_control wo, company_setup co, work_order_control fp " + &
			  "where a." + i_wo_field + " = wo.work_order_number " + &
			  	 "and a." + i_company_field + " = co.gl_company_no " + &
				 "and wo.company_id = co.company_id " + &
				 "and wo.funding_wo_id = fp.work_order_id " + &
				 "and wo.funding_wo_indicator = 0 and fp.funding_wo_indicator = 1 " + &
			") " + &
	 "where (a." + i_wo_field + ", a." + i_company_field + ") in " + &
			 "(select x.work_order_number, y.gl_company_no " + &
				 "from work_order_control x, company_setup y " + &
				"where x.company_id = y.company_id " + &
				  "and x.funding_wo_indicator = 0) "
////		"and interface_batch_id = '" + g_batch_id + "'"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** FP sqls (2) = " + sqls)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + "." + i_fp_field + " : " + &
		sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
else 
	//	No commit...
end if


//  Call the custom function here ... e.g. could have code that updates the
//  "orig" FP field if they are a client that captures those fields.
rc = f_cr_sap_trueup_custom("update orig_fp field", "", i_stg_table)

rtn = long(rc)

if rtn < 0 then  //  rtn is = to sqlca.SQLCode
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + "." + i_fp_field + " (extension function): " + &
		sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

commit;


after_fp_derivation:


//*****************************************************************************************
//
//	 Perform other derivations.
//
//*****************************************************************************************
f_pp_msgs("Performing other derivations at " + string(now()))

sqls = &
	"update " + i_stg_table + " set " + &
		"dr_cr_id = decode(sign(amount), -1, -1, 1), " + &
		"interface_batch_id = " + g_batch_id

if g_debug = "YES" then f_pp_msgs("***DEBUG*** other derivation sqls = " + sqls)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ": " + &
		sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if


//*****************************************************************************************
//
//  Custom Balancing:  Perhaps to check for sum(amount) <> 0 for all ACK fields that are
//                     not being derived.
//
//*****************************************************************************************

//  No f_pp_msgs() here (in case there is no fxn code).  Feel free to add it to the
//  custom function.
rc = f_cr_sap_trueup_custom("custom balancing - before validations", "", i_stg_table)

if rc = "ERROR" then 
	overall_rtn = -1
	goto exit_and_return
end if


//*****************************************************************************************
//
//	 VALIDATIONS:  Call the new validations user object.  It is assumed that combos have
//						already been created.
//
//*****************************************************************************************
f_pp_msgs("Analyzing stg table at: " + string(now()))
sqlca.analyze_table(i_stg_table)

if g_debug = "YES" then
	f_pp_msgs("***DEBUG*** analyze i_stg_table = " + string(sqlca.SQLCode))
end if

f_pp_msgs("Analyzing cr_validations_invalid_ids at: " + string(now()))
sqlca.analyze_table('cr_validations_invalid_ids')

if g_debug = "YES" then
	f_pp_msgs("***DEBUG*** analyze cr_validations_invalid_ids = " + string(sqlca.SQLCode))
end if


f_pp_msgs("Running validations at: " + string(now()))

uo_cr_validation l_uo_cr_validation
l_uo_cr_validation = CREATE uo_cr_validation

f_pp_msgs(" -- Deleting from cr_validations_invalid_ids at: " + string(now()))

lvalidate_rtn = l_uo_cr_validation.uf_delete_invalid_ids(i_stg_table, g_batch_id)

if lvalidate_rtn <> 1 then 
	overall_rtn = -1
	goto exit_and_return
end if

validation_kickouts = false

setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - COMBO';
if isnull(cv_validations) then cv_validations = "NO"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** cv_validations (1) = " + cv_validations)

if cv_validations <> "YES" then goto after_combo_validations


f_pp_msgs(" -- Validating the Batch (Combos) at: " + string(now()))

//	 Which function is this client using?
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'VALIDATIONS - CONTROL OR COMBOS';

if g_debug = "YES" then f_pp_msgs("***DEBUG*** cv_validations (2) = " + cv_validations)

if isnull(cv_validations) then cv_validations = "CONTROL"

if cv_validations = 'CONTROL' then
	lvalidate_rtn = l_uo_cr_validation.uf_validate_control(i_stg_table, g_batch_id)
else
	lvalidate_rtn = l_uo_cr_validation.uf_validate_combos(i_stg_table, g_batch_id)
end if

if lvalidate_rtn = -1 then
	//  Error in uf_validate_(x).  Logged by the function.
	overall_rtn = -1
	goto exit_and_return
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_counter = 0
	select count(*) into :validation_counter	from cr_validations_invalid_ids
	 where lower(table_name) = lower(:i_stg_table);
	if validation_counter = 0 then
	else
		validation_kickouts = true
	end if
end if

after_combo_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
if isnull(cv_validations) then cv_validations = "NO"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** cv_validations (3) = " + cv_validations)

if cv_validations <> "YES" then goto after_project_validations

f_pp_msgs(" -- Validating the Batch (Projects) at: " + string(now()))

lvalidate_rtn = l_uo_cr_validation.uf_validate_projects(i_stg_table, g_batch_id)

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	overall_rtn = -1
	goto exit_and_return
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_counter = 0
	select count(*) into :validation_counter	from cr_validations_invalid_ids
	 where lower(table_name) = lower(:i_stg_table);
	if validation_counter = 0 then
	else
		validation_kickouts = true
	end if
end if

after_project_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - ME';
if isnull(cv_validations) then cv_validations = "NO"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** cv_validations (4) = " + cv_validations)

if cv_validations <> "YES" then goto after_me_validations

f_pp_msgs(" -- Validating the Batch (Element Values) at: " + string(now()))

lvalidate_rtn = l_uo_cr_validation.uf_validate_me(i_stg_table, g_batch_id)

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	overall_rtn = -1
	goto exit_and_return
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_counter = 0
	select count(*) into :validation_counter	from cr_validations_invalid_ids
	 where lower(table_name) = lower(:i_stg_table);
	if validation_counter = 0 then
	else
		validation_kickouts = true
	end if
end if

after_me_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MONTH NUMBER';
if isnull(cv_validations) then cv_validations = "NO"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** cv_validations (5) = " + cv_validations)

if cv_validations <> "YES" then goto after_mn_validations

f_pp_msgs(" -- Validating the Batch (Open Month Number) at: " + string(now()))

lvalidate_rtn = l_uo_cr_validation.uf_validate_month_number(i_stg_table, g_batch_id)

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	overall_rtn = -1
	goto exit_and_return
end if

if lvalidate_rtn = -2 or lvalidate_rtn = -3 then
	//CURRENTLY NOT INSERT INTO THE INVALID IDS TABLE.
	//  Validation Kickouts.
//	validation_counter = 0
//	select count(*) into :validation_counter	from cr_validations_invalid_ids
//	 where lower(table_name) = lower(:i_stg_table);
//	if validation_counter = 0 then
//	else
		validation_kickouts = true
//	end if
end if

after_mn_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - CUSTOM';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations <> "YES" then goto after_custom_validations

f_pp_msgs(" -- Validating the Batch (Custom Validations) at: " + string(now()))

lvalidate_rtn = l_uo_cr_validation.uf_validate_custom(i_stg_table, g_batch_id)

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	overall_rtn = -1
	goto exit_and_return
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_counter = 0
	select count(*) into :validation_counter	from cr_validations_invalid_ids
	 where lower(table_name) = lower(:i_stg_table);
	if validation_counter = 0 then
		//  Historical accident from interfaces at another client ... not sure why I would ever
		//  want to continue on and post the txns if there is a validation kickout!  Especially
		//  with the custom function since it is easy to code SQL in the insert of that fxn that
		//  may fail.
		validation_kickouts = true
	else
		validation_kickouts = true
	end if
end if

after_custom_validations:


DESTROY l_uo_cr_validation


if validation_kickouts then
	f_pp_msgs("  ")
	f_pp_msgs("   -----------------------------------------------------------------------" + &
		"-------------------------------------------------------------")
	f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE TRUEUP RESULTS WILL NOT BE POSTED!  *****")
	f_pp_msgs("   -----------------------------------------------------------------------" + &
		"-------------------------------------------------------------")
	f_pp_msgs("  ")
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	overall_rtn = -2
	goto exit_and_return
end if


f_pp_msgs("Validations complete at: " + string(now()))


//*****************************************************************************************
//
//	 INSERT FROM the stg table TO the CR Detail table.
//
//*****************************************************************************************
f_pp_msgs("Inserting into " + i_detail_table + " at: " + string(now()))

sqls = &
	"insert into " + i_detail_table + " " + &
		"select * from " + i_stg_table

if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_detail_table insert sqls = " + sqls)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into " + i_detail_table + ": " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if


//*****************************************************************************************
//
//  UPDATE cr_cost_repository ...
//
//*****************************************************************************************
f_pp_msgs("Updating cr_cost_repository at " + string(now()))

uo_cr_cost_repository uo_cr
uo_cr = CREATE uo_cr_cost_repository

//pass the source_id and the batch
rtn = uo_cr.uf_insert_to_cr_batch_id(i_source_id, string(g_batch_id))

if rtn = 1 then
else
	f_pp_msgs("  ")
	choose case rtn
		case -1
			f_pp_msgs("ERROR: inserting into cr_temp_cr: " + sqlca.SQLErrText)
		case -2
			f_pp_msgs("ERROR: cr_temp_cr.id UPDATE failed: " + sqlca.SQLErrText)			
		case -3
			f_pp_msgs("ERROR: cr_temp_cr.drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
		case -4
			f_pp_msgs("ERROR: " + i_detail_table + ".drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
		case -5
			f_pp_msgs("ERROR: inserting into cr_cost_repository: " + sqlca.SQLErrText)			
		case -6
			f_pp_msgs("ERROR: deleting from cr_temp_cr UPDATE failed: " + sqlca.SQLErrText)			
		case else
			f_pp_msgs("ERROR: unknown error in uo_cr_cost_repository: " + sqlca.SQLErrText)			
	end choose
	f_pp_msgs("  ")
	rollback;
	//  Detail records were committed !
	sqls = "delete from " + i_detail_table + " where interface_batch_id = '" + g_batch_id + "'"
	execute immediate :sqls;
	if sqlca.SQLCode <> 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: deleting from " + i_detail_table + " (" + g_batch_id + "): " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
	end if
	overall_rtn = -1
	goto exit_and_return		
end if


//*****************************************************************************************
//
//  TRUNCATE the STG table.
//
//*****************************************************************************************
f_pp_msgs("Truncating all records from " + i_stg_table + " at " + string(now()))

sqls = "execute truncate_table('" + i_stg_table + "')"
if g_debug = "YES" then f_pp_msgs("***DEBUG*** truncate i_stg_table sqls = " + sqls)
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Truncating " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("The records must be removed manually before the process runs again!")
	f_pp_msgs("The trueup transactions WERE posted !")
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

// JAK: Moved this to here ...this reference is only used where there are no transactions to post but the interface successfully ran 
//	and determined there was nothing to do.  No need to redo all of that logic each run.
interface_dates:
//*****************************************************************************************
//
//  ALL POSTED.  Now is a great time to update datetime_last_processed.  It is currently
//  i_datetime_last_processed and should now be i_upper_datetime_limit.  Those were the
//  boundaries used when determining which records to process in cr_deriver_control.
//  The changed deriver records in that time_stamp boundary drive the transactions that
//  are considered.
//
//*****************************************************************************************
f_pp_msgs("Updating datetime_last_processed at " + string(now()))

update cr_sap_trueup_control set datetime_last_processed = :i_upper_datetime_limit;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_sap_trueup_control.datetime_last_processed: " + &
		sqlca.SQLErrText)
	f_pp_msgs("The trueup transactions WERE posted !")
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

commit;


//*****************************************************************************************
//
//  INSERT INTO cr_interface_dates ...
//
//*****************************************************************************************
f_pp_msgs("Inserting into cr_interface_dates at " + string(now()))

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))

finished_at = datetime(ddate, ttime)
g_finished_at = finished_at

if isnull(g_batch_id) then g_batch_id = ' '

//  DMJ: 2/2/09: Dates got all whacked out at some point (not clear why it used to work).
//  Added the 'mm/dd/yyyy hh:mm:ss' format to the string() so the variable string would line
//  up with the Oracle date format.
sqls = &
	"insert into cr_interface_dates " + &
		"(interface_id, month_number, month_period, " + &
		 "process_date, total_dollars, total_records, " + &
		 "feeder_system_id, total_debits, total_credits) ( " + &	 
	"select " + string(g_interface_id) + ", month_number, month_period, " + &
	        "to_date('" + string(finished_at, 'mm/dd/yyyy hh:mm:ss') + "','mm/dd/yyyy hh24:mi:ss'), sum(amount), count(*), " + &
			 "interface_batch_id, sum(decode(sign(amount),1,amount,0)), sum(decode(sign(amount),-1,amount,0)) " + &
	  "from " + i_detail_table + " " + &
	  "where interface_batch_id = '" + g_batch_id + "' " + &
	 "group by month_number, month_period, interface_batch_id)"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** cr_interfce_dates sqls = " + sqls)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into cr_interface_dates: " + sqlca.SQLErrText)
	f_pp_msgs("The trueup transactions WERE posted !")
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto exit_and_return
else
	if sqlca.SQLNRows = 0 then
		//  No rows were inserted, meaning that there were no transactions.
		//  Still need to insert a record into cr_interfaces.
		//
		//  DMJ: This was from PNM.  It is not clear that this can ever occur.  I'll leave
		//       the code here for kicks and grins.
		insert into cr_interface_dates
			(interface_id, month_number, month_period, process_date, 
			 total_dollars, total_records, feeder_system_id)
		values
			(:g_interface_id, 0, 0, :finished_at, 0, 0, 
			 :g_batch_id || ' - No Transactions to Post');
	end if
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Inserting 0 record into cr_interface_dates: " + sqlca.SQLErrText)
		f_pp_msgs("The trueup transactions WERE posted !")
		f_pp_msgs("  ")
		rollback;
		overall_rtn = -1
	goto exit_and_return
	end if
end if


commit;

if i_warnings then
	f_pp_msgs("  ")
	f_pp_msgs("*** WARNINGS WERE LOGGED DURING THIS RUN OF THE TRUEUP PROCESS. ***")
	f_pp_msgs("*** Please review this log file thoroughly.")
	f_pp_msgs("  ")
end if


exit_and_return:
if overall_rtn < 0 then
	//	Find the user (from cr_system_control) to send a failure e-mail to.
	select upper(control_value) into :to_user
	from cr_system_control
	where upper(control_name) = 'INTERFACE ERROR EMAIL ADDRESS';
	
	if isnull(to_user) or to_user = "" or to_user = 'NONE' then
	else
		g_msmail = create uo_smtpmail
		uo_winapi = create u_external_function_win32
		// ### 8142: JAK: 2011-11-01:  Consistent return codes
		if overall_rtn = -2 then
			// validation kickouts
			f_send_mail('pwrplant','INTERFACE FAILURE -- Validation Kickouts!','The CR SAP TXN INTERFACE ' + &
				'encountered validation kickouts and terminated without posting the results.',to_user)
		else
			f_send_mail('pwrplant','INTERFACE FAILURE','The CR SAP TXN INTERFACE ' + &
				'encountered an error and terminated.',to_user)
		end if
	end if

	return overall_rtn
end if

return 1

end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_sap_trueup_custom_version nvo_cr_sap_trueup_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_sap_trueup_custom.pbd'
		return nvo_cr_sap_trueup_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

