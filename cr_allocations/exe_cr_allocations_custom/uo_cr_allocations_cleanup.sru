HA$PBExportHeader$uo_cr_allocations_cleanup.sru
$PBExportComments$FE SPECIAL - Function 1 derives the location_id from the work_order.
forward
global type uo_cr_allocations_cleanup from nonvisualobject
end type
end forward

global type uo_cr_allocations_cleanup from nonvisualobject
end type
global uo_cr_allocations_cleanup uo_cr_allocations_cleanup

type variables
uo_ds_top	i_ds_balance, i_ds_collectors, i_ds_preval_coll, i_ds_preval_cn
end variables

forward prototypes
public function longlong uf_cleanup3 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[])
public function longlong uf_cleanup2 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[])
public function longlong uf_cleanup1 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[])
end prototypes

public function longlong uf_cleanup3 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[]);
return 1
end function

public function longlong uf_cleanup2 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[]);
return 1
end function

public function longlong uf_cleanup1 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[]);//******************************************************************************************
//
//  User Object Function  :  uf_cleanup1
//
//  Notes                 :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!
//
//******************************************************************************************
longlong allocation_id, month_number, month_period, counter, clearing_indicator, &
		  intercompany_accounting, rtn, fp_counter, i, balance_counter, numrows
string  sqls, mask_value, sqls_for_ds, collector_value, entity_value, table_name, &
		  charge_number, collector, new_month_number
boolean concat

//datastore i_ds_collectors
//i_ds_collectors = CREATE datastore

//allocation_id = a_uo_alloc.i_allocation_id
//month_number  = a_long_array_arg[2]
//month_period  = a_long_array_arg[3]
//
//select clearing_indicator, intercompany_accounting
//  into :clearing_indicator, :intercompany_accounting
//  from cr_allocation_control
// where allocation_id = :allocation_id;

return 1
end function

on uo_cr_allocations_cleanup.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_allocations_cleanup.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;i_ds_balance =			CREATE uo_ds_top
i_ds_collectors =		CREATE uo_ds_top
i_ds_preval_coll =	CREATE uo_ds_top
i_ds_preval_cn =		CREATE uo_ds_top
end event

