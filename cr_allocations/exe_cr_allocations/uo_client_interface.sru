HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$v10.3 MR 7093; FE SPECIAL - Function 1 derives the location_id from the work_order.
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong i_allocation_id
longlong i_month_number
longlong i_month_period
longlong i_clearing_indicator, i_apply_wo_types
longlong i_cr_company_id
string       i_source_sqls, i_balance_sqls, i_grouping_sqls
boolean      i_run_as_a_test
boolean      i_whole_month
string       i_interco_trigger_col
string       i_original_interco_credit_sql
longlong i_ytd_indicator
string		 i_department_field, i_work_order_field, i_base_limit_detail, i_source_amount_types, &
				 i_balance_amount_types, i_source_table_name, i_balance_table_name, i_gl_journal_category, &
				 i_alloc_stg_table, i_alloc_dtl_table, i_disable_fast
longlong i_elim_flag, i_base_limit, i_derivation_flag, i_result_amount_type, i_allocate_quantity, &
				 i_num_elements, i_target_id, i_exclude_negatives, i_session_id, i_absolute_value_calcs

uo_cr_derivation i_uo_cr_derivation
uo_ds_top i_cr_deriver_type_sources, i_ds_count, i_cr_deriver_type_sources_alloc, &
			 i_cr_deriver_override2, i_cr_deriver_override2_alloc, i_ds_elements

string 	i_use_new_structures_table, i_concurrent_processing, i_suspense_accounting, &
	i_test_or_actual, i_balance_where_clause, i_company_field, i_me_validations, &
	i_run_as_commitments, i_combo_validations, i_combo_or_control, i_proj_based_validations, i_mn_validations, i_custom_validations, &
	i_run_by_dept_credits_method, i_budget_version, i_priority, i_priority2, i_allow_priority_range, &
	i_default_gljc_alloc, i_default_gljc_interco
uo_cr_validation	i_uo_cr_validation
boolean i_suspense_processed, i_rate_field_exists
longlong i_run_as_commitments_flag
uo_ds_top i_ds_sum_amount

// JAK: Dec 17, 2007:  Change all datastores to instances and remove the destroys
uo_ds_top 	i_ds_where_clause, &
	i_ds_source, i_ds_credit, i_ds_credit_insert, i_ds_target, i_ds_interco, i_ds_interco_insert, &
	i_ds_group_by, i_ds_alloc_list, i_ds_company, i_ds_where_clause_sources, i_ds_cr_sources, &
	i_ds_target_trpo, i_ds_credit_trpo, i_ds_distinct_x_cc_values, i_ds_rates_input, i_ds_rates, i_ds_source_totals, &
	i_ds_incr_alloc

uo_cr_allocations_cleanup	i_uo_cleanup
uo_cr_cost_repository i_uo_cr_cost_repository

longlong i_start_month, i_end_month, i_elim_start_month, i_elim_end_month


string i_exe_name = 'cr_allocations.exe'

longlong i_ds_alloc_list_row

string i_balancing_cv
string	i_description
longlong	i_balance_where_clause_id, i_where_clause_id, i_group_by_id, i_credit_id, i_dollar_quantity_indicator, &
	i_clearing_factor_start_month, i_clearing_factor_end_month, i_clearing_id, i_intercompany_accounting, i_intercompany_id, &
	i_dw_id, i_clearing_balance_start_month, i_clearing_Balance_end_month, i_run_by_department, i_auto_reverse, i_rate_id
boolean i_run_for_all_companies, i_have_results
string i_inter_co_sqls, i_external_company_id

longlong i_init_mo_fy

boolean i_fast_allocation

//MWA 201410 - Incremental code development
longlong i_incremental_allocation, i_curr_incr_run_id, i_curr_month_run_id
string i_source_value_field, i_incr_month_source_sql
uo_ds_top i_ds_where_clause_incr
boolean i_incr_run_field_exists
end variables

forward prototypes
public function integer uf_validate (longlong a_group_by_id, longlong a_target_id, longlong a_credit_id)
public function longlong uf_validations ()
public function longlong uf_start_allocations ()
public function longlong uf_auto_reverse ()
public function integer uf_loading2 (longlong a_month_number, longlong a_month_period)
public function longlong uf_run_by_department (longlong a_alloc_id, longlong a_mn, longlong a_mp, longlong a_clear_ind, longlong a_cr_co_id, string a_co_field, string a_gl_jcat, uo_ds_top a_ds_source, longlong a_group_by_id, string a_descr, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, boolean a_custom_dw)
public function longlong uf_derivations ()
public function integer uf_read ()
public function longlong uf_run ()
public function longlong uf_auto_delete_elim (longlong a_allocation_id, longlong a_month_number, longlong a_month_period, string a_run_as)
public function longlong uf_unlock (longlong a_allocation_id)
private function longlong usf_retrieve_where_clause (longlong a_where_clause_id)
private function longlong usf_retrieve_group_by (longlong a_group_by_id)
private function longlong usf_retrieve_target (longlong a_target_id)
private function longlong usf_retrieve_target (longlong a_target_id, string a_company)
private function string usf_build_where_clause_structure (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field)
public function decimal uf_clearing_balance (longlong a_cr_company_id, string a_charging_cost_center, ref decimal a_amount, ref decimal a_quantity)
public subroutine usf_get_variables ()
public subroutine usf_reset_variables ()
private function longlong usf_retrieve_credit (longlong a_credit_id)
private function longlong usf_retrieve_credit (longlong a_credit_id, string a_company)
public function string usf_get_external_company (longlong a_company_id)
public function longlong usf_retrieve_company (longlong a_company_id)
private function string usf_retrieve_where_clause_source (longlong a_where_clause_id)
public function string usf_build_where_clause_ds (longlong a_where_clause_id)
public function longlong usf_get_source_id (string a_table_name)
public function string usf_get_source_gljc (string a_table_name)
private function string usf_get_target_trpo_col (longlong a_target_id, longlong a_row_id, string a_col_name)
private function string usf_get_credit_trpo_col (longlong a_credit_id, longlong a_row_id, string a_col_name)
private function longlong usf_retrieve_interco_criteria (longlong a_intercompany_id)
public function decimal usf_lookup_rate (longlong a_rate_id, string a_company_value, longlong a_month_number)
public function longlong usf_rates_by_structure (longlong a_rate_id, ref longlong a_structure_id, ref string a_element)
public function decimal usf_lookup_rate_by_structure (longlong a_structure_id, string a_v_for_rates)
public function longlong usf_check_kickouts (string a_table_name)
public subroutine uf_save_report_sql (string a_allocation_sqls, boolean a_custom_dw, ref string a_where_clause, ref string a_group_clause)
public function string usf_build_where_clause (longlong a_where_clause_id, longlong a_start_month, longlong a_end_month, longlong a_month_period, longlong a_cr_company_id, string a_amount_types, string a_dept_value)
public function string uf_sources_and_targets (longlong a_group_by_id, string a_descr, longlong a_month_number, longlong a_month_period, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, longlong a_cr_company_id, string a_company_field, boolean a_custom_dw)
public function longlong uf_insert_target_ds (string a_datawindow, boolean a_custom_dw, string a_allocation_sqls, decimal a_clearing_balance, decimal a_clearing_balance_qty)
public function longlong uf_insert_target_direct (string a_datawindow, boolean a_custom_dw, string a_allocation_sqls, decimal a_clearing_balance, decimal a_clearing_balance_qty)
public function longlong uf_insert_credit_ds (boolean a_custom_dw)
public function longlong uf_insert_credit_direct (boolean a_custom_dw)
public function string uf_insert_build_sql_start ()
public function longlong uf_insert_target_direct_calc_rate (string a_allocation_sqls, decimal a_clearing_balance, decimal a_clearing_balance_qty, ref string a_amount_calc_sql, ref string a_quantity_calc_sql, ref string a_clearing_rate, ref string a_clearing_rate_qty)
public function string usf_build_where_clause_incr_range (longlong a_start_month, longlong a_end_month, longlong a_allocation_id, longlong a_cr_company_id)
public function longlong uf_update_cr (boolean a_reversal, longlong a_month_number, longlong a_month_period)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function integer uf_validate (longlong a_group_by_id, longlong a_target_id, longlong a_credit_id);//******************************************************************************************
//
//  Function     :  uf_validate
//
//  Description  :  Validate the group by/target/credit relationships.  If fields are 
//                  being not retained consistantly, the allocation will break.
//
//  Arguments    :  a_group_by_id
//                  a_target_id
//	                 a_credit_id
//
//  Returns      :   1 : if the relationships are valid
//                  -1 : otherwise
//
//******************************************************************************************
string sqls, str_value, col_name
longlong num_cols, r, c, element_id, counter, find_row, num_groups

num_groups = i_ds_group_by.Rowcount()

// 1. Target rows must exists.
if i_ds_target.rowcount() = 0 then
	if i_run_for_all_companies and i_clearing_indicator = 1 then 
		f_pp_msgs("ERROR: No target records exist for company " + i_external_company_id)
	else
		f_pp_msgs("ERROR: No target records exist")
	end if
	return -1
end if

// 2. For Clearing Calc Rate, exactly one target must exist
if i_ds_target.rowcount() <> 1 and i_clearing_indicator = 3 then
	f_pp_msgs("ERROR: Clearing Calc Rate allocations require a single target record!")
	
	if i_run_for_all_companies and i_clearing_indicator = 1 then 
		f_pp_msgs(string(i_ds_target.rowcount()) + " target records exist for company " + i_external_company_id)
	else
		f_pp_msgs(string(i_ds_target.rowcount()) + " target records exist")
	end if
	return -1
end if

// 3. A single credit line must exist.
if i_ds_credit.rowcount() <> 1 then
	f_pp_msgs("ERROR: A single credit entry is required!")
	if i_run_for_all_companies and i_clearing_indicator = 1 then f_pp_msgs("Company " + i_external_company_id)
	return -1
end if
	
	
//  1.  Group By / Target Relationship:
num_cols = long(i_ds_target.describe("datawindow.column.count"))
for r = 1 to i_ds_target.rowcount()
	for c = 1 to num_cols
		col_name     = upper(f_cr_clean_string(i_ds_target.Describe(" #" + string(c) + ".Name")))
		
		// Skip the columns we don't care about.  Get away from doing it based on the "j" variable to avoid confusion...
		choose case col_name
			case 'TARGET_ID', 'ROW_ID', 'TIME_STAMP', 'USER_ID', 'SORT_ORDER', 'RATE'
				continue
		end choose

		str_value = trim(i_ds_target.GetItemString(r, c))
		
		if str_value = "*" or pos(str_value, "*") > 0 then // Incrementally added the pos() for partial masking
			col_name = upper(trim(i_ds_target.Describe("#" + string(c) + ".Name")))
			
			// Get the element id
			find_row = i_ds_elements.Find("description = '" + col_name + "'",1,i_num_elements)
			if find_row <= 0 then
				f_pp_msgs("ERROR:  Unable to find element id during uf_validate")
				f_pp_msgs("Column: " + col_name)
				return -1
			end if
			element_id = i_ds_elements.GetItemNumber(find_row,'element_id')
			
			// is the element id in the group by
			find_row = i_ds_group_by.Find('group_by_id = ' + string(a_group_by_id) + ' and element_id = ' + string(element_id), 1, num_groups)
			
			if find_row <= 0 or isnull(find_row) then
				f_pp_msgs("ERROR: " + string(col_name) + " must be in the SOURCE GROUPING criteria. The allocation cannot run.")
				return -1
			end if
		end if
		
	next  //  for c = 1 to num_cols ...
next  //  for r = 1 to num_rows ...


//  If intercompany accounting is turned on, then the company field 
//  must be in the group_by.
if i_intercompany_accounting = 1 then
	for r = 1 to i_ds_elements.RowCount()
		col_name = i_ds_elements.GetItemString(r, "description")
		if col_name = i_company_field then
			element_id = i_ds_elements.GetItemNumber(r, "element_id")
			exit
		end if
	next

	// is the element id in the group by
	// i_ds_group_by filtered already from the uf_validate function.
	find_row = i_ds_group_by.Find('group_by_id = ' + string(i_group_by_id) + ' and element_id = ' + string(element_id), 1, num_groups)
	
	if find_row <= 0 or isnull(find_row) then
		f_pp_msgs(&
	   	        "This allocation requires intercompany transactions ... " + &
					  "The " + i_company_field + " field must be in the source grouping. ")
		if g_debug = 'YES' then f_pp_msgs('group by id = ' + string(i_group_by_id))
		if g_debug = 'YES' then f_pp_msgs('element id  = ' + string(i_group_by_id))
		return -1
	end if
end if

// ### 8826: JAK: 2013-03-22: 
// If they are using base limits, the base limit detail cannot be null.
if i_clearing_indicator = 3 and not isnull(i_base_limit) then
	if isnull(i_base_limit_detail) or trim(i_base_limit_detail) = '' then
		f_pp_msgs("The Base Limit Detail field is NULL ... No allocation will be calculated. You must enter the segments of the account key in a comma-separated list for which the base limit is to be applied.")
		rollback using sqlca;
		return -1
	end if 
end if

if i_run_by_department = 1 then
	if i_ds_target.getItemString(1, i_department_field) <> "*" then
		f_pp_msgs("The allocation is configured to 'run by department', but the target criteria for "+i_department_field+" does not equal a '*'.")
		rollback using sqlca;
		return -1
	end if
	if i_ds_credit.getItemString(1, i_department_field) <> "*" then
		f_pp_msgs("The allocation is configured to 'run by department', but the credit criteria for "+i_department_field+" does not equal a '*'.")
		rollback using sqlca;
		return -1
	end if
end if

// MWA 20141112 - Incremental allocation validations
if i_incremental_allocation = 1 then
	if i_clearing_indicator <> 1 and i_clearing_indicator <> 2 then
		f_pp_msgs("ERROR: Incremental allocations are only eligible for Loading and Clearing Input Rate allocations.")
		return -1
	end if
	
	if i_elim_flag = 1 then
		f_pp_msgs("ERROR: Allocations cannot be both incremental and eliminations.")
		return -1
	end if		
end if
		
return 1
end function

public function longlong uf_validations ();//*****************************************************************************************
//
//  Function     :  uf_validations
//
//  Description  :  Performs master elements and combo validations against the allocation
//                  staging tables.
//
//  Notes        :  All acctg. key elements are validated against their ME table.  That
//                  means they all must have a ME table.
//
//                  UPDATED FOR THE NEW PARTIAL VALIDATIONS TECHNIQUE.
//
//*****************************************************************************************
string sqls
longlong lvalidate_rtn
boolean validation_kickouts, mn_validation_kickouts

// Populate the interface batch id.
sqls = "update " + i_alloc_stg_table + " set interface_batch_id = '" + string(i_session_id) + "' " + &
		  "where allocation_id = " + string(i_allocation_id)
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating interface_batch_id: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if
	
//*****************************************************************************************
//
//	 VALIDATIONS:  Call the new validations user object.  It is assumed that combos have
//						already been created.
//
//*****************************************************************************************
if g_debug = 'YES' then f_pp_msgs("Analyzing staging table")
sqlca.analyze_table(i_alloc_stg_table)

if g_debug = 'YES' then f_pp_msgs(" -- Deleting from cr_validations_invalid_ids")
lvalidate_rtn = i_uo_cr_validation.uf_delete_invalid_ids(i_alloc_stg_table, string(i_session_id))
if lvalidate_rtn <> 1 then return -1

//  This is the earliest we can delete from this table since uf_delete_invalid_ids needs the
//  records to be in this table.
if i_concurrent_processing = "YES" then
	//  Remove the records from the kickouts table.
	lvalidate_rtn = i_uo_cr_validation.uf_delete_invalid_ids('cr_allocations_stg_kickouts', string(i_allocation_id))
	if lvalidate_rtn <> 1 then return -1

	delete from cr_allocations_stg_kickouts where allocation_id = :i_allocation_id;
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: deleting from cr_allocations_stg_kickouts: " + sqlca.SQLErrText)
		return -1
	end if
end if

// ### JAK: 2011-10-25:  New mn_validation_kickouts
mn_validation_kickouts = false
validation_kickouts = false

if i_combo_validations = "YES" then 
	if g_debug = 'YES' then f_pp_msgs(" -- Validating the Batch (Combos)")
		
	if i_combo_or_control = 'CONTROL' then
		lvalidate_rtn = i_uo_cr_validation.uf_validate_control(i_alloc_stg_table, string(i_session_id))
	else
		lvalidate_rtn = i_uo_cr_validation.uf_validate_combos(i_alloc_stg_table, string(i_session_id))
	end if
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate_(x).  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		lvalidate_rtn = usf_check_kickouts(i_alloc_stg_table)
		if lvalidate_rtn > 0 then
			validation_kickouts = true
		end if
	end if
end if

if i_proj_based_validations = "YES" then 
	if g_debug = 'YES' then f_pp_msgs(" -- Validating the Batch (Projects)")
	
	lvalidate_rtn = i_uo_cr_validation.uf_validate_projects(i_alloc_stg_table, string(i_session_id))
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		lvalidate_rtn = usf_check_kickouts(i_alloc_stg_table)
		if lvalidate_rtn > 0 then
			validation_kickouts = true
		end if
	end if
end if

if i_me_validations = "YES" then
	if g_debug = 'YES' then f_pp_msgs(" -- Validating the Batch (Element Values)")

	lvalidate_rtn = i_uo_cr_validation.uf_validate_me(i_alloc_stg_table, string(i_session_id))

	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if

	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		lvalidate_rtn = usf_check_kickouts(i_alloc_stg_table)
		if lvalidate_rtn > 0 then
			validation_kickouts = true
		end if
	end if
end if


if i_mn_validations = "YES" then
	if g_debug = 'YES' then f_pp_msgs(" -- Month Number validations")
	
	lvalidate_rtn = i_uo_cr_validation.uf_validate_month_number(i_alloc_stg_table, string(i_session_id))
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if
	
	if lvalidate_rtn = -2 or lvalidate_rtn = -3 then
		//  Always true here.  The MN validations are not inserted into the ids table since
		//  MN is not editable.  However, Acct Range validations ARE inserted.  Since we don't
		//  know which one kicked, we just have to assume TRUE.
		validation_kickouts = true
		
		// ### JAK: 2011-10-25:  New mn_validation_kickouts
		if lvalidate_rtn = -2 then mn_validation_kickouts = true
	end if
end if


if i_custom_validations = "YES" then
	if g_debug = 'YES' then f_pp_msgs(" -- Custom validations")
	
	lvalidate_rtn = i_uo_cr_validation.uf_validate_custom(i_alloc_stg_table, string(i_session_id))

	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	end if

	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		// Custom validations may not write to the kickout tables
		validation_kickouts = true
	end if
end if

// If there are month number kickouts, we cannot use suspense accounting and must kickout like a normal validation
if mn_validation_kickouts then i_suspense_accounting = 'NO'

// Apply suspense account if it is turned on and there are validation kickouts
if validation_kickouts and i_suspense_accounting = "YES" then
	//  Call the suspense accounting function.  It will return a value of 2 if 
	//  suspense accounting is turned off in cr_system_control.
	i_uo_cr_validation.i_called_from_allocations_suspense = true
	lvalidate_rtn = i_uo_cr_validation.uf_suspense(i_alloc_stg_table, string(i_session_id))
	i_uo_cr_validation.i_called_from_allocations_suspense = false
	
	if lvalidate_rtn < 0 then
		//  Error in uf_validate.  Logged by the function.
		return -1
	else
		i_suspense_processed = true  //  For msg at the end of the logs.
	end if
end if

// Remove the interface batch id that we currently have on the table.
sqls = "update " + i_alloc_stg_table + " set interface_batch_id = NULL " + &
	 "where allocation_id = " + string(i_allocation_id)
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating " + i_alloc_stg_table + ".interface_batch_id to NULL: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if

// Handle the kickouts now.
if validation_kickouts and i_suspense_accounting <> 'YES' then
	if i_concurrent_processing = "YES" then
		// If concurrent processing, the staging tables are global temp.
		// Need to move the records to cr_allocations_stg_kickouts so the records are saved and can be reviewed.
		sqls = "insert into cr_allocations_stg_kickouts " + &
				 "select * from " + i_alloc_stg_table + " where allocation_id = " + string(i_allocation_id)
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: inserting into cr_allocations_stg_kickouts: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			//  Continue on to the message.
		end if
		
		sqls = "update cr_allocations_stg_kickouts set interface_batch_id = '" + string(i_allocation_id) + "' " + &
				 "where allocation_id = " + string(i_allocation_id)
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_allocations_stg_kickouts.interface_batch_id: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("sqls = " + sqls)
			f_pp_msgs("  ")
			//  Continue on to the message.
		end if
		
		sqls = "update cr_validations_invalid_ids2 set table_name = 'cr_allocations_stg_kickouts' " + &
				  "where id in (select id from " + i_alloc_stg_table + " where allocation_id = " + string(i_allocation_id) + ")"
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_validations_invalid_ids2.table_name: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("sqls = " + sqls)
			f_pp_msgs("  ")
			//  Continue on to the message.
		end if
		
		sqls = "update cr_validations_invalid_ids set table_name = 'cr_allocations_stg_kickouts' " + &
				  "where id in (select id from " + i_alloc_stg_table + " where allocation_id = " + string(i_allocation_id) + ")"
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_validations_invalid_ids.table_name: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("sqls = " + sqls)
			f_pp_msgs("  ")
			//  Continue on to the message.
		end if

		//  We have to commit here for these to stick ... uf_loading is about to fire a rollback.
		commit;
	end if
	
	f_pp_msgs("  ")
	f_pp_msgs("   -----------------------------------------------------------------------" + &
		"   -------------------------------------------------------------")
	f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE ALLOCATION WILL NOT RUN!  *****")
	f_pp_msgs("   -----------------------------------------------------------------------" + &
		"   -------------------------------------------------------------")
	f_pp_msgs("  ")
	
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	return -2
end if

if g_debug = 'YES' then
	f_pp_msgs("Validations complete")
end if

return 1
end function

public function longlong uf_start_allocations ();//******************************************************************************************
//
//  User Object Function:  uf_start_allocations
//
//
//
//******************************************************************************************
integer  rtn, whole_month_int
string   sqls, co_sqls, s_date, test_or_actual, incr_sql_msg
longlong	num_allocs, i, j, month_number_check, lookup_month_number,&
		num_cos, c, counter, incr_rows, m, &
		incr_source_month_number,	incr_run_month_number, &
		incr_min_source_value, incr_max_source_value
datetime msg_dt
date     ddate
time     ttime

if i_whole_month then i_month_period = 0


// MN / MP set by the caller

//*****************************************************************************
//  Get a list of the allocation types, sorted by priority.
//  Also retrieve the cr_alloc_process_control table to determine which 
//  months and periods have not had the allocations ...
//*****************************************************************************
// i_ds_alloc_list already has the data from uf_run and the row = i_ds_alloc_list_row
i = i_ds_alloc_list_row
usf_get_variables() // retrieve allocation values into instance variables.

//  Clear the staging table before we start in case records got committed to the table during a prior run.
sqlca.truncate_table(i_alloc_stg_table)

// Filter the company DS
num_cos = usf_retrieve_company(i_cr_company_id)

// Add another call to custom function after allocation_id is added to global variable.
// 	As of standard interface shell, allocation_id is referenced as g_uo_client.i_allocation_id
rtn = f_cr_allocations_custom('wf_run_alloc_id_loop')
if rtn <> 1 then
	return -1 
end if

i_have_results = false // no results to start with
for c = 1 to num_cos
	//  Temporarily replace the cr_company_id on the allocation so uf_loading2
	//  will run the allocation for just that company.
	setnull(i_cr_company_id)
	i_cr_company_id = i_ds_company.GetItemNumber(c, "cr_company_id")
		
	// Get the company name.
	i_external_company_id = usf_get_external_company(i_cr_company_id)

	// Run the allocation
	rtn = uf_loading2(i_month_number, i_month_period)

	//  If the rtn code is < 0 then a bad error happened ... rollback the 
	//  transactions and exit out ... 0 is OK since that just means neutral
	//  things like no dollars in the pot ... 1 means the allocation ran
	//  successfully.
	if rtn < 0 then
		f_pp_msgs("    Allocation results cannot be calculated!")
		rollback using sqlca;
		return rtn
	elseif rtn > 0 then
		i_have_results = true  // if any company had results, we have results and need to build the summary below.
	else
		f_pp_msgs("    No allocation results will be calculated for this company.")
	end if
	
	if num_cos > 1 and c <> num_cos then
		// We will be running through uf_loading2 multiple times with the records still in the staging table.  Don't want interference between the
		//	multiple runs.  Change the allocation id on the previously processed records to -1 * allocation id, will set them back after we are done 
		//	with the entire loop.
		sqls = "update " + i_alloc_stg_table + " set allocation_id = -1 * allocation_id where allocation_id = " + string(i_allocation_id)
		execute immediate :sqls using sqlca;

		if sqlca.SQLCode <> 0 then
			f_pp_msgs("ERROR: Updating staging_table for allocation_id (run for all companies): " + sqlca.SQLErrText)
			f_pp_msgs("SQL: " + sqls)
			return -1
		end if
	end if
	
next  //  for c = 1 to num_cos ...

if i_have_results then
	
	if num_cos > 1 then
		// We will be running through uf_loading2 multiple times with the records still in the staging table.  Don't want interference between the
		//	multiple runs.  Change the allocation id on the previously processed records to -1 * allocation id, will set them back after we are done 
		//	with the entire loop.
		sqls = "update " + i_alloc_stg_table + " set allocation_id = -1 * allocation_id where allocation_id = -1 * " + string(i_allocation_id)
		execute immediate :sqls using sqlca;

		if sqlca.SQLCode <> 0 then
			f_pp_msgs("ERROR: Updating staging_table for allocation_id pass 2 (run for all companies): " + sqlca.SQLErrText)
			f_pp_msgs("SQL: " + sqls)
			return -1
		end if
	end if
	
	// Update cr_cost_repository after the entire allocation is posted.
	rtn = uf_update_cr(false, i_month_number, i_month_period)
	
	if rtn < 0 then
		return rtn
	end if
end if

//*************************************************************
//  The commit and rollback are managed here, not in any
//  other function ...
//*************************************************************
s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))
msg_dt = datetime(ddate, ttime)
g_finished_at = msg_dt

if rtn >= 0  then
	//  Update cr_alloc_process_control tables as the allocation ran successfully...  
	f_pp_msgs("Updating allocation audit tables ...")
	if i_whole_month then
		whole_month_int = 1
	else
		whole_month_int = 0
	end if
	
	if i_run_as_a_test then
		test_or_actual = 'TEST'
	else
		test_or_actual = 'ACTUAL'
	end if
	
	// MWA 20141103 - Incremental allocation process logging
	if i_incremental_allocation = 1 then
		
		incr_rows = i_ds_where_clause_incr.RowCount()
		
		for m = 1 to incr_rows
			
			incr_source_month_number  =	i_ds_where_clause_incr.GetItemNumber(m,"source_month_number")
			incr_run_month_number		 =	i_ds_where_clause_incr.GetItemNumber(m,"run_month_number")
			incr_min_source_value		 =	i_ds_where_clause_incr.GetItemNumber(m,"min_source_value")
			incr_max_source_value		 =	i_ds_where_clause_incr.GetItemNumber(m,"max_source_value")
			
			// Track all of the incremental runs' details
			insert into cr_alloc_incremental_control
				(incremental_Run_id, allocation_id, month_run_id, 
				 source_month_number,
				 run_month_number, 
				 min_source_value, 
				 max_source_value, 
				 source_table_name, source_sql, finished, incremental_status_id) 
			values (:i_curr_incr_run_id, :i_allocation_id, :i_curr_month_run_id,
				:incr_source_month_number,
				:incr_run_month_number,
				:incr_min_source_value,
				:incr_max_source_value,
				:i_source_table_name, :i_source_sqls, :msg_dt, 1);
			if sqlca.SQLCode = 0 then
				commit;
				if g_debug = "YES" then
					f_pp_msgs("Inserted "+string(sqlca.sqlnrows)+" row into cr_alloc_incremental_control for allocation id " + string(i_allocation_id))
				end if
			else
				f_pp_msgs("ERROR: inserting cr_alloc_incremental_control " + sqlca.SQLErrText)
				rollback;
			end if
			
		next 
		
		// Use the PK to delete prior run records. If it's the first run of the month it will delete 0 rows
		delete from cr_alloc_process_control
		where allocation_id = :i_allocation_id
			and month_number = :i_month_number
			and test_or_actual = :test_or_actual
			and month_period = :i_month_period;
		if sqlca.SQLCode = 0 then
			commit;
		else
			f_pp_msgs("ERROR: deleting incremental record from cr_alloc_process_control " + sqlca.SQLErrText)
			rollback;
		end if
		
		//Log something into the cr_process_control table so that rev/undo/delete can find the charges appropriately	
		insert into cr_alloc_process_control
			(allocation_id, month_number, month_period, finished, 
			 source_sql, test_or_actual, whole_month, run_as_commitments,
			 source_table_name, balance_table_name)
		values
			(:i_allocation_id, :i_month_number, :i_month_period, :msg_dt, 
			 :i_incr_month_source_sql, :test_or_actual, :whole_month_int, :i_run_as_commitments_flag,
			 :i_source_table_name, :i_balance_table_name);
		if sqlca.SQLCode = 0 then
			commit;
		else
			f_pp_msgs("ERROR: inserting incremental record into cr_alloc_process_control " + sqlca.SQLErrText)
			rollback;
		end if
			
		// Set the undone and reprocessed incremental runs as such
		// This should never ever occur during the first run of the month so don't use it then
		update cr_alloc_incremental_control
		set incremental_rerun_id = :i_curr_incr_run_id, rerun_date = :msg_dt, incremental_status_id = 4
		where incremental_status_id = 2
			and allocation_id = :i_allocation_id
			and run_month_number = :i_month_number;

		if sqlca.SQLCode = 0 then
			commit;
				if g_debug = "YES" then
					f_pp_msgs("Updated "+string(sqlca.sqlnrows)+" row(s) on cr_alloc_incremental_control for allocation id " + string(i_allocation_id)+", run month number "+string(i_month_number)+" from status 2 to status 4.")
				end if
		else
			f_pp_msgs("ERROR: updating incremental_rerun_id " + sqlca.SQLErrText)
			rollback;
		end if
			
		
	else //if i_incremental_allocation <> 1...old school
	
		insert into cr_alloc_process_control
			(allocation_id, month_number, month_period, finished, 
			 source_sql, test_or_actual, whole_month, run_as_commitments,
			 source_table_name, balance_table_name)
		values
			(:i_allocation_id, :i_month_number, :i_month_period, :msg_dt, 
			 :i_source_sqls, :test_or_actual, :whole_month_int, :i_run_as_commitments_flag,
			 :i_source_table_name, :i_balance_table_name);
			 
		if sqlca.SQLCode = 0 then
			commit;
		else
			f_pp_msgs("ERROR: inserting cr_alloc_process_control " + sqlca.SQLErrText)
			rollback;
		end if
		
		insert into cr_alloc_process_control2
			(allocation_id, month_number, month_period, balance_sql, test_or_actual)
		values
			(:i_allocation_id, :i_month_number, :i_month_period, :i_balance_sqls, :test_or_actual);
			
		if sqlca.SQLCode = 0 then
			commit;
		else
			f_pp_msgs("ERROR: inserting cr_alloc_process_control2 " + sqlca.SQLErrText)
			rollback;
		end if
		
		insert into cr_alloc_process_control3
			(allocation_id, month_number, month_period, grouping_sql, test_or_actual)
		values
			(:i_allocation_id, :i_month_number, :i_month_period, :i_grouping_sqls, :test_or_actual);
			
		if sqlca.SQLCode = 0 then
			commit;
		else
			f_pp_msgs("ERROR: inserting cr_alloc_process_control3 " + sqlca.SQLErrText)
			rollback;
		end if
		
	end if //if incremental or not
	
else
	rollback;
end if

// MWA 20141104
i_ds_where_clause_incr.reset()

//  For performance:
sqlca.truncate_table(i_alloc_stg_table)

f_pp_msgs("Finished: " + i_description + " at " + string(msg_dt))
f_pp_msgs("*******************************************")

return rtn
end function

public function longlong uf_auto_reverse ();//*******************************************************************************************
//
//  User Object Function  :  uf_auto_reverse
//
//  Arguments    :  none
//
//  Returns      :  longlong (1=good return, -1=error)
//
//  Description  :  This script will book entries reversing the results of an 
//                  allocation from the cost repository.  
//
//*******************************************************************************************
longlong allocation_row, allocation_id, rtn, month_number, month_period, counter, &
		 month_row, i, source_id, max_id, &
		 run_month_number, run_month_period
string sqls, element_descr, table_name, field

month_number       = i_month_number
run_month_period   = i_month_period
allocation_id      = i_allocation_id

// Always reverse the previous month, unless we are in January then do not reverse
//  ### 29232 JAK: 2013-02-05:  Code to support non-calendar fiscal years
if long(mid(string(i_month_number),5,2)) = i_init_mo_fy then
	f_pp_msgs("Auto Reverse: This allocation is running for the first month of the fiscal year.  " + &
		"The prior month will not be reversed.")
	return 1
end if

run_month_number = i_month_number - 1
if mid(string(run_month_number),5,2) = '00' then run_month_number = run_month_number - 88

month_period = 0

if i_clearing_indicator = 4 then
	if i_test_or_actual = "ACTUAL" then
		select max(month_period) into :month_period from cr_inter_company
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number;
	else
		select max(month_period) into :month_period from cr_inter_company_test
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number;
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		select max(month_period) into :month_period from cr_allocations 
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number;
	else
		select max(month_period) into :month_period from cr_allocations_test
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number;
	end if
end if

if isnull(month_period) then month_period = 0

month_period++


//  If the allocation has not run for this month and period, there is nothing to do ...
//  However, if the month and period are not found, we must first check of a period 0
//  in case they ran the allocation on a "whole month" basis.
counter = 0

/*### - MDZ - 9058 - 20120119*/
/*Added trim to upper(test_or_actual) to be consistent.*/
select count(*) into :counter
  from cr_alloc_process_control 
 where allocation_id         = :allocation_id and
 		 month_number          = :run_month_number  and
		 month_period          = :run_month_period  and
		 upper(trim(test_or_actual)) = :i_test_or_actual;

if counter <= 0 then

	run_month_period = 0
	counter          = 0
	
	/*### - MDZ - 9058 - 20120119*/
	/*Added trim to upper(test_or_actual) to be consistent.*/
	select count(*) into :counter
	  from cr_alloc_process_control 
	 where allocation_id         = :allocation_id and
 			 month_number          = :run_month_number and
			 month_period          = :run_month_period and
			 upper(trim(test_or_actual)) = :i_test_or_actual;
	
	if counter <= 0 then
		f_pp_msgs("Auto Reverse: This allocation has not been run for month " + &
			string(run_month_number) + ", period " + string(run_month_period) + &
				".  Nothing to reverse.")
		return 1
	end if
	
end if


//  If the allocation has already been reversed, do not let them reverse it again ...
counter = 0

/*### - MDZ - 9058 - 20120119*/
/*Added trim to upper(test_or_actual) to be consistent.*/
select reversed into :counter
  from cr_alloc_process_control 
 where allocation_id         = :allocation_id     and
 		 month_number          = :run_month_number  and
		 month_period          = :run_month_period  and
		 upper(trim(test_or_actual)) = :i_test_or_actual;
if isnull(counter) then counter = 0

if counter > 0 then
	f_pp_msgs("Auto Reverse: This allocation has already been reversed.")
	return 1
end if

//  BEFORE FIRST:  Flush the temporary holding table ...
// can't truncate due to commits.
delete from cr_reversals_manual;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: deleting from cr_reversals_manual~n~n" + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;
end if


//  FIRST:  Insert records from the cr_allocations or cr_allocations_test table ... or the
//          appropriate inter-company table ... to the cr_reversals_manual table.
//          The "reversal_id is NULL" is EXTREMELY important.  A freshly run allocation
//          will have a NULL reversal_id.  A reversed or undone allocation will have
//          a 0 reversal_id in the original allocation results and a cr_allocations.id
//          in the reversal_id for the reversal records.  The "is NULL" prevents us
//          from undoing the same allocation more than once!
if i_clearing_indicator = 4 then
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then f_pp_msgs( &
			"Auto Reverse: Inserting cr_inter_company records into cr_reversals_manual ...")
		insert into cr_reversals_manual
			(select * from cr_inter_company
		     where month_number = :run_month_number and 
	   	        month_period = :run_month_period and 
					  nvl(interface_batch_id,0) = 0    and
   	   	     allocation_id = :allocation_id   and
					  reversal_id is NULL);
	else
		if g_debug = 'YES' then f_pp_msgs( &
			"Auto Reverse: Inserting cr_inter_company_test records into cr_reversals_manual ...")
		insert into cr_reversals_manual
			(select * from cr_inter_company_test
		     where month_number = :run_month_number and 
	   	        month_period = :run_month_period and 
					  nvl(interface_batch_id,0) = 0    and
   	   	     allocation_id = :allocation_id   and
					  reversal_id is NULL);
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then f_pp_msgs( &
			"Auto Reverse: Inserting cr_allocations records into cr_reversals_manual ...")
		insert into cr_reversals_manual
			(select * from cr_allocations
			  where month_number = :run_month_number and 
	   		     month_period = :run_month_period and 
					  nvl(interface_batch_id,0) = 0    and
   	   		  allocation_id = :allocation_id   and
					  reversal_id is NULL);
	else
		if g_debug = 'YES' then f_pp_msgs( &
			"Auto Reverse: Inserting cr_allocations_test records into cr_reversals_manual ...")
		insert into cr_reversals_manual
			(select * from cr_allocations_test
		     where month_number = :run_month_number and 
		           month_period = :run_month_period and 
					  nvl(interface_batch_id,0) = 0    and
   		        allocation_id = :allocation_id   and
					  reversal_id is NULL);	
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	if i_clearing_indicator = 4 then
		if i_test_or_actual = "ACTUAL" then
			f_pp_msgs("ERROR: Auto Reverse: inserting from cr_inter_company: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: Auto Reverse: inserting from cr_inter_company_test: " + sqlca.SQLErrText)
		end if
	else
		if i_test_or_actual = "ACTUAL" then
			f_pp_msgs("ERROR: Auto Reverse: inserting from cr_allocations: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: Auto Reverse: inserting from cr_allocations_test: " + sqlca.SQLErrText)
		end if
	end if
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  BEFORE SECOND:  Update cr_reversals_manual ... and insert back
//                  into the appropriate table.  The reversal_id field
//                  allows us to keep track of the reversals against their
//                  original transaction.  Then if they ever delete the allocation,
//                  the reversal will be deleted also instead of being stranded.
update cr_reversals_manual
   set amount = -amount, quantity = -quantity, reversal_id = id,
	    month_number = :month_number, month_period = :month_period,
		 dr_cr_id = -dr_cr_id;
	
if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating cr_reversals_manual.amount: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

update cr_reversals_manual
   set target_credit = 'RVTARG'
 where upper(rtrim(target_credit)) = 'TARGET';

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating cr_reversals_manual.target: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

update cr_reversals_manual
   set target_credit = 'RVCRED'
 where upper(rtrim(target_credit)) = 'CREDIT';

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating cr_reversals_manual.target: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

update cr_reversals_manual
   set target_credit = 'RVINCO'
 where upper(rtrim(target_credit)) = 'INT_CO';

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating cr_reversals_manual.int_co: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

update cr_reversals_manual set drilldown_key = drilldown_key || target_credit;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating cr_reversals_manual.drilldown_key: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  Change the (PK) ...
update cr_reversals_manual set id = crdetail.nextval;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating (PK): " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  INSERT the reversal ...
if i_clearing_indicator = 4 then
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then f_pp_msgs("Auto Reverse: Inserting cr_inter_company records ...")
		insert into cr_inter_company (select * from cr_reversals_manual);
	else
		if g_debug = 'YES' then f_pp_msgs("Auto Reverse: Inserting cr_inter_company_test records ...")
		insert into cr_inter_company_test (select * from cr_reversals_manual);
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then f_pp_msgs("Auto Reverse: Inserting cr_allocations records ...")
		insert into cr_allocations (select * from cr_reversals_manual);
	else
		if g_debug = 'YES' then f_pp_msgs("Auto Reverse: Inserting cr_allocations_test records ...")
		insert into cr_allocations_test (select * from cr_reversals_manual);	
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	if i_clearing_indicator = 4 then
		if i_test_or_actual = "ACTUAL" then
			f_pp_msgs("ERROR: Auto Reverse: inserting to cr_inter_company: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: Auto Reverse: inserting to cr_inter_company_test: " + sqlca.SQLErrText)
		end if
	else
		if i_test_or_actual = "ACTUAL" then
			f_pp_msgs("ERROR: Auto Reverse: inserting to cr_allocations: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: Auto Reverse: inserting to cr_allocations_test: " + sqlca.SQLErrText)
		end if
	end if
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  SECOND:  Update cr_cost_repository ...
rtn = uf_update_cr(true, month_number, month_period)

if rtn < 0 then 
	rollback;
	return rtn
end if

//  THIRD:  Update the reversed flag on w_cr_alloc_process_control ...
if g_debug = 'YES' then f_pp_msgs("Auto Reverse: Update the reversed flag on w_cr_alloc_process_control ...")
	
/*### - MDZ - 9058 - 20120119*/
/*Added trim to upper(test_or_actual) to be consistent.*/
update cr_alloc_process_control 
   set reversed = 1
 where allocation_id         = :allocation_id and
 		 month_number          = :run_month_number  and
		 month_period          = :run_month_period  and
		 upper(trim(test_or_actual)) = :i_test_or_actual;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating reversed flag: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_elements
	return -1
end if


commit;



//  FOURTH:  Update the reversal_id to 0 for the original allocation records.

//  08/17/1999 - changed the delete statements below ... they all had
//               "select * from cr_allocations" ...

if g_debug = 'YES' then f_pp_msgs( &
	"Auto Reverse: Update the reversal_id to 0 for the original allocation records ...")
if i_clearing_indicator = 4 then
	if i_test_or_actual = "ACTUAL" then
		update cr_inter_company set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 nvl(interface_batch_id,0) = 0    and
             reversal_id is null and 
             id in (select reversal_id from cr_inter_company 
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									nvl(interface_batch_id,0) = 0 and
							      reversal_id is not null);
	else
		update cr_inter_company_test set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 nvl(interface_batch_id,0) = 0    and
             reversal_id is null and 
             id in (select reversal_id from cr_inter_company_test 
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									nvl(interface_batch_id,0) = 0 and
							      reversal_id is not null);
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		update cr_allocations set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 nvl(interface_batch_id,0) = 0    and
             reversal_id is null and 
             id in (select reversal_id from cr_allocations 
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									nvl(interface_batch_id,0) = 0 and
							      reversal_id is not null);
	else
		update cr_allocations_test set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 nvl(interface_batch_id,0) = 0    and
             reversal_id is null and 
             id in (select reversal_id from cr_allocations_test 
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									nvl(interface_batch_id,0) = 0 and
							      reversal_id is not null);
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Auto Reverse: updating reversal_id to 0: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
//	DESTROY ds_elements
	return -1
end if


commit;


//
//  Must clean up after ourselves ...
//
delete from cr_reversals_manual;
commit;

//DESTROY ds_elements

if g_debug = 'YES' then f_pp_msgs("Auto Reverse: Done with reversal.")

return 1

end function

public function integer uf_loading2 (longlong a_month_number, longlong a_month_period);string sqls, field, col_name, insert_sqls, element_descr, insert_sqls_start, value, &
		 source_value, credit_sqls, credit_group_sqls, table_name, &
		 interco_credit_sql, interco_trigger_col_val, &
		 interco_company_val, retrieved_company_val, string_row_id, &
		 interco_company, interco_flex_value, cross_company_val, &
		 datawindow, &
		 trpo_col_name, fixed_value, &
		 balancing_sqls, rtn_str, allocation_sqls
longlong num_rows, i, id, row_id, &
		 num_cols, j, num_inserts, k, num_credit_cols, z, source_id, &
		 rtn, &
		 interco_pk_id, &
		 counter, &
		 rtn_code, cleanup_long_array_arg[], &
		 ow_id, seq_val
decimal  {2} clearing_balance, clearing_balance_qty, interco_total, credit_amount
boolean      custom_dw
any id_array[]
longlong id_array_pos


DECLARE credit_cursor DYNAMIC CURSOR FOR SQLSA ;

// Validate a valid clearing_indicator
if i_clearing_indicator < 1 or i_clearing_indicator > 6 then
	f_pp_msgs("ERROR:  The value of the Clearing Indicator is not valid.  Valid values are 1 (Loading), 2 (Clearing - Input Rate), 3 (Clearing - Calc Rate), 4 (Intercompany - Trigger), and 6 (Capital Loading)")
	return -1
end if

if isnull(i_cr_company_id) then
	f_pp_msgs("Running the allocation ... " + upper(i_description))
else
	f_pp_msgs("Running the allocation ... " + upper(i_description) + ' for Company ' + i_external_company_id)
end if

// Begin building the SQL INSERT ...

if i_dw_id > 0 then
	custom_dw = true
	if i_clearing_indicator = 3 then i_clearing_indicator = 1
	select datawindow into :datawindow
	  from cr_alloc_custom_dw where dw_id = :i_dw_id;
else
	custom_dw = false
	datawindow = 'dw_temp_dynamic'
end if


// Retrieve the targets, credits, and group by
if i_run_for_all_companies and i_clearing_indicator = 1 then 
	//  "Run for all Companies" ... only for loadings.  Clearing-Calc rate already works for
	//  10000 using the code in the "else".  Don't destroy that.	
	rtn = usf_retrieve_target(i_target_id, i_external_company_id)
	rtn = usf_retrieve_credit(i_credit_id, i_external_company_id)
else
	//  Leave this alone so we don't harm any other existing client allocations.
	rtn = usf_retrieve_target(i_target_id)
	rtn = usf_retrieve_credit(i_credit_id)
end if
rtn = usf_retrieve_group_by(i_group_by_id)

//  VERIFY the grouping/target and target/credit relationships ... if there are fields
//  being retained in the targets or credits that are not retained in the group by or
//  target, the allocation will break.
rtn = uf_validate(i_group_by_id, i_target_id, i_credit_id)

if rtn <> 1 then
	return -1
end if

// JAK: 2014-07-23:  Can we perform this as a fast allocation
// TODO
i_fast_allocation = false
//i_fast_allocation = uf_check_fast_allocation(i_run_for_all_companies, i_clearing_indicator, i_dw_id, i_target_id)

//********************************
//
//  Reverse last month:  
//
//********************************
if i_auto_reverse = 1 then
	f_pp_msgs("Auto Reverse = YES ... reversing last month's entry")
	rtn_code = uf_auto_reverse()
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	if rtn_code < 0 then return rtn_code
	
	f_pp_msgs("Auto Reverse complete")
end if


//********************************
//  A Type 3 allocation "Clearing - Calc Rate" requires a pot of dollars to be held
//  in a separate variable.
//********************************
if i_clearing_indicator = 3 then		
	// No clearing department here...
	rtn = uf_clearing_balance(i_cr_company_id, '', clearing_balance, clearing_balance_qty)
			
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	if rtn < 0 then return rtn
		
	if clearing_balance = 0 and clearing_balance_qty = 0 then	
		if isnull(i_cr_company_id) then
			f_pp_msgs("The balance to be cleared is 0.")		
		else
			f_pp_msgs("The balance to be cleared is 0. Company = " + i_external_company_id + ".")		
		end if
		return 0
	end if
end if


//**********************************************************
//
//  Pull source
//
//**********************************************************
rtn_str = &
	uf_sources_and_targets(i_group_by_id, i_description, a_month_number, a_month_period, &
		i_where_clause_id, i_clearing_id, i_work_order_field, i_cr_company_id, i_company_field, custom_dw)

if rtn_str = 'ERROR' then return -1
allocation_sqls = rtn_str


//**********************************************************
//
//  Targets
//
//**********************************************************
i_ds_source.reset()
f_pp_msgs("    Inserting Targets")
// Call fast allocations first.  Will return -2 if the allocation is ineligible
rtn = uf_insert_target_direct(datawindow, custom_dw, allocation_sqls, clearing_balance, clearing_balance_qty)
if rtn = -2 then rtn = uf_insert_target_ds(datawindow, custom_dw, allocation_sqls, clearing_balance, clearing_balance_qty)

if rtn < 0 then
	// Error logged in function
	return rtn
else
	// rtn stores the number of target records generated.
	//  If there are no source records, then scram ...
	if rtn = 0 then
		if isnull(i_cr_company_id) then
			f_pp_msgs("    There are no records that meet the source criteria.")	
		else
			f_pp_msgs("    There are no records that meet the source criteria for company " + i_external_company_id + ". ")
		end if
		
		return 0
	end if
end if


//*****************************************************************************************
//
//  Run by Deparment Flag:
//
//  3/18/09: Call here if the system switch is turned on to obey the Credit Criteria.
//  Otherwise, call from its original place below.
//
//*****************************************************************************************
if i_run_by_dept_credits_method = "YES" then
	if i_run_by_department = 1 and i_clearing_indicator = 3 then
		//
		//  Call the function that calculates departmental-based clearings ...
		//
		rtn = uf_run_by_department(i_allocation_id, i_month_number, i_month_period, &
			i_clearing_indicator, i_cr_company_id, i_company_field, i_gl_journal_category, i_ds_source, &
			i_group_by_id, i_description, i_where_clause_id, i_clearing_id, i_work_order_field, custom_dw)
		
		if rtn <> 1 then
			f_pp_msgs("ERROR: in the Run by Department logic")
			return -1
		end if
	end if
end if


//*********************************************************************
//
//  Check to see if targets were created:
//    Otherwise, the credits will throw an error since they will not
//    be able to find any records.
//
//*********************************************************************
counter = 0
sqls = 'select count(*) from ' + i_alloc_stg_table
sqls += ' where allocation_id = ' + string(i_allocation_id)
sqls += ' and month_number = ' + string(i_month_number)
sqls += " and target_credit = 'TARGET' and rownum = 1"

rtn = i_ds_count.SetSQLSelect(sqls)
rtn = i_ds_count.RETRIEVE()

if rtn <> 1 then 
	f_pp_msgs("ERROR: verifying that TARGET records were created: " + i_ds_count.i_sqlca_SQLErrText)
	f_pp_msgs('SQL: ' + sqls)
	return -1
end if

counter = i_ds_count.GetItemNumber(1,1)
if counter = 0 then
	f_pp_msgs("No TARGET records written.")
	return 0
end if


//***********************************
//
//  Credits:
//
//***********************************
f_pp_msgs("    Inserting Credits")

// Call fast allocations first.  Will return -2 if the allocation is ineligible
rtn = uf_insert_credit_direct(custom_dw)
if rtn = -2 then rtn = uf_insert_credit_ds(custom_dw)

if rtn < 0 then
	// Error logged in function
	return rtn
end if

//*****************************************************************************************
//
//  Inter-Company:  Receivable and Payable
//
//                  Use the "credit" variables in this code like credit_sqls and
//                  credit_group_sqls to avoid extra variables.
//
//*****************************************************************************************
if i_intercompany_accounting = 1 then
	insert_sqls_start = uf_insert_build_sql_start()
	interco_total = 0
	i_ds_interco_insert.reset()
	
	num_rows = usf_retrieve_interco_criteria(i_intercompany_id)

	//  Build the insert statement and perform the insert ...
	for i = 1 to num_rows				//  Loop over intercompany criteria records ...

		credit_sqls       = "select "
		credit_group_sqls = ""
	
		string_row_id   = upper(trim(i_ds_interco.GetItemString(i, "row_id")))
		interco_company = i_ds_interco.GetItemString(i, i_company_field)
		interco_pk_id   = i_ds_interco.GetItemNumber(i, "id")


		for j = 1 to i_num_elements		//  Loop over the code block to build the REC./PAY.
		
			element_descr = i_ds_elements.GetItemString(j, "description")
		
			value = upper(i_ds_interco.GetItemString(i, element_descr))
		
			//  COMPANY field must have a * ...  NO LONGER ... all companies are in the table
			if element_descr = i_company_field then
				credit_sqls       = credit_sqls + '"' + element_descr + '",'
				credit_group_sqls = credit_group_sqls + '"' + element_descr + '",'
			else
				choose case value
					case "*"  //  Get the field from the target records (put the field name in the
								 //  select and group by of the sql ...
						credit_sqls       = credit_sqls + '"' + element_descr + '",'
						credit_group_sqls = credit_group_sqls + '"' + element_descr + '",'
				
					case else
						//  Need the value and the column alias for this SQL so the interco DS has
						//  the trigger column named correctly.
						credit_sqls = credit_sqls + &
						              "'" + value + "' " + '"' + element_descr + '"' + ","
				end choose
			end if
		
		next  //  for j = 1 to num_elements ... Loop over the code block to build the credits

		//  The newly inserted allocations will have a NULL drilldown_key ... add the 
		//  where clause and group by ...
		
		if isnull(credit_group_sqls) then credit_group_sqls  = ''
		
		// Always need to group by MN, MP, target_credit, and company
		credit_sqls += "month_number, month_period, target_credit, cross_charge_company, -sum(amount) "
		credit_group_sqls += " month_number, month_period, target_credit, cross_charge_company"
															
		credit_sqls +=  " from " + i_alloc_stg_table + " where drilldown_key is null "
		credit_sqls +=  " and allocation_id = " + string(i_allocation_id)
		credit_sqls +=  " group by " + credit_group_sqls
	
		//  Insert the inter-company receivable or payable entry ... 
		//    Records will be retrieved for both companies with the SQL above (from the
		//    target and credit records) ... lookup the correct company and only book
		//    one of the entries.
		if isnull(credit_sqls) then
			f_pp_msgs("ERROR: The INTERCO INSERT SQL is NULL ... ")
			return -1
		end if		
		
		// remove the debug message and just error check this...
//		if g_debug = 'YES' then f_pp_msgs("i_ds_interco_insert credit_sqls = " + credit_sqls)
		
		rtn_str = f_create_dynamic_ds(i_ds_interco_insert, "grid", credit_sqls, sqlca, true)
		if rtn_str <> "OK" then
			f_pp_msgs("ERROR: Retrieving intercompany records: " + i_ds_interco_insert.i_sqlca_sqlerrtext)
			f_pp_msgs("SQL: " + credit_sqls)
			return -1
		end if
				
		num_inserts     = i_ds_interco_insert.RowCount()
		num_credit_cols = long(i_ds_interco_insert.describe("datawindow.column.count"))
				
		for k = 1 to num_inserts
			// ### 38448:  Move this initialization of id_array up so it is always called.
			// ### 11046: JAK: 2012-09-11: Cache blocks of IDs for performance
			if k = 1 then
				f_get_column(id_array,'select crdetail.nextval id from dual connect by level <= ' + string(num_inserts))
				id_array_pos = 1
			end if
			
			retrieved_company_val = i_ds_interco_insert.GetItemString(k, i_company_field)
			
			//  interco_company is from cr_alloc_interco_criteria ...
			//  retrieved_company_val is from the sqls that gets the Target's and Credit's
			//    from the allocation ...
			//  If they don't match, don't book an intercompany transaction ...
			if retrieved_company_val <> interco_company then continue
			
			//  TYPE 4: INTER-COMPANY (TRIGGER) ...
			if i_clearing_indicator = 4 then
				//  TARGETS get a receivable, CREDITS get a payable ...
				//  Need this check since the intercompany criteria table will have both the 
				//  payable and receivable parameters that will both pass the trigger check below.
				choose case upper(trim(i_ds_interco_insert.GetItemString(k, "target_credit")))
					case "TARGET"
						if string_row_id <> "RECEIVABLE" then continue
					case "CREDIT"
						if string_row_id <> "PAYABLE" then continue
					case else  //  The previously booked interco REC/PAY. will be in the DS ...
						continue
				end choose
			
				//  Get the "trigger" column value ...
				interco_trigger_col_val = i_ds_interco_insert.GetItemString(k, i_interco_trigger_col)
			
				//  Build the SQL to retrieve the correct company value ... for example, 
				//  (SELECT company from glc_area_v where area = '5270') 
				interco_credit_sql = i_original_interco_credit_sql + " '" + &
										   interco_trigger_col_val + "'"			

				//  NEED A CURSOR TO PERFORM THIS ...
				PREPARE SQLSA FROM :interco_credit_sql;
				if sqlca.sqlcode <> 0 then
		   		f_pp_msgs("CURSOR PREPARE - Error " + sqlca.sqlerrtext)
					return -1
				end if 

				OPEN DYNAMIC credit_cursor;

				if sqlca.sqlcode <> 0 then
					f_pp_msgs("CURSOR OPEN - Error " + sqlca.sqlerrtext)
					return -1
				end if 

				FETCH credit_cursor INTO :interco_company_val;

				interco_company_val = trim(interco_company_val)

				if sqlca.sqlcode < 0 then
					// SQL Error
					f_pp_msgs("Interco - CURSOR FETCH - Error " + sqlca.sqlerrtext)
					return -1
				elseif sqlca.sqlcode = 100 then 
					// No Rows found
					f_pp_msgs("Interco - CURSOR FETCH - No Records Found")
					f_pp_msgs("SQL: " + interco_credit_sql)
					return -1
				end if 
			
				CLOSE credit_cursor;
			
				//  The entry should only be booked where the company is valid ...
				//  Retrieving duplicate records by company is easier than trying to build
				//  the where clause with company in it ... This check was put in here 1st.
				//  A bug was found and a different company check was inserted just after
				//  "for k = 1 to num_inserts ..." (above).  This one may now be unnecessary,
				//  but we will keep it here just in case.
				//  THIS MUST BE COMMENTED OUT OR THE "TRIGGER" METHODOLOGY WONT WORK ...
				//  11/17/1998
//				if retrieved_company_val <> interco_company_val then continue
				
			else    //  if i_clearing_indicator = 4 then ...
				
				//  This check was put in here 1st.
				//  A bug was found and a different company check was inserted just after
				//  "for k = 1 to num_inserts ..." (above).  This one may now be unnecessary,
				//  but we will keep it here just in case.
				
				if retrieved_company_val <> interco_company then continue
				
				if g_debug = 'YES' then
					f_pp_msgs("retrieved_company_val = " + retrieved_company_val + ", " + &
						"interco_company = " + interco_company + ", " + "string_row_id = " + string_row_id + ", " + &
						"target_credit = " + upper(trim(i_ds_interco_insert.GetItemString(k, "target_credit"))))
				end if
				
				choose case upper(trim(i_ds_interco_insert.GetItemString(k, "target_credit")))
					case "TARGET"
						if string_row_id <> "PAYABLE" then continue
					case "CREDIT"
						if string_row_id <> "RECEIVABLE" then continue
					case else  //  The previously booked interco REC/PAY. will be in the DS ...
						continue
				end choose
				
			end if  //  if i_clearing_indicator = 4 then ...
						
			if id_array_pos <= upperbound(id_array[]) then
				id = id_array[id_array_pos]
				id_array_pos++
			else
				select crdetail.nextval into :id from dual;
			end if
				
			insert_sqls = insert_sqls_start + string(id)
			
			//  The custom DW's may have a mixture of intercompany and non-intercompany
			//  transactions ... need this check to skip certain records.
			//  DMJ - WHY NOT ALLOW THIS FOR ALL ALLOCATIONS?  PNW HAD A LOT OF INTERCO
			//        ALLOCATIONS THAT WOULD ALSO ALLOCATE TO THE SOURCE POOL.  THOSE
			//        KEPT GETTING ERRORS ON THE CRITERIA2 LOOKUP.  NOTE THAT THERE IS
			//        AN INTERCO BALANCING CHECK BELOW SO WE SHOULD BE OK IF THIS OPENS
			//        UP AN OOB HOLE.
			//
			//        FOUND IN TESTING --- IF THE CR_COMPANY_ID IS <NONE> (0 OR NULL AS
			//        THE VALUE).  THEN WE DO NEED TO HAVE THE ERROR FIRE.  IF THEY ARE
			//        MAKING THE TARGETS ON AN ALLOCATION LIKE THIS, THE COMPANY AND THE
			//        CROSS_CHARGE_COMPANY END UP THE SAME.  THEN IF THEY PUT THE CREDITS
			//        TO SOME OTHER COMPANY, THE CREDIT RECORDS WILL TRIGGER INTERCOMPANY
			//        AND THE TARGETS WILL NOT.  WE ARE THEN OOB !
			// ---> COMMENTED OUT THIS "FOUND IN TESTING" ISSUE SINCE I AM TRYING TO
			//      CORRECT IT BY SETTING THE CROSS_CHARGE_COMPANY TO THE CREDIT_CO ABOVE.
//			if custom_dw then
////			if isnull(cr_company_id) or cr_company_id = 0 then
////				//  Cannot continue ... must go through the processing below to catch
////				//  my comment above.
////			else
				if retrieved_company_val = &
				   i_ds_interco_insert.GetItemString(k, "cross_charge_company") &
				then 
					continue
				end if
////			end if
//			end if
			
			if g_debug = 'YES' then f_pp_msgs( &
				"retrieving intercompany accounting from i_ds_interco_insert (id = " + string(interco_pk_id) + ")")
			
			for z = 1 to num_credit_cols - 5  //  Exclude the month_number,month_period,
														 //  target_credit,cross_charge_company,
														 //  amount columns ...
				value    = i_ds_interco_insert.GetItemString(k, z)
				col_name = upper(trim(i_ds_interco_insert.Describe("#" + string(z) + ".Name")))
								
				if trim(value) = "%" then  //  cr_alloc_interco_criteria2 table ...
				
					//  The "field" field will have field names with underscores (no
					//  spaces) ... they may be lower case, however.
					cross_company_val = i_ds_interco_insert.GetItemString(k, "cross_charge_company")
					
					select value into :interco_flex_value
					  from cr_alloc_interco_criteria2
					 where intercompany_id = :i_intercompany_id    and
					       upper(row_id)   = :string_row_id and
							 id              = :interco_pk_id and
							 upper(field)    = :col_name      and
							 company_value   = :cross_company_val;
					
					if isnull(interco_flex_value) or trim(interco_flex_value) = "" then
						f_pp_msgs("Could not find the " + col_name + " value for " + &
						           "the intercompany transaction.")
						f_pp_msgs("intercompany_id = " + string(i_intercompany_id))
						f_pp_msgs("upper(row_id)   = " + string_row_id)
						f_pp_msgs("id              = " + string(interco_pk_id))
						f_pp_msgs("upper(field)    = " + col_name)
						f_pp_msgs("company_value   = " + cross_company_val)
						return -1
					end if
					
					insert_sqls += ",'" + interco_flex_value + "'"
					if i_suspense_accounting = "YES" then
						//  The "orig" field is right next to its ACK counterpart.
						insert_sqls += ",'" + interco_flex_value + "'"
					end if
				else
					insert_sqls += ",'" + value + "'"
					if i_suspense_accounting = "YES" then
						//  The "orig" field is right next to its ACK counterpart.
						insert_sqls += ",'" + value + "'"
					end if
				end if
				
			next  //  for z = 1 to num_credit_cols - 5 ...
			
			//  dr_cr_id ... (+) amount = 1 ... (-) amount = -1 ...
			if i_rate_field_exists then
				insert_sqls += "," + string(0)
			end if

			if i_incr_run_field_exists then
				insert_sqls += "," + string(i_curr_incr_run_id)
			end if
			
			credit_amount = i_ds_interco_insert.GetItemNumber(k, num_credit_cols)
			interco_total = interco_total + credit_amount
			if credit_amount >= 0 then
				insert_sqls += ", 1"
			else
				insert_sqls += ", -1"
			end if
			//  ledger_sign = 1 ...
			insert_sqls += ", 1"
			//  qty = 0 ...
			insert_sqls += ",0"
			//  amount ...
			insert_sqls += "," + string(credit_amount)
			//  month_number and month_period from the source ...
			insert_sqls += "," + &
							  string(i_ds_interco_insert.GetItemNumber(k, "month_number")) + &
							  "," + &
							  string(i_ds_interco_insert.GetItemNumber(k, "month_period"))
			//  gl_journal_category ...
			insert_sqls += ",'" + i_gl_journal_category + "'"
			//  amount_type ...
			insert_sqls += "," + string(i_result_amount_type) // DMJ: 6/11/08: was string(1)
			//  allocation_id ...
			insert_sqls += "," + string(i_allocation_id)
			//  target_credit ...
			insert_sqls += ", 'INT_CO'"
			//  cross_charge_company ...
			insert_sqls += ", '" + cross_company_val + "'"
			
			insert_sqls += ")"		 
		
			if isnull(insert_sqls) then
				f_pp_msgs(&
					"ERROR: The (Execute immediate # 3) is NULL ... " + &
					"Check the intercompany definitions for this allocation type. ")
				return -1
			end if
		
			//  Execute immediate # 3
			execute immediate :insert_sqls using sqlca;

			if sqlca.SQLCode <> 0 then
				f_pp_msgs("ERROR - Intercompany Entries: " + sqlca.SQLErrText)
				f_pp_msgs("insert_sqls = " + insert_sqls)
				return -1
			end if
		
		next  //  for k = 1 to num_inserts ...
	
	next	//  for i = 1 to num_rows ... Loop over credit criteria records ...
	
	if interco_total <> 0 then
		f_pp_msgs("ERROR - Intercompany Entries do not balance: " + string(interco_total))
		
		if i_have_results then
			// results already loaded to the detail table.  Cannot commit!
			f_pp_msgs("Unable to commit records for review to records already recorded to detail table.")
			return -1
		end if
		
		commit using sqlca; // I'm going to see if I can get away with this.  We'd be commiting momentarily anyways.
			// Otherwise, how will we know what's OOB ?  Note that the commit does nothing for clients running with
			// suspense accounting.  In that case, the stg tables are temp tables and the data must be saved off to a
			// real table that the DBA can retrieve for us.
			
		select count(*) into :counter
		from all_tables
		where owner = 'PWRPLANT'
		and table_name = :i_alloc_stg_table
		and temporary = 'Y';
		
		if counter > 0  then
			select crmaint.nextval into :seq_val from dual;
			sqls = "create table cr_alloc_" + string(seq_val) + " as select * from " + i_alloc_stg_table
			execute immediate :sqls;
			if sqlca.SQLCode < 0 then
				f_pp_msgs("ERROR: creating OOB table: " + sqlca.SQLErrText)
			else
				f_pp_msgs("***** staged transactions saved to: cr_alloc_" + string(seq_val))
			end if
		end if
		return -1
		
	end if
	
end if


//*****************************************************************************************
//
//  Run by Deparment Flag:
//
//  9/17/07: Only call this function for Clearing-Calc Rate allocations.  The methodology
//  is simpler for Clearing-Input Rate allocations and is completely handled in this
//  script.
//
//*****************************************************************************************
if i_run_by_dept_credits_method <> "YES" then
	if i_run_by_department = 1 and i_clearing_indicator = 3 then
		//
		//  Call the function that calculates departmental-based clearings ...
		//
		rtn = uf_run_by_department(i_allocation_id, i_month_number, i_month_period, &
			i_clearing_indicator, i_cr_company_id, i_company_field, i_gl_journal_category, i_ds_source, &
			i_group_by_id, i_description, i_where_clause_id, i_clearing_id, i_work_order_field, custom_dw)
		
		if rtn <> 1 then
			f_pp_msgs("ERROR: in the Run by Department logic")
			return -1
		end if
	end if
end if


//*****************************************************************************************
//
//  Base Derivations:
//
//*****************************************************************************************
if i_derivation_flag = 1 then
	rtn = uf_derivations()
	if rtn <> 1 then
		f_pp_msgs("ERROR: in the derivations")
		return -1
	end if
end if



//*****************************************************************************************
//
//  Customizable Updates:
//
//*****************************************************************************************
cleanup_long_array_arg[1] = i_allocation_id
cleanup_long_array_arg[2] = i_month_number	
cleanup_long_array_arg[3] = i_month_period
rtn = i_uo_cleanup.uf_cleanup1(this, cleanup_long_array_arg)
if rtn <> 1 then
	f_pp_msgs("ERROR: in the custom derivations")
	return -1
end if


//*****************************************************************************************
//
//  Balance by Company:  System switched since some companies send allocation results to
//                       their GL's without running intercompany accounting.
//
//*****************************************************************************************
if i_balancing_cv = "YES" then 	
	balancing_sqls = &
		"select count(*) from (" + &
		'select "' + i_company_field + '", gl_journal_category, month_number, sum(amount) ' + &
		"from " + i_alloc_stg_table
		
	balancing_sqls += " where allocation_id = " + string(i_allocation_id) + " "
	balancing_sqls += " and month_number = " + string(i_month_number) + " "
	balancing_sqls += ' group by "' + i_company_field + '", gl_journal_category, month_number '
	balancing_sqls += "having sum(amount) <> 0)"
	
	rtn = i_ds_count.SetSQLSelect(balancing_sqls)
	if rtn < 0 then
		f_pp_msgs("ERROR: Calling SetSQLSelect for balancing check")
		f_pp_msgs(i_ds_count.i_sqlca_sqlerrtext)
		f_pp_msgs("SQL: " + balancing_sqls)
		return -1
	end if

	rtn = i_ds_count.RETRIEVE()
	if rtn < 0 then
		f_pp_msgs("ERROR: Retrieving DS for balancing check")
		f_pp_msgs(i_ds_count.i_sqlca_sqlerrtext)
		f_pp_msgs("SQL: " + balancing_sqls)
		return -1
	end if
		
	counter = 0
	if i_ds_count.RowCount() > 0 then
		counter = i_ds_count.GetItemNumber(1, 1)
		if isnull(counter) then counter = 0
	end if
	
	if counter > 0 then
		f_pp_msgs("----------------------------------------------------------------------------------------------------------------------------------------------")
		f_pp_msgs("ERROR: The allocation results do not balance by " + &
			i_company_field + ", GL Journal Category, and Month Number !")
		f_pp_msgs("----------------------------------------------------------------------------------------------------------------------------------------------")
		
		if i_have_results then
			// results already loaded to the detail table.  Cannot commit!
			f_pp_msgs("Unable to commit records for review to records already recorded to detail table.")
			return -1
		end if
		
		commit using sqlca; // I'm going to see if I can get away with this.  We'd be commiting momentarily anyways.
			// Otherwise, how will we know what's OOB ?  Note that the commit does nothing for clients running with
			// suspense accounting.  In that case, the stg tables are temp tables and the data must be saved off to a
			// real table that the DBA can retrieve for us.
		select count(*) into :counter
		from all_tables
		where owner = 'PWRPLANT'
		and table_name = :i_alloc_stg_table
		and temporary = 'Y';
		
		if counter > 0  then
			select crmaint.nextval into :seq_val from dual;
			sqls = "create table cr_alloc_" + string(seq_val) + " as select * from " + i_alloc_stg_table
			execute immediate :sqls;
			if sqlca.SQLCode < 0 then
				f_pp_msgs("ERROR: creating OOB table: " + sqlca.SQLErrText)
			else
				f_pp_msgs("***** staged transactions saved to: cr_alloc_" + string(seq_val))
			end if
		end if
		return -1
	end if
end if


//*****************************************************************************************
//
//  Validations:
//
//  1)  At this point, if validations are turned on, the transactions will be in the "STG" 
//		  tables.  Otherwise, they will have gone straight to the regular detail tables to
//      limit processing time.
//
//  2)  If we are validating, and since we are about to update the drilldown_key below in
//      the regular detail tables, we must validate the STG tables and then move the
//      records to the regular detail tables.
//
//*****************************************************************************************

f_pp_msgs("    Running validations ...")
rtn = uf_validations()

// ### 8142: JAK: 2011-11-01:  Consistent return codes
if rtn <> 1 then return rtn

return 1
end function

public function longlong uf_run_by_department (longlong a_alloc_id, longlong a_mn, longlong a_mp, longlong a_clear_ind, longlong a_cr_co_id, string a_co_field, string a_gl_jcat, uo_ds_top a_ds_source, longlong a_group_by_id, string a_descr, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, boolean a_custom_dw);//******************************************************************************************
//
//  User Object Function  :  uf_run_by_department
//
//  Notes                 :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!
//
//  ***  DEPARTMENT OR COST CENTER BASED CLEARINGS (CHARGING COST CENTER AT EL PASO):  ***
//
//  General Comment       :  This code contains terms and variables like "charging cost 
//                           center".  That is because the code originated from El Paso's
//                           extension functions.  I am leaving these terms as-is, so we
//                           can easily compare to the original El Paso code if necessary.
//                           Don't be fooled by the terms ... genreally speaking, any ACK
//                           field can be used in this function.
//
//  EL PASO COMMENT:
//  ----------------
//    Certain allocations like unproductive time must have cost center based pots cleared
//    to records within the same cost center.  We will accomplish that here since the 
//    regular clearing-calc-rate allocations simply clear a single pot (we don't want to
//    set up 1 allocation per cost center!).  The methodology is:
//
//    1)  Run the traditional clearing-calc-rate allocation where the pot is the sum of
//        all the cost centers.  This will horribly mis-clear the dollars, but at least
//        it will clear SOMETHING to every possible distribution that can be used as
//        a basis for the "source criteria" in #3 below.
//    2)  Select the list of distinct cost centers from the results of #1.
//    3)  Loop over the list of cost centers and re-clear each cost center's pot:
//        -- Re-determine the pot based on the Balance Criteria for the allocation along
//           with " and cost_center = :cc ".
//        -- Perform the clearing using the formula:
//
//                              original cleared amount 
//              XCC Pot *  --------------------------------
//                         total cleared amount for the XCC
//
//    4)  Re-book the Credits by deleting the original credit and inserting 1 record per
//        cost center ... Bill says there will only be a single ACK combination in the
//        unproductive time pots.
//
//  Notes:
//  ------
//  *) We will write code against a clearing-calc-rate allocation only.  Any other type of
//     allocation would not make sense.  This means we can code against 4 tables instead
//     of 8.
//
//  *) By definition (charging_cost_center remains constant) there can be no intercompany
//     effect ... thus no code for intercompany.
//
//  *) The CREDIT code will be forced to book only a single credit record (even if there
//     is more than one balance ACK in the pot) ... per Bill's instructions --- he said 
//     they make sure all unproductive time goes to a single distribution per XCC.
//
//  *) The nature of this code is such that any clearing-calc-rate allocation that needs
//     to keep dollars constant by XCC can be sent through this code by adding the allocation
//     to the cr_epe_cc_based_allocations table through table maintenance.
//
//  *) This code needs to be BEFORE the company/bus seg derivations below.
//
//  *)  El Paso allocated based on charging cost centers, not target cost centers.
//      Atmos   allocated based on ???
//
//******************************************************************************************
longlong month_number, month_period, counter, clearing_indicator, &
        i, num_distinct_x_cc_values, e, id, &
		  dr_cr_id_for_credit, rtn
string  sqls, company_field, charging_cost_center, element_descr, gl_journal_category
boolean zero_qty, skip_allocate, all_0_balances=true
dec {2} amount, xcc_pot, total_cleared_amount_for_the_xcc, &
		  new_total_cleared_amount_for_the_xcc, amount_for_credit, total_cleared_qty_for_the_xcc, &
		  xcc_pot_qty, new_total_cleared_qty_for_the_xcc, qty_for_credit

// This function only ever handles Clearing Calc Rate allocations.  Other allocation types are handled
//	directly in uf_loading via streamlined code.
if i_clearing_indicator <> 3 then
	f_pp_msgs("ERROR:  uf_run_by_department logic should only run for clearing calc rate allocations!")
	return -1
end if

//  This evolved from a client's extension function. Thus I am leaving the general
//  strucutre the same and setting local variables.
month_number        = a_mn
month_period        = a_mp
clearing_indicator  = a_clear_ind
company_field       = a_co_field
gl_journal_category = a_gl_jcat

//  Get the distinct list of department values.
sqls = 'select distinct "' + i_department_field + '" ' + &
	"from " + i_alloc_stg_table + " " + &
	"where allocation_id = " + string(i_allocation_id) + " " + &
	 "and month_number  = " + string(month_number) + " " + &
	 "and month_period  = " + string(month_period) + " " + &
	 "and target_credit = 'TARGET' "
rtn = i_ds_distinct_x_cc_values.SetSQLSelect(sqls)

if rtn < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Calling SetSQLSelect for distinct " + i_department_field + " values")
	f_pp_msgs(i_ds_distinct_x_cc_values.i_sqlca_sqlerrtext)
	f_pp_msgs(" ")
	return -1
end if

num_distinct_x_cc_values = i_ds_distinct_x_cc_values.Retrieve()

if num_distinct_x_cc_values < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving distinct " + i_department_field + " values")
	f_pp_msgs(i_ds_distinct_x_cc_values.i_sqlca_sqlerrtext)
	f_pp_msgs(" ")
	return -1
end if


//  Loop over each x-cc and re-clear.
for i = 1 to num_distinct_x_cc_values
	zero_qty    = false

	//  Get the charging_cost_center.
	charging_cost_center = i_ds_distinct_x_cc_values.GetItemString(i, 1)
	
	//  Get this charging_cost_center's pot to be re-spread ("XCC Pot" from above).
	//  This function also saves the balance_where_clause in an instance variable
	//  that will be used to generate the single credit per XCC.
	rtn = uf_clearing_balance(i_cr_company_id, charging_cost_center, xcc_pot, xcc_pot_qty)
			
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	if rtn < 0 then return rtn
		
	if isnull(xcc_pot) then xcc_pot = 0
	if isnull(xcc_pot_qty) then xcc_pot_qty = 0 
	
	if g_debug = 'YES' then f_pp_msgs("uf_run_by_department: " + charging_cost_center + ": xcc_pot = " + string(xcc_pot))
	if g_debug = 'YES' then f_pp_msgs("uf_run_by_department: " + charging_cost_center + ": xcc_pot_qty = " + string(xcc_pot_qty))
	
	//  If the XCC has a NULL or 0 balance returned from uf_clearing_balance_xcc then
	//  we have incorrectly allocated to this XCC in the original clearing-calc-rate
	//  allocation ... delete those allocation results and go to the next XCC.
	
	if xcc_pot = 0 and xcc_pot_qty = 0 then
		sqls = &
			"delete from " + i_alloc_stg_table + " " + &
			"where allocation_id = " + string(i_allocation_id) + " " + &
			"and month_number  = " + string(month_number) + " " + &
			"and month_period  = " + string(month_period) + " " + &
			'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: deleting the 0-balance results: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("charging_cost_center = " + charging_cost_center)
			f_pp_msgs("  ")
			return -1
		end if
		
		if g_debug = 'YES' then f_pp_msgs("xcc_pot = 0 and xcc_pot_qty = 0, "+string(sqlca.SQLNrows) + " rows deleted from "+i_alloc_stg_table+" for charging_cost_center = " + charging_cost_center+". Continuing...")
		
		continue
		
	else
		all_0_balances = false
	end if
	
	//  Get the "total cleared amount for the XCC" (the denominator from above).
	total_cleared_amount_for_the_xcc = 0
	total_cleared_qty_for_the_xcc    = 0
	sqls = "select sum(amount), sum(quantity) from " + i_alloc_stg_table + " " + &
		"where allocation_id = " + string(i_allocation_id) + " " + &
		"and month_number  = " + string(month_number) + " " + &
		"and month_period  = " + string(month_period)  + " " + &
		"and target_credit = 'TARGET' " + &
		'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "

	i_ds_sum_amount.SetSQLSelect(sqls)
	i_ds_sum_amount.RETRIEVE()

	if i_ds_sum_amount.RowCount() <> 1 then
		// Issues!
		f_pp_msgs("ERROR: Retrieving i_ds_sum_amount: " + i_ds_sum_amount.i_sqlca_SQLErrText)
		f_pp_msgs("charging_cost_center = " + charging_cost_center)
		return -1
	else
		total_cleared_amount_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
		total_cleared_qty_for_the_xcc    = i_ds_sum_amount.GetItemNumber(1, 2)
		
		if isnull(total_cleared_amount_for_the_xcc) then total_cleared_amount_for_the_xcc = 0
		if isnull(total_cleared_qty_for_the_xcc)    then total_cleared_qty_for_the_xcc = 0
	end if


	//  RE-SPREAD (TARGETS).  AMOUNT & QUANTITY in one pass now
	sqls = "update " + i_alloc_stg_table + " "
	
	if total_cleared_amount_for_the_xcc <> 0 and total_cleared_qty_for_the_xcc <> 0 then 	
		// spread both
		sqls +=	"set " + &
			"amount = " + string(xcc_pot) + " * (amount / " + string(total_cleared_amount_for_the_xcc) + "), " + &
			"quantity = " + string(xcc_pot_qty) + " * (quantity / " + string(total_cleared_qty_for_the_xcc) + ") "
		if i_rate_field_exists then sqls += ", allocation_rate = allocation_rate * (" + string(xcc_pot) + " / " + string(total_cleared_amount_for_the_xcc) + ") "
	
	elseif total_cleared_amount_for_the_xcc <> 0 then
		// amount only
		sqls +=	"set " + &
			"amount = " + string(xcc_pot) + " * (amount / " + string(total_cleared_amount_for_the_xcc) + "), " + &
			"quantity = 0 "
		if i_rate_field_exists then sqls += ", allocation_rate = allocation_rate * (" + string(xcc_pot) + " / " + string(total_cleared_amount_for_the_xcc) + ") "
		
	elseif total_cleared_qty_for_the_xcc <> 0 then
		// quantity only
		sqls +=	"set " + &
			"amount = 0, " + &
			"quantity = " + string(xcc_pot_qty) + " * (quantity / " + string(total_cleared_qty_for_the_xcc) + ") "
		if i_rate_field_exists then sqls += ", allocation_rate = 0 "
	else
		//  There could be cases where a cost center with very few people (like 1)
		//  charged all their time to vacation and such.  With no straight or overtime
		//  payroll, the original allocation --- and thus this re-spread --- cannot
		//  allocate anything.  Check for that condition here and continue so we do
		//  not get a div-by-zero error.
		// both are zero
		// nothing to allocate.
		skip_allocate = true
		
		////maint-41229
		sqls = &
			"delete from " + i_alloc_stg_table + " " + &
			"where allocation_id = " + string(i_allocation_id) + " " + &
			"and month_number  = " + string(month_number) + " " + &
			"and month_period  = " + string(month_period) + " " + &
			'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: deleting the 0-balance results: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("charging_cost_center = " + charging_cost_center)
			f_pp_msgs("  ")
			return -1
		end if
		
		if g_debug = 'YES' then f_pp_msgs("total_cleared_amount_for_the_xcc <> 0 and total_cleared_qty_for_the_xcc <> 0. "+string(sqlca.SQLNrows) + " rows deleted from "+i_alloc_stg_table+" for charging_cost_center = " + charging_cost_center+". Continuing...")
		
		continue
		
	end if
		
	if not skip_allocate then
		sqls += &
			" where allocation_id = " + string(i_allocation_id) + " " + &
			"and month_number  = " + string(month_number) + " " + &
			"and month_period  = " + string(month_period) + " " + &
			"and target_credit = 'TARGET' " + &
			'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating with the RE-SPREAD amount: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("charging_cost_center = " + charging_cost_center)
			f_pp_msgs("  ")
			return -1
		end if
	end if
	
	//  Fix any rounding error from the re-spread:
	//    XCC Pot - new_total_cleared_amount_for_the_xcc plugged to the record in the
	//    allocations table with the max(id) --- i.e. the last record.
	sqls = &
		"select sum(amount), sum(quantity) from " + i_alloc_stg_table + " " + &
		 "where allocation_id = " + string(i_allocation_id) + " " + &
			"and month_number  = " + string(month_number) + " " + &
			"and month_period  = " + string(month_period) + " " + &
			"and target_credit = 'TARGET' " + &
			'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
	
	i_ds_sum_amount.SetSQLSelect(sqls)
	i_ds_sum_amount.RETRIEVE()
	
	if i_ds_sum_amount.RowCount() <> 1 then
		// Issues!
		f_pp_msgs("ERROR: Retrieving i_ds_sum_amount: " + i_ds_sum_amount.i_sqlca_SQLErrText)
		f_pp_msgs("charging_cost_center = " + charging_cost_center)
		return -1
	else
		new_total_cleared_amount_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
		new_total_cleared_qty_for_the_xcc    = i_ds_sum_amount.GetItemNumber(1, 2)
		
		if isnull(new_total_cleared_amount_for_the_xcc) then new_total_cleared_amount_for_the_xcc = 0
		if isnull(new_total_cleared_qty_for_the_xcc)    then new_total_cleared_qty_for_the_xcc = 0
	end if
	
	if xcc_pot <> new_total_cleared_amount_for_the_xcc or xcc_pot_qty <> new_total_cleared_qty_for_the_xcc then
		// Plug needed.
		sqls = &
			"update " + i_alloc_stg_table + " " + &
				"set amount = amount + (" + string(xcc_pot - new_total_cleared_amount_for_the_xcc) + "), " + &
				"    quantity = quantity + (" + string(xcc_pot_qty - new_total_cleared_qty_for_the_xcc) + ") " + &
			 "where allocation_id = " + string(i_allocation_id) + " " + &
				"and month_number  = " + string(month_number) + " " + &
				"and month_period  = " + string(month_period) + " " + &
				"and target_credit = 'TARGET' " + &
				'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
				"and id = " + &
					"(select max(id) from " + i_alloc_stg_table + " " + &
					"where allocation_id = " + string(i_allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "')"

		execute immediate :sqls;
	
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating with the ROUNDING plugs: " + sqlca.SQLErrText)
			f_pp_msgs(" Amount Diff: " + string(xcc_pot - new_total_cleared_amount_for_the_xcc))
			f_pp_msgs(" Quantity Diff: " + string(xcc_pot_qty - new_total_cleared_qty_for_the_xcc))
			f_pp_msgs("charging_cost_center = " + charging_cost_center)
			return -1
		end if
	end if
	
	//  If they have the system switch turned on to obey the Credit Criteria, we don't want to book
	//  the credits here.
	if i_run_by_dept_credits_method = "YES" then goto after_credits
	
	//  CREDITS:
	sqls = &
		"delete from " + i_alloc_stg_table + " " + &
			"where allocation_id = " + string(i_allocation_id) + " " + &
			"and month_number  = " + string(month_number) + " " + &
			"and month_period  = " + string(month_period) + " " + &
			"and target_credit = 'CREDIT' " + &
			'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: deleting the original credits: " + sqlca.SQLErrText)
		f_pp_msgs("charging_cost_center = " + charging_cost_center)
		return -1
	end if

	if g_debug = 'YES' then f_pp_msgs(string(sqlca.SQLNrows) + " credit rows deleted from "+i_alloc_stg_table+" for charging_cost_center = " + charging_cost_center+".")
	
	//  Book the credit.
	sqls = 'insert into ' + i_alloc_stg_table + ' ("ID'
	
	for e = 1 to i_num_elements
		element_descr     = i_ds_elements.GetItemString(e, "description")
		sqls += '","' + element_descr
		if i_suspense_accounting = "YES" then sqls += '","ORIG_' + element_descr //  The "orig" field is right next to its ACK counterpart.
	next
	
	sqls += '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
					  ',"MONTH_NUMBER","MONTH_PERIOD"' + &
					  ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
					  ',"ALLOCATION_ID","TARGET_CREDIT",' + &
					  '"CROSS_CHARGE_COMPANY") (select '
	
	select crdetail.nextval into :id from dual;
	
	sqls += string(id) + ' as "ID'
	
	for e = 1 to i_num_elements
		element_descr     = i_ds_elements.GetItemString(e, "description")
		sqls += '","' + element_descr
		if i_suspense_accounting = "YES" then sqls += '","' + element_descr //  The "orig" field is right next to its ACK counterpart.
	next
	
	//  For now since the rounding plug will force 100% to clear.
	//  Note that cr_cost_repository is hardcoded in uf_clearing_balance.
	amount_for_credit = -xcc_pot
	qty_for_credit    = -xcc_pot_qty
	
	if amount_for_credit < 0 then
		dr_cr_id_for_credit = -1
	else
		dr_cr_id_for_credit =  1
	end if

	sqls += '",' + string(dr_cr_id_for_credit) + ' as DR_CR_ID' + &
					  ',"LEDGER_SIGN",' + string(qty_for_credit) + ' as QUANTITY,' + &
					  string(amount_for_credit) + &
					  ',' + string(month_number) + ' as MONTH_NUMBER,0 as MONTH_PERIOD' + &
					  ', ' + "'" + gl_journal_category + "'" + ' as GL_JOURNAL_CATEGORY' + &
					  ',"AMOUNT_TYPE"' + &
					  ', ' + string(i_allocation_id) + ' as ALLOCATION_ID,' + &
					  "'CREDIT'" + ' as TARGET_CREDIT,' + &
					  'NULL as CROSS_CHARGE_COMPANY from ' + i_balance_table_name + " "
	
	//  The " and rownum = 1" is to force only a single credit record.  Note that
	//  the "where" is already embedded in i_balance_where_clause.
	sqls += " " + i_balance_where_clause + " and rownum = 1)"
	
	if g_debug = 'YES' then f_pp_msgs("uf_run_by_department: CREDIT sqls = " + sqls)
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: inserting the credit record: " + sqlca.SQLErrText)
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs(i_department_field + " = " + charging_cost_center)
		return -1
	end if
	
	if g_debug = 'YES' then f_pp_msgs(string(sqlca.SQLNrows) + " credit rows inserted into "+i_alloc_stg_table+" for charging_cost_center = " + charging_cost_center+".")
		
	after_credits:	
	
next  //  for i = 1 to num_distinct_x_cc_values ...
	
if all_0_balances then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: All available charging cost centers yield 0-balance results.  Cannot process run by department.")
	f_pp_msgs("  ")
	return -1
end if

 
return 1
end function

public function longlong uf_derivations ();//**********************************************************************************************
//
//  User Object Function  :  uf_derivations
//
//  Description           :  Calls base derivations as identified in cr_deriver_type_sources.
//
//  Notes                 :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!
//
//  cr_deriver_type_sources
//  -----------------------
//  TYPE              :  the derivation type (same as entered in cr_deriver_control)
//  SOURCE_ID         :  from cr_sources ... identifies the table to apply derivations against
//  RUN_ORDER         :  for this source_id, the order in which the derivations should run
//  BASIS             :  for update derivations,
//                          the a_join_field argument ... if multiples, comma-separate them
//                       for insert derivations,
//                          the a_join argument ... which is the where clause ... use
//                          "cr." as the prefix for the transaction table
//  INSERT_OR_UPDATE  :  'Update' or 'Insert' to identify which kind of derivation this is
//  DELIMITER         :  for update types only ... the delimiter separating the basis fields
//  OFFSET_TYPE       :  for insert types only ... the type value for the offsets ... if null
//                       then you need a separate record for the offset type (although both
//                       sets of records would be tagged as TARGET)
//  OFFSET_BASIS      :  see BASIS ... for the offset to an insert derivation
//  INCLUDE_DERIVATION_ROLLLUP  :  FUTURE DEVELOPMENT FOR SAP TYPE DERIVATIONS ???
//
//**********************************************************************************************
longlong rtn, max_id, source_id, num_types, t, select_counter, override_counter, counter
string staging_table, sqls, join_fields[], lookup_table, the_type, delimiter, basis, &
       insert_or_update, offset_type, offset_basis, or_name, col_name, suppl_wc, &
		 basis_2nd_arg, basis_6th_arg, id_balancing_basis, null_str_array[]


if g_debug = 'YES' then
	f_pp_msgs("Derivations started")
	i_uo_cr_derivation.i_debug = "YES"
end if


//**********************************************************************************************
//
//  Determine which table we are dealing with.
//  Retrieve the source_id for the table.
//  Retrieve the Type/Sources records.
//
//**********************************************************************************************
if i_clearing_indicator = 4 then  //  Inter-Company ...
	if i_run_as_a_test then
		staging_table = "cr_inter_company_test_stg"
	else
		staging_table = "cr_inter_company_stg"
	end if
else                                                          //  Other Allocations ...
	if i_run_as_a_test then
		staging_table = "cr_allocations_test_stg"
	else
		staging_table = "cr_allocations_stg"
	end if
end if

lookup_table = left(staging_table, len(staging_table) - 4)

source_id = usf_get_source_id(lookup_table)

if isnull(source_id) or source_id = -1 then
	f_pp_msgs("DERIVATIONS: Could not find source_id for: " + staging_table)
	return -1 // Return and halt if this lookup fails.
end if

select_counter = 0
select count(*) into :select_counter from cr_deriver_type_sources_alloc
 where allocation_id = :i_allocation_id and source_id = :source_id and rownum = 1;

if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: select_counter = " + string(select_counter))

if isnull(select_counter) or select_counter = 0 then
	//  No override, use the standard setup
	sqls = "select * from cr_deriver_type_sources where source_id = " + string(source_id)
	i_cr_deriver_type_sources.SetSQLSelect(sqls)
	i_cr_deriver_type_sources.RETRIEVE()
	i_cr_deriver_type_sources.SetSort("run_order a")
	i_cr_deriver_type_sources.Sort()
	num_types = i_cr_deriver_type_sources.RowCount()
else
	//  Override for this allocation.  Note: 0 in run_order means don't run !
	sqls = "select * from cr_deriver_type_sources_alloc " + &
		"where allocation_id = " + string(i_allocation_id) + " and source_id = " + string(source_id) + " and run_order <> 0"
	i_cr_deriver_type_sources_alloc.SetSQLSelect(sqls)
	i_cr_deriver_type_sources_alloc.RETRIEVE()
	i_cr_deriver_type_sources_alloc.SetSort("run_order a")
	i_cr_deriver_type_sources_alloc.Sort()
	num_types = i_cr_deriver_type_sources_alloc.RowCount()
end if

if num_types <= 0 then
	if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: No derivations defined for: " + staging_table)
	return 1 // Return, but allow to continue.  Don't want to halt just due to a cbx checked.
end if


//**********************************************************************************************
//
//  Derivations need a real interface_batch_id.  Use the allocation_id.
//
//**********************************************************************************************
sqls = "update " + staging_table + " set interface_batch_id = allocation_id"
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to allocation_id: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if



FOR t = 1 to num_types
	
	
	
	if isnull(select_counter) or select_counter = 0 then
		//  No override, use the standard setup
		the_type         = trim(i_cr_deriver_type_sources.GetItemString(t, "type"))
		basis            = trim(i_cr_deriver_type_sources.GetItemString(t, "basis"))
		insert_or_update = upper(trim(i_cr_deriver_type_sources.GetItemString(t, "insert_or_update")))
		delimiter        = i_cr_deriver_type_sources.GetItemString(t, "delimiter")
		offset_type      = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_type"))
		offset_basis     = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_basis"))
	else
		//  Override for this allocation.
		the_type         = trim(i_cr_deriver_type_sources_alloc.GetItemString(t, "type"))
		select basis, upper(trim(insert_or_update)), delimiter, offset_type, offset_basis
		  into :basis, :insert_or_update, :delimiter, :offset_type, :offset_basis
		  from cr_deriver_type_sources
		 where "TYPE" = :the_type and source_id = :source_id;
	end if
	
	if isnull(the_type)  or the_type = "" then continue // useless record
	if isnull(basis)     or basis    = "" then continue // useless record
	if isnull(delimiter)                  then delimiter    = ""
	if isnull(offset_type)                then offset_type  = "" // for g_debug below
	if isnull(offset_basis)               then offset_basis = "" // for g_debug below
	if offset_type  = "" and insert_or_update = "INSERT" then continue // useless record
	if offset_basis = "" and insert_or_update = "INSERT" then continue // useless record
	
	if g_debug = 'YES' then
		f_pp_msgs("type = " + the_type)
		f_pp_msgs("basis = " + basis)
		f_pp_msgs("insert_or_update = " + insert_or_update)
		f_pp_msgs("delimiter = " + delimiter)
		f_pp_msgs("offset_type = " + offset_type)
		f_pp_msgs("offset_basis = " + offset_basis)
	end if
	
	
	
	if insert_or_update = "INSERT" then goto insert_derivations
	//*******************************************************************************************
	//
	//  Update Derivations:
	//
	//*******************************************************************************************
	
	//  Parse the basis value (e.g. "company,work_order") into an array for the update function.
	join_fields = null_str_array
	f_parsestringintostringarray(basis, ",", join_fields)
	
	rtn = i_uo_cr_derivation.uf_deriver_update_mult( &
		the_type, &
		staging_table, &
		join_fields, &
		string(i_allocation_id), &
		false, &
		delimiter)
	
	if rtn <> 1 then
		return -1
	end if
	
	continue // a derivation can't be both Update AND Insert
	
	
	
	insert_derivations:
	//*******************************************************************************************
	//
	//  Insert Derivations:
	//
	//    "Basis" is the "where clause" for the derivations.  For example, it might be:
	//    "cr_allocations_stg.work_order = cr_deriver_control.string and target_credit = 'TARGET'"
	//    for the target side of the derivation and:
	//    "cr_allocations_stg.work_order = cr_deriver_control.string and cr_txn_type = 'TARGET'"
	//    for the offset side.
	//
	//    CR TXN TYPE --- for speed, we're assuming that field is on all the allocations txn
	//    tables.  If that ends up bad in the future, we'll add a system switch and evaluate
	//    it before we start looping over allocations.
	//
	//*******************************************************************************************
	
	
	//  -----------------------------------  TARGET SECTION:  -----------------------------------
	
	
	//  Need the max(id) so I can fire a cr_txn_type update below.  Just using brute force.
	//  Also, take advantage of this loop to replace "cr." with the correct table name.
	max_id = 0
	
	if i_clearing_indicator = 4 then  //  Inter-Company ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_inter_company_test_stg where interface_batch_id = :i_allocation_id;
		else
			select max(id) into :max_id from cr_inter_company_stg where interface_batch_id = :i_allocation_id;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_allocations_test_stg where interface_batch_id = :i_allocation_id;
		else
			select max(id) into :max_id from cr_allocations_stg where interface_batch_id = :i_allocation_id;
		end if
	end if
	
	if isnull(max_id) then max_id = 0
	
	//  The basis will almost certainly have "cr." references for the detail table.  Change that here
	//  to the staging_table.
	basis = f_replace_string(basis, "cr.", staging_table + ".", "all")
	
	//  Call the indicated derivation type.
	rtn = i_uo_cr_derivation.uf_deriver( &
		the_type, &
		staging_table, &
		basis)
	
	if rtn <> 1 then
		return -1
	end if
	
	//  Now update the cr_txn_type.  Just using brute force.
	if i_clearing_indicator = 4 then  //  Inter-Company ...
		if i_run_as_a_test then
			update cr_inter_company_test_stg  set cr_txn_type = 'TARGET'
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		else
			update cr_inter_company_stg set cr_txn_type = 'TARGET'
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			update cr_allocations_test_stg set cr_txn_type = 'TARGET'
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		else
			update cr_allocations_stg set cr_txn_type = 'TARGET'
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		end if
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("DERIVATIONS: TARGETS ERROR: updating " + staging_table + ".cr_txn_type: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		return -1
	end if
	
	//  Critical update for the rounding plug.
	counter = 0
	select count(*) into :counter from all_tab_columns
	 where table_name = upper(:staging_table) and column_name = 'CR_DERIVATION_TYPE';
	if isnull(counter) then counter = 0
	
	if counter > 0 then
		if g_debug = 'YES' then
			f_pp_msgs("Performing other updates (cr_derivation_type)")
		end if
		
		sqls = "update " + staging_table + &
					" set cr_derivation_type = '" + the_type + "' where id > " + string(max_id) + &
					" and interface_batch_id = " + string(i_allocation_id)
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("DERIVATIONS: TARGETS ERROR: updating " + staging_table + ".cr_derivation_type: " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			return -1
		end if
	end if
	
	
	//  -----------------------------------  OFFSET SECTION:  -----------------------------------
	
	
	//  Need the max(id) so I can fire a cr_txn_type update below.  Just using brute force.
	//  Also, take advantage of this loop to replace "cr." with the correct table name.
	max_id = 0
	
	if i_clearing_indicator = 4 then  //  Inter-Company ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_inter_company_test_stg where interface_batch_id = :i_allocation_id;
		else
			select max(id) into :max_id from cr_inter_company_stg where interface_batch_id = :i_allocation_id;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_allocations_test_stg where interface_batch_id = :i_allocation_id;
		else
			select max(id) into :max_id from cr_allocations_stg where interface_batch_id = :i_allocation_id;
		end if
	end if
	
	if isnull(max_id) then max_id = 0
	
	//  The offset_basis will almost certainly have "cr." references for the detail table.  Change that here
	//  to the staging_table.
	offset_basis = f_replace_string(offset_basis, "cr.", staging_table + ".", "all")
	
	rtn = i_uo_cr_derivation.uf_deriver( &
		offset_type, &
		staging_table, &
		offset_basis)
	
	if rtn <> 1 then
		return -1
	end if
	
	//  Now update the cr_txn_type.  Just using brute force.
	if i_clearing_indicator = 4  then  //  Inter-Company ...
		if i_run_as_a_test then
			update cr_inter_company_test_stg set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		else
			update cr_inter_company_stg set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			update cr_allocations_test_stg set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		else
			update cr_allocations_stg set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and interface_batch_id = :i_allocation_id;
		end if
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("DERIVATIONS: OFFSETS ERROR: updating " + staging_table + ".cr_txn_type: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		return -1
	end if
	
	
	//  ----------------------------------  ROUNDING SECTION:  ----------------------------------
	
	
	//  These updates were moved from below since the proper MN is needed for the
	//  rounding function.
	if g_debug = 'YES' then
		f_pp_msgs("Performing rounding adjustment")
	end if
	
	//  CLEANUP:
	//  Fix any rounding error.  Rounding must be called for each of the a_type/a_join
	//  values passed to uf_deriver.
	//
	//  And example of the basis variable at this point would be:
	//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup = cr_deriver_control.string
	//  and crb_accounts_payable_stg.cr_txn_type = 'ORIGINAL' 
	//  and crb_accounts_payable_stg.amount_type = 1 
	//  and crb_accounts_payable_stg.company = 'CECO'
	//
	//  NOTE: The last arg (a_table_name_field) is assuming that the "join" field in the stg table
	//  is the first part of the basis from cr_batch_derivation_control (so we can strip off
	//  everything to the left of the "=" sign.  For example:
	//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup
	//
	//  NOTE: balancing uses either the traditional technique or the id_balancing technique.
	//  See comments in the UO function.
	//
	
	basis_2nd_arg = f_replace_string(basis, &
		"target_credit = 'TARGET' and cr_derivation_rate is null and interface_batch_id = " + string(i_allocation_id), &
		"cr_txn_type = 'TARGET' and interface_batch_id = " + string(i_allocation_id), "all")
	
	basis_6th_arg = left(basis, pos(basis, "=") - 1)
	
	id_balancing_basis = "cr_derivation_type = '" + the_type + "' and cr_txn_type = 'TARGET' "
	
	//if id_balancing = 1 then
		rtn = i_uo_cr_derivation.uf_deriver_rounding_error_ids( &
			the_type, staging_table, &
			basis         + " and " + staging_table + ".interface_batch_id = " + string(i_allocation_id), &
			id_balancing_basis + " and " + staging_table + ".interface_batch_id = " + string(i_allocation_id), &
			"cr_derivation_type = '" + the_type + "' and cr_txn_type = 'TARGET' and interface_batch_id = " + string(i_allocation_id), &
			basis_6th_arg)
	//else
	//	rtn = i_uo_cr_derivation.uf_deriver_rounding_error( &
	//		i_type, i_stg_table, &
	//		basis         + " and " + i_stg_table + ".month_number = " + string(g_posting_mn) + &
	//						    " and budget_version = '" + g_bv + "'", &
	//		basis_2nd_arg + " and " + i_stg_table + ".month_number = " + string(g_posting_mn) + &
	//						    " and budget_version = '" + g_bv + "'", &
	//		i_stg_table + ".cr_txn_type = 'TARGET' and " + i_stg_table + ".month_number = " + string(g_posting_mn) + &
	//						    " and budget_version = '" + g_bv + "'", &
	//		basis_6th_arg)
	//end if
	
	if rtn <> 1 then
		return -1
	end if


NEXT  //  FOR t = 1 to num_types ...


//*******************************************************************************************
//
//  Derivation Overrides:  #2 table
//
//    Re-using variables in this section from above.
//
//*******************************************************************************************
override_counter = 0
select count(*) into :override_counter from cr_deriver_or2_sources_alloc
 where allocation_id = :i_allocation_id and source_id = :source_id and rownum = 1;

if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: override_counter = " + string(override_counter))

if isnull(override_counter) or override_counter = 0 then
	//  No override, use the standard setup
	sqls = "select * from cr_deriver_override2_sources where source_id = " + string(source_id)
	i_cr_deriver_override2.SetSQLSelect(sqls)
	i_cr_deriver_override2.RETRIEVE()
	i_cr_deriver_override2.SetSort("run_order a")
	i_cr_deriver_override2.Sort()
	num_types = i_cr_deriver_override2.RowCount()
else
	//  Override for this allocation.  Note: 0 in run_order means don't run !
	sqls = "select * from cr_deriver_or2_sources_alloc " + &
		"where allocation_id = " + string(i_allocation_id) + " and source_id = " + string(source_id) + " and run_order <> 0"
	i_cr_deriver_override2_alloc.SetSQLSelect(sqls)
	i_cr_deriver_override2_alloc.RETRIEVE()
	i_cr_deriver_override2_alloc.SetSort("run_order a")
	i_cr_deriver_override2_alloc.Sort()
	num_types = i_cr_deriver_override2_alloc.RowCount()
end if

if num_types <= 0 then
	if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: No derivations defined for: " + staging_table)
	return 1 // Return, but allow to continue.  Don't want to halt just due to a cbx checked.
end if



FOR t = 1 to num_types
	
	
	
	if isnull(override_counter) or override_counter = 0 then
		//  No override, use the standard setup
		or_name  = trim(i_cr_deriver_override2.GetItemString(t, "override_name"))
		col_name = trim(i_cr_deriver_override2.GetItemString(t, "col_name"))
		suppl_wc = trim(i_cr_deriver_override2.GetItemString(t, "suppl_where_clause"))
	else
		//  Override for this allocation.
		or_name  = trim(i_cr_deriver_override2_alloc.GetItemString(t, "override_name"))
		col_name = trim(i_cr_deriver_override2_alloc.GetItemString(t, "col_name"))
		select suppl_where_clause
		  into :suppl_wc
		  from cr_deriver_override2_sources
		 where override_name = :or_name and col_name = :col_name and source_id = :source_id;
	end if
	
	if isnull(or_name)  or or_name  = "" then continue // useless record
	if isnull(col_name) or col_name = "" then continue // useless record
	if isnull(suppl_wc) then suppl_wc = "" // good record
		
	if g_debug = 'YES' then
		f_pp_msgs("or_name = " + or_name)
		f_pp_msgs("col_name = " + col_name)
		f_pp_msgs("suppl_wc = " + suppl_wc)
	end if
	
	//  Setting these to the staging_table ought to work just fine since you'd only be using
	//  these references if you're trying to update the table from itself (using the "res_table"
	//  and "src_table" keywords.  Otherwise, you'd have an override set up for allocations that
	//  probably has the "src_table" hardcoded (like work_order_control, a custom table, etc.).
	//  In all cases, I imagine, the "src table" will be referenced in the override_sql and
	//  replaced in the uf call.
	i_uo_cr_derivation.pi_s_res_table       = staging_table
	i_uo_cr_derivation.pi_s_src_table       = staging_table
	i_uo_cr_derivation.pi_s_res_arg         = string(i_allocation_id)
	i_uo_cr_derivation.i_suppl_where_clause = suppl_wc
	
	rtn = i_uo_cr_derivation.uf_deriver_update_override(or_name, col_name)
	
	if rtn <> 1 then
		return -1
	end if
	
	

NEXT  //  FOR t = 1 to num_types ...



//**********************************************************************************************
//
//  Derivations need a real interface_batch_id.  Change it back so nothing else has a problem.
//
//**********************************************************************************************
sqls = "update " + staging_table + " set interface_batch_id = NULL"
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to NULL: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if


if g_debug = 'YES' then
	f_pp_msgs("Derivations complete")
end if


return 1
end function

public function integer uf_read ();longlong cv_long, running_session_id, rtn, cmd_line_arg_elements, i
string cmd_line_arg[], sqls, to_user, str_rtn, val
environment env

w_custom_datawindows w	// declared here so the datawindows get included in the build correctly.
rtn = 1

//*****************************************************************************************
//
//   DEBUG
//
//*****************************************************************************************
select control_value into :cv_long from cr_alloc_system_control
 where upper(control_name) = 'DEBUG ACTUALS';
if cv_long = 1 then
	g_debug = 'YES'
	f_pp_msgs("DEBUG MODE")
else
	g_debug = 'NO'
end if
	
		  
//*****************************************************************************************
//
//   System Switches
//
//*****************************************************************************************
i_suspense_accounting = f_cr_system_control('ALLOCATIONS - SUSPENSE ACCOUNTING')
if isnull(i_suspense_accounting) or i_suspense_accounting <> "YES" then i_suspense_accounting = "NO"

i_concurrent_processing = f_cr_system_control('ALLOCATIONS: CONCURRENT PROCESSING')
if isnull(i_concurrent_processing) or i_concurrent_processing <> "YES" then i_concurrent_processing = "NO"

select userenv('sessionid') into :i_session_id from dual;
if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
	// Will check the running session id at an allocation level
else
	//-------  To prevent this process from running  -------
	//-------  multiple times concurrently.          -------
	running_session_id = 0
	select running_session_id into :running_session_id from pp_processes
	 where process_id = :g_process_id;
	
	if isnull(running_session_id) or running_session_id = 0 or running_session_id = i_session_id then		
		update pp_processes set running_session_id = :i_session_id
		 where process_id = :g_process_id;
		
		if sqlca.SQLCode = 0 then
			commit;
		else
			f_pp_msgs(" ")
			f_pp_msgs('ERROR: updating pp_processes.running_session_id: ' + sqlca.SQLErrText)
			f_pp_msgs(" ")
			rollback;
			return -1
		end if
	else
		//  CURRENTLY RUNNING: disconnect and halt.
		f_pp_msgs( ' ')
		f_pp_msgs( ' ')
		f_pp_msgs( 'Another session is currently running this process.')
		f_pp_msgs( '  session_id = ' + string(running_session_id))
		f_pp_msgs( ' ')
		f_pp_msgs( ' ')
	
		rollback;
		return -1
	end if
end if 


//**********************************************************************************
//
//  Before processing the command line args, get this cr_system_control switch that
//  determines whether or not they are allowed to run a range of priorities.
//
//**********************************************************************************
i_allow_priority_range = f_cr_system_control('ALLOCATIONS - ALLOW PRIORITY RANGE')
if isnull(i_allow_priority_range) or i_allow_priority_range = "" or i_allow_priority_range <> "YES" then 
	i_allow_priority_range = "NO"
end if


//**********************************************************************************
//
//  The command line argument cannot be NULL ... 
//
//  It will be passed as a comma separated list in the following format:  
//
//  processing_priority,budget_version,test_or_actual
//
//  where test_or_actual is either TEST or ACTUAL.
//
//  The start_month and end_month variables come from a select against
//  cr_alloc_system_control.
//**********************************************************************************
if g_uo_ppint.i_running_from_source and (isnull(g_command_line_args) or g_command_line_args = "") then
	// Running from source and we don't have a command line.
	// Hardcode it here for testing purposes.
	select control_value into :g_command_line_args 
	from cr_alloc_system_Control where upper(trim(control_name)) = 'CR ALLOC PBDEBUG COMMAND LINE';
end if

if isnull(g_command_line_args) or g_command_line_args = "" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The command line argument is NULL.  Cannot run the Allocations!")
	f_pp_msgs(" ")
	rtn = -1
	goto halt_the_app
end if

//  Translate the command line argument into the appropriate
//  variables.  If the 3 arguments are not passed, log an error.
//
//  DMJ: 7/21/08: Must be AT LEAST that many args, but there could be
//  more now that we added "run as commitments" as an arg.
cmd_line_arg_elements = &
	f_parsestringintostringarray(g_command_line_args, ",", cmd_line_arg)

if i_allow_priority_range = "YES" then
	
	if cmd_line_arg_elements < 4 then
		f_pp_msgs("The command line argument contained " + &
					  string(cmd_line_arg_elements) + " argument(s).  " + &
					 "It must have all 4 arguments that are required.  " + &
					 "Cannot run the Allocations!")
		rtn = -1
		goto halt_the_app
	end if
	
	i_priority       = trim(cmd_line_arg[1])
	i_priority2      = trim(cmd_line_arg[2])
	i_budget_version = trim(cmd_line_arg[3])
	
	/*### - MDZ - 9058 - 20120119*/
	/*Set to upper on the way in.*/
	//i_test_or_actual = trim(cmd_line_arg[4])
	i_test_or_actual = upper(trim(cmd_line_arg[4]))
		
else
	
	if cmd_line_arg_elements < 3 then
		f_pp_msgs("The command line argument contained " + &
					  string(cmd_line_arg_elements) + " argument(s).  " + &
					 "It must have all 3 arguments that are required.  " + &
					 "Cannot run the Allocations!")
		rtn = -1
		goto halt_the_app
	end if
	
	i_priority       = trim(cmd_line_arg[1])
	i_priority2 = i_priority
	i_budget_version = trim(cmd_line_arg[2])
	
	/*### - MDZ - 9058 - 20120119*/
	/*Set to upper on the way in.*/
	//i_test_or_actual = trim(cmd_line_arg[3])
	i_test_or_actual = upper(trim(cmd_line_arg[3]))
	
end if



if i_concurrent_processing = "YES" then
	//  With concurrent processing, we have to feed the start and end month in as part of
	//  cmd_line_arg_elements.
	if i_allow_priority_range = "YES" then
		i_start_month = long(trim(cmd_line_arg[5]))
		i_end_month   = long(trim(cmd_line_arg[6]))
	else
		i_start_month = long(trim(cmd_line_arg[4]))
		i_end_month   = long(trim(cmd_line_arg[5]))
	end if
else
	//
	//  Get the start and end month from cr_alloc_system_control ... Also get the month_period.
	//  Note that a month_period of 0 indicated "whole month".
	//
	setnull(i_start_month)
	select trim(control_value) into :i_start_month from cr_alloc_system_control
	 where upper(trim(control_name)) = 'CR ALLOCATION START MONTH';
	if isnull(i_start_month) then i_start_month = 0
	
	if i_start_month = 0 then
		f_pp_msgs("The argument: " + string(i_start_month) + " for the " + &
			"CR ALLOCATION START MONTH is invalid. Check the " + &
			"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
			"Connot run the CR Allocations!")
		rtn = -1
		goto halt_the_app
	end if
	
	setnull(i_end_month)
	select trim(control_value) into :i_end_month from cr_alloc_system_control
	 where upper(trim(control_name)) = 'CR ALLOCATION ENDING MONTH';
	if isnull(i_end_month) then i_end_month = 0
	
	if i_end_month = 0 then
		f_pp_msgs("The argument: "+ string(i_end_month) + " for the " + &
			"CR ALLOCATION ENDING MONTH is invalid. Check the " + &
			"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
			"Connot run the CR Allocations!")
		rtn = -1
		goto halt_the_app
	end if
end if

if i_concurrent_processing = "YES" then
	if i_allow_priority_range = "YES" then
		if upperbound(cmd_line_arg) >= 7 then
			i_run_as_commitments = trim(cmd_line_arg[7])
		else
			i_run_as_commitments = "NO"
		end if
	else
		if upperbound(cmd_line_arg) >= 6 then
			i_run_as_commitments = trim(cmd_line_arg[6])
		else
			i_run_as_commitments = "NO"
		end if
	end if
else
	if i_allow_priority_range = "YES" then
		if upperbound(cmd_line_arg) >= 5 then
			i_run_as_commitments = trim(cmd_line_arg[5])
		else
			i_run_as_commitments = "NO"
		end if
	else
		if upperbound(cmd_line_arg) >= 4 then
			i_run_as_commitments = trim(cmd_line_arg[4])
		else
			i_run_as_commitments = "NO"
		end if
	end if
end if

if upper(i_run_as_commitments) = "RUN_AS_COMMITMENTS" then
	i_run_as_commitments_flag = 1
else
	setnull(i_run_as_commitments_flag)
end if

setnull(i_month_period)
select trim(control_value) into :i_month_period from cr_alloc_system_control
 where upper(trim(control_name)) = 'CR ALLOCATION MONTH PERIOD';
if isnull(i_month_period) then i_month_period = 0

g_batch_id = i_priority
update pp_processes_occurrences 
   set batch_id = :i_priority
 where process_id = :g_process_id and occurrence_id = :g_occurrence_id
 using g_sqlca_logs;
	
if g_sqlca_logs.SQLCode = 0 then
	commit using g_sqlca_logs;
else
	rollback using g_sqlca_logs;
end if


//**********************************************************************************
//
//  Cache the information that we'll use over and over to save the trips to the database and memory.
//
//**********************************************************************************
f_pp_msgs("Caching Allocation Configuration")

// Retrieve the allocations to be process one time.
i_ds_alloc_list = CREATE uo_ds_top
sqls = "select a.*, b.rate_id from cr_allocation_control a, cr_alloc_target b " + &
	"	where processing_priority between " + i_priority + " and " + i_priority2 + &
	"	and a.target_id = b.target_id "
str_rtn = f_create_dynamic_ds(i_ds_alloc_list, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_allocation_control")
	f_pp_msgs(i_ds_alloc_list.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
elseif i_ds_alloc_list.rowcount() = 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: No allocations to process found in cr_allocation_control")
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if
i_ds_alloc_list.SetSort("processing_priority a, description a")
i_ds_alloc_list.Sort()


// Where Clause Criteria
i_ds_where_clause = CREATE uo_ds_top
sqls = "select * from cr_alloc_where_clause where where_clause_id in (" + &
	"select where_clause_id from cr_allocation_control " + &
	" where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + &
	" union all " + &
	"select balance_where_clause_id from cr_allocation_control " + &
	" where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + &
") "
str_rtn = f_create_dynamic_ds(i_ds_where_clause, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_where_clause")
	f_pp_msgs(i_ds_where_clause.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if
i_ds_where_clause.SetSort("row_id a")
i_ds_where_clause.Sort()


// Where Clause Tables
i_ds_where_clause_sources = CREATE uo_ds_top
sqls = "select nvl(b.table_name,'CR_COST_REPOSITORY') table_name, nvl(a.source_id,0) source_id, a.where_clause_id " + &
	" from cr_alloc_where a, cr_sources b where b.source_id (+) = nvl(a.source_id,0) and a.where_clause_id in (" + &
	"select where_clause_id from cr_allocation_control " + &
	" where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + &
	" union all " + &
	"select balance_where_clause_id from cr_allocation_control " + &
	" where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + &
") "
str_rtn = f_create_dynamic_ds(i_ds_where_clause_sources, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_where and sources")
	f_pp_msgs(i_ds_where_clause_sources.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// Group By Criteria
i_ds_group_by = create uo_ds_top
sqls = "select cr_alloc_group_by.group_by_id, cr_alloc_group_by.element_id, cr_elements.description, cr_alloc_group_by.mask_length, cr_elements.width " + &
	"from cr_alloc_group_by , cr_elements " + &
	"where group_by_id in " + &
	"	(select group_by_id from cr_allocation_control " + &
	"	where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") " + &
	"and cr_alloc_group_by.element_id = cr_elements.element_id"
str_rtn = f_create_dynamic_ds(i_ds_group_by, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_group_by")
	f_pp_msgs(i_ds_group_by.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// Target Criteria
i_ds_target = create uo_ds_top
sqls = "select * from cr_alloc_target_criteria where target_id in " + &
	"	(select target_id from cr_allocation_control " + &
	"	where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") "
str_rtn = f_create_dynamic_ds(i_ds_target, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_target_criteria")
	f_pp_msgs(i_ds_target.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if
i_ds_target.SetSort(i_department_field + " a, rate a")


// Target transposing
i_ds_target_trpo = create uo_ds_top
sqls = "select * from cr_alloc_target_criteria_trpo where target_id in " + &
	"	(select target_id from cr_allocation_control " + &
	"	where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") "
str_rtn = f_create_dynamic_ds(i_ds_target_trpo, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_target_criteria_trpo")
	f_pp_msgs(i_ds_target_trpo.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// Credit Criteria
i_ds_credit = create uo_ds_top
sqls = "select * from cr_alloc_credit_criteria where credit_id in " + &
	"	(select credit_id from cr_allocation_control " + &
	"	where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") "
str_rtn = f_create_dynamic_ds(i_ds_credit, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_credit_criteria")
	f_pp_msgs(i_ds_credit.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// Credit transposing
i_ds_credit_trpo = create uo_ds_top
sqls = "select * from cr_alloc_credit_criteria_trpo where credit_id in " + &
	"	(select credit_id from cr_allocation_control " + &
	"	where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") "
str_rtn = f_create_dynamic_ds(i_ds_credit_trpo, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_credit_criteria_trpo")
	f_pp_msgs(i_ds_credit_trpo.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// Intercompany criteria
i_ds_interco = create uo_ds_top
sqls = "select * from cr_alloc_interco_criteria where intercompany_id in " + &
	"	(select intercompany_id from cr_allocation_control " + &
	"	where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") "
str_rtn = f_create_dynamic_ds(i_ds_interco, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_alloc_interco_criteria")
	f_pp_msgs(i_ds_interco.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// Companies
i_ds_company = create uo_ds_top
sqls = "select * from cr_company"
str_rtn = f_create_dynamic_ds(i_ds_company, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_company")
	f_pp_msgs(i_ds_company.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if
// add a row for null cr_company_id
i = i_ds_company.InsertRow(0)
i_ds_company.SetItem(i,'description','<None>')
i_ds_company.SetItem(i,'owned','0')


// CR Sources
i_ds_cr_sources = create uo_ds_top
sqls = "select * from cr_sources"
str_rtn = f_create_dynamic_ds(i_ds_cr_sources, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_sources")
	f_pp_msgs(i_ds_cr_sources.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if
for i = 1 to i_ds_cr_sources.rowcount()
	val = i_ds_cr_sources.GetItemString(i,'table_name')
	val = f_cr_clean_string(upper(trim(val)))
	i_ds_cr_sources.SetItem(i,'table_name',val)
next


// Deriver type sources
i_cr_deriver_type_sources = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources"
str_rtn = f_create_dynamic_ds(i_cr_deriver_type_sources, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_company")
	f_pp_msgs(i_ds_company.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// CR ELEMENTS 
sqls = 'select * from cr_elements order by "ORDER"'
i_ds_elements = CREATE uo_ds_top
str_rtn = f_create_dynamic_ds(i_ds_elements,'grid',sqls,sqlca,true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_elements")
	f_pp_msgs(i_ds_elements.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if
i_num_elements = i_ds_elements.rowcount()
// always need the cleaned description.  Clean it now.
for i = 1 to i_num_elements
	val = i_ds_elements.GetItemString(i,'description')
	val = f_cr_clean_string(upper(trim(val)))
	i_ds_elements.SetItem(i,'description',val)
next 


// Rates input table
sqls = "select rate_id, structure_id, element_id " + &
	"from cr_rates, cr_structures " + &
	" where cr_rates.structure_id_for_rates = cr_structures.structure_id (+) " + &
	" and rate_id in " + &
	"	(select rate_id from cr_alloc_target where target_id in " + &
	"		(select target_id from cr_allocation_control " + &
	"		where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") " + &
	"	)"
i_ds_rates = CREATE uo_ds_top
str_rtn = f_create_dynamic_ds(i_ds_rates,'grid',sqls,sqlca,true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_rates")
	f_pp_msgs(i_ds_rates.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if

// Rates input table
sqls = "select * from cr_rates_input " + &
	"where (month_number between '" + string(i_start_month) + "' and '" + string(i_end_month) + "' or month_number = 'ALL') " + &
	"and rate_id in " + &
	"	(select rate_id from cr_alloc_target where target_id in " + &
	"		(select target_id from cr_allocation_control " + &
	"		where processing_priority between " + string(i_priority) + " and " + string(i_priority2) + ") " + &
	"	)"
i_ds_rates_input = CREATE uo_ds_top
str_rtn = f_create_dynamic_ds(i_ds_rates_input,'grid',sqls,sqlca,true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_rates_input")
	f_pp_msgs(i_ds_rates_input.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if


// Deriver type sources alloc
i_cr_deriver_type_sources_alloc = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources_alloc"
str_rtn = f_create_dynamic_ds(i_cr_deriver_type_sources_alloc, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_deriver_type_sources_alloc")
	f_pp_msgs(i_cr_deriver_type_sources_alloc.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if

// Override 2 sources
i_cr_deriver_override2 = CREATE uo_ds_top
sqls = "select * from cr_deriver_override2_sources"
str_rtn = f_create_dynamic_ds(i_cr_deriver_override2, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: cr_deriver_override2_sources")
	f_pp_msgs(i_cr_deriver_override2.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if

// Override 2 sources alloc
i_cr_deriver_override2_alloc = CREATE uo_ds_top
sqls = "select * from cr_deriver_or2_sources_alloc"
str_rtn = f_create_dynamic_ds(i_cr_deriver_override2_alloc, "grid", sqls, sqlca, true)
if str_rtn <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Caching cr_deriver_or2_sources_alloc")
	f_pp_msgs(i_cr_deriver_override2_alloc.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
end if

// Default GLJC for Allocations
i_default_gljc_alloc = usf_get_source_gljc('CR_ALLOCATIONS')
i_default_gljc_interco = usf_get_source_gljc('CR_INTER_COMPANY')
		  
		  
//**********************************************************************************
//
//  Other System Control Lookups
//
//**********************************************************************************
f_pp_msgs("Caching CR System Controls")
// Elim Month Parameters
select rtrim(control_value) into :i_elim_start_month from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'CR ELIMINATION START MONTH';
if isnull(i_elim_start_month) then i_elim_start_month = 0

select rtrim(control_value) into :i_elim_end_month from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'CR ELIMINATION ENDING MONTH';
if isnull(i_elim_end_month) then i_elim_end_month = 0

// Run by dept
i_department_field = f_cr_clean_string(trim(upper(f_cr_system_control('DEPT-BASED ALLOCATIONS: DEPT FIELD'))))
if isnull(i_department_field) or trim(i_department_field) = '' then
	f_pp_msgs("No DEPT-BASED ALLOCATIONS: DEPT FIELD was found in cr_system_control ...")
	return -1
end if

// WO Field
i_work_order_field = f_cr_clean_string(trim(upper(f_cr_system_control('WORK ORDER FIELD'))))
if isnull(i_work_order_field) or i_work_order_field = "" then
	f_pp_msgs("No WORK ORDER FIELD was found in cr_system_control ...")
	return -1
end if

// Company field
i_company_field = f_cr_clean_string(trim(upper(f_cr_system_control('COMPANY FIELD'))))
if isnull(i_company_field) or trim(i_company_field) = "" then
	f_pp_msgs("No COMPANY FIELD was found in cr_system_control ...")
	return -1
end if

// Use new structure tables
i_use_new_structures_table = f_cr_system_control('USE NEW CR STRUCTURE VALUES TABLE')
if isnull(i_use_new_structures_table) or i_use_new_structures_table <> 'YES' then i_use_new_structures_table = "NO"

// Run by Dept - Clearing Calc Rate - Credits Method
i_run_by_dept_credits_method = f_cr_system_control('RUN BY DEPT: CALCRATE: OBEY CREDITS')
if isnull(i_run_by_dept_credits_method) or trim(i_run_by_dept_credits_method) <> "YES" then i_run_by_dept_credits_method = "NO"

// OOB Check
i_balancing_cv = f_cr_system_control('ALLOCATIONS - OOB CHECK')
if isnull(i_balancing_cv) or i_balancing_cv <>"YES" then i_balancing_cv = "NO"

// Check to see if we have any intercompany allocation (clearing_indicator = 4).  If we do,
//	retrieve the intercompany system controls.  
//	Only want to do this if we have any so the validations only apply then.
rtn = i_ds_alloc_list.rowcount()
rtn = i_ds_alloc_list.Find('clearing_indicator = 4',1,rtn)
if rtn > 0 then
	// Intercompany trigger SQL.
	select control_value into :i_inter_co_sqls
	  from cr_system_control where upper(control_name) = 'INTER-COMPANY SQL'; 	
	if isnull(i_inter_co_sqls) or trim(i_inter_co_sqls) = "" then
		f_pp_msgs("ERROR: No inter-company criteria was found in cr_system_control ... No allocations will be calculated.")
		rollback;
		return -1
	end if
	
	// Trigger field
	select control_value into :i_interco_trigger_col
	  from cr_system_control where upper(control_name) = 'INTER-COMPANY TRIGGER FIELD';
		  
	if isnull(i_interco_trigger_col) or trim(i_interco_trigger_col) = "" then
		f_pp_msgs("ERROR: No INTER-COMPANY TRIGGER FIELD was found in cr_system_control ... No allocations will be calculated.")
		rollback;
		return -1
	end if	
	i_interco_trigger_col = f_cr_clean_string(upper(trim(i_interco_trigger_col)))

	//  Credit company SQL
	select control_value into :i_original_interco_credit_sql
	  from cr_system_control where upper(control_name) = 'INTER-COMPANY CREDIT COMPANY SQL';
		
	if isnull(i_original_interco_credit_sql) or trim(i_original_interco_credit_sql) = "" then
		f_pp_msgs("ERROR: No INTER-COMPANY CREDIT COMPANY SQL was found in cr_system_control ... No allocations will be calculated.")
		rollback;
		return -1
	end if	
end if

//  ### 29232 JAK: 2013-02-05:  Code to support non-calendar fiscal years
select control_value
into :i_init_mo_fy
from cr_alloc_system_control 
where lower(control_name) = 'initial month of fiscal year';
if isnull(i_init_mo_fy) or i_init_mo_fy < 1 or i_init_mo_fy > 12 then i_init_mo_fy = 1

//  Validation Switches
f_pp_msgs(" ")
i_me_validations = f_cr_system_control('ENABLE VALIDATIONS - ME')
if isnull(i_me_validations) or i_me_validations <> 'YES' then i_me_validations = "NO"
f_pp_msgs("Master Element Validation = " + i_me_validations)

i_combo_validations = f_cr_system_control('ENABLE VALIDATIONS - COMBO')
if isnull(i_combo_validations) or i_combo_validations <> 'YES' then i_combo_validations = "NO"
f_pp_msgs("Combination Validation    = " + i_combo_validations)

i_combo_or_control = f_cr_system_control('VALIDATIONS - CONTROL OR COMBOS')
if isnull(i_combo_or_control) or trim(i_combo_or_control) <> 'CONTROL' then i_combo_or_control = "COMBO"

i_proj_based_validations = f_cr_system_control('ENABLE VALIDATIONS - PROJECTS BASED')
if isnull(i_proj_based_validations) or i_proj_based_validations <> 'YES' then i_proj_based_validations = "NO"
f_pp_msgs("Projects-Based Validation = " + i_proj_based_validations)

i_mn_validations = f_cr_system_control('ENABLE VALIDATIONS - MONTH NUMBER')
if isnull(i_mn_validations) or i_mn_validations <> 'YES' then i_mn_validations = "NO"
f_pp_msgs("Month Number Validation   = " + i_mn_validations)

i_custom_validations = f_cr_system_control('ENABLE VALIDATIONS - CUSTOM')
if isnull(i_custom_validations) or i_custom_validations <> 'YES' then i_custom_validations = "NO"
f_pp_msgs("Custom Validation         = " + i_custom_validations)

i_disable_fast = f_cr_system_control('ALLOCATION - DISABLE DIRECT INSERT')
if isnull(i_disable_fast) or i_disable_fast <> 'YES' then i_disable_fast = "NO"
f_pp_msgs("Disable Fast         = " + i_disable_fast)

//  Set some global variables.
//  -- i_rate_field_exists: simply check cr_allocations_stg and assume everything else is
//     config'ed properly.  Need to consider performance.
i = 0
select count(*) into :i from all_tab_columns 
 where owner = 'PWRPLANT' and table_name = 'CR_ALLOCATIONS_STG' and column_name = 'ALLOCATION_RATE';
if isnull(i) or i = 0 then
	i_rate_field_exists = false
else
	i_rate_field_exists = true
end if

i = 0
select count(*) into :i from all_tab_columns 
 where owner = 'PWRPLANT' and table_name = 'CR_ALLOCATIONS_STG' and column_name = 'INCREMENTAL_RUN_ID';
if isnull(i) or i = 0 then
	i_incr_run_field_exists = false
else
	i_incr_run_field_exists = true
end if

// JAK: Dec 17, 2007:  Change all uo_ds_tops to instances and remove the destroys
//								Will do all of the creates here
i_ds_source 				= CREATE uo_ds_top
i_ds_credit_insert 		= CREATE uo_ds_top
i_ds_interco_insert 		= CREATE uo_ds_top

// ppcostrp components
i_uo_cr_validation = CREATE uo_cr_validation
i_uo_cr_derivation = CREATE uo_cr_derivation
i_uo_cr_cost_repository = create uo_cr_cost_repository

// custom function
i_uo_cleanup				= CREATE uo_cr_allocations_cleanup

//*****************************************************************************************
//
//  Shells of datastores we're going to use later...
//
//*****************************************************************************************
i_ds_sum_amount = CREATE uo_ds_top
sqls = "select sum(amount) amount, sum(quantity) quantity from cr_cost_repository where id = 0"
f_create_dynamic_ds(i_ds_sum_amount, "grid", sqls, sqlca, true)

i_ds_count = CREATE uo_ds_top
sqls = "select count(*) from cr_allocations where id = -678678"
f_create_dynamic_ds(i_ds_count, "grid", sqls, sqlca, true)

i_ds_distinct_x_cc_values = create uo_ds_top
sqls = 'select distinct "' + i_department_field + '" ' + &
	"from cr_allocations_stg " + &
	"where id = 0 "
f_create_dynamic_ds(i_ds_distinct_x_cc_values, "grid", sqls, sqlca, true)

i_ds_where_clause_incr = CREATE uo_ds_top
sqls = "select incremental_run_id, allocation_id, month_run_id, source_month_number " +&
			", run_month_number, min_source_value, max_source_value " +&
		 "from cr_alloc_incremental_control where incremental_run_id = 0 "
f_create_dynamic_ds(i_ds_where_clause_incr, "grid", sqls, sqlca, true)

//*****************************************************************************************
//
//	Flush all of the allocation tables
//
//*****************************************************************************************
sqlca.truncate_table('cr_allocations_stg')
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: flushing cr_allocations_stg: " + sqlca.SQLErrText)
	rtn = -1
	goto halt_the_app
end if

sqlca.truncate_table('cr_allocations_test_stg')
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: flushing cr_allocations_test_stg: " + sqlca.SQLErrText)
	rtn = -1
	goto halt_the_app
end if

sqlca.truncate_table('cr_inter_company_stg')
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: flushing cr_inter_company_stg: " + sqlca.SQLErrText)
	rtn = -1
	goto halt_the_app
end if

sqlca.truncate_table('cr_inter_company_test_stg')
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: flushing cr_inter_company_test_stg: " + sqlca.SQLErrText)
	rtn = -1
	goto halt_the_app
end if

//  Since we've truncated the stg tables, we don't want to leave any validation kickouts
//  hanging around ... or else they will get stranded.
delete from cr_validations_invalid_ids2
 where table_name in ('cr_allocations_stg','cr_allocations_test_stg','cr_inter_company_stg','cr_inter_company_test_stg');
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: deleting from cr_validations_invalid_ids2 for allocation tables: " + sqlca.SQLErrText)
	rtn = -1
	goto halt_the_app
end if

delete from cr_validations_invalid_ids
 where table_name in ('cr_allocations_stg','cr_allocations_test_stg','cr_inter_company_stg','cr_inter_company_test_stg');
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: deleting from cr_validations_invalid_ids2 for allocation tables: " + sqlca.SQLErrText)
	rtn = -1
	goto halt_the_app
end if

//MWA 20141231 analyze the incremental control table for performance
sqlca.analyze_table('cr_alloc_incremental_control');
if sqlca.sqlcode < 0 then
	f_pp_msgs("The analyze_table function returned the following error.  " + &
		"Processing WILL continue.  Analyze Error = " + sqlca.sqlerrtext)
end if



//*****************************************************************************************
//
//  RUN the Allocations:
//
//*****************************************************************************************
i_suspense_processed = false
rtn = uf_run()


//*****************************************************************************************
//
//  HALT the application ...
//
//*****************************************************************************************
halt_the_app:

if rtn = 1 then
	f_pp_msgs("  ")
	f_pp_msgs("The allocations RAN SUCCESSFULLY.")
	f_pp_msgs("  ")
	if i_suspense_processed then
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"   -------------------------------------------------------------")
		f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  " + &
			"SUSPENSE ACCOUNTS HAVE BEEN APPLIED AND THE ALLOCATIONS HAVE BEEN POSTED!  *****")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"   -------------------------------------------------------------")
		f_pp_msgs("  ")
	end if
	g_rtn_code = 0
else
	g_rtn_code = rtn
	
	//  Mail Logic:
	if upper(i_test_or_actual) <> 'TEST' then
		
		// Find the user (from cr_system_control) to send a failure e-mail to.
		setnull(to_user)
		select trim(control_value) into :to_user from cr_system_control
		 where upper(control_name) = 'ALLOCATIONS ERROR EMAIL RECIPIENT';
		if isnull(to_user) or to_user = "" then to_user = "None"
		
		if upper(to_user) <> "NONE" then
			
			//  Set the correct win API variable.
			getenvironment(env) 
			choose case env.OSType
				case Windows!  
					if env.win16 = true then 
						uo_winapi = create u_external_function_winapi
					else
						uo_winapi = create u_external_function_win32
					end if	
				case WindowsNT!
					if env.win16 = true then 
						uo_winapi = create u_external_function_winapi
					else	 
						uo_winapi = create u_external_function_win32
					end if
			end choose
			
			f_pp_msgs("Sending error email to user: " + to_user)
			g_msmail = create uo_smtpmail
			f_send_mail('pwrplant','CR_ALLOCATIONS.EXE: TERMINATED WITHOUT COMPLETING', &
				'The cr_allocations encountered an error or validation kickouts and terminated ' + &
				'without completing.  See the PowerPlant Online Logs for detailed information.',to_user)
		
			destroy g_msmail
		
		end if  //  if upper(to_user) <> "NONE" then ...
		
	end if  //  if upper(i_test_or_actual) <> 'TEST' then ...
	
end if  //  if rtn = 1 then ...

return rtn
end function

public function longlong uf_run ();//******************************************************************************************
//
//  Window Function  :  cb_run
//
//  Description      :  Runs a batch of allocations based on input parameters.
//
//******************************************************************************************
longlong num_rows, i, allocation_id, counter, ii, rtn, elim_flag, num_types, &
			audit_type, audit_offset_type, sess, counter2, rtn_unlock, lookup_month_number, &
			incremental_alloc, counter3, counter4
string   sqls, s_date, descr, dserror, the_type, insert_or_update, offset_type
datetime started_at
date     ddate
time     ttime
longlong orig_start_month, orig_end_month, processing_priority

orig_start_month = i_start_month
orig_end_month = i_end_month

if upper(i_test_or_actual) = "TEST" then
	i_run_as_a_test = true
else
	i_run_as_a_test = false
end if

if i_test_or_actual = 'TEST' then
	f_pp_msgs("  ")
	f_pp_msgs("THIS IS A TEST RUN.  RESULTS WILL BE WRITTEN TO CR_ALLOCATIONS_TEST ")
	f_pp_msgs("                                             OR CR_INTER_COMPANY_TEST ")
end if

if i_month_period = 0 then
	f_pp_msgs("  ")
	if i_allow_priority_range = "YES" then
		f_pp_msgs("Running ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "PRIORITY = " + i_priority + " through " + i_priority2 + ", ")
	else
		f_pp_msgs("Running ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "PRIORITY = " + i_priority + ", ")
	end if
	f_pp_msgs("  ")
else
	f_pp_msgs("  ")
	if i_allow_priority_range = "YES" then
		f_pp_msgs("Running ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "MONTH PERIOD = " + string(i_month_period) + ", " + &
					 "PRIORITY = " + i_priority + " through " + i_priority2 + ", ")
	else
		f_pp_msgs("Running ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "MONTH PERIOD = " + string(i_month_period) + ", " + &
					 "PRIORITY = " + i_priority + ", ")
	end if
	f_pp_msgs("  ")
end if

//  Run derivation audits up-front so we don't slow down each allocation.
uo_ds_top ds_deriver_audits
ds_deriver_audits = CREATE uo_ds_top
sqls = &
	'select distinct "TYPE", insert_or_update, offset_type, audit_type, audit_offset_type ' + &
	  "from cr_deriver_type_sources where source_id in ( " + &
		"select source_id from cr_sources " + &
		 "where upper(trim(description)) in ('ALLOCATIONS','ALLOCATIONS-TEST','INTER-COMPANY','INTER-COMPANY-TEST'))"
f_create_dynamic_ds(ds_deriver_audits, "grid", sqls, sqlca, true)
num_types = ds_deriver_audits.RowCount()

if num_types > 0 then f_pp_msgs("Performing derivation audits")
for i = 1 to num_types
	
	the_type         = ds_deriver_audits.GetItemString(i, 1)
	insert_or_update = ds_deriver_audits.GetItemString(i, 2)
	offset_type      = ds_deriver_audits.GetItemString(i, 3)
	audit_type       = ds_deriver_audits.GetItemNumber(i, 4)
	audit_offset_type= ds_deriver_audits.GetItemNumber(i, 5)
	if isnull(audit_type)        then audit_type        = 0
	if isnull(audit_offset_type) then audit_offset_type = 0
	
	if audit_type <> 1 then goto audit_offsets
	
	//  Currently not auditing "Single Record For Default Type" since we have no default
	//  logic in cr_deriver_type_sources.
	
	if upper(insert_or_update) = "INSERT" then  //  Run "Disallow Negative Percents" ...
		if g_debug = 'YES' then f_pp_msgs("-- Running derivation audit: Disallow Negative Percents, for type: " + the_type + "")
		rtn = i_uo_cr_derivation.uf_deriver_audits(the_type, "Disallow Negative Percents")
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("FAILED AUDIT: There are negative percentages in cr_deriver_control for " + &
				"derivation type: " + the_type + ".")
			f_pp_msgs("The allocations cannot run in this state.")
			f_pp_msgs("  ")
			return -1
		end if
	end if  //  Run "Disallow Negative Percents" ...
	
	audit_offsets:
	
	if audit_offset_type <> 1 then goto next_type
	
	if not isnull(offset_type) and trim(offset_type) <> "" then  //  Run "Single Record For Offsets" ...
		if g_debug = 'YES' then f_pp_msgs("-- Running derivation audit: Single Record For Offsets, for type: " + offset_type + "")
		rtn = i_uo_cr_derivation.uf_deriver_audits(offset_type, "Single Record For Offsets")
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("FAILED AUDIT: There are " + string(i_uo_cr_derivation.i_counter) + " records in " + &
				"cr_deriver_control for the default targets (TYPE = '" + offset_type + "')")
			f_pp_msgs("There must be one and only one of these records.")
			f_pp_msgs("The allocations cannot run in this state.")
			f_pp_msgs("  ")
			return -1
		end if
	end if  //  Run "Single Record For Offsets" ...
	
	next_type:
next
//  End of derivation audits.

//  2/21/06:  NStar needed a custom function to help with elims.
rtn = f_cr_allocations_custom("wf_run_before_loop") 
if rtn < 0 then 
	return -1 
end if 

// Before we start, build out the cr_month_number table so we don't have to do it inside of the loop.
if i_end_month > 0 and i_start_month > 0 then
	for i = i_start_month to i_end_month
		if mod(i,100) >= 13 then continue
		
		//  Insert the next month_number into cr_month_number, if it does not exist ...
		if mod(i,100) = 12 then
			lookup_month_number = i + 89
		else
			lookup_month_number = i + 1
		end if
		
		insert into cr_month_number (month_number)
			select :lookup_month_number
			from dual
			where not exists (select 1 from cr_month_number where month_number = :lookup_month_number);
		
		if sqlca.SQLCode = 0 then
			commit;
		else
			rollback;
		end if
	next
end if

if i_elim_start_month > 0 and i_elim_end_month > 0 and i_ds_alloc_list.Find('elim_flag = 1',1,i_ds_alloc_list.RowCount()) > 0 then
	for i = i_elim_start_month to i_elim_end_month
		if mod(i,100) >= 13 then continue
		
		//  Insert the next month_number into cr_month_number, if it does not exist ...
		if mod(i,100) = 12 then
			lookup_month_number = i + 89
		else
			lookup_month_number = i + 1
		end if
		
		insert into cr_month_number (month_number)
			select :lookup_month_number
			from dual
			where not exists (select 1 from cr_month_number where month_number = :lookup_month_number);
			
		if sqlca.SQLCode = 0 then
			commit;
		else
			rollback;
		end if
	next
end if
f_pp_msgs("*******************************************")
			
num_rows = i_ds_alloc_list.RowCount()

for i = 1 to num_rows
	allocation_id = i_ds_alloc_list.GetItemNumber(i, "allocation_id")
	processing_priority = i_ds_alloc_list.GetItemNumber(i, "processing_priority")
	descr         = i_ds_alloc_list.GetItemString(i, "description")
	elim_flag     = i_ds_alloc_list.GetItemNumber(i, "elim_flag")
	incremental_alloc 	 = i_ds_alloc_list.GetItemNumber(i,"incremental_allocation")

	if isnull(elim_flag) then elim_flag = 0
	if isnull(incremental_alloc) then incremental_alloc = 0
	
	if g_debug = 'YES' then f_pp_msgs("-- elim flag : " + string(elim_flag) )
	if g_debug = 'YES' then f_pp_msgs("-- incremental alloc : " + string( incremental_alloc) )
	if g_debug = 'YES' then f_pp_msgs("-- suspense accounting : " + i_suspense_accounting)
	if g_debug = 'YES' then f_pp_msgs("-- concurrent processing : " + i_concurrent_processing)
	
	if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
		//  Is anyone running this allocation ?
		sess = 0
		select running_session_id into :sess from cr_allocation_control
		 where allocation_id = :allocation_id and running_session_id is not null and rownum = 1;
		if isnull(sess) then sess = 0
		
		if sess > 0 then
			// ### 7485:  JAK: 2011-05-04:  Also write out the allocation id when this occurs since it's not always obvious.
			f_pp_msgs("***** Another session (" + string(sess) + ") is running Allocation ID " + string(allocation_id) + ".")
			f_pp_msgs("***** Continuing on to the next allocation in this priority.")
			f_pp_msgs("*******************************************")
			continue // To the next allocation.
		end if
		
		//  Lock this allocation in case they are running suspense accounting
		//  which allows for concurrent processing.  A commit is OK here since
		//  one will be firing shortly in uf_start_allocations
		update cr_allocation_control set running_session_id = :i_session_id
		 where allocation_id = :allocation_id;
		
		if sqlca.SQLCode <> 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_allocation_control.running_session_id: " + &
				string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		else
			commit;
		end if
	end if  //  if suspense_accounting = "YES" then ...
	
	if elim_flag = 1 then
		i_start_month = i_elim_start_month
		i_end_month = i_elim_end_month
	else
		i_start_month = orig_start_month
		i_end_month = orig_end_month
	end if
	
	for ii = i_start_month to i_end_month
		
		if i_month_period = 0 then
			//  Whole month message.
			f_pp_msgs("Started: Priority: " + string(processing_priority) + "  Allocation ID: " + string(allocation_id) + ":  " + &
						  descr + " for month: " + string(ii))
		else
			//  Month period message.
			f_pp_msgs("Started: Priority: " + string(processing_priority) + "  Allocation ID " + string(allocation_id) + ":  " + &
						  descr + " for month: " + string(ii) + "  period: " + string(i_month_period))
		end if
		
		
		if elim_flag = 1 then
			
			//  DMJ: 12/13/05: Elims must auto-delete since they will need to be re-run multiple
			//                 times during the close.  This is in contrast to allocations which
			//                 generally run once during the close and we do not want auto-delete
			//                 code.
			rtn = uf_auto_delete_elim(allocation_id, ii, i_month_period, i_test_or_actual)
			
			//  With -1 OR -2 there is no point in continuing since in either case we will get
			//  either subsequent errors or invalid results.  Returning -1 will pass that code
			//  out to a scheduler that can be monitored.
			choose case rtn
				case 1
					//  Success ... keep going.
				case -1
					//  SQL error ...
					rollback;
					if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
						rtn = uf_unlock(allocation_id)
						if rtn = -1 then return -1
					end if
					return -1
				case -2
					//  Could not delete the elim results (e.g. it is posted to the GL)
					if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
						rtn = uf_unlock(allocation_id)
						if rtn = -1 then return -1
					end if
					return -1
			end choose
			
		end if
		
		
		//  Cannot run an allocation (Test or Actual) more than one time per month if it has
		//  already run as "whole month".
			if g_debug = 'YES' then f_pp_msgs("-- i month period : " + string(i_month_period))
		if i_month_period = 0 then
			//  WHOLE MONTH:  Commented out the whole_month piece of the where clause.  An 
			//  allocation cannot run in the "whole month" mode if it has already run AT ALL in
			//  the given month.  The implication is that if you start an allocation in a month
			//  using the month_period method, you must finish it up for the month using the 
			//  month_period method.
	
			counter = 0
			select count(*) into :counter
			  from cr_alloc_process_control
			 where allocation_id = :allocation_id and
					 month_number  = :ii            and
					 upper(test_or_actual) = :i_test_or_actual;
			if g_debug = 'YES' then f_pp_msgs("-- counter validation : " + string(counter))

			if incremental_alloc = 1 then
				
				if g_debug = 'YES' then f_pp_msgs("-- run month number :"+string(ii))
				
				counter3 = 0
				select count(*) into :counter3
				  from cr_alloc_incremental_control
				where allocation_id = :allocation_id and
						run_month_number = :ii;
						
				if g_debug = 'YES' then f_pp_msgs("-- incremental validation (counter3): " + string(counter3))
						
				counter4 = 0
				select count(*) into :counter4
				  from cr_alloc_incremental_control
				where allocation_id = :allocation_id
				and incremental_status_id = 1
				and run_month_number > :ii;
				if g_debug = 'YES' then f_pp_msgs("-- incremental validation (counter4) : " + string(counter4))
				
			end if 
			
			if upper(i_run_as_commitments) = "RUN_AS_COMMITMENTS" then
				
				if g_debug = 'YES' then f_pp_msgs(i_run_as_commitments)
				//  We know this allocation ran previously.  Now we need to know if it ran previously
				//  as "commitments".  If yes, then auto-delete those results.
				counter2 = 0
				select count(*) into :counter2
				  from cr_alloc_process_control
				 where allocation_id = :allocation_id and
						 month_number  = :ii            and
						 upper(test_or_actual) = :i_test_or_actual and run_as_commitments = 1;
				if g_debug = 'YES' then f_pp_msgs("-- validation (counter2) : " + string(counter2))
										
				if counter2 > 0 then
					
					//  ***  WE ARE LEVERAGING THIS FUNCTION SINCE IT SHOULD WORK THE SAME
					//  ***  ON THESE REGULAR ALLOCATIONS !!!
					//  DMJ: 12/13/05: Elims must auto-delete since they will need to be re-run multiple
					//                 times during the close.  This is in contrast to allocations which
					//                 generally run once during the close and we do not want auto-delete
					//                 code.
					rtn = uf_auto_delete_elim(allocation_id, ii, i_month_period, i_test_or_actual)
					
					//  With -1 OR -2 there is no point in continuing since in either case we will get
					//  either subsequent errors or invalid results.  Returning -1 will pass that code
					//  out to a scheduler that can be monitored.
					choose case rtn
						case 1
							//  Success ... keep going.
						case -1
							//  SQL error ...
							rollback;
							if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
								rtn = uf_unlock(allocation_id)
								if rtn = -1 then return -1
							end if
							return -1
						case -2
							//  Could not delete the elim results (e.g. it is posted to the GL)
							if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
								rtn = uf_unlock(allocation_id)
								if rtn = -1 then return -1
							end if
							return -1
					end choose
					
					counter = 0 // So the message below does not fire and the allocation will run.
					
				end if  //  if counter2 > 0 then ...
				
			end if  //  if upper(g_run_as_commitments) = "RUN_AS_COMMITMENTS" then ...
			
			if incremental_alloc <> 1 and counter > 0 then //non-incremental check
				f_pp_msgs("    This allocation has already run for " + string(ii) + &
							  ".  You must delete this month's results to re-run" + &
							  " the allocation.")
				f_pp_msgs("*******************************************")
				if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
					rtn = uf_unlock(allocation_id)
					if rtn = -1 then return -1
				end if
				continue
			elseif incremental_alloc = 1 and counter > 0 and counter3 = 0 then
				f_pp_msgs("    This allocation has already run for " + string(ii) + &
							  ".  You must delete this month's results to run the allocation" + &
							  " incrementally.")
				f_pp_msgs("*******************************************")
				if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
					rtn = uf_unlock(allocation_id)
					if rtn = -1 then return -1
				end if	
				continue
			elseif incremental_alloc = 1 and counter4 > 0 then
				f_pp_msgs("    This allocation has already run incrementally in a month" + &
							  " after " + string(ii) + ". You should delete the later month results" + &
							  " to run incrementally for " + string(ii) + ".")
				f_pp_msgs("*******************************************")
				if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
					rtn = uf_unlock(allocation_id)
					if rtn = -1 then return -1
				end if
				continue
			end if
			
		else
			
			//  MONTH PERIOD:  This one is a little more variable.  The "month_period" allocation
			//  cannot run if it has either 1) run for the month and period already -OR- 2) run
			//  for the month in the "whole month" mode.
			counter = 0
			select count(*) into :counter
			  from cr_alloc_process_control
			 where allocation_id = :allocation_id and
				    month_number  = :ii            and
				  ( month_period  = :i_month_period or whole_month   = 1 ) and
					 upper(test_or_actual) = :i_test_or_actual;
			
									if upper(i_run_as_commitments) = "RUN_AS_COMMITMENTS" then
										
										//  We know this allocation ran previously.  Now we need to know if it ran previously
										//  as "commitments".  If yes, then auto-delete those results.
										counter2 = 0
										select count(*) into :counter2
										  from cr_alloc_process_control
										 where allocation_id = :allocation_id and
												 month_number  = :ii            and
											  ( month_period  = :i_month_period or whole_month   = 1 ) and
												 upper(test_or_actual) = :i_test_or_actual and run_as_commitments = 1;
										
										if counter2 > 0 then
											
											//  ***  WE ARE LEVERAGING THIS FUNCTION SINCE IT SHOULD WORK THE SAME
											//  ***  ON THESE REGULAR ALLOCATIONS !!!
											//  DMJ: 12/13/05: Elims must auto-delete since they will need to be re-run multiple
											//                 times during the close.  This is in contrast to allocations which
											//                 generally run once during the close and we do not want auto-delete
											//                 code.
											rtn = uf_auto_delete_elim(allocation_id, ii, i_month_period, i_test_or_actual)
											
											//  With -1 OR -2 there is no point in continuing since in either case we will get
											//  either subsequent errors or invalid results.  Returning -1 will pass that code
											//  out to a scheduler that can be monitored.
											choose case rtn
												case 1
													//  Success ... keep going.
												case -1
													//  SQL error ...
													rollback;
													if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
														rtn = uf_unlock(allocation_id)
														if rtn = -1 then return -1
													end if
													return -1
												case -2
													//  Could not delete the elim results (e.g. it is posted to the GL)
													if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
														rtn = uf_unlock(allocation_id)
														if rtn = -1 then return -1
													end if
													return -1
											end choose
											
											counter = 0 // So the message below does not fire and the allocation will run.
											
										end if  //  if counter2 > 0 then ...
										
									end if  //  if upper(g_run_as_commitments) = "RUN_AS_COMMITMENTS" then ...
			
			if counter > 0 then
				f_pp_msgs("    This allocation has already run for EITHER the month: " + string(ii) + &
							 " and the period: " + string(i_month_period) + " - OR - has already " + &
							 "run for the whole month." + &
							  ".  You must delete this month's results to re-run" + &
							  " the allocation.")
				f_pp_msgs("*******************************************")
				if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
					rtn = uf_unlock(allocation_id)
					if rtn = -1 then return -1
				end if
				continue
			end if
		end if  //  if i_month_period = 0 then ...

					
		//  Cannot run a test allocation if the actual allocation has already run for this
		//  month and period ... potential for misleading results based on source data plus
		//  actual allocation results.
		if i_test_or_actual = "TEST" then
			
			//  Same logic as above ...
			if i_month_period = 0 then
				
				counter = 0
				select count(*) into :counter
				  from cr_alloc_process_control
				 where allocation_id = :allocation_id and
						 month_number  = :ii            and
//						 whole_month   = 1              and
						 upper(test_or_actual) = 'ACTUAL';
				
				if counter > 0 then
					f_pp_msgs("ACTUAL results exist for this allocation for " + string(ii) + &
								 ".  Cannot run this TEST allocation.")
					f_pp_msgs("*******************************************")
					if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
						rtn = uf_unlock(allocation_id)
						if rtn = -1 then return -1
					end if
					continue
				end if
			 				
			else
				
				counter = 0
				select count(*) into :counter
				  from cr_alloc_process_control
				 where allocation_id = :allocation_id and
						 month_number  = :ii            and
					  ( month_period  = :i_month_period or whole_month   = 1 ) and
						 upper(test_or_actual) = 'ACTUAL';
				
				if counter > 0 then
					f_pp_msgs("ACTUAL results exist for this allocation for " + string(ii) + &
								 ".  Cannot run this TEST allocation.")
					f_pp_msgs("*******************************************")
					if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
						rtn = uf_unlock(allocation_id)
						if rtn = -1 then return -1
					end if
					continue
				end if
				
			end if  //  if i_month_period = 0 then ...
			
		end if  //  if run_as = "TEST" then ...
		
		
		//  Cannot run an actual allocation if the test allocation has already run for this
		//  month and period ... potential for double counting or leaving a residual result
		//  in the test table.
		if i_test_or_actual = "ACTUAL" then
			
			//  Same logic as above ...
			if i_month_period = 0 then
				
				counter = 0
				select count(*) into :counter
				  from cr_alloc_process_control
				 where allocation_id = :allocation_id and
						 month_number  = :ii            and
//						 whole_month   = 1              and
						 upper(test_or_actual) = 'TEST';
				
				if counter > 0 then
					f_pp_msgs("TEST results exist for this allocation for " + string(ii) + &
								 ".  Cannot run this ACTUAL allocation.")
					f_pp_msgs("*******************************************")
					if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
						rtn = uf_unlock(allocation_id)
						if rtn = -1 then return -1
					end if
					continue
				end if
			 				
			else
				
				counter = 0
				select count(*) into :counter
				  from cr_alloc_process_control
				 where allocation_id = :allocation_id and
						 month_number  = :ii            and
					  ( month_period  = :i_month_period or whole_month   = 1 ) and
						 upper(test_or_actual) = 'TEST';
				
				if counter > 0 then
					f_pp_msgs("TEST results exist for this allocation for " + string(ii) + &
								 ".  Cannot run this ACTUAL allocation.")
					f_pp_msgs("*******************************************")
					if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
						rtn = uf_unlock(allocation_id)
						if rtn = -1 then return -1
					end if
					continue
				end if
				
			end if  //  if i_month_period = 0 then ...
			
		end if  //  if run_as = "ACTUAL" then ...

		// MWA 20141110
		// Prevent Incremental allocations running as "TEST" altogether. Does not play nice
		// with logic of identifying ranges and what not for more than one run per month
		if i_test_or_actual = "TEST" and incremental_alloc = 1 then
			f_pp_msgs("WARNING: Cannot run incremental allocations in TEST mode." +&
							" 	This allocation will not run!")
			f_pp_msgs("*******************************************")
			if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
				rtn = uf_unlock(allocation_id)
				if rtn = -1 then return -1
			end if
			continue
		end if
		
		i_ds_alloc_list_row = i
		i_allocation_id  = allocation_id
		i_month_number   = ii
		i_month_period   = i_month_period  //  This used to be hardcoded to 0.\
		
		if i_month_period = 0 then
			i_whole_month = true
		else
			i_whole_month = false
		end if
		
		rtn = uf_start_allocations()
		
		if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
			rtn_unlock = uf_unlock(allocation_id)
			if rtn_unlock = -1 then return -1
		end if
		
		if rtn < 0 then
			// ### 8142: JAK: 2011-11-01:  Consistent return codes
			return rtn
		end if
		
	next  //  for ii = month_number to month_number2 ...
	
next  //  for i = 1 to num_rows ...

rtn = f_cr_allocations_custom("After Main Loop")
if rtn < 0 then return rtn

return 1
end function

public function longlong uf_auto_delete_elim (longlong a_allocation_id, longlong a_month_number, longlong a_month_period, string a_run_as);//*******************************************************************************************
//
//  Function     :  wf_auto_delete_elim ... copied in from w_cr_control.(tab).cb_delete
//
//  Description  :  This script will delete the results of an allocation from the cost
//                  repository.  The user selects the allocation type and the month
//                  and period to delete.
//
//  DMJ: 12/12/05:  The batch_id -173 is "elims that have not yet been approved to post 
//                  to the GL".  These need to be OK to delete since they have not really
//                  posted to the GL.  Decode the batch_id.
//
//                  THIS CODE WAS COPIED FROM THE DELETE BUTTON ON W_CR_CONTROL.
//
//  Return Codes :   1 = Success
//                  -1 = SQL error
//                  -2 = Cannot delete the elim results (e.g. it has been posted to the GL)
//
//*******************************************************************************************
longlong rtn, month_number, month_period, counter, month_row, clearing_indicator, reversed, &
		 whole_month, posted_counter
string description


f_pp_msgs("Automatically deleting the prior results ...")


setnull(description)
clearing_indicator = 0

select description, clearing_indicator into :description, :clearing_indicator
  from cr_allocation_control where allocation_id = :a_allocation_id;

if isnull(description) then description = ""
if isnull(clearing_indicator) then clearing_indicator = 0


//
//  THIS WAS IN CB_DELETE ... WE SHOULD SIMPLY BE ABLE TO USE THE MP ARG PASSED
//  INTO THIS FUNCTION.
//
////  If the allocation has not run for this month and period, there is nothing to do ...
////  If it was run as "whole month", month_period = 0 ...
//whole_month = 0
//select whole_month into :whole_month
//  from cr_alloc_process_control 
// where allocation_id         = :a_allocation_id and
// 		 month_number          = :a_month_number  and
//		 upper(test_or_actual) = :a_run_as;
//		 
//if whole_month = 1 then
//	month_period = 0
//end if


//  Has the elim run for this month ?
counter = 0
select count(*) into :counter
  from cr_alloc_process_control 
 where allocation_id         = :a_allocation_id and
		 month_number          = :a_month_number  and
		 month_period          = :a_month_period  and
		 upper(test_or_actual) = :a_run_as        and
		 rownum = 1;

if counter <= 0 then
	//  The elim has not been run yet for this month.  Simply return to the caller.
	//  The cb_delete button was producing a messagebox, but we have no need for an
	//  informational log message in the batch allocation run.
	return 1
end if


//  Elims that are approved to post cannot be deleted ...
counter = 0
select count(*) into :counter from cr_alloc_process_control
 where allocation_id         = :a_allocation_id and
		 month_number          = :a_month_number  and
		 month_period          = :a_month_period  and
		 upper(test_or_actual) = :a_run_as        and
		 (elim_approving_user is not null or elim_approval_date is not null) and
		 rownum = 1;
if isnull(counter) then counter = 0

if counter > 0 then
	f_pp_msgs("  ")
	f_pp_msgs("allocation_id " + string(a_allocation_id) + &
		" has already been approved to post to the GL or Projects.~n" + &
		"The elimination will not be deleted.  Since this priority contains " + &
		"eliminations, the allocation program WILL TERMINATE.")
	f_pp_msgs("  ")
	rollback;  //  Rollback any prior "touching" of this allocation ...
	return -2
end if


//  Warn them ... THERE IS A MESSAGEBOX WARNING IN CB_DELETE AT THIS POINT.  NO
//  NEED FOR THAT IN THE BATCH ALLOCATION RUN.



//  FIRST: delete records from the cr_cost_repository table ...
if g_debug = 'YES' then
	f_pp_msgs("Deleting cr_cost_repository records (Reversal) ...")
end if

//  Deleting any reversals that may exist ... 
//  NO, NO, NO - Only perform this if the allocation has been reversed (it is very slow) ...
//  We must now perform this every time, since the "undo" functionality will populate
//  the reversal id ... there is no way to selectively determine if an allocation has
//  been "un-done" like we can with reversals since the cr_alloc_process_control record
//  gets deleted with an "undo".
if a_run_as = "ACTUAL" then
	reversed = 1
//	reversed = 0
//	select reversed into :reversed
//	  from cr_alloc_process_control
//	 where month_number  = :month_number and 
//   		 month_period  = :month_period and 
//		  	 allocation_id = :allocation_id and
//			 upper(test_or_actual) = 'ACTUAL';
//	if isnull(reversed) then reversed = 0
	
	if reversed = 1 then 	
		if clearing_indicator = 4 then
			
			posted_counter = 0
			select count(*) into :posted_counter from cr_cost_repository where drilldown_key in 
				(select drilldown_key from cr_inter_company a where exists
					(select * from cr_inter_company b
		   		  where a.reversal_id = b.id and
      				     month_number  = :a_month_number and 
  	      				  month_period  = :a_month_period and 
							  nvl(interface_batch_id,'0') = '0' and
		  	      	  	  allocation_id = :a_allocation_id)) 
				and (decode(batch_id,          '-173',NULL,batch_id)           is not null or 
                 decode(cwip_charge_status,'-173',NULL,cwip_charge_status) is not null) 
				and rownum = 1;
			if isnull(posted_counter) then posted_counter = 0
			
			if posted_counter > 0 then
				f_pp_msgs("  ")
				f_pp_msgs("allocation_id " + string(a_allocation_id) + &
					" has already been posted to either the GL or Projects.~n" + &
					"The elimination will not be deleted.  Since this priority contains " + &
					"eliminations, the allocation program WILL TERMINATE.")
				f_pp_msgs("  ")
				rollback;  //  Rollback any prior "touching" of this allocation ...
				return -2
			end if
			
			delete from cr_cost_repository where drilldown_key in 
				(select drilldown_key from cr_inter_company a where exists
					(select * from cr_inter_company b
		   		  where a.reversal_id = b.id and
      				     month_number  = :a_month_number and 
  	      				  month_period  = :a_month_period and 
							  nvl(interface_batch_id,'0') = '0' and
		  	      	  	  allocation_id = :a_allocation_id));
								 
		else
			
			posted_counter = 0
			select count(*) into :posted_counter from cr_cost_repository where drilldown_key in 
				(select drilldown_key from cr_allocations a where exists
					(select * from cr_allocations b
		   		  where a.reversal_id = b.id and
      				     month_number  = :a_month_number and 
  	      				  month_period  = :a_month_period and 
							  nvl(interface_batch_id,'0') = '0' and
		  	      		  allocation_id = :a_allocation_id))
				and (decode(batch_id,          '-173',NULL,batch_id)           is not null or 
                 decode(cwip_charge_status,'-173',NULL,cwip_charge_status) is not null) 
				and rownum = 1;
			if isnull(posted_counter) then posted_counter = 0
			
			if posted_counter > 0 then
				f_pp_msgs("  ")
				f_pp_msgs("allocation_id " + string(a_allocation_id) + &
					" has already been posted to either the GL or Projects.~n" + &
					"The elimination will not be deleted.  Since this priority contains " + &
					"eliminations, the allocation program WILL TERMINATE.")
				f_pp_msgs("  ")
				rollback;
				return -2
			end if
			
			delete from cr_cost_repository where drilldown_key in 
				(select drilldown_key from cr_allocations a where exists
					(select * from cr_allocations b
		   		  where a.reversal_id = b.id and
      				     month_number  = :a_month_number and 
  	      				  month_period  = :a_month_period and 
							  nvl(interface_batch_id,'0') = '0' and
		  	      		  allocation_id = :a_allocation_id));
		end if
	end if  //  if reversed = 1 then  ...
else
	//  "TEST" allocations and inter-company transactions do not 
	//   update the cr_cost_repository ...
end if
					 
if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting reversals from cr_cost_repository: " + &
	            sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  Deleting the results of the allocation itself ...
if g_debug = 'YES' then
	f_pp_msgs("Deleting cr_cost_repository records (Allocation Results) ...")
end if
if a_run_as = "ACTUAL" then
	if clearing_indicator = 4 then
		//  THE "EXISTS" IS SLOW ...
//		delete from cr_cost_repository where 
//   		month_number = :month_number and 
//			month_period = :month_period and exists
//			(select * from cr_inter_company
//		     where month_number  = :month_number  and 
//			        month_period  = :month_period  and 
//               nvl(interface_batch_id,'0') = '0' and
//			        allocation_id = :allocation_id and
//         		  cr_cost_repository.drilldown_key = cr_inter_company.drilldown_key);
		
		posted_counter = 0
		select count(*) into :posted_counter from cr_cost_repository where 
   		month_number = :a_month_number and 
			month_period = :a_month_period and 
			drilldown_key in
			   (select drilldown_key from cr_inter_company
		        where month_number  = :a_month_number  and 
			           month_period  = :a_month_period  and 
						  nvl(interface_batch_id,'0') = '0'  and
	   		        allocation_id = :a_allocation_id)
			and (decode(batch_id,          '-173',NULL,batch_id)           is not null or 
              decode(cwip_charge_status,'-173',NULL,cwip_charge_status) is not null) 
			and rownum = 1;
		if isnull(posted_counter) then posted_counter = 0
			
		if posted_counter > 0 then
			f_pp_msgs("  ")
			f_pp_msgs("allocation_id " + string(a_allocation_id) + &
				" has already been posted to either the GL or Projects.~n" + &
				"The elimination will not be deleted.  Since this priority contains " + &
					"eliminations, the allocation program WILL TERMINATE.")
			f_pp_msgs("  ")
			rollback;  //  Rollback any prior "touching" of this allocation ...
			return -2
		end if
		
		delete from cr_cost_repository where 
   		month_number = :a_month_number and 
			month_period = :a_month_period and 
			drilldown_key in
			   (select drilldown_key from cr_inter_company
		        where month_number  = :a_month_number  and 
			           month_period  = :a_month_period  and 
						  nvl(interface_batch_id,'0') = '0'  and
	   		        allocation_id = :a_allocation_id);
	else
		//  THE "EXISTS" IS SLOW ...
//		delete from cr_cost_repository where 
//   		month_number = :month_number and 
//			month_period = :month_period and exists
//			(select * from cr_allocations
//		     where month_number  = :month_number  and 
//			        month_period  = :month_period  and 
//               nvl(interface_batch_id,'0') = '0' and
//	   		     allocation_id = :allocation_id and
//         		  cr_cost_repository.drilldown_key = cr_allocations.drilldown_key);
		
		posted_counter = 0
		select count(*) into :posted_counter from cr_cost_repository where 
   		month_number = :a_month_number and 
			month_period = :a_month_period and 
			drilldown_key in
				(select drilldown_key from cr_allocations
		        where month_number  = :a_month_number  and 
			           month_period  = :a_month_period  and 
						  nvl(interface_batch_id,'0') = '0'  and
	   		        allocation_id = :a_allocation_id)
			and (decode(batch_id,          '-173',NULL,batch_id)           is not null or 
              decode(cwip_charge_status,'-173',NULL,cwip_charge_status) is not null) 
			and rownum = 1;
		if isnull(posted_counter) then posted_counter = 0
		
		if posted_counter > 0 then
			f_pp_msgs("  ")
			f_pp_msgs("allocation_id " + string(a_allocation_id) + &
				" has already been posted to either the GL or Projects.~n" + &
				"The elimination will not be deleted.  Since this priority contains " + &
					"eliminations, the allocation program WILL TERMINATE.")
			f_pp_msgs("  ")
			rollback;  //  Rollback any prior "touching" of this allocation ...
			return -2
		end if
		
		delete from cr_cost_repository where 
   		month_number = :a_month_number and 
			month_period = :a_month_period and 
			drilldown_key in
				(select drilldown_key from cr_allocations
		        where month_number  = :a_month_number  and 
			           month_period  = :a_month_period  and 
						  nvl(interface_batch_id,'0') = '0'  and
	   		        allocation_id = :a_allocation_id);
	end if
else
	//  "TEST" allocations and inter-company transactions do not 
	//   update the cr_cost_repository ...
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_cost_repository: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  SECOND: delete records from the cr_allocations or cr_allocations_test table ... or the
//          appropriate inter-company table ...

//  Deleting any reversals that may exist ...

//  08/17/1999 - changed the delete statements below ... they all had
//               "select * from cr_allocations" ...
if g_debug = 'YES' then
	f_pp_msgs("Deleting reversals from detail table (Allocation Results) ...")
end if

if clearing_indicator = 4 then
	if a_run_as = "ACTUAL" then
		//  THE "EXISTS" IS SLOW ...
//		delete from cr_inter_company a where exists
//			(select * from cr_inter_company b
//	   	  where a.reversal_id = b.id and
//     			     month_number  = :month_number and 
//  	     			  month_period  = :month_period and 
//                  nvl(interface_batch_id,'0') = '0' and
//	  	      	  allocation_id = :allocation_id);
		delete from cr_inter_company a 
		 where a.reversal_id in
			(select b.id from cr_inter_company b
	   	  where month_number  = :a_month_number and 
  	     			  month_period  = :a_month_period and 
					  nvl(interface_batch_id,'0') = '0' and
	  	      	  allocation_id = :a_allocation_id);
	else
		//  THE "EXISTS" IS SLOW ...
//		delete from cr_inter_company_test a where exists
//			(select * from cr_inter_company_test b
//	   	  where a.reversal_id = b.id and
//     			     month_number  = :month_number and 
//  	     			  month_period  = :month_period and 
//                  nvl(interface_batch_id,'0') = '0' and
//	  	      	  allocation_id = :allocation_id);
		delete from cr_inter_company_test a 
		 where a.reversal_id in
			(select b.id from cr_inter_company_test b
	   	  where month_number  = :a_month_number and 
  	     			  month_period  = :a_month_period and 
					  nvl(interface_batch_id,'0') = '0' and
	  	      	  allocation_id = :a_allocation_id);
	end if
else
	if a_run_as = "ACTUAL" then
		//  THE "EXISTS" IS SLOW ...
//		delete from cr_allocations a where exists
//			(select * from cr_allocations b
//	   	  where a.reversal_id = b.id and
//     			     month_number  = :month_number and 
//  	     			  month_period  = :month_period and 
//                  nvl(interface_batch_id,'0') = '0' and
//	  	      	  allocation_id = :allocation_id);
		delete from cr_allocations a 
		 where a.reversal_id in
			(select b.id from cr_allocations b
	   	  where month_number  = :a_month_number and 
  	     			  month_period  = :a_month_period and 
					  nvl(interface_batch_id,'0') = '0' and
	  	      	  allocation_id = :a_allocation_id);
	else
		//  THE "EXISTS" IS SLOW ...
//		delete from cr_allocations_test a where exists
//			(select * from cr_allocations_test b
//	   	  where a.reversal_id = b.id and
//     			     month_number  = :month_number and 
//  	     			  month_period  = :month_period and 
//                  nvl(interface_batch_id,'0') = '0' and
//	  	      	  allocation_id = :allocation_id);
		delete from cr_allocations_test a 
		 where a.reversal_id in
			(select b.id from cr_allocations_test b
	   	  where month_number  = :a_month_number and 
  	     			  month_period  = :a_month_period and 
					  nvl(interface_batch_id,'0') = '0' and
	  	      	  allocation_id = :a_allocation_id);
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	if clearing_indicator = 4 then
		if a_run_as = "ACTUAL" then
			f_pp_msgs("ERROR: deleting reversals from cr_inter_company: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: deleting reversals from cr_inter_company_test: " + sqlca.SQLErrText)
		end if
	else
		if a_run_as = "ACTUAL" then
			f_pp_msgs("ERROR: deleting reversals from cr_allocations: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: deleting reversals from cr_allocations_test: " + sqlca.SQLErrText)
		end if
	end if
	f_pp_msgs("  ")
	rollback;
	return -1
end if


if g_debug = 'YES' then
	f_pp_msgs("Deleting results from detail table (Allocation Results) ...")
end if

if clearing_indicator = 4 then
	if a_run_as = "ACTUAL" then
		if g_debug = 'YES' then
			f_pp_msgs("Deleting cr_inter_company records ...")
		end if
		delete from cr_inter_company
		 where month_number = :a_month_number and 
	   	    month_period = :a_month_period and 
				 nvl(interface_batch_id,'0') = '0' and
   	   	 allocation_id = :a_allocation_id;
	else
		if g_debug = 'YES' then
			f_pp_msgs("Deleting cr_inter_company_test records ...")
		end if
		delete from cr_inter_company_test
		 where month_number = :a_month_number and 
	   	    month_period = :a_month_period and 
				 nvl(interface_batch_id,'0') = '0' and
   	   	 allocation_id = :a_allocation_id;		
	end if	
else
	if a_run_as = "ACTUAL" then
		if g_debug = 'YES' then
			f_pp_msgs("Deleting cr_allocations records ...")
		end if
		delete from cr_allocations
		 where month_number = :a_month_number and 
	   	    month_period = :a_month_period and 
				 nvl(interface_batch_id,'0') = '0' and
   	   	 allocation_id = :a_allocation_id;
	else
		if g_debug = 'YES' then
			f_pp_msgs("Deleting cr_allocations_test records ...")
		end if
		delete from cr_allocations_test
		 where month_number = :a_month_number and 
		       month_period = :a_month_period and 
				 nvl(interface_batch_id,'0') = '0' and
   		    allocation_id = :a_allocation_id;	
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	if clearing_indicator = 4 then
		if a_run_as = "ACTUAL" then
			f_pp_msgs("ERROR: deleting from cr_inter_company: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: deleting from cr_inter_company_test: " + sqlca.SQLErrText)
		end if
	else
		if a_run_as = "ACTUAL" then
			f_pp_msgs("ERROR: deleting from cr_allocations: " + sqlca.SQLErrText)
		else
			f_pp_msgs("ERROR: deleting from cr_allocations_test: " + sqlca.SQLErrText)
		end if
	end if
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  THIRD(a): delete the record from cr_alloc_process_control3 for this allocation type and
//            month/period combination ...
if g_debug = 'YES' then
	f_pp_msgs("Deleting from cr_alloc_process_control3 ...")
end if

delete from cr_alloc_process_control3
 where month_number          = :a_month_number  and 
       month_period          = :a_month_period  and 
       allocation_id         = :a_allocation_id and
		 upper(test_or_actual) = :a_run_as;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_alloc_process_control3: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  THIRD(b): delete the record from cr_alloc_process_control2 for this allocation type and
//            month/period combination ...
if g_debug = 'YES' then
	f_pp_msgs("Deleting from cr_alloc_process_control2 ...")
end if

delete from cr_alloc_process_control2
 where month_number          = :a_month_number  and 
       month_period          = :a_month_period  and 
       allocation_id         = :a_allocation_id and
		 upper(test_or_actual) = :a_run_as;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_alloc_process_control2: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  THIRD: delete the record from cr_alloc_process_control for this allocation type and
//         month/period combination ...
if g_debug = 'YES' then
	f_pp_msgs("Deleting from cr_alloc_process_control ...")
end if

delete from cr_alloc_process_control
 where month_number          = :a_month_number  and 
       month_period          = :a_month_period  and 
       allocation_id         = :a_allocation_id and
		 upper(test_or_actual) = :a_run_as;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_alloc_process_control: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  THERE IS A COMMIT HERE IN CB_DELETE, BUT OUR PRACTICE IN THE ALLOCATIONS IS
//  TO NOT FIRE A COMMIT.


if g_debug = 'YES' then
	f_pp_msgs("End of wf_auto_delete_elim")
end if

return 1
end function

public function longlong uf_unlock (longlong a_allocation_id);//  The allocation just ran.  A commit or rollback just fired depending on the condition.
//  It should be safe here to free up the running_session_id for this allocation if
//  suspense is set to yes.
update cr_allocation_control set running_session_id = NULL
 where allocation_id = :a_allocation_id;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating cr_allocation_control.running_session_id to NULL: " + &
		string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;  //  OK since a commit or rollback just fired.
end if

return 1
end function

private function longlong usf_retrieve_where_clause (longlong a_where_clause_id);// Lookup the where criteria based on a where clause id.  Will filter i_ds_where_clause to the appropriate rows
// and return the record count.
//	Rows are already retrieved up front.

i_ds_where_clause.SetFilter("where_clause_id = " + string(a_where_clause_id))
i_ds_where_clause.Filter()
i_ds_where_clause.Sort()

return i_ds_where_clause.RowCount()
end function

private function longlong usf_retrieve_group_by (longlong a_group_by_id);// Lookup the group by based on a group by id.  Will filter i_ds_group_by to the appropriate rows
// and return the record count.
//	Rows are already retrieved up front.

i_ds_group_by.SetFilter("group_by_id = " + string(a_group_by_id))
i_ds_group_by.Filter()
i_ds_group_by.Sort()

return i_ds_group_by.RowCount()
end function

private function longlong usf_retrieve_target (longlong a_target_id);// Lookup the target criteria based on a target id.  Will filter i_ds_target to the appropriate rows
// and return the record count.
//	Rows are already retrieved up front.

i_ds_target.SetFilter("target_id = " + string(a_target_id))
i_ds_target.Filter()
i_ds_target.Sort()

return i_ds_target.RowCount()
end function

private function longlong usf_retrieve_target (longlong a_target_id, string a_company);// Lookup the target criteria based on a target id and a company.  Will filter i_ds_target to the appropriate rows
// and return the record count.
//	Rows are already retrieved up front.

i_ds_target.SetFilter("target_id = " + string(a_target_id) + " and " + i_company_field + " = '" + a_company + "'")
i_ds_target.Filter()
i_ds_target.Sort()

if i_ds_target.rowcount() = 0 and a_company <> '*' then 
	// They don't have a record for each company.  Do they have an all companies record that would be usable
	return usf_retrieve_target(a_target_id, '*')
else
	return i_ds_target.RowCount()
end if
end function

private function string usf_build_where_clause_structure (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field);//******************************************************************************************
//
//  Window Function  :  wf_structure_value
//
//  Description      :  Builds the piece of the where clause for hierarchical queries
//								that use the cr_structures.
//
//******************************************************************************************
longlong comma_pos, start_pos, counter, the_last_comma_pos, string_length, i, status
string rollup_value_id, where_clause, description_edit_values[], rollup_value_id_list, &
		 element_description


where_clause         = " "
rollup_value_id_list = " ("


//  DMJ: 1/29/09: Using functions like substr() with structures would bomb because the column
//  alias would end up looking something like "substr(project,1,4)".  Look up the element
//  description instead and use that in the appropriate places.
setnull(element_description)
select description into :element_description 
  from cr_elements
 where element_id in (select element_id from cr_structures where structure_id = :a_structure_id);
if isnull(element_description) then element_description = "NONE"
element_description = f_cr_clean_string(trim(upper(element_description)))


//-------  Parse out the rollup values into an array, if the user selected more  -----------
//-------  than 1 rollup value.                                                  -----------
a_description_edit = f_replace_string(a_description_edit,'}{','}..next..{','all')
f_parsestringintostringarray(a_description_edit, "..next..", description_edit_values)


for i = 1 to upperbound(description_edit_values)
	
	//-------  First, must parse out the value_id from the chosen element_value  ------------
	start_pos = 1
	counter   = 0

	if i_use_new_structures_table = "YES" then
		//  These need to be harcoded to 0 in case the element_value contains a comma.
		comma_pos = 0
		the_last_comma_pos = 0
	else
		do while true
		
			counter++
		
			//  Endless loop protection ...
			if counter > 1000 then
				messagebox("!!!", "Endless loop in wf_structure_value.", Exclamation!)
				exit
			end if
		
			comma_pos = pos(description_edit_values[i], ",", start_pos)
		
			if comma_pos = 0 then exit
		
			the_last_comma_pos = comma_pos
			start_pos          = comma_pos + 1
		
		loop
	end if

	string_length = len(description_edit_values[i])
	
	if i_use_new_structures_table = "YES" then
		//  "{CWIP}" goes to "CWIP}"
		rollup_value_id = right(description_edit_values[i], (string_length - the_last_comma_pos) - 1)
	else
		//  "{CWIP, 15}" goes to " 15}"
		rollup_value_id = right(description_edit_values[i], string_length - the_last_comma_pos)
	end if

	rollup_value_id = trim(left(rollup_value_id, len(rollup_value_id) - 1))
	
	status = 0
	if i_use_new_structures_table = "YES" then
		select status into :status from cr_structure_values2
		 where structure_id = :a_structure_id and element_value = :rollup_value_id;
	else
		select status into :status from cr_structure_values
		 where structure_id = :a_structure_id and value_id = :rollup_value_id;
	end if
		
	if isnull(status) or status = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: the value_id (" + rollup_value_id + ") is INACTIVE or DOES NOT EXIST on " + &
			" structure_id (" + string(a_structure_id) + ")")
		f_pp_msgs("  ")
		return "ERROR"
	end if
	
	if i_use_new_structures_table = "YES" then
		rollup_value_id = "'" + rollup_value_id + "'"
	end if

	if i = upperbound(description_edit_values) then
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ") "
	else
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ","
	end if


	//-------  Build the piece of the where clause to be passed back to the  ---------------- 
	//-------  calling script                                                ----------------
	if i = upperbound(description_edit_values) then
		
		if a_not_field = 1 then
			//  The user clicked the "NOT" checkbox.
			where_clause = where_clause + " " + a_description + " not in ("
		else
			where_clause = where_clause + " " + a_description + " in ("
		end if
		
		if i = 1 then
			if i_use_new_structures_table = "YES" then
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value = " + rollup_value_id + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id = " + rollup_value_id + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		else
			if i_use_new_structures_table = "YES" then
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value in " + rollup_value_id_list + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id in " + rollup_value_id_list + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		end if
	end if

next

return where_clause
end function

public function decimal uf_clearing_balance (longlong a_cr_company_id, string a_charging_cost_center, ref decimal a_amount, ref decimal a_quantity);//******************************************************************************************
//
//  User Object Function:  uf_clearing_balance
//
//	 Description         :  Retrieves the sum(amount) and sum(quantity) based on cr_allocation_control.balance_where_clause_id
//
//  Notes               :  This is hardcoded to return only 1 balance (pot).
//							It retrieves it for all months in the cost_repository, so any
//							remaining balance that is inadvertently left in the pot will
//							clear.
//
//******************************************************************************************
longlong num_rows
string balance_sqls, hint_syntax, rtns, stub

// Build the where clause -- from their selections
// 0 for MP argument -- MP concept doesn't apply to clearings.
balance_sqls = usf_build_where_clause(i_balance_where_clause_id, i_clearing_balance_start_month, i_clearing_balance_end_month, 0, a_cr_company_id, i_balance_amount_types, a_charging_cost_center)
i_balance_where_clause = balance_sqls
if balance_sqls = 'ERROR' then return -1

// Build the full SQL:
balance_sqls = "select sum(amount) amount, sum(quantity) quantity from " + i_balance_table_name + " " + balance_sqls

// ### 10906: JAK: 2012-09-11:  Move f_sql_add_hint lower so it can be applied to latter select statements
// ### - SEK - 061311 - 7742: Look up and add any hints using f_sql_add_hint
hint_syntax = 'cr_alloc_balance_' + string(i_allocation_id)
rtns = f_sql_add_hint(balance_sqls, hint_syntax)

if isnull(rtns) or rtns = "" then
	f_pp_msgs("  ")
	f_pp_msgs("WARNING: Adding hint '" + hint_syntax + "'")
	f_pp_msgs(" THE ALLOCATION WILL CONTINUE TO RUN ")
	f_pp_msgs("  ")
else 
	balance_sqls = rtns
end if

//  Save the balance SQL for reporting ...  Group by returned into stub variable as it will never actually exist.
uf_save_report_sql(balance_sqls, false, i_balance_sqls, stub)

i_ds_sum_amount.Reset()
i_ds_sum_amount.SetSQLSelect(balance_sqls)
num_rows = i_ds_sum_amount.retrieve()

if g_debug = 'YES' then f_pp_msgs("")
if g_debug = 'YES' then f_pp_msgs("uf_clearing_balance: " + a_charging_cost_center + ": balance_sqls = " + balance_sqls)
if g_debug = 'YES' then f_pp_msgs("")
	
if num_rows < 0 then
	f_pp_msgs("ERROR in uf_clearing_balance: " + i_ds_sum_amount.i_sqlca_sqlerrtext)
	return -1
elseif num_rows = 0 then
	a_amount = 0
	a_quantity = 0
else
	a_amount = i_ds_sum_amount.GetItemNumber(1, 1)
	a_quantity = i_ds_sum_amount.GetItemNumber(1, 2)
	
	if isnull(a_amount) then a_amount = 0
	if isnull(a_quantitY) then a_quantity = 0
end if

if i_allocate_quantity <> 1 then a_quantity = 0	// don't allocate quantities

return 0
end function

public subroutine usf_get_variables ();string result_amount_type_string

usf_reset_variables()

if i_ds_alloc_list_row < 0 or i_ds_alloc_list_row > i_ds_alloc_list.rowcount() then return

i_allocation_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'allocation_id')
i_cr_company_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'cr_company_id')
i_description =  i_ds_alloc_list.GetItemString(i_ds_alloc_list_row,'description')
i_where_clause_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'where_clause_id')
i_group_by_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'group_by_id')
i_target_id = i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'target_id')
i_credit_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'credit_id')
i_clearing_indicator =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'clearing_indicator')
i_dollar_quantity_indicator =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'dollar_quantity_indicator')
i_balance_where_clause_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'balance_where_clause_id')
i_clearing_factor_start_month =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'clearing_factor_start_month')
i_clearing_factor_end_month =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'clearing_factor_end_month')
i_clearing_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'clearing_id')
i_intercompany_accounting =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'intercompany_accounting')
i_intercompany_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'intercompany_id')
i_dw_id =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'dw_id')
i_clearing_balance_start_month =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'clearing_balance_start_month')
i_clearing_balance_end_month =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'clearing_balance_end_month')
i_gl_journal_category =  i_ds_alloc_list.GetItemString(i_ds_alloc_list_row,'gl_journal_category')
i_ytd_indicator =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'ytd_indicator')
i_elim_flag =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'elim_flag')
i_run_by_department =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'run_by_department')
i_auto_reverse =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'auto_reverse')
i_base_limit =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'base_limit')
i_base_limit_detail =  i_ds_alloc_list.GetItemString(i_ds_alloc_list_row,'base_limit_detail')
i_balance_amount_types =  i_ds_alloc_list.GetItemString(i_ds_alloc_list_row,'balance_amount_types')
i_source_amount_types =  i_ds_alloc_list.GetItemString(i_ds_alloc_list_row,'source_amount_types')
result_amount_type_string =  i_ds_alloc_list.GetItemString(i_ds_alloc_list_row,'result_amount_type')
i_derivation_flag =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'derivation_flag')
i_allocate_quantity =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'allocate_quantity')
i_apply_wo_types =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'apply_wo_types')
i_exclude_negatives = i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'exclude_negatives')
i_absolute_value_calcs =  i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'absolute_value_calcs')
i_rate_id =   i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'rate_id')
i_incremental_allocation = i_ds_alloc_list.GetItemNumber(i_ds_alloc_list_row,'incremental_allocation')

if isnull(i_cr_company_id)   then i_cr_company_id = 0
if isnull(i_clearing_id)     then i_clearing_id   = 0
if isnull(i_intercompany_accounting) then i_intercompany_accounting = 0
if isnull(i_intercompany_id)      then i_intercompany_id    = 0
if isnull(i_dw_id)           then i_dw_id         = 0
if isnull(i_apply_wo_types) then i_apply_wo_types = 0
if isnull(i_dollar_quantity_indicator) then i_dollar_quantity_indicator = 0
if isnull(i_ytd_indicator)           then i_ytd_indicator           = 0
if isnull(i_elim_flag)               then i_elim_flag               = 0
if isnull(i_run_by_department) then i_run_by_department               = 0
if isnull(i_auto_reverse)              then i_auto_reverse              = 0
if isnull(i_derivation_flag)         then i_derivation_flag         = 0
if isnull(i_source_amount_types)  or trim(i_source_amount_types)  = "" then i_source_amount_types  = "NULL-SOURCE"
if isnull(i_balance_amount_types) or trim(i_balance_amount_types) = "" then i_balance_amount_types = "NULL-BALANCE"
if isnull(i_allocate_quantity)       then i_allocate_quantity       = 0
if isnull(i_exclude_negatives)       then i_exclude_negatives       = 0
if isnull(i_absolute_value_calcs) then i_absolute_value_calcs = 0	    // ### 8474: JAK: 20100330:  Absolute Value Calculation flag.
if isnull(i_rate_id) then i_rate_id = 0
if isnull(i_incremental_allocation) then i_incremental_allocation = 0
//  DO NOT set null i_base_limit to 0 !  Nulls mean something.


if isnull(i_gl_journal_category) or (trim(i_gl_journal_category) = '' and i_gl_journal_category <> ' ') then
	// GL Journal category not provided.  Default it.
	choose case i_clearing_indicator
		case 1, 2, 3, 6
			i_gl_journal_category = i_default_gljc_alloc
		case 4
			i_gl_journal_category = i_default_gljc_interco
	end choose
end if

//  Get the table name.
if i_clearing_indicator = 4  then   //  Inter-Company ...
	i_alloc_dtl_table = 'CR_INTER_COMPANY'
else  //  Normal alloc ...
	i_alloc_dtl_table = 'CR_ALLOCATIONS'
end if
if i_run_as_a_test then i_alloc_dtl_table += '_TEST'	// test mode.  Write to test.

i_alloc_stg_table = i_alloc_dtl_table + '_STG'   // Always write to stg first.

// Amount Types
//  RUN AS COMMITMENTS: the amount type is always 4 (commitments).  Duh.
//  The value in the global variable is of an odd form so a client cannot accidentally
//  run the allocations in this state unless they really intend to (i.e. I decided to 
//  not just have "Yes" identify "run as commitments".
if upper(i_run_as_commitments) = "RUN_AS_COMMITMENTS" then i_result_amount_type = 4

i_result_amount_type = long(result_amount_type_string)
select decode(count(*),0,1,:i_result_amount_type)
	into :i_result_amount_type
	from cr_amount_type
	where amount_type_id = :i_result_amount_type;
if isnull(i_result_amount_type) then i_result_amount_type = 1


// Apply WO Types -- clearing indicator = 6 = loading + wo types
if i_clearing_indicator = 6 then 
	i_apply_wo_types = 1
	i_clearing_indicator = 1
end if

// base limits only apply to clearing_indicator = 3
if i_clearing_indicator <> 3 then setnull(i_base_limit)
if i_clearing_indicator <> 3 then setnull(i_base_limit_detail)

// tables:
i_source_table_name  = usf_retrieve_where_clause_source(i_where_clause_id)
if i_clearing_indicator = 3 then
	i_balance_table_name = usf_retrieve_where_clause_source(i_balance_where_clause_id)
else
	i_balance_table_name = ''
end if

// Eliminations don't support auto-reversals.
if i_elim_flag = 1 then i_auto_reverse = 0

// Rate_IDs don't apply to Clearing Indicator in (3,4)
if i_clearing_indicator = 3 or i_clearing_indicator = 4 then
	i_rate_id = 0 
end if

// MWA 20141103 Incremental dev
if i_incremental_allocation = 1 then
	
	// Current incremental_run_id
	select  CR_Alloc_Incremental_seq.nextval into :i_curr_incr_run_id
	from dual;
	
	// Current incremental month_run_id
	select nvl(max(month_run_id), 0) into :i_curr_month_run_id 
	from cr_alloc_incremental_control
		where allocation_id = :i_allocation_id
			and run_month_number = :i_month_number;
			
	i_curr_month_run_id++	
	
	// Source Value for Incremental Allocations
	if upper(i_source_table_name) = "CR_COST_REPOSITORY" then
		i_source_value_field = "id"
	else
		i_source_value_field = "drilldown_key"
	end if
	
end if 
end subroutine

public subroutine usf_reset_variables ();setnull(i_allocation_id)
setnull(i_cr_company_id)
setnull(i_description)
setnull(i_where_clause_id)
setnull(i_group_by_id)
setnull(i_target_id)
setnull(i_credit_id)
setnull(i_clearing_indicator)
setnull(i_dollar_quantity_indicator)
setnull(i_balance_where_clause_id)
setnull(i_clearing_factor_start_month)
setnull(i_clearing_factor_end_month)
setnull(i_clearing_id)
setnull(i_intercompany_accounting)
setnull(i_intercompany_id)
setnull(i_dw_id)
setnull(i_clearing_balance_start_month)
setnull(i_clearing_balance_end_month)
setnull(i_gl_journal_category)
setnull(i_ytd_indicator)
setnull(i_elim_flag)
setnull(i_run_by_department)
setnull(i_auto_reverse)
setnull(i_base_limit)
setnull(i_base_limit_detail)
setnull(i_balance_amount_types)
setnull(i_source_amount_types)
setnull(i_result_amount_type)
setnull(i_derivation_flag)
setnull(i_allocate_quantity)
setnull(i_apply_wo_types)
setnull(i_absolute_value_calcs)
end subroutine

private function longlong usf_retrieve_credit (longlong a_credit_id);// Lookup the credit criteria based on a credit id.  Will filter i_ds_credit to the appropriate rows
// and return the record count.
//	Rows are already retrieved up front.

i_ds_credit.SetFilter("credit_id = " + string(a_credit_id))
i_ds_credit.Filter()
i_ds_credit.Sort()

return i_ds_credit.RowCount()
end function

private function longlong usf_retrieve_credit (longlong a_credit_id, string a_company);// Lookup the credit criteria based on a credit id and a company.  Will filter i_ds_credit to the appropriate rows
// and return the record count.
//	Rows are already retrieved up front.

i_ds_credit.SetFilter("credit_id = " + string(a_credit_id) + " and " + i_company_field + " = '" + a_company + "'")
i_ds_credit.Filter()
i_ds_credit.Sort()

if i_ds_credit.rowcount() = 0 and a_company <> '*' then 
	// They don't have a record for each company.  Do they have an all companies record that would be usable
	return usf_retrieve_credit(a_credit_id, '*')
else
	return i_ds_credit.RowCount()
end if
end function

public function string usf_get_external_company (longlong a_company_id);// Return the external company id for a cr_company_id
longlong row

if isnull(a_company_id) then return '<None>'

row = i_ds_company.rowcount()
row = i_ds_company.Find('cr_company_id = ' + string(a_company_id ),1,row)

if row <= 0 then return 'ERROR'

return i_ds_company.GetItemString(row,'external_company_id')

end function

public function longlong usf_retrieve_company (longlong a_company_id);if a_company_id = 10000 then
	//  Run the allocation for all companies ... this gives them the ability to 
	//  set up and allocation once that would run for all companies, but keep the
	//  pots and calculated rates whole by company.
	i_run_for_all_companies = true
	i_ds_company.SetFilter('owned = 1 and cr_company_id <> 10000')
elseif isnull(a_company_id) or a_company_id = 0 then
	i_run_for_all_companies = false
	i_ds_company.SetFilter('isnull(cr_company_id)')
else	
	//  Run the allocation for the company the user specified ... or if <none>,
	//  the allocation gets the pot and calculated clearing rates for all companies,
	//  but does not keep them whole by company.
	i_run_for_all_companies = false
	i_ds_company.SetFilter('cr_company_id = ' + string(a_company_id))
end if

i_ds_company.Filter()
i_ds_company.SetSort('external_company_id a')
i_ds_company.Sort()

return i_ds_company.RowCount()
end function

private function string usf_retrieve_where_clause_source (longlong a_where_clause_id);//// Lookup the table named based on a where clause id.  Will do this lookup and store the results in a cached datastore (i_ds_where_clause_sources)
longlong row
row = i_ds_where_clause_sources.rowcount()
row = i_ds_where_clause_sources.Find("where_clause_id=" + string(a_where_clause_id),1,row)

if row <= 0 then 
	f_pp_msgs("ERROR: Could not find the table name for where_clause_id " + string(a_where_clause_id))
	return 'ERROR'
else
	return f_cr_clean_string(trim(upper(i_ds_where_clause_sources.GetItemString(row,'table_name'))))
end if
end function

public function string usf_build_where_clause_ds (longlong a_where_clause_id);// Builds a where clause out of the current rows in i_ds_where_clause
string where_clause
string left_paren, col_name, operator, value1, between_and, value2, right_paren, and_or, sql_where, rc
longlong num_rows, i, structure_id, not_field
boolean skip_structure


// Retrieve the rows
num_rows = usf_retrieve_where_clause(a_where_clause_id)

// Plan to make this function generic in the future and structure_id may not exists
rc = i_ds_where_clause.Describe('structure_id.x')
if rc = '!' or rc = '?' then 
	skip_structure = true
else
	skip_structure = false
end if

for i = 1 to num_rows
	left_paren  = i_ds_where_clause.GetItemString(i, "left_paren")
	col_name = i_ds_where_clause.GetItemString(i, "column_name")
	operator = i_ds_where_clause.GetItemString(i, "operator")
	value1 = i_ds_where_clause.GetItemString(i, "value1")	
	between_and = i_ds_where_clause.GetItemString(i, "between_and")
	value2 = i_ds_where_clause.GetItemString(i, "value2")
	right_paren = i_ds_where_clause.GetItemString(i, "right_paren")
	and_or = i_ds_where_clause.GetItemString(i, "and_or")
	if not skip_structure then
		structure_id = i_ds_where_clause.GetItemNumber(i, "structure_id")
	end if
	
	operator = upper(trim(operator))
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
		
	if pos(col_name, "(") = 0 and operator <> '<NONE>' then
		//  looks just like a column name.  Clean it and put it in quotes.
		col_name = '"' + f_cr_clean_string(upper(trim(col_name))) + '"'
	else
		//  If they manually entered an Oracle function, take it as-is.
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "<>", "NOT IN", "NOT LIKE"
				//  NOT EQUAL TO the structure value ...
				not_field = 1
			case else
				not_field = 0
		end choose
		col_name = usf_build_where_clause_structure(structure_id, col_name, value1, not_field)
		if col_name = 'ERROR' then return 'ERROR'
		operator = '<NONE>'
	end if
	
	choose case operator
		// ******************************************************
		case '=','<>','>','>=','<','<='
		// ******************************************************
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// functional -- don't use quotes
			elseif pos(value1, "||") > 0 then
				// functional -- don't use quotes
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case 'BETWEEN','NOT BETWEEN'
		// ******************************************************
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			elseif pos(value1, "(") > 0 then
				// functional -- don't use quotes
			elseif pos(value1, "||") > 0 then
				// functional -- don't use quotes
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
				
			if mid(trim(value2),1,1) = "'" then
				// have quotes
			elseif pos(value2, "(") > 0 then
				// functional -- don't use quotes
			else
				// Need to add quotes
				value2 = "'" + value2 + "'"
			end if
				
		// ******************************************************
		case 'IN','NOT IN'
		// ******************************************************
			// did they build the parens for me?
			if mid(trim(value1),1,1) = '(' then
				// great, they did it for me
			else
				// I need to add them.
				value1 = "(" + trim(value1) + ")"
			end if
						
			// how about quotes?
			if mid(trim(mid(trim(value1),2,100)),1,1) = "'" then
				// Good syntax
			elseif upper(mid(trim(mid(trim(value1),2,100)),1,6)) = "SELECT" then
				// Scalar - Good syntax
			elseif pos(mid(trim(value1),2,100), "(") > 0 then
				// functional -- don't use quotes
			elseif pos(mid(trim(value1),2,100), "||") > 0 then
				// functional -- don't use quotes
			else
				// Need to add quotes
				value1 = trim(value1)
				value1 = f_replace_string(value1,",","','","all")
				value1 = "('" + mid(value1,2,len(value1)-2) + "')"
			end if
			
			between_and = ''
			value2 = ''
		// ******************************************************
		case 'LIKE','NOT LIKE'
		// ******************************************************
			
			// did they build the quotes for me?
			if mid(trim(value1),1,1) = "'" then
				// have quotes
			else
				// Need to add quotes
				value1 = "'" + value1 + "'"
			end if
				
			// Did they build the wildcards for me?
			if pos(value1,'%') > 0 then
				// yes
			else
				// no, double side wildcard
				value1 = "'%" + mid(value1,2,len(value1)-2) + "%'"
			end if
			
			between_and = ''
			value2 = ''
			
		// ******************************************************
		case '<NONE>'
		// ******************************************************
			// Just take col_name exactly as it.  Allows for exists, not exists, etc.
			// also used for structure points.
			operator = ''
			value1 = ''
			between_and = ''
			value2 = ''
	end choose
	
	if i = num_rows then and_or = ''
	
	sql_where += trim(left_paren + " ") + col_name + " " + operator + " " + value1 + " " +  trim(between_and + " " + value2) + trim(" " + right_paren) + " " + and_or + " "
next

return sql_where
end function

public function longlong usf_get_source_id (string a_table_name);// Return the external company id for a cr_company_id
longlong row

a_table_name = f_cr_clean_string(upper(trim(a_table_name)))
if right(a_table_name,4) = '_STG' then a_table_name = mid(a_table_name,1,len(a_table_name) - 4)

row = i_ds_cr_sources.rowcount()
row = i_ds_cr_sources.Find("table_name = '" + a_table_name + "'",1,row)

if row <= 0 then return -1

return i_ds_cr_sources.GetItemNumber(row,'source_id')

end function

public function string usf_get_source_gljc (string a_table_name);// Return the external company id for a cr_company_id
longlong row

a_table_name = f_cr_clean_string(upper(trim(a_table_name)))
if right(a_table_name,4) = '_STG' then a_table_name = mid(a_table_name,1,len(a_table_name) - 4)

row = i_ds_cr_sources.rowcount()
row = i_ds_cr_sources.Find("table_name = '" + a_table_name + "'",1,row)

if row <= 0 then return ''

return i_ds_cr_sources.GetItemString(row,'gl_journal_category')

end function

private function string usf_get_target_trpo_col (longlong a_target_id, longlong a_row_id, string a_col_name);// Lookup the transposed column based on a target id, row id, and column name.  Will use a find row technique.
//	Rows are already retrieved up front.
longlong find_row
string trpo_col_name

find_row = i_ds_target_trpo.Find("target_id = " + string(a_target_id) + " and row_id = " + string(a_row_id), 1, i_ds_target_trpo.rowcount())
if find_row <= 0 then
	return 'ERROR'
end if
trpo_col_name = i_ds_target_trpo.GetItemString(find_row, a_col_name)
trpo_col_name = f_cr_clean_string(upper(trim(trpo_col_name)))

return trpo_col_name
end function

private function string usf_get_credit_trpo_col (longlong a_credit_id, longlong a_row_id, string a_col_name);// Lookup the transposed column based on a credit id, row id, and column name.  Will use a find row technique.
//	Rows are already retrieved up front.
longlong find_row
string trpo_col_name

find_row = i_ds_credit_trpo.Find("credit_id = " + string(a_credit_id) + " and row_id = " + string(a_row_id), 1, i_ds_credit_trpo.rowcount())
if find_row <= 0 then
	return 'ERROR'
end if
trpo_col_name = i_ds_credit_trpo.GetItemString(find_row, a_col_name)
trpo_col_name = f_cr_clean_string(upper(trim(trpo_col_name)))

return trpo_col_name
end function

private function longlong usf_retrieve_interco_criteria (longlong a_intercompany_id);// Lookup the credit criteria based on a credit id.  Will filter i_ds_credit to the appropriate rows
// and return the record count.
//	Rows are already retrieved up front.

i_ds_interco.SetFilter("intercompany_id = " + string(a_intercompany_id))
i_ds_interco.Filter()
i_ds_interco.Sort()

return i_ds_interco.RowCount()
end function

public function decimal usf_lookup_rate (longlong a_rate_id, string a_company_value, longlong a_month_number);//******************************************************************************************
//
//  User Object Function  :  usf_lookup_rate
//
//  Description  :  Looks up predefined rates from the cr_rates_input table.
//
//                  The user may define the rates in one of 3 ways:
//                    1.  By company and month
//                    2.  By company for ALL months (e.g. billings by customers)
//                    3.  For ALL companies by month (e.g. payroll accruals)
//
//                  Lookup the rates in this order ...
//
//******************************************************************************************
decimal {10} rate
string      mn_string
longlong rows, find_row

//  The cr_rates_input.month_number field is a VarChar(35) to support the
//  entry of "ALL" ...
mn_string = string(a_month_number)

//  This MUST BE set to NULL.  A null value after selects will mean "no rate found"
setnull(rate)

rows = i_ds_rates_input.rowcount()

//  1.  Company and Month:
find_row = i_ds_rates_input.Find("rate_id = " + string(a_rate_id) + " and company_field = '" + a_company_value + "' and month_number = '" + mn_string + "'",1,rows)

//  2.  Company for ALL Months:
if find_row = 0 then find_row = i_ds_rates_input.Find("rate_id = " + string(a_rate_id) + " and company_field = '" + a_company_value + "' and month_number = 'ALL'",1,rows)

//  3.  ALL Companies by Month:
if find_row = 0 then find_row = i_ds_rates_input.Find("rate_id = " + string(a_rate_id) + " and company_field = 'ALL' and month_number = '" + mn_string + "'",1,rows)

//  4.  ALL Companies for ALL Months:
if find_row = 0 then find_row = i_ds_rates_input.Find("rate_id = " + string(a_rate_id) + " and company_field = 'ALL' and month_number = 'ALL'",1,rows)

//  If we get here, we found no rates ... return a NULL.  There is code in
//  uf_loading1 that is expecting a NULL ...
if find_row <= 0 then
	return rate
else
	rate = i_ds_rates_input.GetItemNumber(find_row,'rate')
	return rate
end if
end function

public function longlong usf_rates_by_structure (longlong a_rate_id, ref longlong a_structure_id, ref string a_element);longlong rows, find_row, element_id

setnull(a_structure_id)
setnull(a_element)

rows = i_ds_rates.rowcount()

find_row = i_ds_rates.Find('rate_id = ' + string(a_rate_id),1,rows)

if find_row > 0 then
	a_structure_id = i_ds_rates.GetItemNumber(find_row,'structure_id')
	element_id = i_ds_rates.GetItemNumber(find_row,'element_id')
	
	find_row = i_ds_elements.Find('element_id = ' + string(element_id), 1, i_num_elements)
	if find_row > 0 then
		a_element = i_ds_elements.GetItemString(find_row,'description')
	end if
	
	if trim(a_element) = '' then setnull(a_element)
end if

return 0
end function

public function decimal usf_lookup_rate_by_structure (longlong a_structure_id, string a_v_for_rates);decimal {10} rate
longlong len

setnull(rate)

// ### 6636: JAK: 2011-02-03: Structure based Rates for cr_structure_values
if i_use_new_structures_table = 'NO' then
	select rate into :rate
	from 
	(select rate, level
	from cr_structure_values
	where structure_id = :a_structure_id and status = 1 and rate is not null
	start with structure_id = :a_structure_id
		and (element_value = :a_v_for_rates or substr(element_value,1,:len + 1) = :a_v_for_rates || ':')
	connect by prior rollup_value_id = value_id and structure_id = :a_structure_id
	order by level asc) where rownum = 1;
else
	select rate into :rate
	from 
	(select rate, level
	from cr_structure_values2
	where structure_id = :a_structure_id and status = 1 and rate is not null
	start with structure_id = :a_structure_id
		and (element_value = :a_v_for_rates or substr(element_value,1,:len + 1) = :a_v_for_rates || ':')
	connect by prior parent_value = element_value and structure_id = :a_structure_id
	order by level asc) where rownum = 1;
end if

if isnull(rate) then rate = 0

return rate
end function

public function longlong usf_check_kickouts (string a_table_name);// Checks for the number of validation kickouts.
string sqls
longlong rtn

sqls = "select count(*) from cr_validations_invalid_ids where lower(table_name) = '" + lower(trim(a_table_name)) + "' " + &
	"and id in (select id from " + a_table_name + " where interface_batch_id = '" + string(i_session_id) + "')"
	
rtn = i_ds_count.SetSQLSelect(sqls)	
rtn = i_ds_count.Retrieve()

if rtn > 0 then
	return i_ds_count.GetItemNumber(1,1)
else
	return -1
end if
end function

public subroutine uf_save_report_sql (string a_allocation_sqls, boolean a_custom_dw, ref string a_where_clause, ref string a_group_clause);//******************************************************************************************
//
//  User Object Function  :  uf_save_report_sql
//
//  Description           :  Parse out there where clause, and group by clause to be saved.
//******************************************************************************************
longlong pos
string where_clause

if a_custom_dw then
	setnull(a_where_clause)
	setnull(a_group_clause)
else
	a_where_clause = a_allocation_sqls
	pos = pos(a_where_clause, "where")
	if pos > 0 then
		a_where_clause = right(a_where_clause, len(a_where_clause) - (pos - 1))
		
		pos = pos(a_where_clause, "group by")
		if pos > 0 then
			a_where_clause = left(a_where_clause, pos - 1)
		end if
	else
		a_where_clause = ''
	end if
	
	
	a_group_clause = a_allocation_sqls
	pos = pos(a_group_clause, "group by")
	if pos > 0 then
		a_group_clause = right(a_group_clause, len(a_group_clause) - (pos - 1))
	else
		a_group_clause = ''
	end if
end if

// The SQL may have a company restriction in it that needs to be removed.
if i_run_for_all_companies then
	if isnull(i_cr_company_id) or i_cr_company_id = 0 then
		// no criteria to remove
	else
		where_clause = '(' + i_company_field + " = '" + upper(trim(i_external_company_id)) + "') and "
		a_where_clause = f_replace_string(a_where_clause,where_clause,'','first')
	end if
end if
end subroutine

public function string usf_build_where_clause (longlong a_where_clause_id, longlong a_start_month, longlong a_end_month, longlong a_month_period, longlong a_cr_company_id, string a_amount_types, string a_dept_value);string where_clause, incr_clause

// Check their month inputs
if isnull(a_start_month) or a_start_month = 0 then 
	// current month
	a_start_month = i_month_number
elseif a_start_month = -1 then
	// ### 10519: JAK: 2012-08-10: Prior month option.  This option WILL cross years.
	// prior month
	a_start_month = i_month_number - 1
	if mid(string(a_start_month),5,2) = '00' then a_start_month = a_start_month - 88
end if

if isnull(a_end_month) or a_end_month = 0 then 
	// current month
	a_end_month = i_month_number
elseif a_end_month = -1 then
	// ### 10519: JAK: 2012-08-10: Prior month option.  This option WILL cross years.
	// prior month
	a_end_month = i_month_number - 1
	if mid(string(a_end_month),5,2) = '00' then a_end_month = a_end_month - 88
end if

//  The ytd_indicator overrides any selected months.
if i_ytd_indicator = 1 then
	//  ### 29232 JAK: 2013-02-05:  Code to support non-calendar fiscal years
	if long(mid(string(i_month_number),5,2)) >= i_init_mo_fy then
		a_start_month = truncate(i_month_number / 100,0) * 100 + i_init_mo_fy
	else
		a_start_month = (truncate(i_month_number / 100,0) - 1) * 100 + i_init_mo_fy
	end if
	a_end_month   = i_month_number
	a_month_period = 0
end if

if a_start_month <= 0 or isnull(a_start_month) or a_end_month <= 0 or isnull(a_end_month) then
	f_pp_msgs("ERROR - Invalid month selections")
	f_pp_msgs("Start Month = " + string(a_start_month))
	f_pp_msgs("End Month = " + string(a_end_month))
	return 'ERROR'
end if
	
// Build the where clause -- from their selections
where_clause = usf_build_where_clause_ds(a_where_clause_id)
if where_clause = 'ERROR' then return 'ERROR'

if trim(where_clause) = '' or isnull(where_clause) then
	where_clause = ' where '
else
	where_clause = ' where (' + where_clause + ') and '
end if

// company criteria
if isnull(a_cr_company_id) or a_cr_company_id = 0 then
	// no additional criteria
else
	where_clause += '(' + i_company_field + " = '" + upper(trim(i_external_company_id)) + "') and "
end if

// month criteria
if a_month_period = 0 then
	if a_start_month = a_end_month then
		where_clause += '(month_number = ' + string(a_start_month) + ') and '
	else
		where_clause += '(month_number between ' + string(a_start_month) + ' and ' + string(a_end_month) + ') and '
	end if
else
	where_clause += '(month_number = ' + string(a_start_month) + ' and month_period = ' + string(a_month_period) + ') and '
end if

// amount type criteria.
if trim(a_amount_types) = '' or isnull(a_amount_types) or a_amount_types = "NULL-BALANCE" then
	// no additional criteria
	// Historically, the amount type for balance had no criteria.  Use it all for consistency
elseif trim(a_amount_types) = '' or isnull(a_amount_types) or a_amount_types = "NULL-SOURCE" then
	where_clause += "(amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") and "
else
	where_clause +=  "(amount_type in (" + a_amount_types + ")) and "
end if

// department values
if (a_dept_value <> ' ' and trim(a_dept_value) = '') or isnull(a_dept_value) then
	// no additional criteria
else
	where_clause += '("' + i_department_field + '"' + " = '" + a_dept_value + "' ) and "
end if

// Incremental range lookup
if i_incremental_allocation = 1 then
	
	incr_clause = usf_build_where_clause_incr_range( a_start_month, a_end_month, i_allocation_id, a_cr_company_id )
	
	// MWA 20141105 this isn't the cleanest way to get this usf to return 'ERROR' if the incremental code breaks but it will work
	if incr_clause = 'ERROR' then 
		return 'ERROR'
	else 
		i_incr_month_source_sql = where_clause + i_incr_month_source_sql
		where_clause += incr_clause + " and "
	end if
	
end if

// Remove the training " and "
where_clause = mid(where_clause,1,len(where_clause) - 5)

return where_clause

end function

public function string uf_sources_and_targets (longlong a_group_by_id, string a_descr, longlong a_month_number, longlong a_month_period, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, longlong a_cr_company_id, string a_company_field, boolean a_custom_dw);//******************************************************************************************
//
//  Called from uf_loading2 ... Replaces the code in uf_loading1 from the 
//  SOURCES AND TARGETS section.
//
//******************************************************************************************
longlong num_rows
string allocation_sqls, allocation_where_sqls, select_fields, group_fields, field, group_field, hint_syntax, rtns
longlong i, mask_length, field_width, field_start, start_month, end_month

//********************************
//
//  Sources and Targets:  
//
//********************************
// Retrieve the group by.
num_rows = usf_retrieve_group_by(a_group_by_id)

allocation_sqls = "select "
select_fields = ''
group_fields = ''

for i = 1 to num_rows

	//  The following column reference was changed from "cr_elements_description" to 2, due
	//  to dynamic naming problems when using PB7 with Oracle 8 DB and OR 7 drivers at SO.
	field = upper(trim(i_ds_group_by.GetItemString(i, 3)))
	field = f_cr_clean_string(field)
	group_field = field
	mask_length = i_ds_group_by.GetItemNumber(i, 4)
	field_width = i_ds_group_by.GetItemNumber(i, 5)
	
	if mask_length < 0 then
		mask_length = abs(mask_length)
		field_start = field_width - mask_length + 1
		field       = "'" + fill("0", field_width - mask_length) + "'" + '||substr("' + field + '",' + string(field_start) + "," + string(mask_length) + ') as "' + field + '"'
		group_field = "'" + fill("0", field_width - mask_length) + "'" + '||substr("' + group_field + '",' + string(field_start) + "," + string(mask_length) + ')'
	elseif mask_length > 0 then
		field       = 'substr("' + field + '",1,' + string(mask_length) + ")||'" + fill("0", field_width - mask_length) + "'" + ' as "' + field + '"'
		group_field = 'substr("' + group_field + '",1,' + string(mask_length) + ")||'" + fill("0", field_width - mask_length) + "'"
	end if
	
	select_fields += field + ', '
	group_fields += group_field + ', '
next


// Always include: month_number, month_period, amount, and quantity.
allocation_sqls += select_fields + string(i_month_number) + ' month_number, ' + string(i_month_period) + ' month_period, '

// Add the months to the group by.  In the event that there is no other group, without a group clause
// a record will be returned with null amounts.  With the group, no row is returned which is easier to 
// handle later.
// EG.  	select sum(amount) from cr_cost_repository where 1=2 returns 1 row with a null value
//			select sum(amount) from cr_cost_repository where 1=2 group by 1 returns 0 rows
group_fields += string(i_month_number) + ', ' + string(i_month_period) + ' '

if i_clearing_indicator = 3 and not isnull(i_base_limit) then
	// Base limits.
	//  I.E.  Either the total amount by work order, or, the base limit if that is < the total amount,
	//  ratioed back to the other levels of detail based on their original ratio.
	//  NULLIF is to get around any div-by-zero errors where the parts add to zero by wo, company ... nvl'ing
	//  to 1 then makes the base = to the sum(amount) since the ratio is 1 over 1.
	// ### 8942: JAK 2010-06-22:  Updated base limit logic to take into account a negative limit as well as a positive limit
	allocation_sqls += &
		"round(" + &
			"case when sum(sum(amount)) over(partition by " + i_base_limit_detail + " order by " + i_base_limit_detail + ") > " + string(i_base_limit) + " then " + string(i_base_limit) + "  " + &
			"when sum(sum(amount)) over(partition by " + i_base_limit_detail + " order by " + i_base_limit_detail + ") < " + string(-1 * i_base_limit) + "  then " + string(-1 * i_base_limit) + "  " + &
			"when nvl(sum(sum(amount)) over(partition by " + i_base_limit_detail + " order by " + i_base_limit_detail + "),0) = 0 then 1 " + &
			"else sum(sum(amount)) over(partition by " + i_base_limit_detail + " order by " + i_base_limit_detail + ") end " + &
			"* " + &
			"sum(amount) " + &
			"/ " + &
			"nvl(nullif( sum(sum(amount)) over(partition by " + i_base_limit_detail + " order by " + i_base_limit_detail + "), 0 ), 1) " + &
		", 2) " + " amount, " + &
		"sum(quantity) quantity from " + i_source_table_name
else
	allocation_sqls += "sum(amount) amount, sum(quantity) quantity from " + i_source_table_name
end if
	
// Build the where clause
if i_whole_month or i_clearing_indicator = 3  then
	// partial months doesn't make sense for clearing calc so always force it into this block.
	start_month = i_clearing_factor_start_month
	end_month   = i_clearing_factor_end_month
else
	start_month = i_month_number
	end_month = i_month_number
end if
allocation_where_sqls = usf_build_where_clause(a_where_clause_id, start_month, end_month, i_month_period, a_cr_company_id, i_source_amount_types, '')
if allocation_where_sqls = 'ERROR' then
	f_pp_msgs("ERROR: Building where clause in usf_build_where_clause")
	return 'ERROR'
else
	allocation_sqls += ' ' + allocation_where_sqls
end if

//  Type 4:  Inter-Company (Trigger) ... must add the "Inter-Company criteria" to the
//           where clause ... this is defined in cr_system_control ... the SQL must begin
//           like "select id from cr_cost_repository where ...", returning the id's that
//           pass the inter-company criteria ... 
if i_clearing_indicator = 4 then
	allocation_sqls += " and id in (" + i_inter_co_sqls + ") "
end if

//  Work order Type rules
if i_apply_wo_types = 1 then
	//*****************************************************************************************
	//  All code from this function has been moved to f_cr_wo_clear, so it can be customized
	//  at clients (statuses and so forth) without touching the base pbl.
	//*****************************************************************************************
	f_cr_wo_clear(i_cr_company_id, i_month_number, i_allocation_id, i_clearing_id)
	f_cr_allocations_custom("before_uf_capital_work_orders_sqls")
	
	//  NEW TECHNIQUE USING THE NEW CR_WO_CLEAR TABLE:
	allocation_sqls += ' and exists ' + &
		"(select 1 from cr_wo_clear " + &
		 " where allocation_id = " + string(i_allocation_id) + &
		 " and cr_wo_clear.work_order_number = " + i_source_table_name + '."' + i_work_order_field + '") '
	
	f_cr_allocations_custom("after_uf_capital_work_orders_sqls")
end if


//  GROUP BY ... 
if not (isnull(group_fields) or trim(group_fields) = '') then
	allocation_sqls += " group by " + group_fields
end if

//  If the user did not pick any "group by" fields, the SQL will look like 
//  (SELECT , month_number ... GROUP BY , month_number ...).  Use f_replace_string
//  to correct this.
allocation_sqls = f_replace_string(allocation_sqls, "select ,",   "select ",   "first")
allocation_sqls = f_replace_string(allocation_sqls, "group by ,", "group by ", "first")


// filter the 0 amount records...
//  Apply the "exclude negatives" clause if needed.
if i_exclude_negatives = 1 and i_dollar_quantity_indicator = 1 then
	allocation_sqls += " having sum(quantity) > 0 "
elseif i_exclude_negatives = 1 and i_allocate_quantity = 1 then
	allocation_sqls += " having sum(amount) > 0 or sum(quantity) > 0 "
elseif i_exclude_negatives = 1 then
	allocation_sqls += " having sum(amount) > 0 "
elseif i_allocate_quantity = 1 then 
	allocation_sqls += " having sum(amount) <> 0 or sum(quantity) <> 0" 
elseif i_dollar_quantity_indicator = 1 then
	allocation_sqls += " having sum(quantity) <> 0" 
else
	allocation_sqls += " having sum(amount) <> 0"
end if
	

//  ### 8474: JAK: 20100330:  Absolute Value Calculation flag.  Change all "sum(amount)" to "sum(abs(amount))"
if i_absolute_value_calcs = 1 then
	allocation_sqls = f_replace_string(allocation_sqls,"sum(amount)","sum(abs(amount))","all")
	allocation_sqls = f_replace_string(allocation_sqls,"sum(quantity)","sum(abs(quantity))","all")
end if

if isnull(allocation_sqls) then
	f_pp_msgs("The SOURCE SQL is NULL ... Company = " + i_external_company_id)
	return 'ERROR'
end if

if not a_custom_dw then
	// ### 10906: JAK: 2012-09-11:  Move f_sql_add_hint lower so it can be applied to latter select statements
	// ### - SEK - 061311 - 7742: Look up and add any hints using f_sql_add_hint
	hint_syntax = 'cr_alloc_source_' + string(i_allocation_id)
	rtns = f_sql_add_hint(allocation_sqls, hint_syntax)
	
	if isnull(rtns) or rtns = "" then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Adding hint '" + hint_syntax + "'")
		f_pp_msgs(" THE ALLOCATION WILL CONTINUE TO RUN ")
		f_pp_msgs("  ")
	else 
		allocation_sqls = rtns
	end if
	
	if g_debug = 'YES' then f_pp_msgs("The source SQL (allocation_sqls) = " + allocation_sqls)
end if

//  Save the where and group by for reporting ...
uf_save_report_sql(allocation_sqls, a_custom_dw, i_source_sqls, i_grouping_sqls)

return allocation_sqls
end function

public function longlong uf_insert_target_ds (string a_datawindow, boolean a_custom_dw, string a_allocation_sqls, decimal a_clearing_balance, decimal a_clearing_balance_qty);string rtn_str, insert_sqls_start, element_descr, insert_sqls, e_for_rates, target_co, v_for_rates, col_name, value, fixed_value, trpo_col_name
string interco_trigger_col_val, interco_credit_sql, interco_company_val, credit_co
longlong num_rows, num_cols, num_targets, i, trow, s_id_for_rates, j, row_id, target_count
any id_array[]
longlong id_array_pos, dr_cr_id
decimal{2}	sum_of_source_amount, source_amount, cleared_amount, sum_of_target_amount, target_amount
decimal{4}  sum_of_source_qty, source_quantity, cleared_qty, sum_of_target_qty, target_qty, diff
decimal{10}  rate, rate_qty, clearing_rate, clearing_rate_qty
boolean elig_for_plug
longlong id

if g_debug = 'YES' then f_pp_msgs(" -- Using DS Target Method")

DECLARE credit_cursor2 DYNAMIC CURSOR FOR SQLSA ;

// Get the credit company.
credit_co = i_ds_credit.GetItemString(1, i_company_field)

if g_debug = 'YES' then
	if isnull(credit_co) then f_pp_msgs("credit_co is NULL !!!")
	f_pp_msgs("--- credit_co = " + credit_co)
end if

insert_sqls_start = uf_insert_build_sql_start()

i_ds_source.reset()
i_ds_source.DataObject = a_datawindow  // either the custom dw or dw_temp_dynamic.
i_ds_source.SetTransObject(sqlca)

target_count = 0

// Retrieve the allocation.
if a_custom_dw then
	i_ds_source.RETRIEVE(i_month_number, i_month_period, i_allocation_id)
	
	//  02/23/2005:  Added error checking (complex SQL was blowing the temp space).
	if i_ds_source.i_sqlca_sqlcode < 0 then
		f_pp_msgs("ERROR in the retrieve of the custom DW: " + i_ds_source.i_sqlca_sqlerrtext)
		return -1
	end if
	
	i_ds_source.GroupCalc()
else
	rtn_str = f_create_dynamic_ds(i_ds_source, "grid", a_allocation_sqls, sqlca, true)
	if rtn_str <> "OK" then
		f_pp_msgs("ERROR: Retrieving source criteria: " + i_ds_source.i_sqlca_sqlerrtext)
		f_pp_msgs("SQL: " + a_allocation_sqls)
		return -1
	end if
end if



//  i_ds_source now contains the source records which the rate must be applied to ...
num_rows = i_ds_source.RowCount()

//  If there are no source records, then scram ...
if num_rows < 1 then
//	if isnull(i_cr_company_id) then
//		f_pp_msgs("    There are no records that meet the source criteria.")	
//	else
//		f_pp_msgs("    There are no records that meet the source criteria for company " + i_external_company_id + ". ")
//	end if
	
	return 0
end if

if g_debug = 'YES' then f_pp_msgs("Number of source records in i_ds_source = " + string(num_rows))

num_cols    = long(i_ds_target.describe("datawindow.column.count"))
num_targets = i_ds_target.RowCount()
if a_custom_dw then num_targets = 1

//  Loop over the source records, apply the rate, and create the target records (debits)
//  in the cr_allocations table ...

//  Type 3 allocation "Clearing - Calc Rate" ... Get the denominator for the rate ...
if i_clearing_indicator = 3 then
	sum_of_source_amount = 0
	sum_of_source_qty    = 0
	
	for i = 1 to num_rows  //  Loop over source records ...
		source_amount = i_ds_source.GetItemNumber(i, "amount")
		source_quantity = i_ds_source.GetItemNumber(i, "quantity")
				
		//  Negatives not included.
		if not (i_exclude_negatives = 1 and source_amount < 0) then sum_of_source_amount = sum_of_source_amount + source_amount
		if not (i_exclude_negatives = 1 and source_quantity < 0) then sum_of_source_qty = sum_of_source_qty + source_quantity
	next
	
	// All calculates done off of the quantity field.
	if i_dollar_quantity_indicator = 1 then sum_of_source_amount = sum_of_source_qty
	
	// Calculate the rates to use.
	if sum_of_source_amount = 0 then
		clearing_rate = 0
	else
		clearing_rate = a_clearing_balance / sum_of_source_amount
	end if
	
	if sum_of_source_qty = 0 then
		clearing_rate_qty = 0
	else
		clearing_rate_qty = a_clearing_balance_qty / sum_of_source_qty
	end if
end if

cleared_amount = 0
cleared_qty    = 0
for i = 1 to num_rows  //  Loop over source records ...
	// ### 11046: JAK: 2012-09-11: Cache blocks of IDs for performance
	if i = 1 then
		f_get_column(id_array,'select crdetail.nextval id from dual connect by level <= ' + string(num_rows))
		id_array_pos = 1
	end if
		
	if a_custom_dw then
		if upper(trim(i_ds_source.GetItemString(i, "target_credit"))) <> "TARGET" then continue
		source_amount = i_ds_source.GetItemNumber(i, "amount")
		source_quantity = 0
	else
		source_amount = i_ds_source.GetItemNumber(i, "amount")
		source_quantity = i_ds_source.GetItemNumber(i, "quantity")
	end if
	
	// Negatives not included.
	if i_exclude_negatives = 1 and source_amount < 0 then source_amount = 0
	if i_exclude_negatives = 1 and source_quantity < 0 then source_quantity = 0
	
	// All calculates done off of the quantity field.
	if i_dollar_quantity_indicator = 1 then source_amount = source_quantity
	
	// Do we allocate quantities?
	if i_allocate_quantity = 0 then source_quantity = 0
	
	sum_of_target_amount = 0
	sum_of_target_qty    = 0
	
	for trow = 1 to num_targets 
		//  -------------------------------------------------------------------------------------
		//  9/17/07:  Simplify the RUN BY DEPT allocations for Clearing-Input Rate and Loadings.
		if i_run_by_department = 1 and (i_clearing_indicator = 1 or i_clearing_indicator = 2) then
			//  Is this target row applicable to the source row?  If not, continue.
			if i_ds_source.GetItemString(i, i_department_field) <> i_ds_target.GetItemString(trow, i_department_field) then	continue 
				
			if trow = num_targets then
				elig_for_plug = true // Last record in the targets is always eligible to be plugged.
			else
				if i_ds_target.GetItemString(trow, i_department_field) <> i_ds_target.GetItemString(trow + 1, i_department_field) then
					elig_for_plug = true // Last record for this dept value is always eligible to be plugged.
				else
					elig_for_plug = false // Other records for this department, will plug later.
				end if
			end if
		end if
		// End Of: 9/17/07:  Simplify the RUN BY DEPT allocations for Clearing-Input Rate.
		// ----------------------------------------------------------------------------------------
		
		// Get the next id.
		if id_array_pos <= upperbound(id_array[]) then
			id = id_array[id_array_pos]
			id_array_pos++
		else
			select crdetail.nextval into :id from dual;
		end if

		// Start the SQL
		insert_sqls = insert_sqls_start
		
		// Get the rate to use.
		setnull(rate)
		setnull(rate_qty)
		choose case i_clearing_indicator
			case 3  // clearing calc rate -- rates calculated above
				rate = clearing_rate
				rate_qty = clearing_rate_qty
			case 4  //  "Inter-Company (Trigger)" - Rate is always -1.00 (full clearing)
				rate = -i_ds_target.GetItemNumber(trow, "rate")
				rate_qty = rate
			case else  // Loading / Clearing input rate
				if i_rate_id > 0 then					
					usf_rates_by_structure(i_rate_id, s_id_for_rates, e_for_rates)
					
					if isnull(s_id_for_rates) or s_id_for_rates = 0 then
						// Standard rates
						target_co = i_ds_target.GetItemString(trow, i_company_field)
						if target_co = "*" then target_co = i_ds_source.GetItemString(i, i_company_field)
						
						rate = usf_lookup_rate(i_rate_id, target_co, i_month_number)
					else
						//  Structure based rates
						v_for_rates = i_ds_source.GetItemString(i, e_for_rates)
						if isnull(v_for_rates) then v_for_rates = "value_for_rates_was_null"
						
						rate = usf_lookup_rate_by_structure(s_id_for_rates, v_for_rates)
						if sqlca.SQLCode < 0 then
							f_pp_msgs("ERROR: TARGETS - RATE LOOKUP (FROM STRUCTURE): " + sqlca.sqlerrtext)
							f_pp_msgs("element = " + e_for_rates)
							f_pp_msgs("value   = " + v_for_rates)
							return -1
						elseif sqlca.SQLNRows = 0 then
							f_pp_msgs("ERROR: TARGETS - RATE LOOKUP (STRUCTURE) - No rows returned")
							f_pp_msgs("element = " + e_for_rates)
							f_pp_msgs("value   = " + v_for_rates)
							return -1
						end if
					end if
				else
					// Input rates
					rate = i_ds_target.GetItemNumber(trow, "rate")
				end if  //  if i_rate_id > 0 then ...
							
				// If we didn't find a rate -- default to the target row.
				if isnull(rate) then rate = i_ds_target.GetItemNumber(trow, "rate")
				rate_qty = rate
		end choose
		
		// Do we have different source 
		if isnull(rate) then rate = 0
		if isnull(rate_qty) then rate_qty = 0
		
		// Do we allocate quantities at all?
		if i_allocate_quantity = 0 then rate_qty = 0
				
		// Calculate target amounts
		target_amount = round(source_amount * rate, 2)
		target_qty = round(source_quantity * rate_qty, 2)   // should this be rounded to something other than 2?
		
		// For custom allocations...
		if a_custom_dw then
			target_amount = i_ds_source.GetItemNumber(i, "calc_amount")
			target_qty = 0
		end if
		
		// Track total amounts cleared.
		cleared_amount = cleared_amount + target_amount
		sum_of_target_amount = sum_of_target_amount + target_amount
		cleared_qty = cleared_qty + target_qty
		sum_of_target_qty = sum_of_target_qty + target_qty

		//  Type 2 ... "Clearing - Input Rate" ... Plug any rounding error ...
		if i_clearing_indicator = 2 then
			if trow = num_targets or (i_run_by_department = 1 and elig_for_plug) then
				//  Must be careful ... if the user entered negetive rates,  the calculation of the diff must be with addition ...
				if sign(source_amount) * sign(sum_of_target_amount) < 0 then
					diff = source_amount + sum_of_target_amount
				else
					diff = source_amount - sum_of_target_amount
				end if
				
				if diff <> 0 then
					target_amount = target_amount + diff
				end if
				
				if sign(source_quantity) * sign(sum_of_target_qty) < 0 then
					diff = source_quantity + sum_of_target_qty
				else
					diff = source_quantity - sum_of_target_qty
				end if
				
				if diff <> 0 then
					target_qty = target_qty + diff
				end if
			end if
		end if
		
		//  Type 3 ... "Clearing - Calc Rate" ... Plug any rounding error ...
		if i_clearing_indicator = 3 then
			if i = num_rows then
				diff = a_clearing_balance - cleared_amount
				if diff <> 0 then
					target_amount = target_amount + diff
				end if
				
				diff = a_clearing_balance_qty - cleared_qty
				if diff <> 0 then
					target_qty = target_qty + diff
				end if
			end if
		end if
		
		// Have our final quantity and amount now.  Can we skip the row?
		if target_amount = 0 and target_qty = 0 then continue
		if g_debug = 'YES' then
			f_pp_msgs("Source record = " + string(i) + &
				", Target record = " + string(trow) + &
				", target_amount = " + string(target_amount))
		end if
		
		// build dr_cr_id for below
		dr_cr_id = sign(target_amount)
		if dr_cr_id = 0 then dr_cr_id = 1
		
		insert_sqls += string(id)
		
		for j = 1 to num_cols  //  Loop over the target columns ...
			col_name     = upper(f_cr_clean_string(i_ds_target.Describe(" #" + string(j) + ".Name")))
			
			// Skip the columns we don't care about.  Get away from doing it based on the "j" variable to avoid confusion...
			choose case col_name
				case 'TARGET_ID', 'ROW_ID', 'TIME_STAMP', 'USER_ID', 'SORT_ORDER', 'RATE'
					continue
			end choose
			
			//  All fields must be filled in to create the targets ... if a field is 
			//  blank, assume that it is a hidden field (such as fields allowing for
			//  future use) ...
			value = i_ds_target.GetItemString(trow, j)
			
			if pos(value, "*") > 0 and value <> "*" then
				//  Partial Masking ...
				if right(value, 1) = "*" then // like '12*'
					fixed_value = left(value, len(value) - 1)
					value = i_ds_source.GetItemString(i, col_name)
					value = fixed_value + right(value, len(value) - len(fixed_value))
				elseif left(value, 1) = "*" then // like '*12'
					fixed_value = right(value, len(value) - 1)
					value = i_ds_source.GetItemString(i, col_name)
					value = left(value, len(value) - len(fixed_value)) + fixed_value
				end if
				
				insert_sqls += ",'" + value + "'"
				if i_suspense_accounting = "YES" then insert_sqls += ",'" + value + "'" //  The "orig" field is right next to its ACK counterpart.
			elseif value = "*" or a_custom_dw then
				//  Retain source value ... 
				value = i_ds_source.GetItemString(i, col_name)
				
				insert_sqls += ",'" + value + "'"
				if i_suspense_accounting = "YES" then insert_sqls += ",'" + value + "'" //  The "orig" field is right next to its ACK counterpart.
			elseif value = "+" then
				//  Transpose ...
				row_id = i_ds_target.GetItemNumber(trow, "row_id")
				trpo_col_name = usf_get_target_trpo_col(i_target_id, row_id, col_name)
				
				if isnull(trpo_col_name) or trim(trpo_col_name) = "" or trim(trpo_col_name) = 'ERROR' then
					f_pp_msgs("ERROR: TARGETS - TRANSPOSE: Unable to find the trpo_col_name")
					f_pp_msgs("Target ID: " + string(i_target_id))
					f_pp_msgs("Row ID: " + string(row_id))
					f_pp_msgs("Column: " + string(col_name))
					return -1
				end if
				
				value = i_ds_source.GetItemString(i, trpo_col_name) 
				
				insert_sqls += ",'" + value + "'"
				if i_suspense_accounting = "YES" then insert_sqls += ",'" + value + "'" //  The "orig" field is right next to its ACK counterpart.
			else
				insert_sqls += ",'" + value + "'"
				if i_suspense_accounting = "YES" then insert_sqls += ",'" + value + "'" //  The "orig" field is right next to its ACK counterpart.
			end if
			
			//  DMJ:  03/01/2005:  Must record the target company to use below for the opening up of the intercompany criteria.
			if upper(col_name) = upper(i_company_field) then
				target_co = value
				
				if g_debug = 'YES' then
					f_pp_msgs("i = " + string(i) + ", trow = " + string(trow) + &
						", j = " + string(j) + ", col_name = " + col_name + &
						",company_field = " + i_company_field + ", target_co = " + target_co)
				end if
			end if		
		next		//  for j = 1 to num_cols
		
		if i_rate_field_exists then insert_sqls += "," + string(rate)  // rate field
		if i_incr_run_field_exists then insert_sqls += "," + string(i_curr_incr_run_id)  // incremental run field
		insert_sqls += ","  + string(dr_cr_id) // dr_cr_id		
		insert_sqls += ",1"	// ledger sign
		insert_sqls += "," + string(target_qty) // quantity
		insert_sqls += "," + string(target_amount) // amount
		insert_sqls += "," + string(i_month_number) // month_number
		insert_sqls += "," + string(i_month_period) // month_period
		insert_sqls += ",'" + i_gl_journal_category + "'" //  gl_journal_category
		insert_sqls += "," + string(i_result_amount_type) // amount type
		insert_sqls += "," + string(i_allocation_id) // allocation_id
		insert_sqls += ", 'TARGET'" // target_credit

		//  cross_charge_company ...
	
		//  Type 4:  "Inter-Company (Trigger)" - First change the company based on the trigger
		//            column.
		if i_clearing_indicator = 4 then

			//  Get the "trigger" column value ...
			interco_trigger_col_val = i_ds_source.GetItemString(i, i_interco_trigger_col)
			
			//  Build the SQL to retrieve the correct company value ... for example, 
			//  (SELECT company from glc_area_v where area = '5270') 
			interco_credit_sql = i_original_interco_credit_sql + " '" + interco_trigger_col_val + "'"
			
			//  NEED A CURSOR TO PERFORM THIS ...
			PREPARE SQLSA FROM :interco_credit_sql;
			if sqlca.sqlcode <> 0 then
		   		f_pp_msgs("TARGETS - CURSOR PREPARE - Error " + sqlca.sqlerrtext)
				return -1
			end if 

			OPEN DYNAMIC credit_cursor2;

			if sqlca.sqlcode <> 0 then
				f_pp_msgs("TARGETS - CURSOR OPEN - Error " + sqlca.sqlerrtext)
				return -1
			end if 

			FETCH credit_cursor2 INTO :interco_company_val;

			interco_company_val = trim(interco_company_val)
			
			if sqlca.sqlcode < 0 then
				// SQL Error
				f_pp_msgs("TARGETS - CURSOR FETCH - Error " + sqlca.sqlerrtext)
				return -1
			elseif sqlca.sqlcode = 100 then 
				// No Rows found
				f_pp_msgs("TARGETS - CURSOR FETCH - No Records Found")
				f_pp_msgs("SQL: " + interco_credit_sql)
				return -1
			end if 
			
			CLOSE credit_cursor2;
			
		else    //  if i_clearing_indicator = 4 then ...
			
			if i_intercompany_accounting = 1 then
				if a_custom_dw then
					interco_company_val = i_ds_source.GetItemString(i, "cross_charge_company")
				else
					interco_company_val = i_ds_source.GetItemString(i, i_company_field)
				end if
				
				//  DMJ:  03/01/2005:  ADDITIONAL CHANGE TO GO ALONG WITH THE OPENING UP OF THE 
				//                     INTERCOMPANY LOOKUP BELOW.
				//
				//    Since we opened up the lookup to allow the intercompany criteria to pass even
				//    if the company = cross_charge_company, we have opened up a hole on allocations
				//    where there source criteria is being cleared off of that company (company = 
				//    cross_charge_company) with a negative rate and the offset company is different.
				//    This change should not affect the clearing of CO1 to CO1, 2, and 3 where the
				//    offset is back to CO 1 --- In that case the source CO = the target CO on CO 1
				//    and the offset CO is also CO1.  That was the original case we were trying to 
				//    open up the code for.
				//
				//    Extend this rule to everything ... by virtue of the fact that we allow only 1
				//    credit record in the credit criteria, they can only mask of direct to 1 company.
				//    If masking, do nothing --- else set cross_charge_company = credit_co.
				//    ---> WHAT ABOUT CUSTOM_DW'S !!!
				//    ---> THE ONLY PROBLEM I SEE IS IF THEY USE CO1 AS THE SOURCE AND THEN DIRECT
				//         THE TARGETS TO OTHER COMPANIES (CO2, CO3) AND MASK ON THE CREDIT.  FOR
				//         THIS I AM MAKING INTERCO_COMPANY_VAL = TARGET_CO.
				
//////			//  If the target company and the cross_charge_company are equal ...
//////			if target_co = interco_company_val then
				
				if g_debug = 'YES' then
					f_pp_msgs("if target_co = interco_company_val then ... TRUE")
				end if
				
				//  If the target_co is not equal to the company in the credit criteria (and
				//  the credit criteria <> *) ...
//////			if target_co <> credit_co and credit_co <> "*" then
				if credit_co <> "*" then
					
					if g_debug = 'YES' then
						f_pp_msgs( 'if target_co <> credit_co and credit_co <> "*" then ... TRUE')
					end if
					
					//  Since the source was clearing to its own company in the targets and since
					//  the credit_co is <> to that company ... set the cross_charge_company to
					//  the credit_co to force intercompany accounting against the company that
					//  is being offset.
					interco_company_val = credit_co
					
				else
//////				THIS WAS ADDED AS PART OF THE FINAL CHANGE.
					interco_company_val = target_co
					
					if g_debug = 'YES' then
						f_pp_msgs( 'if target_co <> credit_co and credit_co <> "*" then ... FALSE')
					end if
				end if
			else
				interco_company_val = " "
			end if
			
			if g_debug = 'YES' then f_pp_msgs("interco_company_val = " + interco_company_val)
		end if  //  if i_clearing_indicator = 4 then ...
		
		insert_sqls += ", '" + interco_company_val + "'"
		
		insert_sqls += ")"		
		
		if isnull(insert_sqls) then
			f_pp_msgs("ERROR: The (Execute immediate # 1) is NULL ... ")
			return -1
		end if
		
		
		//  Execute immediate # 1
		if g_debug = 'YES' then f_pp_msgs("insert_sqls = " + insert_sqls)
		execute immediate :insert_sqls using sqlca;
		
		if sqlca.SQLCode <> 0 then
			f_pp_msgs("ERROR - Targets:  " + sqlca.SQLErrText)
			f_pp_msgs("Insert SQL = " + insert_sqls)
			return -1
		else
			target_count += sqlca.sqlnrows
		end if
		
	next //  for trow = 1 to num_targets ...
next //  for i = 1 to num_rows ... 

return target_count
end function

public function longlong uf_insert_target_direct (string a_datawindow, boolean a_custom_dw, string a_allocation_sqls, decimal a_clearing_balance, decimal a_clearing_balance_qty);string company_sql, final_insert_sql, insert_sqls_start, inner_sql, element_descr, element_sql, credit_co
string quantity_calc_sql, amount_calc_sql, amount_rate_sql, quantity_rate_sql, target_value, fixed_value
string trpo_col_name
longlong i, row_id, num_t, rtn, num_targets, a_sbr_structure_id
boolean rounding_needed
string rounding_partition, outer_select_sql, inner_select_sql, rounding_net_amount, rounding_net_quantity, a_sbr_element


// ********************************************************************************
//
//  Validate that the allocation is eligible for fast credits
//
// ********************************************************************************
// Custom allocations are not eligible
if a_custom_dw then return -2	// Allocation not eligible for direct insert method.
if i_clearing_indicator = 4 then return -2 // Type 4:  "Inter-Company (Trigger)" - not eligible

if i_rate_id > 0 then 
	// See if we are using structure based rates.
	usf_rates_by_structure(i_rate_id, a_sbr_structure_id, a_sbr_element)
	if a_sbr_structure_id <> 0 and not isnull(a_sbr_structure_id) then return -2
end if
if i_disable_fast = "YES" then return -2 // system switch to fail over just incase

if g_debug = 'YES' then f_pp_msgs(" -- Using Direct Target Method")

// ********************************************************************************
//
//  Setup
//
// ********************************************************************************
insert_sqls_start = uf_insert_build_sql_start()
// Insert_sqls_start ends in 'values ('.  Strip this off since it will be a select statement.
if pos(insert_sqls_start,'values (') > 0 then 
	insert_sqls_start = mid(insert_sqls_start,1,pos(insert_sqls_start,'values (') - 1)
end if

// Get the credit company.
credit_co = i_ds_credit.GetItemString(1, i_company_field)
if g_debug = 'YES' then
	if isnull(credit_co) then f_pp_msgs("credit_co is NULL !!!")
	f_pp_msgs("--- credit_co = " + credit_co)
end if

// Figure out the fields the quantity calc and amount calcs are based.
quantity_calc_sql = 'quantity'
amount_calc_sql = 'amount'

if i_exclude_negatives = 1 then
	quantity_calc_sql = 'decode(sign(' + quantity_calc_sql + '),-1,0,' + quantity_calc_sql + ')'
	amount_calc_sql = 'decode(sign(' + amount_calc_sql + '),-1,0,' + amount_calc_sql + ')'
end if

// Are all calculates done off of the quantity field?
if i_dollar_quantity_indicator = 1 then 
	amount_calc_sql = quantity_calc_sql
end if

// Do we allocate quantities?
if i_allocate_quantity = 0 then 
	quantity_calc_sql = '0'
end if

// Get rates for clearing allocations.
if i_clearing_indicator = 3 then
	rtn = uf_insert_target_direct_calc_rate(a_allocation_sqls, a_clearing_balance, a_clearing_balance_qty, amount_calc_sql, quantity_calc_sql, amount_rate_sql, quantity_rate_sql)
	if rtn < 0 then
		// Error...message logged in f(x)
		return rtn
	end if
end if

// Are we doing an allocation type where rounding is needed
if i_clearing_indicator = 2 or i_clearing_indicator = 3 then
	rounding_needed = true
else
	rounding_needed = false
end if

if rounding_needed then
	if i_clearing_indicator = 2 then
		// Rounding needs to be done over each individual source record.
		rounding_partition = "min(id)"
		rounding_net_amount = "sum(" + amount_calc_sql + ")"
		rounding_net_quantity = "sum(" + quantity_calc_sql + ")"
	elseif i_clearing_indicator = 3 then
		// Rounding needs to be done over all records.  Run by Department rounding is done as part of a 2nd function
		//	currently so don't need a special partition there
		rounding_partition = "1"
		rounding_net_amount = string(a_clearing_balance)
		rounding_net_quantity = string(a_clearing_balance_qty)
	end if
end if

// ********************************************************************************
//
//  Perform fast inserts.
// 		SQL will be of the form:
//				
//			insert into cr_allocation_stg (id, company, ..., cross_charge_company) 
//					with source_criteria as (select company, ..., sum(quantity), sum(amount) From cr_cost_repository where ... group by ...)
//				select crdetail.nextval, **target criteria row 1** from source_criteria union all
//				select crdetail.nextval, **target criteria row 2** from source_criteria;
//
// ********************************************************************************
// Build the SQL Insert
final_insert_sql = insert_sqls_start

// If we are doing rounding, then need to add the rounding_partition to the a_allocation_sqls.
if rounding_needed then 
	a_allocation_sqls = f_replace_string(a_allocation_sqls," from ", &
		", " + rounding_partition + " rounding_partition, " + rounding_net_amount + " rounding_net_amount, " + rounding_net_quantity + " rounding_net_quantity from ", &
		"first")
end if

// Add the with source_criteria as statement
final_insert_sql += " with source_criteria as (" + a_allocation_sqls + ") "

num_targets = i_ds_target.rowcount()
for num_t = 1 to num_targets
	if i_clearing_indicator = 1 or i_clearing_indicator = 2 then	
		//	Input rates
		
		if i_rate_id > 0 then
			// Lookup rates -- done below.  Already validated above that we are not using structure based rates.
		else
			amount_rate_sql = string(i_ds_target.GetItemNumber(num_t, "rate"))
			quantity_rate_sql = amount_rate_sql	
		end if
	elseif i_clearing_indicator = 3 then
		// Calc rate -- values set above in setup block
	end if
		
	// Column: ID -- handled below
	inner_select_sql += " select "	
	
	// Columns: Elements -- may also have the ORIG_ values if suspense accounting is also turned on.
	for i = 1 to i_num_elements		//  Loop over the code block to build the credits ...
		element_descr = i_ds_elements.GetItemString(i, "description")
		target_value = i_ds_target.GetItemString(num_t,element_descr)	
		
		if pos(target_value, "*") > 0 and target_value <> "*" then
			// Partial Masking
			if right(target_value, 1) = "*" then // like '12*'
				fixed_value = left(target_value, len(target_value) - 1)
				element_sql = "'" + fixed_value + "'" + '||substr("' + element_descr + '", ' + string(len(fixed_value) + 1) + ', 254)'
			elseif left(target_value, 1) = "*" then // like '*12'
				fixed_value = right(target_value, len(target_value) - 1)
				element_sql = 'substr("' + element_descr + '", 1, length("' + element_descr + '")-' + string(len(fixed_value)) + ')' + "||'" + fixed_value + "'"
			end if
					
		elseif target_value = '*' then
			//  Masking
			element_sql = '"' + element_descr + '"'
				
		elseif target_value = '+' then
			// Transposing	
			row_id = i_ds_target.GetItemNumber(num_t, "row_id")
			trpo_col_name = usf_get_target_trpo_col(i_target_id, row_id, element_descr)
			
			if isnull(trpo_col_name) or trim(trpo_col_name) = "" or trim(trpo_col_name) = 'ERROR' then
				f_pp_msgs("ERROR: TARGETS - TRANSPOSE: Unable to find the trpo_col_name")
				f_pp_msgs("Target ID: " + string(i_target_id))
				f_pp_msgs("Row ID: " + string(row_id))
				f_pp_msgs("Column: " + string(element_descr))
				return -1
			end if
			
			element_sql = '"' + trpo_col_name + '"' // Not the current element, but the transpose value.			
			
		else
			// Literal value
			element_sql = "'" + target_value + "'"
			
		end if
		
		inner_select_sql += element_sql + ' "' + element_descr + '", '
		if i_suspense_accounting = "YES" then inner_select_sql += element_sql + ' "ORIG_' + element_descr + '", '	

		if upper(element_descr) = upper(i_company_field) then
			company_sql = element_sql
		end if
				
	next		//  for i = 1 to num_elements ... Loop over the code block to build the credits
	
	// Columns: Allocation Rate
	if i_rate_id > 0 then
		// Lookup rates -- can vary based on each target row based on the company.  Already validated above that we are not using structure based rates.
		amount_rate_sql = "(select min(rate) keep (dense_rank first order by decode(cr_rates_input.company_field," + company_sql + ",1,2), " + &
			"decode(cr_rates_input.month_number,'" + string(i_month_number) + "',1,2)) from cr_rates_input " + &
			"where rate_id = " + string(i_rate_id) + &
			" and cr_rates_input.company_field in (" + company_sql + ",'ALL') " + &
			" and cr_rates_input.month_number in ('" + string(i_month_number) + "','ALL'))"
		quantity_rate_sql = amount_rate_sql	
	end if
		
	if i_rate_field_exists then
		inner_select_sql += amount_rate_sql + ' ALLOCATION_RATE, '
	end if
	
	// Columns: INCREMENTAL_RUN_ID
	if i_incr_run_field_exists then
		inner_select_sql += string(i_curr_incr_run_id) + ' incremental_run_id, '
	end if
	
	// Columns: DR_CR_ID
	inner_select_sql += 'decode(sign(' + amount_calc_sql + ' * ' + amount_rate_sql + '),-1,-1,1) dr_cr_id, '
	
	// Columns: LEDGER_SIGN
	inner_select_sql += '1 ledger_sign, '
	
	// Columns: QUANTITY
	if quantity_calc_sql = '0' then
		// simplify the SQL
		inner_select_sql += '0 quantity, '
	else
		inner_select_sql += 'round(' + quantity_calc_sql + ' * ' + quantity_rate_sql + ',2) quantity, '
	end if
	
	// Columns: AMOUNT
	inner_select_sql += 'round(' + amount_calc_sql + ' * ' + amount_rate_sql + ',2) amount, '
	
	// Columns: MONTH_NUMBER
	inner_select_sql += string(i_month_number) + ' month_number, '
	
	// Columns: MONTH_PERIOD
	inner_select_sql += string(i_month_period) + ' month_period, '
	
	// Columns: GL_JOURNAL_CATEGORY
	inner_select_sql += "'" + i_gl_journal_category + "' gl_journal_category, "
	
	// Columns: AMOUNT_TYPE
	inner_select_sql += string(i_result_amount_type) + ' amount_type, '
	
	// Columns: ALLOCATION_ID
	inner_select_sql += string(i_allocation_id) + ' allocation_id, '
	
	// Columns: TARGET_CREDIT
	inner_select_sql += "'TARGET' target_credit, "
	
	// Columns: CROSS_CHARGE_COMPANY
	if i_intercompany_accounting = 1 then
		if credit_co = '*' then
			// Use target company -- would already be in the group by so no need to add it again.
			inner_select_sql += company_sql + ' cross_charge_company '
		else 
			// fixed credit_co -- use that value
			inner_select_sql += "'" + credit_co + "' cross_charge_company "
		end if	
	else
		inner_select_sql += "' ' cross_charge_company "
	end if
	
	// Pull the rounding partition value forward if we are doing rounding partitions
	if rounding_needed then
		inner_select_sql += ", rounding_partition, rounding_net_amount, rounding_net_quantity "
	end if
	
	// Build from, where
	inner_select_sql += "from source_criteria "
	
	// If run by department and a loading / input rate, only certain rows apply.
	if i_run_by_department = 1 and (i_clearing_indicator = 1 or i_clearing_indicator = 2) then
		inner_select_sql += "where " + i_department_field + " = '" + i_ds_target.GetItemString(num_t, i_department_field) + "' "
	end if
	
	// If we have multiple targets, add them here
	if num_t <> num_targets then
		inner_select_sql += " union all "
	end if
next

// Due to rounding and sequences...wrap the whole thing.
// Column: ID -- couldn't do this on the inside
outer_select_sql += " select crdetail.nextval id, "

// Columns: Elements -- may also have the ORIG_ values if suspense accounting is also turned on.
for i = 1 to i_num_elements		//  Loop over the code block to build the credits ...
	element_descr = i_ds_elements.GetItemString(i, "description")
	outer_select_sql += '"' + element_descr + '", '
	if i_suspense_accounting = "YES" then outer_select_sql += '"ORIG_' + element_descr + '", '	
next		//  for i = 1 to num_elements ... Loop over the code block to build the credits

// Columns: Allocation Rate
if i_rate_field_exists then
	outer_select_sql += "allocation_rate, "
end if

// Columns: INCREMENTAL_RUN_ID
if i_incr_run_field_exists then
	outer_select_sql += "incremental_run_id, "
end if

// Columns: DR_CR_ID -- penny rounding shouldn't effect the dr_cr_id...
outer_select_sql += 'dr_cr_id, '

// Columns: LEDGER_SIGN
outer_select_sql += 'ledger_sign, '

// Columns: QUANTITY & AMOUNT
if rounding_needed then
	// ** PLUG **	
	
	if quantity_calc_sql = '0' then
		// simplify the SQL
		outer_select_sql += '0 quantity, '
	else
		outer_select_sql += 'quantity + ' + &
			"decode(row_number() over (partition by rounding_partition order by abs(quantity) desc),1," + &
				"rounding_net_quantity - sum(quantity) over (partition by rounding_partition)," + &
				"0) quantity, "
	end if

	outer_select_sql += 'amount + ' + &
		"decode(row_number() over (partition by rounding_partition order by abs(amount) desc),1," + &
			"rounding_net_amount - sum(amount) over (partition by rounding_partition)," + &
			"0) amount, "
else
	outer_select_sql += "quantity, amount, "
end if

// Columns: MONTH_NUMBER
outer_select_sql += 'month_number, '

// Columns: MONTH_PERIOD
outer_select_sql += 'month_period, '

// Columns: GL_JOURNAL_CATEGORY
outer_select_sql += "gl_journal_category, "

// Columns: AMOUNT_TYPE
outer_select_sql += 'amount_type, '

// Columns: ALLOCATION_ID
outer_select_sql += 'allocation_id, '

// Columns: TARGET_CREDIT
outer_select_sql += "target_credit, "

// Columns: CROSS_CHARGE_COMPANY
outer_select_sql += "cross_charge_company "

// Build the final inner_select_sql
outer_select_sql += "from (" + inner_select_sql + ") where nvl(quantity,1) <> 0 or nvl(amount,1) <> 0"	// don't want to drop the nulls here. If they happen, we want a SQL error!
inner_select_sql =outer_select_sql	

// Build final SQL
final_insert_sql += inner_select_sql

//  Execute immediate # 2
if isnull(final_insert_sql) then
	f_pp_msgs("ERROR - Inserting Targets (Direct): final_insert_sql is null!")
	return -1
end if

if g_debug = 'YES' then f_pp_msgs("Insert SQL = " + final_insert_sql)
execute immediate :final_insert_sql using sqlca;
if sqlca.SQLCode <> 0 then
	if not g_debug = 'YES' then f_pp_msgs("Insert SQL = " + final_insert_sql)
	if mid(upper(sqlca.SQLErrText),1,9) = 'ORA-01400' and (pos(upper(sqlca.SQLErrText),'AMOUNT') > 0 or pos(upper(sqlca.SQLErrText),'QUANTITY') > 0) and i_rate_id > 0 then
		// Null value being inserted into Quantity / Amount.  Almost certainly a missing rate.
		f_pp_msgs("ERROR - Inserting Targets (Direct): Rates missing for company/month combinations.")
	else
		f_pp_msgs("ERROR - Inserting Targets (Direct): " + sqlca.SQLErrText)
	end if
	return -1
end if

if g_debug = 'YES' then f_pp_msgs("Rows Inserted = " + string(sqlca.sqlnrows))
	
return sqlca.SQLNRows
end function

public function longlong uf_insert_credit_ds (boolean a_custom_dw);longlong num_rows, num_cols, i, j, row_id, num_inserts, num_credit_cols, id_array_pos, id, k, z
string credit_sqls, credit_group_sqls, element_descr, value, fixed_value, insert_sqls, source_value, trpo_col_name
string interco_trigger_col_val, interco_company_val, insert_sqls_start, col_name
any id_array[]
decimal{2} credit_amount
decimal{4} credit_qty
DECLARE credit_cursor DYNAMIC CURSOR FOR SQLSA ;

if g_debug = 'YES' then f_pp_msgs(" -- Using DS Credits Method")

string interco_credit_sql

if a_custom_dw then
	i_ds_credit_insert = i_ds_source // has both the targets and credits 
else
	i_ds_credit_insert.DataObject = "dw_temp_dynamic"
end if

insert_sqls_start = uf_insert_build_sql_start()

// Credits retrieved above for interco purposes.
num_rows = i_ds_credit.RowCount()
num_cols = long(i_ds_credit.describe("datawindow.column.count"))

if a_custom_dw then num_rows = 1

//  NOTE: Currently there can only be 1 credit row per allocation type ...
for i = 1 to num_rows				//  Loop over credit criteria records ...
	
	credit_sqls       = "select "
	credit_group_sqls = ""
	
	for j = 1 to i_num_elements		//  Loop over the code block to build the credits ...
		
		element_descr = i_ds_elements.GetItemString(j, "description")
		value = upper(i_ds_credit.GetItemString(i, element_descr))
		
		if value = " " then
			//  Leave a single space alone ...
		else
			value = trim(value)
		end if
		
		if pos(value, "*") > 0 and value <> "*" then
			//  Partial Masking ...
			if right(value, 1) = "*" then // like '12*' ... 254 hardcoded for performance.
				fixed_value = left(value, len(value) - 1)
				credit_sqls       = credit_sqls       + "'" + fixed_value + "'" + '||substr("' + element_descr + '", 1+' + string(len(value)) + '-1, 254),'
				credit_group_sqls = credit_group_sqls + "'" + fixed_value + "'" + '||substr("' + element_descr + '", 1+' + string(len(value)) + '-1, 254),'
			end if
			
			if left(value, 1) = "*" then // like '*12'
				fixed_value = right(value, len(value) - 1)
				credit_sqls       = credit_sqls       + 'substr("' + element_descr + '", 1, length(' + element_descr + ')-' + string(len(fixed_value)) + ')||' + "'" + fixed_value + "'" + ','
				credit_group_sqls = credit_group_sqls + 'substr("' + element_descr + '", 1, length(' + element_descr + ')-' + string(len(fixed_value)) + ')||' + "'" + fixed_value + "'" + ','
			end if
			//DEBUG: f_pp_msgs("PARTIAL MASKING", col_name + " - " + fixed_value)
			insert_sqls += ",'" + source_value + "'"
			goto after_credit_masking
		end if
		
		choose case value
			case "*"  //  Get the field from the target records (put the field name in the
						 //  select and group by of the sql ...
				credit_sqls       = credit_sqls + '"' + element_descr + '",'
				credit_group_sqls = credit_group_sqls + '"' + element_descr + '",'
				
			case else
				if value = "+" then
					// transpose.
					row_id = i_ds_credit.GetItemNumber(i, "row_id")
					trpo_col_name = usf_get_credit_trpo_col(i_credit_id, row_id, element_descr)
					if isnull(trpo_col_name) or trim(trpo_col_name) = "" or trim(trpo_col_name) = 'ERROR' then
						f_pp_msgs("ERROR: CREDITS - TRANSPOSE: Unable to find the trpo_col_name")
						f_pp_msgs("Credit ID: " + string(i_credit_id))
						f_pp_msgs("Row ID: " + string(row_id))
						f_pp_msgs("Column: " + string(element_descr))
						return -1
					end if
						
					credit_sqls       = credit_sqls + '"' + trpo_col_name + '",' // Not the current element, but the transpose value.
					credit_group_sqls = credit_group_sqls + '"' + trpo_col_name + '",' // Not the current element, but the transpose value.
				else
					credit_sqls = credit_sqls + "'" + value + "',"
				end if
				
		end choose
		
		after_credit_masking:
		
	next		//  for j = 1 to num_elements ... Loop over the code block to build the credits

	if isnull(credit_group_sqls) then credit_group_sqls  = ''
	
	// Always need to group by MN, MP, and company
	if i_clearing_indicator = 4 or i_intercompany_accounting <> 1 then
		//  If they are doing intercompany accounting, they should be using
		//  a negative rate with the billable companies as the targets.
		//  This is the opposite of the "Trigger" method and the reason we
		//  don't have this SQL for type 4 ... Mulitple credits must
		//  be booked on the billing company's books with the cross_charge_company
		//  field filled in correctly.
		credit_sqls += "month_number, month_period, cross_charge_company, -sum(amount), -sum(quantity) " 
		credit_group_sqls += " month_number, month_period, cross_charge_company"
	
	else
		credit_sqls += "month_number, month_period, " + i_company_field + " cross_charge_company, -sum(amount), -sum(quantity) " 
		credit_group_sqls += " month_number, month_period, " + i_company_field
	end if
	
	credit_sqls +=  " from " + i_alloc_stg_table + " where drilldown_key is null "
	credit_sqls +=  " and allocation_id = " + string(i_allocation_id)
	credit_sqls +=  " group by " + credit_group_sqls
									 
	//  Insert the credits ...
	if isnull(credit_sqls) then
		f_pp_msgs("ERROR: The CREDIT INSERT SQL is NULL ... ")
		return -1
	end if
	
	if a_custom_dw then
	else
	   f_create_dynamic_ds(i_ds_credit_insert, "grid", credit_sqls, sqlca, true)
	end if

	if g_debug = 'YES' then
		f_pp_msgs("credit_sqls = " + credit_sqls)
		f_pp_msgs("num_inserts = " + string(num_inserts))
	end if

	num_inserts     = i_ds_credit_insert.RowCount()
	num_credit_cols = long(i_ds_credit_insert.describe("datawindow.column.count"))

	if num_inserts = 0 then
		f_pp_msgs("ERROR: building i_ds_credit_insert!  No rows returned!")
		f_pp_msgs("credit_sqls = " + credit_sqls)
		return -1
	end if
	
	//  Type 4:  "Inter-Company (Trigger)" - First change the company based on the trigger
	//            column.
	if i_clearing_indicator = 4 then

		for k = 1 to num_inserts  //  Inter-Company ...
			
			//  Get the "trigger" column value ...
			interco_trigger_col_val = i_ds_credit_insert.GetItemString(k, i_interco_trigger_col)
			
			//  Build the SQL to retrieve the correct company value ... for example, 
			//  (SELECT company from glc_area_v where area = '5270') 
			interco_credit_sql = i_original_interco_credit_sql + " '" + &
									   interco_trigger_col_val + "'"
			
			//  NEED A CURSOR TO PERFORM THIS ...
			PREPARE SQLSA FROM :interco_credit_sql;
			if sqlca.sqlcode <> 0 then
		  	 	f_pp_msgs("ERROR: CURSOR PREPARE: " + sqlca.sqlerrtext)
				return -1
			end if 

			OPEN DYNAMIC credit_cursor;

			if sqlca.sqlcode <> 0 then
				f_pp_msgs("ERROR: CURSOR OPEN: " + sqlca.sqlerrtext)
				return -1
			end if 

			FETCH credit_cursor INTO :interco_company_val;

			interco_company_val = trim(interco_company_val)

			if sqlca.sqlcode < 0 then
				// SQL Error
				f_pp_msgs("CREDITS - CURSOR FETCH - Error " + sqlca.sqlerrtext)
				return -1
			elseif sqlca.sqlcode = 100 then 
				// No Rows found
				f_pp_msgs("CREDITS - CURSOR FETCH - No Records Found")
				f_pp_msgs("SQL: " + interco_credit_sql)
				return -1
			end if 
			
			
			CLOSE credit_cursor;
			
			//  Plug the company from the target into the cross_charge_company
			//  field ... Then replace it with the company from the trigger.
			i_ds_credit_insert.SetItem(k, "cross_charge_company", i_ds_credit_insert.GetItemString(k, i_company_field))
			i_ds_credit_insert.SetItem(k, i_company_field, interco_company_val)
			
		next  //  for k = 1 to num_inserts  //  Inter-Company ...
		
	end if  //  if i_clearing_indicator = 4 then ...

	for k = 1 to num_inserts
		// ### 38448:  Move this initialization of id_array up so it is always called.
		// ### 11046: JAK: 2012-09-11: Cache blocks of IDs for performance
		if k = 1 then
			f_get_column(id_array,'select crdetail.nextval id from dual connect by level <= ' + string(num_inserts))
			id_array_pos = 1
		end if
		
		if a_custom_dw then
			if upper(trim(i_ds_source.GetItemString(k, "target_credit"))) <> "CREDIT" then continue
		end if

		
		if id_array_pos <= upperbound(id_array[]) then
			id = id_array[id_array_pos]
			id_array_pos++
		else
			select crdetail.nextval into :id from dual;
		end if
		
		insert_sqls = insert_sqls_start + string(id)
		
		//  Need to perform a separate loop for the custom DW allocations or else the
		//  insert will be out of order ... This will avoid having to format the DW SQL
		//  in a particular order.
		if a_custom_dw then
			for z = 1 to num_cols
				if z <= 4 then continue  //  Skip the credit_id, row_id, time_stamp, user_id
				col_name = i_ds_credit.Describe(" #" + string(z) + ".Name")
				col_name = f_replace_string(col_name, " ", "_", "all")
				col_name = f_replace_string(col_name, "/", "_", "all")
				col_name = f_replace_string(col_name, "-", "_", "all")
				value = i_ds_credit_insert.GetItemString(k, col_name)
				insert_sqls += ",'" + value + "'"			
				if i_suspense_accounting = "YES" then
					//  The "orig" field is right next to its ACK counterpart.
					insert_sqls += ",'" + value + "'"
				end if
			next
		else
			for z = 1 to num_credit_cols - 5  //  Exclude the month_number,month_period,
														 //  cross_charge_company,amount columns ... DMJ: 6/11/08: CHANGED FROM 4
				value = i_ds_credit_insert.GetItemString(k, z)
				insert_sqls += ",'" + value + "'"			
				if i_suspense_accounting = "YES" then
					//  The "orig" field is right next to its ACK counterpart.
					insert_sqls += ",'" + value + "'"
				end if
			
			next
		end if
				
		//  dr_cr_id ... (+) amount = 1 ... (-) amount = -1 ...
		if i_rate_field_exists then
			insert_sqls += "," + string(0)
		end if
		
		if i_incr_run_field_exists then
			insert_sqls += "," + string(i_curr_incr_run_id)
		end if
		
		if a_custom_dw then
			credit_amount = i_ds_credit_insert.GetItemNumber(k, "calc_amount")
			credit_qty    = 0
		else
			credit_amount = i_ds_credit_insert.GetItemNumber(k, num_credit_cols - 1) // DMJ: 6/11/08: ADDED -1
			if i_allocate_quantity = 1 then
				credit_qty = i_ds_credit_insert.GetItemNumber(k, num_credit_cols)
			else
				 credit_qty = 0
			end if
		end if
		
		if i_allocate_quantity = 1 then
			if credit_amount = 0 and credit_qty = 0 then continue
		else
			if credit_amount = 0 then continue
		end if
		
		if credit_amount >= 0 then
			insert_sqls += ", 1"
		else
			insert_sqls += ", -1"
		end if
		//  ledger_sign = 1 ...
		insert_sqls += ", 1"
		//  qty = 0 ...
		if i_allocate_quantity = 1 then
			insert_sqls += "," + string(credit_qty)
		else
			insert_sqls += ",0"
		end if
		//  amount ...
		insert_sqls += "," + string(credit_amount)
		//  month_number and month_period from the source ...
		insert_sqls += "," + &
						  string(i_ds_credit_insert.GetItemNumber(k, "month_number")) + &
						  "," + &
						  string(i_ds_credit_insert.GetItemNumber(k, "month_period"))
		//  gl_journal_category ...
		insert_sqls += ",'" + i_gl_journal_category + "'"
		//  amount_type ...
		insert_sqls += "," + string(i_result_amount_type) // DMJ: 6/11/08: was string(1)
		//  allocation_id ...
		insert_sqls += "," + string(i_allocation_id)
		//  target_credit ...
		insert_sqls += ", 'CREDIT'"
		//  cross_charge_company ...
		if isnull(i_ds_credit_insert.GetItemString(k, "cross_charge_company")) then
			insert_sqls += ",' '"
		else	
			insert_sqls += ",'" + &
				i_ds_credit_insert.GetItemString(k, "cross_charge_company") + "'"
		end if
		
		insert_sqls += ")"		 
		
		if isnull(insert_sqls) then
			f_pp_msgs("ERROR: The (Execute immediate # 2) is NULL ... ")
			return -1
		end if

		//  Execute immediate # 2
		execute immediate :insert_sqls using sqlca;

		if sqlca.SQLCode <> 0 then
			f_pp_msgs("ERROR - Inserting Credits: " + sqlca.SQLErrText)
			f_pp_msgs("Insert SQL = " + insert_sqls)
			return -1
		end if
		
	next  //  for k = 1 to num_inserts ...
	
next	//  for i = 1 to num_rows ... Loop over credit criteria records ...

return 0
end function

public function longlong uf_insert_credit_direct (boolean a_custom_dw);string insert_sqls_start, insert_sql, credit_value, inner_group_sql, element_sql, inner_sql
longlong i, row_id
string element_descr, fixed_value, trpo_col_name

// ********************************************************************************
//
//  Validate that the allocation is eligible for fast credits
//
// ********************************************************************************
// Custom allocations are not eligible
if a_custom_dw then return -2	// Allocation not eligible for direct insert method.
if i_clearing_indicator = 4 then return -2 // Type 4:  "Inter-Company (Trigger)" - not eligible
if i_disable_fast = "YES" then return -2 // system switch to fail over just incase

if g_debug = 'YES' then f_pp_msgs(" -- Using Direct Credits Method")

// ********************************************************************************
//
//  Setup
//
// ********************************************************************************
insert_sqls_start = uf_insert_build_sql_start()
// Insert_sqls_start ends in 'values ('.  Strip this off since it will be a select statement.
if pos(insert_sqls_start,'values (') > 0 then 
	insert_sqls_start = mid(insert_sqls_start,1,pos(insert_sqls_start,'values (') - 1)
end if


// ********************************************************************************
//
//  Perform fast inserts.
//
// ********************************************************************************
// Build the SQL Insert
insert_sql = insert_sqls_start

inner_sql = "select "
inner_group_sql = " group by "

// Column: ID -- will be done on outer select.  All other fields will be part of the inner_sql and group
insert_sql += " select crdetail.nextval id, "

// Columns: Elements -- may also have the ORIG_ values if suspense accounting is also turned on.
for i = 1 to i_num_elements		//  Loop over the code block to build the credits ...
	element_descr = i_ds_elements.GetItemString(i, "description")
	credit_value = upper(i_ds_credit.GetItemString(1, element_descr))
	
	if credit_value = " " then
		//  Leave a single space alone as a literal value
	else
		credit_value = trim(credit_value)
	end if
	
	if pos(credit_value, "*") > 0 and credit_value <> "*" then
		//  Partial Masking ...
		if right(credit_value, 1) = "*" then // like '12*' ... 254 hardcoded for performance.
			fixed_value = left(credit_value, len(credit_value) - 1)
			element_sql = "'" + fixed_value + "'" + '||substr("' + element_descr + '", ' + string(len(fixed_value) + 1) + ', 254)'
		elseif left(credit_value, 1) = "*" then // like '*12'
			fixed_value = right(credit_value, len(credit_value) - 1)
			element_sql = 'substr("' + element_descr + '", 1, length("' + element_descr + '")-' + string(len(fixed_value)) + ')' + "||'" + fixed_value + "'"
		end if
		
		inner_sql += element_sql + ' "' + element_descr + '", '
		if i_suspense_accounting = "YES" then inner_sql += element_sql + ' "ORIG_' + element_descr + '", '
		inner_group_sql += element_sql + ', '
		
	elseif credit_value = '*' then
		//  Masking
		element_sql = '"' + element_descr + '"'
		
		inner_sql += element_sql + ' "' + element_descr + '", '
		if i_suspense_accounting = "YES" then inner_sql += element_sql + ' "ORIG_' + element_descr + '", '
		inner_group_sql += element_sql + ', '
		
	elseif credit_value = '+' then
		// Transposing
		row_id = i_ds_credit.GetItemNumber(1, "row_id")
		trpo_col_name = usf_get_credit_trpo_col(i_credit_id, row_id, element_descr)
		if isnull(trpo_col_name) or trim(trpo_col_name) = "" or trim(trpo_col_name) = 'ERROR' then
			f_pp_msgs("ERROR: CREDITS - TRANSPOSE: Unable to find the trpo_col_name")
			f_pp_msgs("Credit ID: " + string(i_credit_id))
			f_pp_msgs("Row ID: " + string(row_id))
			f_pp_msgs("Column: " + string(element_descr))
			return -1
		end if
		element_sql = '"' + trpo_col_name + '"' // Not the current element, but the transpose value.
				
		inner_sql += element_sql + ' "' + element_descr + '", '
		if i_suspense_accounting = "YES" then inner_sql += element_sql + ' "ORIG_' + element_descr + '", '
		inner_group_sql += element_sql + ', '
		
	else
		// Literal value
		element_sql = "'" + credit_value + "'"
		
		inner_sql += element_sql + ' "' + element_descr + '", '
		if i_suspense_accounting = "YES" then inner_sql += element_sql + ' "ORIG_' + element_descr + '", '
		// no group
		
	end if
next		//  for j = 1 to num_elements ... Loop over the code block to build the credits


// Columns: Allocation Rate -- hardcoded to 0.
if i_rate_field_exists then
	inner_sql += '0 ALLOCATION_RATE, '
end if

// Columns: INCREMENTAL_RUN_ID
if i_incr_run_field_exists then
	inner_sql += string(i_curr_incr_run_id) + ' incremental_run_id, '
end if

// Columns: DR_CR_ID
inner_sql += 'decode(sign(sum(amount) * -1),-1,-1,1) dr_cr_id, '

// Columns: LEDGER_SIGN
inner_sql += '1 ledger_sign, '

// Columns: QUANTITY
inner_sql += 'sum(quantity) * -1 quantity, '

// Columns: AMOUNT
inner_sql += 'sum(amount) * -1 amount, '

// Columns: MONTH_NUMBER
inner_sql += 'month_number, '
inner_group_sql += 'month_number, '

// Columns: MONTH_PERIOD
inner_sql += 'month_period, '
inner_group_sql += 'month_period, '

// Columns: GL_JOURNAL_CATEGORY
inner_sql += 'gl_journal_category, '
inner_group_sql += 'gl_journal_category, '

// Columns: AMOUNT_TYPE
inner_sql += string(i_result_amount_type) + ' amount_type, '

// Columns: ALLOCATION_ID
inner_sql += string(i_allocation_id) + ' allocation_id, '

// Columns: TARGET_CREDIT
inner_sql += "'CREDIT' target_credit, "

// Columns: CROSS_CHARGE_COMPANY
if i_intercompany_accounting = 1 then
inner_sql += i_company_field + ' cross_charge_company '
inner_group_sql += i_company_field + ', '
	
else
	inner_sql += "' ' cross_charge_company "
end if

// Build from, where
inner_sql += "from " + i_alloc_stg_table + " where drilldown_key is null and allocation_id = " + string(i_allocation_id)
inner_group_sql = mid(inner_group_sql,1,len(inner_group_sql) - 2)  // remove the trailing ", "
inner_sql += inner_group_sql
inner_sql += " having sum(quantity) <> 0 or sum(amount) <> 0"

// Build and execute final SQL
insert_sql += "inner.* from (" + inner_sql + ") inner"


//  Execute immediate # 2
if isnull(insert_sql) then
	f_pp_msgs("ERROR - Inserting Credits (Direct): insert_sql is null!")
	return -1
end if

if g_debug = 'YES' then f_pp_msgs("Insert SQL = " + insert_sql)
execute immediate :insert_sql using sqlca;

if sqlca.SQLCode <> 0 then
	if not g_debug = 'YES' then f_pp_msgs("Insert SQL = " + insert_sql)
	f_pp_msgs("ERROR - Inserting Credits (Direct): " + sqlca.SQLErrText)
	return -1
elseif sqlca.SQLNRows = 0 then
	f_pp_msgs("WARNING - No credit records inserted (Direct)")
//	f_pp_msgs("Insert SQL = " + insert_sql)
	return 0
else 
	if g_debug = 'YES' then f_pp_msgs("Rows Inserted = " + string(sqlca.sqlnrows))
	
	return 0
end if
end function

public function string uf_insert_build_sql_start ();string insert_sqls_start, element_descr
longlong i

//  Begin building the SQL INSERT ...
insert_sqls_start = 'insert into ' + i_alloc_stg_table + ' ("ID'

for i = 1 to i_num_elements
	element_descr     = i_ds_elements.GetItemString(i, "description")
	insert_sqls_start += '","' + element_descr
	if i_suspense_accounting = "YES" then insert_sqls_start += '","ORIG_' + element_descr //  The "orig" field is right next to its ACK counterpart.
next

if i_rate_field_exists then
	insert_sqls_start += '","ALLOCATION_RATE'
end if

if i_incr_run_field_exists then
	insert_sqls_start += '","INCREMENTAL_RUN_ID'
end if

insert_sqls_start += '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
									 				 ',"MONTH_NUMBER","MONTH_PERIOD"' + &
													 ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
									 				 ',"ALLOCATION_ID","TARGET_CREDIT",' + &
													 '"CROSS_CHARGE_COMPANY") values ('
													 
return insert_sqls_start

end function

public function longlong uf_insert_target_direct_calc_rate (string a_allocation_sqls, decimal a_clearing_balance, decimal a_clearing_balance_qty, ref string a_amount_calc_sql, ref string a_quantity_calc_sql, ref string a_clearing_rate, ref string a_clearing_rate_qty);longlong num_rows
decimal{2} amount_source
decimal{4} quantity_source
decimal{10} clearing_rate, clearing_rate_qty

// ********************************************************************************
//
//  Calculate the rates to be used for a clearing calc rate allocation
//
// ********************************************************************************
string sqls

sqls = "with source_criteria as (" + a_allocation_sqls + ") " + &
	"select sum(" + a_amount_calc_sql + ") amount, sum(" + a_quantity_calc_sql + ") quantity from source_criteria"
	
i_ds_sum_amount.Reset()
i_ds_sum_amount.SetSQLSelect(sqls)
num_rows = i_ds_sum_amount.retrieve()

if num_rows < 0 then
	f_pp_msgs("ERROR in uf_insert_target_direct_calc_rate: " + i_ds_sum_amount.i_sqlca_sqlerrtext)
	f_pp_msgs("ERROR SQL: " + sqls)
	return -1
elseif num_rows = 0 then
	amount_source = 0
	quantity_source = 0
else
	amount_source = i_ds_sum_amount.GetItemNumber(1, 1)
	quantity_source = i_ds_sum_amount.GetItemNumber(1, 2)
	
	if isnull(amount_source) then amount_source = 0
	if isnull(quantity_source) then quantity_source = 0
end if

// Calculate the rates to use.
if amount_source = 0 then
	clearing_rate = 0
else
	clearing_rate = a_clearing_balance / amount_source
end if

if quantity_source = 0 then
	clearing_rate_qty = 0
else
	clearing_rate_qty = a_clearing_balance_qty / quantity_source
end if

// Pass the values back via reference
a_clearing_rate = string(clearing_rate)
a_clearing_rate_qty = string(clearing_rate_qty)

return 0
end function

public function string usf_build_where_clause_incr_range (longlong a_start_month, longlong a_end_month, longlong a_allocation_id, longlong a_cr_company_id);longlong rtn, curr_min_src_val, curr_max_src_val, i, incr_rows, incr_rtn, temp_min_value, &
	temp_max_value, reporting_min_value, reporting_max_value
string range_sqls, incr_stg_sqls1, between_sqls, incr_sqls, between_sqls2

if g_debug = "YES" then
	f_pp_msgs("Incremental allocation code to build where clause range (usf_build_where_clause_incr_range)")
	f_pp_msgs("Incremental_run_id = " + string(i_curr_incr_run_id))
end if

//************************************************************
//
// Get variables for an incremental run
//
//************************************************************

// between statement for finding range of value for incremental run
if UPPER(i_source_value_field) = 'ID' then
	between_sqls = " id between INC.min_source_value and INC.max_source_value "
else
	between_sqls = " to_number(drilldown_key) between INC.min_source_value and INC.max_source_value "
end if

temp_max_value = 0
temp_min_value = 0
curr_min_src_val = 99999999999
curr_max_src_val = 0

//************************************************************
//
// Create the select statement that will be used to find the incremental CR range(s)
//
//************************************************************

// use the to_number in the select in case the source table is not the CR_CR
incr_stg_sqls1 = "select " +&
						string(i_curr_incr_run_id) + " incremental_run_id " +&
						", " + string(a_allocation_id) + " allocation_id " +&
						", " + string(i_curr_month_run_id) + " month_run_id " +&
						", month_number source_month_number " +&
						", " + string(i_month_number) + " run_month_number " +&
						", to_number(min(" + i_source_value_field + ")) min_source_value " +&
						", to_number(max(" + i_source_value_field + ")) max_source_value " +&
					 "from " + i_source_table_name +&
					 	" where "
						 
//MWA 20150107
//For the vast majority of incremental runs there won't be any deleting/reversing/undoing prior to it
//So adding the "and" clause in here proved to speed up identifying the new range significantly.
//In the case of running, undoing, and rerunning the select statement will return 0 upon the rerun
//because all of the undone incremental control records will have an incremental_status_id = 2, not 1
//This applies to reversed (3) and reprocessed (4) as well. Lastly, this only works if the source of the
//allocation is from one month...much trickier when the source is over a range of months
if a_start_month <> a_end_month then
	incr_stg_sqls1 += " month_number between " + string(a_start_month) + " and " + string(a_end_month)
else 
	incr_stg_sqls1 += " month_number = " + string(a_end_month) +&
		" and " + i_source_value_field + " >= " +&
		" ( " +&
		" select nvl(to_number(max(max_source_value)),0) " +&
		" from cr_alloc_incremental_control " +&
		" where allocation_id =  " + string(a_allocation_id) +&
		" and incremental_status_id = 1 " +&
		" and run_month_number = " + string(i_month_number) +&
		" and incremental_run_id <  " +string(i_curr_incr_run_id) +&
		" ) "
end if

// company criteria - borrowed this from usf_build_where_clause
if isnull(a_cr_company_id) or a_cr_company_id = 0 or a_cr_company_id = 10000 then
	// no additional criteria
else
	incr_stg_sqls1 +=		" and " + i_company_field + " = '" + upper(trim(i_external_company_id)) + "' "
end if

// Need to use this sql later so make it separate from the rest of the select statement
// by the next time it's used. 
// Incremental_status_id: 1 = Processed, 2 = Undone/Deleted (ready to reprocess), 3 = Reversed, 4 = Reprocessed

range_sqls = " and "+&
	" ( "+&
	" not exists (select 1 "+&
	" from cr_alloc_incremental_control INC "+&
	" where INC.allocation_id =  " + string(a_allocation_id)+" "+&
	" and month_number = INC.source_month_number "+&
	" and " + between_sqls +&
	" and INC.incremental_run_id <  " +string(i_curr_incr_run_id) + " "+&
	" and INC.incremental_status_id in (1,3) " +&
	" ) " +&
	" ) "
	
incr_stg_sqls1 += range_sqls + " group by month_number "

if g_debug = "YES" then
	f_pp_msgs("Incr SQL Select: " + incr_stg_sqls1)
end if

//************************************************************
//
// Find the overall CR range for the incremental run
//
//************************************************************
		
incr_rtn = i_ds_where_clause_incr.SetSQLSelect(incr_stg_sqls1)
incr_rtn = i_ds_where_clause_incr.RETRIEVE()
if incr_rtn < 0 then
	f_pp_msgs("ERROR: No data to run incrementally for allocation_id: " + string(a_allocation_id))
	f_pp_msgs(i_ds_where_clause_incr.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + incr_stg_sqls1)
	return 'ERROR'
elseif incr_rtn = 0 then
	f_pp_msgs("0 rows retrieved to run incrementally for allocation_id: " + string(a_allocation_id))
end if

incr_rows = i_ds_where_clause_incr.RowCount()
for i = 1 to incr_rows
	//Get the overall maximum value for the range that will be used in usf_build_where_clause
	temp_max_value = i_ds_where_clause_incr.GetItemNumber(i, 'max_source_value')
	
	if temp_max_value > curr_max_src_val then
		curr_max_src_val = temp_max_value
	end if
	
	//Get the overall minimum value for the range that will be used in usf_build_where_clause
	temp_min_value = i_ds_where_clause_incr.GetItemNumber(i, 'min_source_value')
	
	if temp_min_value < curr_min_src_val then
		curr_min_src_val = temp_min_value
	end if	
next	

if incr_rtn <= 0 then
	curr_min_src_val = 0
	curr_max_src_val = 0
end if

f_pp_msgs("Incremental Range: " + string(i_source_value_field) + " between " + string(curr_min_src_val) + " and " + string(curr_max_src_val))


//************************************************************
//
// For reporting, create the where clause to put in cr_alloc_process_control that
// will capture the the entire month's worth of source charges for all incremental runs
//
//************************************************************
	
	if incr_rtn > 0 then
		reporting_min_value = 0
		//reporting_min_value is the lower limit of the range that will be used for monthly reporting
		select nvl(min(min_source_value),0) into :reporting_min_value
		from cr_alloc_incremental_control
		where run_month_number = :i_month_number
			and incremental_status_id in (1,2)
			and allocation_id = :i_allocation_id;
			
		if reporting_min_value = 0 then
			reporting_min_value = curr_min_src_val
		end if 
		
		reporting_max_value = curr_max_src_val
		
	else //if incr_rtn = 0 
		reporting_min_value = 0
		//reporting_min_value is the lower limit of the range that will be used for monthly reporting
		//reporting_max_value is the upper limit
		select nvl(min(min_source_value),0),nvl(max(max_source_value),0) into :reporting_min_value,:reporting_max_value
		from cr_alloc_incremental_control
		where run_month_number = :i_month_number
			and incremental_status_id in (1,2)
			and allocation_id = :i_allocation_id;
				
	end if
		
	if UPPER(i_source_value_field) = 'ID' then
		i_incr_month_source_sql = " (id between " + string(reporting_min_value) + " and " + string(reporting_max_value) + ") "
	else
		i_incr_month_source_sql = " (drilldown_key between '" + string(reporting_min_value) + "' and '" + string(reporting_max_value) + "') "
	end if
	
	
	i_incr_month_source_sql += " and ( exists ( " +&
													" select 1 from cr_alloc_incremental_control INC " +&
													" where INC.allocation_id = " + string(i_allocation_id) +&
													" and INC.incremental_status_id in (1,3) " +&
													" and month_number = INC.source_month_number " +&
													" and INC.run_month_number = " + string(i_month_number) +&
													" and " + string(i_source_value_field) + " between INC.min_source_value and INC.max_source_value " +&
													" ) " +&
												" ) "

//************************************************************
//
// Create the where clause to return to usf_build_where_clause
//
//************************************************************

// between statement to return to usf_build_where_clause
// if the source table is the CR use the PK, if it's a CR detail table use the DDK
if UPPER(i_source_value_field) = 'ID' then
	between_sqls2 = "id between " + string(curr_min_src_val) + " and " + string(curr_max_src_val)
else // use tick marks ---> ' <---- around the values for drilldown_key usage
	between_sqls2 = "drilldown_key between '" + string(curr_min_src_val) + "' and '" + string(curr_max_src_val) + "' "
end if
 
incr_sqls = " (" + between_sqls2 + ") " + range_sqls


if g_debug = "YES" then
	f_pp_msgs("Incr SQLS: " + incr_sqls)
end if

return incr_sqls
end function

public function longlong uf_update_cr (boolean a_reversal, longlong a_month_number, longlong a_month_period);//*****************************************************************************************
//
//  Update cr_cost_repository:
//
//*****************************************************************************************
longlong source_id, rtn, orig_process_id
string batch_id, cwip_charge_status, where_clause, sqls

// Get the source id
source_id = usf_get_source_id(i_alloc_dtl_table)

// Do we goto the GL / CWIP
if i_elim_flag = 1 or i_run_as_commitments_flag = 1 then
	// Do not post
	batch_id = '-173'
	cwip_charge_status = '-173'
else
	// Post
	batch_id = ''
	cwip_charge_status = ''
end if

// What do we summarize?
//  Get all records (targets and credits) for this allocation_id, month_number, month_period combination ...
// ### 9113: JAK: 2011-12-06:  added the reversal_id and interface_batch_id joins below to match
//	what's in uf_loading2.  Was causing problems with adjusting JEs / auto reversals on run for all
//	company allocations.
where_clause = " where allocation_id = " + string(i_allocation_id) + " and "
where_clause += " month_number = " + string(a_month_number) + " and "
where_clause += " month_period = " + string(a_month_period)  + " and "
where_clause += " nvl(interface_batch_id,'0') = '0' and "
if a_reversal then
	// summarizing reversal transactions
	where_clause += " target_credit in ('RVTARG','RVCRED','RVINCO') "
else
	// summarizing regular transactions
	where_clause += " reversal_id is null "
end if

if i_run_as_a_test then
		
	sqls = "insert into " + i_alloc_dtl_table + " select * from " + i_alloc_stg_table
	execute immediate :sqls using sqlca;
	
	if sqlca.SQLCode <> 0 then
		f_pp_msgs("ERROR: inserting into detail table from staging table (test allocations): " + sqlca.SQLErrText)
		f_pp_msgs("SQL: " + sqls)
		return -1
	end if
	
	//  Cannot fire a truncate_table here since there has been no commit (lock error).
	//  We will truncate in uf_start_allocations after the commit (for performance).
	//  We must still delete here since we may be in an "all companies" loop and the
	//  commit does not fire until after the loop.
	sqls = "delete from " + i_alloc_stg_table
	execute immediate :sqls using sqlca;
	
	if sqlca.SQLCode <> 0 then
		f_pp_msgs("ERROR: Deleting from staging table (test allocations): " + sqlca.SQLErrText)
		f_pp_msgs("SQL: " + sqls)
		return -1
	end if
	
	// TEST MODE
	// Need to populate the drilldown key and return since we don't summarize these.
	f_pp_msgs("Marking drilldown_key on test results ...")

	sqls = 'update ' + i_alloc_dtl_table + ' set drilldown_key = -1 '
	sqls += where_clause
	
	execute immediate :sqls;
	
	if sqlca.SQLCode <> 0 then
		f_pp_msgs("ERROR: Updating drilldown_key = -1 for test allocations: " + sqlca.SQLErrText)
		return -1
	end if
	
else
	// ACTUAL MODE		
	f_pp_msgs("Updating cr_cost_repository ...")
	// don't want all of the messages.  put a fake g_process_id in to suppress the logs.
	if g_debug <> 'YES' then orig_process_id = g_process_id 
	if g_debug <> 'YES' then g_process_id = -187596
	rtn = i_uo_cr_cost_repository.uf_insert_to_cr_general_from_stg(i_alloc_stg_table, where_clause, batch_id, batch_id, cwip_charge_status)
	if g_debug <> 'YES' then g_process_id = orig_process_id
	
	if rtn = 1 then
	else
		choose case rtn
			case -11
				f_pp_msgs("ERROR: Source ID lookup failed: " + sqlca.SQLErrText)
			case -12
				f_pp_msgs("ERROR: Truncating cr_temp_cr before start failed: " + sqlca.SQLErrText)
			case -13
				f_pp_msgs("ERROR: inserting into cr_temp_cr: " + sqlca.SQLErrText)
			case -14
				f_pp_msgs("ERROR: drilldown_key UPDATE failed: " + sqlca.SQLErrText)
			case -15
				f_pp_msgs("ERROR: inserting into cr_cost_repository: " + sqlca.SQLErrText)
			case -16
				f_pp_msgs("ERROR: Insert into CR Detail table from CR Staging Table failed: " + sqlca.SQLErrText)
			case -17
				f_pp_msgs("ERROR: Clearing CR Staging Table failed: " + sqlca.SQLErrText)
			case -18
				f_pp_msgs("ERROR: deleting from cr_temp_cr failed: " + sqlca.SQLErrText)
			case else
				f_pp_msgs("ERROR: unknown error in uo_cr_cost_repository: " + sqlca.SQLErrText)
		end choose
		return -1
	end if
end if

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_exe_cr_allocations_custom_version nvo_exe_cr_allocations_custom_version
nvo_exe_crallo_custom_dw_version nvo_exe_crallo_custom_dw_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'exe_cr_allocations_custom.pbd'
		return nvo_exe_cr_allocations_custom_version.custom_version
	case 'exe_crallo_custom_dw.pbd'
		return nvo_exe_crallo_custom_dw_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

