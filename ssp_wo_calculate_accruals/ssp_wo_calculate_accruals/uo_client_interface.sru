HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_calculate_accruals.exe'

nvo_wo_control i_nvo_wo_control
end variables

forward prototypes
public function longlong uf_read ()
private function boolean uf__mainaccrualcalc ()
private function boolean uf_validate (longlong a_company_id, string a_company_descr)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// call the main accrual calc function and return success or failure
if uf__mainaccrualcalc( ) then
	commit;
	i_nvo_wo_control.of_releaseprocess( process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)
	return rtn_success
else
	rollback;
	// log the list of companies that failed to approve accruals
	i_nvo_wo_control.of_log_failed_companies("CALCULATE ACCRUALS")
	return rtn_failure
end if
end function

private function boolean uf__mainaccrualcalc ();/************************************************************************************************************************************************************
 **
 **	uf__mainaccrualcalc()
 **	
 **	The main function for accrual calculation.  This is called by uf_read and will call all other functions
 **
 **	INFORMATION:
 **		Parameters passed from PowerPlan and Job Server:
 **		g_ssp_parms.long_arg = Company IDs
 **		g_ssp_parms.date_arg[1] = Accrual Month 
 **	
 **	Parameters	:	none
 **
 **	Returns		:	boolean	:	true if successful
 **										false if not
 **
 ************************************************************************************************************************************************************/
longlong i, num_companies, month_number, rtn
i_nvo_wo_control.of_constructor()
string process_msg

// if any company in the loop does not calculate accruals
// continue on to the next company but return a failure
boolean b_all_success
b_all_success = true

// these parameters originate from the w_wo_control window
num_companies = upperBound( g_ssp_parms.long_arg )
i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_wo_control.i_month = datetime( g_ssp_parms.date_arg[1] )

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("Calculate Accruals - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

// populate the company descs
rtn = i_nvo_wo_control.of_getDescriptionsFromids( i_nvo_wo_control.i_company_idx )
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if 

// loop over the companies and process accruals
f_pp_msgs("Processing Accruals...")
for i = 1 to num_companies
	try
		// change the company in the NVO
		i_nvo_wo_control.of_companychanged( i , i_nvo_wo_control.i_month )
		f_pp_msgs("   Company_id: " + string( i_nvo_wo_control.i_company_idx[ i ] ) )
		
		// validate this company can run accruals.
		if uf_validate( i_nvo_wo_control.i_company_idx[ i ], i_nvo_wo_control.i_company_descr[ i ] ) then
			// try to delete the accruals.  The function throws an exception if the SQL errors, so catch it.
			f_accrual_delete( i_nvo_wo_control.i_company_idx[ i ], i_nvo_wo_control.i_month )
			// rebuild the work order summaries since deleted from cwip_charge
			f_update_wo_summary_month_accr( i_nvo_wo_control.i_company_idx[ i ], i_nvo_wo_control.i_month )
			
			// calculate the accruals for the company
			f_accrual_calculation( i_nvo_wo_control.i_company_idx[ i ], i_nvo_wo_control.i_month )
			// update wo summary again since accruals are calculated now
			f_update_wo_summary_month_accr( i_nvo_wo_control.i_company_idx[ i ], i_nvo_wo_control.i_month )
		end if
		i_nvo_wo_control.i_ds_wo_control.setItem(1, 'accrual_calc', today())
		// commit by company
		i_nvo_wo_control.of_updatedw( )
	catch( exception e )
		f_pp_msgs( e.getMessage() )
		rollback;
		// keep track of all the companies that failed to approve accruals so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(i)
	end try
next

f_pp_msgs("Process ended")
//i_nvo_wo_control.of_cleanup( 6, 'email wo close: automatic unitization', "Auto Unitization")

if not b_all_success then
	f_pp_msgs("***********************************************************")
	f_pp_msgs("WARNING: AT LEAST ONE COMPANY HAD AN ERROR.  PLEASE REVIEW THE LOGS")
	f_pp_msgs("***********************************************************")
end if

return b_all_success
end function

private function boolean uf_validate (longlong a_company_id, string a_company_descr);/************************************************************************************************************************************************************
 **
 **	uf_validate()
 **	
 **	This function will validate that the accrual calculation can process for the passed in company and month
 **
 **	
 **	Parameters	:	a_company_id		:	longlong	:	The company to check
 **
 **	Returns		:	boolean	:	true if company can process
 **										false if the company cannot process
 **
 ************************************************************************************************************************************************************/
string wo_accr_process_ans
datetime approved_date

// check to see if this company is setup to process accruals.
wo_accr_process_ans = f_pp_system_control_company("Work Order Accrual Calculation", a_company_id)
if wo_accr_process_ans = "yes" then 
	// the company is setup to process accruals.  Now check to see if this month is already approved
	approved_date = i_nvo_wo_control.i_ds_wo_control.getItemDateTime(1, 'accrual_approval')
	if not isnull(approved_date) then
		f_pp_msgs( "Accruals have already been approved." )
		return false
	end if
	// Make sure the month isn't closed
	if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		return false
	end if
else
	f_pp_msgs( "System Control: 'Work Order Accrual Calculation' not set for Company: " + string(a_company_id) + " - " + a_company_descr )
	return false
end if

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_wo_calculate_accruals_custom_version nvo_ssp_wo_calculate_accruals_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version

choose case a_pbd_name
	case 'ssp_wo_calculate_accruals_custom.pbd'
		return nvo_ssp_wo_calculate_accruals_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version	
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

