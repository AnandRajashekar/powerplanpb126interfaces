HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_gl_recon.exe'

nvo_cpr_control i_nvo_cpr_control

end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function boolean uf_gl_recon_main ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure, rtn
string process_msg
w_datawindows w

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Process G/L Recon logic
if not uf_gl_recon_main() then
	i_nvo_cpr_control.of_log_failed_companies("RECONCILE GL")
	rtn = rtn_failure
elseif upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
	i_nvo_cpr_control.of_log_failed_companies("RECONCILE GL")
	rtn = rtn_failure
else
	rtn = rtn_success
end if

// Release the concurrency lock
i_nvo_cpr_control.of_releaseProcess(process_msg)
f_pp_msgs("Release Process Status: " + process_msg)

return rtn
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function boolean uf_gl_recon_main ();/************************************************************************************************************************************************************
**
**	uf_gl_recon_main()
**	
**	This function corresponds to w_cpr_control.cb_gl_reconciliation.event clicked().
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
longlong i, rc, pp_stat_id, ret, num_failed_companies, rtn
string args[]

i_nvo_cpr_control.of_constructor()

// Set instance variables based on the parameters passed in g_ssp_parms
i_nvo_cpr_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_cpr_control.i_original_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_original_col_num = g_ssp_parms.long_arg2[1]
i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return false
end if

// Get the descriptions
rtn = i_nvo_cpr_control.of_getDescriptionsFromIds(i_nvo_cpr_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// Verify whether the selected companies can be processed together
if (i_nvo_cpr_control.of_selectedCompanies(i_nvo_cpr_control.i_company_idx) = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if

// Process the selected companies
for i = 1 to upperbound(i_nvo_cpr_control.i_company_idx)
	rc = i_nvo_cpr_control.of_setupCompany(i)
	
	if rc < 1 then continue // next company
	
	
	if isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'je_released')) then
		f_pp_msgs("The PowerPlant GL Journal Entries must be released for the month before reconciling to the GL.")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = i
		
		continue
	end if
	
	if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = i
		
		continue
	end if
	
	// Call f_reconcile_gl to Reconcile PowerPlant with GL ( after GL transactions have
	// been passed to GL and results returned ) .
	
	f_pp_msgs("GL Reconcile started for company " + i_nvo_cpr_control.i_company_descr[i]  +  " at " + String(Today(), "hh:mm:ss"))
	
	pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "G/L Reconciliation: " + i_nvo_cpr_control.i_company_descr[i])

	// Call the custom function
	ret = f_reconcile_gl(i_nvo_cpr_control.i_month)
	
	if ret = -999 then
		f_pp_msgs("GL Reconciliation process is not set up for company " + i_nvo_cpr_control.i_company_descr[i] )
	elseif ret <> 1 then
		f_pp_msgs("Possible that external Journal Entry was posted to GL, but NOT to PowerPlant." )
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = i
		
		continue
	else
		f_pp_msgs("PowerPlant and General Ledger are in Balance. ")
	end if
	
	// Call Dynamic Validation option
	args[1] = string(i_nvo_cpr_control.i_month)
	args[2] = string(i_nvo_cpr_control.i_company)
	
	ret = f_wo_validation_control(1007,args)
	
	if ret < 0 then
		rollback;
		f_pp_msgs("GL Reconciliation (Dynamic Validations) Failed.")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = i
		
		continue
	end if
	
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
	
	i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'gl_reconciled',today())
	
	i_nvo_cpr_control.of_updateDW() 
next

// Email users
i_nvo_cpr_control.of_cleanup(7, 'email cpr close: g/l reconciliation', 'G/L Reconciliation')

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_gl_recon_custom_version nvo_ssp_gl_recon_custom_version

choose case a_pbd_name
	case 'ssp_gl_recon_custom.pbd'
		return nvo_ssp_gl_recon_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

