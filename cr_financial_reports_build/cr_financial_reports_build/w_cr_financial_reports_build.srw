HA$PBExportHeader$w_cr_financial_reports_build.srw
forward
global type w_cr_financial_reports_build from window
end type
type dw_cr_financial_reports_edit from datawindow within w_cr_financial_reports_build
end type
type dw_cr_financial_reports_setup_grid from datawindow within w_cr_financial_reports_build
end type
type dw_cr_financial_reports_columns_grid from datawindow within w_cr_financial_reports_build
end type
end forward

global type w_cr_financial_reports_build from window
integer width = 2533
integer height = 1500
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
dw_cr_financial_reports_edit dw_cr_financial_reports_edit
dw_cr_financial_reports_setup_grid dw_cr_financial_reports_setup_grid
dw_cr_financial_reports_columns_grid dw_cr_financial_reports_columns_grid
end type
global w_cr_financial_reports_build w_cr_financial_reports_build

on w_cr_financial_reports_build.create
this.dw_cr_financial_reports_edit=create dw_cr_financial_reports_edit
this.dw_cr_financial_reports_setup_grid=create dw_cr_financial_reports_setup_grid
this.dw_cr_financial_reports_columns_grid=create dw_cr_financial_reports_columns_grid
this.Control[]={this.dw_cr_financial_reports_edit,&
this.dw_cr_financial_reports_setup_grid,&
this.dw_cr_financial_reports_columns_grid}
end on

on w_cr_financial_reports_build.destroy
destroy(this.dw_cr_financial_reports_edit)
destroy(this.dw_cr_financial_reports_setup_grid)
destroy(this.dw_cr_financial_reports_columns_grid)
end on

event open;//*****************************************************************************************
//
//  There are some DW's on this window that will be referenced by the UO:
//
//*****************************************************************************************
dw_cr_financial_reports_columns_grid.SetTransObject(sqlca)
dw_cr_financial_reports_setup_grid.SetTransObject(sqlca)
dw_cr_financial_reports_edit.SetTransObject(sqlca)
end event

type dw_cr_financial_reports_edit from datawindow within w_cr_financial_reports_build
integer x = 37
integer y = 216
integer width = 1152
integer height = 88
integer taborder = 30
boolean titlebar = true
string title = "dw_cr_financial_reports_edit"
string dataobject = "dw_cr_financial_reports_edit"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cr_financial_reports_setup_grid from datawindow within w_cr_financial_reports_build
integer x = 37
integer y = 116
integer width = 1152
integer height = 88
integer taborder = 20
boolean titlebar = true
string title = "dw_cr_financial_reports_setup_grid"
string dataobject = "dw_cr_financial_reports_setup_grid"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cr_financial_reports_columns_grid from datawindow within w_cr_financial_reports_build
integer x = 37
integer y = 16
integer width = 1152
integer height = 88
integer taborder = 10
boolean titlebar = true
string title = "dw_cr_financial_reports_columns_grid"
string dataobject = "dw_cr_financial_reports_columns_grid"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

