HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong i_month_number, i_tt_dollar_type_sort_order[], i_structure_id_or[], i_report_id, &
		 i_dollar_columns
string i_temp_tables[], i_temp_tables_or[], i_tt_dollar_types[], i_use_new_structures_table, &
       i_temp_table, i_col_col, i_col_val_for_insert
		 
string i_calling_user_id, i_default_bv
longlong i_num_reports_to_build, i_num_errors, i_start_month, i_end_month, i_calling_session_id

uo_ds_top i_ds_generic_string_audit, i_ds_formula_audit1, i_ds_formula_audit2, &
			 i_ds_formula_audit3, i_ds_dollar_types, i_ds_levels, i_ds_str_override, &
			 i_ds_formulas, i_ds_formulas2

string i_exe_name = 'cr_financial_reports_build.exe'
end variables

forward prototypes
public function longlong uf_drop ()
public function string uf_get_col_val (longlong a_row)
public subroutine uf_delete_remove_row ()
public function longlong uf_update_temp_table_from_cols ()
public function longlong uf_start ()
public function longlong uf_zero_line_item (longlong a_id_counter, longlong a_header_row, string a_col_val_for_insert, string a_val_without_squigglies, longlong a_mn, longlong a_sort_order, string a_dollar_type, longlong a_dollar_type_sort_order, longlong a_setup_id)
public function longlong uf_create_temp_table (longlong a_month_number, string a_top_point)
public function longlong uf_build_one ()
public function integer uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_drop ();longlong t, o
string sqls

for t = 1 to upperbound(i_temp_tables)
	
	i_temp_table = i_temp_tables[t]
	
	sqls = "drop table " + i_temp_table
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		//messagebox("Build", "ERROR: dropping table " + i_temp_table + ":~n~n" + sqlca.SQLErrText, &
		//	Exclamation!)
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: dropping table " + i_temp_table + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	else
		commit;
	end if
		
next

//  For structure overrides.
for o = 1 to upperbound(i_temp_tables_or)
	sqls = "drop table " + i_temp_tables_or[o]
	execute immediate :sqls;
	if sqlca.SQLCode < 0 then
		//messagebox("Build", "ERROR: dropping table " + i_temp_tables_or[o] + ":~n~n" + sqlca.SQLErrText, &
		//	Exclamation!)
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: dropping table " + i_temp_tables_or[o] + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	else
		commit;
	end if
next

return 1
end function

public function string uf_get_col_val (longlong a_row);longlong i
string col_val, rtn_col_val
datawindow d
//  *****  VARIABLES FOR THE STAND-ALONE  *****
w_cr_financial_reports_build w
w = w_cr_financial_reports_build

d = w.dw_cr_financial_reports_setup_grid

//
//  There are 10 line item columns.  The functionality is: if we find a value in a column
//  (working from left to right), use it.  Otherwise, go to the next column.
//
setnull(col_val)
i_col_col            = ""
rtn_col_val          = ""
i_col_val_for_insert = ""

for i = 1 to 10
	
	col_val = trim(d.GetItemString(a_row, "col" + string(i)))
	
	if isnull(col_val) or col_val = "" then
		
		//  Go check the next column ...
		continue
		
	else
		
		//
		//  NOTE:  ALL THE CODE BELOW IS HERE ON PURPOSE SO LABELS ENTERED ALONG WITH A STRUCTURE
		//         REF WILL OVERRIDE THE STRUCTURE DESCRIPTION.  IT WAS NEVER INTENDED TO CAPTURE
		//         A CASE LIKE A DESCRIPTIVE LABEL IN COL1 AND A 2ND LABEL FROM THE STRUCTURE IN
		//         COL2.
		//
		
		//  Return the col_val ... set the instance variable that identifies which of the 10
		//  columns to populate.  Check for NULL or "" so that the case of:
		//  col1 = Total Assets, col2 = {Total Assets}
		//  does not try to put a label in col2.  Also, i_col_col must follow this pattern since
		//  it is the insert part of the select that is using rtn_col_val.
		if isnull(rtn_col_val) or rtn_col_val = "" then
			rtn_col_val = rtn_col_val + "'" + col_val + "', "
			i_col_col = i_col_col + "col" + string(i) + ", "
		end if
		
//		exit // NO ... NEED TO CONTINUE TO STRING COLS TOGETHER.

		if i_col_val_for_insert = "" and left(col_val, 1) = "{" then
			//  This is for the "start with" clause since it can only be a single value.  The idea
			//  is that they could set up a line like: col1 = Total Assets, col2 = {Total Assets}
			i_col_val_for_insert = col_val
		end if
		
	end if
	
next

return rtn_col_val
end function

public subroutine uf_delete_remove_row ();//
//  COL1:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col1 in 
	(select replace(replace(col1,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col1:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col1: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL2:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col2 in 
	(select replace(replace(col2,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col2:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col2: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL3:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col3 in 
	(select replace(replace(col3,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col3:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col3: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL4:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col4 in 
	(select replace(replace(col4,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col4:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col4: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL5:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col5 in 
	(select replace(replace(col5,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col5:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col5: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL6:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col6 in 
	(select replace(replace(col6,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col6:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col6: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL7:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col7 in 
	(select replace(replace(col7,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col7:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col7: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL8:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col8 in 
	(select replace(replace(col8,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col8:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col8: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL9:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col9 in 
	(select replace(replace(col9,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col9:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col9: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


//
//  COL10:
//
delete from cr_financial_reports_run
 where report_id = :i_report_id	and col10 in 
	(select replace(replace(col10,'{',''),'}','') from cr_financial_reports_setup
	  where report_id = :i_report_id and remove_row = 1);

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", "ERROR: in wf_delete_remove_row deleting from " + &
	//	"cr_financial_reports_run for col10:~n~n" + sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: in wf_delete_remove_row deleting from " + &
		"cr_financial_reports_run for col10: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return
end if


end subroutine

public function longlong uf_update_temp_table_from_cols ();longlong num_rows, i, col_str_id, the_level, t, array_cell
string sqls, col_val, col_val_without_squigglies, total_temp_tables[]
datawindow d
//  STAND-ALONE ONLY: VARIABLES
w_cr_financial_reports_build w
w = w_cr_financial_reports_build


d = w.dw_cr_financial_reports_columns_grid
//  Need to re-retrieve since the_level was just updated by the caller !
d.RETRIEVE(i_report_id)


//
//  Need the structure_id (if one exists) for the report columns.
//
col_str_id = 0
select columns_structure_id into :col_str_id from cr_financial_reports
 where report_id = :i_report_id;
if isnull(col_str_id) or col_str_id = 0 then
	//
	//  No structure selected ... exit out of this function leaving the detail values in the
	//  temp table.
	//
	return 1
end if


num_rows = d.RowCount()


//*****************************************************************************************
//
//  LOOP OVER THE LIST OF TEMP TABLES (MORE THAN ONE WHEN RUNNING WITH DOLLAR COLUMNS).
//  PERFORM THE UPDATES FOR EACH TEMP TABLE.
//
//*****************************************************************************************

//  There may be temp tables in both the i_temp_tables array and the i_temp_tables_or
//  array.  Build a local array of all temp tables and use it for this loop.
array_cell = 0
for t = 1 to upperbound(i_temp_tables)
	array_cell++
	total_temp_tables[array_cell] = i_temp_tables[t]
next

for t = 1 to upperbound(i_temp_tables_or)
	array_cell++
	total_temp_tables[array_cell] = i_temp_tables_or[t]
next


for t = 1 to upperbound(total_temp_tables)

i_temp_table = total_temp_tables[t]

//SetMicroHelp("Updating temp table " + i_temp_table + " ...")
f_pp_msgs("Updating temp table " + i_temp_table + " ... at " + string(now()))




//
//  Note, the column levels have already been added into the temp table and the levels field
//  in cr_financial_reports_columns was already updated in the Build One button.
//
//  The the_level field is used to differentiate different column values in the report that
//  contain the same companies.  For example, consider a report with columns:
//  {Oper Cos}  {Non-Reg Cos}  {Consolidated Parent}  and consider a company like
//  United Electric Company.  Clearly, that company will fall under both {Oper Cos} AND
//  the {Consolidated Parent}.  The the_level field keeps these records segregated in the
//  temp table since {Consolidated Parent} will have a the_level = 1 and {Oper Cos}
//  will have a the_level = 2.
//
for i = 1 to num_rows
	
	col_val   = d.GetItemString(i, "column_value")
	the_level = d.GetItemNumber(i, "the_level")
	
	if isnull(col_val) then
		//messagebox("Build One", &
		//	"ERROR: cr_financial_reports_columns.column_value is NULL for row " + string(i) + ":~n~n", + &
		//		Exclamation!)
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: cr_financial_reports_columns.column_value is NULL for row " + string(i) + "!")
		f_pp_msgs("  ")
		return -1
	end if
	
	if isnull(the_level) then
		//messagebox("Build One", &
		//	"ERROR: cr_financial_reports_columns.the_level is NULL for " + col_val + ":~n~n", + &
		//		Exclamation!)
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: cr_financial_reports_columns.the_level is NULL for " + col_val + "!")
		f_pp_msgs("  ")
		return -1
	end if
	
	col_val_without_squigglies = f_replace_string(col_val, "{", "", "all")
	col_val_without_squigglies = f_replace_string(col_val_without_squigglies, "}", "", "all")
	
	if i_use_new_structures_table = "YES" then
		//
		//  THIS SQL WORKED AND WAS TESTED EXTENSIVELY, BUT ORACLE DID NOT PERFORM WELL WITH
		//  THE_LEVEL JOINED INTO THE INNER SUB-SELECT.  IN FACT, THE_LEVEL IS BEING USED TO
		//  RESTRICT THE RECORDS BEING UPDATED IN I_TEMP_TABLE, SO IT IS NOT CLEAR THAT WE
		//  EVEN NEED IT IN THE INNER SUB-SELECT.  IT WILL RESTRICT JUST AS WELL IN THE
		//  OUTER WHERE CLAUSE THAT IS PART OF THE UPDATE ITSELF.  ALSO, PLACING IT IN THE
		//  OUTER WHERE WILL PROBABLY TRIGGER THE USE OF A COMPOSITE INDEX, AND AT THE VERY
		//  LEAST, WILL REDUCE THE NUMBER OF ROWS BEING EVALUATED IN A FULL TABLE SCAN OF
		//  THE TEMP TABLE.
		//
		//  FURTHERMORE, IT IS NOT AT ALL OBVIOUS WHY THE UPDATE WAS NOT NULLING OUT OTHER
		//  RECORDS IN THE TEMP TABLE THAT CONTAINED THE STRUCTURE VALUES BUT HAD A
		//  DIFFERENT THE_LEVEL VALUE ... BUT IT DIDN'T.  THE NEW CONSTRUCT MAKES IT MUCH
		//  MORE OBVIOUS THAT THE CORRECT RECORDS ARE BEING UPDATED.
		//
//		sqls = &
//			"update " + i_temp_table + " set col_element_value = '" + col_val_without_squigglies + "' " + &
//			 "where col_element_value in ( " + &
//				"select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) " + &
//				  "from cr_structure_values2 " + &
//				 "where structure_id = " + string(col_str_id) + " and detail_budget = 1 and status = 1 " + &
//				  " and the_level = " + string(the_level) + " " + &
//				 "start with element_value = '" + col_val_without_squigglies + &
//				 "' and structure_id = " + string(col_str_id) + " " + &
//				 "connect by prior element_value = parent_value and structure_id = " + string(col_str_id) + ") "
		sqls = &
			"update " + i_temp_table + " set col_element_value = '" + col_val_without_squigglies + "' " + &
			 "where the_level = " + string(the_level) + " and col_element_value in ( " + &
				"select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) " + &
				  "from cr_structure_values2 " + &
				 "where structure_id = " + string(col_str_id) + " and detail_budget = 1 and status = 1 " + &
				 "start with element_value = '" + col_val_without_squigglies + &
				 "' and structure_id = " + string(col_str_id) + " " + &
				 "connect by prior element_value = parent_value and structure_id = " + string(col_str_id) + ") "
	else
		//
		//  SEE COMMENT ABOVE.
		//
//		sqls = &
//			"update " + i_temp_table + " set col_element_value = '" + col_val_without_squigglies + "' " + &
//			 "where col_element_value in ( " + &
//				"select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) " + &
//				  "from cr_structure_values " + &
//				 "where structure_id = " + string(col_str_id) + " and detail_budget = 1 and status = 1 " + &
//				 " and the_level = " + string(the_level) + " " + &
//				 "start with element_value = '" + col_val_without_squigglies + &
//				 "' and structure_id = " + string(col_str_id) + " " + &
//				 "connect by prior value_id = rollup_value_id and structure_id = " + string(col_str_id) + ") "
		sqls = &
			"update " + i_temp_table + " set col_element_value = '" + col_val_without_squigglies + "' " + &
			 "where the_level = " + string(the_level) + " and col_element_value in ( " + &
				"select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) " + &
				  "from cr_structure_values " + &
				 "where structure_id = " + string(col_str_id) + " and detail_budget = 1 and status = 1 " + &
				 "start with element_value = '" + col_val_without_squigglies + &
				 "' and structure_id = " + string(col_str_id) + " " + &
				 "connect by prior value_id = rollup_value_id and structure_id = " + string(col_str_id) + ") "
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		//messagebox("Build One", "ERROR: updating table " + i_temp_table + ":~n~n" + sqlca.SQLErrText, &
		//	Exclamation!)
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating table " + i_temp_table + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	else
		commit;
	end if
	
next  //  for i = 1 to num_rows



//*****************************************************************************************
//
//  END OF THIS OUTER LOOP:
//  LOOP OVER THE LIST OF TEMP TABLES (MORE THAN ONE WHEN RUNNING WITH DOLLAR COLUMNS).
//  PERFORM THE UPDATES FOR EACH TEMP TABLE.
//
//*****************************************************************************************
next  //  for t = 1 to upperbound(total_temp_tables) ...


return 1
end function

public function longlong uf_start ();longlong num_reports_to_build, i, build_in_progress, rtn, counter
string sqls, rpt_descr
w_cr_financial_reports_build w
w = w_cr_financial_reports_build


//
//  SETUP:  None at the moment ...
//


//  System switch for the new structure values table.
setnull(i_use_new_structures_table)
select upper(rtrim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(rtrim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"


//
//  ARE THERE ANY ROWS IN CR_FINANCIAL_REPORTS_BUILD FOR THIS USER ?  IF SO, THEY HAVE
//  ALREADY INSERTED A REPORT OR REPORTS TO BE BUILT (ONLINE, SELECTIVE BUILD).  ADHERE
//  TO THEIR WISHES.  IF THERE ARE NO REPORTS IN THE TABLE FOR THIS USER, THEY WANT TO
//  REBUILD ALL (NEED TO INSERT ALL REPORTS FOR THEM).
//
//  AT THIS POINT, WE ARE SIMPLY DETERMINING WHICH REPORTS THE USER HAS REQUESTED TO
//  BUILD.  LATER, IN THE LOOP, WE WILL DETERMINE IF ANY OTHER USER IS ALREADY BUILDING
//  THE SAME REPORT.
//
num_reports_to_build = 0
select count(*) into :num_reports_to_build from cr_financial_reports_build
 where users = :i_calling_user_id and session_id = :i_calling_session_id;
if isnull(num_reports_to_build) then num_reports_to_build = 0

//f_pp_msgs("DEBUG: i_calling_user_id = " + i_calling_user_id)
//f_pp_msgs("DEBUG: i_calling_session_id = " + string(i_calling_session_id))
//f_pp_msgs("DEBUG: num_reports_to_build = " + string(num_reports_to_build))

if num_reports_to_build = 0 then
	//  They want to rebuild all ... insert all report_ids.
	insert into cr_financial_reports_build
		(report_id, users, session_id, build_in_progress)
			(select report_id, :i_calling_user_id, :i_calling_session_id, 0
				from cr_financial_reports);
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: inserting into cr_financial_reports_build (all report_id values): " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	else
		commit;
	end if
end if


//
//  LOOP OVER THE REPORTS THAT THIS USER WANTS TO BUILD AND FIRE THE LOGIC.
//
datastore ds_reports_to_build
ds_reports_to_build = CREATE datastore

sqls = "select * from cr_financial_reports_build " + &
        "where users = '" + i_calling_user_id + &
		  "' and session_id = " + string(i_calling_session_id)

f_create_dynamic_ds(ds_reports_to_build, "grid", sqls, sqlca, true)

num_reports_to_build = ds_reports_to_build.RowCount()

i_num_reports_to_build = num_reports_to_build
i_num_errors           = 0

for i = 1 to num_reports_to_build
	
	if i = 1 then
		f_pp_msgs("  ")
		f_pp_msgs("*******************************************")
	end if
	
	i_report_id = ds_reports_to_build.GetItemNumber(i, "report_id")
	
	setnull(rpt_descr)
	select description into :rpt_descr from cr_financial_reports
	 where report_id = :i_report_id;
	if isnull(rpt_descr) then rpt_descr = ""
	
	f_pp_msgs("Started report_id: " + string(i_report_id) + " (" + rpt_descr + ") at " + &
		string(now()))
	
	//  Is any other user building this report currently ?  We are interrogating at this
	//  point instead of relying on what was returned in the datastore (just in case some
	//  user has jumped in front of us for this report.
	build_in_progress = 0
	select count(*) into :build_in_progress from cr_financial_reports_build
	 where report_id = :i_report_id and build_in_progress = 1;
	if isnull(build_in_progress) then build_in_progress = 0
	
	if build_in_progress <> 0 then
		//  If someone else is building, skip this report.
		f_pp_msgs("----- THIS REPORT IS BEING BUILT BY ANOTHER USER.  SKIPPING TO THE NEXT REPORT.")
		continue
	else
		//  Mark the report that we are about to build so no one else can build it.
		update cr_financial_reports_build set build_in_progress = 1
		 where report_id = :i_report_id and users = :i_calling_user_id 
		   and session_id = :i_calling_session_id;
		if sqlca.SQLCode < 0 then
			i_num_errors++
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_financial_reports_build.build_in_progress to 1: " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			continue  //  To the next report ...
		end if
	end if
	
	
	//  Retrieve DW's that are on the window.
	w.dw_cr_financial_reports_columns_grid.RETRIEVE(i_report_id)
	w.dw_cr_financial_reports_setup_grid.RETRIEVE(i_report_id)
	w.dw_cr_financial_reports_edit.RETRIEVE(i_report_id)
	
	
	//  Build the report.
	rtn = uf_build_one()
	
	if rtn <> 1 then
		i_num_errors++
		//  Let uf_build_one() handle the error messages.
		rollback;
		f_pp_msgs("*******************************************")
		continue  //  To the next report ...
	end if
	
	
	//  Update cr_financial_reports_build.build_in_progress to 0.
	update cr_financial_reports_build set build_in_progress = 0
	 where report_id = :i_report_id and users = :i_calling_user_id 
	   and session_id = :i_calling_session_id;
	if sqlca.SQLCode < 0 then
		i_num_errors++
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating cr_financial_reports_build.build_in_progress to 0: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		continue  //  To the next report ...
	end if
	
	
	f_pp_msgs("Finished report_id: " + string(i_report_id) + " (" + rpt_descr + ") at " + &
		string(now()))
	f_pp_msgs("*******************************************")
	
next


//
//  DELETE THE RECORDS FROM CR_FINANCIAL_REPORTS_BUILD NO THAT THEY HAVE BEEN BUILT.
//
delete from cr_financial_reports_build
 where users = :i_calling_user_id and session_id = :i_calling_session_id;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_financial_reports_build: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;
end if


//
//  FOR PERFORMANCE:  TRUNC THE TABLE IF THERE ARE NO RECORDS.  THERE SHOULD BE ONLY A
//  SPLIT-SECOND OF OPPORTUNITY FOR ERROR BETWEEN AND COUNT AND THE TRUNCATE IF ANOTHER
//  USER INSERTS RECORDS.  AND IN GENERAL, THERE WILL NOT BE A LARGE NUMBER OF USERS
//  INSERTING INTO THIS TABLE ANYWAY.
//
counter = 0
select count(*) into :counter from cr_financial_reports_build;
if isnull(counter) then counter = 0

if counter = 0 then
	sqlca.truncate_table('cr_financial_reports_build');
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: truncating cr_financial_reports_build: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	else
		commit;
	end if
end if

return 1

end function

public function longlong uf_zero_line_item (longlong a_id_counter, longlong a_header_row, string a_col_val_for_insert, string a_val_without_squigglies, longlong a_mn, longlong a_sort_order, string a_dollar_type, longlong a_dollar_type_sort_order, longlong a_setup_id);longlong mo, mo_jan
string sqls

if upper(a_dollar_type) = "CY MONTHLY ACTIVITY" or &
	upper(a_dollar_type) = "CY MONTHLY BALANCES"    &
then
	//  Need an insert for each month in the "monthly" report.  The dollar_type will
	//  be the mo variable.  The string(a_mn) should still be OK for the month_number
	//  in cr_financial_reports_run.  The dollar_type_sort_order should also be
	//  the mo variable.
	mo_jan = long(left(string(a_mn), 4) + "01")
	for mo = mo_jan to a_mn
		sqls = &
			"insert into cr_financial_reports_run " + &
				"(report_id, id, " + &
				 "header_row, " + &
				  i_col_col + " column_value, " + &
				 "column_sort_order, month_number, amount, " + &
				 "sort_order, dollar_type, " + &
				 "dollar_type_sort_order, setup_id) " + &
			"(select " + string(i_report_id) + ", " + string(a_id_counter) + ", " + &
					string(a_header_row) + ", " + &
					a_col_val_for_insert + " '" + a_val_without_squigglies + "', " + &
					"-1, " + string(a_mn) + ", 0, " + &
					string(a_sort_order) + ", '" + string(mo) + "', " + &
					string(mo) + ", " + string(a_setup_id) + " " + &
				"from dual)"
		
		if isnull(sqls) then
			f_pp_msgs("  ")
			//messagebox("Build", "ERROR: zero line item sqls is NULL:~n~n", Exclamation!)
			f_pp_msgs("ERROR: zero line item sqls is NULL!")
			f_pp_msgs("  ")
			rollback;
			uf_drop()
			return -1
		end if
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			//messagebox("Build", "ERROR: inserting zero line item into cr_financial_reports_run(1):~n~n" + &
			//	sqlca.SQLErrText, Exclamation!)
			f_pp_msgs("ERROR: inserting zero line item into cr_financial_reports_run(1): " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			uf_drop()
			return -1
		end if
		
	next
else
	sqls = &
		"insert into cr_financial_reports_run " + &
			"(report_id, id, " + &
			 "header_row, " + &
			  i_col_col + " column_value, " + &
			 "column_sort_order, month_number, amount, " + &
			 "sort_order, dollar_type, " + &
			 "dollar_type_sort_order, setup_id) " + &
		"(select " + string(i_report_id) + ", " + string(a_id_counter) + ", " + &
				string(a_header_row) + ", " + &
				a_col_val_for_insert + " '" + a_val_without_squigglies + "', " + &
				"-1, " + string(a_mn) + ", 0, " + &
				string(a_sort_order) + ", '" + a_dollar_type + "', " + &
				string(a_dollar_type_sort_order) + ", " + string(a_setup_id) + " " + &
			"from dual)"
end if

//  In these cases, return 1.  The execute was fired above due to monthly looping.
if upper(a_dollar_type) = "CY MONTHLY ACTIVITY" or &
	upper(a_dollar_type) = "CY MONTHLY BALANCES"    &
then return 1

if isnull(sqls) then
	f_pp_msgs("  ")
	//messagebox("Build", "ERROR: zero line item sqls is NULL:~n~n", Exclamation!)
	f_pp_msgs("ERROR: zero line item sqls is NULL!")
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return -1
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build", "ERROR: inserting zero line item into cr_financial_reports_run(1):~n~n" + &
	//	sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: inserting zero line item into cr_financial_reports_run(1): " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return -1
end if

return 1
end function

public function longlong uf_create_temp_table (longlong a_month_number, string a_top_point);longlong structure_id, element_id, element_id_for_cols, rtn, num_levels, &
		 structure_id_for_cols, i, the_level, dollar, t, dollar_type_sort_order, &
		 thousands_flag, num_or, o, structure_id_or, tt_counter, mn, mn2, ix_value
string sqls, element, element_for_cols, sqls2, dollar_type, amount_table, amount_col, &
		 amt_type_str, sqls_or[], sqls_or2[], quantity_col, sqls_or2_helper[], user_id, &
		 ix_tablespace, bv_str
//  STAND-ALONE ONLY: VARIABLES
longlong s_user_info_session_id
w_cr_financial_reports_build w
w = w_cr_financial_reports_build


//SetPointer(hourglass!)


select user into :user_id from dual;
//  STAND-ALONE ONLY: S_USER_INFO DOES NOT EXIST HERE.
select userenv('sessionid') into :s_user_info_session_id from dual;


//  E.G. --- crfinpwrplant4138
if i_dollar_columns = 1 then
	//  1 table per dollar column ...
	sqls = "select * from cr_financial_reports_dol_type where report_id = " + string(i_report_id)
	i_ds_dollar_types.SetSQLSelect(sqls)
	i_ds_dollar_types.RETRIEVE()
	for i = 1 to i_ds_dollar_types.RowCount()
		//  STAND-ALONE ONLY: CHANGED s_user_info.session_id TO s_user_info_session_id
		//  STAND-ALONE ONLY: CHANGED s_user_info.user_id TO lower(user_id)
		i_temp_tables[i] = "crfin" + lower(user_id) + string(s_user_info_session_id) + &
			"_" + string(i)
		dollar_type = i_ds_dollar_types.GetItemString(i, "dollar_type")
		dollar_type_sort_order = i_ds_dollar_types.GetItemNumber(i, "sort_order")
		i_tt_dollar_types[i] = dollar_type
		i_tt_dollar_type_sort_order[i] = dollar_type_sort_order
	next
else
	//  Only 1 table ...
	//  STAND-ALONE ONLY: CHANGED s_user_info.session_id TO s_user_info_session_id
	//  STAND-ALONE ONLY: CHANGED s_user_info.user_id TO lower(user_id)
	i_temp_table = "crfin" + lower(user_id) + string(s_user_info_session_id)
	i_temp_tables[1]     = i_temp_table
	//  These types of reports also have a dollar type (but only 1) ... min for safety.
	setnull(dollar_type)
	select min(dollar_type) into :dollar_type from cr_financial_reports_dol_type
	 where report_id = :i_report_id;
	if isnull(dollar_type) then
		//messagebox("Build One", "No dollar types defined for this report ... defaulting to CY Curr Mo.")
		f_pp_msgs("  ")
		f_pp_msgs("INFORMATION: No dollar types defined for this report ... " + &
			"defaulting to CY Curr Mo.")
		f_pp_msgs("  ")
		dollar_type = "CY Curr Mo"
	end if
	i_tt_dollar_types[1] = dollar_type
	dollar_type_sort_order = 0
	select sort_order into :dollar_type_sort_order from cr_financial_reports_dol_type
	 where report_id = :i_report_id and dollar_type = :dollar_type;
	if isnull(dollar_type_sort_order) then dollar_type_sort_order = 0
	i_tt_dollar_type_sort_order[1] = dollar_type_sort_order
end if


////  Get info about the report's rows.
structure_id = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "rows_structure_id")

element_id = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "rows_from")

setnull(element)
select upper(description) into :element from cr_elements 
 where element_id = :element_id;
if isnull(element) then element = ""

element = f_cr_clean_string(element)


//  Get info about the report's columns.
element_id_for_cols = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "columns_from")

setnull(element_for_cols)
select upper(description) into :element_for_cols from cr_elements 
 where element_id = :element_id_for_cols;
if isnull(element_for_cols) then element_for_cols = ""

element_for_cols = f_cr_clean_string(element_for_cols)


//
//  NOTE:  THIS SQL WILL NOT WORK FOR ASYMMETRIC REPORTS ... SINCE THERE IS NO LINK TO THE
//         TOP POINT, NOTHING COMES BACK IN THE HIERARCHICAL SQL.  HOWEVER, I'M NOT 100%
//         CONFIDIENT IN THE SQL THAT SEEMS TO WORK FOR ASYMMETRIC REPORTS, SO I WILL ONLY
//         FIRE IT IF THIS SQL RETURNS NO LEVELS.
//
//  Determine the structure levels in the "cols".
structure_id_for_cols = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "columns_structure_id")

//******************************************************************************************
//  FOR SOME REASON, THE JOIN DID NOT WORK AT NSTAR USING CR_STRUCTURE_VALUES.  I CHANGED
//  BOTH SQLS SINCE IT SEEMED MORE EFFICIENT TO JUST USE AN IN-LIST.
//******************************************************************************************
//if i_use_new_structures_table = "YES" then
//	sqls = &
//		"select distinct level " + &
//		  "from cr_structure_values2 a, cr_financial_reports_columns b " + &
//		 "where a.structure_id = " + string(structure_id_for_cols) + &
//		  " and b.report_id = " + string(i_report_id) + &
//		  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = replace(replace(b.column_value, '{', ''), '}', '') " + &
//		 "start with a.element_value = '" + a_top_point + "' " + &
//			"and a.structure_id = " + string(structure_id_for_cols) + " " + &
//		"connect by prior a.element_value = a.parent_value " + &
//		    "and a.structure_id = " + string(structure_id_for_cols)
//else
//	sqls = &
//		"select distinct level " + &
//		  "from cr_structure_values a, cr_financial_reports_columns b " + &
//		 "where a.structure_id = " + string(structure_id_for_cols) + &
//		  " and b.report_id = " + string(i_report_id) + &
//		  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = replace(replace(b.column_value, '{', ''), '}', '') " + &
//		 "start with a.element_value = '" + a_top_point + "' " + &
//			"and a.structure_id = " + string(structure_id_for_cols) + " " + &
//		"connect by prior a.value_id = a.rollup_value_id " + &
//		    "and a.structure_id = " + string(structure_id_for_cols)
//end if

if i_use_new_structures_table = "YES" then
	sqls = &
		"select distinct level " + &
		  "from cr_structure_values2 a " + &
		 "where a.structure_id = " + string(structure_id_for_cols) + &
		  " and a.element_value in (select replace(replace(column_value, '{', ''), '}', '') " + &
		  								    " from cr_financial_reports_columns " + &
											" where report_id = " + string(i_report_id) + ") " + &
		 "start with a.element_value = '" + a_top_point + "' " + &
			"and a.structure_id = " + string(structure_id_for_cols) + " " + &
		"connect by prior a.element_value = a.parent_value " + &
		    "and a.structure_id = " + string(structure_id_for_cols)
else
	//  Substring-ing to remove the ", 12345" from the end of the value (the value_id).
	//  substr( replace-stuff, 1, to-the-comma-pos-minus-1
	//  The instr() is "start at the last position and find the first comma".
	sqls = &
		"select distinct level " + &
		  "from cr_structure_values a " + &
		 "where a.structure_id = " + string(structure_id_for_cols) + &
		  " and a.element_value in (" + &
			  "select substr( replace(replace(column_value, '{', ''), '}', ''), " + &
			                 "1, " + &
								  "instr( replace(replace(column_value, '{', ''), '}', '') ,',', -1, 1 ) - 1 ) " + &
		  		" from cr_financial_reports_columns " + &
			  " where report_id = " + string(i_report_id) + ") " + &
		 "start with a.element_value = '" + a_top_point + "' " + &
			"and a.structure_id = " + string(structure_id_for_cols) + " " + &
		"connect by prior a.value_id = a.rollup_value_id " + &
		    "and a.structure_id = " + string(structure_id_for_cols)
end if

i_ds_levels.SetSQLSelect(sqls)
num_levels = i_ds_levels.RETRIEVE()

//
//  IF NO LEVELS FROM ABOVE, THEN WE MUST HAVE AN ASYMMETRIC REPORT.  USE DIFFERENT SQL
//
if num_levels > 0 then goto after_assym_levels

if i_use_new_structures_table = "YES" then
	sqls = &
		"select distinct level " + &
		  "from cr_structure_values2 a " + &
		 "where a.structure_id = " + string(structure_id_for_cols) + &
		  " and a.element_value in (select replace(replace(column_value, '{', ''), '}', '') " + &
		  									 " from cr_financial_reports_columns " + &
											" where report_id = " + string(i_report_id) + ") " + &
		 "start with a.element_value in (select replace(replace(column_value, '{', ''), '}', '') from cr_financial_reports_columns where report_id = " + string(i_report_id) + ") " + &
			"and a.structure_id = " + string(structure_id_for_cols) + " " + &
		"connect by prior a.element_value = a.parent_value " + &
		    "and a.structure_id = " + string(structure_id_for_cols)
else
	sqls = &
		"select distinct level " + &
		  "from cr_structure_values a " + &
		 "where a.structure_id = " + string(structure_id_for_cols) + &
		  " and a.element_value in (select replace(replace(column_value, '{', ''), '}', '') " + &
		  									 " from cr_financial_reports_columns " + &
											" where report_id = " + string(i_report_id) + ") " + &
		 "start with a.element_value in (select replace(replace(column_value, '{', ''), '}', '') from cr_financial_reports_columns where report_id = " + string(i_report_id) + ") " + &
			"and a.structure_id = " + string(structure_id_for_cols) + " " + &
		"connect by prior a.value_id = a.rollup_value_id " + &
		    "and a.structure_id = " + string(structure_id_for_cols)
end if

i_ds_levels.SetSQLSelect(sqls)
num_levels = i_ds_levels.RETRIEVE()

after_assym_levels:

if num_levels <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("WARNING: no levels returned in ds_levels ...")
	f_pp_msgs("  ")
end if

i_ds_levels.SetSort("1 a")
i_ds_levels.Sort()


//*****************************************************************************************
//
//  BEFORE BUILDING THE TEMP TABLES, DETERMINE IF WE HAVE ANY STRUCTURE OVERRIDES IN THE
//  LINE ITEMS.
//
//*****************************************************************************************
sqls = &
	"select distinct a.override_structure_id " + &
	  "from cr_financial_reports_setup a, cr_financial_reports b " + &
	 "where a.report_id = b.report_id and b.report_id = " + string(i_report_id) + " " + &
		"and a.override_structure_id <> b.rows_structure_id"

i_ds_str_override.SetSQLSelect(sqls)
num_or = i_ds_str_override.RETRIEVE()



//*****************************************************************************************
//
//  LOOP OVER THE LIST OF TEMP TABLES (MORE THAN ONE WHEN RUNNING WITH DOLLAR COLUMNS).
//  PERFORM THE UPDATES FOR EACH TEMP TABLE.
//
//*****************************************************************************************
tt_counter = 0

for t = 1 to upperbound(i_temp_tables)

i_temp_table = i_temp_tables[t]

dollar_type = i_tt_dollar_types[t]

choose case upper(dollar_type)
	case "CY CURR MO", "CY ANNUAL", "CY PRIOR MO", "CY CURR QTR", "CY TME", &
		  "PY CURR MO", "PY ANNUAL", "PY PRIOR MO", "PY CURR QTR", "PY TME", &
		  "CY MONTHLY ACTIVITY"
		amount_table = "cr_cost_repository"
		amount_col   = "amount"
		quantity_col = "quantity"
		amt_type_str = " cr.amount_type in (1,3,99) and "
		bv_str       = ""
	case "CY YTD", "CY PRIOR MO YTD", "CY PRIOR DEC", "PY YTD", "PY PRIOR MO YTD", &
		  "CY MONTHLY BALANCES"
		amount_table = "cr_balances"
		amount_col   = "amount"
		quantity_col = "quantity"
		amt_type_str = ""
		bv_str       = ""
	case "CY CURR MO BDG", "CY YTD BDG", "CY ANNUAL BDG", "CY PRIOR MO BDG", &
		  "CY PRIOR MO YTD BDG", "CY CURR QTR BDG", "CY PRIOR DEC BDG", "CY TME BDG", &
		  "CY MONTHLY ACTIVITY BDG", "CY MONTHLY BALANCES BDG", "PY CURR MO BDG", "PY YTD BDG", &
		  "PY ANNUAL BDG", "PY PRIOR MO BDG", "PY PRIOR MO YTD BDG", "PY CURR QTR BDG", &
		  "PY TME BDG"
		amount_table = "cr_budget_data"
		amount_col   = "amount"
		quantity_col = "quantity"
		amt_type_str = ""
		bv_str       = " cr.budget_version = '" + i_default_bv + "' and "
end choose

choose case upper(dollar_type)
	case "CY CURR MO", "CY YTD", "CY CURR MO BDG", "CY YTD BDG"
		mn  = a_month_number
		mn2 = a_month_number
		//  Until we have a cr_balances_bdg table ...
		if upper(dollar_type) = "CY YTD BDG" then
			mn  = long(left(string(a_month_number), 4) + "01")
			mn2 = a_month_number
		end if
	case "CY ANNUAL", "CY MONTHLY ACTIVITY", "CY MONTHLY BALANCES", "CY ANNUAL BDG", &
		  "CY MONTHLY ACTIVITY BDG", "CY MONTHLY BALANCES BDG"
		mn  = long(left(string(a_month_number), 4) + "01")
		mn2 = a_month_number
	case "CY PRIOR MO", "CY PRIOR MO YTD", "CY PRIOR MO BDG", "CY PRIOR MO YTD BDG"
//  NO, SINCE THIS IS CURRENT-YEAR, SIMPLY GO BACK 1 MONTH TO PERIOD 0 FOR CY PRIOR MO.
//		if right(string(a_month_number), 2) = "01" then
//			mn = a_month_number - 89
//		else
//			mn = a_month_number - 1
//		end if
		mn  = a_month_number - 1
		mn2 = a_month_number - 1
	case "CY CURR QTR", "CY CURR QTR BDG"
		choose case right(string(a_month_number), 2)
			case "01", "02", "03"
				mn  = long(left(string(a_month_number), 4) + "01")
			case "04", "05", "06"
				mn  = long(left(string(a_month_number), 4) + "04")
			case "07", "08", "09"
				mn  = long(left(string(a_month_number), 4) + "07")
			case "10", "11", "12"
				mn  = long(left(string(a_month_number), 4) + "10")
			case else
				//  Just in case they run for something like period 13.  Just run as current month.
				mn = a_month_number
		end choose
		mn2 = a_month_number
	case "CY PRIOR DEC", "CY PRIOR DEC BDG"
		mn  = long(left(string(a_month_number - 100), 4) + "12")
		mn2 = long(left(string(a_month_number - 100), 4) + "12")
	case "CY TME", "CY TME BDG"
		choose case right(string(a_month_number), 2)
			case "00"
				//  I guess we'll just take ourselves back 12 months (e.g. 200600 - 99 = 200501)
				//  and also include the period 00 to include BOY adjustments.
				mn  = a_month_number - 99
			case "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11"
				mn  = a_month_number - 99
			case else // month 12 and greater equals the entire year (including adj periods)
				mn  = long(left(string(a_month_number), 4) + "01")
		end choose
		mn2 = a_month_number
	case "PY CURR MO", "PY YTD", "PY CURR MO BDG", "PY YTD BDG"
		mn  = a_month_number - 100
		mn2 = a_month_number - 100
	case "PY ANNUAL", "PY ANNUAL BDG"
		mn  = long(left(string(a_month_number), 4) + "01") - 100
		mn2 = a_month_number - 100
	case "PY PRIOR MO", "PY PRIOR MO YTD", "PY PRIOR MO BDG", "PY PRIOR MO YTD BDG"
		mn  = a_month_number - 101
		mn2 = a_month_number - 101
	case "PY CURR QTR", "PY CURR QTR BDG"
		choose case right(string(a_month_number), 2)
			case "01", "02", "03"
				mn  = long(left(string(a_month_number), 4) + "01") - 100
			case "04", "05", "06"
				mn  = long(left(string(a_month_number), 4) + "04") - 100
			case "07", "08", "09"
				mn  = long(left(string(a_month_number), 4) + "07") - 100
			case "10", "11", "12"
				mn  = long(left(string(a_month_number), 4) + "10") - 100
			case else
				//  Just in case they run for something like period 13.  Just run as current month.
				mn = a_month_number - 100
		end choose
		mn2 = a_month_number - 100
	case "PY TME", "PY TME BDG"
		choose case right(string(a_month_number), 2)
			case "00"
				//  I guess we'll just take ourselves back 12 months (e.g. 200600 - 99 = 200501)
				//  and also include the period 00 to include BOY adjustments.
				mn  = a_month_number - 199
			case "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11"
				mn  = a_month_number - 199
			case else // month 12 and greater equals the entire year (including adj periods)
				mn  = long(left(string(a_month_number), 4) + "01") - 100
		end choose
		mn2 = a_month_number - 100
end choose



//SetMicroHelp("Creating temp table " + i_temp_table + " ...")
f_pp_msgs("Creating temp table " + i_temp_table + " ... at " + string(now()))



//  Build the temp table.
if i_use_new_structures_table = "YES" then
	sqls = &
		"create table " + i_temp_table + " as " + &
			"select structure_id, substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value, parent_value, " + &
					 "rpad(' ',75) as col_element_value, 0 as month_number, 0 as quantity, 0 as amount, " + &
					 "0 as the_level " + &
			  "from cr_structure_values2 " + &
			 "where structure_id = " + string(structure_id) + " and detail_budget = 0 "
	
	if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
	   upper(dollar_type) = "CY MONTHLY BALANCES"    &
	then
		sqls2 = &
				 " " + &
				 "union all " + &
				 " " + &
				"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, a.parent_value, " + &
						 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
						 "cr.month_number, sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
						 "-9999 as the_level " + &
				  "from cr_structure_values2 a, " + amount_table + " cr " + &
				 "where " + bv_str + amt_type_str + " structure_id = " + string(structure_id) + &
				  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
					"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
				  " and a.detail_budget = 1 and status = 1 " + &
				 "group by a.structure_id, a.element_value, a.parent_value, " + &
							 "cr." + '"' + element_for_cols + '"' + ", cr.month_number "
	else
		sqls2 = &
				 " " + &
				 "union all " + &
				 " " + &
				"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, a.parent_value, " + &
						 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
						 string(a_month_number) + ", sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
						 "-9999 as the_level " + &
				  "from cr_structure_values2 a, " + amount_table + " cr " + &
				 "where " + bv_str + amt_type_str + " structure_id = " + string(structure_id) + &
				  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
					"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
				  " and a.detail_budget = 1 and status = 1 " + &
				 "group by a.structure_id, a.element_value, a.parent_value, " + &
							 "cr." + '"' + element_for_cols + '"' + " "
	end if
	
	//  Structure overrides ... place SQL in sqls_or[] array.  The temp table will be
	//  i_temp_table || "or" || the loop variable.
	if num_or > 0 then
		for o = 1 to num_or
			structure_id_or = i_ds_str_override.GetItemNumber(o, 1)
			tt_counter++
			i_temp_tables_or[tt_counter]  = i_temp_table + "or" + string(o)
			i_structure_id_or[tt_counter] = structure_id_or
			sqls_or[o] = &
				"create table " + i_temp_table + "or" + string(o) + " as " + &
				"select structure_id, substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value, parent_value, " + &
						 "rpad(' ',75) as col_element_value, 0 as month_number, 0 as quantity, 0 as amount, " + &
						 "0 as the_level " + &
				  "from cr_structure_values2 " + &
				 "where structure_id = " + string(structure_id_or) + " and detail_budget = 0 "
			
			sqls_or2[o] = ""
			
			if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
				upper(dollar_type) = "CY MONTHLY BALANCES"    &
			then
				sqls_or2_helper[o] = &
					" " + &
					 "union all " + &
					 " " + &
					"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, a.parent_value, " + &
							 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
							 "cr.month_number, sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
							 "-9999 as the_level " + &
					  "from cr_structure_values2 a, " + amount_table + " cr " + &
					 "where " + bv_str + amt_type_str + " structure_id = " + string(structure_id_or) + &
					  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
						"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
					  " and a.detail_budget = 1 and status = 1 " + &
					 "group by a.structure_id, a.element_value, a.parent_value, " + &
								 "cr." + '"' + element_for_cols + '"' + ", cr.month_number "
			else
				sqls_or2_helper[o] = &
					" " + &
					 "union all " + &
					 " " + &
					"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, a.parent_value, " + &
							 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
							 string(a_month_number) + ", sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
							 "-9999 as the_level " + &
					  "from cr_structure_values2 a, " + amount_table + " cr " + &
					 "where " + bv_str + amt_type_str + " structure_id = " + string(structure_id_or) + &
					  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
						"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
					  " and a.detail_budget = 1 and status = 1 " + &
					 "group by a.structure_id, a.element_value, a.parent_value, " + &
								 "cr." + '"' + element_for_cols + '"' + " "
			end if
		next
	end if
	
	for i = 1 to num_levels
		the_level = i_ds_levels.GetItemNumber(i, 1)
		sqls = sqls + f_replace_string(sqls2, "-9999", string(the_level), "all")
		//  For structure overrides ...
		for o = 1 to num_or
			sqls_or2[o] = sqls_or2[o] + &
				f_replace_string(sqls_or2_helper[o], "-9999", string(the_level), "all")
		next
	next
	
else  //  if i_use_new_structures_table = "YES" then ...
	
	//  Don't use the "insert" technique on the summary points !!!  The sqls2 is going to
	//  point explicitly to the summary points as they appear.
	sqls = &
		"create table " + i_temp_table + " as " + &
			"select c.structure_id, c.element_value as element_value, p.element_value as parent_value, " + &
					 "rpad(' ',75) as col_element_value, 0 as month_number, 0 as quantity, 0 as amount, " + &
					 "0 as the_level " + &
			  "from cr_structure_values c, cr_structure_values p " + &
			 "where c.structure_id = " + string(structure_id) + " and c.detail_budget = 0 " + &
			  " and c.rollup_value_id = p.value_id (+) and c.structure_id = p.structure_id (+)"
	
	if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
	   upper(dollar_type) = "CY MONTHLY BALANCES"    &
	then
		sqls2 = &
				 " " + &
				 "union all " + &
				 " " + &
				"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, p.element_value as parent_value, " + &
						 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
						 "cr.month_number, sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
						 "-9999 as the_level " + &
				  "from cr_structure_values a, " + amount_table + " cr, cr_structure_values p " + &
				 "where " + bv_str + amt_type_str + " a.structure_id = " + string(structure_id) + &
				  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
					"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
				  " and a.detail_budget = 1 and a.status = 1 " + &
				  " and a.rollup_value_id = p.value_id and a.structure_id = p.structure_id " + &
				 "group by a.structure_id, a.element_value, p.element_value, " + &
							 "cr." + '"' + element_for_cols + '"' + ", cr.month_number "
	else
		sqls2 = &
				 " " + &
				 "union all " + &
				 " " + &
				"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, p.element_value as parent_value, " + &
						 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
						 string(a_month_number) + ", sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
						 "-9999 as the_level " + &
				  "from cr_structure_values a, " + amount_table + " cr, cr_structure_values p " + &
				 "where " + bv_str + amt_type_str + " a.structure_id = " + string(structure_id) + &
				  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
					"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
				  " and a.detail_budget = 1 and a.status = 1 " + &
				  " and a.rollup_value_id = p.value_id and a.structure_id = p.structure_id " + &
				 "group by a.structure_id, a.element_value, p.element_value, " + &
							 "cr." + '"' + element_for_cols + '"' + " "
	end if
	
	//  Structure overrides ... place SQL in sqls_or[] array.  The temp table will be
	//  i_temp_table || "or" || the loop variable.
	if num_or > 0 then
		for o = 1 to num_or
			structure_id_or = i_ds_str_override.GetItemNumber(o, 1)
			tt_counter++
			i_temp_tables_or[tt_counter]  = i_temp_table + "or" + string(o)
			i_structure_id_or[tt_counter] = structure_id_or
			sqls_or[o] = &
				"create table " + i_temp_table + "or" + string(o) + " as " + &
				"select c.structure_id, c.element_value as element_value, p.element_value as parent_value, " + &
						 "rpad(' ',75) as col_element_value, 0 as month_number, 0 as quantity, 0 as amount, " + &
						 "0 as the_level " + &
				  "from cr_structure_values c, cr_structure_values p " + &
				 "where c.structure_id = " + string(structure_id_or) + " and c.detail_budget = 0 " + &
				  " and c.rollup_value_id = p.value_id (+) and c.structure_id = p.structure_id (+)"
			
			sqls_or2[o] = ""
			
			if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
				upper(dollar_type) = "CY MONTHLY BALANCES"    &
			then
				sqls_or2_helper[o] = &
					" " + &
					 "union all " + &
					 " " + &
					"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, p.element_value as parent_value, " + &
							 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
							 "cr.month_number, sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
							 "-9999 as the_level " + &
					  "from cr_structure_values a, " + amount_table + " cr, cr_structure_values p " + &
					 "where " + bv_str + amt_type_str + " a.structure_id = " + string(structure_id_or) + &
					  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
						"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
					  " and a.detail_budget = 1 and a.status = 1 " + &
					  " and a.rollup_value_id = p.value_id and a.structure_id = p.structure_id " + &
					 "group by a.structure_id, a.element_value, p.element_value, " + &
								 "cr." + '"' + element_for_cols + '"' + ", cr.month_number "
			else
				sqls_or2_helper[o] = &
					" " + &
					 "union all " + &
					 " " + &
					"select a.structure_id, substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) as element_value, p.element_value as parent_value, " + &
							 "cr." + '"' + element_for_cols + '"' + " as col_element_value, " + &
							 string(a_month_number) + ", sum(cr." + quantity_col + "), sum(cr." + amount_col + "), " + &
							 "-9999 as the_level " + &
					  "from cr_structure_values a, " + amount_table + " cr, cr_structure_values p " + &
					 "where " + bv_str + amt_type_str + " a.structure_id = " + string(structure_id_or) + &
					  " and substr(a.element_value, 1, decode(instr(a.element_value, ':'), 0, length(a.element_value) + 1, instr(a.element_value, ':')) - 1) = cr. " + '"' + element + '" ' + &
						"and cr.month_number between " + string(mn) + " and " + string(mn2) + &
					  " and a.detail_budget = 1 and a.status = 1 " + &
					  " and a.rollup_value_id = p.value_id and a.structure_id = p.structure_id " + &
					 "group by a.structure_id, a.element_value, p.element_value, " + &
								 "cr." + '"' + element_for_cols + '"' + " "
			end if
		next
	end if
	
	for i = 1 to num_levels
		the_level = i_ds_levels.GetItemNumber(i, 1)
		sqls = sqls + f_replace_string(sqls2, "-9999", string(the_level), "all")
		//  For structure overrides ...
		for o = 1 to num_or
			sqls_or2[o] = sqls_or2[o] + &
				f_replace_string(sqls_or2_helper[o], "-9999", string(the_level), "all")
		next
	next
end if  //  if i_use_new_structures_table = "YES" then ...


//  Execute the sqls for the basic temp table ...
//  STAND-ALONE ONLY: MUST WRITE SQLS TO THE LOGS OR TEXT LOG
//tab_1.tabpage_setup.mle_temp_table.Text = sqls
if g_debug = 'YES' then
	f_pp_msgs("TEMP TABLE SQLS = " + sqls)
	f_write_log(g_log_file, "TEMP TABLE SQLS = " + sqls)
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	//messagebox("Build", "ERROR: creating table " + i_temp_table + ":~n~n" + sqlca.SQLErrText, &
	//	Exclamation!)
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: creating table " + i_temp_table + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  Add an index to the basic temp table ... the ix name can be a max of 30 characters.
//  It must be unique since multiple users may be building reports at the same time.
setnull(ix_tablespace)
select control_value into :ix_tablespace from cr_system_control
 where lower(control_name) = 'cr indexes tablespace';
if isnull(ix_tablespace) or trim(ix_tablespace) = "" then
	ix_tablespace = "pwrplant_idx"
end if

select crfinrepindexes.nextval into :ix_value from dual;

sqls = "create index crfinrep_" + string(ix_value) + "_ix" + string(t) + " on " + &
	i_temp_table + " (col_element_value, the_level) tablespace pwrplant_idx"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	//messagebox("Build", "ERROR: creating index crfinreptemp1_ix on " + i_temp_table + ":~n~n" + &
	//	sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: creating index crfinreptemp1_ix on " + i_temp_table + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return -1
end if


//  Analyze the temp table ...
//  STAND-ALONE ONLY:
//SetMicroHelp("Analyzing temp table " + i_temp_table + " ...")
f_pp_msgs("Analyzing temp table " + i_temp_table + " ... at " + string(now()))

sqls = "begin dbms_stats.gather_table_stats(ownname=>'" + user_id + &
	"',tabname=>'" + i_temp_table + "',estimate_percent=> 25 , cascade=> TRUE );end;"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	//messagebox("Build", "INFORMATION: dbms_stats.gather_table_stats returned:~n~n" + &
	//	sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("  ")
	f_pp_msgs("INFORMATION: dbms_stats.gather_table_stats returned: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
end if


//  Execute any override sqls ...
for o = 1 to num_or
	//  STAND-ALONE ONLY:
	//SetMicroHelp("Creating temp table " + i_temp_table + "or" + string(o) + " ...")
	f_pp_msgs("Creating temp table " + i_temp_table + "or" + string(o) + " ... at " + string(now()))
	sqls = sqls_or[o] + sqls_or2[o]
	execute immediate :sqls;
	if sqlca.SQLCode < 0 then
		//  STAND-ALONE ONLY: MUST WRITE SQLS TO THE LOGS OR TEXT LOG
		f_write_log(g_log_file, sqls)
		if g_debug = 'YES' then
			f_pp_msgs("TEMP TABLE SQLS (OR" + string(o) + ") = " + sqls)
			f_write_log(g_log_file, "TEMP TABLE SQLS (OR" + string(o) + ") = " + sqls)
		end if
		//messagebox("Build", "ERROR: creating table " + i_temp_table + "or" + string(o) + &
		//	":~n~n" + sqlca.SQLErrText, Exclamation!)
		f_pp_msgs("ERROR: creating table " + i_temp_table + "or" + string(o) + ": " + sqlca.SQLErrText)
		rollback;
		uf_drop()
		return -1
	end if
	//  Analyze the temp table ...
	//  STAND-ALONE ONLY:
	//SetMicroHelp("Analyzing temp table " + i_temp_table + "or" + string(o) + " ...")
	f_pp_msgs("Analyzing temp table " + i_temp_table + "or" + string(o) + " ... at " + string(now()))
	sqls = "begin dbms_stats.gather_table_stats(ownname=>'" + user_id + &
		"',tabname=>'" + i_temp_table + "or" + string(o) + "',estimate_percent=> 25 , cascade=> TRUE );end;"
	execute immediate :sqls;
	if sqlca.SQLCode < 0 then
		//messagebox("Build", "INFORMATION: dbms_stats.gather_table_stats returned:~n~n" + &
		//	sqlca.SQLErrText, Exclamation!)
		f_pp_msgs("  ")
		f_pp_msgs("INFORMATION: dbms_stats.gather_table_stats returned:~n~n" + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
	end if
next


//
//  THOUSANDS FLAG:  If on, divide amount by 1000 ...
//
thousands_flag = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "thousands_flag")

if thousands_flag = 1 then
	//SetMicroHelp("Applying thousands flag ...")
	f_pp_msgs("Applying thousands flag ... at " + string(now()))
	
	sqls = &
		"update " + i_temp_table + " set amount = amount / 1000"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		//messagebox("Build", "ERROR: updating table " + i_temp_table + " (amount / 1000):~n~n" + &
		//	sqlca.SQLErrText, Exclamation!)
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating table " + i_temp_table + " (amount / 1000):~n~n" + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		uf_drop()
		return -1
	end if
	
end if


//
//  NO LONGER CALLING THIS HERE ... THIS WAS THE WRONG PLACE AND METHOD TO CHANGE 
//  SIGNS (AT THE DETAIL POINT LEVEL OF DETAIL) AND WOULD GIVE US BAD RESULTS LATER.
//  INSTEAD, ACCOUNT_TYPE HAS BEEN ADDED TO THE STRUCTURE VALUES TABLE AND WILL BE
//  APPLIED LATER IN THE CODE ... EVEN THE SUMMARY POINTS MUST HAVE ACCOUNT TYPES 
//  TO HAVE THEIR SIGNS CHANGED.  FOR EXAMPLE, ADDING UP ACCOUNTS TO GET NET INCOME
//  WOULD YIELD A NEGATIVE NUMBER (IF ONE IS MAKING MONEY).
//
////  Call the custom function that will change the sign on credit accounts.  We will have
////  to key off of some "account" type field on the ME table, but every client ends up
////  having a different custom field for this (based on their GL usually).
//rtn = f_cr_financial_reports_account_type(i_temp_table, i_report_id, a_month_number, &
//	element_id, element_id_for_cols)
//
//if rtn <> 1 then
//	rollback;
//	wf_drop()
//	return -1
//end if




//*****************************************************************************************
//
//  END OF THIS OUTER LOOP:
//  LOOP OVER THE LIST OF TEMP TABLES (MORE THAN ONE WHEN RUNNING WITH DOLLAR COLUMNS).
//  PERFORM THE UPDATES FOR EACH TEMP TABLE.
//
//*****************************************************************************************
next  //  for t = 1 to upperbound(i_temp_tables) ...




return 1

end function

public function longlong uf_build_one ();//*****************************************************************************************
//
//  Button  :  cb_build_one
//
//  Description  :  Build the data for the selected reports (i.e. process the dollar
//                  amounts and insert records into cr_financial_reports_run).
//
//  Notes:
//  ------
//    The table we process against will be of the form:
//      element_value / parent_value / column_value / amount
//    We will avoid processing directly against cr_cost_repository or other tables.
//
//  Stand-Alone Notes:
//  ------------------
//  1) uf_build_one is called in a report_id loop, so the i_report_id variable is
//     already populated.
//  2) The global datastores used in this function were instance variables in
//     w_cr_financial_reports before porting the code into this stand-alone.  They are
//     still named with an "i_" so we don't have to re-write all the code.
//
//*****************************************************************************************
longlong num_rows, i, sort_order, rtn, id_counter, column_sort_order, num_cols, ii, mn, &
		 columns_structure_id, header_row, t, dollar_type_sort_order, rows_structure_id , &
		 columns_element_id, element_id, counter, num_rows2, iii, array_looper, cell, &
		 array_looper2, empty_array_long[], or_structure_id, o, num_or, loop_start, &
		 formula_id, setup_id, strip_pos, mo, mo_jan, dtl_point
string col_val, sqls, col_val_for_insert, rpt_column_value, val_without_squigglies, &
		 top_point, dollar_type, formula_description, formula_li_array[], &
		 formula_helper_array[], empty_array[], formula_element, amount_name, sqls_helper, &
		 amount_sqls, amount_or_quantity, amount_string_for_run, save_i_temp_table, &
		 element_table, element_column, row_values, column_values, descr_field, msg_string, &
		 formula, col_name, col_name_array[], col_val_for_count, build_cv, &
		 save_i_col_val_for_insert
boolean found_value, bad_formulas, b_dtl_point
datawindow d, d_edit
//  *****  VARIABLES FOR THE STAND-ALONE  *****
w_cr_financial_reports_build w
w = w_cr_financial_reports_build


//  *****  NOT IN THE STAND-ALONE.  *****
//SetPointer(hourglass!)


//  *****  NOT IN THE STAND-ALONE.  *****
////  Needed below.
//setnull(build_cv)
//select upper(trim(control_value)) into :build_cv from cr_system_control
// where upper(trim(control_name)) = 'CR FINREP - BUILD METHOD';
//if isnull(build_cv) or build_cv = "" then build_cv = "WINDOW"


//  *****  NOT IN THE STAND-ALONE.  *****
////
////  They can multi-select and de-select using Ctrl and Shift.  This script
////  needs to have at least one report selected.
////
//if dw_cr_financial_reports_grid_s.GetSelectedRow(0) = 0 then
//	messagebox("Build", "Please select a report.")
//	return
//end if


//  *****  NOT IN THE STAND-ALONE.  *****
////
////  They can multi-select Ctrl and Shift.  If they are not running the stand-alone exe,
////  this script needs to have a single report selected.
////
//if build_cv <> "EXE" then
//	counter = 0
//	for i = 1 to dw_cr_financial_reports_grid_s.RowCount()
//		if dw_cr_financial_reports_grid_s.isselected(i) then counter++
//		if counter > 1 then
//			messagebox("Build", "Please select only one report.")
//			return
//		end if
//	next
//end if


//  *****  NOT IN THE STAND-ALONE.  *****
////
////  First, prompt to save changes ... in case we need code in the next script
////  that relies on the underlying data.
////
//d_edit = tab_1.tabpage_setup.dw_cr_financial_reports_edit
//
//d_edit.AcceptText()
//
//if (d_edit.ModifiedCount() > 0 &
//or  d_edit.DeletedCount()  > 0) &
//and tab_1.tabpage_setup.cb_update_setup.Enabled then
//	
//	rtn = messagebox("Cancel", "Save Changes ?", Question!, YesNo!)
//	
//	if rtn = 1 then tab_1.tabpage_setup.cb_update_setup.TriggerEvent(clicked!)
//	
//	if rtn = 2 then
//		return
//	end if
//	
//end if


//  *****  NOT IN THE STAND-ALONE.  *****
////
////  Prompt the user.
////
//rtn = messagebox("Build", "You are about to rebuild the data for the selected report.~n~n" + &
//	"Continue ?", Question!, YesNo!, 1)
//
//if rtn <> 1 then return


//  *****  NOT IN THE STAND-ALONE.  *****
////
////  Have them enter the month number ...
////
//OpenWithParm(w_cr_financial_reports_mn_prompt, i_month_number)
//
//mn = message.DoubleParm
//
//if mn = 0 then
//	messagebox("Build", "Cannot build ... 0 returned as the month number.")
//	return
//end if



//
//  LOOP OVER THE START AND END MONTH AND BUILD REPORTS FOR THOSE MONTH NUMBERS.
//
for mn = i_start_month to i_end_month


//
//  Change the instance variable ...
//
i_month_number = mn


//  *****  NOT IN THE STAND-ALONE.  *****
////
////  This looks like a good place to slot in the check to determine if this code or
////  the stand-alone will run.
////
//if build_cv = "EXE" then
//	wf_exe()
//	return
//end if


//
//  What needs to be NULLed out so a prior Build One on a different report
//  does not foul up a subsequent report.
//
i_tt_dollar_type_sort_order[] = empty_array_long
i_structure_id_or[]           = empty_array_long
i_temp_tables[]               = empty_array
i_temp_tables_or[]            = empty_array
i_tt_dollar_types[]           = empty_array


//
//  Re-Retrieve the invisible DW's that keep track of the row and column setup.
//
w.dw_cr_financial_reports_setup_grid.RETRIEVE(i_report_id)
w.dw_cr_financial_reports_columns_grid.RETRIEVE(i_report_id)


//
//  Need this for later when building temp table(s) ...
//
i_dollar_columns = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "dollar_columns")


//
//  Update the levels in the columns table.  This will help us determine how much data to
//  insert into the temp table.  Each column level needs to have the raw data inserted
//  so it can be totaled up to the points in that level ... think consolidated
//  financials where the company columns add up to a consolidate total column at the
//  far right side of the report.  That total is simply a summary point 1 level up
//  from the sub companies.
//
columns_structure_id = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "columns_structure_id")

columns_element_id = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "columns_from")

element_id = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "rows_from")

rows_structure_id = &
	w.dw_cr_financial_reports_edit.GetItemNumber(1, "rows_structure_id")

row_values = &
	w.dw_cr_financial_reports_edit.GetItemString(1, "row_values")

column_values = &
	w.dw_cr_financial_reports_edit.GetItemString(1, "column_values")



//
//  AUDITS:  Important audits to prevent build errors ...
//


//  ***  LINE ITEMS MISSING FROM THE STRUCTURE:

sqls = &
	"select col1 from cr_financial_reports_setup " + &
	" where report_id = 4 and col1 like '{%' " + &
	" union " + &
	"select col2 from cr_financial_reports_setup " + &
	" where report_id = 4 and col2 like '{%' " + &
	" union " + &
	"select col3 from cr_financial_reports_setup " + &
	" where report_id = 4 and col3 like '{%' " + &
	" union " + &
	"select col4 from cr_financial_reports_setup " + &
	" where report_id = 4 and col4 like '{%' " + &
	" union " + &
	"select col5 from cr_financial_reports_setup " + &
	" where report_id = 4 and col5 like '{%' " + &
	" union " + &
	"select col6 from cr_financial_reports_setup " + &
	" where report_id = 4 and col6 like '{%' " + &
	" union " + &
	"select col7 from cr_financial_reports_setup " + &
	" where report_id = 4 and col7 like '{%' " + &
	" union " + &
	"select col8 from cr_financial_reports_setup " + &
	" where report_id = 4 and col8 like '{%' " + &
	" union " + &
	"select col9 from cr_financial_reports_setup " + &
	" where report_id = 4 and col9 like '{%' " + &
	" union " + &
	"select col10 from cr_financial_reports_setup " + &
	" where report_id = 4 and col10 like '{%' " + &
	"   minus   " + &
	"select '{'||element_value||'}' from "

if i_use_new_structures_table = "YES" then
	sqls = sqls + "cr_structure_values2"
else
	sqls = sqls + "cr_structure_values"
end if


i_ds_generic_string_audit.SetSQLSelect(sqls)
num_rows = i_ds_generic_string_audit.RETRIEVE()

if num_rows > 0 then
	msg_string = ""
	for i = 1 to num_rows
		if i <> 1 then
			msg_string = msg_string + " --- "
		end if
		msg_string = msg_string + i_ds_generic_string_audit.GetItemString(i, 1)
	next
	f_pp_msgs("  ")
	f_pp_msgs("AUDIT ERROR: Cannot build ... the following values in the line item definitions " + &
		"no longer exist in structure_id " + string(rows_structure_id) + ": " + &
		msg_string)
	f_pp_msgs("  ")
	return -1
end if



//  ***  COLUMN DEFS MISSING FROM THE STRUCTURE:

if i_use_new_structures_table = "YES" then
	sqls = &
		"select column_value from cr_financial_reports_columns " + &
		 "where report_id = " + string(i_report_id) + &
		" minus " + &
		"select '{'||element_value||'}' from cr_structure_values2 " + &
		" where structure_id = " + string(columns_structure_id)
else
//	sqls = &
//		"select substr( column_value, 1, instr( replace(replace(column_value, '{', ''), '}', '') ,',', -1, 1 ) ) || '}' from cr_financial_reports_columns " + &
//		 "where report_id = " + string(i_report_id) + &
//		" minus " + &
//		"select '{'||element_value||'}' from cr_structure_values " + &
//		" where structure_id = " + string(columns_structure_id)
	sqls = &
		"select column_value from cr_financial_reports_columns " + &
		 "where report_id = " + string(i_report_id) + &
		" minus " + &
		"select '{'||element_value||'}' from cr_structure_values " + &
		" where structure_id = " + string(columns_structure_id)
end if

i_ds_generic_string_audit.SetSQLSelect(sqls)
num_rows = i_ds_generic_string_audit.RETRIEVE()

if num_rows > 0 then
	msg_string = ""
	for i = 1 to num_rows
		if i <> 1 then
			msg_string = msg_string + " --- "
		end if
		msg_string = msg_string + i_ds_generic_string_audit.GetItemString(i, 1)
	next
	f_pp_msgs("  ")
	f_pp_msgs("Cannot build ... the following values in the column definitions " + &
		"no longer exist in structure_id " + string(columns_structure_id) + ": " + &
		msg_string)
	f_pp_msgs("  ")
	return -1
end if


//  ***  FORMULAS IN THE LINE ITEMS THAT DO NOT EXIST (MAY HAVE BEEN DELETED):.

sqls = &
	"select col1 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col1 like '<%' " + &
	" union " + &
	"select col2 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col2 like '<%' " + &
	" union " + &
	"select col3 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col3 like '<%' " + &
	" union " + &
	"select col4 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col4 like '<%' " + &
	" union " + &
	"select col5 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col5 like '<%' " + &
	" union " + &
	"select col6 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col6 like '<%' " + &
	" union " + &
	"select col7 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col7 like '<%' " + &
	" union " + &
	"select col8 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col8 like '<%' " + &
	" union " + &
	"select col9 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col9 like '<%' " + &
	" union " + &
	"select col10 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col10 like '<%' " + &
	" minus " + &
	"select '<'||description||'>' from cr_financial_reports_formulas " + &
	 "where report_id = " + string(i_report_id)

i_ds_generic_string_audit.SetSQLSelect(sqls)
num_rows = i_ds_generic_string_audit.RETRIEVE()

if num_rows > 0 then
	msg_string = ""
	for i = 1 to num_rows
		if i <> 1 then
			msg_string = msg_string + " --- "
		end if
		msg_string = msg_string + i_ds_generic_string_audit.GetItemString(i, 1)
	next
	f_pp_msgs("  ")
	f_pp_msgs("Cannot build ... the following formulas do not exist in this " + &
		"report's formula definitions: " + &
		msg_string)
	f_pp_msgs("  ")
	return -1
end if


//  ***  FORMULAS REFERENCING LINE ITEMS THAT NO LONGER EXIST:

sqls = &
	"select col1 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col1 like '<%' " + &
	" union " + &
	"select col2 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col2 like '<%' " + &
	" union " + &
	"select col3 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col3 like '<%' " + &
	" union " + &
	"select col4 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col4 like '<%' " + &
	" union " + &
	"select col5 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col5 like '<%' " + &
	" union " + &
	"select col6 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col6 like '<%' " + &
	" union " + &
	"select col7 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col7 like '<%' " + &
	" union " + &
	"select col8 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col8 like '<%' " + &
	" union " + &
	"select col9 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col9 like '<%' " + &
	" union " + &
	"select col10 from cr_financial_reports_setup " + &
	 "where report_id = " + string(i_report_id) + " and col10 like '<%' "
	

i_ds_formula_audit1.SetSQLSelect(sqls)
num_rows = i_ds_formula_audit1.RETRIEVE()

//  Loop over any formulas found in this report's line items.
bad_formulas = false
msg_string   = ""

for i = 1 to num_rows
	
	formula_description = i_ds_formula_audit1.GetItemString(i, 1)
	
	//  Get this formula's formula_id.
	sqls = "select * from cr_financial_reports_formulas " + &
			  "where '<'||description||'>' = '" + formula_description + "' " + &
			    "and report_id = " + string(i_report_id)
	
	i_ds_formula_audit2.SetSQLSelect(sqls)
	num_rows2 = i_ds_formula_audit2.RETRIEVE()
	
	formula_id = i_ds_formula_audit2.GetItemNumber(1, "formula_id")
	formula    = i_ds_formula_audit2.GetItemString(1, "description")
	
	//  Are any of this formula's elements missing from the line items ?
	sqls = "select formula_element from cr_financial_reports_formulas2 " + &
			  "where formula_element like '{%' " + &
				 "and formula_id = " + string(formula_id) + " " + &
			  "minus (" + &
			 "select col1||', col1' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			 " union " + &
			 "select col2||', col2' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col3||', col3' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col4||', col4' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col5||', col5' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col6||', col6' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col7||', col7' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col8||', col8' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col9||', col9' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			  " union " + &
			 "select col10||', col10' from cr_financial_reports_setup " + &
			  "where report_id = " + string(i_report_id) + &
			         ")"
	
	i_ds_formula_audit3.SetSQLSelect(sqls)
	num_rows2 = i_ds_formula_audit3.RETRIEVE()
	
	//  Loop to show the message.
	if num_rows2 > 0 then
		bad_formulas = true
		
		for ii = 1 to num_rows2
			msg_string = msg_string + "formula = " + formula + "; element = " + &
				i_ds_formula_audit3.GetItemString(ii, 1) + ", "
		next
	end if
	
next  //  for i = 1 to num_rows ...

if bad_formulas then
	f_pp_msgs("  ")
	f_pp_msgs("Cannot build ... the following formulas have invalid elements:" + &
				" " + msg_string)
	f_pp_msgs("  ")
	return -1
end if

//
//  END OF:
//  AUDITS:  Important audits to prevent build errors ...
//



//
//  Update the_level ...
//
if i_use_new_structures_table = "YES" then
	//
	//  Get the toplevel point so the update below uses the entire structure (you don't
	//  know what levels they have selected).
	//
	setnull(top_point)
	select element_value into :top_point from cr_structure_values2
	 where structure_id = :columns_structure_id and parent_value = 'CRTOPLEVEL'
	 	and status = 1;
	if isnull(top_point) then top_point = ""
//	//
//	//  Update the levels.  DID NOT WORK WITHOUT THE TOP POINT IN THE COLUMNS !!!
//	//
//	update cr_financial_reports_columns cr set the_level = (
//	select level
//	  from cr_structure_values2 a, cr_financial_reports_columns b
//	 where a.structure_id = :columns_structure_id and b.report_id = :i_report_id
//		and a.element_value = replace(replace(b.column_value, '{', ''), '}', '')
//		and cr.column_value = '{'||a.element_value||'}'
//	 start with a.element_value = :top_point and a.structure_id = :columns_structure_id
//	connect by prior a.element_value = a.parent_value and a.structure_id = :columns_structure_id)
//	where report_id = :i_report_id;
////
////  THIS WORKED WITH ALL SYMMETRIC REPORTS ... HOWEVER, IT ENDED UP PUTTING THE NATURAL
////  LEVEL IN ASYMMETRIC REPORTS INSTEAD OF THE RELATIVE LEVEL BASED ON THE POINTS SELECTED
////  FOR THE REPORT.
////
//	update cr_financial_reports_columns cr set the_level = (
//		select the_level from (
//			select element_value, level as the_level
//			  from cr_structure_values2 a
//			 where a.structure_id = :columns_structure_id
//			 start with a.element_value = :top_point
//				and a.structure_id = :columns_structure_id
//		  connect by prior a.element_value = a.parent_value 
//				and a.structure_id = :columns_structure_id) l
//		 where cr.column_value = '{'||substr(l.element_value, 1, decode(instr(l.element_value, ':'), 0, length(l.element_value) + 1, instr(l.element_value, ':')) - 1)||'}')
//	 where report_id = :i_report_id;
//
//  THIS SHOULD WORK WITH BOTH TYPES OF REPORTS ... CHANGED THE "START WITH" CLAUSE
//  TO NOT START AT THE TOPLEVEL.  WE NEED MAX(LEVEL) SINCE THE SYMMETRIC REPORTS WILL
//  END UP WITH DUPLICATION DUE THE THE IN-LIST "START WITH" CLAUSE.
//
	update cr_financial_reports_columns cr set the_level = (
		select the_level from (
			select element_value, max(level) as the_level
			  from cr_structure_values2 a
			 where a.structure_id = :columns_structure_id
			 start with a.element_value in 
			 			(select replace(replace(column_value, '{', ''), '}', '') 
						 	from cr_financial_reports_columns where report_id = :i_report_id)
				and a.structure_id = :columns_structure_id
		  connect by prior a.element_value = a.parent_value 
				and a.structure_id = :columns_structure_id group by element_value) l
		 where cr.column_value = '{'||l.element_value||'}')
	 where report_id = :i_report_id;
	//
	//  THIS USED TO READ (NEXT TO LAST LINE):
	//  where cr.column_value = '{'||substr(l.element_value, 1, decode(instr(l.element_value, ':'), 0, length(l.element_value) + 1, instr(l.element_value, ':')) - 1)||'}')
	//  It also read that way in the "else" block.  I think I outsmarted myself here.  First
	//  it bombed at NStar on their companies that looked like '002: NStar'.  And then it bombed
	//  during my own testing.  The "columns" table is as-is, so I change the where clause
	//  to just join on '{'||l.element_value||'}'
	//
	
	//
	//  Error Check.
	//
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating cr_financial_reports_columns.the_level: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
else
	//
	//  Get the toplevel point so the update below uses the entire structure (you don't
	//  know what levels they have selected).
	//
	setnull(top_point)
	select element_value into :top_point from cr_structure_values
	 where structure_id = :columns_structure_id and rollup_value_id = 0
	 	and status = 1;
	if isnull(top_point) then top_point = ""
	//	//
	//	//  Update the levels.  DID NOT WORK WITHOUT THE TOP POINT IN THE COLUMNS !!!
	//	//
//	update cr_financial_reports_columns cr set the_level = (
//		select the_level from (
//			select element_value, value_id, level as the_level
//			  from cr_structure_values a
//			 where a.structure_id = :columns_structure_id
//			 start with a.element_value = :top_point
//				and a.structure_id = :columns_structure_id
//		  connect by prior a.value_id = a.rollup_value_id
//				and a.structure_id = :columns_structure_id) l
//		 where cr.column_value = '{'||l.element_value||'}')
//	 where report_id = :i_report_id;
	////
	////  COMMENT LIKE THE SECTION ABOVE:
	////  THIS WORKED WITH ALL SYMMETRIC REPORTS ... HOWEVER, IT ENDED UP PUTTING THE NATURAL
	////  LEVEL IN ASYMMETRIC REPORTS INSTEAD OF THE RELATIVE LEVEL BASED ON THE POINTS SELECTED
	////  FOR THE REPORT.
	////
	
	//
	//  THIS SHOULD WORK WITH BOTH TYPES OF REPORTS ... CHANGED THE "START WITH" CLAUSE
	//  TO NOT START AT THE TOPLEVEL.  WE NEED MAX(LEVEL) SINCE THE SYMMETRIC REPORTS WILL
	//  END UP WITH DUPLICATION DUE THE THE IN-LIST "START WITH" CLAUSE.
	//
	update cr_financial_reports_columns cr set the_level = (
		select the_level from (
			select element_value, max(level) as the_level
			  from cr_structure_values a
			 where a.structure_id = :columns_structure_id
			 start with a.element_value in 
			 			(select replace(replace(column_value, '{', ''), '}', '') 
						 	from cr_financial_reports_columns where report_id = :i_report_id)
				and a.structure_id = :columns_structure_id
		  connect by prior a.value_id = a.rollup_value_id
				and a.structure_id = :columns_structure_id group by element_value) l
		 where cr.column_value = '{'||l.element_value||'}')
	 where report_id = :i_report_id;
	//
	//  Error Check.
	//
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating cr_financial_reports_columns.the_level: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if


//
//  Create the temp table with the summarized $$$ ...
//
rtn = uf_create_temp_table(mn, top_point)

if rtn <> 1 then return -1


//
//  Update the "col_element_value" field using the cr_financial_reports_columns data.
//
//SetMicroHelp("Updating temp table " + i_temp_table + " with column structure data ...")
f_pp_msgs("Updating temp table " + i_temp_table + " with column structure data ... at " + string(now()))

rtn = uf_update_temp_table_from_cols()

if rtn <> 1 then
	uf_drop()
	return -1
end if





//
//  AS IT TURNS OUT, THIS WILL NOT WORK IN ALL CASES.  FOR EXAMPLE, IF SOMEONE WROTE A
//  REPORT WITH THE PARENT COMPANY, ALL CONSOLS UNDER THAT PARENT COMPANY, AND ONE OF 
//  THOSE CONSOLS INDIVIDUAL COMPANIES (I.E. THEY WANT TO SEE COMPANY DETAIL UNDER ONE
//  OF THEIR CHILD CONSOLIDATED LEVELS).  SINCE THE TOPLEVEL COMPANY POINT WOULD BE IN
//  A REPORT LIKE THIS, ALL INDIVIDUAL COMPANIES END UP IN STRUCTURE SQL BELOW AND NOTHING
//  GETS DELETED.  WE WILL SIMPLY RUN A DELETE AT THE END OF THIS SCRIPT TO CLEAN UP
//  CR_FINANCIAL_REPORTS_RUN (WHICH BY THE WAY IS MUCH EASIER).
//
////
////  At this point, we have all data in the temp table.  In other words, there has been
////  no restriction of data based on the column values in the report definition.  For
////  example --- let's say that the user created an income statement to pull some 
////  subsidiary companies and their consolidated total.  Without this delete, all companies
////  would creep into the report.  This delete will remove any col_element_value values
////  that do not pass the cr_financial_reports_columns definition.
////
//for t = 1 to upperbound(i_temp_tables)
//	
//	i_temp_table = i_temp_tables[t]
//	
//	SetMicroHelp("Deleting extra data from temp table " + i_temp_table + " ...")
//	
//	sqls = &
//		"delete from " + i_temp_table + " " + &
//		" where col_element_value not in " + &
//				" ( " + &
//				"select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) from cr_structure_values2 " + &
//				" where structure_id = " + string(columns_structure_id) + " " + &
//				"   and detail_budget = 1 and status = 1 " + &
//				" start with '{'||element_value||'}' in " + &
//					"(select column_value from cr_financial_reports_columns " + &
//					  "where report_id = " + string(i_report_id) + ") " + &
//						" and structure_id = " + string(columns_structure_id) + " " + &
//				" connect by prior element_value = parent_value " + &
//					"  and structure_id = " + string(columns_structure_id) + " " + &
//				" ) " + &
//		"   and col_element_value in " + &
//				"(select element_value from cr_structure_values2 " + &
//				  "where structure_id = " + string(columns_structure_id) + " and detail_budget = 1) "
//	
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode < 0 then
//		messagebox("Build", "ERROR: deleting excess data from " + i_temp_table + ":~n~n" + &
//			sqlca.SQLErrText, Exclamation!)
//		rollback;
//		return -1
//	end if
//	
//next  //  for t = 1 to upperbound(i_temp_tables) ...




//
//  The temp table is in good shape ... delete the "run" records for this report before
//  re-inserting.
//
f_pp_msgs("Deleting the prior report results ... at " + string(now()))

delete from cr_financial_reports_run 
 where report_id = :i_report_id and month_number = :mn;

if sqlca.SQLCode < 0 then
	//messagebox("Build", "ERROR: deleting from cr_financial_reports_run:~n~n" + sqlca.SQLErrText, &
	//	Exclamation!)
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: deleting from cr_financial_reports_run: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return -1
end if


//
//  Get the number of setup rows for this report.  We will loop over those rows and
//  process the $$$ and structure information.
//
f_pp_msgs("Processing line items ... at " + string(now()))

d = w.dw_cr_financial_reports_setup_grid
num_rows = d.RowCount()

num_cols = w.dw_cr_financial_reports_columns_grid.RowCount()




// **************  LOOP OVER EACH TEMP TABLE AND PERFORM THE "FOR I" LOOP  ****************
for t = 1 to upperbound(i_temp_tables)
	
i_temp_table = i_temp_tables[t]

dollar_type = i_tt_dollar_types[t]

dollar_type_sort_order = i_tt_dollar_type_sort_order[t]




id_counter = 0

//
//  START OF:
//  Applying each Line Item in the setup table here.
//
for i = 1 to num_rows
	
	//
	//  Hardcoded GetItems ...
	//
	sort_order      = d.GetItemNumber(i, "sort_order")
	header_row      = d.GetItemNumber(i, "header_row")
	or_structure_id = d.GetItemNumber(i, "override_structure_id")
	amount_or_quantity = upper(d.GetItemString(i, "amount_or_quantity"))
	setup_id        = d.GetItemNumber(i, "id")
	dtl_point       = d.GetItemNumber(i, "dtl_point")
	
	if isnull(dtl_point) then dtl_point = 0
	if dtl_point = 1 then
		b_dtl_point = true
	else
		b_dtl_point = false
	end if
	
	if isnull(sort_order) then sort_order = 0
	if isnull(header_row) then header_row = 0
	id_counter++
	
	if amount_or_quantity = "QUANTITY" then
		amount_string_for_run = "sum(quantity)"
	else
		amount_string_for_run = "sum(amount)"
	end if
	
	//
	//  What column is populated for this row?
	//
	col_val = uf_get_col_val(i)
		
	//
	//  Do we insert a blank line item or process a structure point?
	//
	//  Insert -1 as column_sort_order ... it is updated below.
	//
	if left(i_col_val_for_insert, 1) = "{" then
		col_val_for_insert   = f_replace_string(col_val, "{", "", "all")
		col_val_for_insert   = f_replace_string(col_val_for_insert, "}", "", "all")
		i_col_val_for_insert = f_replace_string(i_col_val_for_insert, "{", "", "all")
		i_col_val_for_insert = f_replace_string(i_col_val_for_insert, "}", "", "all")
		
		save_i_temp_table = i_temp_table
		
		//  The number of cells in i_temp_tables corresponds to the number of dollar types.
		//  The number of cells in i_temp_tables_or is the number of distinct override structure
		//  ids times then number of temp tables.  Divide to get the number of disinct structure
		//  overrides.
		num_or = upperbound(i_temp_tables_or) / upperbound(i_temp_tables)
		
		if or_structure_id <> rows_structure_id then
			//  We've got ourselves an override!
			//  First, interrogate 2 instance arrays to determine which OR temp table to use.
			//  There could be multiples if there were overrides with different OR
			//  structure ids.
			//
			//  i_structure_id_or is basically in sections (1 per temp table - that is 1 per
			//  dollar column).  Use a formula to determine where to start in the array.
			//
			loop_start = ((t - 1) * num_or) + 1
			for o = loop_start to upperbound(i_structure_id_or)
				if i_structure_id_or[o] = or_structure_id then
					//  Change the i_temp_table ...
					i_temp_table = i_temp_tables_or[o]
					exit
				end if
			next
		end if
		
		//  If a detail_point line item was selected from the structure, the SQL below will not
		//  work without stripping off the label.  This is because the detail point records in
		//  the temp table are in their natural state (i.e. with actual account values and such).
		//  Summary points work fine without stripping the label because the temp table contains
		//  the entire element_value from the structure (including the label).  There is no
		//  "natural state" for summary point data like there is for detail point data.
		if b_dtl_point then
			save_i_col_val_for_insert = i_col_val_for_insert
			select substr(:i_col_val_for_insert, 
							  1, 
							  decode(instr(:i_col_val_for_insert, ':'), 
										0, length(:i_col_val_for_insert) + 1, 
										instr(:i_col_val_for_insert, ':')) - 1)
			  into :i_col_val_for_insert
			  from dual;
		end if
		
		//  Note: there is a comma at the end of i_col_col and col_val.
		//        col_val and col_val_for_insert has single-quotes around the values.
		if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
			upper(dollar_type) = "CY MONTHLY BALANCES"    &
		then
			//  The               temp_table.month_number needs to be inserted into the 
			//      cr_financial_reports_run.dollar_type
			//
			//  The cr_financial_reports_run.month_number cannot come from
			//                    temp_table.month_number (it contains 12 months!)
			//  Instead, hardcode it from the input argument.
			//
			//  The cr_financial_reports_run.dollar_type_sort_order needs to be the
			//                    temp_table.month_number so the months sort accross the top.
			sqls = &
				"insert into cr_financial_reports_run " + &
					"(report_id, id, " + &
					 "header_row, " + &
					  i_col_col + " column_value, " + &
					 "column_sort_order, month_number, amount, " + &
					 "sort_order, dollar_type, " + &
					 "dollar_type_sort_order, setup_id) " + &
				"(select " + string(i_report_id) + ", " + string(id_counter) + ", " + &
							string(header_row) + ", " + &
							col_val_for_insert + " col_element_value, " + &
							"-1, " + string(i_month_number) + ", " + amount_string_for_run + ", " + &
							string(sort_order) + ", to_char(month_number), " + &
							"month_number, " + string(setup_id) + " " + &
					"from " + i_temp_table + " where trim(col_element_value) is not null " + &
				  "start with element_value = '" + i_col_val_for_insert + "' " + &
				  "connect by prior element_value = parent_value " + &
				  "group by col_element_value, month_number)"
		else
			sqls = &
				"insert into cr_financial_reports_run " + &
					"(report_id, id, " + &
					 "header_row, " + &
					  i_col_col + " column_value, " + &
					 "column_sort_order, month_number, amount, " + &
					 "sort_order, dollar_type, " + &
					 "dollar_type_sort_order, setup_id) " + &
				"(select " + string(i_report_id) + ", " + string(id_counter) + ", " + &
							string(header_row) + ", " + &
							col_val_for_insert + " col_element_value, " + &
							"-1, month_number, " + amount_string_for_run + ", " + &
							string(sort_order) + ", '" + dollar_type + "', " + &
							string(dollar_type_sort_order) + ", " + string(setup_id) + " " + &
					"from " + i_temp_table + " where trim(col_element_value) is not null " + &
				  "start with element_value = '" + i_col_val_for_insert + "' " + &
				  "connect by prior element_value = parent_value " + &
				  "group by col_element_value, month_number)"
		end if
		
		if b_dtl_point then
			i_col_val_for_insert = save_i_col_val_for_insert
		end if
		
		if isnull(sqls) then
			f_pp_msgs("  ")
			//messagebox("Build", "ERROR: line item sqls is NULL (1):~n~n", Exclamation!)
			f_pp_msgs("ERROR: line item sqls is NULL (1)!")
			f_pp_msgs("  ")
			rollback;
			uf_drop()
			return -1
		end if
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			//messagebox("Build", "ERROR: inserting into cr_financial_reports_run(1):~n~n" + sqlca.SQLErrText, &
			//	Exclamation!)
			f_pp_msgs("ERROR: inserting into cr_financial_reports_run(1): " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			uf_drop()
			return -1
		end if
		
		//  The "mod" was added because "Dollar Column" reports can have a partial number of
		//  records inserted (1 per company/"column" that has data).  In that case, we need to
		//  insert a zero dollar record for the other companies/"columns".
		if sqlca.SQLNRows = 0 or &
			(i_dollar_columns = 1 and mod(sqlca.SQLNRows, num_cols) <> 0) &
		then
			//  We have a line item that does not have any transaction data rolling up to it.
			//  Insert it with zeros as the amount so it still shows up on the report.  Note
			//  that the for ii loop is exactly the same as in the "else" below for header
			//  line items.
			for ii = 1 to num_cols
				rpt_column_value = w.dw_cr_financial_reports_columns_grid.GetItemString(ii, "column_value")
				val_without_squigglies = f_replace_string(rpt_column_value, "{", "", "all")
				val_without_squigglies = f_replace_string(val_without_squigglies, "}", "", "all")
				//  We now have to perform a count due to the addition in the "if" clause 
				//  above (the mod() addtion).  We will potentially come rumbling through here
				//  when some, but not all, of the companies/"columns" have this line item already
				//  inserted.  Count to determine which case we have.  First, get rid of any
				//  quotes and such from col_val_for_insert (looks like "'Purchased Power', "
				//  to start).  COL_VAL_FOR_INSERT IS AN ARGUMENT TO WF_ZERO_LINE_ITEM ...
				//  COL_VAL_FOR_COUNT IS JUST FOR THE COUNT(*) !!!
				col_val_for_count = ""
				select substr(:col_val_for_insert, 2, length(:col_val_for_insert) - 4)
				  into :col_val_for_count from dual;
				
				if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
					upper(dollar_type) = "CY MONTHLY BALANCES"    &
				then
					//  Cannot evaluate by dollar_type ... must evaluate for all months instead.
					//  Technically, we will then have some "partial" records --- e.g. we have
					//  data in Jan, Mar, and Sep, but not the other months.  I think this will be
					//  OK since that is how non-Dollar-Column reports work anyway.  I am really just
					//  trying to catch cases where something like Puchased Power is completely
					//  missing for the whole year for a company/"column".
					mo_jan = long(left(string(mn), 4) + "01")
					counter = 0
					select count(*) into :counter from cr_financial_reports_run 
					 where report_id = :i_report_id
						and header_row = :header_row
						and (  col1  = :col_val_for_count // A line item, e.g. Purchased Power
							 or col2  = :col_val_for_count or col3  = :col_val_for_count
							 or col4  = :col_val_for_count or col5  = :col_val_for_count
							 or col6  = :col_val_for_count or col7  = :col_val_for_count
							 or col8  = :col_val_for_count or col9  = :col_val_for_count
							 or col10 = :col_val_for_count  )
						and column_value = :val_without_squigglies // The true column definitions
						and month_number = :mn
						and dollar_type between to_char(:mo_jan) and to_char(:mo);
					if isnull(counter) or counter = 0 then
						rtn = uf_zero_line_item(id_counter, header_row, col_val_for_insert, &
							val_without_squigglies, mn, sort_order, dollar_type, dollar_type_sort_order, &
							setup_id)
						if rtn <> 1 then return -1
					end if
				else
					counter = 0
					select count(*) into :counter from cr_financial_reports_run 
					 where report_id = :i_report_id
						and header_row = :header_row
						and (  col1  = :col_val_for_count // A line item, e.g. Purchased Power
							 or col2  = :col_val_for_count or col3  = :col_val_for_count
							 or col4  = :col_val_for_count or col5  = :col_val_for_count
							 or col6  = :col_val_for_count or col7  = :col_val_for_count
							 or col8  = :col_val_for_count or col9  = :col_val_for_count
							 or col10 = :col_val_for_count  )
						and column_value = :val_without_squigglies // The true column definitions
						and month_number = :mn
						and dollar_type = :dollar_type;
					if isnull(counter) or counter = 0 then
						rtn = uf_zero_line_item(id_counter, header_row, col_val_for_insert, &
							val_without_squigglies, mn, sort_order, dollar_type, dollar_type_sort_order, &
							setup_id)
						if rtn <> 1 then return -1
					end if
				end if
				
			next
		end if
		
		//  But the temp table back (in the case where an override changed it).
		i_temp_table = save_i_temp_table
		
		//
		//  Update the (line_item) sort_order ...
		//
		
	else
		//
		//  No structure ... insert a blank line item.  Must insert once per column value.
		//
		//  Leave amount null.  Since there are no squigglies on col_val, just use that
		//  for the i_col_col (e.g. "col1").
		//		
		for ii = 1 to num_cols
			
			rpt_column_value = w.dw_cr_financial_reports_columns_grid.GetItemString(ii, "column_value")
			
			//  WE DON'T WANT TO STRIP THIS OFF OF COLUMNS ...
//			select substr(:rpt_column_value, 1, decode(instr(:rpt_column_value, ':'), 0, length(:rpt_column_value) + 1, instr(:rpt_column_value, ':')) - 1)
//			  into :rpt_column_value from dual;
			
			val_without_squigglies = f_replace_string(rpt_column_value, "{", "", "all")
			val_without_squigglies = f_replace_string(val_without_squigglies, "}", "", "all")
			
			//  Note: there is a comma at the end of i_col_col and col_val.
			if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
				upper(dollar_type) = "CY MONTHLY BALANCES"    &
			then
				//  Need an insert for each month in the "monthly" report.  The dollar_type will
				//  be the mo variable.  The string(mn) should still be OK for the month_number
				//  in cr_financial_reports_run.  The dollar_type_sort_order should also be
				//  the mo variable.
				mo_jan = long(left(string(i_month_number), 4) + "01")
				for mo = mo_jan to i_month_number
					sqls = &
						"insert into cr_financial_reports_run " + &
							"(report_id, id, " + &
							 "header_row, " + &
							  i_col_col + " column_value, " + &
							 "column_sort_order, month_number, sort_order, dollar_type, " + &
							 "dollar_type_sort_order, setup_id) " + &
						"(select " + string(i_report_id) + ", " + string(id_counter) + ", " + &
									string(header_row) + ", " + &
									col_val + " '" + val_without_squigglies + "', " + &
									"-1, " + string(mn) + ", " + string(sort_order) + ", '" + string(mo) + "', " + &
									string(mo) + ", " + string(setup_id) + " " + &
							"from dual)"
					
					if isnull(sqls) then
						f_pp_msgs("  ")
						//messagebox("Build", "ERROR: line item sqls is NULL (2):~n~n", Exclamation!)
						f_pp_msgs("ERROR: line item sqls is NULL (2)!")
						f_pp_msgs("  ")
						rollback;
						uf_drop()
						return -1
					end if
					
					execute immediate :sqls;
					
					if sqlca.SQLCode < 0 then
						f_pp_msgs("  ")
						//messagebox("Build", "ERROR: inserting into cr_financial_reports_run(2):~n~n" + &
						//	sqlca.SQLErrText, Exclamation!)
						f_pp_msgs("ERROR: inserting into cr_financial_reports_run(2): " + &
							sqlca.SQLErrText)
						f_pp_msgs("  ")
						rollback;
						uf_drop()
						return -1
					end if
					
				next
			else
				sqls = &
					"insert into cr_financial_reports_run " + &
						"(report_id, id, " + &
						 "header_row, " + &
						  i_col_col + " column_value, " + &
						 "column_sort_order, month_number, sort_order, dollar_type, " + &
						 "dollar_type_sort_order, setup_id) " + &
					"(select " + string(i_report_id) + ", " + string(id_counter) + ", " + &
								string(header_row) + ", " + &
								col_val + " '" + val_without_squigglies + "', " + &
								"-1, " + string(mn) + ", " + string(sort_order) + ", '" + dollar_type + "', " + &
								string(dollar_type_sort_order) + ", " + string(setup_id) + " " + &
						"from dual)"
			end if
			
			//  In these cases, go to the next value in the "for ii" loop.  The execute
			//  was fired above due to monthly looping.
			if upper(dollar_type) = "CY MONTHLY ACTIVITY" or &
				upper(dollar_type) = "CY MONTHLY BALANCES"    &
			then continue
			
			if isnull(sqls) then
				f_pp_msgs("  ")
				//messagebox("Build", "ERROR: line item sqls is NULL (2):~n~n", Exclamation!)
				f_pp_msgs("ERROR: line item sqls is NULL (2)!")
				f_pp_msgs("  ")
				rollback;
				uf_drop()
				return -1
			end if
			
			execute immediate :sqls;
			
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				//messagebox("Build", "ERROR: inserting into cr_financial_reports_run(2):~n~n" + &
				//	sqlca.SQLErrText, Exclamation!)
				f_pp_msgs("ERROR: inserting into cr_financial_reports_run(2): " + &
					sqlca.SQLErrText)
				f_pp_msgs("  ")
				rollback;
				uf_drop()
				return -1
			end if
			
		next
	end if
	
	commit;
next  //  for i = 1 to num_rows ...
//
//  END OF:
//  Applying each Line Item in the setup table here.
//




// **************  END OF:                                                 ****************
// **************  LOOP OVER EACH TEMP TABLE AND PERFORM THE "FOR I" LOOP  ****************
next  //  for t = 1 to upperbound(i_temp_tables) ...




//
//  Delete any column_value values that are not in the report definition.  See the
//  commented out code above for details.
//
f_pp_msgs("Deleting records where column_value is not in the report definition ... at " + string(now()))

//delete from cr_financial_reports_run 
// where '{'||column_value||'}' in
//		(
//		select '{'||column_value||'}'
//		  from cr_financial_reports_run 
//		 where report_id = :i_report_id and month_number = :mn
//		 minus
//		select column_value from cr_financial_reports_columns 
//		 where report_id = :i_report_id
//		)
//	and month_number = :mn;
//delete from cr_financial_reports_run 
// where column_value in
//		(
//		select column_value
//		  from cr_financial_reports_run 
//		 where report_id = :i_report_id and month_number = :mn
//		 minus
//		select replace(replace(
//					 substr(column_value, 1, decode(instr(column_value, ':'), 
//															  0, length(column_value) + 1, 
//															  instr(column_value, ':')) - 1)
//				 , '{', ''), '}', '')
//		  from cr_financial_reports_columns 
//		 where report_id = :i_report_id
//		)
//	and month_number = :mn;
delete from cr_financial_reports_run 
 where column_value in
		(
		select column_value
		  from cr_financial_reports_run 
		 where report_id = :i_report_id and month_number = :mn
		 minus
		select replace(replace(
					 column_value
				 , '{', ''), '}', '')
		  from cr_financial_reports_columns 
		 where report_id = :i_report_id
		)
	and month_number = :mn;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build", "ERROR: deleting excess data from cr_financial_reports_run:~n~n" + &
	//	sqlca.SQLErrText, Exclamation!)
	f_pp_msgs("ERROR: deleting excess data from cr_financial_reports_run: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return -1
end if

commit;



//  MOVED WF_DELETE_REMOVE_ROW() FROM THIS SPOT TO THE BOTTOM OF THE SCRIPT.
//  ROWS WERE BEING DELETED BEFORE FORMULAS COULD BE COMPUTED AGINST THEM.



//
//  Update the amount's signs based on the account type ... For now I am limiting this
//  section to reports that have Account as line items.  It's not obvious what to do about
//  reports that are of the form Cost Center x Cost Element.
//
//  This section updates where the ROWS are the Account Field !!!
//

//  Is this an Account structure (ROWS)?
counter = 0
select count(*) into :counter
  from cr_structures a, cr_elements b, cr_system_control c
 where a.structure_id = :rows_structure_id
	and a.element_id = b.element_id
	and upper(replace(b.description, '_', ' ')) = upper(replace(c.control_value, '_', ' '))
	and upper(c.control_name) = 'FERC ACCOUNT FIELD';
if isnull(counter) then counter = 0

if counter = 0 then
	goto after_amount_type_update_rows
end if

f_pp_msgs("Updating dollar signs (ROWS) ... at " + string(now()))

//  Loop over each of the 10 columns and run the appropriate update ... after this code
//  we call a custom function just in case we need funny business to get this right.
for i = 1 to 10
	
	if i_use_new_structures_table = "YES" then
		sqls = &
			"update cr_financial_reports_run a set a.amount = ( " + &
				"select a.amount * c.amount_sign from cr_structure_values2 b, cr_account_type c " + &
				 "where a.col" + string(i) + " = b.element_value " + &
					"and a.report_id = " + string(i_report_id) + " " + &
					"and a.month_number = " + string(mn) + " " + &
					"and b.structure_id = " + string(rows_structure_id) + " " + &
					"and b.account_type = c.account_type) " + &
			 "where a.report_id = " + string(i_report_id) + " " + &
				"and a.month_number = " + string(mn) + " " + &
				"and a.col" + string(i) + " in " + &
					"(select element_value from cr_structure_values2 " + &
					  "where structure_id = " + string(rows_structure_id) + ")"
	else
		sqls = &
			"update cr_financial_reports_run a set a.amount = ( " + &
				"select a.amount * c.amount_sign from cr_structure_values b, cr_account_type c " + &
				 "where a.col" + string(i) + " = b.element_value " + &
					"and a.report_id = " + string(i_report_id) + " " + &
					"and a.month_number = " + string(mn) + " " + &
					"and b.structure_id = " + string(rows_structure_id) + " " + &
					"and b.account_type = c.account_type) " + &
			 "where a.report_id = " + string(i_report_id) + " " + &
				"and a.month_number = " + string(mn) + " " + &
				"and a.col" + string(i) + " in " + &
					"(select element_value from cr_structure_values " + &
					  "where structure_id = " + string(rows_structure_id) + ")"
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		//messagebox("Build One", &
		//	"ERROR: updating cr_financial_reports_run.amount * amount_sign (ROWS):~n~n" + &
		//		sqlca.SQLErrText, Exclamation!)
		f_pp_msgs( &
			"ERROR: updating cr_financial_reports_run.amount * amount_sign (ROWS): " + &
				sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		uf_drop()
		return -1
	end if
	
next


//  The label is here since we may want to handle "accounts as columns" report in
//  the custom function.
after_amount_type_update_rows:


//
//  Update the amount's signs based on the account type ... For now I am limiting this
//  section to reports that have Account as columns.  It's not obvious what to do about
//  reports that are of the form Cost Center x Cost Element.
//
//  This section updates where the COLUMNS are the Account Field !!!
//

//  The columns_structure_id was retrieved above ...

//  Is this an Account structure (COLUMNS)?
counter = 0
select count(*) into :counter
  from cr_structures a, cr_elements b, cr_system_control c
 where a.structure_id = :columns_structure_id
	and a.element_id = b.element_id
	and upper(replace(b.description, '_', ' ')) = upper(replace(c.control_value, '_', ' '))
	and upper(c.control_name) = 'FERC ACCOUNT FIELD';
if isnull(counter) then counter = 0

if counter = 0 then
	goto after_amount_type_update_cols
end if

f_pp_msgs("Updating dollar signs (COLUMNS) ... at " + string(now()))

//  We do not need a loop as we did with the rows update ... run a single update and
//  after this code we call a custom function just in case we need funny business to 
//  get this right.
if i_use_new_structures_table = "YES" then
	sqls = &
		"update cr_financial_reports_run a set a.amount = ( " + &
			"select a.amount * c.amount_sign from cr_structure_values2 b, cr_account_type c " + &
			 "where a.column_value = b.element_value " + &
				"and a.report_id = " + string(i_report_id) + " " + &
				"and a.month_number = " + string(mn) + " " + &
				"and b.structure_id = " + string(columns_structure_id) + " " + &
				"and b.account_type = c.account_type) " + &
		 "where a.report_id = " + string(i_report_id) + " " + &
			"and a.month_number = " + string(mn) + " " + &
			"and a.column_value in " + &
				"(select element_value from cr_structure_values2 " + &
				  "where structure_id = " + string(columns_structure_id) + ")"
else
	sqls = &
		"update cr_financial_reports_run a set a.amount = ( " + &
			"select a.amount * c.amount_sign from cr_structure_values b, cr_account_type c " + &
			 "where a.column_value = b.element_value " + &
				"and a.report_id = " + string(i_report_id) + " " + &
				"and a.month_number = " + string(mn) + " " + &
				"and b.structure_id = " + string(columns_structure_id) + " " + &
				"and b.account_type = c.account_type) " + &
		 "where a.report_id = " + string(i_report_id) + " " + &
			"and a.month_number = " + string(mn) + " " + &
			"and a.column_value in " + &
				"(select element_value from cr_structure_values " + &
				  "where structure_id = " + string(columns_structure_id) + ")"
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build One", &
	//	"ERROR: updating cr_financial_reports_run.amount * amount_sign (COLUMNS):~n~n" + &
	//		sqlca.SQLErrText, Exclamation!)
	f_pp_msgs( &
		"ERROR: updating cr_financial_reports_run.amount * amount_sign (COLUMNS): " + &
			sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return -1
end if


//  The label is here since we may want to handle "accounts as columns" report in
//  the custom function.
after_amount_type_update_cols:


//  Call the custom function ... THIS SHOULD ONLY BE USED IF THE ACCOUNT TYPE SETTINGS
//  IN THE STRUCTURE VALUES CANNOT GET THE ANSWER RIGHT.
rtn = f_cr_financial_reports_account_type(i_report_id, mn, &
	element_id, columns_element_id)

if rtn <> 1 then
	//  You need to handle the error message in the function.
	rollback;
	uf_drop()
	return -1
end if




commit;




//
//  Apply LINE ITEM FORMULAS ... note that we placed this AFTER the amount sign update.
//
f_pp_msgs("Applying line item formulas ... at " + string(now()))

for i = 1 to 10
	
	//  Get the distinct formulas in this column.
	sqls = &
		"select distinct replace(replace(col" + string(i) + ",'<',''),'>','') " + &
		 " from cr_financial_reports_run " + &
		" where report_id = " + string(i_report_id) + " and month_number = " + string(mn) + &
	     " and substr(col" + string(i) + ",1,1) = '<'"
	
	i_ds_formulas.SetSQLSelect(sqls)
	num_rows = i_ds_formulas.RETRIEVE()
	
	//  Loop over each formula.
	for ii = 1 to num_rows
		
		formula_description = i_ds_formulas.GetItemString(ii, 1)
		
		//  Get the formula line items.
		sqls = &
			"select * from cr_financial_reports_formulas2 " + &
			" where formula_id in " + &
				" (select formula_id from cr_financial_reports_formulas " + &
				  " where report_id = " + string(i_report_id) + &
				    " and description = '" + formula_description + "') " + & 
			" order by sort_order"
			
		i_ds_formulas2.SetSQLSelect(sqls)
		num_rows2 = i_ds_formulas2.RETRIEVE()
		
		//  If they have a formula specified in the line items, but there is no formula data,
		//  the execute at the end of this loop will bomb.  Check for this condition.
		if num_rows2 <= 0 then
			f_pp_msgs("  ")
			//messagebox("Build", &
			//	"WARNING: formula: " + formula_description + ": does not have any setup data.  This " + &
			//		"formula will not be computed.")
			f_pp_msgs( &
				"WARNING: formula: " + formula_description + ": does not have any setup data.  This " + &
					"formula will not be computed.")
			f_pp_msgs("  ")
			continue //  Just continue to the next one since we've warned them ...
		end if
		
		//  Just in case ...
		i_ds_formulas2.SetSort("sort_order a")
		i_ds_formulas2.Sort()
		
		//  Start by building arrays that will help us later.
		formula_li_array     = empty_array
		formula_helper_array = empty_array
		
		//  Loop over each line item in this formula.
		for iii = 1 to num_rows2
			
			formula_element = i_ds_formulas2.GetItemString(iii, "formula_element")
			
			//  Only need the helper arrays for the meat of the formula.
			if left(formula_element, 1) = "{" then
				//  7/10/06: Need to strip off the ", col1" off of the end now that we are saving
				//           it in the formula_element (to build formulas in columns other than
				//           the formula_element's column.
				//           First, get the col_name before we destroy the original formula_element.
				strip_pos = pos(formula_element, "}, col")
				if right(formula_element, 2) = "10" then
					col_name = "col10"
				else
					col_name = right(formula_element, 4)
				end if
				formula_element = left(formula_element, strip_pos)
				//  Find out if the value is already in the array, if not, add it.
				found_value = false
				for array_looper = 1 to upperbound(formula_li_array)
					if formula_li_array[array_looper] = formula_element then
						//  We already added this item to the array and assinged an amount variable.
						found_value = true
						exit
					end if
				next
				//  If the value was not in the helper arrays, add it.
				if not found_value then
					cell = upperbound(formula_li_array) + 1
					formula_li_array[cell]     = formula_element
					formula_helper_array[cell] = "SUM(AMOUNT_" + string(cell) + ")"
					col_name_array[cell]       = col_name
				end if
			end if
			
		next  //  for iii = 1 to num_rows2 ... (Loop over each line item in this formula)
		
		//
		//  BUILD AND APPLY THE FORMULA.  This is a bit iterative.
		//
		sqls = ""
		
		//  Here we build the inner "view" part of the SQL ... this is the "table" that
		//  we will select the answer from and that will be joined to cr_financial_report_run
		//  (by unioning together 1 SQL per formula element).
		for array_looper = 1 to upperbound(formula_li_array)
			
			formula_element = formula_li_array[array_looper]
			amount_name     = formula_helper_array[array_looper]
			col_name        = col_name_array[array_looper]
			
			formula_element = f_replace_string(formula_element, "{", "", "all")
			formula_element = f_replace_string(formula_element, "}", "", "all")
			
			//  First we need 1 more array loop to build the amount SQL.  If the 2 loop variables
			//  are equal, then we know that we need an amount field.  Otherwise, we hardcode
			//  0 as the amount.
			amount_sqls = ""
			
			for array_looper2 = 1 to upperbound(formula_li_array)
				if array_looper2 = array_looper then
					amount_sqls = amount_sqls + " amount as amount_" + string(array_looper2)
				else
					amount_sqls = amount_sqls + " 0 as amount_" + string(array_looper2)
				end if
				if array_looper2 <> upperbound(formula_li_array) then
					amount_sqls = amount_sqls + ", "
				end if
			next
			
			if array_looper = 1 then
				sqls = sqls + &
					"select report_id, id, column_value, month_number, dollar_type, " + &
							  amount_sqls + &
					 " from cr_financial_reports_run " + &
					" where report_id = " + string(i_report_id) + &
					  " and " + col_name + " = '" + formula_element + "' " + &
					  " and month_number = " + string(mn) + " and header_row = 0 "
			else
				sqls = sqls + " union all " + &
					"select report_id, id, column_value, month_number, dollar_type, " + &
							  amount_sqls + &
					 " from cr_financial_reports_run " + &
					" where report_id = " + string(i_report_id) + &
					  " and " + col_name + " = '" + formula_element + "' " + &
					  " and month_number = " + string(mn) + " and header_row = 0 "
			end if
		
		next  //  for array_looper = 1 to upperbound(formula_li_array) ... ("inner view" SQL)
			
		//  Here we loop over the formula line items and create the formula in the select
		//  statement that wraps around the previous SQL.
		sqls_helper = " select report_id, column_value, month_number, dollar_type, "
		
		for iii = 1 to num_rows2
			formula_element = i_ds_formulas2.GetItemString(iii, "formula_element")
			if left(formula_element, 1) = "{" then
				//  7/10/06: Need to strip off the ", col1" off of the end now that we are saving
				//           it in the formula_element (to build formulas in columns other than
				//           the formula_element's column.
				strip_pos = pos(formula_element, "}, col")
				formula_element = left(formula_element, strip_pos)
				//  Use the arrays to determine the field name.
				for array_looper = 1 to upperbound(formula_li_array)
					if formula_li_array[array_looper] = formula_element then
						//  Found.  Change the value of formula_element ( e.g. SUM(AMOUNT_1) ).
						formula_element = formula_helper_array[array_looper]
					else
						//  Not found.
						continue
					end if
				next
			else
				//  This is something like a sign (e.g. "+") ... use as-is
			end if
			sqls_helper = sqls_helper + " " + formula_element + " "
		next
		
		//  Alias the answer for later and add the open paren that will be in front of
		//  the "inner view".
		sqls_helper = sqls_helper + " as the_answer from ( "
		
		//  Apply sqls_helper to the front of sqls.  Add the group by to the back end.
		sqls = sqls_helper + sqls + &
			") group by report_id, column_value, month_number, dollar_type "
		
		//  Check to see if we need a Having clause to avoid div-by-0 errors.
		for array_looper = 1 to upperbound(formula_helper_array)
			formula_element = formula_helper_array[array_looper]
			//  i.e. --- looking for something like " / SUM(AMOUNT_1)" ... the formula_helper_array
			//           will have cells like SUM(AMOUNT_1), SUM(AMOUNT_2), etc.
			if pos(sqls, "  /  " + formula_element + "", 1) > 0 then
				//  We have a divisor ...
				sqls = sqls + " having " + formula_element + " <> 0 "
			end if
		next
		
		//  The final product ...
		sqls = &
			"update cr_financial_reports_run a set amount = (" + &
			"select the_answer from (" + &
			sqls + ") b " + & 
			" where a.report_id = b.report_id and a.column_value = b.column_value " + &
			  " and a.month_number = b.month_number and a.dollar_type = b.dollar_type) " + &
			  " " + &
			" where report_id = " + string(i_report_id) + &
			  " and month_number = " + string(mn) + " " + &
			  " and col" + string(i) + " = '<" + formula_description + ">'"
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			//messagebox("Build", &
			//	"WARNING: There was an error updating the amount for formula " + &
			//		formula_description + ":~n~n" + sqlca.SQLErrText + "~n~n" + "The remainder " + &
			//			"of the report WILL be generated.", Exclamation!)
			f_pp_msgs( &
				"WARNING: There was an error updating the amount for formula " + &
					formula_description + ": " + sqlca.SQLErrText)
			f_pp_msgs("The remainder of the report WILL be generated.")
			f_pp_msgs("  ")
			rollback;
			continue //  No need to kick all the way out.  The rest of the report will be OK.
		end if			
		
	next  //  for ii = 1 to num_rows ... (Loop over each formula in this column)
	
next // for i = 1 to 10 ... (for each of the 10 columns)

//  *** END OF: Apply LINE ITEM FORMULAS ...



////
////  Apply COLUMN FORMULAS ... note that we placed this AFTER the amount sign update.
////
//SetMicroHelp("Applying column formulas ...")
//
////  Get the distinct formulas for this report.
//sqls = &
//	"select * from cr_financial_reports_form_col " + &
//	 "where report_id = " + string(i_report_id)
//
//i_ds_formulas.SetSQLSelect(sqls)
//num_rows = i_ds_formulas.RETRIEVE()
//
////  Loop over each formula for this report.
//for ii = 1 to num_rows
//	
//	formula_description = i_ds_formulas.GetItemString(ii, 1)
//	
//	//  Get the formula line items.
//	sqls = &
//		"select * from cr_financial_reports_form_col2 " + &
//		" where formula_id in " + &
//			" (select formula_id from cr_financial_reports_form_col " + &
//			  " where report_id = " + string(i_report_id) + &
//				 " and description = '" + formula_description + "') " + & 
//		" order by sort_order"
//		
//	i_ds_formulas2.SetSQLSelect(sqls)
//	num_rows2 = i_ds_formulas2.RETRIEVE()
//	
//	//  If they have a formula specified in the columns, but there is no formula data,
//	//  the execute at the end of this loop will bomb.  Check for this condition.
//	if num_rows2 <= 0 then
//		messagebox("Build", &
//			"WARNING: formula: " + formula_description + ": does not have any setup data.  This " + &
//				"formula will not be computed.")
//		continue //  Just continue to the next one since we've warned them ...
//	end if
//	
//	//  Just in case ...
//	i_ds_formulas2.SetSort("sort_order a")
//	i_ds_formulas2.Sort()
//	
//	//  Start by building arrays that will help us later.
//	formula_li_array     = empty_array
//	formula_helper_array = empty_array
//	
//	//  Loop over each line item in this formula.
//	for iii = 1 to num_rows2
//		
//		formula_element = i_ds_formulas2.GetItemString(iii, "formula_element")
//		
//		//  Only need the helper arrays for the meat of the formula.
//		if left(formula_element, 1) = "{" then
////			//  Find out if the value is already in the array, if not, add it.
////			found_value = false
////			for array_looper = 1 to upperbound(formula_li_array)
////				if formula_li_array[array_looper] = formula_element then
////					//  We already added this item to the array and assinged an amount variable.
////					found_value = true
////					exit
////				end if
////			next
////			//  If the value was not in the helper arrays, add it.
////			if not found_value then
////				cell = upperbound(formula_li_array) + 1
////				formula_li_array[cell]     = formula_element
////				formula_helper_array[cell] = "SUM(AMOUNT_" + string(cell) + ")"
////			end if
//		end if
//		
//	next  //  for iii = 1 to num_rows2 ... (Loop over each line item in this formula)
//	
////	//
////	//  BUILD AND APPLY THE FORMULA.  This is a bit iterative.
////	//
////	sqls = ""
////	
////	//  Here we build the inner "view" part of the SQL ... this is the "table" that
////	//  we will select the answer from and that will be joined to cr_financial_report_run
////	//  (by unioning together 1 SQL per formula element).
////	for array_looper = 1 to upperbound(formula_li_array)
////		
////		formula_element = formula_li_array[array_looper]
////		amount_name     = formula_helper_array[array_looper]
////		
////		formula_element = f_replace_string(formula_element, "{", "", "all")
////		formula_element = f_replace_string(formula_element, "}", "", "all")
////		
////		//  First we need 1 more array loop to build the amount SQL.  If the 2 loop variables
////		//  are equal, then we know that we need an amount field.  Otherwise, we hardcode
////		//  0 as the amount.
////		amount_sqls = ""
////		
////		for array_looper2 = 1 to upperbound(formula_li_array)
////			if array_looper2 = array_looper then
////				amount_sqls = amount_sqls + " amount as amount_" + string(array_looper2)
////			else
////				amount_sqls = amount_sqls + " 0 as amount_" + string(array_looper2)
////			end if
////			if array_looper2 <> upperbound(formula_li_array) then
////				amount_sqls = amount_sqls + ", "
////			end if
////		next
////		
////		if array_looper = 1 then
////			sqls = sqls + &
////				"select report_id, id, column_value, month_number, dollar_type, " + &
////						  amount_sqls + &
////				 " from cr_financial_reports_run " + &
////				" where report_id = " + string(i_report_id) + &
////				  " and col" + string(i) + " = '" + formula_element + "' " + &
////				  " and month_number = " + string(mn) + " and header_row = 0 "
////		else
////			sqls = sqls + " union all " + &
////				"select report_id, id, column_value, month_number, dollar_type, " + &
////						  amount_sqls + &
////				 " from cr_financial_reports_run " + &
////				" where report_id = " + string(i_report_id) + &
////				  " and col" + string(i) + " = '" + formula_element + "' " + &
////				  " and month_number = " + string(mn) + " and header_row = 0 "
////		end if
////	
////	next  //  for array_looper = 1 to upperbound(formula_li_array) ... ("inner view" SQL)
////		
////	//  Here we loop over the formula line items and create the formula in the select
////	//  statement that wraps around the previous SQL.
////	sqls_helper = " select report_id, column_value, month_number, dollar_type, "
////	
////	for iii = 1 to num_rows2
////		formula_element = i_ds_formulas2.GetItemString(iii, "formula_element")
////		if left(formula_element, 1) = "{" then
////			//  Use the arrays to determine the field name.
////			for array_looper = 1 to upperbound(formula_li_array)
////				if formula_li_array[array_looper] = formula_element then
////					//  Found.  Change the value of formula_element ( e.g. SUM(AMOUNT_1) ).
////					formula_element = formula_helper_array[array_looper]
////				else
////					//  Not found.
////					continue
////				end if
////			next
////		else
////			//  This is something like a sign (e.g. "+") ... use as-is
////		end if
////		sqls_helper = sqls_helper + " " + formula_element + " "
////	next
////	
////	//  Alias the answer for later and add the open paren that will be in front of
////	//  the "inner view".
////	sqls_helper = sqls_helper + " as the_answer from ( "
////	
////	//  Apply sqls_helper to the front of sqls.  Add the group by to the back end.
////	sqls = sqls_helper + sqls + &
////		") group by report_id, column_value, month_number, dollar_type "
////	
////	//  Check to see if we need a Having clause to avoid div-by-0 errors.
////	for array_looper = 1 to upperbound(formula_helper_array)
////		formula_element = formula_helper_array[array_looper]
////		//  i.e. --- looking for something like " / SUM(AMOUNT_1)" ... the formula_helper_array
////		//           will have cells like SUM(AMOUNT_1), SUM(AMOUNT_2), etc.
////		if pos(sqls, "  /  " + formula_element + "", 1) > 0 then
////			//  We have a divisor ...
////			sqls = sqls + " having " + formula_element + " <> 0 "
////		end if
////	next
////	
////	//  The final product ...
////	sqls = &
////		"update cr_financial_reports_run a set amount = (" + &
////		"select the_answer from (" + &
////		sqls + ") b " + & 
////		" where a.report_id = b.report_id and a.column_value = b.column_value " + &
////		  " and a.month_number = b.month_number and a.dollar_type = b.dollar_type) " + &
////		  " " + &
////		" where report_id = " + string(i_report_id) + &
////		  " and month_number = " + string(mn) + " " + &
////		  " and col" + string(i) + " = '<" + formula_description + ">'"
////	
////	execute immediate :sqls;
////	
////	if sqlca.SQLCode < 0 then
////		messagebox("Build", &
////			"WARNING: There was an error updating the amount for formula " + &
////				formula_description + ":~n~n" + sqlca.SQLErrText + "~n~n" + "The remainder " + &
////					"of the report WILL be generated.", Exclamation!)
////		rollback;
////		continue //  No need to kick all the way out.  The rest of the report will be OK.
////	end if			
//	
//next  //  for ii = 1 to num_rows ... (Loop over each formula for this report)
//
////  *** END OF: Apply COLUMN FORMULAS ...



//
//  Update the column_sort_order for this report ...
//
f_pp_msgs("Updating column_sort_order ... at " + string(now()))

update cr_financial_reports_run a set column_sort_order = 
	(select column_sort_order from cr_financial_reports_columns b
	  where a.report_id = b.report_id 
	  	 and a.report_id = :i_report_id and b.report_id = :i_report_id
		 and a.column_value = replace(replace(b.column_value, '{', ''), '}', ''))
 where report_id = :i_report_id and month_number = :mn;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	//messagebox("Build", "ERROR: updating column_sort_order:~n~n" + sqlca.SQLErrText, &
	//	Exclamation!)
	f_pp_msgs("ERROR: updating column_sort_order: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	uf_drop()
	return -1
end if


//
//  Apply the description settings based on the row_values and column_values fields.
//

//  ROWS:
if row_values = "Descriptions" or row_values = "Both" then
	
	f_pp_msgs("Applying descriptions to row values ... at " + string(now()))
	
	setnull(element_table)
	setnull(element_column)
	select element_table, upper(element_column) 
	  into :element_table, :element_column 
	  from cr_elements
	 where element_id = :element_id;
	if isnull(element_table)  or element_table  = "" then goto after_row_descrs
	if isnull(element_column) or element_column = "" then goto after_row_descrs
	
	if row_values = "Descriptions" then
		descr_field = "b.description"
	else
		descr_field = 'b."' + element_column + '"||' + "': '" + "|| b.description"
	end if
		
	for i = 1 to 10
		
		sqls = &
			"update cr_financial_reports_run a set col" + string(i) + " = ( " + &
				"select " + descr_field + " from " + element_table + " b " + &
				" where a.col" + string(i) + ' = b."' + element_column + '"' + &
				  " and b.element_type = 'Actuals') " + &
			" where report_id = " + string(i_report_id) + &
			  " and month_number = " + string(mn) + &
			  " and col" + string(i) + ' in (select "' + element_column + '" ' + &
			  										   " from " + element_table + &
													  " where element_type = 'Actuals')"
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			//messagebox("Build", "ERROR: updating col" + string(i) + " with descriptions:~n~n" + &
			//	sqlca.SQLErrText, Exclamation!)
			f_pp_msgs("ERROR: updating col" + string(i) + " with descriptions: " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			uf_drop()
			return -1
		end if
		
	next
	
end if

after_row_descrs:

//  COLUMNS
if column_values = "Descriptions" or column_values = "Both" then
	
	f_pp_msgs("Applying descriptions to column values ... at " + string(now()))
	
	setnull(element_table)
	setnull(element_column)	
	select element_table, upper(element_column) 
	  into :element_table, :element_column 
	  from cr_elements
	 where element_id = :columns_element_id;
	if isnull(element_table)  or element_table  = "" then goto after_col_descrs
	if isnull(element_column) or element_column = "" then goto after_col_descrs
	
	if column_values = "Descriptions" then
		descr_field = "b.description"
	else
		descr_field = 'b."' + element_column + '"||' + "': '" + "|| b.description"
	end if
		
	sqls = &
		"update cr_financial_reports_run a set column_value = ( " + &
			"select " + descr_field + " from " + element_table + " b " + &
			" where a.column_value" + ' = b."' + element_column + '"' + &
			  " and b.element_type = 'Actuals') " + &
		" where report_id = " + string(i_report_id) + &
		  " and month_number = " + string(mn) + &
		  " and column_value" + ' in (select "' + element_column + '" ' + &
												" from " + element_table + &
											  " where element_type = 'Actuals')"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		//messagebox("Build", "ERROR: updating column_value with descriptions:~n~n" + &
		//	sqlca.SQLErrText, Exclamation!)
		f_pp_msgs("ERROR: updating column_value with descriptions: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		uf_drop()
		return -1
	end if
	
end if

after_col_descrs:


//
//  86 the angle-brackets on the formulas so those line items don't look stupid.  This code
//  is at the end in case any code above needs access to the angle-brackets.
//
for i = 1 to 10
	
	if i = 1 then
		f_pp_msgs("Removing angle-brackets from formulas ... at " + string(now()))
	end if
	
	sqls = &
		"update cr_financial_reports_run set col" + string(i) + " = " + &
			" substr(col" + string(i) + ", 2, length(col" + string(i) + ") - 2) " + &
		" where month_number = " + string(mn) + &
		  " and report_id = " + string(i_report_id) + " " + &
		  " and substr(col" + string(i) + ", 1, 1) = '<' " + &
		  " and substr(col" + string(i) + ", length(col" + string(i) + "), 1) = '>'"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		//messagebox("Build", "ERROR: removing angle-brackets from col" + string(i) + ":~n~n" + &
		//	sqlca.SQLErrText, Exclamation!)
		f_pp_msgs("ERROR: removing angle-brackets from col" + string(i) + ": " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		uf_drop()
		return -1
	end if
	
next

commit;


//
//  Delete any line items marked as "remove row" in the setup table.
//  ---  It is not super-critical if this does not work, so I am not error checking
//       or issuing a "return".
//
f_pp_msgs('Deleting records marked as "remove row" ... at ' + string(now()))

uf_delete_remove_row()

commit;


//
//  Drop the i_temp_table ...
//
f_pp_msgs("Dropping the temp table ... at " + string(now()))
uf_drop()


//  *****  NOT IN THE STAND-ALONE.  *****
//SetMicroHelp("Ready")
//messagebox(this.text, "Done.")


next  //  for i = i_start_month to i_end_month ...


return 1
end function

public function integer uf_read ();
//**********************************************************************************
//
//  The command line argument cannot be NULL ... 
//
//  The command line argument is the following format:  
//
//  user_id    (that is calling this process)
//  session_id (that is calling this process)
//
//**********************************************************************************
string command_line_args_array[], sqls
longlong cmd_line_arg_elements, rtn

if isnull(g_command_line_args) or g_command_line_args = "" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The command line argument is NULL.  Cannot run the CR Financial Reports - Build!")
	f_pp_msgs(" ")
	return -1
end if

//  Translate the command line argument into the appropriate
//  variables.  If the 2 arguments are not passed, log an error.
cmd_line_arg_elements = f_parsestringintostringarray(g_command_line_args, ",", command_line_args_array[])

if cmd_line_arg_elements <> 2 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The command line argument contained " + &
		string(cmd_line_arg_elements) + " argument(s).  " + &
		"It must have both arguments that are required.  " + &
		"Cannot run the CR Financial Reports - Build!")
	f_pp_msgs(" ")
	return -1
end if

i_calling_user_id    = lower(trim(command_line_args_array[1]))
i_calling_session_id =  long(trim(command_line_args_array[2]))


//
//  Get the start and end month from cr_alloc_system_control ... Also get the month_period.
//  Note that a month_period of 0 indicated "whole month".
//
setnull(i_start_month)
select rtrim(control_value) into :i_start_month from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'CR FIN. REPORTS BUILD START MONTH';
if isnull(i_start_month) then i_start_month = 0

if i_start_month = 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The argument: " + string(i_start_month) + " for the " + &
		"CR FIN. REPORTS BUILD START MONTH is invalid. Check the " + &
		"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
		"Connot run the CR Financial Reports - Build!")
	f_pp_msgs(" ")
	return -1
end if

setnull(i_end_month)
select rtrim(control_value) into :i_end_month from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'CR FIN. REPORTS BUILD ENDING MONTH';
if isnull(i_end_month) then i_end_month = 0

if i_end_month = 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The argument: "+ string(i_end_month) + " for the " + &
		"CR FIN. REPORTS BUILD ENDING MONTH is invalid. Check the " + &
		"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
		"Connot run the CR Financial Reports - Build!")
	f_pp_msgs(" ")
	return -1
end if

f_pp_msgs("  ")
f_pp_msgs("Running for " + string(i_start_month) + " through " + string(i_start_month))

//*****************************************************************************************
//
//  Get the Structures Variable:  New variable to determine which structure values
//                                table is being used.
//
//*****************************************************************************************
setnull(i_use_new_structures_table)
select upper(rtrim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(rtrim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"

//  For budget data.
i_default_bv = ""
select trim(control_value) into :i_default_bv from cr_alloc_system_control
 where upper(trim(control_name)) = 'CR FIN. REPORTS BUILD BDG VERSION';
if isnull(i_default_bv) then i_default_bv = ""


//*****************************************************************************************
//
//  Instanced variables for datastores used in the UO ... these were instance variables in
//  w_cr_financial_reports_build previously.  They are named like "i_" so I don't have
//  to re-write all the code.
//
//*****************************************************************************************
i_ds_levels         = CREATE uo_ds_top
i_ds_dollar_types   = CREATE uo_ds_top
i_ds_formulas       = CREATE uo_ds_top
i_ds_formulas2      = CREATE uo_ds_top
i_ds_str_override   = CREATE uo_ds_top
i_ds_formula_audit1 = CREATE uo_ds_top
i_ds_formula_audit2 = CREATE uo_ds_top
i_ds_formula_audit3 = CREATE uo_ds_top
i_ds_generic_string_audit = CREATE uo_ds_top

sqls = "select rpad(' ', 2000) from dual"
f_create_dynamic_ds(i_ds_generic_string_audit, "grid", sqls, sqlca, true)

sqls = "select 100 from dual"
f_create_dynamic_ds(i_ds_levels, "grid", sqls, sqlca, true)

sqls = "select * from cr_financial_reports_dol_type where report_id = -678"
f_create_dynamic_ds(i_ds_dollar_types, "grid", sqls, sqlca, true)

sqls = &
	"select distinct replace(replace(col1,'<',''),'>','') " + &
	 " from cr_financial_reports_run " + &
	" where report_id = 0 and month_number = 0 and substr(col1,1,1) = '<'"
f_create_dynamic_ds(i_ds_formulas, "grid", sqls, sqlca, true)

sqls = &
	"select * from cr_financial_reports_formulas2 " + &
	" where formula_id in " + &
		" (select formula_id from cr_financial_reports_formulas " + &
		  " where report_id = 0 and description = 'just4setup') " + & 
	" order by sort_order"
f_create_dynamic_ds(i_ds_formulas2, "grid", sqls, sqlca, true)

sqls = &
	"select distinct a.override_structure_id " + &
	  "from cr_financial_reports_setup a, cr_financial_reports b " + &
	 "where a.report_id = b.report_id and b.report_id = -678 " + &
		"and a.override_structure_id <> b.rows_structure_id"
f_create_dynamic_ds(i_ds_str_override, "grid", sqls, sqlca, true)

sqls = "select col1 from cr_financial_reports_setup where report_id = -678"
f_create_dynamic_ds(i_ds_formula_audit1, "grid", sqls, sqlca, true)

sqls = "select * from cr_financial_reports_formulas where report_id = -678"
f_create_dynamic_ds(i_ds_formula_audit2, "grid", sqls, sqlca, true)

sqls = "select formula_element from cr_financial_reports_formulas2 where formula_id = -678 " + &
	"and formula_line_id = -678"
f_create_dynamic_ds(i_ds_formula_audit3, "grid", sqls, sqlca, true)


////*****************************************************************************************
////
////  There are some DW's on this window that will be referenced by the UO:
////
////*****************************************************************************************
open(w_cr_financial_reports_build)
//dw_cr_financial_reports_columns_grid.SetTransObject(sqlca)
//dw_cr_financial_reports_setup_grid.SetTransObject(sqlca)
//dw_cr_financial_reports_edit.SetTransObject(sqlca)


//*****************************************************************************************
//
//  RUN the BUILD:
//
//*****************************************************************************************
rtn = uf_start()


//*****************************************************************************************
//
//  HALT the application ...
//
//*****************************************************************************************
if rtn = 1 then
	f_pp_msgs("  ")
	f_pp_msgs("The report build RAN SUCCESSFULLY.")
	f_pp_msgs("  ")
	f_pp_msgs(string(i_num_reports_to_build - i_num_errors) + " out of " + &
		string(i_num_reports_to_build) + " reports were rebuilt.")
	f_pp_msgs("  ")
	g_rtn_code = 0
else
	g_rtn_code = -1
end if  

return g_rtn_code
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_financial_reports_build_custom_version nvo_cr_financial_reports_build_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_financial_reports_build_custom.pbd'
		return nvo_cr_financial_reports_build_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

