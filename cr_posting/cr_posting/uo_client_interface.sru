HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_company, i_output_location
string i_stg_table_name, i_description, i_detail_posting
longlong 	 i_obey_gl_rollup, i_output_method, i_posting_id, i_array_counter
uo_ds_top i_ds_posting_audit, i_ds_elements, i_ds_sources
longlong	i_posting_id_array[]
string					i_where_clause_array[]

string	i_exe_name = 'cr_posting.exe'
string i_obey_approval

// ### - SEB - 8616 - 091311: New system switch for standard SAP integration
string i_use_standard_sap, i_sap_pop_tables[]
uo_ds_top i_ds_sap_posting_cols
longlong i_c = 1
end variables

forward prototypes
public function string uf_clean_string (string a_string)
public function longlong uf_create_file_or_table ()
public function longlong uf_update_summary_values ()
public function longlong uf_insert_post_to_gl ()
public function longlong uf_cc_id ()
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function string uf_clean_string (string a_string);//*****************************************************************************************************************
//
//  Function:     uf_clean_string (copy of f_cr_clean_string)
//
//  Description:  Change any bad characters to underscores
//
//*****************************************************************************************************************

string source, bad_characters[]
longlong start_pos
longlong i, counter

source  = a_string
counter = 0
bad_characters[1] = "/"
bad_characters[2] = " "
bad_characters[3] = "-"

for i = 1 to upperbound(bad_characters)
	start_pos = 1
	// Find the first occurrence of bad_characters.
	start_pos = pos(source, bad_characters[i], start_pos)

	// Only enter the loop if you find bad_characters.
	do while start_pos > 0

		// Replace bad_characters with "_".
		source = Replace(source, start_pos, len(bad_characters[i]), "_")

	// Find the next occurrence of old_str.

	start_pos = Pos(source, bad_characters[i], start_pos + 1)
	
	//  Endless loop protection ...
	counter ++
	if counter = 100 then 
		messagebox("!!!", "You have an endless loop.")
		exit
	end if
	
	loop
next

return source
end function

public function longlong uf_create_file_or_table ();longlong i, rtn, posted_to_gl, num_cols,  j, col_dec, front_nine_length
string sqls, create_text_file, gl_column_name, post_to_gl_table, file_extension, &
		col_width, col_type, nines, gl_cols, front_nines

if i_output_method = 1 then //table

	//	DML: 042006: Build the insert dynamically to include the column lists in case the posting
	//		is against a table over a link and we don't need to insert into every column.
	//		If we don't have to insert into every column our staging table will not be 
	//		shaped the same as the posting table.

	//	sqls = ' insert into ' + i_output_location + ' select * from ' + i_stg_table_name 

	uo_ds_top ds_post_to_gl
	ds_post_to_gl = create uo_ds_top

	sqls = " select replace(replace(replace(gl_column_name,' ','_'),'-','_'),'/','_') gl_column_name " + &
			 " from cr_post_to_gl_columns " + &
			 " where posting_id = " + string(i_posting_id) + "order by " + '"ORDER"'
			 
	f_create_dynamic_ds(ds_post_to_gl,"grid",sqls,sqlca,true)
	rtn = ds_post_to_gl.rowcount()
	
	gl_cols = ""
	for i = 1 to rtn 
		gl_column_name = ds_post_to_gl.getitemstring(i,"gl_column_name")
		gl_column_name = '"' + upper(gl_column_name) + '"'
		gl_cols = gl_cols + gl_column_name + ", "
	next 
	gl_cols = LEFT(gl_cols, LEN(gl_cols) - 2)

	sqls = 	' insert into ' + i_output_location +&
				' ( ' + gl_cols + ' ) select ' + gl_cols +&
				' from ' + i_stg_table_name 
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Inserting into " + i_output_location + " : " + sqlca.sqlerrtext)
		rollback;
		f_pp_msgs(sqls)
		return -1
	else
		destroy ds_post_to_gl
	end if
	
else	//file
	
	uo_ds_top ds_create_text_file
	ds_create_text_file = create uo_ds_top
	
	file_extension = lower(right(i_output_location,3) )
	
	if file_extension = 'dat' then  // fixed width
		// Get the table column definitions
		sqls = " select replace(replace(replace(gl_column_name,' ','_'),'-','_'),'/','_'), " + &
				' "WIDTH","TYPE","DECIMAL" from cr_post_to_gl_columns ' + &
				" where posting_id = " + string(i_posting_id) + ' order by "ORDER", gl_column_name'
		uo_ds_top ds_cr_columns
		ds_cr_columns = create uo_ds_top
		f_create_dynamic_ds(ds_cr_columns,"grid",sqls,sqlca,true)
		num_cols = ds_cr_columns.rowcount()
		
		if num_cols = 0 then return 1
		
		sqls = 'select '

		for i = 1 to num_cols
			
			setnull(col_width)
			gl_column_name = ds_cr_columns.getitemstring(i,1)
			col_width = string(ds_cr_columns.getitemnumber(i,2))
			col_type = ds_cr_columns.getitemstring(i,3)
			
			if col_type = "Character" then
				sqls = sqls + "rpad(" + gl_column_name + "," + col_width + ")"
			else
				
				
				//###CWB AC-2136 Dynamically determine front 9's
				if isnull(col_width) then col_width = "0"
				setnull(col_dec)
				col_dec = ds_cr_columns.getitemnumber(i,4)
				if isnull(col_dec) then col_dec = 0
				
				front_nine_length = long(col_width) - col_dec - 3
				if front_nine_length > 0 then
					front_nines = "9"
					for j = 2 to front_nine_length
						front_nines = front_nines + "9"
					next
				end if
				if not isnull(col_dec) and col_dec > 0 then
					nines = "9"
					for j = 2 to col_dec
						nines = nines + "9"
					next
					// Removed the lpad on this statement as the to_char builds the correct length.
					sqls = sqls + "to_char(" + gl_column_name + ",'" + front_nines+"0." + nines + "')"
				else
					sqls = sqls + "lpad(to_char(" + gl_column_name + ")," + col_width + ")"
				end if
			end if
			
			if i <> num_cols then
				sqls = sqls + "||"
			else
				sqls = sqls + " from " + i_stg_table_name
			end if
			
		next

	else
		sqls = ' select * from ' + i_stg_table_name
	end if
	
	f_create_dynamic_ds(ds_create_text_file,"grid",sqls,sqlca,true)
	
	choose case file_extension
		case 'txt', 'dat'
			rtn = ds_create_text_file.SaveAs(i_output_location,text!,false)
		case 'xls'
			rtn = ds_create_text_file.SaveAs(i_output_location,excel!,false)
		case 'csv'
			rtn = ds_create_text_file.SaveAs(i_output_location,csv!,false)
	end choose
	
	
	if rtn <> 1 then
		f_pp_msgs("ERROR: Creating the text file. ")
		f_pp_msgs(sqls)
		return -1
	end if
	
end if
	
return 1
end function

public function longlong uf_update_summary_values ();//*****************************************************************************************
//
//  Function  			:  uf_update_summary_values
//
//	 Description      :  Performs an update that changes the detailed code block values
//								in cr_temp_cr_gl_temp to summary code block values.    The results in
//                      cr_temp_cr_gl_temp very well may end up with many records containing
//                      the same summary code block.  They will be summarized in the
//                      uf_insert_post_to_gl function.
//								
//*****************************************************************************************
longlong i, rtn, num_elements, values_by_company
string sqls, element_table, element_column, cr_element, company_field

uo_ds_top ds_elements
ds_elements = create uo_ds_top

// ### 7894 JAK: 2011-06-21:  Values by company logic was never added here...
sqls = " select element_table,element_column,description, values_by_company " + &
	" from cr_elements " + &
	" where (gl_actual_or_budget = 1 or gl_actual_or_budget is null) "
f_create_dynamic_ds(ds_elements,"grid",sqls,sqlca,true)

//	Get the company value from the system control
select upper(replace(replace(replace(control_value,' ','_'),'-','_'),'/','_')) into :company_field
from cr_system_control
where lower(control_name) = 'company field';

num_elements = ds_elements.rowcount()
for i = 1 to num_elements
	
	element_table 	= upper(ds_elements.getitemstring(i,"element_table"))
	element_column	= upper(ds_elements.getitemstring(i,"element_column"))
	cr_element		= upper(ds_elements.getitemstring(i,"description"))
	values_by_company		= ds_elements.getitemnumber(i,"values_by_company")
	
	element_table 	= f_cr_clean_string(element_table)
	element_column = f_cr_clean_string(element_column)
	cr_element 		= f_cr_clean_string(cr_element)
	if isnull(values_by_company) or values_by_company <> 1 then values_by_company = 0

//	In case the element is a reserved word user upper and double quotes
//	element_table  = '~""' + element_table  + '~""'
//	element_column = '~""' + element_column + '~""'
	element_table  = '~"' + element_table  + '~"'
	element_column = '~"' + element_column + '~"'
	cr_element 		= '~"' + cr_element 		+ '~"'

	sqls = "update cr_temp_cr_gl_temp a set " + cr_element + " = ( " + &
		" select b.budget_value from " + element_table + " b " + &
		" where a." + cr_element + " = b." + element_column
	
	// ### 7894 JAK: 2011-06-21:  Values by company logic was never added here...	
	if values_by_company = 1 then sqls += " and a." + company_field + " = b." + company_field
	
	sqls += " and b.element_type = 'Actuals' ) "
	
	execute immediate :sqls;
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Updating cr_temp_cr_gl_temp." + element_column + ": " + sqlca.sqlerrtext)
		f_pp_msgs(sqls)
		rollback;
		return -1
	end if

next

return 1
end function

public function longlong uf_insert_post_to_gl ();string post_to_gl_table, sqls, gl_column_name, cr_column_name, create_text_file, &
	company, company_field, fields[], company_where, gl_journal_category, gljc_where, &
	log_sqls, s_date, spec_proc_dates2_sqls, sqls2, where_clause, group_clause, detail_field, scalar_replace, &
	amount_type, amount_type_where, null_array[]
longlong i, rtn, posted_to_gl, cr_column, d, num_trans, amount_field, needs_replace, j
dec {2} debits, credits
boolean subselect

uo_ds_top ds_post_to_gl
ds_post_to_gl = create uo_ds_top

//	Get the company value from the system control
select upper(replace(replace(replace(control_value,' ','_'),'-','_'),'/','_')) into :company_field
from cr_system_control
where lower(control_name) = 'company field';

// ### 6202: JAK: Add Amount Type restriction
select company, gl_journal_category, amount_type into :company, :gl_journal_category, :amount_type
from cr_post_to_gl_control 
where posting_id = :i_posting_id;


//	parse the company value out and create the "in" 
if company = '*' or isnull(company) or trim(company) = '' then
	company_where = ''
else
	// ### 7764 JAK 2011-06-06:  reset the fields array
	fields = null_array
	
	company_where = '"' + company_field + '" in ('
	f_parsestringintostringarray(company,',',fields[])
	for d = 1 to upperbound(fields)
		company_where += "'" + fields[d] + "',"	
	next
	company_where = mid(company_where,1,len(company_where) - 1) + ') and '
end if

//	parse the gl_journal_category value out and create the "in" 
if gl_journal_category = '*' or isnull(gl_journal_category) or trim(gl_journal_category) = '' then
	gljc_where = ''
else
	// ### 7764 JAK 2011-06-06:  reset the fields array
	fields = null_array
	
	gljc_where = '"GL_JOURNAL_CATEGORY" in ('
	f_parsestringintostringarray(gl_journal_category,',',fields[])
	for d = 1 to upperbound(fields)
		gljc_where += "'" + fields[d] + "',"	
	next
	gljc_where = mid(gljc_where,1,len(gljc_where) - 1) + ') and '
end if

//	parse the amount type value out and create the "in" 
if amount_type = '*' or isnull(amount_type) or trim(amount_type) = '' then
	amount_type_where = ''
else
	// ### 7764 JAK 2011-06-06:  reset the fields array
	fields = null_array
	
	amount_type_where = '"AMOUNT_TYPE" in ('
	f_parsestringintostringarray(amount_type,',',fields[])
	for d = 1 to upperbound(fields)
		amount_type_where += fields[d] + ","	
	next
	amount_type_where = mid(amount_type_where,1,len(amount_type_where) - 1) + ') and '
end if

// Build the complete where clause
// ### 8191  JAK: 2011-07-27:  Extra spaces here caused the right(where_clause,4) below
//	to not work.  Remove those extra spaces.
//where_clause = ' where ' + company_where + ' ' + gljc_where + ' ' + amount_type_where
where_clause = ' where ' + company_where + gljc_where + amount_type_where
if right(where_clause,4) = 'and ' then 
	where_clause = mid(where_clause,1,len(where_clause) - 4)
end if
if trim(where_clause) = 'where' then where_clause = ''

//	DML: 080907: 1370: Don't replace spaces or special characters with underscores if
//		the column is not a CR column.  Any space hardcoding or text with spaces were
//		being turned to underscores.
// ### 8193  JAK: 2011-07-27:  Posting_ID hardcoded to 1 below need changed to i_posting_id
sqls = " select replace(replace(replace(gl_column_name,' ','_'),'-','_'),'/','_') gl_column_name, " + &
	"cr_column_name,  " + &
	"cr_column, 0 amount_field , " + &
	"case when exists (select 1 from cr_sources a, cr_sources_fields b where a.source_id = b.source_id  " + &
	"	and a.cr_column_name like '%' || a.table_name || '.' || upper(replace(replace(replace(replace(b.description,' ','_'),'-','_'),'/','_'),'\','_')) || '%') " + &
	"then 1 else 0 end needs_replace " + &
	"from cr_post_to_gl_columns  a " + &
	'where posting_id = ' + string(i_posting_id) + ' order by "ORDER" '
		 
f_create_dynamic_ds(ds_post_to_gl,"grid",sqls,sqlca,true)
rtn = ds_post_to_gl.rowcount()

if i_detail_posting = 'YES' then
	sqls = "select a.table_name || '.' || upper(replace(replace(replace(replace(b.description,' ','_'),'-','_'),'/','_'),'\','_')) detail_field, " + &
		"	'(select ' || upper(replace(replace(replace(replace(b.description,' ','_'),'-','_'),'/','_'),'\','_')) || ' from ' || a.table_name || ' z where z.id = cr_temp_cr_gl_temp.id) '  scalar_replace " + &
		"from cr_sources a, cr_sources_fields b  " + &
		"where a.source_id = b.source_id  " + &
		"and exists (select 1 " + &
		"	from cr_post_to_gl_columns c " + &
		"	where c.cr_column_name like '%' || a.table_name || '.' || upper(replace(replace(replace(replace(b.description,' ','_'),'-','_'),'/','_'),'\','_')) || '%') "
	
	uo_ds_top ds_scalars_replace
	ds_scalars_replace = create uo_ds_top
	f_create_dynamic_ds(ds_scalars_replace,"grid",sqls,sqlca,true)
end if

// Check and see if we're using a subselect...if so we'll have to build the insert / groups differently
subselect = false
for i = 1 to rtn
	cr_column_name = ds_post_to_gl.getitemstring(i,"cr_column_name")
	needs_replace = ds_post_to_gl.getitemnumber(i,"needs_replace")
	if pos(upper(cr_column_name),'(SELECT') > 0 or needs_replace = 1 then
		subselect = true
		exit
	end if
next

sqls = " insert into " + i_stg_table_name + " ( "

for i = 1 to rtn 

	gl_column_name = ds_post_to_gl.getitemstring(i,"gl_column_name")
	gl_column_name = '"' + upper(gl_column_name) + '"'
	
	if i = rtn then
		sqls = sqls + gl_column_name + ") "
	else
		sqls = sqls + gl_column_name + ", "
	end if

next 


if subselect then		
	// Populate the amount field and update the cr_column_name if necessary
	for i = 1 to rtn 
		cr_column_name = ds_post_to_gl.getitemstring(i,"cr_column_name")
		cr_column = ds_post_to_gl.getitemnumber(i,"cr_column")
		needs_replace = ds_post_to_gl.getitemnumber(i,"needs_replace")
		gl_column_name = ds_post_to_gl.getitemstring(i,"gl_column_name") 
		gl_column_name = '"' + upper(gl_column_name) + '"'
		
		if needs_replace = 1 then
			cr_column = 0
			for j = 1 to ds_scalars_replace.rowcount()
				detail_field = ds_scalars_replace.getitemstring(j,'detail_field')
				scalar_replace = ds_scalars_replace.getitemstring(j,'scalar_replace')
				
				//WJT: If a second column name contains the full column name of a previous column, he second column substitution will be pre-empted by the first column and create a syntax error/corruption
				// Quick fix: Validate that the columns to be replaced match exactly so no partial replacements occur 
				if cr_column_name = detail_field then
					cr_column_name = f_replace_string(cr_column_name, detail_field, scalar_replace, 'all')
				end if
			next
		else
			if cr_column = 1 then cr_column_name = f_cr_clean_string(cr_column_name)
		end if
		
		if cr_column = 1 and lower(cr_column_name) <> 'amount' and &
			lower(cr_column_name) <> 'quantity' and lower(cr_column_name) <> 'dr_amount' and &
			lower(cr_column_name) <> 'cr_amount' then
			// Not an amount...continue to the next field
			continue
		end if
		
		choose case lower(cr_column_name)
			case 'amount','quantity'
				ds_post_to_gl.setitem(i,'amount_field',1)
				continue
			case 'dr_amount'
				ds_post_to_gl.setitem(i,'amount_field',1)
				ds_post_to_gl.setitem(i,'cr_column_name','decode(sign(amount),1,amount,0)')
				continue
			case 'cr_amount'
				ds_post_to_gl.setitem(i,'amount_field',1)
				ds_post_to_gl.setitem(i,'cr_column_name','decode(sign(amount),-1,amount * -1,0)')
				continue
		end choose
		
		if pos(upper(cr_column_name),'SUM(') > 0 then
			ds_post_to_gl.setitem(i,'amount_field',1)
			ds_post_to_gl.setitem(i,'cr_column_name',mid(cr_column_name,5,len(cr_column_name) - 5))
			continue
		end if	
		
		if lower(gl_column_name) = '"amount"' or lower(gl_column_name) = '"quantity"' then
			ds_post_to_gl.setitem(i,'amount_field',1)
			continue
		end if			
	next
	
	//	build the select
	sqls2 = "select " 
	sqls = sqls + " select "
	group_clause = "group by "
	
	for i = 1 to rtn
		cr_column_name = ds_post_to_gl.getitemstring(i,"cr_column_name")
		cr_column = ds_post_to_gl.getitemnumber(i,"cr_column")
		gl_column_name = ds_post_to_gl.getitemstring(i,"gl_column_name") 
		gl_column_name = '"' + upper(gl_column_name) + '"'
		amount_field = ds_post_to_gl.getitemnumber(i,'amount_field')
		needs_replace = ds_post_to_gl.getitemnumber(i,"needs_replace")
		
		if needs_replace = 1 then
			cr_column = 0
			for j = 1 to ds_scalars_replace.rowcount()
				detail_field = ds_scalars_replace.getitemstring(j,'detail_field')
				scalar_replace = ds_scalars_replace.getitemstring(j,'scalar_replace')
				
				//WJT: If a second column name contains the full column name of a previous column, he second column substitution will be pre-empted by the first column and create a syntax error/corruption
				// Quick fix: Validate that the columns to be replaced match exactly so no partial replacements occur 
				if cr_column_name = detail_field then
					cr_column_name = f_replace_string(cr_column_name, detail_field, scalar_replace, 'all')
				end if
			next
		else
			if cr_column = 1 then cr_column_name = f_cr_clean_string(cr_column_name)
		end if
		
		if cr_column_name = "'_'" then cr_column_name = "' '"

		//	if this is a CR column then put the upper and double quotes
		if cr_column = 1 and pos(lower(cr_column_name),'amount') = 0 and &
			pos(lower(cr_column_name),'quantity') = 0  then
			cr_column_name = '"' + upper(cr_column_name) + '"'
		end if
		
		sqls2 = sqls2 + cr_column_name + ' as ' + gl_column_name + ','
		
		if amount_field = 0 then
			sqls = sqls + gl_column_name + ','
			group_clause = group_clause + gl_column_name + ','
		else
			sqls = sqls + 'sum(' + gl_column_name + '),'
		end if
	next
	
	if right(trim(sqls2),1) = ',' then sqls2 = mid(trim(sqls2),1,len(trim(sqls2)) - 1)
	if right(trim(sqls),1) = ',' then sqls = mid(trim(sqls),1,len(trim(sqls)) - 1)
	if right(trim(group_clause),1) = ',' then group_clause = mid(trim(group_clause),1,len(trim(group_clause)) - 1)
	
	sqls = sqls + " from (" + sqls2 + " from cr_temp_cr_gl_temp " + where_clause + ") " + group_clause
	
	execute immediate :sqls;
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Inserting into " + i_stg_table_name + ": " + sqlca.sqlerrtext)
		rollback;
		f_pp_msgs(sqls)
		return -1
	end if
else
	
	//	build the select
	sqls = sqls + " select " 
	
	for i = 1 to rtn 
	
		cr_column_name = ds_post_to_gl.getitemstring(i,"cr_column_name")
	//	company = ds_post_to_gl.getitemstring(i,"company")
		cr_column = ds_post_to_gl.getitemnumber(i,"cr_column")
		
		if cr_column = 1 then cr_column_name = f_cr_clean_string(cr_column_name)
		
		// DMJ: 10/19/2007: abs(amount) would not work with duplicate records.
		gl_column_name = ds_post_to_gl.getitemstring(i,"gl_column_name")
		gl_column_name = '"' + upper(gl_column_name) + '"'
		
		//	DML: 072903: Blank values are being turned to underscores.  I need the
		//	replace command for the blanks in the column name if it is a CR column.
		//	However, if they just want a blank space don't turn it into an underscore.
		if cr_column_name = "'_'" then cr_column_name = "' '"
		
		//	if this is a CR column then put the upper and double quotes
		if cr_column = 1 and lower(cr_column_name) <> 'amount' and &
			lower(cr_column_name) <> 'quantity' and lower(cr_column_name) <> 'dr_amount' and &
			lower(cr_column_name) <> 'cr_amount' then
			cr_column_name = '"' + upper(cr_column_name) + '"'
		end if
		
		choose case lower(cr_column_name)
			case 'amount'
				//	DML: 092605: If the dr_cr_id is incorrect then the abs will cause
				//		an oob journal.  Although the dr_cr_id should be corrected it
				//		shouldn't impact posting to the GL.  So, take the amount as is.
				//		Make this change for the dr_amount and cr_amount columns too.
	//			cr_column_name = 'sum(abs(amount) * dr_cr_id)'
				cr_column_name = 'sum(amount)'
			case 'quantity'
				cr_column_name = 'sum(quantity)'
			//	DML: 092204: When posting to Oracle, we need a separate DR and CR amount
			//		columns.  cr_post_to_gl_columns will plug in dr_amount or cr_amount
			//		in the CR column name and this code will handle it.
			case 'dr_amount'
	//			cr_column_name = 'sum(decode(dr_cr_id,1,amount,0))'
				cr_column_name = 'sum(decode(sign(amount),1,amount,0))'
			case 'cr_amount'
				//cr_column_name = 'sum(decode(dr_cr_id,-1,amount * dr_cr_id,0))'
				cr_column_name = 'sum(decode(sign(amount),-1,amount * -1,0))'
		end choose
		
		// DMJ: 10/19/2007: abs(amount) would not work with duplicate records.
		// DMJ: 10/31/2007: in the 10/19 change I had incorrectly referenced "amount", "quantity", etc.
		// Those values had been changed above to "sum(amount)", "sum(quantity)", etc.
		if lower(gl_column_name) = '"amount"' and lower(cr_column_name) <> "sum(amount)" and &
			lower(cr_column_name) <> "sum(quantity)" and lower(cr_column_name) <> "sum(decode(sign(amount),1,amount,0))" and &
			lower(cr_column_name) <> "sum(decode(sign(amount),-1,amount * -1,0))" &
		then
			cr_column_name =  "sum(" + cr_column_name + ")"
		end if
		
		if i = rtn then
			sqls = sqls + cr_column_name 
		else
			sqls = sqls + cr_column_name + ", "
		end if
		
	next 	
	
	sqls = sqls + " from cr_temp_cr_gl_temp " + where_clause + " group by "
		
	//	build the group by ...if no amount, quantity, group by acts as a distinct
	for i = 1 to rtn 
	
		cr_column_name = ds_post_to_gl.getitemstring(i,"cr_column_name")
		cr_column = ds_post_to_gl.getitemnumber(i,"cr_column")
		
		if cr_column = 1 then cr_column_name = f_cr_clean_string(cr_column_name)
		
		gl_column_name = ds_post_to_gl.getitemstring(i,"gl_column_name") // DMJ: 10/1/08
		
		//	DML: 072903: Blank values are being turned to underscores.  I need the
		//	replace command for the blanks in the column name if it is a CR column.
		//	However, if they just want a blank space don't turn it into an underscore.
		if cr_column_name = "'_'" then cr_column_name = "' '"
		
		//	if this is a CR column then put the upper and double quotes
		if cr_column = 1 and lower(cr_column_name) <> 'amount' and &
			lower(cr_column_name) <> 'quantity' and lower(cr_column_name) <> 'dr_amount' and &
			lower(cr_column_name) <> 'cr_amount' then
			cr_column_name = '"' + upper(cr_column_name) + '"'
		end if
		
		choose case lower(cr_column_name)
			//	DML: 092204: When posting to Oracle, we need a separate DR and CR amount
			//		columns.  cr_post_to_gl_columns will plug in dr_amount or cr_amount
			//		in the CR column name and this code will handle it.
			case 'amount','quantity','dr_amount','cr_amount'
				continue
		end choose
		
		// DMJ: 10/19/2007: abs(amount) would not work with duplicate records.
		if lower(gl_column_name) = "amount" and lower(cr_column_name) <> "amount" and &
			lower(cr_column_name) <> "quantity" and lower(cr_column_name) <> "dr_amount" and &
			lower(cr_column_name) <> "cr_amount" &
		then
			//  We just added a "sum()" above ... don't add this to the group by.
			continue
		end if
		
		// DMJ: 07/21/2008: need to support sub-selects ... if left(1) = "(" then don't
		// add to group by.
		if left(cr_column_name, 1) = "(" then
			continue
		end if
		
		// DMJ: 10/07/2008: need to support summing of non-standard fields ... 
		// e.g. "sum(decode( ... ))".
		if pos(upper(cr_column_name),"SUM(") > 0 then
			continue
		end if
		
		if i = rtn then
			sqls = sqls + cr_column_name
		else
			sqls = sqls + cr_column_name + ", "
		end if
		
	next 
	
	//	DML: 092204: If an amount/quantity field are last on the table the comma
	//		doesn't get trimmed.
	if right(trim(sqls),1) = ',' then
		sqls = mid(sqls,1,len(sqls) - 2)
	end if
	
	execute immediate :sqls;
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Inserting into " + i_stg_table_name + ": " + sqlca.sqlerrtext)
		rollback;
		f_pp_msgs(sqls)
		return -1
	end if
end if

i_array_counter++
i_posting_id_array[i_array_counter] = i_posting_id
if isnull(where_clause) or where_clause = "" then where_clause = 'nowhereclause'
i_where_clause_array[i_array_counter] = where_clause	
	
s_date = string(today(), "yyyy-mm-dd hh:mm:ss")

//	Provide an audit for this posting in the log and build the sqls string for cr_special_processing_dates2.
if where_clause = 'nowhereclause' then
	log_sqls = "select count(*), nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0) " + &
		"from cr_temp_cr_gl_temp " 
	spec_proc_dates2_sqls = "insert into cr_special_processing_dates2 " + &
		" (id, process_date, posting_id, total_dollars, total_records, total_debits, total_credits, batch_id) " + &
		" select 11, to_date('" + s_date + "','yyyy-mm-dd hh24:mi:ss'), " + string(i_posting_id) + ",nvl(sum(amount),0),count(*), " + &
		" nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0), " + string(g_batch_id) + &
		" from cr_temp_cr_gl_temp "
else
	log_sqls = "select count(*), nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0) " + &
		"from cr_temp_cr_gl_temp " + where_clause
	spec_proc_dates2_sqls = "insert into cr_special_processing_dates2 " + &
		" (id, process_date, posting_id, total_dollars, total_records, total_debits, total_credits, batch_id) " + &
		" select 11, to_date('" + s_date + "','yyyy-mm-dd hh24:mi:ss'), " + string(i_posting_id) + ",nvl(sum(amount),0),count(*), " + &
		" nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0), " + string(g_batch_id) + &
		" from cr_temp_cr_gl_temp " + where_clause
end if
i_ds_posting_audit.setsqlselect(log_sqls)	
i_ds_posting_audit.retrieve()
num_trans 	= i_ds_posting_audit.getitemnumber(1,1)
debits 		= i_ds_posting_audit.getitemdecimal(1,2)
credits 		= i_ds_posting_audit.getitemdecimal(1,3)

f_pp_msgs("")
f_pp_msgs("Posting " + string(num_trans, "#,##0;(#,##0)") + " transactions.")
f_pp_msgs("Total credits = " + string(credits, "$#,##0.00;($#,##0.00)"))
f_pp_msgs("Total debits = " + string(debits, "$#,##0.00;($#,##0.00)"))
f_pp_msgs("")

execute immediate :spec_proc_dates2_sqls;
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Inserting into cr_special_processing_dates2: " + sqlca.sqlerrtext)
	//	Having a rollback here means the posting will also rollback?  For now at least, it
	//		is all or nothing per posting including the audit table.
	rollback;
	f_pp_msgs(spec_proc_dates2_sqls)
	return -1
end if

return 1
end function

public function longlong uf_cc_id ();//*****************************************************************************************
//
//  Window Function  :  uf_cc_id
//
//	 Description 		: 	1) Inserts any new code block combinations into cr_gl_id
//								2) Update cr_temp_cr_gl with a gl_id
//							: 	DML: 032105: No longer update the gl_id and batch_id in this function
//								3) Backfill cr_cost_repository.gl_id to indicate it has been sent
//
//*****************************************************************************************
longlong file_rows, i, temp_rows, gl_id, rtn, struct_num, resp_appl_id, resp_id, &
       user_id, counter, id, max_gl_id, num_rows
string sqls, resp_name, user_name, field, gl_field

//setnull(obey_approval)
//select upper(trim(control_value)) into :obey_approval from cr_system_control
//where upper(trim(control_name)) = 'CR POSTING - OBEY APPROVAL';
//if isnull(obey_approval) or obey_approval = "" then
//	obey_approval = "NO"
//end if

max_gl_id = 0
select max(gl_id) into :max_gl_id from cr_gl_id;
if isnull(max_gl_id) then max_gl_id = 0

uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
sqls = 'select * from cr_elements order by "ORDER" '
f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)
num_rows = ds_elements.RowCount()

//insert new code block combos into cr_gl_id
sqls = "insert into cr_gl_id ("

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = uf_clean_string(field)
	
	if i = num_rows then
		sqls = sqls + '"' + upper(field) + '", gl_id ) '
	else
		sqls = sqls + '"' + upper(field) + '", '
	end if
next

sqls = sqls + " select a." 

for i = 1 to num_rows
	
	field = ds_elements.GetItemString(i, "description")
	field = uf_clean_string(field)
	
	if i = num_rows then
		sqls = sqls + '"' + upper(field) + '", '  + string(max_gl_id) + " + rownum as gl_id "
	else
		sqls = sqls + '"' + upper(field) + '", a.'
	end if
	
next

sqls = sqls + " from ( select distinct "

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = uf_clean_string(field)

	gl_field = ds_elements.GetItemString(i, "gl_element")
	if isnull(gl_field) or gl_field = "" then
		//not gl field so use space
		field = "' ' as " + upper(field)
	else
		field = '"' + upper(field) + '" '
	end if
	
	if i = num_rows then
		sqls = sqls + field + " from cr_temp_cr_gl) a, "
	else
		sqls = sqls + field + ', '
	end if
next

sqls = sqls + " ( select " 

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = uf_clean_string(field)
	
	if i = num_rows then
		sqls = sqls + '"' + upper(field) + '" ' + ", gl_id from cr_gl_id) b "
	else
		sqls = sqls + '"' + upper(field) + '", '
	end if
next

sqls = sqls + " where a."

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = uf_clean_string(field)
	
	sqls = sqls + '"' + upper(field) + '"' + " = b." + '"' + upper(field) + '"' + " (+) " 
	
	if i <> num_rows then
		sqls = sqls + " and a."
	end if
	
next

sqls = sqls + " and b.gl_id is null "

execute immediate :sqls;
if sqlca.sqlcode < 0 then
	f_write_log(g_log_file, "ERROR: Inserting into cr_gl_id: " + sqlca.sqlerrtext)
	f_pp_msgs("ERROR: Inserting into cr_gl_id: " + sqlca.sqlerrtext)
	f_pp_msgs(sqls)
	return sqlca.sqlcode
end if
	
// Clear out the cr_temp_cr_gl_ids table
f_pp_msgs(" -- deleting from cr_temp_cr_gl_ids ...")
sqlca.truncate_table('cr_temp_cr_gl_ids');
if sqlca.SQLCode < 0 then
		f_write_log(g_log_file, "Error: truncating cr_temp_cr_gl_ids: " + sqlca.SQLErrText)
		f_pp_msgs("Error: truncating cr_temp_cr_gl_ids: " + sqlca.SQLErrText)
		return sqlca.SQLCode
end if

// Insert into cr_temp_cr_gl_ids
f_pp_msgs(" -- inserting into cr_temp_cr_gl_ids ...")

sqls = "insert into cr_temp_cr_gl_ids (id, gl_id) ( " + &
	"select a.id, b.gl_id from cr_temp_cr_gl a, cr_gl_id b where "  

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = uf_clean_string(field)
	
	gl_field = ds_elements.GetItemString(i, "gl_element")
	if isnull(gl_field) or gl_field = "" then
		//not gl field so use space
		sqls = sqls + "' ' = b." + '"' + upper(field) + '"' 
	else
		sqls = sqls + 'a."' + upper(field) + '"' + " = b." + '"' + upper(field) + '"' 
	end if
	
	if i <> num_rows then
		sqls = sqls + " and "
	else
		sqls = sqls + ") "
	end if
	
next

execute immediate :sqls;
if sqlca.SQLCode < 0 then
		f_write_log(g_log_file, "Error: inserting into cr_temp_cr_gl_ids: " + sqlca.SQLErrText)
		f_pp_msgs("Error: inserting into cr_temp_cr_gl_ids: " + sqlca.SQLErrText)
		f_pp_msgs(sqls)
		return sqlca.SQLCode
end if

//	Assign the gl_id to cr_temp_cr_gl (table containing records to send to the GL) 
//		from cr_temp_cr_gl_ids (where the new code block combos where inserted)

f_pp_msgs(" -- updating cr_temp_cr_gl.gl_id ...")
update cr_temp_cr_gl a set gl_id = (
	select gl_id from cr_temp_cr_gl_ids c
	 where a.id = c.id);
if sqlca.SQLCode < 0 then
		f_write_log(g_log_file, "Error: updating cr_temp_cr_gl.gl_id: " + sqlca.SQLErrText)
		f_pp_msgs("Error: updating cr_temp_cr_gl.gl_id: " + sqlca.SQLErrText)
		return sqlca.SQLCode
end if

//// DML: 032105: At Atmos, they need to send the gl_id and batch_id
////	with the posting.  This function is called after all the postings
////	have completed.  The uf_cc_id function will be moved to right after
////	the insert into cr_temp_cr_gl but without the updates to cr_cost_repository
////	The updates to cr_cost_repository need to stay in their current location
////	so they do not fire until all postings have completed.
//
////  Back fill the cr_cost_repository.batch_id with this id ...
////	DML: 122004: If new transactions are loaded while this process is running
////		then this update could touch more records then desired.  We should 
////		have an id restriction similar to the restriction below on the gl_id update.
f_pp_msgs("Updating cr_temp_cr_gl.batch_id ...")

if i_obey_approval = 'YES' then
	update cr_temp_cr_gl set batch_id = :g_batch_id
	 where batch_id = 999999999999999 and id in (select id from cr_temp_cr_gl) using sqlca;
else
	update cr_temp_cr_gl set batch_id = :g_batch_id
	 where batch_id is NULL and id in (select id from cr_temp_cr_gl) using sqlca;
end if

if sqlca.SQLCode < 0 then
	f_write_log(g_log_file, "ERROR: Updating cr_temp_cr_gl.batch_id: " + &
	       sqlca.SQLErrText )
	f_pp_msgs("ERROR: Updating cr_temp_cr_gl.batch_id: " + &
	       sqlca.SQLErrText )
	return sqlca.SQLCode
end if
//			 
//
////	Backfill the cr_cost_repository.gl_id 
//f_pp_msgs("Updating cr_cost_repository.gl_id ...")
//update cr_cost_repository a set gl_id = (
//	select gl_id from cr_temp_cr_gl b
//    where a.id = b.id)
//	where batch_id = :g_batch_id; 
//  	 
// //where batch_id is null
// 
// if sqlca.SQLCode < 0 then
//		f_write_log(g_log_file, "Error: updating cr_cost_repository_gl.gl_id: " + sqlca.SQLErrText)
//		f_pp_msgs("Error: updating cr_cost_repository.gl_id: " + sqlca.SQLErrText)
//		return sqlca.SQLCode
//end if

f_pp_msgs("")

return 0
end function

public function longlong uf_read ();integer  rtn, gl_actual_or_budget, asys_rtn = 1, g
string   sqls, element, company, ferc, prov_area, targ_area, resource, task, sid, &
         work_order, cost_code, location_id, file_name, lookup_element, dataobject, &
         s_date, analyze_sqls, col1, col2, col3, col4, col5, col6, col7, col8, col9, &
         server_name, where_clause, to_user, &
         element_list, detail_element_list, table_name, rtns
boolean  write_text_file, view_failure, updated_gl_id, int_error = false, no_wc_found
longlong file_rows, i, view_rows, num_postings, l, counter, num_active_postings, overall_rtn
longlong cur_batch_id, source_id, sap_counter
date     ddate
time     ttime
datetime started_at, finished_at
dec {2} debit, credit
   
sqlca.truncate_table('cr_temp_cr_gl');

if sqlca.SQLCode < 0 then
   f_pp_msgs("ERROR: Truncating cr_temp_cr_gl: " + &
          sqlca.SQLErrText )
      rollback;
      overall_rtn = -1
      goto exit_and_return
end if
/*
// ### 8194  JAK: 2011-07-27:  Can't truncate in the loop below otherwise posting 1
//	could be saved and then posting 2 error.  Will truncate here to clear the high water mark
//	and then just delete below.
sqlca.truncate_table('cr_temp_cr_gl_temp');
	
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: Truncating cr_temp_cr_gl_temp: " + sqlca.SQLErrText )
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if
*/
i_ds_posting_audit = create uo_ds_top

sqls = "select count(*), sum(decode(sign(amount),-1,0,amount)), sum(decode(sign(amount),-1,amount,0)) from cr_temp_cr_gl where 1 = 0"
f_create_dynamic_ds(i_ds_posting_audit,"grid",sqls,sqlca,true)

//*****************************************************************************************
//
//  Method:  
//         1) Loop over cr_post_to_gl_columns and build the sql for
//            each required posting
//         2) Based on the output method, create a text file or a table
//
//*****************************************************************************************
//   0 as the posting_id indicates the function was called at the beginning
rtn = f_cr_posting(0,0)

if rtn < 0 then
      rollback;
      overall_rtn = -1
      goto exit_and_return
//      return -1
end if

f_pp_msgs("Setting up interface at " + string(now()))
setnull(i_obey_approval)
select upper(trim(control_value)) into :i_obey_approval from cr_system_control
where upper(trim(control_name)) = 'CR POSTING - OBEY APPROVAL';
if isnull(i_obey_approval) or i_obey_approval = "" then
   i_obey_approval = "NO"
end if

// JAK 20091210:  New system switch that will load cr_temp_cr_gl_temp from the detail tables
//   and as such will have the detail id, quantity, and amount
setnull(i_detail_posting)
select upper(trim(control_value)) into :i_detail_posting from cr_system_control
where trim(upper(control_name)) = 'CR POSTING - LOAD FROM DETAIL';
if isnull(i_detail_posting) or i_detail_posting = "" then
   i_detail_posting = "NO"
end if

// ### - SEB - 8616 - 091311: New system switch for standard SAP integration
setnull(i_use_standard_sap)
select upper(trim(control_value)) into :i_use_standard_sap from cr_system_control
 where upper(trim(control_name)) = 'CR POSTING: USE STANDARD SAP';
if isnull(i_use_standard_sap) or i_use_standard_sap = "" then
	i_use_standard_sap = "NO"
end if

// ### - SEB - 8616 - 091911: Create some datastores
if i_use_standard_sap = "YES" then
	// If this function is called, this version of CR Posting should not be used.  Write a 
	// message to the logs to indicate that they should be using the SAP Adapter version of 
	// CR Posting.
	
	f_pp_msgs("  ")
	f_pp_msgs("This is the incorrect version of CR Posting for use with the SAP Adapter!")
	f_pp_msgs("Please contact Release Management to get the correct version of this interface.")
	f_pp_msgs("  ")
	
	return -1
end if

if i_detail_posting = "YES" then
   i_ds_sources = CREATE uo_ds_top
   sqls = "select source_id, table_name from cr_sources order by 1"
   f_create_dynamic_ds(i_ds_sources, "grid", sqls, sqlca, true)
   
   i_ds_elements = CREATE uo_ds_top
   sqls = 'select * from cr_elements order by "ORDER"'
   f_create_dynamic_ds(i_ds_elements, "grid", sqls, sqlca, true)
   
   for i = 1 to i_ds_elements.rowcount()
      element_list = element_list + '"' + upper(f_cr_clean_string(i_ds_elements.getitemstring(i,'description'))) + '",'
      detail_element_list = detail_element_list + 'a."' + upper(f_cr_clean_string(i_ds_elements.getitemstring(i,'description'))) + '",'
   next
end if

f_pp_msgs("Checking for records to post at " + string(now()))
if i_obey_approval = 'YES' then
   //   DML: 092605: If the dr_cr_id is incorrect then the abs will cause
   //      an oob journal.  Although the dr_cr_id should be corrected it
   //      shouldn't impact posting to the GL.  So, take the amount as is.
   //select count(*), sum(decode(dr_cr_id,1,amount,0)), sum(decode(dr_cr_id,-1,amount,0)) 
   select count(*), sum(decode(sign(amount),-1,0,amount)), sum(decode(sign(amount),-1,amount,0)) 
   into :g_record_count, :debit, :credit
   from cr_cost_repository
   where batch_id = 999999999999999;
else
   select count(*), sum(decode(sign(amount),-1,0,amount)), sum(decode(sign(amount),-1,amount,0)) 
   into :g_record_count, :debit, :credit
   from cr_cost_repository
   where batch_id is null;
end if

if g_record_count = 0 then 
   f_pp_msgs("")
   f_pp_msgs("--------There are no transactions to be posted----------")
   f_pp_msgs("")
   goto the_end
end if

g_total_dollars = debit + credit

g_total_credits = credit
g_total_debits = debit

//   The total record count/debits/credits eligible to be posted doesn't mean
//      that is what will post since other criteria can be applied during
//      each posting.  Change the message to indicate this.
f_pp_msgs("")
//f_pp_msgs("Posting " + string(g_record_count) + " transactions to the General Ledger")
f_pp_msgs("There are " + string(g_record_count, "#,##0;(#,##0)") + " transactions eligible to be posted.")
f_pp_msgs("Total credits = " + string(credit, "$#,##0.00;($#,##0.00)"))
f_pp_msgs("Total debits = " + string(debit, "$#,##0.00;($#,##0.00)"))
f_pp_msgs("")

//   If there are no active postings terminate.
select count(*) into :num_active_postings
from cr_post_to_gl_control
where status = 1;
if isnull(num_active_postings) then num_active_postings = 0

if num_active_postings = 0 then
	//   There are no active postings
	f_pp_msgs("--------Although there are transactions to post, there are no active postings.----------")
	f_pp_msgs("")
	goto the_end
end if

if i_obey_approval = 'YES' then
	//   Get the unposted (batch_id = 999999999999999) from cr_cost_repository
	insert into cr_temp_cr_gl
	select * from cr_cost_repository where batch_id = 999999999999999;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Inserting unposted records into cr_temp_cr_gl : " + sqlca.sqlerrtext)
		rollback;
		overall_rtn = -1
		goto exit_and_return
	end if
else
	//   Get the unposted (batch_id = null) from cr_cost_repository
	insert into cr_temp_cr_gl
	select * from cr_cost_repository where batch_id is null;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Inserting unposted records into cr_temp_cr_gl : " + sqlca.sqlerrtext)
		rollback;
		overall_rtn = -1
		goto exit_and_return
	end if
end if

//   Analyze cr_temp_cr_gl
//   DML: 040506: It is ok to have an analyze here.  Firing a commit at this point 
//      should be ok although that might depend on what is being done in 
//      f_cr_posting(0,0) above.  If an error occurs, this table is cleared out
//      in the close event.
//sqls = 'analyze table cr_temp_cr_gl compute statistics'
//execute immediate :sqls;
sqlca.analyze_table('cr_temp_cr_gl');

//   Increment the batch_id
g_batch_id = '0'
select max(batch_id) into :g_batch_id 
  from cr_special_processing_dates where id = 11 using sqlca;
if isnull(g_batch_id) then g_batch_id = '0'
cur_batch_id = long(g_batch_id)
cur_batch_id++
g_batch_id = string(cur_batch_id)

//// DML: 032105: At Atmos, they need to send the gl_id and batch_id
////   with the posting.  Update it after the insert to cr_temp_cr_gl
////   so it is available to the posting but do not backfill cr_cost_repository 
////   until after all postings have successfully completed.
rtn = uf_cc_id()
if rtn < 0 then
   int_error = true
   rollback;
   goto clear_stg_table
//   return -1
end if

//   Retrieve the posting information from the posting control table
//   DML: 013006: Only pull active postings (active = 1, inactive = 0)
sqls = ' select posting_id, company, output_method, output_location, ' + &
       ' obey_gl_rollup, stg_table_name, description from cr_post_to_gl_control ' + &
       ' where status = 1 order by description '

uo_ds_top ds_post_to_gl_columns
ds_post_to_gl_columns = create uo_ds_top

f_create_dynamic_ds(ds_post_to_gl_columns,"grid",sqls,sqlca,true)
num_postings = ds_post_to_gl_columns.rowcount()


// ### 8194  JAK: 2011-07-27:  Can't truncate in the loop below otherwise posting 1
//	could be saved and then posting 2 error.  Will truncate before processing anything to clear the high water mark
//	and then just delete below.
for l = 1 to num_postings
	i_stg_table_name = ds_post_to_gl_columns.getitemstring(l,"stg_table_name")
	sqlca.truncate_table(i_stg_table_name);
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: Truncating " + i_stg_table_name + ": " + sqlca.SQLErrText )
		rollback;
		overall_rtn = -1
		goto exit_and_return
	end if
next

for l = 1 to num_postings
   
   i_description = ds_post_to_gl_columns.getitemstring(l,"description")
   i_company = ds_post_to_gl_columns.getitemstring(l,"company")
   i_posting_id = ds_post_to_gl_columns.getitemnumber(l,"posting_id")
   i_output_method = ds_post_to_gl_columns.getitemnumber(l,"output_method")
   i_output_location = ds_post_to_gl_columns.getitemstring(l,"output_location")
   i_obey_gl_rollup = ds_post_to_gl_columns.getitemnumber(l,"obey_gl_rollup")
   i_stg_table_name = ds_post_to_gl_columns.getitemstring(l,"stg_table_name")
   
   f_pp_msgs("Posting: " + i_description + " ... " + string(l) + " of " + string(num_postings))

//   g_do_not_write_batch_id = true
	
	// ### 8194  JAK: 2011-07-27:  Can't truncate in the loop otherwise posting 1
	//	could be saved and then posting 2 error.  Will truncate at the top to clear the high water mark
	//	and then just delete here.
	
   //   cr_temp_cr_gl_temp needs to be refreshed each time through the loop
   //   b/c the ability to roll up elements means this table could be different
   //   for each posting depending on the posting parameters.
//   sqlca.truncate_table('cr_temp_cr_gl_temp');
   sqls = 'delete from cr_temp_cr_gl_temp'
	execute immediate :sqls;
	
   if sqlca.SQLCode < 0 then
      f_pp_msgs("ERROR: Deleting cr_temp_cr_gl_temp: " + sqlca.SQLErrText )
      rollback;
      overall_rtn = -1
      goto exit_and_return
//      return -1
   end if
   
//   sqlca.truncate_table(i_stg_table_name);
   sqls = 'delete from ' + i_stg_table_name
	execute immediate :sqls;
   
   if sqlca.SQLCode < 0 then
      f_pp_msgs("ERROR: Deleting " + i_stg_table_name + ": " + sqlca.SQLErrText )
      rollback;
      overall_rtn = -1
      goto exit_and_return
//      return -1
   end if
      
   if i_detail_posting = 'YES' then
      // Load cr_temp_cr_gl_temp from the detail tables
      
      sqls = "insert into cr_temp_cr_gl_temp ("
      sqls = sqls + element_list
      sqls = sqls + "ID, DR_CR_ID,  LEDGER_SIGN,  QUANTITY,  AMOUNT,  MONTH_NUMBER,  MONTH_PERIOD,  " + &
         "GL_JOURNAL_CATEGORY,  SOURCE_ID,  AMOUNT_TYPE,  DRILLDOWN_KEY,  GL_ID,  BATCH_ID,  " + &
         "CWIP_CHARGE_STATUS,  CR_ID,  BALANCES_ID) ("
         
      for i = 1 to i_ds_sources.rowcount()
         source_id = i_ds_sources.getitemnumber(i,"source_id")
         table_name = i_ds_sources.getitemstring(i,"table_name")
         
         sqls = sqls + " SELECT " + detail_element_list
         sqls = sqls + "a.ID, a.DR_CR_ID,  a.LEDGER_SIGN,  a.QUANTITY,  a.AMOUNT,  a.MONTH_NUMBER,  a.MONTH_PERIOD,  " + &
            "a.GL_JOURNAL_CATEGORY,  b.SOURCE_ID,  a.AMOUNT_TYPE,  a.DRILLDOWN_KEY,  b.GL_ID,  b.BATCH_ID,  " + &
            "b.CWIP_CHARGE_STATUS,  b.CR_ID,  b.BALANCES_ID FROM " + table_name + " a, cr_temp_cr_gl b " + &
            "where a.drilldown_key = b.drilldown_key and b.source_id = " + string(source_id)  + " UNION ALL "
      next

      sqls = mid(sqls,1,len(sqls) - 10) + ")"
      
      execute immediate :sqls;
   else
      insert into cr_temp_cr_gl_temp
      select * from cr_temp_cr_gl;
   end if
   
   if sqlca.SQLCode < 0 then
      f_pp_msgs("ERROR: Inserting into cr_temp_cr_gl_temp: " + sqlca.SQLErrText )

      rollback;
      overall_rtn = -1
      goto exit_and_return
//      return -1
   end if
   
   //   DML: 032105: Call the custom function before the rollup occurs.
   rtn = f_cr_posting(0,i_posting_id)

   if rtn < 0 then
      rollback;
      int_error = true
      goto clear_stg_table
   end if
   
   //   wf_update_summary_values looks up the budget_value (really the GL Value)
   //   from cr_elements and updates cr_temp_cr_gl_temp with this rollup value.  It is 
   //   then summarized in the uf_insert_post_to_gl function.  Since this 
   //   interface can be used to post to different systems, we should check to 
   //   see if this particular posting should obey the rollup flag.
   
   //   RULES
   //   obey_gl_rollup = 0 means no
   //   obey_gl_rollup = 1 means yes
   
   if i_obey_gl_rollup = 1 then   
      
      rtn = uf_update_summary_values()
      if rtn < 0 then
         int_error = true
         goto clear_stg_table
//         return -1
      end if
      
   end if
   
//   Build the insert command and insert into the posting stage table
   rtn = uf_insert_post_to_gl()
   if rtn < 0 then
      int_error = true
      goto clear_stg_table
//      return -1
   end if
   
   //   Call the custom function now that the data is in the staging table
   //   but before the file is created or the insert into the posting table 
   //   is complete.
   rtn = f_cr_posting(1,i_posting_id)

   if rtn < 0 then
      rollback;
      int_error = true
      goto clear_stg_table
//      return -1
   end if
   
   //   Create the file or insert into the posting table
	rtn = uf_create_file_or_table()
	
   if rtn < 0 then
      int_error = true
      goto clear_stg_table
//      return -1
   end if
   
   //   uf_create_file_or_table was successful.  Another crack at the custom function
    rtn = f_cr_posting(3,i_posting_id)

    if rtn < 0 then
      int_error = true
      goto clear_stg_table
//      return -1
   end if    
   
//   commit; HOLD COMMIT UNTIL ALL SUCCEED (after next)
   
next

commit; // everything good so far so commit the interface rows.

////   Backfill cr_cost_repository.gl_id/batch_id
f_pp_msgs("Updating cr_cost_repository.batch_id ...")
//update cr_cost_repository set batch_id = :g_batch_id
// where batch_id is NULL and id in (select id from cr_temp_cr_gl) using sqlca;

//   Loop over the where clause array.  If I find any where_clause = 'nowhereclause' then I 
//   know there is a posting that hit every record eligible.  Therefore, I only have to fire
//   one update against cr_cost_repository that hits all the eligible rows.  This would happen
//   if they checked "All Companies" and "All Journal Categories".
for g = 1 to upperbound(i_where_clause_array)
   
   where_clause = i_where_clause_array[g]
   if where_clause = 'nowhereclause' then
      no_wc_found = true
   end if
   
next

if no_wc_found then
   //   At least one of the postings hit all the records eligible to post.
   
   //   The cr_temp_cr_gl restriction is to make sure we don't update any new 
   //   rows that might have been loaded while this process was executing.
   //   The id restriction is needed to make sure we don't mark any rows that 
   //   were loaded into cr_cost_repository while cr_posting is still running.
   if i_obey_approval = 'YES' then
      sqls = "update cr_cost_repository set batch_id = " + string(g_batch_id) + &
              "where batch_id = 999999999999999 and id in (select id from cr_temp_cr_gl) "
   else
      sqls = "update cr_cost_repository set batch_id = " + string(g_batch_id) + &
              "where batch_id is NULL and id in (select id from cr_temp_cr_gl) "
   end if
    
   execute immediate :sqls;

   if sqlca.SQLCode < 0 then
      f_pp_msgs("ERROR: Updating cr_cost_repository.batch_id: " + &
             sqlca.SQLErrText + "The interface records WERE created.")
      f_pp_msgs("SQLS: " + sqls)
      rollback;
      int_error = true
      goto clear_stg_table
   end if
   
else   //   All postings have a where clause restriction.
   
   for g = 1 to upperbound(i_where_clause_array)
      
      where_clause = i_where_clause_array[g]
      
      if i_obey_approval = 'YES' then
         sqls = "update cr_cost_repository set batch_id = " + string(g_batch_id) + ' ' + &
          where_clause + " and batch_id = 999999999999999 and id in (select id from cr_temp_cr_gl) "
      else
         sqls = "update cr_cost_repository set batch_id = " + string(g_batch_id) + ' ' + &
          where_clause + " and batch_id is NULL and id in (select id from cr_temp_cr_gl) "
      end if
       
      execute immediate :sqls;
 
      if sqlca.SQLCode < 0 then
         f_pp_msgs("ERROR: Updating cr_cost_repository.batch_id: " + &
                sqlca.SQLErrText + "The interface records WERE created.")
         f_pp_msgs("SQLS: " + sqls)
         rollback;
         int_error = true
         goto clear_stg_table
      end if
      
   next
   
end if
       

//   Backfill the cr_cost_repository.gl_id 
//   A where clause restriction similar to batch_id is not needed for the gl_id because
//      there is a restriction that the batch_id equals what we just filled in above effectively
//      acting as our where clause.
f_pp_msgs("Updating cr_cost_repository.gl_id ...")
update cr_cost_repository a set gl_id = (
	select gl_id from cr_temp_cr_gl b
	where a.id = b.id)
	where batch_id = :g_batch_id; 

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: Updating cr_cost_repository.gl_id: " + &
		sqlca.SQLErrText + "The interface records WERE created.")
	rollback;
	int_error = true
	goto clear_stg_table
end if


//   Clear out the "main" temp table.
sqlca.truncate_table('cr_temp_cr_gl');

if sqlca.sqlcode < 0 then
   f_pp_msgs("ERROR: Truncating cr_temp_cr_gl : " + sqlca.sqlerrtext)
   rollback;
   int_error = true
   goto clear_stg_table
end if

clear_stg_table:

destroy i_ds_posting_audit

for l = 1 to num_postings
   
   i_stg_table_name = ds_post_to_gl_columns.getitemstring(l,"stg_table_name")
   
   sqlca.truncate_table(i_stg_table_name);
   
   if sqlca.sqlcode < 0 then
      f_pp_msgs("ERROR: Truncating " + i_stg_table_name + " : " + sqlca.sqlerrtext)
      rollback;
      overall_rtn = -1
      goto exit_and_return
//      return -1
   end if
   
next

if int_error then
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

//   Call a custom function for any additional processing.
//   -1 as the posting_id indicates the function was called at the end
rtn = f_cr_posting(2,-1)

if rtn<0 then 
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

exit_and_return:
if overall_rtn < 0 then
	rollback;
	//   Find the user (from cr_system_control) to send a failure e-mail to.
	select upper(control_value) into :to_user
	from cr_system_control
	where upper(control_name) = 'CR POSTING ERROR EMAIL RECIPIENT';
   
	if isnull(to_user) or to_user = "" or to_user = 'NONE' then
	else
		g_msmail = create uo_smtpmail
		uo_winapi = create u_external_function_win32
		f_send_mail('pwrplant','INTERFACE FAILURE','The CR POSTING interface ' + &
			'encountered an error and terminated.',to_user)
	end if
	
	return -1
else 
   //  Hard Code 11 - "The interface to GL" ...
	s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
	ddate  = date(left(s_date, 10))
	ttime  = time(right(s_date, 8))
	
   	finished_at = datetime(ddate, ttime)

   
   insert into cr_special_processing_dates
      (id, process_date, total_dollars, total_debits, total_credits, total_records, batch_id)
   values
      (11, :finished_at, :g_total_dollars, :g_total_debits, :g_total_credits, :g_record_count, :g_batch_id)
   using sqlca;
      
   if sqlca.SQLCode <> 0 then
      f_pp_msgs("ERROR: Updating cr_special_processing_dates: " + &
             sqlca.SQLErrText + "The interface records WERE created.")
      rollback using sqlca;
   else
      commit using sqlca;
   end if
end if

commit;
the_end:
return 1

end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_posting_custom_version nvo_cr_posting_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_posting_custom.pbd'
		return nvo_cr_posting_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

