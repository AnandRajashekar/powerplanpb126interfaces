HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong i_bv_id, i_year, i_component_id, i_cr_budget_factor_id, i_cr_budget_fixed_factor_id, &
		  i_bv_start_year, i_bv_budget_year, i_bv_end_year, i_actuals_month, i_template_id, &
		  i_step2_row_index, i_step2_column_index, i_open_for_entry, i_rclick_row, &
		  i_skip_value, i_formatting_start, i_start_year, i_end_year, i_version_years[], i_spread_years[], &
		  i_structure_id, i_handles[], i_clicked_item_handle
string  i_dept_element, i_dept_value, i_element_for_rows, i_element_table_for_rows,i_sle_dept, &
		  i_element_for_cols, i_bdg_element_for_cols, i_element_table_for_cols, i_dd_key,i_element_value,  &
		  i_step2_row, i_step2_column, i_company_field, i_company_value, i_step2_alloc_field, &
		  i_alternate_entry, i_use_new_structures_table, i_col_array[], i_sort_string, i_alt_entry_direct_table, &
		  i_text_band_computed[], i_col_array_for_summary_band[], i_alias_array[], i_dept_element_bdg, i_dept_element_act, &
		  i_values[], i_label, i_rollup_value_id_list, i_click_or_hover, i_enforce_active_entry, i_default_company, i_col_name
boolean i_from_buttons, i_step1_btns_enabled, i_updated = true, i_step2_validate, &
		  i_cb_compute_ran, i_finish_button, i_labor_all_years = true, i_calc_all_in_progress, i_dw_entry_created, i_called_from_finish
decimal {2} i_step2_control_total, i_alloc_amount, i_alloc_row, i_amount_to_spread
string      i_menu_items[], i_menu_events[], i_rclick_column, i_rclick_rowval, i_amt_qty_both_filter, i_year_filter, i_spread_all_or_one
string 	i_element_group, i_element_column, i_element_table, i_spread_amt_qty_both, i_selection_screen_sqls, i_selection_screen_sqls_where, &
			i_derivations_or_validations, i_ifb_id, i_selection_screen_dept_sqls, i_selection_screen_labor_sqls, i_selection_screen_dept_where, &
			i_element_datawindow, i_selection_screen_dept_suppla_where, i_selection_screen_dept_supplb_where, i_selection_screen_dept_where_no_yr, &
			i_selection_screen_sqls_no_yr
longlong	i_element_id, i_labor_year,i_search_row,i_row, i_view_only, i_calc_all_rtn, i_budget_value_type_id, i_template_year_restriction, i_y1, i_y2, i_appr_counter, i_addl_fields_clicked_row
string	i_coltype, i_colname, i_search_table, i_search_field, i_return_value, i_element_array_descr[], i_element_table_array_descr[], i_ack_filter, i_addl_fields_clicked, &
			i_addl_sql_price_qty, i_gross_up_filter
boolean	i_do_column, i_called_from_calc_all, i_called_from_calc_validations, i_called_from_calc_derivations, i_control_total_filter, &
			i_retrieve_entire_group, i_previous, i_view_options, i_send_for_approval, i_called_from_send_for_appr, i_using_approvals, i_approved,&
			i_item_changed_rtn_val, i_gross_up_filter_applied, i_zero_out_row
			
boolean	i_obey_disable_month = false, i_have_disabled_month
			
uo_ds_top i_ds_elements, i_ds_where
uo_cr_derivation i_uo_cr_derivation
uo_cr_validation_bdg i_uo_cr_validation_bdg
uo_ds_top i_cr_deriver_type_sources_budget
uo_ds_top i_cr_deriver_type_sources
uo_ds_top i_cr_element_descr_lookup
uo_ds_top i_ds_labels

string i_exe_name = 'cr_budget_entry_calc_all.exe'
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_update ()
public function longlong uf_run ()
public function longlong uf_derivations ()
public function longlong uf_validations ()
public function longlong uf_apply_default_values ()
public function longlong uf_price_qty ()
public function longlong uf_calc_all_gross_up ()
public function longlong uf_allocations ()
public function longlong uf_calculations ()
public function longlong uf_reverse_price_qty ()
public function longlong uf_setup ()
public function longlong uf_build_dw_entry ()
public function longlong uf_build_cr_budget_data (string a_type)
public function longlong uf_add_missing_amt_rows ()
public function longlong uf_calc_totals_all ()
public function string uf_calculations_missing_combos_sqls (longlong a_rate_override_id)
public function string uf_build_calculations_where_clause (longlong a_where_clause_id)
public function longlong uf_parse (string a_stringtoparse, string a_separator, ref string a_array_to_hold[])
public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field)
public function longlong uf_add_missing_qty_rows ()
public function longlong uf_finish ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

longlong msg_rtn, num_calc_items, calc_all_array_id[], g, rtn, i, empty_array[]
string calc_all_array_descr[], sqls, msg_str, rtns, obey_disable_month, bv, template_descr

// Perform some initial setup that was performed in the open event of w_cr_budget_entry_alt
setnull(i_element_for_rows)
setnull(i_element_table_for_rows)
select element_table, element_column into :i_element_table_for_rows, :i_element_for_rows from cr_elements
 where budget_step1_rows = 1;
if isnull(i_element_for_rows) then i_element_for_rows = ""

setnull(i_element_for_cols)
setnull(i_element_table_for_cols)
select element_table, element_column, budgeting_element
  into :i_element_table_for_cols, :i_element_for_cols, :i_bdg_element_for_cols
  from cr_elements
 where budget_step1_column = 1;
if isnull(i_element_for_cols)     then i_element_for_cols     = ""
if isnull(i_bdg_element_for_cols) then i_bdg_element_for_cols = ""

//  I'm not sure what we would do if there is more than 1 of these ... currently this variable
//  is only referenced in f_cr_budget_step2_calc, so technically, they could have more than
//  one field that is allocated to in the "Detail" step.  The customizable DW would be set up
//  to handle the multiple fields, and the only thing that would fail is the above function
//  that allocates calculated values down to the detail level.  However, that function is 
//  customizable, so you could probably get around it.
setnull(i_step2_alloc_field)
select budgeting_element into :i_step2_alloc_field
  from cr_elements
 where budget_step2_alloc = 1;
if isnull(i_step2_alloc_field)    then i_step2_alloc_field     = ""

//  Get the name of the "company" field in the code block.
setnull(i_company_field)
select upper(rtrim(control_value)) into :i_company_field from cr_system_control
 where upper(rtrim(control_name)) = 'COMPANY FIELD';
if isnull(i_company_field) or i_company_field = "" then
	messagebox("Budget Entry", "Cannot find the COMPANY FIELD in cr_system_control.")
end if

//	Are we using the alternate entry method?
select upper(rtrim(control_value)) into :i_alternate_entry from cr_system_control 
where upper(control_name) = 'CR BUDGET: ALTERNATE ENTRY';
if isnull(i_alternate_entry) or i_alternate_entry = "" then
	messagebox("Budget Entry", "Cannot find the entry type in cr_system_control.")
end if

//	For the alternate entry method, are we using the direct table entry or the "ATC Method"?
select upper(rtrim(control_value)) into :i_alt_entry_direct_table from cr_system_control 
where upper(control_name) = 'CR BUDGET: DIRECT TABLE ENTRY';
if isnull(i_alt_entry_direct_table) or i_alt_entry_direct_table = "" then
	messagebox("Budget Entry", "Cannot find the entry type (2) in cr_system_control.")
end if

//	For displaying the descriptions.  Do I display when hovering on a value or clicking on a value?
select upper(rtrim(control_value)) into :i_click_or_hover from cr_system_control 
where upper(control_name) = 'CR BUDGET DESCR: CLICK OR HOVER';
if isnull(i_click_or_hover) or i_click_or_hover = "" then
	messagebox("Budget Entry", "Cannot find the description display type in cr_system_control.")
	i_click_or_hover = 'HOVER'
end if

setnull(i_use_new_structures_table)
select upper(rtrim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(rtrim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"

select upper(rtrim(control_value)) into :obey_disable_month from cr_system_control
 where upper(rtrim(control_name)) = 'BDG ENTRY - OBEY DISABLE MONTH';
if isnull(obey_disable_month) then obey_disable_month = "NO"

if obey_disable_month = "YES" then i_obey_disable_month = true

i_cr_deriver_type_sources = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources"
f_create_dynamic_ds(i_cr_deriver_type_sources, "grid", sqls, sqlca, true)

i_cr_deriver_type_sources_budget = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources_budget"
f_create_dynamic_ds(i_cr_deriver_type_sources_budget, "grid", sqls, sqlca, true)

i_cr_element_descr_lookup = CREATE uo_ds_top
sqls = "select min(description) from cr_elements where 1 = 0"
f_create_dynamic_ds(i_cr_element_descr_lookup, "grid", sqls, sqlca, true)

rtn = f_cr_budget_entry_custom('OPEN')

if rtn < 0 then
	return -1
end if

// Get the budget versions and templates to loop over
sqls = "select cr_budget_version_id, template_id from cr_bdg_entry_calc_all_schedule"

uo_ds_top ds_schedule
ds_schedule = CREATE uo_ds_top

rtns = f_create_dynamic_ds(ds_schedule,'grid',sqls,sqlca,true)

if rtns <> 'OK' then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Creating ds_schedule: " + rtns)
	f_pp_msgs("--i_sqlca_sqlcode: " + string(ds_schedule.i_sqlca_sqlcode))
	f_pp_msgs("--i_sqlca_sqlerrtext: " + ds_schedule.i_sqlca_sqlerrtext)
	f_pp_msgs("--i_sqlca_sqlsyntax: " + ds_schedule.i_sqlca_sqlsyntax)
end if

for i = 1 to ds_schedule.rowcount()

	i_bv_id = ds_schedule.GetItemNumber(i,1)
	i_template_id = ds_schedule.GetItemNumber(i,2)
	
//	g_bv_id = i_bv_id
	
	select budget_version into :bv from cr_budget_version where cr_budget_version_id = :i_bv_id;
	if isnull(bv) then bv = ""
	
	select description into :template_descr from cr_budget_templates where template_id = :i_template_id;
	if isnull(template_descr) then template_descr = ""
	
	if bv = "" and template_descr = "" then
		f_pp_msgs("Processing cr budget version id '" + string(i_bv_id) + "' and template id '" + string(i_template_id) + "' at " + string(now()))
	elseif bv = "" and template_descr <> "" then
		f_pp_msgs("Processing cr budget version id '" + string(i_bv_id) + "' and template '" + template_descr + "' at " + string(now()))
	elseif bv <> "" and template_descr = "" then
		f_pp_msgs("Processing budget version '" + bv + "' and template id '" + string(i_template_id) + "' at " + string(now()))
	else
		f_pp_msgs("Processing budget version '" + bv + "' and template '" + template_descr + "' at " + string(now()))
	end if

	// Call uf_setup to assign values to all of the necessary instance variables
	rtn = uf_setup()
	
	if rtn = -1 then
		return -1
	end if
	
	sqls  = "select a.id,b.description,a.run_order from cr_budget_calc_all_template a, cr_budget_calc_all_control b where a.id = b.id and template_id = " + string(i_template_id) + &
		" order by run_order "
	uo_ds_top ds_calc_items
	ds_calc_items = create uo_ds_top
	f_create_dynamic_ds(ds_calc_items,"grid",sqls,sqlca,true)
	num_calc_items = ds_calc_items.rowcount()
	
	if isnull(num_calc_items) or num_calc_items = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: There are no Calc All items defined for this template!")
		f_pp_msgs("  ")
		return -1
	end if
	
	ds_calc_items.setsort("#3 A")
	ds_calc_items.sort()
	
	calc_all_array_id = empty_array
	
	for g = 1 to num_calc_items
		calc_all_array_id[g] = ds_calc_items.getitemnumber(g,1)
		calc_all_array_descr[g] = ds_calc_items.getitemstring(g,2)
	next
	
	f_pp_msgs("The following processes will be run in the order displayed: ")
	for g = 1 to upperbound(calc_all_array_id)
		f_pp_msgs("--" + calc_all_array_descr[g])
	next
	
	i_called_from_calc_all = true
	
	for g = 1 to upperbound(calc_all_array_id)
		choose case calc_all_array_id[g]
			case 1
				//	Derivations
				f_pp_msgs("Starting to run Derivations at " + string(now()))
				i_calc_all_rtn = 1
				i_called_from_calc_validations = false
				i_called_from_calc_derivations = true
				rtn = uf_run()
				do while i_calc_all_in_progress
					//	used to prevent moving on to the next calc
				loop
				if i_calc_all_rtn = -1 or rtn = -1 then 
					f_pp_msgs("  ")
					f_pp_msgs("An error occurred during the Derivations step of the Calc All process.")
					f_pp_msgs("The remaining steps will not be processed.")
					f_pp_msgs("  ")
					return -1
				else
					f_pp_msgs("Derivations completed successfully at " + string(now()))
				end if
				rtn = uf_update()
				if rtn = -1 then
					return -1
				end if
			case 2
				//	Validations
				f_pp_msgs("Starting to run Validations at " + string(now()))
				i_calc_all_rtn = 1
				i_called_from_calc_validations = true
				i_called_from_calc_derivations = false
				rtn = uf_run()
				do while i_calc_all_in_progress
					//	used to prevent moving on to the next calc
				loop
				if i_calc_all_rtn = -1 or rtn = -1 then 
					f_pp_msgs("  ")
					f_pp_msgs("An error occurred during the Validations step of the Calc All process.")
					f_pp_msgs("The remaining steps will not be processed.")
					f_pp_msgs("  ")
					return -1
				else
					f_pp_msgs("Validations completed successfully at " + string(now()))
				end if
				rtn = uf_update()
				if rtn = -1 then
					return -1
				end if
			case 3
				//	Price Quantity
				f_pp_msgs("Starting to run Price Quantity at " + string(now()))
				i_calc_all_rtn = 1
				rtn = uf_price_qty()
				do while i_calc_all_in_progress
					//	used to prevent moving on to the next calc
				loop
				if i_calc_all_rtn = -1 or rtn = -1 then
					f_pp_msgs("  ")
					f_pp_msgs("An error occurred during the Price Quantity step of the Calc All process.")
					f_pp_msgs("The remaining steps will not be processed.")
					f_pp_msgs("  ")
					return -1
				else
					f_pp_msgs("Price Quantity completed successfully at " + string(now()))
				end if
				rtn = uf_update()
				if rtn = -1 then
					return -1
				end if
			case 4
				//	Gross Up
				f_pp_msgs("Starting to run Gross Up at " + string(now()))
				i_calc_all_rtn = 1
				rtn = uf_calc_all_gross_up()
				//	don't need the loop here because using a function.
				if i_calc_all_rtn = -1 or rtn = -1 then 
					f_pp_msgs("  ")
					f_pp_msgs("An error occurred during the Gross Up step of the Calc All process.")
					f_pp_msgs("The remaining steps will not be processed.")
					f_pp_msgs("  ")
					return -1
				else
					f_pp_msgs("Gross Up completed successfully at " + string(now()))
				end if
				rtn = uf_update()
				if rtn = -1 then
					return -1
				end if
			case 5
				//	Allocations
				f_pp_msgs("Starting to run Allocations at " + string(now()))
				i_calc_all_rtn = 1
				rtn = uf_allocations()
				do while i_calc_all_in_progress
					//	used to prevent moving on to the next calc
				loop
				if i_calc_all_rtn = -1 or rtn = -1 then 
					f_pp_msgs("  ")
					f_pp_msgs("An error occurred during the Allocations step of the Calc All process.")
					f_pp_msgs("The remaining steps will not be processed.")
					f_pp_msgs("  ")
					return -1
				else
					f_pp_msgs("Allocations completed successfully at " + string(now()))
				end if
				rtn = uf_update()
				if rtn = -1 then
					return -1
				end if
			case 6
				//	Calculations
				f_pp_msgs("Starting to run Calculations at " + string(now()))
				i_calc_all_rtn = 1
				rtn = uf_calculations()
				do while i_calc_all_in_progress
					//	used to prevent moving on to the next calc
				loop
				if i_calc_all_rtn = -1 or rtn = -1 then 
					f_pp_msgs("  ")
					f_pp_msgs("An error occurred during the Calculations step of the Calc All process.")
					f_pp_msgs("The remaining steps will not be processed.")
					f_pp_msgs("  ")
					return -1
				else
					f_pp_msgs("Calculations completed successfully at " + string(now()))
				end if
				rtn = uf_update()
				if rtn = -1 then
					return -1
				end if
			case 7
				// Reverse Price Quantity
				f_pp_msgs("Starting to run Reverse Price Quantity at " + string(now()))
				i_calc_all_rtn = 1
				rtn = uf_reverse_price_qty()
				do while i_calc_all_in_progress
					//	used to prevent moving on to the next calc
				loop
				if i_calc_all_rtn = -1 or rtn = -1 then 
					f_pp_msgs("  ")
					f_pp_msgs("An error occurred during the Reverse Price Quantity step of the Calc All process.")
					f_pp_msgs("The remaining steps will not be processed.")
					f_pp_msgs("  ")
					return -1
				else
					f_pp_msgs("Reverse Price Quantity completed successfully at " + string(now()))
				end if
				rtn = uf_update()
				if rtn = -1 then
					return -1
				end if
			case else
				//	Unknown
				f_pp_msgs("ERROR: Unknown id : " + string(calc_all_array_id[g]))
				return -1
		end choose
	next  // for g = 1 to upperbound(calc_all_array_id)
	
	i_called_from_calc_all = false
	
	commit;
	
	// After Calc All has finished for a budget version, send it over to cr_budget_data
	f_pp_msgs("Starting to run the Finish process at " + string(now()))
	rtn = uf_finish()
	
	if rtn < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("An error occurred during the Finish process.")
		f_pp_msgs("  ")
		return -1
	elseif rtn = 2 then
		f_pp_msgs("The next budget version will be processed without submitting the current version to cr_budget_data.")
		continue
	end if
	
	commit;
	
next // for i = 1 to ds_schedule.rowcount()

return 1
end function

public function longlong uf_update ();longlong rtn, selected_tab, num_rows, i
decimal {2} alloc_amount, total, mo_total
string sqls, element, month_number_str, insert_sqls, select_sqls, select_sqls_hold, &
	insert_sqls_hold, budget_version, gl_jrnl_cat, delete_sqls, delete_sqls_hold
longlong num_elements, g, id, min_year, max_year, yr, month, total_month_counter, &
	budget_step2_alloc, gg
dec {2} month_amount
datawindow dw_entry

rtn = f_cr_budget_entry_custom('BEFORE UPDATE')
if rtn < 0 then
	return -1
end if
rtn = 0

//rtn = tab_entry.tabpage_alternate_entry.dw_entry.update()
if rtn < 0 then
	f_pp_msgs("ERROR: " + sqlca.sqlerrtext)
	rollback;
//	i_send_for_approval = false
	i_calc_all_in_progress = false
	return -1
else
	rtn = f_cr_budget_entry_custom('AFTER UPDATE')
	if rtn < 0 then
		i_calc_all_in_progress = false
		return -1
	end if
	rtn = 0
	commit;
//	i_updated = true
//	i_send_for_approval = true
end if

return 1
end function

public function longlong uf_run ();longlong rtn
boolean run_derivations, run_validations
string msg_box


i_calc_all_in_progress = true
if i_called_from_calc_validations then 
	run_validations = true
else
	run_validations = false
end if
if i_called_from_calc_derivations then 
	run_derivations = true
else
	run_derivations = false
end if

if run_derivations then

	rtn = uf_derivations()
	if rtn < 0 then 
		i_calc_all_rtn = -1
		i_calc_all_in_progress = false
		return -1
	end if
	
	////	Apply the default value from cr_elements for anything still with a blank space
	rtn = uf_apply_default_values()
	if rtn < 0 then 
		i_calc_all_rtn = -1
		i_calc_all_in_progress = false
		return -1
	end if
	
end if

if run_validations then

	rtn = uf_validations()
	if rtn = -1 then 
		i_calc_all_rtn = -1
		i_calc_all_in_progress = false
		return -1
	end if


end if

i_calc_all_in_progress = false

return 1
end function

public function longlong uf_derivations ();//**********************************************************************************************
//
//  User Object Function  :  uf_derivations
//
//  Description           :  Calls base derivations as identified in cr_deriver_type_sources.
//
//  Notes                 :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!
//
//  cr_deriver_type_sources
//  -----------------------
//  TYPE              :  the derivation type (same as entered in cr_deriver_control)
//  SOURCE_ID         :  from cr_sources ... identifies the table to apply derivations against
//  RUN_ORDER         :  for this source_id, the order in which the derivations should run
//  BASIS             :  for update derivations,
//                          the a_join_field argument ... if multiples, comma-separate them
//                       for insert derivations,
//                          the a_join argument ... which is the where clause ... use
//                          "cr." as the prefix for the transaction table
//  INSERT_OR_UPDATE  :  'Update' or 'Insert' to identify which kind of derivation this is
//  DELIMITER         :  for update types only ... the delimiter separating the basis fields
//  OFFSET_TYPE       :  for insert types only ... the type value for the offsets ... if null
//                       then you need a separate record for the offset type (although both
//                       sets of records would be tagged as TARGET)
//  OFFSET_BASIS      :  see BASIS ... for the offset to an insert derivation
//  INCLUDE_DERIVATION_ROLLLUP  :  FUTURE DEVELOPMENT FOR SAP TYPE DERIVATIONS ???
//
//**********************************************************************************************
longlong rtn, max_id, source_id, num_types, t, select_counter, override_counter, process_id, occurrence_id, &
	num_rows, g, actuals_month, actuals_mn, start_year
string staging_table, sqls, join_fields[], lookup_table, the_type, delimiter, basis, &
       insert_or_update, offset_type, offset_basis, or_name, col_name, suppl_wc, dev_role, cmd, bv, null_str_array[]
boolean error_found

//i_uo_cr_derivation = create uo_cr_derivation_bdg
i_uo_cr_derivation = create uo_cr_derivation

//**********************************************************************************************
//	Run derivation audits
//		-- Currently there are no audits applying to update derivations
//**********************************************************************************************

staging_table = 'cr_budget_data_temp'
lookup_table = staging_Table

//source_id = -678 // 0 might end up meaning something - e.g. budget entry
source_id = -33

select_counter = 0
select count(*) into :select_counter from cr_deriver_type_sources_budget
 where template_id = :i_template_id and source_id = :source_id and rownum = 1;

if isnull(select_counter) or select_counter = 0 then
	//  No override, use the standard setup
	sqls = "select * from cr_deriver_type_sources where source_id = " + string(source_id)
	i_cr_deriver_type_sources.SetSQLSelect(sqls)
	i_cr_deriver_type_sources.RETRIEVE()
	i_cr_deriver_type_sources.SetSort("run_order a")
	i_cr_deriver_type_sources.Sort()
	num_types = i_cr_deriver_type_sources.RowCount()
else
	//  Override for this template.
	sqls = "select * from cr_deriver_type_sources_budget " + &
		"where template_id = " + string(i_template_id) + " and source_id = " + string(source_id)
	i_cr_deriver_type_sources_budget.SetSQLSelect(sqls)
	i_cr_deriver_type_sources_budget.RETRIEVE()
	i_cr_deriver_type_sources_budget.SetSort("run_order a")
	i_cr_deriver_type_sources_budget.Sort()
	num_types = i_cr_deriver_type_sources_budget.RowCount()
end if

if num_types <= 0 then
//	if g_debug then f_pp_msgs("DERIVATIONS: No derivations defined for: " + staging_table)
	return 1 // Return, but allow to continue.  Don't want to halt just due to a cbx checked.
end if

//**********************************************************************************************
//  Derivations need a real interface_batch_id.  
//**********************************************************************************************
select costrepositorybdg.nextval into :i_ifb_id from dual;

// SEK 050510: Update gross_up_id as well so the gross up factors can be retained after
//		the records are rebuilt
sqls = "update cr_budget_data_entry set interface_batch_id = '" + string(i_ifb_id) + "', " + &
		 " gross_up_id = id " + &
		 " where cr_budget_version_id = " + string(i_bv_id) 
//		 ' and ' + i_selection_screen_sqls 
	
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating cr_budget_data_entry.interface_batch_id : " + &
		sqlca.SQLErrText)
	f_pp_msgs(sqls)
	f_pp_msgs("  ")
	rollback;
	error_found = true
	return -1
else
	commit;
end if

select budget_version, actuals_month, start_year into :bv, :actuals_month, :start_year
from cr_budget_version
where cr_budget_version_id = :i_bv_id;

//**********************************************************************************************
//  The derivations need a month_number field for effective dating.  Send the data to cr_budget_data_temp
//**********************************************************************************************
rtn = uf_build_cr_budget_data('Derivations')
if rtn < 0 then return -1

//
//	SEK 041510: Don't want to run derivations against rows that have amounts in actuals months
//
if isnull(actuals_month) then actuals_month = 0

if actuals_month > 0 then
	actuals_mn = start_year*100 + actuals_month

	sqls = "update " + staging_table + " set interface_batch_id = '-9999999999' where month_number <= " + string(actuals_mn)
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id : " + &
			sqlca.SQLErrText)
		f_pp_msgs(sqls)
		f_pp_msgs("  ")
		rollback;
		error_found = true
		return -1
//	else
//		commit;
	end if
end if

FOR t = 1 to num_types
	
	setpointer(hourglass!)
	
	if isnull(select_counter) or select_counter = 0 then
		//  No override, use the standard setup
		the_type         = trim(i_cr_deriver_type_sources.GetItemString(t, "type"))
		basis            = trim(i_cr_deriver_type_sources.GetItemString(t, "basis"))
		insert_or_update = upper(trim(i_cr_deriver_type_sources.GetItemString(t, "insert_or_update")))
		delimiter        = i_cr_deriver_type_sources.GetItemString(t, "delimiter")
		offset_type      = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_type"))
		offset_basis     = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_basis"))
	else
		//  Override for this template.
		the_type         = trim(i_cr_deriver_type_sources_budget.GetItemString(t, "type"))
		select basis, upper(trim(insert_or_update)), delimiter, offset_type, offset_basis
		  into :basis, :insert_or_update, :delimiter, :offset_type, :offset_basis
		  from cr_deriver_type_sources
		 where "TYPE" = :the_type and source_id = :source_id;
	end if
	
	if isnull(the_type)  or the_type = "" then continue // useless record
	if isnull(basis)     or basis    = "" then continue // useless record
	if isnull(delimiter)                  then delimiter    = ""
	if isnull(offset_type)                then offset_type  = "" // for g_debug below
	if isnull(offset_basis)               then offset_basis = "" // for g_debug below
	if offset_type  = "" and insert_or_update = "INSERT" then continue // useless record
	if offset_basis = "" and insert_or_update = "INSERT" then continue // useless record
	
	f_pp_msgs("Derivation Type = " + the_type)
	
	
	if insert_or_update = "INSERT" then goto insert_derivations
	//*******************************************************************************************
	//
	//  Update Derivations:
	//
	//*******************************************************************************************
	
	//  Parse the basis value (e.g. "company,work_order") into an array for the update function.
	join_fields = null_str_array
	f_parsestringintostringarray(basis, ",", join_fields)
	
	i_uo_cr_derivation.i_called_for_budget = true
	rtn = i_uo_cr_derivation.uf_deriver_update_mult( &
		the_type, &
		staging_table, &
		join_fields, &
		string(i_ifb_id), &
		false, &
		delimiter)
	
	if rtn <> 1 then
		//	return -1
		//	Allow the ifb_id clear out below
	end if
	
	if actuals_month > 0 then
		
		sqls = "update " + staging_table + " set interface_batch_id = '" + string(i_ifb_id) + "' where " + & 
					" interface_batch_id = '-9999999999'"
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("  ERROR: Resetting " + staging_table + ".interface_batch_id : " + &
				sqlca.SQLErrText)
			f_pp_msgs(sqls)
			f_pp_msgs("  ")
			rollback;
			error_found = true
			return -1
	//	else
	//		commit;
		end if
	end if
	
	rtn = uf_build_cr_budget_data('Results - Derivations')
	if rtn < 0 then 
		error_found = true
		rollback;
		return -1
	end if
	
	////	Clear out the interface_batch_id.  The selection screen criteria can be different each time.  
	////	Say interface_batch_id = 1 has 10 rows, interface_batch_id = 2 has 8 of the rows from interface_batch_id = 1.
	////		Two rows will remain with interface_batch_id = 1 which will be misleading.
	sqls = "update cr_budget_data_entry set interface_batch_id = null where cr_budget_version_id = " + string(i_bv_id) 
//	' and ' + i_selection_screen_sqls 
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id = null : " + &
			sqlca.SQLErrText)
		f_pp_msgs(sqls)
		f_pp_msgs("  ")
		rollback;
		error_found = true
		return -1
//	else
//		commit;
	end if
	
	delete from cr_budget_data_temp where drilldown_key = :i_dd_key and budget_version = :bv;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("  ERROR: Deleting from cr_budget_data_temp: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		error_found = true
		return -1
	else
		commit;
	end if
	
	//**********************************************************************************************
	//  Clear out the data used in derivations from cr_budget_data
	//**********************************************************************************************
	sqls = "delete from cr_budget_data where budget_version = '" + bv + "' and interface_batch_id = '" + string(i_ifb_id) + "' " // and " + i_selection_screen_sqls
	
	execute immediate :sqls;

	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("  ERROR: Deleting from cr_budget_data: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		error_found = true
		return -1
	else
		commit;
	end if
	
	if rtn <> 1 then
		error_found = true
		return -1
	end if
	
	continue // a derivation can't be both Update AND Insert
	
	insert_derivations:
	
////	For now, budget derivations can only be update derivations.  Comment out insert derivation logic
	
//	//*******************************************************************************************
//	//
//	//  Insert Derivations:
//	//
//	//    "Basis" is the "where clause" for the derivations.  For example, it might be:
//	//    "cr_allocations_stg.work_order = cr_deriver_control.string and target_credit = 'TARGET'"
//	//    for the target side of the derivation and:
//	//    "cr_allocations_stg.work_order = cr_deriver_control.string and cr_txn_type = 'TARGET'"
//	//    for the offset side.
//	//
//	//    CR TXN TYPE --- for speed, we're assuming that field is on all the allocations txn
//	//    tables.  If that ends up bad in the future, we'll add a system switch and evaluate
//	//    it before we start looping over allocations.
//	//
//	//*******************************************************************************************
//	
//	
//	//  -----------------------------------  TARGET SECTION:  -----------------------------------
//	
//	
//	//  Need the max(id) so I can fire a cr_txn_type update below.  Just using brute force.
//	//  Also, take advantage of this loop to replace "cr." with the correct table name.
//	max_id = 0
//	
//	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
//		if i_run_as_a_test then
//			select max(id) into :max_id from cr_inter_company_test_stg where interface_batch_id = :i_allocation_id;
//		else
//			select max(id) into :max_id from cr_inter_company_stg where interface_batch_id = :i_allocation_id;
//		end if
//	else                                                          //  Other Allocations ...
//		if i_run_as_a_test then
//			select max(id) into :max_id from cr_allocations_test_stg where interface_batch_id = :i_allocation_id;
//		else
//			select max(id) into :max_id from cr_allocations_stg where interface_batch_id = :i_allocation_id;
//		end if
//	end if
//	
//	if isnull(max_id) then max_id = 0
//	
//	//  The basis will almost certainly have "cr." references for the detail table.  Change that here
//	//  to the staging_table.
//	basis = f_replace_string(basis, "cr.", staging_table + ".", "all")
//	
//	//  Call the indicated derivation type.
//	rtn = i_uo_cr_derivation.uf_deriver( &
//		the_type, &
//		staging_table, &
//		basis)
//	
//	if rtn <> 1 then
//		return -1
//	end if
//	
//	//  Now update the cr_txn_type.  Just using brute force.
//	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
//		if i_run_as_a_test then
//			update cr_inter_company_test_stg  set cr_txn_type = 'TARGET'
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		else
//			update cr_inter_company_stg set cr_txn_type = 'TARGET'
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		end if
//	else                                                          //  Other Allocations ...
//		if i_run_as_a_test then
//			update cr_allocations_test_stg set cr_txn_type = 'TARGET'
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		else
//			update cr_allocations_stg set cr_txn_type = 'TARGET'
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		end if
//	end if
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("DERIVATIONS: TARGETS ERROR: updating " + staging_table + ".cr_txn_type: " + &
//			sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//	
//	//  -----------------------------------  OFFSET SECTION:  -----------------------------------
//	
//	
//	//  Need the max(id) so I can fire a cr_txn_type update below.  Just using brute force.
//	//  Also, take advantage of this loop to replace "cr." with the correct table name.
//	max_id = 0
//	
//	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
//		if i_run_as_a_test then
//			select max(id) into :max_id from cr_inter_company_test_stg where interface_batch_id = :i_allocation_id;
//		else
//			select max(id) into :max_id from cr_inter_company_stg where interface_batch_id = :i_allocation_id;
//		end if
//	else                                                          //  Other Allocations ...
//		if i_run_as_a_test then
//			select max(id) into :max_id from cr_allocations_test_stg where interface_batch_id = :i_allocation_id;
//		else
//			select max(id) into :max_id from cr_allocations_stg where interface_batch_id = :i_allocation_id;
//		end if
//	end if
//	
//	if isnull(max_id) then max_id = 0
//	
//	rtn = i_uo_cr_derivation.uf_deriver( &
//		offset_type, &
//		staging_table, &
//		offset_basis)
//	
//	if rtn <> 1 then
//		return -1
//	end if
//	
//	//  Now update the cr_txn_type.  Just using brute force.
//	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
//		if i_run_as_a_test then
//			update cr_inter_company_test_stg set cr_txn_type = 'OFFSET', quantity = -quantity
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		else
//			update cr_inter_company_stg set cr_txn_type = 'OFFSET', quantity = -quantity
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		end if
//	else                                                          //  Other Allocations ...
//		if i_run_as_a_test then
//			update cr_allocations_test_stg set cr_txn_type = 'OFFSET', quantity = -quantity
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		else
//			update cr_allocations_stg set cr_txn_type = 'OFFSET', quantity = -quantity
//			 where id > :max_id and interface_batch_id = :i_allocation_id;
//		end if
//	end if
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("DERIVATIONS: OFFSETS ERROR: updating " + staging_table + ".cr_txn_type: " + &
//			sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//
//
//
//NEXT  //  FOR t = 1 to num_types ...
//
//
////*******************************************************************************************
////
////  Derivation Overrides:  #2 table
////
////    Re-using variables in this section from above.
////
////*******************************************************************************************
//override_counter = 0
//select count(*) into :override_counter from cr_deriver_or2_sources_alloc
// where allocation_id = :i_allocation_id and source_id = :source_id and rownum = 1;
//
//if g_debug then f_pp_msgs("DERIVATIONS: override_counter = " + string(override_counter))
//
//if isnull(override_counter) or override_counter = 0 then
//	//  No override, use the standard setup
//	sqls = "select * from cr_deriver_override2_sources where source_id = " + string(source_id)
//	i_cr_deriver_override2.SetSQLSelect(sqls)
//	i_cr_deriver_override2.RETRIEVE()
//	i_cr_deriver_override2.SetSort("run_order a")
//	i_cr_deriver_override2.Sort()
//	num_types = i_cr_deriver_override2.RowCount()
//else
//	//  Override for this allocation.
//	sqls = "select * from cr_deriver_or2_sources_alloc " + &
//		"where allocation_id = " + string(i_allocation_id) + " and source_id = " + string(source_id)
//	i_cr_deriver_override2_alloc.SetSQLSelect(sqls)
//	i_cr_deriver_override2_alloc.RETRIEVE()
//	i_cr_deriver_override2_alloc.SetSort("run_order a")
//	i_cr_deriver_override2_alloc.Sort()
//	num_types = i_cr_deriver_override2_alloc.RowCount()
//end if
//
//if num_types <= 0 then
//	if g_debug then f_pp_msgs("DERIVATIONS: No derivations defined for: " + staging_table)
//	return 1 // Return, but allow to continue.  Don't want to halt just due to a cbx checked.
//end if
//
//
//
//FOR t = 1 to num_types
//	
//	
//	
//	if isnull(override_counter) or override_counter = 0 then
//		//  No override, use the standard setup
//		or_name  = trim(i_cr_deriver_override2.GetItemString(t, "override_name"))
//		col_name = trim(i_cr_deriver_override2.GetItemString(t, "col_name"))
//		suppl_wc = trim(i_cr_deriver_override2.GetItemString(t, "suppl_where_clause"))
//	else
//		//  Override for this allocation.
//		or_name  = trim(i_cr_deriver_override2_alloc.GetItemString(t, "override_name"))
//		col_name = trim(i_cr_deriver_override2_alloc.GetItemString(t, "col_name"))
//		select suppl_where_clause
//		  into :suppl_wc
//		  from cr_deriver_override2_sources
//		 where override_name = :or_name and col_name = :col_name and source_id = :source_id;
//	end if
//	
//	if isnull(or_name)  or or_name  = "" then continue // useless record
//	if isnull(col_name) or col_name = "" then continue // useless record
//	if isnull(suppl_wc) then suppl_wc = "" // good record
//		
//	if g_debug then
//		f_pp_msgs("or_name = " + or_name)
//		f_pp_msgs("col_name = " + col_name)
//		f_pp_msgs("suppl_wc = " + suppl_wc)
//	end if
//	
//	//  Setting these to the staging_table ought to work just fine since you'd only be using
//	//  these references if you're trying to update the table from itself (using the "res_table"
//	//  and "src_table" keywords.  Otherwise, you'd have an override set up for allocations that
//	//  probably has the "src_table" hardcoded (like work_order_control, a custom table, etc.).
//	//  In all cases, I imagine, the "src table" will be referenced in the override_sql and
//	//  replaced in the uf call.
//	i_uo_cr_derivation.pi_s_res_table       = staging_table
//	i_uo_cr_derivation.pi_s_src_table       = staging_table
//	i_uo_cr_derivation.pi_s_res_arg         = string(i_allocation_id)
//	i_uo_cr_derivation.i_suppl_where_clause = suppl_wc
//	
//	rtn = i_uo_cr_derivation.uf_deriver_update_override(or_name, col_name)
//	
//	if rtn <> 1 then
//		return -1
//	end if
//	
//	
//
NEXT  //  FOR t = 1 to num_types ...
//
//
//
////**********************************************************************************************
////
////  Derivations need a real interface_batch_id.  Change it back so nothing else has a problem.
////
////**********************************************************************************************
//sqls = "update " + staging_table + " set interface_batch_id = NULL"
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to NULL: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//
//if g_debug then
//	f_pp_msgs("Derivations complete at: " + string(now()))
//end if


// SEK 050510: Update cr_budget_entry_gross_up with the new ids from the derivation results
//		so that previously generated gross up factors are not lost.
sqls = "update cr_budget_entry_gross_up g " + &
		 "   set id = ( " + & 
		 "		select id from cr_budget_data_entry " + &
		 "		 where cr_budget_data_entry.gross_up_id = g.id " + &
		 "		   and cr_budget_data_entry.cr_budget_version_id = " + string(i_bv_id) + &
		 "			) " + &
		 " where exists ( " + &
		 "		select 1 from cr_budget_data_entry " + &
		 "		 where cr_budget_data_entry.gross_up_id = g.id " + &
		 "		   and cr_budget_data_entry.cr_budget_version_id = " + string(i_bv_id) + &
		 "			) "
		 
		 
//		 "			and " + i_selection_screen_sqls + &
//and " + i_selection_screen_sqls + "
		 
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating cr_budget_entry_gross_up.id : " + &
		sqlca.SQLErrText)
	f_pp_msgs(sqls)
	f_pp_msgs("  ")
	rollback;
	error_found = true
	return -1
else
	commit;
end if		 



return 1
end function

public function longlong uf_validations ();//*****************************************************************************************
//
//  Function     :  wf_validations
//
//*****************************************************************************************
longlong num_rows, i, vs_flag, ii, invalid_rowcount, counter, lvalidate_rtn, &
		 validation_counter, process_id, rtn, occurrence_id, start_year, actuals_month, actuals_mn
string sqls, cr_element, staging_table, me_table_element, me_table, element_descr, &
		 invalid_value, validate_rtn, cv_validations, bv, dev_role, cmd, cv_actuals
boolean validation_kickouts, validation_found


staging_table = 'cr_budget_data_temp'

//**********************************************************************************************
//  Validations need a real interface_batch_id.  
//**********************************************************************************************
select costrepositorybdg.nextval into :i_ifb_id from dual;

sqls = "update cr_budget_data_entry set interface_batch_id = '" + string(i_ifb_id) + "' where cr_budget_version_id = " + string(i_bv_id) 
//	' and ' + i_selection_screen_sqls 
	
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating cr_budget_data_entry.interface_batch_id : " + &
		sqlca.SQLErrText)
	f_pp_msgs(sqls)
	f_pp_msgs("  ")
	return -1
end if

//**********************************************************************************************
//  The validations need a month_number field for effective dating.  Send the data to cr_budget_data_temp
//**********************************************************************************************
rtn = uf_build_cr_budget_data('Validations')
if rtn < 0 then return -1


//*****************************************************************************************
//	 VALIDATIONS:  NOTE: SEK 042610: Southern wanted to be able to run validations
// 		in the entry screen, and not run budget validations anywhere else.  Added
//			budget entry system switches to handle this.
//*****************************************************************************************

sqlca.analyze_table(staging_table)

f_pp_msgs("Running validations at: " + string(now()))

// SEK 050410: Southern wants to only validate the months that haven't been updated with
//		actuals.  Delete the actuals months from cr_budget_data_temp prior to validating.
// SEK 062410: Southern wants to also obey the disabled month so they don't validate the
//		first forecast month after the actuals month since they have actuals data coming from
//		a custom extension.
setnull(cv_actuals)
select upper(trim(control_value)) into :cv_actuals from cr_system_control
 where upper(trim(control_name)) = 'BDG ENTRY - VALIDATE ACTUALS MONTHS';
if isnull(cv_actuals) then cv_actuals = "YES"

if cv_actuals <> "NO" then
	goto validations
else
	select start_year, actuals_month into :start_year, :actuals_month
	  from cr_budget_version
	 where cr_budget_version_id = :i_bv_id;
	 
	if isnull(start_year) then start_year = 0
	if isnull(actuals_month) then actuals_month = 0
	
	if i_obey_disable_month and i_have_disabled_month then
		actuals_mn = start_year*100 + actuals_month + 1
	else
		actuals_mn = start_year*100 + actuals_month
	end if
	
	if actuals_mn = 0 then goto validations
	
	f_pp_msgs("The CR system control value for validating actuals months is set to 'No'.")
	f_pp_msgs("Only months after " + string(actuals_mn) + " will be validated.")
	
	sqls = "delete from " + staging_table + " where month_number <= " + string(actuals_mn)
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("  ERROR: Deleting from cr_validations_invalid_ids2: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if

validations:

i_uo_cr_validation_bdg = CREATE uo_cr_validation_bdg

////	For budgeting, don't need the invalid ids delete.  The ids tables are cleaned up below.
////	Whatever the set of data is being viewed will be assigned an interface batch id when 
////	triggering validations.  There might be 10 rows the first time validations are fired and
////	then only 8 the second time.  Those 8 get a new ifb_id which makes it look like the 
////	remaining 2 were the only rows making up a batch ... that would be misleading.
////	Validating the budget data is not like a transaction interface or JEs.  
//lvalidate_rtn = i_uo_cr_validation_bdg.uf_delete_invalid_ids(staging_table, string(i_ifb_id))
//
//if lvalidate_rtn <> 1 then return -1

validation_kickouts = false

setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS-BDG ENTRY-COMBO';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations <> "YES" then 
	goto after_combo_validations
else
	validation_found = true
end if

f_pp_msgs(" -- Validating the Batch (Combos) at: " + string(now()))

//	 Which function is this client using?
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'VALIDATIONS - CONTROL OR COMBOS';
if isnull(cv_validations) then cv_validations = "CONTROL"

if cv_validations = 'CONTROL' then
	lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_control(staging_table, string(i_ifb_id))
else
	lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_combos(staging_table, string(i_ifb_id))
end if

if lvalidate_rtn = -1 then
	//  Error in uf_validate_(x).  Logged by the function.
	return -1
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_kickouts = true
//	wf_display_kickouts()
end if

after_combo_validations:

//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_project_validations
//
////if g_debug then
//	f_pp_msgs(" -- Validating the Batch (Projects) at: " + string(now()))
////end if
//
//lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_projects(staging_table, string(i_ifb_id))
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 then
//	//  Validation Kickouts.
//	validation_kickouts = true
//end if
//
//after_project_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS-BDG ENTRY-ME';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations <> "YES" then 
	goto after_me_validations
else
	validation_found = true
end if

//if g_debug then
	f_pp_msgs(" -- Validating the Batch (Element Values) at: " + string(now()))
//end if

lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_me(staging_table, string(i_ifb_id))

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	return -1
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_kickouts = true
end if

after_me_validations:

//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MONTH NUMBER';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_mn_validations
//
////if g_debug then
//	f_pp_msgs(" -- Month Number validations at: " + string(now()))
////end if
//
//lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_month_number(staging_table, string(i_ifb_id))
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 then
//	//  Always true here.  The MN validations are not inserted into the ids table since
//	//  MN is not editable.  However, Acct Range validations ARE inserted.  Since we don't
//	//  know which one kicked, we just have to assume TRUE.
//	validation_kickouts = true
//end if
//
//after_mn_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS-BDG ENTRY-CUSTOM';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations <> "YES" then 
	goto after_custom_validations
else
	validation_found = true
end if

//if g_debug then
	f_pp_msgs(" -- Custom validations at: " + string(now()))
//end if

lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_custom(staging_table, string(i_ifb_id))

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	return -1
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_kickouts = true
//	wf_display_kickouts()
end if

after_custom_validations:



if validation_kickouts then
	
//	if g_suspense_accounting = "YES" then
//		//  Call the suspense accounting function.  It will return a value of 2 if 
//		//  suspense accounting is turned off in cr_system_control.
//		i_uo_cr_validation_bdg.i_called_from_allocations_suspense = true
//		lvalidate_rtn = i_uo_cr_validation_bdg.uf_suspense(staging_table, string(i_ifb_id))
//		i_uo_cr_validation_bdg.i_called_from_allocations_suspense = false
//		
//		if lvalidate_rtn < 0 then
//			//  Error in uf_validate.  Logged by the function.
//			return -1
//		else
//			g_suspense_processed = true  //  For msg at the end of the logs.
//		end if
//	end if
	
//	//  DMJ: 6/25/08: SAME CODE THAT WOULD FIRE BELOW.
//	sqls = "update " + staging_table + " set interface_batch_id = NULL"
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to NULL: " + &
//			sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//	if g_suspense_accounting = "YES" then
//	else
//		f_pp_msgs("  ")
//		f_pp_msgs("   -----------------------------------------------------------------------" + &
//			"   -------------------------------------------------------------")
//		f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE ALLOCATION WILL NOT RUN!  *****")
//		f_pp_msgs("   -----------------------------------------------------------------------" + &
//			"   -------------------------------------------------------------")
//		f_pp_msgs("  ")
//		return -1
//	end if
end if


//**********************************************************************************************
//  Clear out the interface_batch_id
//**********************************************************************************************
sqls = "update cr_budget_data_entry set interface_batch_id = null where cr_budget_version_id = " + string(i_bv_id) 
//	' and ' + i_selection_screen_sqls 
			
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating cr_budget_data_entry.interface_batch_id to NULL: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;
end if

select budget_version into :bv
from cr_budget_version
where cr_budget_version_id = :i_bv_id;


//**********************************************************************************************
//  Clear out cr_validations_invalid_ids2
//**********************************************************************************************
//sqls = "delete from cr_validations_invalid_ids2 where table_name = 'cr_budget_data' and id in (" + &
//	"select id from cr_budget_data where budget_version = '" + bv + "' and interface_batch_id = '" + string(i_ifb_id) + "' and " + i_selection_screen_sqls + ")"

sqls = "delete from cr_validations_invalid_ids2 where table_name = 'cr_budget_data_temp' and id in (" + &
	"select id from cr_budget_data_temp where budget_version = '" + bv + "' and drilldown_key = '" + i_dd_key + "')"
	
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Deleting from cr_validations_invalid_ids2: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else 
	commit;
end if

//**********************************************************************************************
//  Clear out cr_validations_invalid_ids
//**********************************************************************************************
//sqls = "delete from cr_validations_invalid_ids where table_name = 'cr_budget_data' and id in (" + &
//	"select id from cr_budget_data where budget_version = '" + bv + "' and interface_batch_id = '" + string(i_ifb_id) + "' and " + i_selection_screen_sqls + ")"

sqls = "delete from cr_validations_invalid_ids where table_name = 'cr_budget_data_temp' and id in (" + &
	"select id from cr_budget_data_temp where budget_version = '" + bv + "' and drilldown_key = '" + i_dd_key + "')"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Deleting from cr_validations_invalid_ids: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;
end if

//**********************************************************************************************
//  Clear out the data used in validations from cr_budget_data_temp
//**********************************************************************************************
//sqls = "delete from cr_budget_data where budget_version = '" + bv + "' and interface_batch_id = '" + string(i_ifb_id) + "' and " + i_selection_screen_sqls

delete from cr_budget_data_temp where drilldown_key = :i_dd_key and budget_version = :bv;

//execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Deleting from cr_budget_data_temp: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;
end if


f_pp_msgs("Validations complete at: " + string(now()))

if validation_kickouts then
	return -2
else
	return 1
end if

return 1
end function

public function longlong uf_apply_default_values ();string cv, sqls, element, default_value
longlong num_elements, i, apply_default_values

select apply_default_values into :apply_default_values
from cr_budget_templates
where template_id = :i_template_id;

if isnull(apply_default_values) then apply_default_values = 0

if apply_default_values <> 1 then return 1

////	Create ds regardless of control value ... used below too.
uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
sqls = "select * from cr_elements"
f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)

ds_elements.SetSort("order a")
ds_elements.Sort()

num_elements = ds_elements.RowCount()

//if cv = 'YES' then
	for i = 1 to num_elements
		element = '"' + upper(f_cr_clean_string(ds_elements.getitemstring(i,'budgeting_element'))) + '"'
		default_value = ds_elements.getitemstring(i,'default_value')
		if isnull(default_value) or default_value = "" then default_value = ' '
		sqls = "update cr_budget_data_entry set " + element + " = '" + default_value + "' where nvl(" + element + ",' ') = ' ' and cr_budget_version_id = " + string(i_bv_id) 
//			' and ' + i_selection_screen_sqls 

		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			messagebox("Update Error","ERROR: Updating default_value : " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
	next
	
//end if

return 1
end function

public function longlong uf_price_qty ();string sqls, element, element_str, element_str2, element_str_select, budgeting_element,element_column,element_table, sqls_in, sqls_in_select
longlong rtn, rate_type_id, g, num_rows, counter,start_year, actuals_month
datawindow dw_entry

if i_called_from_calc_all then
	i_calc_all_in_progress = true
end if

////	What is the rate type ... get from the template
rate_type_id = 0
select rate_type_id into :rate_type_id
from cr_budget_rate_type_template
where template_id = :i_template_id;

if rate_type_id = 0 or isnull(rate_type_id) then 
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: There is no rate type defined for this template.  A rate type must be assigned to this template" + &
		" to be able to price quantities.")
	f_pp_msgs("  ")
	i_calc_all_in_progress = false
	return -1
end if

start_year = 0
select start_year,actuals_month into :start_year, :actuals_month
from cr_budget_version
where cr_budget_version_id = :i_bv_id;

if isnull(actuals_month) then actuals_month = 0

if start_year = 0 or isnull(start_year) then 
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Could not find the budget version start year")
	f_pp_msgs("  ")
	i_calc_all_in_progress = false
	return -1
end if

//	If there are 'Qty' rows with no corresponding 'Amt' row then make sure to add the 'Amt' row.
//	This situation could occur since "Type" is an option when inserting sets.
rtn = uf_add_missing_amt_rows()
if rtn < 0 then
	i_calc_all_in_progress = false
	return -1
end if

////	Get the elements defined as the level of detail for the rate type
sqls = 'select upper(b.budgeting_element) from cr_elements b, cr_budget_rate_type_element a where ' + &
	'a.element_id = b.element_id and a.rate_type_id = ' + string(rate_type_id)
uo_ds_top ds_elements
ds_elements = create uo_ds_top
f_create_dynamic_ds(ds_elements,"grid",sqls,sqlca,true)
num_rows = ds_elements.rowcount()
element_str = ' and '
element_str2 = ' and (year,'
element_str_select = ' select year,'
sqls_in = ' and (year,'
sqls_in_select = ' select distinct year,'
for g = 1 to num_rows
	element = ds_elements.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	element_str = element_str + 'b.' + element + ' = c.' + element + ' and '
	element_str = element_str + 'a.' + element + ' = c.' + element + ' and '
	element_str2 = element_str2 + 'a.' + element + ','
	sqls_in = sqls_in + " nvl(a." + element + ",' '),"
	sqls_in_select = sqls_in_select + " nvl(a." + element + ",' '),"
	element_str_select = element_str_select + element + ','
next

////	Get the remaining elements
//// SEK 062410: Don't want to include elements that aren't in the template, otherwise the update below won't work properly...
////		i.e. the Qty row has had derivations run against it and the corresponding Amt row was added in the insert above.
sqls = 'select upper(b.budgeting_element) from cr_elements b where element_id not in (select element_id from cr_budget_rate_type_element ' + &
		 ' where rate_type_id = ' + string(rate_type_id) + ') ' + &
		 ' and element_id in (select element_id from cr_budget_templates_fields where template_id = ' + string(i_template_id) + ")"
ds_elements.setsqlselect(sqls)
ds_elements.retrieve()
num_rows = ds_elements.rowcount()
for g = 1 to num_rows
	element = ds_elements.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls_in = sqls_in + " nvl(a." + element + ",' '),"
	sqls_in_select = sqls_in_select + " nvl(" + element + ",' '),"
	element_str = element_str + "nvl(a." + element + ",' ') = nvl(c." + element + ",' ') and "
next

////	Get the detail attributes
sqls = "select upper(description) from cr_budget_additional_fields"
uo_ds_top ds_detail_full
ds_detail_full = create uo_ds_top
f_create_dynamic_ds(ds_detail_full,"grid",sqls,sqlca,true)
num_rows = ds_detail_full.rowcount()
for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	//	use 0 on the nvl so I don't have to check the data type
	sqls_in = sqls_in + " nvl(a." + element + ",'0'),"
	sqls_in_select = sqls_in_select + " nvl(" + element + ",'0'),"
	element_str = element_str + " nvl( a." + element + ",'0') = nvl(c." + element + ",'0') and "
//	element_str2 = element_str2 + 'a.' + element + ','
//	element_str_select = element_str_select + element + ','
next

destroy ds_detail_full
	
element_str = mid(element_str,1,len(element_str) - 4)
element_str2 = mid(element_str2,1,len(element_str2) - 1) + ') in ('
element_str_select  = mid(element_str_select,1,len(element_str_select) - 1)
element_str_select = element_str_select + ' from cr_budget_rate_type_rates where rate_type_id = ' + string(rate_type_id) + ') '

sqls_in = mid(sqls_in,1,len(sqls_in) - 1) + ') in ('
sqls_in_select = mid(sqls_in_select,1,len(sqls_in_select) - 1) + ' from cr_budget_data_entry where cr_budget_version_id =  ' + string(i_bv_id) + " "


// SEK 042910: Added logic to obey disabled month
if i_obey_disable_month and i_have_disabled_month then
	sqls = " update cr_budget_data_entry a set (jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec) = (select " + &
		"sum(decode(a.year," + string(start_year) + ",case when 1 <= " + string(actuals_month+1) + " then a.jan else round(c.jan * b.jan,2) end, round(c.jan * b.jan,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 2 <= " + string(actuals_month+1) + " then a.feb else round(c.feb * b.feb,2) end, round(c.feb * b.feb,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 3 <= " + string(actuals_month+1) + " then a.mar else round(c.mar * b.mar,2) end, round(c.mar * b.mar,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 4 <= " + string(actuals_month+1) + " then a.apr else round(c.apr * b.apr,2) end, round(c.apr * b.apr,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 5 <= " + string(actuals_month+1) + " then a.may else round(c.may * b.may,2) end, round(c.may * b.may,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 6 <= " + string(actuals_month+1) + " then a.jun else round(c.jun * b.jun,2) end, round(c.jun * b.jun,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 7 <= " + string(actuals_month+1) + " then a.jul else round(c.jul * b.jul,2) end, round(c.jul * b.jul,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 8 <= " + string(actuals_month+1) + " then a.aug else round(c.aug * b.aug,2) end, round(c.aug * b.aug,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 9 <= " + string(actuals_month+1) + " then a.sep else round(c.sep * b.sep,2) end, round(c.sep * b.sep,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 10 <= " + string(actuals_month+1) + " then a.oct else round(c.oct * b.oct,2) end, round(c.oct * b.oct,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 11 <= " + string(actuals_month+1) + " then a.nov else round(c.nov * b.nov,2) end, round(c.nov * b.nov,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 12 <= " + string(actuals_month+1) + " then a.dec else round(c.dec * b.dec,2) end, round(c.dec * b.dec,2))) " + &
		"from cr_budget_rate_type_rates b, cr_budget_data_entry c " + &
		'where c.cr_budget_version_id = ' + string(i_bv_id) + ' and c.year = b.year and b.year = a.year and b.rate_type_id = ' + string(rate_type_id) + ' and c."TYPE" = ' + &
		" 'Qty' " + element_str + ') where a."TYPE" = ' + &
		" 'Amt' and a.cr_budget_version_id = " + string(i_bv_id) //+ element_str2 + element_str_select
else
	sqls = " update cr_budget_data_entry a set (jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec) = (select " + &
		"sum(decode(a.year," + string(start_year) + ",case when 1 <= " + string(actuals_month) + " then a.jan else round(c.jan * b.jan,2) end, round(c.jan * b.jan,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 2 <= " + string(actuals_month) + " then a.feb else round(c.feb * b.feb,2) end, round(c.feb * b.feb,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 3 <= " + string(actuals_month) + " then a.mar else round(c.mar * b.mar,2) end, round(c.mar * b.mar,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 4 <= " + string(actuals_month) + " then a.apr else round(c.apr * b.apr,2) end, round(c.apr * b.apr,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 5 <= " + string(actuals_month) + " then a.may else round(c.may * b.may,2) end, round(c.may * b.may,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 6 <= " + string(actuals_month) + " then a.jun else round(c.jun * b.jun,2) end, round(c.jun * b.jun,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 7 <= " + string(actuals_month) + " then a.jul else round(c.jul * b.jul,2) end, round(c.jul * b.jul,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 8 <= " + string(actuals_month) + " then a.aug else round(c.aug * b.aug,2) end, round(c.aug * b.aug,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 9 <= " + string(actuals_month) + " then a.sep else round(c.sep * b.sep,2) end, round(c.sep * b.sep,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 10 <= " + string(actuals_month) + " then a.oct else round(c.oct * b.oct,2) end, round(c.oct * b.oct,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 11 <= " + string(actuals_month) + " then a.nov else round(c.nov * b.nov,2) end, round(c.nov * b.nov,2))), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 12 <= " + string(actuals_month) + " then a.dec else round(c.dec * b.dec,2) end, round(c.dec * b.dec,2))) " + &
		"from cr_budget_rate_type_rates b, cr_budget_data_entry c " + &
		'where c.cr_budget_version_id = ' + string(i_bv_id) + ' and c.year = b.year and b.year = a.year and b.rate_type_id = ' + string(rate_type_id) + ' and c."TYPE" = ' + &
		" 'Qty' " + element_str + ') where a."TYPE" = ' + &
		" 'Amt' and a.cr_budget_version_id = " + string(i_bv_id) //+ element_str2 + element_str_select
end if

//sqls = sqls + " and " + i_selection_screen_sqls + i_addl_sql_price_qty
		 
//sqls_in_select = sqls_in_select + " and " + i_selection_screen_sqls + i_addl_sql_price_qty
	
sqls_in_select = sqls_in_select + 'and "TYPE" = '
sqls_in_select = sqls_in_select + "'Qty' "

////	If they selected a budget component from the first screen apply that where clause criteria here.
counter = 0
select b.budgeting_element, b.element_column, b.element_table, count(*)
into :budgeting_element, :element_column, :element_table, :counter
from cr_budget_templates_fields a, cr_elements b
where a.element_id = b.element_id
and a.template_id = :i_template_id
and nvl(identifies_labor,0) = 1
group by b.budgeting_element, b.element_column, b.element_table;

if isnull(counter) then counter = 0

if counter > 0 then  // identified a column as labor ... did they choose a component?
	
	budgeting_element = f_cr_clean_string(budgeting_element)
	
	if i_component_id = 3 then
	  	// i_selection_screen_labor_sqls is not populated in this case so
	  	// skip this step
	else
//		sqls = sqls + " and " + budgeting_element + " " + i_selection_screen_labor_sqls
//		sqls_in_select = sqls_in_select + " and " + budgeting_element + " " + i_selection_screen_labor_sqls
	end if
end if

sqls_in_select = sqls_in_select  + ') '

//sqls = sqls + sqls_in + sqls_in_select 

sqls = sqls + " and exists (select 1 from cr_budget_rate_type_rates b, cr_budget_data_entry c " + &
		'where c.cr_budget_version_id = ' + string(i_bv_id) + ' and c.year = b.year and b.year = a.year and b.rate_type_id = ' + string(rate_type_id) + ' and c."TYPE" = ' + &
		" 'Qty' " + element_str + ')'

	
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_budget_data_entry : " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	f_pp_msgs(sqls)
	rollback;
	i_calc_all_in_progress = false
	i_calc_all_rtn = -1
	return -1
end if

commit;

//if cbx_pq_apply_gross_up.checked then
//	wf_price_qty_gross_up()
//end if

rtn = uf_calc_totals_all()

if rtn < 0 then
	i_calc_all_in_progress = false
	return -1
end if

i_calc_all_in_progress = false

return 1
end function

public function longlong uf_calc_all_gross_up ();////
////	This function is called by the calc all button to apply the gross up to the entry screen data.  Most of this code was pulled from 
////		w_cr_budget_entry_alt_gross_up.  I planned to have a function that I could call in both windows for the code that overlaps
////		between here and the gross up window but had a terrible time with PB crashing on me when working with the new function (multiple times)
////
string sqls
longlong rtn, id, actuals_month, start_year, year, num_rows, g
dec {8} jan_rate,feb_rate,mar_rate,apr_rate,may_rate,jun_rate,jul_rate,aug_rate,sep_rate,oct_rate,nov_rate,dec_rate
dec {2} jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,decc

i_calc_all_in_progress = true

actuals_month = i_actuals_month
start_year = i_bv_start_year

if isnull(actuals_month) then actuals_month = 0
if isnull(start_year) then start_year = 0

	
if i_obey_disable_month and i_have_disabled_month then
	sqls = "update cr_budget_data_entry a " + &
			 '	set (jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,"DEC") = ( ' + &
			 "		select decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 1 then a.jan else round(nvl(a.jan,0) + (nvl(a.jan,0) * nvl(b.jan,0)),2) end, " + &
			 "						  round(nvl(a.jan,0) + (nvl(a.jan,0) * nvl(b.jan,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 2 then a.feb else round(nvl(a.feb,0) + (nvl(a.feb,0) * nvl(b.feb,0)),2) end, " + &
			 "						  round(nvl(a.feb,0) + (nvl(a.feb,0) * nvl(b.feb,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 3 then a.mar else round(nvl(a.mar,0) + (nvl(a.mar,0) * nvl(b.mar,0)),2) end,  " + &
			 "						  round(nvl(a.mar,0) + (nvl(a.mar,0) * nvl(b.mar,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 4 then a.apr else round(nvl(a.apr,0) + (nvl(a.apr,0) * nvl(b.apr,0)),2) end, " + &
			 "						  round(nvl(a.apr,0) + (nvl(a.apr,0) * nvl(b.apr,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 5 then a.may else round(nvl(a.may,0) + (nvl(a.may,0) * nvl(b.may,0)),2) end, " + &
			 "						  round(nvl(a.may,0) + (nvl(a.may,0) * nvl(b.may,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 6 then a.jun else round(nvl(a.jun,0) + (nvl(a.jun,0) * nvl(b.jun,0)),2) end, " + &
			 "						  round(nvl(a.jun,0) + (nvl(a.jun,0) * nvl(b.jun,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 7 then a.jul else round(nvl(a.jul,0) + (nvl(a.jul,0) * nvl(b.jul,0)),2) end, " + &
			 "						  round(nvl(a.jul,0) + (nvl(a.jul,0) * nvl(b.jul,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 8 then a.aug else round(nvl(a.aug,0) + (nvl(a.aug,0) * nvl(b.aug,0)),2) end, " + &
			 "						  round(nvl(a.aug,0) + (nvl(a.aug,0) * nvl(b.aug,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 9 then a.sep else round(nvl(a.sep,0) + (nvl(a.sep,0) * nvl(b.sep,0)),2) end, " + &
			 "						  round(nvl(a.sep,0) + (nvl(a.sep,0) * nvl(b.sep,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 10 then a.oct else round(nvl(a.oct,0) + (nvl(a.oct,0) * nvl(b.oct,0)),2) end, " + &
			 "						  round(nvl(a.oct,0) + (nvl(a.oct,0) * nvl(b.oct,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + "+1 <= 11 then a.nov else round(nvl(a.nov,0) + (nvl(a.nov,0) * nvl(b.nov,0)),2) end, " + &
			 "						  round(nvl(a.nov,0) + (nvl(a.nov,0) * nvl(b.nov,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + '+1 <= 12 then a."DEC" else round(nvl(a."DEC",0) + (nvl(a."DEC",0) * nvl(b."DEC",0)),2) end, ' + &
			 '						  round(nvl(a."DEC",0) + (nvl(a."DEC",0) * nvl(b."DEC",0)),2)) ' + &
			 "		  from cr_budget_entry_gross_up b " + &
			 "		 where a.id = b.id) " + &
			 " where cr_budget_version_id = " + string(i_bv_id) + &
			 "	  and exists ( " + &
			 "		select 1 from cr_budget_entry_gross_up b " + &
			 "		 where a.id = b.id)"
			 
	execute immediate :sqls;		 
			 
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating cr_budget_data_entry.months with gross up: " + sqlca.SQLErrText)
		f_pp_msgs("SQLS: " + sqls)
		f_pp_msgs("  ")
		rollback;
		i_calc_all_in_progress = false
		return -1
	else
		commit;
	end if
else	
	sqls = "update cr_budget_data_entry a " + &
			 '	set (jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,"DEC") = ( ' + &
			 "		select decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 1 then a.jan else round(nvl(a.jan,0) + (nvl(a.jan,0) * nvl(b.jan,0)),2) end, " + &
			 "						  round(nvl(a.jan,0) + (nvl(a.jan,0) * nvl(b.jan,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 2 then a.feb else round(nvl(a.feb,0) + (nvl(a.feb,0) * nvl(b.feb,0)),2) end, " + &
			 "						  round(nvl(a.feb,0) + (nvl(a.feb,0) * nvl(b.feb,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 3 then a.mar else round(nvl(a.mar,0) + (nvl(a.mar,0) * nvl(b.mar,0)),2) end,  " + &
			 "						  round(nvl(a.mar,0) + (nvl(a.mar,0) * nvl(b.mar,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 4 then a.apr else round(nvl(a.apr,0) + (nvl(a.apr,0) * nvl(b.apr,0)),2) end, " + &
			 "						  round(nvl(a.apr,0) + (nvl(a.apr,0) * nvl(b.apr,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 5 then a.may else round(nvl(a.may,0) + (nvl(a.may,0) * nvl(b.may,0)),2) end, " + &
			 "						  round(nvl(a.may,0) + (nvl(a.may,0) * nvl(b.may,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 6 then a.jun else round(nvl(a.jun,0) + (nvl(a.jun,0) * nvl(b.jun,0)),2) end, " + &
			 "						  round(nvl(a.jun,0) + (nvl(a.jun,0) * nvl(b.jun,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 7 then a.jul else round(nvl(a.jul,0) + (nvl(a.jul,0) * nvl(b.jul,0)),2) end, " + &
			 "						  round(nvl(a.jul,0) + (nvl(a.jul,0) * nvl(b.jul,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 8 then a.aug else round(nvl(a.aug,0) + (nvl(a.aug,0) * nvl(b.aug,0)),2) end, " + &
			 "						  round(nvl(a.aug,0) + (nvl(a.aug,0) * nvl(b.aug,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 9 then a.sep else round(nvl(a.sep,0) + (nvl(a.sep,0) * nvl(b.sep,0)),2) end, " + &
			 "						  round(nvl(a.sep,0) + (nvl(a.sep,0) * nvl(b.sep,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 10 then a.oct else round(nvl(a.oct,0) + (nvl(a.oct,0) * nvl(b.oct,0)),2) end, " + &
			 "						  round(nvl(a.oct,0) + (nvl(a.oct,0) * nvl(b.oct,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + " <= 11 then a.nov else round(nvl(a.nov,0) + (nvl(a.nov,0) * nvl(b.nov,0)),2) end, " + &
			 "						  round(nvl(a.nov,0) + (nvl(a.nov,0) * nvl(b.nov,0)),2)), " + &
			 "		 decode(a.year," + string(start_year) + ", " + &
			 "						  case when " +string(actuals_month) + ' <= 12 then a."DEC" else round(nvl(a."DEC",0) + (nvl(a."DEC",0) * nvl(b."DEC",0)),2) end, ' + &
			 '						  round(nvl(a."DEC",0) + (nvl(a."DEC",0) * nvl(b."DEC",0)),2)) ' + &
			 "		  from cr_budget_entry_gross_up b " + &
			 "		 where a.id = b.id) " + &
			 " where cr_budget_version_id = " + string(i_bv_id) + &
			 "	  and exists ( " + &
			 "		select 1 from cr_budget_entry_gross_up b " + &
			 "		 where a.id = b.id)"
			 
	execute immediate :sqls;	
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating cr_budget_data_entry.months with gross up: " + sqlca.SQLErrText)
		f_pp_msgs("SQLS: " + sqls)
		f_pp_msgs("  ")
		rollback;
		i_calc_all_in_progress = false
		return -1
	else
		commit;
	end if
	
end if

rtn = uf_calc_totals_all()

if rtn < 0 then
	i_calc_all_in_progress = false
	return -1
end if

i_calc_all_in_progress = false

return 1
end function

public function longlong uf_allocations ();// Not currently using allocations... do this later

return 1
end function

public function longlong uf_calculations ();////
////	Calculations are essentially a "run fast" for the allocations.  It bypasses the more formal allocation processing, i.e. no visible, 
////  auto-updated log, alloc uo call, etc.  
////

string sqls, element_str, element_str2, element_str_select, element, insert_sqls, delete_1, select_sqls, table_name, budgeting_element, element_column, &
	element_table, where_clause, group_sqls, table_name2, create_sqls2, table_name_gu, orig_sqls, calc_selection_screen_dept_sqls, element_list
longlong num_allocations, rtn, b, allocation_id, num_rows, g, sessionid, counter, rate_override_id, target_id, where_clause_id, actuals_month

string month_array[], session_id, users,nvl_element_list
longlong n, start_year

if i_called_from_calc_all then
	i_calc_all_in_progress = true
end if

select actuals_month, start_year into :actuals_month, :start_year
  from cr_budget_version
 where cr_budget_version_id = :i_bv_id;

if isnull(actuals_month) then actuals_month = 0

select userenv('sessionid'),user 
into :session_id,:users
from dual;

if len(session_id) > 14 then
	session_id = right(session_id,14)
end if

//// SEK 01/12/10: Since cr_budget_data_entry is aliased above with "a", we need to replace the "cr_budget_data_entry"
////					  alias in i_selection_screen_dept_sqls with "a".
//calc_selection_screen_dept_sqls = f_replace_string(i_selection_screen_dept_sqls,"cr_budget_data_entry.","a.","all")

////
////		cr_budget_templates_alloc gives me the allocations assigned to this template.  From there, I can look up the rate_override_id value
////			to be used in the calculation.  Only one can be assigned to an allocation.  
////
sqls = "select allocation_id, run_order from cr_budget_templates_alloc where template_id = " + string(i_template_id) + " order by run_order " 
uo_ds_top ds_templates_alloc
ds_templates_alloc = create uo_ds_top
f_create_dynamic_ds(ds_templates_alloc,"grid",sqls,sqlca,true)

//	Sort ... just to be safe
ds_templates_alloc.setsort("#2 A")
ds_templates_alloc.sort()

num_allocations = ds_templates_alloc.rowcount()

// Can't delete and replace since some rows may have been updated with actuals
	
// ********************************************************************************************
//
//			 	 !!!!!!!!								NEW METHOD								!!!!!!!!		
//	--------------------------------------------------------------------------------------------
//	
//		STEP 1: Create temp table shaped like cr_budget_data_entry
//		STEP 2: Insert calculation results into temp table
//		STEP 3: Update months after the actuals month to 0 for the prior results
//		STEP 4: Update months with results from temp table
//		STEP 5: Insert missing combos
//		STEP 6: Delete any rows with zeros in all months from cr_budget_data_entry
//
// ********************************************************************************************
month_array[1] = 'jan'
month_array[2] = 'feb'
month_array[3] = 'mar'
month_array[4] = 'apr'
month_array[5] = 'may'
month_array[6] = 'jun'
month_array[7] = 'jul'
month_array[8] = 'aug'
month_array[9] = 'sep'
month_array[10] = 'oct'
month_array[11] = 'nov'
month_array[12] = 'dec'

////
////	STEP 1: Create temp table shaped like cr_budget_data_entry
//// 
sqls = "drop table cbde_calc_" + session_id
execute immediate :sqls;	

sqls = "create table cbde_calc_" + session_id + " as select * from cr_budget_data_entry where 1 = 2"
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Creating cbde_calc_" + session_id + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	i_calc_all_in_progress = false
	i_calc_all_rtn = -1
	return -1
end if

for b = 1 to num_allocations
	
	allocation_id = ds_templates_alloc.getitemnumber(b,1)
	
	select nvl(rate_override_id,0), target_id, where_clause_id
	into :rate_override_id , :target_id, :where_clause_id
	from cr_allocation_control_bdg 
	where allocation_id = :allocation_id;
	
	if rate_override_id = 0 then continue
	
	////
	////		The calculations functionality is limited.  Only one target is allowed.  If more than one target is needed then set
	////		up an entry screen allocation.
	////
	select count(*) into :counter
	from cr_alloc_target_criteria_bdg
	where target_id = :target_id;
	
	if isnull(counter) then counter = 0
	
	if counter > 1 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: The Target criteria for a Calculation can only contain one target row.  : " + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		rollback;
		i_calc_all_in_progress = false
		return -1
	end if
	
	////
	////		Get the elements defined as the level of detail for the rate type
	////
	sqls = 'select upper(b.budgeting_element) from cr_elements b, CR_BUDGET_ENT_RATE_OVR_ELEMENT a where ' + &
		'a.element_id = b.element_id and a.rate_override_id = ' + string(rate_override_id)
	uo_ds_top ds_elements
	ds_elements = create uo_ds_top
	f_create_dynamic_ds(ds_elements,"grid",sqls,sqlca,true)
	num_rows = ds_elements.rowcount()
	element_str = ' and '
	element_str2 = ' and (a.year,'
	element_str_select = ' select year,'
	for g = 1 to num_rows
		element = ds_elements.getitemstring(g,1)
		element = '"' + f_cr_clean_string(element) + '"'
		element_str = element_str + 'a.' + element + ' = b.' + element + ' and '
		element_str2 = element_str2 + 'a.' + element + ','
		element_str_select = element_str_select + element + ','
	next
	
	element_str = mid(element_str,1,len(element_str) - 4)
	element_str2 = mid(element_str2,1,len(element_str2) - 1) + ') in ('
	element_str_select  = mid(element_str_select,1,len(element_str_select) - 1)
	element_str_select = element_str_select + ' from CR_BUDGET_ENT_RATE_OVR_RATES where rate_override_id = ' + string(rate_override_id) + ') '
	
	insert_sqls = "insert into cbde_calc_" + session_id + " (id,cr_budget_version_id,year,allocation_id," + &
		'"TYPE",jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec,'
	
	group_sqls = " group by "
	
	sqls = 'select upper(budgeting_element) from cr_elements '
	uo_ds_top ds_elements_full
	ds_elements_full = create uo_ds_top
	f_create_dynamic_ds(ds_elements_full,"grid",sqls,sqlca,true)
	num_rows = ds_elements_full.rowcount()
	for g = 1 to num_rows
		element = ds_elements_full.getitemstring(g,1)
		element = '"' + f_cr_clean_string(element) + '"'
		delete_1 = delete_1 + " and nvl( a." + element + ",' ') = nvl(b." + element + ",' ')"
		insert_sqls = insert_sqls + element + ","
		group_sqls = group_sqls + " decode(v." + element+ ",'*',a." + element + ",v." + element + "),"
		select_sqls = select_sqls + " decode(v." + element+ ",'*',a." + element + ",v." + element + "),"
		element_list = element_list + element + ","
		nvl_element_list = nvl_element_list + "nvl(" + element + ",' '),"
	next
	
	destroy ds_elements_full
	
	////
	////	STEP 2: Insert calculation results into temp table
	////
	for g = 1 to 2
		
		////
		////		Build the insert.  Because of alias needs, I cannot use the i_selection_screen_sqls variable when 
		////		adding the selection screen criteria.
		////
		if g = 1 then
	
			sqls = insert_sqls + 'split_labor_row) '
			select_sqls = select_sqls + '2 '
			
			if i_obey_disable_month and i_have_disabled_month then
				sqls = sqls + " select crbudgets.nextval, y.* from (select " + string(i_bv_id) + ", a.year," + string(allocation_id) + ",'Amt', " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 1 > " + string(actuals_month+1) + " then sum(round(a.jan * b.jan,2)) else 0 end, " + &
							" sum(round(a.jan * b.jan,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &	
							" case when 2 > " + string(actuals_month+1) + " then sum(round(a.feb * b.feb,2)) else 0 end, " + & 
							" sum(round(a.feb * b.feb,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &	
							" case when 3 > " + string(actuals_month+1) + " then sum(round(a.mar * b.mar,2)) else 0 end,  " + &
							" sum(round(a.mar * b.mar,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &	
							" case when 4 > " + string(actuals_month+1) + " then sum(round(a.apr * b.apr,2)) else 0 end, " + &
							" sum(round(a.apr * b.apr,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 5 > " + string(actuals_month+1) + " then sum(round(a.may * b.may,2)) else 0 end, " + &
							" sum(round(a.may * b.may,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 6 > " + string(actuals_month+1) + " then sum(round(a.jun * b.jun,2)) else 0 end, " + &
							" sum(round(a.jun * b.jun,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 7 > " + string(actuals_month+1) + " then sum(round(a.jul * b.jul,2)) else 0 end, " + &
							" sum(round(a.jul * b.jul,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 8 > " + string(actuals_month+1) + " then sum(round(a.aug * b.aug,2)) else 0 end, " + &
							" sum(round(a.aug * b.aug,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 9 > " + string(actuals_month+1) + " then sum(round(a.sep * b.sep,2)) else 0 end, " + &
							" sum(round(a.sep * b.sep,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 10 > " + string(actuals_month+1) + " then sum(round(a.oct * b.oct,2)) else 0 end, " + &
							" sum(round(a.oct * b.oct,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 11 > " + string(actuals_month+1) + " then sum(round(a.nov * b.nov,2)) else 0 end, " + &
							" sum(round(a.nov * b.nov,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 12 > " + string(actuals_month+1) + " then sum(round(a.dec * b.dec,2)) else 0 end, " + &
							" sum(round(a.dec * b.dec,2))), " + &
						select_sqls + &
						" from CR_BUDGET_ENT_RATE_OVR_RATES b, cr_budget_data_entry a, cr_alloc_target_criteria_bdg v " + &
						" where a.cr_budget_version_id = " + string(i_bv_id) 
			else
				sqls = sqls + " select crbudgets.nextval, y.* from (select " + string(i_bv_id) + ", a.year," + string(allocation_id) + ",'Amt', " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 1 > " + string(actuals_month) + " then sum(round(a.jan * b.jan,2)) else 0 end, " + &
							" sum(round(a.jan * b.jan,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &	
							" case when 2 > " + string(actuals_month) + " then sum(round(a.feb * b.feb,2)) else 0 end, " + & 
							" sum(round(a.feb * b.feb,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &	
							" case when 3 > " + string(actuals_month) + " then sum(round(a.mar * b.mar,2)) else 0 end,  " + &
							" sum(round(a.mar * b.mar,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &	
							" case when 4 > " + string(actuals_month) + " then sum(round(a.apr * b.apr,2)) else 0 end, " + &
							" sum(round(a.apr * b.apr,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 5 > " + string(actuals_month) + " then sum(round(a.may * b.may,2)) else 0 end, " + &
							" sum(round(a.may * b.may,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 6 > " + string(actuals_month) + " then sum(round(a.jun * b.jun,2)) else 0 end, " + &
							" sum(round(a.jun * b.jun,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 7 > " + string(actuals_month) + " then sum(round(a.jul * b.jul,2)) else 0 end, " + &
							" sum(round(a.jul * b.jul,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 8 > " + string(actuals_month) + " then sum(round(a.aug * b.aug,2)) else 0 end, " + &
							" sum(round(a.aug * b.aug,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 9 > " + string(actuals_month) + " then sum(round(a.sep * b.sep,2)) else 0 end, " + &
							" sum(round(a.sep * b.sep,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 10 > " + string(actuals_month) + " then sum(round(a.oct * b.oct,2)) else 0 end, " + &
							" sum(round(a.oct * b.oct,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 11 > " + string(actuals_month) + " then sum(round(a.nov * b.nov,2)) else 0 end, " + &
							" sum(round(a.nov * b.nov,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 12 > " + string(actuals_month) + " then sum(round(a.dec * b.dec,2)) else 0 end, " + &
							" sum(round(a.dec * b.dec,2))), " + & 
						select_sqls + &
						" from CR_BUDGET_ENT_RATE_OVR_RATES b, cr_budget_data_entry a, cr_alloc_target_criteria_bdg v " + &
						" where a.cr_budget_version_id = " + string(i_bv_id) 
			end if
			sqls = sqls + ' and b.year = a.year and a."TYPE" = ' + &
				" 'Amt' and b.rate_override_id = " + string(rate_override_id) + " and v.target_id = " + string(target_id) + " " 
			
		else
			
			sqls = insert_sqls + 'split_labor_row) '
//			select_sqls = select_sqls + '2 '
			
			if i_obey_disable_month and i_have_disabled_month then
				sqls = sqls + " select crbudgets.nextval, y.* from (select " + string(i_bv_id) + ", a.year," + string(allocation_id) + ",'Amt', " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 1 > " + string(actuals_month+1) + " then sum(round(a.jan * v.rate,2)) else 0 end, " + &
							" sum(round(a.jan * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 2 > " + string(actuals_month+1) + " then sum(round(a.feb * v.rate,2)) else 0 end, " + & 
							" sum(round(a.feb * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 3 > " + string(actuals_month+1) + " then sum(round(a.mar * v.rate,2)) else 0 end,  " + &
							" sum(round(a.mar * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 4 > " + string(actuals_month+1) + " then sum(round(a.apr * v.rate,2)) else 0 end, " + &
							" sum(round(a.apr * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 5 > " + string(actuals_month+1) + " then sum(round(a.may * v.rate,2)) else 0 end, " + &
							" sum(round(a.may * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 6 > " + string(actuals_month+1) + " then sum(round(a.jun * v.rate,2)) else 0 end, " + &
							" sum(round(a.jun * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 7 > " + string(actuals_month+1) + " then sum(round(a.jul * v.rate,2)) else 0 end, " + &
							" sum(round(a.jul * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 8 > " + string(actuals_month+1) + " then sum(round(a.aug * v.rate,2)) else 0 end, " + &
							" sum(round(a.aug * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 9 > " + string(actuals_month+1) + " then sum(round(a.sep * v.rate,2)) else 0 end, " + &
							" sum(round(a.sep * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 10 > " + string(actuals_month+1) + " then sum(round(a.oct * v.rate,2)) else 0 end, " + &
							" sum(round(a.oct * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 11 > " + string(actuals_month+1) + " then sum(round(a.nov * v.rate,2)) else 0 end, " + &
							" sum(round(a.nov * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 12 > " + string(actuals_month+1) + " then sum(round(a.dec * v.rate,2)) else 0 end, " + &
							" sum(round(a.dec * v.rate,2))), " + &
						select_sqls + &
						" from  cr_budget_data_entry a, cr_alloc_target_criteria_bdg v " + &
						" where a.cr_budget_version_id = " + string(i_bv_id) 
			else
				sqls = sqls + " select crbudgets.nextval, y.* from (select " + string(i_bv_id) + ", a.year," + string(allocation_id) + ",'Amt', " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 1 > " + string(actuals_month) + " then sum(round(a.jan * v.rate,2)) else 0 end, " + &
							" sum(round(a.jan * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 2 > " + string(actuals_month) + " then sum(round(a.feb * v.rate,2)) else 0 end, " + & 
							" sum(round(a.feb * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 3 > " + string(actuals_month) + " then sum(round(a.mar * v.rate,2)) else 0 end,  " + &
							" sum(round(a.mar * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 4 > " + string(actuals_month) + " then sum(round(a.apr * v.rate,2)) else 0 end, " + &
							" sum(round(a.apr * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 5 > " + string(actuals_month) + " then sum(round(a.may * v.rate,2)) else 0 end, " + &
							" sum(round(a.may * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 6 > " + string(actuals_month) + " then sum(round(a.jun * v.rate,2)) else 0 end, " + &
							" sum(round(a.jun * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 7 > " + string(actuals_month) + " then sum(round(a.jul * v.rate,2)) else 0 end, " + &
							" sum(round(a.jul * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 8 > " + string(actuals_month) + " then sum(round(a.aug * v.rate,2)) else 0 end, " + &
							" sum(round(a.aug * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 9 > " + string(actuals_month) + " then sum(round(a.sep * v.rate,2)) else 0 end, " + &
							" sum(round(a.sep * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 10 > " + string(actuals_month) + " then sum(round(a.oct * v.rate,2)) else 0 end, " + &
							" sum(round(a.oct * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 11 > " + string(actuals_month) + " then sum(round(a.nov * v.rate,2)) else 0 end, " + &
							" sum(round(a.nov * v.rate,2))), " + &
						" decode(a.year," + string(start_year) + ", " + &
							" case when 12 > " + string(actuals_month) + " then sum(round(a.dec * v.rate,2)) else 0 end, " + &
							" sum(round(a.dec * v.rate,2))), " + & 
						select_sqls + &
						" from  cr_budget_data_entry a, cr_alloc_target_criteria_bdg v " + &
						" where a.cr_budget_version_id = " + string(i_bv_id) 
			end if
			sqls = sqls + ' and a."TYPE" = ' + &
				" 'Amt' and v.target_id = " + string(target_id) + " " + uf_calculations_missing_combos_sqls(rate_override_id) + " " 
			
		end if
		
		if g = 1 then
			sqls = sqls + element_str
		end if

//		sqls = sqls + " and " + calc_selection_screen_dept_sqls
		
		where_clause = uf_build_calculations_where_clause(where_clause_id)
		
		if where_clause = 'ERROR' then 
			i_calc_all_in_progress = false
			return -1
		end if
		
		sqls = sqls + where_clause 
			
		if counter > 0 then  // identified a column as labor ... did they choose a component?
			
			budgeting_element = f_cr_clean_string(budgeting_element)
			choose case i_component_id
				case 3
					//	All
				case else
//					sqls = sqls + ' and a.' + budgeting_element + " " + i_selection_screen_labor_sqls
			end choose
		end if
			
		if g = 1 then
			sqls = sqls + element_str2 + element_str_select + &
				group_sqls + 'a.year ) y '
		else
			sqls = sqls +   &
				group_sqls + 'a.year ) y '
		end if
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting Amt rows into cbde_calc_" + session_id + " (" + string(g) + "): " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			f_pp_msgs(sqls)
			rollback;
			i_calc_all_in_progress = false
			i_calc_all_rtn = -1
			return -1
		end if
	
		commit;
		
	next	
	
	
	counter = 0
	select b.budgeting_element, b.element_column, b.element_table, count(*)
	into :budgeting_element, :element_column, :element_table, :counter
	from cr_budget_templates_fields a, cr_elements b
	where a.element_id = b.element_id
	and a.template_id = :i_template_id
	and nvl(identifies_labor,0) = 1
	group by b.budgeting_element, b.element_column, b.element_table;

	if isnull(counter) then counter = 0
	
	// loop over the months
	for n = 1 to upperbound(month_array)
		
		if (i_obey_disable_month and i_have_disabled_month and n <= actuals_month + 1) or &
			(not i_obey_disable_month and n <= actuals_month) then continue
			
		////
		////	STEP 3: Update months after the actuals month to 0 for the prior results
		////		-- update the allocation_id and split_labor_row to 0 as well so the insert
		////			for missing combos doesn't	duplicate lines
		////
		sqls = "update cr_budget_data_entry a set " + month_array[n] + " = 0, " + &
				 " allocation_id = 0, split_labor_row = 0 " + &
				 " where cr_budget_version_id = " + string(i_bv_id) + &
				 "   and nvl(allocation_id,0) = " + string(allocation_id) + &
				 '   and "TYPE" =  ' + " 'Amt' " + &
				 "   and exists (" + &
				 "		select 1 from cr_budget_data_entry b " + &
				 "		 where nvl(b.split_labor_row ,0) = 2 " + delete_1 + &
				 " 		and a.cr_budget_version_id = b.cr_budget_version_id " + &
				 "			and a.year = b.year) "
		
//		sqls = sqls + " and " + calc_selection_screen_dept_sqls
	
		////	If they selected a budget component from the first screen apply that where clause criteria here.	
		if counter > 0 then  // identified a column as labor ... did they choose a component?
			
			budgeting_element = f_cr_clean_string(budgeting_element)
			choose case i_component_id
				case 3
					//	All
				case else
//					sqls = sqls + ' and a.' + budgeting_element + " " + i_selection_screen_labor_sqls
			end choose
		end if
				 
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating cr_budget_data_entry." + month_array[n] + " to 0: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			f_pp_msgs(sqls)
			rollback;
			i_calc_all_in_progress = false
			i_calc_all_rtn = -1
			return -1
		end if
		
		////
		////	STEP 4: Update months with results from temp table
		////
		sqls = "update cr_budget_data_entry a set " + month_array[n] + " = ( " + &
				 " 	select " + month_array[n] + " from cbde_calc_" + session_id + " b " + &
				 "		 where b.cr_budget_version_id = " + string(i_bv_id) + &
				 " 		and nvl(b.allocation_id,0) = " + string(allocation_id) + &
				 "			and a.cr_budget_version_id = b.cr_budget_version_id " + &
				 "			and nvl(a.allocation_id,0) = 0 " + &
				 "			and a.year = b.year " + &
				 '			and b."TYPE"' + " = 'Amt' " + &
				 "			and nvl(a.split_labor_row,0) = 0 " + delete_1 + &
				 "		), " + &
				 "	allocation_id = " + string(allocation_id) + ", split_labor_row = 2 " + &
				 " where cr_budget_version_id = " + string(i_bv_id) + &
				 "   and nvl(allocation_id,0) = 0 " + &
				 '   and "TYPE" =  ' + " 'Amt' " + &
				 "   and exists (" + &
				 "		select 1 from cbde_calc_" + session_id + " b " + &
				 "		 where b.cr_budget_version_id = " + string(i_bv_id) + &
				 " 		and nvl(b.allocation_id,0) = " + string(allocation_id) + &
				 "			and a.cr_budget_version_id = b.cr_budget_version_id " + &
				 "			and nvl(a.allocation_id,0) = 0 " + &
				 "			and a.year = b.year " + &
				 '			and b."TYPE"' + " = 'Amt' " + &
				 "			and nvl(a.split_labor_row,0) = 0 " + delete_1 + &
				 "		) " 
		
//		sqls = sqls + " and " + calc_selection_screen_dept_sqls
	
		////	If they selected a budget component from the first screen apply that where clause criteria here.
		if isnull(counter) then counter = 0
		
		if counter > 0 then  // identified a column as labor ... did they choose a component?
			
			budgeting_element = f_cr_clean_string(budgeting_element)
			choose case i_component_id
				case 3
					//	All
				case else
//					sqls = sqls + ' and a.' + budgeting_element + " " + i_selection_screen_labor_sqls
			end choose
		end if
				 
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating cr_budget_data_entry." + month_array[n] + ": " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			f_pp_msgs(sqls)
			rollback;
			i_calc_all_in_progress = false
			i_calc_all_rtn = -1
			return -1
		end if
		
		
	next // for n = 1 to upperbound(month_number)
	
	////
	////	STEP 5: Insert missing combos
	////
	insert_sqls = f_replace_string(insert_sqls,"cbde_calc_" + session_id,"cr_budget_data_entry","all")
	
	sqls = insert_sqls + 'split_labor_row) '
	
	sqls = sqls + " select crbudgets.nextval, y.* from ( " + &
			 " select " + string(i_bv_id) + ", year, " + string(allocation_id) + ",'Amt', " + &
			 "	jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec, " + &
			 element_list + " 2 " + & 
			 " from cbde_calc_" + session_id + " " + &
			 " where cr_budget_version_id = " + string(i_bv_id) + &
			 "	and nvl(allocation_id,0) = " + string(allocation_id) + &
			 "	and nvl(split_labor_row,0) = 2 " + &
			 " and (" + nvl_element_list + " year, 'Amt') in ( " + &
			 "	 select " + nvl_element_list + " year, 'Amt' from cbde_calc_" + session_id + &
			 "   where cr_budget_version_id = " + string(i_bv_id) + &
			 "		 and nvl(allocation_id,0) = " + string(allocation_id) + &
			 "		 and nvl(split_labor_row,0) = 2 " + &
			 "	 minus " + &
			 "  select " + nvl_element_list + " year, " + '"TYPE"' + " from cr_budget_data_entry a " + &
			 "   where a.cr_budget_version_id = " + string(i_bv_id) + &
			 "		 and nvl(a.allocation_id,0) = " + string(allocation_id) + &
			 "		 and nvl(a.split_labor_row,0) = 2 " 
			 
//	sqls = sqls + " and " + calc_selection_screen_dept_sqls

	////	If they selected a budget component from the first screen apply that where clause criteria here.	
	if counter > 0 then  // identified a column as labor ... did they choose a component?
		
		budgeting_element = f_cr_clean_string(budgeting_element)
		choose case i_component_id
			case 3
				//	All
			case else
//				sqls = sqls + ' and a.' + budgeting_element + " " + i_selection_screen_labor_sqls
		end choose
	end if
	
	sqls = sqls + ") "
	
	sqls = sqls + ' ) y '

	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Inserting Amt rows into cr_budget_data_entry: " + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		i_calc_all_in_progress = false
		i_calc_all_rtn = -1
		return -1
	end if

	////
	////	STEP 6: Delete any rows with zeros in all months from cr_budget_data_entry
	////
	sqls = "delete from cr_budget_data_entry a " + &
			 " where cr_budget_version_id = " + string(i_bv_id) + &
			 "   and nvl(allocation_id,0) = " + string(allocation_id) + &
			 '   and "TYPE" =  ' + " 'Amt' " + &
			 "   and exists (" + &
			 "		select 1 from cr_budget_data_entry b " + &
			 "		 where nvl(b.split_labor_row ,0) = 2 " + delete_1 + &
			 " 		and a.cr_budget_version_id = b.cr_budget_version_id " + &
			 "			and a.year = b.year) "

//	sqls = sqls + " and " + calc_selection_screen_dept_sqls

	////	If they selected a budget component from the first screen apply that where clause criteria here.
	if counter > 0 then  // identified a column as labor ... did they choose a component?
		
		budgeting_element = f_cr_clean_string(budgeting_element)
		choose case i_component_id
			case 3
				//	All
			case else
//				sqls = sqls + ' and a.' + budgeting_element + " " + i_selection_screen_labor_sqls
		end choose
	end if
	
	// not including total in the where clause since the totals aren't calculated until the end of this script
	sqls = sqls + &
			 " and jan = 0 and feb = 0 and mar = 0 and apr = 0 and may = 0 and jun = 0 " + &
			 " and jul = 0 and aug = 0 and sep = 0 and oct = 0 and nov = 0 and dec = 0 "
			 
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Deleting rows from cr_budget_data_entry with all zeros: " + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		i_calc_all_in_progress = false
		i_calc_all_rtn = -1
		return -1
	end if
	
	commit;
	
	////
	////	Truncate the temp table for the next allocation in the loop
	////
	sqls = "delete from cbde_calc_" + session_id
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Deleting from cbde_calc_" + session_id + ": " + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		i_calc_all_in_progress = false
		i_calc_all_rtn = -1
		return -1
	end if
	
next

rtn = uf_calc_totals_all()

if rtn < 0 then
	i_calc_all_in_progress = false
	return -1
end if

// Drop temp table
sqls = "drop table cbde_calc_" + session_id
execute immediate :sqls;	

commit;

i_calc_all_in_progress = false


return 1
end function

public function longlong uf_reverse_price_qty ();string sqls, element, element_str, element_str2, element_str_select, budgeting_element,element_column,element_table, sqls_in, sqls_in_select, &
		 rate_check_sqls, rate_check_elements, rate_join_sqls
longlong rtn, rate_type_id, g, num_rows, counter,start_year, actuals_month

if i_called_from_calc_all then
	i_calc_all_in_progress = true
end if

////	What is the rate type ... get from the template
rate_type_id = 0
select rate_type_id into :rate_type_id
from cr_budget_rate_type_template
where template_id = :i_template_id;

if rate_type_id = 0 or isnull(rate_type_id) then 
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: There is no rate type defined for this template.  A rate type must be assigned to this template" + &
		" to be able to price quantities.")
	f_pp_msgs("  ")
	i_calc_all_in_progress = false
	return -1
end if

start_year = 0
select start_year,actuals_month into :start_year, :actuals_month
from cr_budget_version
where cr_budget_version_id = :i_bv_id;

if isnull(actuals_month) then actuals_month = 0

if start_year = 0 or isnull(start_year) then 
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Could not find the budget version start year")
	f_pp_msgs("  ")
	i_calc_all_in_progress = false
	return -1
end if


rtn = uf_add_missing_qty_rows()
if rtn < 0 then 
	i_calc_all_in_progress = false
	return -1
end if

////	Get the elements defined as the level of detail for the rate type
sqls = 'select upper(b.budgeting_element) from cr_elements b, cr_budget_rate_type_element a where ' + &
	'a.element_id = b.element_id and a.rate_type_id = ' + string(rate_type_id)
uo_ds_top ds_elements
ds_elements = create uo_ds_top
f_create_dynamic_ds(ds_elements,"grid",sqls,sqlca,true)
num_rows = ds_elements.rowcount()
element_str = ' and '
element_str2 = ' and (year,'
element_str_select = ' select year,'
sqls_in = ' and (year,'
sqls_in_select = ' select distinct year,'
for g = 1 to num_rows
	element = ds_elements.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	element_str = element_str + 'b.' + element + ' = c.' + element + ' and '
	element_str = element_str + 'a.' + element + ' = c.' + element + ' and '
	element_str2 = element_str2 + 'a.' + element + ','
	sqls_in = sqls_in + " nvl(a." + element + ",' '),"
	sqls_in_select = sqls_in_select + " nvl(a." + element + ",' '),"
	element_str_select = element_str_select + element + ','
next

////	Get the remaining elements
//// SEK 062410: Don't want to include elements that aren't in the template, otherwise the update below won't work properly...
////		i.e. the Amt row has had derivations run against it and the corresponding Qty row was added in the insert above.
sqls = 'select upper(b.budgeting_element) from cr_elements b where element_id not in (select element_id from cr_budget_rate_type_element ' + &
		 ' where rate_type_id = ' + string(rate_type_id) + ') ' + &
		 ' and element_id in (select element_id from cr_budget_templates_fields where template_id = ' + string(i_template_id) + ")"
ds_elements.setsqlselect(sqls)
ds_elements.retrieve()
num_rows = ds_elements.rowcount()
for g = 1 to num_rows
	element = ds_elements.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls_in = sqls_in + " nvl(a." + element + ",' '),"
	sqls_in_select = sqls_in_select + " nvl(" + element + ",' '),"
	element_str = element_str + "nvl(a." + element + ",' ') = nvl(c." + element + ",' ') and "
next

////	Get the detail attributes
sqls = "select upper(description) from cr_budget_additional_fields"
uo_ds_top ds_detail_full
ds_detail_full = create uo_ds_top
f_create_dynamic_ds(ds_detail_full,"grid",sqls,sqlca,true)
num_rows = ds_detail_full.rowcount()
for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	//	use 0 on the nvl so I don't have to check the data type
	sqls_in = sqls_in + " nvl(a." + element + ",'0'),"
	sqls_in_select = sqls_in_select + " nvl(" + element + ",'0'),"
	element_str = element_str + " nvl( a." + element + ",'0') = nvl(c." + element + ",'0') and "
//	element_str2 = element_str2 + 'a.' + element + ','
//	element_str_select = element_str_select + element + ','
next

destroy ds_detail_full
	
element_str = mid(element_str,1,len(element_str) - 4)
element_str2 = mid(element_str2,1,len(element_str2) - 1) + ') in ('
element_str_select  = mid(element_str_select,1,len(element_str_select) - 1)
element_str_select = element_str_select + ' from cr_budget_rate_type_rates where rate_type_id = ' + string(rate_type_id) + ') '

sqls_in = mid(sqls_in,1,len(sqls_in) - 1) + ') in ('
sqls_in_select = mid(sqls_in_select,1,len(sqls_in_select) - 1) + ' from cr_budget_data_entry where cr_budget_version_id =  ' + string(i_bv_id) + " "


// SEK 042910: Added logic to obey disabled month
if i_obey_disable_month and i_have_disabled_month then
	sqls = " update cr_budget_data_entry a set (jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec) = (select " + &
		"sum(decode(a.year," + string(start_year) + ",case when 1 <= " + string(actuals_month+1) + " then a.jan else case b.jan when 0 then 0 else round(c.jan / b.jan,2) end end, case b.jan when 0 then 0 else round(c.jan / b.jan,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 2 <= " + string(actuals_month+1) + " then a.feb else case b.feb when 0 then 0 else round(c.feb / b.feb,2) end end, case b.feb when 0 then 0 else round(c.feb / b.feb,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 3 <= " + string(actuals_month+1) + " then a.mar else case b.mar when 0 then 0 else round(c.mar / b.mar,2) end end, case b.mar when 0 then 0 else round(c.mar / b.mar,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 4 <= " + string(actuals_month+1) + " then a.apr else case b.apr when 0 then 0 else round(c.apr / b.apr,2) end end, case b.apr when 0 then 0 else round(c.apr / b.apr,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 5 <= " + string(actuals_month+1) + " then a.may else case b.may when 0 then 0 else round(c.may / b.may,2) end end, case b.may when 0 then 0 else round(c.may / b.may,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 6 <= " + string(actuals_month+1) + " then a.jun else case b.jun when 0 then 0 else round(c.jun / b.jun,2) end end, case b.jun when 0 then 0 else round(c.jun / b.jun,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 7 <= " + string(actuals_month+1) + " then a.jul else case b.jul when 0 then 0 else round(c.jul / b.jul,2) end end, case b.jul when 0 then 0 else round(c.jul / b.jul,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 8 <= " + string(actuals_month+1) + " then a.aug else case b.aug when 0 then 0 else round(c.aug / b.aug,2) end end, case b.aug when 0 then 0 else round(c.aug / b.aug,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 9 <= " + string(actuals_month+1) + " then a.sep else case b.sep when 0 then 0 else round(c.sep / b.sep,2) end end, case b.sep when 0 then 0 else round(c.sep / b.sep,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 10 <= " + string(actuals_month+1) + " then a.oct else case b.oct when 0 then 0 else round(c.oct / b.oct,2) end end, case b.oct when 0 then 0 else round(c.oct / b.oct,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 11 <= " + string(actuals_month+1) + " then a.nov else case b.nov when 0 then 0 else round(c.nov / b.nov,2) end end, case b.nov when 0 then 0 else round(c.nov / b.nov,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 12 <= " + string(actuals_month+1) + " then a.dec else case b.dec when 0 then 0 else round(c.dec / b.dec,2) end end, case b.dec when 0 then 0 else round(c.dec / b.dec,2) end)) " + &
		"from cr_budget_rate_type_rates b, cr_budget_data_entry c " + &
		'where c.cr_budget_version_id = ' + string(i_bv_id) + ' and c.year = b.year and b.year = a.year and b.rate_type_id = ' + string(rate_type_id) + ' and c."TYPE" = ' + &
		" 'Amt' " + element_str + ') where a."TYPE" = ' + &
		" 'Qty' and a.cr_budget_version_id = " + string(i_bv_id) + element_str2 + element_str_select
else
	sqls = " update cr_budget_data_entry a set (jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec) = (select " + &
		"sum(decode(a.year," + string(start_year) + ",case when 1 <= " + string(actuals_month) + " then a.jan else case b.jan when 0 then 0 else round(c.jan / b.jan,2) end end, case b.jan when 0 then 0 else round(c.jan / b.jan,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 2 <= " + string(actuals_month) + " then a.feb else case b.feb when 0 then 0 else round(c.feb / b.feb,2) end end, case b.feb when 0 then 0 else round(c.feb / b.feb,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 3 <= " + string(actuals_month) + " then a.mar else case b.mar when 0 then 0 else round(c.mar / b.mar,2) end end, case b.mar when 0 then 0 else round(c.mar / b.mar,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 4 <= " + string(actuals_month) + " then a.apr else case b.apr when 0 then 0 else round(c.apr / b.apr,2) end end, case b.apr when 0 then 0 else round(c.apr / b.apr,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 5 <= " + string(actuals_month) + " then a.may else case b.may when 0 then 0 else round(c.may / b.may,2) end end, case b.may when 0 then 0 else round(c.may / b.may,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 6 <= " + string(actuals_month) + " then a.jun else case b.jun when 0 then 0 else round(c.jun / b.jun,2) end end, case b.jun when 0 then 0 else round(c.jun / b.jun,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 7 <= " + string(actuals_month) + " then a.jul else case b.jul when 0 then 0 else round(c.jul / b.jul,2) end end, case b.jul when 0 then 0 else round(c.jul / b.jul,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 8 <= " + string(actuals_month) + " then a.aug else case b.aug when 0 then 0 else round(c.aug / b.aug,2) end end, case b.aug when 0 then 0 else round(c.aug / b.aug,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 9 <= " + string(actuals_month) + " then a.sep else case b.sep when 0 then 0 else round(c.sep / b.sep,2) end end, case b.sep when 0 then 0 else round(c.sep / b.sep,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 10 <= " + string(actuals_month) + " then a.oct else case b.oct when 0 then 0 else round(c.oct / b.oct,2) end end, case b.oct when 0 then 0 else round(c.oct / b.oct,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 11 <= " + string(actuals_month) + " then a.nov else case b.nov when 0 then 0 else round(c.nov / b.nov,2) end end, case b.nov when 0 then 0 else round(c.nov / b.nov,2) end)), " + &
		"sum(decode(a.year," + string(start_year) + ",case when 12 <= " + string(actuals_month) + " then a.dec else case b.dec when 0 then 0 else round(c.dec / b.dec,2) end end, case b.dec when 0 then 0 else round(c.dec / b.dec,2) end)) " + &
		"from cr_budget_rate_type_rates b, cr_budget_data_entry c " + &
		'where c.cr_budget_version_id = ' + string(i_bv_id) + ' and c.year = b.year and b.year = a.year and b.rate_type_id = ' + string(rate_type_id) + ' and c."TYPE" = ' + &
		" 'Amt' " + element_str + ') where a."TYPE" = ' + &
		" 'Qty' and a.cr_budget_version_id = " + string(i_bv_id) + element_str2 + element_str_select
end if

//sqls = sqls + " and " + i_selection_screen_sqls + i_addl_sql_price_qty
//
//sqls_in_select = sqls_in_select + " and " + i_selection_screen_sqls + i_addl_sql_price_qty

sqls_in_select = sqls_in_select + 'and "TYPE" = '
sqls_in_select = sqls_in_select + "'Amt' "

////	If they selected a budget component from the first screen apply that where clause criteria here.
counter = 0
select b.budgeting_element, b.element_column, b.element_table, count(*)
into :budgeting_element, :element_column, :element_table, :counter
from cr_budget_templates_fields a, cr_elements b
where a.element_id = b.element_id
and a.template_id = :i_template_id
and nvl(identifies_labor,0) = 1
group by b.budgeting_element, b.element_column, b.element_table;

if isnull(counter) then counter = 0

if counter > 0 then  // identified a column as labor ... did they choose a component?
	
	budgeting_element = f_cr_clean_string(budgeting_element)
	
	if i_component_id = 3 then
	  	// i_selection_screen_labor_sqls is not populated in this case so
	  	// skip this step
	else
//		sqls = sqls + " and " + budgeting_element + " " + i_selection_screen_labor_sqls
//		sqls_in_select = sqls_in_select + " and " + budgeting_element + " " + i_selection_screen_labor_sqls
	end if

end if

sqls_in_select = sqls_in_select  + ') '

sqls = sqls + sqls_in + sqls_in_select 


execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_budget_data_entry : " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	f_pp_msgs(sqls)
	rollback;
	i_calc_all_in_progress = false
	i_calc_all_rtn = -1
	return -1
end if

commit;

//if tab_options.tabpage_calc.cbx_pq_apply_gross_up.checked then
//	wf_price_qty_gross_up()
//end if

rtn = uf_calc_totals_all()

if rtn = -1 then
	i_calc_all_in_progress = false
	return -1
end if

i_calc_all_in_progress = false

return 1
end function

public function longlong uf_setup ();string cv, field_name, sqls, control_total_label, disable_month
longlong cr_approval_status_id, start_year, end_year, rtn, disable_year, display_control_total, enable_control_total

select restrict_to_current_year into :i_template_year_restriction
from cr_budget_templates
where template_id = :i_template_id;

if isnull(i_template_year_restriction) then i_template_year_restriction = 0

select id into :i_budget_value_type_id
from cr_budget_value_type_template
where template_id = :i_template_id;

if isnull(i_budget_value_type_id) or i_budget_value_type_id = 0 then
	f_pp_msgs("ERROR: Cannot find any valid values for this template.")
	return -1
end if

//  Get the name of the "Department" field in the code block.
setnull(cv)
select upper(rtrim(control_value)) into :cv from cr_system_control
 where upper(rtrim(control_name)) = 'DEPARTMENT FIELD';
if isnull(cv) or cv = "" then
	f_pp_msgs("ERROR: Cannot find the DEPARTMENT FIELD in cr_system_control.")
	return -1
end if

select element_id, element_group, datawindow
into :i_element_id, :i_element_group, :i_element_datawindow
from cr_budget_templates_budget_by
where template_id = :i_template_id;

i_element_group = f_cr_clean_string(i_element_group)

select element_table, element_column,budgeting_element
into :i_element_table,:i_element_column, :i_dept_element_bdg
from cr_elements
where element_id = :i_element_id;

field_name = f_cr_clean_string(i_element_column)
i_element_column  = f_cr_clean_string(i_element_column)
i_dept_element_bdg = f_cr_clean_string(i_dept_element_bdg)
i_dept_element_act = f_cr_clean_string(cv)


// For w_cr_field_search
select default_company into :i_default_company
from cr_budget_version where cr_budget_version_id = :i_bv_id;
if isnull(i_default_company) then i_default_company = "" 

////
////	i_actuals_month is looked up in wf_disable_months.  That function is not called
////		when using the alternate entry method.  So, copy the look up here.  I left the 
////		lookup where it is now as well.  I'm trying to touch as little existing code 
////		as possible
////
select actuals_month into :i_actuals_month
from cr_budget_version
where cr_budget_version_id = :i_bv_id;

i_bv_start_year  = 0
i_bv_budget_year = 0
i_bv_end_year    = 0
i_open_for_entry = 0
//  i_open_for_entry:
//    There is a yes/no switch on cr_budget_version that determines if the budget version
//    can be altered.
select start_year, current_year, end_year, open_for_entry
  into :i_bv_start_year, :i_bv_budget_year, :i_bv_end_year, :i_open_for_entry
  from cr_budget_version where cr_budget_version_id = :i_bv_id;
if isnull(i_bv_start_year)  then i_bv_start_year  = 0
if isnull(i_bv_budget_year) then i_bv_budget_year = 0
if isnull(i_bv_end_year)    then i_bv_end_year    = 0
if isnull(i_open_for_entry) then i_open_for_entry = 0

select display_control_total, control_total_label,enable_control_total, lower(disable_month), disable_year
into :display_control_total, :control_total_label, :enable_control_total, :disable_month, :disable_year
from cr_budget_templates
where template_id = :i_template_id;

if isnull(display_control_total) then display_control_total = 0
if isnull(enable_control_total) then enable_control_total = 0
if isnull(control_total_label) then control_total_label = 'none'
if isnull(disable_month) then disable_month = ""

if disable_month = "" then
	i_have_disabled_month = false
else
	i_have_disabled_month = true
end if

// Hard Code to 3 for now
i_component_id = 3

//	Are we enforcing active entry
select upper(rtrim(control_value)) into :i_enforce_active_entry from cr_system_control 
where upper(control_name) = 'CR BUDGET: CHECK ACTIVE ENTRY';
if isnull(i_enforce_active_entry) or i_enforce_active_entry = "" then
	messagebox("Active Entry", "Cannot find the CR Budget: Check Active Entry control in cr_system_control.")
	i_enforce_active_entry = 'YES'
end if

if i_enforce_active_entry = 'YES' then

//	uo_ds_top ds_ae
//	ds_ae = create uo_ds_top
//
//	dept_where = " in (" + &
//			" select element_value from " + &
//			" 	(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
//			" 		from cr_structure_values2_bdg_temp a" + &
//			" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
//			" 		start with structure_id =  " + string(i_structure_id) + &
//			" 		and element_value in (" + i_rollup_value_id_list + ") " + &
//			" 		connect by prior element_value = parent_value " + &
//			" 		and structure_id =  " + string(i_structure_id) + ") "
//
//	sqls = "select count(*),users from cr_budget_active_entry where dept_value in (" + &
//		"select " + i_element_column + " from " + i_element_table + " where element_type = 'Budget' and " + i_element_column + " " + dept_where + ")) " + &
//		" and cr_budget_version_id = " + string(i_bv_id) + &
//		" group by users "
//		
//	f_create_dynamic_ds(ds_ae,"grid",sqls,sqlca,true)
//	if ds_ae.rowcount() > 0 then
//		////	Set an instance variable so the close event knows if dw_entry was created.  If it has not been then the sql in the close event will fail.  It doesn't need to fire.
//		i_dw_entry_created = false
//		users = ds_ae.getitemstring(1,2)
//		messagebox("Next", "User " + trim(users) + " is modifying the budget for " + &
//			": " + i_rollup_value_id_list)
//		return
//	else
//		i_dw_entry_created = true
//	end if
//	
//	sqls = "insert into cr_budget_active_entry (users,cr_budget_version_id,dept_value)  " + &
//		" select '" + s_user_info.user_id + "'," + string(i_bv_id) + ",element_value from " + &
//		" (select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
//			" 		from cr_structure_values2_bdg_temp a" + &
//			" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
//			" 		start with structure_id =  " + string(i_structure_id) + &
//			" 		and element_value in (" + i_rollup_value_id_list + ") " + &
//			" 		connect by prior element_value = parent_value " + &
//			" 		and structure_id =  " + string(i_structure_id) + ") "
//	
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode = 0 then
//		commit;
//	elseif mid(sqlca.sqlerrtext,1, 9) = 'ORA-00001' then
//		messagebox("Next", "This " + msg_element + " is being modified by another user.")
//		return
//	else
//		messagebox("Next", "ERROR: inserting into cr_budget_active_entry:~n~n" + &
//			sqlca.SQLErrText, Exclamation!)
//		rollback;
//		return
//	end if
//	
//	destroy ds_ae
	
end if		//	if i_enforce_active_entry then
	
		
if i_alternate_entry = 'YES' then
	
	//	Build the year filter 
	select start_year, end_year into :start_year,:end_year
	from cr_budget_version 
	where cr_budget_version_id = :i_bv_id;
	
	
	// Don't call for now...
//	rtn = wf_build_dw_entry()
//	if rtn < 0 then return
	
	uo_ds_top ds_approval_counter
	ds_approval_counter = create uo_ds_top
	
	sqls = " select count(*) " + &
			" from cr_budget_approval " + &
			" where cr_budget_version_id = " + string(i_bv_id) + &
			" and revision = (select max(revision) " + &
			" 	from cr_budget_approval " + &
			" 	where cr_budget_version_id = " + string(i_bv_id) + &
			" 	) "  + &
			" and cr_approval_status_id in (2,3)	"
			
//						" and department " + i_selection_screen_dept_where + &
//and department " + i_selection_screen_dept_where + "
			
	f_create_dynamic_ds(ds_approval_counter,"grid",sqls,sqlca,true)
	if ds_approval_counter.rowcount() > 0 then
		i_appr_counter = ds_approval_counter.getitemnumber(1,1)
		if isnull(i_appr_counter) then i_appr_counter = 0
		destroy ds_approval_counter
	else
		i_appr_counter = 0
	end if
	
	if i_appr_counter > 0 then 
		i_approved = true
		f_pp_msgs("Information: Approvals have been started so this " + f_replace_string(i_dept_element_bdg,'_',' ','All') + " will be marked as View Only")
	end if

	uo_ds_top ds_approval_status
	ds_approval_status = create uo_ds_top

	sqls = " select cr_approval_status_id " + &
			" from cr_budget_approval " + &
			" where cr_budget_version_id = " + string(i_bv_id) + &
			" and revision = (select max(revision) " + &
			" 	from cr_budget_approval " + &
			" 	where cr_budget_version_id = " + string(i_bv_id) + &
			" 	) "  
			
//						" and department " + i_selection_screen_dept_where + &
//and department " + i_selection_screen_dept_where + "
			
	f_create_dynamic_ds(ds_approval_status,"grid",sqls,sqlca,true)
	if ds_approval_status.rowcount() > 0 then
		cr_approval_status_id = ds_approval_status.getitemnumber(1,1)
		if isnull(cr_approval_status_id) then cr_approval_status_id = 0
		destroy ds_approval_status
	else
		cr_approval_status_id = 0
	end if
		
	if cr_approval_status_id = 4 then
		i_approved = true
		////	Simulate approvals in progress
		i_appr_counter = 1
		f_pp_msgs("Information: The max approval revision for this "  + f_replace_string(i_dept_element_bdg,'_',' ','All') + " has been approved.~n~nChanges cannot be made until the current approval revision is marked as 'un-approved'.")
	end if
	
end if

return 1
end function

public function longlong uf_build_dw_entry ();//**********************************************************************************************
//
//  The where clause for estimate_charge_type reflects work order estimates !!! 
//
//**********************************************************************************************
longlong yr, ii, iii, min_year, max_year, num_years, btn_color, num_cols, i, &
		 window_color, num_months, co_id, rtn, max_revision, budget_version_id, &
		 open_for_entry, pp_cv, max_year_hold, counter, g, num_rows, num_elements, yr_value, &
		 apply_to_year, gg, num_total_columns, element_id, where_clause_id, computed_col_bv_id, &
		 group_by_counter, amount_x, level, b, num_additional_fields, num_elements_check, total_x, &
		 color, addl_x, x_1, col_width, display_type, group_by_id, actuals_month, start_year, array_counter, addl_x2, &
		 col_set, addl_x_mult, wcid, display_control_total, enable_control_total, apply_on_recalc, disable_year
string sqls, outer_sqls, dw_syntax, presentation, error_syntaxfromSQL, error_create, col, &
		 x_val, width_val, compute_num, last_compute_num, all_total, rc, extra_field, &
		 account_field, work_order_field, ext_company_field, setting, cv,  &
		 computed_field_name, computed_field_descr, subselect, subselect_alias, join_to_cbd, &
		 element_column, element_table, element_descr, outer_group_sqls, inner_group_sqls, &
		 budget_version, join_sqls, budget_or_actuals, descr, col_array[], reset_array[], &
		 col_alias, col_name, element_col_for_group, name, group_by_1, group_by_2, group_by_3, &
		 subtot_columns[], subtot_columns2[], subtot_columns3[], split_scroll_array[], split_scroll_pos, &
		 amount_or_quantity, sqls_b1, detail_fields, field, element_array[], ddlb_values, sqls_da, budgeting_element, &
		 group_by_1_1, actuals_month_array[], element_column_array[], element_table_array[], sort_string_hold, b_descr, control_total_label, &
		 disable_month 
boolean found, disable_col, description_column, element_column_b
datawindow dw_entry

longlong dml

sqls = "select a.* from cr_elements a, cr_budget_templates_fields b where a.element_id = b.element_id and " + &
	 " b.template_id = " + string(i_template_id) + ' order by display_order '
uo_ds_top ds_elements2
ds_elements2 = create uo_ds_top
f_create_dynamic_ds(ds_elements2,"grid",sqls,sqlca,true)
num_elements = ds_elements2.rowcount()

i_ds_elements = ds_elements2

for g = 1 to num_elements
	array_counter++
	element_array[array_counter] = f_cr_clean_string(ds_elements2.getitemstring(g,"budgeting_element"))
	element_table_array[array_counter] = f_cr_clean_string(ds_elements2.getitemstring(g,"element_table"))
	element_column_array[array_counter] = f_cr_clean_string(ds_elements2.getitemstring(g,"element_column"))
next


i_element_array_descr = element_array
i_element_table_array_descr	= element_table_array
	

////
////	Are there any computed columns for the selected template?
////
sqls = "select a.* from cr_budget_where a, cr_budget_templates_computed b where a.where_clause_id = b.where_clause_id  and b.template_id = " + string(i_template_id) + &
	" order by b.display_order "
uo_ds_top ds_where_clause2
ds_where_clause2 = create uo_ds_top
f_create_dynamic_ds(ds_where_clause2,"grid",sqls,sqlca,true)
num_rows = ds_where_clause2.rowcount()

i_ds_where = ds_where_clause2

sqls = "select "
for b = 1 to upperbound(element_array)
	col = element_array[b]
	sqls = sqls + col + ','
next

////	type and year are always included.  Put them before any computed columns
sqls = sqls + 'type,year,'

////	add the computed columns to the select string
for b = 1 to num_rows
	////	Now, we are going to allow the columns to be changed.  There are two actuals columns (e.g. current year actuals, prior year actuals, etc)
	////		and two budget columns (e.g. current_approved_budget, current_approved_forecast, etc)  These four columns exist on cr_budget_data_entry,
	////		So, make sure to select the correct columns.  I will relabel them at the end to the description they defined.
//		col = f_cr_clean_string(ds_where_clause2.getitemstring(b,"description"))
	wcid = ds_where_clause2.getitemnumber(b,"where_clause_id")
	choose case wcid
		case 1
			col = 'prior_year_actuals'
		case 2
			col = 'current_year_actuals'
		case 3
			col = 'current_approved_budget'
		case 4
			col = 'current_approved_forecast'
	end choose
	sqls = sqls + col + ','
next

////	 Do we add the control total?
select display_control_total, control_total_label,enable_control_total, lower(disable_month), disable_year
into :display_control_total, :control_total_label, :enable_control_total, :disable_month, :disable_year
from cr_budget_templates
where template_id = :i_template_id;

if isnull(display_control_total) then display_control_total = 0
if isnull(enable_control_total) then enable_control_total = 0
if isnull(control_total_label) then control_total_label = 'none'
if isnull(disable_month) then disable_month = ""

if disable_month = "" then
	i_have_disabled_month = false
else
	i_have_disabled_month = true
end if

if display_control_total = 1 then
	col = 'control_total'
	sqls = sqls + col + ','
end if

////
//// Add any detail attributes
////
uo_ds_top ds_da2
ds_da2 = create uo_ds_top
sqls_da = "select * from cr_budget_additional_fields where id in (select id from cr_budget_templates_dtl_fields " + &
	" where template_id = " + string(i_template_id) + ')'
f_create_dynamic_ds(ds_da2,"grid",sqls_da,sqlca,true)
num_additional_fields = ds_da2.rowcount()
for g = 1 to num_additional_fields
	field = f_cr_clean_string(ds_da2.getitemstring(g,"description"))
	if g = num_additional_fields then
		detail_fields = detail_fields + field
	else
		detail_fields = detail_fields + field + ','
	end if
next

////
////	Add the remaining static columns to the select
////
sqls = sqls + "total,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec"
if num_additional_fields = 0 then
	sqls = sqls + ",cr_budget_version_id,id"
else
	sqls = sqls + ', ' + detail_fields + ",cr_budget_version_id,id " 
end if

//////	The computed column was added to the select above.  If they checked the recalc option then add
//////		a computed field (e.g. prior_year_actuals_1) that contains the subselect.  Below, use dot notation
//////		to move this computed amount to the selected column (prior_year_actuals) so the dw.update works.
//////		A computed column cannot be used in the dw.update for a column even if the computed column is named
//////		the same as the column on the table because it wasn't retrieved.  I'm retrieving it above and calculating it here.
//for b = 1 to num_rows
//	if tab_entry.tabpage_step1.cbx_recalc_computed.checked then
//		sqls = sqls + ','
//		where_clause_id = ds_where_clause2.getitemnumber(b,"where_clause_id")
//		apply_on_recalc = ds_where_clause2.getitemnumber(b,"apply_on_recalc")
//		if isnull(apply_on_recalc) then apply_on_recalc = 1
//		if apply_on_recalc = 1 then
//			if ds_where_clause2.getitemstring(b,"budget_or_actuals") = 'Actuals' then
//				sqls = sqls + wf_build_subselect(where_clause_id,0,'Actuals',0,0)
//			else
//				computed_col_bv_id = ds_where_clause2.getitemnumber(b,"cr_budget_version_id")
//				sqls = sqls + wf_build_subselect(where_clause_id,0,'Budget',computed_col_bv_id,0)
//			end if
//		end if
//	end if
//next
	

sqls = sqls + " from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + " and " + i_dept_element_bdg + &
	" in (" + &
	" select element_value from " + &
	" 	(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
	" 		from cr_structure_values2_bdg_temp a" + &
	" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
	" 		start with structure_id =  " + string(i_structure_id) + &
	" 		and element_value in (" + i_rollup_value_id_list + ") " + &
	" 		connect by prior element_value = parent_value " + &
	" 		and structure_id =  " + string(i_structure_id) + ") "

if i_template_year_restriction = 1 then
	sqls = sqls + " ) and year = " + string(i_bv_budget_year) + " "
else
	sqls = sqls + ") "
end if
	
i_selection_screen_sqls =  " " + i_dept_element_bdg + &
	" in (" + &
	" select element_value from " + &
	" 	(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
	" 		from cr_structure_values2_bdg_temp a" + &
	" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
	" 		start with structure_id =  " + string(i_structure_id) + &
	" 		and element_value in (" + i_rollup_value_id_list + ") " + &
	" 		connect by prior element_value = parent_value " + &
	" 		and structure_id =  " + string(i_structure_id) + ") " 
	
// If the template is restricting on a certain year, other windows that use i_selection_screen_sqls will blow up
//	because they select from tables without year as a field.  Use the new i_selection_screen_sqls_no_yr in those 
// windows instead.
i_selection_screen_sqls_no_yr = i_selection_screen_sqls + ") "

if i_template_year_restriction = 1 then
	i_selection_screen_sqls = i_selection_screen_sqls + " ) and year = " + string(i_bv_budget_year) + " "
else
	i_selection_screen_sqls = i_selection_screen_sqls + ") "
end if

					
////	There will be times where I want the element or element grouping critiera without the labor component 
////		(e.g. allocation results with a cost element different from the labor component selected...unlikely but possible?
////
//// SEK 01/12/10: If the template is restricted by the current year, the allocations will blow up because cr_budget_data_entry
////					 is aliased and another table with year as a field is selected.  Alias the selected fields with 
////					 cr_budget_data_entry here because that will work in all places and the allocations code will replace
////					 it with the appropriate alias.

i_selection_screen_dept_sqls = " cr_budget_data_entry." + right(i_selection_screen_sqls,len(i_selection_screen_sqls) - 1)

if i_template_year_restriction = 1 then
	i_selection_screen_dept_sqls = f_replace_string(i_selection_screen_dept_sqls, "year", "cr_budget_data_entry.year","all")
end if

i_selection_screen_dept_where =   + &
	" in (" + &
	" select element_value from " + &
	" 	(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
	" 		from cr_structure_values2_bdg_temp a" + &
	" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
	" 		start with structure_id =  " + string(i_structure_id) + &
	" 		and element_value in (" + i_rollup_value_id_list + ") " + &
	" 		connect by prior element_value = parent_value " + &
	" 		and structure_id =  " + string(i_structure_id) + ") " 
	
// If the template is restricting on a certain year, other windows that use i_selection_screen_dept_where will blow up
//	because they select from tables without year as a field.  Use the new i_selection_screen_dept_where_no_yr in those 
// windows instead.
i_selection_screen_dept_where_no_yr = i_selection_screen_dept_where + ") "

if i_template_year_restriction = 1 then
	i_selection_screen_dept_where = i_selection_screen_dept_where + " ) and year = " + string(i_bv_budget_year) + " "
else
	i_selection_screen_dept_where = i_selection_screen_dept_where + ") "
end if

i_selection_screen_dept_supplb_where =   + &
" in (" + &
" select element_value from " + &
" 	(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
" 		from cr_structure_values2_bdg_temp a" + &
" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
" 		start with structure_id =  " + string(i_structure_id) + &
" 		and element_value in (" + i_rollup_value_id_list + ") " + &
" 		connect by prior element_value = parent_value " + &
" 		and structure_id =  " + string(i_structure_id) + ") )" 

////	Right now, making the assumption the departments (or budget by field) will be at the same level for budget and actuals.
////		Otherwise, the join gets a little tricky and performance suffers. From our clients so far, this is ok.
i_selection_screen_dept_suppla_where =   + &
" in (" + &
" select element_value from " + &
" 	(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
" 		from cr_structure_values2_bdg_temp a" + &
" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
" 		start with structure_id =  " + string(i_structure_id) + &
" 		and element_value in (" + i_rollup_value_id_list + ") " + &
" 		connect by prior element_value = parent_value " + &
" 		and structure_id =  " + string(i_structure_id) + ") )" 
		
	
////
////	Did they choose a component to filter on?
////
counter = 0
select b.budgeting_element, b.element_column, b.element_table,b.description, count(*)
into :budgeting_element, :element_column, :element_table,:b_descr, :counter
from cr_budget_templates_fields a, cr_elements b
where a.element_id = b.element_id
and a.template_id = :i_template_id
and nvl(identifies_labor,0) = 1
group by b.budgeting_element, b.element_column, b.element_table,b.description;

if isnull(counter) then counter = 0

if counter > 0 then  // identified a column as labor ... did they choose a component?
	choose case i_component_id
		case 1	// Labor
			sqls = sqls + ' and ' + f_cr_clean_string(budgeting_element) + ' in (select ' + element_column + ' from ' + element_table + ' where '
			sqls = sqls + element_table + ".element_type = 'Budget' and "
			sqls = sqls + element_table + ".labor_flag in ('L','LC') ) "
			
			i_selection_screen_labor_sqls = ' in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_labor_sqls = i_selection_screen_labor_sqls + element_table + ".element_type = 'Budget' and "
			i_selection_screen_labor_sqls = i_selection_screen_labor_sqls + element_table + ".labor_flag in ('L','LC') ) "
			
			i_selection_screen_sqls = i_selection_screen_sqls + ' and ' + f_cr_clean_string(budgeting_element) + ' in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_sqls = i_selection_screen_sqls + element_table + ".element_type = 'Budget' and "
			i_selection_screen_sqls = i_selection_screen_sqls + element_table + ".labor_flag in ('L','LC') ) "
			
			i_selection_screen_dept_suppla_where = i_selection_screen_dept_suppla_where + ' and "' + upper(f_cr_clean_string(b_descr)) + '" in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_dept_suppla_where = i_selection_screen_dept_suppla_where + element_table + ".element_type = 'Actuals' and "
			i_selection_screen_dept_suppla_where = i_selection_screen_dept_suppla_where + element_table + ".labor_flag in ('L','LC') ) "
			
			i_selection_screen_dept_supplb_where = i_selection_screen_dept_supplb_where + ' and "' + upper(f_cr_clean_string(budgeting_element)) + '" in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_dept_supplb_where = i_selection_screen_dept_supplb_where + element_table + ".element_type = 'Budget' and "
			i_selection_screen_dept_supplb_where = i_selection_screen_dept_supplb_where + element_table + ".labor_flag in ('L','LC') ) "
			
			
			
		case 2 	// Non-Labor
			sqls = sqls + ' and ' + f_cr_clean_string(budgeting_element) + ' in (select ' + element_column + ' from ' + element_table + ' where '
			sqls = sqls + element_table + ".element_type = 'Budget' and "
			sqls = sqls + element_table + ".labor_flag in ('NL','NC') ) "
			
			i_selection_screen_labor_sqls = ' in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_labor_sqls = i_selection_screen_labor_sqls + element_table + ".element_type = 'Budget' and "
			i_selection_screen_labor_sqls = i_selection_screen_labor_sqls + element_table + ".labor_flag in ('NL','NC') ) "
			
			i_selection_screen_sqls = i_selection_screen_sqls + ' and ' + f_cr_clean_string(budgeting_element) + ' in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_sqls = i_selection_screen_sqls + element_table + ".element_type = 'Budget' and "
			i_selection_screen_sqls = i_selection_screen_sqls + element_table + ".labor_flag in ('NL','NC') ) "
			
			i_selection_screen_dept_suppla_where = i_selection_screen_dept_suppla_where + ' and "' + upper(f_cr_clean_string(b_descr)) + '" in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_dept_suppla_where = i_selection_screen_dept_suppla_where + element_table + ".element_type = 'Actuals' and "
			i_selection_screen_dept_suppla_where = i_selection_screen_dept_suppla_where + element_table + ".labor_flag in ('NL','NC') ) "
			
			i_selection_screen_dept_supplb_where = i_selection_screen_dept_supplb_where + ' and "' + upper(f_cr_clean_string(budgeting_element)) + '" in (select ' + element_column + ' from ' + element_table + ' where '
			i_selection_screen_dept_supplb_where = i_selection_screen_dept_supplb_where + element_table + ".element_type = 'Budget' and "
			i_selection_screen_dept_supplb_where = i_selection_screen_dept_supplb_where + element_table + ".labor_flag in ('NL','NC') ) "
			
		case 3	// All
			// no extra sql is needed since all components are being retrieved
	end choose
end if

	
return 1


end function

public function longlong uf_build_cr_budget_data (string a_type);////	a_type = 'Allocations' - builds cr_budget_data_temp from cr_budget_data_entry to apply entry screen allocations.
////									The drilldown_key will contain 'BUDGET ENTRY ALLOCATIONS' || user_id || sessionid
////	a_type = 'Finish' - builds cr_budget_data from cr_budget_data_entry when submitting the budget.
////									The drilldown_key will contain 'FINISH' || user_id || sessionid
////	a_type = 'Results - Allocations' - builds cr_budget_data_entry from cr_budget_data_temp after the budget entry allocations.
////									The drilldown_key will remain null
////	a_type = 'Validations' - builds cr_budget_data_temp from cr_budget_data_entry to apply entry screen validations.
////									The drilldown_key will contain 'BUDGET ENTRY VALIDATIONS' || user_id || sessionid
////	a_type = 'Derivations' - builds cr_budget_data_temp from cr_budget_data_entry to apply entry screen derivations.
////									The drilldown_key will contain 'BUDGET ENTRY DERIVATIONS' || user_id || sessionid
////									This was done because effective dated derivations required month_number and month_number
////									isn't on cr_budget_data_entry
////	a_type = 'Results - Derivations' - builds cr_budget_data_entry from cr_budget_data_temp after the budget entry derivations.
////									The drilldown_key will remain null


string sqls, month_array[], bv, users, gl_jrnl_cat
longlong g, b, num_rows, session_id, num_rows_addl
longlong ent,temp

////		What gl_journal_category do I use?
select control_value into :gl_jrnl_cat
from cr_system_control 
where lower(control_name) = 'cr budget gl jrnl category';

if isnull(gl_jrnl_cat) then gl_jrnl_cat = 'not found'

uo_ds_top ds_budget_elements
ds_budget_elements = create uo_ds_top
sqls = "select budgeting_element from cr_elements"
f_create_dynamic_ds(ds_budget_elements,"grid",sqls,sqlca,true)

uo_ds_top ds_budget_addl_fields
ds_budget_addl_fields = create uo_ds_top
sqls = "select description from cr_budget_additional_fields"
f_create_dynamic_ds(ds_budget_addl_fields,"grid",sqls,sqlca,true)

num_rows = ds_budget_elements.rowcount()
num_rows_addl = ds_budget_addl_fields.rowcount()

if a_type = 'Results - Allocations' then
	
	sqls = "insert into cr_budget_data_entry (id,"
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + '"TYPE",year,prior_year_actuals,current_year_actuals,current_approved_budget,current_approved_forecast,' + &
		'total,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec,cr_budget_version_id,allocation_id,split_labor_row,gross_up_id) select crbudgets.nextval,aa.* from (select '
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + "'Amt', year,0 as prior_year_actuals,0 as current_year_actuals,0 as current_approved_budget,0 as current_approved_forecast" + &
		",0 as total,sum(jan),sum(feb),sum(mar),sum(apr),sum(may),sum(jun),sum(jul),sum(aug),sum(sep)," + &
		"sum(oct),sum(nov),sum(dec)," + string(i_bv_id) + ",allocation_id,split_labor_row,gross_up_id from (select "
		
		for g = 1 to num_rows
			sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
		next
		
		for g = 1 to num_rows_addl
			sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
		next
		
		// SEK 11/02/09: Changed i_selection_screen_sqls to i_selection_screen_sqls_no_yr since 
		//		cr_budget_data_temp doesn't have year on it and the user can restrict a template
		//		to the current year...
		
//		sqls = sqls + "  to_number(substr(month_number,1,4)) year, " + &
//		" case to_number(substr(month_number,5,2)) when 1 then amount else 0 end jan, " + &
//		" case to_number(substr(month_number,5,2)) when 2 then amount else 0 end feb, " + &
//		" case to_number(substr(month_number,5,2)) when 3 then amount else 0 end mar, " + &
//		" case to_number(substr(month_number,5,2)) when 4 then amount else 0 end apr, " + &
//		" case to_number(substr(month_number,5,2)) when 5 then amount else 0 end may, " + &
//		" case to_number(substr(month_number,5,2)) when 6 then amount else 0 end jun, " + &
//		" case to_number(substr(month_number,5,2)) when 7 then amount else 0 end jul, " + &
//		" case to_number(substr(month_number,5,2)) when 8 then amount else 0 end aug, " + &
//		" case to_number(substr(month_number,5,2)) when 9 then amount else 0 end sep, " + &
//		" case to_number(substr(month_number,5,2)) when 10 then amount else 0 end oct, " + &
//		" case to_number(substr(month_number,5,2)) when 11 then amount else 0 end nov, " + &
//		" case to_number(substr(month_number,5,2)) when 12 then amount else 0 end dec, " + &
//		" allocation_id " + &
//		" from cr_budget_data_temp where budget_version = (select budget_version from cr_budget_version " + &
//			" where cr_budget_version_id = " + string(i_bv_id) + ')  allocation_id is not null and ' + i_selection_screen_sqls + &
//			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + &
//		") group by " 

		sqls = sqls + "  to_number(substr(month_number,1,4)) year, " + &
		" case to_number(substr(month_number,5,2)) when 1 then amount else 0 end jan, " + &
		" case to_number(substr(month_number,5,2)) when 2 then amount else 0 end feb, " + &
		" case to_number(substr(month_number,5,2)) when 3 then amount else 0 end mar, " + &
		" case to_number(substr(month_number,5,2)) when 4 then amount else 0 end apr, " + &
		" case to_number(substr(month_number,5,2)) when 5 then amount else 0 end may, " + &
		" case to_number(substr(month_number,5,2)) when 6 then amount else 0 end jun, " + &
		" case to_number(substr(month_number,5,2)) when 7 then amount else 0 end jul, " + &
		" case to_number(substr(month_number,5,2)) when 8 then amount else 0 end aug, " + &
		" case to_number(substr(month_number,5,2)) when 9 then amount else 0 end sep, " + &
		" case to_number(substr(month_number,5,2)) when 10 then amount else 0 end oct, " + &
		" case to_number(substr(month_number,5,2)) when 11 then amount else 0 end nov, " + &
		" case to_number(substr(month_number,5,2)) when 12 then amount else 0 end dec, " + &
		" allocation_id, split_labor_row, gross_up_id " + &
		" from cr_budget_data_temp where budget_version = (select budget_version from cr_budget_version " + &
			" where cr_budget_version_id = " + string(i_bv_id) + ') and allocation_id is not null ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + &
		") group by " 		
		
//		and ' + i_selection_screen_sqls_no_yr + &
		
		for g = 1 to num_rows
			sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
		next
		
		for g = 1 to num_rows_addl
			sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
		next
		
		sqls = sqls + "year,allocation_id,split_labor_row,gross_up_id having sum(jan) <> 0 or sum(feb) <> 0 or sum(mar) <> 0 or sum(apr) <> 0 or " + &
			"sum(may) <> 0 or sum(jun) <> 0 or sum(jul) <> 0 or sum(aug) <> 0 or sum(sep) <> 0 or " + &
			"sum(oct) <> 0 or sum(nov) <> 0 or sum(dec) <> 0 ) aa "
		
		execute immediate :sqls;
	
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs('ERROR: Returning allocation results : ' + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			f_pp_msgs(sqls)
			rollback;
			return -1
		else
			commit;
		end if
		
end if

if a_type = 'Allocations' then
	
	select budget_version into :bv 
	from cr_budget_version
	where cr_budget_version_id = :i_bv_id;
	
	select userenv('sessionid'),user 
	into :session_id,:users
	from dual;
	
	i_dd_key = 'BUDGET ENTRY ALLOCATIONS:' + users + ':' + string(session_id)
	
	month_array[1] = 'jan'
	month_array[2] = 'feb'
	month_array[3] = 'mar'
	month_array[4] = 'apr'
	month_array[5] = 'may'
	month_array[6] = 'jun'
	month_array[7] = 'jul'
	month_array[8] = 'aug'
	month_array[9] = 'sep'
	month_array[10] = 'oct'
	month_array[11] = 'nov'
	month_array[12] = 'dec'
	
	sqls = "insert into cr_budget_data_temp (id,"
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,quantity,amount,inception_to_date_amount,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id) select crbudgets.nextval,bb.* from (select '
	
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,sum(quantity),sum(amount),sum(inception_to_date_amount),month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id from (select '
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
		for g = 1 to num_rows_addl
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
		next
	
		sqls = sqls + '1 as ledger_sign,decode(sign(' + month_array[b] + '),-1,-1,1) as dr_cr_id,0 as quantity, nvl(' + month_array[b] + ',0) as amount, nvl(' + &
			 month_array[b] + ",0) as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'" + gl_jrnl_cat + "' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 " allocation_id, split_labor_row, gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + ' ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + ' and "TYPE" = ' + &
			"'Amt' union all select "
			 
//			 and ' + i_selection_screen_sqls + &
	next
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
		for g = 1 to num_rows_addl
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
		next
	
		sqls = sqls + '1 as ledger_sign,1 as dr_cr_id,nvl(' + month_array[b] + ',0) as quantity, 0 as amount, ' + &
			 "0 as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'" + gl_jrnl_cat + "' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 " allocation_id, split_labor_row, gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + ' ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'" + ' and "TYPE" = ' + &
			"'Qty' union all select "
			
//			and ' + i_selection_screen_sqls + &
			 
	next
	
	sqls = mid(sqls,1,len(sqls) - 17)
	
	sqls = sqls + ')  group by ' 
	
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' '), "
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0'), "
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id having sum(amount) <> 0 or sum(quantity) <> 0) bb'
		
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs('ERROR: Staging rows for the allocations: ' + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		return -1
	else
		commit;
	end if
	
	
end if

if a_type = 'Finish' then
	
	select budget_version into :bv 
	from cr_budget_version
	where cr_budget_version_id = :i_bv_id;
	
	select userenv('sessionid'),user 
	into :session_id,:users
	from dual;
	
	i_dd_key = 'FINISH :' + users + ':' + string(session_id)
	
	month_array[1] = 'jan'
	month_array[2] = 'feb'
	month_array[3] = 'mar'
	month_array[4] = 'apr'
	month_array[5] = 'may'
	month_array[6] = 'jun'
	month_array[7] = 'jul'
	month_array[8] = 'aug'
	month_array[9] = 'sep'
	month_array[10] = 'oct'
	month_array[11] = 'nov'
	month_array[12] = 'dec'
	
	sqls = "insert into cr_budget_data (id,"
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,quantity,amount,inception_to_date_amount,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id) select crbudgets.nextval,bb.* from (select '
	
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,sum(quantity),sum(amount),sum(inception_to_date_amount),month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id from (select '
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
		for g = 1 to num_rows_addl
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
		next
	
		sqls = sqls + '1 as ledger_sign,decode(sign(' + month_array[b] + '),-1,-1,1) as dr_cr_id,0 as quantity, nvl(' + month_array[b] + ',0) as amount, nvl(' + &
			 month_array[b] + ",0) as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'O&M' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 "allocation_id,split_labor_row,gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + '  and "TYPE" = ' + &
			"'Amt' union all select "
			
//			and ' + i_selection_screen_sqls + '
			 
	next
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
		for g = 1 to num_rows_addl
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
		next
	
		sqls = sqls + '1 as ledger_sign,1 as dr_cr_id,nvl(' + month_array[b] + ',0) as quantity, 0 as amount, ' + &
			 "0 as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'O&M' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 " allocation_id,split_labor_row,gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + '  and "TYPE" = ' + &
			"'Qty' union all select "
			 
//			 and ' + i_selection_screen_sqls + '
	next
	
	sqls = mid(sqls,1,len(sqls) - 17)
	
	sqls = sqls + ')  group by ' 
	
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' '), "
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
			",'0'), "
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id having sum(amount) <> 0 or sum(quantity) <> 0) bb'
		
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs('ERROR: Submitting : ' + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		return -1
	else
		commit;
	end if
	
	
end if

if a_type = 'Validations' then
	
	select budget_version into :bv 
	from cr_budget_version
	where cr_budget_version_id = :i_bv_id;
	
	select userenv('sessionid'),user 
	into :session_id,:users
	from dual;
	
	i_dd_key = 'BUDGET ENTRY VALIDATIONS:' + users + ':' + string(session_id)
	
	month_array[1] = 'jan'
	month_array[2] = 'feb'
	month_array[3] = 'mar'
	month_array[4] = 'apr'
	month_array[5] = 'may'
	month_array[6] = 'jun'
	month_array[7] = 'jul'
	month_array[8] = 'aug'
	month_array[9] = 'sep'
	month_array[10] = 'oct'
	month_array[11] = 'nov'
	month_array[12] = 'dec'
	
	sqls = "insert into cr_budget_data_temp (id,"
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,quantity,amount,inception_to_date_amount,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id) select crbudgets.nextval,bb.* from (select '
		
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
	next
	
//	for g = 1 to num_rows_addl
//		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
//	next

	for g = 1 to num_rows_addl
		sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '" as ' + &
				 upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1)))  + ','
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,sum(quantity),sum(amount),sum(inception_to_date_amount),month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id from (select '
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
//		for g = 1 to num_rows_addl
//			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
//		next

		for g = 1 to num_rows_addl
			sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '" as ' + &
					 upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1)))  + ','
		next
	
		sqls = sqls + '1 as ledger_sign,decode(sign(' + month_array[b] + '),-1,-1,1) as dr_cr_id,0 as quantity, nvl(' + month_array[b] + ',0) as amount, nvl(' + &
			 month_array[b] + ",0) as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'" + gl_jrnl_cat + "' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 " allocation_id, split_labor_row,gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + ' ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + ' and "TYPE" = ' + &
			"'Amt' union all select "
			
//			 and ' + i_selection_screen_sqls
			 
	next
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
//		for g = 1 to num_rows_addl
//			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
//		next

		for g = 1 to num_rows_addl
			sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '" as ' + &
					 upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1)))  + ','
		next
	
		sqls = sqls + '1 as ledger_sign,1 as dr_cr_id,nvl(' + month_array[b] + ',0) as quantity, 0 as amount, ' + &
			 "0 as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'" + gl_jrnl_cat + "' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 " allocation_id, split_labor_row,gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + ' ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + ' and "TYPE" = ' + &
			"'Qty' union all select "
			 
//			 and ' + i_selection_screen_sqls 
	next
	
	sqls = mid(sqls,1,len(sqls) - 17)
	
	sqls = sqls + ')  group by ' 
	
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' '), "
	next
	
//	for g = 1 to num_rows_addl
//		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0'), "
//	next

	for g = 1 to num_rows_addl
		sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id having sum(amount) <> 0 or sum(quantity) <> 0) bb'
		
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs('ERROR: Staging rows for the validations: ' + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		return -1
	else
		commit;
	end if
	
	
end if

if a_type = 'Derivations' then
	select budget_version into :bv 
	from cr_budget_version
	where cr_budget_version_id = :i_bv_id;
	
	select userenv('sessionid'),user 
	into :session_id,:users
	from dual;
	
	i_dd_key = 'BUDGET ENTRY DERIVATIONS:' + users + ':' + string(session_id)
	
	month_array[1] = 'jan'
	month_array[2] = 'feb'
	month_array[3] = 'mar'
	month_array[4] = 'apr'
	month_array[5] = 'may'
	month_array[6] = 'jun'
	month_array[7] = 'jul'
	month_array[8] = 'aug'
	month_array[9] = 'sep'
	month_array[10] = 'oct'
	month_array[11] = 'nov'
	month_array[12] = 'dec'
	
	sqls = "insert into cr_budget_data_temp (id,"
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	// SEK 041210: Adding allocation_id and split_labor_row... if these fields are not included, then any calculations
	//		performed prior to derivations will be duplicated on the next run of calculations since the orignal records won't be deleted.
	sqls = sqls + 'ledger_sign,dr_cr_id,quantity,amount,inception_to_date_amount,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id) select crbudgets.nextval,bb.* from (select '
		
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
	next
	
//	for g = 1 to num_rows_addl
//		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
//	next

	for g = 1 to num_rows_addl
		sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '" as ' + &
				 upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1)))  + ','
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,sum(quantity),sum(amount),sum(inception_to_date_amount),month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id from (select '
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
//		for g = 1 to num_rows_addl
//			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
//		next
		
		for g = 1 to num_rows_addl
			sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '" as ' + &
					 upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1)))  + ','
		next
	
		sqls = sqls + '1 as ledger_sign,decode(sign(' + month_array[b] + '),-1,-1,1) as dr_cr_id,0 as quantity, nvl(' + month_array[b] + ',0) as amount, nvl(' + &
			 month_array[b] + ",0) as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'" + gl_jrnl_cat + "' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 " allocation_id as allocation_id, split_labor_row as split_labor_row, gross_up_id as gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + ' ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + ' and "TYPE" = ' + &
			"'Amt' union all select "
			
//			and ' + i_selection_screen_sqls
			 
	next
	
	for b = 1 to 12
		
		for g = 1 to num_rows
			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' ') as " + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + ","
		next
		
//		for g = 1 to num_rows_addl
//			sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0') as " + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + ","
//		next

		for g = 1 to num_rows_addl
			sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '" as ' + &
					 upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1)))  + ','
		next
	
		sqls = sqls + '1 as ledger_sign,1 as dr_cr_id,nvl(' + month_array[b] + ',0) as quantity, 0 as amount, ' + &
			 "0 as inception_to_date_amount, to_number(to_char(year) || '" + f_lpad(string(b),2,'0') + "') as month_number," + &
			 " 0 as month_period, '" + bv + "' as budget_version,'" + gl_jrnl_cat + "' as gl_journal_category, 6 as source_id, 2 as amount_type,'" + i_dd_key + "' as drilldown_key, " + &
			 " allocation_id as allocation_id, split_labor_row as split_labor_row, gross_up_id as gross_up_id " + &
			' from cr_budget_data_entry where cr_budget_version_id = ' + string(i_bv_id) + ' ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + ' and "TYPE" = ' + &
			"'Qty' union all select "
			
//			and ' + i_selection_screen_sqls
			 
	next
	
	sqls = mid(sqls,1,len(sqls) - 17)
	
	sqls = sqls + ')  group by ' 
	
	for g = 1 to num_rows
		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '"' + &
			",' '), "
	next
	
//	for g = 1 to num_rows_addl
//		sqls = sqls + ' nvl("' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '"' + &
//			",'0'), "
//	next

	for g = 1 to num_rows_addl
		sqls = sqls + ' "' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + 'ledger_sign,dr_cr_id,month_number,month_period,budget_version,' + &
		'gl_journal_category,source_id,amount_type,drilldown_key,allocation_id,split_labor_row,gross_up_id having sum(amount) <> 0 or sum(quantity) <> 0) bb'
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs('ERROR: Staging rows for the derivations: ' + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		return -1
	else
		commit;		
	end if
	
	
end if

if a_type = 'Results - Derivations' then
	sqls = " delete  from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + ' ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  
			
//			and ' + i_selection_screen_sqls
	
	execute immediate :sqls;
	
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs('ERROR: Deleting the derived rows: ' + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		rollback;
		return -1
	end if

	
	sqls = "insert into cr_budget_data_entry (id,"
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	// SEK 041210: Adding split_labor_row... if these fields are not included, then any calculations
	//		performed prior to derivations will be duplicated on the next run of calculations since the orignal records won't be deleted.
	sqls = sqls + '"TYPE",year,prior_year_actuals,current_year_actuals,current_approved_budget,current_approved_forecast,' + &
		'total,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec,cr_budget_version_id,allocation_id,split_labor_row,gross_up_id) select crbudgets.nextval,aa.* from (select '
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + "'Amt', year,0 as prior_year_actuals,0 as current_year_actuals,0 as current_approved_budget,0 as current_approved_forecast" + &
		",0 as total,sum(jan),sum(feb),sum(mar),sum(apr),sum(may),sum(jun),sum(jul),sum(aug),sum(sep)," + &
		"sum(oct),sum(nov),sum(dec)," + string(i_bv_id) + ",allocation_id,split_labor_row,min(gross_up_id) from (select "
		
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	// SEK 11/02/09: Changed i_selection_screen_sqls to i_selection_screen_sqls_no_yr since 
	//		cr_budget_data_temp doesn't have year on it and the user can restrict a template
	//		to the current year...
	
//	sqls = sqls + "  to_number(substr(month_number,1,4)) year, " + &
//		" case to_number(substr(month_number,5,2)) when 1 then amount else 0 end jan, " + &
//		" case to_number(substr(month_number,5,2)) when 2 then amount else 0 end feb, " + &
//		" case to_number(substr(month_number,5,2)) when 3 then amount else 0 end mar, " + &
//		" case to_number(substr(month_number,5,2)) when 4 then amount else 0 end apr, " + &
//		" case to_number(substr(month_number,5,2)) when 5 then amount else 0 end may, " + &
//		" case to_number(substr(month_number,5,2)) when 6 then amount else 0 end jun, " + &
//		" case to_number(substr(month_number,5,2)) when 7 then amount else 0 end jul, " + &
//		" case to_number(substr(month_number,5,2)) when 8 then amount else 0 end aug, " + &
//		" case to_number(substr(month_number,5,2)) when 9 then amount else 0 end sep, " + &
//		" case to_number(substr(month_number,5,2)) when 10 then amount else 0 end oct, " + &
//		" case to_number(substr(month_number,5,2)) when 11 then amount else 0 end nov, " + &
//		" case to_number(substr(month_number,5,2)) when 12 then amount else 0 end dec, " + &
//		" allocation_id " + &
//		" from cr_budget_data_temp where budget_version = (select budget_version from cr_budget_version " + &
//			" where cr_budget_version_id = " + string(i_bv_id) + ') and ' + i_selection_screen_sqls + &
//			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + &
//		") group by " 

	sqls = sqls + "  to_number(substr(month_number,1,4)) year, " + &
		" case to_number(substr(month_number,5,2)) when 1 then amount else 0 end jan, " + &
		" case to_number(substr(month_number,5,2)) when 2 then amount else 0 end feb, " + &
		" case to_number(substr(month_number,5,2)) when 3 then amount else 0 end mar, " + &
		" case to_number(substr(month_number,5,2)) when 4 then amount else 0 end apr, " + &
		" case to_number(substr(month_number,5,2)) when 5 then amount else 0 end may, " + &
		" case to_number(substr(month_number,5,2)) when 6 then amount else 0 end jun, " + &
		" case to_number(substr(month_number,5,2)) when 7 then amount else 0 end jul, " + &
		" case to_number(substr(month_number,5,2)) when 8 then amount else 0 end aug, " + &
		" case to_number(substr(month_number,5,2)) when 9 then amount else 0 end sep, " + &
		" case to_number(substr(month_number,5,2)) when 10 then amount else 0 end oct, " + &
		" case to_number(substr(month_number,5,2)) when 11 then amount else 0 end nov, " + &
		" case to_number(substr(month_number,5,2)) when 12 then amount else 0 end dec, " + &
		" allocation_id,split_labor_row,gross_up_id " + &
		" from cr_budget_data_temp where budget_version = (select budget_version from cr_budget_version " + &
			" where cr_budget_version_id = " + string(i_bv_id) + ') ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + &
		") group by " 
			
//		and ' + i_selection_screen_sqls_no_yr	
		
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + "year,allocation_id,split_labor_row having sum(jan) <> 0 or sum(feb) <> 0 or sum(mar) <> 0 or sum(apr) <> 0 or " + &
		"sum(may) <> 0 or sum(jun) <> 0 or sum(jul) <> 0 or sum(aug) <> 0 or sum(sep) <> 0 or " + &
		"sum(oct) <> 0 or sum(nov) <> 0 or sum(dec) <> 0 " 
		
	// Need to bring the QTY records back from cr_budget_data_temp as well
	sqls = sqls + " union all select " 
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + "'Qty', year,0 as prior_year_actuals,0 as current_year_actuals,0 as current_approved_budget,0 as current_approved_forecast" + &
		",0 as total,sum(jan),sum(feb),sum(mar),sum(apr),sum(may),sum(jun),sum(jul),sum(aug),sum(sep)," + &
		"sum(oct),sum(nov),sum(dec)," + string(i_bv_id) + ",allocation_id,split_labor_row,min(gross_up_id) from (select "
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	// SEK 11/02/09: Changed i_selection_screen_sqls to i_selection_screen_sqls_no_yr since 
	//		cr_budget_data_temp doesn't have year on it and the user can restrict a template
	//		to the current year...
	
//	sqls = sqls + "  to_number(substr(month_number,1,4)) year, " + &
//		" case to_number(substr(month_number,5,2)) when 1 then quantity else 0 end jan, " + &
//		" case to_number(substr(month_number,5,2)) when 2 then quantity else 0 end feb, " + &
//		" case to_number(substr(month_number,5,2)) when 3 then quantity else 0 end mar, " + &
//		" case to_number(substr(month_number,5,2)) when 4 then quantity else 0 end apr, " + &
//		" case to_number(substr(month_number,5,2)) when 5 then quantity else 0 end may, " + &
//		" case to_number(substr(month_number,5,2)) when 6 then quantity else 0 end jun, " + &
//		" case to_number(substr(month_number,5,2)) when 7 then quantity else 0 end jul, " + &
//		" case to_number(substr(month_number,5,2)) when 8 then quantity else 0 end aug, " + &
//		" case to_number(substr(month_number,5,2)) when 9 then quantity else 0 end sep, " + &
//		" case to_number(substr(month_number,5,2)) when 10 then quantity else 0 end oct, " + &
//		" case to_number(substr(month_number,5,2)) when 11 then quantity else 0 end nov, " + &
//		" case to_number(substr(month_number,5,2)) when 12 then quantity else 0 end dec, " + &
//		" allocation_id " + &
//		" from cr_budget_data_temp where budget_version = (select budget_version from cr_budget_version " + &
//			" where cr_budget_version_id = " + string(i_bv_id) + ') and ' + i_selection_screen_sqls + &
//			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + &
//		") group by " 

	sqls = sqls + "  to_number(substr(month_number,1,4)) year, " + &
		" case to_number(substr(month_number,5,2)) when 1 then quantity else 0 end jan, " + &
		" case to_number(substr(month_number,5,2)) when 2 then quantity else 0 end feb, " + &
		" case to_number(substr(month_number,5,2)) when 3 then quantity else 0 end mar, " + &
		" case to_number(substr(month_number,5,2)) when 4 then quantity else 0 end apr, " + &
		" case to_number(substr(month_number,5,2)) when 5 then quantity else 0 end may, " + &
		" case to_number(substr(month_number,5,2)) when 6 then quantity else 0 end jun, " + &
		" case to_number(substr(month_number,5,2)) when 7 then quantity else 0 end jul, " + &
		" case to_number(substr(month_number,5,2)) when 8 then quantity else 0 end aug, " + &
		" case to_number(substr(month_number,5,2)) when 9 then quantity else 0 end sep, " + &
		" case to_number(substr(month_number,5,2)) when 10 then quantity else 0 end oct, " + &
		" case to_number(substr(month_number,5,2)) when 11 then quantity else 0 end nov, " + &
		" case to_number(substr(month_number,5,2)) when 12 then quantity else 0 end dec, " + &
		" allocation_id,split_labor_row,gross_up_id " + &
		" from cr_budget_data_temp where budget_version = (select budget_version from cr_budget_version " + &
			" where cr_budget_version_id = " + string(i_bv_id) + ') ' + &
			" and interface_batch_id = '" + string(i_ifb_id) + "'"  + &
		") group by " 
		
//		and' + i_selection_screen_sqls_no_yr
	
	for g = 1 to num_rows
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_elements.getitemstring(g,1))) + '",'
	next
	
	for g = 1 to num_rows_addl
		sqls = sqls + '"' + upper(f_cr_clean_string(ds_budget_addl_fields.getitemstring(g,1))) + '",'
	next
	
	sqls = sqls + "year,allocation_id,split_labor_row having sum(jan) <> 0 or sum(feb) <> 0 or sum(mar) <> 0 or sum(apr) <> 0 or " + &
		"sum(may) <> 0 or sum(jun) <> 0 or sum(jul) <> 0 or sum(aug) <> 0 or sum(sep) <> 0 or " + &
		"sum(oct) <> 0 or sum(nov) <> 0 or sum(dec) <> 0 " 
		
	sqls = sqls + "	) aa "
	
	execute immediate :sqls;

	if sqlca.sqlcode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs('ERROR: Returning derivation results : ' + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		f_pp_msgs(sqls)
		f_write_log(g_log_file,sqls)
		rollback;
		return -1
	else
		commit;
	end if
		
end if

return 1

return 1
end function

public function longlong uf_add_missing_amt_rows ();longlong num_arrays, g, array_counter,num_rows, gg, rtn
string element_header_array[], yr_str, letter_array[], sqls, element

uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
// SEK 062410: Changing from all elements to only the elements in the template.
sqls = "select * from cr_elements where element_id in (select element_id from cr_budget_templates_fields " + &
		 " where template_id = " + string(i_template_id) + ")"
f_create_dynamic_ds(ds_elements,'grid',sqls,sqlca,true)

num_arrays = ds_elements.rowcount()

sqls = "select upper(description) from cr_budget_additional_fields"
uo_ds_top ds_detail_full
ds_detail_full = create uo_ds_top
f_create_dynamic_ds(ds_detail_full,"grid",sqls,sqlca,true)
num_rows = ds_detail_full.rowcount()

sqls = 'insert into cr_budget_data_entry (id,prior_year_actuals,current_year_actuals,current_approved_budget,' + &
	'current_approved_forecast,	total,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,"DEC",cr_budget_version_id,' 
	

for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next
sqls = sqls + 'year,"TYPE") select crbudgets.nextval, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, ' + string(i_bv_id) + "," 

for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next

sqls = sqls + "year,'Amt' from ( select " 

for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next

//sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
//	" and " + i_dept_element_bdg +  i_selection_screen_dept_where + " and " + &
//	'"TYPE" = ' + &
//	"'Qty' minus select " 

sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
	" and " + '"TYPE" = ' + "'Qty' minus select " 

for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next

//sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
//	" and " + i_dept_element_bdg + i_selection_screen_dept_where + " and " + &
//		'"TYPE" = ' + &
//		"'Amt' )"

sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
	" and " + '"TYPE" = ' + "'Amt' )"
	
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs('ERROR: Inserting into cr_budget_data_entry: ' + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	f_pp_msgs(sqls)
	rollback;
	return -1
//else
//	commit;
//	destroy ds_detail_full
//	return 1
end if

rtn = f_cr_budget_entry_custom('ADD MISSING AMT ROWS')

if rtn < 0 then
	return -1
end if

commit;
destroy ds_detail_full
destroy ds_elements
return 1

end function

public function longlong uf_calc_totals_all ();string sqls

sqls = "update cr_budget_data_entry " + &
		 " set total = nvl(jan,0)+nvl(feb,0)+nvl(mar,0)+nvl(apr,0)+nvl(may,0)+nvl(jun,0)+" + &
		 "						 nvl(jul,0)+nvl(aug,0)+nvl(sep,0)+nvl(oct,0)+nvl(nov,0)+nvl(dec,0) " + &
		 " where cr_budget_version_id = " + string(i_bv_id)
		 
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_budget_data_entry.total: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;
end if

return 1
end function

public function string uf_calculations_missing_combos_sqls (longlong a_rate_override_id);string sqls, element_str, element, budgeting_element, element_column, element_table,table_name,element_str2
longlong num_rows, rate_override_id, g, counter

rate_override_id = a_rate_override_id

table_name = 'cr_budget_ent_rate_ovr_rates'

sqls = "select upper(c.budgeting_element) from cr_budget_ent_rate_ovr_element a, cr_elements c " + &
	" where a.element_id = c.element_id and a.rate_override_id = " + string(rate_override_id)
uo_ds_top ds_elements
ds_elements = create uo_ds_top
f_create_dynamic_ds(ds_elements,"grid",sqls,sqlca,true)

num_rows = ds_elements.rowcount()
	
for g = 1 to num_rows
	element = ds_elements.getitemstring(g,1)
	element = f_cr_clean_string(element)
	
	element_str = element_str + '"' + element + '",'
	element_str2 = element_str2 + 'a."' + element + '",'
next
element_str = mid(element_str,1,len(element_str) - 1)
element_str2 = mid(element_str2,1,len(element_str2) - 1)

sqls = " and (" + element_str2 + ",year) in ("
sqls = sqls + ' select ' + element_str + ',year from cr_budget_data_entry ' 

//sqls = sqls + " where " + i_selection_screen_sqls
//
//sqls = sqls + " and cr_budget_version_id = " + string(i_bv_id)  + " " 

sqls = sqls + " where cr_budget_version_id = " + string(i_bv_id)  + " " 

////	If they selected a budget component from the first screen apply that where clause criteria here.
counter = 0
select b.budgeting_element, b.element_column, b.element_table, count(*)
into :budgeting_element, :element_column, :element_table, :counter
from cr_budget_templates_fields a, cr_elements b
where a.element_id = b.element_id
and a.template_id = :i_template_id
and nvl(identifies_labor,0) = 1
group by b.budgeting_element, b.element_column, b.element_table;

if isnull(counter) then counter = 0

if counter > 0 then  // identified a column as labor ... did they choose a component?
	choose case i_component_id
		case 1	// Labor
			sqls = sqls + ' and ' + f_cr_clean_string(budgeting_element) + ' in (select ' + element_column + ' from ' + element_table + ' where '
			sqls = sqls + element_table + ".element_type = 'Budget' and "
			sqls = sqls + element_table + ".labor_flag in ('L','LC') ) "
		case 2 	// Non-Labor
			sqls = sqls + ' and ' + f_cr_clean_string(budgeting_element) + ' in (select ' + element_column + ' from ' + element_table + ' where '
			sqls = sqls + element_table + ".element_type = 'Budget' and "
			sqls = sqls + element_table + ".labor_flag in ('NL','NC') ) "
		case 3	// All
			// no extra sql is needed since all components are being retrieved
	end choose
end if

sqls = sqls + ' minus select ' + element_str + ',year from ' + table_name + ' where rate_override_id = ' + string(rate_override_id) + ")"

return sqls
end function

public function string uf_build_calculations_where_clause (longlong a_where_clause_id);string where_clause, sqls, rtn_f_create, left_paren, col_name, operator, not_upper_value1, orig_value1, value1, between_and, value2, right_paren, and_or, structure_where
longlong num_rows, i, structure_id, not_field, counter

where_clause = ' '

//  Where Clause:
uo_ds_top ds_where_clause
ds_where_clause = CREATE uo_ds_top
ds_where_clause.DataObject = "dw_temp_dynamic"
ds_where_clause.SetTransObject(sqlca)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(a_where_clause_id) + " order by row_id"

rtn_f_create = f_create_dynamic_ds(ds_where_clause, "grid", sqls, sqlca, true)

num_rows = ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		where_clause = where_clause + " and ( "
	end if
	
	left_paren  = upper(trim(ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(ds_where_clause.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(ds_where_clause.GetItemString(i, "value1"))
	orig_value1 = upper(ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{" then
			value1 = not_upper_value1
		else
			value1 = upper(trim(ds_where_clause.GetItemString(i, "value1")))
		end if
	end if
	
	between_and = upper(trim(ds_where_clause.GetItemString(i, "between_and")))
	value2      = upper(trim(ds_where_clause.GetItemString(i, "value2")))
	right_paren = upper(trim(ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(ds_where_clause.GetItemString(i, "and_or")))
	structure_id = ds_where_clause.GetItemNumber(i, "structure_id")
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0
	
	if pos(col_name, "(") = 0 then
		col_name = 'a."' + f_cr_clean_string(col_name) + '"'
	end if
		
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = &
			uf_structure_value(structure_id, col_name, value1, not_field)
		if structure_where = "ERROR" then
			messagebox('wf_build_calculations_where_clause','ERROR building structure where clause')
			return "ERROR"
		end if
		where_clause = &
			where_clause + left_paren + structure_where + right_paren + " " + and_or + " "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			where_clause = where_clause + &
									left_paren + ' ' + col_name +   &
									" " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or + " "
		else
			where_clause = where_clause + &
									left_paren + ' ' + col_name +  &
									" " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or + " "
		end if
	else
		where_clause = where_clause + &
								left_paren + ' ' + col_name + &
									" " + operator + " " + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or	+ " "
	end if
	

	bottom:
	
	if i = num_rows then
		where_clause = where_clause + " )"
	end if
	
next

return where_clause
end function

public function longlong uf_parse (string a_stringtoparse, string a_separator, ref string a_array_to_hold[]);//*****************************************************************************************
//
//  Window Function  :  wf_parse
//
//  Description      :  This function will parse string  into an array of elements denoted
//								by the separator.  The number of elements parsed is the return 
//								value of the function.  For example, if ~t were the separator, 
//								"red~twhite~tblue" would be parsed into an array containing red, 
//								white and blue.  The value 3 would be returned.
//
//  Notes            :  A MODIFIED VERSION OF:  f_parsestringintoarray from ppsystem.
//								This functions MUST INCLUDE the "separator" in the string that is
//								placed in the array (The "{").
//
//  Arguments :	a_stringtoparse :		string			      : string to parse
//						a_separator	    :		string			      : what to look for
//						a_array_to_hold : 	string array(by ref) : gets filled with values
//
//   
//  Returns   :  longlong :  The number of elements returned into the array.
//								  If the separator is not found then the original string is 
//                        placed in array_to_hold and 1 is returned.
//
//******************************************************************************************
longlong separator_pos, separator_len, count, start_pos
string holder

start_pos     = 1
separator_len = Len(a_separator)
separator_pos = Pos(a_stringtoparse, a_separator, start_pos)
IF Trim(a_stringtoparse) = "" or IsNull(a_stringtoparse) THEN 
	RETURN 0
ELSEIF separator_pos = 0 THEN 
	a_array_to_hold[1] = trim(a_stringtoparse)
	RETURN 1
END IF
DO WHILE separator_pos > 0
	if count = 0 then  //  Modified
		count++         //  Modified
		goto bottom		 //  Modified
	end if				 //  Modified
	holder = Mid(a_stringtoparse, start_pos - 1, (separator_pos - start_pos) + 1)  //  Modified
	a_array_to_hold[count] = trim(holder)
	count++  //  Moved below the previous line
	bottom:  //  Modified
	start_pos = separator_pos + separator_len
	separator_pos =  Pos(a_stringtoparse, a_separator, start_pos)	
LOOP
holder = Mid(a_stringtoparse, start_pos - 1, Len(a_stringtoparse))
//  NOT IN THIS WINDOW FUNCTION ... //count = count + 1
a_array_to_hold[count] = trim(holder)

RETURN count


end function

public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field);//******************************************************************************************
//
//  Window Function  :  wf_structure_value
//
//  Description      :  Builds the piece of the where clause for hierarchical queries
//								that use the cr_structures.
//
//******************************************************************************************
longlong comma_pos, start_pos, counter, the_last_comma_pos, string_length, i, status
string rollup_value_id, where_clause, description_edit_values[], rollup_value_id_list


where_clause         = " "
rollup_value_id_list = " ("


//-------  Parse out the rollup values into an array, if the user selected more  -----------
//-------  than 1 rollup value.                                                  -----------
uf_parse(a_description_edit, "{", description_edit_values)


for i = 1 to upperbound(description_edit_values)
	
	//-------  First, must parse out the value_id from the chosen element_value  ------------
	start_pos = 1
	counter   = 0

	if i_use_new_structures_table = "YES" then
		//  These need to be harcoded to 0 in case the element_value contains a comma.
		comma_pos = 0
		the_last_comma_pos = 0
	else
		do while true
		
			counter++
		
			//  Endless loop protection ...
			if counter > 1000 then
				messagebox("!!!", "Endless loop in wf_structure_value.", Exclamation!)
				exit
			end if
		
			comma_pos = pos(description_edit_values[i], ",", start_pos)
		
			if comma_pos = 0 then exit
		
			the_last_comma_pos = comma_pos
			start_pos          = comma_pos + 1
		
		loop
	end if

	string_length = len(description_edit_values[i])
	
	if i_use_new_structures_table = "YES" then
		//  "{CWIP}" goes to "CWIP}"
		rollup_value_id = right(description_edit_values[i], (string_length - the_last_comma_pos) - 1)
	else
		//  "{CWIP, 15}" goes to " 15}"
		rollup_value_id = right(description_edit_values[i], string_length - the_last_comma_pos)
	end if

	rollup_value_id = trim(left(rollup_value_id, len(rollup_value_id) - 1))
	
	status = 0
	if i_use_new_structures_table = "YES" then
		select status into :status from cr_structure_values2
		 where structure_id = :a_structure_id and element_value = :rollup_value_id;
	else
		select status into :status from cr_structure_values
		 where structure_id = :a_structure_id and value_id = :rollup_value_id;
	end if
		
	if isnull(status) or status = 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: the value_id (" + rollup_value_id + ") is INACTIVE on " + &
			" structure_id (" + string(a_structure_id) + ")")
		f_pp_msgs(" ")
		return "ERROR"
	end if
	
	if i_use_new_structures_table = "YES" then
		rollup_value_id = "'" + rollup_value_id + "'"
	end if

	if i = upperbound(description_edit_values) then
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ") "
	else
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ","
	end if


	//-------  Build the piece of the where clause to be passed back to the  ---------------- 
	//-------  calling script                                                ----------------
	if i = upperbound(description_edit_values) then
		
		if a_not_field = 0 then
			//  The user clicked the "NOT" checkbox.
			where_clause = where_clause + " " + a_description + " not in ("
		else
			where_clause = where_clause + " " + a_description + " in ("
		end if
		
		if i = 1 then
			if i_use_new_structures_table = "YES" then
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value = " + rollup_value_id + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id = " + rollup_value_id + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		else
			if i_use_new_structures_table = "YES" then
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value in " + rollup_value_id_list + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
			else
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + a_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + a_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id in " + rollup_value_id_list + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
			end if
		end if
	end if

next

return where_clause
end function

public function longlong uf_add_missing_qty_rows ();longlong num_arrays, g, array_counter,num_rows, gg, rtn
string element_header_array[], yr_str, letter_array[], sqls, element


uo_ds_top ds_elements
//ds_elements = w_cr_budget_entry_alt.i_ds_elements
ds_elements = CREATE uo_ds_top
// SEK 062410: Changing from all elements to only the elements in the template.
sqls = "select * from cr_elements where element_id in (select element_id from cr_budget_templates_fields " + &
		 " where template_id = " + string(i_template_id) + ")"
f_create_dynamic_ds(ds_elements,'grid',sqls,sqlca,true)

num_arrays = ds_elements.rowcount()

sqls = "select upper(description) from cr_budget_additional_fields"
uo_ds_top ds_detail_full
ds_detail_full = create uo_ds_top
f_create_dynamic_ds(ds_detail_full,"grid",sqls,sqlca,true)
num_rows = ds_detail_full.rowcount()

sqls = 'insert into cr_budget_data_entry (id,prior_year_actuals,current_year_actuals,current_approved_budget,' + &
	'current_approved_forecast,	total,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,"DEC",cr_budget_version_id,' 
	
for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next
sqls = sqls + 'year,"TYPE") select crbudgets.nextval, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, ' + string(i_bv_id) + "," 

for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next

sqls = sqls + "year,'Qty' from ( select " 

for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next

//sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
//	" and " + i_dept_element_bdg +  i_selection_screen_dept_where + " and " + &
//	'"TYPE" = ' + &
//	"'Amt' minus select " 

sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
	" and " + '"TYPE" = ' + "'Amt' minus select "

for g = 1 to num_arrays
	sqls = sqls + 	'"' + upper(f_cr_clean_string(ds_elements.getitemstring(g,"budgeting_element"))) + '",'
next

for g = 1 to num_rows
	element = ds_detail_full.getitemstring(g,1)
	element = '"' + f_cr_clean_string(element) + '"'
	sqls = sqls + element + ','
next

//sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
//	" and " + i_dept_element_bdg + i_selection_screen_dept_where + " and " + &
//		'"TYPE" = ' + &
//		"'Qty' )"

sqls = sqls + "year from cr_budget_data_entry where cr_budget_version_id = " + string(i_bv_id) + &
	" and " + '"TYPE" = ' + "'Qty' )"

execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs('ERROR: Inserting into cr_budget_data_entry: ' + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	f_pp_msgs(sqls)
	rollback;
	return -1
//else
//	commit;
//	destroy ds_detail_full
//	return 1
end if

rtn = f_cr_budget_entry_custom('ADD MISSING QTY ROWS')

if rtn < 0 then
	return -1
end if

commit;
destroy ds_detail_full
return 1

return 1
end function

public function longlong uf_finish ();longlong selected_tab, selected_row, counter, num_rows, num_cols, c, r, rtn, override, &
	year, start_year, end_year, year_cnt, col_count, element_id, cr_approval_status_id, bbb
string  word, default_cv, users, sqls, rowval, colval, budget_version, msg_element, bv_descr, dept_where
boolean oob, null_out_element_group
decimal {2} summary_amount, detail_amount

i_dw_entry_created = false
i_approved = false

selected_tab = 2

setpointer(hourglass!)

choose case selected_tab
		
	case 1  //  The user is on the "Budget Version" tab.

	case 2
				
		////	At this point, the records are committed in cr_budget_data_entry.  Call the derivations.
		i_called_from_finish = true
		rtn = uf_derivations()
		i_called_from_finish = false
		if rtn < 0 then return -1
		
		////	Apply the default value from cr_elements for anything still with a blank space
		rtn = uf_apply_default_values()
		if rtn < 0 then return -1
				
		////	Validate. 
		rtn = uf_validations()
		if rtn = -1 then return -1

		if rtn = -2 then
			if i_called_from_send_for_appr then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: Validation kickouts exist.  The budget data will not be sent for approval until all validation kickouts are resolved")
				f_pp_msgs("  ")
			else
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: Validation kickouts exist.  The budget data will not post until all validation kickouts are resolved")
				f_pp_msgs("  ")
			end if
			return 2 // we can continue on to the next budget version in the loop
		end if

		select budget_version into :bv_descr from cr_budget_version where cr_budget_version_id = :i_bv_id;
		
		////	Delete any previously submitted data
		sqls = " delete from cr_budget_data where budget_version = '" + bv_descr + "'"
		////	Use i_selection_screen_dept_sqls and not i_selection_screen_sqls.  There could be allocation results (e.g. diff cost element)
		////		from the labor component chosen although probably unlikely.  Just deleting for the version and dept (or set of depts) 
		////		should be sufficient.  -- NO ... FOR NOW WE ARE GOING TO FORCE THE LABOR COMPONENT.  
////		sqls = sqls + ' and ' + i_selection_screen_sqls
		
		// SEK 11/04/09: Changed to allow current year restriction
//		sqls = sqls + ' and ' + i_selection_screen_sqls_no_yr
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: deleting previous results from cr_budget_data:~n~n" + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs(sqls)
			rollback;
			return -1
		else
			commit;
		end if
		
		
		// SEK 062810: Southern isn't using this function so skip this call for now...
//		//	i_year is third variable.  It isn't correct when using the alternate entry method
//		//	i_dept_value also doesn't mean anything anymore
//		rtn = f_cr_budget_step3(i_dept_value, i_bv_id, 0)
		if rtn < 0 then return -1
		rtn = uf_build_cr_budget_data('Finish')
		
		if rtn < 1 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: FINISH was unsuccessful!")
			f_pp_msgs("  ")
			return -1
		else
			f_pp_msgs("FINISH was successful!")
		end if
		

		// SEK 062810: Skip this for now...

//		////		This comment applies to the ATC install.  We have evolved to cr_budget_data_entry
//		////			which is a staging environment.  cr_budget_data is still where finished versions go.
//		
//		//	For the direct entry method, they work directly against cr_budget_data.  With the 
//		//	wizard, you can "Finish" unlimted times which isn't much different.  However, with 
//		//	the wizard you can at least tell if a department has ever been submitted by
//		//	looking at cr_budget_data and looking to see if the department is there.  With direct
//		//	entry, there isn't a way of telling if they have touched the department since the 
//		//	initial submit.  In hindsight, a cr_budget_data_stg could have been used.  ATC likes
//		//	this last_submitted concept better.  The reason is when they update a forecast with 
//		// actuals they don't have an easy way of checking the actuals across all cost centers.
//		//	Since cr_budget_data is populated immediately with this method they can check the actuals
//		//	update for all cost centers at once.  They also like knowing the last time a department
//		//	was submitted.  Even with the wizard, you only know it was done ... but not when.
//		
//		select element_id into :element_id
//		from cr_budget_templates_budget_by
//		where template_id = :i_template_id;
//		
//		if isnull(i_template_id) then i_template_id = 0
//
////		sqls = "delete from cr_budget_submitted where cr_budget_version_id = " + string(i_bv_id) + &
////			" and element_id = " + string(element_id) + " and element_value = '" + i_dept_value + "'"
//		sqls = "delete from cr_budget_submitted where cr_budget_version_id = " + string(i_bv_id) + &
//			" and element_id = " + string(element_id) 
////			+ " and element_value "
//		
////		dept_where = " in (" + &
////					" select element_value from " + &
////					" 	(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
////					" 		from cr_structure_values2_bdg_temp a" + &
////					" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
////					" 		start with structure_id =  " + string(i_structure_id) + &
////					" 		and element_value in (" + i_rollup_value_id_list + ") " + &
////					" 		connect by prior element_value = parent_value " + &
////					" 		and structure_id =  " + string(i_structure_id) + ")) "
////					
////			sqls = sqls + dept_where
//			
//		execute immediate :sqls;
//		
//		if sqlca.SQLCode < 0 then
//			messagebox("Finish", "ERROR: deleting from cr_budget_submitted:~n~n" + & 
//				sqlca.SQLErrText, Exclamation!)
//			rollback;
//			return
//		end if
//		
//		
////		sqls = "insert into cr_budget_submitted (element_id,cr_budget_version_id,last_submit_date,element_value,submitting_user) values (" + &
////			string(element_id) + "," + string(i_bv_id) + ",sysdate,'" + i_dept_value + "','" + &
////			s_user_info.user_id + "')"
//
//		sqls = "insert into cr_budget_submitted (element_id,cr_budget_version_id,last_submit_date,submitting_user,element_value) select " + &
//			string(element_id) + "," + string(i_bv_id) + ",sysdate,'" + s_user_info.user_id + "', element_value from "
//		sqls = sqls + "(" + &
//					" select element_value from " + &
//					" 	(select distinct substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as element_value" + &
//					" 		from cr_structure_values2_bdg_temp a" + &
//					" 		where structure_id = " + string(i_structure_id) + " and status = 1  and detail_budget = 1" + &
//					" 		start with structure_id =  " + string(i_structure_id) + &
//					" 		and element_value in (" + i_rollup_value_id_list + ") " + &
//					" 		connect by prior element_value = parent_value " + &
//					" 		and structure_id =  " + string(i_structure_id) + ")) "
//
//		execute immediate :sqls;
//		
//		if sqlca.SQLCode < 0 then
//			messagebox("Finish", "ERROR: inserting into cr_budget_submitted:~n~n" + & 
//				sqlca.SQLErrText, Exclamation!)
//			rollback;
//			return
//		end if
//		
		commit;
//		
//		if rtn = 1 then
//			SetMicroHelp("Ready")
//			messagebox("Finish", &
//				"Records were successfully updated.")
//		else
//			SetMicroHelp("Ready")
//			messagebox("Finish", &
//				"Updating CR BUDGET DATA was unsuccessful.")
//		end if
		
end choose

f_cr_budget_entry_custom('CB_FINISH')


return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_budget_entry_calc_all_custom_version nvo_cr_budget_entry_calc_all_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_budget_entry_calc_all_custom.pbd'
		return nvo_cr_budget_entry_calc_all_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

