HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Stub for the junky ppsystem_interface.pbl
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'cr_batch_derivation_bdg.exe'

string i_bv, i_dt, i_co_arg
longlong i_start_mn, i_end_mn, i_basis_mn2, i_posting_mn2
string i_basis_mn, i_posting_mn

longlong 			i_bd_queue_count, i_source_id, i_max_id_processed, i_sort_order, i_i_records
string 		i_type, i_co, i_company_field, i_detail_table, i_stg_table, i_wo_field, i_proj_field, &
				i_mn_string, i_ce_field, i_ce_me_table, i_ce_me_table_field
uo_ds_top	i_ds_bd_queue, i_ds_counter, i_ds_ifb_id, i_ds_max_id, i_ds_interface_dates, i_ds_distinct_source_id, &
				i_ds_sum_amount, i_cr_deriver_type_sources, i_ds_null_rollup, &
				i_ds_all_tab_columns_bdg

uo_cr_derivation i_uo_cr_derivation

string i_type_array[], i_co_array[]
longlong i_posted_source_ids[]

longlong	i_ce_me_by_company
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_txn ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();string cmd_line_arg[], s_date, to_user
longlong cmd_line_arg_elements, start_year, end_year, running_session_id, runnini_start_mn, runnini_end_mn
longlong session_id, batch_id, do_counter, rtn
boolean committed, overall_kickouts = false

longlong running_start_mn, running_end_mn

//*****************************************************************************************
//
//  A single arg which is the budget version.
//
//  DMJ:  THE ARG CAPTURE NEEDS TO BE ABOVE THE RUNNING_SESSION_ID CHECK SINCE WE ALLOW
//  CONCURRENT PROCESSING WITHIN DIFFERENT BATCH_DERIVATION_TYPE VALUES.
//
//*****************************************************************************************
if isnull(g_command_line_args) or g_command_line_args = "" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The command line argument with the budget version is NULL.  Cannot run the interface!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if

cmd_line_arg_elements = &
	f_parsestringintostringarray(g_command_line_args, ",", cmd_line_arg)

if cmd_line_arg_elements <> 2 and cmd_line_arg_elements <> 4 and cmd_line_arg_elements <> 5 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The command line argument contained " + &
		string(cmd_line_arg_elements) + " argument(s).  " + &
		"It must have 2, 4, or 5 arguments.  " + &
		"Cannot run the interface")
	f_pp_msgs(" ")
	rollback;
	return -1
end if

i_bv = trim(cmd_line_arg[1])
i_dt = trim(cmd_line_arg[2])

i_co_arg = "NO ARG"

if cmd_line_arg_elements = 4 or cmd_line_arg_elements = 5 then
	i_start_mn = long(trim(cmd_line_arg[3]))
	i_end_mn = long(trim(cmd_line_arg[4]))
	if cmd_line_arg_elements = 5 then
		i_co_arg = trim(cmd_line_arg[5])
		f_pp_msgs("i_co_arg = " + i_co_arg)
	end if
else
	// If the start and end month aren't passed in the command line argument,
	// look up the start and end year for the budget version to use as the start
	// and end months.
	select start_year, end_year into :start_year, :end_year
	  from cr_budget_version where budget_version = :i_bv;
	  
	if isnull(start_year) or start_year = 0 or isnull(end_year) or end_year = 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Cannot find the start and end year for budget version '" + i_bv + "'")
		f_pp_msgs(" ")
		rollback;
		return -1
	end if
	
	i_start_mn = long(string(start_year) + "01")
	i_end_mn   = long(string(end_year)   + "12")
	
	f_pp_msgs("Default Months Used: " + string(i_start_mn) + " to " + string(i_end_mn))
	f_pp_msgs("  ")
	
end if


//*****************************************************************************************
//
//  To prevent this process from running multiple times concurrently.
//
//*****************************************************************************************
running_session_id = 0
select running_session_id, start_month, end_month 
  into :running_session_id, :running_start_mn, :running_end_mn
  from cr_batch_derivation_dates_bdg
 where "TYPE" = :i_dt and budget_version = :i_bv
 	and (:i_start_mn between start_month and end_month
	 	  or
		  :i_end_mn between start_month and end_month);
		  
if isnull(running_start_mn) then running_start_mn = 0
if isnull(running_end_mn) then running_end_mn = 0
 
if isnull(running_session_id) or running_session_id = 0 then
	//  NOT CURRENTLY RUNNING:  update the record with this session_id.
	select userenv('sessionid') into :session_id from dual;

	batch_id = 0
	select max(batch_id) into :batch_id
	  from cr_batch_derivation_dates_bdg
	 where "TYPE" = :i_dt and budget_version = :i_bv;
	 
	if isnull(batch_id) then batch_id = 0
	
	batch_id++
	
	committed = false
	
	insert into cr_batch_derivation_dates_bdg (
		"TYPE", source_id, budget_version, start_month, end_month, running_session_id, batch_id)
	values (:i_dt,-3,:i_bv,:i_start_mn,:i_end_mn,:session_id,:batch_id);
	
	//
	//  DMJ: 07/07/10: SINCE THIS EXE ALLOWS FOR CONCURRENT PROCESSING, AND SINCE
	//  THE SELECT ABOVE ON PP_PROCESSES_OCCURRENCES USES A MAX(ID) TECHNIQUE, THERE
	//  IS THE POSSIBILITY THAT THIS INSERT WILL FAIL.  UPON FAILURE, LOOP BACK AROUND
	//  AND INCREMENT THE G_OCCURRENCE_ID VALUE.  WE WANT TO CONTINUE TO USE THIS
	//  TECHNIQUE (AS OPPOSED TO A SEQUENCE) SINCE IT IS DESIRABLE FOR THE CLIENT
	//  TO HAVE LOW, SEQUENTIAL NUMBERING.
	//
	if sqlca.SQLCode <> 0 then
		
		rollback; // THE FAILED INSERT FROM ABOVE.
		
		do_counter = 0
		do while true
						
			do_counter++
						
			batch_id++
			
			insert into cr_batch_derivation_dates_bdg (
				"TYPE", source_id, budget_version, start_month, end_month, running_session_id, batch_id)
			values (:i_dt,-3,:i_bv,:i_start_mn,:i_end_mn,:session_id,:batch_id);
			
			if sqlca.SQLCode = 0 then
				commit;
				//  EXIT THE LOOP SINCE WE HAD A SUCCESSFUL INSERT.
				committed = true
				exit
			else
				f_pp_msgs("---------------------------------------------------------------------------------------------")
				f_pp_msgs(string(now()))
				f_pp_msgs("ERROR: inserting into cr_batch_derivation_dates_bdg: " + sqlca.SQLErrText)
				f_pp_msgs("batch_id = " + string(batch_id))
				f_pp_msgs("incrementing batch_id by 1 and retrying ...")
				f_pp_msgs("---------------------------------------------------------------------------------------------")
				//  ENDLESS LOOP INSURANCE: A LARGE NUMBER OF OCCURRENCES OF THIS
				//  AT THE SAME TIME WOULD BE UNLIKELY.  EXIT EVENTUALLY AND LOG
				//  A MESSAGE.  NOTE: THIS IS IN THIS SPOT, SO ON THE "FINAL" FAILED
				//  INSERT, WE WILL JUMP OUT TO THE PRE-EXISTING CODE HANDLING
				//  THE ROLLBACK AND TERMINATION OF THE PROCESS.
				if do_counter > 250 then
					f_pp_msgs("  ")
					f_pp_msgs(string(now()))
					f_pp_msgs("ERROR: cr_batch_derivation_dates_bdg: do_counter > 250 ... possible endless loop ...")
					f_pp_msgs("  ")
					exit // NO ROLLBACK BEFORE THIS SINCE COMMITTED = FALSE AND THE IF BLOCK BELOW WILL NEED TO RUN!
				end if
				
				rollback;
				//  LOOP BACK AROUND AND INSERT AGAIN.
			end if
			
		loop
	end if
	
	if not committed then
		if sqlca.SQLCode = 0 then
			commit;
		else
			rollback;
			rtn = -1
		end if
	end if  //  if not committed then ...
	
else
	
	//  CURRENTLY RUNNING: disconnect and halt.
	f_pp_msgs(' ')
	f_pp_msgs(' ')
	f_pp_msgs('Another session is currently running this process for Batch Derivation Type = ' + i_dt)
	f_pp_msgs('  Start Month: ' + string(running_start_mn) + ', End Month: ' + string(running_end_mn))
	f_pp_msgs('  session_id = ' + string(running_session_id))
	f_pp_msgs(' ')
	f_pp_msgs(' ')

	return -1
	
end if


//*****************************************************************************************
//
//  Object      :  uo_cr_batch_derivation_bdg
//  UO Function :  uf_read
//
//	 Returns     :  1 if successful
// 					-1 otherwise
//
//*****************************************************************************************
longlong s, i, lvalidate_rtn, validation_counter, c, m, num_months
longlong max_id
string sqls, detail_table, cv_validations, null_string_array[], co_val, &
		 co_ifb_array[], rc, sq, ds_error, budget_col, col_list
boolean this_source_has_results, validation_kickouts, re_processing_a_prior_batch
date ddate
time ttime
datetime finished_at



//*****************************************************************************************
//
//  Get the interface_id from cr_interfaces ...
//    DO NOT perform an upper on the select value ... 
//      This must be before the check for invalid ids !
//
//*****************************************************************************************
f_pp_msgs("Retrieve interface_id for CR BATCH DERIVATION at " + string(now()))

g_interface_id = 0
select interface_id into :g_interface_id from cr_interfaces
 where lower(description) = 'cr batch derivation bdg';
if isnull(g_interface_id) then g_interface_id = 0
 
if g_interface_id = 0 then
	f_write_log(g_log_file, "ERROR: No (CR Batch Derivation BDG) entry exists in cr_interfaces ... Cannot run the CR Batch Derivation BDG process!")
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: No (CR Batch Derivation BDG) entry exists in cr_interfaces ... Cannot run the CR Batch Derivation BDG process!")
	f_pp_msgs("  ")
	g_do_not_write_batch_id = true
	rtn = -1
	goto halt_the_app
end if


//*****************************************************************************************
//
//  ALL CONNECTED.  CHECK THE DEBUG VARIABLE.
//
//*****************************************************************************************
setnull(g_debug)
select upper(trim(control_value)) into :g_debug from cr_system_control
 where upper(trim(control_name)) = 'CR BATCH DERIVATION BDG - DEBUG';
if isnull(g_debug) or g_debug = "" then g_debug = "NO"


//*****************************************************************************************
//
//  Setup:
//
//*****************************************************************************************
f_pp_msgs("Performing setup at " + string(now()))

//  Get the company field.
i_company_field = ""
select upper(trim(control_value)) into :i_company_field from cr_system_control
 where upper(trim(control_name)) = 'COMPANY FIELD';
i_company_field = f_cr_clean_string(i_company_field)
if isnull(i_company_field) or i_company_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COMPANY FIELD in cr_system_control!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

select upper(trim(budgeting_element)) into :i_company_field from cr_elements
 where upper(replace(replace(replace(description,' ','_'), '/','_'), '-','_')) = :i_company_field;
i_company_field = f_cr_clean_string(i_company_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_company_field = " + i_company_field)
if isnull(i_company_field) or i_company_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COMPANY FIELD in the budgeting_element!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

//  Get the work_order field.
//  NOTE FOR BDG:  I'm coding this for cases where the work order field contains rollup
//  values in the budget tables (i.e. the same ACK field is used for both).  That was the
//  original design of the CR, so I'll adhere to that here.  If there are any dual-field
//  clients where this causes grief, I'll deal with it at a later date.
i_wo_field = ""
select upper(trim(control_value)) into :i_wo_field from cr_system_control
 where upper(trim(control_name)) = 'WORK ORDER FIELD';
i_wo_field = f_cr_clean_string(i_wo_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_wo_field = " + i_wo_field)
if isnull(i_wo_field) or i_wo_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the WORK ORDER FIELD in cr_system_control!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

select upper(trim(budgeting_element)) into :i_wo_field from cr_elements
 where upper(replace(replace(replace(description,' ','_'), '/','_'), '-','_')) = :i_wo_field;
i_wo_field = f_cr_clean_string(i_wo_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_wo_field = " + i_wo_field)
if isnull(i_wo_field) or i_wo_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the WORK ORDER FIELD in the budgeting_element!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

//  Get the funding_project field.
i_proj_field = ""
select upper(trim(control_value)) into :i_proj_field from cr_system_control
 where upper(trim(control_name)) = 'FUNDING PROJECT FIELD';
i_proj_field = f_cr_clean_string(i_proj_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_proj_field = " + i_proj_field)
if isnull(i_proj_field) or i_proj_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the FUNDING PROJECT FIELD in cr_system_control!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

select upper(trim(budgeting_element)) into :i_proj_field from cr_elements
 where upper(replace(replace(replace(description,' ','_'), '/','_'), '-','_')) = :i_proj_field;
i_proj_field = f_cr_clean_string(i_proj_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_proj_field = " + i_proj_field)
if isnull(i_proj_field) or i_proj_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the FUNDING PROJECT FIELD in the budgeting_element!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if


//  Get the cost_element field.
i_ce_field = ""
select upper(trim(control_value)) into :i_ce_field from cr_system_control
 where upper(trim(control_name)) = 'COST ELEMENT FIELD';
i_ce_field = f_cr_clean_string(i_ce_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_field = " + i_ce_field)
if isnull(i_ce_field) or i_ce_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT FIELD in cr_system_control!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

// ### 7924:  JAK: 2011-06-27:  Values by company logic.
i_ce_me_table = ""
i_ce_me_table_field = ""
i_ce_me_by_company = 0
select upper(trim(element_table)), upper(trim(element_column)), values_by_company
  into :i_ce_me_table, :i_ce_me_table_field, :i_ce_me_by_company
  from cr_elements
 where upper(replace(replace(replace(description,' ','_'),'-','_'),'/','_')) = :i_ce_field;
if i_ce_me_by_company <> 1 or isnull(i_ce_me_by_company) then i_ce_me_by_company = 0
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_me_table = " + i_ce_me_table)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_me_table_field = " + i_ce_me_table_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_me_by_company = " + string(i_ce_me_by_company))

if isnull(i_ce_me_table) or i_ce_me_table = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT MASTER ELEMENT TABLE in cr_elements!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if
if isnull(i_ce_me_table_field) or i_ce_me_table_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT MASTER ELEMENT TABLE in cr_elements!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

select upper(trim(budgeting_element)) into :i_ce_field from cr_elements
 where upper(replace(replace(replace(description,' ','_'), '/','_'), '-','_')) = :i_ce_field;
i_ce_field = f_cr_clean_string(i_ce_field)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ce_field = " + i_ce_field)
if isnull(i_ce_field) or i_ce_field = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: could not find the COST ELEMENT FIELD in the budgeting_element!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if


////  Get the basis month_number ... I'm having some debate with myself about a single value for
////  this concept, or a month number range.  PROS:  1) If they miss something historically (due to
////  not passing the basis, having a wo exclusion, etc.), we would be able to catch it up ...
////  2) others?  CONS:  1) Performance could be whacked ... 2) Non-trivial, we'd have to summarize
////  away month number (or something like that) to avoid creating a zillion records in the current
////  period ... 3) I'm not wild about building in life-to-date logic in this process - I want the
////  batch derivations to essentially behave as derivations would if they were called from within
////  transaction interface - I also don't want to duplicate the fundamental logic that already
////  exists in the trueup (we have enough stuff kludged up in PP that does the same fundamental thing.
////  I don't relish the thought of adding another).
//i_basis_mn = 0
//select control_value into :i_basis_mn from cr_alloc_system_control
// where upper(trim(control_name)) = 'CR BATCH DERIVATION BASIS MN';
//if isnull(i_basis_mn) then i_basis_mn = 0
//if i_basis_mn = 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Basis Month Number not found in cr_alloc_system_control !")
//	f_pp_msgs("  ")
//	rollback;
//	rtn = -1
//	goto halt_the_app
//end if


//  For the distinct sources that we will process.  Just a "union" since
//  we want the distinct list.
f_pp_msgs("Initializing i_ds_distinct_source_id at " + string(now()))
i_ds_distinct_source_id = CREATE uo_ds_top
sqls = &
	"select distinct source_id from cr_batch_derivation_control where source_id = -3 " + &
	"union " + &
	"select distinct source_id from cr_batch_derivation_queue where source_id = -3"
f_create_dynamic_ds(i_ds_distinct_source_id, "grid", sqls, sqlca, true)

//  For counting in uf_txn().
f_pp_msgs("Initializing i_ds_counter at " + string(now()))
i_ds_counter = CREATE uo_ds_top
sqls = "select count(*) from cr_budget_data where 1 = 0 and budget_version = 'asdf'"
//sqls = "select count(*) from cr_budget_data where rownum = 1"
f_create_dynamic_ds(i_ds_counter, "grid", sqls, sqlca, true)

//  For determining the ifb_id when there are validation kickouts.
f_pp_msgs("Initializing i_ds_ifb_id at " + string(now()))
i_ds_ifb_id = CREATE uo_ds_top
sqls = "select min(interface_batch_id) from cr_budget_data where 1 = 0 and budget_version = 'asdf'"
//sqls = "select min(interface_batch_id) from cr_budget_data where rownum = 1"
f_create_dynamic_ds(i_ds_ifb_id, "grid", sqls, sqlca, true)

//  For determining the max(id) in the stg table during derivations.
f_pp_msgs("Initializing i_ds_max_id at " + string(now()))
i_ds_max_id = CREATE uo_ds_top
sqls = "select max(id) from cr_budget_data where 1 = 0 and budget_version = 'asdf'"
//sqls = "select max(id) from cr_budget_data where rownum = 1"
f_create_dynamic_ds(i_ds_max_id, "grid", sqls, sqlca, true)

//  For balancing the results to the original transactions.
f_pp_msgs("Initializing i_ds_sum_amount at " + string(now()))
i_ds_sum_amount = CREATE uo_ds_top
sqls = "select sum(amount) from cr_budget_data where 1 = 0 and budget_version = 'asdf'"
//sqls = "select sum(amount) from cr_budget_data where rownum = 1"
f_create_dynamic_ds(i_ds_sum_amount, "grid", sqls, sqlca, true)

//  For the update derivations.
f_pp_msgs("Initializing i_cr_deriver_type_sources at " + string(now()))
i_cr_deriver_type_sources = CREATE uo_ds_top
//sqls = "select * from cr_deriver_type_sources where source_id = 2122789"
sqls = "select * from cr_deriver_type_sources where source_id = -3"
f_create_dynamic_ds(i_cr_deriver_type_sources, "grid", sqls, sqlca, true)

f_pp_msgs("Initializing i_ds_interface_dates at " + string(now()))
i_ds_interface_dates = CREATE uo_ds_top
sqls = &
	"select month_number, month_period, sum(amount), " + &
			 "count(*), sum(decode(sign(amount),1,amount,0)), " + &
			 "sum(decode(sign(amount),-1,amount,0)) " + &
	  "from cr_budget_data " + &
	 "where id = -678 and budget_version = 'asdf' " + &
	 "group by month_number, month_period"
//sqls = &
//	"select month_number, month_period, sum(amount), " + &
//			 "count(*), sum(decode(sign(amount),1,amount,0)), " + &
//			 "sum(decode(sign(amount),-1,amount,0)) " + &
//	  "from cr_budget_data " + &
//	 "where rownum = 1 " + &
//	 "group by month_number, month_period"
f_create_dynamic_ds(i_ds_interface_dates, "grid", sqls, sqlca, true)

//  For checking for NULL values in the cr_derivation_rollup field.
f_pp_msgs("Initializing i_ds_null_rollup at " + string(now()))
i_ds_null_rollup = CREATE uo_ds_top
sqls = 'select distinct "' + i_ce_field + '" from cr_budget_data where 1 = 2' + " and budget_version = 'asdf'"
//sqls = 'select distinct "' + i_ce_field + '" from cr_budget_data where rownum = 1'
f_create_dynamic_ds(i_ds_null_rollup, "grid", sqls, sqlca, true)

f_pp_msgs("Initializing ds_co at " + string(now()))
uo_ds_top ds_co
ds_co = CREATE uo_ds_top
sqls = "select distinct " + i_company_field + " from cr_budget_data where 1 = 2 and budget_version = 'asdf'"
//sqls = "select distinct " + i_company_field + " from cr_budget_data where rownum = 1"
f_pp_msgs("-- sqls = " + sqls)
f_create_dynamic_ds(ds_co, "grid", sqls, sqlca, true)

i_uo_cr_derivation = CREATE uo_cr_derivation

uo_cr_validation_bdg l_uo_cr_validation
l_uo_cr_validation = CREATE uo_cr_validation_bdg



//  NOTE FOR BDG:  Analyze 1 time here instead of for every month in uf_txn()
//  DMJ: 11/06/09: BIG TIME WASTER !!!
//f_pp_msgs("Performing update derivations: Analyzing table cr_budget_data at: " + string(now()))
//sqlca.analyze_table('cr_budget_data')


// SEK 12/14/09: Added cmd line args for the start and end month numbers to be used
//  NOTE FOR BDG:  Instead of a system control for the month_number, create a loop over the
//  month_numbers in this budget version and process each one in order.
//datastore ds_months
//ds_months = CREATE datastore
////sqls = "select distinct month_number from cr_budget_data where budget_version = '" + i_bv +"'"
////sqls = "select 201001 from dual union select 201002 from dual union select 201003 from dual union select 201004 from dual union " + &
////	"select 201005 from dual union select 201006 from dual union select 201007 from dual union select 201008 from dual union " + &
////	"select 201009 from dual union select 201010 from dual union select 201011 from dual union select 201012 from dual"
////sqls = "select 201001 from dual union select 201002 from dual union select 201003 from dual union select 201004 from dual union " + &
////	"select 201005 from dual union select 201006 from dual union select 201007 from dual union select 201008 from dual union " + &
////	"select 201009 from dual union select 201010 from dual union select 201011 from dual union select 201012 from dual union " + &
////	"select 201101 from dual union select 201102 from dual union select 201103 from dual union select 201104 from dual union " + &
////	"select 201105 from dual union select 201106 from dual union select 201107 from dual union select 201108 from dual union " + &
////	"select 201109 from dual union select 201110 from dual union select 201111 from dual union select 201112 from dual union " + &
////	"select 201201 from dual union select 201202 from dual union select 201203 from dual union select 201204 from dual union " + &
////	"select 201205 from dual union select 201206 from dual union select 201207 from dual union select 201208 from dual union " + &
////	"select 201209 from dual union select 201210 from dual union select 201211 from dual union select 201212 from dual union " + &
////	"select 201301 from dual union select 201302 from dual union select 201303 from dual union select 201304 from dual union " + &
////	"select 201305 from dual union select 201306 from dual union select 201307 from dual union select 201308 from dual union " + &
////	"select 201309 from dual union select 201310 from dual union select 201311 from dual union select 201312 from dual union " + &
////	"select 201401 from dual union select 201402 from dual union select 201403 from dual union select 201404 from dual union " + &
////	"select 201405 from dual union select 201406 from dual union select 201407 from dual union select 201408 from dual union " + &
////	"select 201409 from dual union select 201410 from dual union select 201411 from dual union select 201412 from dual "
//sqls = "select start_year, end_year from cr_budget_version where budget_version = '" + i_bv + "'"
//f_create_dynamic_ds(ds_months, "grid", sqls, sqlca, true)
//ds_months.SetSort("#1 a")
//ds_months.Sort()
//num_months = ds_months.RowCount()
//if num_months < 0 then
//	f_pp_msgs("ERROR: No budget version record found for budget version : " + i_bv)
//	f_pp_msgs(sqls)
//	rtn = -1
//	goto halt_the_app
//end if


i_ds_all_tab_columns_bdg = CREATE uo_ds_top

//JDS 2012-07-17 - ###Maint 10698  -  - CR_DERVIATION_TYPE left out of this list; also corrected a typo on CR_DERIVATION_RATE
sqls = "select column_name, column_id from all_tab_columns " + &
		 " where table_name = 'CR_BUDGET_DATA' " + &
		 "	and column_name not in ( " + &
		 "	select upper(replace(replace(replace(description,' ','_'),'-','_'),'/','_')) from cr_budget_additional_fields " + &
		 "	 where upper(replace(replace(replace(description,' ','_'),'-','_'),'/','_')) not in (" + &
		 "		'CR_DERIVATION_STATUS','CR_TXN_TYPE','CR_DERIVATION_RATE','LINE_DESCRIPTION','AMOUNT','QUANTITY','INCEPTION_TO_DATE_AMOUNT', 'CR_DERIVATION_TYPE'))"

f_create_dynamic_ds(i_ds_all_tab_columns_bdg, "grid", sqls, sqlca, true)

i_ds_all_tab_columns_bdg.SetSort("column_id a")
i_ds_all_tab_columns_bdg.Sort()


//*****************************************************************************************
//
//  Analyze some tables.
//
//*****************************************************************************************
sqlca.analyze_table('cr_batch_derivation_control')
sqlca.analyze_table('cr_batch_derivation_company')
sqlca.analyze_table('cr_batch_derivation_exclusion')
sqlca.analyze_table('cr_batch_derivation_excl_wo')

/// Create i_ds_bd_queue outside of the outermost loop
i_ds_bd_queue = CREATE uo_ds_top

if i_co_arg = "NO ARG" then
	//  Normal SQL to join in all companies that are configured.
//	sqls = &
//		'select a.sort_order, a."TYPE", b.company ' + &
//		  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
//		 "where a.timing = 'Cycle' and a.source_id = -672 and a.batch_derivation_type = 'ASDF'"
	sqls = &
		'select a.sort_order, a."TYPE", b.company ' + &
		  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
		 "where a.timing = 'Cycle' and a.source_id <> -672 and a.batch_derivation_type <> 'ASDF'"
else
	//  Add i_co_arg to the SQL to run for 1 company.
//	sqls = &
//		'select a.sort_order, a."TYPE", b.company ' + &
//		  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
//		 "where a.timing = 'Cycle' and a.source_id = -672 and a.batch_derivation_type = 'ASDF' " + &
//		   "and company = 'ABC'"
	sqls = &
		'select a.sort_order, a."TYPE", b.company ' + &
		  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
		 "where a.timing = 'Cycle' and a.source_id <> -672 and a.batch_derivation_type <> 'ASDF' " + &
		   "and company <> 'ABC'"
end if

f_pp_msgs("Initializing i_ds_bd_queue at " + string(now()))

ds_error = f_create_dynamic_ds(i_ds_bd_queue, "grid", sqls, sqlca, true)

if ds_error <> 'OK' then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Creating i_ds_bd_queue: " + ds_error)
	f_pp_msgs("  ")
	rollback;
	rtn = -1
	goto halt_the_app
end if

longlong st, en
// SEK 12/14/09: Added cmd line args for the start and end month numbers to be used
//st = long(string(ds_months.GetItemNumber(1, 1)) + "01")
//en = long(string(ds_months.GetItemNumber(1, 2)) + "12")
st = i_start_mn
en = i_end_mn

//FOR M = 1 TO NUM_MONTHS
FOR M = ST to EN
	
	if (long(right(string(m), 2)) > 12 and long(right(string(m), 2)) <= 99) or long(right(string(m), 2)) = 0 then continue
	
	i_basis_mn   = string(m)
	i_posting_mn = string(m)
	
//	if m <> 201002 then continue
	
//	i_basis_mn2   = ds_months.GetItemNumber(m, 2)
//	i_posting_mn2 = ds_months.GetItemNumber(m, 2)
	
	
//	i_basis_mn   = long(string(i_basis_mn)   + "01")
//	i_posting_mn = long(string(i_posting_mn) + "01")
	
//	i_basis_mn2   = long(string(i_basis_mn2)   + "12")
//	i_posting_mn2 = long(string(i_posting_mn2) + "12")
	
	//  DMJ:  TESTING ...
//	i_basis_mn = 201001
//	i_posting_mn = 201001





//  NOT FOR BUDGET:
////*****************************************************************************************
////
////  There is a CR Alloc System Control value that determines the posting month number
////  for these transactions.  Since this process is looping over CR sources, and
////  validations will fire for each source, we will be alerted later if this MN
////  is closed for any of the company/source combinations.
////
////*****************************************************************************************
//f_pp_msgs("Retrieving month number at " + string(now()))
//
//i_posting_mn = 0
//select control_value into :i_posting_mn from cr_alloc_system_control
// where upper(trim(control_name)) = 'CR BATCH DERIVATION POSTING MN';
//if isnull(i_posting_mn) then i_posting_mn = 0
//
//if i_posting_mn = 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Month Number not found in cr_alloc_system_control !")
//	f_pp_msgs("  ")
//	rollback;
//	rtn = -1
//	goto halt_the_app
//end if


f_pp_msgs("The budget version is: " + i_bv)
f_pp_msgs("The basis month number  is: " + string(i_basis_mn))
f_pp_msgs("The basis month number2 is: " + string(i_basis_mn2))
f_pp_msgs("Transactions will post to the following month number: " + string(i_posting_mn))
f_pp_msgs("Through: " + string(i_posting_mn2))


//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:  Just in case we need to mess around really early (like
//                         fiddling with cr_deriver_control or something).
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom_bdg("way up-front", 0, " ", "too early for a table name")

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	rtn = -1
	goto halt_the_app
end if


//*****************************************************************************************
//
//  OUTERMOST LOOP:  Over the list of source_ids to process.  We know that this is the
//                   master list of sources configured for batch derivations.  Below, we
//                   will worry about "Cycle" vs. "On Demand" for each source.
//
//*****************************************************************************************
FOR s = 1 TO i_ds_distinct_source_id.RowCount()



setnull(g_batch_id)

i_source_id = i_ds_distinct_source_id.GetItemNumber(s, 1)

//  NOT FOR BUDGET:
//select lower(table_name) into :i_detail_table 
//  from cr_sources
// where source_id = :i_source_id;

i_detail_table = "cr_budget_data"

i_stg_table = i_detail_table + "_stg"
i_stg_table = left(i_stg_table, 2) + "b" + right(i_stg_table, len(i_stg_table) - 2)

i_stg_table = "crb_budget_data"

f_pp_msgs("  ")
f_pp_msgs("*************************************************************************************")
f_pp_msgs("Source Id: " + string(i_source_id))
f_pp_msgs("DTL Table: " + i_detail_table)
f_pp_msgs("Month Number: " + string(i_posting_mn) + " through " + string(i_posting_mn2))
f_pp_msgs("*************************************************************************************")
f_pp_msgs("  ")



////
////  CLEAR OUT ANY DERIVATION RECORDS THAT ARE LEFT OVER FROM PRIOR VALIDATION KICKOUTS
////  OR FROM AN OOB CONDITION.
////
////  We must also fire code similar to uf_delete_invalid_ids in case the TARGET or OFFSET
////  txns kicked in validations.  Otherwise we will strand these records in the invalid
////  ids table which would cause problems later.
////
////  FOR CRB DERIVATIONS:  SINCE THE CRB STAGING TABLES ARE ONLY USED FOR THIS PROCESS,
////  AND SINCE WE ARE ABOUT TO TRUNCATE THE TABLES, WE'LL OMIT ANY WHERE CLAUSES REFERENCING
////  IDS AND SUCH (LIKE WE HAVE IN TRANSACTION INTERFACES).
////
//f_pp_msgs("Clearing any prior kickouts (cr_validations_invalid_ids2) at " + string(now()))
//
//sqls = &
//	"delete from cr_validations_invalid_ids2 " + &
//	 "where lower(table_name) = '" + i_stg_table + "' " + &
//	   "and exists (" + &
//			"select 1 from " + i_detail_table + " " + &
//			 "where budget_version = '" + i_bv + "' and month_number = " + string(i_posting_mn) + " " + &
//			   "and cr_validations_invalid_ids2.id = cr_budget_data.id)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Clearing any prior kickouts (cr_validations_invalid_ids2): " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	rtn = -1
//	goto halt_the_app
//end if
//
//
//f_pp_msgs("Clearing any prior kickouts (cr_validations_invalid_ids) at " + string(now()))
//
//sqls = &
//	"delete from cr_validations_invalid_ids " + &
//	 "where lower(table_name) = '" + i_stg_table + "' " + &
//	   "and exists (" + &
//			"select 1 from " + i_detail_table + " " + &
//			 "where budget_version = '" + i_bv + "' and month_number = " + string(i_posting_mn) + " " + &
//			   "and cr_validations_invalid_ids.id = cr_budget_data.id)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Deleting any prior kickouts (cr_validations_invalid_ids): " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	rtn = -1
//	goto halt_the_app
//end if



//  REALLY, REALLY NOT FOR BUDGET !!!!!  6/18/09:  OK now since we are using the global
//  temp table as i_stg_table.
//  Trunc the crb staging table.
f_pp_msgs("Truncating " + i_stg_table + " at " + string(now()))
sqlca.truncate_table(i_stg_table)

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: truncating " + i_stg_table + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	rtn = -1
	goto halt_the_app
end if



////	 "If this interface ran previously but stopped b/c of validation kickouts, I don't want 
////  to get any new data until the previous run has been loaded."
////
////  Were there validation kikcouts or other problems with the prior run ?  If so, there will
////  be records left in the CR detail table that are marked as 'I'.  If that is the case,
////  re-process them.  This functions just as an interface would and makes the logic consistent
////  with transaction interface controls.
//f_pp_msgs("Checking to re-process a prior batch at " + string(now()))
//
//sqls = &
//	"select count(*) from " + i_detail_table + &
//	" where month_number = " + string(i_basis_mn) + &
//	  " and cr_derivation_status = 'I' " + &
//	  " and budget_version = '" + i_bv + "'"
//i_ds_counter.SetSQLSelect(sqls)
//i_ds_counter.RETRIEVE()
//
//if i_ds_counter.RowCount() <= 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: count(*) where cr_derivation_status = 'I' is <= 0 for: " + i_detail_table)
//	f_pp_msgs("  ")
//	rollback;
//	rtn = -1
//	goto halt_the_app
//else
//	if i_ds_counter.GetItemNumber(1, 1) > 0 then
//		//  We are reprocessing a prior batch.  Do not mark any new transactions.
//		//  with an 'I'.
//		f_pp_msgs("*****  RE-PROCESSING A PRIOR BATCH  *****")
//		f_pp_msgs("  ")
//		re_processing_a_prior_batch = true
//	else
//		//  We used to have the "update to 'I'" code here, but it would hit companies
//		//  that should not be part of this run.
//		re_processing_a_prior_batch = false
//	end if
//end if



//*****************************************************************************************
//
//  Determine the Batch Derivation Queue:
//
//  The queue is a combination of those types/sources that whose timing
//  indicator = 'Cycle', plus, and 'On Demand' combinations that have been submitted
//  since the last run.  For the 'Cycle' records, the cr_batch_derivation_company table
//  determines which companies are processed.  Note: not "union all" since we want a
//  distinct list.
//
//  The PK on cr_batch_derivation_control is simply (derivation) TYPE and SOURCE_ID.
//  Thus, if a source is defined as 'Cycle' it must cycle for all companies.  Otherwise,
//  define it as 'On Demand' and each company must submit their "run" of the batch
//  derivations individually.
//
//*****************************************************************************************
f_pp_msgs("Determining the Batch Derivation Queue at " + string(now()))

//i_ds_bd_queue = CREATE uo_ds_top
//
//
//  DMJ:  11/17/09:  QUEUE CONCEPT MADE SENSE FOR ACTUALS RUNNING EVERY DAY, BUT NOT SO MUCH
//  FOR BUDGET THAT WOULD BE RUN ON DEMAND.  ADDED A BATCH_DERIVATION_TYPE FIELD SO DIFFERENT
//  COMPANIES CAN RUN "THEIR" DERIVATIONS ON DEMAND.
//

//sqls = &
//	'select a.sort_order, a."TYPE", b.company ' + &
//	  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
//	 "where a.timing = 'Cycle' and a.source_id = " + string(i_source_id) + " and a.sort_order <> 0" + &
//	" union " + &
//	'select sort_order, "TYPE", company ' + &
//	  "from cr_batch_derivation_queue where source_id = " + string(i_source_id) + " and sort_order <> 0"

if i_co_arg = "NO ARG" then
	//  Normal SQL to join in all companies that are configured.
	sqls = &
		'select a.sort_order, a."TYPE", b.company ' + &
		  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
		 "where a.timing = 'Cycle' and a.source_id = " + string(i_source_id) + " and a.batch_derivation_type = '" + i_dt + "'"
else
	//  Add i_co_arg to the SQL to run for 1 company.
	sqls = &
		'select a.sort_order, a."TYPE", b.company ' + &
		  "from cr_batch_derivation_control a, cr_batch_derivation_company b " + &
		 "where a.timing = 'Cycle' and a.source_id = " + string(i_source_id) + " and a.batch_derivation_type = '" + i_dt + "' " + &
		   "and company = '" + i_co_arg + "'"
end if

//f_create_dynamic_ds(i_ds_bd_queue, "grid", sqls, sqlca, true)

i_ds_bd_queue.setsqlselect(sqls)
i_ds_bd_queue.retrieve()

if isnull(sqls) then sqls = ""


//if i_ds_bd_queue.i_sqlca_sqlcode < 0 then
	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Retrieving the queue!")
	f_pp_msgs("SQLS: " + sqls)
	f_pp_msgs("---SQLCODE: " + string(i_ds_bd_queue.i_sqlca_sqlcode))
	f_pp_msgs("---SQLERRTEXT: " + i_ds_bd_queue.i_sqlca_sqlerrtext)
	f_pp_msgs("---SQLSYNTAX: " + i_ds_bd_queue.i_sqlca_sqlsyntax)
	f_pp_msgs(string(i_ds_bd_queue.rowcount()) + " rows retrieved.")
	f_pp_msgs("  ")
//else
//	f_pp_msgs(string(i_ds_bd_queue.rowcount()) + " rows retrieved.")
//end if

i_ds_bd_queue.SetSort("sort_order a, type a, company a")
i_ds_bd_queue.Sort()

i_bd_queue_count = i_ds_bd_queue.RowCount()

if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_bd_queue_count = " + string(i_bd_queue_count))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_ds_bd_queue sqls = " + sqls)

if i_bd_queue_count <= 0 then
	f_pp_msgs("No records returned from the queue.  There are no batch derivations to be processed.")
	f_pp_msgs("  ")
	continue  //  On to the next source.
end if



//*****************************************************************************************
//
//  Loop over the queue and process each "TYPE", company that has been
//  submitted for processing.
//
//*****************************************************************************************
f_pp_msgs("Starting the Batch Derivation process at " + string(now()))
f_pp_msgs("  ")

if g_debug = "YES" then i_uo_cr_derivation.i_debug = "YES"

this_source_has_results = false

i_type_array = null_string_array
i_co_array   = null_string_array

for i = 1 to i_bd_queue_count
	
	i_sort_order = i_ds_bd_queue.GetItemNumber(i, "sort_order")
	i_type       = i_ds_bd_queue.GetItemString(i, "type")
	i_co         = i_ds_bd_queue.GetItemString(i, "company")
	
	i_type_array[upperbound(i_type_array) + 1] = i_type
	i_co_array[upperbound(i_co_array) + 1]     = i_co
	
	f_pp_msgs("-------------------------------------------------------------------------------------")
	f_pp_msgs("Derivation Type: " + i_type)
	f_pp_msgs(i_company_field + ": " + i_co)
	f_pp_msgs("Month Number: " + string(i_posting_mn) + " through " + string(i_posting_mn2))
	f_pp_msgs("-------------------------------------------------------------------------------------")
	
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_detail_table = " + i_detail_table)
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** i_stg_table = " + i_stg_table)
	
	//  Mark the records in the CR detail table to be processed.  This was originally in code above,
	//  but would hit companies that should not be in this run.
	//
	//  If we ARE NOT re-processing a prior batch, we need to mark the cr_derivation_status
	//  to 'I' at this point.
	if not re_processing_a_prior_batch then
		
		//  1: Apply GL Journal Category exclusions.  Mark with the "negative" of the GL Journal Category
		//  value.  That will allow us to null out those records if a client ever incorrectly enters
		//  a GL JCAT value in the exclusion table.
		f_pp_msgs("Applying GL Journal Category exclusions at " + string(now()))
		sqls = "update " + i_detail_table + " set cr_derivation_status = '-'||substr(gl_journal_category,1,34) " + &
			"where month_number = " + string(i_basis_mn) + " and (cr_derivation_status is null or cr_derivation_status = '0') " + &
			  "and " + i_company_field + " = '" + i_co + "' " + &
			  "and gl_journal_category in (select gl_journal_category from cr_batch_derivation_exclusion) " + &
			  "and budget_version = '" + i_bv + "'"
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Applying GL Journal Category exclusions: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			rtn = -1
			goto halt_the_app
		end if
		
		//  2: Apply Work Order exclusions.  Mark with the "negative" of the Work Order
		//  value.  That will allow us to null out those records if a client ever incorrectly enters
		//  a WO value in the exclusion table.
		f_pp_msgs("Applying Work Order exclusions at " + string(now()))
		sqls = "update " + i_detail_table + " set cr_derivation_status = '-'||substr(" + i_wo_field + ",1,34) " + &
			"where month_number = " + string(i_basis_mn) + " and (cr_derivation_status is null or cr_derivation_status = '0') " + &
			  "and " + i_company_field + " = '" + i_co + "' " + &
			  "and " + i_wo_field + " in (select work_order_number from cr_batch_derivation_excl_wo) " + &
			  "and budget_version = '" + i_bv + "'"
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Applying Work Order exclusions: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			rtn = -1
			goto halt_the_app
		end if
		
//
// DMJ: 11/17/09: I AM SIMPLY GOING TO APPLY THIS WHERE CLAUSE TO THE INSERT IN UF_TXN().
// IT IS NOT EFFICIENT TO DO BOTH.
//
//		//  3: Update CR Derivation Status to 'I'.
//		f_pp_msgs("Updating CR Derivation Status to 'I' at " + string(now()))
//		sqls = "update " + i_detail_table + " set cr_derivation_status = 'I' " + &
//			"where month_number = " + string(i_basis_mn) + " and cr_derivation_status is null " + &
//			  "and " + i_company_field + " = '" + i_co + "' " + &
//			  "and budget_version = '" + i_bv + "'"
//		execute immediate :sqls;
//		i_i_records = sqlca.SQLNRows
//		if sqlca.SQLCode < 0 then
//			f_pp_msgs("  ")
//			f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_status to 'I': " + sqlca.SQLErrText)
//			f_pp_msgs("  ")
//			rollback;
//			rtn = -1
//			goto halt_the_app
//		else
//			f_pp_msgs("--- " + string(sqlca.SQLNRows) + " updated")
//		end if
		
	end if
	
	//  Call the "transaction interface" logic.
	f_pp_msgs("Calling uf_txn() at " + string(now()))
	rtn = uf_txn()
	
	f_pp_msgs("  ")
	
	if rtn = 2 then continue  //  No TARGET records generated.  Nothing to do.
	
	if rtn <> 1 then
		//  Messages handled in uf_txn().
		rollback;
		rtn = -1
		goto halt_the_app
	end if
	
	this_source_has_results = true  //  If we're here, then we posted some results.
	
next  //  for i = 1 to i_bd_queue_count ... types and companies ...



//*****************************************************************************************
//
//  THE FOLLOWING DOES NOT MAKE SENSE IF THERE WERE NO RESULTS:
//
//*****************************************************************************************
f_pp_msgs("Completing the derivation logic for source_id = " + string(i_source_id))
f_pp_msgs("This source has results = " + string(this_source_has_results))

if this_source_has_results then
	//  Continue on.
else
	goto after_summarization
end if



//*****************************************************************************************
//
//	 VALIDATIONS:  Call the new validations user object.  It is assumed that combos have
//						already been created.
//
//  For this process, we will simply update the interface_batch_id to -1 since we
//  may not want to burn the nextval at this point.
//
//*****************************************************************************************
////-----------------------------------------------------------------------------------------
//// SEK 091410: Commented out for performance reasons... the interface batch id will already
////		be set to -1 in uf_txn.  Plus, the validations are commented out below so this 
////		isn't needed anyway.
////-----------------------------------------------------------------------------------------
//f_pp_msgs("Updating interface_batch_id to '" + i_bv + "' at at: " + string(now()))
//
//sqls = "update " + i_stg_table + " set interface_batch_id = '" + i_bv + "' " + &
//		 " where budget_version = '" + i_bv + "'"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	//  OOB !
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Updating " + i_stg_table + ".interface_batch_id to '" + i_bv + "': " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	rtn = -1
//	goto halt_the_app
//end if
////-----------------------------------------------------------------------------------------
//// END SEK 091410: Commented out for performance reasons... this is included in the insert in 
////		uf_txn
////-----------------------------------------------------------------------------------------


//  DMJ: 11/06/09:
//  SOMEWHAT OF A TIME WASTER.  WITH VALIDATIONS REMOVED, WE SHOULD
//  NOT NEED TO HAVE FRESH STATS.
//f_pp_msgs("Analyzing stg and other tables at: " + string(now()))
//sqlca.analyze_table(i_stg_table)

//sqlca.analyze_table('cr_validations_invalid_ids')



//f_pp_msgs("Running validations at: " + string(now()))
//
////  MOVED ABOVE DUE TO THE LOOPS.
////l_uo_cr_validation l_uo_cr_validation
////l_uo_cr_validation = CREATE l_uo_cr_validation
//
//f_pp_msgs(" -- Deleting from cr_validations_invalid_ids at: " + string(now()))
//
//lvalidate_rtn = l_uo_cr_validation.uf_delete_invalid_ids(i_stg_table, i_bv)
//
//if lvalidate_rtn <> 1 then return -1
//
//validation_kickouts = false
//
//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - COMBO';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_combo_validations
//
//
//f_pp_msgs(" -- Validating the Batch (Combos) at: " + string(now()))
//
////	 Which function is this client using?
//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'VALIDATIONS - CONTROL OR COMBOS';
//if isnull(cv_validations) then cv_validations = "CONTROL"
//
//if cv_validations = 'CONTROL' then
//	lvalidate_rtn = l_uo_cr_validation.uf_validate_control(i_stg_table, i_bv)
//else
//	lvalidate_rtn = l_uo_cr_validation.uf_validate_combos(i_stg_table, i_bv)
//end if
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate_(x).  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 then
//	//  Validation Kickouts.
//	validation_counter = 0
//	select count(*) into :validation_counter	from cr_validations_invalid_ids
//	 where lower(table_name) = :i_stg_table;
//	if validation_counter = 0 then
//	else
//		validation_kickouts = true
//	end if
//end if
//
//after_combo_validations:
//
//
//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_project_validations
//
//f_pp_msgs(" -- Validating the Batch (Projects) at: " + string(now()))
//
//lvalidate_rtn = l_uo_cr_validation.uf_validate_projects(i_stg_table, i_bv)
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 then
//	//  Validation Kickouts.
//	validation_counter = 0
//	select count(*) into :validation_counter	from cr_validations_invalid_ids
//	 where lower(table_name) = :i_stg_table;
//	if validation_counter = 0 then
//	else
//		validation_kickouts = true
//	end if
//end if
//
//after_project_validations:
//
//
//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - ME';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_me_validations
//
//f_pp_msgs(" -- Validating the Batch (Element Values) at: " + string(now()))
//
//lvalidate_rtn = l_uo_cr_validation.uf_validate_me(i_stg_table, i_bv)
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 then
//	//  Validation Kickouts.
//	validation_counter = 0
//	select count(*) into :validation_counter	from cr_validations_invalid_ids
//	 where lower(table_name) = :i_stg_table;
//	if validation_counter = 0 then
//	else
//		validation_kickouts = true
//	end if
//end if
//
//after_me_validations:
//
//
//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MONTH NUMBER';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_mn_validations
//
//f_pp_msgs(" -- Validating the Batch (Open Month Number) at: " + string(now()))
//
//lvalidate_rtn = l_uo_cr_validation.uf_validate_month_number(i_stg_table, i_bv)
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 or lvalidate_rtn = -3 then
//	//CURRENTLY NOT INSERT INTO THE INVALID IDS TABLE.
//	//  Validation Kickouts.
////	validation_counter = 0
////	select count(*) into :validation_counter	from cr_validations_invalid_ids
////	 where lower(table_name) = 'cr_gas_master_stg';
////	if validation_counter = 0 then
////	else
//		validation_kickouts = true
////	end if
//end if
//
//after_mn_validations:
//
//
//f_pp_msgs(" -- Validating the Batch (Custom Validations) at: " + string(now()))
//
//lvalidate_rtn = l_uo_cr_validation.uf_validate_custom(i_stg_table, i_bv)
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 then
//	//  Validation Kickouts.
//	validation_counter = 0
//	select count(*) into :validation_counter	from cr_validations_invalid_ids
//	 where lower(table_name) = :i_stg_table;
//	if validation_counter = 0 then
//		//  Historical accident from interfaces at another client ... not sure why I would ever
//		//  want to continue on and post the txns if there is a validation kickout!  Especially
//		//  with the custom function since it is easy to code SQL in the insert of that fxn that
//		//  may fail.
//		validation_kickouts = true
//	else
//		validation_kickouts = true
//	end if
//end if
//
//
//
/////////////////////////////////////////////  PROCESS SUSPENSE  ///////////////////////////////////////////
//
//if validation_kickouts then
//	
//	
//	// N/A FOR THIS PROCESS AT THE MOMENT ... THIS IS AN EXAMPLE OF HOW TO HOLD TRANSACTIONS IN THE STAGING
//	// TABLE IF CERTAIN CONDITIONS SHOULD NOT TRIGGER SUSPENSE ACCOUNTING.  FOR NOW, SUSPENSE
//	// WILL ALWAYS BE TRIGGERED AT SOUTHERN.
//	//	//  For PNW, hold the batch if there were charge numbers that did not exist in
//	//	//  cr_deriver_control. That condition was detected above, but we want to run the full
//	//	//  validation processing before terminating the interface.  This condition will prevent
//	//	//  suspense accounting and will hold the batch to be reviewed in the kickouts window.
//	//	if cn_invalid or invalid_mn or missing_fp then
//	//		//  Set the variable below to the "hold the batch" value and skip the suspense function.
//	//		lvalidate_rtn = 2
//	//		goto after_suspense_function
//	//	end if
//	
//	
//	//  Call the suspense accounting function.  It will return a value of 2 if 
//	//  suspense accounting is turned off in cr_system_control.
//	lvalidate_rtn = l_uo_cr_validation.uf_suspense(i_stg_table, i_bv)
//	
//	if lvalidate_rtn < 0 then
//		//  Error in uf_validate.  Logged by the function.
//		return -1
//	end if
//	
//	
//	//	after_suspense_function:  // N/A FOR THIS PROCESS AT THE MOMENT ... SEE ABOVE.
//	
//	
//	if lvalidate_rtn = 2 then
//	
//		f_pp_msgs("  ")
//		f_pp_msgs("   -----------------------------------------------------------------------" + &
//			"-------------------------------------------------------------")
//		f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE BATCH WILL NOT BE LOADED!  *****") 
//		f_pp_msgs("   -----------------------------------------------------------------------" + &
//			"-------------------------------------------------------------")
//		f_pp_msgs("  ")
//		
//		// ### 8142: JAK: 2011-11-01:  Consistent return codes
//		overall_kickouts = true
//	
//	else
//		
//		f_pp_msgs("  ")
//		f_pp_msgs("   -----------------------------------------------------------------------" + &
//			"-------------------------------------------------------------")
//		f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  " + &
//			"SUSPENSE ACCOUNTS HAVE BEEN APPLIED AND THE BATCH WILL BE POSTED!  *****")
//		f_pp_msgs("   -----------------------------------------------------------------------" + &
//			"-------------------------------------------------------------")
//		f_pp_msgs("  ")
//		
//	end if
//	
//	if lvalidate_rtn = 2 then
//		//  No suspense accounting ... hold the batch.  Goto a spot down below that will avoid
//		//  the code that posts into the detail and summary tables.
//		//goto after_posting  //  return -1 (this is what an interface has)  --- FOR THIS PROCESS IT'S NOT CLEAR
//			//  THAT WE'D EVER WANT TO NOT WRITE OUT THE TRANSACTIONS TO CR_BUDGET_DATA ... OTHERWISE THERE IS NO
//			//  WAY TO REVIEW THE KICKOUTS.  SO THIS EFFECTIVELY SIMULATES MEMO VALIDATIONS WHEN RUNNING FOR BUDGET.
//	end if
//end if
//
//
//
/////////////////////////////////////////  END OF: PROCESS SUSPENSE  ///////////////////////////////////////
//
//
//
//f_pp_msgs("Validations complete at: " + string(now()))















//
//  PLACEHOLDER FOR FUTURE INTERCO PROCESSING ... NOT NEEDED INITIALLY AT SOUTHERN
//
//f_pp_msgs("NULLing out drilldown_key on INTERCO at: " + string(now()))
//
//update cr_gas_master_stg
//	set drilldown_key = NULL
// where drilldown_key = 'INTERCO';
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("(GM) ERROR: NULLing out drilldown_key on INTERCO: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


//after_validations:


////---------------------------------------------------------------------------------------
//// SEK 091510: Changed for performance: Rather than updating for each company and then 
////		inserting, simply do one insert for each company and populate the interface 
////		batch id on the insert.
////---------------------------------------------------------------------------------------
////*****************************************************************************************
////
////	 UPDATE the interface_batch_id ... we do this here, so we can be at the latest
////  possible spot where we know we have some results (so we don't burn nextvals for
////  no good reason --- since we will be truncating the stg table and re-running in the
////  event of kickouts).
////
////*****************************************************************************************
//f_pp_msgs("Updating " + i_stg_table + ".interface_batch_id at: " + string(now()))
f_pp_msgs("Inserting into " + i_detail_table + " at: " + string(now()))

//  Get the distinct list of companies ... for ifb_id by company.
sqls = "select distinct " + i_company_field + " from " + i_stg_table
ds_co.SetSQLSelect(sqls)
ds_co.RETRIEVE()

co_ifb_array = null_string_array

// Build list of insert and select columns
col_list = ""
for c = 1 to i_ds_all_tab_columns_bdg.RowCount()
	budget_col = upper(i_ds_all_tab_columns_bdg.GetItemString(c, 1))
	// Skip Interface Batch Id so it can be listed at the end so we can provide the 
	//	new batch id for each company below
	if c = i_ds_all_tab_columns_bdg.RowCount() then
		if budget_col = 'INTERFACE_BATCH_ID' then 
			// remove the last comma that was added since interface batch id is the last column
			// on the table
			col_list = left(col_list,len(col_list)-2)
		else
			col_list = col_list + '"' + budget_col + '" '
		end if
	else
		if budget_col = 'INTERFACE_BATCH_ID' then continue
		col_list = col_list + '"' + budget_col + '", '
	end if
next

for c = 1 to ds_co.RowCount()
	
	co_val = ds_co.GetItemString(c, 1)
	
	f_pp_msgs("--" + i_company_field + " = " + co_val)
	
	setnull(g_batch_id) // added in this loop to trigger one per company
	
	if isnull(g_batch_id) then
		//  This is the first source in this run to post any transactions.  Get the
		//  nextval here.
		select costrepository.nextval into :g_batch_id from dual;
		if isnull(g_batch_id) or g_batch_id = "" then g_batch_id = '0'
		
		if g_batch_id = '0' then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Could not get the interface_batch_id from costrepository.nextval")
			f_pp_msgs("  ")
			rollback;
			rtn = -1
			goto halt_the_app
		end if
	end if
	
	co_ifb_array[c] = g_batch_id
	
	// SEK 091510 Insert instead of update:
	sqls = "insert into " + i_detail_table + "(" + col_list + ", interface_batch_id)" + &
			 "select " + col_list + ", '" + g_batch_id + "' " + & 
			 "  from " + i_stg_table + &
			 " where " + i_company_field + " = '" + co_val + "'" + &
			 "	  and budget_version = '" + i_bv + "'"
			 
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Inserting into " + i_detail_table + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		sq = "drop table crbdbdg_insert_error"
		execute immediate :sq;
		sq = "create table crbdbdg_insert_error as select * from " + i_stg_table
		execute immediate :sq;
		rollback;
		rtn = -1
		goto halt_the_app
	else
		f_pp_msgs("--- " + string(sqlca.SQLNRows) + " inserted for " + i_company_field + " '" + co_val + "' at " + string(now()))
	end if
			 
//	sqls = "update " + i_stg_table + " set interface_batch_id = '" + g_batch_id + "' " + &
//			  "where " + i_company_field + " = '" + co_val + "'" + &
//			   " and budget_version = '" + i_bv + "'"
//	
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating " + i_stg_table + ".interface_batch_id: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		rollback;
//		rtn = -1
//		goto halt_the_app
//	end if

next

////---------------------------------------------------------------------------------------
//// SEK 091510: Changed for performance: Commented out insert since it is taking place
////		above now.
////---------------------------------------------------------------------------------------
////*****************************************************************************************
////
////	 INSERT FROM i_stg_table TO i_detail_table.
////
////*****************************************************************************************
//f_pp_msgs("Inserting into " + i_detail_table + " at: " + string(now()))
//
//sqls = "insert into " + i_detail_table + " select * from " + i_stg_table
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Inserting into " + i_detail_table + ": " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	sq = "drop table crbdbdg_insert_error"
//	execute immediate :sq;
//	sq = "create table crbdbdg_insert_error as select * from " + i_stg_table
//	execute immediate :sq;
//	rollback;
//	rtn = -1
//	goto halt_the_app
//else
//	f_pp_msgs("--- " + string(sqlca.SQLNRows) + " inserted at " + string(now()))
//end if



////*****************************************************************************************
////
////  UPDATE cr_cost_repository ...
////
////*****************************************************************************************
//f_pp_msgs("Updating cr_cost_repository at " + string(now()))
//
//uo_cr_cost_repository uo_cr
//uo_cr = CREATE uo_cr_cost_repository
//
//for c = 1 to ds_co.RowCount()
//	
//	co_val = ds_co.GetItemString(c, 1)
//	
//	f_pp_msgs("--" + i_company_field + " = " + co_val)
//	
//	g_batch_id = co_ifb_array[c]
//	
//	//pass the source_id and the batch
//	rtn = uo_cr.uf_insert_to_cr_batch_id(i_source_id, string(g_batch_id))
//	
//	if rtn = 1 then
//	else
//		choose case rtn
//			case -1
//				f_pp_msgs("ERROR: inserting into cr_temp_cr: " + sqlca.SQLErrText)
//			case -2
//				f_pp_msgs("ERROR: cr_temp_cr.id UPDATE failed: " + sqlca.SQLErrText)			
//			case -3
//				f_pp_msgs("ERROR: cr_temp_cr.drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
//			case -4
//				f_pp_msgs("ERROR: " + i_detail_table + ".drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
//			case -5
//				f_pp_msgs("ERROR: inserting into cr_cost_repository: " + sqlca.SQLErrText)			
//			case -6
//				f_pp_msgs("ERROR: deleting from cr_temp_cr UPDATE failed: " + sqlca.SQLErrText)			
//			case else
//				f_pp_msgs("ERROR: unknown error in uo_cr_cost_repository: " + sqlca.SQLErrText)			
//		end choose
//		rollback;
//		rtn = -1
//		goto halt_the_app
//	end if
//
//next



//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:  For SAP-like clients you'd want to mark the cwip_charge_status
//                         to -id on the offsets (assuming that the "original" txns were
//                         marked in the same manner.  This is a good place to do that.
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom_bdg("just after summarization", 0, " ", "too late for a table name")

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	rtn = -1
	goto halt_the_app
end if


after_summarization:



//  REALLY, REALLY NOT FOR BUDGET ... 6/18/09:  OK now since we are using the global
//  temp table as i_stg_table.
//*****************************************************************************************
//
//  TRUNCATE i_stg_table ...
//
//*****************************************************************************************
f_pp_msgs("Truncating all records from " + i_stg_table + " at " + string(now()))

sqlca.truncate_table(i_stg_table)

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Truncating " + i_stg_table + ": " + sqlca.SQLErrtext)
	f_pp_msgs("  ")
	rollback;
	rtn = -1
	goto halt_the_app
end if



//
//  DMJ: 10/30/09: SCS TESTING:
//    NOT CLEAR WHY WOULD WOULD NEED THIS SQL TO RUN.  IT IS DIFFICULT TO INDEX
//    AND GET GOOD PERFORMANCE.
//
////*****************************************************************************************
////
////  PREP FOR LATER ... NOT CLEAR IF WE WILL KEEP THIS CODE.
////
////*****************************************************************************************
//max_id = 0
//sqls = "select max(id) from " + i_detail_table + " where cr_derivation_status = 'I'" + &
//		   " and budget_version = '" + i_bv + "'"
//i_ds_max_id.SetSQLSelect(sqls)
//i_ds_max_id.RETRIEVE()
//if i_ds_max_id.RowCount() <= 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: selecting the max(id) from " + i_detail_table + " (no rows returned): " + sqlca.SQLErrtext)
//	f_pp_msgs("  ")
//	rollback;
//	rtn = -1
//	goto halt_the_app
//end if
//max_id = i_ds_max_id.GetItemNumber(1, 1)
//if isnull(max_id) then max_id = 0





//
//  DMJ:  11/10/09:  FOR BUDGETING IT IS APPROPRIATE TO LET THE SAME RECORDS GO THROUGH MULTIPLE
//  TIMES.  THE BATCH DERIVATIONS MAY BE USED ALMOST LIKE ALLOCATIONS, SO YOU COULD GET SCENARIOS
//  LIKE: RUN SOME BATCH DERIVATIONS, RUN SOME REAL ALLOCATIONS, RUN SOME MORE BATCH DERIVATIONS.
//  SO WE CANNOT PERMANENTLY MARK RECORDS AS "DO NOT RUN THESE THROUGH THE DERIVATIONS AGAIN".
//
////*****************************************************************************************
////
////  MARK THE BASIS RECORDS WITH A 'P' FOR PROCESSED.
////  CHANGE: MARK THEM WITH THE IFB ID SO THEY CAN BE UN-MARKED IF A USER DELETES
////          THE INTERFACE BATCH.
////
////    We still want to do this when there were no results generated so the
////    process does not keep spinning through the same records over and over again.
////    
////    Note: the statement directly above means that transactions where we do not find
////    a match in CDC.STRING will also be marked and will not re-process.  This works
////    the same as a transaction interface where the txns do not get any derivation
////    results and the trueup must perform the "initial" split at some point.
////    A "default" derivation type can aid with this (e.g. to prevent $$$ from being
////    stranded in a clearing account).
////
////*****************************************************************************************
//f_pp_msgs("Updating with Interface Batch Id at " + string(now()))
//
//if this_source_has_results then
//	
//	for c = 1 to ds_co.RowCount()
//		
//		co_val = ds_co.GetItemString(c, 1)
//		
//		f_pp_msgs("--" + i_company_field + " = " + co_val)
//		
//		g_batch_id = co_ifb_array[c]
//		
//		//  Update ifb_id with the g_batch_id variable we got above.
//		sqls = "update " + i_detail_table + " set cr_derivation_status = '" + g_batch_id + "' " + &
//				  "where month_number = " + string(i_basis_mn) + " and cr_derivation_status = 'I' " + &
//				    "and " + i_company_field + " = '" + co_val + "'" + &
//				   " and budget_version = '" + i_bv + "'"
//		
//		if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
//		
//		execute immediate :sqls;
//		
//		if sqlca.SQLCode < 0 then
//			f_pp_msgs("  ")
//			f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_status with interface batch id: " + sqlca.SQLErrText)
//			f_pp_msgs("  ")
//			f_pp_msgs("sqls = " + sqls)
//			f_pp_msgs("  ")
//			rollback;
//			rtn = -1
//			goto halt_the_app
//		end if
//		
//	next
//
//else
//	//  No results means no interface batch, but we still want to mark the processed records so
//	//  they don't continue to be processed over and over again.  Let's just use negative id.
//	//  This will give us the ability to gracefully null these out at some point if they decide
//	//  that these records really should have created results (e.g. perhaps due to missing CDC
//	//  records at the time this ran).
//	sqls = "update " + i_detail_table + " set cr_derivation_status = -id " + &
//			  "where month_number = " + string(i_basis_mn) + " and cr_derivation_status = 'I'" + &
//			   " and budget_version = '" + i_bv + "'"
//	
//	if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
//	
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_status with interface batch id: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		f_pp_msgs("sqls = " + sqls)
//		f_pp_msgs("  ")
//		rollback;
//		rtn = -1
//		goto halt_the_app
//	end if
//	
//end if





//
//  THIS TABLE IS STUPID TO BEING WITH AND REALLY STUPID FOR BUDGET.
//
////*****************************************************************************************
////
////  INSERT the batch derivation results ...
////
////    We still want to do this when there were no results generated so the
////    max_id_processed gets incremented.
////
////*****************************************************************************************
//f_pp_msgs("Inserting batch derivation results at " + string(now()))
//
//s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
//ddate  = date(left(s_date, 10))
//ttime  = time(right(s_date, 8))
//
//finished_at = datetime(ddate, ttime)
//g_finished_at = finished_at
//
//for i = 1 to upperbound(i_type_array)
//	
//	if this_source_has_results then
//		for c = 1 to ds_co.RowCount()
//			if i_co_array[i] = ds_co.GetItemString(c, 1) then
//				g_batch_id = co_ifb_array[c]
//			end if
//		next
//	else
//		setnull(g_batch_id)
//	end if
//	
//	insert into cr_batch_derivation_results
//		(sort_order, "TYPE", source_id, company, process_date, max_id_processed, interface_batch_id)
//	values
//		(:i_sort_order, :i_type_array[i], :i_source_id, :i_co_array[i], :g_finished_at, :max_id, :g_batch_id);
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Inserting into cr_batch_derivation_results: " + sqlca.SQLErrtext)
//		f_pp_msgs("  ")
//		rollback;
//		rtn = -1
//		goto halt_the_a	pp
//	end if
//	
//	//  This seems like the proper place to flush records from the "on demand" queue
//	//  if they exist.
//	delete from cr_batch_derivation_queue
//	 where "TYPE" = :i_type_array[i] and source_id = :i_source_id and company = :i_co_array[i];
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: deleting from cr_batch_derivation_queue: " + sqlca.SQLErrtext)
//		f_pp_msgs("  ")
//		rollback;
//		rtn = -1
//		goto halt_the_app
//	end if
//	
//next



//*****************************************************************************************
//
//  COMMIT HERE ... don't want to kill the interface without loading if the only
//	 thing that fails is the insert into cr_interface_dates below or the 
//	 archiving.  Both of which are easy to do by hand and can be left alone as
//	 longlong as they are done before the next run of the interface.
//
//  As we do in a txn interface ... s/b OK since we've posted and logged the max_id
//  and such.
//
//*****************************************************************************************

i_posted_source_ids[upperbound(i_posted_source_ids) + 1] = i_source_id

commit;



//*****************************************************************************************
//
//  INSERT INTO cr_interface_dates ...
//
//    Note: if there are multiple types and companies for a source, it will already
//    be in the arrays.
//
//*****************************************************************************************
f_pp_msgs("Capturing cr_interface_dates statistics at " + string(now()))

//  If no records posted yet, then no point in firing this.
if isnull(g_batch_id) then goto after_interface_dates

for c = 1 to ds_co.RowCount()
	
	co_val = ds_co.GetItemString(c, 1)
	
	f_pp_msgs("--" + i_company_field + " = " + co_val)
	
	g_batch_id = co_ifb_array[c]
	
	//  DMJ: 11/6/09:
	//    Killing any BV indexes since they are going to be less efficient than IFB_ID.  When
	//    patitioning, Oracle seemed to want to select any indexs with BV on the front.
	sqls = &
		"insert into cr_interface_dates " + &
			"(company, interface_id, month_number, month_period, process_date, " + &
			 "total_dollars, total_records, feeder_system_id, total_debits, total_credits) (" + &
		"select '" + co_val + "'," + string(g_interface_id) + ", month_number, month_period, sysdate, sum(amount), " + &
				 "count(*), interface_batch_id, sum(decode(sign(amount),1,amount,0)), " + &
				 "sum(decode(sign(amount),-1,amount,0)) " + &
		  "from " + i_detail_table + " " + &
		 "where interface_batch_id = '" + g_batch_id + "' " + &
	     " and budget_version||'' = '" + i_bv + "' " + &
		 "group by month_number, month_period, interface_batch_id)"
	
	if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Inserting into cr_interface_dates: " + sqlca.SQLErrtext)
		f_pp_msgs("  ")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		rollback;
		rtn = -1
		goto halt_the_app
	end if
	
next

after_interface_dates:



//*****************************************************************************************
//
//  JUMP OFF POINT IN THE EVENT OF VALIDATION KICKOUTS:
//
//    To prevent posting the transactions and flushing the stg table.
//
//*****************************************************************************************
//after_posting:


commit;



//*****************************************************************************************
//
//  END OF OUTERMOST LOOP:  Over the list of source_ids to process.
//
//*****************************************************************************************
NEXT  //  FOR s = 1 TO i_ds_distinct_source_id.RowCount() ...


NEXT  //  FOR M = 1 TO NUM_MONTHS ...



DESTROY i_uo_cr_derivation
DESTROY l_uo_cr_validation

//*****************************************************************************************
//
//  HALT the application ...
//
//*****************************************************************************************
halt_the_app:

update cr_batch_derivation_dates_bdg
set running_session_id = NULL
where "TYPE" = :i_dt
and budget_version = :i_bv
and batch_id = :batch_id;

if sqlca.SQLCode = 0 then
	commit;
else
	rollback;
end if

if rtn = 1 then
	g_rtn_code = 0
else
	g_rtn_code = rtn
	
	//
	//  On error, send an email.
	//
	//	Find the user (from cr_system_control) to send a failure e-mail to.
	select control_value into :to_user
	  from cr_system_control
	 where upper(control_name) = 'INTERFACE ERROR EMAIL ADDRESS';
	
	if upper(to_user) <> "NONE" then
		g_msmail = create uo_smtpmail
		f_send_mail('pwrplant', 'INTERFACE FAILURE', 'The CR BATCH DERIVATION BDG ' + &
			'encountered an error and terminated.', to_user)
	end if
	
end if

if overall_kickouts then
	return -2
else
	return g_rtn_code
end if
end function

public function longlong uf_txn ();//*****************************************************************************************
//
//  Object      :  uo_cr_batch_derivation_bdg
//  UO Function :  uf_txn
//  Description :  Behaves like a transaction interface, booking the batch derivation
//                 results to the appropriate detail table(s).
//
//	 Returns     :  1 if successful
// 					-1 on error
//						 2 if no targets generated (nothing to do)
//
//*****************************************************************************************
longlong counter, rtn, lvalidate_rtn, validation_counter, i, ix, j, &
		 update_derivations, num_types, t, update_cr_derivation_rollup, &
		 update_rollup_on_detail_table, no_source_transactions, &
		 id_balancing, elig_counter
// SEK 082410: Changed max_id and orig_max_id from longlong to longlong due to volume of
//		data at Southern.  Also changing tbl_id just in case (though this sequence
//		will most likely never get above 2147483647); I'm not changing the counters
//		because we'll have major performance issues if they have over 2 billion records anyway.
longlong max_id, orig_max_id, tbl_id
string sqls, basis, offset_basis, offset_type, gl_jcat, mn_string, cv_validations, &
		 s_date, filter_string, the_type, update_basis, delimiter, join_fields[], &
		 basis_2nd_arg, basis_6th_arg, rc, ce_val, id_balancing_basis, split_quantities, &
		 tbl_sqls, null_str_array[]
boolean update_orig, validation_kickouts, no_update_derivations
decimal {2} total_orig, total_targ, total_offs


//*****************************************************************************************
//
//  Need the original max(id) so I don't overwrite the id field on those transactions
//  if we have a loop of more than 1 derivation type.  Otherwise, on types 2 to n, the
//  cr_derivation_id will not explain the id it came from.
//
//*****************************************************************************************
f_pp_msgs("selecting orig_max_id at " + string(now()))
orig_max_id = 0
sqls = "select max(id) from " + i_stg_table // No need for company in this where clause
i_ds_max_id.SetSQLSelect(sqls)
i_ds_max_id.RETRIEVE()
if i_ds_max_id.RowCount() > 0 then
	orig_max_id = i_ds_max_id.GetItemNumber(1, 1)
end if
if isnull(orig_max_id) then orig_max_id = 0

f_pp_msgs("--- orig_max_id = " + string(orig_max_id))


//*****************************************************************************************
//
//  Get the interface_id from cr_interfaces ...
//    DO NOT perform an upper on the select value ... 
//      This must be before the check for invalid ids !
//
//*****************************************************************************************

//  NOT CLEAR WHAT WE WOULD DO FOR BUDGET:
////
////  CLEAR OUT ANY INTERCOMPANY RECORDS THAT ARE LEFT OVER FROM PRIOR VALIDATION KICKOUTS.
////
//f_pp_msgs("Deleting any prior intercompany transactions at " + string(now()))
//
//sqls = "delete from " + i_stg_table + " where drilldown_key = 'INTERCO'"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Deleting any prior intercompany transactions in " + i_stg_table + ": " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


//  NOT FOR BUDGET:
////	 ONLY UPDATING ORIG_PROJECT, ORIG_WORK_ORDER THE 1ST TIME THROUGH.
////  ****************************************************************************  NOT CLEAR IF WE NEED THIS  ***************************************
//counter = 0
//sqls = "select count(*) from " + i_stg_table
//i_ds_counter.SetSQLSelect(sqls)
//i_ds_counter.RETRIEVE()
//if i_ds_counter.RowCount() > 0 then
//	counter = i_ds_counter.GetItemNumber(1, 1)
//end if
//if isnull(counter) then counter = 0
//
//if counter > 0 then
//	//  Not the 1st time through since the STG table has records.
//	update_orig = false
//else
//	update_orig = true
//end if
	


//
//  NOT VALID FOR THIS PROCESS.  WE DON'T WANT TO REPROCESS ANY INVALID RECORDS IN THE
//  STG TABLE SINCE THAT WOULD PRECLUDE THEM FROM ALTERING THE DERIVATION CONTROL RECORDS
//  AND GENERATE A DIFFERENT RESULT.
//
////*****************************************************************************************
////
////	 If this process ran previously but stopped b/c of validation kickouts, I don't want 
////  to book any new transaction until the previous run has been loaded.
////
////*****************************************************************************************
//f_pp_msgs("Checking cr_validations_invalid_ids for prior validation kickouts at " + &
//	string(now()))
//
//counter = 0
//select count(*) into :counter
//  from cr_validations_invalid_ids
// where lower(table_name) = :i_stg_table;
//
//if counter > 0 then
//	//  THERE WERE VALIDAITON KICKOUTS IN THE PREVIOUS RUN.
//	f_pp_msgs("  ")
//	f_pp_msgs("There are validation kickouts from a previous run.  The process" + &
//		" will go straight to validations.")
//	f_pp_msgs("NEW BATCH DERIVATION TRANSACTIONS WILL NOT BE GENERATED!")
//	f_pp_msgs("  ")
//	
//	//	Get the batch_id from the previous run to pass to validations
//	setnull(g_batch_id)
//	sqls = "select min(interface_batch_id) from " + i_stg_table
//	i_ds_ifb_id.SetSQLSelect(sqls)
//	i_ds_ifb_id.RETRIEVE()
//	if i_ds_ifb_id.RowCount() > 0 then
//		g_batch_id = i_ds_ifb_id.GetItemString(1, 1)
//	end if
//	
//	if isnull(g_batch_id) then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: could not determine the min(interface_batch_id) for: " + i_stg_table)
//		f_pp_msgs("  ")
//		rollback;
//		return -1
//	end if
//	
//	goto skip_insert
//else
//
//	
//	//  THERE WERE NO CR VALIDAITON KICKOUTS IN THE PREVIOUS RUN.  HOWEVER ...
//	
//	//	 If there are records that passed validations but for which I could not
//	//	 derive a (funding) project or gl_location, then I need to go straight to validations.
//	f_pp_msgs("Checking " + i_stg_table + " for prior project derivation kickouts at " + &
//		string(now()))
//	
//	counter = 0
//	sqls = &
//		"select count(*) from " + i_stg_table + &
//		" where " + i_proj_field + " = ' ' and " + i_wo_field + " <> ' '"
//	i_ds_counter.SetSQLSelect(sqls)
//	i_ds_counter.RETRIEVE()
//	if i_ds_counter.RowCount() > 0 then
//		counter = i_ds_counter.GetItemNumber(1, 1)
//	end if
//	if isnull(counter) then counter = 0
//	
//	if counter > 0 then
//		f_pp_msgs("")
//		f_pp_msgs("There are (funding) project derivation kickouts from a previous run.  The " + &
//			"interface will go straight to validations.")
//		f_pp_msgs("NEW BATCH DERIVATION TRANSACTIONS WILL NOT BE GENERATED!")
//		f_pp_msgs("")
//		
//		//	Get the batch_id from the previous run to pass to validations
//		setnull(g_batch_id)
//		sqls = "select min(interface_batch_id) from " + i_stg_table
//		i_ds_ifb_id.SetSQLSelect(sqls)
//		i_ds_ifb_id.RETRIEVE()
//		if i_ds_ifb_id.RowCount() > 0 then
//			g_batch_id = i_ds_ifb_id.GetItemString(1, 1)
//		end if
//		
//		goto skip_insert
//		
//	end if
//	
//
//	//  THERE WERE NO CR VALIDAITON KICKOUTS NOR FP/GL LOC DERIVATION KICKOUTS IN THE 
//	//  PREVIOUS RUN.  If there are records in cr_payroll_stg, use that batch_id.
//	//  These would be due to an error in the interface.  
//	
//	counter = 0
//	sqls = "select count(*) from " + i_stg_table
//	i_ds_counter.SetSQLSelect(sqls)
//	i_ds_counter.RETRIEVE()
//	if i_ds_counter.RowCount() > 0 then
//		counter = i_ds_counter.GetItemNumber(1, 1)
//	end if
//	if isnull(counter) then counter = 0
//
//	if counter > 0 then
//		//  Get the interface_batch_id from stg table.
//		setnull(g_batch_id)
//		sqls = "select min(interface_batch_id) from " + i_stg_table
//		i_ds_ifb_id.SetSQLSelect(sqls)
//		i_ds_ifb_id.RETRIEVE()
//		if i_ds_ifb_id.RowCount() > 0 then
//			g_batch_id = i_ds_ifb_id.GetItemString(1, 1)
//		end if
//		
//		if isnull(g_batch_id) then
//			
//			//  Don't get one here (burns numbers if there are no records to post).
//			need_to_get_a_batch_id = true
//			
//			if g_batch_id = '0' then
//				f_pp_msgs("  ")
//				f_pp_msgs("ERROR: Could not get the batch_id from costrepository.nextval")
//				f_pp_msgs("  ")
//				return -1
//			end if
//		end if
//		
//		//  This catches invalid month_numbers scne they do not get logged in the invalid_ids.
//		goto skip_insert
//	else
//		
//		//  Don't get one here (burns numbers if there are no records to post).
//		need_to_get_a_batch_id = true
//				
//	end if
//	
//end if



//*****************************************************************************************
//
//  GET CONTROL VALUES FROM CR_BATCH_DERIVATION_CONTROL:
//
//*****************************************************************************************
setnull(basis)
setnull(offset_basis)
setnull(offset_type)
setnull(gl_jcat)
setnull(filter_string)
setnull(update_derivations)
setnull(update_cr_derivation_rollup)
setnull(update_rollup_on_detail_table)
setnull(no_source_transactions)
setnull(id_balancing)
setnull(split_quantities)
select trim(basis), trim(offset_basis), trim(offset_type), trim(gl_journal_category),
		 trim(filter_string), update_derivations, update_cr_derivation_rollup, 
		 update_rollup_on_detail_table, no_source_transactions, id_balancing,
		 split_quantities
  into :basis, :offset_basis, :offset_type, :gl_jcat,
  		 :filter_string, :update_derivations, :update_cr_derivation_rollup,
		 :update_rollup_on_detail_table, :no_source_transactions, :id_balancing,
		 :split_quantities
  from cr_batch_derivation_control
 where "TYPE" = :i_type and source_id = :i_source_id;
if isnull(basis) or basis = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.basis is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if
if isnull(offset_basis) or offset_basis = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.offset_basis is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if
if isnull(offset_type) or offset_type = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.offset_type is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if
if isnull(gl_jcat) or gl_jcat = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: cr_batch_derivation_control.gl_jcat is NULL for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if
if isnull(filter_string) then
	filter_string = ""  //  This field is optional.
end if
if mid(trim(lower(filter_string)),1,5) = 'where' then filter_string = mid(trim(filter_string),6)

// ### BAT - Maint 8966 - 20120327 - Don't force "and" to the beginning of filter_string if filter_string = ""
if filter_string <> "" then
	if mid(trim(lower(filter_string)),1,3) <> 'and' then filter_string = 'and ' + filter_string
end if 

if isnull(update_derivations) then
	update_derivations = 0
end if
if isnull(update_cr_derivation_rollup) then
	update_cr_derivation_rollup = 0
end if
if isnull(update_rollup_on_detail_table) then
	update_rollup_on_detail_table = 0
end if
if isnull(no_source_transactions) then
	no_source_transactions = 0
end if
if isnull(id_balancing) then
	id_balancing = 0
end if
if isnull(split_quantities) then
	split_quantities = "None"
end if

if g_debug = "YES" then f_pp_msgs("***DEBUG*** basis = " + basis)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** offset_basis = " + offset_basis)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** offset_type = " + offset_type)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** gl_jcat = " + gl_jcat)
if g_debug = "YES" then f_pp_msgs("***DEBUG*** update_derivations = " + string(update_derivations))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** update_cr_derivation_rollup = " + string(update_cr_derivation_rollup))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** update_rollup_on_detail_table = " + string(update_rollup_on_detail_table))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** no_source_transactions = " + string(no_source_transactions))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** id_balancing = " + string(id_balancing))
if g_debug = "YES" then f_pp_msgs("***DEBUG*** split_quantities = " + string(split_quantities))



//*****************************************************************************************
//
//  DETERMINE IF THERE IS ANYTHING TO DO.
//
//*****************************************************************************************
f_pp_msgs("Counting the records to be processed at " + string(now()))

//
// DMJ: 11/17/09: MAKES THIS OBSOLETE
//
//counter = i_i_records


//
//  COMMENTED OUT FOR PERFORMANCE:  THIS IS THE SAME NUMBER AS THE NUMBER
//  OF RECORDS UPDATED IN THE 'I' UPDATE IN UF_READ()
//
//counter = 0
//sqls = &
//	"select count(*) from " + i_detail_table + &
//	" where month_number = " + string(i_basis_mn) + &
//	  " and cr_derivation_status = 'I' " + &
//	  " and " + i_company_field + " = '" + i_co + "' " + &
//	  " and budget_version = '" + i_bv + "'"
//if filter_string <> "" then
//	f_pp_msgs("*** filter_string = " + filter_string + " ***")
//end if
//sqls = sqls + " " + filter_string
//if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)
//i_ds_counter.SetSQLSelect(sqls)
//i_ds_counter.RETRIEVE()
//if i_ds_counter.RowCount() > 0 then
//	counter = i_ds_counter.GetItemNumber(1, 1)
//end if
//if isnull(counter) then counter = 0


//
// DMJ: 11/17/09: MAKES THIS OBSOLETE
//
//if counter <= 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("There are no eligible transactions.  Nothing to do.")
//	return 2
//end if
//
//f_pp_msgs(string(counter) + " records are eligible to be processed")



////*****************************************************************************************
////
////  TELL THEM IF THERE IS NOTHING TO DO.
////
////  *************************************************  I THINK THIS HAS TO GO AFTER THE DERIVATION DETERMINES IF ANY RECORDS WERE CREATED  ****************
////
////*****************************************************************************************
//num_rows = 0
////num_rows2 = 0
//select count(*) into :num_rows  from cr_so_payroll_stg   where upload_indicator = 'I';
////select count(*) into :num_rows2 from cr_so_inventory_issues_stg where upload_indicator = 'I';
//if isnull(num_rows)  then num_rows  = 0
////if isnull(num_rows2) then num_rows2 = 0
//
//if num_rows = 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("------------------------------------------------------------------------------------")
//	f_pp_msgs(": There are no Payroll entries waiting to be posted to the CR.")
//	f_pp_msgs("------------------------------------------------------------------------------------")
//	f_pp_msgs("  ")
//	rollback;
//	goto halt_the_app
//end if
//
//
//
////*****************************************************************************************
////
////  GET AN INTERFACE BATCH ID IF NECESSARY:  Getting it here instead of above prevents
////  us from burning sequence values if there are no records to post.
////
////  *************************************************  I THINK THIS HAS TO GO AFTER THE DERIVATION DETERMINES IF ANY RECORDS WERE CREATED  ****************
////
////*****************************************************************************************
//if need_to_get_a_batch_id then
//	select costrepository.nextval into :g_batch_id from dual;
//	if isnull(g_batch_id) then g_batch_id = '0'
//	
//	if g_batch_id = '0' then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Could not get the batch_id from costrepository.nextval")
//		f_pp_msgs("  ")
//		rtn = -1
//		goto halt_the_app
//	end if
//end if
//


//*****************************************************************************************
//
//  PERFORM THE INSERT FROM THE DETAIL TABLE TO THE STAGING TABLE.
//
//  These are the "ORIGINAL" records that will be used as the basis for the derivations.
//  
//  Notes:
//  ------
//    1)  The id can be inserted as-is since these records will be deleted later.
//    2)  Month number will be updated later on the results to the current period.
//    3)  IFB Id will be updated later, after the derivations have run, and we know if
//        there are any derivation results.  We don't want to burn nextvals for
//        no good reason.
//
//*****************************************************************************************

if no_source_transactions = 1 then goto after_source_transaction_insert

//  Insert the records that need to be processed.
f_pp_msgs("Inserting from " + i_detail_table + " to " + i_stg_table + " at " + string(now()))

//
// DMJ: 11/17/09: I AM SIMPLY GOING TO APPLY THIS WHERE CLAUSE TO THE INSERT IN UF_TXN().
// IT IS NOT EFFICIENT TO DO BOTH.  SEE UF_READ()
//
// DMJ: 11/23/09: SEEMS LIKE BUDGET DATA ENTRY POSTS TO 
//

//////sqls = &
//////	"insert into " + i_stg_table + " " + &
//////	"select * from " + i_detail_table + &
//////	" where month_number = " + string(i_basis_mn) + &
//////	  " and cr_derivation_status = 'I' " + &
//////	  " and " + i_company_field + " = '" + i_co + "' " + &
//////	  " and budget_version = '" + i_bv + "'" // THIS IS VERY OLD CODE ...


// -------------------------------------------------------------------------------------
// DMJ PERF: COMMENTED OUT TO AVOID SOME UPDATES:
// -------------------------------------------------------------------------------------
//sqls = &
//	"insert into " + i_stg_table + " " + &
//	"select * from " + i_detail_table + &
//	" where month_number = " + string(i_basis_mn) + &
//	  " and (cr_derivation_status IS NULL or cr_derivation_status = '0') " + &
//	  " and " + i_company_field + " = '" + i_co + "' " + &
//	  " and budget_version = '" + i_bv + "'" // COMMENTED OUT IN LIEU OF THE CODE BELOW
// -------------------------------------------------------------------------------------


// -------------------------------------------------------------------------------------
// DMJ PERF: ADDED TO AVOID SOME UPDATES:
// -------------------------------------------------------------------------------------
longlong c
string budget_col, group_sqls
sqls = "insert into " + i_stg_table + " ("
group_sqls = "group by "
for c = 1 to i_ds_all_tab_columns_bdg.RowCount()
	budget_col = upper(i_ds_all_tab_columns_bdg.GetItemString(c, 1))
	if c = i_ds_all_tab_columns_bdg.RowCount() then
		sqls = sqls + '"' + budget_col + '" '
	else
		sqls = sqls + '"' + budget_col + '", '
	end if
next
sqls = sqls + ") (select crbudgets.nextval, a.* from (select "
for c = 1 to i_ds_all_tab_columns_bdg.RowCount()
	budget_col = upper(i_ds_all_tab_columns_bdg.GetItemString(c, 1))
	if c = i_ds_all_tab_columns_bdg.RowCount() then
		choose case budget_col
			case "ID"
				//  Handled in the outer SQL ...
			case "CR_DERIVATION_STATUS"
				sqls = sqls + "'I' cr_derivation_status "
			case "CR_TXN_TYPE"
				sqls = sqls + "'ORIGINAL' cr_txn_type "
			case "CR_DERIVATION_RATE"
				sqls = sqls + "NULL cr_derivation_rate "
			case "CR_DERIVATION_ID"
				sqls = sqls + "NULL cr_derivation_id "
			case "LINE_DESCRIPTION"
				sqls = sqls + "NULL line_description "
			case "AMOUNT"
				sqls = sqls + "sum(amount) amount "
			case "QUANTITY"
				sqls = sqls + "sum(quantity) quantity "
			case "INCEPTION_TO_DATE_AMOUNT"
				sqls = sqls + "sum(inception_to_date_amount) inception_to_date_amount "
			case else
				sqls       = sqls       + '"' + budget_col + '" '
				group_sqls = group_sqls + '"' + budget_col + '" '
		end choose
	else
		choose case budget_col
			case "ID"
				//  Handled in the outer SQL ...
			case "CR_DERIVATION_STATUS"
				sqls = sqls + "'I' cr_derivation_status, "
			case "CR_TXN_TYPE"
				sqls = sqls + "'ORIGINAL' cr_txn_type, "
			case "CR_DERIVATION_RATE"
				sqls = sqls + "NULL cr_derivation_rate, "
			case "CR_DERIVATION_ID"
				sqls = sqls + "NULL cr_derivation_id, "
			case "LINE_DESCRIPTION"
				sqls = sqls + "NULL line_description, "
			case "AMOUNT"
				sqls = sqls + "sum(amount) amount, "
			case "QUANTITY"
				sqls = sqls + "sum(quantity) quantity, "
			case "INCEPTION_TO_DATE_AMOUNT"
				sqls = sqls + "sum(inception_to_date_amount) inception_to_date_amount, "
			case else
				sqls       = sqls       + '"' + budget_col + '", '
				group_sqls = group_sqls + '"' + budget_col + '", '
		end choose
	end if
next
sqls = sqls + &
	"from " + i_detail_table + &
	" where month_number = " + string(i_basis_mn) + &
	  " and (cr_derivation_status IS NULL or cr_derivation_status = '0') " + &
	  " and " + i_company_field + " = '" + i_co + "' " + &
	  " and budget_version = '" + i_bv + "' "
////sqls = sqls + ")"
// -------------------------------------------------------------------------------------



if filter_string <> "" then
	f_pp_msgs("*** filter_string = " + filter_string + " ***")
end if

sqls = sqls + " " + filter_string


// -------------------------------------------------------------------------------------
// DMJ PERF: ADDED TO AVOID SOME UPDATES: (CONTINUED FROM ABOVE)
// -------------------------------------------------------------------------------------
sqls = sqls + " " + group_sqls
sqls = sqls + ") a )"
// -------------------------------------------------------------------------------------


if g_debug = "YES" then f_pp_msgs("***DEBUG*** sqls = " + sqls)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: inserting into " + i_stg_table + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	//commit; // The append hint requires a commit or subsequent
	//	// updates to the i_stg_table will fail.
end if

elig_counter =sqlca.SQLNRows
f_pp_msgs(string(sqlca.SQLNRows) + " records inserted and are eligible to be processed")



f_pp_msgs("Gathering stats at 5 percent at " + string(now()))
sqlca.analyze_table_pct(i_stg_table, -5)



// -------------------------------------------------------------------------------------
// DMJ PERF: COMMENTED OUT TO AVOID SOME UPDATES:
// -------------------------------------------------------------------------------------
////
//// DMJ: 11/17/09: THE STG RECORDS NEED TO HAVE THE 'I' THOUGH ... SEE COMMENT BELOW.
////
//f_pp_msgs("Updating " + i_stg_table + ".cr_derivation_status at " + string(now()))
//sqls = &
//	"update " + i_stg_table + &
//	  " set cr_derivation_status = 'I' " + &
//	 "where " + i_company_field + " = '" + i_co + "' and (cr_derivation_status IS NULL or cr_derivation_status = '0') " + &
//	  " and budget_version = '" + i_bv + "'" 
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_txn_type: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


// -------------------------------------------------------------------------------------
// DMJ PERF: COMMENTED OUT TO AVOID SOME UPDATES:
// -------------------------------------------------------------------------------------
////  Initialize the "derivation" fields ... note that these fields are "ORIGINAL" and
////  the rate and id need to be NULLed out in the event that these transactions were
////  the result of prior derivations.
////
////  DMJ: 12/28/09: ADDED THE ORIG_MAX_ID CLAUSE.
////  WHEN RUNNING MULTIPLE DERIVATION TYPES AT THE SAME TIME, THE DELETE OF 'ORIGINAL' RECORDS
////  BELOW WAS DELETING THE EARLIER DERIVATION TYPES RESULTS SINCE THIS UPDATE HAD SET CR_TXN_TYPE
////  TO 'ORIGINAL'.  KEEPING 'TARGET' AND 'OFFSET' ON PRIOR RESULTS SHOULD ALLOW THEM TO BE USED
////  IN A SUBSEQUENT DERIVATION WHILE NOT BEING DELETED BELOW.  NOTE: THIS IS WHY WE ALLOW THEM
////  TO BE UPDATED TO 'I' IN THE SQL JUST ABOVE THIS (OR ELSE WE COULD ONLY ENFORCE DEPENDENCIES
////  BY RUNNING THE BATCH DERIVATION EXE TWICE).
////
////  DMJ: ID > ORIG_MAX_ID DID NOT WORK.  THERE IS A FUNDAMENTAL PROBLEM WITH THE ORDER OF EVENTS
////  IN THE BUDGET DERIVATIONS DUE TO THE PERFORMANCE CHANGES.  THE ORDER OF EVENTS IN THE UF_READ()
////  LOOPS IS NOW BAD.  MN AND THEN DERIVATION TYPE IS A POOR LOOPING CONSTRUCT.  IF THE USER TRIGGERS
////  2 DERIVATIONS INDIVIDUALLY, THE FIRST WILL RUN FOR (x) MONTHS AND THEN THE SECOND WILL RUN FOR (x)
////  MONTHS.  WHEREAS, IF THEY TRY TO RUN THEM TOGETHER, TYPE 1 WILL RUN FOR MN 1, THEN TYPE 2 WILL RUN
////  FOR MN1, THEN THE SAME FOR MN2, MN3, ETC.  THIS IS FUNDAMENTALLY DIFFERENT BEHAVIOR AND WOULD ALTER
////  THE DEPENDENCIES BASED ON HOW THEY RAN!  NO GOOD!.  THE ULTIMATE FIX WILL BE TO CHANGE THE LOOPING
////  STRUCTURE TO BE DERIVATION TYPE THEN MONTH_NUMBER.  THIS WILL ENSURE THAT THE EXE RUNS THE SAME WAY
////  REGARDLESS OF HOW THEY PHYSICALLY RUN.  IT IS TOO LATE AT SOUTHERN ON 12/28/09 TO IMPLEMENT THIS
////  FIX.  INSTEAD WE WILL RUN THE GPC-FERC-SUB AND FERC-SUB-LOC DERIVATIONS INDIVIDUALLY TO ENFORCE
////  THE DESIRED BEHAVIOR.
////
//f_pp_msgs("Updating " + i_stg_table + ".cr_txn_type at " + string(now()))
//sqls = &
//	"update " + i_stg_table + &
//	  " set cr_txn_type = 'ORIGINAL', cr_derivation_rate = NULL, cr_derivation_id = NULL " + &
//	 "where " + i_company_field + " = '" + i_co + "' and cr_derivation_status = 'I' " + &
//	  " and budget_version = '" + i_bv + "' " // The "I" is
//	 		// super-critical in this where clause !!!  Otherwise, we'll be clobbering any existing
//			// target and offset records already in the crb table !!!  This could occur if 2 or more
//			// derivation types are being run against this source !!!  The cr_derivation_status on
//			// results is set to 'P' below so they are avoided by this update !!!
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_txn_type: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if

after_source_transaction_insert:


//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:
//    Right after we've inserted into the "crb" staging table.  You might put code here
//    like that in the SAP transaction interfaces where we:  perform an initial ferc
//    account update, audit retire_je_code values against CDC, etc.
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom_bdg("right after insert to crb_stg", 0, " ", i_stg_table)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if



//*****************************************************************************************
//
//  Run the Insert Derivations:
//
//    "Basis" is the "where clause" for the derivations.  For example, it might be:
//    "cr_allocations_stg.work_order = cr_deriver_control.string and target_credit = 'TARGET'"
//    for the target side of the derivation and:
//    "cr_allocations_stg.work_order = cr_deriver_control.string and cr_txn_type = 'TARGET'"
//    for the offset side.
//
//    Note: if you want to restrict the amount types, you have to include something in
//    the basis where clause.  E.G. " and cr.amount_type = 1 " would restrict to actuals.
//
//    CR TXN TYPE --- for speed, we're assuming that field is on all the transaction
//    tables.  If that ends up bad in the future, we'll add a system switch and evaluate
//    it before we get to this code.
//
//    CR DERIVATION ROLLUP --- If you plan on setting the update_cr_derivation_rollup flag
//    to Yes (and plan on having it in the CDC.STRING) then the field MUST BE on all the
//    transaction tables ... or else you will get a rude surprise in the logs.
//
//*****************************************************************************************
f_pp_msgs("Starting the derivations at " + string(now()))


//  CUSTOM FUNCTION CALL:
//    Just before the derivations fire.  This is almost a duplicate of the one above,
//    but it plays into the logic that was built into interfaces at Pepco (where some
//    of their derivation audits were by retire_je_code).  This call would be a good
//    place to add audits like "Single Record For Default Type", or to call
//    uf_deriver_create_offset.
rc = f_cr_batch_derivation_custom_bdg("just before derivations fire", 0, " ", i_stg_table)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if


//  -----------------------------------  TARGET SECTION:  -----------------------------------


//  Need the max(id) so I can fire a cr_txn_type update below.  Just using brute force.
//  Also, take advantage of this loop to replace "cr." with the correct table name.
max_id = 0
sqls = "select max(id) from " + i_stg_table // No need for company in this where clause
i_ds_max_id.SetSQLSelect(sqls)
i_ds_max_id.RETRIEVE()
if i_ds_max_id.RowCount() > 0 then
	max_id = i_ds_max_id.GetItemNumber(1, 1)
end if
if isnull(max_id) then max_id = 0

f_pp_msgs("-- max_id = " + string(max_id))

if max_id <= 0 and elig_counter > 0  then
	//  Since we counted above, there should be records in the stg table at this point.
	//  If max_id is 0, I want to know about it.
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: max_id = 0 for: " + i_stg_table)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  Update the cr_derivation_rollup field (generally a rollup of cost elements
//  kept in the cdc.string.  If a client is using this process and not updating
//  the cr_derivation_rollup field in any transaction interfaces, it may be appropriate
//  to update the detail table too.  This would keep that field consistent on all
//  records, and aid in reconciling queries.
if update_cr_derivation_rollup = 1 then
	
	if update_rollup_on_detail_table = 1 then
		// ### 7924:  JAK: 2011-06-27:  Values by company logic.
		f_pp_msgs("Updating cr_derivation_rollup on detail table at " + string(now()))
		sqls = "update " + i_detail_table + " a set cr_derivation_rollup = (" + &
			"select cr_derivation_rollup from " + i_ce_me_table + " b " + &
			 'where a."' + i_ce_field + '" = b."' + i_ce_me_table_field + '"'
			 
		if i_ce_me_by_company = 1 then sqls += ' and a."' + i_company_field + '" = b."' + i_company_field + '"'
			 
		sqls += " and b.element_type= 'Budget') " + &
		"where month_number = " + string(i_basis_mn) + &
	    " and cr_derivation_status = 'I' " + &
	    " and " + i_company_field + " = '" + i_co + "'" + &
	    " and budget_version = '" + i_bv + "'"
		execute immediate :sqls;
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating " + i_detail_table + ".cr_derivation_rollup: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		//  It's not obvious that we should hang up the process with a NULL check at this point.  If the
		//  "cost elements" with a NULL are important, they should kick on the check below against the
		//  staging table.
	end if
	
	// ### 7924:  JAK: 2011-06-27:  Values by company logic.
	f_pp_msgs("Updating cr_derivation_rollup on staging table at " + string(now()))
	sqls = "update " + i_stg_table + " a set cr_derivation_rollup = (" + &
		"select cr_derivation_rollup from " + i_ce_me_table + " b " + &
		 'where a."' + i_ce_field + '" = b."' + i_ce_me_table_field + '"'
			 
	if i_ce_me_by_company = 1 then sqls += ' and a."' + i_company_field + '" = b."' + i_company_field + '"'
		 
	sqls += " and b.element_type= 'Budget') " + &
	"where " + i_company_field + " = '" + i_co + "'" + &
	    " and budget_version = '" + i_bv + "'"
	execute immediate :sqls;
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_derivation_rollup: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	sqls = 'select distinct "' + i_ce_field + '" from ' + i_stg_table + ' ' + &
	"where (cr_derivation_rollup is null or cr_derivation_status = '0') and " + i_company_field + " = '" + i_co + "'" + &
	    " and budget_version = '" + i_bv + "'"
	i_ds_null_rollup.SetSQLSelect(sqls)
	i_ds_null_rollup.RETRIEVE()
	if i_ds_null_rollup.RowCount() > 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: NULL cr_derivation_rollup values detected for the following " + i_ce_field + " values:")
		f_pp_msgs("---------------------------------------------------------------------------------------------------")
		for i = 1 to i_ds_null_rollup.RowCount()
			ce_val = i_ds_null_rollup.GetItemString(i, 1)
			f_pp_msgs(ce_val)
		next
		f_pp_msgs("  ")
		f_pp_msgs("The process cannot continue.")
		rollback;
		return -1
	end if
	
end if

//  The basis will almost certainly have "cr." references for the detail table.  Change that here
//  to the staging_table.
basis = f_replace_string(basis, "cr.", i_stg_table + ".", "all")

//  Need to include the company in the basis string since uf_txn() is being called in a company
//  loop.  Otherwise we run the risk of perfoming derivations over and over again on companies
//  that come earlier in the loop.
basis = basis + " and " + i_stg_table + "." + i_company_field + " = '" + i_co + "'" + &
				    " and budget_version = '" + i_bv + "'"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** basis = " + basis)

i_uo_cr_derivation.i_gl_jcat = gl_jcat

//  Call the indicated derivation type.
f_pp_msgs("Inserting derivation target results at " + string(now()))

rtn = i_uo_cr_derivation.uf_deriver( &
	i_type, &
	i_stg_table, &
	basis)

if rtn <> 1 then
	//  Assume that messages were logged by uf_deriver().
	rollback;
	return -1
else
	f_pp_msgs(string(sqlca.SQLNRows) + " rows inserted.")
end if

//  Now update the cr_txn_type.  Just using brute force.  All records > max_id were
//  just inserted by uf_deriver().
//
//  DMJ: 11/22/09: SETTING TO 'P' MAKES IT MUCH HARDER TO ENFORCE DEPENDENCIES.  REMOVING THIS
//  PART OF THE UPDATE.
//
//sqls = &
//	"update " + i_stg_table + " set cr_txn_type = 'TARGET', cr_derivation_status = 'P', cr_derivation_type = '" + i_type + "' " + &
//	 "where id > " + string(max_id) + &
//     " and budget_version = '" + i_bv + "'"

sqls = &
	"update " + i_stg_table + " set cr_txn_type = 'TARGET', cr_derivation_status = NULL, cr_derivation_type = '" + i_type + "' " + &
	 "where id > " + string(max_id) + &
     " and budget_version = '" + i_bv + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("DERIVATIONS: TARGETS ERROR: updating " + i_stg_table + ".cr_txn_type: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if



//  If there are no target records, log a message and return to the caller which will
//  loop through to the next value.
counter = 0
sqls = "select count(*) from " + i_stg_table + &
		 " where cr_txn_type = 'TARGET' and " + i_company_field + " = '" + i_co + "' " + &
			 "and id > " + string(max_id) + &
	      " and budget_version = '" + i_bv + "'"  // For the case of multiple derivation types that
			 		// are building on each other.  I.E. We don't want the count(*) for the 2nd
					// type to pick up records from the first.
i_ds_counter.SetSQLSelect(sqls)
i_ds_counter.RETRIEVE()
if i_ds_counter.RowCount() > 0 then
	counter = i_ds_counter.GetItemNumber(1, 1)
end if
if isnull(counter) then counter = 0

if counter <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("No TARGET records generated.  Nothing to do.")
	f_pp_msgs("  ")
	f_pp_msgs("basis = " + basis)
	f_pp_msgs("  ")
	//  This has to be done here, since we won't get to the code below.
	f_pp_msgs("Deleting ORIGINAL transactions at " + string(now()))
	sqls = "delete from " + i_stg_table + " where cr_txn_type = 'ORIGINAL'" + &
	    	   " and budget_version = '" + i_bv + "'"
	execute immediate :sqls;
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Deleting ORIGINAL transactions from " + i_stg_table + ": " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	return 2
end if



//
//  MOVED ABOVE FOR PERFORMANCE:  EFFECTIVELY HAVE MADE THIS A REQUIRED FIELD FOR THE BUDGET
//  BATCH DERIVATIONS.
//
////  Additional call here to update the cr_derivation_type since the offsets
////  may need it (in the case of multiple, possibley overlapping, derivation types.
//counter = 0
//select count(*) into :counter from all_tab_columns
// where table_name = upper(:i_stg_table) and column_name = 'CR_DERIVATION_TYPE';
//if isnull(counter) then counter = 0
//
//if counter > 0 then
//	f_pp_msgs("Performing other updates (cr_derivation_type) at " + string(now()))
//	
//	sqls = "update " + i_stg_table + &
//				" set cr_derivation_type = '" + i_type + "' where id > " + string(max_id) + &
//		      " and budget_version = '" + i_bv + "'"
//	
//	execute immediate :sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_derivation_type: " + &
//			sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		rollback;
//		return -1
//	end if
//end if



//  -----------------------------------  OFFSET SECTION:  -----------------------------------



f_pp_msgs("Inserting derivation offset results at " + string(now()))

//  Need the max(id) again so I can fire a cr_txn_type update below against the offsets.
max_id = 0
//  The SQL is already set in the ds for this staging table.
i_ds_max_id.RETRIEVE()
if i_ds_max_id.RowCount() > 0 then
	max_id = i_ds_max_id.GetItemNumber(1, 1)
end if
if isnull(max_id) then max_id = 0

f_pp_msgs("--- max_id = " + string(max_id))


//  The basis will almost certainly have "cr." references for the detail table.  Change that here
//  to the staging_table.
offset_basis = f_replace_string(offset_basis, "cr.", i_stg_table + ".", "all")

//  Need to include the company in the basis string since uf_txn() is being called in a company
//  loop.  Otherwise we run the risk of perfoming derivations over and over again on companies
//  that come earlier in the loop.
offset_basis = offset_basis + " and " + i_stg_table + "." + i_company_field + " = '" + i_co + "'" + &
								      " and budget_version = '" + i_bv + "'"

//  The " id in (select nvl(cr_derivation_id,0) from ... " piece of the
//  argument was added to avoid the following situation.  If a "STRING" in CDC only
//  had a record for the "offset" derivation type, then that "STRING"'s transactions
//  would get and offset record here, but no target record above.  Then, the default
//  target logic below WOULD NOT insert a default target records because it already
//  had an " id not in (select nvl(cr_derivation_id,0) from ..." piece in its argument.
//  I.E. the stranded offset was providing the cr_derivation_id that kept this "STRING"
//  from going through the default logic.  Thus, we ended up OOB.  I want to avoid BOTH
//  targets and credits if the CDC data is not whole for this "STRING" and let it just
//  go through the default logic below (since I can't prevent someone from manually
//  entering bad data in CDC).
//
//  DMJ: 11/24/09: THIS WAS LEFT OVER FROM SAP/TXN INTEGRATION AND WAS NOT APPROPRIATE FOR THE
//  BATCH DERIVATIONS.  THIS CLAUSE WOULD PREVENT US FROM BUILDING OFFSETS OFF OF THE TARGET
//  RECORDS AND THUS WOULD NOT ALLOW FOR PERCENTAGES THAT DID NOT ADD UP TO 1.00
//
////////offset_basis = offset_basis + " and " + &
////////	i_stg_table + ".id in (select nvl(cr_derivation_id,0) from " + i_stg_table + " " + &
////////                          "where cr_txn_type = 'TARGET' and cr_derivation_type = '" + i_type + "')"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** offset_basis = " + offset_basis)

//  Call the indicated offset derivation type.
rtn = i_uo_cr_derivation.uf_deriver( &
	offset_type, &
	i_stg_table, &
	offset_basis)

if rtn <> 1 then
	//  Assume that messages were logged by uf_deriver().
	rollback;
	return -1
else
	f_pp_msgs(string(sqlca.SQLNRows) + " rows inserted.")
end if

//  Now update the cr_txn_type.  Just using brute force.
//
//  DMJ: 11/22/09: SETTING TO 'P' MAKES IT MUCH HARDER TO ENFORCE DEPENDENCIES.  REMOVING THIS
//  PART OF THE UPDATE.
//
if split_quantities = "CR Derivation Rollup" or split_quantities = "Split All" then
	//  The split function will end up handling the sign since it multiplies by the cr_derivation_rate.
//	sqls = &
//		"update " + i_stg_table + " set cr_txn_type = 'OFFSET', cr_derivation_status = 'P' " + &
//		 "where id > " + string(max_id)
	sqls = &
		"update " + i_stg_table + " set cr_txn_type = 'OFFSET', cr_derivation_status = NULL " + &
		 "where id > " + string(max_id)
else
	sqls = &
		"update " + i_stg_table + " set cr_txn_type = 'OFFSET', quantity = -quantity " + &
		 "where id > " + string(max_id) + &
		  " and budget_version = '" + i_bv + "'"
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("DERIVATIONS: OFFSETS ERROR: updating " + i_stg_table + ".cr_txn_type: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if


//  ----------------------------------  ROUNDING SECTION:  ----------------------------------


////  These updates were moved from below since the proper MN is needed for the
////  rounding function.
//f_pp_msgs("Performing other updates (month_number) at " + string(now()))
//
//sqls = "update " + i_stg_table + &
//			" set month_number = " + string(i_posting_mn) + ", month_period = 0 " + &
//		  "where " + i_company_field + " = '" + i_co + "'" + &
//	      " and budget_version = '" + i_bv + "'"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".month_number: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


counter = 0
select count(*) into :counter from all_tab_columns
 where table_name = upper(:i_stg_table) and column_name = 'CR_DERIVATION_TYPE';
if isnull(counter) then counter = 0

if counter > 0 then
	f_pp_msgs("Performing other updates (cr_derivation_type) at " + string(now()))
	
	sqls = "update " + i_stg_table + &
				" set cr_derivation_type = '" + i_type + "' where id*1 > " + string(orig_max_id) + &
		      " and budget_version = '" + i_bv + "'"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating " + i_stg_table + ".cr_derivation_type: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if


//  CUSTOM FUNCTION CALL:
//    Just after the derivations fire.  This call would be a good place to add logic
//    for "default" derivation types.  Base code for the defaults is not obvious since
//    some clients (like Pepco) had multiple default types (by retire_je_code).
rc = f_cr_batch_derivation_custom_bdg("just after derivations fire", 0, " ", i_stg_table)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if


//  CLEANUP:
//  Fix any rounding error.  Rounding must be called for each of the a_type/a_join
//  values passed to uf_deriver.
//
//  And example of the basis variable at this point would be:
//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup = cr_deriver_control.string
//  and crb_accounts_payable_stg.cr_txn_type = 'ORIGINAL' 
//  and crb_accounts_payable_stg.amount_type = 1 
//  and crb_accounts_payable_stg.company = 'CECO'
//
//  NOTE: The last arg (a_table_name_field) is assuming that the "join" field in the stg table
//  is the first part of the basis from cr_batch_derivation_control (so we can strip off
//  everything to the left of the "=" sign.  For example:
//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup
//
//  NOTE: balancing uses either the traditional technique or the id_balancing technique.
//  See comments in the UO function.
//

if id_balancing = 2 then goto after_rounding

f_pp_msgs("Performing rounding adjustment at " + string(now()))

basis_2nd_arg = f_replace_string(basis, "cr_txn_type = 'ORIGINAL'", "cr_txn_type = 'TARGET'", "all")

basis_6th_arg = left(basis, pos(basis, "=") - 1)

id_balancing_basis = "cr_derivation_type = '" + i_type + "' and cr_txn_type = 'TARGET' and " + i_company_field + " = '" + i_co + "'" + &
					     " and budget_version = '" + i_bv + "'"

if id_balancing = 1 then
	rtn = i_uo_cr_derivation.uf_deriver_rounding_error_ids( &
		i_type, i_stg_table, &
		basis         + " and " + i_stg_table + ".month_number = " + string(i_posting_mn) + &
						    " and budget_version = '" + i_bv + "'", &
		id_balancing_basis + " and " + i_stg_table + ".month_number = " + string(i_posting_mn) + &
							      " and budget_version = '" + i_bv + "'", &
		"cr_derivation_type = '" + i_type + "' and cr_txn_type = 'TARGET' and month_number = " + string(i_posting_mn) + &
													    " and budget_version = '" + i_bv + "'", &
		basis_6th_arg)
else
	rtn = i_uo_cr_derivation.uf_deriver_rounding_error( &
		i_type, i_stg_table, &
		basis         + " and " + i_stg_table + ".month_number = " + string(i_posting_mn) + &
						    " and budget_version = '" + i_bv + "'", &
		basis_2nd_arg + " and " + i_stg_table + ".month_number = " + string(i_posting_mn) + &
						    " and budget_version = '" + i_bv + "'", &
		i_stg_table + ".cr_txn_type = 'TARGET' and " + i_stg_table + ".month_number = " + string(i_posting_mn) + &
						    " and budget_version = '" + i_bv + "'", &
		basis_6th_arg)
end if

if rtn <> 1 then
	//  No msgs ... uf_deriver_rounding_error handles its own.
	rollback;
	return -1
end if

after_rounding:

//*****************************************************************************************
//
//	 COMMIT.
//
//    This is really the first place we could have a problem.  So I'm firing a commit
//    here to save the records in case we need to audit something.  They are about to
//    get committed anyways before validations run.  The beginning of the source loop
//    in uf_read truncates this table, so there should be no danger.
//
//*****************************************************************************************
commit;



//*******************************************************************************************
//
//  BALANCE:  Verify that the targets and offsets = the original amount.
//
//*******************************************************************************************

if id_balancing = 2 then goto after_balancing

f_pp_msgs("Balancing TARGET and OFFSET results against ORIGINAL transactions at " + string(now()))

// ### - SEB - 8791 - 100911: Need to include cr_derivation_type in the where clause
sqls = &
	"select sum(amount) from " + i_stg_table + &	
	" where cr_txn_type = 'ORIGINAL' " + &
	  " and id in (select cr_derivation_id from " + i_stg_table + " where cr_derivation_type = '" + i_type + "') " + &
	  " and " + i_company_field + " = '" + i_co + "'" + &
     " and budget_version = '" + i_bv + "'"
//	" where cr_txn_type = 'ORIGINAL' and id in (select cr_derivation_id from " + i_stg_table + ") " + &
//	  " and " + i_company_field + " = '" + i_co + "'" + &
//     " and budget_version = '" + i_bv + "'"
i_ds_sum_amount.SetSQLSelect(sqls)
i_ds_sum_amount.RETRIEVE()

if i_ds_sum_amount.RowCount() <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: i_ds_sum_amount.RETRIEVE() returned no records for ORIGINAL !")
	f_pp_msgs("*** sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	total_orig = i_ds_sum_amount.GetItemDecimal(1, 1)
end if

// ### - SEB - 8791 - 100911: Need to include cr_derivation_type in the where clause
sqls = "select sum(amount) from " + i_stg_table + " where cr_txn_type = 'TARGET' " + &
			" and " + i_company_field + " = '" + i_co + "'" + &
	      " and budget_version = '" + i_bv + "'" + &
			" and cr_derivation_type = '" + i_type + "' "
i_ds_sum_amount.SetSQLSelect(sqls)
i_ds_sum_amount.RETRIEVE()

if i_ds_sum_amount.RowCount() <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: i_ds_sum_amount.RETRIEVE() returned no records for TARGET !")
	f_pp_msgs("*** sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	total_targ = i_ds_sum_amount.GetItemDecimal(1, 1)
	if isnull(total_targ) then total_targ = 0
end if

// ### - SEB - 8791 - 100911: Need to include cr_derivation_type in the where clause
sqls = "select sum(amount) from " + i_stg_table + " where cr_txn_type = 'OFFSET' " + &
			" and " + i_company_field + " = '" + i_co + "'" + &
	      " and budget_version = '" + i_bv + "'" + &
			" and cr_derivation_type = '" + i_type + "' "
i_ds_sum_amount.SetSQLSelect(sqls)
i_ds_sum_amount.RETRIEVE()

if i_ds_sum_amount.RowCount() <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: i_ds_sum_amount.RETRIEVE() returned no records for OFFSET !")
	f_pp_msgs("*** sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	total_offs = -i_ds_sum_amount.GetItemDecimal(1, 1)
	if isnull(total_offs) then total_offs = 0
end if

if total_orig <> total_targ then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: The TARGET results do not equal the ORIGINAL transactions !")
	f_pp_msgs("  ")
	f_pp_msgs("TARGETS:  " + string(total_targ))
	f_pp_msgs("ORIGINAL: " + string(total_orig))
	f_pp_msgs("  ")
	//  Need to save the records or it may be impossible to determine the cause of
	//  the problem.  Cannot just commit since with suspense accounting, the stg tables
	//  are temp tables.  Create a table here, and the DBA will have to aid in retrieving
	//  the records.
	//
	//  Need to then delete the allocation results since they will be committed when the
	//  create table fires !!!
	tbl_id = 0
	select crmaint.nextval into :tbl_id from dual;
	if isnull(tbl_id) then tbl_id = 0
	tbl_sqls = "create table crbatderbdgoobt_" + string(tbl_id) + " as "  + &
					 "select * from " + i_stg_table // I'll just get everything for this run.
						 // It is a global temp, so we are isolated.
	execute immediate :tbl_sqls;
	rollback;
	return -1
end if

if total_orig <> total_offs then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: The OFFSET results do not equal the ORIGINAL transactions !")
	f_pp_msgs("  ")
	f_pp_msgs("OFFSETS:  " + string(total_offs))
	f_pp_msgs("ORIGINAL: " + string(total_orig))
	f_pp_msgs("  ")
	tbl_id = 0
	select crmaint.nextval into :tbl_id from dual;
	if isnull(tbl_id) then tbl_id = 0
	tbl_sqls = "create table crbatderbdgoobo_" + string(tbl_id) + " as "  + &
					 "select * from " + i_stg_table // I'll just get everything for this run.
						 // It is a global temp, so we are isolated.
	execute immediate :tbl_sqls;
	rollback;
	return -1
end if
after_balancing:


//*******************************************************************************************
//
//  GET RID OF THE ORIGINAL RECORDS:
//
//    Very important here since we may end up processing another derivation type in
//    the source_id loop and it will end up inserting its own 'ORIGINAL' records.
//
//*******************************************************************************************
f_pp_msgs("Deleting ORIGINAL transactions at " + string(now()))

sqls = "delete from " + i_stg_table + " where cr_txn_type = 'ORIGINAL'" + &
	      " and budget_version = '" + i_bv + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting ORIGINAL transactions from " + i_stg_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if



//*******************************************************************************************
//
//  Other Updates:
//
//    Like gl_journal_category
//
//*******************************************************************************************

//
//  DMJ: 12/16/09: UF_DERIVER_WITH_SRC_TABLE WAS ALREADY DOING THIS UPON INSERT.
//  DOING IT HERE REPLACES IDS THAT MAY HAVE BEEN LOGGED IN THE CR_DERIVATION_ID
//  FROM INSERT DERIVATIONS THAT FIRED ABOVE ... NOT TO MENTION THE EXTRA 
//  PERFORMANCE DRAG.
//  
//f_pp_msgs("Performing other updates (id) at " + string(now()))
//
//if upper(i_stg_table) = "CR_BUDGET_DATA" or upper(i_stg_table) = "CR_BUDGET_DATA_TEST" or &
//	upper(i_stg_table) = "CR_BUDGET_REVERSALS" or upper(i_stg_table) = "CRB_BUDGET_DATA" &
//then
//	sqls = "update " + i_stg_table + " set id = crbudgets.nextval where id*1 > " + string(orig_max_id) + &
//				" and budget_version = '" + i_bv + "'"
//else
//	sqls = "update " + i_stg_table + " set id = crdetail.nextval where id*1 > " + string(orig_max_id) + &
//				" and budget_version = '" + i_bv + "'"
//end if
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".id: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if


// -------------------------------------------------------------------------------------
// DMJ PERF: COMMENTED OUT TO AVOID SOME UPDATES:  CODE ADDED TO UO_CR_DERIVATION
// -------------------------------------------------------------------------------------
//f_pp_msgs("Performing other updates (gl_journal_category) at " + string(now()))
//
//sqls = "update " + i_stg_table + " set gl_journal_category = '" + gl_jcat + "' where id*1 > " + string(orig_max_id) + &
//	      " and budget_version = '" + i_bv + "'"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".gl_journal_category: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if
// -------------------------------------------------------------------------------------


//f_pp_msgs("Performing other updates (dr_cr_id) at " + string(now()))
//
//sqls = "update " + i_stg_table + &
//			" set dr_cr_id = decode(sign(amount), -1, -1, 1) where id*1 > " + string(orig_max_id) + &
//	      " and budget_version = '" + i_bv + "'"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: updating " + i_stg_table + ".dr_cr_id: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if



//
//  "UPDATE" DERIVATIONS WOULD GO HERE.  E.G. WHAT IF THEY "SPLIT" SOME THINGS WITH THE
//  INSERT DERIVATIONS THAT POSTED TO OTHER DERIVATION KEYS?  WE WOULD NEED TO LOOP OVER
//  CR_DERIVER_TYPE_SOURCES FOR THIS SOURCE_ID AND FIRE THE UPDATE DERIVATIONS.  I SUSPECT
//  I WILL NEED A FLAG ON CR_BATCH_DERIVATION_CONTROL TO TURN THIS ON OR OFF, IN CASE
//  DIFFERENT CLIENTS DO/DON'T WANT TO FIRE THESE.
//

//*****************************************************************************************
//
//  UPDATE DERIVATIONS.
//
//    -4 is the source_id in cr_deriver_type_sources.  We did a dedicate source_id
//    instead of the source_id values for each detail table because of the conflict
//    between interfaces and ADJ JE's.  They both hit the same detail tables, but may
//    need to be configured differently.  However, it is unlikely that you'd want them
//    to be configured differently for this process.  Thus, this process gets its own
//    source_id and its own derivation type config.
//
//    BUDGET NOTE:  using source_id -44 for budget
//   
//*****************************************************************************************
if update_derivations = 1 then
	//  Allow them to fire.
	no_update_derivations = false
else
	no_update_derivations = true
	goto after_update_derivations
end if

//  WILL BE TOO SLOW FOR BUDGET ... ADDED AN ANALYZE BEFORE THE MONTH LOOP IN UF_READ INSTEAD.
//f_pp_msgs("Performing update derivations: Analyzing table " + i_stg_table + " at: " + string(now()))
//sqlca.analyze_table(i_stg_table)  //  s/b/ fine since we've already committed anyways


f_pp_msgs("Performing update derivations at " + string(now()))

sqls = &
	"select * from cr_deriver_type_sources " + &
	 "where source_id = -3 and upper(trim(insert_or_update))  ='UPDATE'"
i_cr_deriver_type_sources.SetSQLSelect(sqls)
i_cr_deriver_type_sources.RETRIEVE()
i_cr_deriver_type_sources.SetSort("run_order a")
i_cr_deriver_type_sources.Sort()
num_types = i_cr_deriver_type_sources.RowCount()

if num_types <= 0 then
	if g_debug = "YES" then f_pp_msgs("DERIVATIONS: No derivations defined for: " + i_stg_table)
end if

//  Derivations need a real interface_batch_id.  Use -1 and null it out later.
sqls = "update " + i_stg_table + " set interface_batch_id = '-1'" + &
	      " where budget_version = '" + i_bv + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ".interface_batch_id to -1: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

for t = 1 to num_types
	
	the_type            = trim(i_cr_deriver_type_sources.GetItemString(t, "type"))
	update_basis        = trim(i_cr_deriver_type_sources.GetItemString(t, "basis"))
	delimiter           = i_cr_deriver_type_sources.GetItemString(t, "delimiter")
	
	if isnull(the_type)  or the_type = "" then continue // useless record
	if isnull(basis)     or basis    = "" then continue // useless record
	if isnull(delimiter)                  then delimiter    = ""
	
	f_pp_msgs("Performing update derivations: " + the_type + " at " + string(now()))
	
	if g_debug = "YES" then
		f_pp_msgs("type = " + the_type)
		f_pp_msgs("basis = " + basis)
		f_pp_msgs("delimiter = " + delimiter)
	end if
		////201001CJR:  If you have multiple update derivations and the first one has 3 fields in the join fields and the second one has 2 fields 
	///the thrid field from the first derivation type will still be in the array so we need to reset the array each time.
	join_fields = null_str_array
	//  Parse the basis value (e.g. "company,work_order") into an array for the update function.
	f_parsestringintostringarray(update_basis, ",", join_fields)
	
	rtn = i_uo_cr_derivation.uf_deriver_update_mult( &
		the_type, &
		i_stg_table, &
		join_fields, &
		"-1", &
		false, &
		delimiter)
	
	if rtn <> 1 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: in uf_deriver_update_mult(): see message above")
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
next



//update cr_payroll_stg set orig_activity = activity, orig_ewo = ewo, orig_project = project,
//			orig_location = location, orig_ferc_sub = ferc_sub, orig_rorg = rorg, orig_ai = ai
//where maximo_wo <> :mwo_default and maximo_wo <> ' ';
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Updating cr_payroll_stg orig fields to MWO derivation values " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if

after_update_derivations:



//*******************************************************************************************
//
//  Derivation Overrides:  #2 table
//
//    Re-using variables in this section from above.
//
//  *****  PLACEHOLDER FOR FUTURE CODE  *****
//
//*******************************************************************************************





//*******************************************************************************************
//
//  Split Quantities:  
//
//    While we're here with a filled in ifb_id, might as well split the quantities.
//
//*******************************************************************************************
if split_quantities = "CR Derivation Rollup" then
	
	if no_update_derivations then
		
		f_pp_msgs("Updating interface_batch_id to -1 at " + string(now()))
		
		//  Derivations need a real interface_batch_id.  Use -1 and null it out later.
		sqls = "update " + i_stg_table + " set interface_batch_id = '-1'" + &
					" where budget_version = '" + i_bv + "'"
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating " + i_stg_table + ".interface_batch_id to -1: " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("sqls = " + sqls)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	end if
	
	//  Do I need any debug messages ?
	
	f_pp_msgs("Splitting quantities at " + string(now()))
	
	rtn = i_uo_cr_derivation.uf_split_quantities( &
		i_stg_table, &
		"-1")
	
	if rtn <> 1 then
		//  Assume that messages were logged by uf_deriver().
		rollback;
		return -1
	else
		f_pp_msgs(string(sqlca.SQLNRows) + " rows inserted.")
	end if
	
end if

if split_quantities = "Split All" then
	
	if no_update_derivations then
		
		f_pp_msgs("Updating interface_batch_id to -1 at " + string(now()))
		
		//  Derivations need a real interface_batch_id.  Use -1 and null it out later.
		sqls = "update " + i_stg_table + " set interface_batch_id = '-1'" + &
					" where budget_version = '" + i_bv + "'"
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating " + i_stg_table + ".interface_batch_id to -1: " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("sqls = " + sqls)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
	end if
	
	//  Do I need any debug messages ?
	
	f_pp_msgs("Splitting quantities at " + string(now()))
		
	i_uo_cr_derivation.i_split_quantities_all_wc = " and id > " + string(orig_max_id) + " " + &
														        " and budget_version = '" + i_bv + "'"
	
	rtn = i_uo_cr_derivation.uf_split_quantities_all( &
		i_stg_table, &
		"-1")
	
	if rtn <> 1 then
		//  Assume that messages were logged by uf_deriver().
		rollback;
		return -1
	else
		f_pp_msgs(string(sqlca.SQLNRows) + " rows updated.")
	end if
	
end if



//*****************************************************************************************
//
//  CUSTOM FUNCTION CALL:
//    Right after we've inserted into the "crb" staging table.  You might put code here
//    like that in the SAP transaction interfaces where we:  perform an initial ferc
//    account update, audit retire_je_code values against CDC, etc.
//
//*****************************************************************************************
rc = f_cr_batch_derivation_custom_bdg("split quantities", 0, " ", i_stg_table)

if rc = "ERROR" then
	//  Put the messages in the custom function.
	rollback;
	return -1
end if


//*******************************************************************************************
//
//  Derivations need a real interface_batch_id.  Change it back so nothing else
//  has a problem.
//
//*******************************************************************************************
sqls = "update " + i_stg_table + " set interface_batch_id = NULL" + &
       " where budget_version = '" + i_bv + "'"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating " + i_stg_table + ".interface_batch_id to NULL: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

if g_debug = "YES" then
	f_pp_msgs("Update derivations complete at: " + string(now()))
end if








//
//  PLACEHOLDER FOR FUTURE CODE ... WE CERTAINLY COULD HIT AN INTERCOMPANY SCENARIO.
//
//////*****************************************************************************************
//////
//////	 PERFORM INTERCOMPANY BALANCING:  Before the validations.  Must loop over the months
//////                                   since interco balancing is by month.
//////
//////  Note:  the month_period is unique for each month_number.
//////
//////*****************************************************************************************
////f_pp_msgs("Performing intercompany balancing at " + string(now()))
////
////uo_cr_interco_balancing uo_cr_interco_balancing
////uo_cr_interco_balancing = CREATE uo_cr_interco_balancing
////
////datastore ds_months
////ds_months = CREATE datastore
////sqls = "select distinct month_number, month_period from cr_gas_master_stg"
////f_create_dynamic_ds(ds_months, "grid", sqls, sqlca, true)
////num_months = ds_months.RowCount()
////
////for i = 1 to num_months
////	f_pp_msgs("-- Month " + string(i) + " of " + string(num_months))
////	mn = ds_months.GetItemNumber(i, 1)
////	mp = ds_months.GetItemNumber(i, 2)
////	
////	rtn = uo_cr_interco_balancing.uf_interco_balancing_x_charge("cr_gas_master_stg", &
////		"interface_batch_id", g_batch_id, mn, mp)
////	if rtn <> 1 then
////		f_pp_msgs("  ")
////		f_pp_msgs("(AP) ERROR: in uf_interco_balancing_x_charge: rtn = " + string(rtn))
////		f_pp_msgs("  ")
////		rollback;
////		return -1
////	end if
////	
////	rtn = uo_cr_interco_balancing.uf_interco_balancing("cr_gas_master_stg", &
////		"interface_batch_id", g_batch_id, mn, mp)
////	if rtn <> 1 then
////		f_pp_msgs("  ")
////		f_pp_msgs("(GM) ERROR: in uf_interco_balancing: rtn = " + string(rtn))
////		f_pp_msgs("  ")
////		rollback;
////		return -1
////	end if
////next
////
////DESTROY ds_months
////DESTROY uo_cr_interco_balancing



//  NOT FOR BUDGET --- BUDGET DOES NOT NEED TO BALANCE BY COMPANY
////*****************************************************************************************
////
////	 ENSURE THAT THE BATCH BALANCES BY COMPANY.
////
////    This is really the first place we could have a problem.  So I'm firing a commit
////    here to save the records in case we need to audit something.  They are about to
////    get committed anyways before validations run.  The beginning of the source loop
////    in uf_read truncates this table, so there should be no danger.
////
////*****************************************************************************************
//counter = 0
//sqls = &
//	"select count(*) from (" + &
//		"select " + i_company_field + ", gl_journal_category, amount_type, month_number, sum(amount) " + &
//		  "from " + i_stg_table + " " + &
//	   " where budget_version = '" + i_bv + "' " + &
//		 "group by " + i_company_field + ", gl_journal_category, amount_type, month_number having sum(amount) <> 0)"
//i_ds_counter.SetSQLSelect(sqls)
//i_ds_counter.RETRIEVE()
//if i_ds_counter.RowCount() > 0 then
//	counter = i_ds_counter.GetItemNumber(1, 1)
//end if
//if isnull(counter) then counter = 0
//
//if counter > 0 then
//	//  OOB !
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: " + i_stg_table + " is out of balance by: " + i_company_field + &
//		", gl_journal_category, amount_type, and month_number !")
//	f_pp_msgs("  ")
//	rollback;
//	return -1
//end if



//
//  PLACEHOLDER FOR FUTURE CODE ... IN THE EVENT OF INTERCOMPANY AND/OR SUSPENSE.
//
//////*****************************************************************************************
//////
//////	 DERIVE SOME VALUES ON INTERCOMPANY BALANCING TRANSACTIONS:
//////
//////  The "orig" fields and the je_nbr are not right at this point.  Update them here.
//////  DO NOT RE-UPDATE THE ORIGINAL TRANSACTIONS !!!  IF THERE WERE VALIDATION KICKOUTS AND
//////  THE INTERFACE RE-RUNS, WE WILL LOSE THE ORIGINAL VALUES FROM THE FEEDER.
//////
//////  It ought to be sufficient to check for NULLS in the orig_business_unit field since
//////  the intercomany txns should be the only ones that have that condition.  That way I
//////  don't need a zillion SQL commands.
//////
//////*****************************************************************************************
////f_pp_msgs("Updating intercompany transaction attributes at " + string(now()))
////
////update cr_payroll_stg
////	set orig_business_unit = gl_business_unit, orig_account = account,
////		 orig_location = gl_location, orig_project = project, orig_work_order = gl_work_order,
////		 orig_cost_type = cost_type, orig_home_center = home_center,
////		 orig_prog_actv = ' ', orig_cust_serv = ' ', je_nbr = substr(gl_journal_category,3,3),
////		 adj_period = decode(substr(month_number,5,2), '00', '901', '13', '998', '14', '999', ' '),
////		 drilldown_key = 'INTERCO', orig_company = company, orig_task = ' ', orig_affiliate = ' ',
////		 orig_program_code = ' ', orig_chartfield_3 = ' '
//// where ...;
////
////if sqlca.SQLCode < 0 then
////	f_pp_msgs("  ")
////	f_pp_msgs("ERROR: Updating intercompany transaction attributes: " + &
////		sqlca.SQLErrText)
////	f_pp_msgs("  ")
////	rollback;
////	return -1
////end if



//
//  PLACEHOLDER FOR FUTURE FP DERIVATIONS ... NOT NEEDED INITIALLY AT SOUTHERN
//
//////*****************************************************************************************
//////
//////	 DERIVE THE FUNDING PROJECT.  I AM DOING THIS BEFORE VALIDATIONS, SO ANY PROJECTS THAT
//////  ARE LEFT AS BLANK WILL FAIL THE VALIDATIONS.
//////
//////  I PLACED THIS AFTER THE SKIP_INSERT, SO PRIOR VALIDATION KICKOUTS ON PROJECT CAN BE
//////  CLEANED UP BY CHANGING VALUES IN THE WORK ORDER TABLES (POTENTIALLY).
//////
//////  SINCE THERE IS A VALIDATION CHECK AT THE TOP FOR PROJECTS = ' ', I MUST SET ALL 
//////  PROJECTS TO ' ' B4 THE DERIVATION.
//////
//////	 DERIVE GL_LOC FROM THE CLASS CODE OF THE FUNDING PROJECT.  CANNOT USE THE LEFT 3
//////  CHARACTERS INTO THE FUTURE.
//////
//////  THIS IS AFTER THE I/U CODE TO ENSURE THAT THOSE RECORDS GET THE RIGHT PROJECT.
//////
//////*****************************************************************************************
////f_pp_msgs("Updating cr_gas_master_stg.project to ' ' at " + string(now()))
////
////update cr_gas_master_stg a set project = ' '
//// where gl_work_order <> ' ';
////
////if sqlca.sqlcode < 0 then
////	f_pp_msgs("  ")
////	f_pp_msgs("(GM) ERROR: Updating cr_gas_master_stg.project to ' ': " + &
////		sqlca.sqlerrtext)
////	f_pp_msgs("  ")
////	rollback;
////	return -1
////else 
////	//	No commit...
////end if
////
////f_pp_msgs("Updating cr_gas_master_stg.project at " + string(now()))
////
////update cr_gas_master_stg a
////set project = 			
////	(select fp.work_order_number 
////	   from work_order_control wo, company_setup co, work_order_control fp
////	  where a.gl_work_order = wo.work_order_number 
////	  	 and a.company = co.gl_company_no
////		 and wo.company_id = co.company_id
////		 and wo.funding_wo_id = fp.work_order_id
////		 and wo.funding_wo_indicator = 0
////		 and fp.funding_wo_indicator = 1
////	)	  
////where (a.gl_work_order, a.company) in 
////		(select x.work_order_number, y.gl_company_no 
////		   from work_order_control x, company_setup y
////		  where x.company_id = y.company_id
////		  and x.funding_wo_indicator = 0)
////and interface_batch_id = :g_batch_id
////;
////
////if sqlca.sqlcode < 0 then
////	f_pp_msgs("  ")
////	f_pp_msgs("(GM) ERROR: Backfilling cr_gas_master_stg.project : " + &
////		sqlca.sqlerrtext)
////	f_pp_msgs("  ")
////	rollback;
////	return -1
////else 
////	//	No commit...
////end if
////
////if update_orig then
////	f_pp_msgs("Updating cr_gas_master_stg.orig_project at " + string(now()))
////	
////	update cr_gas_master_stg a
////		set orig_project = project, orig_work_order = gl_work_order
////	 where interface_batch_id = :g_batch_id;
////	
////	if sqlca.sqlcode < 0 then
////		f_pp_msgs("  ")
////		f_pp_msgs("(JL) ERROR: Backfilling cr_gas_master_stg.project : " + &
////			sqlca.sqlerrtext)
////		f_pp_msgs("  ")
////		rollback;
////		return -1
////	else 
////		//	No commit...
////	end if
////end if



return 1

end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_batch_derivation_bdg_custom_version nvo_cr_batch_derivation_bdg_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_batch_derivation_bdg_custom.pbd'
		return nvo_cr_batch_derivation_bdg_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

