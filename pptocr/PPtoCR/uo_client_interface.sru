HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string	i_exe_name = 'pptocr.exe'

string i_table_name, i_staging_table_name
longlong i_source_id
uo_cr_validation i_uo_cr_validations
end variables

forward prototypes
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//*****************************************************************************************
//
//  Object      :  uo_cr_powerplant
//  UO Function :  uf_read
//
//	 Returns     :  1 if successful
// 					-1 otherwise
//
//*****************************************************************************************
string validate_rtn, source_description, sqls, elements[], defaults[], column_name, column_sql, cv_validations, to_user, &
	cv_memo_val, company_value, company_field, allow_splitting, successful_batches[], gljc_in_gl_account, omit_company, &
	reimb
decimal  {2} amount_stag, amount_cr
longlong rtn, overall_rtn, comp, interface_id, num_elements, starting_pos[], da[], i, widths[], num_rows, count_stag, count_cr, &
	lvalidate_rtn
any results[]
uo_ds_top ds_results, ds_cr_columns, ds_balancing
boolean validation_kickouts, prior_kickouts, gl_trans_to_process, reimb_gl_trans_to_process

//*****************************************************************************************
//
//  Command Line parameters will contain a company id if they only want to run for one company
//
//*****************************************************************************************
// ### - SEK - 7078 - 031711: Need to trim g_command_line_args
g_command_line_args = trim(g_command_line_args)

if not isnull(g_command_line_args) and trim(g_command_line_args) <> '' then f_pp_msgs("Parms : " + g_command_line_args)
if isnumber(g_command_line_args) then 
	
	comp  = long(g_command_line_args)
	
	select gl_company_no
		into :company_value
		from company_setup
		where company_id = :comp;
		
	if trim(company_value) = '' or isnull(company_value) then
		f_pp_msgs("ERROR: Getting company value from Company Setup!")
		rollback;
		overall_rtn = -1
		goto halt_the_app
	end if
else
	setnull(comp)
end if

//*****************************************************************************************
//
//  Setup:
//
//*****************************************************************************************
// **********************
// Init variables
// **********************
prior_kickouts = false
overall_rtn = 1

// **********************
// Rollback segment
// **********************
f_set_large_rollback_segment()

// **********************
// Source
// **********************
// ### JAK: Maint 5553: 10/5/2010: CR System Switch for which CR Source to write transactions
select control_value 
	into :i_source_id
	from cr_system_control
	where upper(trim(control_name)) = 'PPTOCR SOURCE ID';
	
if SQLCA.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR:  Getting Source ID from CR System Control: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	rollback;
	overall_rtn = -1
	goto halt_the_app
end if 

// If we don't find a valid source, default to POWERPLANT
if isnull(i_source_id) or i_source_id = 0 then
	select source_id
		into :i_source_id
		from cr_sources
		where upper(trim(description)) = 'POWERPLANT';
end if

select upper(trim(description)), lower(table_name), lower(table_name) || '_stg' 
	into :source_description, :i_table_name, :i_staging_table_name 
	from cr_sources 
	where source_id = :i_source_id;

if SQLCA.sqlcode <> 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR:  Getting table names: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	rollback;
	overall_rtn = -1
	goto halt_the_app
end if 

if trim(i_table_name) = '' or isnull(i_table_name) then
	f_pp_msgs("ERROR: CR Source for PPtoCR not found!  Please verify that the CR System Control 'PPtoCR Source ID' points to a valid CR Source.")
	rollback;
	overall_rtn = -1
	goto halt_the_app
end if

// **********************
// GL JC Mapping
// ### JAK: Maint 5590: 10/25/2010: CR System Switch for where to pull GL Journal Category
// **********************
select upper(trim(control_value)) 
	into :gljc_in_gl_account
	from cr_system_control where upper(control_name) = 'PPTOCR: GLJC IN GL ACCOUNT';
if isnull(gljc_in_gl_account) or (gljc_in_gl_account <> 'YES' and gljc_in_gl_account <> 'NO') then gljc_in_gl_account = 'NO'



// **********************
// REIMB TRANSACTIONS
// ### 7660: JAK: 2011-06-16: Option for PPtoCR to also read transactions from REIMB_GL_TRANSACTION
// **********************
select upper(trim(control_value))
	into :reimb
	from cr_system_control
	where upper(trim(control_name)) = 'PPTOCR: PROCESS REIMB';
if isnull(reimb) or reimb <> 'YES' then reimb = 'NO'


// **********************
// Interface ID
// **********************
select interface_id into :interface_id
  from cr_interfaces
 where upper(trim(source)) = :source_description;
if isnull(interface_id) then interface_id = 0
 
if interface_id = 0 then
	f_pp_msgs(" ")
	f_pp_msgs("No entry exists in cr_interfaces for '" + source_description + "' ... Cannot run the PPtoCR Interface!")
	f_pp_msgs(" ")
	overall_rtn = -1
	goto halt_the_app
end if


// **********************
// Allow splitting
// **********************
select upper(trim(control_value)) 
	into :allow_splitting
	from cr_system_control
	where upper(control_name) = 'PPTOCR ALLOW BATCH SPLITTING';
if isnull(allow_splitting) or (allow_splitting <> 'YES' and allow_splitting <> 'NO') then allow_splitting = 'NO'


// **********************
// Omit Company
// ### JAK : 5763 : Omit company from balancing call
// **********************
select upper(trim(control_value)) 
	into :omit_company
	from cr_system_control
	where upper(control_name) = 'OMIT COMPANY FROM SEND BALANCING';
if isnull(omit_company) or (omit_company <> 'YES' and omit_company <> 'NO') then omit_company = 'NO'


// **********************
// Company field 
// **********************
select upper(trim(control_value)) 
	into :company_field
	from cr_system_control where upper(control_name) = 'COMPANY FIELD';

if trim(company_field) = '' or isnull(company_field) then
	f_pp_msgs("ERROR: Getting company field from CR System Control!")
	rollback;
	overall_rtn = -1
	goto halt_the_app
else
	company_field = f_cr_clean_string(company_field)
end if 

// **********************
//  Elements of the accounting key
// **********************
datastore ds_elements
ds_elements = CREATE datastore
sqls = 'select * from cr_elements order by "ORDER"'
f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)
num_elements = ds_elements.RowCount()

starting_pos[1] = 1
for i = 1 to num_elements
	elements[i] = f_cr_clean_string(lower(ds_elements.GetItemString(i, "description")))
	widths[i] = ds_elements.GetItemNumber(i, "width")
	defaults[i] = ds_elements.GetItemString(i, "default_value")
	if defaults[i] = '' or isnull(defaults[i]) then defaults[i] = ' '
	starting_pos[i+1] =  starting_pos[i] + widths[i] + 1
next


//*****************************************************************************************
//
//	Call the Custom Function Before Starting any processing
//
//*****************************************************************************************
prior_kickouts_restart:
rtn = f_pptocr_custom(1)
if rtn < 0 then
	rollback;
	overall_rtn = -1
	goto halt_the_app
end if


//*****************************************************************************************
//
//	 If this interface ran previously but stopped b/c of validation kickouts, I don't want 
//  to get any new data until the previous run has been loaded.
//
//*****************************************************************************************
if allow_splitting = 'YES' then
else
	f_pp_msgs("Checking cr_validations_invalid_ids for prior validation kickouts at " + string(now()))
	
	num_rows = 0
	select count(*) into :num_rows
	from cr_validations_invalid_ids
	where lower(table_name) = :i_staging_table_name;
	
	if num_rows > 0 then
		//  THERE WERE VALIDAITON KICKOUTS IN THE PREVIOUS RUN.
		f_pp_msgs("")
		f_pp_msgs("There are oustanding records from a previous run.  The interface will finish processing the existing records before new data is loaded.")
		f_pp_msgs("")
		
		//	Get the batch_id from the previous run to pass to validations
		sqls = "select min(abs(interface_batch_id)) from " + i_staging_table_name
		rtn = f_get_column(results[],sqls)
		
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Could not get the batch_id from the staging table: " + sqlca.sqlerrtext)
			f_pp_msgs("SQL: " + sqls)
			f_pp_msgs("  ")
			rtn = -1
			goto halt_the_app
		else
			g_batch_id = string(results[1])
		end if
		
		prior_kickouts = true
		goto skip_insert
	else	
		//  THERE WERE NO CR VALIDATION KICKOUTS IN THE PREVIOUS RUN.  HOWEVER ...
		//   If there are records in staging table, use that batch_id.
		//  These would be due to other errors in the interface.  

		sqls = "select count(*) from " + i_staging_table_name
		rtn = f_get_column(results[],sqls)
		
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Checking record count from staging table: " + sqlca.sqlerrtext)
			f_pp_msgs("SQL: " + sqls)
			f_pp_msgs("  ")
			rtn = -1
			goto halt_the_app
		end if
		
		if results[1] > 0 then
			//	Get the batch_id from the previous run to pass to validations
			sqls = "select min(abs(interface_batch_id)) from " + i_staging_table_name
			rtn = f_get_column(results[],sqls)
			
			if rtn <> 1 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: Could not get the batch_id from the staging table (2): " + sqlca.sqlerrtext)
				f_pp_msgs("SQL: " + sqls)
				f_pp_msgs("  ")
				rtn = -1
				goto halt_the_app
			else
				g_batch_id = string(results[1])
			end if
			
			//  THERE WERE VALIDAITON KICKOUTS IN THE PREVIOUS RUN.
			f_pp_msgs("")
			f_pp_msgs("There are oustanding records from a previous run.  The interface will finish processing the existing records before new data is loaded.")
			f_pp_msgs("")
			
			prior_kickouts = true
			goto skip_insert			
		end if
		
	end if
end if


//*****************************************************************************************
//
//  MARK THE RECORDS TO BE READ.
//
//*****************************************************************************************
select count(*) into :num_rows
	from pp_system_control_company a 
	where upper(trim(control_name)) = 'PP TO CR - LOAD PENDING TRANS'
	and upper(trim(control_value)) = 'YES';
	
if num_rows > 0 then
	f_pp_msgs("Updating gl_transaction.gl_status_id from Pending (1) to Approved (2) at " + string(now()))
	sqls = "update gl_transaction set gl_status_id = 2 where gl_status_id = 1 and trim(company_number) in " + &
		"(select gl_company_no  " + &
		"from company_setup cs " + &
		"where upper( " + &
		"	nvl((	select control_value from pp_system_control_company a  " + &
		"			where upper(trim(control_name)) = 'PP TO CR - LOAD PENDING TRANS'  " + &
		"			and a.company_id = cs.company_id), " + &
		"			nvl((	select control_value from pp_system_control_company a  " + &
		"					where upper(trim(control_name)) = 'PP TO CR - LOAD PENDING TRANS'  " + &
		"					and a.company_id = -1),'yes'))) = 'YES') "
	if not isnull(comp) then 
		// Only select company
		sqls = sqls + " and trim(company_number) = '" + company_value + "'"
	end if
	
	execute immediate :sqls;
		
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating gl_transaction.gl_status_id from Pending to Approved: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
	
	// ### 7660: JAK: 2011-06-15:  Process REIMB transactions as well.
	if reimb = 'YES' then
		f_pp_msgs("Updating reimb_gl_transaction.gl_status_id from Pending (1) to Approved (2) at " + string(now()))
		sqls = f_replace_string(sqls,'gl_transaction','reimb_gl_transaction','all')
		execute immediate :sqls;
			
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating reimb_gl_transaction.gl_status_id from Pending to Approved: " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
	end if
end if

f_pp_msgs("Updating gl_transaction.gl_status_id from Approved (2) to In Process (14) at " + string(now()))
sqls = "update gl_transaction set gl_status_id = 14 where gl_status_id = 2 and trim(gl_account) not like '%ERROR%'"
if not isnull(comp) then 
	// Only select company
	sqls = sqls + " and trim(company_number) = '" + company_value + "'"
end if

execute immediate :sqls;
	
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating gl_transaction.gl_status_id from Approved to In Process: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

// ### 7660: JAK: 2011-06-15:  Process REIMB transactions as well.
if reimb = 'YES' then
	f_pp_msgs("Updating reimb_gl_transaction.gl_status_id from Approved (2) to In Process (14) at " + string(now()))
	sqls = f_replace_string(sqls,'gl_transaction','reimb_gl_transaction','all')
	execute immediate :sqls;
		
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating reimb_gl_transaction.gl_status_id from Approved to In Process: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if


//*****************************************************************************************
//
//  Update gl_transaction where gl_account = 'IGNORE'
//		- Moved the IGNORE logic below the updates to 14 above.  It was possible before to post entries into
//			GL Transaction between those 2 statements which resulted in the IGNORE entries ending up in the CR.
//
//*****************************************************************************************
f_pp_msgs("Updating gl_transaction where gl_account = 'IGNORE' at " + string(now()))
update gl_transaction set gl_status_id = 3
	where trim(upper(gl_account)) like '%IGNORE%' and gl_status_id = 14;
	
if sqlca.sqlcode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating gl_transaction where gl_account = 'IGNORE': " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	return -1
end if 

// ### 7660: JAK: 2011-06-15:  Process REIMB transactions as well.
if reimb = 'YES' then
	f_pp_msgs("Updating reimb_gl_transaction where gl_account = 'IGNORE' at " + string(now()))
	update reimb_gl_transaction set gl_status_id = 3
		where trim(upper(gl_account)) like '%IGNORE%' and gl_status_id = 14;
		
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating reimb_gl_transaction where gl_account = 'IGNORE': " + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if 
end if


//*****************************************************************************************
//
//  TELL THEM IF THERE IS NOTHING TO DO.
//
//*****************************************************************************************
// ### 8673: JAK: 2011-09-27:  fix the looping that can occur due to the restart.
gl_trans_to_process = false
reimb_gl_trans_to_process = false

num_rows = 0
select count(*) into :num_rows from gl_transaction 
	where gl_status_id = 14;
if isnull(num_rows) then num_rows = 0
if num_rows > 0 then gl_trans_to_process = true

 // ### 7660: JAK: 2011-06-15:  Process REIMB transactions as well.
if reimb = 'YES' then
	num_rows = 0
	select count(*) + :num_rows into :num_rows from reimb_gl_transaction 
		where gl_status_id = 14;
if isnull(num_rows) then num_rows = 0
if num_rows > 0 then reimb_gl_trans_to_process = true
end if
	

if not gl_trans_to_process and not reimb_gl_trans_to_process and allow_splitting = 'NO' then
	f_pp_msgs("  ")
	f_pp_msgs("There are no GL Transaction entries waiting to be posted to the CR.")
	f_pp_msgs("  ")
	rollback;
	goto halt_the_app
elseif not gl_trans_to_process and not reimb_gl_trans_to_process and allow_splitting = 'YES' then
	// Need to make sure there aren't records in the staging table already...
	sqls = "select count(*) from " + i_staging_table_name
	rtn = f_get_column(results[],sqls)
	
	if rtn <> 1 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Checking record count from staging table: " + sqlca.sqlerrtext)
		f_pp_msgs("SQL: " + sqls)
		f_pp_msgs("  ")
		rtn = -1
		goto halt_the_app
	end if
	
	if results[1] = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("There are no GL Transaction entries waiting to be posted to the CR.")
		f_pp_msgs("  ")
		rollback;
		goto halt_the_app
	end if
end if


//*****************************************************************************************
//
//  GET AN INTERFACE_BATCH_ID:  Getting it here instead of above prevents
//  us from burning sequence values if there are no records to post.
//
//*****************************************************************************************
if allow_splitting = 'YES' then
	g_batch_id = 'NEW'
else
	select costrepository.nextval into :g_batch_id from dual;
	if isnull(g_batch_id) then g_batch_id = '0'
	
	if g_batch_id = '0' then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Could not get the batch_id from costrepository.nextval")
		f_pp_msgs("  ")
		rtn = -1
		goto halt_the_app
	end if
end if

//*****************************************************************************************
//
//  Check for optional detailed attributes
//
//*****************************************************************************************
ds_cr_columns = create uo_ds_top 
// ### 7660: JAK: 2011-06-15:  Process REIMB transactions as well.
sqls = "select cr_column_name, nvl(sql_to_populate,'null') sql_to_populate, nvl(reimb_sql_to_populate,'null') reimb_sql_to_populate  from cr_pptocr_da_mapping"
f_create_dynamic_ds(ds_cr_columns,'grid',sqls,sqlca,true)

num_rows = ds_cr_columns.rowcount()

 for i = 1 to num_rows
	column_name = ds_cr_columns.GetItemString(i,1)
	select count(*) into :da[i]
		from all_tab_columns
		where table_name = upper(:i_staging_table_name) and column_name = upper(:column_name);
	
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Doing field lookup in all_tab_columns: " + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if 
next

if gl_trans_to_process then 
	//*****************************************************************************************
	//
	//  Build the insert into the staging table and do the insert
	//
	//*****************************************************************************************
	f_pp_msgs("Insert from gl_transaction to " + i_staging_table_name + " at " + string(now()))
	
	sqls = 'insert into ' + i_staging_table_name + ' ("ID'
		
	for i = 1 to num_elements
		sqls = sqls + '","' + upper(elements[i])
	next
	
	sqls = sqls + &
		'","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
		 ',"MONTH_NUMBER","MONTH_PERIOD"' + &
		 ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE","INTERFACE_BATCH_ID"'
	
	for i = 1 to num_rows
		if da[i] > 0 then 
			column_name = ds_cr_columns.getitemstring(i,'CR_COLUMN_NAME')
			sqls = sqls + ',"' + upper(column_name) + '"'
		end if
	next
	
	
	sqls = sqls + ') select crdetail.nextval,'
	
	 for i = 1 to num_elements
		sqls = sqls + 'nvl(trim(substr(gl_account,' + string(starting_pos[i]) + ',' + string(widths[i]) + ")),'" + defaults[i] + "'),"
	next
	
	sqls = sqls + 'decode(sign(decode(debit_credit_indicator,0,-amount,amount)),-1,-1,1),'		// DR_CR_ID
	sqls = sqls + '1,'		// LEDGER_SIGN
	sqls = sqls + '0,'		// QUANTITY
	sqls = sqls + 'decode(debit_credit_indicator,0,-amount,amount),'										// AMOUNT
	sqls = sqls + "to_char(month,'yyyymm')," 																	// MONTH_NUMBER
	sqls = sqls + '0,'		// MONTH_PERIOD
	if gljc_in_gl_account = 'YES' then
		// ### 7701: JAK: 2011-05-31: starting_pos[upperbound(starting_pos)] already has the right starting positions with widths and spaces padded.  
		//	Removed the extra padding here.
		sqls = sqls + "nvl(trim(substr(gl_account," + string(starting_pos[upperbound(starting_pos)]) + ",35))," 	// GL_JOURNAL_CATEGORY
		sqls = sqls + "nvl((select trim(external_je_code) from standard_journal_entries b where trim(a.gl_je_code) = trim(b.gl_je_code)),nvl(trim(a.gl_je_code),'NULL'))),"
	else
		sqls = sqls + "nvl((select trim(external_je_code) from standard_journal_entries b where trim(a.gl_je_code) = trim(b.gl_je_code)),nvl(trim(a.gl_je_code),'NULL'))," 	// GL_JOURNAL_CATEGORY
	end if
	sqls = sqls + 'nvl(amount_type,1),'		// AMOUNT_TYPE
	sqls = sqls + "'" + string(g_batch_id) + "'" 	// INTERFACE_BATCH_ID
	
	for i = 1 to num_rows
		if da[i] > 0 then 
			column_sql =  ds_cr_columns.getitemstring(i,'SQL_TO_POPULATE')
			sqls = sqls + ',' + column_sql
		end if
	next
	
	sqls = sqls + " from gl_transaction a where gl_status_id = 14"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode <> 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Inserting into " + i_staging_table_name + ": " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		rollback;
		overall_rtn = -1
		goto halt_the_app
	end if
end if

 // ### 7660: JAK: 2011-06-15:  Process REIMB transactions as well.
if reimb = 'YES' and reimb_gl_trans_to_process then	
	//*****************************************************************************************
	//
	//  Build the insert into the staging table and do the insert
	//
	//*****************************************************************************************
	f_pp_msgs("Insert from reimb_gl_transaction to " + i_staging_table_name + " at " + string(now()))
	
	sqls = 'insert into ' + i_staging_table_name + ' ("ID'
		
	for i = 1 to num_elements
		sqls = sqls + '","' + upper(elements[i])
	next
	
	sqls = sqls + &
		'","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
		 ',"MONTH_NUMBER","MONTH_PERIOD"' + &
		 ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE","INTERFACE_BATCH_ID","DRILLDOWN_KEY"'
	
	for i = 1 to num_rows
		if da[i] > 0 then 
			column_name = ds_cr_columns.getitemstring(i,'CR_COLUMN_NAME')
			sqls = sqls + ',"' + upper(column_name) + '"'
		end if
	next
	
	
	sqls = sqls + ') select crdetail.nextval,'
	
	 for i = 1 to num_elements
		sqls = sqls + 'nvl(trim(substr(gl_account,' + string(starting_pos[i]) + ',' + string(widths[i]) + ")),'" + defaults[i] + "'),"
	next
	
	sqls = sqls + 'decode(sign(decode(debit_credit_indicator,0,-amount,amount)),-1,-1,1),'		// DR_CR_ID
	sqls = sqls + '1,'		// LEDGER_SIGN
	sqls = sqls + '0,'		// QUANTITY
	sqls = sqls + 'decode(debit_credit_indicator,0,-amount,amount),'										// AMOUNT
	sqls = sqls + "to_char(month,'yyyymm')," 																	// MONTH_NUMBER
	sqls = sqls + '0,'		// MONTH_PERIOD
	if gljc_in_gl_account = 'YES' then
		// ### 7701: JAK: 2011-05-31: starting_pos[upperbound(starting_pos)] already has the right starting positions with widths and spaces padded.  
		//	Removed the extra padding here.
		sqls = sqls + "nvl(trim(substr(gl_account," + string(starting_pos[upperbound(starting_pos)]) + ",35))," 	// GL_JOURNAL_CATEGORY
		sqls = sqls + "nvl((select trim(external_je_code) from standard_journal_entries b where trim(a.gl_je_code) = trim(b.gl_je_code)),nvl(trim(a.gl_je_code),'NULL'))),"
	else
		sqls = sqls + "nvl((select trim(external_je_code) from standard_journal_entries b where trim(a.gl_je_code) = trim(b.gl_je_code)),nvl(trim(a.gl_je_code),'NULL'))," 	// GL_JOURNAL_CATEGORY
	end if
	sqls = sqls + '1,'		// AMOUNT_TYPE  // hardcoded to 1 for Reimb
	sqls = sqls + "'" + string(g_batch_id) + "'," 	// INTERFACE_BATCH_ID
	sqls = sqls + "'REIMB'" 	// DRILLDOWN_KEY
	
	for i = 1 to num_rows
		if da[i] > 0 then 
			column_sql =  ds_cr_columns.getitemstring(i,'REIMB_SQL_TO_POPULATE')
			sqls = sqls + ',' + column_sql
		end if
	next
	
	sqls = sqls + " from reimb_gl_transaction a where gl_status_id = 14"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode <> 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Inserting into " + i_staging_table_name + " (2): " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		rollback;
		overall_rtn = -1
		goto halt_the_app
	end if
end if

if gl_trans_to_process then
	//*****************************************************************************************
	//
	//	 BALANCE THE TRANSACTIONS IN STAGING TABLE AGAINST gl_transaction.
	//
	//*****************************************************************************************
	f_pp_msgs("Balancing " + i_staging_table_name + " to gl_transaction at " + string(now()))
	
	select count(*), sum(decode(debit_credit_indicator,0,-amount,amount)) into :count_stag, :amount_stag
		from gl_transaction 
		where gl_status_id = 14;
	
	if isnull(count_stag)  then count_stag = 0
	if isnull(amount_stag) then amount_stag = 0
	
	sqls = "select count(*) count_cr, sum(amount) amount_cr "
	sqls += "  from " + i_staging_table_name + " "
	sqls += " where interface_batch_id = '" + g_batch_id + "' and drilldown_key is null "
	ds_results = create uo_ds_top
	f_create_dynamic_ds(ds_results,'grid',sqls,sqlca,true)
	
	count_cr = ds_results.getitemnumber(1,1)
	amount_cr = ds_results.getitemnumber(1,2)
	if isnull(count_cr)  then count_cr = 0
	if isnull(amount_cr) then amount_cr = 0
	destroy ds_results
	
	if count_stag <> count_cr or amount_stag <> amount_cr then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: " + i_staging_table_name + " and gl_transaction are out of balance!")
		f_pp_msgs(" -- gl_transaction has a record count = " + string(count_stag) + " and a dollar amount = " + string(amount_stag) )
		f_pp_msgs(" -- " + i_staging_table_name + " has a record count = " + string(count_cr) + " and a dollar amount = " + string(amount_cr) )
		f_pp_msgs("PP TO CR DATA WILL NOT BE LOADED INTO THE COST REPOSITORY!")
		f_pp_msgs("  ")
		rollback;  //  Note this rolls back all intermediate inserts and updates so at square 1.
		return -1
	end if
end if

if reimb_gl_trans_to_process then
	//*****************************************************************************************
	//
	//	 BALANCE THE TRANSACTIONS IN STAGING TABLE AGAINST gl_transaction.
	//
	//*****************************************************************************************
	f_pp_msgs("Balancing " + i_staging_table_name + " to reimb_gl_transaction at " + string(now()))
	
	select count(*), sum(decode(debit_credit_indicator,0,-amount,amount)) into :count_stag, :amount_stag
		from reimb_gl_transaction 
		where gl_status_id = 14;
	
	if isnull(count_stag)  then count_stag = 0
	if isnull(amount_stag) then amount_stag = 0
	
	sqls = "select count(*) count_cr, sum(amount) amount_cr "
	sqls += "  from " + i_staging_table_name + " "
	sqls += " where interface_batch_id = '" + g_batch_id + "' and drilldown_key = 'REIMB' "
	ds_results = create uo_ds_top
	f_create_dynamic_ds(ds_results,'grid',sqls,sqlca,true)
	
	count_cr = ds_results.getitemnumber(1,1)
	amount_cr = ds_results.getitemnumber(1,2)
	if isnull(count_cr)  then count_cr = 0
	if isnull(amount_cr) then amount_cr = 0
	destroy ds_results
	
	if count_stag <> count_cr or amount_stag <> amount_cr then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: " + i_staging_table_name + " and reimb_gl_transaction are out of balance!")
		f_pp_msgs(" -- reimb_gl_transaction has a record count = " + string(count_stag) + " and a dollar amount = " + string(amount_stag) )
		f_pp_msgs(" -- " + i_staging_table_name + " has a record count = " + string(count_cr) + " and a dollar amount = " + string(amount_cr) )
		f_pp_msgs("PP TO CR DATA WILL NOT BE LOADED INTO THE COST REPOSITORY!")
		f_pp_msgs("  ")
		rollback;  //  Note this rolls back all intermediate inserts and updates so at square 1.
		return -1
	end if
end if

// ### 9844: JAK: 2012-10-29:  Issue when existing records in staging but no new transactions, g_batch_id remained 'NEW' causing the code below to fail.
// If splitting, switch to g_batch_id = '0' now
if allow_Splitting = 'YES' then
	g_batch_id = '0'
end if

if gl_trans_to_process then
	//*****************************************************************************************
	//
	//  MARK THE GL_TRANSACTION RECORDS AS READ.
	//
	//*****************************************************************************************
	f_pp_msgs("Updating gl_transaction.gl_status_id from In Process (14) to Done (3) at " + string(now()))
	
	if allow_splitting = 'YES' then
		// don't have a real batch id we can assign...
		update gl_transaction  
			set gl_status_id = 3
			where gl_status_id = 14;
		
		sqls = "update " + i_staging_table_name + " "
		sqls += " set interface_batch_id = '" + g_batch_id + "' "
		sqls += " where interface_batch_id <> '" + g_batch_id + "' and drilldown_key is null "
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating interface_batch_id to '0': " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if	
	else
		update gl_transaction  
			set gl_status_id = 3, description = substr(:g_batch_id || ' ' || description,1,35)
			where gl_status_id = 14;
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating gl_transaction.gl_status_id from In Process to Done: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if


if reimb_gl_trans_to_process then
	//*****************************************************************************************
	//
	//  MARK THE REIMB_GL_TRANSACTION RECORDS AS READ.
	//
	//*****************************************************************************************
	f_pp_msgs("Updating reimb_gl_transaction.gl_status_id from In Process (14) to Done (3) at " + string(now()))
	
	if allow_splitting = 'YES' then
		// don't have a real batch id we can assign...
		update reimb_gl_transaction  
			set gl_status_id = 3
			where gl_status_id = 14;
		
		sqls = "update " + i_staging_table_name + " "
		sqls += " set interface_batch_id = '" + g_batch_id + "' "
		sqls += " where interface_batch_id <> '" + g_batch_id + "' and drilldown_key = 'REIMB' "
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating interface_batch_id to '0': " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if	
	else
		update reimb_gl_transaction  
			set gl_status_id = 3, description = substr(:g_batch_id || ' ' || description,1,35)
			where gl_status_id = 14;
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating reimb_gl_transaction.gl_status_id from In Process to Done: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if
end if

commit;

skip_insert:
		
//*****************************************************************************************
//
//  MONTH_PERIOD:
//
//*****************************************************************************************
f_pp_msgs("Updating month period at " + string(now()))

//	 Increment the month_period (existing months) on cr_interface_month_period.
sqls = "update cr_interface_month_period a set month_period = month_period + 1 "
sqls += " where source_id = " + string(i_source_id) + " "
sqls += " and exists (select 1 from " + i_staging_table_name + " b where a.month_number = b.month_number and b.month_period = 0) "
	
execute immediate :sqls;
	
if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_interface_month_period (existing months) : " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  Insert new month numbers and their corresponding month periods.
sqls = "insert into cr_interface_month_period (source_id, month_number, month_period) "
sqls += " select distinct " + string(i_source_id) + ", month_number, 1 "
sqls += " from " + i_staging_table_name + " a where month_period = 0 "
sqls += " and not exists (select 1 from cr_interface_month_period b where a.month_number = b.month_number and b.source_id = " + string(i_source_id) + ") "
	
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into cr_interface_month_period (new months): " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	return -1
end if  

//  Update the month period (existing months) on staging table.
sqls = "update " + i_staging_table_name + " a "
sqls += " set month_period = ( "
sqls += "  select b.month_period  "
sqls += "  from cr_interface_month_period b "
sqls += "  where a.month_number = b.month_number "
sqls += "  and b.source_id = " + string(i_source_id) + ") "
sqls += " where month_period = 0 "
	
execute immediate :sqls;
	
if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating " + i_staging_table_name + ".month_period: " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	return -1
end if



//*****************************************************************************************
//
//  ### 7821: JAK: 2011-06-09:  Change validation object to an instance variable
//	so that the parameters can be modified in the custom function if needed.
//
//*****************************************************************************************
i_uo_cr_validations = create uo_cr_validation


//*****************************************************************************************
//
//  Call Custom Function Before Validations
//
//*****************************************************************************************
rtn = f_pptocr_custom(2)
if rtn < 0 then
	rollback;
	overall_rtn = -1
	goto halt_the_app
end if


//*****************************************************************************************
//
//  Interco Balancing
// ### JAK : 5763 : Omit company from balancing call
//
//*****************************************************************************************
select count(*) into :num_rows
from cr_interco_balancing;

if num_rows > 0 and omit_company = 'NO' then
	f_pp_msgs("Running Interco Balancing at " + string(now()))
	sqls = "select distinct month_number, month_period from " + i_staging_table_name
	uo_ds_top ds_months
	ds_months = create uo_ds_top
	f_create_dynamic_ds(ds_months,'grid',sqls,sqlca,true)
	
	uo_cr_interco_balancing l_uo_cr_interco_balancing
	l_uo_cr_interco_balancing = CREATE uo_cr_interco_balancing
		
	for i = 1 to ds_months.rowcount()
		f_pp_msgs("Month: " + string(ds_months.getitemnumber(i,'month_number')))
		f_pp_msgs("Month Period: " + string(ds_months.getitemnumber(i,'month_period')))
		//Start Inter Co Balancing
		rtn = l_uo_cr_interco_balancing.uf_interco_balancing(i_staging_table_name, 	"interface_batch_id", g_batch_id, &
			ds_months.getitemnumber(i,'month_number'),ds_months.getitemnumber(i,'month_period'))
	next
end if

//*****************************************************************************************
//
//	 VALIDATIONS:  Call the new validations user object.  It is assumed that combos have
//						already been created.
//
//*****************************************************************************************
f_pp_msgs("Analyzing stg table at " + string(now()))
sqlca.analyze_table(i_staging_table_name)

f_pp_msgs("Analyzing cr_validations_invalid_ids at " + string(now()))
sqlca.analyze_table('cr_validations_invalid_ids')

f_pp_msgs("Running validations at " + string(now()))
f_pp_msgs(" -- Deleting from cr_validations_invalid_ids at " + string(now()))

lvalidate_rtn = i_uo_cr_validations.uf_delete_invalid_ids(i_staging_table_name, g_batch_id)

if lvalidate_rtn <> 1 then 
	overall_rtn = -1
	goto halt_the_app
end if

validation_kickouts = false

//  Balancing
// ### JAK : 5763 : Omit company from balancing call
f_pp_msgs(" -- Validating the batch balances at " + string(now()))
if omit_company = 'YES' then
	sqls = "select gl_journal_category,month_number,sum(amount) amount " + &
		"from " + i_staging_table_name + " where interface_batch_id = '" + g_batch_id + "' group by " + &
		"gl_journal_category,month_number having sum(amount) <> 0"
else
	sqls = "select " + company_field + ",gl_journal_category,month_number,sum(amount) amount " + &
		"from " + i_staging_table_name + " where interface_batch_id = '" + g_batch_id + "' group by " + &
		company_field + ",gl_journal_category,month_number having sum(amount) <> 0"
end if
	
ds_balancing = create uo_ds_top
f_create_dynamic_ds(ds_balancing,'grid',sqls,sqlca,true)

for i = 1 to ds_balancing.rowcount()
	if i = 1 then
		// Insert these records into the kickouts table so they show up there for review
		if omit_company = 'YES' then
			sqls = "insert into cr_validations_invalid_ids2 " + &
				"(table_name, id, msg) " + &
				"select '" + i_staging_table_name + "', id, 'GL Journal Category and Month do not balance.' " + &
				"from " + i_staging_table_name + " " + &
				"where (gl_journal_category, month_number) in " + &
				"(select gl_journal_category, month_number  " + &
				"from " + i_staging_table_name + " " + &
				"where interface_batch_id = '" + g_batch_id + "' " + &
				"group by gl_journal_category, month_number " + &
				"having sum(amount) <> 0)"
		else
			sqls = "insert into cr_validations_invalid_ids2 " + &
				"(table_name, id, msg) " + &
				"select '" + i_staging_table_name + "', id, '" + company_field + ", GL Journal Category, and Month do not balance.' " + &
				"from " + i_staging_table_name + " " + &
				"where (" + company_field + ", gl_journal_category, month_number) in " + &
				"(select " + company_field + ", gl_journal_category, month_number  " + &
				"from " + i_staging_table_name + " " + &
				"where interface_batch_id = '" + g_batch_id + "' " + &
				"group by " + company_field + ", gl_journal_category, month_number " + &
				"having sum(amount) <> 0)"
		end if
			
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_validations_invalid_ids2: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			overall_rtn = -1
			goto halt_the_app
		end if
		
		insert into cr_validations_invalid_ids (table_name, id) (
		select distinct table_name, id from cr_validations_invalid_ids2
		 where table_name = :i_staging_table_name
		minus
		select table_name, id from cr_validations_invalid_ids);
	
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_validations_invalid_ids: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			overall_rtn = -1
			goto halt_the_app
		end if
		
		validation_kickouts = true
		
		f_pp_msgs("")
		f_pp_msgs("ERROR:  The following sets of data are out of balance and cannot be posted:")
	end if
	
	if omit_company = 'YES' then
		f_pp_msgs(ds_balancing.getitemstring(i,1) + '-' + string(ds_balancing.getitemnumber(i,2)) + ':  ' + string(ds_balancing.getitemnumber(i,3),'$#,##0.00;($#,##0.00)'))
	else
		f_pp_msgs(ds_balancing.getitemstring(i,1) + '-' + ds_balancing.getitemstring(i,2) + '-' + string(ds_balancing.getitemnumber(i,3)) + ':  ' + string(ds_balancing.getitemnumber(i,4),'$#,##0.00;($#,##0.00)'))
	end if
	if i = ds_balancing.rowcount() then
		// Can save the data like this for review.
		f_pp_msgs("")
	end if
next

// COMBO VALIDATIONS
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - COMBO';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then 
	f_pp_msgs(" -- Validating the Batch (Combos) at " + string(now()))
	//	 Which function is this client using?
	setnull(cv_validations)
	select upper(trim(control_value)) into :cv_validations from cr_system_control 
	 where upper(trim(control_name)) = 'VALIDATIONS - CONTROL OR COMBOS';
	if isnull(cv_validations) then cv_validations = "CONTROL"
	
	if cv_validations = 'CONTROL' then
		lvalidate_rtn = i_uo_cr_validations.uf_validate_control(i_staging_table_name, g_batch_id)
	else
		lvalidate_rtn = i_uo_cr_validations.uf_validate_combos(i_staging_table_name, g_batch_id)
	end if
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate_(x).  Logged by the function.
		overall_rtn = -1
		goto halt_the_app
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		num_rows = 0
		select count(*) into :num_rows	from cr_validations_invalid_ids
		 where lower(table_name)= :i_staging_table_name;
		if num_rows > 0 then validation_kickouts = true
	end if
end if

// PROJECT BASED
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then
	f_pp_msgs(" -- Validating the Batch (Projects) at " + string(now()))
	lvalidate_rtn = i_uo_cr_validations.uf_validate_projects(i_staging_table_name, g_batch_id)
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		overall_rtn = -1
		goto halt_the_app
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		num_rows = 0
		select count(*) into :num_rows	from cr_validations_invalid_ids
		 where lower(table_name)= :i_staging_table_name;
		if num_rows > 0 then validation_kickouts = true
	end if
end if

// MASTER ELEMENT VALIDATIONS
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - ME';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then 
	f_pp_msgs(" -- Validating the Batch (Element Values) at " + string(now()))
	lvalidate_rtn = i_uo_cr_validations.uf_validate_me(i_staging_table_name, g_batch_id)
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		overall_rtn = -1
		goto halt_the_app
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		//  Validation Kickouts.
		num_rows = 0
		select count(*) into :num_rows from cr_validations_invalid_ids
		 where lower(table_name)= :i_staging_table_name;
		if num_rows > 0 then validation_kickouts = true
	end if
end if

// MONTH NUMBER / RANGE BASED VALIDATIONS
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MONTH NUMBER';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then 
	f_pp_msgs(" -- Validating the Batch (Open Month Number) at " + string(now()))
	lvalidate_rtn = i_uo_cr_validations.uf_validate_month_number(i_staging_table_name, g_batch_id)
	
	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		overall_rtn = -1
		goto halt_the_app
	end if
	
	if lvalidate_rtn = -2 then
		// Month Number is currently not inserting into the kickouts table.
		//  Validation Kickouts.
		
		// Build the records from the kickout tables here as it isn't done in the UO
		sqls = "insert into cr_validations_invalid_ids2 " + &
			"(table_name, id, msg) " + &
			"select '" + i_staging_table_name + "', id, 'MONTH IS NOT OPEN' " + &
			"from " + i_staging_table_name + " " + &
			"where (" + company_field + ", month_number) in " + &
			"(select " + company_field + ", month_number  " + &
			"from " + i_staging_table_name + " " + &
			"where interface_batch_id = '" + g_batch_id + "' " + &
			"minus " + &
			"select " + company_field + ", month_number " + &
			"from cr_open_month_number " + &
			"where source_id = " + string(i_source_id) + " " + &
			"and status = 1 )"
		
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_validations_invalid_ids2: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			overall_rtn = -1
			goto halt_the_app
		end if
		
		insert into cr_validations_invalid_ids (table_name, id) (
		select distinct table_name, id from cr_validations_invalid_ids2
		 where table_name = :i_staging_table_name
		minus
		select table_name, id from cr_validations_invalid_ids);
	
		if sqlca.sqlcode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Inserting into cr_validations_invalid_ids: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			overall_rtn = -1
			goto halt_the_app
		end if
			
		validation_kickouts = true
	end if
end if

// CUSTOM VALIDATIONS
// ### 7515: JAK: 2011-05-06:  Custom validations needs to check the system control.
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - CUSTOM';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations = "YES" then
	f_pp_msgs(" -- Validating the Batch (Custom Validations) at " + string(now()))
	lvalidate_rtn = i_uo_cr_validations.uf_validate_custom(i_staging_table_name, g_batch_id)

	if lvalidate_rtn = -1 then
		//  Error in uf_validate.  Logged by the function.
		overall_rtn = -1
		goto halt_the_app
	end if
	
	if lvalidate_rtn = -2 and not validation_kickouts then
		validation_kickouts = true
	end if
end if

// CHECK FOR VALIDATION ERRORS
if validation_kickouts then
//	cv_memo_val = ""
//	select upper(trim(control_value)) into :cv_memo_val from cr_system_control
//	 where upper(trim(control_name)) = 'VALIDATIONS: MEMO VAL INTERFACES';
//	if isnull(cv_memo_val) or cv_memo_val = "" then cv_memo_val = "NO"
	
	// ### 11638: JAK: 2012-12-19:  Disable memo validations for PPtoCR.
	cv_memo_val = "NO"
	if cv_memo_val = "NO" then
		f_pp_msgs("  ")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE BATCH WILL NOT BE LOADED!  *****")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("  ")
		
		overall_rtn = -2
		if allow_splitting = 'NO' then
			goto halt_the_app
		end if
	else
		f_pp_msgs("  ")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("   *****  MEMO VALIDATION KICKOUTS EXIST!  THE BATCH WILL BE LOADED!  *****")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("  ")
		
		//  DO NOT KICK OUT --- MEMO VALIDATIONS ONLY !!!
		//  WITH MEMO VALIDATIONS, WE MUST FLUSH THE INVALID_IDS TABLES.
		lvalidate_rtn = i_uo_cr_validations.uf_delete_invalid_ids(i_staging_table_name, g_batch_id)
		
		if lvalidate_rtn <> 1 then 
			overall_rtn = -1
			goto halt_the_app
		end if
	end if
end if

f_pp_msgs("Validations complete at " + string(now()))


//*****************************************************************************************
//
//	 Splitting
//
//*****************************************************************************************
if allow_splitting = 'YES' then
	// Look at the records by company, gl_journal_category, and month.  If there were any kickouts for that combination
	//	the hold everything.  If not, let them post.
	sqls = "update " + i_staging_table_name + " a " + &
		"	set interface_batch_id = 'LOAD' " + &
		"	where not exists " + &
		"	(select 1 " + &
		"	from " + i_staging_table_name + " b, cr_validations_invalid_ids c " + &
		"	where b.id = c.id " + &
		"	and b." + company_field + " = a." + company_field + " " + &
		"	and b.gl_journal_category = a.gl_journal_category " + &
		"	and b.month_number = a.month_number)"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Updating interface_batch_id = 'LOAD': " + sqlca.sqlerrtext)
		f_pp_msgs("  ")
		rollback;
		overall_rtn = -1
		goto halt_the_app
	elseif sqlca.SQLNRows > 0 then
		// There are transactions that can be loaded.  Assign a real batch id now.
		select costrepository.nextval into :g_batch_id from dual;
		if isnull(g_batch_id) then g_batch_id = '0'
		
		if g_batch_id = '0' then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Could not get the batch_id from costrepository.nextval")
			f_pp_msgs("  ")
			rtn = -1
			goto halt_the_app
		end if
		
		sqls = "update " + i_staging_table_name + " "
		sqls += "set interface_batch_id = '" + g_batch_id + "' "
		sqls += "where interface_batch_id = 'LOAD' "
	
		execute immediate :sqls;
						
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating interface_batch_id = 'LOAD': " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			overall_rtn = -1
			goto halt_the_app
		end if
		
		sqls = "update gl_transaction set description = substr('" + g_batch_id + " ' || description,1,35) " + &
			"where gl_trans_id in (select gl_trans_id from " + i_staging_table_name + " where interface_batch_id = '" + g_batch_id + "')"
			
		execute immediate :sqls;
						
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Backfilling Interface Batch ID on GL Trans: " + sqlca.sqlerrtext)
			f_pp_msgs("  ")
			rollback;
			overall_rtn = -1
			goto halt_the_app
		end if
		
		
		// ### 7660: JAK: 2011-06-15:  Process REIMB transactions as well.	
		if reimb = 'YES' then
			sqls = "update reimb_gl_transaction set description = substr('" + g_batch_id + " ' || description,1,35) " + &
				"where gl_trans_id in (select gl_trans_id from " + i_staging_table_name + " where interface_batch_id = '" + g_batch_id + "')"
				
			execute immediate :sqls;
							
			if sqlca.SQLCode < 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: Backfilling Interface Batch ID on GL Trans: " + sqlca.sqlerrtext)
				f_pp_msgs("  ")
				rollback;
				overall_rtn = -1
				goto halt_the_app
			end if
		end if
	elseif sqlca.SQLNRows = 0 then
		goto halt_the_app
	end if
end if

// Update the Orig IFB ID field if it exists
sqls ="'update " + i_staging_table_name + " set orig_ifb_id = interface_batch_id where interface_batch_id = '" + g_batch_id + "'"
execute immediate :sqls;
// No error check as the field isn't required
	
//*****************************************************************************************
//
//	 INSERT FROM STAGING INTO DETAIL
//
//*****************************************************************************************
f_pp_msgs("Inserting into " + i_table_name + " at " + string(now()))

sqls = "insert into " + i_table_name + " "
sqls += "select * from " + i_staging_table_name + " where interface_batch_id = '" + g_batch_id + "'"
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into " + i_table_name + ": " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	overall_rtn = -1
	goto halt_the_app
end if


//*****************************************************************************************
//
//  UPDATE cr_cost_repository ...
//
//*****************************************************************************************
f_pp_msgs("Updating cr_cost_repository at " + string(now()))

uo_cr_cost_repository uo_cr
uo_cr = CREATE uo_cr_cost_repository

rtn = uo_cr.uf_insert_to_cr_general(i_source_id, "where interface_batch_id = '" + g_batch_id + "'",'','',"decode(drilldown_key,'REIMB',null,-id)")

if rtn = 1 then
else
	choose case rtn
		case -1
			f_pp_msgs("ERROR: inserting into cr_temp_cr: " + sqlca.SQLErrText)
		case -2
			f_pp_msgs("ERROR: cr_temp_cr.id UPDATE failed: " + sqlca.SQLErrText)			
		case -3
			f_pp_msgs("ERROR: cr_temp_cr.drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
		case -4
			f_pp_msgs("ERROR: " + i_table_name + ".drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
		case -5
			f_pp_msgs("ERROR: inserting into cr_cost_repository: " + sqlca.SQLErrText)			
		case -6
			f_pp_msgs("ERROR: deleting from cr_temp_cr UPDATE failed: " + sqlca.SQLErrText)			
		case else
			f_pp_msgs("ERROR: unknown error in uo_cr_cost_repository: " + sqlca.SQLErrText)			
	end choose
	rollback;
	overall_rtn = -1
	goto halt_the_app
end if


//*****************************************************************************************
//
//  DELETE OUT OF STAGING TABLE ...
//
//*****************************************************************************************
f_pp_msgs("Deleting records from " + i_staging_table_name + " at " + string(now()))
sqls = "delete from " + i_staging_table_name + " where interface_batch_id = '" + g_batch_id + "' "
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting records from " + i_staging_table_name + ": " + sqlca.SQLErrText)
	f_pp_msgs("The records must be removed manually before the interface runs again!")
	rollback;
	overall_rtn = -1
	goto halt_the_app
else
	// If the table is empty now (which it should be), truncate it to clear the stats
	sqls += "select count(*) from " + i_staging_table_name
	rtn = f_get_column(results[],sqls)
	
	if rtn > 0 then
		if results[1] = 0 then sqlca.truncate_table(i_staging_table_name)
	end if
end if

//*****************************************************************************************
//
//  COMMIT HERE ... don't want to kill the interface without loading if the only
//	 thing that fails is the insert into cr_interface_dates below or the 
//	 archiving.  Both of which are easy to do by hand and can be left alone as
//	 longlong as they are done before the next run of the interface.
//
//*****************************************************************************************
commit;


//*****************************************************************************************
//
//  INSERT INTO cr_interface_dates ...
//
//*****************************************************************************************
f_pp_msgs("Inserting into cr_interface_dates at " + string(now()))

// RJO 2-3-2009: Changed this to use interface_id instead of g_interface_id, and read from detail, not staging (already deleted these records)
sqls = "insert into cr_interface_dates "
sqls += "  (interface_id, month_number, month_period, process_date, total_dollars, total_records, "
sqls += "  feeder_system_id, total_debits, total_credits) ( "
sqls += "	select " + string(interface_id) + ", month_number, month_period, sysdate, sum(amount), count(*),  "
sqls += "  interface_batch_id, sum(decode(sign(amount),1,amount,0)), sum(decode(sign(amount),-1,amount,0)) "
sqls += "  from " + i_table_name + " "
sqls += "  where interface_batch_id = '" + g_batch_id + "' "
sqls += "  group by month_number, month_period, interface_batch_id) "
execute immediate :sqls;
	
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: Inserting into cr_interface_dates: " + sqlca.SQLErrText)
	f_pp_msgs("The interface records WERE committed.")
	rollback;
	overall_rtn = -1
	goto halt_the_app
else
	if sqlca.SQLNRows = 0 then
		//  No rows were inserted, meaning that there were no transactions.
		//  Still need to insert a record into cr_interfaces. 
		// RJO 2-3-2009: Changed this to use interface_id instead of g_interface_id
		insert into cr_interface_dates
			(interface_id, month_number, month_period, process_date, total_dollars, total_records, feeder_system_id)
		values
			(:interface_id, 0, 0, sysdate, 0, 0, :g_batch_id || ' - No Transactions to Post');
	else
		successful_batches[upperbound(successful_batches) + 1] = g_batch_id
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: Inserting into cr_interface_dates: " + sqlca.SQLErrText)
		f_pp_msgs("The interface records WERE committed.")
		rollback;
		overall_rtn = -1
		goto halt_the_app
	end if
end if

commit;

//Call Custom Function At the Very End
rtn = f_pptocr_custom(3)
if rtn < 0 then
	rollback;
	overall_rtn = -1
	goto halt_the_app
else
	commit;
end if

//*****************************************************************************************
//
//  IF THE INTERFACE JUST FINISHED PROCESSING PRIOR KICKOUTS, RESTART THE INTERFACE TO PICKUP NEW DATA.
//
//*****************************************************************************************
if prior_kickouts and allow_splitting = 'NO' and overall_rtn >= 0 then
	// ### 8673: JAK: 2011-09-27:  fix the looping that can occur due to the restart.
	prior_kickouts = false
	f_pp_msgs(" ")
	f_pp_msgs("Successfully processed previous records.  Restarting the interface to process any new records.")
	f_pp_msgs(" ")
	goto prior_kickouts_restart
end if

//*****************************************************************************************
//
//  HALT the application ...
//
//*****************************************************************************************
halt_the_app:

if overall_rtn = 1 then
	overall_rtn = 0
else
		
	//
	//  On error, send an email.
	//
	//	Find the user (from cr_system_control) to send a failure e-mail to.
	select control_value into :to_user
	  from cr_system_control
	 where upper(control_name) = 'INTERFACE ERROR EMAIL ADDRESS';
	
	// ### 7306: JAK: 2011-04-04:  The g_msmail needs to be created before the f(x) is called.
	g_msmail = create uo_smtpmail
	if overall_rtn = -2 then
		if upper(to_user) <> "NONE" then
			f_send_mail('pwrplant', 'INTERFACE FAILURE', 'The PPTOCR interface encountered validation issues and was unable to post all transactions.', to_user)
		end if
	else
		if upper(to_user) <> "NONE" then
			f_send_mail('pwrplant', 'INTERFACE FAILURE', 'The PPTOCR interface encountered an error and terminated.', to_user)
		end if
	end if
end if

f_pp_msgs(" ")
for i = 1 to upperbound(successful_batches)
	f_pp_msgs("--- Interface Batch ID " + successful_batches[i] + " successfully posted!")
next
if overall_rtn = -2 and allow_splitting = 'YES' and upperbound(successful_batches) > 0 then
	f_pp_msgs("--- Other validations kickouts exist and need to be reviewed!")
end if
		
return overall_rtn
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_pptocr_custom_version nvo_pptocr_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'pptocr_custom.pbd'
		return nvo_pptocr_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

