HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string	i_exe_name = 'cr_sum_ab.exe'
end variables

forward prototypes
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();string sqls, ix_sqls, ix_sqls_2, element_string, element, insert_sqls, datatype, &	
			element_column, element_table, sqls1, insert_element, insert_element_string, &
			s_date, sqls2, ix_tablespace, indiv_ix_sqls, user_id
longlong num_elements, i, colwidth, decimals, max_id, rtn, cr_sum_count, cr_sum_ab_rollup
boolean go_again = false
date     ddate
time     ttime

//drop the cr_sum_ab_temp table
	//it will also be dropped at the end of the interface.
	//if this is the first time it has run, the table won't exist so don't error check.
sqls = "drop table cr_sum_ab_temp"
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	rollback;
end if

//drop the cr_sum_ab_temp_1 table
sqls = "drop table cr_sum_ab_temp_1"
execute immediate :sqls;

if sqlca.sqlcode < 0 then
	rollback;
end if

//truncate the cr_sum_ab table
g_sqlca_logs.truncate_table("cr_sum_ab")

select count(*) into :cr_sum_count from cr_sum_ab;

if cr_sum_count > 0 then
	f_pp_msgs("ERROR: truncating table cr_sum_ab" + sqlca.sqlerrtext)
	rollback;
	return -1
end if


setnull(ix_tablespace)
select trim(control_value) into :ix_tablespace
  from cr_system_control where control_name = 'CR Indexes Tablespace';
if isnull(ix_tablespace) or ix_tablespace = "" then
	ix_tablespace = "pwrplant_idx"
end if


//create the datastore to loop over the element values and create the sql string
uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
sqls = 'select * from cr_elements order by "ORDER" '
f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)
num_elements = ds_elements.RowCount()

sqls               = "create table cr_sum_ab_temp  (  " + &
								" ab_id number(22,0), cr_id number(22,0), "

/*### - MDZ - 9580 - 20120224*/
/*The index should not be unique.  The cr_sum_ab_temp table will almost suredly have
	duplicate account keys in it, due to one row for actuals and one row for budget.*/
//ix_sqls            = 'create unique index cr_sum_ab_temp_1_ix on cr_sum_ab_temp (' 
ix_sqls            = 'create index cr_sum_ab_temp_1_ix on cr_sum_ab_temp (' 
ix_sqls_2            = 'create index cr_sum_ab_temp_2_ix on cr_sum_ab_temp (' 
element_string     = ""

//loop over the elements to get code block set #1
for i = 1 to num_elements
	
	//  Create the Accounting Key string ...
	//
	element     = lower(ds_elements.GetItemString(i, "description"))
	element     = f_cr_clean_string(element)
	datatype = ds_elements.GetItemString(i, "type")
	colwidth = ds_elements.GetItemNumber(i, "width")
	decimals = ds_elements.GetItemNumber(i, "decimal")
		
	element_string     = element_string     + '"' + upper(element)     + '" '
	
	if upper(datatype) = "VARCHAR2" then
		element_string      = element_string   + " " + datatype + "("  + string(colwidth) + ") not null, "
	else
		element_string      = element_string      + string(colwidth) + "," + string(decimals) + ") not null, "
	end if
	
	if i = num_elements then
		ix_sqls     = ix_sqls     + '"' + upper(element)     + '" '
	else
		ix_sqls     = ix_sqls     + '"' + upper(element)     + '", '
	end if
	
next

ix_sqls     = ix_sqls     + ") tablespace " + ix_tablespace


//loop over the elements to get code block set #2
for i = 1 to num_elements
	
	//  Create the Accounting Key string ...
	//
	element     = lower(ds_elements.GetItemString(i, "description"))
	element     = f_cr_clean_string(element)
	element 		= element + '_1'
	datatype = ds_elements.GetItemString(i, "type")
	colwidth = ds_elements.GetItemNumber(i, "width")
	decimals = ds_elements.GetItemNumber(i, "decimal")
		
	element_string     = element_string     + '"' + upper(element)    + '" '
	
	if i = num_elements then
		ix_sqls_2     = ix_sqls_2     + '"' + upper(element)     + '" '
	else
		ix_sqls_2     = ix_sqls_2     + '"' + upper(element)     + '", '
	end if
	
	//  DMJ:  MAKE THESE NULLABLE FOR PERFORMANCE ... SO WE DO NOT HAVE TO ADD A WHERE CLAUSE WITH
	//        AND "IN" SUB-SELECT TO THE UPDATE.  IT CREATED A NASTY NESTED LOOP AT PNW.
	if upper(datatype) = "VARCHAR2" then
		element_string      = element_string   + " " + datatype + "("  + string(colwidth) + ") null, "
	else
		element_string      = element_string      + string(colwidth) + "," + string(decimals) + ") null, "
	end if
	
next

element_string = element_string + '"TIME_STAMP" date null, ' + &
				  '"USER_ID" char(8) null, '

ix_sqls_2     = ix_sqls_2     + ") tablespace " + ix_tablespace

//trim the last comma of the element string
element_string = left(element_string, len(element_string) - 2)
sqls = sqls + " " + element_string + ") storage (initial 10M next 1M) "


f_pp_msgs("Creating the cr_sum_ab_temp table at " + string(now()))

execute immediate :sqls;
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: creating table cr_sum_ab_temp " + sqlca.sqlerrtext)
	rollback;
	f_pp_msgs("sqls = " + sqls)
	return -1
end if

f_pp_msgs("Adding the dollar columns to cr_sum_ab_temp at " + string(now()))

//add the dollar columns to cr_sum_ab_temp (and month_number...)
sqls = "alter table cr_sum_ab_temp add (month_number number(22,0),"
	sqls = sqls + "cy_beg_bal number(22,2),cy_curr_mo number(22,2),cy_prior_mo number(22,2),"
	sqls = sqls + "cy_curr_qtr number(22,2),cy_ytd number(22,2),cy_itd number(22,2),"
	sqls = sqls + "cy_tme number(22,2),cy_q1 number(22,2),cy_q2 number(22,2),"
	sqls = sqls + "cy_q3 number(22,2),cy_q4 number(22,2),cy_curr_mo_original_bdg number(22,2),"
	sqls = sqls + "cy_ytd_original_bdg number(22,2),cy_tot_yr_original_bdg number(22,2),"
	sqls = sqls + "cy_curr_mo_revised_bdg number(22,2),cy_ytd_revised_bdg number(22,2),"
	sqls = sqls + "cy_tot_yr_revised_bdg number(22,2),"
	sqls = sqls + "py_curr_mo number(22,2),py_prior_mo number(22,2),py_curr_qtr number(22,2),"
	sqls = sqls + "py_ytd number(22,2),py_itd number(22,2),py_tme number(22,2),"
	sqls = sqls + "py_q1 number(22,2),py_q2 number(22,2),py_q3 number(22,2),py_q4 number(22,2)"
	sqls = sqls + ")"
	
execute immediate :sqls;
if sqlca.SQLCode <> 0 then
	f_pp_msgs("Error: adding additional columns to cr_sum_ab_temp: " + ":~n~n" + &
				sqlca.SQLErrText)
	rollback;
	return -1
end if






// ---------------------------------------
//
//   MOVED IX'S FROM HERE.
//
// ---------------------------------------







//insert the data from cr_sum into cr_sum_ab_temp
insert_sqls = ""
element_string = ""
insert_sqls = "insert into cr_sum_ab_temp select 0, cr_id, "

go_again:
for i = 1 to num_elements
	
	//  Create the Accounting Key string ...
	//
	element     = lower(ds_elements.GetItemString(i, "description"))
	element     = f_cr_clean_string(element)

	datatype = ds_elements.GetItemString(i, "type")
	colwidth = ds_elements.GetItemNumber(i, "width")
	decimals = ds_elements.GetItemNumber(i, "decimal")
	
	element_string     = element_string     + '"' + upper(element)     + '", '
	
	//since there are two sets of code block on cr_sum_ab_temp, loop over the
	//	elements once more
	if go_again = false then
		if i = num_elements then
			go_again = true
			goto go_again
		end if
	end if
	
next

element_string = element_string + " null, null, "

insert_sqls = insert_sqls + " " + element_string + & 
"month_number, cy_beg_bal, cy_curr_mo, cy_prior_mo, cy_curr_qtr, cy_ytd, cy_itd, " + &
"cy_tme, cy_q1, cy_q2, cy_q3, cy_q4, cy_curr_mo_original_bdg, cy_ytd_original_bdg, " + & 
"cy_tot_yr_original_bdg, cy_curr_mo_revised_bdg, cy_ytd_revised_bdg, cy_tot_yr_revised_bdg, " + &
"py_curr_mo, py_prior_mo, py_curr_qtr, py_ytd, py_itd, py_tme, py_q1, py_q2, py_q3, py_q4 " + &
" from cr_sum "


f_pp_msgs("Inserting into cr_sum_ab_temp at " + string(now()))

execute immediate :insert_sqls;
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: inserting into cr_sum_ab_temp " + sqlca.sqlerrtext)
	rollback;
	f_pp_msgs("insert_sqls = " + insert_sqls)
	return -1
end if



//  *****  NOT SURE IF THIS HELPS  *****  //
//// -------------------------------------------
////
////   SPECIAL INDEXES ON INDIVIDUAL COLUMNS.
////
//// -------------------------------------------
//for i = 1 to num_elements
//	
//	
//	//  Create the Accounting Key string ...
//	//
//	element     = lower(ds_elements.GetItemString(i, "description"))
//	element     = f_cr_clean_string(element)
//	
//	
//	f_pp_msgs("Building " + element + " index at " + string(now()))
//	
//	indiv_ix_sqls = "create index indiv2" + element + "ix on cr_sum_ab_temp (" + element + ") tablespace " + ix_tablespace
//	
//	execute immediate :indiv_ix_sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("ERROR: Creating individual index: " + sqlca.sqlerrtext)
//		f_pp_msgs("indiv_ix_sqls = " + indiv_ix_sqls)
//		rollback;
//	end if
//	
//	
//	element 		= element + '_1'
//	
//	f_pp_msgs("Building " + element + " index at " + string(now()))
//	
//	indiv_ix_sqls = "create index indiv2" + element + "ix on cr_sum_ab_temp (" + element + ") tablespace " + ix_tablespace
//	
//	execute immediate :indiv_ix_sqls;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("ERROR: Creating individual index_1: " + sqlca.sqlerrtext)
//		f_pp_msgs("indiv_ix_sqls = " + indiv_ix_sqls)
//		rollback;
//	end if
//	
//	
//next



//  DMJ: 04/18/2005: Removed the pwrplant. reference ... when user ids other than the 
//                   pwrplant id are used, the temp tables will be created under that
//                   user_id.
//
//
//	 JAK: 04/18/2006: Cannot use the analyze_table function as it also hardcodes the
//							pwrplant owner now.  Use the DBMS_STATS per Roger's request.
//
//analyze for performance
f_pp_msgs("Analyzing cr_sum_ab_temp at " + string(now()))

select user into :user_id from dual;

sqls = "begin dbms_stats.gather_table_stats(ownname=>'" + user_id + &
	"',tabname=>'cr_sum_ab_temp',estimate_percent=> 25 , cascade=> TRUE );end;"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: analyzing cr_sum_ab_temp: " + sqlca.SQLErrText + &
		"The process WILL continue.")
	f_pp_msgs("  ")
end if


//backfill the budgeting elements on the 2nd code block set
f_pp_msgs("Backfilling budgeting elements at " + string(now()))

element_string = ""

for i = 1 to num_elements
	
	element     		= lower(ds_elements.GetItemString(i, "description"))
	element     		= f_cr_clean_string(element)
	element 				= element + '_1'
	element_table 		= lower(ds_elements.GetItemString(i, "element_table"))
	cr_sum_ab_rollup  = ds_elements.GetItemNumber(i, "cr_sum_ab_rollup")
	
	if isnull(cr_sum_ab_rollup) then cr_sum_ab_rollup = 0 //  0 = Actual (no summarization)
		
//	if isnull(element_table) or trim(element_table) = "" then
//		f_pp_msgs("  ")
//		f_pp_msgs("Skipping element " + element + " the cr_elements.element_table " + &
//			"has a blank value.")
//		f_pp_msgs("  ")
//		continue
//	end if
	
	element_table     = f_cr_clean_string(element_table)
	element_column 	= lower(ds_elements.GetItemString(i, "element_column"))
	
//	if isnull(element_table) or trim(element_table) = "" then
//		f_pp_msgs("  ")
//		f_pp_msgs("Skipping element " + element + " the cr_elements.element_column " + &
//			"has a blank value.")
//		f_pp_msgs("  ")
//		continue
//	end if
	
	element_column     = f_cr_clean_string(element_column)
	
	element_string     = element_string     +  element + ", "
	
	// this is done above instead.
//	//create indexes on the cr_sum_ab_temp 2nd code block set for the backfilling
//	sqls = " create index ix_" + element + " on cr_sum_ab_temp (" + element + &
//		") tablespace " + ix_tablespace
//	execute immediate :sqls;
//	
//	sqls = " analyze table cr_sum_ab_temp compute statisitcs "
//	execute immediate :sqls;


	//  DMJ:  COMMENT OUT THE SUB-SELECT FOR PERFORMANCE.  PNW GOT A NASTY NESTED LOOP.
	//        WE WILL UPDATE NULLS WITH ORIG VALUE LATER.
	sqls = " update cr_sum_ab_temp set " + element +  " = ( " + &
		" select nvl(budgeting_value,' ') from " + element_table + &
		" where cr_sum_ab_temp." + element + " = " + element_table + "." + element_column + &
		" and lower(" + element_table + ".element_type) = 'actuals') "
//		" where " + element + " in (select " + element_column + " from " + element_table + &
//		"   where lower(element_type) = 'actuals') " 


	//  This can happen if the element_table or element_column fields are not filled in on
	//  cr_elements (FE future_use for example).
	if isnull(sqls) then continue
		
	if cr_sum_ab_rollup = 1 then  //  1 = Summarize
		
		f_pp_msgs("Backfilling budgeting elements (element = " + element + ") at " + string(now()))
		
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("ERROR: backfilling code block #2 on cr_sum_ab_temp " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("sqls = " + sqls)
			return -1
		end if
		
		
		
		//  DMJ:  IF WE SUMMARIZED, AND SINCE WE ELIMINATED THE SUB-SELECT WHERE CLAUSE, THERE
		//        COULD BE NULL VALUES IN THE _1 FIELDS.  THIS WOULD OCCUR IF THERE ARE MISSING
		//        VALUES IN THE ME TABLES.  UPDATE NULLS IN THE _1 FIELDS WITH THE ORIGINAL
		//        VALUE.
		f_pp_msgs("Backfilling budgeting elements (element = " + element + &
			", cleaning up NULLS) at " + string(now()))
		
		sqls = " update cr_sum_ab_temp set " + element +  " = " + left(element, (len(element) - 2)) + &
				 "  where " + element + " is null "
		
		execute immediate :sqls;
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("ERROR: backfilling code block #2 (cleaning up NULLS) on cr_sum_ab_temp " + &
				sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("sqls = " + sqls)
			return -1
		end if
				
		
		
	end if

next  //  for i = 1 to num_elements ...



//  Call the custom function so we can perform custom backfills if needed.  For example,
//  If an entire element should roll to ' ', a straight update in the custom function
//  would be faster than the backfill.
rtn = f_cr_sum_ab_custom("after budget element backfill")

if rtn <> 1 then
	f_pp_msgs("ERROR: in f_cr_sum_ab_custom!")
	rollback;
	return -1
end if



// ---------------------------------------
//
//   MOVED IX'S TO HERE.
//
// ---------------------------------------
//create the index on cr_sum_ab_temp
f_pp_msgs("Adding 1st composite index to cr_sum_ab_temp at " + string(now()))
execute immediate :ix_sqls;
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Creating index on code block set #1 : " + sqlca.sqlerrtext)
	f_pp_msgs("ix_sqls = " + ix_sqls)
	
	/*### - MDZ - 9580 - 20120224*/
	/*If index fails the interface should not rollback.  Added additional message to let user know it may affect processing time.*/
//	rollback;
	f_pp_msgs("Interface will continue, but processing speed may be slow.")
end if
f_pp_msgs("Adding 2nd composite index to cr_sum_ab_temp at " + string(now()))
execute immediate :ix_sqls_2;
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Creating index on code block set #2 : " + sqlca.sqlerrtext)
	f_pp_msgs("ix_sqls_2 = " + ix_sqls_2)
	
	/*### - MDZ - 9580 - 20120224*/
	/*If index fails the interface should not rollback.  Added additional message to let user know it may affect processing time.*/
//	rollback;
	f_pp_msgs("Interface will continue, but processing speed may be slow.")
end if


//  MOVED FROM BELOW.
f_pp_msgs("Adding cr_id index to cr_sum_ab_temp at " + string(now()))
sqls = " create index ix_cr_id_ab_temp on cr_sum_ab_temp (cr_id) tablespace " + ix_tablespace
execute immediate :sqls;



//trim the last comma
element_string = left(element_string, len(element_string) - 2)

//create a cr_sum_ab_temp_1 table to help with the backfilling of the ab_id to cr_sum
//	for drilling down purposes.  If performance is bad, this piece might need to be 
//	reviewed.
f_pp_msgs("Creating cr_sum_ab_temp_1 at " + string(now()))

sqls = " create table cr_sum_ab_temp_1 as ( select rownum id, b.* from ( " + &
	" select distinct " + element_string + " from cr_sum_ab_temp ) b ) "
	
execute immediate :sqls;	
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: creating table cr_sum_ab_temp_1 " + sqlca.sqlerrtext)
	rollback;
	f_pp_msgs("sqls = " + sqls)
	return -1
end if

//update the ab_id on cr_sum_ab to be used in the following ways...
	//	1) backfilling the cr_sum.ab_id for drilling down to cr_sum
	//	2) included in the group by on the sum up to cr_sum_ab to be the ab_id

f_pp_msgs("Backfilling cr_sum_ab_temp.ab_id at " + string(now()))
	
sqls = " update cr_sum_ab_temp a set ab_id = ( select id from cr_sum_ab_temp_1 b " + &
	" where a."

sqls2 = "create index cr_sum_ab_temp_1_comp_ix on cr_sum_ab_temp_1 ("

element_string = ""
for i = 1 to num_elements
	
	element     		= lower(ds_elements.GetItemString(i, "description"))
	element     		= f_cr_clean_string(element)
	element 				= element + '_1'
	element_string     = element_string     +  element + ", "
	
	//create indexes on the cr_sum_ab_temp_1 set for the backfilling
	sqls1 = " create index ix_1_" + element + " on cr_sum_ab_temp_1 (" + element + &
		") tablespace " + ix_tablespace
	execute immediate :sqls1;
	
	if i = num_elements then
		//  DMJ:  remove where clause for performance.
//		sqls = sqls + element + " = b." + element + ")  where ("
		sqls = sqls + element + " = b." + element + ")  "
	else
		sqls = sqls + element + " = b." + element + " and a."
	end if
	
	if i = num_elements then
		sqls2 = sqls2 + element
	else
		sqls2 = sqls2 + element + ","
	end if
	
next 

sqls2 = sqls2 + ") tablespace " + ix_tablespace

execute immediate :sqls2;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: creating composite index on cr_sum_ab_temp_1 " + sqlca.sqlerrtext)
	rollback;
	f_pp_msgs("sqls2 = " + sqls2)
	return -1
end if


//  DMJ: 04/18/2005: Removed the pwrplant. reference ... when user ids other than the 
//                   pwrplant id are used, the temp tables will be created under that
//                   user_id.
//
//	 JAK: 04/18/2006: Cannot use the analyze_table function as it also hardcodes the
//							pwrplant owner now.  Use the DBMS_STATS per Roger's request.
//
f_pp_msgs("Analyzing table cr_sum_ab_temp_1 at " + string(now()))

select user into :user_id from dual;

sqls1 = "begin dbms_stats.gather_table_stats(ownname=>'" + user_id + &
	"',tabname=>'cr_sum_ab_temp_1',estimate_percent=> 25 , cascade=> TRUE );end;"

execute immediate :sqls1;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: analyzing cr_sum_ab_temp_1: " + sqlca.SQLErrText + &
		"The process WILL continue.")
	f_pp_msgs("  ")
end if


//trim the last comma
element_string = left(element_string, len(element_string) - 2)

//  DMJ:  remove where clause for performance.
//sqls = sqls + element_string + ") in ( select " + element_string + &
//	" from cr_sum_ab_temp_1) "
		
execute immediate :sqls;	
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: backfilling ab_id on cr_sum_ab_temp " + sqlca.sqlerrtext)
	rollback;
	f_pp_msgs("sqls = " + sqls)
	return -1
end if
	
//now backfill the ab_id on cr_sum using the code block values #2 to enable drilling down.
//the cr_id is on the cr_sum_ab_temp table 

//
//  MOVED ABOVE FOR PERFORMANCE ... DON'T WANT 2 ANALYZES ON CR_SUM_AB_TEMP.
//
//sqls = " create index ix_cr_id_ab_temp on cr_sum_ab_temp (cr_id) tablespace " + ix_tablespace
//execute immediate :sqls;
//
//sqls = " analyze table cr_sum_ab_temp compute statisitcs "
//execute immediate :sqls;

commit;

f_pp_msgs("Backfilling cr_sum.ab_id at " + string(now()))

update cr_sum a
set ab_id = ( 
	select ab_id 
	from cr_sum_ab_temp b
	where a.cr_id = b.cr_id)
where a.cr_id in (
	select cr_id
	from cr_sum_ab_temp);
	
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: backfilling ab_id on cr_sum " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

//sum up to cr_sum_ab by code block set #2
f_pp_msgs("Rolling cr_sum_ab_temp up to cr_sum_ab at " + string(now()))

element_string = ""
sqls = " insert into cr_sum_ab ( ab_id, " 

for i = 1 to num_elements
	
	//  Create the Accounting Key string ...
	//
	insert_element     = lower(ds_elements.GetItemString(i, "budgeting_element"))
	insert_element     = f_cr_clean_string(insert_element)
	element 				 = lower(ds_elements.GetItemString(i, "description"))
	element     		 = f_cr_clean_string(element)
	element 				 = element + "_1"
	
	insert_element_string     = insert_element_string     +  insert_element + ", "
	element_string				  = element_string				+  element + ", "
	
next


sqls = sqls + insert_element_string

sqls = sqls + " month_number, cy_beg_bal, cy_curr_mo, cy_prior_mo, cy_curr_qtr, " + & 
"cy_ytd, cy_itd, cy_tme, cy_q1, cy_q2, cy_q3, cy_q4, cy_curr_mo_original_bdg, cy_ytd_original_bdg, " + & 
"cy_tot_yr_original_bdg, cy_curr_mo_revised_bdg, cy_ytd_revised_bdg, cy_tot_yr_revised_bdg, " + &
"py_curr_mo, py_prior_mo, py_curr_qtr, py_ytd, py_itd, py_tme, py_q1, py_q2, py_q3, py_q4 ) " + &
" ( select ab_id, " + element_string + " month_number, sum(cy_beg_bal), sum(cy_curr_mo), sum(cy_prior_mo), sum(cy_curr_qtr), " + & 
"sum(cy_ytd), sum(cy_itd), sum(cy_tme), sum(cy_q1), sum(cy_q2), sum(cy_q3), sum(cy_q4), sum(cy_curr_mo_original_bdg), sum(cy_ytd_original_bdg), " + & 
"sum(cy_tot_yr_original_bdg), sum(cy_curr_mo_revised_bdg), sum(cy_ytd_revised_bdg), sum(cy_tot_yr_revised_bdg), " + &
"sum(py_curr_mo), sum(py_prior_mo), sum(py_curr_qtr), sum(py_ytd), sum(py_itd), sum(py_tme), sum(py_q1), sum(py_q2), sum(py_q3), sum(py_q4) " + &
"from cr_sum_ab_temp group by ab_id, " + element_string + " month_number )"

execute immediate :sqls;	
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: inserting into cr_sum_ab " + sqlca.sqlerrtext)
	rollback;
	f_pp_msgs("sqls = " + sqls)
	return -1
end if

sqlca.analyze_table('cr_sum_ab')

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: analyzing cr_sum_ab: " + sqlca.SQLErrText + &
		"The process WILL continue.")
	f_pp_msgs("  ")
end if


//************************************************************************
//  Truncate the temp tables to free up space.
//************************************************************************
//THIS DOES NOT WORK SINCE THE TABLES ARE NOT OWNED BY PWRPLANT
//AND THE truncate_table EXPECTS THE TABLES TO BE OWNED BY PWRPLANT
//g_sqlca_logs.truncate_table("cr_sum_ab_temp")
//g_sqlca_logs.truncate_table("cr_sum_ab_temp_1")

// JAK 4/19/2006 - Can't use the truncate table function as it is looking for a pwrplant
//		owned table.  Can use a direct call since the user running this interface
//		owns the tables it is truncating.
sqls = "truncate table cr_sum_ab_temp"
execute immediate :sqls;

sqls = "truncate table cr_sum_ab_temp_1"
execute immediate :sqls;


f_pp_msgs(" ")
f_pp_msgs("The cr_sum_ab table was successfully built.")
return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_sum_ab_custom_version nvo_cr_sum_ab_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_sum_ab_custom.pbd'
		return nvo_cr_sum_ab_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

