HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_release_je_wo.exe'

//instance variable for non-visual object CPR control
nvo_wo_control i_nvo_wo_control


end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function boolean uf_release_je_wo_main ()
public function boolean uf_setdate (longlong a_index)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Process Release JE logic
if not uf_release_je_wo_main() then
	// log the list of companies that failed to perform auto unitization
	i_nvo_wo_control.of_log_failed_companies("RELEASE JOURNAL ENTRIES")
	return rtn_failure
end if

i_nvo_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function boolean uf_release_je_wo_main ();/************************************************************************************************************************************************************
**
** uf_release_je_wo()
** 
** This function corresponds to w_wo_control.cb_release_je.event clicked().
**  
** Parameters : No parameters needed
**  
** Returns : (boolean) :   true  : Successful.
**                                  false : Error Encountered
**
************************************************************************************************************************************************************/
    
// Declare variables
longlong ret, num_elements, i, pp_stat_id, month_number, rtn
string comp_descr, process_msg
w_datawindows w  // dummy variable to FORCE powerbuilder into including this window in the EXE (and hence the databwindows

// if any company in the loop has an error, it continues on to the next company but returns a failure status to uf_read
boolean b_all_success
b_all_success = true

// Call constructors
i_nvo_wo_control.of_constructor( )

// Set instance variables
i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg 				//Array of longs
i_nvo_wo_control.i_month = datetime(g_ssp_parms.date_arg[1])		//Array of month - only get value stored in first element of array

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("Release Journal Entries - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

// Get the descriptions
rtn = i_nvo_wo_control.of_getDescriptionsFromIds(i_nvo_wo_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// Validate selected companies
if (i_nvo_wo_control.of_selectedCompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if

longlong num_companies, c
num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c=1 to num_companies
	//	it's selected, update the data window
	i_nvo_wo_control.of_companychanged(c,i_nvo_wo_control.i_month)
	
	if i_nvo_wo_control.i_ds_wo_control.retrieve(  i_nvo_wo_control.i_company_idx[c], i_nvo_wo_control.i_month ) = 1 then

		// Make sure the month isn't closed
		if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
			f_pp_msgs("This month is closed.")
			return false
		end if
	
	// Failed to retrieve company
	else
		f_pp_msgs("Error retrieving data for company: " + i_nvo_wo_control.i_ds_wo_control.i_sqlca_sqlerrtext)
		return false
	end if
next

// Start of Processing
f_pp_msgs("Process started on " + String(Today(), "mm/dd/yyyy") + " at " + String(Today(), "hh:mm:ss"))
for i = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	//set the company
	i_nvo_wo_control.of_companychanged( i , i_nvo_wo_control.i_month )
	
	// Log start of processing for this company
	f_pp_msgs(" ")
	f_pp_msgs("Release Journal Entries WO started for company " + i_nvo_wo_control.i_company_descr[i]  +  " at " + String(Today(), "hh:mm:ss"))
	pp_stat_id = f_pp_statistics_start("WO Monthly Close", "Release Journal Entries WO: " + i_nvo_wo_control.i_company_descr[i])
	
	//maint 43828 - change status of gl_status_id to 2 for the company and month (logic copied from f_release_je)
	string company_number
	company_number = "INVALID"
	select gl_company_no into :company_number
	from company
	where company_id = :i_nvo_wo_control.i_company ;
	
	if isnull(company_number) then company_number = " "
	
	if company_number = "INVALID" then
		rollback;
		f_pp_msgs("Unable to find gl_company_no for " + String(i_nvo_wo_control.i_company))
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(i)
		continue
	end if
	
	Update gl_transaction
		set GL_STATUS_ID = 2
	 where to_number(to_char(month,'yyyymm')) = to_number(to_char(:i_nvo_wo_control.i_month,'yyyymm'))
		and gl_status_id < 2 
		and trim(company_number) = trim(:company_number) ; 
	
	if sqlca.sqlcode = -1 then		
		rollback;
		f_pp_msgs("GL Status Update #1 Failed.~nSQL: "+sqlca.sqlerrtext)
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(i)
		continue
	end if
	//end maint 43828
	
	// Call custom function
	ret = f_create_wo_gl_trans()
	if ret = -1 then
		rollback;
		f_pp_msgs("Release Journal Entries WO failed from the call to custom function f_create_wo_gl_trans().")
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(i)
		continue
	end if	
	
	// Dynamic validations
	string args[]
	args[1] = string(i_nvo_wo_control.i_month)
	args[2] = string(i_nvo_wo_control.i_company_idx[i])
	ret = f_wo_validation_control(1057,args)
	if ret < 0 then
		rollback;
		f_pp_msgs("Release JE WO failed from the call to dynamic validation f_wo_validation_control.")
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(i)
		continue
	end if
	
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
	
	// Set date
	if not uf_setdate(i) then
		f_pp_msgs("Failed to set date on column je_released table wo_process_control from the call to uf_setdate(i).")
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(i)
		continue
	end if
	
	// Log end of processing for this company
	f_pp_msgs("Release Journal Entries WO completed for company " + i_nvo_wo_control.i_company_descr[i]  +  " at " + String(Today(), "hh:mm:ss"))

next

// Email users and log end of processing
i_nvo_wo_control.of_cleanup(6, 'email wo close: release journal entries', 'WO Release Journal Entries')
f_pp_msgs( " ")
f_pp_msgs("Process ended on " + String(Today(), "mm/dd/yyyy") + " at " + String(Today(), "hh:mm:ss"))

return b_all_success
end function

public function boolean uf_setdate (longlong a_index);/************************************************************************************************************************************************************
**
** uf_setdate()
** 
** This function updates the column je_released on table wo_process_control
**  
** Parameters : a_index - Company Index
**  
** Returns : (boolean) :   true  : Successful.
**                                  false : Error Encountered
**
************************************************************************************************************************************************************/

// Retrieve company
if i_nvo_wo_control.i_ds_wo_control.retrieve(  i_nvo_wo_control.i_company_idx[a_index], i_nvo_wo_control.i_month ) = 1 then

	// Try to update column je_released
	if i_nvo_wo_control.i_ds_wo_control.setItem( 1, 'je_released', today() ) = 1 then
		f_pp_msgs("Successful in setting JE Release date for this company.")
	else
		f_pp_msgs("Failure in setting JE Release date for this company.")
		return false
	end if

// Failed to retrieve company
else
	f_pp_msgs("Error retrieving data for company: " + i_nvo_wo_control.i_ds_wo_control.i_sqlca_sqlerrtext)
	return false
end if

// Update and commit to database
if i_nvo_wo_control.of_updatedw( ) < 1 then
		f_pp_msgs("Error in updating and committing to database")	
		return false
end if
		
return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_release_je_wo_custom_version nvo_ssp_release_je_wo_custom_version

choose case a_pbd_name
	case 'ssp_release_je_wo_custom.pbd'
		return nvo_ssp_release_je_wo_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

