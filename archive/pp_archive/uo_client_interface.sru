HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name
boolean ib_constraints_disabled = FALSE
longlong il_job_error
end variables

forward prototypes
public function longlong uf_read ()
public function integer uf_archive_wo_tables ()
public function string uf_get_column_list (string a_table_name)
public function integer uf_modify_constraints (string a_modify_type)
public function integer uf_archive_standard_type (longlong al_job_id)
public function integer uf_archive_blanket_type (longlong al_job_id)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
longlong ll_rtn

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS';

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE';

/************************* START: custom code ***********************************/

ll_rtn = uf_archive_wo_tables();

if ib_constraints_disabled then 
   ll_rtn = uf_modify_constraints('enable')
	if ll_rtn = rtn_failure then
		return rtn_failure
	end if
end if




/************************* END:   custom code ***********************************/

return rtn_success
end function

public function integer uf_archive_wo_tables ();longlong ll_job_id, ll_job_count, ll_archive_type_count, ll_rtn, ll_interface_error
string ls_archive_type, ls_disable_constraints
boolean lb_constraints_disabled

// Initialize variables
ll_job_count = 0
ll_interface_error = 0
ll_archive_type_count = 0
lb_constraints_disabled = FALSE


// ARCHIVE_WO_AVAILABLE_LIST table
//
//			ARCHIVE_STATUS - ('NOT READY', 'READY', 'PROCESS', 'PROCESSING', 'ARCHIVED', 'FAILED')
// 			NOT READY - On the selection window no Work Orders are selected to Archive.
//	   		READY - On the selection window the Work Order is set to READY when it is selected to Archive
//		   	PROCESS - The PP_ARCHIVE Batch application will set the Work Order to a status of PROCESS if it is in READY status.
//          FAILED - The PP_ARCHIVE Batch application will set the Work Order to a status of FAILED if any errors occur.
//          ARCHIVED - The PP_ARCHIVE Batch application will set the Work Order to a status of ARCHIVED if the transaction is commited.
//			
//			ARCHIVE_TYPE - ('STANDARD', 'BLANKET', 'CLEARING')
//
// Archive Process:
//							Will archive Work Orders by Jobs
//							Within a Job the Work Orders will be processed in sets by Archive Type and a Count number that represents the number
//                   of records to process at one time. The Count is a System COntrol - "Number of records to Archive"


// Set all Approved records to a status of PROCESS
update ARCHIVE_WO_AVAILABLE_LIST
   set ARCHIVE_STATUS = 'PROCESS'
 where ARCHIVE_STATUS = 'READY'
   and ARCHIVE_TYPE is not null;

if sqlca.SQLNRows < 1 then 
   f_pp_msgs("  ")	
	f_pp_msgs("MSG: No records to archive.")
   f_pp_msgs("  ")
	rollback;
	return 1
end if

if sqlca.SQLCode <> 0 then
   f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating ARCHIVE_WO_AVAILABLE_LIST to 'PROCESS' : " + &
             string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
   rollback;
   return -1
else
   commit; // This will allow users to see what records are being processed.
end if;

// We have records to Archive so Decide if we are going to disable constraints
select upper(CONTROL_VALUE)
  into :ls_disable_constraints
  from (select 1 ITEM_ORDER, UPPER(CONTROL_VALUE) CONTROL_VALUE
          from PP_SYSTEM_CONTROL_COMPANY
         where UPPER(CONTROL_NAME) = UPPER('Disable WO FKs')
        union
        select 2 ITEM_ORDER, 'NO Control' CONTROL_VALUE from DUAL)
 where ROWNUM = 1;

if sqlca.SQLCode <> 0 then
   f_pp_msgs("  ")
	f_pp_msgs("ERROR: Getting System Control Value for 'Disable WO FKs': " + &
             string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if	

if ls_disable_constraints = upper('NO Control') then
	f_pp_msgs("Message: 'Disable WO FKs' System Control Value doesn't exist.~r~n" + &
             "Foreign Key's will not be disabled.")
end if				 
				 
if ls_disable_constraints  = 'YES' then 
   ll_rtn = uf_modify_constraints('disable')
	ib_constraints_disabled = TRUE // Set to TRUE even if disable failed so we try to enable any FK's that got disabled.	
	if ll_rtn = -1 then
		return -1
   end if
end if

any la_job_id_array[]
longlong ll_job_id_array[], i
string ls_sql

ls_sql = "select distinct JOB_ID" +&
         "  from ARCHIVE_WO_AVAILABLE_LIST" +&
	      " where ARCHIVE_STATUS = 'PROCESS'" +&
         "   and ARCHIVE_TYPE is not null" +&
         " order by JOB_ID"
			
f_get_column(la_job_id_array, ls_sql)

ll_job_id_array = la_job_id_array

for i = 1 to upperbound(ll_job_id_array)
   il_job_error = 0
   ll_job_id = ll_job_id_array[i]
	
   ll_job_count = ll_job_count + 1

   f_pp_msgs("*******************************")	
   f_pp_msgs("Processing started for Job: " + string(ll_job_id))	
   f_pp_msgs("*******************************")	
	
	// Archive by ARCHIVE TYPE ('STANDARD', 'BLANKET', 'CLEARING') within a JOB
	
   any la_archive_type_array[]
	string ls_archive_type_array[]
   longlong j
	
	ls_sql = "select distinct ARCHIVE_TYPE" +&
            "  from ARCHIVE_WO_AVAILABLE_LIST" +&
            " where JOB_ID = " + string(ll_job_id) +&
            "   and ARCHIVE_STATUS = 'PROCESS'" +&
   	      " order by ARCHIVE_TYPE"

	f_get_column(la_archive_type_array, ls_sql)
	
	ls_archive_type_array = la_archive_type_array

	for j = 1 to upperbound(ls_archive_type_array)

   ls_archive_type = ls_archive_type_array[j]

		ll_archive_type_count = ll_archive_type_count + 1
         f_pp_msgs("*****************************************")			
		   f_pp_msgs("Archiving records of type " + ls_archive_type + " for Job " + string(ll_job_id))
         f_pp_msgs("*****************************************")				
		   if ls_archive_type = 'STANDARD' then
            ll_rtn = uf_archive_standard_type(ll_job_id)
				if ll_rtn = -1 or il_job_error = 1 then
      			// Mark the records that were not in PROCESSING back to READY
	      		update ARCHIVE_WO_AVAILABLE_LIST
		      		set ARCHIVE_STATUS = 'READY' 
			       where ARCHIVE_STATUS = 'PROCESS'
   				   and ARCHIVE_TYPE = 'STANDARD';
               f_pp_msgs("*****************************************")				
				   f_pp_msgs("ERROR: in processing Job: " + string(ll_job_id) + " in uf_archive_standard_type")
               f_pp_msgs("*****************************************")				
					// Continue to Process the next Archive Type or Job.  If records failed in the previous 
					// Archive Type or Job then those records were updated to FAILED.
					// return -1
					ll_interface_error = 1
				end if
			elseif ls_archive_type = 'BLANKET' then
				ll_rtn = uf_archive_blanket_type(ll_job_id);
				if ll_rtn = -1 or il_job_error = 1 then
      			// Mark the records that were not in PROCESSING back to READY
	      		update ARCHIVE_WO_AVAILABLE_LIST
		      		set ARCHIVE_STATUS = 'READY' 
			       where ARCHIVE_STATUS = 'PROCESS'
   				   and ARCHIVE_TYPE = 'BLANKET';

               f_pp_msgs("*****************************************")				
				   f_pp_msgs("ERROR: in processing Job: " + string(ll_job_id) + " in uf_archive_blanket_type")
               f_pp_msgs("*****************************************")				

					// Continue to Process the next Archive Type or Job.  If records failed in the previous 
					// Archive Type or Job then those records were updated to FAILED.
					// return -1
					ll_interface_error = 1					
				end if
			elseif ls_archive_type = 'CLEARING' then
				//ll_rtn = uf_archive_clearing_type(ll_job_id);
				if ll_rtn = -1 or il_job_error = 1 then
      			// Mark the records that were not in PROCESSING back to READY
	      		update ARCHIVE_WO_AVAILABLE_LIST
		      		set ARCHIVE_STATUS = 'READY' 
			       where ARCHIVE_STATUS = 'PROCESS'
   				   and ARCHIVE_TYPE = 'CLEARING';

               f_pp_msgs("*****************************************")				
				   f_pp_msgs("ERROR: in processing Job: " + string(ll_job_id) + " in uf_archive_clearing_type")
               f_pp_msgs("*****************************************")				

					// Continue to Process the next Archive Type or Job.  If records failed in the previous 
					// Archive Type or Job then those records were updated to FAILED.
					// return -1
					ll_interface_error = 1					
				end if
			else
            f_pp_msgs("  ")
            f_pp_msgs("ERROR: No Archive Type specified for some records in ARCHIVE_WO_AVAILABLE_LIST for Job: " + string(ll_job_id) +" : " + &
             string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
            f_pp_msgs("  ")
				return -1
			end if
				
		   f_pp_msgs("Completed Archiving records of type " + ls_archive_type + " for Job " + string(ll_job_id))

   next

	if ll_archive_type_count < 1 then
      f_pp_msgs("ERROR: Invalid or No Archive Types listed in ARCHIVE_WO_AVAILABLE_LIST for Job: " + string(ll_job_id))
		rollback;
		return -1
	end if

   if il_job_error = 1 then
      f_pp_msgs("*****************************************")				
		f_pp_msgs("Some Archive Types in Job: " + string(ll_job_id) + " Failed.")
      f_pp_msgs("*****************************************")				
	else
      f_pp_msgs("*****************************************")				
      f_pp_msgs("Completed Archiving all Archive Types for Job: " + string(ll_job_id))
      f_pp_msgs("*****************************************")				
   end if

next	

if ll_interface_error = 1 then
   f_pp_msgs("*****************************")
   f_pp_msgs("ERROR: Some Jobs had Errors.  Records with Errors were flaged.  Jobs or Sets without errors have processed.")
   f_pp_msgs("*****************************")
end if;

if ll_job_count < 1 then
   f_pp_msgs("  ")	
   f_pp_msgs("MSG: No records to archive.")
   f_pp_msgs("  ")
	rollback;
	return -1
end if

return 1
end function

public function string uf_get_column_list (string a_table_name);boolean lb_first_column = TRUE
longlong ll_num_columns = 0
string ls_column, ls_table_columns
	
// Get Column Names for a given table and have them delimited with a comma	
declare cur_table_columns cursor for 
   select COLUMN_NAME
     from ALL_TAB_COLUMNS
    where OWNER = 'PWRPLANT'
      and TABLE_NAME = :A_TABLE_NAME
	 order by COLUMN_ID;

   open cur_table_columns;
   fetch cur_table_columns 
	   into :ls_column;

   do while sqlca.sqlcode = 0 
   
		ll_num_columns = ll_num_columns + 1;
		if lb_first_column then
         ls_table_columns = ls_table_columns + ls_column;
	   else 
         ls_table_columns = ls_table_columns + ',' + ls_column;
		end if;

      lb_first_column = FALSE
      fetch cur_table_columns 
		   into :ls_column;		
   loop
	
close cur_table_columns;

return ls_table_columns

end function

public function integer uf_modify_constraints (string a_modify_type);string ls_sql, ls_alter_constraint, ls_table_name, ls_constraint_name


ls_sql = "select 'alter table ' || TABLE_NAME || ' " + lower(a_modify_type) + " constraint ' || CONSTRAINT_NAME ALTER_CONSTRAINT," +&
         "       TABLE_NAME," +&
         "       CONSTRAINT_NAME" +&			
			"  from USER_CONSTRAINTS UC" +&
			" where CONSTRAINT_TYPE = 'R'" +&
			"	and STATUS = 'ENABLED'" +&
			"	and R_CONSTRAINT_NAME in" +&
			"		 (select CONSTRAINT_NAME" +&
			"			 from ALL_CONSTRAINTS" +&
			"			where OWNER = 'PWRPLANT'" +&
			"			  and CONSTRAINT_NAME in" +&
			"					(select CONSTRAINT_NAME" +&
			"						from ALL_CONSTRAINTS" +&
			"					  where OWNER = 'PWRPLANT'" +&
			"						 and TABLE_NAME in (select TABLE_NAME from ARCHIVE_TABLE_LIST)" +&
			"						 and CONSTRAINT_TYPE = 'P'))" +&
			" order by TABLE_NAME"

declare cur_arc_constraints dynamic cursor for SQLSA;
prepare SQLSA from :ls_sql;
open dynamic cur_arc_constraints;
fetch cur_arc_constraints 
   into :ls_alter_constraint, :ls_table_name, :ls_constraint_name;

   do while sqlca.sqlcode = 0
		execute immediate :ls_alter_constraint;
		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not " + a_modify_type + " constraint: " + ls_constraint_name + "on table: " + ls_table_name)
			f_pp_msgs("SQL Statement: " + ls_alter_constraint)
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("Transaction rolled back.")						
			return -1
		else
   		f_pp_msgs("Constraint: " + ls_constraint_name + " on table: " + ls_table_name + " was " + a_modify_type + "d.")
		end if
		
      fetch cur_arc_constraints 
         into :ls_alter_constraint, :ls_table_name, :ls_constraint_name;
   loop
	
close cur_arc_constraints;

return 1
end function

public function integer uf_archive_standard_type (longlong al_job_id);string ls_table, ls_archive_table, ls_standard_ind_exists_clause
string ls_column, ls_column_list, ls_sql, ls_sql2, ls_disable_constraints
longlong ll_num_columns, ll_rec_cnt_to_process, ll_inserted_rows, ll_deleted_rows
longlong ll_inserted_rows2, ll_deleted_rows2, ll_set_error, ll_job_error
longlong ll_total_records, ll_records_processed, ll_set_num
boolean lb_first_column

ll_set_error = 0
ll_job_error = 0

// Get number of records to process as one transaction.
select upper(CONTROL_VALUE)
  into :ll_rec_cnt_to_process
  from PP_SYSTEM_CONTROL_COMPANY
 where CONTROL_NAME = 'Number of records to Archive';
 
if sqlca.SQLCode <> 0 then
   f_pp_msgs("  ")
	f_pp_msgs("ERROR: Getting Control Value for the Number of Records to Archive in one transaction." + &
             string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
   rollback;
   return -1
end if	
 
select count(*)
  into :ll_total_records
  from ARCHIVE_WO_AVAILABLE_LIST
 where ARCHIVE_STATUS = 'PROCESS'
   and JOB_ID = to_char(:al_job_id)
	and ARCHIVE_TYPE = 'STANDARD';

if sqlca.SQLCode <> 0 then
   f_pp_msgs("  ")
	f_pp_msgs("ERROR: Getting record count for job: " + string(al_job_id) + " : " + &
             string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
   rollback;
   return -1
end if

ll_set_num = 0
f_pp_msgs("*******************************************")
f_pp_msgs("Start of Archive job " + string(al_job_id))
f_pp_msgs("*******************************************")

do while ll_total_records > 0
   ll_total_records = ll_total_records - ll_rec_cnt_to_process
   ll_set_num = ll_set_num + 1	
   // Archive all records for a given Job and Archive Type commit in increments of ll_rec_cnt_to_process records.

	f_pp_msgs("Start of Archive Set " + string(ll_set_num) + " in job " + string(al_job_id))

   update ARCHIVE_WO_AVAILABLE_LIST
      set ARCHIVE_STATUS = 'PROCESSING' 
    where ARCHIVE_STATUS = 'PROCESS'
      and JOB_ID = to_char(:al_job_id)
   	and ARCHIVE_TYPE = 'STANDARD'
   	and rownum <= :ll_rec_cnt_to_process;

   if sqlca.SQLCode <> 0 then
      f_pp_msgs("  ")
   	f_pp_msgs("ERROR: Updating ARCHIVE_STATUS to PROCESSING for job: " + string(al_job_id) + " : " + &
                string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
    	f_pp_msgs("  ")
      rollback;
      return -1
	end if
   commit;
	
  // Get Table Name to Archive for the 'STANDARD' Archive Type

   declare cur_archive_tables cursor for
      select ATL.TABLE_NAME, ATL.ARCHIVE_TABLE_NAME, ATL.STANDARD_IND_EXISTS_CLAUSE
        from ARCHIVE_TABLE_LIST ATL,
		     ALL_TABLES AT
   	 where ATL.SUBSYSTEM = 'WORK ORDER'
   	   and ATL.STANDARD_IND = 1
	   and ATL.TABLE_NAME = AT.TABLE_NAME
         and AT.OWNER ='PWRPLANT'
   	 order by ATL.POSITION desc, ATL.TABLE_NAME;

   open cur_archive_tables;
   fetch cur_archive_tables
      into :ls_table, :ls_archive_table, :ls_standard_ind_exists_clause;
   
	//********************************
	// Loop over all tables to Archive
	//********************************
   do while sqlca.sqlcode = 0
		if ls_standard_ind_exists_clause <> '' then
   		// Get exists table name
         longlong ll_start_pos, ll_end_pos
         string ls_from_table_name
         // Replace all commas with spaces
	   	ls_sql = ls_standard_ind_exists_clause
         // Find the first occurrence of a comma.
         ll_start_pos = Pos(ls_sql, ",", ll_start_pos)
         // Only enter the loop if you find a comma.
         DO WHILE ll_start_pos > 0
            // Replace comma with a space.
            ls_sql = Replace(ls_sql, ll_start_pos, 1, " ")
            // Find the next occurrence of a comma.
            ll_start_pos = Pos(ls_sql, ",", ll_start_pos + 1)
         LOOP

         ll_start_pos = lastpos(upper(ls_sql), 'FROM') + 5
         ll_end_pos = pos(ls_sql, " ", ll_start_pos) - ll_start_pos
         ls_from_table_name = mid(ls_sql, ll_start_pos, ll_end_pos)
		else
			ls_from_table_name = ls_table
		end if
      //***********************
		// Build insert statement
		//***********************
      ls_column_list = uf_get_column_list(ls_table)
		ls_sql= "insert into " + ls_archive_table + " (" + ls_column_list + ") select " + ls_column_list + " from " + ls_table + " where "
		// If not an exists clause then add work_order_id clause so insert will fail if table doesn't have work_order_id column
		// In other words the table should have an exists clause if it doesn't have a work_order_id column.
		if ls_standard_ind_exists_clause = '' or isnull(ls_standard_ind_exists_clause) then
			ls_sql = ls_sql + "WORK_ORDER_ID is not null and exists ("
      else	// If there is an exists clause
			ls_sql = ls_sql + ls_standard_ind_exists_clause + "("
		end if
		// Add generic exists clause
		ls_sql = ls_sql + "select WORK_ORDER_ID from ARCHIVE_WO_AVAILABLE_LIST where ARCHIVE_STATUS = 'PROCESSING' " +&
		                  "and JOB_ID = " + string(al_job_id) + " and ARCHIVE_TYPE = 'STANDARD' " +&
		                  "and " + ls_from_table_name + ".WORK_ORDER_ID = ARCHIVE_WO_AVAILABLE_LIST.WORK_ORDER_ID)"
		if not ls_standard_ind_exists_clause = "" then
			ls_sql = ls_sql + ")"
		end if
		
		//*****************************
		// Special Case - CO_TENANCY_WO
		//*****************************
		if ls_table = 'CO_TENANCY_WO' then
			ls_sql2 = f_replace_string(ls_sql, 'CO_TENANCY_WO.WORK_ORDER_ID', 'CO_TENANCY_WO.CO_TENANT_WO', 'first')
         f_write_log(g_log_file, ls_sql2 + ';')	
		   execute immediate :ls_sql2;
		   if sqlca.sqlcode = -1 then
			   f_pp_msgs("SQL Error: Could not insert into " + ls_archive_table)
		   	f_pp_msgs("SQL Statement: " + ls_sql)
   			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
	   		rollback;
		   	f_pp_msgs("************************************************************")			
		   	f_pp_msgs("Transaction rolled back for Set " + string(ll_set_num) + " of Job " + string(al_job_id))			
		   	f_pp_msgs("************************************************************")			

   			// Mark the records as Failed
	   		update ARCHIVE_WO_AVAILABLE_LIST
		   		set ARCHIVE_STATUS = 'FAILED' 
			    where ARCHIVE_STATUS = 'PROCESSING'
				   and JOB_ID = to_char(:al_job_id)
   				and ARCHIVE_TYPE = 'STANDARD';
            // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
				
				ll_set_error = 1
				exit
	   		// return -1
   		else
      		f_pp_msgs(string(sqlca.SQLNRows) + " Rows were inserted into the " + ls_archive_table + " for Set " +&
		    	                                     string(ll_set_num) + " of Job " + string(al_job_id))
            // Don't check inserted count for special case
   			//ll_inserted_rows2 = sqlca.SQLNRows															 
      	end if
		end if
		//***********************************
		// END - Special Case - CO_TENANCY_WO
		//***********************************
			
	   //**************************
		// Insert into Archive table
		//**************************
      f_write_log(g_log_file, ls_sql+ ';')	
		execute immediate :ls_sql;
	
		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not insert into " + ls_archive_table)
			f_pp_msgs("SQL Statement: " + ls_sql)
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
	   	f_pp_msgs("************************************************************")			
	   	f_pp_msgs("Transaction rolled back for Set " + string(ll_set_num) + " of Job " + string(al_job_id))			
	   	f_pp_msgs("************************************************************")			
			
			// Mark the records as Failed
			update ARCHIVE_WO_AVAILABLE_LIST
				set ARCHIVE_STATUS = 'FAILED' 
			 where ARCHIVE_STATUS = 'PROCESSING'
				and JOB_ID = to_char(:al_job_id)
				and ARCHIVE_TYPE = 'STANDARD';
            // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
				ll_set_error = 1
				exit
	   		// return -1
		else
   		f_pp_msgs(string(sqlca.SQLNRows) + " Rows were inserted into the " + ls_archive_table + " for Set " +&
			                                     string(ll_set_num) + " of Job " + string(al_job_id))
			ll_inserted_rows = sqlca.SQLNRows															 
   	end if
		
		//***********************
		// Build delete statement
		//***********************
		ls_sql = "delete from " + ls_table + " where "
		if not ls_standard_ind_exists_clause = "" or not isnull(ls_standard_ind_exists_clause) then
			ls_sql = ls_sql + ls_standard_ind_exists_clause + "("
		else
			ls_sql = ls_sql + "exists ("
		end if
		// Add generic exists clause
		ls_sql = ls_sql + "select WORK_ORDER_ID from ARCHIVE_WO_AVAILABLE_LIST where ARCHIVE_STATUS = 'PROCESSING'" +&
		                  " and JOB_ID = " + string(al_job_id) + " and ARCHIVE_TYPE = 'STANDARD' " +&
                        "and " + ls_from_table_name + ".WORK_ORDER_ID = ARCHIVE_WO_AVAILABLE_LIST.WORK_ORDER_ID)"
		if not ls_standard_ind_exists_clause = "" or not isnull(ls_standard_ind_exists_clause) then
			ls_sql = ls_sql + ")"
		end if
       
		//*****************************
		// Special Case - CO_TENANCY_WO
		//*****************************
		if ls_table = 'CO_TENANCY_WO' then
			ls_sql2 = f_replace_string(ls_sql, 'CO_TENANCY_WO.WORK_ORDER_ID', 'CO_TENANCY_WO.CO_TENANT_WO', 'first')
         f_write_log(g_log_file, ls_sql2 + ';')	
		   execute immediate :ls_sql2;
		
			if sqlca.sqlcode = -1 then
				f_pp_msgs("SQL Error: Could not delete from " + ls_table)
				f_pp_msgs("SQL Statement: " + ls_sql)
				f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
				rollback;
		   	f_pp_msgs("************************************************************")			
   	   	f_pp_msgs("Transaction rolled back for Set " + string(ll_set_num) + " of Job " + string(al_job_id))			
		   	f_pp_msgs("************************************************************")			
	
				// Mark the records as Failed
				update ARCHIVE_WO_AVAILABLE_LIST
					set ARCHIVE_STATUS = 'FAILED' 
				 where ARCHIVE_STATUS = 'PROCESSING'
					and JOB_ID = to_char(:al_job_id)
					and ARCHIVE_TYPE = 'STANDARD';

            // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
				ll_set_error = 1				
				exit
	   		// return -1
			else
				f_pp_msgs(string(sqlca.SQLNRows) + " Rows were deleted from the " + ls_table)
//				// Don't check deleted count for special case
//				// ll_deleted_rows2 = sqlca.SQLNRows
//				if ll_deleted_rows2 <> ll_inserted_rows2 then
//					f_pp_msgs("SQL Error: Inserted rows: " + string(ll_inserted_rows2) + " Deleted rows: " + string(ll_deleted_rows2))
//					f_pp_msgs("SQL Error: Deleted record count doesn't match Inserted record count for table: " + ls_table)
//					f_pp_msgs("SQL Statement: " + ls_sql)
//					f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
//					rollback;
//   		   	f_pp_msgs("************************************************************")			
//      	   	f_pp_msgs("Transaction rolled back for Set " + string(ll_set_num) + " of Job " + string(al_job_id))			
//   		   	f_pp_msgs("************************************************************")			
//	
//					// Mark the records as Failed
//					update ARCHIVE_WO_AVAILABLE_LIST
//						set ARCHIVE_STATUS = 'FAILED' 
//					 where ARCHIVE_STATUS = 'PROCESSING'
//						and JOB_ID = to_char(:al_job_id)
//						and ARCHIVE_TYPE = 'STANDARD';
//         
//			      // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
//				   ll_set_error = 1					
//	   			exit
//	      		// return -1
//				end if
			end if
		end if		
		
		//***********************************
		// END - Special Case - CO_TENANCY_WO
		//***********************************
								
		//***********************
		// Delete from Base table
		//***********************
      f_write_log(g_log_file, ls_sql+ ';')			
		execute immediate :ls_sql;
		
		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not delete from " + ls_table)
			f_pp_msgs("SQL Statement: " + ls_sql)
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
	   	f_pp_msgs("************************************************************")			
  	   	f_pp_msgs("Transaction rolled back for Set " + string(ll_set_num) + " of Job " + string(al_job_id))			
	   	f_pp_msgs("************************************************************")			

			// Mark the records as Failed
			update ARCHIVE_WO_AVAILABLE_LIST
				set ARCHIVE_STATUS = 'FAILED' 
			 where ARCHIVE_STATUS = 'PROCESSING'
				and JOB_ID = to_char(:al_job_id)
				and ARCHIVE_TYPE = 'STANDARD';

         // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
			ll_set_error = 1
   		exit
	  		// return -1

		else
   		f_pp_msgs(string(sqlca.SQLNRows) + " Rows were deleted from the " + ls_table)
			ll_deleted_rows = sqlca.SQLNRows
			// Don't check Special Case
			if ls_table <> "CO_TENANCY_WO" then
   			if ll_deleted_rows <> ll_inserted_rows then
	   			f_pp_msgs("SQL Error: Inserted rows: " + string(ll_inserted_rows) + " Deleted rows: " + string(ll_deleted_rows))				
               f_pp_msgs("SQL Error: Deleted record count doesn't match Inserted record count for table: " + ls_table)
			      f_pp_msgs("SQL Statement: " + ls_sql)
			      f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
   			   rollback;
	   	   	f_pp_msgs("************************************************************")			
    	      	f_pp_msgs("Transaction rolled back for Set " + string(ll_set_num) + " of Job " + string(al_job_id))			
		      	f_pp_msgs("************************************************************")			

   			   // Mark the records as Failed
	   		   update ARCHIVE_WO_AVAILABLE_LIST
		   	      set ARCHIVE_STATUS = 'FAILED' 
			       where ARCHIVE_STATUS = 'PROCESSING'
				      and JOB_ID = to_char(:al_job_id)
				      and ARCHIVE_TYPE = 'STANDARD';
   			
               // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
		   		ll_set_error = 1
   		      exit
	  		      // return -1
   			end if
			end if
		end if
		
   fetch cur_archive_tables
      into :ls_table, :ls_archive_table, :ls_standard_ind_exists_clause;
   loop
	
	close cur_archive_tables;
	
   update ARCHIVE_WO_AVAILABLE_LIST
      set ARCHIVE_STATUS = 'ARCHIVED' 
    where ARCHIVE_STATUS = 'PROCESSING'
      and JOB_ID = to_char(:al_job_id)
   	and ARCHIVE_TYPE = 'STANDARD';

	if sqlca.sqlcode = -1 then
		f_pp_msgs("SQL Error: Could not update ARCHIVE_WO_AVAILABLE_LIST to 'ARCHIVED' status.")
		f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
		rollback;
		f_pp_msgs("Transaction rolled back.")
		
		return -1
	end if

	insert into ARC_ARCHIVE_WO_AVAILABLE_LIST
		(WORK_ORDER_ID, JOB_ID, ARCHIVE_STATUS, ARCHIVE_TYPE, WORK_ORDER_NUMBER, CWIP_BALANCE,
		 RWIP_BALANCE, CWIP_MONTH_NUMBER, RWIP_MONTH_NUMBER, ARCHIVE_FLAG, NON_UNIT_BALANCE,
		 OTHER_BALANCE, NON_UNIT_MONTH_NUMBER, OTHER_MONTH_NUMBER, NUM_CHARGES, LONG_DESCRIPTION,
		 TIME_STAMP, USER_ID,  TAX_BALANCE, ORIGINAL_COST_RETIRE_BALANCE, CREATED_DATE, APPROVED_DATE)
		(select WORK_ORDER_ID,
				  JOB_ID,
				  ARCHIVE_STATUS,
				  ARCHIVE_TYPE,
				  WORK_ORDER_NUMBER,
				  CWIP_BALANCE,
				  RWIP_BALANCE,
				  CWIP_MONTH_NUMBER,
				  RWIP_MONTH_NUMBER,
				  ARCHIVE_FLAG,
				  NON_UNIT_BALANCE,
				  OTHER_BALANCE,
				  NON_UNIT_MONTH_NUMBER,
				  OTHER_MONTH_NUMBER,
				  NUM_CHARGES,
				  LONG_DESCRIPTION,
				  TIME_STAMP,
				  USER_ID, 
				  TAX_BALANCE,
				  ORIGINAL_COST_RETIRE_BALANCE,
				  CREATED_DATE,
				  APPROVED_DATE
			from ARCHIVE_WO_AVAILABLE_LIST
		  where ARCHIVE_STATUS = 'ARCHIVED');
 
	if sqlca.sqlcode = -1 then
		f_pp_msgs("SQL Error: Could not update insert into ARC_ARCHIVE_WO_AVAILABLE_LIST archived records.")
		f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
		rollback;
		f_pp_msgs("Transaction rolled back.")						
		
		return -1
	end if
		  
	delete from ARCHIVE_WO_AVAILABLE_LIST
		where ARCHIVE_STATUS = 'ARCHIVED';

	if sqlca.sqlcode = -1 then
		f_pp_msgs("SQL Error: Could not delete from ARCHIVE_WO_AVAILABLE_LIST archived records.")
		f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
		rollback;
		f_pp_msgs("Transaction rolled back.")	
		
		return -1
	end if
		
	commit;
   
	if ll_set_error = 1 then
      f_pp_msgs("*********************************************")
      f_pp_msgs("Set " + string(ll_set_num) + " in Job " + string(al_job_id) + " failed.")
      f_pp_msgs("*********************************************")
		ll_job_error = 1
   else
      f_pp_msgs("*********************************************")
   	f_pp_msgs("All tables have been Archived for Set " + string(ll_set_num) + " in Job " + string(al_job_id))
      f_pp_msgs("*********************************************")
   end if	
	ll_set_error = 0
loop	

if ll_job_error = 1 then
   f_pp_msgs("*********************************************")
   f_pp_msgs("Some Sets in Job " + string(al_job_id) + " had errors.  If Sets existed without errors then they should have processed.")
   f_pp_msgs("*********************************************")	
   il_job_error = 1
else	
   f_pp_msgs("*********************************************")
   f_pp_msgs("All Sets in Job " + string(al_job_id) + " have been archived.")
   f_pp_msgs("*********************************************")	
end if	

return 1

end function

public function integer uf_archive_blanket_type (longlong al_job_id);string ls_table, ls_archive_table, ls_blanket_ind_exists_clause
string ls_column, ls_column_list, ls_sql, ls_disable_constraints
longlong ll_num_columns, ll_rec_cnt_to_process, ll_inserted_rows, ll_deleted_rows
longlong ll_total_records, ll_records_processed, ll_set_num, ll_set_error, ll_month_error, ll_job_error
boolean lb_first_column

ll_set_error = 0
ll_month_error = 0
ll_job_error = 0

// Get number of records to process as one transaction.
select upper(CONTROL_VALUE)
  into :ll_rec_cnt_to_process
  from PP_SYSTEM_CONTROL_COMPANY
 where CONTROL_NAME = 'Number of records to Archive';
 
if sqlca.SQLCode <> 0 then
   f_pp_msgs("  ")
	f_pp_msgs("ERROR: Getting Control Value for the Number of Records to Archive in one transaction." + &
             string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
   rollback;
   return -1
end if	
 
f_pp_msgs("*******************************************")
f_pp_msgs("Start of Blanket Archiving job " + string(al_job_id))
f_pp_msgs("*******************************************")
 
// Get Distinct List of Month Numbers for all BLANKETS in the job
	
any la_archive_monthnumber_array[]
longlong ll_archive_monthnumber_array[]
longlong j, ll_month_number
	
ls_sql = "select distinct BLANKET_MONTH_NUMBER" +&
         "  from ARCHIVE_WO_AVAILABLE_LIST" +&
         " where JOB_ID = " + string(al_job_id) +&
         "   and ARCHIVE_STATUS = 'PROCESS'" +&
         "   and ARCHIVE_TYPE = 'BLANKET'" +&
   	   " order by BLANKET_MONTH_NUMBER"

f_get_column(la_archive_monthnumber_array, ls_sql)

ll_archive_monthnumber_array = la_archive_monthnumber_array

ll_set_num = 0
	
f_pp_msgs("*******************************************")
f_pp_msgs("Start of Archive job " + string(al_job_id))
f_pp_msgs("*******************************************")

for j = 1 to upperbound(ll_archive_monthnumber_array)
   // Process records in sets by Month Number.
   ll_month_number = ll_archive_monthnumber_array[j]	

	select count(*)
	  into :ll_total_records
	  from ARCHIVE_WO_AVAILABLE_LIST
	 where ARCHIVE_STATUS = 'PROCESS'
		and JOB_ID = to_char(:al_job_id)
		and ARCHIVE_TYPE = 'BLANKET'
		and BLANKET_MONTH_NUMBER = :ll_month_number;
	
	if sqlca.SQLCode <> 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: Getting record count for month: " + string(ll_month_number) + " in job: " + string(al_job_id) + " : " + &
					 string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		rollback;
		return -1
	end if

   f_pp_msgs("*******************************************")
   f_pp_msgs("Processing Job:" + string(al_job_id) + " Month: " + string(ll_month_number))
   f_pp_msgs("*******************************************")
	
	do while ll_total_records > 0
		ll_total_records = ll_total_records - ll_rec_cnt_to_process
		ll_set_num = ll_set_num + 1	
		// Archive all records for a given Job, month_number, Blanket Type commit in increments of ll_rec_cnt_to_process records.
	
		f_pp_msgs("Start of Archive Set " + string(ll_set_num) + " in job " + string(al_job_id))
	
		update ARCHIVE_WO_AVAILABLE_LIST
			set ARCHIVE_STATUS = 'PROCESSING' 
		 where ARCHIVE_STATUS = 'PROCESS'
			and JOB_ID = to_char(:al_job_id)
			and ARCHIVE_TYPE = 'BLANKET'
   		and BLANKET_MONTH_NUMBER = :ll_month_number			
			and rownum <= :ll_rec_cnt_to_process;
	
		if sqlca.SQLCode <> 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating ARCHIVE_STATUS to PROCESSING for job: " + string(al_job_id) + " : " + &
						 string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if

		commit;
	   
		// Create insert for ARCHIVE_WO_CHARGE_ID_TEMP table - Table of Charges	
   	ls_sql = "insert into ARCHIVE_WO_CHARGE_ID_TEMP (WORK_ORDER_ID, CHARGE_ID) " +&
		         "   select CC.WORK_ORDER_ID, CC.CHARGE_ID " +&
               "     from CWIP_CHARGE CC, " +&
					"          WORK_ORDER_CHARGE_GROUP WOCG " +&
               "    where MONTH_NUMBER <= " + string(ll_month_number) + " " +&
					"      and CC.WORK_ORDER_ID = WOCG.WORK_ORDER_ID " +&
					"      and CC.CHARGE_ID = WOCG.CHARGE_ID " +&
					"      and exists " +&
               "    (select WORK_ORDER_ID " +&
               "       from ARCHIVE_WO_AVAILABLE_LIST AWAL " +&
               "      where ARCHIVE_STATUS = 'PROCESSING' " +&
               "        and JOB_ID = " + string(al_job_id) +&
               "        and ARCHIVE_TYPE = 'BLANKET' " +&
               "        and BLANKET_MONTH_NUMBER = " + string(ll_month_number) +&
               "        and CC.WORK_ORDER_ID = AWAL.WORK_ORDER_ID)"
					
      f_write_log(g_log_file, ls_sql+ ';')				
		execute immediate :ls_sql;
		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not insert into ARCHIVE_WO_CHARGE_ID_TEMP")
			f_pp_msgs("SQL Statement: " + ls_sql)
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("Transaction rolled back.")
	
			// Mark the records as Failed
			update ARCHIVE_WO_AVAILABLE_LIST
				set ARCHIVE_STATUS = 'FAILED' 
			 where ARCHIVE_STATUS = 'PROCESSING'
				and JOB_ID = to_char(:al_job_id)
				and ARCHIVE_TYPE = 'BLANKET';
	
			return -1
		else
			f_pp_msgs(string(sqlca.SQLNRows) + " Rows were inserted into the ARCHIVE_WO_CHARGE_ID_TEMP for Month Number: " + string(ll_month_number) + " in Set: " + string(ll_set_num))
		end if	
					
      // Create insert for ARCHIVE_WO_UNIT_ITEM_TEMP table	- Table of Charge Groups and Unit Items			
		delete from ARCHIVE_WO_UNIT_ITEM_TEMP;
/*
   	ls_sql = "insert into ARCHIVE_WO_UNIT_ITEM_TEMP (WORK_ORDER_ID, CHARGE_GROUP_ID, UNIT_ITEM_ID) "+&
		         "   select distinct WORK_ORDER_ID, CHARGE_GROUP_ID, UNIT_ITEM_ID " +&
					"     from CHARGE_GROUP_CONTROL " +&
					"    where exists(select WORK_ORDER_ID, CHARGE_GROUP_ID " +&
					"                   from ARCHIVE_WO_CHARGE_ID_TEMP " +&
					"                  where CHARGE_GROUP_CONTROL.WORK_ORDER_ID = ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID " +&
					"                    and CHARGE_GROUP_CONTROL.CHARGE_GROUP_ID = ARCHIVE_WO_CHARGE_ID_TEMP.CHARGE_GROUP_ID)"
*/
	
	   ls_sql = "insert into ARCHIVE_WO_UNIT_ITEM_TEMP (CHARGE_GROUP_ID, WORK_ORDER_ID, UNIT_ITEM_ID)        " +&
		         "select CHARGE_GROUP_ID, WORK_ORDER_ID, UNIT_ITEM_ID                                         " +&
					"  from CHARGE_GROUP_CONTROL                                                                 " +&
					" where exists (select WORK_ORDER_ID                                                         " +&
					"          from ARCHIVE_WO_CHARGE_ID_TEMP                                                    " +&
					"         where CHARGE_GROUP_CONTROL.WORK_ORDER_ID = ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID)" +&
					"   and not exists (select distinct WORK_ORDER_ID, CHARGE_GROUP_ID                           " +&
					"          from (select WORK_ORDER_ID, CHARGE_ID, CHARGE_GROUP_ID                            " +&
					"                  from WORK_ORDER_CHARGE_GROUP                                              " +&
					"                 where exists (select WORK_ORDER_ID                                         " +&
					"                          from ARCHIVE_WO_CHARGE_ID_TEMP                                    " +&
					"                         where WORK_ORDER_CHARGE_GROUP.WORK_ORDER_ID =                      " +&
					"                               ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID)                     " +&
					"                minus                                                                       " +&
					"                select WORK_ORDER_ID, CHARGE_ID, CHARGE_GROUP_ID                            " +&
					"                  from WORK_ORDER_CHARGE_GROUP                                              " +&
					"                 where exists (select WORK_ORDER_ID                                         " +&
					"                          from ARCHIVE_WO_CHARGE_ID_TEMP                                    " +&
					"                         where WORK_ORDER_CHARGE_GROUP.WORK_ORDER_ID =                      " +&
					"                               ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID                      " +&
					"                           and WORK_ORDER_CHARGE_GROUP.CHARGE_ID =                          " +&
					"                               ARCHIVE_WO_CHARGE_ID_TEMP.CHARGE_ID)) T1                     " +&
					"         where T1.WORK_ORDER_ID = CHARGE_GROUP_CONTROL.WORK_ORDER_ID                        " +&
					"           and T1.CHARGE_GROUP_ID = CHARGE_GROUP_CONTROL.CHARGE_GROUP_ID)                   " +&
					"                                                                                            "

      f_write_log(g_log_file, ls_sql + ';')				
		execute immediate :ls_sql;

		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not insert into ARCHIVE_WO_UNIT_ITEM_TEMP")
			f_pp_msgs("SQL Statement: " + ls_sql)
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("Transaction rolled back.")
	
			// Mark the records as Failed
			update ARCHIVE_WO_AVAILABLE_LIST
				set ARCHIVE_STATUS = 'FAILED' 
			 where ARCHIVE_STATUS = 'PROCESSING'
				and JOB_ID = to_char(:al_job_id)
				and ARCHIVE_TYPE = 'BLANKET';
	
			return -1
		else
			f_pp_msgs(string(sqlca.SQLNRows) + " Rows were inserted into the ARCHIVE_WO_UNIT_ITEM_TEMP for Month Number: " + string(ll_month_number) + " in Set: " + string(ll_set_num))
			ll_inserted_rows = sqlca.SQLNRows															 			
		end if	
		
		
		ls_sql = "insert into ARCHIVE_WO_UNIT_ITEM_TEMP                                                          " +&
					"   (CHARGE_GROUP_ID, WORK_ORDER_ID, UNIT_ITEM_ID)                                              " +&
					"   select CHARGE_GROUP_ID, WORK_ORDER_ID, UNIT_ITEM_ID                                         " +&
					"     from CHARGE_GROUP_CONTROL                                                                 " +&
					"    where exists                                                                               " +&
					"    (select WORK_ORDER_ID                                                                      " +&
					"             from ARCHIVE_WO_CHARGE_ID_TEMP                                                    " +&
					"            where CHARGE_GROUP_CONTROL.WORK_ORDER_ID = ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID)" +&
					"      and exists                                                                               " +&
					"    (select T1.WORK_ORDER_ID, T1.UNIT_ITEM_ID                                                  " +&
					"             from UNITIZED_WORK_ORDER T1, WO_UNIT_ITEM_PEND_TRANS T2                           " +&
					"            where T1.WORK_ORDER_ID = T2.WORK_ORDER_ID                                          " +&
					"              and T1.UNIT_ITEM_ID = T2.UNIT_ITEM_ID                                            " +&
					"              and CHARGE_GROUP_CONTROL.WORK_ORDER_ID = T1.WORK_ORDER_ID                        " +&
					"              and CHARGE_GROUP_CONTROL.UNIT_ITEM_ID = T1.UNIT_ITEM_ID                          " +&
					"              and exists                                                                       " +&
					"            (select distinct WORK_ORDER_ID                                                     " +&
					"                     from ARCHIVE_WO_CHARGE_ID_TEMP                                            " +&
					"                    where T1.WORK_ORDER_ID = ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID))         " +&
					"   minus                                                                                       " +&
					"   select CHARGE_GROUP_ID, WORK_ORDER_ID, UNIT_ITEM_ID from ARCHIVE_WO_UNIT_ITEM_TEMP          "

																																						

      f_write_log(g_log_file, ls_sql + ';')				
		execute immediate :ls_sql;

		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not insert into ARCHIVE_WO_UNIT_ITEM_TEMP step 2")
			f_pp_msgs("SQL Statement: " + ls_sql)
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("Transaction rolled back.")
	
			// Mark the records as Failed
			update ARCHIVE_WO_AVAILABLE_LIST
				set ARCHIVE_STATUS = 'FAILED' 
			 where ARCHIVE_STATUS = 'PROCESSING'
				and JOB_ID = to_char(:al_job_id)
				and ARCHIVE_TYPE = 'BLANKET';
	
			return -1
		else
			f_pp_msgs(string(sqlca.SQLNRows) + " Rows were inserted for Step 2 into the ARCHIVE_WO_UNIT_ITEM_TEMP for Month Number: " + string(ll_month_number) + " in Set: " + string(ll_set_num))
			ll_inserted_rows = sqlca.SQLNRows															 			
		end if			
//////Use Pend Transaction Archive for unit items that did not originate from CWIP Charge
////		ls_sql = "insert into ARCHIVE_WO_UNIT_ITEM_TEMP (CHARGE_GROUP_ID, WORK_ORDER_ID, UNIT_ITEM_ID)        " +&
////		         "select CHARGE_GROUP_ID, WORK_ORDER_ID, UNIT_ITEM_ID                                         " +&
////					"  from CHARGE_GROUP_CONTROL                                                                 " +&
////					" where exists (select WORK_ORDER_ID                                                         " +&
////					"          from ARCHIVE_WO_CHARGE_ID_TEMP                                                    " +&
////					"         where CHARGE_GROUP_CONTROL.WORK_ORDER_ID = ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID)" +&
////					"   and not exists (select distinct WORK_ORDER_ID, CHARGE_GROUP_ID                           " +&
////					"          from (select WORK_ORDER_ID, CHARGE_ID, CHARGE_GROUP_ID                            " +&
////					"                  from WORK_ORDER_CHARGE_GROUP                                              " +&
////					"                 where exists (select WORK_ORDER_ID                                         " +&
////					"                          from ARCHIVE_WO_CHARGE_ID_TEMP                                    " +&
////					"                         where WORK_ORDER_CHARGE_GROUP.WORK_ORDER_ID =                      " +&
////					"                               ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID)                     " +&
////					"                minus                                                                       " +&
////					"                select WORK_ORDER_ID, CHARGE_ID, CHARGE_GROUP_ID                            " +&
////					"                  from WORK_ORDER_CHARGE_GROUP                                              " +&
////					"                 where exists (select WORK_ORDER_ID                                         " +&
////					"                          from ARCHIVE_WO_CHARGE_ID_TEMP                                    " +&
////					"                         where WORK_ORDER_CHARGE_GROUP.WORK_ORDER_ID =                      " +&
////					"                               ARCHIVE_WO_CHARGE_ID_TEMP.WORK_ORDER_ID                      " +&
////					"                           and WORK_ORDER_CHARGE_GROUP.CHARGE_ID =                          " +&
////					"                               ARCHIVE_WO_CHARGE_ID_TEMP.CHARGE_ID)) T1                     " +&
////					"         where T1.WORK_ORDER_ID = CHARGE_GROUP_CONTROL.WORK_ORDER_ID                        " +&
////					"           and T1.CHARGE_GROUP_ID = CHARGE_GROUP_CONTROL.CHARGE_GROUP_ID)                   " +&
////					"                                                                                            "
////
////      f_write_log(g_log_file, ls_sql)				
////		execute immediate :ls_sql;
////
////		if sqlca.sqlcode = -1 then
////			f_pp_msgs("SQL Error: Could not insert into ARCHIVE_WO_UNIT_ITEM_TEMP")
////			f_pp_msgs("SQL Statement: " + ls_sql)
////			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
////			rollback;
////			f_pp_msgs("Transaction rolled back.")
////	
////			// Mark the records as Failed
////			update ARCHIVE_WO_AVAILABLE_LIST
////				set ARCHIVE_STATUS = 'FAILED' 
////			 where ARCHIVE_STATUS = 'PROCESSING'
////				and JOB_ID = to_char(:al_job_id)
////				and ARCHIVE_TYPE = 'BLANKET';
////	
////			return -1
////		else
////			f_pp_msgs(string(sqlca.SQLNRows) + " Rows were inserted into the ARCHIVE_WO_UNIT_ITEM_TEMP for Month Number: " + string(ll_month_number) + " in Set: " + string(ll_set_num))
////		end if	
		// Get Table Name to Archive for the 'BLANKET' Archive Type
	
		declare cur_archive_tables cursor for
			select TABLE_NAME, ARCHIVE_TABLE_NAME, BLANKET_IND_EXISTS_CLAUSE
			  from ARCHIVE_TABLE_LIST
			 where SUBSYSTEM = 'WORK ORDER'
				and BLANKET_IND = 1
			 order by POSITION desc, TABLE_NAME;
	
		open cur_archive_tables;
		fetch cur_archive_tables
			into :ls_table, :ls_archive_table, :ls_blanket_ind_exists_clause;

	   //*******************************
	   //Loop over all tables to Archive
   	//*******************************
		do while sqlca.sqlcode = 0

			ls_column_list = uf_get_column_list(ls_table)			
			//************************************
   		// Special Case - CHARGE_GROUP_CONTROL
			//************************************
			if ls_table = 'CHARGE_GROUP_CONTROL' then
				ls_sql = "insert into " + ls_archive_table + " (" + ls_column_list + ") " +&
				        "select " + ls_column_list +&
						  "  from " + ls_table +&
						  " where exists (select WORK_ORDER_ID, CHARGE_GROUP_ID " +&
						  "                 from ARCHIVE_WO_UNIT_ITEM_TEMP" +&
                    "                where ARCHIVE_WO_UNIT_ITEM_TEMP.WORK_ORDER_ID = " + ls_table + ".WORK_ORDER_ID" +&
						  "                  and ARCHIVE_WO_UNIT_ITEM_TEMP.CHARGE_GROUP_ID = " + ls_table + ".CHARGE_GROUP_ID)"
			elseif ls_blanket_ind_exists_clause = 'Special Case - UNIT_ITEM_ID' then
	   		//create Archive table insert statements for Unit Item related tables
				ls_sql= "insert into " + ls_archive_table + " (" + ls_column_list + ") " +&
				        "select " + ls_column_list +&
						  "  from " + ls_table +&
						  " where exists (select WORK_ORDER_ID, UNIT_ITEM_ID " +&
						  "                 from ARCHIVE_WO_UNIT_ITEM_TEMP" +&
                    "                where ARCHIVE_WO_UNIT_ITEM_TEMP.WORK_ORDER_ID = " + ls_table + ".WORK_ORDER_ID" +&
						  "                  and ARCHIVE_WO_UNIT_ITEM_TEMP.UNIT_ITEM_ID = " + ls_table + ".UNIT_ITEM_ID)"
						  
			elseif ls_blanket_ind_exists_clause = 'Special Case - CHARGE_ID' then
	   		//create Archive table insert statements for Charge related tables
				ls_sql= "insert into " + ls_archive_table + " (" + ls_column_list + ") " +&
				        "select " + ls_column_list +&
						  "  from " + ls_table +&
						  " where exists (select CHARGE_ID " +&
						  "                 from ARCHIVE_WO_CHARGE_ID_TEMP" +&
                    "                where ARCHIVE_WO_CHARGE_ID_TEMP.CHARGE_ID = " + ls_table + ".CHARGE_ID)"
			else			
				if ls_blanket_ind_exists_clause <> '' then
					// Get exists table name
					longlong ll_start_pos, ll_end_pos
					string ls_from_table_name
					// Replace all commas with spaces
					ls_sql = ls_blanket_ind_exists_clause
					// Find the first occurrence of a comma.
					ll_start_pos = Pos(ls_sql, ",", ll_start_pos)
					// Only enter the loop if you find a comma.
					DO WHILE ll_start_pos > 0
						// Replace comma with a space.
						ls_sql = Replace(ls_sql, ll_start_pos, 1, " ")
						// Find the next occurrence of a comma.
						ll_start_pos = Pos(ls_sql, ",", ll_start_pos + 1)
					LOOP
		
					ll_start_pos = lastpos(upper(ls_sql), 'FROM') + 5
					ll_end_pos = pos(ls_sql, " ", ll_start_pos) - ll_start_pos
					ls_from_table_name = mid(ls_sql, ll_start_pos, ll_end_pos)
				else
					ls_from_table_name = ls_table
				end if
		
            //***********************
		      // Build insert statement
	   	   //***********************

				ls_sql= "insert into " + ls_archive_table + " (" + ls_column_list + ") select " + ls_column_list + " from " + ls_table + " where "
				// If not an exists clause then add work_order_id clause so insert will fail if table doesn't have work_order_id column
				// In other words the table should have an exists clause if it doesn't have a work_order_id column.
				if ls_blanket_ind_exists_clause = '' or isnull(ls_blanket_ind_exists_clause) then
					ls_sql = ls_sql + "WORK_ORDER_ID is not null and MONTH_NUMBER <= " + string(ll_month_number) + " and exists ("
				else	// If there is an exists clause
					ls_sql = ls_sql + f_replace_string(ls_blanket_ind_exists_clause,"<MONTH_NUMBER>",string(ll_month_number),"FIRST") + "("
				end if
				// Add generic exists clause
				ls_sql = ls_sql + "select WORK_ORDER_ID from ARCHIVE_WO_AVAILABLE_LIST where ARCHIVE_STATUS = 'PROCESSING' " +&
										"and JOB_ID = " + string(al_job_id) + " and ARCHIVE_TYPE = 'BLANKET' and BLANKET_MONTH_NUMBER = " + string(ll_month_number) + " " +&
										"and " + ls_from_table_name + ".WORK_ORDER_ID = ARCHIVE_WO_AVAILABLE_LIST.WORK_ORDER_ID)"
				if not ls_blanket_ind_exists_clause = "" then
					ls_sql = ls_sql + ")"
				end if
/*	
				// Special Case - CHARGE_GROUP_CONTROL
				if ls_table = 'CHARGE_GROUP_CONTROL' then
					ls_sql = ls_sql + "and exists(select WORK_ORDER_ID from ARCHIVE_WO_AVAILABLE_LIST where ARCHIVE_STATUS = 'PROCESSING' " +&
											"and JOB_ID = " + string(al_job_id) + " and ARCHIVE_TYPE = 'BLANKET' and BLANKET_MONTH_NUMBER = " + string(ll_month_number) + " " +&
											"and CHARGE_GROUP_CONTROL.WORK_ORDER_ID = ARCHIVE_WO_AVAILABLE_LIST.WORK_ORDER_ID)"
				end if
*/				
         end if			
			
         //***********************************
			// Special Case - UNITIZED_WORK_ORDER
			//***********************************
			if ls_table = 'UNITIZED_WORK_ORDER' then
				ls_sql = ls_sql + ' and UNIT_ITEM_ID <> 0'
			end if

   	   //**************************
	   	// Insert into Archive table
		   //**************************
         f_write_log(g_log_file, ls_sql + ';')		
			execute immediate :ls_sql;
			if sqlca.sqlcode = -1 then
				f_pp_msgs("SQL Error: Could not insert into " + ls_archive_table)
				f_pp_msgs("SQL Statement: " + ls_sql)
				f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
				rollback;
				f_pp_msgs("Transaction rolled back.")
	
				// Mark the records as Failed
				update ARCHIVE_WO_AVAILABLE_LIST
					set ARCHIVE_STATUS = 'FAILED' 
				 where ARCHIVE_STATUS = 'PROCESSING'
					and JOB_ID = to_char(:al_job_id)
					and ARCHIVE_TYPE = 'BLANKET';
	
            // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
				ll_set_error = 1
				ll_month_error = 1
				exit
	   		// return -1
			else
				f_pp_msgs(string(sqlca.SQLNRows) + " Rows were inserted into the " + ls_archive_table + " for Month Number: " + string(ll_month_number) + " in Set: " + string(ll_set_num))
			   ll_inserted_rows = sqlca.SQLNRows															 				
			end if
			//***********************
			// Build delete statement
			//***********************
			if ls_table = 'CHARGE_GROUP_CONTROL' then
				ls_sql= "delete from " + ls_table +&
						  " where exists (select WORK_ORDER_ID, CHARGE_GROUP_ID " +&
						  "                 from ARCHIVE_WO_UNIT_ITEM_TEMP" +&
                    "                where ARCHIVE_WO_UNIT_ITEM_TEMP.WORK_ORDER_ID = " + ls_table + ".WORK_ORDER_ID" +&
						  "                  and ARCHIVE_WO_UNIT_ITEM_TEMP.CHARGE_GROUP_ID = " + ls_table + ".CHARGE_GROUP_ID)"
			elseif ls_blanket_ind_exists_clause = 'Special Case - UNIT_ITEM_ID' then
	   		//create Archive table delete statement
				ls_sql= "delete from " + ls_table +&
						  " where exists (select WORK_ORDER_ID, UNIT_ITEM_ID " +&
						  "                 from ARCHIVE_WO_UNIT_ITEM_TEMP" +&
                    "                where ARCHIVE_WO_UNIT_ITEM_TEMP.WORK_ORDER_ID = " + ls_table + ".WORK_ORDER_ID" +&
						  "                  and ARCHIVE_WO_UNIT_ITEM_TEMP.UNIT_ITEM_ID = " + ls_table + ".UNIT_ITEM_ID)"
			elseif ls_blanket_ind_exists_clause = 'Special Case - CHARGE_ID' then
	   		//create Archive table insert statements for Charge related tables
				ls_sql= "delete from " + ls_table +&
						  " where exists (select CHARGE_ID " +&
						  "                 from ARCHIVE_WO_CHARGE_ID_TEMP" +&
                    "                where ARCHIVE_WO_CHARGE_ID_TEMP.CHARGE_ID = " + ls_table + ".CHARGE_ID)"
   		else
				
				ls_sql = "delete from " + ls_table + " where "
				if not ls_blanket_ind_exists_clause = "" or not isnull(ls_blanket_ind_exists_clause) then
					ls_sql = ls_sql + f_replace_string(ls_blanket_ind_exists_clause,"<MONTH_NUMBER>",string(ll_month_number),"FIRST") + "("
				else
					ls_sql = ls_sql + " MONTH_NUMBER <= " + string(ll_month_number) + " and exists("
				end if
				// Add generic exists clause
				ls_sql = ls_sql + "select WORK_ORDER_ID from ARCHIVE_WO_AVAILABLE_LIST where ARCHIVE_STATUS = 'PROCESSING'" +&
										" and JOB_ID = " + string(al_job_id) + " and ARCHIVE_TYPE = 'BLANKET' and BLANKET_MONTH_NUMBER = " + string(ll_month_number) + " " +&
										"and " + ls_from_table_name + ".WORK_ORDER_ID = ARCHIVE_WO_AVAILABLE_LIST.WORK_ORDER_ID)"
				if not ls_blanket_ind_exists_clause = "" or not isnull(ls_blanket_ind_exists_clause) then
					ls_sql = ls_sql + ")"
				end if
				
			
/*	
				// Special Case - CHARGE_GROUP_CONTROL
				if ls_table = 'CHARGE_GROUP_CONTROL' then
					ls_sql = ls_sql + "and exists(select WORK_ORDER_ID from ARCHIVE_WO_AVAILABLE_LIST where ARCHIVE_STATUS = 'PROCESSING' " +&
											"and JOB_ID = " + string(al_job_id) + " and ARCHIVE_TYPE = 'BLANKET' and BLANKET_MONTH_NUMBER = " + string(ll_month_number) + " " +&
											"and CHARGE_GROUP_CONTROL.WORK_ORDER_ID = ARCHIVE_WO_AVAILABLE_LIST.WORK_ORDER_ID)"
				end if
*/				
         end if										
			// Delete from Base table
			
         //***********************************
			// Special Case - UNITIZED_WORK_ORDER
			//***********************************
			if ls_table = 'UNITIZED_WORK_ORDER' then
				ls_sql = ls_sql + ' and UNIT_ITEM_ID <> 0'
			end if
      
   	   //**************************
	   	// Delete from Archive table
		   //**************************
		   f_write_log(g_log_file, ls_sql + ';')					
			execute immediate :ls_sql;

			if sqlca.sqlcode = -1 then
				f_pp_msgs("SQL Error: Could not delete from " + ls_table)
				f_pp_msgs("SQL Statement: " + ls_sql)
				f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
				rollback;
				f_pp_msgs("Transaction rolled back.")	
	
				// Mark the records as Failed
				update ARCHIVE_WO_AVAILABLE_LIST
					set ARCHIVE_STATUS = 'FAILED' 
				 where ARCHIVE_STATUS = 'PROCESSING'
					and JOB_ID = to_char(:al_job_id)
					and ARCHIVE_TYPE = 'BLANKET';
				
            // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
				ll_set_error = 1
				ll_month_error = 1				
				exit
	   		// return -1
			else
				f_pp_msgs(string(sqlca.SQLNRows) + " Rows were deleted from the " + ls_table + " for Month Number: " + string(ll_month_number) + " in Set: " + string(ll_set_num))
   			ll_deleted_rows = sqlca.SQLNRows
	   		if ll_deleted_rows <> ll_inserted_rows then
               f_pp_msgs("SQL Error: Deleted record count doesn't match Inserted record count for table: " + ls_table)
			      f_pp_msgs("SQL Statement: " + ls_sql)
			      f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
   			   rollback;
	   		   f_pp_msgs("Transaction rolled back.")						
 
	   		   // Mark the records as Failed
		   	   update ARCHIVE_WO_AVAILABLE_LIST
			         set ARCHIVE_STATUS = 'FAILED' 
			       where ARCHIVE_STATUS = 'PROCESSING'
				      and JOB_ID = to_char(:al_job_id)
   				   and ARCHIVE_TYPE = 'STANDARD';
			
               // Just Exit the Current Loop and let the next set process.  The Errored records have been marked.
	   			ll_set_error = 1
	   			ll_month_error = 1					
		   		exit
	   	   	// return -1
		   	end if
			end if
		
		   fetch cur_archive_tables
			  into :ls_table, :ls_archive_table, :ls_blanket_ind_exists_clause;
      loop // Next Table to Archive
			
		close cur_archive_tables;
		
		update ARCHIVE_WO_AVAILABLE_LIST
			set ARCHIVE_STATUS = 'ARCHIVED' 
		 where ARCHIVE_STATUS = 'PROCESSING'
			and JOB_ID = to_char(:al_job_id)
			and ARCHIVE_TYPE = 'BLANKET';
	
		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not update ARCHIVE_WO_AVAILABLE_LIST to 'ARCHIVED' status.")
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("Transaction rolled back.")						
			return -1
		end if
	
		
		insert into ARC_ARCHIVE_WO_AVAILABLE_LIST
			(WORK_ORDER_ID, JOB_ID, ARCHIVE_STATUS, ARCHIVE_TYPE, WORK_ORDER_NUMBER, CWIP_BALANCE,
			 RWIP_BALANCE, CWIP_MONTH_NUMBER, RWIP_MONTH_NUMBER, ARCHIVE_FLAG, ARCHIVE_YEAR, NON_UNIT_BALANCE,
			 OTHER_BALANCE, NON_UNIT_MONTH_NUMBER, OTHER_MONTH_NUMBER, NUM_CHARGES, LONG_DESCRIPTION,
			 BLANKET_MONTH_NUMBER, TIME_STAMP, USER_ID, CREATED_DATE,APPROVED_DATE,TAX_BALANCE,ORIGINAL_COST_RETIRE_BALANCE)
			(select WORK_ORDER_ID,
					  JOB_ID,
					  ARCHIVE_STATUS,
					  ARCHIVE_TYPE,
					  WORK_ORDER_NUMBER,
					  CWIP_BALANCE,
					  RWIP_BALANCE,
					  CWIP_MONTH_NUMBER,
					  RWIP_MONTH_NUMBER,
					  ARCHIVE_FLAG,
					  ARCHIVE_YEAR,
					  NON_UNIT_BALANCE,
					  OTHER_BALANCE,
					  NON_UNIT_MONTH_NUMBER,
					  OTHER_MONTH_NUMBER,
					  NUM_CHARGES,
					  LONG_DESCRIPTION,
					  BLANKET_MONTH_NUMBER,
					  TIME_STAMP,
					  USER_ID,
					  CREATED_DATE,
					  APPROVED_DATE,
					 TAX_BALANCE,
		  			 ORIGINAL_COST_RETIRE_BALANCE
				from ARCHIVE_WO_AVAILABLE_LIST
			  where ARCHIVE_STATUS = 'ARCHIVED');

		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not insert into ARC_ARCHIVE_WO_AVAILABLE_LIST the archived records.")
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("Transaction rolled back.")						
			return -1
		end if
			  
		delete from ARCHIVE_WO_AVAILABLE_LIST
			where ARCHIVE_STATUS = 'ARCHIVED';
	
		if sqlca.sqlcode = -1 then
			f_pp_msgs("SQL Error: Could not delete from ARCHIVE_WO_AVAILABLE_LIST the archived records.")
			f_pp_msgs("SQL Error: " + sqlca.sqlerrtext)
			rollback;
			f_pp_msgs("Transaction rolled back.")						
			return -1
		end if
		//RLQ	
		//rollback;
		commit;

      if ll_month_error = 1 then
         f_pp_msgs("*********************************************")
         f_pp_msgs("Set " + string(ll_set_num) + " in Job " + string(al_job_id) + " failed for Month Number: " + string(ll_month_number))
         f_pp_msgs("*********************************************")
         ll_job_error = 1
      else
         f_pp_msgs("*********************************************")
	      f_pp_msgs("All tables have been Archived for Set " + string(ll_set_num) + " for Month Number: " + string(ll_month_number) + " in Job " + string(al_job_id))
         f_pp_msgs("*********************************************")
      end if   
      ll_month_error = 0
		
	loop	
	
	if ll_set_error = 1 then
      f_pp_msgs("*********************************************")
      f_pp_msgs("Some Months in Set " + string(ll_set_num) + " failed for Job " + string(al_job_id))
      f_pp_msgs("*********************************************")
      ll_job_error = 1
   else
      f_pp_msgs("*********************************************")
	   f_pp_msgs("All Months have been Archived for Set " + string(ll_set_num))
      f_pp_msgs("*********************************************")
	end if
	
	ll_set_error = 0
next	

if ll_job_error = 1 then
	f_pp_msgs("*********************************************")
	f_pp_msgs("Some Sets in Job " + string(al_job_id) + " failed.")
	f_pp_msgs("*********************************************")
	il_job_error = 1
else
	f_pp_msgs("*********************************************")
	f_pp_msgs("All Sets in " + string(al_job_id) + " processed.")
	f_pp_msgs("*********************************************")
end if

return 1;

end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

