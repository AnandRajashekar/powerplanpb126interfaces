HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_preview_je.exe'

longlong i_company_idx[]
longlong i_month_number
datetime i_month

boolean i_run_accruals, i_run_afudc, i_run_aro, i_run_lease, i_run_depr

nvo_wo_control i_nvo_wo_control
nvo_cpr_control i_nvo_cpr_control

uo_ds_top i_ds_depr_activity

string i_curr_comp_descr
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function string uf_getcustomversion (string a_pbd_name)
public function boolean uf_accruals ()
public function boolean uf_afc_oh_wip ()
public function boolean uf_aro ()
public function boolean uf_ls_depr ()
public function boolean uf_depr ()
public function boolean uf_afc_before_oh ()
public function boolean uf_afc_after_oh ()
public function boolean uf_afc_real ()
public function boolean uf_clear_previews ()
public function boolean uf_afc_wip_comp ()
public function string uf_aro_db_preview (datetime a_month, longlong a_company_id)
public function longlong uf_aro_update_preview (longlong a_company_id, datetime a_month)
public function longlong uf_aro_preview_for_company (longlong a_company_id, datetime a_month)
public function longlong uf_aro_determine_sob (longlong a_aro_id)
public function integer uf_aro_preview_accretion (longlong a_company_id, datetime a_month)
public function longlong uf_reg_entries_preview (longlong a_company_id, datetime a_month, string a_aro_depr)
public function boolean uf_aro_retrieve_co (longlong a_cc)
public function string uf_ls_book_je (longlong a_ls_asset_id, longlong a_trans_type, longlong a_amt, longlong a_asset_act_id, longlong a_dg_id, string a_wo_id, longlong a_gl_account_id, longlong a_gain_loss, longlong a_pend_trans_id, longlong a_company_id, datetime a_month, longlong a_dr_cr, string a_gl_jc, longlong a_sob_id, string a_orig)
public function boolean uf_depr_retrieve_co ()
public function boolean uf_depr_gl_trans_post (longlong a_row, string a_source, longlong a_je_method)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// populate parameters passed from the ssp parm, needed for processing.
i_company_idx = g_ssp_parms.long_arg
i_month = datetime(g_ssp_parms.date_arg[1])
i_month_number = year(g_ssp_parms.date_arg[1]) * 100 + month(g_ssp_parms.date_arg[1])

i_run_accruals = g_ssp_parms.boolean_arg[1]
i_run_afudc = g_ssp_parms.boolean_arg2[1]
i_run_aro = g_ssp_parms.boolean_arg3[1]
i_run_lease = g_ssp_parms.boolean_arg4[1]
i_run_depr = g_ssp_parms.boolean_arg5[1]

//lock the process
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_company_idx, i_month_number, process_msg)) = false then
          f_pp_msgs("SSP Preview JE - " + &
                         "There has been a concurrency error. Please check that processes are not currently running.  Process message : "+process_msg)
          return rtn_failure
end if

//clear out previously generated preview records
if not uf_clear_previews() then
	return rtn_failure
end if

////fire off the different function specific to the type of jes the user wants a preview of
if i_run_accruals then
	//1 : true if "Accruals" is checked
	if not uf_accruals() then
		return rtn_failure
	end if
end if

if i_run_afudc then
	//2 : true if "AFUDC, Overheads, & Wip Computations is checked
	if not uf_afc_oh_wip() then
		return rtn_failure
	end if
end if

if i_run_aro then
	//3 : true if "ARO" is checked
	if not uf_aro() then
		return rtn_failure
	end if
end if

if i_run_lease then
	//4 : true if "Lease Depreciation" is checked
	if not uf_ls_depr() then
		return rtn_failure
	end if
end if

if i_run_depr then
	//5 : true if "Depreciation" is checked
	if not uf_depr() then
		return rtn_failure
	end if
end if

//unlock the process
i_nvo_wo_control.of_releaseprocess( process_msg )
f_pp_msgs("Release Process Status: " + process_msg )

return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_preview_je_custom_version nvo_ssp_preview_je_custom_version
nvo_ppprojct_custom_version nvo_ppprojct_custom_version
nvo_ppdepr_interface_custom_version nvo_ppdepr_interface_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'ssp_preview_je_custom.pbd'
		return nvo_ssp_preview_je_custom_version.custom_version
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
	case 'ppdepr_interface_custom.pbd'
		return nvo_ppdepr_interface_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version	
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose

return ""
end function

public function boolean uf_accruals ();//**********************************************************************************************************************************************************
//
// uf_accruals()
// 
// Logic to generate the preview of the Accruals journal entries
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                                false : There was an error
//
//**********************************************************************************************************************************************************

longlong i, j, num_companies, month_number, rtn, wo_accr_ct, num_gl_rows, je_id, trans_id, je_method_id, acct_id
string process_msg, je_code, je_code_option, company, work_order_number, acct

i_nvo_wo_control.of_constructor()

// reference data window
w_datawindows w_dw

// these parameters originate from the w_wo_control window
i_nvo_wo_control.i_company_idx = i_company_idx
i_nvo_wo_control.i_month = i_month
i_nvo_wo_control.of_currentmonth( i_month )
i_nvo_wo_control.i_month_number = i_month_number

f_pp_msgs("Preview JE for Accruals : ")
f_pp_msgs("Month: " + string(i_nvo_wo_control.i_month, "mm/dd/yyyy"))
for i = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	f_pp_msgs("Company ID: " + string(i_nvo_wo_control.i_company_idx[i]))
next

// populate the company descs
rtn = i_nvo_wo_control.of_getDescriptionsFromids( i_nvo_wo_control.i_company_idx )
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// loop over the companies and process JE creation
f_pp_msgs("Processing JE for Accruals...")
for i = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	
	// change the company in the NVO
	i_nvo_wo_control.of_companychanged( i , i_nvo_wo_control.i_month )

	select gl_company_no into :company 
	from company where company_id = :i_nvo_wo_control.i_company_idx[i];

	wo_accr_ct = long(f_pp_system_control_company("Work Order Accrual Charge Type", i_nvo_wo_control.i_company_idx[i]))

	uo_ds_top dw_gl_transaction
	dw_gl_transaction = CREATE uo_ds_top
	dw_gl_transaction.Dataobject = 'dw_gl_transaction_preview'
	dw_gl_transaction.SetTransObject(sqlca)
		 
	datastore dw_gl_charges
	dw_gl_charges = CREATE datastore
	dw_gl_charges.Dataobject = 'dw_gl_charges_accruals'
	dw_gl_charges.SetTransObject(sqlca)

	num_gl_rows    = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month, i_nvo_wo_control.i_company_idx[i], wo_accr_ct)
	//f_pp_msgs("num_gl_rows: " + string(num_gl_rows) + ", month:" + string(i_nvo_wo_control.i_month) + ", company:" + string(i_nvo_wo_control.i_company_idx[i]) + ", chargetype:" + string(wo_accr_ct))
	select je_id into :je_id 
	from gl_je_control where process_id = 'Accruals';
	
	je_code_option = f_pp_system_control_company('Accruals Approval - Use External JE', i_nvo_wo_control.i_company_idx[i])
	
	select external_je_code into :je_code 
	from standard_journal_entries where je_id = :je_id;
	
	if je_code_option = 'yes' or je_code_option = '' then
		select external_je_code into :je_code 
		from standard_journal_entries where je_id = :je_id;
	else
		select gl_je_code into :je_code 
		from standard_journal_entries where je_id = :je_id;
	end if

	for j = 1 to num_gl_rows
		
		if mod(j, 50) = 0 then
			f_pp_msgs("Generating Preview GL Journal Entries (Accruals) ... Finished " + &
				string(j) + " of " + string(num_gl_rows))
		end if
	
		dw_gl_transaction.insertrow(1)
		
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
		dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
		dw_gl_transaction.setitem(1,'month',          dw_gl_charges.getitemdatetime(j,'charge_mo_yr'))
		dw_gl_transaction.setitem(1,'company_number', company)
		dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(j,'amount'))
		work_order_number = dw_gl_charges.getitemstring(j, 'work_order_number')	
	
		je_code = trim(je_code)
		
		je_method_id = dw_gl_charges.getitemdecimal(j,'je_method_id')
		
		acct_id = dw_gl_charges.getitemnumber(j,'credit_gl_account')
		
		acct = f_autogen_je_account(dw_gl_charges, j, 30, acct_id,je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			return false
		end if
		
		dw_gl_transaction.setitem(1,'gl_account',             acct)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(j,'work_order_number'))
		dw_gl_transaction.setitem(1,'je_method_id',           je_method_id)
		dw_gl_transaction.setitem(1,'amount_type',            1)
		dw_gl_transaction.setitem(1,'trans_type', 30) //30 - Accrual Credit
		
		dw_gl_transaction.InsertRow(1)
		
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
		
		dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
		dw_gl_transaction.setitem(1,'month',          dw_gl_charges.getitemdatetime(j,'charge_mo_yr'))
		dw_gl_transaction.setitem(1,'company_number', company)
		dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(j,'amount'))
		
		acct_id = dw_gl_charges.getitemnumber(j, 'gl_account_id')
		
		acct = f_autogen_je_account(dw_gl_charges, j, 31, acct_id,je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			return false
		end if
		
		dw_gl_transaction.setitem(1,'gl_account',             acct)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'je_method_id',           je_method_id)
		dw_gl_transaction.setitem(1,'amount_type',            1)
		dw_gl_transaction.setitem(1,'trans_type', 		31) //31 - Accrual Debit
		
		dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(j,'work_order_number'))
		
	next

	if dw_gl_transaction.Update() <> 1 then
		f_pp_msgs("Error updating General Ledger Transaction Previews. " + dw_gl_transaction.i_sqlca_sqlerrtext) 
	 
		rollback using sqlca;
		return false
	end if
	
next

return true
end function

public function boolean uf_afc_oh_wip ();//**********************************************************************************************************************************************************
//
// uf_afc_oh_wip()
// 
// Generate Preview JEs for AFUDC, Overheads, WIP Calcs.  This should loosely mimic the process flow of the AFUDC OH WIP approvals SSP.
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                                false : There was an error
//
//**********************************************************************************************************************************************************

longlong ndx, rtn, num_companies, c
string wo_process_ans

i_nvo_wo_control.of_constructor()

i_nvo_wo_control.i_company_idx = i_company_idx
i_nvo_wo_control.i_month = i_month
i_nvo_wo_control.of_currentmonth( i_month )
i_nvo_wo_control.i_month_number = i_month_number

f_pp_msgs("Preview JE for Overheads and AFUDC WIP : ")
f_pp_msgs("Month: " + string(i_nvo_wo_control.i_month, "mm/dd/yyyy"))
for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	f_pp_msgs("Company ID: " + string(i_nvo_wo_control.i_company_idx[ndx]))
next

// BEGIN validate input
if (upperbound(i_nvo_wo_control.i_company_idx) < 1) then
	f_pp_msgs("No Company IDs passed into interface")
	return false
end if
// END validate input

i_nvo_wo_control.i_afudc_error = false
i_nvo_wo_control.i_no_summary = false

// populate the company descs
rtn = i_nvo_wo_control.of_getdescriptionsfromids( i_nvo_wo_control.i_company_idx )
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c=1 to num_companies
	
	i_nvo_wo_control.of_companychanged(c,i_nvo_wo_control.i_month)
	
next


	for c=1 to num_companies
		//	it's selected, update the rest of the window
		i_nvo_wo_control.of_companychanged(c, i_month )
		
		i_curr_comp_descr = i_nvo_wo_control.i_company_descr[c]
		
		f_pp_msgs("Processing company " + i_curr_comp_descr)

		//  Get pp_system_control variable to determine whether or not to calculate
		//  Afudc, Overheads, or Both ...
		wo_process_ans = f_pp_system_control_company("Work Order Calculation", i_nvo_wo_control.i_company)
		
		if isnull(wo_process_ans) or wo_process_ans = "" then
			wo_process_ans = "afudc/overheads"
		end if
		
		f_pp_msgs("wo_process_ans: " + string(wo_process_ans))
		
		CHOOSE CASE wo_process_ans
			CASE 'afudc'
				f_pp_msgs(  "Starting AFUDC approval.")
			CASE 'overheads'
				f_pp_msgs(  "Starting Overhead approval.")
			CASE 'none'
				f_pp_msgs(  "Skipping OH and AFUDC approval.")
				goto overhead_calc
			CASE ELSE
				f_pp_msgs( "Starting Overhead/AFUDC approval.")
		END CHOOSE
	
		if wo_process_ans = 'none' then 
			goto overhead_calc
		end if
			
		if (wo_process_ans =  'overheads' or &
												wo_process_ans = "afudc/overheads") then	
			uf_afc_before_oh()
			if i_nvo_wo_control.i_afudc_error then 
				rollback;
				goto overhead_calc
			end if 
			if wo_process_ans ='overheads' then goto overhead_calc_after
		end if
		
		// setup the transiant CURRENT_AFUDC_VIEW by using the dw_cpr_act_month to write
		// the session ID to the CPR_ACT_MONTH table
		i_nvo_wo_control.of_currentmonth(i_nvo_wo_control.i_month)
		
		uf_afc_real()
		if i_nvo_wo_control.i_afudc_error then 
			rollback;
			goto overhead_calc
		end if 
		
		overhead_calc_after:
	
		if (wo_process_ans =  'overheads' or &
												wo_process_ans = "afudc/overheads") then	
			uf_afc_after_oh()
			if i_nvo_wo_control.i_afudc_error then 
				rollback; 
			end if 
		end if
		
		overhead_calc:
		if i_nvo_wo_control.i_afudc_error then 
			f_wo_status_box("Preview JE Overheads and AFUDC", "ERROR in Overheads and AFUDC Approval!")
		end if 
	
		commit;
		
	next

f_pp_msgs("Preview JE for Overheads and AFUDC WIP process ended on " + String(Today(), "mm/dd/yyyy") + " at " + String(Today(), "hh:mm:ss"))

return not i_nvo_wo_control.i_afudc_error

end function

public function boolean uf_aro ();//**********************************************************************************************************************************************************
//
// uf_aro()
// 
// Logic to generate the preview of the ARO journal entries
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                                false : There was an error
//
//**********************************************************************************************************************************************************
longlong rtn, num_failed_companies
longlong total_company, cc
boolean rc
//nvo_aro_approve aro_approve

// Construct the nvo
i_nvo_cpr_control.of_constructor()

// Get the ssp parameters
i_nvo_cpr_control.i_company_idx[]  = g_ssp_parms.long_arg[]
i_nvo_cpr_control.i_original_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return false
end if

// Get the descriptions
rtn = i_nvo_cpr_control.of_getDescriptionsFromIds(i_nvo_cpr_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

total_company = upperbound(i_nvo_cpr_control.i_company_idx)

f_pp_msgs( "Begin ARO Approval: " + string(now()))

//aro_approve = CREATE nvo_aro_approve
for cc = 1 to total_company
	
	//rc = i_nvo_cpr_control.of_setupCompany(cc)
	rc = uf_aro_retrieve_co(cc)
	
	if not rc then 
		f_pp_msgs("Error populating cpr_company!")
		f_pp_msgs("The ARO preview will not continue")
		return false
	end if
	
	rtn = uf_aro_preview_for_company(i_nvo_cpr_control.i_company_idx[cc], i_nvo_cpr_control.i_original_month)
	
	if rtn = -1 then
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		return false
	elseif rtn = 1 then
		commit;
	end if
	
next//end company loop

//DESTROY aro_approve

i_nvo_cpr_control.of_cleanUp(3, 'email cpr close: aro approval', 'ARO Approval')

return true
end function

public function boolean uf_ls_depr ();//**********************************************************************************************************************************************************
//
// uf_ls_depr()
// 
// Logic to generate the preview of the Lease Depr journal entries
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                                false : There was an error
//
//**********************************************************************************************************************************************************
longlong ll_company_arr[], company_index, ds_index, ds_row_count
datetime ld_month
string sqls, rtn, l_gl_je_code
uo_ds_top ds_ls_depr

// Populate local vars with parameters
ll_company_arr = i_company_idx
ld_month = i_month

// Loop through companies
for company_index = 1 to upperbound(ll_company_arr)

	f_pp_msgs("Starting Logs for Depreciation Preview JE Process. Company ID: " + string(ll_company_arr[company_index]) + " - " + "Month: " + string(ld_month, "mm/yyyy"))

	// Get GL JE code
	select nvl(e.external_je_code, nvl(e.gl_je_code, 'LAMDEPR'))
	  into :l_gl_je_code
	  from gl_je_control g, standard_journal_entries e
	 where e.je_id = g.je_id
	   and g.process_id = 'LAMDEPR';
	
	f_pp_msgs("Generating depreciation preview journals")
	
	// Create datastore
	ds_ls_depr = create uo_ds_top
	sqls = "select lcam.ls_asset_id as ls_asset_id, " + &
                 "cd.set_of_books_id as sob_id, " + &
                 "cd.curr_depr_expense + cd.depr_exp_adjust + cd.depr_exp_alloc_adjust as depr_exp, " + &
                 "cd.depr_group_id, " + &
                 "dg.reserve_acct_id as reserve_acct_id, " + &
                 "dg.expense_acct_id as expense_acct_id " + &
            "from cpr_depr cd, cpr_ledger cl, ls_cpr_asset_map lcam, ls_asset la, depr_group dg " + &
           "where cd.asset_id = cl.asset_id " + &
             "and cl.subledger_indicator = -100 " + &
             "and lcam.asset_id = cl.asset_id " + &
             "and (cd.curr_depr_expense + cd.depr_exp_adjust + cd.depr_exp_alloc_adjust) <> 0 " + &
             "and la.ls_asset_id = lcam.ls_asset_id " + &
             "and la.company_id = " + string(ll_company_arr[company_index]) + " " + &
             "and cd.gl_posting_mo_yr = to_date('" + string(ld_month, "mm/yyyy") + "', 'mm/yyyy') " + &
             "and cl.depr_group_id = dg.depr_group_id"
	rtn = f_create_dynamic_ds(ds_ls_depr, "grid", sqls, sqlca, true)
	if rtn <> "OK" then
		f_pp_msgs("Error creating lease depreciation datastore: " + rtn)
		return false
	end if

	// Loop through results
	ds_row_count = ds_ls_depr.rowCount()
	
	if ds_row_count = 0 then
		f_pp_msgs("Nothing to process")
	end if
	
	for ds_index = 1 to ds_row_count
		
		// debit
		rtn = uf_ls_book_je(                                                             &
			ds_ls_depr.getItemNumber(ds_index, 'ls_asset_id'),      /* a_ls_asset_id   */ &
			3032,                                                   /* a_trans_type    */ &
			ds_ls_depr.getItemNumber(ds_index, 'depr_exp'),         /* a_amt           */ &
			0,                                                      /* a_asset_act_id  */ &
			-1,                                                     /* a_dg_id         */ &
			'null',                                                 /* a_wo_id         */ &
			ds_ls_depr.getItemNumber(ds_index, 'expense_acct_id'),  /* a_gl_account_id */ &
			0,                                                      /* a_gain_loss     */ &
			-1,                                                     /* a_pend_trans_id */ &
			ll_company_arr[company_index],                          /* a_company_id    */ &
			ld_month,                                               /* a_month         */ &
			1,                                                      /* a_dr_cr         */ &
			l_gl_je_code,                                           /* a_gl_jc         */ &
			ds_ls_depr.getItemNumber(ds_index, 'sob_id'),           /* a_sob_id        */ &
			''                                                      /* a_orig          */ &
		)

		if rtn <> "OK" then
			f_pp_msgs("Writing depreciation preview JEs for ls_asset_id: " + string(ds_ls_depr.getItemNumber(ds_index, 'ls_asset_id')) + " : " + rtn)
			return false
		end if
		
		// credit
		rtn = uf_ls_book_je(                                                             &
			ds_ls_depr.getItemNumber(ds_index, 'ls_asset_id'),      /* a_ls_asset_id   */ &
			3033,                                                   /* a_trans_type    */ &
			ds_ls_depr.getItemNumber(ds_index, 'depr_exp'),         /* a_amt           */ &
			0,                                                      /* a_asset_act_id  */ &
			-1,                                                     /* a_dg_id         */ &
			'null',                                                 /* a_wo_id         */ &
			ds_ls_depr.getItemNumber(ds_index, 'reserve_acct_id'),  /* a_gl_account_id */ &
			0,                                                      /* a_gain_loss     */ &
			-1,                                                     /* a_pend_trans_id */ &
			ll_company_arr[company_index],                          /* a_company_id    */ &
			ld_month,                                               /* a_month         */ &
			0,                                                      /* a_dr_cr         */ &
			l_gl_je_code,                                           /* a_gl_jc         */ &
			ds_ls_depr.getItemNumber(ds_index, 'sob_id'),           /* a_sob_id        */ &
			''                                                      /* a_orig          */ &
		)

		if rtn <> "OK" then
			f_pp_msgs("Writing depreciation preview JEs for ls_asset_id: " + string(ds_ls_depr.getItemNumber(ds_index, 'ls_asset_id')) + " : " + rtn)
			return false
		end if

	next
	
	f_pp_msgs("Depreciation JE Preview Process Complete for Company ID: " + string(ll_company_arr[company_index]))
	
	// Destroy datastore
	destroy ds_ls_depr
	
next

return true
end function

public function boolean uf_depr ();//**********************************************************************************************************************************************************
//
// uf_depr()
// 
// Logic to generate the preview of the Depr journal entries
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                                false : There was an error
//
//**********************************************************************************************************************************************************

longlong cc, total_company, rc, je_id, check, num_failed_companies, rtn, &
			je_rows, je, je_rows10[], je_rows11[], empty[], book_count, book_include, &
			je_method, amt_type, jebooks_rows, b, book, convention, i, je_by_asset, &
			dg_id, trans_id, ll_month, ll_long_args[], rows, depr_ind
datetime nullval
string   company, je_code, comp_descr, ret, args[], month_string, je_descr,&
			gl_acct
boolean b_failed_company = false, do_not_update, je_skip[], emptyb[]
s_gl_trans_parms lst_gl_trans_parms 

f_pp_msgs("Beginning depreciation preview journal entry generation process for the following companies:")
for cc = 1 to upperbound(i_company_idx)
	f_pp_msgs("	" + string(i_company_idx[cc]))
next

i_nvo_cpr_control.of_constructor()


// Set the month number, company, specific things done in i_nvo_cpr_control.of_setupfromcompaniesandmonth
i_nvo_cpr_control.i_month_number	= i_month_number
i_nvo_cpr_control.i_month	= i_month
i_nvo_cpr_control.i_original_month = i_month
i_nvo_cpr_control.i_company_idx = i_company_idx

rtn = i_nvo_cpr_control.of_getdescriptionsfromids( i_company_idx ) 
if rtn < 0 then
	return false
end if

rtn = i_nvo_cpr_control.of_currentmonth( i_month ) 
if rtn < 0 then
	return false
end if

setnull(nullval)

f_set_large_rollback_segment()

total_company = upperbound(i_company_idx)

for cc = 1 to total_company
	
	
	f_pp_msgs("Beginning loop over companies.")
	
	i_nvo_cpr_control.i_company = i_company_idx[cc]
	i_nvo_cpr_control.i_ds_cpr_control.retrieve(i_nvo_cpr_control.i_company, i_month)
	if not uf_depr_retrieve_co() then
		f_pp_msgs("uf_depr_retrieve_co returned an error.")
		continue // next company
	end if
	
	do_not_update = false
	je_rows10 = empty
	je_rows11 = empty
	je_skip = emptyb
	
	//Maint 8604
	select 1 into :check
	from pend_depr_activity a, depr_group g
	where (gl_post_mo_yr = :i_nvo_cpr_control.i_month or current_mo_yr = :i_nvo_cpr_control.i_month)
	and a.depr_group_id = g.depr_group_id
	and g.company_id = :i_nvo_cpr_control.i_company
	and rownum = 1
	;
	
// setup other fields	
	SetNull(company)
	Setnull(je_id)
	Setnull(je_code)
	select gl_company_no into :company from company where company_id = :i_nvo_cpr_control.i_company;
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("Error getting Company Number: " + sqlca.sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if IsNull(company) then
		f_pp_msgs("Company Number not found for ID: " + String(i_nvo_cpr_control.i_company))
		
		b_failed_company = true
		goto next_company
	end if		
	
	select je_id into :je_id from gl_je_control where process_id = 'Depreciation Expense';
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("Error getting Journal ID: " + sqlca.sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if IsNull(je_id) then
		f_pp_msgs("Journal ID not found for 'Depreciation Expense'")
		
		b_failed_company = true
		goto next_company
	end if		
	
	select gl_je_code into :je_code from standard_journal_entries where je_id = :je_id;
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("Error getting GL Journal Code: " + sqlca.sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if IsNull(je_code) then
		f_pp_msgs("GL Journal Code not found for ID: " + String(je_id))
		
		b_failed_company = true
		goto next_company
	end if		
	
	select description  into :comp_descr from company where company_id = :i_nvo_cpr_control.i_company;

	f_pp_msgs("Generation of Depr Preview JEs started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))

	f_pp_msgs("Calling f_pre_preview_depr_co_custom.")
 	 ret = f_pre_preview_depr_co_custom(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month)
	 if Trim(ret) <> "" then
	  if ret = "next_company" then 
		f_pp_msgs("Error in f_pre_preview_depr_co_custom.")
		
		b_failed_company = true
		goto next_company
	  else
		f_pp_msgs("ERROR: Approval Aborted. See previous message(s).")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		if upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
			i_nvo_cpr_control.of_log_failed_companies("PREVIEW JE DEPR")
		end if
		
		return false
		
	  end if
	  
	 end if 
	 
	 // Call Dynamic Validation option
	f_pp_msgs("Calling dynamic validation.")
	args[1] = string(i_nvo_cpr_control.i_month)
	args[2] = string(i_nvo_cpr_control.i_company)
	args[3] = 'f_pre_preview_depr_co_custom'
	
	rtn = f_wo_validation_control(1005,args)
	
	if rtn < 0 then
		f_pp_msgs("General Ledger Transactions Release (Dynamic Validations) Failed.")
		
		b_failed_company = true
		goto next_company
	end if
	
	// Generate the General Ledger Transactions.
	

	f_pp_msgs("Generate the Depreciation Preview GL Transactions..."+ ' ' + string(now()))	
	
	//	f_autogen_je_account is used to customize the contents 
	//	of gl_transaction.gl_account.
	//	in order to use the existing f_autogen_je_account
	//	we need to pass it a datastore.
	//	thus, in the base this dw (dw_depr_expense) needs
	//	to be a datastore
	uo_ds_top ds_depr_expense, ds_je_method_trans_type, ds_je_method_set_of_books, ds_depr_journal, ds_gl_transaction
	ds_depr_expense = CREATE uo_ds_top
	ds_depr_expense.DataObject = "dw_depr_expe_gl"
	ds_depr_expense.SetTransObject(sqlca)

	ds_je_method_trans_type = CREATE uo_ds_top
	ds_je_method_trans_type.DataObject = "dw_depr_je_method_trans_type"
	ds_je_method_trans_type.SetTransObject(sqlca)

	ds_je_method_set_of_books = CREATE uo_ds_top
	ds_je_method_set_of_books.DataObject = "dw_depr_je_method_set_of_books"
	ds_je_method_set_of_books.SetTransObject(sqlca)
	
	/*DJL - Maint 4940 - 02/2011 - Add option to create Depreciation Journals from CPR Depr by Asset.
	This datawindow retrieves depreciation expense at an asset level. If assets are group depr(subledger 0), then use an allocation to 
	gather depreciation expense.*/
	ds_depr_journal = CREATE uo_ds_top
	ds_depr_journal.DataObject = "dw_depr_expense_je_by_asset"
	ds_depr_journal.SetTransObject(sqlca)
	/**************************************************************************///End Maint 4940
	
	uo_autogen_je_account uo_autogen
	uo_autogen = create uo_autogen_je_account

	ds_gl_transaction = CREATE uo_ds_top
	ds_gl_transaction.dataobject = 'dw_gl_transaction_preview'
	ds_gl_transaction.settransobject(sqlca)
	
	month_string = String(i_nvo_cpr_control.i_month, "MM/YYYY")

	// Create depr. expense transactions
	// before we start let's check that depr exp cr & dr use same je methods so we don't create one-sided entry
	je_rows = ds_je_method_trans_type.Retrieve(10, i_nvo_cpr_control.i_company)
	for je = 1 to je_rows
		je_rows10[je] = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
	next
	je_rows = ds_je_method_trans_type.Retrieve(11, i_nvo_cpr_control.i_company)
	for je = 1 to je_rows
		je_rows11[je] = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
	next
	
	// check totals first
	if UpperBound(je_rows10) <> UpperBound(je_rows11) then
		f_pp_msgs("ERROR: Depr JE Methods do not match." )
		f_pp_msgs("Depr Expense Debits (10): " + String(UpperBound(je_rows10)) + " JE methods" )
		f_pp_msgs("Depr Expense Credits (11): " + String(UpperBound(je_rows11)) + " JE methods")
		
		b_failed_company = true
		goto next_company
	end if
	
	// same counts so verify same method ids
	for je = 1 to UpperBound(je_rows10)
		if je_rows10[je] <> je_rows11[je] then
			f_pp_msgs("ERROR: Depr JE Methods do not match." )
			f_pp_msgs("Depr Expense Debits (10) Method #" + String(je) + " is id=" + String(je_rows10[je]) )
			f_pp_msgs("Depr Expense Credits (11) Method #" + String(je) + " is id=" + String(je_rows11[je]))
			
			b_failed_company = true
			goto next_company
		else
			// make sure ALL books in the JE Method are valid for this company
			// this should not longer be possible because of the new je_method_company_view, but what the heck leave it here...
			select count(*), sum(nvl(include_indicator,0)), je.description
			into :book_count, :book_include, :je_descr
			from je_method_set_of_books j, company_set_of_books c, je_method je
			where j.set_of_books_id = c.set_of_books_id (+)
			and j.je_method_id = :je_rows10[je]
			and c.company_id = :i_nvo_cpr_control.i_company
			and j.je_method_id = je.je_method_id
			group by je.description
			;
			
			if book_count <> book_include then
				je_skip[je] = true
				f_pp_msgs("Warning: Depr JE Method Books do not match.  THIS JE METHOD WILL BE SKIPPED for Depreciation Expense." )
				f_pp_msgs("Method " + je_descr +" has " + String(book_count) + " but company uses " + String(book_include) )
			else
				je_skip[je] = false
			end if			
		
		end if
		
	next
	
	//get the je methods for depr expense debits
	je_rows = ds_je_method_trans_type.Retrieve(10, i_nvo_cpr_control.i_company)
	if je_rows < 0 then
		f_pp_msgs("ERROR retrieving JE Method-Trans Type data for (10) Depreciation Expense debit entries.~nSQL:" + ds_je_method_trans_type.i_sqlca_sqlerrtext)
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
	if je_rows = 0 then
		f_pp_msgs("ERROR No records round in JE Method-Trans Type data for (10) Depreciation Expense debit entries.")
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
		
	for je = 1 to je_rows
		if je_skip[je] then continue
		
		je_method = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
		amt_type =  ds_je_method_trans_type.GetItemNumber(je, "amount_type")
		
		// now get the set of books for the je method
		jebooks_rows = ds_je_method_set_of_books.Retrieve(je_method)
		if jebooks_rows < 0 then
			f_pp_msgs("Error retrieving JE Method-Set of Books data for (10) Depreciation Expense debit entries.~nSQL:" + ds_je_method_set_of_books.i_sqlca_sqlerrtext)
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
		if jebooks_rows = 0 then
			f_pp_msgs("No records round in JE Method-Set of Books data for (10) Depreciation Expense debit entries.")
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
			
		for b = 1 to jebooks_rows
			book = ds_je_method_set_of_books.GetItemNumber(b, "set_of_books_id")
			convention = ds_je_method_set_of_books.GetItemNumber(b, "reversal_convention")

			// create all the debits
			rows  = ds_depr_expense.retrieve(month_string, i_nvo_cpr_control.i_company, book)
			if rows < 0 then
				f_pp_msgs("Error retrieving Depreciation Expense entries.~nSQL:" + ds_depr_expense.i_sqlca_sqlerrtext)
				ds_gl_transaction.Reset()
				
				b_failed_company = true
				goto next_company
			end if
		
			for i = 1 to rows
		
				f_pp_msgs("Depr Expense Debit: JE Method " + String(je_method) + ", Book" + String(book) + ", Row " + String(i) + " of " + String(rows))
				je_by_asset = ds_depr_expense.getitemnumber(i, 'je_by_asset')
				// ### DJL - 05/2011 Maint 7665 - Properly gather Depr Group
				dg_id = ds_depr_expense.getitemnumber(i, 'depr_group_id')
			
				//DJL - Maint 4940 - 02/2011- No je by asset use original trans type
				if  je_by_asset = 0 then
					
					ds_gl_transaction.insertrow(1)
					
					gl_acct =  f_autogen_je_account(ds_depr_expense, i, 10, ds_depr_expense.getitemnumber(i,'debit_account_id'), je_method ) 
					if left(gl_acct, 5) = "ERROR" then
						f_pp_msgs("Error in f_autogen_je_account:  " + gl_acct)
						do_not_update = true
					end if
					
					trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
					ds_gl_transaction.setitem(1,'gl_trans_id',trans_id)
					ds_gl_transaction.setitem(1,'month',i_nvo_cpr_control.i_month)
					ds_gl_transaction.setitem(1,'company_number',company)
					ds_gl_transaction.setitem(1,'amount', & 
												ds_depr_expense.getitemdecimal(i,'depreciation_expense') * convention )
					ds_gl_transaction.setitem(1,'description', & 
														string(ds_depr_expense.getitemnumber(i,'depr_group_id')))
					ds_gl_transaction.setitem(1,'gl_account', gl_acct)
					ds_gl_transaction.setitem(1,'debit_credit_indicator',1)
					ds_gl_transaction.setitem(1,'gl_je_code',je_code)
					ds_gl_transaction.setitem(1,'gl_status_id',1)
					ds_gl_transaction.setitem(1, 'source', 'DEPR APPROVAL')
					ds_gl_transaction.setitem(1, 'amount_type', amt_type)
					ds_gl_transaction.setitem(1, 'je_method_id', je_method)
					ds_gl_transaction.setitem(1, 'trans_type', 10)
					
				elseif  je_by_asset = 1 then
					
					//Uses new trans type 39 - depr expense debit JE by asset
					lst_gl_trans_parms.a_month = i_nvo_cpr_control.i_month
					lst_gl_trans_parms.a_company = company
					lst_gl_trans_parms.a_description = string(ds_depr_expense.getitemnumber(i,'depr_group_id'))
				     lst_gl_trans_parms.a_debit_credit_indicator = 1
					lst_gl_trans_parms.a_gl_je_code = je_code
					lst_gl_trans_parms.a_gl_status_id = 1
					lst_gl_trans_parms.a_source = 'DEPR APPROVAL'
					lst_gl_trans_parms.a_amount_type = amt_type
					
					ll_month = long(String(i_nvo_cpr_control.i_month, "yyyymm"))
					
					ll_long_args[1] = ll_month
					ll_long_args[2] = i_nvo_cpr_control.i_company
					ll_long_args[3] = dg_id
					ll_long_args[4] = book
					
					//num_assets = ds_depr_journal.retrieve(month_string, i_company, book, dg_id)
					
					rtn = uo_autogen.uf_autogen_je_account(lst_gl_trans_parms, ll_long_args[], 39, je_method , i_nvo_cpr_control.i_company,  ds_depr_expense.getitemnumber(i,'debit_account_id'))
					if rtn  < 0 then
						f_pp_msgs("Error in uf_autogen_je_account depr expense credit by asset: " + gl_acct)
						do_not_update = true
					end if
					
				end if// end maint 4940 je by asset, end maint 8827 JE by asset speed fix
				
			next // transaction
		next  // SOB
	next // je_method
		
	//get the je methods for depr expense credits
	je_rows = ds_je_method_trans_type.Retrieve(11, i_nvo_cpr_control.i_company)
	if je_rows < 0 then
		f_pp_msgs("Error retrieving JE Method-Trans Type data for (11) Depreciation Expense credit entries.~nSQL:" + ds_je_method_trans_type.i_sqlca_sqlerrtext)
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
	if je_rows = 0 then
		f_pp_msgs("No records round in JE Method-Trans Type data for (11) Depreciation Expense credit entries.")
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
		
	for je = 1 to je_rows
		if je_skip[je] then continue

		je_method = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
		amt_type =  ds_je_method_trans_type.GetItemNumber(je, "amount_type")
		
		// now get the set of books for the je method
		jebooks_rows = ds_je_method_set_of_books.Retrieve(je_method)
		if jebooks_rows < 0 then
			f_pp_msgs("Error retrieving JE Method-Set of Books data for (11) Depreciation Expense credit entries.~nSQL:" + ds_je_method_set_of_books.i_sqlca_sqlerrtext)
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
		if jebooks_rows = 0 then
			f_pp_msgs("No records round in JE Method-Set of Books data for (11) Depreciation Expense credit entries.")
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
			
		for b = 1 to jebooks_rows
			book = ds_je_method_set_of_books.GetItemNumber(b, "set_of_books_id")
			convention = ds_je_method_set_of_books.GetItemNumber(b, "reversal_convention")

			// create all the credits
			rows  = ds_depr_expense.retrieve(month_string, i_nvo_cpr_control.i_company, book)
			if rows < 0 then
				f_pp_msgs("Error retrieving Depreciation Expense entries.~nSQL:" + ds_depr_expense.i_sqlca_sqlerrtext)
				ds_gl_transaction.Reset()
				
				b_failed_company = true
				goto next_company
			end if
		
			for i = 1 to rows
		
				f_pp_msgs("Depr Expense Credit: JE Method " + String(je_method) + ", Book" + String(book) + ", Row " + String(i) + " of " + String(rows))
			
				depr_ind = ds_depr_expense.getitemnumber(i, 'depreciation_indicator')
				je_by_asset = ds_depr_expense.getitemnumber(i, 'je_by_asset')
				dg_id = ds_depr_expense.getitemnumber(i, 'depr_group_id')
				
				
				//DJL - Maint 4940 - 02/2011- No je by asset use original trans type
				if  je_by_asset = 0 then
					
					ds_gl_transaction.insertrow(1)
					gl_acct = f_autogen_je_account(ds_depr_expense, i, 11,  ds_depr_expense.getitemnumber(i,'credit_account_id'), je_method )
					
					if left(gl_acct, 5) = "ERROR" then
						f_pp_msgs("Error in f_autogen_je_account:  " + gl_acct)
						do_not_update = true
					end if
					
					trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
				
					ds_gl_transaction.setitem(1,'gl_trans_id',trans_id)
					ds_gl_transaction.setitem(1,'month',i_nvo_cpr_control.i_month)
					ds_gl_transaction.setitem(1,'company_number',company)
					ds_gl_transaction.setitem(1,'amount', & 
														ds_depr_expense.getitemdecimal(i,'depreciation_expense') * convention)
					ds_gl_transaction.setitem(1,'description', & 
														string(ds_depr_expense.getitemnumber(i,'depr_group_id')))
					ds_gl_transaction.setitem(1,'gl_account', gl_acct)
					ds_gl_transaction.setitem(1,'debit_credit_indicator',0)
					ds_gl_transaction.setitem(1,'gl_je_code',je_code)
					ds_gl_transaction.setitem(1,'gl_status_id',1)
					ds_gl_transaction.setitem(1, 'source', 'DEPR APPROVAL')
					ds_gl_transaction.setitem(1, 'amount_type', amt_type)
					ds_gl_transaction.setitem(1, 'je_method_id', je_method)
					ds_gl_transaction.setitem(1, 'trans_type', 11)
					
				/*DJL - Maint 4940 - 02/2011 - Add option to create Depreciation Journals from CPR Depr by Asset.
				  Detailed asset info, je by asset*/
				// ### DJL - Maint 8827 - 11.2011
				// Depr journals by asset speed fix
				elseif  je_by_asset = 1 then
					
					//Uses new trans type 40 - depr expense credit JE by asset
					lst_gl_trans_parms.a_month = i_nvo_cpr_control.i_month
					lst_gl_trans_parms.a_company = company
					lst_gl_trans_parms.a_description = string(ds_depr_expense.getitemnumber(i,'depr_group_id'))
				     lst_gl_trans_parms.a_debit_credit_indicator = 0
					lst_gl_trans_parms.a_gl_je_code = je_code
					lst_gl_trans_parms.a_gl_status_id = 1
					lst_gl_trans_parms.a_source = 'DEPR APPROVAL'
					lst_gl_trans_parms.a_amount_type = amt_type
					
					ll_month = long(String(i_nvo_cpr_control.i_month, "yyyymm"))
					
					ll_long_args[1] = ll_month
					ll_long_args[2] = i_nvo_cpr_control.i_company
					ll_long_args[3] = dg_id
					ll_long_args[4] = book
					
					rtn = uo_autogen.uf_autogen_je_account(lst_gl_trans_parms, ll_long_args[], 40, je_method , i_nvo_cpr_control.i_company, ds_depr_expense.getitemnumber(i,'credit_account_id'))
					if rtn  < 0 then
						f_pp_msgs("Error in uf_autogen_je_account depr expense debit by asset: " + gl_acct)
						do_not_update = true
					end if
					
				end if// end maint 4940 je by asset, end maint 8827 JE by asset speed fix
	
			next // transaction
		next  // SOB
	next // je_method

	if do_not_update then
		f_pp_msgs(" ")
		f_pp_msgs("* * * * Transactions NOT created due to errors! * * * *")
		f_pp_msgs(" ")
		
		b_failed_company = true
		goto next_company
	end if
	rtn = ds_gl_transaction.update()

	if rtn <> 1 then
	
		rollback using sqlca;
	
		f_pp_msgs("Error updating Expense General Ledger Preview Transactions.")
		
		b_failed_company = true
		goto next_company
	end if
	// 
	// Generate the Recurring Reserve General Ledger Transactions.
	
	f_pp_msgs("Generate the Recurring Input Reserve GL Transactions..."+ ' ' + string(now()))	
	
	// check for recurring transactions already loaded
	// Maint 8604:  change ds_depr_activity to i_ds_depr_activity
	i_ds_depr_activity = CREATE uo_ds_top
	i_ds_depr_activity.dataobject = "dw_depr_recur2gl"
	i_ds_depr_activity.SetTransObject(sqlca)

	//get the je methods for manual transactions
	je_rows = ds_je_method_trans_type.Retrieve(18, i_nvo_cpr_control.i_company)
	if je_rows < 0 then
		f_pp_msgs("ERROR retrieving JE Method-Trans Type data for Manual Reserve Transaction (18 & 19) entries.~nSQL:" + ds_je_method_trans_type.i_sqlca_sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if je_rows = 0 then
		f_pp_msgs("ERROR No records round in JE Method-Trans Type data for Manual Reserve Transaction (18 & 19) entries.")
		
		b_failed_company = true
		goto next_company
	end if
		
	for je = 1 to je_rows
		je_method = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
		amt_type =  ds_je_method_trans_type.GetItemNumber(je, "amount_type")
		
		// make sure ALL books in the JE Method are valid for this company
		select count(*), sum(nvl(include_indicator,0)), je.description
		into :book_count, :book_include, :je_descr
		from je_method_set_of_books j, company_set_of_books c, je_method je
		where j.set_of_books_id = c.set_of_books_id (+)
		and j.je_method_id = :je_method
		and c.company_id = :i_nvo_cpr_control.i_company
		and j.je_method_id = je.je_method_id
		group by je.description
		; 
		
		if book_count <> book_include then
			f_pp_msgs("Warning: Depr JE Method Books do not match.  THIS JE METHOD WILL BE SKIPPED." )
			f_pp_msgs("Method " + je_descr +" has " + String(book_count) + " but company uses " + String(book_include) )
			continue
		end if			
		
		// now get the set of books for the je method
		jebooks_rows = ds_je_method_set_of_books.Retrieve(je_method)
		if jebooks_rows < 0 then
			f_pp_msgs("Error retrieving JE Method-Set of Books data for Manual Reserve Transaction (18 & 19) entries.~nSQL:" + ds_je_method_set_of_books.i_sqlca_sqlerrtext)
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
		if jebooks_rows = 0 then
			f_pp_msgs("No records round in JE Method-Set of Books data for Manual Reserve Transaction (18 & 19) entries.")
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
			
		for b = 1 to jebooks_rows
			book = ds_je_method_set_of_books.GetItemNumber(b, "set_of_books_id")
			convention = ds_je_method_set_of_books.GetItemNumber(b, "reversal_convention")

			rows = i_ds_depr_activity.Retrieve(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month, book)
			
			if rows < 0 then
				if g_process_id > 0 then
					f_pp_msgs("Error retrieving Recurring Input Reserve entries.~nSQL:" + i_ds_depr_activity.i_sqlca_sqlerrtext)
				end if
				rollback;
				
				b_failed_company = true
				goto next_company
			end if
		
		
			for i = 1 to rows
		
				f_pp_msgs("Recurring Input: " + String(i) + " of " + String(rows))
				
				if not uf_depr_gl_trans_post(i, "DEPR RECUR", je_method) then 

					rollback using sqlca;
					
					f_pp_msgs("Error updating Recurring Reserve General Ledger Transactions.")
					
					b_failed_company = true
					goto next_company
				end if
			
			next
		next // JE book
	next // JE method

	f_pp_msgs( "     Processing Reg Entries...")
		
	rtn = this.uf_reg_entries_preview(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month, 'DEPR')
	if rtn <> 1 or isnull(rtn) then
		
		f_pp_msgs( "Error During Preview of Reg Entries")
		rollback;
		
		b_failed_company = true
		goto next_company
	end if
	
	////no errors with this company, so commit
	f_pp_msgs( "    No errors with this company, so committing preview journal entries.")
	commit;
	
next_company:

	if b_failed_company = true then
		f_pp_msgs( "    Failed company encountered, so rolling back preview journal entries created for this company.")
		rollback;
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
	end if
	
	DESTROY ds_depr_expense
	DESTROY ds_je_method_set_of_books
	DESTROY ds_je_method_trans_type
	DESTROY ds_depr_journal
	DESTROY uo_autogen

	if not isnull(ds_gl_transaction) and isvalid(ds_gl_transaction) then 
		ds_gl_transaction.Reset()
	end if
	
	b_failed_company = false


next // company



if upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
	i_nvo_cpr_control.of_log_failed_companies("PREVIEW DEPR")
	return false
else
	return true
end if
end function

public function boolean uf_afc_before_oh ();//**********************************************************************************************************************************************************
//
// uf_afc_before_oh()
// 
// Generate Preview JEs for Overheads 
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************


string  company, je_code, month, where_str, acct, loading_dw
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
////	added for company loop
longlong	c, num_companies
datetime ovh_approval
//	$$$ mtc 20090702
longlong je_method_id


f_pp_msgs(  "Generating Preview Journal Entries for Before Overheads")

f_pp_msgs("Generating Preview GL Journal Entries - Month: " + string(i_nvo_wo_control.i_month_number) + &
	 " Company:  " + i_curr_comp_descr + " Comp Id: " + string(i_nvo_wo_control.i_company))

// Generate the General Ledger Transactions.

// create uo_ds_top
uo_ds_top dw_gl_transaction
dw_gl_transaction = CREATE uo_ds_top
dw_gl_transaction.Dataobject = 'dw_gl_transaction_preview'
dw_gl_transaction.SetTransObject(sqlca)

uo_ds_top dw_afudc_gl_trans
dw_afudc_gl_trans = CREATE uo_ds_top
dw_afudc_gl_trans.Dataobject = 'dw_afudc_gl_trans'
dw_afudc_gl_trans.SetTransObject(sqlca)

uo_ds_top dw_clearing_wo_control
dw_clearing_wo_control = CREATE uo_ds_top
dw_clearing_wo_control.Dataobject = 'dw_clearing_wo_control'
dw_clearing_wo_control.SetTransObject(sqlca)

uo_ds_top dw_gl_charges
dw_gl_charges = CREATE uo_ds_top
dw_gl_charges.Dataobject = 'dw_gl_charges_afc'
dw_gl_charges.SetTransObject(sqlca)

num_clear_rows = dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)

f_pp_msgs("Generating Preview GL Journal Entries (Overheads) ...")

dw_gl_charges.Dataobject = 'dw_gl_charges_oh'

dw_gl_charges.settransobject(sqlca)

where_str   = " and cwip_charge.status in " &
				+ "(select distinct clearing_id from clearing_wo_control where before_afudc_indicator = 1) " 

f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month_number, i_nvo_wo_control.i_company)


select gl_company_no into :company 
  from company where company_id = :i_nvo_wo_control.i_company;

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Generating Preview GL Journal Entries (Overheads) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.insertrow(1)
		
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)

	dw_gl_transaction.setitem(1,'gl_status_id',1)

	amount = dw_gl_charges.getitemdecimal(i,'amount')

	ce_id    = dw_gl_charges.getitemnumber(i, 'cost_element_id')
	acct_id  = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	// The Overhead Type is saved in the Status column of the charges
	oh_id = dw_gl_charges.getitemnumber(i, 'status')
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')

	row_num = dw_clearing_wo_control.find("clearing_id = " + string(oh_id), &
														1, num_clear_rows)

	if row_num < 0 then
		f_pp_msgs("dw Find Error on dw_clearing_wo_control.")
		
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if

		
	if row_num = 0 then
		f_pp_msgs("Could not find the dw_clearing_wo_control record for clearing_id " + &
			"= " + string(oh_id))

		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if		
	
	clear_type  = dw_clearing_wo_control.getitemnumber(row_num, 'clearing_indicator')
	credit_acct = dw_clearing_wo_control.getitemnumber(row_num, 'credit_gl_account')
	je_code     = dw_clearing_wo_control.getitemstring(row_num, 'clearing_je')

	je_code = trim(je_code)

	dw_gl_transaction.setitem(1,'gl_je_code', je_code)
	
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')

	if clear_type = 1 then                         // clearing overhead
	
		acct = f_autogen_je_account(dw_gl_charges,i, 2,	acct_id,je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if
		
		dw_gl_transaction.setitem(1,'gl_account', acct)

		if amount > 0 then
			dw_gl_transaction.setitem(1,'amount',                 amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		else
			dw_gl_transaction.setitem(1,'amount',                -amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		end if
		
		 // store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))

		dw_gl_transaction.setitem(1,'amount_type', 1)
		dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
		dw_gl_transaction.setitem(1,'trans_type', 2)
		
		
	else														  // loading overhead
		
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		
		acct = f_autogen_je_account(dw_gl_charges,i, 3,	acct_id, je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if

		dw_gl_transaction.setitem(1,'gl_account', acct)

		dw_gl_transaction.setitem(1,'amount_type', 1)
		dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
		dw_gl_transaction.setitem(1,'trans_type', 3)


		// Must also create credit offset transaction
		dw_gl_transaction.InsertRow(1)
		
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

		dw_gl_transaction.setitem(1,'gl_trans_id',            trans_id)
		dw_gl_transaction.setitem(1,'month',                  i_nvo_wo_control.i_month)
		dw_gl_transaction.setitem(1,'company_number',         company)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		
		 // store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))

		dw_gl_transaction.setitem(1,'amount_type', 1)
		dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
			
		loading_dw = f_pp_system_control_company('Loading Datawindow', i_nvo_wo_control.i_company)

		if loading_dw <> 'charges' then
			
			acct = f_autogen_je_account(dw_clearing_wo_control, row_num, 7,	credit_acct, je_method_id)
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		else
			 acct = f_autogen_je_account(dw_gl_charges, i, 7,  credit_acct, je_method_id)
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		end if

		dw_gl_transaction.setitem(1,'gl_account', acct)
		dw_gl_transaction.setitem(1,'trans_type', 7)

	end if 
	
next // Rows

f_pp_msgs("Updating ...")

ret = dw_gl_transaction.Update()

if ret <> 1 then
	f_pp_msgs("Error updating Preview GL Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext)
 
	rollback using sqlca;
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	return false
end if

return true
end function

public function boolean uf_afc_after_oh ();//**********************************************************************************************************************************************************
//
// uf_afc_after_oh()
// 
// Generate Preview JEs for Overheads 
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string  company, je_code, month, where_str, acct, loading_dw
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
////	added for company loop
longlong	c, num_companies
//	$$$ mtc 20090702
longlong je_method_id

//
//	loop over the lb, and react to the selected rows
//
f_pp_msgs(  "Generating Preview Journal Entries for After Overheads")

f_pp_msgs("Generating Preview GL Journal Entries - Month: " + string(i_nvo_wo_control.i_month_number) + &
	 " Company:  " + i_curr_comp_descr + " Comp Id: " + string(i_nvo_wo_control.i_company))

// Generate the General Ledger Transactions.

// create uo_ds_top
uo_ds_top dw_gl_transaction
dw_gl_transaction = CREATE uo_ds_top
dw_gl_transaction.Dataobject = 'dw_gl_transaction_preview'
dw_gl_transaction.SetTransObject(sqlca)

uo_ds_top dw_afudc_gl_trans
dw_afudc_gl_trans = CREATE uo_ds_top
dw_afudc_gl_trans.Dataobject = 'dw_afudc_gl_trans'
dw_afudc_gl_trans.SetTransObject(sqlca)

uo_ds_top dw_clearing_wo_control
dw_clearing_wo_control = CREATE uo_ds_top
dw_clearing_wo_control.Dataobject = 'dw_clearing_wo_control'
dw_clearing_wo_control.SetTransObject(sqlca)

uo_ds_top dw_gl_charges
dw_gl_charges = CREATE uo_ds_top
dw_gl_charges.Dataobject = 'dw_gl_charges_afc'
dw_gl_charges.SetTransObject(sqlca)

num_clear_rows = dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)

f_pp_msgs("Generating Preview GL Journal Entries (Overheads) ...")

dw_gl_charges.Dataobject = 'dw_gl_charges_oh'

dw_gl_charges.settransobject(sqlca)

where_str   = " and cwip_charge.status in " &
				+ "(select distinct clearing_id from clearing_wo_control where before_afudc_indicator = 0) " 

f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month_number, i_nvo_wo_control.i_company)

select gl_company_no into :company 
from company where company_id = :i_nvo_wo_control.i_company;

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Generating Preview GL Journal Entries (Overheads) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.insertrow(1)
		
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)

	dw_gl_transaction.setitem(1,'gl_status_id',1)

	amount = dw_gl_charges.getitemdecimal(i,'amount')
	
	ce_id    = dw_gl_charges.getitemnumber(i, 'cost_element_id')
	acct_id  = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	// The Overhead Type is saved in the Status column of the charges	  
	oh_id = dw_gl_charges.getitemnumber(i, 'status')
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')
	
	row_num = dw_clearing_wo_control.find("clearing_id = " + string(oh_id), &
														1, num_clear_rows)
	
	if row_num < 0 then
		f_pp_msgs( "ERROR: dw Find Error on dw_clearing_wo_control")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if

		
	if row_num = 0 then
		f_pp_msgs( "Could not find the dw_clearing_wo_control record for clearing_id " + &
			"= " + string(oh_id))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if		
	
	clear_type  = dw_clearing_wo_control.getitemnumber(row_num, 'clearing_indicator')
	credit_acct = dw_clearing_wo_control.getitemnumber(row_num, 'credit_gl_account')
	je_code     = dw_clearing_wo_control.getitemstring(row_num, 'clearing_je')
	
	je_code = trim(je_code)
	
	dw_gl_transaction.setitem(1,'gl_je_code', je_code)

	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	if clear_type = 1 then                         // clearing overhead
	
		acct = f_autogen_je_account(dw_gl_charges,i,2,	acct_id, je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if
		dw_gl_transaction.setitem(1,'gl_account', acct)

		if amount > 0 then
			dw_gl_transaction.setitem(1,'amount',                 amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		else
			dw_gl_transaction.setitem(1,'amount',                -amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		end if
		
		 // store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))
		
		dw_gl_transaction.setitem(1,'amount_type',1)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',2)
		
	else	// loading overhead
		
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		
		acct = f_autogen_je_account(dw_gl_charges,i,3,	acct_id, je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if
		dw_gl_transaction.setitem(1,'gl_account', acct)
		
		dw_gl_transaction.setitem(1,'amount_type',1)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',3)

		// Must also create credit offset transaction
		dw_gl_transaction.InsertRow(1)
		
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

		dw_gl_transaction.setitem(1,'gl_trans_id',            trans_id)
		dw_gl_transaction.setitem(1,'month',                  i_nvo_wo_control.i_month)
		dw_gl_transaction.setitem(1,'company_number',         company)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		
		// store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))
		
		dw_gl_transaction.setitem(1,'amount_type',1)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		
		loading_dw = f_pp_system_control_company('Loading Datawindow', i_nvo_wo_control.i_company)
			
		if loading_dw <> 'charges' then
			
			acct = f_autogen_je_account(dw_clearing_wo_control,row_num, 7,	credit_acct, je_method_id)
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		else
			 acct = f_autogen_je_account(dw_gl_charges,i, 7,  credit_acct, je_method_id)  
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		end if
		 
		dw_gl_transaction.setitem(1,'gl_account', acct)
		dw_gl_transaction.setitem(1,'trans_type', 7)

	end if 
	
next// Rows

f_pp_msgs("Updating ...")

ret = dw_gl_transaction.Update()

if ret <> 1 then
	f_pp_msgs( "Error updating General Ledger Preview Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext)
	rollback using sqlca;
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	return false
end if

return true
end function

public function boolean uf_afc_real ();//**********************************************************************************************************************************************************
//
// uf_afc_after_oh()
// 
// Generate Preview JEs for AFUDC and CPI.  Also calls the WIP comp function.
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string  company, je_code, month, where_str, acct, wo_process_ans, comp_descr, je_code_option
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i, check_debt_afudc, check_equity_afudc, debt_account_id, equity_account_id  
integer mo, yr, ret, clear_type, rtn, have_wip
decimal {2} amount
////	added for company loop
longlong	c, num_companies, pp_stat_id, my_count

longlong wip_rtn

uo_wip_computations uo_wip
uo_wip = create uo_wip_computations

select count(*) into :have_wip
from   wip_computation  where rownum = 1;

longlong je_method_id

//
//	loop over the lb, and react to the selected rows
//
f_pp_msgs( "Generating Preview Journal Entries for AFUDC ")

f_pp_msgs("Generating Preview GL Journal Entries - Month: " + string(i_nvo_wo_control.i_month_number) + &
	 " Company:  " + i_curr_comp_descr + " Comp Id: " + string(i_nvo_wo_control.i_company) )

select description  into :comp_descr from company where company_id = :i_nvo_wo_control.i_company;

f_pp_msgs("Generating Preview GL Journal Entries started for company " +  comp_descr +  " at " + String(Today(), "hh:mm:ss"))

// Generate the General Ledger Transactions.

// create uo_ds_top
uo_ds_top dw_gl_transaction
dw_gl_transaction = CREATE uo_ds_top
dw_gl_transaction.Dataobject = 'dw_gl_transaction_preview'
dw_gl_transaction.SetTransObject(sqlca)

uo_ds_top dw_afudc_gl_trans
dw_afudc_gl_trans = CREATE uo_ds_top
dw_afudc_gl_trans.Dataobject = 'dw_afudc_gl_trans'
dw_afudc_gl_trans.SetTransObject(sqlca)

uo_ds_top dw_clearing_wo_control
dw_clearing_wo_control = CREATE uo_ds_top
dw_clearing_wo_control.Dataobject = 'dw_clearing_wo_control'
dw_clearing_wo_control.SetTransObject(sqlca)

uo_ds_top dw_gl_charges
dw_gl_charges = CREATE uo_ds_top
dw_gl_charges.Dataobject = 'dw_gl_charges_afc'
dw_gl_charges.SetTransObject(sqlca)

where_str   = " and cost_element_id in " &
				+ "(select distinct equity_cost_element from afudc_control) " &
				+ " and cwip_charge.company_id  = " + string(i_nvo_wo_control.i_company)
				
f_add_where_clause_ds(dw_gl_charges, "", where_str, false)

num_rows       = dw_afudc_gl_trans.RETRIEVE(i_nvo_wo_control.i_month)
num_clear_rows = dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)
num_gl_rows    = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month)

if num_rows <= 0 then
	// For debugging purposes...
	datetime cpr_month
	string user_id
	longlong session_id, afc_count
	select month, user_id, session_id
	  into :cpr_month, :user_id, :session_id
	  from cpr_act_month;
	  
	select count(*)
	  into :afc_count
	  from current_afudc_view;
	  
	 if num_rows < 0 then
	 	f_pp_msgs("ERROR: dw_afudc_gl_trans.retrieve failed.")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	elseif num_rows = 0 then
		f_pp_msgs("Information: dw_afudc_gl_trans.retrieve resulted in 0 rows")
		f_pp_msgs("Information: CPR_ACT_MONTH.USER_ID = '" + user_id + "', MONTH = '" + string(cpr_month) + "', SESSSION_ID = '" + string(session_id) + "'")
		f_pp_msgs("Information: CURRENT_AFUDC_VIEW COUNT = '" + string(afc_count) + "'")
	end if
end if

if num_gl_rows < 0 then
	f_pp_msgs(  "ERROR:  Generating Preview GL Journal Entries (AFUDC Equity) : dw_gl_charges.RETRIEVE failed  .")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if

f_pp_msgs( "Generating " + string(num_gl_rows) + " Preview GL Journal Entries (AFUDC Equity) ...")

 
 check_equity_afudc = 0
 
 Select count(*) into :check_equity_afudc
 from cwip_charge where status = 1 and
 month_number = :i_nvo_wo_control.i_month_number and
 company_id = :i_nvo_wo_control.i_company and
 cost_element_id in (select equity_cost_element from afudc_control);

if check_equity_afudc > 0 and num_gl_rows <= 0 then
	f_pp_msgs(  "ERROR:  Generating Preview GL Journal Entries (AFUDC Equity) : Failed to retrieve Debt charges for Journals  .")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if      


select gl_company_no into :company 
  from company where company_id = :i_nvo_wo_control.i_company;

select je_id into :je_id 
  from gl_je_control where process_id = 'AFUDC';
  
// lkkk
je_code_option = f_pp_system_control_company('AFUDC Approval - Use External JE', i_nvo_wo_control.i_company)

if je_code_option = 'yes' or je_code_option = '' then
	select external_je_code into :je_code 
	 from standard_journal_entries where je_id = :je_id;
else
	select gl_je_code into :je_code 
	 from standard_journal_entries where je_id = :je_id;
end if

// Process the AFUDC Equity transactions
for i = 1 to num_gl_rows
	
	if mod(i, 50) = 0 then
		f_pp_msgs("Generating Preview GL Journal Entries (AFUDC Equity) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if
	
	dw_gl_transaction.insertrow(1)

	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')									
	// Must find the credit account for this afudc type
	afc_id = dw_gl_charges.getitemnumber(i, 'afudc_type_id')

	row_num = dw_afudc_gl_trans.Find("afudc_type_id = " + string(afc_id), 1, num_rows)

	if row_num < 0 then
		f_pp_msgs(  "ERROR: dw Find Error on Afudc Type  ")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if
		
	if row_num = 0 then
		f_pp_msgs( "Afudc Type not found. Check for missing debt and equity accounts " + &
			"in the AFUDC_DATA table.")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if
	
	// ### DJL 062011 
	// Maint 7591 - Change AFUDC approval to pass gl_account_id (instead of external_account_code) to f_autogen_je_account.
	//acct = dw_afudc_gl_trans.getitemstring(row_num, 'equity_account')
	equity_account_id = dw_afudc_gl_trans.getitemnumber(row_num, 'equity_account_id')
	
	//	$$$ mtc 20090702
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	acct = f_autogen_je_account(dw_gl_charges, i, 0, equity_account_id, je_method_id)	
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
		
	je_code = trim(je_code)

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',1)
	dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
	dw_gl_transaction.setitem(1,'trans_type', 0)
	
	
	dw_gl_transaction.InsertRow(1)

	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
	
	acct_id = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	acct = f_autogen_je_account(dw_gl_charges, i, 1, acct_id,je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if								

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i, 'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type', 1)
	dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
	dw_gl_transaction.setitem(1,'trans_type', 1)

next

dw_gl_charges.Dataobject = 'dw_gl_charges_afc'

dw_gl_charges.SetTransObject(sqlca)

where_str   = " and cost_element_id in " &
				+ "(select distinct debt_cost_element from afudc_control) " &
				+ " and cwip_charge.company_id  = " + string(i_nvo_wo_control.i_company)


f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month)

// maint 5649 
if num_gl_rows < 0 then
	f_pp_msgs(  "ERROR:  Generating Preview GL Journal Entries (AFUDC Debt) : dw_gl_charges.RETRIEVE failed  .")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
 end if
 
 f_pp_msgs( "Generating " + string(num_gl_rows) + " Preview GL Journal Entries (AFUDC Debt) ...")

 check_debt_afudc = 0
 
 Select count(*) into :check_debt_afudc
 from cwip_charge where status = 1 and
 month_number = :i_nvo_wo_control.i_month_number and
 company_id = :i_nvo_wo_control.i_company and
 cost_element_id in (select debt_cost_element from afudc_control);

 if check_debt_afudc > 0 and num_gl_rows <= 0 then
	f_pp_msgs(  "ERROR:  Generating Preview GL Journal Entries (AFUDC Debt) : Failed to retrieve Debt charges for Journals  .")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
 end if      

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Generating Preview GL Journal Entries (AFUDC Debt) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.InsertRow(1)

	trans_id = f_topkey_no_dw('gl_transaction', 'gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',  		 dw_gl_charges.getitemdecimal(i,'amount'))
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')																				
	afc_id = dw_gl_charges.getitemnumber(i,'afudc_type_id')

	row_num = dw_afudc_gl_trans.Find("afudc_type_id = " + string(afc_id), 1, num_rows)

	if row_num < 0 then
		f_pp_msgs(  "ERROR: dw Find Error on Afudc Type  ")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if

		
	if row_num = 0 then
		f_pp_msgs( "Afudc Type not found. Check for missing debt and equity accounts " + &
			"in the AFUDC_DATA table.")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if	
	
	// Maint 7591 - Change AFUDC approval to pass gl_account_id (instead of external_account_code) to f_autogen_je_account.
	//acct = dw_afudc_gl_trans.getitemstring(row_num,'debt_account')
	debt_account_id = dw_afudc_gl_trans.getitemnumber(row_num, 'debt_account_id')
	
	//	$$$ mtc 20090702
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	acct = f_autogen_je_account(dw_gl_charges, i, -1, debt_account_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
	
	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',1)
	dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
	dw_gl_transaction.setitem(1,'trans_type',-1)

	dw_gl_transaction.insertrow(1)

	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
											
	acct_id = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	acct = f_autogen_je_account(dw_gl_charges, i, 1, acct_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))

	dw_gl_transaction.setitem(1,'amount_type', 1)
	dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
	dw_gl_transaction.setitem(1,'trans_type', 1)
	
next // Debt Row

// BEGIN: create journals for CPI (if "turned on" via je_method table)
f_pp_msgs("Generating Preview GL Journal Entries (CPI) ...")

dw_gl_charges.Dataobject = 'dw_gl_charges_cpi'

dw_gl_charges.SetTransObject(sqlca)

where_str   = " and cwip_charge.company_id  = " + string(i_nvo_wo_control.i_company) + " "
  
f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month)

// maint 5649 
if num_gl_rows < 0 then
	f_pp_msgs(  "ERROR:  Generating Preview GL Journal Entries (CPI) : dw_gl_charges.RETRIEVE failed  .")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if
 
f_pp_msgs( "Generating " + string(num_gl_rows) + " Preview GL Journal Entries (CPI) ...")

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Generating Preview GL Journal Entries (CPI) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.InsertRow(1)

	trans_id = f_topkey_no_dw('gl_transaction', 'gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')                                                            
	afc_id = dw_gl_charges.getitemnumber(i,'afudc_type_id')
	
	row_num = dw_afudc_gl_trans.Find("afudc_type_id = " + string(afc_id), 1, num_rows)
		
	if row_num < 0 then
		f_pp_msgs(  "ERROR: dw Find Error on CPI  ")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if
		
	if row_num = 0 then
		f_pp_msgs( "Afudc Type not found. Check for missing debt and equity accounts " + &
		"in the AFUDC_DATA table.")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if   
	
  // Maint 7591 - Change AFUDC approval to pass gl_account_id (instead of external_account_code) to f_autogen_je_account.	
  // acct = dw_afudc_gl_trans.getitemstring(row_num,'debt_account')
  debt_account_id = dw_afudc_gl_trans.getitemnumber(row_num, 'debt_account_id')
	
	//   $$$ mtc 20090702
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	acct = f_autogen_je_account(dw_gl_charges, i, 35, debt_account_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
	
	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',99)
	dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
	dw_gl_transaction.setitem(1,'trans_type',35)

	dw_gl_transaction.insertrow(1)

	trans_id = f_topkey_no_dw('gl_transaction', 'gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
											
	acct_id = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	acct = f_autogen_je_account(dw_gl_charges, i, 36, acct_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the preview transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
	
	// lots of folks do not have gl_account.external_account_code populated for CPI, 
	// so this is a default to prevent an Oracle error (gl_transaction.gl_account is not nullable)
	if acct = '' or IsNull(acct) then acct = 'CPI'

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',99)
	dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
	dw_gl_transaction.setitem(1,'trans_type',36)
	
next // CPI Row
// END: create journals for CPI (if "turned on" via je_method table)

f_pp_msgs("Updating ...")

ret = dw_gl_transaction.Update()

if ret <> 1 then
	f_pp_msgs( "Error updating General Ledger Preview Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext )
	rollback using sqlca;
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if

if have_wip > 0 then

	if not this.uf_afc_wip_comp() then
		rollback using sqlca;
		f_pp_msgs( "Error in Generating Preview JEs for WIP Comp. ")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		if isvalid(uo_wip) then
			destroy uo_wip
		end if
		rollback;
		return false
	end if
 
end if

return true
end function

public function boolean uf_clear_previews ();//**********************************************************************************************************************************************************
//
// uf_clear_previews()
// 
// Clear out previously loaded entries for the company, month, and processing type passed to the SSP.  This is run prior to the generation logic.
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string sqls, co_ids_str, trans_type_ids_str, aro_depr

trans_type_ids_str = ""

//Trans Types specific to AFUDC, Overheads, WIP Comps

//AFUDC, Overheads, & Wip Computations
 
if i_run_accruals then
	//1 : true if "Accruals" is checked
	trans_type_ids_str = trans_type_ids_str+"30,31,"
end if

if i_run_afudc then
	//2 : true if "AFUDC, Overheads, & Wip Computations is checked
	trans_type_ids_str = trans_type_ids_str+"-1,0,1,2,3,7,33,34,35,36,"
end if

if i_run_aro then
	//3 : true if "ARO" is checked
	trans_type_ids_str = trans_type_ids_str+"20,21,22,23,24,25,26,27,28,41,42,"
end if

if i_run_lease then
	//4 : true if "Lease Depreciation" is checked
	trans_type_ids_str = trans_type_ids_str+"3032,3033,"
end if

if i_run_depr then
	//5 : true if "Depreciation" is checked
	trans_type_ids_str = trans_type_ids_str+"10,11,18,19,"
end if

if trans_type_ids_str <> '' then
	//remove the trailing ","
	f_pp_msgs(" Building statement to clear out previously previewed journal entries.")
	trans_type_ids_str = mid( trans_type_ids_str,1, len( trans_type_ids_str ) - 1)
	f_pp_msgs(" Trans Types to clear: "+trans_type_ids_str)
	
	co_ids_str = f_parsenumarrayintostring( i_company_idx )
	f_pp_msgs(" Company IDs to clear: "+co_ids_str)
	
	//build and execute the delete statement for the current month
	sqls = "delete from gl_transaction_preview "+&
		" where to_char(month,'yyyymm') = "+string(i_month_number)+" "+&
		" and trim(company_number) in (  "+&
		" 	select trim(gl_company_no) "+&
		" 	from company "+&
		" 	where company_id in ( "+co_ids_str+ " ) "+&
		" ) "+&
		" and trans_type in ( "+trans_type_ids_str+") "
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR clearing out previously generated preview JEs.  Error: " + sqlca.SQLErrText)
		f_pp_msgs("Failed SQLS : "+sqls)	
		rollback;					
		return false
	end if
	
	//if ARO and/or Accruals are being run, clear previous entries for the month after month_number
	if (i_run_aro or i_run_accruals) then
		f_pp_msgs(" Clearing out ARO and/or Accruals preview data")
		
		//get a date with next month, actual day doesn't matter
		date month_number_next
		string month_next, month, year, trans_type_ids_next_month_str
		month_number_next = relativeDate(date(i_month),31)
		
		//construct the month number string
		month = string(month(month_number_next))
		if len(month) = 1 then //prepend a 0 in case its a single digit month
			month = '0' + month
		end if
		year = string(year(month_number_next))
		month_next = year+month
		
		//construct the string of types to clear entries for
		if i_run_aro then
			trans_type_ids_next_month_str = '20,21,22,23,24,25,26,27,28,41,42,'
		end if
		
		if i_run_accruals then 
			trans_type_ids_next_month_str = trans_type_ids_next_month_str + '30,31,'
		end if
		
		//remove the trailing ","
		f_pp_msgs(" Building statement to clear out previously previewed ARO/Accruals journal entries.")
		trans_type_ids_next_month_str = mid( trans_type_ids_next_month_str,1, len( trans_type_ids_next_month_str ) - 1)
		
		sqls = "delete from gl_transaction_preview "+&
		" where to_char(month,'yyyymm') = "+string(month_next)+" "+&
		" and trim(company_number) in (  "+&
		" 	select trim(gl_company_no) "+&
		" 	from company "+&
		" 	where company_id in ( "+co_ids_str+ " ) "+&
		" ) "+&
		" and trans_type in ( "+trans_type_ids_next_month_str+") "
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR clearing out previously generated preview JEs for ARO and/or Accruals.  Error: " + sqlca.SQLErrText)
			f_pp_msgs("Failed SQLS : "+sqls)	
			rollback;					
			return false
		end if
		
	end if
	
	//REG entries don't have trans types, so we have to run another delete statement...
	if i_run_aro or i_run_depr then		
		if i_run_aro and i_run_depr then 
			aro_depr = "'ARO','DEPR'"
		elseif i_run_aro and NOT i_run_depr then
			aro_depr = "'ARO'"
		elseif NOT i_run_aro and i_run_depr then
			aro_depr = "'DEPR'"
		end if
		f_pp_msgs(" Clearing REG "+aro_depr+" entries for company ids: "+co_ids_str)
	
		//build and execute the delete statement
		sqls = "delete from gl_transaction_preview "+&
			" where source like 'REG ENTRIES%' "+&
			" and trim(company_number) in (  "+&
			" 	select trim(gl_company_no) "+&
			" 	from company "+&
			" 	where company_id in ( "+co_ids_str+ " ) "+&
			" ) "+&
			" and (month, trim(DESCRIPTION)) in "+&
			" ( "+&
			" select GL_POSTING_MO_YR, trim(TO_CHAR(ENTRY_TRANS_ID)) "+&
			" from REGULATORY_TRANSACTIONS "+&
			" where COMPANY_ID in ( "+co_ids_str+ " ) "+&
			" and PROCESSING_MONTH = TO_DATE('10/2012', 'mm/yyyy') "+&
			" and to_char(PROCESSING_MONTH,'yyyymm') = "+string(i_month_number)+" "+&
			" and ENTRY_ID in "+&
			" 	( "+&
			" 		select ENTRY_ID "+&
			" 		from REGULATORY_ENTRIES, REGULATORY_ENTRY_TYPE "+&
			" 		where REGULATORY_ENTRIES.ENTRY_TYPE_ID = REGULATORY_ENTRY_TYPE.ENTRY_TYPE_ID "+&
			" 		and REGULATORY_ENTRY_TYPE.ARO_DEPR in ( "+aro_depr+" ) "+&
			" 	) "+&
			" ) "
			
		execute immediate :sqls;

		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR clearing out previously generated REG preview JEs.  Error: " + sqlca.SQLErrText)
			f_pp_msgs("Failed SQLS : "+sqls)	
			rollback;					
			return false
		end if
		
	end if
					 
end if

return true

end function

public function boolean uf_afc_wip_comp ();//**********************************************************************************************************************************************************
//
// uf_afc_wip_comp()
// 
// Generate Preview JEs for WIP Comps - pulled and edited from uo_wip_computations.uf_wip_comp_approve
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string  company, je_code, month, where_str, acct,wo_process_ans, comp_descr, je_code_option
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i, ext_rtn
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
string wip_comp_desc

longlong je_method_id, amount_type

//Call Extension Function
	
ext_rtn = f_wip_comp_extension(-1,i_nvo_wo_control.i_month_number, 'approve',-1,i_nvo_wo_control.i_company)

if ext_rtn = -1 then
	f_pp_msgs("Error Occured in f_wip_comp_extension for wip computation")
	return false
end if

// Generate the General Ledger Transactions.
	
	// create uo_ds_top
	uo_ds_top dw_gl_transaction
	dw_gl_transaction = CREATE uo_ds_top
	dw_gl_transaction.Dataobject = 'dw_gl_transaction_preview'
	dw_gl_transaction.SetTransObject(sqlca)
	
	uo_ds_top dw_gl_charges
	dw_gl_charges = CREATE uo_ds_top
	dw_gl_charges.Dataobject = 'dw_gl_charges_wip_comp'
	dw_gl_charges.SetTransObject(sqlca)

	num_gl_rows    = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month,i_nvo_wo_control.i_company)

	//JRD - Deal with To Company Id later
	select gl_company_no into :company 
	  from company where company_id = :i_nvo_wo_control.i_company;
	
	
	// Process the AFUDC Equity transactions
	for i = 1 to num_gl_rows
		
		if mod(i, 50) = 0 then
			f_pp_msgs("Creating GL Journal Entries (AFUDC Equity) ... Finished " + &
				string(i) + " of " + string(num_gl_rows))
		end if
		
		dw_gl_transaction.insertrow(1)
	
		//First do WIP Side
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
		dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
		dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
		dw_gl_transaction.setitem(1,'company_number', company)
		dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
//		w_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')
		afc_id = dw_gl_charges.getitemnumber(i, 'afudc_type_id')
		wip_comp_desc = dw_gl_charges.getitemstring(i,'description')
		acct_id = dw_gl_charges.getitemnumber(i, 'wip_gl_account')		
		je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
		amount_type  = dw_gl_charges.getitemdecimal(i,'amount_type')
		acct = f_autogen_je_account(dw_gl_charges, i, 33, acct_id,je_method_id)
		////	maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct )
			rollback using sqlca;
			return false
		end if
		
		je_code =	dw_gl_charges.getitemstring(i,'journal_code')
		je_code = trim(je_code)
		
		dw_gl_transaction.setitem(1,'gl_account',             acct)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 1) //original side the sign is same
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
		dw_gl_transaction.setitem(1,'source', left(wip_comp_desc,35))
		dw_gl_transaction.setitem(1,'amount_type',amount_type)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',33)
		
		//Now do Offset Side
		dw_gl_transaction.InsertRow(1)
	
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
		dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
		dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
		dw_gl_transaction.setitem(1,'company_number', company)
		dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
		
		acct_id = dw_gl_charges.getitemnumber(i, 'offset_gl_account')		
		acct = f_autogen_je_account(dw_gl_charges, i, 34, acct_id,je_method_id)
		////	maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct )
			rollback using sqlca;
			return false
		end if
	
		dw_gl_transaction.setitem(1,'gl_account',             acct)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 0) //offset side sign on amount will be flipped
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		
		dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
		dw_gl_transaction.setitem(1,'source', left(wip_comp_desc,35))
		dw_gl_transaction.setitem(1,'amount_type',amount_type)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',34)
		
	next
	
	f_pp_msgs("Updating ...")

	ret = dw_gl_transaction.Update()
	
	if ret <> 1 then
		f_pp_msgs( + &
		"Error updating General Ledger Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext) 
		rollback using sqlca;
		return false
	end if
	

return true
end function

public function string uf_aro_db_preview (datetime a_month, longlong a_company_id);/************************************************************************************************************************************************************
 **
 **	uf_aro_db_preview()
 **	
 **	Creates gl transactions in gl_transaction_preview
 **	No commits in this function
 **	This function is a re-creation of the relevant logic from the PKG_ARO 
 **	PL/SQL package used in ARO approval
 **	
 **	This function is currently called from 
 **		- w_preview_jes.cb_run.clicked
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	string			:	""		:	The calculation was successful
 **										:	"ERROR"		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
 string str_ret, sqls, rtn, gl_acc_str_debit, gl_acc_str_credit, row
 longlong i
 longlong asset_id, depr_group_id, cor_reserve_acct_id, je_method_id, set_of_books_id, reversal_convention, amount_type, amount

//DECLARE F_APPROVE_REGULATED PROCEDURE FOR 
//	PKG_ARO.F_APPROVE_REGULATED (:a_company_id, :a_month);
//
//execute F_APPROVE_REGULATED;
//if sqlca.sqlCode <> 0 then
//	messageBox("ERROR", "Error creating ORACLE function F_APPROVE_REGULATED: " + sqlca.sqlErrText)
//	return "ERROR"
//end if
//
//FETCH F_APPROVE_REGULATED INTO :str_ret;
//if sqlca.sqlCode <> 0 then
//	messageBox("ERROR", "Error executing ORACLE function F_APPROVE_REGULATED: " + sqlca.sqlErrText)
//	return "ERROR"
//end if
//
//close F_APPROVE_REGULATED;

uo_ds_top ds_rows
ds_rows = create uo_ds_top
ds_rows.setTransObject(sqlca)

//create the query
sqls = 'select pend.set_of_books_id,'	+ &
			  			'pend.depr_group_id,' + &
						'dg.cor_reserve_acct_id,'	 + &
						'aro.cor_credit_acct_id,'	+&
						'pend.amount,' + &
						'aro.asset_id,' + &
						'msob.reversal_convention,' + &
						'm.amount_type,' + &
						'm.je_method_id' + &
					'from pend_aro_reg_activity pend,' +	&
						'aro,' + &
						'depr_group dg,' + &
						'je_method m,' + &
						'je_method_set_of_books msob,' + &
						'je_method_trans_type mtype' + &
					'where aro.aro_id = pend.aro_id' + &
						'and dg.depr_group_id = pend.depr_group_id' + &
						'and m.je_method_id = mtype.je_method_id' + &
						'and msob.set_of_books_id = pend.set_of_books_id' + &
						'and msob.je_method_id = m.je_method_id' + &
						'and mtype.trans_type = 41' +	&
						'and aro.company_id = A_COMPANY_ID' +	&
						'and pend.gl_post_mo_yr = A_MONTH)'
						
rtn =  f_create_dynamic_ds( ds_rows, "grid", sqls, sqlca, true )

for i = 1 to ds_rows.rowCount()
	//grab the relevant parameters from the query
	asset_id = ds_rows.getItemNumber(i,'asset_id')
	depr_group_id = ds_rows.getItemNumber(i, 'depr_group_id')
	cor_reserve_acct_id = ds_rows.getItemNumber(i, 'cor_reserve_acct_id')
	je_method_id = ds_rows.getItemNumber(i, 'je_method_id')
	set_of_books_id = ds_rows.getItemNumber(i, 'set_of_books_id')
	reversal_convention = ds_rows.getItemNumber(i, 'reversal_convention')
	amount_type = ds_rows.getItemNumber(i, 'amount_type')
	amount = ds_rows.getItemNumber(i, 'amount')
	//get the gl debit account string
	select pp_gl_transaction(41, :asset_id, 0, :depr_group_id, 0, :cor_reserve_acct_id, &
									 0, 0, :je_method_id, :set_of_books_id, :reversal_convention, &
									 :amount_type)
	into :gl_acc_str_debit
	from dual;
	//get the gl credit account string
	select pp_gl_transaction(42, :asset_id, 0, :depr_group_id, 0, :cor_reserve_acct_id, &
									 0, 0, :je_method_id, :set_of_books_id, :reversal_convention, &
									 :amount_type)
	into :gl_acc_str_credit
	from dual;
	//debit insert
	insert into gl_transaction_preview
				(gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount,
				gl_je_code, gl_status_id, description, source, asset_id, amount_type, je_method_id,
				trans_type)
			select
				pwrplant1.nextval, :A_MONTH, c.gl_company_no, :GL_ACC_STR_DEBIT, 1, :amount,
				'ARO', 1, 'Regulated ARO Debit', 'ARO PREVIEW', :asset_id, :amount_type, :je_method_id, 41
			from company c
			where c.company_id = :A_COMPANY_ID;
	if sqlca.sqlCode <> 0 then
		messageBox("ERROR", "Error inserting into GL Transaction Preview " + sqlca.sqlErrText)
		return "ERROR"
	end if
	//credit insert
	insert into gl_transaction_preview
				(gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount,
				gl_je_code, gl_status_id, description, source, asset_id, amount_type, je_method_id,
				trans_type)
			select
				pwrplant1.nextval, :A_MONTH, c.gl_company_no, :GL_ACC_STR_DEBIT, 0, :amount,
				'ARO', 1, 'Regulated ARO Credit', 'ARO PREVIEW', :asset_id, :amount_type, :je_method_id, 42
			from company c
			where c.company_id = :A_COMPANY_ID;
	if sqlca.sqlCode <> 0 then
		messageBox("ERROR", "Error inserting into GL Transaction Preview " + sqlca.sqlErrText)
		return "ERROR"
	end if
next


str_ret = "OK"
return str_ret
end function

public function longlong uf_aro_update_preview (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	uf_aro_update_preview()
 **	
 **	Updates staging table with completion status
 **	
 **	This function is currently called from 
 **		- w_preview_jes.cb_run.clicked
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	longlong		:	1		:	The calculation was successful
 **										:	-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

//update cpr_control
//set aro_approved = sysdate
//where company_id = :a_company_id
//and accounting_month = :a_month;
//
//if sqlca.sqlcode <> 0 then
//	f_pp_msgs("ERROR: Updating aro approved on cpr control: " + sqlca.sqlerrtext)
//	return -1
//end if

return 1
end function

public function longlong uf_aro_preview_for_company (longlong a_company_id, datetime a_month);/*********************************************************************
*
*	uf_aro_preview_for_company(): Runs ARO Preview for a given company/month
*									No commits or rollbacks
*
*	Parameters: longlong a_company_id: The company to process
*					datetime a_month: The month to process
*	
*	Returns: 1 Success, -1 Failure
*
**********************************************************************/

longlong rtn, pp_stat_id
string comp_descr, rtn_str
//nvo_aro_reg reg_aro_engine

// Log some info
f_pp_msgs("Company_id: " + string(a_company_id)+ ' ' + string(now()))
g_stats.set_met_start("ARO Preview for Company")

//Dont need validations

// Validate ARO has already been calculated this month
//if not uf_aro_calculated(a_company_id, a_month) then
//	f_pp_msgs("The ARO results must be calculated in order to be approved.")
//	return -1
//end if

// Validate ARO hasn't already been approved this month
//if uf_aro_approved(a_company_id, a_month) then
//	f_pp_msgs("The ARO results have already been approved.")
//	return -1
//end if

// Validate next month is open
//if not uf_next_month_open(a_company_id, a_month) then
//	f_pp_msgs("Next month must be open before approving the ARO results.")
//	return -1
//end if

select description  
into :comp_descr 
from company 
where company_id = :a_company_id;
f_pp_msgs("")
f_pp_msgs("ARO Preview Processing Started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))

// Start Statistics
pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "ARO Preview: " + comp_descr)

// Preview accretion
f_pp_msgs( "    Creating Journal Entries..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Preview: Creating Journal Entries")

rtn = uf_aro_preview_accretion(a_company_id, a_month)
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error during ARO Approval function")
	return -1
end if
g_stats.set_met_end("ARO Approval: Creating Journal Entries")

// Preview reg entries
f_pp_msgs( "     Previewing Reg Entries..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Preview: Reg Entries")
rtn = uf_reg_entries_preview(a_company_id, a_month, 'ARO')
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error During Approving of Reg Entries ARO's")
	return -1
end if
g_stats.set_met_end("ARO Preview: Reg Entries")

// Preview regulated ARO's
f_pp_msgs( "     Prevewing Regulated ARO's..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Preview: Regulated ARO's")
//reg_aro_engine = create nvo_aro_reg

rtn_str = uf_aro_db_preview(a_month, a_company_id)
if rtn_str <> 'OK' or isnull(rtn_str) then
	f_pp_msgs("Error During Preview of Regulated ARO's")
	return -1
else
//	DESTROY reg_aro_engine
end if
g_stats.set_met_end("ARO preview: Regulated ARO's")

// Update ARO preview on Preview Table
g_stats.set_met_start("ARO Preview: Update Preview Table")
rtn = uf_aro_update_preview(a_company_id, a_month)
if rtn <> 1 then
	return -1
end if
g_stats.set_met_end("ARO Preview: Update Preview Table")

// End Statistics
if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
g_stats.set_met_end("ARO Calc for Company")

f_pp_msgs("ARO Preview Completed for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
f_pp_msgs("")

return 1
end function

public function longlong uf_aro_determine_sob (longlong a_aro_id);/************************************************************************************************************************************************************
 **
 **	f_aro_determine_sob()
 **	
 **	Determines the set of books for an ARO.  This function is currently called from:
 **		f_aro_approve_accretion()
 **		f_aro_calc_settlement()
 **		uo_aro_estimates.uf_settlement_aro()
 **		w_aro_details.wf_je_liability_account()
 **		w_aro_settlement.cb_close.clicked()
 **	
 **	Parameters	:	longlong	:	(a_aro_id) The ARO for which to determine set of books
 **	
 **	Returns		:	longlong	:	1		:	The calculation was successful
 **										-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			aro_id, bs_id, sob, num_rows
string			sqls, s_rtn

uo_ds_top 	ds_sob

////
////	Store off the ARO ID.
////
		aro_id = a_aro_id

////
////	Get the ARO book summary ID from book summary.  Use the override on ARO if there is one.
////
		select 	nvl( aro.book_summary_override, bs_view.book_summary_id )
		into 		:bs_id
		from 		aro,
					(	select 	max( book_summary_id ) book_summary_id
						from 		book_summary 
						where 	upper( trim( book_summary_type ) ) = 'ARO' 
					) bs_view
		where 	aro.aro_id = :aro_id;

////
////	Build a datastore to retrieve the set of books IDs using this basis.
////	Only include sets of books that are making journal entries.
////
		ds_sob = create uo_ds_top
		
		sqls =	"	select 	set_of_books_id " + "~r~n" + &
				"	from 		set_of_books " + "~r~n" + &
				"	where 	basis_" + string( bs_id ) + "_indicator = 1 " + "~r~n" + &
				"	and 		set_of_books_id in " + "~r~n" + &
				"				(	select 	set_of_books_id " + "~r~n" + &
				"					from 		je_method_set_of_books " + "~r~n" + &
				"				) "
		
		s_rtn = f_create_dynamic_ds( ds_sob, "grid", sqls, sqlca, true )
		
		if s_rtn <> "OK" then
			messageBox( "Error", "Error occurred while creating the datastore for set of books determination.~n~nError: " + s_rtn )
			rollback;
			destroy ds_sob
			return -1
		end if

////
////	Make sure there is only one row.
////
		num_rows = ds_sob.rowCount()
		
		if num_rows = 0 then
			messageBox( "Error", "The book summary for ARO ID " + string( aro_id ) + " is not related to any Set Of Books." )
			rollback;
			destroy ds_sob
			return -1
		end if
		
		if num_rows > 1 then
			messageBox( "Error", "The book summary for ARO ID " + string( aro_id ) + " is related to " + string( num_rows ) + " Set of Books.  AROs can only be related to a single Set of Books." )
			rollback;
			destroy ds_sob
			return -1
		end if

////
////	Get the set of books ID and return.
////
		sob = ds_sob.getItemNumber( 1, "set_of_books_id" )
		destroy ds_sob
		return sob
end function

public function integer uf_aro_preview_accretion (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	f_aro_preview_accretion()
 **	
 **	Previews the accretion for a company and month.  Creates entries in GL Transaction Staging.
 **	
 **	This function is currently called from 
 **		- w_preview_jes - cb_run.clicked()
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to approve.
 **						datetime		:	( a_month ) The month to approve.
 **	
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			rtn, trans_id, acct_id, aro_id, bs_id, je_method_count, je_method_id, reversal_convention, deb_cred_ind
longlong 			i, month_number, liab_acct, gain_acct, loss_acct, rows, liability_account, acctng_adj_account, reserve_acct_id, accretion_account, month, year
longlong	trans_depr_exp_cred, trans_aro_accr_deb, trans_aro_accr_cred, trans_aro_gain_loss_deb, trans_aro_gain_loss_cred, trans_aro_settle_liab, trans_aro_settle_wo, trans_aro_settle_ext_gl, trans_aro_tr_adj_deb
decimal{2} 	gain_loss, depr, accr, amount
datetime 	gl_posting_mo_yr, new_month
string 		ext_gl_acct, je_code, company_num, sqls, s_rtn, bs_override, trans_desc, settlement_cr_by_acct
boolean 		pp_msgs, multiple_je_method

uo_ds_top 	dss[]
uo_ds_top 	ds_aro_je_method
uo_ds_top 	ds_gl_transaction
uo_ds_top 	ds_gl_charges
uo_ds_top 	ds_aro_settle
uo_ds_top 	ds_aro_accretion_gl
uo_ds_top 	ds_aro_gainloss_gl
uo_ds_top	ds_aro_transition_month_end
uo_ds_top 	ds_aro_accretion_gl_trans
uo_ds_top 	ds_aro_accretion_gl_adj	

////
////	Keep track of the datastores to destroy on errors
////
dss[upperBound(dss)+1] = ds_aro_je_method
dss[upperBound(dss)+1] = ds_gl_transaction
dss[upperBound(dss)+1] = ds_gl_charges
dss[upperBound(dss)+1] = ds_aro_settle
dss[upperBound(dss)+1] = ds_aro_accretion_gl
dss[upperBound(dss)+1] = ds_aro_gainloss_gl
dss[upperBound(dss)+1] = ds_aro_transition_month_end
dss[upperBound(dss)+1] = ds_aro_accretion_gl_trans
dss[upperBound(dss)+1] = ds_aro_accretion_gl_adj
		
////
////	Initialize the booleans.
////
		pp_msgs = false
		multiple_je_method = true
		
////
////	Set up the transaction type constants.
////
////	11 - Depr expense credit
////	20 - ARO Accretion Debit (to expense)
////	21 - ARO Accretion Credit (to liability)
////	22 - ARO Transition Adjustment Debit 
////	24 - ARO Gain(-)/Loss(+) Debit (to liability)
////	25 - ARO Gain(-)/Loss(+) Credit (to g/l acct)
////	26 - ARO Settlement Debit to Liability
////	27 - ARO Settlement Credit to Work Order
////	28 - ARO Settlement Credit external_gl_account
////
		trans_depr_exp_cred = 11
		trans_aro_accr_deb = 20
		trans_aro_accr_cred = 21
		trans_aro_tr_adj_deb = 22
		trans_aro_gain_loss_deb = 24
		trans_aro_gain_loss_cred = 25
		trans_aro_settle_liab = 26		
		trans_aro_settle_wo = 27		
		trans_aro_settle_ext_gl = 28	
		
////
////	If this is month end process, update the messages.
////
		 if g_process_id > 0 then
			f_pp_msgs( "ARO Approval for : " + string( a_company_id ) )
			pp_msgs = true
		end if 
		
////
////	Look up if set of books override is turned on.
////	- Yes - Allow user to specify the Book Summary the ARO will add new layer ARC dollars. 
////	- No - Will use Book Summary Type: ARO.
////
		bs_override = upper( f_pp_system_control( "ARO BOOK SUMMARY OVERRIDE" ) )
		if isNull( bs_override ) then bs_override = "NO"
		
////
////	Enable ARO Settlement Credits by External GL Acct.  Get the system control value to see if they are enabled.
////	Set the datastore for GL charges based on whether they are doing charges by external GL account or not.
////
////	JDH - Maintenance 5962
////
		settlement_cr_by_acct = upper( f_pp_system_control( "ARO Settlmnt Credits by Ext GL Acct" ) )
		if isNull( settlement_cr_by_acct ) then settlement_cr_by_acct = "NO"

		ds_gl_charges = create uo_ds_top
		
		if settlement_cr_by_acct = "YES" then 
			ds_gl_charges.dataobject = "dw_gl_charges_aro_ext_acct"
		else
			ds_gl_charges.dataobject = "dw_gl_charges_aro"
		end if

		ds_gl_charges.setTransObject( sqlca )
		
////
////	Set up the datastores.
////
		ds_gl_transaction = create uo_ds_top
		ds_gl_transaction.dataobject = "dw_gl_transaction_preview"
		ds_gl_transaction.setTransObject( sqlca )
		
		ds_aro_settle = create uo_ds_top
		ds_aro_settle.dataobject = "dw_aro_settlement_header"
		ds_aro_settle.setTransObject( sqlca )
		
		ds_aro_accretion_gl = create uo_ds_top
		ds_aro_accretion_gl.dataobject = "dw_aro_accretion_gl"
		ds_aro_accretion_gl.setTransObject( sqlca )
		
		ds_aro_gainloss_gl = create uo_ds_top
		ds_aro_gainloss_gl.dataobject = "dw_aro_gainloss_gl"
		ds_aro_gainloss_gl.setTransObject( sqlca )
		
		ds_aro_transition_month_end = create uo_ds_top
		ds_aro_transition_month_end.dataobject = "dw_aro_transition_month_end"
		ds_aro_transition_month_end.setTransObject( sqlca )
		
		ds_aro_accretion_gl_trans = create uo_ds_top
		ds_aro_accretion_gl_trans.dataobject = "dw_aro_accretion_gl_transition"
		ds_aro_accretion_gl_trans.setTransObject( sqlca )
		
		ds_aro_accretion_gl_adj = create uo_ds_top
		ds_aro_accretion_gl_adj.dataobject = "dw_aro_accretion_gl_adj"
		ds_aro_accretion_gl_adj.setTransObject( sqlca )
		
		ds_aro_je_method = create uo_ds_top

////
////	If there is only one JE Method, skip all the datastore look ups.  Just create the datastore once now.
////	It will retrieve the one JE Method and its reversal convention.
////	We don't need to restrict by trans_type, as we can assume this JE method is related to all trans types.
////	NOTE: This will use set of books ID 1, meaning it is NOT using the overrides by ARO.
////
		setNull( je_method_count )
		select 	count(*) 
		into 		:je_method_count
		from 		je_method 
		where 	amount_type = 1;
		
		if isNull( je_method_count ) then je_method_count = 0

		if je_method_count = 1 then
		////
		////	Set the set of books to 1.
		////
				bs_id = 1			
				
		////
		////	Retrieve the JE Methods.
		////
				sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
						"				je_method_set_of_books.reversal_convention " + "~r~n" + &
						" 	from 		je_method_set_of_books, " + "~r~n" + &
						"				company_je_method_view " + "~r~n" + &
						" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
						" 	and 		je_method_set_of_books.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
						" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
			
				s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
			
				if s_rtn <> "OK" then
					messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (for only one method).~n~nError: " + s_rtn )
					destroy ds_aro_je_method
					destroy ds_gl_transaction
					destroy ds_gl_charges
					destroy ds_aro_settle
					destroy ds_aro_accretion_gl
					destroy ds_aro_gainloss_gl
					destroy ds_aro_transition_month_end
					destroy ds_aro_accretion_gl_trans
					destroy ds_aro_accretion_gl_adj
					rollback;
					return -1
				end if
			
		////
		////	Set the boolean.
		////
				multiple_je_method = false
		end if
		
////
////	Get the company number.
////
		setNull( company_num )
		
		select 	gl_company_no 
		into 		:company_num 
		from 		company 
		where 	company_id = :a_company_id;
		
////
////	Set the GL Posting Mo Yr and convert it to a month number.
////
		gl_posting_mo_yr = a_month
		month_number = year( date( gl_posting_mo_yr ) ) * 100 + month( date( gl_posting_mo_yr ) )

////
////	Get the JE code.
////
		select 	standard_journal_entries.gl_je_code 
		into 		:je_code
		from 		standard_journal_entries, 
					gl_je_control
		where 	gl_je_control.process_id = 'ARO'
		and 		standard_journal_entries.je_id = gl_je_control.je_id;

		if isNull( je_code ) or je_code = "" then
			f_status_box( "ARO Approval", "Cannot find GL JE Code for the ARO Process." )
			destroy ds_aro_je_method
			destroy ds_gl_transaction
			destroy ds_gl_charges
			destroy ds_aro_settle
			destroy ds_aro_accretion_gl
			destroy ds_aro_gainloss_gl
			destroy ds_aro_transition_month_end
			destroy ds_aro_accretion_gl_trans
			destroy ds_aro_accretion_gl_adj
			return -1
		end if

////
////	SETTLEMENT ENTRIES
////

////
////	Retrieve the GL Charges.  This will give us the charges in CWIP Charge by ARO.
////
		rows = ds_gl_charges.retrieve( month_number, a_company_id )
		
		if rows < 0 then
			f_status_box( "ARO Approval", "Error retrieving Settlement entries.~n~nError:" + ds_gl_charges.i_sqlca_SQLErrText )
			destroy ds_aro_je_method
			destroy ds_gl_transaction
			destroy ds_gl_charges
			destroy ds_aro_settle
			destroy ds_aro_accretion_gl
			destroy ds_aro_gainloss_gl
			destroy ds_aro_transition_month_end
			destroy ds_aro_accretion_gl_trans
			destroy ds_aro_accretion_gl_adj
			return -1
		else
			if pp_msgs then f_pp_msgs( "Processing Settlement Entries.  Records to process : " + string( rows ) )
		end if
		
////
////	Loop through the rows.
////
		for i = 1 to rows
		////
		////	Update the status.
		////
				if pp_msgs and mod( i, 50 ) = 1 then f_pp_msgs( "Processing record " + string( i ) + " of " + string( rows ) )
				if rows > 500 and mod( i, 50 ) = 1 then f_progressbar( "ARO Approval", "Settlement Journals " + string( a_company_id ), rows, i )
				
		////
		////	Get the data from the row and retrieve the settlement header datastore.
		////	It will only have one row.
		////
				aro_id = ds_gl_charges.getItemNumber( i, "aro_id" )
				acct_id = ds_gl_charges.getItemNumber( i, "gl_account_id" )
				amount = ds_gl_charges.getItemDecimal( i, "amount" )
				
				ds_aro_settle.retrieve( aro_id )
			
		////
		////	Get the data for the ARO from the header.
		////
				liab_acct = ds_aro_settle.getItemNumber( 1, "liability_account" )
				gain_acct = ds_aro_settle.getItemNumber( 1, "gain_account" )
				loss_acct = ds_aro_settle.getItemNumber( 1, "loss_account" )
				
		////
		////	If the override is turned on, get the set of books.  Otherwise, use 1.
		////
				if bs_override = "YES" then
					bs_id = uf_aro_determine_sob( aro_id )
				else
					bs_id = 1
				end if
			
				if bs_id < 0 then
					rollback;
					destroy ds_aro_je_method
					destroy ds_gl_transaction
					destroy ds_gl_charges
					destroy ds_aro_settle
					destroy ds_aro_accretion_gl
					destroy ds_aro_gainloss_gl
					destroy ds_aro_transition_month_end
					destroy ds_aro_accretion_gl_trans
					destroy ds_aro_accretion_gl_adj
					return -1
				end if
						
		////
		////	TRANSACTION TYPE: 27 - ARO Settlement Credit to Work Order
		////
		
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_settle_wo ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 27).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
		
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
						
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_gl_charges, i, trans_aro_settle_wo, acct_id, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
			
				////
				////	Insert a row into the GL Transaction datastore.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setItem( 1, "description", "ARO Settlement Credit" )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )		
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_settle_wo)
					
				////
				////	Calculate the amount.
				////
						amount = amount * reversal_convention
						
						if amount > 0 then
							ds_gl_transaction.setItem( 1, "amount", amount )
							ds_gl_transaction.setItem( 1, "debit_credit_indicator", 1 )
						else
							ds_gl_transaction.setItem( 1, "amount", -amount )
							ds_gl_transaction.setItem( 1, "debit_credit_indicator", 0 )
						end if
						
						amount = amount * reversal_convention
				next //JE methods - trans type 27
			
		////
		////	TRANSACTION TYPE: 26 - ARO Settlement Debit to Liability
		////
			
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_settle_liab ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 26).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
			
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
						
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_settle, 1, trans_aro_settle_liab, liab_acct, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if		
			
				////
				////	Insert a row into the GL Transaction datastore.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setItem( 1, "description", "ARO Settlement Debit" )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )		
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_settle_liab)
			
				////
				////	Calculate the amount.
				////
						amount = amount * reversal_convention
						
						if amount > 0 then
							ds_gl_transaction.setItem( 1, "amount", amount )
							ds_gl_transaction.setItem( 1, "debit_credit_indicator", 0 )
						else
							ds_gl_transaction.setItem( 1, "amount", -amount )
							ds_gl_transaction.setItem( 1, "debit_credit_indicator", 1 )
						end if
						
						amount = amount * reversal_convention
				next //JE methods - trans type 26
		next //GL Charges (ds_gl_charges)
		
////
////	ACCRETION EXPENSE ENTRIES
////
////	Create journals for accretion, based on the accreted column on the ARO Liability table.
////

////
////	Calculate the new month.
////
		month = month( date( a_month ) )
		year = year( date( a_month ) )
		
		month = month + 1
		
		if month = 13 then 
			month = 1
			year = year + 1
		end if
		
		new_month = datetime( date( string( month ) + "/1/" + string( year ) ) )
	
////
////	Retrieve the accretion datastore for the month and company.
////
		rows = ds_aro_accretion_gl.retrieve( a_month, a_company_id )
		
		if rows < 0 then
			f_status_box( "ARO Approval", "Error retrieving Accretion Expense entries.~n~nError:" + ds_aro_accretion_gl.i_sqlca_SQLErrText )
			destroy ds_aro_je_method
			destroy ds_gl_transaction
			destroy ds_gl_charges
			destroy ds_aro_settle
			destroy ds_aro_accretion_gl
			destroy ds_aro_gainloss_gl
			destroy ds_aro_transition_month_end
			destroy ds_aro_accretion_gl_trans
			destroy ds_aro_accretion_gl_adj
			return -1
		else
			if pp_msgs then f_pp_msgs( "Processing Accretion Expense entries.  Records to process : " + string( rows ) )
		end if

////
////	Loop through the accretion rows.
////
		for i = 1 to rows
		////
		////	Update the messages.
		////
				if pp_msgs and mod( i, 50 ) = 1 then f_pp_msgs( "Processing record " + string( i ) + " of " + string( rows ) )
				if rows > 500 and mod( i,50 ) = 1 then f_progressbar( "ARO Approval", "Accretion Journals " + string( a_company_id ), rows, i )
				
		////
		////	Get the data from the row.
		////
				aro_id = ds_aro_accretion_gl.getItemNumber( i, "aro_id" )
				accr = ds_aro_accretion_gl.getItemDecimal( i, "accretion_expense" )
				accretion_account = ds_aro_accretion_gl.getItemNumber( i, "accretion_account" )
				liability_account = ds_aro_accretion_gl.getItemNumber( i, "liability_account" )
				
		////
		////	If the override is turned on, get the set of books.  Otherwise, use 1.
		////
				if bs_override = "YES" then
					bs_id = uf_aro_determine_sob( aro_id )
				else
					bs_id = 1
				end if
			
				if bs_id < 0 then
					rollback;
					destroy ds_aro_je_method
					destroy ds_gl_transaction
					destroy ds_gl_charges
					destroy ds_aro_settle
					destroy ds_aro_accretion_gl
					destroy ds_aro_gainloss_gl
					destroy ds_aro_transition_month_end
					destroy ds_aro_accretion_gl_trans
					destroy ds_aro_accretion_gl_adj
					return -1
				end if
				
		////
		////	TRANSACTION TYPE: 20 - ARO Accretion Debit (to expense)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_accr_deb ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 20).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
				
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
						
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_accretion_gl, i, trans_aro_accr_deb, accretion_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
						
				////
				////	Insert a row into the GL Transaction datastore.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", new_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setItem( 1, "description", "Accretion Expense: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "source", "ARO FUTURE" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "amount", accr * reversal_convention )
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 1 )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_accr_deb)
				next //JE methods - trans type 20
		
		////
		////	TRANSACTION TYPE: 21 - ARO Accretion Credit (to liability)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_accr_cred ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 21).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
	
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
						
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_accretion_gl, i, trans_aro_accr_cred, liability_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
		
				////
				////	Insert a row into the GL Transaction datastore.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", new_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setItem( 1, "description", "Accrete to Liab: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "source", "ARO FUTURE" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "amount", accr * reversal_convention )
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 0 )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_accr_cred)
				next //JE methods - trans type 21
		next //Accretion Amounts (ds_aro_accretion_gl)
		
////
////	GAIN/LOSS ENTRIES
////
////	Create journals for Gain/Loss, based on the gain_loss and input_gain_loss on the ARO Liability table.
////
		
////
////	Retrieve the gain/loss datastore for the month and company.
////
		rows = ds_aro_gainloss_gl.retrieve( a_company_id, a_month )
		
		if rows < 0 then
			f_status_box( "ARO Approval", "Error retrieving Gain/Loss entries.~n~nError:" + ds_aro_gainloss_gl.i_sqlca_SQLErrText )
			destroy ds_aro_je_method
			destroy ds_gl_transaction
			destroy ds_gl_charges
			destroy ds_aro_settle
			destroy ds_aro_accretion_gl
			destroy ds_aro_gainloss_gl
			destroy ds_aro_transition_month_end
			destroy ds_aro_accretion_gl_trans
			destroy ds_aro_accretion_gl_adj
			return -1
		else
			if pp_msgs then f_pp_msgs( "Processing Gain/Loss Entries.  Records to process : " + string( rows ) )
		end if

////
////	Loop through the gain/loss rows.
////
		for i = 1 to rows
		////
		////	Update the messages.
		////
				if pp_msgs and mod( i, 50 ) = 1 then f_pp_msgs( "Processing record " + string( i ) + " of " + string( rows ) )
				if rows > 500 and mod( i,50 ) = 1 then f_progressbar( "ARO Approval", "Gain/Loss Journals " + string( a_company_id ), rows, i )
			
		////
		////	Get the data from the row.
		////
				aro_id = ds_aro_gainloss_gl.getItemNumber( i, "aro_id" )
				gain_loss = ds_aro_gainloss_gl.getItemDecimal( i, "gain_loss" )
				gain_acct = ds_aro_gainloss_gl.getItemNumber( i, "gain_account_id" )
				loss_acct = ds_aro_gainloss_gl.getItemNumber( i, "loss_account_id" )
				liability_account = ds_aro_gainloss_gl.getItemNumber( i,"liability_account_id" )
				
		////
		////	If the override is turned on, get the set of books.  Otherwise, use 1.
		////
				if bs_override = "YES" then
					bs_id = uf_aro_determine_sob( aro_id )
				else
					bs_id = 1
				end if
			
				if bs_id < 0 then
					rollback;
					destroy ds_aro_je_method
					destroy ds_gl_transaction
					destroy ds_gl_charges
					destroy ds_aro_settle
					destroy ds_aro_accretion_gl
					destroy ds_aro_gainloss_gl
					destroy ds_aro_transition_month_end
					destroy ds_aro_accretion_gl_trans
					destroy ds_aro_accretion_gl_adj
					return -1
				end if
				
		////
		////	TRANSACTION TYPE: 25 - ARO Gain(-)/Loss(+) Credit (to g/l acct)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_gain_loss_cred ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 25).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
				
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
						
				////
				////	Compute the gain/loss.
				////
						gain_loss = gain_loss * reversal_convention
						
				////
				////	Set some of the data based on if this is a gain or a loss.
				////
						if gain_loss < 0 then 
							//Gain
							setNull( ext_gl_acct )
							ext_gl_acct = f_autogen_je_account( ds_aro_gainloss_gl, i, trans_aro_gain_loss_cred, gain_acct, je_method_id )
							if mid(ext_gl_acct, 1, 5) = "ERROR" then
								f_status_box("ARO Approval", ext_gl_acct)
								if pp_msgs then f_pp_msgs(ext_gl_acct)
								g_ds_func.of_destroy_datastores(dss)
								rollback;
								return -1
							end if
							trans_desc = "ARO Gain: " + string( aro_id )
						else //Loss
							setNull( ext_gl_acct )
							ext_gl_acct = f_autogen_je_account( ds_aro_gainloss_gl, i, trans_aro_gain_loss_cred, loss_acct, je_method_id )
							if mid(ext_gl_acct, 1, 5) = "ERROR" then
								f_status_box("ARO Approval", ext_gl_acct)
								if pp_msgs then f_pp_msgs(ext_gl_acct)
								g_ds_func.of_destroy_datastores(dss)
								rollback;
								return -1
							end if
							trans_desc = "ARO Loss: " + string( aro_id )
						end if
						
				////
				////	Insert a row into the GL Transaction datastore.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 0 )
						ds_gl_transaction.setitem( 1, "amount", gain_loss * -1 )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", trans_desc )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_gain_loss_cred)
			
				////
				////	Reset the gain/loss.
				////
						gain_loss = gain_loss * reversal_convention
				next //JE methods - trans type 25
		
		////
		////	TRANSACTION TYPE: 24 - ARO Gain(-)/Loss(+) Debit (to liability)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_gain_loss_deb ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 24).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
		
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
						
				////
				////	Compute the gain/loss.
				////
						gain_loss = gain_loss * reversal_convention
						
				////
				////	Set some of the data based on if this is a debit or credit.
				////
						if gain_loss > 0 then 
							trans_desc = "ARO G/L Credit: " + string( aro_id )
							amount = gain_loss
							deb_cred_ind = 0
						else
							trans_desc = "ARO G/L Debit: " + string( aro_id )
							amount = gain_loss * -1
							deb_cred_ind = 1
						end if
						
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_gainloss_gl, i, trans_aro_gain_loss_deb, liability_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
						
				////
				////	Insert a row into the GL Transaction datastore.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", deb_cred_ind )
						ds_gl_transaction.setitem( 1, "amount", amount )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", trans_desc )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_gain_loss_deb)
			
				////
				////	Reset the gain/loss.
				////
						gain_loss = gain_loss * reversal_convention
				next //JE methods - trans type 24
		next //Gain Loss records (ds_aro_gainloss_gl)

////
////	TRANSITION MONTH END ENTRIES
////
////	Create journals for Transition, based on the ARO Transition table.  This only occurs for layers with a gl_posting_mo_yr = a_month.
////
	
////
////	Retrieve the transition datastore for the month and company.
////
		rows = ds_aro_transition_month_end.retrieve( a_month, a_company_id )
		
		if rows < 0 then
			f_status_box( "ARO Approval", "Error retrieving Transition Month End entries.~n~nError:" + ds_aro_transition_month_end.i_sqlca_SQLErrText )
			destroy ds_aro_je_method
			destroy ds_gl_transaction
			destroy ds_gl_charges
			destroy ds_aro_settle
			destroy ds_aro_accretion_gl
			destroy ds_aro_gainloss_gl
			destroy ds_aro_transition_month_end
			destroy ds_aro_accretion_gl_trans
			destroy ds_aro_accretion_gl_adj
			return -1
		else
			if pp_msgs then f_pp_msgs( "Processing Transition Month End Entries.  Records to process : " + string( rows ) )
		end if

////
////	Loop through the transition rows.
////
		for i = 1 to rows
		////
		////	Update the messages.
		////
				if pp_msgs and mod( i, 50 ) = 1 then f_pp_msgs( "Processing record " + string( i ) + " of " + string( rows ) )
				
		////
		////	Get the data from the row.
		////
				aro_id = ds_aro_transition_month_end.getItemNumber( i, "aro_id" )
				depr = ds_aro_transition_month_end.getItemNumber( i, "depr" )
				accr = ds_aro_transition_month_end.getItemNumber( i, "accr" )
				liability_account = ds_aro_transition_month_end.getItemNumber( i, "liability_account" )
				acctng_adj_account =ds_aro_transition_month_end.getItemNumber( i, "acctng_adj_account" )
				reserve_acct_id = ds_aro_transition_month_end.getItemNumber( i, "reserve_acct_id" )
	
		////
		////	If the override is turned on, get the set of books.  Otherwise, use 1.
		////
				if bs_override = "YES" then
					bs_id = uf_aro_determine_sob( aro_id )
				else
					bs_id = 1
				end if
			
				if bs_id < 0 then
					rollback;
					destroy ds_aro_je_method
					destroy ds_gl_transaction
					destroy ds_gl_charges
					destroy ds_aro_settle
					destroy ds_aro_accretion_gl
					destroy ds_aro_gainloss_gl
					destroy ds_aro_transition_month_end
					destroy ds_aro_accretion_gl_trans
					destroy ds_aro_accretion_gl_adj
					return -1
				end if
		
		////
		////	TRANSACTION TYPE: 21 - ARO Accretion Credit (to liability)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_accr_cred ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 21).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
				
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
		
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_transition_month_end, i, trans_aro_accr_cred, liability_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
				
				////
				////	Insert a row into the GL Transaction datastore - Credit to Liability for Accum. Accretion.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 0 )
						ds_gl_transaction.setitem( 1, "amount", accr * reversal_convention )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", "Liability Adj: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_accr_cred)
				next //JE methods - trans type 21
		
		////
		////	TRANSACTION TYPE: 11 - Depr expense credit
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_depr_exp_cred ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 11).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
		
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
		
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_transition_month_end, i, trans_depr_exp_cred, reserve_acct_id, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
				
				////
				////	Insert a row into the GL Transaction datastore - Credit to Accum Depreciation.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 0 )
						ds_gl_transaction.setitem( 1, "amount", depr * reversal_convention )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", "Depr Adj for ARO: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "trans_type", trans_depr_exp_cred)
				next //JE methods - trans type 11
	
		////
		////	TRANSACTION TYPE: 22 - ARO Transition Adjustment Debit 
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_tr_adj_deb ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 22).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if	
	
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
		
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_transition_month_end, i, trans_aro_tr_adj_deb, acctng_adj_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
				
				////
				////	Insert a row into the GL Transaction datastore - Debit to Accum. Acctng Adjment to offset Depr and Accr.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 1 )
						ds_gl_transaction.setitem( 1, "amount", ( accr + depr ) * reversal_convention )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", "ARO Accum. Acct. Adjust.: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_tr_adj_deb)
				next //JE methods - trans type 22	
		next //Transition records (ds_aro_transition_month_end)

////
////	TRANSITION ACCRETION ENTRIES
////
////	Create journals for Transition Accretion, based on the accreted column on the ARO Liability table.
////

////
////	Retrieve the accretion datastore for the month and company.
////
		rows = ds_aro_accretion_gl_trans.retrieve( a_month, a_company_id )
		
		if rows < 0 then
			f_status_box( "ARO Approval", "Error retrieving Transition Accretion entries.~n~nError:" + ds_aro_accretion_gl_trans.i_sqlca_SQLErrText )
			destroy ds_aro_je_method
			destroy ds_gl_transaction
			destroy ds_gl_charges
			destroy ds_aro_settle
			destroy ds_aro_accretion_gl
			destroy ds_aro_gainloss_gl
			destroy ds_aro_transition_month_end
			destroy ds_aro_accretion_gl_trans
			destroy ds_aro_accretion_gl_adj
			return -1
		else
			if pp_msgs then f_pp_msgs( "Processing Transition Accretion Entries.  Records to process : " + string( rows ) )
		end if
	
////
////	Loop through the accretion rows.
////
		for i = 1 to rows
		////
		////	Update the messages.
		////
				if pp_msgs and mod( i, 50 ) = 1 then f_pp_msgs( "Processing record " + string( i ) + " of " + string( rows ) )
				
		////
		////	Get the data from the row.
		////
				aro_id = ds_aro_accretion_gl_trans.getItemNumber( i, "aro_id" )
				accretion_account = ds_aro_accretion_gl_trans.getItemNumber( i, "accretion_account" )
				liability_account = ds_aro_accretion_gl_trans.getItemNumber( i,"liability_account" )
				accr = ds_aro_accretion_gl_trans.getItemDecimal( i, "accretion_expense" )
				
		////
		////	If the override is turned on, get the set of books.  Otherwise, use 1.
		////
				if bs_override = "YES" then
					bs_id = uf_aro_determine_sob( aro_id )
				else
					bs_id = 1
				end if
			
				if bs_id < 0 then
					rollback;
					destroy ds_aro_je_method
					destroy ds_gl_transaction
					destroy ds_gl_charges
					destroy ds_aro_settle
					destroy ds_aro_accretion_gl
					destroy ds_aro_gainloss_gl
					destroy ds_aro_transition_month_end
					destroy ds_aro_accretion_gl_trans
					destroy ds_aro_accretion_gl_adj
					return -1
				end if
				
		////
		////	TRANSACTION TYPE: 20 - ARO Accretion Debit (to expense)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_accr_deb ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 20).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
				
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
		
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_accretion_gl_trans, i, trans_aro_accr_deb, accretion_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
				
				////
				////	Insert a row into the GL Transaction datastore - Credit to Liability for Accum. Accretion.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 1 )
						ds_gl_transaction.setitem( 1, "amount", accr * reversal_convention )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", "Trans Acc Expense: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_accr_deb)
				next //JE methods - trans type 20

		////
		////	TRANSACTION TYPE: 21 - ARO Accretion Credit (to liability)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_accr_cred ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 21).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
	
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
		
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_accretion_gl_trans, i, trans_aro_accr_cred, liability_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
				
				////
				////	Insert a row into the GL Transaction datastore - Credit to Liability for Accum. Accretion.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 0 )
						ds_gl_transaction.setitem( 1, "amount", accr * reversal_convention )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", "Trans Acc Liab: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_accr_cred)
				next //JE methods - trans type 21
		next //Transition Accretion records (ds_aro_accretion_gl_trans)

////
////	ACCRETION ADJUSTMENT ENTRIES
////
////	Create journals for Accretion Adj, based on the accretion_adjust column on the ARO Liability table.
////
	
////
////	Retrieve the accretion datastore for the month and company.
////
		rows = ds_aro_accretion_gl_adj.retrieve( a_month, a_company_id )
		
		if rows < 0 then
			f_status_box( "ARO Approval", "Error retrieving Accretion Adjustment entries.~n~nError:" + ds_aro_accretion_gl_adj.i_sqlca_SQLErrText )
			destroy ds_aro_je_method
			destroy ds_gl_transaction
			destroy ds_gl_charges
			destroy ds_aro_settle
			destroy ds_aro_accretion_gl
			destroy ds_aro_gainloss_gl
			destroy ds_aro_transition_month_end
			destroy ds_aro_accretion_gl_trans
			destroy ds_aro_accretion_gl_adj
			return -1
		else
			if pp_msgs then f_pp_msgs( "Processing Accretion Adjustment Entries.  Records to process : " + string( rows ) )
		end if

////
////	Loop through the accretion rows.
////
		for i = 1 to rows
		////
		////	Update the messages.
		////
				if pp_msgs and mod( i, 50 ) = 1 then f_pp_msgs( "Processing record " + string( i ) + " of " + string( rows ) )
				if rows > 500 and mod( i, 50 ) = 1 then f_progressbar( "ARO Approval", "Accretion Adjustment Journals " + string( a_company_id ), rows, i )
				
		////
		////	Get the data from the row.
		////
				aro_id = ds_aro_accretion_gl_adj.getItemNumber( i, "aro_id" )
				accretion_account = ds_aro_accretion_gl_adj.getItemNumber( i, "accretion_account" )
				liability_account = ds_aro_accretion_gl_adj.getItemNumber( i, "liability_account" )
				accr = ds_aro_accretion_gl_adj.getItemDecimal( i, "accretion_expense" )
				
		////
		////	If the override is turned on, get the set of books.  Otherwise, use 1.
		////
				if bs_override = "YES" then
					bs_id = uf_aro_determine_sob( aro_id )
				else
					bs_id = 1
				end if
			
				if bs_id < 0 then
					rollback;
					destroy ds_aro_je_method
					destroy ds_gl_transaction
					destroy ds_gl_charges
					destroy ds_aro_settle
					destroy ds_aro_accretion_gl
					destroy ds_aro_gainloss_gl
					destroy ds_aro_transition_month_end
					destroy ds_aro_accretion_gl_trans
					destroy ds_aro_accretion_gl_adj
					return -1
				end if
	
		////
		////	TRANSACTION TYPE: 20 - ARO Accretion Debit (to expense)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_accr_deb ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 20).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
	
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
		
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_accretion_gl_adj, i, trans_aro_accr_deb, accretion_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
		
				////
				////	Insert a row into the GL Transaction datastore - Credit to Liability for Accum. Accretion.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 1 )
						ds_gl_transaction.setitem( 1, "amount", accr * reversal_convention )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", "Accr Adj Expense: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_accr_deb)
				next //JE methods - trans type 20
	
		////
		////	TRANSACTION TYPE: 21 - ARO Accretion Credit (to liability)
		////
				
		////
		////	If we have multiple JE methods, create the datastore to retrieve.
		////	If we only have one, we created the datastore above.
		////
				if multiple_je_method then
					sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
							"				je_method_set_of_books.reversal_convention " + "~r~n" + &
							" 	from 		je_method_set_of_books, " + "~r~n" + &
							"				je_method_trans_type, " + "~r~n" + &
							"				company_je_method_view " + "~r~n" + &
							" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
							" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
							" 	and 		je_method_trans_type.trans_type = " + string( trans_aro_accr_cred ) + " " + "~r~n" + &
							" 	and 		je_method_trans_type.je_method_id = company_je_method_view.je_method_id " + "~r~n" + &
							" 	and 		company_je_method_view.company_id = " + string( a_company_id ) + " "
				
					s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
				
					if s_rtn <> "OK" then
						messageBox( "Error", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 21).~n~nError: " + s_rtn )
						destroy ds_aro_je_method
						destroy ds_gl_transaction
						destroy ds_gl_charges
						destroy ds_aro_settle
						destroy ds_aro_accretion_gl
						destroy ds_aro_gainloss_gl
						destroy ds_aro_transition_month_end
						destroy ds_aro_accretion_gl_trans
						destroy ds_aro_accretion_gl_adj
						rollback;
						return -1
					end if
				end if
			
		////
		////	Loop through the JE methods.
		////
				for je_method_count = 1 to ds_aro_je_method.rowCount()
				////
				////	Get the data from the row.
				////
						je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
						reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
		
				////
				////	Get the external GL account.
				////
						setNull( ext_gl_acct )
						ext_gl_acct = f_autogen_je_account( ds_aro_accretion_gl_adj, i, trans_aro_accr_cred, liability_account, je_method_id )
						if mid(ext_gl_acct, 1, 5) = "ERROR" then
							f_status_box("ARO Approval", ext_gl_acct)
							if pp_msgs then f_pp_msgs(ext_gl_acct)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
		
				////
				////	Insert a row into the GL Transaction datastore - Credit to Liability for Accum. Accretion.
				////	Set the data.
				////
						ds_gl_transaction.insertRow( 1 )
							
						//trans_id = f_topkey_no_dw( "gl_transaction", "gl_trans_id" )
						select pwrplant1.nextval into :trans_id from dual;
						if sqlca.sqlcode <> 0 then
							f_pp_msgs("ERROR: Getting new gl_trans_id: " + sqlca.SQLErrText)
							g_ds_func.of_destroy_datastores(dss)
							rollback;
							return -1
						end if
								
						ds_gl_transaction.setItem( 1, "gl_trans_id", trans_id )
						ds_gl_transaction.setItem( 1, "month", a_month )
						ds_gl_transaction.setItem( 1, "company_number", company_num )
						ds_gl_transaction.setItem( 1, "gl_status_id", 1 )
						ds_gl_transaction.setItem( 1, "gl_je_code", je_code )
						ds_gl_transaction.setitem( 1, "source", "ARO CURRENT" )
						ds_gl_transaction.setitem( 1, "je_method_id", je_method_id )	
						ds_gl_transaction.setitem( 1, "debit_credit_indicator", 0 )
						ds_gl_transaction.setitem( 1, "amount", accr * reversal_convention )	
						ds_gl_transaction.setItem( 1, "gl_account", ext_gl_acct )
						ds_gl_transaction.setitem( 1, "description", "Accr. Adj to Liab: " + string( aro_id ) )
						ds_gl_transaction.setitem( 1, "trans_type", trans_aro_accr_cred)
				next //JE methods - trans type 21	
		next //Accretion Adjustment records (ds_aro_accretion_gl_adj)
		
////
////	We're finished with the datastores (except for ds_gl_transaction).
////
		destroy ds_aro_je_method
		destroy ds_gl_charges
		destroy ds_aro_settle
		destroy ds_aro_accretion_gl
		destroy ds_aro_gainloss_gl
		destroy ds_aro_transition_month_end
		destroy ds_aro_accretion_gl_trans
		destroy ds_aro_accretion_gl_adj
						
//////
//////	Update the status on ARO Liability in approval only
//////
//		update 	aro_liability
//		set 		status = 1 
//		where 	aro_id in 
//					( 	select 	aro_id 
//						from 		aro 
//						where 	company_id = :a_company_id 
//					)
//		and 		to_char( month_yr, 'yyyymm' ) = to_char( :a_month, 'yyyymm' );
//		
//		if sqlca.SQLCode < 0 then
//			f_status_box( "ARO Processing", "Error updating Status on ARO Liability for Current Month.  Error: " + sqlca.SQLErrText )
//			destroy ds_gl_transaction
//			rollback;
//			return -1
//		end if
//	
//		update 	aro_liability
//		set 		status = 8 
//		where 	aro_id in 
//					( 	select 	aro_id 
//						from 		aro 
//						where 	company_id = :a_company_id 
//					)
//		and 		to_char( month_yr, 'yyyymm' ) = to_char( add_months( :a_month, 1 ), 'yyyymm' );
//		
//		if sqlca.SQLCode < 0 then
//			f_status_box( "ARO Processing", "Error updating Status on ARO Liability for Next Month.  Error: " + sqlca.SQLErrText )
//			destroy ds_gl_transaction
//			rollback;
//			return -1
//		end if

////
////	Save the entries into GL Transaction.
////
		rtn = ds_gl_transaction.Update()
		
		if rtn < 0 then
			f_status_box( "ARO Approval", "Error updating GL Transactions.  Error: " + ds_gl_transaction.i_sqlca_SQLErrText )
			destroy ds_gl_transaction
			rollback;
			return -1
		end if

////
////	Function completed successfully.
////
		if isValid( w_progressbar ) then close( w_progressbar )
		return 1
end function

public function longlong uf_reg_entries_preview (longlong a_company_id, datetime a_month, string a_aro_depr);/************************************************************************************************************************************************************
 **
 **	uf_reg_entries_preview()
 **	
 **	Previews the regulatory entries for a company and month.  Creates entries in GL Transaction.
 **	
 **	This function is currently called from 
 **		- w_cpr_control - cb_aro_approval.clicked()
 **		- w_cpr_control - cb_depr_approval.clicked()
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to approve.
 **						datetime		:	( a_month ) The month to approve.
 **						string			:	( a_aro_depr ) Whether this is an ARO or Depr entry. It will be either "ARO" or "DEPR".
 **	
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong	i, rows, gl_check, entry_id, entry_type_id
longlong	entries[]
string		sqls, month_str, entry_name, table_name, month_column, column_expression, entry_aro_depr
any 		results[]

////
////	Update the status box.
////
		f_pp_msgs("Processing Regulatory Entry Preview JEs " )

////
////	Retrieve the entry IDs for ARO/Depr.  Make sure there are regulatory transactions for the company and month being processed.
////	The rows in regulatory_transactions get created by f_reg_entries_calc.
////
		month_str = string( a_month, "yyyymm" )
		
		sqls =		"	select 	entry_id " + "~r~n" + &
					"	from 		regulatory_entries " + "~r~n" + &
					"	where 	entry_type_id in " + "~r~n" + &
					"				( 	select 	entry_type_id " + "~r~n" + &
					"					from 		regulatory_entry_type " + "~r~n" + &
					"					where 	lower( aro_depr ) = '" + lower( a_aro_depr ) + "' " + "~r~n" + &
					"				) " + "~r~n" + &
					" 	and 		entry_id in " + "~r~n" + &
					"				( 	select 	entry_id " + "~r~n" + &
					"					from 		regulatory_transactions " + "~r~n" + &
					"					where 	company_id = " + string( a_company_id ) + " " + "~r~n" + &
					" 					and 		to_char( processing_month, 'yyyymm' ) = " + month_str + " " + "~r~n" + &
					"				) "
					
		rows = 0
		rows = f_get_column( results[], sqls )
		entries[] = results[]

		f_pp_msgs("Reg entries to process: " + string ( rows ) )
		if rows = 0 then return 1

////
////	Loop through the entries and process.
////
		for i = 1 to rows
		////
		////	Get the entry ID to process.  Retrieve its type and name.
		////
				entry_id = entries[i]
				
				setNull( entry_type_id )
				setNull( entry_name )
				
				select 	entry_type_id, 
							entry_name 
				into 		:entry_type_id, 
							:entry_name
				from 		regulatory_entries
				where 	entry_id = :entry_id;
				
		////
		////	Update the status box.
		////
				f_pp_msgs( "Reg Entry Name: " + entry_name )
				
		////
		////	Get the regulatory entry data for this type.
		////
				setNull( table_name )
				setNull( month_column )
				setNull( column_expression )
				setNull( entry_aro_depr )
				
				select 	table_name, 
							month_column, 
							column_expression, 
							lower( aro_depr )
				into 		:table_name, 
							:month_column, 
							:column_expression, 
							:entry_aro_depr
				from 		regulatory_entry_type
				where 	entry_type_id = :entry_type_id;
				
		////
		////	Check for the GL account string information.
		////	There can be more than 1 row in regulatory_transactions for a given entry_id (e.g., map multiple depr groups to the same reg entry).
		////	So we need to get a count of rows where gl_acct_string is missing, rather than just retrieving the gl_acct_string.
		////
				setNull( gl_check )
				
				select 	count(*) 
				into 		:gl_check
				from 		regulatory_transactions
				where 	processing_month = :a_month
				and 		company_id = :a_company_id
				and 		status = 1
				and 		entry_id = :entry_id
				and 		adj_flag in ( 0, 1 )
				and 		( gl_acct_string is null or length( trim( gl_acct_string ) ) = 0 );
				
				if isNull( gl_check ) then gl_check = 0
				
				if gl_check > 0 then
					f_pp_msgs( "The GL account string information is missing on one or more Regulatory Preview Transactions" )
					rollback;
					return -1
				end if
				
		////			
		////	Insert into GL Transaction Preview.
		////
				insert into gl_transaction_preview 
				( gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount, gl_je_code, gl_status_id, description, source )
				(	select 	pwrplant1.nextval gl_trans_id, 
								regulatory_transactions.gl_posting_mo_yr month, 
								company_setup.gl_company_no company_number, 
								regulatory_transactions.gl_acct_string gl_account, 
								1 debit_credit_indicator, 
								regulatory_transactions.amount amount, 
								regulatory_transactions.gl_je_code gl_je_code, 
								1 gl_status_id,
								to_char( regulatory_transactions.entry_trans_id ) description, 
								'REG ENTRIES '||to_char( regulatory_transactions.processing_month,'yyyymm' ) source
					from 		regulatory_transactions, 
								company_setup
					where 	regulatory_transactions.processing_month = :a_month
					and 		regulatory_transactions.company_id = :a_company_id
					and 		regulatory_transactions.company_id = company_setup.company_id
					and 		regulatory_transactions.status = 1
					and 		regulatory_transactions.entry_id = :entry_id
					and 		regulatory_transactions.adj_flag in ( 0, 1 )
				);
			
				if sqlca.SQLCode < 0 then
					f_pp_msgs( "SQL error occurred while inserting into GL Transaction for Regulatory Preview Transactions.  Error: " + sqlca.SQLErrText )
					rollback;
					return -1
				end if
				
		////
		////	Update the status box with the number of lines created.
		////
				f_pp_msgs( "Reg Lines Created: " + string( sqlca.SQLNRows ) )
				
		////
		////	If the transaction has been approved, set status = 2 in regulatory_transactions.
		////	All transactions that we inserted in the statement above are now considered approved.
		////
//				update 	regulatory_transactions
//				set 		status = 2
//				where 	processing_month = :a_month
//				and 		company_id = :a_company_id
//				and 		status = 1
//				and 		entry_id = :entry_id
//				and 		adj_flag in ( 0, 1 );
//			
//				if sqlca.SQLCode < 0 then
//					f_status_box( "Regulatory Entries Processing", "SQL error occurred while updating the status to approved on Regulatory Transactions.  Error: " + sqlca.SQLErrText )
//					rollback;
//					return -1
//				end if
		next //loop through entries

////
////	Function completed successfully.
////
		return 1
end function

public function boolean uf_aro_retrieve_co (longlong a_cc);//**********************************************************************************************************************************************************
//
// uf_aro_retrieve_co(a_cc)
// 
// Mimic's the bottom section of nvo_cpr_control.of_retrieve_company.  used to refresh cpr_company for processing (does not insert into pp interface or pp interface dates)
// 
// Parameters : a_cc		: index of company being retrieved
// 
// Returns : (boolean) 	: true : There was no error
//                      : false : There was an error
//
//**********************************************************************************************************************************************************

string where_cpr_company, sqls
longlong rtn

i_nvo_cpr_control.i_company = i_nvo_cpr_control.i_company_idx[a_cc]
i_nvo_cpr_control.i_ds_cpr_company.Settransobject(sqlca)

where_cpr_company =  " upper(cpr_company.user_id) = '" & 
                                                                                + trim(upper(s_user_info.user_id)) + "'" &
               + " and cpr_company.session_id = " + string(s_user_info.session_id) &
               + " and cpr_company.batch_report_id = " + string(0)

sqls = "delete from cpr_company  where " +  where_cpr_company
execute immediate :sqls;
if sqlca.sqlcode <> 0 then
                f_pp_msgs("Failed to delete from cpr_company where "+where_cpr_company)
                return false
else 
                commit;
end if


i_nvo_cpr_control.i_ds_cpr_company.insertrow(1)

i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'user_id',upper(s_user_info.user_id))
i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'session_id',s_user_info.session_id)
i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'batch_report_id',0)
i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'company_id',i_nvo_cpr_control.i_company)

rtn = i_nvo_cpr_control.i_ds_cpr_company.update()

if rtn = 1 then

   commit using sqlca;

   i_nvo_cpr_control.i_ds_cpr_company.reset()
   i_nvo_cpr_control.i_ds_cpr_company.resetupdate()
   return true

else

   f_pp_msgs("Failed to insert into cpr_company.")
   rollback using sqlca;
   return false

end if


end function

public function string uf_ls_book_je (longlong a_ls_asset_id, longlong a_trans_type, longlong a_amt, longlong a_asset_act_id, longlong a_dg_id, string a_wo_id, longlong a_gl_account_id, longlong a_gain_loss, longlong a_pend_trans_id, longlong a_company_id, datetime a_month, longlong a_dr_cr, string a_gl_jc, longlong a_sob_id, string a_orig);string sqls, rtn, ls_gl_account
longlong ds_row_count, ds_index, l_my_count, ll_je_method_id, ll_set_of_books_id, ll_amount_type, ll_reversal_convention
uo_ds_top ds_ls_method_rec

// Create datastore
ds_ls_method_rec = create uo_ds_top
sqls = "select m.je_method_id        as je_method_id, " + &
				  "s.set_of_books_id     as set_of_books_id, " + &
				  "m.amount_type         as amount_type, " + &
				  "s.reversal_convention as reversal_convention " + &
		   "from je_method              m, " + &
				  "je_method_set_of_books s, " + &
				  "company_set_of_books   c, " + &
				  "je_method_trans_type   t " + &
		  "where m.je_method_id = s.je_method_id " + &
			 "and c.set_of_books_id = s.set_of_books_id " + &
			 "and c.include_indicator = 1 " + &
			 "and c.company_id = " + string(a_company_id) + " " + &
			 "and m.je_method_id = t.je_method_id " + &
			 "and t.trans_type = " + string(a_trans_type) + " " + &
			 "and c.set_of_books_id = " + string(a_sob_id)
rtn = f_create_dynamic_ds(ds_ls_method_rec, "grid", sqls, sqlca, true)
if rtn <> "OK" then
	return "Error creating JE method datastore: " + rtn
end if

// Loop through results
ds_row_count = ds_ls_method_rec.rowCount()
for ds_index = 1 to ds_row_count

	ll_je_method_id        = ds_ls_method_rec.getItemNumber(ds_index, 'je_method_id')
	ll_set_of_books_id     = ds_ls_method_rec.getItemNumber(ds_index, 'set_of_books_id')
	ll_amount_type         = ds_ls_method_rec.getItemNumber(ds_index, 'amount_type')
	ll_reversal_convention = ds_ls_method_rec.getItemNumber(ds_index, 'reversal_convention')

	// Before getting the GL account, check to see if we want to ignore the JE type
	select count(1)
	  into :l_my_count
	  from ls_lease_type_jes_exclude a, ls_ilr_options lo, ls_asset la
	 where lo.revision = la.approved_revision
	   and lo.lease_cap_type_id = a.lease_cap_type_id
	   and a.trans_type = :a_trans_type
	   and a.company_id = :a_company_id
	   and la.ls_asset_id = :a_ls_asset_id
	   and lo.ilr_id=la.ilr_id;
	
	ls_gl_account = "IGNORE"
	
	// Get GL account
	if l_my_count = 0 then
		select pp_gl_transaction2(:a_trans_type,:a_ls_asset_id,:a_asset_act_id,:a_dg_id,decode(:a_wo_id, 'null', null, to_number(:a_wo_id)),
                                :a_gl_account_id,:a_gain_loss,:a_pend_trans_id,:ll_je_method_id,
										  :ll_set_of_books_id,:ll_amount_type,:ll_reversal_convention)
		  into :ls_gl_account
		  from dual;

		if lastpos(lower(ls_gl_account), 'error') > 0 then
			return ls_gl_account
		end if
	end if

	// Insert into gl_transaction_preview
	insert into gl_transaction_preview
			  (gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount,
			  gl_je_code, gl_status_id, description, source, originator, comments, pend_trans_id,
			  asset_id, amount_type, je_method_id, tax_orig_month_number, trans_type)
	select  pwrplant1.nextval,
			  :a_month,
			  c.gl_company_no,
			  :ls_gl_account,
			  :a_dr_cr,
			  :a_amt,
			  :a_gl_jc,
			  to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', :a_company_id)), 1)),
			  'TRANS TYPE: ' || to_char(:a_trans_type),
			  'LESSEE',
			  :a_orig,
			  case when :a_trans_type in (3041, 3045, 3040, 3046) then to_char(:a_asset_act_id) else '' end,
			  :a_pend_trans_id,
			  :a_ls_asset_id,
			  :ll_amount_type,
			  :ll_je_method_id,
			  null,
			  :a_trans_type
		from company_setup c
	  where c.company_id = :a_company_id;
	  
next

return "OK"
end function

public function boolean uf_depr_retrieve_co ();//**********************************************************************************************************************************************************
//
// uf_depr_retrieve_co()
// 
// Mimic's the bottom section of nvo_cpr_control.of_retrieve_company.  used to refresh cpr_company for processing (does not insert into pp interface or pp interface dates)
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                                false : There was an error
//
//**********************************************************************************************************************************************************

string where_cpr_company, sqls
longlong rtn

i_nvo_cpr_control.i_ds_cpr_company.Settransobject(sqlca)

where_cpr_company =  " upper(cpr_company.user_id) = '" & 
					+ trim(upper(s_user_info.user_id)) + "'" &
               + " and cpr_company.session_id = " + string(s_user_info.session_id) &
               + " and cpr_company.batch_report_id = " + string(0)

sqls = "delete from cpr_company  where " +  where_cpr_company
execute immediate :sqls;
if sqlca.sqlcode <> 0 then
	f_pp_msgs("Failed to delete from cpr_company where "+where_cpr_company)
	return false
else 
	commit;
end if


i_nvo_cpr_control.i_ds_cpr_company.insertrow(1)

i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'user_id',upper(s_user_info.user_id))
i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'session_id',s_user_info.session_id)
i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'batch_report_id',0)
i_nvo_cpr_control.i_ds_cpr_company.setitem(1,'company_id',i_nvo_cpr_control.i_company)

rtn = i_nvo_cpr_control.i_ds_cpr_company.update()

if rtn = 1 then

   commit using sqlca;

   i_nvo_cpr_control.i_ds_cpr_company.reset()
   i_nvo_cpr_control.i_ds_cpr_company.resetupdate()
   return true

else

   f_pp_msgs("Failed to insert into cpr_company.")
   rollback using sqlca;
   return false

end if

end function

public function boolean uf_depr_gl_trans_post (longlong a_row, string a_source, longlong a_je_method);//**********************************************************************************************************************************************************
//
// uf_depr_retrieve_co()
// 
// Mimic's  uo_depr_activity_post.uf_gl_transaction_post for preview journal entries.  Everything should match, other than the gl trans dw's dataobjec is the preview journal entry dw.
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                                false : There was an error
//
//**********************************************************************************************************************************************************


longlong depr_act_id, rtn, trans_id, dg_id, sob_id,company_id, amt_type, conv
string company, gl_account_str, error_msg
datetime moyr
boolean post_trf=false
decimal amount

depr_act_id = i_ds_depr_activity.GetItemNumber(a_row, "depr_activity_code_id")
dg_id = i_ds_depr_activity.getitemnumber(a_row,'depr_group_id')
sob_id = i_ds_depr_activity.getitemnumber(a_row,'set_of_books_id')
moyr = i_ds_depr_activity.getitemDateTime(a_row,'gl_post_mo_yr')
amount = i_ds_depr_activity.getitemdecimal(a_row,'amount')

// !!AJS: 12-JUN-2011: ignore salvage exp adj (submitted w/ 7636)
if depr_act_id = g_pp_constants.depr_exp_adjust &
or depr_act_id = g_pp_constants.cost_of_removal_exp_adjust &
or depr_act_id = g_pp_constants.salvage_exp_adjust then	
	f_pp_msgs("Exiting at start of uf depr gl trans post function.")
	return true
end if

select gl_company_no, dg.company_id into :company , :company_id
from company co, depr_group dg
where co.company_id = dg.company_id
and depr_group_id = :dg_id;
if sqlca.sqlcode <> 0 then
	f_pp_msgs("Error retrieving company id for dg id "+string(dg_id)+" during uf depr gl trans post function.")
	return false
end if
if IsNull(company) then 
	f_pp_msgs("Error retrieving gl_company_no for dg id "+string(dg_id)+" during uf depr gl trans post function.")
	return false
end if		

if depr_act_id = g_pp_constants.reserve_trans_in &
or depr_act_id = g_pp_constants.reserve_trans_out &
or depr_act_id = g_pp_constants.cost_of_removal_trans_in &
or depr_act_id = g_pp_constants.cost_of_removal_trans_out then
	//Maint 34663:  always perform depr reserve transfer journals in this uo object
	post_trf = true
end if

// get the amount type and convention
isnull(amt_type)
select amount_type into :amt_type
from je_method
where je_method_id = :a_je_method;

if sqlca.sqlcode <> 0 then
	f_pp_msgs("Error retrieving amount type for je method id "+string(a_je_method)+" during uf depr gl trans post function.")
	return false
end if

if IsNull(amt_type) then 
	f_pp_msgs("Error unable to retrieve amount type for je method id "+string(a_je_method)+" during uf depr gl trans post function.")
	return false
end if

isnull(conv)
select reversal_convention into :conv
from je_method_set_of_books
where je_method_id = :a_je_method
and set_of_books_id = :sob_id;

if sqlca.sqlcode <> 0 then
	f_pp_msgs("Error unable to retrieve reversal_convention for je method id "+string(a_je_method)+", set of books id "+string(sob_id)+" during uf depr gl trans post function.")
	return false
end if
if IsNull(conv) then 
	f_pp_msgs("Error reversal_convention not found for je method id "+string(a_je_method)+", set of books id "+string(sob_id)+" during uf depr gl trans post function.")
	return false
end if

f_pp_msgs("updating gl transaction previews: $" + string(amount) )

uo_ds_top ds_gl_trans
ds_gl_trans = CREATE uo_ds_top
ds_gl_trans.DataObject = "dw_gl_transaction_preview"
ds_gl_trans.SetTransObject(sqlca)

///// $$$ARE 2/11/2003 changed base to put depr_group_id: in front of description, allowing allocation of gl_transactions
///// 	by depr_group where needed (e.g. Southern) at lines 52 and 78
if Trim(i_ds_depr_activity.getitemstring(a_row,'debit_account')) <> 'Intra-Company Transfer' then
	
	ds_gl_trans.insertrow(1)
	
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
	ds_gl_trans.setitem(1,'gl_trans_id',trans_id)
	ds_gl_trans.setitem(1,'month',moyr)
	ds_gl_trans.setitem(1,'company_number',company)
	///// ###ARE 1/31/2006 sign is backwards on JEs for trf, flip * -1 to else side
	if depr_act_id = 7 or depr_act_id = 14 then
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * -1 * conv )
	else
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * conv )
	end if
	ds_gl_trans.setitem(1,'description', & 
										mid(string(dg_id) + ':' + &
										string(i_ds_depr_activity.getitemnumber(a_row,'depr_activity_id'))+":"+i_ds_depr_activity.GetItemString(a_row,"description"),1,35))
	gl_account_str = f_autogen_je_account(i_ds_depr_activity, a_row, 18, long(i_ds_depr_activity.getitemstring(a_row,'debit_account')), a_je_method )
	if Left(gl_account_str,5) = "ERROR" then
		f_pp_msgs("f_autogen_je_account() error for offset (debit): " + gl_account_str)
		destroy ds_gl_trans 
		rollback;
		return false
	end if
		
	ds_gl_trans.setitem(1,'gl_account', gl_account_str)
	ds_gl_trans.setitem(1,'debit_credit_indicator',1)
	ds_gl_trans.setitem(1,'gl_je_code',i_ds_depr_activity.getitemstring(a_row,'gl_je_code'))
	ds_gl_trans.setitem(1,'gl_status_id',1)
	ds_gl_trans.setitem(1,'source', a_source)
	ds_gl_trans.setitem(1,'je_method_id', a_je_method)
	ds_gl_trans.setitem(1,'amount_type', amt_type)
	ds_gl_trans.setitem(1,'trans_type', 18)
end if

if Trim(i_ds_depr_activity.getitemstring(a_row,'credit_account')) <> 'Intra-Company Transfer' then
	
	ds_gl_trans.insertrow(1)
	
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
	
	ds_gl_trans.setitem(1,'gl_trans_id',trans_id)
	ds_gl_trans.setitem(1,'month',moyr)
	ds_gl_trans.setitem(1,'company_number',company)
	///// ###ARE 3/6/2006 sign is backwards on JEs for trf, flip * -1 to else side
	if depr_act_id = 7 or depr_act_id = 14 then
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * -1 * conv )
	else
		ds_gl_trans.setitem(1,'amount', i_ds_depr_activity.getitemdecimal(a_row,'amount') * conv )
	end if
	ds_gl_trans.setitem(1,'description', & 
										mid(string(dg_id) + ':' + &
										string(i_ds_depr_activity.getitemnumber(a_row,'depr_activity_id'))+":"+i_ds_depr_activity.GetItemString(a_row,"description"),1,35))
	gl_account_str = f_autogen_je_account(i_ds_depr_activity, a_row, 19, long(i_ds_depr_activity.getitemstring(a_row,'credit_account')), a_je_method )
	if Left(gl_account_str,5) = "ERROR" then
		f_pp_msgs("f_autogen_je_account() error for reserve (credit): " + gl_account_str)
		destroy ds_gl_trans
		rollback;
		return false
	end if
		
	ds_gl_trans.setitem(1,'gl_account', gl_account_str)
	ds_gl_trans.setitem(1,'debit_credit_indicator',0)
	ds_gl_trans.setitem(1,'gl_je_code',i_ds_depr_activity.getitemstring(a_row,'gl_je_code'))
	ds_gl_trans.setitem(1,'gl_status_id',1)
	ds_gl_trans.setitem(1,'source',a_source)
	ds_gl_trans.setitem(1,'je_method_id', a_je_method)
	ds_gl_trans.setitem(1,'amount_type', amt_type)
	ds_gl_trans.setitem(1,'trans_type', 19)
end if

rtn = ds_gl_trans.Update()

if rtn < 0 then
	error_msg =  ds_gl_trans.i_sqlca_sqlerrtext
	rollback;   
	f_pp_msgs("Error: ds_gl_trans.Update() failed " + error_msg)
	destroy ds_gl_trans 
	return false
end if 

DESTROY ds_gl_trans

return true

end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

