HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_mortality_curve_points_ret from datawindow within w_datawindows
end type
type dw_balance_cpr_depr from datawindow within w_datawindows
end type
type dw_balance_account_summ from datawindow within w_datawindows
end type
type dw_wo_process_control from datawindow within w_datawindows
end type
type dw_clearing_wo_control from datawindow within w_datawindows
end type
type dw_gl_transaction from datawindow within w_datawindows
end type
type dw_gl_charges_afc from datawindow within w_datawindows
end type
type dw_afudc_gl_trans from datawindow within w_datawindows
end type
type dw_gl_charges_oh from datawindow within w_datawindows
end type
type dw_wo_interface_dates_all from datawindow within w_datawindows
end type
type dw_wo_process_afudc_ovh from datawindow within w_datawindows
end type
type dw_pp_interface_dates_check from datawindow within w_datawindows
end type
type dw_gl_charges_wip_comp from datawindow within w_datawindows
end type
type dw_cpr_act_month from datawindow within w_datawindows
end type
type dw_gl_charges_cpi from datawindow within w_datawindows
end type
type dw_pp_verify_close from datawindow within w_datawindows
end type
type dw_cr_element_definitions from datawindow within w_datawindows
end type
type dw_gl_charges_accruals from datawindow within w_datawindows
end type
type dw_aro_accretion_gl_adj from datawindow within w_datawindows
end type
type dw_aro_gainloss_gl from datawindow within w_datawindows
end type
type dw_gl_charges_aro from datawindow within w_datawindows
end type
type dw_aro_settlement_header from datawindow within w_datawindows
end type
type dw_aro_accretion_gl from datawindow within w_datawindows
end type
type dw_aro_accretion_gl_transition from datawindow within w_datawindows
end type
type dw_cpr_company from datawindow within w_datawindows
end type
type dw_gl_charges_aro_ext_acct from datawindow within w_datawindows
end type
type dw_aro_transition_month_end from datawindow within w_datawindows
end type
type dw_interface_dates_all from datawindow within w_datawindows
end type
type dw_balance_cpr_subledger from datawindow within w_datawindows
end type
type dw_balance_depr_subledger from datawindow within w_datawindows
end type
type dw_balance_subl_depr_basis from datawindow within w_datawindows
end type
type dw_company_set_of_books from datawindow within w_datawindows
end type
type dw_cpr_control from datawindow within w_datawindows
end type
type dw_cpr_control_bal_quantity from datawindow within w_datawindows
end type
type dw_depr_calc from datawindow within w_datawindows
end type
type dw_depr_expe_gl from datawindow within w_datawindows
end type
type dw_depr_je_method_set_of_books from datawindow within w_datawindows
end type
type dw_depr_je_method_trans_type from datawindow within w_datawindows
end type
type dw_depr_ledger_all from datawindow within w_datawindows
end type
type dw_depr_recur2gl from datawindow within w_datawindows
end type
type dw_subledger_name from datawindow within w_datawindows
end type
type dw_vint_net_salvage_update from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
string tag = "w_datawindows"
integer width = 4539
integer height = 2072
boolean titlebar = true
string title = "w_datawindows"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_mortality_curve_points_ret dw_mortality_curve_points_ret
dw_balance_cpr_depr dw_balance_cpr_depr
dw_balance_account_summ dw_balance_account_summ
dw_wo_process_control dw_wo_process_control
dw_clearing_wo_control dw_clearing_wo_control
dw_gl_transaction dw_gl_transaction
dw_gl_charges_afc dw_gl_charges_afc
dw_afudc_gl_trans dw_afudc_gl_trans
dw_gl_charges_oh dw_gl_charges_oh
dw_wo_interface_dates_all dw_wo_interface_dates_all
dw_wo_process_afudc_ovh dw_wo_process_afudc_ovh
dw_pp_interface_dates_check dw_pp_interface_dates_check
dw_gl_charges_wip_comp dw_gl_charges_wip_comp
dw_cpr_act_month dw_cpr_act_month
dw_gl_charges_cpi dw_gl_charges_cpi
dw_pp_verify_close dw_pp_verify_close
dw_cr_element_definitions dw_cr_element_definitions
dw_gl_charges_accruals dw_gl_charges_accruals
dw_aro_accretion_gl_adj dw_aro_accretion_gl_adj
dw_aro_gainloss_gl dw_aro_gainloss_gl
dw_gl_charges_aro dw_gl_charges_aro
dw_aro_settlement_header dw_aro_settlement_header
dw_aro_accretion_gl dw_aro_accretion_gl
dw_aro_accretion_gl_transition dw_aro_accretion_gl_transition
dw_cpr_company dw_cpr_company
dw_gl_charges_aro_ext_acct dw_gl_charges_aro_ext_acct
dw_aro_transition_month_end dw_aro_transition_month_end
dw_interface_dates_all dw_interface_dates_all
dw_balance_cpr_subledger dw_balance_cpr_subledger
dw_balance_depr_subledger dw_balance_depr_subledger
dw_balance_subl_depr_basis dw_balance_subl_depr_basis
dw_company_set_of_books dw_company_set_of_books
dw_cpr_control dw_cpr_control
dw_cpr_control_bal_quantity dw_cpr_control_bal_quantity
dw_depr_calc dw_depr_calc
dw_depr_expe_gl dw_depr_expe_gl
dw_depr_je_method_set_of_books dw_depr_je_method_set_of_books
dw_depr_je_method_trans_type dw_depr_je_method_trans_type
dw_depr_ledger_all dw_depr_ledger_all
dw_depr_recur2gl dw_depr_recur2gl
dw_subledger_name dw_subledger_name
dw_vint_net_salvage_update dw_vint_net_salvage_update
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_mortality_curve_points_ret=create dw_mortality_curve_points_ret
this.dw_balance_cpr_depr=create dw_balance_cpr_depr
this.dw_balance_account_summ=create dw_balance_account_summ
this.dw_wo_process_control=create dw_wo_process_control
this.dw_clearing_wo_control=create dw_clearing_wo_control
this.dw_gl_transaction=create dw_gl_transaction
this.dw_gl_charges_afc=create dw_gl_charges_afc
this.dw_afudc_gl_trans=create dw_afudc_gl_trans
this.dw_gl_charges_oh=create dw_gl_charges_oh
this.dw_wo_interface_dates_all=create dw_wo_interface_dates_all
this.dw_wo_process_afudc_ovh=create dw_wo_process_afudc_ovh
this.dw_pp_interface_dates_check=create dw_pp_interface_dates_check
this.dw_gl_charges_wip_comp=create dw_gl_charges_wip_comp
this.dw_cpr_act_month=create dw_cpr_act_month
this.dw_gl_charges_cpi=create dw_gl_charges_cpi
this.dw_pp_verify_close=create dw_pp_verify_close
this.dw_cr_element_definitions=create dw_cr_element_definitions
this.dw_gl_charges_accruals=create dw_gl_charges_accruals
this.dw_aro_accretion_gl_adj=create dw_aro_accretion_gl_adj
this.dw_aro_gainloss_gl=create dw_aro_gainloss_gl
this.dw_gl_charges_aro=create dw_gl_charges_aro
this.dw_aro_settlement_header=create dw_aro_settlement_header
this.dw_aro_accretion_gl=create dw_aro_accretion_gl
this.dw_aro_accretion_gl_transition=create dw_aro_accretion_gl_transition
this.dw_cpr_company=create dw_cpr_company
this.dw_gl_charges_aro_ext_acct=create dw_gl_charges_aro_ext_acct
this.dw_aro_transition_month_end=create dw_aro_transition_month_end
this.dw_interface_dates_all=create dw_interface_dates_all
this.dw_balance_cpr_subledger=create dw_balance_cpr_subledger
this.dw_balance_depr_subledger=create dw_balance_depr_subledger
this.dw_balance_subl_depr_basis=create dw_balance_subl_depr_basis
this.dw_company_set_of_books=create dw_company_set_of_books
this.dw_cpr_control=create dw_cpr_control
this.dw_cpr_control_bal_quantity=create dw_cpr_control_bal_quantity
this.dw_depr_calc=create dw_depr_calc
this.dw_depr_expe_gl=create dw_depr_expe_gl
this.dw_depr_je_method_set_of_books=create dw_depr_je_method_set_of_books
this.dw_depr_je_method_trans_type=create dw_depr_je_method_trans_type
this.dw_depr_ledger_all=create dw_depr_ledger_all
this.dw_depr_recur2gl=create dw_depr_recur2gl
this.dw_subledger_name=create dw_subledger_name
this.dw_vint_net_salvage_update=create dw_vint_net_salvage_update
this.Control[]={this.dw_mortality_curve_points_ret,&
this.dw_balance_cpr_depr,&
this.dw_balance_account_summ,&
this.dw_wo_process_control,&
this.dw_clearing_wo_control,&
this.dw_gl_transaction,&
this.dw_gl_charges_afc,&
this.dw_afudc_gl_trans,&
this.dw_gl_charges_oh,&
this.dw_wo_interface_dates_all,&
this.dw_wo_process_afudc_ovh,&
this.dw_pp_interface_dates_check,&
this.dw_gl_charges_wip_comp,&
this.dw_cpr_act_month,&
this.dw_gl_charges_cpi,&
this.dw_pp_verify_close,&
this.dw_cr_element_definitions,&
this.dw_gl_charges_accruals,&
this.dw_aro_accretion_gl_adj,&
this.dw_aro_gainloss_gl,&
this.dw_gl_charges_aro,&
this.dw_aro_settlement_header,&
this.dw_aro_accretion_gl,&
this.dw_aro_accretion_gl_transition,&
this.dw_cpr_company,&
this.dw_gl_charges_aro_ext_acct,&
this.dw_aro_transition_month_end,&
this.dw_interface_dates_all,&
this.dw_balance_cpr_subledger,&
this.dw_balance_depr_subledger,&
this.dw_balance_subl_depr_basis,&
this.dw_company_set_of_books,&
this.dw_cpr_control,&
this.dw_cpr_control_bal_quantity,&
this.dw_depr_calc,&
this.dw_depr_expe_gl,&
this.dw_depr_je_method_set_of_books,&
this.dw_depr_je_method_trans_type,&
this.dw_depr_ledger_all,&
this.dw_depr_recur2gl,&
this.dw_subledger_name,&
this.dw_vint_net_salvage_update}
end on

on w_datawindows.destroy
destroy(this.dw_mortality_curve_points_ret)
destroy(this.dw_balance_cpr_depr)
destroy(this.dw_balance_account_summ)
destroy(this.dw_wo_process_control)
destroy(this.dw_clearing_wo_control)
destroy(this.dw_gl_transaction)
destroy(this.dw_gl_charges_afc)
destroy(this.dw_afudc_gl_trans)
destroy(this.dw_gl_charges_oh)
destroy(this.dw_wo_interface_dates_all)
destroy(this.dw_wo_process_afudc_ovh)
destroy(this.dw_pp_interface_dates_check)
destroy(this.dw_gl_charges_wip_comp)
destroy(this.dw_cpr_act_month)
destroy(this.dw_gl_charges_cpi)
destroy(this.dw_pp_verify_close)
destroy(this.dw_cr_element_definitions)
destroy(this.dw_gl_charges_accruals)
destroy(this.dw_aro_accretion_gl_adj)
destroy(this.dw_aro_gainloss_gl)
destroy(this.dw_gl_charges_aro)
destroy(this.dw_aro_settlement_header)
destroy(this.dw_aro_accretion_gl)
destroy(this.dw_aro_accretion_gl_transition)
destroy(this.dw_cpr_company)
destroy(this.dw_gl_charges_aro_ext_acct)
destroy(this.dw_aro_transition_month_end)
destroy(this.dw_interface_dates_all)
destroy(this.dw_balance_cpr_subledger)
destroy(this.dw_balance_depr_subledger)
destroy(this.dw_balance_subl_depr_basis)
destroy(this.dw_company_set_of_books)
destroy(this.dw_cpr_control)
destroy(this.dw_cpr_control_bal_quantity)
destroy(this.dw_depr_calc)
destroy(this.dw_depr_expe_gl)
destroy(this.dw_depr_je_method_set_of_books)
destroy(this.dw_depr_je_method_trans_type)
destroy(this.dw_depr_ledger_all)
destroy(this.dw_depr_recur2gl)
destroy(this.dw_subledger_name)
destroy(this.dw_vint_net_salvage_update)
end on

type dw_mortality_curve_points_ret from datawindow within w_datawindows
integer x = 3136
integer y = 36
integer width = 997
integer height = 400
integer taborder = 50
boolean bringtotop = true
boolean titlebar = true
string title = "dw_mortality_curve_points_ret"
string dataobject = "dw_mortality_curve_points_ret"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_cpr_depr from datawindow within w_datawindows
integer x = 2107
integer y = 140
integer width = 997
integer height = 400
integer taborder = 30
boolean bringtotop = true
boolean titlebar = true
string title = "dw_balance_cpr_depr"
string dataobject = "dw_balance_cpr_depr"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_account_summ from datawindow within w_datawindows
integer x = 2107
integer y = 36
integer width = 997
integer height = 400
integer taborder = 40
boolean titlebar = true
string title = "dw_balance_account_summ"
string dataobject = "dw_balance_account_summ"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_control from datawindow within w_datawindows
integer x = 50
integer y = 36
integer width = 997
integer height = 400
integer taborder = 10
boolean titlebar = true
string title = "dw_wo_process_control"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_wo_control from datawindow within w_datawindows
integer x = 50
integer y = 140
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_clearing_wo_control"
string dataobject = "dw_clearing_wo_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_transaction from datawindow within w_datawindows
integer x = 1079
integer y = 36
integer width = 997
integer height = 400
integer taborder = 30
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_transaction"
string dataobject = "dw_gl_transaction_preview"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_afc from datawindow within w_datawindows
integer x = 50
integer y = 244
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_charges_afc"
string dataobject = "dw_gl_charges_afc"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_afudc_gl_trans from datawindow within w_datawindows
integer x = 50
integer y = 352
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_afudc_gl_trans"
string dataobject = "dw_afudc_gl_trans"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_oh from datawindow within w_datawindows
integer x = 50
integer y = 460
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_charges_oh"
string dataobject = "dw_gl_charges_oh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_interface_dates_all from datawindow within w_datawindows
integer x = 50
integer y = 572
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_wo_interface_dates_all"
string dataobject = "dw_wo_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_afudc_ovh from datawindow within w_datawindows
integer x = 50
integer y = 676
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_wo_process_afudc_ovh"
string dataobject = "dw_wo_process_afudc_ovh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_interface_dates_check from datawindow within w_datawindows
integer x = 50
integer y = 784
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_pp_interface_dates_check"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_wip_comp from datawindow within w_datawindows
integer x = 50
integer y = 892
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_charges_wip_comp"
string dataobject = "dw_gl_charges_wip_comp"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_act_month from datawindow within w_datawindows
integer x = 50
integer y = 1004
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_cpr_act_month"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_cpi from datawindow within w_datawindows
integer x = 50
integer y = 1108
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_charges_cpi"
string dataobject = "dw_gl_charges_cpi"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_verify_close from datawindow within w_datawindows
integer x = 50
integer y = 1216
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_pp_verify_close"
string dataobject = "dw_pp_verify_close"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cr_element_definitions from datawindow within w_datawindows
integer x = 50
integer y = 1324
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_cr_element_definitions"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_accruals from datawindow within w_datawindows
integer x = 1079
integer y = 140
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_charges_accruals"
string dataobject = "dw_gl_charges_accruals"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_aro_accretion_gl_adj from datawindow within w_datawindows
integer x = 1079
integer y = 244
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_aro_accretion_gl_adj"
string dataobject = "dw_aro_accretion_gl_adj"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_aro_gainloss_gl from datawindow within w_datawindows
integer x = 1079
integer y = 348
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_aro_gainloss_gl"
string dataobject = "dw_aro_gainloss_gl"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_aro from datawindow within w_datawindows
integer x = 1079
integer y = 452
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_charges_aro"
string dataobject = "dw_gl_charges_aro"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_aro_settlement_header from datawindow within w_datawindows
integer x = 1079
integer y = 556
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_aro_settlement_header"
string dataobject = "dw_aro_settlement_header"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_aro_accretion_gl from datawindow within w_datawindows
integer x = 1079
integer y = 660
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_aro_accretion_gl"
string dataobject = "dw_aro_accretion_gl"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_aro_accretion_gl_transition from datawindow within w_datawindows
integer x = 1079
integer y = 764
integer width = 997
integer height = 400
integer taborder = 10
boolean bringtotop = true
boolean titlebar = true
string title = "dw_aro_accretion_gl_transition"
string dataobject = "dw_aro_accretion_gl_transition"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_company from datawindow within w_datawindows
integer x = 1079
integer y = 868
integer width = 997
integer height = 400
integer taborder = 40
boolean bringtotop = true
boolean titlebar = true
string title = "dw_cpr_company"
string dataobject = "dw_cpr_company"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_aro_ext_acct from datawindow within w_datawindows
integer x = 1079
integer y = 972
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_gl_charges_aro_ext_acct"
string dataobject = "dw_gl_charges_aro_ext_acct"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_aro_transition_month_end from datawindow within w_datawindows
integer x = 1079
integer y = 1076
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_aro_transition_month_end"
string dataobject = "dw_aro_transition_month_end"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_interface_dates_all from datawindow within w_datawindows
integer x = 1079
integer y = 1180
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_interface_dates_all"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_cpr_subledger from datawindow within w_datawindows
integer x = 2107
integer y = 244
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_balance_cpr_subledger"
string dataobject = "dw_balance_cpr_subledger"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_depr_subledger from datawindow within w_datawindows
integer x = 2107
integer y = 348
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_balance_depr_subledger"
string dataobject = "dw_balance_depr_subledger"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_subl_depr_basis from datawindow within w_datawindows
integer x = 2107
integer y = 452
integer width = 997
integer height = 400
integer taborder = 30
boolean bringtotop = true
boolean titlebar = true
string title = "dw_balance_subl_depr_basis"
string dataobject = "dw_balance_subl_depr_basis"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_company_set_of_books from datawindow within w_datawindows
integer x = 2107
integer y = 556
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_company_set_of_books"
string dataobject = "dw_company_set_of_books"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_control from datawindow within w_datawindows
integer x = 2107
integer y = 660
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_cpr_control"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_control_bal_quantity from datawindow within w_datawindows
integer x = 2107
integer y = 764
integer width = 997
integer height = 400
integer taborder = 30
boolean bringtotop = true
boolean titlebar = true
string title = "dw_cpr_control_bal_quantity"
string dataobject = "dw_cpr_control_bal_quantity"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_calc from datawindow within w_datawindows
integer x = 2107
integer y = 868
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_depr_calc"
string dataobject = "dw_depr_calc"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_expe_gl from datawindow within w_datawindows
integer x = 1079
integer y = 1284
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_depr_expe_gl"
string dataobject = "dw_depr_expe_gl"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_je_method_set_of_books from datawindow within w_datawindows
integer x = 2107
integer y = 972
integer width = 997
integer height = 400
integer taborder = 30
boolean bringtotop = true
boolean titlebar = true
string title = "dw_depr_je_method_set_of_books"
string dataobject = "dw_depr_je_method_set_of_books"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_je_method_trans_type from datawindow within w_datawindows
integer x = 2107
integer y = 1076
integer width = 997
integer height = 400
integer taborder = 20
boolean bringtotop = true
boolean titlebar = true
string title = "dw_depr_je_method_trans_type"
string dataobject = "dw_depr_je_method_trans_type"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_ledger_all from datawindow within w_datawindows
integer x = 2107
integer y = 1176
integer width = 997
integer height = 400
integer taborder = 30
boolean bringtotop = true
boolean titlebar = true
string title = "dw_depr_ledger_all"
string dataobject = "dw_depr_ledger_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_recur2gl from datawindow within w_datawindows
integer x = 2107
integer y = 1280
integer width = 997
integer height = 400
integer taborder = 30
boolean bringtotop = true
boolean titlebar = true
string title = "dw_depr_recur2gl"
string dataobject = "dw_depr_recur2gl"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_subledger_name from datawindow within w_datawindows
integer x = 3136
integer y = 144
integer width = 997
integer height = 400
integer taborder = 60
boolean bringtotop = true
boolean titlebar = true
string title = "dw_subledger_name"
string dataobject = "dw_subledger_name"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_vint_net_salvage_update from datawindow within w_datawindows
integer x = 3136
integer y = 248
integer width = 997
integer height = 400
integer taborder = 70
boolean bringtotop = true
boolean titlebar = true
string title = "dw_vint_net_salvage_update"
string dataobject = "dw_vint_net_salvage_update"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

