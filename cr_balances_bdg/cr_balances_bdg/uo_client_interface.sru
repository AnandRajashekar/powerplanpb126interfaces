HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string	i_exe_name = 'cr_balances_bdg.exe'

string	i_amount_types, i_co_field
longlong	i_num_elements
string	i_actual_field[], i_budget_field[], i_element_table_arr[], i_element_column_arr[]
longlong i_values_by_company[]
string i_element_string, i_join_on_ack

uo_cr_where_from_ds i_uo_cr_where_from_ds
end variables

forward prototypes
public function longlong uf_read ()
public subroutine uf_3_build_month (longlong a_prior_month, longlong a_month, string a_bv)
public subroutine uf_1_summary_bv (longlong a_actual_month, string a_bv)
public subroutine uf_2_copy_act_to_bdg (longlong a_actual_month, string a_bv)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();string bv_list, bv_array[], bv
longlong actuals_month, num_months, i, j, min_mn, max_mn, prior_month, mn
string sqls
uo_ds_top ds_months


//*****************************************************************************************
//   Setup
//*****************************************************************************************
// 	ACK
uo_ds_top	ds_elements
ds_elements = CREATE uo_ds_top
sqls = 'select description actual_field, nvl(budgeting_element, description) budget_field, element_table, element_column, nvl(values_by_company,0) values_by_company ' + &
	' from cr_elements where include_cr_balances = 1 order by "ORDER" '
f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)
i_num_elements = ds_elements.RowCount()
for i = 1 to i_num_elements
	i_actual_field[i] = '"' + f_cr_clean_string(upper(trim(ds_elements.GetItemString(i,'actual_field')))) + '"'
	i_budget_field[i] = '"' + f_cr_clean_string(upper(trim(ds_elements.GetItemString(i,'budget_field')))) + '"'
	i_element_table_arr[i] = '"' + f_cr_clean_string(upper(trim(ds_elements.GetItemString(i,'element_table')))) + '"'
	i_element_column_arr[i] = '"' + f_cr_clean_string(upper(trim(ds_elements.GetItemString(i,'element_column')))) + '"'
	i_values_by_company[i] = ds_elements.GetItemNumber(i,'values_by_company')
next 

//  Create the Accounting Key string ...
i_element_string = ' '
i_join_on_ack = ' '
for i = 1 to i_num_elements
	i_element_string += i_budget_field[i] + ', '
	i_join_on_ack += "and a." + i_budget_field[i] + " = b." + i_budget_field[i]
next
i_element_string = left(i_element_string, len(i_element_string) - 2)

//   Amount Type System Control
i_amount_types = f_cr_system_control('CR BALANCES: AMOUNT TYPES')
if isnull(i_amount_types) or i_amount_types = '' then i_amount_types = '1,3'

//	Company System Control
i_co_field = f_cr_system_control('COMPANY FIELD')
if isnull(i_co_field) or i_co_field = '' then
	g_rte.msg('Company field not defined in CR System Control')
	throw g_rte
end if

//   Budget Versions to Process
select control_value into :bv_list
from cr_alloc_system_control
where upper(trim(control_name)) = 'CR BALANCES BDG - BUDGET VERSION(S)';
if isnull(bv_list) or trim(bv_list) = '' then
	g_rte.msg('Budget Versions to be build budget balances is not populated in CR Alloc System Control')
	throw g_rte	
end if
f_parsestringintostringarray(bv_list,',',bv_array)

//	Months datastore
ds_months = create uo_ds_top
sqls = "select distinct month_number from cr_budget_data where -1 = 0"
f_create_dynamic_ds(ds_months,'grid',sqls,sqlca,true)

//*****************************************************************************************
//
//  Upfront validations...
//
//*****************************************************************************************
// CR Balances is required.
select month_number
	into :actuals_month
	from cr_balances
	where rownum = 1;
if g_rte.check_sql('Select from cr_balances for actuals month') then throw g_rte

if isnull(actuals_month) or actuals_month = 0 then
	// No data in cr_balances.  Throw error.
	g_rte.msg(-1,"No data found in CR_BALANCES")
	throw g_rte
end if

//************************************************************************
// Clearing old data
//  Will just truncate .. any budget versions no longer needing to be rebuilt should just be cleared.  
//************************************************************************
f_pp_msgs("Deleting old balance information")
sqlca.truncate_table('cr_balances_bdg')
if g_rte.check_sql('Truncating cr_balances_bdg') then throw g_rte
	
//*****************************************************************************************
//
//  Look over the array of budget versions and process one at a time
//
//*****************************************************************************************
for i = 1 to upperbound(bv_array)
	// What budget version am I processing.
	bv = bv_array[i]
	f_pp_msgs(" ")
	f_pp_msgs("Processing budget version: " + bv)
	
	
	//************************************************************************
	// What months am I processing
	//************************************************************************	
	// What months do we have budget data for?
	f_pp_msgs(" --- Determining months to process")
	sqls = "select distinct month_number from cr_budget_data where budget_version = '" + bv + "' order by month_number"
	ds_months.SetSQLSelect(sqls)
	num_months = ds_months.retrieve()
	if g_rte.check_sql('Select months range from cr_budget_data',ds_months) then throw g_rte

	if num_months = 0 then
		f_pp_msgs("No rows found for the budget version in cr_budget_data.  Continuing to the next budget version.")
		continue
	else
		min_mn = ds_months.GetItemNumber(1,1)
		max_mn = ds_months.GetItemNumber(num_months,1)
	end if
	
	//	Have I updated with actuals on this BV?
	select actuals_month into :actuals_month 
		from cr_budget_version where budget_version = :bv;		
	if g_rte.check_sql('Select actuals month from cr_budget_version') then throw g_rte
	
	if isnull(actuals_month) or actuals_month = 0 then
		// Haven't updated with actuals.  Will pull actuals for the month prior to min_mn and then increment from there.
		select max(month_number)
			into :actuals_month
			from cr_balances
			where month_number < :min_mn;
		if g_rte.check_sql('Select from cr_balances for actuals month') then throw g_rte
		
		if isnull(actuals_month) or actuals_month = 0 then
			// No data in cr_balances.  Throw error.
			g_rte.msg(-1,"No data found in CR_BALANCES",'Prior to month ' + string(min_mn))
			throw g_rte
		end if
	end if
	
	// At this point have an actuals month and months to process.
	f_pp_msgs(" --- Actuals Month: " + string(actuals_month))
	f_pp_msgs(" --- Budget Months: " + string(min_mn) + ' - ' + string(max_mn))
	
	//************************************************************************
	//  Summarize the budget version to minimize the reads from cr_budget_data
	//************************************************************************
	uf_1_summary_bv(actuals_month, bv)
	
	//************************************************************************
	//  Roll actuals for actuals_month from cr_balances to cr_balances_bdg and convert to budget ACK
	//************************************************************************
	uf_2_copy_act_to_bdg(actuals_month, bv)
	
	//************************************************************************
	//  Process months incrementally
	//************************************************************************
	for j = 1 to num_months
		if j = 1 then
			prior_month = actuals_month
		else
			prior_month = mn
		end if
		
		mn = ds_months.GetItemNumber(j,1)
		uf_3_build_month(prior_month, mn, bv)		
		
		// Going to commit after each month.  Not sure we can do a full budget version between commits... seems like too much data.
		commit;
	next 
	
	// Cleanup the staging table before we shut down.  This will also commit the data.
	sqlca.truncate_table('cr_balances_bdg_stg')
	if g_rte.check_sql('Truncating staging table') then throw g_rte

next 

return 1
end function

public subroutine uf_3_build_month (longlong a_prior_month, longlong a_month, string a_bv);//*********************************************************************************************
//
//  Step 3  :  Building cr_balances_bdg for current month
//
//  NOTES   :  Steps are: 	1)  Clear current month from cr_balances_bdg.  
//									2)  Roll forward previous month from cr_balances_bdg.
//									3)  Insert missing combinations for current month.
//									4)  Updating balances from cr_cost_repository.
//
//*********************************************************************************************
string sqls, element, where_sqls, left_paren, col_name, operator, &
	value1, value2, between_and, right_paren, and_or, structure_where, update_sqls
longlong num_rows, i, include, structure_id, not_field, first_month
uo_ds_top ds_where_clause,  ds_months, ds_income_accounts

f_pp_msgs(" --- Building cr_balances_bdg for month " + string(a_month))

// Full delete already done in uf_read.  Don't have to do it here.
////*****************************************************************
////  1:  Clearing current month
////*****************************************************************
//f_pp_msgs(" ------ Clearing cr_balances_bdg")
//delete from cr_balances_bdg where month_number = :a_month and budget_version = :a_bv;
//if g_rte.check_sql("Clearing current month from cr_balances_bdg") then throw g_rte

//*****************************************************************
//  2:  Rolling previous month forward
//*****************************************************************
f_pp_msgs(" ------ Rolling forward balances from previous month")
sqls = 'insert into cr_balances_bdg ("BUDGET_VERSION", "MONTH_NUMBER", "AMOUNT_TYPE", "QUANTITY", "AMOUNT", ' + &
	i_element_string + ') select "BUDGET_VERSION", ' + string(a_month) + ', "AMOUNT_TYPE", "QUANTITY", "AMOUNT", ' + &
	i_element_string + ' from cr_balances_bdg where month_number = ' + string(a_prior_month) + " and budget_version = '" + a_bv + "'"
execute immediate :sqls;
if g_rte.check_sql("Rolling forward previous month in cr_balances_bdg") then throw g_rte

//*****************************************************************
//  3:  Insert missing combinations
//*****************************************************************
f_pp_msgs(" ------ Inserting new accounting key combinations")
sqls = 'insert into cr_balances_bdg ("BUDGET_VERSION", "MONTH_NUMBER", "AMOUNT_TYPE", "QUANTITY", "AMOUNT", ' + &
	i_element_string + ') select "BUDGET_VERSION", ' + string(a_month) + ', "AMOUNT_TYPE", 0, 0, ' + i_element_string
sqls += " from cr_balances_bdg_stg a where budget_version = '" + a_bv + "' and amount_type in (" + i_amount_types + ") and month_number = " + string(a_month)
sqls += " and not exists (select 1 from cr_balances_bdg b where b.budget_version = '" + a_bv + "' and a.amount_type = b.amount_type "
sqls += i_join_on_ack + ")"
execute immediate :sqls;
if g_rte.check_sql("Inserting new accounting key combinations") then throw g_rte
	
//*****************************************************************
//  4:  Checking for Balances on Income Accounts
//*****************************************************************
select upper(trim(control_value)) 
	into :first_month
	from cr_alloc_system_control 
	where upper(trim(control_name)) = upper(trim('Initial Month of Fiscal Year'));

// If it's 1 then zero out balances in period 0. 
if isnull(first_month) or first_month <= 1 or first_month > 12 then first_month = 0
first_month = long(mid(string(a_month),1,4)) * 100 + first_month

if first_month <= a_month and first_month > a_prior_month then	
	f_pp_msgs(" ------ Performing year-end rollforward")
	
	
	sqls = "select * from cr_balances_bdg_ye_close order by row_id"
	ds_where_clause = create uo_ds_top
	f_create_dynamic_ds(ds_where_clause, "grid", sqls, sqlca, true)
	num_rows = ds_where_clause.RowCount()
	if g_rte.check_sql_no_rows('No Income Account criteria is in the system',ds_where_clause) then throw g_rte
	
	if not isvalid(i_uo_cr_where_from_ds) then i_uo_cr_where_from_ds = create uo_cr_where_from_ds
	where_sqls = i_uo_cr_where_from_ds.uf_build_sql_where(ds_where_clause)
	if mid(where_sqls,1,5) = 'ERROR' then 
		g_rte.msg('Unable to build where clause from cr_balances_bdg_ye_close',where_sqls)
		throw g_rte
	end if
	
	where_sqls = ' month_number = ' + string(a_month) + " and budget_version = '" + a_bv + "' and (" + where_sqls + ")"
	
	update_sqls = 'update cr_balances_bdg set amount = 0, quantity = 0 where ' + where_sqls
	
	execute immediate :update_sqls;
	if g_rte.check_sql("Performing year-end rollforward") then throw g_rte
end if

//*****************************************************************
//  5:  Updating balances
//*****************************************************************
f_pp_msgs(" ------ Updating quantities and amounts")
//sqlca.analyze_table('cr_balances_bdg');
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("WARNING: the analyze_table function failed on cr_balances_bdg.")
//	f_pp_msgs("The process WILL continue.")
//	f_pp_msgs("  ")
//end if

sqls = "update cr_balances_bdg a set amount = amount + "
sqls += 	"(select amount from cr_balances_bdg_stg b "
sqls +=	"where budget_version = '" + a_bv + "' "
sqls +=	"and a.amount_type = b.amount_type "
sqls +=	"and b.month_number = " + string(a_month)
sqls += 	i_join_on_ack + "), "
sqls += "quantity = quantity + "
sqls += 	"(select quantity from cr_balances_bdg_stg b "
sqls +=	"where budget_version = '" + a_bv + "' "
sqls +=	"and a.amount_type = b.amount_type "
sqls +=	"and b.month_number = " + string(a_month)
sqls += 	i_join_on_ack + ") "
sqls += "where a.month_number = " + string(a_month)
sqls += "and a.budget_version = '" + a_bv + "' "
sqls += "and exists " 
sqls += 	"(select 1 from cr_balances_bdg_stg b "
sqls +=	"where budget_version = '" + a_bv + "' "
sqls +=	"and a.amount_type = b.amount_type "
sqls +=	"and b.month_number = " + string(a_month)
sqls += 	i_join_on_ack + ")"

execute immediate :sqls;
if g_rte.check_sql("Updating amounts/quantities in cr_balances_bdg") then throw g_rte

//*****************************************************************
//  6:  Clearing zero balances entries
//*****************************************************************
f_pp_msgs(" ------ Clearing zero balances entries")

delete from cr_balances_bdg where quantity = 0 and amount = 0 and month_number = :a_month and budget_version = :a_bv;
if g_rte.check_sql("Clearing zero balances entries") then throw g_rte
end subroutine

public subroutine uf_1_summary_bv (longlong a_actual_month, string a_bv);string sqls
longlong i

//*****************************************************************
//  Summarize cr_budget_data to a staging table at the appropriate level of detail
//*****************************************************************
f_pp_msgs(" --- Summarizing budget version")
sqlca.truncate_table('cr_balances_bdg_stg')
if g_rte.check_sql('Truncating staging table') then throw g_rte

sqls = 'insert into cr_balances_bdg_stg ("BUDGET_VERSION", "MONTH_NUMBER", "AMOUNT_TYPE", "QUANTITY", "AMOUNT", ' + &
	i_element_string + ') select  "BUDGET_VERSION", month_number, "AMOUNT_TYPE", sum(quantity) quantity, sum(amount), ' + i_element_string
sqls += " from cr_budget_data a where budget_version = '" + a_bv + "' and amount_type in (" + i_amount_types + ") and month_number > " + string(a_actual_month)
sqls += " group by budget_version, month_number, amount_type, " + i_element_string
execute immediate :sqls;
if g_rte.check_sql("Inserting summarized budget version into cr_balances_bdg") then throw g_rte

return
end subroutine

public subroutine uf_2_copy_act_to_bdg (longlong a_actual_month, string a_bv);string sqls
longlong i

// Insert into
f_pp_msgs(" --- Initializing balances from actuals")
sqls = 'insert into cr_balances_bdg ('
for i = 1 to i_num_elements
	sqls += i_budget_field[i] + ', '
next
sqls += ' month_number, amount_type, budget_version, quantity, amount) '

// Select
sqls += 'select '
for i = 1 to i_num_elements
	sqls += 'nvl(me_' + string(i) + '.budgeting_value, bal.' + i_actual_field[i] + ') ' + i_budget_field[i] + ', '
next
sqls += " month_number, amount_type, '" + a_bv + "' budget_version, sum(quantity) quantity, sum(amount) amount "

// From
sqls += " from cr_balances bal, "
for i = 1 to i_num_elements
	sqls += i_element_table_arr[i] + ' me_' + string(i) + ', '
next
sqls = mid(sqls,1,len(sqls) - 2)

// Where
sqls += " where bal.month_number = " + string(a_actual_month)
for i = 1 to i_num_elements
	sqls += " and bal." + i_actual_field[i] + " = me_" + string(i) + '.' + i_element_column_arr[i] + " (+)"
	sqls += " and 'Actuals' = me_" + string(i) + ".element_type (+)"
	if i_values_by_company[i] = 1 then sqls += " and bal." + i_co_field + " = me_" + string(i) + '.' + i_co_field + " (+)"
next

// Group
sqls += " group by " 
for i = 1 to i_num_elements
	sqls += 'nvl(me_' + string(i) + '.budgeting_value, bal.' + i_actual_field[i] + '), '
next
sqls += " month_number, amount_type "

// Having
sqls += " having sum(amount) <> 0 or sum(quantity) <> 0 " 

// Execute
execute immediate :sqls;
if g_rte.check_sql('Initializing cr_balances_bdg for budget') then throw g_rte
end subroutine

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_balances_bdg_custom_version nvo_cr_balances_bdg_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_balances_bdg_custom.pbd'
		return nvo_cr_balances_bdg_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

