HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_auto_unitization.exe'

nvo_wo_control i_nvo_wo_control
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function boolean uf__main_auto_unitization ()
public function integer uf_check_for_unapproved_charges (longlong a_company_id, longlong a_month_number)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Process Close Charge Collection logic
if not uf__main_auto_unitization() then
	// log the list of companies that failed to perform auto unitization
	i_nvo_wo_control.of_log_failed_companies("AUTO UNITIZE")
	return rtn_failure
end if

return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function boolean uf__main_auto_unitization ();//**********************************************************************************************************************************************************
//
// uf__main_auto_unitization()
// 
// The main workflow for the Interface.  Calls all the necessary functions Auto Unitize
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************


datetime run_date, afudc_calc_date, afudc_apprv_date, accrual_calc_date, accrual_apprv_date
integer  rtn
string   yes_no, gl_je_code, month_string, savedir, late_opt, rtn_str, late_months_str, yes_no_late, comp_descr, fast_101_cv, regular_opt
longlong run, errors, late_months, month_number, new_date,mn,  check_count, rtn2
boolean isFirst
string late_charge_mos_option


string prior_wos_to_run, prior_months_to_run, prior_late_charge_options, late_charge_options, late_charge_mos, wos_to_run

////	added for company loop
longlong	c, num_companies, counter, process_id, pp_stat_id, i
uo_ds_top ds_users
boolean vfy_users
string  sqls,  user_id, find_str

longlong ndx
string logTemp

// if any company in the loop has an Auto Unitization error,
// it continues on to the next company but returns a failure
// status to uf_read
boolean b_all_success
b_all_success = true

i_nvo_wo_control.of_constructor()

// initialize the late charge options since they get used below in comparisons
late_charge_options = "-72608"
late_charge_mos = "-72608"
wos_to_run = "-72608"

// Parameters passed from PowerPlan and Job Server:
// g_ssp_parms.long_arg = Company IDs
// g_ssp_parms.date_arg[1] = Unitization Month 
// g_ssp_parms.string_arg[1] = Work Orders to Unitize
// g_ssp_parms.string_arg2[1] = Late Unitization Options
// g_ssp_parms.long_arg2[1] = Previous Months to Process  (default to 1 if not present and no month end option found)

// these parameters originate from the w_wo_control window
i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_wo_control.i_month = datetime(g_ssp_parms.date_arg[1])

// Work Orders to Unitize
if (upperbound(g_ssp_parms.string_arg) = 1) then
	i_nvo_wo_control.i_wos_to_run = g_ssp_parms.string_arg[1]
else
	setnull(i_nvo_wo_control.i_wos_to_run)
end if

// Late Unitization Options
if (upperbound(g_ssp_parms.string_arg2) = 1) then
	i_nvo_wo_control.i_late_charge_options = g_ssp_parms.string_arg2[1]
else
	setnull(i_nvo_wo_control.i_late_charge_options)
end if

// Previous Months to Process
if (upperbound(g_ssp_parms.long_arg2) = 1) then
	i_nvo_wo_control.i_late_charge_mos = g_ssp_parms.long_arg2[1]
else
	setnull(i_nvo_wo_control.i_late_charge_mos)
end if
		
// Print parameters to the log
f_pp_msgs("WO Auto Unitization")
f_pp_msgs("******************  Begin Parameter List  ******************")

for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	f_pp_msgs("** Company ID: " + string(i_nvo_wo_control.i_company_idx[ndx]))
next

f_pp_msgs("** Month: " + string(i_nvo_wo_control.i_month, "mm/dd/yyyy"))

logTemp = string(i_nvo_wo_control.i_wos_to_run)
if isnull(logTemp) then logTemp = "NULL"
f_pp_msgs("** Work Orders to Unitize: " + logTemp)

logTemp = string(i_nvo_wo_control.i_late_charge_options)
if isnull(logTemp) then logTemp = "NULL"
f_pp_msgs("** Late Unitization Options: " + logTemp)

logTemp = string(i_nvo_wo_control.i_late_charge_mos)
if isnull(logTemp) then logTemp = "NULL"
f_pp_msgs("** Previous Months to Process: " + logTemp)


// done printing parameters to the log

// populate the company descs
rtn2 = i_nvo_wo_control.of_getdescriptionsfromids( i_nvo_wo_control.i_company_idx )
if rtn2 <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// had to get the datawindow to retrieve
i_nvo_wo_control.of_companychanged(1, i_nvo_wo_control.i_month)

//maint-44823:  afudc and overhead calc validations are inside uf_check_for_unapproved_charges()

// check if the multiple companies chosen can be processed  together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if 

//  Check if Auto 101 is turned off for their system ...
yes_no = "NO"

yes_no =  upper(f_pp_system_control_company('AUTOMATIC UNITIZATION IS TURNED ON', -1))

if  yes_no = '' then yes_no = "NO"

if yes_no = "YES" then
	//  The gl_je_code must be defined in gl_je_control as "AUTOMATIC 101" ...
	setnull(gl_je_code)
	select gl_je_code into :gl_je_code
 	 from standard_journal_entries, gl_je_control
 	where standard_journal_entries.je_id = gl_je_control.je_id and
		 upper(ltrim(rtrim(process_id))) = 'AUTOMATIC 101';

	if gl_je_code = "" or isnull(gl_je_code) then
		f_pp_msgs( " There is no gl_je_code defined in gl_je_control for Automatic 101. " + &
			"Cannot run Automatic Unitization.")
		return false
	end if
end if

savedir = f_get_temp_dir()

if savedir = "" then savedir = 'c:'
i_nvo_wo_control.i_filename_101 = savedir + "\unit_101.txt"
filedelete(i_nvo_wo_control.i_filename_101)

i_nvo_wo_control.i_filename_101_grp = savedir + "\unit_101_grp_error.txt"
filedelete(i_nvo_wo_control.i_filename_101_grp)

isFirst = true
num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c=1 to num_companies
	//	it's selected, update the rest of the window
	i_nvo_wo_control.of_companychanged(c, i_nvo_wo_control.i_month)
	
	//	If 'AUTO101 - FAST NO LOOP' is not set to 'YES' then Auto 101 CANNOT be automated since the old code
	// was not ported to the SSP.
	fast_101_cv = upper(f_pp_system_control_company('AUTO101 - FAST NO LOOP', i_nvo_wo_control.i_company))
	
	if fast_101_cv <> 'YES' then
		f_pp_msgs("ERROR: 'AUTO101 - FAST NO LOOP' is not set to 'YES'.  This process cannot be run outside of the main application.")
		return false
	end if
	
	// Make sure the month isn't closed
	if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		return false
	end if
	
	yes_no_late = upper(f_pp_system_control_company('AUTO101 - LATE CHARGE UNITIZATION', i_nvo_wo_control.i_company))
	
	
	if isnull(yes_no_late) then
		yes_no_late = upper(f_pp_system_control_company('AUTO101 - LATE CHARGE UNITIZATION', -1))
	end if

	
	// ensure month end options are the same for all companies
	// only retrieve the value from a Month End Option if the value was not passed as a parameter
	// Work Orders to Unitize
	if isnull(i_nvo_wo_control.i_wos_to_run) then
		try
			wos_to_run  = f_pp_month_end_options(i_nvo_wo_control.i_company, g_process_id, 1)
		catch (exception e1)
			f_pp_msgs("ERROR: Could not find month end option value for 'Work Orders to Unitize' in pp_month_end_options.")
			f_pp_msgs(e1.text)
			return false
		end try
	end if

	// only retrieve the value from a Month End Option if the value was not passed as a parameter
	// Late Unitization Options
	if isnull(i_nvo_wo_control.i_late_charge_options) then
		try
			late_charge_options = f_pp_month_end_options(i_nvo_wo_control.i_company, g_process_id, 2)
		catch (exception e2)
			f_pp_msgs("ERROR: Could not find month end option value for 'Late Unitization Options' in pp_month_end_options.")
			f_pp_msgs(e2.text)
			return false
		end try
	end if	
	
	// look for Previous Months to Process (i_late_charge_mos) in a parameter first and secondly in the pp_month_end_options table
	// if not found in either place default it to 1
	if isnull(i_nvo_wo_control.i_late_charge_mos) then
		try
			late_charge_mos = f_pp_month_end_options(i_nvo_wo_control.i_company, g_process_id, 3)
		catch (exception e3)
			f_pp_msgs("Could not find month end option value for 'Previous Months to Process' in pp_month_end_options.  Defaulting to '1'.")
			f_pp_msgs(e3.text)
			late_charge_mos = "1"
		end try
	end if
	
//	prior_wos_to_run, prior_months_to_run, prior_late_charge_options
	if isFirst = false and yes_no_late <> late_opt then
		f_pp_msgs("Cannot process multiple companies because 'AUTO101 - LATE CHARGE UNITIZATION'" + &
		" system control has different values")
		return false
	end if
	if isFirst = false and prior_wos_to_run <> wos_to_run then
		f_pp_msgs("Cannot process multiple companies because month end option 'Work Orders to Unitize' are different for each company")
		return false
	end if
	if isFirst = false and prior_months_to_run <> late_charge_mos then
		f_pp_msgs("Cannot process multiple companies because month end option 'Previous Months to Process' are different for each company")
		return false
	end if
	if isFirst = false and prior_late_charge_options <> late_charge_options then
		f_pp_msgs("Cannot process multiple companies because month end option 'Late Unitization Options' are different for each company")
		return false
	end if
	
	isFirst = false
	late_opt = yes_no_late
	prior_wos_to_run = wos_to_run
	prior_months_to_run = late_charge_mos
	prior_late_charge_options = late_charge_options
	
	   ////Need to verify calc vs approval dates to prevent unapproved charges from closing

	rtn = uf_check_for_unapproved_charges(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number)
	if rtn < 0 then
		f_pp_msgs( "ERROR: Processing Automatic 101 Closings: Unapproved calculated charges found! "+ ' ' + string(now()))
		return false
	end if
next

// made it out of the loop.  All options are good.  Set the nvo variables
if isnull(i_nvo_wo_control.i_wos_to_run) then
	i_nvo_wo_control.i_wos_to_run = prior_wos_to_run
	f_pp_msgs("** Work Orders to Unitize: " + i_nvo_wo_control.i_wos_to_run)
end if
if isnull(i_nvo_wo_control.i_late_charge_options) then
	i_nvo_wo_control.i_late_charge_options = prior_late_charge_options
	f_pp_msgs("** Late Unitization Options: " + i_nvo_wo_control.i_late_charge_options)
end if
if isnull(i_nvo_wo_control.i_late_charge_mos) then
	i_nvo_wo_control.i_late_charge_mos = long(prior_months_to_run)
	f_pp_msgs("** Previous Months to Process: " + string(i_nvo_wo_control.i_late_charge_mos))
end if

i_nvo_wo_control.i_late_opt = late_opt

// Set the values related to Late Charge Options that would have come from the w_wo_unitization_options window
// based on the i_nvo_wo_control.i_late_charge_options passed in as a parameter
if i_nvo_wo_control.i_late_charge_options = 'Unitization and Late Charge Unitization' or i_nvo_wo_control.i_late_charge_options = 'Late Charge Unitization Only' then
	
	if i_nvo_wo_control.i_late_charge_mos <= 0  then
		f_pp_msgs("Previous Months to Process must be greater than 0")
		return false
	end if
	
	i_nvo_wo_control.i_late_opt = 'YES'
	 
	if i_nvo_wo_control.i_late_charge_options = 'Unitization and Late Charge Unitization' then 
		i_nvo_wo_control.i_late_only = false
	else
		i_nvo_wo_control.i_late_only = true
	end if
	
else
	
	 i_nvo_wo_control.i_late_opt = 'NO'
 
end if

// only use the late_opt that gets passed in as a parameter
late_opt = i_nvo_wo_control.i_late_opt

 if upper(late_opt) = 'YES' then
	//late_months_str =  f_get_string("Late Charge Months ( 1 = Current Month)", 3, "A", "1") 
	if isnull(i_nvo_wo_control.i_late_charge_mos) then
		setnull(late_months_str)
	else
		late_months_str = string(i_nvo_wo_control.i_late_charge_mos)
	end if
	// will be null if the user hits cancel
	if isNull(late_months_str) then
		late_months = 0
		f_pp_msgs("Processing stopped")
		return false
	end if
		
	late_months  = long(late_months_str)
	
	if late_months < 0 then
		f_pp_msgs("Invalid number of months entered")
		return false
	end if
		
	month_number = i_nvo_wo_control.i_month_number
	
	if late_months > 0 then
 
		select to_number(to_char(  add_months(to_date(:month_number, 'yyyymm'), - :late_months) ,'yyyymm'))
		into :new_date 
		from dual;
	
		i_nvo_wo_control.i_late_month_number = new_date 
 
  	else
  	 	i_nvo_wo_control.i_late_month_number = 0 // do not do it
  	end if
	
end if

////Setup the PB uo object for new faster unitization
uo_wo_unitization uo_fast101
uo_fast101 = CREATE uo_wo_unitization
// set instance variables that used to be accessed directly from the w_wo_control window
uo_fast101.of_setInstanceVariables(false, true, i_nvo_wo_control.i_late_charge_mos, i_nvo_wo_control.i_wos_to_run, i_nvo_wo_control.i_late_month_number)

//unitization config needs to be verified before starting unitization for any companies
//unitization config validations need to be performed whether or not fast101 is turned on.
rtn = uo_fast101.of_validate_correct_unitization_config(g_process_id, false)
DESTROY uo_fast101
if rtn <> 1 then
	f_pp_msgs(LEFT(string(today(),'mm/dd/yyyy hh:mm:ss.ff'),16) + ' HALT: Error encountered in Unitization Config validations.') 
	return false
end if 

////
////	loop over the lb, and react to the selected rows
////
num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c=1 to num_companies
	//	it's selected, update the rest of the window
	i_nvo_wo_control.of_companychanged(c, i_nvo_wo_control.i_month)

	select description  into :comp_descr from company where company_id = :i_nvo_wo_control.i_company;
			
	f_pp_msgs("Automatic Unitization Started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))

 	pp_stat_id = f_pp_statistics_start("WO Monthly Close", "Automatic Unitization: " + comp_descr)
	
	if yes_no = "YES" then
		//yes_no = system control value for 'AUTOMATIC UNITIZATION IS TURNED ON'
		
		uo_fast101 = CREATE uo_wo_unitization
		// set instance variables that used to be accessed directly from the w_wo_control window
		uo_fast101.of_setInstanceVariables(false, true, i_nvo_wo_control.i_late_charge_mos, i_nvo_wo_control.i_wos_to_run, i_nvo_wo_control.i_late_month_number)

		//i_late_only is set in w_wo_unitization_options
		if i_nvo_wo_control.i_late_only then
			regular_opt = 'NO'
		else
			regular_opt = 'YES'
		end if
		
		rtn = uo_fast101.of_start_company(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number, g_process_id, regular_opt, late_opt, false)  
		DESTROY uo_fast101

		if rtn < 0 then
			f_pp_msgs("ERROR Processing Automatic 101 Closings for "+ comp_descr + '. ' + string(now()))
			// keep track of all the companies that failed to unitize so they can be logged later
			b_all_success = false
			i_nvo_wo_control.of_add_to_failed_company_list(c)
			// do not set the date for this company since it failed to process
			// but move on to the next company and try to process it
			continue
		end if 		 

	else
		
		f_pp_msgs("Automatic Unitization has been turned off in pp_system_control")
		
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'auto_close',today()) 
	
		i_nvo_wo_control.of_updatedw( )	
		
		return false
		
	end if


	i_nvo_wo_control.i_ds_wo_control.setitem(1,'auto_close',today()) 
	
	i_nvo_wo_control.of_updatedw( )
	
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)

next

f_pp_msgs("Done. Please click on the Auto 101 menu and check for errors.")

f_pp_msgs("Process ended on " + String(Today(), "mm/dd/yyyy") + " at " + String(Today(), "hh:mm:ss"))

i_nvo_wo_control.of_cleanup( 6, 'email wo close: automatic unitization', "Auto Unitization")

return b_all_success
end function

public function integer uf_check_for_unapproved_charges (longlong a_company_id, longlong a_month_number);//////////////////////////////////////////////////////////////////////////////////////////////////////
////function name:  wf_check_for_unapproved_charges
////arguments:  a_company_id & a_month_number
////returns 1 for success; -1 for invalid data found
//////////////////////////////////////////////////////////////////////////////////////////////////////
//maint-44823:  accomodate clients who split afudc and overhead calc
longlong check_overhead_calculation, check_afudc_calc, check_accrual_calc, check_before_oh_calc, check_after_oh_calc, check_external_oh_calc 
longlong check_overhead_approval, check_afudc_approval, check_accrual_approval, check_before_oh_approval, check_after_oh_approval, check_external_oh_approval 
longlong powerplant_closed, cpr_closed, split_check, wo_accr_ct, cc_check, powerplant_closed_prior_month, company_cpr_control_check
string wo_accr_process_ans

 ////Need to verify calc vs approval dates to prevent unapproved charges from closing
 
 //first setnull() for all variables  
setnull(check_overhead_calculation) 
setnull(check_afudc_calc)
setnull(check_accrual_calc)
setnull(check_before_oh_calc)
setnull(check_after_oh_calc)
setnull(check_external_oh_calc)
setnull(check_overhead_approval)
setnull(check_afudc_approval)
setnull(check_accrual_approval)
setnull(check_before_oh_approval)
setnull(check_after_oh_approval)
setnull(check_external_oh_approval)
setnull(powerplant_closed)
setnull(cpr_closed)

//check if cpr is already closed
select to_number(to_char(cpr_closed,'yyyymm')) into :cpr_closed
from cpr_control 
where company_id = :a_company_id and to_number(to_char(accounting_month,'yyyymm')) = :a_month_number;

if not isnull(cpr_closed) then
	f_pp_msgs( "ERROR: The CPR is already closed for this month for company_id = " + string(a_company_id))
	return -1
end if  

//compare the wo_process_control dates for calc vs. approval
select  to_number(to_char(overhead_calculation,'yyyymm')), 
to_number(to_char(afudc_calc,  'yyyymm')), 
to_number(to_char(before_oh_calc, 'yyyymm')), 
to_number(to_char(after_oh_calc , 'yyyymm')), 
to_number(to_char(overhead_approval, 'yyyymm')), 
to_number(to_char(afudc_approval,  'yyyymm')), 
to_number(to_char(before_oh_approval,'yyyymm')),  
to_number(to_char(after_oh_approval ,'yyyymm')), 
to_number(to_char(accrual_calc ,'yyyymm')),
to_number(to_char(accrual_approval ,'yyyymm')),
to_number(to_char(external_oh_calc ,'yyyymm')),
to_number(to_char(external_oh_approval ,'yyyymm')) ,
to_number(to_char(powerplant_closed ,'yyyymm'))
into :check_overhead_calculation, :check_afudc_calc, :check_before_oh_calc, :check_after_oh_calc,   
	:check_overhead_approval, :check_afudc_approval,  :check_before_oh_approval, :check_after_oh_approval ,
	:check_accrual_calc, :check_accrual_approval, :check_external_oh_calc , :check_external_oh_approval, :powerplant_closed
from wo_process_control 
where company_id = :a_company_id and to_number(to_char(accounting_month,'yyyymm')) = :a_month_number;

if not isnull(powerplant_closed) then
	f_pp_msgs("ERROR: The WO Control is already closed for this month for company_id = " + string(a_company_id) )
	return -1
end if 

//if the company uses split afc/oh calculation and approvals, the "overhead_calculation" gets changed to null if one of the calcs get approved and there are none needing approval.
if not isnull(check_overhead_calculation) and isnull(check_overhead_approval) then
   f_pp_msgs("ERROR:  You have not Approved Overheads for company_id = " + string(a_company_id))
   return -1
end if

//Accruals calculation
wo_accr_process_ans = f_pp_system_control_company("Work Order Accrual Calculation", a_company_id)
if isnull(wo_accr_process_ans) or wo_accr_process_ans = "" then wo_accr_process_ans = 'no'
	
if wo_accr_process_ans = 'yes' then
	wo_accr_ct = long(f_pp_system_control_company("Work Order Accrual Charge Type", a_company_id))
	if not isnull(check_accrual_calc) and isnull(check_accrual_approval) then
		f_pp_msgs("ERROR:  You have not Approved Accruals for company_id = " + string(a_company_id))
		return -1
	end if
else
	wo_accr_ct = 0	
end if

if not isnull(check_external_oh_calc) and isnull(check_external_oh_approval) then
   f_pp_msgs("ERROR:  You have not Approved External Overheads for company_id = " + string(a_company_id))
   return -1
end if

//need to check if company is setup for split afudc/overhead processing
split_check = 0 
select count(*) into :split_check 
from afudc_oh_process_control
where company_id = :a_company_id;

//determine if afudc calculated charges are in the CWIP_CHARGE table
cc_check = 0 
select count(*) into :cc_check 
from cwip_charge 
where company_id = :a_company_id  
and month_number = :a_month_number 
and status = 1;

if split_check > 0 then	
	//company is setup for split afudc processing, the afudc_approval date should be filled in
	if cc_check > 0 and isnull(check_afudc_approval) then
		f_pp_msgs("ERROR: There are calculated Afudc charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id)) 
		return -1 
	end if
else	
	//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
	if cc_check > 0 and isnull(check_overhead_approval) then
		f_pp_msgs("ERROR: There are Afudc calculated charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
end if 

//determine if before overheads calculated charges are in the CWIP_CHARGE table
cc_check = 0 
select count(*) into :cc_check 
from cwip_charge 
where company_id = :a_company_id  
and month_number = :a_month_number 
and status in (select clearing_id from clearing_wo_control where before_afudc_indicator = 1)
and charge_type_id <> :wo_accr_ct;	

if split_check > 0 then	
	//company is setup for split afudc processing, the before_oh_approval date should be filled in
	if cc_check > 0 and isnull(check_before_oh_approval) then
		f_pp_msgs("ERROR: There are calculated Before OH charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
else	
	//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
	if cc_check > 0 and isnull(check_overhead_approval) then
		f_pp_msgs("ERROR: There are Before OH calculated charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
end if 

//determine if After Overheads calculated charges are in the CWIP_CHARGE table
cc_check = 0 
select count(*) into :cc_check 
from cwip_charge 
where company_id = :a_company_id  
and month_number = :a_month_number 
and status in (select clearing_id from clearing_wo_control where before_afudc_indicator = 0)
and charge_type_id <> :wo_accr_ct;	

if split_check > 0 then	
	//company is setup for split afudc processing, the after_oh_approval date should be filled in
	if cc_check > 0 and isnull(check_after_oh_approval) then
		f_pp_msgs( "ERROR: There are calculated After OH charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
else	
	//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
	if cc_check > 0 and isnull(check_overhead_approval) then
		f_pp_msgs("ERROR: There are After OH calculated charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
end if 

//check if prior month is still open
setnull(powerplant_closed_prior_month)
select  to_number(to_char(powerplant_closed,'yyyymm')) 
into :powerplant_closed_prior_month
from wo_process_control where company_id = :a_company_id and to_char(add_months(accounting_month, 1),'yyyymm')  = :a_month_number;

if isnull(powerplant_closed_prior_month) then	
	//check if the calc has been run and not yet approved for prior month
	setnull(check_overhead_calculation)
	setnull(check_overhead_approval)
	select  to_number(to_char(overhead_calculation,'yyyymm')), to_number(to_char(overhead_approval, 'yyyymm')) 
	into :check_overhead_calculation, :check_overhead_approval
	from wo_process_control where company_id = :a_company_id and to_char(add_months(accounting_month, 1),'yyyymm')  = :a_month_number;

	if not isnull(check_overhead_calculation) and isnull(check_overhead_approval) then		
		f_pp_msgs("ERROR: There are unapproved charges for the prior month for company_id = " + string(a_company_id))
		return -1
	end if
end if 

company_cpr_control_check = 0
select count(1) into :company_cpr_control_check
from cpr_control 
where company_id = :a_company_id
and to_number(to_char(accounting_month,'yyyymm')) = :a_month_number;

if company_cpr_control_check = 0 then
	f_pp_msgs("ERROR: The CPR Control table is not set up for this Company and Month." )
	return -1
end if 

return 1

end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppprojct_custom_version nvo_ppprojct_custom_version

choose case a_pbd_name
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

