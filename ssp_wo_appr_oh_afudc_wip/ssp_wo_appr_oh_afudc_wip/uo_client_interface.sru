HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_appr_oh_afudc_wip.exe'

nvo_wo_control i_nvo_wo_control

string i_calc_ovh_before  
string i_calc_ovh_after   
string i_calc_afudc       
string i_appr_ovh_before   
string i_appr_ovh_after   
string i_appr_afudc

string i_curr_comp_descr
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function boolean uf__main_appr_oh_afudc_wip ()
public function boolean uf_none_approval (datetime a_month)
public function boolean uf_before_oh_approval (datetime a_month)
public function boolean uf_afudc_real_approval (datetime a_month)
public function boolean uf_after_oh_approval (datetime a_month)
public function boolean uf_retrieve_dates ()
public function boolean uf_wo_afudc_ovh_control_approve (boolean cbx_appr_ovh_before_checked, boolean cbx_appr_afudc_checked, boolean cbx_appr_ovh_after_checked, boolean cbx_appr_ovh_before_enabled, boolean cbx_appr_afudc_enabled)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Process Close Charge Collection logic
if not uf__main_appr_oh_afudc_wip() then
	return rtn_failure
end if

i_nvo_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function boolean uf__main_appr_oh_afudc_wip ();//**********************************************************************************************************************************************************
//
// uf__main_appr_oh_afudc_wip()
// 
// The main workflow for the Interface.  Calls all the necessary functions to approve Overheads,
//   AFUDC and WIP
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string  company, je_code, month, where_str, acct, wo_process_ans, comp_descr, logTemp
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i, rtn
integer mo, yr, ret, clear_type
decimal {2} amount
////	added for company loop
longlong	c, num_companies, process_id,  my_count, company2, month_number
uo_ds_top ds_users
boolean vfy_users
string  sqls, find_str,  user_id, text2, process_msg

longlong ndx
boolean cbx_appr_ovh_before_checked
boolean cbx_appr_afudc_checked
boolean cbx_appr_ovh_after_checked
boolean cbx_appr_afudc_enabled
boolean cbx_appr_ovh_before_enabled

i_nvo_wo_control.of_constructor()

// these parameters originate from the w_wo_control window
i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_wo_control.i_month = datetime(g_ssp_parms.date_arg[1])

// setup the transiant CURRENT_AFUDC_VIEW by using the dw_cpr_act_month to write
// the session ID to the CPR_ACT_MONTH table
i_nvo_wo_control.of_currentmonth(i_nvo_wo_control.i_month)

i_nvo_wo_control.i_month_number = year(g_ssp_parms.date_arg[1]) * 100 + month(g_ssp_parms.date_arg[1])

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("Approve Overheads and AFUDC WIP - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

f_pp_msgs("Approve Overheads and AFUDC WIP")
f_pp_msgs("Attempting to Approve Overheads and AFUDC WIP for: ")
f_pp_msgs("Month: " + string(i_nvo_wo_control.i_month, "mm/dd/yyyy"))
for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	f_pp_msgs("Company ID: " + string(i_nvo_wo_control.i_company_idx[ndx]))
next

if (upperbound(g_ssp_parms.boolean_arg) = 1) then
	logTemp = string(g_ssp_parms.boolean_arg[1])
	if isnull(logTemp) then logTemp = "NULL"
	f_pp_msgs("Approve Overhead Before: " + logTemp)
end if

if (upperbound(g_ssp_parms.boolean_arg2) = 1) then
	logTemp = string(g_ssp_parms.boolean_arg2[1])
	if isnull(logTemp) then logTemp = "NULL"
	f_pp_msgs("Approve AFUDC: " + logTemp)
end if

if (upperbound(g_ssp_parms.boolean_arg3) = 1) then
	logTemp = string(g_ssp_parms.boolean_arg3[1])
	if isnull(logTemp) then logTemp = "NULL"
	f_pp_msgs("Approve Overhead After: " + logTemp)
end if

if (upperbound(g_ssp_parms.boolean_arg4) = 1) then
	logTemp = string(g_ssp_parms.boolean_arg4[1])
	if isnull(logTemp) then logTemp = "NULL"
	f_pp_msgs("Approve Overhead Before Enabled: " + logTemp)
end if

if (upperbound(g_ssp_parms.boolean_arg5) = 1) then
	logTemp = string(g_ssp_parms.boolean_arg5[1])
	if isnull(logTemp) then logTemp = "NULL"
	f_pp_msgs("Approve AFUDC Enabled: " + logTemp)
end if

// BEGIN validate input
if (upperbound(i_nvo_wo_control.i_company_idx) < 1) then
	f_pp_msgs("No Company IDs passed into interface")
	return false
end if
// END validate input

i_nvo_wo_control.i_afudc_error = false
i_nvo_wo_control.i_no_summary = false

// populate the company descs
rtn = i_nvo_wo_control.of_getdescriptionsfromids( i_nvo_wo_control.i_company_idx )
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// check if the multiple companies chosen can be processed together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do " + &
				" not line up")
	return false
end if

num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c=1 to num_companies
	//	it's selected, update the data window
	i_nvo_wo_control.of_companychanged(c,i_nvo_wo_control.i_month)
	
	// Make sure the month isn't closed
	if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		return false
	end if
	
next

// calling this function to simulate a clicked event and to set the i_owned, i_company, retrieve the i_ds_interface_dates_all and i_ds_wo_control data stores, and to get the i_month_number instance variable
i_nvo_wo_control.of_companychanged(1, i_nvo_wo_control.i_month)

if not i_nvo_wo_control.i_split_oh_afudc then

	// SEB: Moving loop
	num_companies = upperbound(i_nvo_wo_control.i_company_idx)
	for c=1 to num_companies
		//	it's selected, update the rest of the window
		i_nvo_wo_control.of_companychanged(c, datetime(g_ssp_parms.date_arg[1]))
		
		i_curr_comp_descr = i_nvo_wo_control.i_company_descr[c]
		
		f_pp_msgs("Processing company " + i_curr_comp_descr)

		//  Get pp_system_control variable to determine whether or not to calculate
		//  Afudc, Overheads, or Both ...
		wo_process_ans = f_pp_system_control_company("Work Order Calculation", i_nvo_wo_control.i_company)
		
		if isnull(wo_process_ans) or wo_process_ans = "" then
			wo_process_ans = "afudc/overheads"
		end if
		
		f_pp_msgs("wo_process_ans: " + string(wo_process_ans))
		
		CHOOSE CASE wo_process_ans
			CASE 'afudc'
				f_pp_msgs(  "Starting AFUDC approval."+ ' ' + string(now()))
			CASE 'overheads'
				f_pp_msgs(  "Starting Overhead approval."+ ' ' + string(now()))
			CASE 'none'
				f_pp_msgs(  "Skipping OH and AFUDC approval."+ ' ' + string(now()))
				goto overhead_calc
			CASE ELSE
				f_pp_msgs( "Starting Overhead/AFUDC approval."+ ' ' + string(now()))
		END CHOOSE
	
		if wo_process_ans = 'none' then 
			uf_none_approval(datetime(g_ssp_parms.date_arg[1]))
			goto overhead_calc
		end if
			
		if (wo_process_ans =  'overheads' or &
												wo_process_ans = "afudc/overheads") then	
			uf_before_oh_approval(datetime(g_ssp_parms.date_arg[1]))
			//Maint 29754
			if i_nvo_wo_control.i_afudc_error then 
				rollback;
				goto overhead_calc
			end if 
			if wo_process_ans ='overheads' then goto overhead_calc_after
		end if
		
		// setup the transiant CURRENT_AFUDC_VIEW by using the dw_cpr_act_month to write
		// the session ID to the CPR_ACT_MONTH table
		i_nvo_wo_control.of_currentmonth(i_nvo_wo_control.i_month)
		
		uf_afudc_real_approval(datetime(g_ssp_parms.date_arg[1]))
	
		//Maint 29754
		if i_nvo_wo_control.i_afudc_error then 
			rollback;
			goto overhead_calc
		end if 
		
		overhead_calc_after:
	
		if (wo_process_ans =  'overheads' or &
												wo_process_ans = "afudc/overheads") then	
			uf_after_oh_approval(datetime(g_ssp_parms.date_arg[1]))
			//Maint 29754
			if i_nvo_wo_control.i_afudc_error then 
				rollback; 
			end if 
		end if
		
		overhead_calc:
		//Maint 29754
		if i_nvo_wo_control.i_afudc_error then 
			f_wo_status_box("Approve Overheads and AFUDC", "ERROR in Overheads and AFUDC Approval!" + ' ' + string(now()))
		else
			f_wo_status_box("Approve Overheads and AFUDC", "Finished Overheads and AFUDC Approval!" + ' ' + string(now()))
		end if 
	
		commit;
		
	next

else
	i_nvo_wo_control.i_calc  = false
	i_nvo_wo_control.i_approve = true
	
	// make sure no NULL values were sent by Job Server
	if isnull(g_ssp_parms.boolean_arg[1]) or isnull(g_ssp_parms.boolean_arg2[1]) or &
		isnull(g_ssp_parms.boolean_arg3[1]) or isnull(g_ssp_parms.boolean_arg4[1]) or &
		isnull(g_ssp_parms.boolean_arg5[1]) then
		f_pp_msgs("ERROR: NULL Value Detected in AFUDC and Overhead Process Control Approval Options!!!")
		return false
	end if
	
	// make sure PowerPlant passed in values corresponding to the check boxes
	// on the AFUDC window
	if ( (upperbound(g_ssp_parms.boolean_arg) = 1) and (upperbound(g_ssp_parms.boolean_arg2) = 1) and &
		  (upperbound(g_ssp_parms.boolean_arg3) = 1) and (upperbound(g_ssp_parms.boolean_arg4) = 1) and &
		  (upperbound(g_ssp_parms.boolean_arg5) = 1) ) then
		  
		cbx_appr_ovh_before_checked = g_ssp_parms.boolean_arg[1]
		cbx_appr_afudc_checked = g_ssp_parms.boolean_arg2[1]
		cbx_appr_ovh_after_checked = g_ssp_parms.boolean_arg3[1]
		cbx_appr_ovh_before_enabled = g_ssp_parms.boolean_arg4[1]
		cbx_appr_afudc_enabled = g_ssp_parms.boolean_arg5[1]
		
		uf_wo_afudc_ovh_control_approve(cbx_appr_ovh_before_checked,cbx_appr_afudc_checked, &
												  cbx_appr_ovh_after_checked, cbx_appr_ovh_before_enabled, &
												  cbx_appr_afudc_enabled)
	else
		f_pp_msgs("ERROR: AFUDC and Overhead Process Control Approval Options Required For Selected Companies But Not Present!!!")
		return false
	end if
end if

f_pp_msgs("Process ended on " + String(Today(), "mm/dd/yyyy") + " at " + String(Today(), "hh:mm:ss"))

i_nvo_wo_control.of_cleanup( 3, 'email wo close: approve overhead and afudc', "Approve Overheads and AFUDC")

return not i_nvo_wo_control.i_afudc_error
end function

public function boolean uf_none_approval (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_none_approval()
// 
// Validate and Approve Overheads AFUDC and WIP calculations
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************


string  company, je_code, month, where_str, acct
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
//	added for company loop
longlong	c, num_companies


//
//  They cannot do this twice ...
//
if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "overhead_approval")) then
	f_pp_msgs("Approve Over. and AFUDC: Approval None - You have already approved the overheads and AFUDC for this month.")
	return false
end if

// check if calc is done already
if isnull(i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1,'overhead_calculation')) then 
	f_pp_msgs("Approve Over. and AFUDC - The calculation must be performed first " &
				+ "in order to be approved.") 
	return false
end if

if not i_nvo_wo_control.i_split_oh_afudc then
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_approval',today()) 

	i_nvo_wo_control.of_updatedwnocommit( )
end if	
	


return true
end function

public function boolean uf_before_oh_approval (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_before_oh_approval()
// 
// Validate and Approve Overheads AFUDC and WIP calculations
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************


string  company, je_code, month, where_str, acct, loading_dw
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
////	added for company loop
longlong	c, num_companies
datetime ovh_approval
//	$$$ mtc 20090702
longlong je_method_id


f_pp_msgs(  "Creating Journal Entries for Before Overheads"+ ' ' + string(now()))


//
//  They cannot do this twice ...
//
if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "overhead_approval")) then
	f_pp_msgs("Approve Over. and AFUDC. Before OVH - You have already approved the overheads and AFUDC for this month.")
	rollback;
	return false
end if

ovh_approval = i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "before_oh_approval")

if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "before_oh_approval")) then
	f_pp_msgs("Approve Over. and AFUDC - You have already approved the before overheads for this month.")
	rollback;
	return false
end if
	

// check if calc is done already
if isnull(i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1,'overhead_calculation')) then 
	f_pp_msgs("Approve Over. and AFUDC - The calculation must be performed first " &
				+ "in order to be approved.")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if

f_pp_msgs("Creating GL Journal Entries - Month: " + string(i_nvo_wo_control.i_month_number) + &
	 " Company:  " + i_curr_comp_descr + " Comp Id: " + string(i_nvo_wo_control.i_company)+ ' ' + string(now()))

// Generate the General Ledger Transactions.

// create uo_ds_top
uo_ds_top dw_gl_transaction
dw_gl_transaction = CREATE uo_ds_top
dw_gl_transaction.Dataobject = 'dw_gl_transaction'
dw_gl_transaction.SetTransObject(sqlca)

uo_ds_top dw_afudc_gl_trans
dw_afudc_gl_trans = CREATE uo_ds_top
dw_afudc_gl_trans.Dataobject = 'dw_afudc_gl_trans'
dw_afudc_gl_trans.SetTransObject(sqlca)

uo_ds_top dw_clearing_wo_control
dw_clearing_wo_control = CREATE uo_ds_top
dw_clearing_wo_control.Dataobject = 'dw_clearing_wo_control'
dw_clearing_wo_control.SetTransObject(sqlca)

uo_ds_top dw_gl_charges
dw_gl_charges = CREATE uo_ds_top
dw_gl_charges.Dataobject = 'dw_gl_charges_afc'
dw_gl_charges.SetTransObject(sqlca)

num_clear_rows = dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)

f_pp_msgs("Creating GL Journal Entries (Overheads) ...")

dw_gl_charges.Dataobject = 'dw_gl_charges_oh'

dw_gl_charges.settransobject(sqlca)

where_str   = " and cwip_charge.status in " &
				+ "(select distinct clearing_id from clearing_wo_control where before_afudc_indicator = 1) " 

f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month_number, i_nvo_wo_control.i_company)


select gl_company_no into :company 
  from company where company_id = :i_nvo_wo_control.i_company;

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Creating GL Journal Entries (Overheads) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.insertrow(1)
		
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)

	dw_gl_transaction.setitem(1,'gl_status_id',1)

	amount = dw_gl_charges.getitemdecimal(i,'amount')

	ce_id    = dw_gl_charges.getitemnumber(i, 'cost_element_id')
	acct_id  = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	// The Overhead Type is saved in the Status column of the charges
	oh_id = dw_gl_charges.getitemnumber(i, 'status')
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')

	row_num = dw_clearing_wo_control.find("clearing_id = " + string(oh_id), &
														1, num_clear_rows)

	if row_num < 0 then
		f_pp_msgs("dw Find Error on dw_clearing_wo_control.")
		
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if

		
	if row_num = 0 then
		f_pp_msgs("Could not find the dw_clearing_wo_control record for clearing_id " + &
			"= " + string(oh_id))

		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if		
	
	clear_type  = dw_clearing_wo_control.getitemnumber(row_num, 'clearing_indicator')
	credit_acct = dw_clearing_wo_control.getitemnumber(row_num, 'credit_gl_account')
	je_code     = dw_clearing_wo_control.getitemstring(row_num, 'clearing_je')

	je_code = trim(je_code)

	dw_gl_transaction.setitem(1,'gl_je_code', je_code)
	
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')

	if clear_type = 1 then                         // clearing overhead
	
		acct = f_autogen_je_account(dw_gl_charges,i, 2,	acct_id,je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if
		
		dw_gl_transaction.setitem(1,'gl_account', acct)

		if amount > 0 then
			dw_gl_transaction.setitem(1,'amount',                 amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		else
			dw_gl_transaction.setitem(1,'amount',                -amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		end if
		
		 // store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))

		dw_gl_transaction.setitem(1,'amount_type', 1)
		dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
		dw_gl_transaction.setitem(1,'trans_type', 2)
		
		
	else														  // loading overhead
		
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		
		acct = f_autogen_je_account(dw_gl_charges,i, 3,	acct_id, je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if

		dw_gl_transaction.setitem(1,'gl_account', acct)

		dw_gl_transaction.setitem(1,'amount_type', 1)
		dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
		dw_gl_transaction.setitem(1,'trans_type', 3)


		// Must also create credit offset transaction
		dw_gl_transaction.InsertRow(1)
		
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

		dw_gl_transaction.setitem(1,'gl_trans_id',            trans_id)
		dw_gl_transaction.setitem(1,'month',                  i_nvo_wo_control.i_month)
		dw_gl_transaction.setitem(1,'company_number',         company)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		
		 // store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))

		dw_gl_transaction.setitem(1,'amount_type', 1)
		dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
			
		loading_dw = f_pp_system_control_company('Loading Datawindow', i_nvo_wo_control.i_company)

		if loading_dw <> 'charges' then
			
			acct = f_autogen_je_account(dw_clearing_wo_control, row_num, 7,	credit_acct, je_method_id)
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		else
			 acct = f_autogen_je_account(dw_gl_charges, i, 7,  credit_acct, je_method_id)
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		end if

		dw_gl_transaction.setitem(1,'gl_account', acct)
		dw_gl_transaction.setitem(1,'trans_type', 7)

	end if 
	
next // Rows

f_pp_msgs("Updating ...")

ret = dw_gl_transaction.Update()

if ret <> 1 then
	f_pp_msgs("Error updating General Ledger Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext+ ' ' + string(now()))
 
	rollback using sqlca;
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	return false
end if

if not i_nvo_wo_control.i_split_oh_afudc then
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'before_oh_approval', today())
	i_nvo_wo_control.of_updatedwnocommit()
else
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'before_oh_approval', today())
	
	longlong before_ind, afudc_ind, after_ind
	
	select before_oh_indicator, afudc_indicator, after_oh_indicator
	into :before_ind, :afudc_ind, :after_ind
	from		afudc_oh_process_control
	where company_id = :i_nvo_wo_control.i_company
	;

	if sqlca.sqlcode < 0 then
		f_pp_msgs("Error retrieving Indicators during date update : " + sqlca.sqlerrtext)
	end if	
	
	if isnull(before_ind) or isnull(afudc_ind) or isnull(after_ind) then
		f_pp_msgs("Error retrieving Indicators from afudc_oh_process_control, dates not updated properly")
	end if
	
	date null_date
	SetNull(null_date)
	
	if after_ind = 1 or afudc_ind = 1 then
		if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "overhead_calculation")) and &
			(isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1,"after_oh_calc")) and &
			isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1,"afudc_calc"))) then
				i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation', null_date)
		end if
	elseif after_ind = 0 and afudc_ind = 0 then //AFter Ind = 0 
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_approval', today())
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'before_oh_approval', today())
	end if		
	
	i_nvo_wo_control.of_updatedwnocommit()
end if


return true
end function

public function boolean uf_afudc_real_approval (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_afudc_real_approval()
// 
// Validate and Approve Overheads AFUDC and WIP calculations
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************


string  company, je_code, month, where_str, acct, wo_process_ans, comp_descr, je_code_option
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i, check_debt_afudc, check_equity_afudc, debt_account_id, equity_account_id  
integer mo, yr, ret, clear_type, rtn, have_wip
decimal {2} amount
////	added for company loop
longlong	c, num_companies, pp_stat_id, my_count

longlong wip_rtn

uo_wip_computations uo_wip
uo_wip = create uo_wip_computations

select count(*) into :have_wip
from   wip_computation  where rownum = 1;

longlong je_method_id

//
//	loop over the lb, and react to the selected rows
//
f_pp_msgs( "Creating Journal Entries for AFUDC "+ string(now()))


//
//  They cannot do this twice ...
//
if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "overhead_approval")) then
	f_pp_msgs("Approve Over. and AFUDC: Approval - You have already approved the overheads and AFUDC for this month.")
	rollback;
	return false
end if

if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "afudc_approval")) then
	f_pp_msgs("Approve Over. and AFUDC - You have already approved the AFUDC for this month.")
	rollback;
	return false
end if

// check if calc is done already
if isnull(i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1,'overhead_calculation')) then 
	f_pp_msgs("Approve Over. and AFUDC - The calculation must be performed first " &
				+ "in order to be approved.")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if

f_pp_msgs("Creating GL Journal Entries - Month: " + string(i_nvo_wo_control.i_month_number) + &
	 " Company:  " + i_curr_comp_descr + " Comp Id: " + string(i_nvo_wo_control.i_company) + ' ' + string(now()))

select description  into :comp_descr from company where company_id = :i_nvo_wo_control.i_company;

f_pp_msgs("Creating GL Journal Entries started for company " +  comp_descr +  " at " + String(Today(), "hh:mm:ss"))
pp_stat_id = f_pp_statistics_start("WO Monthly Close", "Approve Overheads and AFUDC (cb_afudc_real_approval): " + comp_descr)

// Generate the General Ledger Transactions.

// create uo_ds_top
uo_ds_top dw_gl_transaction
dw_gl_transaction = CREATE uo_ds_top
dw_gl_transaction.Dataobject = 'dw_gl_transaction'
dw_gl_transaction.SetTransObject(sqlca)

uo_ds_top dw_afudc_gl_trans

dw_afudc_gl_trans = CREATE uo_ds_top
dw_afudc_gl_trans.Dataobject = 'dw_afudc_gl_trans'
dw_afudc_gl_trans.SetTransObject(sqlca)

uo_ds_top dw_clearing_wo_control
dw_clearing_wo_control = CREATE uo_ds_top
dw_clearing_wo_control.Dataobject = 'dw_clearing_wo_control'
dw_clearing_wo_control.SetTransObject(sqlca)

uo_ds_top dw_gl_charges
dw_gl_charges = CREATE uo_ds_top
dw_gl_charges.Dataobject = 'dw_gl_charges_afc'
dw_gl_charges.SetTransObject(sqlca)

where_str   = " and cost_element_id in " &
				+ "(select distinct equity_cost_element from afudc_control) " &
				+ " and cwip_charge.company_id  = " + string(i_nvo_wo_control.i_company)
				
f_add_where_clause_ds(dw_gl_charges, "", where_str, false)

num_rows       = dw_afudc_gl_trans.RETRIEVE(i_nvo_wo_control.i_month)
num_clear_rows = dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)
num_gl_rows    = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month)

if num_rows <= 0 then
	// For debugging purposes...
	datetime cpr_month
	string user_id
	longlong session_id, afc_count
	select month, user_id, session_id
	  into :cpr_month, :user_id, :session_id
	  from cpr_act_month;
	  
	select count(*)
	  into :afc_count
	  from current_afudc_view;
	  
	 if num_rows < 0 then
	 	f_pp_msgs("ERROR: dw_afudc_gl_trans.retrieve failed.")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	elseif num_rows = 0 then
		f_pp_msgs("Information: dw_afudc_gl_trans.retrieve resulted in 0 rows")
		f_pp_msgs("Information: CPR_ACT_MONTH.USER_ID = '" + user_id + "', MONTH = '" + string(cpr_month) + "', SESSSION_ID = '" + string(session_id) + "'")
		f_pp_msgs("Information: CURRENT_AFUDC_VIEW COUNT = '" + string(afc_count) + "'")
	end if
end if

// maint 5649 
if num_gl_rows < 0 then
	f_pp_msgs(  "ERROR:  Creating GL Journal Entries (AFUDC Equity) : dw_gl_charges.RETRIEVE failed  ."  + string(now()))
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if

f_pp_msgs( "Creating " + string(num_gl_rows) + " GL Journal Entries (AFUDC Equity) ..."+ ' ' + string (now()))

 
 check_equity_afudc = 0
 
 Select count(*) into :check_equity_afudc
 from cwip_charge where status = 1 and
 month_number = :i_nvo_wo_control.i_month_number and
 company_id = :i_nvo_wo_control.i_company and
 cost_element_id in (select equity_cost_element from afudc_control);

if check_equity_afudc > 0 and num_gl_rows <= 0 then
	f_pp_msgs(  "ERROR:  Creating GL Journal Entries (AFUDC Equity) : Failed to retrieve Debt charges for Journals  ."  + string(now()))
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if      


select gl_company_no into :company 
  from company where company_id = :i_nvo_wo_control.i_company;

select je_id into :je_id 
  from gl_je_control where process_id = 'AFUDC';
  
// lkkk
je_code_option = f_pp_system_control_company('AFUDC Approval - Use External JE', i_nvo_wo_control.i_company)

if je_code_option = 'yes' or je_code_option = '' then
	select external_je_code into :je_code 
	 from standard_journal_entries where je_id = :je_id;
else
	select gl_je_code into :je_code 
	 from standard_journal_entries where je_id = :je_id;
end if

// Process the AFUDC Equity transactions
for i = 1 to num_gl_rows
	
	if mod(i, 50) = 0 then
		f_pp_msgs("Creating GL Journal Entries (AFUDC Equity) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if
	
	dw_gl_transaction.insertrow(1)

	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')									
	// Must find the credit account for this afudc type
	afc_id = dw_gl_charges.getitemnumber(i, 'afudc_type_id')

	row_num = dw_afudc_gl_trans.Find("afudc_type_id = " + string(afc_id), 1, num_rows)

	if row_num < 0 then
		f_pp_msgs(  "ERROR: dw Find Error on Afudc Type  " + ' ' + string(now()))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if
		
	if row_num = 0 then
		f_pp_msgs( "Afudc Type not found. Check for missing debt and equity accounts " + &
			"in the AFUDC_DATA table."+ ' ' + string(now()))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if
	
	// ### DJL 062011 
	// Maint 7591 - Change AFUDC approval to pass gl_account_id (instead of external_account_code) to f_autogen_je_account.
	//acct = dw_afudc_gl_trans.getitemstring(row_num, 'equity_account')
	equity_account_id = dw_afudc_gl_trans.getitemnumber(row_num, 'equity_account_id')
	
	//	$$$ mtc 20090702
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	acct = f_autogen_je_account(dw_gl_charges, i, 0, equity_account_id, je_method_id)	
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
		
	je_code = trim(je_code)

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',1)
	dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
	dw_gl_transaction.setitem(1,'trans_type', 0)
	
	
	dw_gl_transaction.InsertRow(1)

	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
	
	acct_id = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	acct = f_autogen_je_account(dw_gl_charges, i, 1, acct_id,je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if								

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i, 'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type', 1)
	dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
	dw_gl_transaction.setitem(1,'trans_type', 1)

next

dw_gl_charges.Dataobject = 'dw_gl_charges_afc'

dw_gl_charges.SetTransObject(sqlca)

where_str   = " and cost_element_id in " &
				+ "(select distinct debt_cost_element from afudc_control) " &
				+ " and cwip_charge.company_id  = " + string(i_nvo_wo_control.i_company)


f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month)

// maint 5649 
if num_gl_rows < 0 then
	f_pp_msgs(  "ERROR:  Creating GL Journal Entries (AFUDC Debt) : dw_gl_charges.RETRIEVE failed  ."  + string(now()))
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
 end if
 
 f_pp_msgs( "Creating " + string(num_gl_rows) + " GL Journal Entries (AFUDC Debt) ..."+ ' ' + string (now()))    

 check_debt_afudc = 0
 
 Select count(*) into :check_debt_afudc
 from cwip_charge where status = 1 and
 month_number = :i_nvo_wo_control.i_month_number and
 company_id = :i_nvo_wo_control.i_company and
 cost_element_id in (select debt_cost_element from afudc_control);

 if check_debt_afudc > 0 and num_gl_rows <= 0 then
	f_pp_msgs(  "ERROR:  Creating GL Journal Entries (AFUDC Debt) : Failed to retrieve Debt charges for Journals  ."  + string(now()))
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
 end if      

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Creating GL Journal Entries (AFUDC Debt) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.InsertRow(1)

	trans_id = f_topkey_no_dw('gl_transaction', 'gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',  		 dw_gl_charges.getitemdecimal(i,'amount'))
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')																				
	afc_id = dw_gl_charges.getitemnumber(i,'afudc_type_id')

	row_num = dw_afudc_gl_trans.Find("afudc_type_id = " + string(afc_id), 1, num_rows)

	if row_num < 0 then
		f_pp_msgs(  "ERROR: dw Find Error on Afudc Type  " + ' ' + string(now()))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if

		
	if row_num = 0 then
		f_pp_msgs( "Afudc Type not found. Check for missing debt and equity accounts " + &
			"in the AFUDC_DATA table."+ ' ' + string(now()))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if	
	
	// Maint 7591 - Change AFUDC approval to pass gl_account_id (instead of external_account_code) to f_autogen_je_account.
	//acct = dw_afudc_gl_trans.getitemstring(row_num,'debt_account')
	debt_account_id = dw_afudc_gl_trans.getitemnumber(row_num, 'debt_account_id')
	
	//	$$$ mtc 20090702
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	acct = f_autogen_je_account(dw_gl_charges, i, -1, debt_account_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
	
	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',1)
	dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
	dw_gl_transaction.setitem(1,'trans_type',-1)

	dw_gl_transaction.insertrow(1)

	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
											
	acct_id = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	acct = f_autogen_je_account(dw_gl_charges, i, 1, acct_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))

	dw_gl_transaction.setitem(1,'amount_type', 1)
	dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
	dw_gl_transaction.setitem(1,'trans_type', 1)
	
next // Debt Row

// BEGIN: create journals for CPI (if "turned on" via je_method table)
f_pp_msgs("Creating GL Journal Entries (CPI) ...")

dw_gl_charges.Dataobject = 'dw_gl_charges_cpi'

dw_gl_charges.SetTransObject(sqlca)

where_str   = " and cwip_charge.company_id  = " + string(i_nvo_wo_control.i_company) + " "
  
f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month)

// maint 5649 
if num_gl_rows < 0 then
	f_pp_msgs(  "ERROR:  Creating GL Journal Entries (CPI) : dw_gl_charges.RETRIEVE failed  ."  + string(now()))
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if
 
f_pp_msgs( "Creating " + string(num_gl_rows) + " GL Journal Entries (CPI) ..."+ ' ' + string (now()))

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Creating GL Journal Entries (CPI) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.InsertRow(1)

	trans_id = f_topkey_no_dw('gl_transaction', 'gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')                                                            
	afc_id = dw_gl_charges.getitemnumber(i,'afudc_type_id')
	
	row_num = dw_afudc_gl_trans.Find("afudc_type_id = " + string(afc_id), 1, num_rows)
		
	if row_num < 0 then
		f_pp_msgs(  "ERROR: dw Find Error on CPI  " + ' ' + string(now()))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if
		
	if row_num = 0 then
		f_pp_msgs( "Afudc Type not found. Check for missing debt and equity accounts " + &
		"in the AFUDC_DATA table."+ ' ' + string(now()))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if   
	
  // Maint 7591 - Change AFUDC approval to pass gl_account_id (instead of external_account_code) to f_autogen_je_account.	
  // acct = dw_afudc_gl_trans.getitemstring(row_num,'debt_account')
  debt_account_id = dw_afudc_gl_trans.getitemnumber(row_num, 'debt_account_id')
	
	//   $$$ mtc 20090702
	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	acct = f_autogen_je_account(dw_gl_charges, i, 35, debt_account_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
	
	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',99)
	dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
	dw_gl_transaction.setitem(1,'trans_type',35)

	dw_gl_transaction.insertrow(1)

	trans_id = f_topkey_no_dw('gl_transaction', 'gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)
	dw_gl_transaction.setitem(1,'amount',         dw_gl_charges.getitemdecimal(i,'amount'))
											
	acct_id = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	acct = f_autogen_je_account(dw_gl_charges, i, 36, acct_id, je_method_id)
	//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
	if mid(acct,1,5) = 'ERROR' then
		f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
		rollback using sqlca;
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		return false
	end if
	
	// lots of folks do not have gl_account.external_account_code populated for CPI, 
	// so this is a default to prevent an Oracle error (gl_transaction.gl_account is not nullable)
	if acct = '' or IsNull(acct) then acct = 'CPI'

	dw_gl_transaction.setitem(1,'gl_account',             acct)
	dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
	dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
	dw_gl_transaction.setitem(1,'gl_status_id',           1)
	
	dw_gl_transaction.setitem(1,'description', dw_gl_charges.getitemstring(i,'work_order_number'))
	
	dw_gl_transaction.setitem(1,'amount_type',99)
	dw_gl_transaction.setitem(1,'je_method_id', je_method_id)
	dw_gl_transaction.setitem(1,'trans_type',36)
	
next // CPI Row
// END: create journals for CPI (if "turned on" via je_method table)

f_pp_msgs("Updating ...")

ret = dw_gl_transaction.Update()

if ret <> 1 then
	f_pp_msgs( "Error updating General Ledger Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext + ' ' + string(now()))  
	rollback using sqlca;
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if

// REG CWIP Changes

if have_wip > 0 then

	wip_rtn = uo_wip.uf_wip_comp_approve(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number)
	if wip_rtn = -1 then
		rollback using sqlca;
		f_pp_msgs( "Error in WIP Comp Approvals. " + ' ' + string(now()))  
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		if isvalid(uo_wip) then
			destroy uo_wip
		end if
		rollback;
		return false
	end if
	
	// REG CWIP Changes END
 
end if


if not i_nvo_wo_control.i_split_oh_afudc then
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'afudc_approval',today()) 
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_approval',today())
	i_nvo_wo_control.of_updatedwnocommit( )
else
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'afudc_approval', today())

	longlong before_ind, afudc_ind, after_ind
	
	select before_oh_indicator, afudc_indicator, after_oh_indicator
	into :before_ind, :afudc_ind, :after_ind
	from		afudc_oh_process_control
	where company_id = :i_nvo_wo_control.i_company
	;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs( "Error retrieving Indicators during date update : " + sqlca.sqlerrtext) 
	end if	
	
	if isnull(before_ind) or isnull(afudc_ind) or isnull(after_ind) then
		f_pp_msgs( "Error retrieving Indicators from afudc_oh_process_control, dates not updated properly") 
	end if
	
	date null_date
	SetNull(null_date)
	
	if after_ind = 1 then
		if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "overhead_calculation")) and &
			isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "after_oh_calc")) then
			i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_calculation', null_date)
		end if
	else//AFter Ind = 0 
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_approval', today())
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'afudc_approval', today())
	end if		
	
	i_nvo_wo_control.of_updatedwnocommit( )
end if
if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)



return true
end function

public function boolean uf_after_oh_approval (datetime a_month);//**********************************************************************************************************************************************************
//
// uf_after_oh_approval()
// 
// Validate and Approve Overheads AFUDC and WIP calculations
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

string  company, je_code, month, where_str, acct, loading_dw
longlong trans_id, acct_id, oh_id, wo_id, row_num, num_rows, num_gl_rows, num_clear_rows, &
		  clear_id, afc_id, ce_id, credit_acct, je_id, i
integer mo, yr, ret, clear_type, rtn
decimal {2} amount
////	added for company loop
longlong	c, num_companies
//	$$$ mtc 20090702
longlong je_method_id

//
//	loop over the lb, and react to the selected rows
//
f_pp_msgs(  "Creating Journal Entries for After Overheads")


//
//  They cannot do this twice ...
//
if i_nvo_wo_control.i_split_oh_afudc then
	if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "overhead_approval")) then
		f_pp_msgs("Approve Over. and AFUDC: After Approval - You have already approved the overheads and AFUDC for this month.")
		rollback;
		return false
	end if
end if		

// check if calc is done already
if isnull(i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1,'overhead_calculation')) then 
	f_pp_msgs("Approve Over. and AFUDC - The calculation must be performed first " &
				+ "in order to be approved.")
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	rollback;
	return false
end if

if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, "after_oh_approval")) then
	f_pp_msgs("Approve Over. and AFUDC - You have already approved the after overheads for this month.")
	rollback;
	return false
end if

f_pp_msgs(  "Month: " + string(i_nvo_wo_control.i_month_number) + &
	 " Company:  " + i_curr_comp_descr + " Comp Id: " + string(i_nvo_wo_control.i_company))

// Generate the General Ledger Transactions.

// create uo_ds_top
uo_ds_top dw_gl_transaction
dw_gl_transaction = CREATE uo_ds_top
dw_gl_transaction.Dataobject = 'dw_gl_transaction'
dw_gl_transaction.SetTransObject(sqlca)

uo_ds_top dw_afudc_gl_trans
dw_afudc_gl_trans = CREATE uo_ds_top
dw_afudc_gl_trans.Dataobject = 'dw_afudc_gl_trans'
dw_afudc_gl_trans.SetTransObject(sqlca)

uo_ds_top dw_clearing_wo_control
dw_clearing_wo_control = CREATE uo_ds_top
dw_clearing_wo_control.Dataobject = 'dw_clearing_wo_control'
dw_clearing_wo_control.SetTransObject(sqlca)

uo_ds_top dw_gl_charges
dw_gl_charges = CREATE uo_ds_top
dw_gl_charges.Dataobject = 'dw_gl_charges_afc'
dw_gl_charges.SetTransObject(sqlca)

num_clear_rows = dw_clearing_wo_control.RETRIEVE(i_nvo_wo_control.i_company)

f_pp_msgs("Creating GL Journal Entries (Overheads) ...")

dw_gl_charges.Dataobject = 'dw_gl_charges_oh'

dw_gl_charges.settransobject(sqlca)

where_str   = " and cwip_charge.status in " &
				+ "(select distinct clearing_id from clearing_wo_control where before_afudc_indicator = 0) " 

f_add_where_clause_ds(dw_gl_charges,"",where_str,false)

num_gl_rows = dw_gl_charges.RETRIEVE(i_nvo_wo_control.i_month_number, i_nvo_wo_control.i_company)

select gl_company_no into :company 
from company where company_id = :i_nvo_wo_control.i_company;

for i = 1 to num_gl_rows

	if mod(i, 50) = 0 then
		f_pp_msgs("Creating GL Journal Entries (Overheads) ... Finished " + &
			string(i) + " of " + string(num_gl_rows))
	end if

	dw_gl_transaction.insertrow(1)
		
	trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

	dw_gl_transaction.setitem(1,'gl_trans_id',    trans_id)
	dw_gl_transaction.setitem(1,'month',          i_nvo_wo_control.i_month)
	dw_gl_transaction.setitem(1,'company_number', company)

	dw_gl_transaction.setitem(1,'gl_status_id',1)

	amount = dw_gl_charges.getitemdecimal(i,'amount')
	
	ce_id    = dw_gl_charges.getitemnumber(i, 'cost_element_id')
	acct_id  = dw_gl_charges.getitemnumber(i, 'gl_account_id')

	// The Overhead Type is saved in the Status column of the charges	  
	oh_id = dw_gl_charges.getitemnumber(i, 'status')
	i_nvo_wo_control.i_work_order_number = dw_gl_charges.getitemstring(i, 'work_order_number')
	
	row_num = dw_clearing_wo_control.find("clearing_id = " + string(oh_id), &
														1, num_clear_rows)
	
	if row_num < 0 then
		f_pp_msgs( "ERROR: dw Find Error on dw_clearing_wo_control")
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if

		
	if row_num = 0 then
		f_pp_msgs( "Could not find the dw_clearing_wo_control record for clearing_id " + &
			"= " + string(oh_id))
		i_nvo_wo_control.i_afudc_error = true
		i_nvo_wo_control.i_no_summary = true
		rollback;
		return false
	end if		
	
	clear_type  = dw_clearing_wo_control.getitemnumber(row_num, 'clearing_indicator')
	credit_acct = dw_clearing_wo_control.getitemnumber(row_num, 'credit_gl_account')
	je_code     = dw_clearing_wo_control.getitemstring(row_num, 'clearing_je')
	
	je_code = trim(je_code)
	
	dw_gl_transaction.setitem(1,'gl_je_code', je_code)

	je_method_id = dw_gl_charges.getitemdecimal(i,'je_method_id')
	
	if clear_type = 1 then                         // clearing overhead
	
		acct = f_autogen_je_account(dw_gl_charges,i,2,	acct_id, je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if
		dw_gl_transaction.setitem(1,'gl_account', acct)

		if amount > 0 then
			dw_gl_transaction.setitem(1,'amount',                 amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		else
			dw_gl_transaction.setitem(1,'amount',                -amount)
			dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		end if
		
		 // store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))
		
		dw_gl_transaction.setitem(1,'amount_type',1)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',2)
		
	else	// loading overhead
		
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 1)
		
		acct = f_autogen_je_account(dw_gl_charges,i,3,	acct_id, je_method_id)
		//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
		if mid(acct,1,5) = 'ERROR' then
			f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
			rollback using sqlca;
			i_nvo_wo_control.i_afudc_error = true
			i_nvo_wo_control.i_no_summary = true
			return false
		end if
		dw_gl_transaction.setitem(1,'gl_account', acct)
		
		dw_gl_transaction.setitem(1,'amount_type',1)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		dw_gl_transaction.setitem(1,'trans_type',3)

		// Must also create credit offset transaction
		dw_gl_transaction.InsertRow(1)
		
		trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')

		dw_gl_transaction.setitem(1,'gl_trans_id',            trans_id)
		dw_gl_transaction.setitem(1,'month',                  i_nvo_wo_control.i_month)
		dw_gl_transaction.setitem(1,'company_number',         company)
		dw_gl_transaction.setitem(1,'gl_status_id',           1)
		dw_gl_transaction.setitem(1,'gl_je_code',             je_code)
		dw_gl_transaction.setitem(1,'amount',                 amount)
		dw_gl_transaction.setitem(1,'debit_credit_indicator', 0)
		
		// store oh_id
		dw_gl_transaction.setitem(1,'description', string(oh_id))
		
		dw_gl_transaction.setitem(1,'amount_type',1)
		dw_gl_transaction.setitem(1,'je_method_id',je_method_id)
		
		loading_dw = f_pp_system_control_company('Loading Datawindow', i_nvo_wo_control.i_company)
			
		if loading_dw <> 'charges' then
			
			acct = f_autogen_je_account(dw_clearing_wo_control,row_num, 7,	credit_acct, je_method_id)
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		else
			 acct = f_autogen_je_account(dw_gl_charges,i, 7,  credit_acct, je_method_id)  
			//// maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
			if mid(acct,1,5) = 'ERROR' then
				f_pp_msgs("Error occurred while generating the transaction's gl account.  Generated value : " + acct)
				rollback using sqlca;
				i_nvo_wo_control.i_afudc_error = true
				i_nvo_wo_control.i_no_summary = true
				return false
			end if
		end if
		 
		dw_gl_transaction.setitem(1,'gl_account', acct)
		dw_gl_transaction.setitem(1,'trans_type', 7)

	end if 
	
next// Rows

f_pp_msgs("Updating ...")

ret = dw_gl_transaction.Update()

if ret <> 1 then
	f_pp_msgs( "Error updating General Ledger Transactions. " + dw_gl_transaction.i_sqlca_sqlerrtext)
	rollback using sqlca;
	i_nvo_wo_control.i_afudc_error = true
	i_nvo_wo_control.i_no_summary = true
	return false
end if

if not i_nvo_wo_control.i_split_oh_afudc then
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_approval',today()) 
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'after_oh_approval', today())

	i_nvo_wo_control.of_updatedwnocommit( )
else
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'after_oh_approval', today())
	
	longlong before_ind, afudc_ind, after_ind
	
	select before_oh_indicator, afudc_indicator, after_oh_indicator
	into :before_ind, :afudc_ind, :after_ind
	from		afudc_oh_process_control
	where company_id = :i_nvo_wo_control.i_company
	;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs( "Error retrieving Indicators during date update : " + sqlca.sqlerrtext)
	end if	
	
	if isnull(before_ind) or isnull(afudc_ind) or isnull(after_ind) then
		f_pp_msgs( "Error retrieving Indicators from afudc_oh_process_control, dates not updated properly")
	end if
	
	date null_date
	SetNull(null_date)
	
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'overhead_approval', today())
	i_nvo_wo_control.i_ds_wo_control.setitem(1,'after_oh_approval', today())
	
	i_nvo_wo_control.of_updatedwnocommit( )
end if

return true
end function

public function boolean uf_retrieve_dates ();//**********************************************************************************************************************************************************
//
// uf_retrieve_dates()
// 
// The function emulates the wf_retrieve_dates from the w_wo_afudc_ovh_control window
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

i_nvo_wo_control.i_ds_wo_process_afudc_ovh.retrieve(i_nvo_wo_control.i_company,i_nvo_wo_control.i_month_number)

i_calc_ovh_before  = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'before_oh_calc'))
i_calc_ovh_after   = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'after_oh_calc'))
i_calc_afudc       = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'afudc_calc'))
i_appr_ovh_before  = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'before_oh_approval'))
i_appr_ovh_after   = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'after_oh_approval'))
i_appr_afudc       = string(i_nvo_wo_control.i_ds_wo_process_afudc_ovh.getitemdatetime(1, 'afudc_approval'))

return true
end function

public function boolean uf_wo_afudc_ovh_control_approve (boolean cbx_appr_ovh_before_checked, boolean cbx_appr_afudc_checked, boolean cbx_appr_ovh_after_checked, boolean cbx_appr_ovh_before_enabled, boolean cbx_appr_afudc_enabled);//**********************************************************************************************************************************************************
//
// uf_wo_afudc_ovh_control_approve()
// 
// Validate and Approve Overheads AFUDC and WIP calculations
// This function emulates the old workflow of the w_wo_afudc_ovh_control window in PowerPlan
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************

longlong c, num_companies
datetime process_date
 
if cbx_appr_ovh_before_checked = false and &
cbx_appr_afudc_checked = false and &
cbx_appr_ovh_after_checked = false then
  f_pp_msgs("ERROR: Please select an approval option ")
  return false
end if


// SEB: Adding company loop here and removing the loop from each of the calc functions
num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c = 1 to num_companies
		
	//the company is selected, update the rest of the window
	i_nvo_wo_control.of_companychanged(c,datetime(g_ssp_parms.date_arg[1]))
	
	i_curr_comp_descr = i_nvo_wo_control.i_company_descr[c]
	
	f_pp_msgs("Processing company " + i_curr_comp_descr + ", ID: " + string(i_nvo_wo_control.i_company))
	
	uf_retrieve_dates()

	if cbx_appr_ovh_before_checked = true then
		
		if isnull(i_calc_ovh_before) then
			f_pp_msgs("ERROR: Cannot Approve Before Ovh before it is Calculated")
			return false
		end if
		
		uf_before_oh_approval(datetime(g_ssp_parms.date_arg[1]))
	end if

	uf_retrieve_dates()
	
	if cbx_appr_afudc_checked = true then
		
		if isnull(i_appr_ovh_before) and cbx_appr_ovh_before_enabled = true then
			f_pp_msgs("ERROR: Cannot Approve AFUDC since Before Ovh is not Approved")
			return false
		end if
		
		if isnull(i_calc_afudc) then
			f_pp_msgs("ERROR: Cannot Approve AFUDC before it is Calculated")
			return false
		end if
		
		// setup the transiant CURRENT_AFUDC_VIEW by using the dw_cpr_act_month to write
		// the session ID to the CPR_ACT_MONTH table
		i_nvo_wo_control.of_currentmonth(i_nvo_wo_control.i_month)
		
		uf_afudc_real_approval(datetime(g_ssp_parms.date_arg[1]))
	end if

	uf_retrieve_dates()
	
	if cbx_appr_ovh_after_checked = true then
		
		if isnull(i_calc_ovh_after) then
			f_pp_msgs("ERROR: Cannot Approve After Ovh before it is Calculated")
			return false
		end if
		
		if isnull(i_appr_afudc) and cbx_appr_afudc_enabled = true then
			f_pp_msgs("ERROR: Cannot Approve After Ovh since Afudc is not a Approved")
			return false
		end if
		
		if isnull(i_appr_ovh_before) and cbx_appr_ovh_before_enabled = true then
			f_pp_msgs("ERROR: Cannot Approve After Ovh since Before Ovh is not Approved")
			return false
		end if
		
		uf_after_oh_approval(datetime(g_ssp_parms.date_arg[1]))
			
	end if

	commit;
next

f_pp_msgs("Approval. Overheads and AFUDC - Done!")

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version

choose case a_pbd_name
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

