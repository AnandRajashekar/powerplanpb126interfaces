HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_pp_verify_close from datawindow within w_datawindows
end type
type dw_cr_element_definitions from datawindow within w_datawindows
end type
type dw_gl_charges_wip_comp from datawindow within w_datawindows
end type
type dw_pp_interface_dates_check from datawindow within w_datawindows
end type
type dw_wo_process_afudc_ovh from datawindow within w_datawindows
end type
type dw_gl_charges_cpi from datawindow within w_datawindows
end type
type dw_cpr_act_month from datawindow within w_datawindows
end type
type dw_wo_interface_dates_all from datawindow within w_datawindows
end type
type dw_gl_charges_oh from datawindow within w_datawindows
end type
type dw_gl_charges_afc from datawindow within w_datawindows
end type
type dw_afudc_gl_trans from datawindow within w_datawindows
end type
type dw_gl_transaction from datawindow within w_datawindows
end type
type dw_clearing_wo_control from datawindow within w_datawindows
end type
type dw_wo_process_control from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
string tag = "w_datawindows"
integer width = 3168
integer height = 2880
boolean titlebar = true
string title = "w_datawindows"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_pp_verify_close dw_pp_verify_close
dw_cr_element_definitions dw_cr_element_definitions
dw_gl_charges_wip_comp dw_gl_charges_wip_comp
dw_pp_interface_dates_check dw_pp_interface_dates_check
dw_wo_process_afudc_ovh dw_wo_process_afudc_ovh
dw_gl_charges_cpi dw_gl_charges_cpi
dw_cpr_act_month dw_cpr_act_month
dw_wo_interface_dates_all dw_wo_interface_dates_all
dw_gl_charges_oh dw_gl_charges_oh
dw_gl_charges_afc dw_gl_charges_afc
dw_afudc_gl_trans dw_afudc_gl_trans
dw_gl_transaction dw_gl_transaction
dw_clearing_wo_control dw_clearing_wo_control
dw_wo_process_control dw_wo_process_control
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_pp_verify_close=create dw_pp_verify_close
this.dw_cr_element_definitions=create dw_cr_element_definitions
this.dw_gl_charges_wip_comp=create dw_gl_charges_wip_comp
this.dw_pp_interface_dates_check=create dw_pp_interface_dates_check
this.dw_wo_process_afudc_ovh=create dw_wo_process_afudc_ovh
this.dw_gl_charges_cpi=create dw_gl_charges_cpi
this.dw_cpr_act_month=create dw_cpr_act_month
this.dw_wo_interface_dates_all=create dw_wo_interface_dates_all
this.dw_gl_charges_oh=create dw_gl_charges_oh
this.dw_gl_charges_afc=create dw_gl_charges_afc
this.dw_afudc_gl_trans=create dw_afudc_gl_trans
this.dw_gl_transaction=create dw_gl_transaction
this.dw_clearing_wo_control=create dw_clearing_wo_control
this.dw_wo_process_control=create dw_wo_process_control
this.Control[]={this.dw_pp_verify_close,&
this.dw_cr_element_definitions,&
this.dw_gl_charges_wip_comp,&
this.dw_pp_interface_dates_check,&
this.dw_wo_process_afudc_ovh,&
this.dw_gl_charges_cpi,&
this.dw_cpr_act_month,&
this.dw_wo_interface_dates_all,&
this.dw_gl_charges_oh,&
this.dw_gl_charges_afc,&
this.dw_afudc_gl_trans,&
this.dw_gl_transaction,&
this.dw_clearing_wo_control,&
this.dw_wo_process_control}
end on

on w_datawindows.destroy
destroy(this.dw_pp_verify_close)
destroy(this.dw_cr_element_definitions)
destroy(this.dw_gl_charges_wip_comp)
destroy(this.dw_pp_interface_dates_check)
destroy(this.dw_wo_process_afudc_ovh)
destroy(this.dw_gl_charges_cpi)
destroy(this.dw_cpr_act_month)
destroy(this.dw_wo_interface_dates_all)
destroy(this.dw_gl_charges_oh)
destroy(this.dw_gl_charges_afc)
destroy(this.dw_afudc_gl_trans)
destroy(this.dw_gl_transaction)
destroy(this.dw_clearing_wo_control)
destroy(this.dw_wo_process_control)
end on

type dw_pp_verify_close from datawindow within w_datawindows
integer x = 846
integer y = 1428
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_pp_verify_close"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cr_element_definitions from datawindow within w_datawindows
integer x = 87
integer y = 1440
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_wip_comp from datawindow within w_datawindows
integer x = 2281
integer y = 956
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_gl_charges_wip_comp"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_interface_dates_check from datawindow within w_datawindows
integer x = 1559
integer y = 960
integer width = 686
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_afudc_ovh from datawindow within w_datawindows
integer x = 823
integer y = 956
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_wo_process_afudc_ovh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_cpi from datawindow within w_datawindows
integer x = 64
integer y = 956
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_gl_charges_cpi"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_act_month from datawindow within w_datawindows
integer x = 2277
integer y = 504
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_interface_dates_all from datawindow within w_datawindows
integer x = 2263
integer y = 52
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_wo_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_oh from datawindow within w_datawindows
integer x = 1518
integer y = 516
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_gl_charges_oh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_charges_afc from datawindow within w_datawindows
integer x = 786
integer y = 508
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_gl_charges_afc"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_afudc_gl_trans from datawindow within w_datawindows
integer x = 1486
integer y = 48
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_afudc_gl_trans"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_transaction from datawindow within w_datawindows
integer x = 55
integer y = 508
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_gl_transaction"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_clearing_wo_control from datawindow within w_datawindows
integer x = 759
integer y = 40
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_clearing_wo_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_control from datawindow within w_datawindows
integer x = 41
integer y = 36
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

