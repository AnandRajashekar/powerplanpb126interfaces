HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_5 from datawindow within w_datawindows
end type
type dw_4 from datawindow within w_datawindows
end type
type dw_3 from datawindow within w_datawindows
end type
type dw_2 from datawindow within w_datawindows
end type
type dw_1 from datawindow within w_datawindows
end type
type dw_wo_est_retires_106 from datawindow within w_datawindows
end type
type dw_wo_interface_dates_all from datawindow within w_datawindows
end type
type dw_pp_interface_dates_check from datawindow within w_datawindows
end type
type dw_wo_process_control from datawindow within w_datawindows
end type
type dw_wo_class_code_update_args from datawindow within w_datawindows
end type
type dw_wo_charges_to_buckets_106 from datawindow within w_datawindows
end type
type dw_wo_to_unitize_106 from datawindow within w_datawindows
end type
type dw_wo_to_unitize_101_late from datawindow within w_datawindows
end type
type dw_wo_to_unitize_101 from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer width = 2674
integer height = 3396
boolean titlebar = true
string title = "w_datawindows"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_5 dw_5
dw_4 dw_4
dw_3 dw_3
dw_2 dw_2
dw_1 dw_1
dw_wo_est_retires_106 dw_wo_est_retires_106
dw_wo_interface_dates_all dw_wo_interface_dates_all
dw_pp_interface_dates_check dw_pp_interface_dates_check
dw_wo_process_control dw_wo_process_control
dw_wo_class_code_update_args dw_wo_class_code_update_args
dw_wo_charges_to_buckets_106 dw_wo_charges_to_buckets_106
dw_wo_to_unitize_106 dw_wo_to_unitize_106
dw_wo_to_unitize_101_late dw_wo_to_unitize_101_late
dw_wo_to_unitize_101 dw_wo_to_unitize_101
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_5=create dw_5
this.dw_4=create dw_4
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_1=create dw_1
this.dw_wo_est_retires_106=create dw_wo_est_retires_106
this.dw_wo_interface_dates_all=create dw_wo_interface_dates_all
this.dw_pp_interface_dates_check=create dw_pp_interface_dates_check
this.dw_wo_process_control=create dw_wo_process_control
this.dw_wo_class_code_update_args=create dw_wo_class_code_update_args
this.dw_wo_charges_to_buckets_106=create dw_wo_charges_to_buckets_106
this.dw_wo_to_unitize_106=create dw_wo_to_unitize_106
this.dw_wo_to_unitize_101_late=create dw_wo_to_unitize_101_late
this.dw_wo_to_unitize_101=create dw_wo_to_unitize_101
this.Control[]={this.dw_5,&
this.dw_4,&
this.dw_3,&
this.dw_2,&
this.dw_1,&
this.dw_wo_est_retires_106,&
this.dw_wo_interface_dates_all,&
this.dw_pp_interface_dates_check,&
this.dw_wo_process_control,&
this.dw_wo_class_code_update_args,&
this.dw_wo_charges_to_buckets_106,&
this.dw_wo_to_unitize_106,&
this.dw_wo_to_unitize_101_late,&
this.dw_wo_to_unitize_101}
end on

on w_datawindows.destroy
destroy(this.dw_5)
destroy(this.dw_4)
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_1)
destroy(this.dw_wo_est_retires_106)
destroy(this.dw_wo_interface_dates_all)
destroy(this.dw_pp_interface_dates_check)
destroy(this.dw_wo_process_control)
destroy(this.dw_wo_class_code_update_args)
destroy(this.dw_wo_charges_to_buckets_106)
destroy(this.dw_wo_to_unitize_106)
destroy(this.dw_wo_to_unitize_101_late)
destroy(this.dw_wo_to_unitize_101)
end on

type dw_5 from datawindow within w_datawindows
integer x = 750
integer y = 1904
integer width = 686
integer height = 400
integer taborder = 100
string title = "none"
string dataobject = "dw_pend_trans_update"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_4 from datawindow within w_datawindows
integer x = 41
integer y = 1900
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_pend_basis"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_3 from datawindow within w_datawindows
integer x = 1477
integer y = 1492
integer width = 686
integer height = 400
integer taborder = 100
string title = "none"
string dataobject = "dw_sub_account_select"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_2 from datawindow within w_datawindows
integer x = 41
integer y = 1488
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_class_code_pending_trans"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_datawindows
integer x = 736
integer y = 1488
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_basis_labels"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_est_retires_106 from datawindow within w_datawindows
integer x = 32
integer y = 1076
integer width = 553
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "dw_wo_est_retires_106"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_interface_dates_all from datawindow within w_datawindows
integer x = 631
integer y = 728
integer width = 549
integer height = 312
integer taborder = 80
string title = "none"
string dataobject = "dw_wo_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_interface_dates_check from datawindow within w_datawindows
integer x = 32
integer y = 704
integer width = 549
integer height = 332
integer taborder = 70
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_control from datawindow within w_datawindows
integer x = 1202
integer y = 376
integer width = 549
integer height = 320
integer taborder = 60
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_class_code_update_args from datawindow within w_datawindows
integer x = 622
integer y = 368
integer width = 549
integer height = 320
integer taborder = 50
string title = "none"
string dataobject = "dw_wo_class_code_update_args"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_charges_to_buckets_106 from datawindow within w_datawindows
integer x = 32
integer y = 364
integer width = 549
integer height = 320
integer taborder = 40
string title = "none"
string dataobject = "dw_wo_charges_to_buckets_106"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_to_unitize_106 from datawindow within w_datawindows
integer x = 1198
integer y = 40
integer width = 549
integer height = 320
integer taborder = 30
string title = "none"
string dataobject = "dw_wo_to_unitize_106"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_to_unitize_101_late from datawindow within w_datawindows
integer x = 617
integer y = 32
integer width = 549
integer height = 320
integer taborder = 20
string title = "none"
string dataobject = "dw_wo_to_unitize_101_late"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_to_unitize_101 from datawindow within w_datawindows
integer x = 32
integer y = 32
integer width = 549
integer height = 320
integer taborder = 10
string title = "none"
string dataobject = "dw_wo_to_unitize_101"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

