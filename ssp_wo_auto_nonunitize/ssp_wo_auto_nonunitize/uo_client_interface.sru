HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
//Declaration of Instance Variables
boolean i_auto106
date i_old_month
longlong i_company_ids[]
string i_yes_no_late

nvo_wo_control i_nvo_wo_control
string i_exe_name = 'ssp_wo_auto_nonunitize.exe non'
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function boolean uf_auto_nonunitize_main ()
public function integer uf_auto_106 ()
public function integer uf_check_for_unapproved_charges (longlong a_company_id, longlong a_month_number)
public subroutine uf_check_option_array (longlong a_company_ids[], boolean a_option_values[], longlong a_option_id, string a_option_description)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

// Declare variables
longlong rtn_success, rtn_failure, rtn, month_number
string process_msg, title

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// instantiate any objects needed
uo_ds_top ds_users

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

//call constructor on i_nvo_wo_control
i_nvo_wo_control.of_constructor()

//Populate instance variables with arguments from SSP parms
i_company_ids = g_ssp_parms.long_arg
i_old_month = g_ssp_parms.date_arg[1]

// Set indicator to process auto non-unitize or failed non-unitization
if (upperbound(g_ssp_parms.boolean_arg) = 1) then
	i_auto106 = g_ssp_parms.boolean_arg[1]
else
	f_pp_msgs("ERROR: Boolean Arg was not populated; Interface cannot determine whether to Run Auto Non-Unitized or Non-Unitized Failed Unitization")
	return rtn_failure
end if

// Not allowing this functionality in the SSP
setnull(i_nvo_wo_control.auto_106_after_101)

// Set the late indicator for processing failed non unitization
if (upperbound(g_ssp_parms.string_arg2) = 1) then
	i_nvo_wo_control.i_yes_no_late = g_ssp_parms.string_arg2[1]
else
	setnull(i_nvo_wo_control.i_yes_no_late)
end if

// Previous Months to Process
if (upperbound(g_ssp_parms.long_arg2) = 1) then
	i_nvo_wo_control.i_late_charge_mos = g_ssp_parms.long_arg2[1]
else
	setnull(i_nvo_wo_control.i_late_charge_mos)
end if

//setup i_wo_control
rtn = i_nvo_wo_control.of_setupfromcompaniesandmonth(i_company_ids, i_old_month)
if rtn <> 1 then 
	f_pp_msgs("An error occurred when setting up the interface. Review logs for additional information.")
	return rtn_failure
end if

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if i_auto106 = true then
    i_exe_name = "ssp_wo_auto_nonunitize.exe non"
	title = "Automatic Non-Unitized - "
else
   i_exe_name = "ssp_wo_auto_nonunitize.exe failed"
	title = "Non-Unitized Failed Unitization - "	  
end if
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs(title + "There has been a concurrency error. Please check that processes are not currently running")
          return rtn_failure
end if 

// check if the multiple companies chosen can be processed  together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return rtn_failure
end if

// Process logic for auto non-unitize
if not uf_auto_nonunitize_main() then
	return rtn_failure
end if

i_nvo_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function boolean uf_auto_nonunitize_main ();//**********************************************************************************************************************************************************
//
// uf_auto_nonunitize_main()
// 
// This function performs the calculation for auto non-unitization
// 
// Parameters : None
// 
// Returns : (boolean) : true : There was no error
//                       false : There was an error
//
//**********************************************************************************************************************************************************
// Variables
datetime run_date, afudc_apprv_date, afudc_calc_date, accrual_apprv_date, accrual_calc_date, auto_close, auto_non_unitized
longlong rtn, counter, run,errors
string   yes_no, month_string, auto101_yes_no, late_opt, savedir, comp_descr, yes_no_late, fast_106
////	added for company loop
longlong	c, num_companies, process_id, pp_stat_id, i, check, company_id
boolean   isFirst
uo_ds_top ds_users
boolean vfy_users
string  sqls,  user_id, find_str, prior_months_to_run, late_charge_mos

longlong late_months, month_number, new_date

// reference data window
w_datawindows w_dw

if i_auto106 = true then
	f_pp_msgs("Automatic Non-Unitized Process Started")
else
	f_pp_msgs("Non-Unitized Failed Unitization Process Started")
end if
	
// Force a retrieve for the data stores
i_nvo_wo_control.of_companychanged(1, i_nvo_wo_control.i_month)

//maint-44823:  afudc, overhead, and accruals calc validations are inside uf_check_for_unapproved_charges()

if not i_auto106 then
	// they must have run 106 and 101
	auto_close = i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1, "auto_close")
	auto_non_unitized = i_nvo_wo_control.i_ds_wo_control.getitemdatetime(1, "auto_non_unitized")
	
	if  isnull(auto_close) then
			f_pp_msgs("Non-Unitized Failed Unitization - ERROR:  You have not Run Automatic Unitization.")
			return false
	end if
	
	if  isnull(auto_non_unitized) then
			f_pp_msgs("Non-Unitized Failed Unitization - ERROR:  You have not Run Automatic Non-Unitized")
			return false
	end if
end if

// Validate companies line up
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if

num_companies	= upperbound(i_company_ids)

if isnull(i_nvo_wo_control.i_yes_no_late) then
	for c=1 to num_companies
		//	it's selected, update the rest of the window
		i_nvo_wo_control.of_companychanged(c, i_nvo_wo_control.i_month)
		
		yes_no_late = upper(f_pp_system_control_company('AUTO101 - LATE CHARGE UNITIZATION', i_nvo_wo_control.i_company))
		if isnull(yes_no_late) then
			yes_no_late = upper(f_pp_system_control_company('AUTO101 - LATE CHARGE UNITIZATION', -1))
		end if
		
		if c > 1 and yes_no_late <> late_opt then
			f_pp_msgs("Cannot process multiple companies because 'AUTO101 - LATE CHARGE UNITIZATION'" + &
			" system control has different values")
			return false
		end if
		late_opt = yes_no_late
	next
	
	i_nvo_wo_control.i_yes_no_late = late_opt
end if

if not i_auto106 and upper(i_nvo_wo_control.i_yes_no_late) = 'YES' then
	// Check for month end option values first
	setNull(late_charge_mos)
	for c = 1 to num_companies
		//	it's selected, update the rest of the window
		i_nvo_wo_control.of_companychanged(c, i_nvo_wo_control.i_month)
		
		// look for Previous Months to Process (i_late_charge_mos) in a parameter first and secondly in the pp_month_end_options table
		// if not found in either place default it to 1
		if isnull(i_nvo_wo_control.i_late_charge_mos) then
			try
				late_charge_mos = f_pp_month_end_options(i_nvo_wo_control.i_company, g_process_id, 1)
			catch (exception e)
				f_pp_msgs("Could not find month end option value for 'Previous Months to Process' in pp_month_end_options.  Defaulting to '1'.")
				f_pp_msgs(e.text)
				late_charge_mos = "1"
			end try
		else
			late_charge_mos = string(i_nvo_wo_control.i_late_charge_mos)
		end if
		
		//	prior_wos_to_run, prior_months_to_run, prior_late_charge_options
		if c > 1 and prior_months_to_run <> late_charge_mos then
			f_pp_msgs("Cannot process multiple companies because month end option 'Previous Months to Process' are different for each company")
			return false
		end if
		
		prior_months_to_run = late_charge_mos
	next
	
	if not isnull(late_charge_mos) then 
		i_nvo_wo_control.i_late_charge_mos = longlong(late_charge_mos)
	end if
 
 	// Now verify the value
	if isNull(i_nvo_wo_control.i_late_charge_mos) then
		f_pp_msgs("ERROR: Late Month Option is set to 'YES', but the Late Charge Months variable is null!  Processing will stop.")
		return false
	end if
		
	late_months = i_nvo_wo_control.i_late_charge_mos
	
	if late_months < 0 then
		f_pp_msgs("ERROR: Invalid number of months entered for the Late Charge Months variable!  Processing will stop.")
		return false
	end if
		
	month_number = i_nvo_wo_control.i_month_number
	
	if late_months > 0 then
 
   	select to_number(to_char(  add_months(to_date(:month_number, 'yyyymm'), - :late_months) ,'yyyymm'))
    	into :new_date 
   	from dual;
   
   	i_nvo_wo_control.i_late_month_number = new_date 
 
  	else
   	select to_number(to_char(  add_months(to_date(:month_number, 'yyyymm'), - 1) ,'yyyymm'))
    	into :new_date 
   	from dual;
		
   	i_nvo_wo_control.i_late_month_number = new_date
  	end if
	
end if

//	Loop over the company listbox, and react to the selected rows
for c=1 to num_companies

	company_id = i_company_ids[c]
	i_nvo_wo_control.of_companychanged(c, i_nvo_wo_control.i_month)
	select description  into :comp_descr from company where company_id = :i_nvo_wo_control.i_company;
	
	if i_auto106 = true then
		pp_stat_id = f_pp_statistics_start("WO Monthly Close", "Automatic Non-unitized: " + comp_descr)
	else
		pp_stat_id = f_pp_statistics_start("WO Monthly Close", "Non-Unitized Failed Unitization: " + comp_descr)
	end if
	
	//	If 'AUTO106 - FAST NO LOOP' is not set to 'YES' then Auto 106 CANNOT be automated since the old code
	// was not ported to the SSP.
	fast_106 = upper(f_pp_system_control_company('AUTO106 - FAST NO LOOP', i_nvo_wo_control.i_company))
	
	if fast_106 <> 'YES' then
		f_pp_msgs("ERROR: 'AUTO106 - FAST NO LOOP' is not set to 'YES'.  This process cannot be run outside of the main application.")
		return false
	end if
	
	// Make sure the month isn't closed
	if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		return false
	end if
	
	//  Run Auto 106
	yes_no = "NO"
	yes_no = upper(f_pp_system_control_company('AUTOMATIC 106 IS TURNED ON', i_nvo_wo_control.i_company))
	if yes_no = '' then yes_no = "NO"
	if yes_no = "YES" then
		// Need to verify calc vs approval dates to prevent unapproved charges from closing
		rtn = uf_check_for_unapproved_charges(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number)
		
		if rtn < 0 then
			return false  
		end if
		
		// Call Auto Non-Unitization
		rtn = uf_auto_106()
		
		if rtn < 0 then
			f_pp_msgs( "ERROR: Processing Automatic 106 Closings"+ ' ' + string(now()))
			return false
		end if
	else
		// Notify user Auto 106 has been turned off
		f_pp_msgs( "The Automatic Non-Unitized process has been turned off in pp_system_control.")
		// Update date indicating that auto non-unitized has been run
		if i_auto106 = true then
			i_nvo_wo_control.i_ds_wo_control.setitem(1,'auto_non_unitized',today()) 
		else
			i_nvo_wo_control.i_ds_wo_control.setitem(1,'auto_non_unitized_after_101',today())
		end if
		
		i_nvo_wo_control.of_updatedw()	
		return true
	end if

	// Run Estimated Retirements
	f_pp_msgs( "Checking for Estimated Retirements for Month: " + string(i_nvo_wo_control.i_month_number) + &
	 				" Company:  " + i_nvo_wo_control.i_company_descr[c] + " Comp Id: " + string( i_nvo_wo_control.i_company)+ ' ' + string(now()))
	rtn = f_wo_estimated_retires(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month)
	if rtn <> 1 then 
		f_pp_msgs( "ERROR: Processing Estimated Retirements(106)"+ ' ' + string(now()))		
		return false
	end if
	
	// Update date indicating that auto non-unitized has been run
	if i_auto106 = true then
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'auto_non_unitized',today()) 
	else
		i_nvo_wo_control.i_ds_wo_control.setitem(1,'auto_non_unitized_after_101',today())
	end if
	
	i_nvo_wo_control.of_updatedw()	
	
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)

next
 
// Email users and log end of processing
i_nvo_wo_control.of_cleanup(5, 'email wo close: automatic non-unitized', 'Auto Non-Unitized')

return true
end function

public function integer uf_auto_106 ();//********************************************************************************************
//
//	Function:	uf_auto_106
//
//	Purpose:		Automatic 106 closings
//
//********************************************************************************************
 
longlong  rtn

//Create UserObject to process Fast 106.  There is a commit in this uo object.
//uo_wo_auto106_closing uo_wo_auto106_closing
uo_wo_auto106_closing = CREATE uo_wo_auto106_closing 

//need to know what is calling Auto106
if i_auto106 = true then 
	//regular 106	
	rtn = uo_wo_auto106_closing.of__main(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number, '106', i_nvo_wo_control.i_late_month_number)
else 
	// failed 101
	rtn = uo_wo_auto106_closing.of__main(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number, '101 FAILED', i_nvo_wo_control.i_late_month_number)
	
	if rtn < 0 then
		f_pp_msgs(string(now()) + ": HALT: errors encountered!") 
	else
		f_pp_msgs(string(now()) + ": Finished!")	
	end if
	
	// late 101 
	if i_nvo_wo_control.i_yes_no_late = 'YES' then
		rtn = uo_wo_auto106_closing.of__main(i_nvo_wo_control.i_company, i_nvo_wo_control.i_month_number, '101 LATE', i_nvo_wo_control.i_late_month_number)
		
		if rtn < 0 then
			f_pp_msgs(string(now()) + ": HALT: errors encountered!") 
		else
			f_pp_msgs(string(now()) + ": Finished!")	
		end if 
	end if
end if
	
DESTROY uo_wo_auto106_closing
return rtn

end function

public function integer uf_check_for_unapproved_charges (longlong a_company_id, longlong a_month_number);//////////////////////////////////////////////////////////////////////////////////////////////////////
////function name:  wf_check_for_unapproved_charges
////arguments:  a_company_id & a_month_number
////returns 1 for success; -1 for invalid data found
//////////////////////////////////////////////////////////////////////////////////////////////////////
//maint-44823:  accomodate for clients using split afudc vs overhead calc
longlong check_overhead_calculation, check_afudc_calc, check_accrual_calc, check_before_oh_calc, check_after_oh_calc, check_external_oh_calc 
longlong check_overhead_approval, check_afudc_approval, check_accrual_approval, check_before_oh_approval, check_after_oh_approval, check_external_oh_approval 
longlong powerplant_closed, cpr_closed, split_check, wo_accr_ct, cc_check, powerplant_closed_prior_month, company_cpr_control_check
string wo_accr_process_ans

 ////Need to verify calc vs approval dates to prevent unapproved charges from closing
 
 //first setnull() for all variables  
setnull(check_overhead_calculation) 
setnull(check_afudc_calc)
setnull(check_accrual_calc)
setnull(check_before_oh_calc)
setnull(check_after_oh_calc)
setnull(check_external_oh_calc)
setnull(check_overhead_approval)
setnull(check_afudc_approval)
setnull(check_accrual_approval)
setnull(check_before_oh_approval)
setnull(check_after_oh_approval)
setnull(check_external_oh_approval)
setnull(powerplant_closed)
setnull(cpr_closed)

//check if cpr is already closed
select to_number(to_char(cpr_closed,'yyyymm')) into :cpr_closed
from cpr_control 
where company_id = :a_company_id and to_number(to_char(accounting_month,'yyyymm')) = :a_month_number;

if not isnull(cpr_closed) then
	f_pp_msgs( "ERROR: The CPR is already closed for this month for company_id = " + string(a_company_id))
	return -1
end if  

//compare the wo_process_control dates for calc vs. approval
select  to_number(to_char(overhead_calculation,'yyyymm')), 
to_number(to_char(afudc_calc,  'yyyymm')), 
to_number(to_char(before_oh_calc, 'yyyymm')), 
to_number(to_char(after_oh_calc , 'yyyymm')), 
to_number(to_char(overhead_approval, 'yyyymm')), 
to_number(to_char(afudc_approval,  'yyyymm')), 
to_number(to_char(before_oh_approval,'yyyymm')),  
to_number(to_char(after_oh_approval ,'yyyymm')), 
to_number(to_char(accrual_calc ,'yyyymm')),
to_number(to_char(accrual_approval ,'yyyymm')),
to_number(to_char(external_oh_calc ,'yyyymm')),
to_number(to_char(external_oh_approval ,'yyyymm')) ,
to_number(to_char(powerplant_closed ,'yyyymm'))
into :check_overhead_calculation, :check_afudc_calc, :check_before_oh_calc, :check_after_oh_calc,   
	:check_overhead_approval, :check_afudc_approval,  :check_before_oh_approval, :check_after_oh_approval ,
	:check_accrual_calc, :check_accrual_approval, :check_external_oh_calc , :check_external_oh_approval, :powerplant_closed
from wo_process_control 
where company_id = :a_company_id and to_number(to_char(accounting_month,'yyyymm')) = :a_month_number;

if not isnull(powerplant_closed) then
	f_pp_msgs("ERROR: The WO Control is already closed for this month for company_id = " + string(a_company_id) )
	return -1
end if 

//if the company uses split afc/oh calculation and approvals, the "overhead_calculation" gets changed to null if one of the calcs get approved and there are none needing approval.
if not isnull(check_overhead_calculation) and isnull(check_overhead_approval) then
   f_pp_msgs("ERROR:  You have not Approved Overheads for company_id = " + string(a_company_id))
   return -1
end if

//Accruals calculation
wo_accr_process_ans = f_pp_system_control_company("Work Order Accrual Calculation", a_company_id)
if isnull(wo_accr_process_ans) or wo_accr_process_ans = "" then wo_accr_process_ans = 'no'
	
if wo_accr_process_ans = 'yes' then
	wo_accr_ct = long(f_pp_system_control_company("Work Order Accrual Charge Type", a_company_id))
	if not isnull(check_accrual_calc) and isnull(check_accrual_approval) then
		f_pp_msgs("ERROR:  You have not Approved Accruals for company_id = " + string(a_company_id))
		return -1
	end if
else
	wo_accr_ct = 0	
end if

if not isnull(check_external_oh_calc) and isnull(check_external_oh_approval) then
   f_pp_msgs("ERROR:  You have not Approved External Overheads for company_id = " + string(a_company_id))
   return -1
end if

//need to check if company is setup for split afudc/overhead processing
split_check = 0 
select count(*) into :split_check 
from afudc_oh_process_control
where company_id = :a_company_id;

//determine if afudc calculated charges are in the CWIP_CHARGE table
cc_check = 0 
select count(*) into :cc_check 
from cwip_charge 
where company_id = :a_company_id  
and month_number = :a_month_number 
and status = 1;

if split_check > 0 then	
	//company is setup for split afudc processing, the afudc_approval date should be filled in
	if cc_check > 0 and isnull(check_afudc_approval) then
		f_pp_msgs("ERROR: There are calculated Afudc charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id)) 
		return -1 
	end if
else	
	//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
	if cc_check > 0 and isnull(check_overhead_approval) then
		f_pp_msgs("ERROR: There are Afudc calculated charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
end if 

//determine if before overheads calculated charges are in the CWIP_CHARGE table
cc_check = 0 
select count(*) into :cc_check 
from cwip_charge 
where company_id = :a_company_id  
and month_number = :a_month_number 
and status in (select clearing_id from clearing_wo_control where before_afudc_indicator = 1)
and charge_type_id <> :wo_accr_ct;	

if split_check > 0 then	
	//company is setup for split afudc processing, the before_oh_approval date should be filled in
	if cc_check > 0 and isnull(check_before_oh_approval) then
		f_pp_msgs("ERROR: There are calculated Before OH charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
else	
	//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
	if cc_check > 0 and isnull(check_overhead_approval) then
		f_pp_msgs("ERROR: There are Before OH calculated charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
end if 

//determine if After Overheads calculated charges are in the CWIP_CHARGE table
cc_check = 0 
select count(*) into :cc_check 
from cwip_charge 
where company_id = :a_company_id  
and month_number = :a_month_number 
and status in (select clearing_id from clearing_wo_control where before_afudc_indicator = 0)
and charge_type_id <> :wo_accr_ct;	

if split_check > 0 then	
	//company is setup for split afudc processing, the after_oh_approval date should be filled in
	if cc_check > 0 and isnull(check_after_oh_approval) then
		f_pp_msgs( "ERROR: There are calculated After OH charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
else	
	//company is setup for regular combined afudc/ovh processing, the general overhead_approval date should be filled in
	if cc_check > 0 and isnull(check_overhead_approval) then
		f_pp_msgs("ERROR: There are After OH calculated charges found in Proj Mang that have not been approved for company_id = " + string(a_company_id))
		return -1 
	end if
end if 

//check if prior month is still open
setnull(powerplant_closed_prior_month)
select  to_number(to_char(powerplant_closed,'yyyymm')) 
into :powerplant_closed_prior_month
from wo_process_control where company_id = :a_company_id and to_char(add_months(accounting_month, 1),'yyyymm')  = :a_month_number;

if isnull(powerplant_closed_prior_month) then	
	//check if the calc has been run and not yet approved for prior month
	setnull(check_overhead_calculation)
	setnull(check_overhead_approval)
	select  to_number(to_char(overhead_calculation,'yyyymm')), to_number(to_char(overhead_approval, 'yyyymm')) 
	into :check_overhead_calculation, :check_overhead_approval
	from wo_process_control where company_id = :a_company_id and to_char(add_months(accounting_month, 1),'yyyymm')  = :a_month_number;

	if not isnull(check_overhead_calculation) and isnull(check_overhead_approval) then		
		f_pp_msgs("ERROR: There are unapproved charges for the prior month for company_id = " + string(a_company_id))
		return -1
	end if
end if 

company_cpr_control_check = 0
select count(1) into :company_cpr_control_check
from cpr_control 
where company_id = :a_company_id
and to_number(to_char(accounting_month,'yyyymm')) = :a_month_number;

if company_cpr_control_check = 0 then
	f_pp_msgs("ERROR: The CPR Control table is not set up for this Company and Month." )
	return -1
end if 

return 1

end function

public subroutine uf_check_option_array (longlong a_company_ids[], boolean a_option_values[], longlong a_option_id, string a_option_description);boolean check_month_end_option
longlong i

for i = 1 to upperbound(a_company_ids)

	//make sure boolean_arg is populated
	if upperbound(a_option_values) < i then 
		check_month_end_option = true
	elseif isnull(a_option_values[i]) then //having this as a separate ELSEIF is important. If we used an OR in the IF clause, an array out of bounds exception occurs.
		check_month_end_option = true
	else 
		check_month_end_option = false
	end if
	
	if check_month_end_option = true then 
		f_pp_msgs("Checking month end option for '" + a_option_description + "'")	
		try
//			if lower(f_pp_month_end_options(a_company_ids[i], g_process_id, 1)) = 'yes' then  //option_id 1 = 'Continue if GL Reconciliation has not been completed?'
//				f_pp_msgs("'" + a_option_description + "' set to Yes.")	
//				a_option_values[i] = false
//			else 
//				f_pp_msgs("'" + a_option_description + "' set to No.")	
//				a_option_values[i] = true
//			end if
		catch (exception e)
			f_pp_msgs("Could not find value for '" + a_option_description + "' month end option. Defaulting to 'No'.")
			a_option_values[i] = true	
		end try
	end if 
	
next
end subroutine

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppprojct_custom_version nvo_ppprojct_custom_version

choose case a_pbd_name
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

