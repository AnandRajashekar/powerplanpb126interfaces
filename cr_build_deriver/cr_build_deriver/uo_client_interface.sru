HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string	i_exe_name = 'cr_build_deriver.exe'
end variables

forward prototypes
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();string sqls
longlong i, num_rows, rtn, wo_id, last_wo, revision
double amount

//  CHECK THE DEBUG VARIABLE.
setnull(g_debug)
select upper(trim(control_value)) into :g_debug from cr_system_control
 where upper(trim(control_name)) = 'CR BUILD DERIVER - DEBUG';
 
If g_debug <> "YES" Then g_debug = "NO"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** Calling custom function f_cr_build_deriver_custom(BEGIN)" )

//  call the custom function
rtn = f_cr_build_deriver_custom('BEGIN')

if rtn < 0 then 
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: During f_cr_build_deriver_custom (BEGIN): " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//	Delete from cr_deriver_control where derivations are turned off
f_pp_msgs("Delete from cr_deriver_control where derivations are turned off at " + string(now()))
delete from cr_deriver_control cdc
	where exists 
		(select work_order_id
		from work_order_account woa
		where cdc.work_order_id = woa.work_order_id
		and woa.cr_deriver_type_estimate is null)
	and type in 
		(select cr_derivation_type
		from cr_deriver_wo_link_control);
		
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Delete from cr_deriver_control: " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

f_pp_msgs("Analyzing wo_estimate at " + string(now()))
sqlca.analyze_table('wo_estimate')

// Find work orders that we need to build deriver records for.  Look for the last time this interface successfully ran,
//	and get the list of work orders that have been updated since then.
// ### 6907:  Added "or" statement so records got picked up if no records in cr_deriver_control exists.
// PP-43824 Move the nvl inside the subselect
f_pp_msgs(" ")
f_pp_msgs("Retrieving list of work orders to update derivation records at " + string(now()))
sqls = &
	"	select work_order_id, revision, sum(amount) amount " + &
	"	from wo_estimate a " + &
	"	where revision = (select max(revision) from wo_estimate b where a.work_order_id = b.work_order_id) " + &
	"	and exists (select 1 from work_order_account b, cr_deriver_wo_link_control ctrl where a.work_order_id = b.work_order_id and b.cr_deriver_type_estimate = ctrl.cr_derivation_type and lower(trim(project_basis)) = 'unit estimate') " + &
	"	and a.time_stamp > (select nvl(max(time_stamp),'01-JAN-2000') from cr_deriver_control cdc where cdc.work_order_id = a.work_order_id) " + &
	"	group by work_order_id, revision " + & 
	"	order by work_order_id asc, revision desc"

if g_debug = "YES" then f_pp_msgs("***DEBUG*** " + sqls  + '~r~n' )

uo_ds_top ds_wos
ds_wos = create uo_ds_top
f_create_dynamic_ds(ds_wos,'grid',sqls,sqlca,true)

uo_wo_cr_derivations wo_cr_derivations
wo_cr_derivations = create uo_wo_cr_derivations

num_rows = ds_wos.rowcount()

if g_debug = "YES" then f_pp_msgs("***DEBUG*** Work Order count" + string(num_rows) )

last_wo = 0
for i = 1 to num_rows
	if mod(i,100) = 0 or i = num_rows or i = 1 then
		f_pp_msgs("Process work order " + string(i) + " of " + string(num_rows) + " at " + string(now()))
	end if
	
	wo_id = ds_wos.getitemnumber(i,'work_order_id')
	revision = ds_wos.getitemnumber(i,'revision')
	amount = ds_wos.getitemnumber(i,'amount')
	
	if g_debug = "YES" then f_pp_msgs("***DEBUG***  wo_id, revision, amount = " + string(wo_id) + ", " + string(revision) + ", " + string(amount) )
	
	wo_cr_derivations.i_revision = revision
	rtn = wo_cr_derivations.uf_build_wo_cr_derivations(wo_id, 'unit estimate')

	if rtn <> 0 then
		f_pp_msgs(" ")
		f_pp_msgs (	"ERROR in CR Derivations - " + wo_cr_derivations.i_error)
		if not isnull(wo_cr_derivations.i_sqls) and  wo_cr_derivations.i_sqls <> "" then f_pp_msgs("SQL: " + wo_cr_derivations.i_sqls)
		if not isnull(wo_cr_derivations.i_error_location) and  wo_cr_derivations.i_error_location <> "" then f_pp_msgs("Error Location: " + wo_cr_derivations.i_error_location)
		if not isnull(wo_id) and  wo_id <> 0 then f_pp_msgs("Work Order ID: " + string(wo_id))
		if not isnull(revision) and  revision <> 0 then f_pp_msgs("Revision: " + string(revision))
		f_pp_msgs(" ")
		rollback;
		// PP-41232 Continue processing with other work orders even though this wo failed
		//return -1
	end if
	
	commit;
next

if g_debug = "YES" then f_pp_msgs("***DEBUG*** Calling custom function f_cr_build_deriver_custom(END) " )

//  call the custom function
rtn = f_cr_build_deriver_custom('END')

if rtn < 0 then 
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: During f_cr_build_deriver_custom (END): " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

return 0
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_build_deriver_custom_version nvo_cr_build_deriver_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_build_deriver_custom.pbd'
		return nvo_cr_build_deriver_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

