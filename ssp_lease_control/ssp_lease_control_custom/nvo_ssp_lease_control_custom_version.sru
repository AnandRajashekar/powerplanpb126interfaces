forward
global type nvo_ssp_lease_control_custom_version from nonvisualobject
end type
end forward

global type nvo_ssp_lease_control_custom_version from nonvisualobject autoinstantiate
end type

type variables
string custom_version = '2016.1.2.0'
end variables

on nvo_ssp_lease_control_custom_version.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_ssp_lease_control_custom_version.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

