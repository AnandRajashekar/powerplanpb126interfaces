HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_lease_control.exe'
end variables

forward prototypes
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
public subroutine uf_reset_msg_order ()
public function longlong uf_build_forecast (longlong a_revision_id, string a_errmsg)
public function longlong uf_copy_forecast_ilrs (longlong a_revision, ref string a_status)
public function boolean uf_set_db_msg_order ()
public function longlong uf_convert_forecast (longlong a_revision_id, ref string a_msg)
public function longlong uf_buildsummaryforecast (ref string a_msg)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure, ll_i, rtn
string msg, sqls, lock_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

nvo_ls_mec nvo_mec
nvo_mec = create nvo_ls_mec

if not uf_set_db_msg_order() then return rtn_failure

// Make sure the string arg is populated
if upperbound(g_ssp_parms.string_arg) = 1 then
	choose case g_ssp_parms.string_arg[1]
		case "Calc Payments"
			if upperbound(g_ssp_parms.long_arg) > 0 then
				// Lock the process
				rtn = 0
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					if not f_pp_set_process("CALC_PMTS|" + string(g_ssp_parms.long_arg[ll_i]) + ";","CALC_PMTS|" + string(g_ssp_parms.long_arg[ll_i]) + ";",lock_msg) then
						msg = msg + lock_msg + "~r~n"
						rtn = -1
					end if
				next
				
				if rtn = 0 then
					// Calc Payments
					rtn=nvo_mec.of_calcPayments(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
				end if
					
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					// Release the lock
					if not f_pp_release_process("CALC_PMTS|" + string(g_ssp_parms.long_arg[ll_i]) + ";", lock_msg) then
						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
					end if
				next
			else
				msg = "An incorrect number of long_arg parameters was provided. The Company parameter (g_ssp_parms.long_arg) must have at least one Company in the array!"
				rtn = -1
			end if
		case "Approve Payments"			
			if upperbound(g_ssp_parms.long_arg) > 0 then
				// Lock the process
				rtn = 0
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					if not f_pp_set_process("APPROVE_PMTS|" + string(g_ssp_parms.long_arg[ll_i]) + ";","APPROVE_PMTS|" + string(g_ssp_parms.long_arg[ll_i]) + ";",lock_msg) then
						msg = msg + lock_msg + "~r~n"
						rtn = -1
					end if
				next
				
				if rtn = 0 then
					// Approve Payments
					rtn=nvo_mec.of_approvePayments(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
				end if
					
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					// Release the lock
					if not f_pp_release_process("APPROVE_PMTS|" + string(g_ssp_parms.long_arg[ll_i]) + ";", lock_msg) then
						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
					end if
				next
			else
				msg = "An incorrect number of long_arg parameters was provided. The Company parameter (g_ssp_parms.long_arg) must have at least one Company in the array!"
				rtn = -1
			end if
		case "Approve Depr"
			if upperbound(g_ssp_parms.long_arg) > 0 then
				// Lock the process
				rtn = 0
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					if not f_pp_set_process("APPROVE_DEPR|" + string(g_ssp_parms.long_arg[ll_i]) + ";","APPROVE_DEPR|" + string(g_ssp_parms.long_arg[ll_i]) + ";",lock_msg) then
						msg = msg + lock_msg + "~r~n"
						rtn = -1
					end if
				next 
				
				if rtn = 0 then
					// Approve Depr
					rtn=nvo_mec.of_approveDepr(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
				end if
					
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					// Release the lock
					if not f_pp_release_process("APPROVE_DEPR|" + string(g_ssp_parms.long_arg[ll_i]) + ";", lock_msg) then
						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
					end if
				next 
			else
				msg = "An incorrect number of long_arg parameters was provided. The Company parameter (g_ssp_parms.long_arg) must have at least one Company in the array!"
				rtn = -1
			end if
		case "Calc Accruals"						
			if upperbound(g_ssp_parms.long_arg) > 0 then
				// Lock the process
				rtn = 0
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					if not f_pp_set_process("CALC_ACCR|" + string(g_ssp_parms.long_arg[ll_i]) + ";","CALC_ACCR|" + string(g_ssp_parms.long_arg[ll_i]) + ";",lock_msg) then
						msg = msg + lock_msg +"~r~n"
						rtn = -1
					end if
				next
				
				if rtn = 0 then
					// Calc Accruals
					rtn=nvo_mec.of_calcAccruals(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
				end if
						
				// Release the lock
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					if not f_pp_release_process("CALC_ACCR|" + string(g_ssp_parms.long_arg[ll_i]) + ";", lock_msg) then
						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
					end if
				next 
			else
				msg = "An incorrect number of long_arg parameters was provided. The Company parameter (g_ssp_parms.long_arg) must have at least one Company in the array!"
				rtn = -1
			end if
		case "Approve Accruals"			
			if upperbound(g_ssp_parms.long_arg) > 0 then
				// Lock the process
				rtn = 0
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					if not f_pp_set_process("APPROVE_ACCR|" + string(g_ssp_parms.long_arg[ll_i]) + ";","APPROVE_ACCR|" + string(g_ssp_parms.long_arg[ll_i]) + ";",lock_msg) then
						msg = msg + lock_msg + "~r~n"
						rtn = -1
					end if
				next 
				
				if rtn = 0 then
					// Approve Accruals
					rtn=nvo_mec.of_approveAccruals(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
				end if
					
				// Release the lock
				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
					if not f_pp_release_process("APPROVE_ACCR|" + string(g_ssp_parms.long_arg[ll_i]) + ";", lock_msg) then
						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
					end if
				next
			else
				msg = "An incorrect number of long_arg parameters was provided. The Company parameter (g_ssp_parms.long_arg) must have at least one Company in the array!"
				rtn = -1
			end if
//		case "Approve Currency Gain/Loss"
//			if upperbound(g_ssp_parms.long_arg) > 0 and upperbound(g_ssp_parms.long_arg2) = 1 then
//				//Lock the process
//				rtn = 0
//				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
//					if not f_pp_set_process("APPROVE_CURRENCY_GAIN_LOSS|" + string(g_ssp_parms.long_arg[ll_i]) + "|" + string(g_ssp_parms.long_arg2[1]) + ";", &
//																	"CURRENCY_GAIN_LOSS|" + string(g_ssp_parms.long_arg[ll_i]) + "|" + string(g_ssp_parms.long_arg2[1]) +  ";", lock_msg) &
//					then
//						msg = msg + lock_msg + "~r~n"
//						rtn = -1
//					end if
//				next
//				
//				if rtn = 0 then
//					//Approve Currency Gain/Loss
//					rtn = nvo_mec.of_approvecurrencygainloss(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, g_ssp_parms.long_arg2[1], msg)
//				end if
//						
//				//Release the lock
//				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
//					if not f_pp_release_process("APPROVE_CURRENCY_GAIN_LOSS|" + string(g_ssp_parms.long_arg[ll_i]) + "|" + string(g_ssp_parms.long_arg2[1])  + ";", lock_msg) then
//						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
//					end if
//				next
//			else
//				msg = "An incorrect number of long_arg parameters was provided. The Company parameter (g_ssp_parms.long_arg) " + &
//								"must have at least one Company in the array and the Set of Books parameter (g_ssp_parms.long_arg2[1] must be the only Set of Books populated!"
//				rtn = -1
//			end if
		case "LAM Closed" 
			rtn=nvo_mec.of_closelease(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
		case "Open Next"
			rtn = nvo_mec.of_openNext(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
//		case "Auto Retirements"
//			if upperbound(g_ssp_parms.long_arg) > 0 then
//				// Lock the process
//				rtn = 0
//				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
//					if not f_pp_set_process("AUTO_RETIRE|" + string(g_ssp_parms.long_arg[ll_i]) + ";","AUTO_RETIRE|" + string(g_ssp_parms.long_arg[ll_i]) + ";",lock_msg) then
//						msg = msg + lock_msg + "~r~n"
//						rtn = -1
//					end if
//				next
//				
//				if rtn = 0 then
//					// Auto Retirements
//					rtn = nvo_mec.of_autoretire(g_ssp_parms.date_arg[1], g_ssp_parms.long_arg, msg)
//				end if
//					
//				// Release the lock
//				for ll_i = 1 to upperBound(g_ssp_parms.long_arg)
//					if not f_pp_release_process("AUTO_RETIRE|" + string(g_ssp_parms.long_arg[ll_i]) + ";", lock_msg) then
//						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
//					end if
//				next
//			else
//				msg = "An incorrect number of long_arg parameters was provided. The Company parameter (g_ssp_parms.long_arg) must have at least one Company in the array!"
//				rtn = -1
//			end if
		case "Build Forecast"
			if upperbound(g_ssp_parms.long_arg) = 1 then
				// Lock the process
				if not f_pp_set_process("BUILD_FCST|" + string(g_ssp_parms.long_arg[1]) + ";","BUILD_FCST|" + string(g_ssp_parms.long_arg[1]) + ";",lock_msg) then
					msg = lock_msg
					rtn = -1
				else
					// Build Forecast
					rtn = uf_build_forecast(g_ssp_parms.long_arg[1], msg)
					
					// Release the lock
					if not f_pp_release_process("BUILD_FCST|" + string(g_ssp_parms.long_arg[1]) + ";", lock_msg) then
						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
					end if
				end if
			else
				msg = "An incorrect number of long_arg parameters was provided. The Revision parameter (g_ssp_parms.long_arg[1]) is the only expected long_arg paramter!"
				rtn = -1
			end if
		case "Copy Forecast ILRS"
			// Lock the process
			if not f_pp_set_process("COPY_FCST_ILRS|" + string(g_ssp_parms.long_arg[1]) + ";","COPY_FCST_ILRS|" + string(g_ssp_parms.long_arg[1]) + ";",lock_msg) then
				msg = lock_msg
				rtn = -1
			else
				// Copy ILRs
				rtn = uf_copy_forecast_ilrs(g_ssp_parms.long_arg[1], msg)
				
				// Release the lock
				if not f_pp_release_process("COPY_FCST_ILRS|" + string(g_ssp_parms.long_arg[1]) + ";", lock_msg) then
					f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
				end if
			end if
		case "Convert Forecast"
			if upperbound(g_ssp_parms.long_arg) = 1 then
				// Lock the process
				if not f_pp_set_process("CONVERT_FCST|" + string(g_ssp_parms.long_arg[1]) + ";","CONVERT_FCST|" + string(g_ssp_parms.long_arg[1]) + ";",lock_msg) then
					msg = lock_msg
					rtn = -1
				else
					// Convert Forecast: Place holder for PP-45366
					rtn = uf_convert_forecast(g_ssp_parms.long_arg[1], msg)
					
					// Release the lock
					if not f_pp_release_process("CONVERT_FCST|" + string(g_ssp_parms.long_arg[1]) + ";", lock_msg) then
						f_pp_msgs("ERROR: Releasing process lock: " + lock_msg)
					end if
				end if
			else
				msg = "An incorrect number of long_arg parameters was provided. The Revision parameter (g_ssp_parms.long_arg[1]) is the only expected long_arg paramter!"
				rtn = -1
			end if
	end choose 
else
	msg = "An incorrect number of string_arg parameters was provided. The Process parameter (g_ssp_parms.string_arg[1]) is the only expected string_arg parameter!"
	g_ssp_parms.string_arg[1] = "NO PROCESS NAME ENTERED"
	rtn = -1
end if

uf_reset_msg_order()

if rtn = -1 then 
	f_pp_msgs("Error executing process " + g_ssp_parms.string_arg[1] + ". " + msg + " Please review the logs for errors.")
	return rtn_failure
else 
	f_pp_msgs("Process " + g_ssp_parms.string_arg[1] + " successfully executed!")
end if 


return rtn_success
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

return ""
end function

public subroutine uf_reset_msg_order ();select nvl(max(msg_order),0) + 1
into :g_msg_order
from pp_processes_messages
where process_id = :g_process_id
	and occurrence_id = :g_occurrence_id;
	
return 
end subroutine

public function longlong uf_build_forecast (longlong a_revision_id, string a_errmsg);string rtns, msg =  " "

//check if this is a Summary forecast

If g_ssp_parms.long_arg2[1] = 2 then
	
	DECLARE F_BUILD_SUMMARY_FORECAST PROCEDURE FOR
	PKG_LEASE_SCHEDULE.F_BUILD_SUMMARY_FORECAST (:a_revision_id, :g_ssp_parms.decimal_arg[1] );
	
	EXECUTE F_BUILD_SUMMARY_FORECAST;
	if sqlca.sqlcode <> 0 then
		a_errMsg = "ERROR: Creating ORACLE function F_BUILD_SUMMARY_FORECAST" + sqlca.SQLErrText
		rollback;
		return -1
	end if
	
	FETCH F_BUILD_SUMMARY_FORECAST INTO :rtns;
	if sqlca.sqlcode <> 0 then
		a_errMsg = "ERROR: Executing ORACLE function F_BUILD_SUMMARY_FORECAST" + sqlca.SQLErrText
		rollback;
		return -1
	end if
	
	CLOSE F_BUILD_SUMMARY_FORECAST;
	if rtns <> 'OK' then
		a_errMsg = rtns
		rollback;
		return -1
	end if

end if 

DECLARE F_BUILD_FORECAST PROCEDURE FOR
	PKG_LEASE_SCHEDULE.F_BUILD_FORECAST (:a_revision_id);
	
EXECUTE F_BUILD_FORECAST;

if sqlca.sqlcode <> 0 then
	a_errMsg = "ERROR: Creating ORACLE function F_BUILD_FORECAST" + sqlca.SQLErrText
	rollback;
	return -1
end if

FETCH F_BUILD_FORECAST INTO :rtns;

if sqlca.sqlcode <> 0 then
	a_errMsg = "ERROR: Executing ORACLE function F_BUILD_FORECAST" + sqlca.SQLErrText
	rollback;
	return -1
end if

CLOSE F_BUILD_FORECAST;

if rtns <> 'OK' then
	a_errMsg = rtns
	rollback;
	return -1
else
	commit;
end if

return 1
end function

public function longlong uf_copy_forecast_ilrs (longlong a_revision, ref string a_status);uo_ds_top ds_ilr
longlong i, ilr_id, curr_revision, rtn
ds_ilr = CREATE uo_ds_top
string sqls, rtns, status

// Delete out previously created forecasts
status = "Deleting ls_asset_schedule for existing revisions for this forecast"
f_pp_msgs(status)
delete from ls_asset_schedule
where revision = :a_revision;

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	
status="Deleting ls_ilr_schedule for existing revisions for this forecast"
f_pp_msgs(status)
DELETE FROM LS_ILR_SCHEDULE
WHERE REVISION = :A_REVISION;

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	
  
status="Deleting ls_depr_forecast for existing revisions for this forecast"
f_pp_msgs(status)
delete from ls_depr_forecast
where revision = :a_revision;

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	
  
status="Deleting ls_ilr_asset_map for existing revisions for this forecast"
f_pp_msgs(status)
delete from ls_ilr_asset_map
where revision = :a_revision; 

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	
  
status="Deleting ls_ilr_options for existing revisions for this forecast"
f_pp_msgs(status)
delete from ls_ilr_options
where revision = :a_revision;

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	

status="Deleting ls_ilr_payment_term for existing revisions for this forecast"
f_pp_msgs(status)
delete from ls_ilr_payment_term
where revision = :a_revision;

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	

status="Deleting ls_ilr_approval for existing revisions for this forecast"
f_pp_msgs(status)
delete from ls_ilr_approval
where revision = :a_revision;

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	

status="Deleting ls_ilr_amounts_set_of_books for existing revisions for this forecast"
f_pp_msgs(status)
delete from ls_ilr_amounts_set_of_books
where revision = :a_revision;

if sqlca.sqlcode<0 then 
	a_status = status + ": " + sqlca.sqlerrtext
	f_pp_msgs(a_status)
	return -1
end if 
	

if not uf_set_db_msg_order() then return -1

// Next, loop over the ILRs and call PKG_LEASE_ILR.F_COPYREVISION()
sqls  = "select m.ilr_id, i.current_revision from ls_forecast_ilr_master m, ls_ilr i"
sqls += " where m.revision = " + string(a_revision)
sqls += "	and m.ilr_id = i.ilr_id"

rtns = f_create_dynamic_ds(ds_ilr,'grid',sqls,sqlca,true)

if rtns <> 'OK' then
	a_status = rtns
	return -1
end if

for i = 1 to ds_ilr.rowcount()
	ilr_id = ds_ilr.GetItemNumber(i, 1)
	curr_revision = ds_ilr.GetItemNumber(i, 2)
	
	rtn = sqlca.f_copyRevision(ilr_id, curr_revision, a_revision)
	
	if sqlca.sqlcode < 0 then 
		a_status = "ERROR: Executing oracle function F_COPYREVISION: " + sqlca.sqlerrtext
		return -1
	end if
	
	if rtn = -1 then 
		a_status = "Error in Oracle Function F_COPYREVISION: " + F_PP_NVL(sqlca.sqlerrtext, "")
		RETURN -1
	END IF 
	
next

//WMD update our to cap type to match the existing one on the ILR revision if it was null 
update ls_forecast_ilr_master a
set to_cap_type_id = (select lease_cap_type_id from ls_ilr_options where ilr_id = a.ilr_id and revision = :a_revision)
where to_cap_type_id is null 
	and revision = :a_revision;
	
	if sqlca.sqlcode < 0 then 
		a_status = "ERROR: Updating null To Cap Type ID's : " + sqlca.sqlerrtext
		return -1
	end if
	
update ls_ilr_options a
set lease_cap_type_id = (select to_cap_type_id from ls_forecast_ilr_master where ilr_id = a.ilr_id and revision = :a_revision)
where revision = :a_revision
	and (ilr_id, revision) in (select ilr_id, revision from ls_forecast_ilr_master where revision = :a_revision and to_cap_type_id <> a.lease_cap_type_id);

	if sqlca.sqlcode < 0 then 
		a_status = "ERROR: Updating lease cap type on ILR options : " + sqlca.sqlerrtext
		return -1
	end if

commit;
	
return 1
end function

public function boolean uf_set_db_msg_order ();string sqls

sqls = "begin " +&
"pkg_pp_log.g_process_id:= " + string(g_process_id)+";" + "~r~n" +&
"pkg_pp_log.g_occurrence_id:= " + string(g_occurrence_id)+";" + "~r~n" +&
"pkg_pp_log.g_msg_order:= " + string(g_msg_order)+";" + "~r~n" +&
"end;" 

execute immediate :sqls;

if sqlca.sqlcode <0 then 
	f_pp_msgs(sqlca.sqlerrtext)
	f_pp_msgs(sqls)
	return false
end if 

return true
end function

public function longlong uf_convert_forecast (longlong a_revision_id, ref string a_msg);string rtns

//DECLARE PKG_LEASE_SCHEDULE.F_CONVERT_FORECAST 

DECLARE F_CONVERT_FORECAST PROCEDURE FOR
	PKG_LEASE_SCHEDULE.F_CONVERT_FORECAST (:a_revision_id );
	
	EXECUTE F_CONVERT_FORECAST;
	if sqlca.sqlcode <> 0 then
		a_Msg = "ERROR: Creating ORACLE function F_CONVERT_FORECAST" + sqlca.SQLErrText
		rollback;
		return -1
	end if
	
	FETCH F_CONVERT_FORECAST INTO :rtns;
	if sqlca.sqlcode <> 0 then
		a_Msg = "ERROR: Executing ORACLE function F_CONVERT_FORECAST" + sqlca.SQLErrText
		rollback;
		return -1
	end if
	
	CLOSE F_CONVERT_FORECAST;
	if rtns <> 'OK' then
		a_Msg = rtns
		rollback;
		return -1
	end if

commit;

return 1
end function

public function longlong uf_buildsummaryforecast (ref string a_msg);// This function builds summary forecast for a revison

longlong l_revision, l_ilrs[], l_rowcount, indx, l_ilr_id, l_company_id, l_to_cap_type_id, l_pre_payment_sw
uo_ds_top lds
string sqls, rtn

l_revision = g_ssp_parms.long_arg[1] 
l_ilrs = g_ssp_parms.long_arg3 // list of ilrs 

lds = create uo_ds_top
sqls = "select to_cap_type_id, pre_payment_sw, ilr_id, company_id " + "~r~n" 
sqls += "from (" + "~r~n" 
sqls += "select ilr.ilr_id, a.to_cap_type_id, ll.pre_payment_sw, ilr.company_id, row_number() over(partition by a.to_cap_type_id, ll.pre_payment_sw, ilr.company_id order by ilr.ilr_id) the_row " + "~r~n" 
sqls += "from ls_forecast_ilr_master a, ls_ilr ilr, ls_lease ll " + "~r~n" 
sqls += "where a.revision = " + string(l_revision) + "~r~n" 
sqls +=   "and a.ilr_id = ilr.ilr_id " + "~r~n" 
sqls +=   "and ilr.lease_id = ll.lease_id) " + "~r~n" 
sqls += "where the_row =1 " 

rtn = f_create_dynamic_ds(lds, "grid", sqls, sqlca, true)
if rtn <> "OK" then
	a_msg =  rtn
	return -1
end if

l_rowcount = lds.RowCount()

for indx = 1 to l_rowcount
	
	l_ilr_id = lds.GetItemNumber(indx, "ilr_id")
	l_company_id = lds.GetItemNumber(indx, "company_id")
	l_to_cap_type_id = lds.GetItemNumber(indx, "to_cap_type_id")
	l_pre_payment_sw = lds.GetItemNumber(indx, "pre_payment_sw")
	
    //Merge into ls_ilr_payment_term the total payment amounts by month for each grouping
    sqls = "merge into ls_ilr_payment_term a using ( ~r~n" 
    sqls+= "       with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000)  ~r~n" 
    sqls += "          select " + string(l_ilr_id)  + " as ilr_id, " + string(l_revision) + " as revision, ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1) as month,  ~r~n"  
    sqls += "             sum(case when mod(N.THE_ROW, DECODE(ilrpt.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =  ~r~n" 
    sqls += "                        DECODE(ilrpt.PAYMENT_FREQ_ID, 4, 0, ll.pre_payment_sw) then  ~r~n" 
    sqls += "                    ilrpt.PAID_AMOUNT ~r~n" 
    sqls += "                   else ~r~n" 
    sqls += "                    0 ~r~n" 
    sqls += "                end) as PAID_AMOUNT, ~r~n" 
    sqls += "           (select max(payment_term_id) from ls_ilr_payment_term where ilr_id = " + string( l_ilr_id) + " and revision = " + string(l_revision) + " ) as max_payment_term_id, ~r~n" 
    sqls += "           row_number() over(order by ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1)) adder ~r~n" 
    sqls += "        from ls_forecast_ilr_master a, ls_ilr_payment_term ilrpt, ls_lease ll, ls_ilr ilr, n ~r~n" 
    sqls += "            where a.ilr_id = ilrpt.ilr_id ~r~n" 
    sqls += "              and a.revision = ilrpt.revision ~r~n" 
    sqls += "              and a.to_cap_type_id = " + string(l_to_cap_type_id) + " ~r~n" 
    sqls += "              and a.ilr_id = ilr.ilr_id ~r~n" 
    sqls += "              and ilr.company_id =" + string(l_company_id) + " ~r~n" 
    sqls += "              and ilr.lease_id = ll.lease_id ~r~n" 
    sqls += "              and ll.pre_payment_sw = " + string(l_pre_payment_sw) + " ~r~n" 
    sqls += "              and N.THE_ROW <= ilrpt.NUMBER_OF_TERMS * DECODE(ilrpt.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) ~r~n" 
    sqls += "              and ilrpt.payment_term_type_id = 2 ~r~n" 
    sqls += "        group by " + string( l_ilr_id) + " , " + string(l_revision) + ", ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1) ~r~n" 
    sqls += "        ) b ~r~n" 
    sqls += "    on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.payment_term_date = b.month) ~r~n" 
    sqls += "        when matched then update set payment_freq_id = 4 /* monthly */, ~r~n" 
    sqls += "                             number_of_terms = 1, ~r~n" 
    sqls += "                             paid_amount = b.paid_amount, ~r~n" 
    sqls += "                             contingent_amount = 0 ~r~n" 
    sqls += "        when not matched then insert (ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, paid_amount) ~r~n" 
    sqls += "            values (b.ilr_id, b.revision, b.max_payment_term_id + adder, 2, b.month, 4 /* monthly */, 1, b.paid_amount)  "
            
	IF not isNull(sqls) then
		execute immediate :sqls;
	else
		a_msg = 'Null string being passed to execute immediate - merge into.'
		return -1
	end if
	
	if sqlca.sqlCode <> 0 then
		a_msg = sqlca.sqlErrText
		return -1
	end if
	
	// Insert a $0 row for any months in the middle of the payment term that may not have had a payment
	
	sqls = " insert into ls_ilr_payment_term ~r~n "
	sqls += " (ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, paid_amount) ~r~n "
	sqls += " select " + string( l_ilr_id) + ", ~r~n "
	sqls +=  string(l_revision) + ", ~r~n" 
	sqls += "        max_payment_term + rownum, ~r~n "
	sqls += "        2, ~r~n "
	sqls += "        month, ~r~n "
	sqls += "        4, ~r~n "
	sqls += "        1, ~r~n "
	sqls += "        0 ~r~n "
	sqls += " from        ~r~n "
	sqls += "  (with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000) ~r~n "
	sqls += " select add_months(min_date, the_row) month, max_payment_term ~r~n "
	sqls += " from ~r~n "
	sqls += " (select min(payment_term_date) min_date, max(payment_term_date) max_date, max(payment_term_id) max_payment_term ~r~n "
	sqls += " from ls_ilr_payment_term ilrpt ~r~n "
	sqls += " where ilr_id = " + string( l_ilr_id) + " ~r~n "
	sqls += "   and revision = " + string(l_revision) + " ), n ~r~n "
	sqls += " where n.the_row < months_between(max_date, min_date)) a ~r~n "
	sqls += " where not exists (select 1 from ls_ilr_payment_term where ilr_id = " + string( l_ilr_id) + " and revision = " + string(l_revision) + " and payment_term_date = a.month) "

	IF not isNull(sqls) then
		execute immediate :sqls;
	else
		a_msg = 'Null string being passed to execute immediate - insert into.'
		return -1
	end if
	
	if sqlca.sqlCode <> 0 then
		a_msg = sqlca.sqlErrText
		return -1
	end if
	
	//Re-Sort the ilr_payment_term table: 
	
	sqls = " merge into ls_ilr_payment_term a using( ~r~n "
	sqls += " select row_number() over(partition by ilr_id, revision order by payment_term_date) the_row, a.* ~r~n "
	sqls += " From ls_ilr_payment_term a ~r~n "
	sqls += " where ilr_id =  " + string( l_ilr_id) + " ~r~n "
	sqls += " and revision = " + string(l_revision) + " ~r~n "
	sqls += " ) b ~r~n "
	sqls += " on (a.ilr_id = b.ilr_id and a.payment_term_date = b.payment_term_date and a.revision = b.revision) ~r~n "
	sqls += " when matched then update set payment_term_id = b.the_row ~r~n "
	
	IF not isNull(sqls) then
		execute immediate :sqls;
	else
		a_msg = 'Null string being passed to execute immediate - merge into.'
		return -1
	end if
	
	if sqlca.sqlCode <> 0 then
		a_msg = sqlca.sqlErrText
		return -1
	end if
	
	//Delete out the payment term records for the other ILR's that have now been consolidated into another ILR: 
	
    sqls = "delete from ls_ilr_payment_term ~r~n "
    sqls+= "where ilr_id <> " + string( l_ilr_id) + " ~r~n "
    sqls+= "and (ilr_id, revision) in ~r~n "
    sqls+= "(select a.ilr_id, a.revision ~r~n "
    sqls+= "from ls_forecast_ilr_master a, ls_ilr ilr, ls_lease ll ~r~n "
    sqls+= "where a.ilr_id = ilr.ilr_id ~r~n "
    sqls+= "and ilr.lease_id = ll.lease_id ~r~n "
    sqls+= "and ll.pre_payment_sw = " + string(l_pre_payment_sw) + " ~r~n "
    sqls+= "and a.to_cap_type_id = " + string(l_to_cap_type_id) + " ~r~n "
    sqls+= "and a.revision = " + string(l_revision) + " ~r~n "
    sqls+= "and ilr.company_id = " + string(l_company_id) + " )"
	 
	IF not isNull(sqls) then
		execute immediate :sqls;
	else
		a_msg = 'Null string being passed to execute immediate - delete from.'
		return -1
	end if
	
	if sqlca.sqlCode <> 0 then
		a_msg = sqlca.sqlErrText
		return -1
	end if
	 
next

return 1
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

