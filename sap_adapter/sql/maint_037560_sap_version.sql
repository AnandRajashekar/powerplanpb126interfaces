update pp_processes
set version = '6.0.0.1'
where executable_file in ('cr_sap_master_data.exe', 'cr_sap_txn_interface.exe')
;

commit
;