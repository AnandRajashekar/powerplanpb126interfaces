--* Multi-Currency Control table
create table CR_SAP_POSTING_MC_CONTROL (
	POSTING_ID number(22,0),
	COMPANY varchar2(4),
	TIME_STAMP date,
	USER_ID varchar2(18),
	MULTI_CURRENCY number(1,0)
);

alter table CR_SAP_POSTING_MC_CONTROL
add constraint CR_SAP_POSTING_MC_CONTROL_PK primary key (POSTING_ID, COMPANY);

alter table CR_SAP_POSTING_MC_CONTROL
add constraint CR_SAP_POSTING_MC_CONTROL_FK1
foreign key (POSTING_ID)
references CR_SAP_POSTING_CONTROL (POSTING_ID);



--* Multi-Currency Currency table
create table CR_SAP_POSTING_MC_CURR (
	POSTING_ID number(22,0),
	COMPANY varchar2(4),
	CURRENCY_ISO varchar2(3),
	TIME_STAMP date,
	USER_ID varchar2(18),
	CURR_TYPE varchar2(2),
	CURRENCY varchar2(5)
);

alter table CR_SAP_POSTING_MC_CURR
add constraint CR_SAP_POSTING_MC_CURR_PK primary key (POSTING_ID, COMPANY, CURRENCY_ISO);

alter table CR_SAP_POSTING_MC_CURR
add constraint CR_SAP_POSTING_MC_CURR_FK1
foreign key (POSTING_ID, COMPANY)
references CR_SAP_POSTING_MC_CONTROL (POSTING_ID, COMPANY);



--* Comments
comment on table CR_SAP_POSTING_MC_CONTROL is 'A table to keep track of which companies use multi currency for each CR posting.';
comment on column CR_SAP_POSTING_MC_CONTROL.POSTING_ID is 'System-assigned identifier of particular CR Posting.';
comment on column CR_SAP_POSTING_MC_CONTROL.COMPANY is 'A 4 character company identifier that references the SAP company ID.';
comment on column CR_SAP_POSTING_MC_CONTROL.MULTI_CURRENCY is 'A 1 or 0 value signifying whether or not this company uses multiple currencies for this posting.';

comment on table CR_SAP_POSTING_MC_CURR is 'A list of currencies used for each company and posting combination.';
comment on column CR_SAP_POSTING_MC_CURR.POSTING_ID is 'System-assigned identifier of particular CR Posting.';
comment on column CR_SAP_POSTING_MC_CURR.COMPANY is 'A 4 character company identifier that references the SAP company ID.';
comment on column CR_SAP_POSTING_MC_CURR.CURRENCY_ISO is 'A 3 character ISO currency code. Ex: USD, EUR, etc.';
comment on column CR_SAP_POSTING_MC_CURR.CURR_TYPE is 'A 2 character currency type from SAP.';
comment on column CR_SAP_POSTING_MC_CURR.CURRENCY is 'A 5 character currency key (CUKY) from SAP.';