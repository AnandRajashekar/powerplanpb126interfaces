alter table cr_sap_posting_doc_control add (multiple_currencies number(22,0));

/*INSERT INTO cr_sap_gl_to_cr_col_map VALUES (
	'ACCDOC',
	'MC_GROUP_ID',
	'GL ID',
	1);

update cr_sap_gl_to_cr_col_map
	set cr_column_name = 'CASE WHEN TO_NUMBER(TO_CHAR(SYSDATE,''YYYYMM'')) > MONTH_NUMBER THEN TO_CHAR(TRUNC(ADD_MONTHS(TO_DATE(TO_CHAR(MONTH_NUMBER),''YYYYMM''),1),''MONTH'')-1,''YYYYMMDD'') WHEN TO_NUMBER(TO_CHAR(SYSDATE,''YYYYMM'')) < MONTH_NUMBER THEN TO_CHAR(TRUNC(TO_DATE(TO_CHAR(MONTH_NUMBER),''YYYYMM''),''MONTH''),''YYYYMMDD'') ELSE TO_CHAR(SYSDATE,''YYYYMMDD'') END'
 where gl_column_name in ('POSTGDATE', 'PSTNG_DATE');
*/