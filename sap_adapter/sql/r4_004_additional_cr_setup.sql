-- Additional CR Setup that is client specific and the statements below MUST be reviewed and updated

-- Validations - Suspense Accounting
update cr_system_control
	set control_value = 'No'
 where control_id = 99;

-- Enable Validations - Custom
update cr_system_control
	set control_value = 'No'
 where control_id = 108;

-- Enable Validations - ME
update cr_system_control
	set control_value = 'Yes'
 where control_id = 78;

-- Enable Validations - Month Number
update cr_system_control
	set control_value = 'Yes'
 where control_id = 91;

-- Enable Validations - Custom - MANJE
update cr_system_control
	set control_value = 'No'
 where control_id = 124;

-- Enable Manual Validations - Combo
update cr_system_control
	set control_value = 'No'
 where control_id = 131;

-- Enable Validations-BDG - Combo
update cr_system_control
	set control_value = 'No'
 where control_id = 186;

-- Enable Validations-BDG - ME
update cr_system_control
	set control_value = 'No'
 where control_id = 185;
commit;

-- Structures - Fast Build
update cr_system_control
	set control_value = 'Yes'
 where control_id = 225;
commit;

-- CR_STRUCTURES
insert into cr_structures (
	structure_id, description, element_id, structure_type, company_value)
values (
	2, 'SAP - Work Breakdown Structures', 11, 'Reporting', null);
insert into cr_structures (
	structure_id, description, element_id, structure_type, company_value)
values (
	3, 'SAP - Profit Centers', 6, 'Reporting', null);
insert into cr_structures (
	structure_id, description, element_id, structure_type, company_value)
values (
	4, 'SAP - Cost Centers', 2, 'Reporting', null);
insert into cr_structures (
	structure_id, description, element_id, structure_type, company_value)
values (
	5, 'SAP - Cost Elements', 3, 'Reporting', null);
commit;

-- Custom CR System Control Entries
insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	'10000', 'SAP Client (MANDT) Value', '010', 
	'The SAP client value used for pulling in master element and transactional data.');

insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	'10001', 'SAP Chart of Accounts (KTOPL) Value', '(''1000'')', 
	'The SAP chart of accounts value(s) used for pulling in master element and transactional data.');

insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	'10002', 'SAP Controlling Area (KOKRS) Value', '(''1000'')', 
	'The SAP chart of accounts value(s) used for pulling in master element and transactional data.');

insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	'10003', 'Default Cost Element Structure', 'No', 
	'Build the default cost element structure in the validations interface.');

insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	'10004', 'SAP Currency Code (WAERS) Value', '(''USD'')', 
	'The SAP currency code value(s) used for pulling in master element and transactional data.');

insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	'10005', 'SAP Language Code (SPRAS) Value', '(''E'')', 
	'The SAP language code value(s) used for pulling in master element and transactional data.');

insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	'10006', 'SAP Tax Procedure (KALSM) Value', '(''TAXUSJ'')', 
	'The SAP tax procedure value(s) used for pulling in master element and transactional data.');

insert into cr_system_control (
	control_id, control_name, control_value, long_description)
values (
	10007, 	'SAP Order Status (ASTNR) Value', '(''10'',''20'',''30'',''40'',''50'')',
	'The SAP order status value(s) used for pulling in master element and transactional data.');

commit;

-- Hide Certain Elements
/* For example:
-- Assignment
update cr_elements
	set hide = 1
 where element_id = 8;

-- Object Number
update cr_elements
	set hide = 1
 where element_id = 15;*/


-- Validation Exclusion
/* For example:
-- Assignment
insert into cr_validation_exclusion (
	table_name, element_id)
values (
	'*', 8);

-- Object Number
insert into cr_validation_exclusion (
	table_name, element_id)
values (
	'*', 15);*/

commit;

-- PP_PROCESSES
insert into pp_processes (
	process_id, description, long_description, executable_file, version, allow_concurrent)
select max(process_id) + 1,
		 'CR - SAP Master Data',
		 'CR - SAP Master Data',
		 'cr_sap_master_data.exe',
		 '1.0.0.1',
		 0
  from pp_processes;

insert into pp_processes (
	process_id, description, long_description, executable_file, version, allow_concurrent)
select max(process_id) + 1,
		 'CR SAP MD Connector',
		 'CR SAP MD Connector',
		 null,
		 null,
		 0
  from pp_processes;

insert into pp_processes (
	process_id, description, long_description, executable_file, version, allow_concurrent)
select max(process_id) + 1,
		 'CR SAP Transaction Importer',
		 'CR SAP Transaction Importer',
		 null,
		 null,
		 0
  from pp_processes;

insert into pp_processes (
	process_id, description, long_description, executable_file, version, allow_concurrent)
select max(process_id) + 1,
		 'CR SAP Txn Interface',
		 'CR SAP Txn Interface',
		 'cr_sap_txn_interface.exe',
		 '1.0.0.1',
		 0
  from pp_processes;

insert into pp_processes (
	process_id, description, long_description, executable_file, version, allow_concurrent)
select max(process_id) + 1,
		 'CR SAP POSTING BAPI CALL',
		 'CR SAP POSTING BAPI CALL',
		 null,
		 null,
		 0
  from pp_processes;
commit;

INSERT INTO cr_interfaces VALUES (
	11,
	'CR SAP TXN Interface - AP',
	'Accounts Payable',
	'N/A',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL);
INSERT INTO cr_interfaces VALUES (
	12,
	'CR SAP TXN Interface - AR',
	'Accounts Receivable',
	'N/A',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL);
INSERT INTO cr_interfaces VALUES (
	13,
	'CR SAP TXN Interface - GL',
	'General Ledger',
	'N/A',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL);
INSERT INTO cr_interfaces VALUES (
	14,
	'CR SAP TXN Interface - INV',
	'Inventory',
	'N/A',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL);
INSERT INTO cr_interfaces VALUES (
	15,
	'CR SAP TXN Interface - LABOR',
	'Labor',
	'N/A',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL);

commit;