// This script is included for clients on versions of PowerPlan 10.3.5 or older.

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010798_cr.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.6.0   11/12/2012 Joseph King    Point Release
||============================================================================
*/

/*
   drop table pwrplant.cr_data_mover_module_join;
   drop table pwrplant.cr_data_mover_module_column;
   drop table pwrplant.cr_data_mover_module_table;
   drop table pwrplant.cr_data_mover_module;
*/
alter table CR_DATA_MOVER_MAP_COLUMNS add FROM_FUNCTION varchar2(30);
comment on COLUMN CR_DATA_MOVER_MAP_COLUMNS.FROM_FUNCTION is 'Optional function to be used on the "From Column" such as sum, count, max, min, or avg.';

create table CR_DATA_MOVER_MODULE
(
 MODULE     varchar2(30) not null,
 USER_ID    varchar2(18),
 TIME_STAMP date
);

alter table CR_DATA_MOVER_MODULE
   add constraint CR_DATA_MOVER_MODULE_PK
       primary key (MODULE)
       using index tablespace PWRPLANT_IDX;

comment on table  CR_DATA_MOVER_MODULE            is '(C) [03] The CR Data Mover Module table is the master list of modules to be used in the CR Data Mover when extracting data.';
comment on column CR_DATA_MOVER_MODULE.MODULE     is 'The name of the module.';
comment on column CR_DATA_MOVER_MODULE.USER_ID    is 'Standard system-assigned user id used for audit purposes.';
comment on column CR_DATA_MOVER_MODULE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

create table CR_DATA_MOVER_MODULE_TABLE
(
 MODULE        varchar2(30) not null,
 TABLE_NAME    varchar2(30) not null,
 DB_TABLE_NAME varchar2(30),
 USER_ID       varchar2(18),
 TIME_STAMP    date
);

alter table CR_DATA_MOVER_MODULE_TABLE
   add constraint CR_DATA_MOVER_MODULE_TABLE_PK
       primary key (MODULE, TABLE_NAME)
       using index tablespace PWRPLANT_IDX;

comment on table  CR_DATA_MOVER_MODULE_TABLE               is '(C) [03] The CR Data Mover Module Table table is the master list of tables for each modules to be used in the CR Data Mover when extracting data.';
comment on column CR_DATA_MOVER_MODULE_TABLE.MODULE        is 'The name of the module.';
comment on column CR_DATA_MOVER_MODULE_TABLE.TABLE_NAME    is 'The alias that will be used for the table in the from clause.';
comment on column CR_DATA_MOVER_MODULE_TABLE.DB_TABLE_NAME is 'The name of the actual database table that will be used in the from clause.';
comment on column CR_DATA_MOVER_MODULE_TABLE.USER_ID       is 'Standard system-assigned user id used for audit purposes.';
comment on column CR_DATA_MOVER_MODULE_TABLE.TIME_STAMP    is 'Standard system-assigned timestamp used for audit purposes.';

create table CR_DATA_MOVER_MODULE_COLUMN
(
 MODULE      varchar2(30) not null,
 DESCRIPTION varchar2(30) not null,
 TABLE_NAME  varchar2(30) not null,
 SQL         varchar2(4000) not null,
 USER_ID     varchar2(18),
 TIME_STAMP  date
);

alter table CR_DATA_MOVER_MODULE_COLUMN
   add constraint CR_DATA_MOVER_MODULE_COLUMN_PK
       primary key (MODULE, DESCRIPTION)
       using index tablespace PWRPLANT_IDX; /* Table does not need to be part of PK -- description should be unique across all tables */

comment on table  CR_DATA_MOVER_MODULE_COLUMN             is '(C) [03] The CR Data Mover Module Table table is the master list of tables for each modules to be used in the CR Data Mover when extracting data.';
comment on column CR_DATA_MOVER_MODULE_COLUMN.MODULE      is 'The name of the module.';
comment on column CR_DATA_MOVER_MODULE_COLUMN.DESCRIPTION is 'The description of the value being selected and what will be selectable via the mapping screens.';
comment on column CR_DATA_MOVER_MODULE_COLUMN.TABLE_NAME  is 'The alias of the table from CR Data Mover Module Table that the data will be pulled.';
comment on column CR_DATA_MOVER_MODULE_COLUMN.SQL         is 'The SQL used to retrieve the appropriate value.  The SQL can be direct columns, functions of columns, or scalar subselects.';
comment on column CR_DATA_MOVER_MODULE_COLUMN.USER_ID     is 'Standard system-assigned user id used for audit purposes.';
comment on column CR_DATA_MOVER_MODULE_COLUMN.TIME_STAMP  is 'Standard system-assigned timestamp used for audit purposes.';

create table CR_DATA_MOVER_MODULE_JOIN
(
 MODULE            varchar2(30) not null,
 TABLE_NAME        varchar2(30) not null,
 TABLE_NAME_PARENT varchar2(30),
 JOIN_STRING       varchar2(4000),
 USER_ID           varchar2(18),
 TIME_STAMP        date
);

alter table CR_DATA_MOVER_MODULE_JOIN
   add constraint CR_DATA_MOVER_MODULE_JOIN_PK
       primary key (MODULE, TABLE_NAME)
       using index tablespace PWRPLANT_IDX;

comment on table  CR_DATA_MOVER_MODULE_JOIN                   is '(C) [03] The CR Data Mover Module Join table is the master list of joins between the tables for each modules to be used in the CR Data Mover when extracting data.';
comment on column CR_DATA_MOVER_MODULE_JOIN.MODULE            is 'The name of the module.';
comment on column CR_DATA_MOVER_MODULE_JOIN.TABLE_NAME        is 'The alias of the table from CR Data Mover Module Table that is being joined.';
comment on column CR_DATA_MOVER_MODULE_JOIN.TABLE_NAME_PARENT is 'The alias of the parent table from CR Data Mover Module Table that will be joined.  If the table is the primary table, such as work_order_control, table_name_parent can be left null.';
comment on column CR_DATA_MOVER_MODULE_JOIN.JOIN_STRING       is 'The SQL used in the where clause to join the table name and its parent.  If the table is the primary table, such as work_order_control, table_name_parent can be left null.';
comment on column CR_DATA_MOVER_MODULE_JOIN.USER_ID           is 'Standard system-assigned user id used for audit purposes.';
comment on column CR_DATA_MOVER_MODULE_JOIN.TIME_STAMP        is 'Standard system-assigned timestamp used for audit purposes.';

alter table CR_DATA_MOVER_MODULE_TABLE
   add constraint CR_DATA_MOVER_MODULE_TABLEFK1
       foreign key (MODULE)
       references cr_data_mover_module (MODULE);

alter table CR_DATA_MOVER_MODULE_COLUMN
   add constraint CR_DATA_MOVER_MODULE_COLUMNFK1
       foreign key (MODULE,TABLE_NAME)
       references CR_DATA_MOVER_MODULE_TABLE (MODULE, TABLE_NAME);

alter table CR_DATA_MOVER_MODULE_JOIN
   add constraint CR_DATA_MOVER_MODULE_JOINFK1
       foreign key (MODULE, TABLE_NAME)
       references CR_DATA_MOVER_MODULE_TABLE (MODULE,TABLE_NAME);

alter table CR_DATA_MOVER_MODULE_JOIN
   add constraint CR_DATA_MOVER_MODULE_JOINFK2
       foreign key (MODULE, TABLE_NAME_PARENT)
       references CR_DATA_MOVER_MODULE_TABLE (MODULE, TABLE_NAME);

alter table CR_DATA_MOVER add TARGET_COLUMNS varchar2(2000);
comment on column CR_DATA_MOVER.TARGET_COLUMNS is 'List of columns that will be updated / inserted into on the target table.  If blank, all columns will be assumed.';

update CR_DATA_MOVER
   set TARGET_COLUMNS = '<Mappings / Filters>'
 where TABLE_NAME = '<Mappings / Filters>';
commit;

-- * New external field on WO Status
alter table WORK_ORDER_STATUS add EXTERNAL_WORK_ORDER_STATUS varchar2(35);
comment on column WORK_ORDER_STATUS.EXTERNAL_WORK_ORDER_STATUS is 'Translation of the PowerPlan Work Order Status to the value used by external systems.';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('external_work_order_status', 'work_order_status', 'e', 'External Work Order Status', 7, 0);

update POWERPLANT_TABLES set PP_TABLE_TYPE_ID = 's' where UPPER(TABLE_NAME) = 'WORK_ORDER_STATUS';

-- * Scripts to load MODULE: PM
/*
   cr_data_mover_module    where module = 'PM'
   cr_data_mover_module_table where module = 'PM'
   cr_data_mover_module_join  where module = 'PM'
   cr_data_mover_module_column   where module = 'PM' and substr(description,1,3) <> 'CC_'
*/

delete from CR_DATA_MOVER_MODULE_COLUMN where MODULE = 'PM';
delete from CR_DATA_MOVER_MODULE_TABLE where MODULE = 'PM';
delete from CR_DATA_MOVER_MODULE_JOIN where MODULE = 'PM';
delete from CR_DATA_MOVER_MODULE where MODULE = 'PM';

insert into CR_DATA_MOVER_MODULE
   (MODULE, USER_ID, TIME_STAMP)
values
   ('PM', 'PWRPLANT', TO_DATE('2012-09-25 13:48:12', 'yyyy-mm-dd hh24:mi:ss'));

insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL', 'PWRPLANT',
    TO_DATE('2012-09-25 13:51:49', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_TYPE', 'WORK_ORDER_TYPE', 'PWRPLANT',
    TO_DATE('2012-09-25 14:19:03', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_CURRENT', 'WO_EST_MONTHLY', 'PWRPLANT',
    TO_DATE('2012-09-27 13:15:05', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_MAX', 'WO_EST_MONTHLY', 'PWRPLANT',
    TO_DATE('2012-09-27 13:15:05', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_CURRENT_ECT', 'ESTIMATE_CHARGE_TYPE', 'PWRPLANT',
    TO_DATE('2012-09-27 13:32:07', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_MAX_ECT', 'ESTIMATE_CHARGE_TYPE', 'PWRPLANT',
    TO_DATE('2012-09-27 13:32:07', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'ASSET_LOCATION', 'ASSET_LOCATION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:12:14', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'DEPARTMENT', 'DEPARTMENT', 'PWRPLANT',
    TO_DATE('2012-09-26 18:07:44', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'DIVISION', 'DIVISION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:11:38', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'MAJOR_LOCATION', 'MAJOR_LOCATION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:12:14', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'COUNTY', 'COUNTY', 'PWRPLANT', TO_DATE('2012-09-26 18:14:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'PROP_TAX_LOCATION', 'PROP_TAX_LOCATION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:14:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'STATE', 'STATE', 'PWRPLANT', TO_DATE('2012-09-26 18:14:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'TAX_LOCATION', 'TAX_LOCATION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:14:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_STATUS', 'WORK_ORDER_STATUS', 'PWRPLANT',
    TO_DATE('2012-09-26 18:19:40', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_CONTRACT_ADM', 'PP_SECURITY_USERS', 'PWRPLANT',
    TO_DATE('2012-09-26 18:26:42', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_ENGINEER', 'PP_SECURITY_USERS', 'PWRPLANT',
    TO_DATE('2012-09-26 18:26:42', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_OTHER', 'PP_SECURITY_USERS', 'PWRPLANT',
    TO_DATE('2012-09-26 18:26:42', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PLANT_AC', 'PP_SECURITY_USERS', 'PWRPLANT',
    TO_DATE('2012-09-26 18:26:42', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PROJ_MGR', 'PP_SECURITY_USERS', 'PWRPLANT',
    TO_DATE('2012-09-26 18:26:42', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'COMPANY_SETUP', 'COMPANY_SETUP', 'PWRPLANT',
    TO_DATE('2012-09-25 13:54:12', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_ACCOUNT', 'WORK_ORDER_ACCOUNT', 'PWRPLANT',
    TO_DATE('2012-09-25 13:51:49', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_INITIATOR', 'WORK_ORDER_INITIATOR', 'PWRPLANT',
    TO_DATE('2012-09-25 13:51:49', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'BUSINESS_SEGMENT', 'BUSINESS_SEGMENT', 'PWRPLANT',
    TO_DATE('2012-09-25 13:54:12', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'FUNDING_PROJECT', 'WORK_ORDER_CONTROL', 'PWRPLANT',
    TO_DATE('2012-09-25 14:30:05', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'REIMB_WORK_ORDER', 'REIMB_WORK_ORDER', 'PWRPLANT',
    TO_DATE('2012-09-27 14:32:04', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'CLOSING_OPTION', 'CLOSING_OPTION', 'PWRPLANT',
    TO_DATE('2012-09-27 10:03:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_TABLE
   (MODULE, TABLE_NAME, DB_TABLE_NAME, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_GROUP', 'WORK_ORDER_GROUP', 'PWRPLANT',
    TO_DATE('2012-11-06 10:58:23', 'yyyy-mm-dd hh24:mi:ss'));

insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_CURRENT', 'WORK_ORDER_CONTROL',
    'WEM_CURRENT.WORK_ORDER_ID = WORK_ORDER_CONTROL.WORK_ORDER_ID and WEM_CURRENT.REVISION = WORK_ORDER_CONTROL.CURRENT_REVISION',
    'PWRPLANT', TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_TYPE', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_CONTROL.WORK_ORDER_TYPE_ID = WORK_ORDER_TYPE.WORK_ORDER_TYPE_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_MAX', 'WORK_ORDER_CONTROL',
    'WEM_MAX.WORK_ORDER_ID = WORK_ORDER_CONTROL.WORK_ORDER_ID and WEM_MAX.REVISION = (select max(revision) from wo_est_monthly b where wem_max.work_order_id = b.work_order_id)',
    'PWRPLANT', TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_CURRENT_ECT', 'WEM_CURRENT',
    'WEM_CURRENT.EST_CHG_TYPE_ID = WEM_CURRENT_ECT.EST_CHG_TYPE_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WEM_MAX_ECT', 'WEM_MAX', 'WEM_MAX.EST_CHG_TYPE_ID = WEM_MAX_ECT.EST_CHG_TYPE_ID',
    'PWRPLANT', TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'DEPARTMENT', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_CONTROL.DEPARTMENT_ID = DEPARTMENT.DEPARTMENT_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'DIVISION', 'DEPARTMENT', 'DEPARTMENT.DIVISION_ID = DIVISION.DIVISION_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'ASSET_LOCATION', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_CONTROL.ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'MAJOR_LOCATION', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_CONTROL.MAJOR_LOCATION_ID = MAJOR_LOCATION.MAJOR_LOCATION_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'COUNTY', 'ASSET_LOCATION',
    'ASSET_LOCATION.COUNTY_ID = COUNTY.COUNTY_ID and ASSET_LOCATION.STATE_ID = COUNTY.STATE_ID',
    'PWRPLANT', TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'PROP_TAX_LOCATION', 'ASSET_LOCATION',
    'ASSET_LOCATION.PROP_TAX_LOCATION_ID = PROP_TAX_LOCATION.PROP_TAX_LOCATION_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'STATE', 'ASSET_LOCATION', 'ASSET_LOCATION.STATE_ID = STATE.STATE_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'TAX_LOCATION', 'ASSET_LOCATION',
    'ASSET_LOCATION.TAX_LOCATION_ID = TAX_LOCATION.TAX_LOCATION_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_STATUS', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_CONTROL.WO_STATUS_ID = WORK_ORDER_STATUS.WO_STATUS_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_CONTRACT_ADM', 'WORK_ORDER_INITIATOR',
    'WORK_ORDER_INITIATOR.CONTRACT_ADM = CONTACT_CONTRACT_ADM.USERS (+)', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_ENGINEER', 'WORK_ORDER_INITIATOR',
    'WORK_ORDER_INITIATOR.ENGINEER = CONTACT_ENGINEER.USERS (+)', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_OTHER', 'WORK_ORDER_INITIATOR',
    'WORK_ORDER_INITIATOR.OTHER = CONTACT_OTHER.USERS (+)', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PLANT_AC', 'WORK_ORDER_INITIATOR',
    'WORK_ORDER_INITIATOR.PLANT_ACCOUNTANT = CONTACT_PLANT_AC.USERS (+)', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PROJ_MGR', 'WORK_ORDER_INITIATOR',
    'WORK_ORDER_INITIATOR.PROJ_MGR = CONTACT_PROJ_MGR.USERS (+)', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_ACCOUNT', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_ACCOUNT.WORK_ORDER_ID = WORK_ORDER_CONTROL.WORK_ORDER_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'REIMB_WORK_ORDER', 'WORK_ORDER_CONTROL',
    'REIMB_WORK_ORDER.work_order_id (+) = work_order_control.work_order_id', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_CONTROL', null, null, 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_INITIATOR', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_INITIATOR.WORK_ORDER_ID = WORK_ORDER_CONTROL.WORK_ORDER_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'BUSINESS_SEGMENT', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_CONTROL.BUS_SEGMENT_ID = BUSINESS_SEGMENT.BUS_SEGMENT_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'COMPANY_SETUP', 'WORK_ORDER_CONTROL',
    'WORK_ORDER_CONTROL.COMPANY_ID = COMPANY_SETUP.COMPANY_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'FUNDING_PROJECT', 'WORK_ORDER_CONTROL',
    'FUNDING_PROJECT.WORK_ORDER_ID = WORK_ORDER_CONTROL.FUNDING_WO_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'CLOSING_OPTION', 'WORK_ORDER_ACCOUNT',
    'WORK_ORDER_ACCOUNT.CLOSING_OPTION_ID = CLOSING_OPTION.CLOSING_OPTION_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:49:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_JOIN
   (MODULE, TABLE_NAME, TABLE_NAME_PARENT, JOIN_STRING, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_GROUP', 'WORK_ORDER_ACCOUNT',
    'WORK_ORDER_ACCOUNT.WORK_ORDER_GRP_ID = WORK_ORDER_GROUP.WORK_ORDER_GRP_ID', 'PWRPLANT',
    TO_DATE('2012-11-06 10:59:10', 'yyyy-mm-dd hh24:mi:ss'));

insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_TOTAL_ADDITIONS', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = work_order_control.current_revision and expenditure_type_id = 1)',
    'PWRPLANT', TO_DATE('2012-09-27 13:28:39', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_TOTAL_REMOVAL', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = work_order_control.current_revision and expenditure_type_id = 2)',
    'PWRPLANT', TO_DATE('2012-09-27 13:28:39', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_TOTAL', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = (select max(revision) from wo_est_monthly c where est.work_order_id = c.work_order_id))',
    'PWRPLANT', TO_DATE('2012-09-27 13:29:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_TOTAL_ADDITIONS', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = (select max(revision) from wo_est_monthly c where est.work_order_id = c.work_order_id) and expenditure_type_id = 1)',
    'PWRPLANT', TO_DATE('2012-09-27 13:29:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_TOTAL_REMOVAL', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = (select max(revision) from wo_est_monthly c where est.work_order_id = c.work_order_id) and expenditure_type_id = 2)',
    'PWRPLANT', TO_DATE('2012-09-27 13:29:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_TYPE', 'WORK_ORDER_TYPE', 'WORK_ORDER_TYPE.EXTERNAL_WORK_ORDER_TYPE',
    'PWRPLANT', TO_DATE('2012-09-27 13:51:14', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_TOTAL', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = work_order_control.current_revision)  ',
    'PWRPLANT', TO_DATE('2012-09-27 13:28:39', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_DTL_AMOUNT', 'WEM_CURRENT', 'WEM_CURRENT.TOTAL', 'PWRPLANT',
    TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_DTL_EST_CHG_TYPE', 'WEM_CURRENT_ECT',
    'WEM_CURRENT_ECT.EXTERNAL_EST_CHG_TYPE', 'PWRPLANT',
    TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_DTL_YEAR', 'WEM_CURRENT', 'WEM_CURRENT.YEAR', 'PWRPLANT',
    TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_REVISION', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.CURRENT_REVISION',
    'PWRPLANT', TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_DTL_AMOUNT', 'WEM_MAX', 'WEM_MAX.TOTAL', 'PWRPLANT',
    TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_DTL_EST_CHG_TYPE', 'WEM_MAX_ECT', 'WEM_MAX_ECT.EXTERNAL_EST_CHG_TYPE',
    'PWRPLANT', TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_DTL_YEAR', 'WEM_MAX', 'WEM_MAX.YEAR', 'PWRPLANT',
    TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_REVISION', 'WORK_ORDER_CONTROL',
    '(select max(revision) from wo_est_monthly c where work_order_control.work_order_id = c.work_order_id)',
    'PWRPLANT', TO_DATE('2012-09-27 13:41:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_CURR_REV_TOTAL_EXCLUSIONS', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = work_order_control.current_revision and nvl(b.exclude_from_total_charges,0) = 0)',
    'PWRPLANT', TO_DATE('2012-09-27 13:47:06', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_MAX_REV_TOTAL_EXCLUSIONS', 'WORK_ORDER_CONTROL',
    '(select sum(total) from wo_est_monthly est, estimate_charge_type b where est.est_chg_type_id = b.est_chg_type_id and b.processing_type_id not in (1,5) and est.work_order_id = work_order_control.work_order_id and est.revision = (select max(revision) from wo_est_monthly c where est.work_order_id = c.work_order_id) and nvl(b.exclude_from_total_charges,0) = 0)',
    'PWRPLANT', TO_DATE('2012-09-27 13:47:50', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'DEPARTMENT_DESCRIPTION', 'DEPARTMENT', 'DEPARTMENT.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:11:28', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'COMPLETION_DATE', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.COMPLETION_DATE', 'PWRPLANT',
    TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'DESCRIPTION', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_COMPLETE_DATE', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.EST_COMPLETE_DATE',
    'PWRPLANT', TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_IN_SERVICE_DATE', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.EST_IN_SERVICE_DATE',
    'PWRPLANT', TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_START_DATE', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.EST_START_DATE', 'PWRPLANT',
    TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'FUNDING_WO_INDICATOR', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.FUNDING_WO_INDICATOR',
    'PWRPLANT', TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'IN_SERVICE_DATE', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.IN_SERVICE_DATE', 'PWRPLANT',
    TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LONG_DESCRIPTION', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.LONG_DESCRIPTION',
    'PWRPLANT', TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'NOTES', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.NOTES', 'PWRPLANT',
    TO_DATE('2012-09-26 18:10:54', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'DEPARTMENT_CODE', 'DEPARTMENT', 'DEPARTMENT.EXTERNAL_DEPARTMENT_CODE', 'PWRPLANT',
    TO_DATE('2012-09-26 18:11:28', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'DIVISION_DESCRIPTION', 'DIVISION', 'DIVISION.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:11:53', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_PROP_TAX', 'PROP_TAX_LOCATION', 'PROP_TAX_LOCATION.LOCATION_CODE', 'PWRPLANT',
    TO_DATE('2012-09-26 18:17:16', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_STATE_DESCRIPTION', 'STATE', 'STATE.LONG_DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:15:24', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_STATE', 'ASSET_LOCATION', 'ASSET_LOCATION.STATE_ID', 'PWRPLANT',
    TO_DATE('2012-09-26 18:15:38', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_ASSET', 'ASSET_LOCATION', 'ASSET_LOCATION.EXT_ASSET_LOCATION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:16:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_ASSET_DESCRIPTION', 'ASSET_LOCATION', 'ASSET_LOCATION.LONG_DESCRIPTION',
    'PWRPLANT', TO_DATE('2012-09-26 18:16:13', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_MAJOR_DESCR', 'MAJOR_LOCATION', 'MAJOR_LOCATION.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:16:46', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_PROP_TAX_DESCRIPTION', 'PROP_TAX_LOCATION', 'PROP_TAX_LOCATION.DESCRIPTION',
    'PWRPLANT', TO_DATE('2012-09-26 18:17:16', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_TAX_DESCRIPTION', 'TAX_LOCATION', 'TAX_LOCATION.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:17:34', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_COUNTY', 'COUNTY', 'COUNTY.COUNTY_CODE', 'PWRPLANT',
    TO_DATE('2012-09-26 18:18:58', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'LOCATION_COUNTY_DESCRIPTION', 'COUNTY', 'COUNTY.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:18:58', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WO_STATUS_DESCRIPTION', 'WORK_ORDER_STATUS', 'WORK_ORDER_STATUS.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:19:40', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WO_STATUS_ID', 'WORK_ORDER_STATUS', 'WORK_ORDER_STATUS.WO_STATUS_ID', 'PWRPLANT',
    TO_DATE('2012-09-26 18:19:40', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'BUS_SEGMENT', 'BUSINESS_SEGMENT', 'BUSINESS_SEGMENT.EXTERNAL_BUS_SEGMENT', 'PWRPLANT',
    TO_DATE('2012-09-26 18:20:09', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'BUS_SEGMENT_DESCRIPTION', 'BUSINESS_SEGMENT', 'BUSINESS_SEGMENT.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:20:09', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'COMPANY_DESCRIPTION', 'COMPANY_SETUP', 'COMPANY_SETUP.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-26 18:20:50', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'COMPANY_ID', 'COMPANY_SETUP', 'COMPANY_SETUP.COMPANY_ID', 'PWRPLANT',
    TO_DATE('2012-09-26 18:20:50', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'COMPANY_NUMBER', 'COMPANY_SETUP', 'COMPANY_SETUP.GL_COMPANY_NO', 'PWRPLANT',
    TO_DATE('2012-09-26 18:20:50', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'FUNDING_PROJECT_DESCRIPTION', 'FUNDING_PROJECT', 'FUNDING_PROJECT.DESCRIPTION',
    'PWRPLANT', TO_DATE('2012-09-26 18:21:11', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_CONTRACT_ADM', 'WORK_ORDER_INITIATOR', 'WORK_ORDER_INITIATOR.CONTRACT_ADM',
    'PWRPLANT', TO_DATE('2012-09-26 18:22:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_ENGINEER', 'WORK_ORDER_INITIATOR', 'WORK_ORDER_INITIATOR.ENGINEER', 'PWRPLANT',
    TO_DATE('2012-09-26 18:22:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_OTHER', 'WORK_ORDER_INITIATOR', 'WORK_ORDER_INITIATOR.OTHER', 'PWRPLANT',
    TO_DATE('2012-09-26 18:22:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PLANT_ACCOUNTANT', 'WORK_ORDER_INITIATOR',
    'WORK_ORDER_INITIATOR.PLANT_ACCOUNTANT', 'PWRPLANT',
    TO_DATE('2012-09-26 18:22:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PROJ_MGR', 'WORK_ORDER_INITIATOR', 'WORK_ORDER_INITIATOR.PROJ_MGR', 'PWRPLANT',
    TO_DATE('2012-09-26 18:22:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'INITIATION_DATE', 'WORK_ORDER_INITIATOR', 'WORK_ORDER_INITIATOR.INITIATION_DATE',
    'PWRPLANT', TO_DATE('2012-09-26 18:22:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_CONTRACT_ADM_EMAIL', 'CONTACT_CONTRACT_ADM', 'CONTACT_CONTRACT_ADM.MAIL_ID',
    'PWRPLANT', TO_DATE('2012-09-26 18:28:03', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_CONTRACT_ADM_FIRST', 'CONTACT_CONTRACT_ADM', 'CONTACT_CONTRACT_ADM.FIRST_NAME',
    'PWRPLANT', TO_DATE('2012-09-26 18:28:03', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_CONTRACT_ADM_LAST', 'CONTACT_CONTRACT_ADM', 'CONTACT_CONTRACT_ADM.LAST_NAME',
    'PWRPLANT', TO_DATE('2012-09-26 18:28:03', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_ENGINEER_EMAIL', 'CONTACT_ENGINEER', 'CONTACT_ENGINEER.MAIL_ID', 'PWRPLANT',
    TO_DATE('2012-09-26 18:28:27', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_ENGINEER_FIRST', 'CONTACT_ENGINEER', 'CONTACT_ENGINEER.FIRST_NAME', 'PWRPLANT',
    TO_DATE('2012-09-26 18:28:27', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_ENGINEER_LAST', 'CONTACT_ENGINEER', 'CONTACT_ENGINEER.LAST_NAME', 'PWRPLANT',
    TO_DATE('2012-09-26 18:28:27', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_OTHER_EMAIL', 'CONTACT_OTHER', 'CONTACT_OTHER.MAIL_ID', 'PWRPLANT',
    TO_DATE('2012-09-26 18:28:47', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_OTHER_FIRST', 'CONTACT_OTHER', 'CONTACT_OTHER.FIRST_NAME', 'PWRPLANT',
    TO_DATE('2012-09-26 18:28:47', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_OTHER_LAST', 'CONTACT_OTHER', 'CONTACT_OTHER.LAST_NAME', 'PWRPLANT',
    TO_DATE('2012-09-26 18:28:47', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PLANT_ACCOUNTANT_EMAIL', 'CONTACT_PLANT_AC', 'CONTACT_PLANT_AC.MAIL_ID',
    'PWRPLANT', TO_DATE('2012-09-26 18:29:20', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PLANT_ACCOUNTANT_FIRST', 'CONTACT_PLANT_AC', 'CONTACT_PLANT_AC.FIRST_NAME',
    'PWRPLANT', TO_DATE('2012-09-26 18:29:20', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PLANT_ACCOUNTANT_LAST', 'CONTACT_PLANT_AC', 'CONTACT_PLANT_AC.LAST_NAME',
    'PWRPLANT', TO_DATE('2012-09-26 18:29:20', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PROJ_MGR_EMAIL', 'CONTACT_PROJ_MGR', 'CONTACT_PROJ_MGR.MAIL_ID', 'PWRPLANT',
    TO_DATE('2012-09-26 18:29:38', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PROJ_MGR_FIRST', 'CONTACT_PROJ_MGR', 'CONTACT_PROJ_MGR.FIRST_NAME', 'PWRPLANT',
    TO_DATE('2012-09-26 18:29:38', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PROJ_MGR_LAST', 'CONTACT_PROJ_MGR', 'CONTACT_PROJ_MGR.LAST_NAME', 'PWRPLANT',
    TO_DATE('2012-09-26 18:29:38', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_TYPE_DESCRIPTION', 'WORK_ORDER_TYPE', 'WORK_ORDER_TYPE.DESCRIPTION',
    'PWRPLANT', TO_DATE('2012-09-26 18:30:17', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_ENGINEER_EXTERNAL', 'CONTACT_ENGINEER', 'CONTACT_ENGINEER.EXT_USER_CODE',
    'PWRPLANT', TO_DATE('2012-09-27 13:03:02', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_OTHER_EXTERNAL', 'CONTACT_OTHER', 'CONTACT_OTHER.EXT_USER_CODE', 'PWRPLANT',
    TO_DATE('2012-09-27 13:03:22', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PLANT_ACCOUNTANT_EXT', 'CONTACT_PLANT_AC', 'CONTACT_PLANT_AC.EXT_USER_CODE',
    'PWRPLANT', TO_DATE('2012-09-27 13:04:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_PROJ_MGR_EXTERNAL', 'CONTACT_PROJ_MGR', 'CONTACT_PROJ_MGR.EXT_USER_CODE',
    'PWRPLANT', TO_DATE('2012-09-27 13:04:18', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WO_STATUS', 'WORK_ORDER_STATUS', 'WORK_ORDER_STATUS.EXTERNAL_WORK_ORDER_STATUS',
    'PWRPLANT', TO_DATE('2012-09-27 14:12:23', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'ACTUALS_TOTAL_EXCLUSIONS', 'WORK_ORDER_CONTROL',
    '(select sum(amount) from cwip_charge, charge_type where work_order_control.work_order_id = cwip_charge.work_order_id and cwip_charge.charge_type_id = charge_type.charge_type_id and processing_type_id not in (1,5) and nvl(exclude_from_total_charges,0) = 0)',
    'PWRPLANT', TO_DATE('2012-09-27 14:26:11', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CLOSING_OPTION', 'CLOSING_OPTION', 'CLOSING_OPTION.CLOSING_OPTION_ID', 'PWRPLANT',
    TO_DATE('2012-09-27 14:26:58', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CLOSING_OPTION_DESCRIPTION', 'CLOSING_OPTION', 'CLOSING_OPTION.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-09-27 14:26:58', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CLOSE_DATE', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.CLOSE_DATE', 'PWRPLANT',
    TO_DATE('2012-09-27 14:27:29', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'REIMB_INDICATOR', 'REIMB_WORK_ORDER', 'nvl2(REIMB_WORK_ORDER.WORK_ORDER_ID,1,0)',
    'PWRPLANT', TO_DATE('2012-09-27 14:32:04', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CONTACT_CONTRACT_ADM_EXTERNAL', 'CONTACT_CONTRACT_ADM',
    'CONTACT_CONTRACT_ADM.EXT_USER_CODE', 'PWRPLANT',
    TO_DATE('2012-09-27 13:02:46', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'ACTUALS_TOTAL_ADDITIONS', 'WORK_ORDER_CONTROL',
    '(select sum(amount) from cwip_charge, charge_type where work_order_control.work_order_id = cwip_charge.work_order_id and cwip_charge.charge_type_id = charge_type.charge_type_id and processing_type_id not in (1,5) and expenditure_type_id = 1)',
    'PWRPLANT', TO_DATE('2012-09-27 14:26:11', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'ACTUALS_TOTAL_REMOVAL', 'WORK_ORDER_CONTROL',
    '(select sum(amount) from cwip_charge, charge_type where work_order_control.work_order_id = cwip_charge.work_order_id and cwip_charge.charge_type_id = charge_type.charge_type_id and processing_type_id not in (1,5) and expenditure_type_id = 2)',
    'PWRPLANT', TO_DATE('2012-09-27 14:26:11', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'ACTUALS_TOTAL', 'WORK_ORDER_CONTROL',
    '(select sum(amount) from cwip_charge, charge_type where work_order_control.work_order_id = cwip_charge.work_order_id and cwip_charge.charge_type_id = charge_type.charge_type_id and processing_type_id not in (1,5))',
    'PWRPLANT', TO_DATE('2012-09-27 14:26:11', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_NUMBER', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.WORK_ORDER_NUMBER',
    'PWRPLANT', TO_DATE('2012-09-25 13:54:12', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'FUNDING_PROJECT_NUMBER', 'FUNDING_PROJECT', 'FUNDING_PROJECT.WORK_ORDER_NUMBER',
    'PWRPLANT', TO_DATE('2012-09-25 14:31:24', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_IN_SERVICE_DATE_YYYYMMDD', 'WORK_ORDER_CONTROL',
    'to_char(WORK_ORDER_CONTROL.EST_IN_SERVICE_DATE,''yyyymmdd'')', 'PWRPLANT',
    TO_DATE('2012-10-01 16:49:04', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_START_DATE_YYYYMMDD', 'WORK_ORDER_CONTROL',
    'to_char(WORK_ORDER_CONTROL.EST_START_DATE,''yyyymmdd'')', 'PWRPLANT',
    TO_DATE('2012-10-01 16:49:04', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'IN_SERVICE_DATE_YYYYMMDD', 'WORK_ORDER_CONTROL',
    'to_char(WORK_ORDER_CONTROL.IN_SERVICE_DATE,''yyyymmdd'')', 'PWRPLANT',
    TO_DATE('2012-10-01 16:49:04', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'CLOSE_DATE_YYYYMMDD', 'WORK_ORDER_CONTROL',
    'to_char(WORK_ORDER_CONTROL.CLOSE_DATE,''yyyymmdd'')', 'PWRPLANT',
    TO_DATE('2012-10-01 16:49:34', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'COMPLETION_DATE_YYYYMMDD', 'WORK_ORDER_CONTROL',
    'to_char(WORK_ORDER_CONTROL.COMPLETION_DATE,''yyyymmdd'')', 'PWRPLANT',
    TO_DATE('2012-10-01 16:49:34', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'EST_COMPLETE_DATE_YYYYMMDD', 'WORK_ORDER_CONTROL',
    'to_char(WORK_ORDER_CONTROL.EST_COMPLETE_DATE,''yyyymmdd'')', 'PWRPLANT',
    TO_DATE('2012-10-01 16:49:34', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_ID', 'WORK_ORDER_CONTROL', 'WORK_ORDER_CONTROL.WORK_ORDER_ID', 'PWRPLANT',
    TO_DATE('2012-11-06 10:59:10', 'yyyy-mm-dd hh24:mi:ss'));
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, sql, USER_ID, TIME_STAMP)
values
   ('PM', 'WORK_ORDER_GROUP', 'WORK_ORDER_GROUP', 'WORK_ORDER_GROUP.DESCRIPTION', 'PWRPLANT',
    TO_DATE('2012-11-06 10:58:42', 'yyyy-mm-dd hh24:mi:ss'));
commit;

-- * Class codes for MODULE: PM
insert into CR_DATA_MOVER_MODULE_COLUMN
   (MODULE, DESCRIPTION, TABLE_NAME, SQL)
   select 'PM',
      'CC_' || upper(
         replace(replace(replace(replace(replace(replace(
         trim(substr(DESCRIPTION,1,30 - 1 - 3 - length(CLASS_CODE_ID))) || '_' || CLASS_CODE_ID
         ,' ','_'),'-','_'),'/','_'),'\','_'),')','_'),'(','_')
         ),
      'WORK_ORDER_CONTROL',
      '(select "VALUE" from work_order_class_code where work_order_control.work_order_id = work_order_class_code.work_order_id and work_order_class_code.class_code_id = ' || class_code_id || ')'
   from CLASS_CODE a
   where
      nvl(CWIP_INDICATOR,0) > 0;
commit;

-- * PP Integration Components
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('Data Mover Batch', 'Program Name', null, null, null, null, null, null, null, null, null,
    'PWRPLANT', TO_DATE('2012-10-22 11:00:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('Data Mover Where', 'Where Clause (where batch like ''%'')', null, null, null, null, null, null,
    null, null, null, 'PWRPLANT', TO_DATE('2012-10-22 11:00:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('Create MX Locations', 'Batch / Instance', null, null, null, null, null, null, null, null, null,
    null, null);
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('Create MX CU', 'Batch / Instance', null, null, null, null, null, null, null, null, null, null,
    null);
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('Create Work Order', 'Company Id (-1 = all companies)', 'Batch / Instance', null, null, null,
    null, null, null, null, null, 'PWRPLANT',
    TO_DATE('2012-10-22 11:00:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('Create Job Task', 'Company Id (-1 = all companies)', 'Batch / Instance', null, null, null,
    null, null, null, null, null, 'PWRPLANT',
    TO_DATE('2012-10-22 11:00:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('Load Unit Est', 'Company Id (-1 = all companies)', 'Replace Estimates (true / false)',
    'Process OCR (true / false)', 'Batch / Instance', null, null, null, null, null, null, 'PWRPLANT',
    TO_DATE('2012-10-22 11:00:33', 'yyyy-mm-dd hh24:mi:ss'));
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('OCR Process', 'Batch / Instance', 'Replace OCRs (true / false)', null, null, null, null, null,
    null, null, null, null, null);
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('PP Translate', 'PP Translate ID', null, null, null, null, null, null, null, null, null, null,
    null);
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE, ARGUMENT7_TITLE, ARGUMENT8_TITLE, ARGUMENT9_TITLE, ARGUMENT10_TITLE, USER_ID,
    TIME_STAMP)
values
   ('CR Transactions', 'API Data Set / Instance', 'CR Source ID',
    'Req. Balanced Entries (true / false)', 'Post to CWIP (true / false)',
    'Post to GL (true / false)', null, null, null, null, null, 'PWRPLANT',
    TO_DATE('2012-10-22 11:00:33', 'yyyy-mm-dd hh24:mi:ss'));

-- * PP Integration -- Allow Concurrent
update PP_PROCESSES set ALLOW_CONCURRENT = 1 where UPPER(DESCRIPTION) like 'PP_INTEGRATION';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (251, 0, 10, 3, 6, 0, 10798, 'C:\PlasticWks\powerplant\sql\maint_scripts', 'maint_010798_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
