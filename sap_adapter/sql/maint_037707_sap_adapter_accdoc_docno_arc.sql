-- Add CR_ID to CR_SAP_ACCDOC_DOCNO
alter table CR_SAP_ACCDOC_DOCNO add (CR_ID number(22,0) null);

-- Create archive table 
create table CR_SAP_ACCDOC_DOCNO_ARC (
COMP_CODE varchar2(4) not null,
ID number(22,0) not null,
AC_DOC_NO varchar2(10),
ITEMNO_ACC number(10,0),
CR_ID number(22,0),
BATCH_ID number(22,0),
ARCHIVE_DATETIME date);

create or replace public synonym CR_SAP_ACCDOC_DOCNO_ARC for pwrplant.CR_SAP_ACCDOC_DOCNO_ARC;
grant all on CR_SAP_ACCDOC_DOCNO_ARC to pwrplant_role_dev;

-- Make sure CR_ID is created as part of new posting types created
insert into cr_sap_gl_to_cr_col_map (
	bapi_type, gl_column_name, cr_column_name, cr_column)
values (
	'ACCDOC', 'CR_ID', 'ID', 1);
commit;