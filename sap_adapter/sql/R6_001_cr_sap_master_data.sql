alter table cr_sap_md_control
add cr_element_id number(22,0)
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Company'
)
where element_id in ('COMPANY_CODE')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Cost Center'
)
where element_id in ('COST_CTR', 'COST_CTR_ALL', 'COST_CTR_TXT', 'COST_CTR_TXT_ALL' )
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Cost Element'
)
where element_id in ('COST_ELEM_COA', 'COST_ELEM_COA_ALL', 'COST_ELEM_TXT', 'COST_ELEM_TXT_ALL')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Operation'
)
where element_id in ('OPERATION', 'OPERATION_ALL')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Profit Center'
)
where element_id in ('PROF_CTR', 'PROF_CTR_ALL', 'PROF_CTR_CO', 'PROF_CTR_TXT', 'PROF_CTR_TXT_ALL')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Account'
)
where element_id in ('GL_ACCT', 'GL_ACCT_2', 'GL_ACCT_CO', 'GL_ACCT_CO2', 'GL_ACCT_TXT', 'GL_ACCT_TXT2')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Trading Partner'
)
where element_id in ('TRAD_PTR')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Document Type'
)
where element_id in ('DOC_TYPE_TXT')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Business Area'
)
where element_id in ('BUS_AREA_TXT')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Jurisdiction'
)
where element_id in ('TAX_JUR')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Tax Code'
)
where element_id in ('TAX_CODE')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Segment'
)
where element_id in ('SEGMENT')
;

update cr_sap_md_control
set cr_element_id =
(
	select e.element_id
	from cr_elements e
	where e.description = 'Order Number'
)
where element_id in ('ORDER_HDR', 'ORDER_HDR_ALL', 'INT_ORD')
;

commit;