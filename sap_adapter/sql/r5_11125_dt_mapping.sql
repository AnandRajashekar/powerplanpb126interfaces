alter table cr_sap_txn_load_control add (source_map_option number(22,0));

create table cr_sap_txn_dt_where (
dt_where_id number(22,0),
description varchar2(35),
source_id number(22,0));

alter table cr_sap_txn_dt_where add (
constraint cr_sap_txn_dt_where_pk primary key (dt_where_id) using index tablespace pwrplant_idx);

create or replace public synonym cr_sap_txn_dt_where for pwrplant.cr_sap_txn_dt_where;
grant all on cr_sap_txn_dt_where to pwrplant_role_dev;

create table cr_sap_txn_dt_where_clause (
dt_where_id number(22,0),
row_id number(22,0),
left_paren varchar2(1),
column_name varchar2(254),
operator varchar2(35),
value1 varchar2(1000),
between_and varchar2(3),
value2 varchar2(35),
and_or varchar2(3),
right_paren varchar2(1),
structure_id number(22,0),
user_id varchar2(18),
time_stamp date);

alter table cr_sap_txn_dt_where_clause add (
constraint cr_sap_txn_dt_where_clause_pk primary key (dt_where_id, row_id) using index tablespace pwrplant_idx);

create or replace public synonym cr_sap_txn_dt_where_clause for pwrplant.cr_sap_txn_dt_where_clause;
grant all on cr_sap_txn_dt_where_clause to pwrplant_role_dev;

ALTER TABLE cr_sap_txn_dt_where_clause
add CONSTRAINT r_cr_sap_txn_dt_wc1
  FOREIGN KEY (dt_where_id)
  REFERENCES cr_sap_txn_dt_where (dt_where_id);