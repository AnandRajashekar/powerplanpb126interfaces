create index cr_sap_md_ce_stg_kstar_ix on cr_sap_md_cost_element_stg (kstar) tablespace pwrplant_idx;

drop index crsapmd_acctcomp_ix1;
create index crsapmd_acctcomp_ix1 on cr_sap_md_account_comp_stg (trim(ltrim(saknr,'0')), bukrs) tablespace pwrplant_idx;

create index crsapmdacctcoa_ltrimsaknr_ix on cr_sap_md_account_coa_stg (trim(ltrim(saknr,'0'))) tablespace pwrplant_idx;

create index crsap_co_area2_bukrs_ix on cr_sap_co_area_assignment (trim(bukrs)) tablespace pwrplant_idx;

drop index cr_sap_md_op_vb_ix;
create index cr_sap_md_op_vb_ix on cr_sap_md_operation_stg (vornr, nvl(trim(bukrs),'no'), trim(ltrim(objnr,'0'))) tablespace pwrplant_idx;