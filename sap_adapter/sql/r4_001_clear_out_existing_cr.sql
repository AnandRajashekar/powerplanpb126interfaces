-- CR ELEMENTS
create table cr_elements_old as (
	select * from cr_elements);

create table cr_elements_fields_old as (
	select * from cr_elements_fields);

create table cr_manual_je_element_hide_old as (
	select * from cr_manual_je_element_hide);

truncate table cr_elements_fields;
truncate table cr_manual_je_element_hide;
delete from cr_elements;
commit;

-- CR SOURCES
create table cr_sources_old as (
	select * from cr_sources);

create table cr_sources_fields_old as (
	select * from cr_sources_fields);

create table cr_interface_month_period_old as (
	select * from cr_interface_month_period);

delete from cr_sources_fields;
delete from cr_interface_month_period;
delete from cr_sources;
commit;

-- CR COST REPOSITORY
create table cr_cost_repository_old as (	
	select * from cr_cost_repository);
drop table cr_cost_repository;
drop table arccr_cost_rep;

-- CR DETAIL TABLES
create table cr_allocations_old as (
	select * from CR_ALLOCATIONS);
drop table cr_allocations;
drop table cr_allocations_stg;
drop table arccr_allocations;

create table cr_allocations_test_old as (
	select * from CR_ALLOCATIONS_TEST);
drop table cr_allocations_test;
drop table cr_allocations_test_stg;
drop table arccr_allocations_test;

create table cr_inter_company_old as (
	select * from CR_INTER_COMPANY);
drop table cr_inter_company;
drop table cr_inter_company_stg;
drop table arccr_inter_company;

create table cr_inter_company_test_old as (
	select * from CR_INTER_COMPANY_TEST);
drop table cr_inter_company_test;
drop table cr_inter_company_test_stg;
drop table arccr_inter_company_test;

create table cr_powerplant_old as (
	select * from CR_POWERPLANT);
drop table cr_powerplant;
drop table cr_powerplant_stg;
drop table arccr_powerplant;

create table cr_reversals_manual_old as (
	select * from CR_REVERSALS_MANUAL);
drop table cr_reversals_manual;
drop table cr_reversals_manual_stg;
drop table arccr_reversals_manual_stg;

create table cr_journal_lines_old as (
	select * from CR_JOURNAL_LINES);
drop table cr_journal_lines;
drop table cr_journal_lines_stg;
drop table arccr_journal_lines_stg;

---- Add existing client specific sources here

-- CWIP CHARGE SETUP
create table cr_to_cwip_control_old as (
	select * from cr_to_cwip_control);

create table cr_to_cwip_trans_clause_old as (
	select * from cr_to_cwip_translate_clause);

create table cr_to_cwip_translate_old as (
	select * from cr_to_cwip_translate);

create table cr_to_cwip_table_list_old as (
	select * from cr_to_cwip_table_list);

create table cr_to_cc_detail_tables_old as (
	select * from cr_to_cc_detail_tables);

delete from cr_to_cwip_control;
delete from cr_to_cwip_translate_clause;
delete from cr_to_cwip_translate;
delete from cr_to_cwip_table_list;
delete from cr_to_cc_detail_tables;
commit;

-- CR POSTING
create table cr_post_to_gl_control_old as (
	select * from cr_post_to_gl_control);

create table cr_post_to_gl_columns_old as (
	select * from cr_post_to_gl_columns);

delete from cr_post_to_gl_columns;
delete from cr_post_to_gl_control;
commit;

-- CR SYSTEM CONTROL
create table cr_system_control_old as (
	select * from cr_system_control);

-- CR ALLOCATION TABLES
create table cr_alloc_interco_criteria_old as (
	select * from cr_alloc_interco_criteria);

create table cr_alloc_interco_criteria2_old as (
	select * from cr_alloc_interco_criteria2);

delete from cr_alloc_interco_criteria2;
delete from cr_alloc_interco_criteria;
commit;

-- CR VALIDATION TABLES
create table cr_validation_combos_old as (
	select * from cr_validation_combos);
truncate table cr_validation_combos;

create table cr_validation_rules_old as (
	select * from cr_validation_rules);
delete from  cr_validation_rules;
commit;

create table cr_open_month_number_old as (
	select * from cr_open_month_number);
truncate table cr_open_month_number;

-- CR COMPANY SECURITY
create table cr_company_security_old as (
	select * from cr_company_security);
truncate table cr_company_security;

-- CR GL ID
create table cr_gl_id_old as (
	select * from cr_gl_id);
truncate table cr_gl_id;

create table cr_ab_id_old as (
	select * from cr_ab_id);
truncate table cr_ab_id;

-- CR BALANCES
create table cr_balances_old as (
	select * from cr_balances);

create table cr_balances_id_old as (
	select * from cr_balances_id);

truncate table cr_balances_id;
truncate table cr_balances;

-- CR SUM, CR SUM AB
create table cr_sum_old as (
	select * from cr_sum);

create table cr_sum_hist_old as (
	select * from cr_sum_hist);

create table cr_sum_ab_old as (
	select * from cr_sum_ab);

create table cr_sum_ab_hist_old as (
	select * from cr_sum_ab_hist);

truncate table cr_sum_hist;
truncate table cr_sum;
truncate table cr_sum_ab_hist;
truncate table cr_sum_ab;

-- ALL DETAILS STAGING TABLES
create table cr_all_details_dr_stg_old as (
	select * from cr_all_details_dr_stg);

create table cr_all_details_orig_stg_old as (
	select * from cr_all_details_orig_stg);

truncate table cr_all_details_dr_stg;
truncate table cr_all_details_orig_stg;

-- CR CANCEL TABLES
create table cr_cancel_header_old as (
	select * from cr_cancel_header);

create table cr_cancel_detail_old as (
	select * from cr_cancel_detail);

create table cr_cancel_exceptions_old as (
	select * from cr_cancel_exceptions);

create table cr_cancel_results_old as (
	select * from cr_cancel_results);

-- CR RECON TABLES
---- Add tables only if client is using reconciliation

-- PP JOURNAL LAYOUTS
create table pp_journal_layouts_old as (
	select * from pp_journal_layouts);
truncate table pp_journal_layouts;