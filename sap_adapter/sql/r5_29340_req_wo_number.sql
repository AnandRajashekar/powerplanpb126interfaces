alter table cr_sap_txn_load_control add req_work_order_number number(22,0);

update cr_sap_txn_load_control set req_work_order_number = 0;

commit;
