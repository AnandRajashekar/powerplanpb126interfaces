/* PP Processes */
insert into pp_processes
	(process_id, description, long_description, running_session_id, allow_concurrent)
	select nvl(max(process_id),0) + 1, 'WO SAP POSTING BAPI CALL', 'WO SAP POSTING BAPI CALL', 0, 0
	from pp_processes;
commit;

/* Headers */	
create table cr_sap_wo_hdr_out_stg (
	action varchar2(35),
	update_datetime date,
	order_number varchar2(12),
	description varchar2(35), 
	company_code  varchar2(4),
	cost_center varchar2(10),
	sales_tax_jurisdiction varchar2(15),
	order_type varchar2(4),
	controlling_area varchar2(4),
	business_area varchar2(4),
	user0 varchar2(20), 
	user1 varchar2(20), 
	user2 varchar2(20), 
	user3 varchar2(20), 
	user4 varchar2(11), 
	user5 varchar2(8), 
	user6 varchar2(15), 
	user7 varchar2(8), 
	user8 varchar2(8), 
	user9 varchar2(1),
	user_id varchar2(18),
	time_stamp date); 
alter table cr_sap_wo_hdr_out_stg add (constraint cr_sap_wo_hdr_out_stg_pk primary key (update_datetime, order_number));

create table cr_sap_wo_hdr_ss (
	order_number varchar2(12),
	description varchar2(35), 
	company_code  varchar2(4),
	cost_center varchar2(10),
	sales_tax_jurisdiction varchar2(15),
	order_type varchar2(4),
	controlling_area varchar2(4),
	business_area varchar2(4),
	user0 varchar2(20), 
	user1 varchar2(20), 
	user2 varchar2(20), 
	user3 varchar2(20), 
	user4 varchar2(11), 
	user5 varchar2(8), 
	user6 varchar2(15), 
	user7 varchar2(8), 
	user8 varchar2(8), 
	user9 varchar2(1),
	user_id varchar2(18),
	time_stamp date); 
alter table cr_sap_wo_hdr_ss add (constraint cr_sap_wo_hdr_ss_pk primary key (order_number));

create table cr_sap_wo_hdr_ss_stg (
	order_number varchar2(12),
	description varchar2(35), 
	company_code  varchar2(4),
	cost_center varchar2(10),
	sales_tax_jurisdiction varchar2(15),
	order_type varchar2(4),
	controlling_area varchar2(4),
	business_area varchar2(4),
	user0 varchar2(20), 
	user1 varchar2(20), 
	user2 varchar2(20), 
	user3 varchar2(20), 
	user4 varchar2(11), 
	user5 varchar2(8), 
	user6 varchar2(15), 
	user7 varchar2(8), 
	user8 varchar2(8), 
	user9 varchar2(1),
	user_id varchar2(18),
	time_stamp date); 

/* Auth Amount */
create table cr_sap_wo_auth_amt_stg (
	action varchar2(35),
	update_datetime date,
	order_number varchar2(12),
	amount number(22,0),
	currency_code varchar2(5),
	fiscal_year number(4,0),
	user_id varchar2(18),
	time_stamp date); 
	
create table cr_sap_wo_auth_amt_ss (
	order_number varchar2(12),
	amount number(22,0),
	currency_code varchar2(5),
	fiscal_year number(4,0),
	user_id varchar2(18),
	time_stamp date); 
alter table cr_sap_wo_auth_amt_ss add (constraint cr_sap_wo_auth_amt_ss_pk primary key (order_number));

create table cr_sap_wo_auth_amt_ss_stg (
	order_number varchar2(12),
	amount number(22,0),
	currency_code varchar2(5),
	fiscal_year number(4,0),
	user_id varchar2(18),
	time_stamp date); 

/* Plan Amount */
create table cr_sap_wo_plan_amt_stg (
	action varchar2(35),
	update_datetime date,
	order_number varchar2(12),
	version varchar2(3),
	controlling_area varchar2(4),
	plan_type varchar2(10),
	currency_code varchar2(1),
	period_from number(3,0),
	period_to number(3,0),
	fiscal_year number(4,0),
	cost_element varchar2(10),
	amount number(22,0),
	distribution_key number(22,0),
	user_id varchar2(18),
	time_stamp date); 
	
create table cr_sap_wo_plan_amt_ss (
	order_number varchar2(12),
	version varchar2(3),
	controlling_area varchar2(4),
	plan_type varchar2(10),
	currency_code varchar2(1),
	period_from number(3,0),
	period_to number(3,0),
	fiscal_year number(4,0),
	cost_element varchar2(10),
	amount number(22,0),
	distribution_key number(22,0),
	user_id varchar2(18),
	time_stamp date); 
alter table cr_sap_wo_plan_amt_ss add (constraint cr_sap_wo_plan_amt_ss_pk primary key (order_number, version, fiscal_year, cost_element));
	
create table cr_sap_wo_plan_amt_ss_stg (
	order_number varchar2(12),
	version varchar2(3),
	controlling_area varchar2(4),
	plan_type varchar2(10),
	currency_code varchar2(1),
	period_from number(3,0),
	period_to number(3,0),
	fiscal_year number(4,0),
	cost_element varchar2(10),
	amount number(22,0),
	distribution_key number(22,0),
	user_id varchar2(18),
	time_stamp date); 

/* Status */
create table cr_sap_wo_status_stg (
	action varchar2(35),
	update_datetime date,
	order_number varchar2(12),
	business_transaction varchar2(4),
	pp_status number(22,0),
	sap_status varchar2(4),
	user_id varchar2(18),
	time_stamp date); 
	
create table cr_sap_wo_status_ss_stg (
	order_number varchar2(12),
	business_transaction varchar2(4),
	pp_status number(22,0),
	sap_status varchar2(4),
	user_id varchar2(18),
	time_stamp date); 
	
create table cr_sap_wo_status_ss (
	order_number varchar2(12),
	business_transaction varchar2(4),
	pp_status number(22,0),
	sap_status varchar2(4),
	user_id varchar2(18),
	time_stamp date); 
alter table cr_sap_wo_status_ss add (constraint cr_sap_wo_status_ss_pk primary key (order_number));
	
create table cr_sap_wo_hdr_out_arc as select a.*, sysdate archive_timestamp from cr_sap_wo_hdr_out_stg a where -1 = 0;
create table cr_sap_wo_auth_amt_arc as select a.*, sysdate archive_timestamp from cr_sap_wo_auth_amt_stg a where -1 = 0;
create table cr_sap_wo_plan_amt_arc as select a.*, sysdate archive_timestamp from cr_sap_wo_plan_amt_stg a where -1 = 0;
create table cr_sap_wo_status_arc as select a.*, sysdate archive_timestamp from cr_sap_wo_status_stg a where -1 = 0;

update work_order_status set external_work_order_status = 
	decode(wo_status_id,
		1, 'CRTD',
		2, 'RLSD',
		3, 'RLSD',
		4, 'RLSD',
		5, 'TECO',
		6, 'TECO',
		7, 'CLSD',
		8, 'CLSD');
commit;

/* Mappings not delivered in the base */
INSERT INTO "CR_DATA_MOVER_MODULE_COLUMN" ("MODULE", "DESCRIPTION", "TABLE_NAME", "SQL", "USER_ID", "TIME_STAMP") VALUES ('PM', 'WORK_ORDER_NUMBER_LPAD12', 'WORK_ORDER_CONTROL', 
'lpad(WORK_ORDER_CONTROL.WORK_ORDER_NUMBER,12,''0'')', 'PWRPLANT', to_date('2012-10-26 14:07:41', 'yyyy-mm-dd hh24:mi:ss')) ; 

/* Delete prior config */
delete from cr_data_mover				where batch like '%SAP%';
delete from cr_data_mover_database		where DATABASE_ID in (select DATABASE_ID from cr_data_mover where batch like '%SAP%');
delete from cr_data_mover_filter_clause	where cr_data_mover_filter_id in (select cr_data_mover_filter_id from cr_data_mover_filter where description like '%SAP%');
delete from cr_data_mover_filter		where cr_data_mover_filter_id in (select cr_data_mover_filter_id from cr_data_mover_filter where description like '%SAP%');
delete from cr_data_mover_map_columns	where cr_data_mover_map_id in (select cr_data_mover_map_id from cr_data_mover_map where description like '%SAP%');
delete from cr_data_mover_map			where cr_data_mover_map_id in (select cr_data_mover_map_id from cr_data_mover_map where description like '%SAP%');
commit;
	
/* Data Mover config */
/* 
	1. cr_data_mover_database		where DATABASE_ID in (select DATABASE_ID from cr_data_mover where batch like '%SAP%')
	2. cr_data_mover_filter			where cr_data_mover_filter_id in (select cr_data_mover_filter_id from cr_data_mover where batch like '%SAP%')
	3. cr_data_mover_filter_clause	where cr_data_mover_filter_id in (select cr_data_mover_filter_id from cr_data_mover where batch like '%SAP%')
	4. cr_data_mover_map			where cr_data_mover_map_id in (select cr_data_mover_map_id from cr_data_mover where batch like '%SAP%')
	5. cr_data_mover_map_columns	where cr_data_mover_map_id in (select cr_data_mover_map_id from cr_data_mover where batch like '%SAP%')
	6. cr_data_mover				where batch like '%SAP%'
*/
INSERT INTO "CR_DATA_MOVER_DATABASE" ("DATABASE_ID", "DATABASE_SERVER", "DATABASE_USERID", "DATABASE_PASSWORD", "DBMS", "ODBC_STRING", "USER_ID", "TIME_STAMP", 
"ENCRYPTED_PASSWORD") VALUES (1000, null, null, null, 'o10', null, 'PWRPLANT', to_date('2012-09-27 15:08:13', 'yyyy-mm-dd hh24:mi:ss'), 0) ; 

INSERT INTO "CR_DATA_MOVER_FILTER" ("CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (1000, 'SAP Work Order Filter', 'PWRPLANT', 
to_date('2012-10-01 16:55:49', 'yyyy-mm-dd hh24:mi:ss')) ; 

INSERT INTO "CR_DATA_MOVER_FILTER" ("CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (1001, 'SAP Work Order Auth Amount Filter', 'PWRPLANT', 
to_date('2012-10-24 13:52:09', 'yyyy-mm-dd hh24:mi:ss')) ; 

INSERT INTO "CR_DATA_MOVER_FILTER_CLAUSE" ("CR_DATA_MOVER_FILTER_ID", "ROW_ID", "USER_ID", "TIME_STAMP", "LEFT_PAREN", "COLUMN_NAME", "OPERATOR", "VALUE1", "BETWEEN_AND", "VALUE2",
 "AND_OR", "RIGHT_PAREN") VALUES (1000, 1, 'PWRPLANT', to_date('2012-10-01 16:55:49', 'yyyy-mm-dd hh24:mi:ss'), null, 'module.PM.FUNDING_WO_INDICATOR', '=', '0', null, null, null, null) ; 
INSERT INTO "CR_DATA_MOVER_FILTER_CLAUSE" ("CR_DATA_MOVER_FILTER_ID", "ROW_ID", "USER_ID", "TIME_STAMP", "LEFT_PAREN", "COLUMN_NAME", "OPERATOR", "VALUE1", "BETWEEN_AND", "VALUE2",
 "AND_OR", "RIGHT_PAREN") VALUES (1001, 1, 'PWRPLANT', to_date('2012-10-24 14:03:47', 'yyyy-mm-dd hh24:mi:ss'), null, 'module.PM.FUNDING_WO_INDICATOR', '=', '0', null, null, 'and', null) ; 
INSERT INTO "CR_DATA_MOVER_FILTER_CLAUSE" ("CR_DATA_MOVER_FILTER_ID", "ROW_ID", "USER_ID", "TIME_STAMP", "LEFT_PAREN", "COLUMN_NAME", "OPERATOR", "VALUE1", "BETWEEN_AND", "VALUE2",
 "AND_OR", "RIGHT_PAREN") VALUES (1001, 2, 'PWRPLANT', to_date('2012-10-24 14:03:47', 'yyyy-mm-dd hh24:mi:ss'), null, 'module.PM.EST_CURR_REV_TOTAL_EXCLUSIONS', '<>', '0', null, null, null, null) ; 

 INSERT INTO "CR_DATA_MOVER_MAP" ("CR_DATA_MOVER_MAP_ID", "DESCRIPTION", "TO_TABLE_NAME", "FROM_TABLE_NAME", "USER_ID", "TIME_STAMP") VALUES (1000, 'SAP Work Order Extract', 
'cr_sap_wo_hdr_ss_stg', 'MODULE: PM', 'PWRPLANT', to_date('2012-10-01 16:55:49', 'yyyy-mm-dd hh24:mi:ss')) ; 
INSERT INTO "CR_DATA_MOVER_MAP" ("CR_DATA_MOVER_MAP_ID", "DESCRIPTION", "TO_TABLE_NAME", "FROM_TABLE_NAME", "USER_ID", "TIME_STAMP") VALUES (1001, 
'SAP Work Order Auth Amounts Extract', 'cr_sap_wo_auth_amt_ss_stg', 'MODULE: PM', 'PWRPLANT', to_date('2012-10-01 16:55:49', 'yyyy-mm-dd hh24:mi:ss')) ; 
INSERT INTO "CR_DATA_MOVER_MAP" ("CR_DATA_MOVER_MAP_ID", "DESCRIPTION", "TO_TABLE_NAME", "FROM_TABLE_NAME", "USER_ID", "TIME_STAMP") VALUES (1002, 
'SAP Work Order Plan Amounts Extract', 'cr_sap_wo_plan_amt_ss_stg', 'MODULE: PM', 'PWRPLANT', to_date('2012-10-01 16:55:49', 'yyyy-mm-dd hh24:mi:ss')) ; 
INSERT INTO "CR_DATA_MOVER_MAP" ("CR_DATA_MOVER_MAP_ID", "DESCRIPTION", "TO_TABLE_NAME", "FROM_TABLE_NAME", "USER_ID", "TIME_STAMP") VALUES (1003, 'SAP Work Order Status Extract', 
'cr_sap_wo_status_ss_stg', 'MODULE: PM', 'PWRPLANT', to_date('2012-10-04 16:00:19', 'yyyy-mm-dd hh24:mi:ss')) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'BUSINESS_AREA', '''9900''', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 4, null, 80, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'COMPANY_CODE', 'module.PM.COMPANY_NUMBER', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 4, null, 30, 0, null) 
; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'CONTROLLING_AREA', '''9900''', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 4, null, 70, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'COST_CENTER', 'module.PM.DEPARTMENT_CODE', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 10, null, 40, 0, null) 
; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'DESCRIPTION', 'module.PM.DESCRIPTION', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 35, null, 20, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'ORDER_NUMBER', 'module.PM.WORK_ORDER_NUMBER', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 12, null, 10, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'ORDER_TYPE', '''SMAN''', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 4, null, 60, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'SALES_TAX_JURISDICTION', 'module.PM.LOCATION_ASSET', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 15, null, 50,
 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'TIME_STAMP', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:05', 'yyyy-mm-dd hh24:mi:ss'), 'Date', 0, null, 200, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER0', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 20, null, 90, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER1', 'module.PM.EST_IN_SERVICE_DATE_YYYYMMDD', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 20, null, 100, 
0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER2', 'module.PM.CONTACT_PROJ_MGR_EXTERNAL', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 20, null, 110, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER3', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 20, null, 120, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER4', 'module.PM.ACTUALS_TOTAL_EXCLUSIONS', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 11, null, 130, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER5', 'module.PM.EST_START_DATE_YYYYMMDD', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 8, null, 140, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER6', 'module.PM.LOCATION_ASSET', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 15, null, 150, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER7', 'module.PM.IN_SERVICE_DATE_YYYYMMDD', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 8, null, 160, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER8', 'module.PM.CLOSE_DATE_YYYYMMDD', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 8, null, 170, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER9', 'module.PM.REIMB_INDICATOR', 'PWRPLANT', to_date('2012-11-12 13:54:56', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 1, null, 180, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1000, 'USER_ID', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:05', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 18, null, 190, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1001, 'AMOUNT', 'module.PM.EST_CURR_REV_TOTAL_EXCLUSIONS', 'PWRPLANT', to_date('2012-11-12 13:55:11', 'yyyy-mm-dd hh24:mi:ss'), 'Number', 22, 0, 20, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1001, 'CURRENCY_CODE', '''USD''', 'PWRPLANT', to_date('2012-11-12 13:55:11', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 5, null, 50, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1001, 'FISCAL_YEAR', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:11', 'yyyy-mm-dd hh24:mi:ss'), 'Number', 4, 0, 60, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1001, 'ORDER_NUMBER', 'module.PM.WORK_ORDER_NUMBER_LPAD12', 'PWRPLANT', to_date('2012-11-12 13:55:11', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 12, null, 10,
 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1001, 'TIME_STAMP', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:11', 'yyyy-mm-dd hh24:mi:ss'), 'Date', 0, null, 40, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1001, 'USER_ID', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:11', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 18, null, 30, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'AMOUNT', 'module.PM.EST_CURR_REV_DTL_AMOUNT', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Number', 22, 0, 100, 0, 'sum') 
; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'CONTROLLING_AREA', '''9900''', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 4, null, 30, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'COST_ELEMENT', 'module.PM.EST_CURR_REV_DTL_EST_CHG_TYPE', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 10, 
null, 90, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'CURRENCY_CODE', '''C''', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 1, null, 50, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'DISTRIBUTION_KEY', '2', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 10, null, 110, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'FISCAL_YEAR', 'module.PM.EST_CURR_REV_DTL_YEAR', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Number', 4, 0, 80, 0, null) 
; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'ORDER_NUMBER', 'module.PM.WORK_ORDER_NUMBER', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 12, null, 10, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'PERIOD_FROM', '1', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Number', 3, 0, 60, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'PERIOD_TO', '12', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Number', 3, 0, 70, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'PLAN_TYPE', '''TEST''', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 10, null, 40, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'TIME_STAMP', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Date', 0, null, 130, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'USER_ID', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 18, null, 120, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1002, 'VERSION', '0', 'PWRPLANT', to_date('2012-11-12 13:55:28', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 3, null, 20, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1003, 'BUSINESS_TRANSACTION', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:42', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 4, null, 20, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1003, 'ORDER_NUMBER', 'module.PM.WORK_ORDER_NUMBER', 'PWRPLANT', to_date('2012-11-12 13:55:35', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 12, null, 10, 0, 
null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1003, 'PP_STATUS', 'module.PM.WO_STATUS_ID', 'PWRPLANT', to_date('2012-11-12 13:55:35', 'yyyy-mm-dd hh24:mi:ss'), 'Number', 22, 0, 30, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1003, 'SAP_STATUS', 'module.PM.WO_STATUS', 'PWRPLANT', to_date('2012-11-12 13:55:35', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 4, null, 40, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1003, 'TIME_STAMP', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:35', 'yyyy-mm-dd hh24:mi:ss'), 'Date', 0, null, 60, 0, null) ; 
INSERT INTO "CR_DATA_MOVER_MAP_COLUMNS" ("CR_DATA_MOVER_MAP_ID", "TO_COLUMN_NAME", "FROM_COLUMN_NAME", "USER_ID", "TIME_STAMP", "TYPE", "WIDTH", "DECIMAL", "ORDER", "PRIMARY_KEY", 
"FROM_FUNCTION") VALUES (1003, 'USER_ID', 'NULL', 'PWRPLANT', to_date('2012-11-12 13:55:35', 'yyyy-mm-dd hh24:mi:ss'), 'Character', 18, null, 50, 0, null) ; 

INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 11, 1000, '<Mappings / Filters>', '<Mappings / Filters>', null, null, null, null, 'replace', 0, 1, 1, 'Push', 'PWRPLANT', to_date('2012-10-03 14:58:59', 
'yyyy-mm-dd hh24:mi:ss'), ' ', 1000, 1000, 'SAP WO Header Extract', '<Mappings / Filters>') ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 14, 1000, 'cr_sap_wo_hdr_ss_stg,cr_sap_wo_hdr_ss', 'cr_sap_wo_hdr_out_stg', null, null, null, null, 'merge', 0, 1, 2, 'Push', 'PWRPLANT', 
to_date('2012-11-12 13:54:36', 'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Header Merge', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 17, 1000, ' ', 'cr_sap_wo_hdr_ss_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:54:36', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Header Clear STG', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 21, 1000, '<Mappings / Filters>', '<Mappings / Filters>', null, null, null, null, 'replace', 0, 1, 1, 'Push', 'PWRPLANT', to_date('2012-10-24 13:56:26', 
'yyyy-mm-dd hh24:mi:ss'), ' ', 1001, 1001, 'SAP WO Auth Amounts Extract', '<Mappings / Filters>') ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 24, 1000, 'cr_sap_wo_auth_amt_ss_stg,cr_sap_wo_auth_amt_ss', 'cr_sap_wo_auth_amt_stg', null, null, null, null, 'merge', 0, 1, 2, 'Push', 'PWRPLANT', 
to_date('2012-11-12 13:54:36', 'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Auth Amounts Merge', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 27, 1000, ' ', 'cr_sap_wo_auth_amt_ss_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-10-04 15:59:56', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Auth Amounts Clear STG', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 31, 1000, '<Mappings / Filters>', '<Mappings / Filters>', null, null, null, null, 'replace', 0, 1, 1, 'Push', 'PWRPLANT', to_date('2012-10-03 14:58:59', 
'yyyy-mm-dd hh24:mi:ss'), ' ', 1002, 1000, 'SAP WO Plan Amounts Extract', '<Mappings / Filters>') ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 34, 1000, 'cr_sap_wo_plan_amt_ss_stg,cr_sap_wo_plan_amt_ss', 'cr_sap_wo_plan_amt_stg', null, null, null, null, 'merge', 0, 1, 2, 'Push', 'PWRPLANT', 
to_date('2012-11-12 13:54:36', 'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Plan Amounts Merge', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 37, 1000, ' ', 'cr_sap_wo_plan_amt_ss_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-10-04 15:59:56', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Plan Amounts Clear STG', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 41, 1000, '<Mappings / Filters>', '<Mappings / Filters>', null, null, null, null, 'replace', 0, 1, 1, 'Push', 'PWRPLANT', to_date('2012-10-04 16:00:24', 
'yyyy-mm-dd hh24:mi:ss'), ' ', 1003, 1000, 'SAP WO Status Extract', '<Mappings / Filters>') ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 44, 1000, 
'select ''UPDATE'', sysdate, order_number,   case    when new_sap_status = ''CLSD'' then ''BABS''   when new_sap_status = ''TECO'' then ''BTAB''   when new_sap_status = ''CRTD'' then ''BFRZ''   when new_sap_status = ''RLSD''     and old_sap_status = ''CRTD'' then ''BFRE''   when new_sap_status = ''RLSD''     and old_sap_status = ''TECO'' then ''BUTA''   when new_sap_status = ''RLSD''     and old_sap_status = ''CLSD'' then ''BUTA''   end business_transaction,   new_pp_status,   new_sap_status,   null, null  from   (select order_number,     nvl(max(old_pp_status),1) old_pp_status,     nvl(max(old_sap_status),''CRTD'')  old_sap_status,     nvl(max(new_pp_status),8) new_pp_status,     nvl(max(new_sap_status),''CLSD'') new_sap_status   from    (select order_number,       pp_status old_pp_status, sap_status old_sap_status,       null new_pp_status, null new_sap_status    from cr_sap_wo_status_ss    union all    select order_number,       null, null,       pp_status, sap_status    from cr_sap_wo_status_ss_stg)   group by order_number)  where new_sap_status <> old_sap_status',
 'cr_sap_wo_status_stg', null, null, null, null, 'insert', 0, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:54:36', 'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 
'SAP WO Status Merge', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 45, 1000, 'cr_sap_wo_status_ss_stg', 'cr_sap_wo_status_ss', null, null, null, null, 'replace', 0, 1, 2, 'Push', 'PWRPLANT', to_date('2012-10-04 16:52:54', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Status Merge', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract', 47, 1000, ' ', 'cr_sap_wo_status_ss_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-10-04 15:59:59', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Status Clear STG', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 11, 1000, ' ', 'cr_sap_wo_hdr_ss', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-10-04 16:50:19', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Header Clear Snapshot', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 12, 1000, ' ', 'cr_sap_wo_hdr_out_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:53:38', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Header Clear STG', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 21, 1000, ' ', 'cr_sap_wo_auth_amt_ss', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:53:38', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Auth Amounts Clear Snapshot', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 22, 1000, ' ', 'cr_sap_wo_auth_amt_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:53:38', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Auth Amounts Clear STG', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 31, 1000, ' ', 'cr_sap_wo_plan_amt_ss', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:53:38', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Plan Amounts Clear Snapshot', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 32, 1000, ' ', 'cr_sap_wo_plan_amt_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:53:38', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Plan Amounts Clear STG', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 41, 1000, ' ', 'cr_sap_wo_status_ss', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:53:38', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Status Clear Snapshot', null) ; 
INSERT INTO "CR_DATA_MOVER" ("BATCH", "PRIORITY", "DATABASE_ID", "SQL", "TABLE_NAME", "PARAMETER1", "PARAMETER2", "PARAMETER3", "PARAMETER4", "ACTION", "COMMIT_COUNTER", 
"MAX_ERRORS", "STATUS", "DIRECTION", "USER_ID", "TIME_STAMP", "DELETE_WHERE_CLAUSE", "CR_DATA_MOVER_MAP_ID", "CR_DATA_MOVER_FILTER_ID", "DESCRIPTION", "TARGET_COLUMNS") VALUES 
('SAP WO Extract - Full Send', 42, 1000, ' ', 'cr_sap_wo_status_stg', null, null, null, null, 'delete', 999999999, 1, 2, 'Push', 'PWRPLANT', to_date('2012-11-12 13:53:38', 
'yyyy-mm-dd hh24:mi:ss'), ' ', null, null, 'SAP WO Status Clear STG', null) ; 
commit;

/* PP Integration */
INSERT INTO "PP_INTEGRATION" ("BATCH", "PRIORITY", "USER_ID", "TIME_STAMP", "COMPONENT", "ARGUMENT1", "ARGUMENT2", "ARGUMENT3", "ARGUMENT4", "ARGUMENT5", "ARGUMENT6", "ARGUMENT7", 
"ARGUMENT8", "ARGUMENT9", "ARGUMENT10", "STATUS", "DESCRIPTION") VALUES ('SAP WO Extract', 1, 'PWRPLANT', to_date('2012-10-01 17:16:49', 'yyyy-mm-dd hh24:mi:ss'), 
'Data Mover Batch', 'SAP WO Extract', null, null, null, null, null, null, null, null, null, 1, 'SAP WO Extract') ; 
commit;