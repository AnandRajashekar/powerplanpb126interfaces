insert into pp_system_control_company
(
	control_id, control_name, control_value, description,
	long_description,
	company_id
	
)
select max(control_id) + 1, 'sap adapter force operation', 'yes', 'dw_yes_no',
	'yes means force the operation map to sync with latest mapping from SAP. No means keep the original mapping',
	-1
from pp_system_control_company
;

commit;


insert into pp_system_control_company
(
	control_id, control_name, control_value, description,
	long_description,
	company_id
	
)
select max(control_id) + 1, 'sap adapter force order', 'yes', 'dw_yes_no',
	'yes means force the order number map to sync with latest mapping from SAP. No means keep the original mapping',
	-1
from pp_system_control_company
;

commit;