create table cr_sap_manalloc_docno (
id number(22,0),
doc_no varchar2(10),
itemno_acc number(22,0));

alter table cr_sap_manalloc_docno add (
constraint cr_sap_manalloc_docno_pk primary key (id) using index tablespace pwrplant_idx);

create or replace public synonym cr_sap_manalloc_docno for pwrplant.cr_sap_manalloc_docno;
grant all on cr_sap_manalloc_docno to pwrplant_role_dev;