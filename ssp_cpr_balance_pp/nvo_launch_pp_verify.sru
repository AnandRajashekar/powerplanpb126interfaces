HA$PBExportHeader$nvo_launch_pp_verify.sru
forward
global type nvo_launch_pp_verify from nonvisualobject
end type
end forward

global type nvo_launch_pp_verify from nonvisualobject
end type
global nvo_launch_pp_verify nvo_launch_pp_verify

type variables
nvo_cpr_control i_nvo_cpr_control 

string i_exe_name = "ssp_cpr_balance_pp.exe"
end variables
forward prototypes
public function longlong uf_launch_pp_verify (longlong a_company_ids[], date a_month)
end prototypes

public function longlong uf_launch_pp_verify (longlong a_company_ids[], date a_month);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  nvo_launch_pp_verify.uf_launch_pp_verify
//
//   Purpose  :  This function will insert records into the pp_verify_batch table and launch the pp_verify executable
//
//	 Inputs: 		a_company_ids[] 	:	list of company IDs to run pp_verify for 
//					a_month				:	The month to run pp_verify for 
//
//	 Return	:	longlong 	:	 1 = success
//								-1 = failure
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  12-17-2014      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

uo_ds_top ds_pp_verify_close

//setup i_nvo_cpr_control
if i_nvo_cpr_control.of_setupfromcompaniesandmonth(a_company_ids, a_month) <> 1 then 
	return -1 //of_setupfromcompaniesandmonth has required messaging within it, so just return here. 
end if

i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return -1
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_cpr_control.of_selectedcompanies(a_company_ids) = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return -1
end if 

///////////////////////////////////////
//
//	Begin actual work to launch pp_verify
//
///////////////////////////////////////

if upperbound(a_company_ids)  < 1 then 
	f_pp_msgs("No company IDs to process.")
	
	// Release the concurrency lock
	i_nvo_cpr_control.of_releaseProcess(process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)

	return 1
elseif upperbound(a_company_ids) = 1 then
	f_pp_msgs("Balance PowerPlan started for the following company:")
else
	f_pp_msgs("Balance PowerPlan started for the following companies:")
end if 

longlong i
string description
for i = 1 to upperbound(a_company_ids)
	select description
	into :description
	from company_setup 
	where company_id = :a_company_ids[i];
	f_pp_msgs("	" + description)
next 
	

longlong num_criteria, j, k, verify_id, total_company, len, ret
string vfy_cmd, ini_file, vfy_path, vfy_cmd_nopass

ds_pp_verify_close = create uo_ds_top
ds_pp_verify_close.dataobject = "dw_pp_verify_close"
ds_pp_verify_close.settransobject(sqlca) 
num_criteria = ds_pp_verify_close.retrieve()	

if num_criteria = 0 then
	f_pp_msgs("No Balancing Checks are selected"  )
	
	// Release the concurrency lock
	i_nvo_cpr_control.of_releaseProcess(process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)
	
	return 1
end if

total_company = upperbound(a_company_ids)

k = 0
for i = 1 to num_criteria
	k++
	verify_id  = ds_pp_verify_close.getitemnumber(i, 'verify_id')
	for j = 1 to total_company
		
		delete from pp_verify_batch
		where company_id = :a_company_ids[j] and verify_id = :verify_id and
		manual_batch = 1;
		
		//CBS maint 43245 - hardcode "version" to zero. 
		//	Only used for Tax verifies, which should never be processed through this EXE. 
		//	Tax module makes it own call to ppverify.exe
		insert into pp_verify_batch(company_id, verify_id, manual_batch, frequency, user_id,
		version)
		values(:a_company_ids[j], :verify_id, 1, 0, user, 0); 
		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("Error inserting into pp_verify_batch " + sqlca.sqlerrtext)
		end if
		
	next
	if sqlca.sqlcode < 0 then
		f_pp_msgs("Error inserting into pp_verify_batch " + sqlca.sqlerrtext)
		
		// Release the concurrency lock
		i_nvo_cpr_control.of_releaseProcess(process_msg)
		f_pp_msgs("Release Process Status: " + process_msg)
		
		return -1
	end if
next
	
commit;

vfy_cmd = f_pp_system_control('VERIFY AS DOS COMMAND')
if vfy_cmd = '' then
	f_pp_msgs("ERROR: Verify program name is missing in system control table.")
	
	// Release the concurrency lock
	i_nvo_cpr_control.of_releaseProcess(process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)
	
	return -1
end if
	
ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "pwrplant.ini")
len = len(ini_file)
for i = len to 1 step -1
	 if mid(ini_file,i,1) = "\" then
			vfy_path = mid(ini_file,1,i)
			exit
	 end if  
next

   
	
// add the path if there is not path already in the system control
if pos(vfy_cmd, ':\') = 0 then
  	vfy_cmd = vfy_path + vfy_cmd
else
	vfy_path = mid(vfy_cmd, 1, pos(vfy_cmd, ' '))
end if

vfy_cmd_nopass = vfy_cmd

vfy_cmd = vfy_cmd + ' ' + sqlca.ServerName + ',' + sqlca.DBMS + ',' + sqlca.userid + ',' + &
	 sqlca.LogPass + ',' + 'close' +  ',' + string(month(date(a_month))) + ',' + &
	 string(year(date(a_month)))
vfy_cmd_nopass = vfy_cmd_nopass + ' ' + sqlca.ServerName + ',' + sqlca.DBMS + ',' + sqlca.userid + ',' + &
	 '******,' + 'close' +  ',' + string(month(date(a_month))) + ',' + &
	 string(year(date(a_month)))


vfy_cmd = f_replace_string(vfy_cmd, ',trace ', ',', 'all')
vfy_cmd = f_replace_string(vfy_cmd, ',trs ', ',', 'all')

vfy_cmd_nopass = f_replace_string(vfy_cmd_nopass, ',trace ', ',', 'all')
vfy_cmd_nopass = f_replace_string(vfy_cmd_nopass, ',trs ', ',', 'all')

f_pp_msgs("Launching verify with command line: " + vfy_cmd_nopass)

ret = run(vfy_cmd)

if ret = -1 then
	f_pp_msgs("ERROR: Verify Program Could not be Started: " + vfy_cmd_nopass)
	
	// Release the concurrency lock
	i_nvo_cpr_control.of_releaseProcess(process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)
	
	return -1
else
	f_pp_msgs(" ")
	f_pp_msgs(" ")
	f_pp_msgs("Verify program started in the background.")
	f_pp_msgs(" ")
	f_pp_msgs("Please check the online logs and then the results window.")
	f_pp_msgs(" ")
end if

// Release the concurrency lock
i_nvo_cpr_control.of_releaseProcess(process_msg)
f_pp_msgs("Release Process Status: " + process_msg)

return 1 
end function

on nvo_launch_pp_verify.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_launch_pp_verify.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;

i_nvo_cpr_control.of_constructor()
end event

