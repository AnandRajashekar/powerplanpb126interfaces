HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_pp_verify_close from datawindow within w_datawindows
end type
type dw_pp_interface_dates_check from datawindow within w_datawindows
end type
type dw_interface_dates_all from datawindow within w_datawindows
end type
type dw_cpr_control from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer width = 4754
integer height = 1980
boolean titlebar = true
string title = "w_datawindows"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_pp_verify_close dw_pp_verify_close
dw_pp_interface_dates_check dw_pp_interface_dates_check
dw_interface_dates_all dw_interface_dates_all
dw_cpr_control dw_cpr_control
end type
global w_datawindows w_datawindows

event open;//comment so i can save
end event

on w_datawindows.create
this.dw_pp_verify_close=create dw_pp_verify_close
this.dw_pp_interface_dates_check=create dw_pp_interface_dates_check
this.dw_interface_dates_all=create dw_interface_dates_all
this.dw_cpr_control=create dw_cpr_control
this.Control[]={this.dw_pp_verify_close,&
this.dw_pp_interface_dates_check,&
this.dw_interface_dates_all,&
this.dw_cpr_control}
end on

on w_datawindows.destroy
destroy(this.dw_pp_verify_close)
destroy(this.dw_pp_interface_dates_check)
destroy(this.dw_interface_dates_all)
destroy(this.dw_cpr_control)
end on

type dw_pp_verify_close from datawindow within w_datawindows
integer x = 2807
integer y = 592
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_pp_verify_close"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_interface_dates_check from datawindow within w_datawindows
integer x = 3250
integer y = 1212
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_interface_dates_all from datawindow within w_datawindows
integer x = 1234
integer y = 864
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_control from datawindow within w_datawindows
integer x = 338
integer y = 820
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

