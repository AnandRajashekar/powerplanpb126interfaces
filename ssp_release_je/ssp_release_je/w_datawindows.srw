HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_4 from datawindow within w_datawindows
end type
type dw_3 from datawindow within w_datawindows
end type
type dw_2 from datawindow within w_datawindows
end type
type dw_1 from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer x = 837
integer y = 388
integer width = 1970
integer height = 1212
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
dw_4 dw_4
dw_3 dw_3
dw_2 dw_2
dw_1 dw_1
end type
global w_datawindows w_datawindows

type variables

end variables
on w_datawindows.create
this.dw_4=create dw_4
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_1=create dw_1
this.Control[]={this.dw_4,&
this.dw_3,&
this.dw_2,&
this.dw_1}
end on

on w_datawindows.destroy
destroy(this.dw_4)
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_1)
end on

type dw_4 from datawindow within w_datawindows
integer x = 782
integer y = 480
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_3 from datawindow within w_datawindows
integer x = 50
integer y = 488
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_2 from datawindow within w_datawindows
integer x = 754
integer y = 36
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_gl_transaction"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_datawindows
integer x = 37
integer y = 40
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

