HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$dummy
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong i_wo_id
string i_exe_name = 'populate_matlrec.exe'
end variables

forward prototypes
public function integer uf_clear_stg_tables ()
public function integer uf_insert_cr_matl_stg (boolean a_transfer_rows)
public function integer uf_load_matlrec (string a_update_where, string a_update_matltrans)
public function integer uf_process_matlrec (string a_sql, string a_where, boolean a_transfer, string a_update_where)
public function integer uf_backfill_to_detail_table ()
public function longlong uf_read ()
public function integer uf_mark_detail_tables_never_send ()
public function integer uf_set_max_src_ids ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function integer uf_clear_stg_tables ();f_pp_msgs("DELETING: deleting all entries in staging tables:")
f_pp_msgs("   cr_matlrec_stg")
f_pp_msgs("   matlrec_trans_stg")

delete from cr_matlrec_stg;
delete from matlrec_trans_stg;

return 1
end function

public function integer uf_insert_cr_matl_stg (boolean a_transfer_rows);longlong count_inserted

f_pp_msgs("~nINSERTING: inserting into the cr_matlrec_stg table")

if a_transfer_rows = true then
	
	insert into cr_matlrec_stg
	(select * from cr_matlrec_stg_temp cmv
	where cmv.interface_batch_id in
	(select to_char(journal_id) from cr_journals));
	IF sqlca.sqlcode < 0 THEN
		f_pp_msgs("ERROR: executing sql: insert into cr_matlrec_stg (transfers) ")
		f_pp_msgs("   ERROR message: " + sqlca.sqlerrtext)
		return -1
	end if
	
else
	
	////	this is called first, so clear and repopulate the "temp" table, cr_matlrec_stg_temp.
	////	the point of this table was to avoid hitting the view twice, by using this table we only hit the view once.
	
	delete from cr_matlrec_stg_temp;
	IF sqlca.sqlcode < 0 THEN
		f_pp_msgs("ERROR: executing sql: delete from cr_matlrec_stg_temp")
		f_pp_msgs("   ERROR message: " + sqlca.sqlerrtext)
		return -1
	end if
	
	
	insert into cr_matlrec_stg_temp
		select * from cr_matlrec_view;
	IF sqlca.sqlcode < 0 THEN
		f_pp_msgs("ERROR: executing sql: insert into cr_matlrec_stg_temp")
		f_pp_msgs("   ERROR message: " + sqlca.sqlerrtext)
		return -1
	end if
	
	insert into cr_matlrec_stg
	(select * from cr_matlrec_stg_temp cmv
	where cmv.interface_batch_id not in
	(select to_char(journal_id) from cr_journals));
	IF sqlca.sqlcode < 0 THEN
		f_pp_msgs("ERROR: executing sql: insert into cr_matlrec_stg (non transfers)")
		f_pp_msgs("   ERROR message: " + sqlca.sqlerrtext)
		return -1
	end if

end if

select	count(*)
into		:count_inserted
from		cr_matlrec_stg;
f_pp_msgs("   INSERTED: " + string(count_inserted) +&
				" rows from the cr into the staging table")
				
return 1
end function

public function integer uf_load_matlrec (string a_update_where, string a_update_matltrans);longlong count_inserted, rtn
string sql_update
string sql_update_matl

select	count(*)
into		:count_inserted
from		matlrec_trans_stg;
f_pp_msgs("      INSERTED: " + string(count_inserted) +&
				" rows into the matlrec staging table.")
				
// set the matlrec_trans_id for the ids that already exist in the table
sql_update_matl = &
	"update matlrec_trans_stg" +&
	" set matlrec_trans_stg.matlrec_trans_id = " +&
	" (select matlrec_trans.matlrec_trans_id" +&
	" from matlrec_trans where " + a_update_matltrans +&
	" and matlrec_trans.work_order_id = matlrec_trans_stg.work_order_id)" +&
	" where exists (select 1 from matlrec_trans " +&
						" where matlrec_trans.work_order_id = matlrec_trans_stg.work_order_id and " +&
						a_update_matltrans + ")"

execute immediate :sql_update_matl;
IF sqlca.sqlcode < 0 THEN
	f_pp_msgs("~nERROR: error updating matlrec_trans_id for groups already in matlrec_trans table")
	return -1
end if

// set the matlrec_trans_id
f_pp_msgs("~nSETTING: setting the matlrec_trans_id" +&
			" in the matlrec_trans_stg table")
update matlrec_trans_stg set matlrec_trans_id = pwrplant1.nextval
where nvl(matlrec_trans_id,0) = 0;
IF sqlca.sqlcode < 0 THEN
	return -1
end if

f_pp_msgs("~nUPDATING: updating the quantities in matlrec_trans for" +&
			" rows that also exist in the CR and have not been read into matlrec yet")
update matlrec_trans mt set
(
	mt.amount,
	mt.est_amount,
	mt.qty_charged,
	mt.qty_estimated,
	mt.qty_unreconciled,
	mt.qty_installed,
	mt.qty_surplus,
	mt.qty_surplus_transfer,
	mt.qty_scrapped,
	mt.qty_return_to_inv,
	mt.qty_other
) =
(select 
	mt.amount + mts.amount,
	mt.est_amount + mts.est_amount,
	mt.qty_charged + mts.qty_charged,
	mt.qty_estimated + mts.qty_estimated,
	mt.qty_unreconciled + mts.qty_unreconciled,
	mt.qty_installed + mts.qty_installed,
	mt.qty_surplus + mts.qty_surplus,
	mt.qty_surplus_transfer + mts.qty_surplus_transfer,
	mt.qty_scrapped + mts.qty_scrapped,
	mt.qty_return_to_inv + mts.qty_return_to_inv,
	mt.qty_other + mts.qty_other
from matlrec_trans_stg mts
where mts.matlrec_trans_id = mt.matlrec_trans_id
)
where exists (select 1 from matlrec_trans_stg mts
					where mts.matlrec_trans_id = mt.matlrec_trans_id);

IF sqlca.sqlcode < 0 THEN
	f_pp_msgs("~nERROR: error updating the quantities in matlrec_trans from the staging table")
	return -1
end if

// ### DJL - Maint 7532 - Add custom function to MatlRec
rtn = f_matlrec_custom(2)
if rtn < 0 then
	rollback;
	return -1
end if

// inserting from the matlrec_stg into the real table
f_pp_msgs("~nINSERTING: inserting into the matlrec_trans table from stg")
insert into matlrec_trans
(select * from matlrec_trans_stg 
where not exists (select 1 from matlrec_trans mt
						where mt.matlrec_trans_id =
							matlrec_trans_stg.matlrec_trans_id)
);
IF sqlca.sqlcode < 0 THEN
	return -1
end if

// backfill the matlrec_trans_id to the matlrec_stg table
f_pp_msgs("~nBACKFILL: backfill the matlrec_trans_id to the detail source tables")
f_pp_msgs("  backfilling: cr_matlrec_stg")
sql_update = &
	"update cr_matlrec_stg" +&
	" set cr_matlrec_stg.matlrec_trans_id =" +&
	" (select matlrec_trans_stg.matlrec_trans_id" +&
	" from matlrec_trans_stg" +&
	" where matlrec_trans_stg.work_order_id = cr_matlrec_stg.work_order_id" +&
	a_update_where + ")"
execute immediate :sql_update;
IF sqlca.sqlcode < 0 THEN
	return -1
end if

// BACKFILL FROM CR_MATLREC_STG TO THE DETAIL TABLE THE ROW CAME FROM
uf_backfill_to_detail_table()
IF sqlca.sqlcode < 0 THEN
	return -1
end if

return 1
end function

public function integer uf_process_matlrec (string a_sql, string a_where, boolean a_transfer, string a_update_where);longlong count

uf_clear_stg_tables()
uf_insert_cr_matl_stg(a_transfer)
IF sqlca.sqlcode < 0 THEN
	f_pp_msgs("ERROR: executing sql: ")
	f_pp_msgs("   ERROR message: " + sqlca.sqlerrtext)
	return -1
end if

select count(*)
into :count
from cr_matlrec_stg;

if isnull(count) or count = 0 then
	f_pp_msgs("  There are ZERO transactions to process")
	return 1
end if

f_pp_msgs("~nINSERTING: inserting into the matlrec_trans_stg table")
f_pp_msgs("   This will summarize the rows in the cr_matlrec_stg table")
// execute the rows that are not a transfer row

execute immediate :a_sql;
IF sqlca.sqlcode < 0 THEN
	f_pp_msgs("ERROR: executing sql: ")
	f_pp_msgs("   ERROR message: " + sqlca.sqlerrtext)
	return -1
end if

uf_load_matlrec(a_where, a_update_where)
IF sqlca.sqlcode < 0 THEN
	f_pp_msgs("ERROR: executing sql: ")
	f_pp_msgs("   ERROR message: " + sqlca.sqlerrtext)
	return -1
end if

return 1
end function

public function integer uf_backfill_to_detail_table ();longlong min_src, max_src, i, count
string source_name
string sql_update

select min(source_id), max(source_id)
into :min_src, :max_src
from matlrec_sources
;
IF sqlca.sqlcode < 0 THEN
	return -1
end if

// loop through each source and backfill the matlrec trans id
for i = min_src to max_src
	select count(*)
	into :count
	from matlrec_sources
	where source_id = :i
	;
	
	// if there is a gap in the source ids
	if isnull(count) or count = 0 then
		continue
	end if
	
	select table_name
	into :source_name
	from matlrec_sources
	where source_id = :i
	;
	
	f_pp_msgs("  backfilling: " + source_name)

	/*  Modified by Cathy for RT#1766755 (3/17/2008)
	sql_update = "update " + source_name +&
            " sn set sn.matlrec_trans_id = " +&
            " (select cms.matlrec_trans_id" +&
            " from cr_matlrec_stg cms" +&
            " where cms.source = '" + source_name + "'" +&
            " and cms.id = sn.id)" +&
            " where exists" +&
            " (select 1 from cr_matlrec_stg cms2" +&
            " where cms2.source = '" + source_name + "'" +&
            " and cms2.id = sn.id)" */

	sql_update = "update " + source_name +&
		" sn set sn.matlrec_trans_id = " +&
		" (select cms.matlrec_trans_id" +&
		" from cr_matlrec_stg cms" +&
		" where cms.source = '" + source_name + "'" +&
		" and cms.id = sn.id)" +&
		" where sn.id in " +& 
		" (select id from cr_matlrec_stg cms2" +& 
		" where cms2.source = '" + source_name + "')" 

	execute immediate :sql_update;
	
	IF sqlca.sqlcode < 0 THEN
		return -1
	end if
	
next

return 1
end function

public function longlong uf_read ();// These will be used to hold the unique list of custom_fields
uo_ds_top 	ds_custom_fields
string 		sql_custom
string		custom_fields_for_select
string		custom_fields_for_insert		//same as above except without
														//the table dot notation
string		custom_fields_group_by
string		cf_label
longlong	i, num_custom_fields
// END UNIQUE custom fields

// These variables are used for determining the group by
longlong include_group_by
// END GROUP by vars

// These variables are used to build the sql
string	sql_insert, sql_execute
string	sql_execute_trans
string	sql_inner_select, sql_inner_from 
string	sql_inner_where, sql_inner_group
string	sql_inner_sel_trans, sql_inner_where_trans
string	sql_update_where
string	sql_update_matltrans
// END SQL strings

//Holds return values from function
longlong rtn
//end return values

// ### DJL - Maint 7532 - Add custom function to MatlRec
rtn = f_matlrec_custom(1)
if rtn < 0 then
	rollback;
	return -1
end if


// ### BSB 20090302
// first set the max ids
if uf_set_max_src_ids() < 0 then
	return -1
end if

// obtain the distinct list of custom fields
ds_custom_fields = create uo_ds_top
sql_custom = &		
	"select matlrec_label, is_group_by" +&
	" from matlrec_dist_labels" +&
	" order by matlrec_label"
f_create_dynamic_ds(	ds_custom_fields, 'grid', &
							sql_custom, sqlca, true)
// END OBTAIN custom fields

// Build the custom_fields_for_select string
// This string represents the portion of the select
// statement that is custom fields
num_custom_fields = ds_custom_fields.rowCount()
custom_fields_for_select = ""
custom_fields_for_insert = ""
custom_fields_group_by = ""
sql_update_where = ""
sql_update_matltrans = ""
for i = 1 to num_custom_fields
	cf_label = ds_custom_fields.getItemString(i, "matlrec_label")
	include_group_by = ds_custom_fields.getItemNumber(i, "is_group_by")
	
	custom_fields_for_insert = custom_fields_for_insert +&
		" " + cf_label + ","	
	
	// If the field needs to be included in the group by
	// then include it
	if include_group_by = 1 then
		custom_fields_for_select = custom_fields_for_select +&
			" nvl(cr_matlrec_stg." + cf_label + ", ' ') as " + cf_label + ","
		custom_fields_group_by = custom_fields_group_by +&
			" nvl(cr_matlrec_stg." + cf_label + ", ' '), "
		sql_update_where = sql_update_where +&
			" and nvl(cr_matlrec_stg." +&
				cf_label + ", ' ') = matlrec_trans_stg." + cf_label
		sql_update_matltrans = sql_update_matltrans +&
			" matlrec_trans." +&
				cf_label + " = matlrec_trans_stg." + cf_label + " and "
	else
		// Grab the maximum value because you don't want to group by this
		custom_fields_for_select = custom_fields_for_select +&
			" nvl(max(cr_matlrec_stg." + cf_label + "), ' ') as " + cf_label + ", "
	end if
next
sql_update_matltrans = left(sql_update_matltrans, len(sql_update_matltrans)-4)

// END BUILD the custom field portion of the select
sql_insert = &
	"insert into matlrec_trans_stg (" +&
	" " + custom_fields_for_insert +&
	" matlrec_trans_id," +&
	" work_order_id," +&
	" amount," +&
	" est_amount," +&
	" qty_charged," +&
	" qty_estimated," +&
	" qty_unreconciled," +&
	" qty_installed," +&
	" qty_surplus," +&
	" qty_surplus_transfer," +&
	" qty_scrapped," +&
	" qty_return_to_inv," +&
	" qty_other)"
	
sql_inner_select = &
	" select " +&
	" " + custom_fields_for_select +&
	" 0 as matlrec_trans_id," +&
	" cr_matlrec_stg.work_order_id as work_order_id," +&
	" sum(cr_matlrec_stg.amount) as amount," +&
	" 0 as est_amount," +&
	" sum(cr_matlrec_stg.quantity) as qty_charged," +&
	" 0 as qty_estimated," +&
	" sum(cr_matlrec_stg.quantity) as qty_unreconciled," +&
	" 0 as qty_installed," +&
	" 0 as qty_surplus," +&
	" 0 as qty_surplus_transfer," +&
	" 0 as qty_scrapped," +&
	" 0 as qty_return_to_inv," +&
	" 0 as qty_other"
	
// NOW build the inner select for transfer materials
// in this case the negative quantity (the credit) should go to
// qty_surplus_tranfer instead of qty_unreconciled.
//
// but the positive qty (debit to the receiving work order) should be treated as reconcilable.
sql_inner_sel_trans = &
	" select " +&
	" " + custom_fields_for_select +&
	" 0 as matlrec_trans_id," +&
	" cr_matlrec_stg.work_order_id as work_order_id," +&
	" sum(cr_matlrec_stg.amount) as amount," +&
	" 0 as est_amount," +&
	" sum(cr_matlrec_stg.quantity) as qty_charged," +&
	" 0 as qty_estimated," +&
	" sum( decode(sign(cr_matlrec_stg.quantity),-1,0,cr_matlrec_stg.quantity) ) as qty_unreconciled," +&
	" 0 as qty_installed," +&
	" 0 as qty_surplus," +&
	" sum( decode(sign(cr_matlrec_stg.quantity),-1,cr_matlrec_stg.quantity,0) ) as qty_surplus_transfer," +&
	" 0 as qty_scrapped," +&
	" 0 as qty_return_to_inv," +&
	" 0 as qty_other"
	
sql_inner_where = &
	" where cr_matlrec_stg.interface_batch_id not in" +&
	" (select to_char(journal_id) from cr_journals)"

// the where clause for the transfer materials
sql_inner_where_trans = &
	" where cr_matlrec_stg.interface_batch_id in" +&
	" (select to_char(journal_id) from cr_journals)"
	
sql_inner_from = " from cr_matlrec_stg"

sql_inner_group = " group by " +&
	custom_fields_group_by + " cr_matlrec_stg.work_order_id"
	
sql_execute = &
	sql_insert + " " +&
	sql_inner_select + " " +&
	sql_inner_from + " " +&
	sql_inner_where + " " +&
	sql_inner_group + " "	

sql_execute_trans = &
	sql_insert + " " +&
	sql_inner_sel_trans + " " +&
	sql_inner_from + " " +&
	sql_inner_where_trans + " " +&
	sql_inner_group + " "


////  Test email function 
////==================================================================
//string ls_msg
//longlong	 ll_id 
//
//select messages into :ls_msg from  pp_processes_notifications  
//where process_id = 30 and users = 'rr804529' ; 
//
//if (IsNull(ls_msg)) or (ls_msg = '') then 
//	select process_id into :ll_id from  pp_processes_notifications  
//	where users = 'rr804529' and messages > ' ';
//end if
//IF sqlca.sqlcode < 0 THEN
//	return -1
//end if
////==================================================================


// PERFORM THE MATLREC POPULATION FOR THE ENTRIES THAT
// DID NOT COME FROM A TRANSFER
// WE WILL DO THE ONES THAT COME FROM A TRANSFER NEXT
f_pp_msgs("~n~rPERFORMING INSERT FOR ROWS THAT WERE NOT INSTANTIATED" +&
				" BY A TRANSFER")
uf_process_matlrec(sql_execute, sql_update_where, false, sql_update_matltrans)
IF sqlca.sqlcode < 0 THEN
	return -1
end if

// PERFORM THE MATLREC POPULATION FOR THE ENTRIES THAT
// COME FROM A TRANSFER
uf_clear_stg_tables()
f_pp_msgs("~n~rPERFORMING INSERT FOR ROWS THAT WERE INSTANTIATED" +&
				" BY A TRANSFER")
uf_process_matlrec(sql_execute_trans, sql_update_where, true, sql_update_matltrans)
IF sqlca.sqlcode < 0 THEN
	return -1
end if

uf_clear_stg_tables()
IF sqlca.sqlcode < 0 THEN
	return -1
end if

uf_mark_detail_tables_never_send()
IF sqlca.sqlcode < 0 THEN
	return -1
end if

// ### DJL - Maint 7532 - Add commit
commit;

// ### DJL - Maint 7532 - Add custom function to MatlRec
rtn = f_matlrec_custom(3)
if rtn < 0 then
	rollback;
	return -1
else
	commit;
end if

return 1
end function

public function integer uf_mark_detail_tables_never_send ();longlong min_src, max_src, i, count, max_src_id
string source_name
string sql_update

select min(source_id), max(source_id)
into :min_src, :max_src
from matlrec_sources
;
IF sqlca.sqlcode < 0 THEN
	return -1
end if

// loop through each source and backfill the matlrec trans id
for i = min_src to max_src
	select count(*)
	into :count
	from matlrec_sources
	where source_id = :i
	;
	
	// if there is a gap in the source ids
	if isnull(count) or count = 0 then
		continue
	end if
	
	select table_name
	into :source_name
	from matlrec_sources
	where source_id = :i
	;
	
	f_pp_msgs("  Marking to -id for records that fail where clause: " + source_name)

	
	// ### DJL - 062011
	// Maint 7843 - Gather max source id here. Helps with performance
	select a.max_source_id 
	into :max_src_id
	from matlrec_max_source_ids a 
	where a.source_id = :i;
	
	// ### BSB
	// now we want to update the matlrec_trans_id = -id
	// if it is still 0 since it did not match any of the where clause
	sql_update = "update " + source_name +&
		" sn set sn.matlrec_trans_id = -id" +&
		" where sn.matlrec_trans_id = 0" +&
		" and sn.id <= " + string(max_src_id)//(select a.max_source_id from matlrec_max_source_ids a where a.source_id = " + string(i) + ")"

	execute immediate :sql_update;
	
	IF sqlca.sqlcode < 0 THEN
		return -1
	end if
	
next

return 1
end function

public function integer uf_set_max_src_ids ();longlong min_src, max_src, i, count
string source_name
string sql_insert

sqlca.truncate_table("matlrec_max_source_ids")

select min(source_id), max(source_id)
into :min_src, :max_src
from matlrec_sources
;
IF sqlca.sqlcode < 0 THEN
	return -1
end if

// loop through each source and backfill the matlrec trans id
for i = min_src to max_src
	select count(*)
	into :count
	from matlrec_sources
	where source_id = :i
	;
	
	// if there is a gap in the source ids
	if isnull(count) or count = 0 then
		continue
	end if
	
	select table_name
	into :source_name
	from matlrec_sources
	where source_id = :i
	;
	
	f_pp_msgs("  Grabbing max_id for: " + source_name)

	// ### BSB
	// now we want to grab the max_ids so that we can update
	// records to not process in future
	
	// ### DJL - 062011
	// Maint 7843 - Explicity list columns of matlrec_max_source_ids
	sql_insert = "insert into matlrec_max_source_ids (source_id, max_source_id)" +&
		" select " + string(i) +&
		", max(id)" +&
		" from " + source_name
	execute immediate :sql_insert;
	
	IF sqlca.sqlcode < 0 THEN
		return -1
	end if
next

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_populate_matlrec_custom_version nvo_populate_matlrec_custom_version

choose case a_pbd_name
	case 'populate_matlrec_custom.pbd'
		return nvo_populate_matlrec_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

