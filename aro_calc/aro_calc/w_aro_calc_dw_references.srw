HA$PBExportHeader$w_aro_calc_dw_references.srw
forward
global type w_aro_calc_dw_references from window
end type
type dw_22 from datawindow within w_aro_calc_dw_references
end type
type dw_21 from datawindow within w_aro_calc_dw_references
end type
type dw_20 from datawindow within w_aro_calc_dw_references
end type
type dw_19 from datawindow within w_aro_calc_dw_references
end type
type dw_18 from datawindow within w_aro_calc_dw_references
end type
type dw_17 from datawindow within w_aro_calc_dw_references
end type
type dw_16 from datawindow within w_aro_calc_dw_references
end type
type dw_15 from datawindow within w_aro_calc_dw_references
end type
type dw_14 from datawindow within w_aro_calc_dw_references
end type
type dw_13 from datawindow within w_aro_calc_dw_references
end type
type dw_12 from datawindow within w_aro_calc_dw_references
end type
type dw_11 from datawindow within w_aro_calc_dw_references
end type
type dw_10 from datawindow within w_aro_calc_dw_references
end type
type dw_9 from datawindow within w_aro_calc_dw_references
end type
type dw_8 from datawindow within w_aro_calc_dw_references
end type
type dw_7 from datawindow within w_aro_calc_dw_references
end type
type dw_6 from datawindow within w_aro_calc_dw_references
end type
type dw_5 from datawindow within w_aro_calc_dw_references
end type
type dw_4 from datawindow within w_aro_calc_dw_references
end type
type dw_3 from datawindow within w_aro_calc_dw_references
end type
type dw_2 from datawindow within w_aro_calc_dw_references
end type
type dw_1 from datawindow within w_aro_calc_dw_references
end type
end forward

global type w_aro_calc_dw_references from window
integer width = 4754
integer height = 2200
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_22 dw_22
dw_21 dw_21
dw_20 dw_20
dw_19 dw_19
dw_18 dw_18
dw_17 dw_17
dw_16 dw_16
dw_15 dw_15
dw_14 dw_14
dw_13 dw_13
dw_12 dw_12
dw_11 dw_11
dw_10 dw_10
dw_9 dw_9
dw_8 dw_8
dw_7 dw_7
dw_6 dw_6
dw_5 dw_5
dw_4 dw_4
dw_3 dw_3
dw_2 dw_2
dw_1 dw_1
end type
global w_aro_calc_dw_references w_aro_calc_dw_references

on w_aro_calc_dw_references.create
this.dw_22=create dw_22
this.dw_21=create dw_21
this.dw_20=create dw_20
this.dw_19=create dw_19
this.dw_18=create dw_18
this.dw_17=create dw_17
this.dw_16=create dw_16
this.dw_15=create dw_15
this.dw_14=create dw_14
this.dw_13=create dw_13
this.dw_12=create dw_12
this.dw_11=create dw_11
this.dw_10=create dw_10
this.dw_9=create dw_9
this.dw_8=create dw_8
this.dw_7=create dw_7
this.dw_6=create dw_6
this.dw_5=create dw_5
this.dw_4=create dw_4
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_1=create dw_1
this.Control[]={this.dw_22,&
this.dw_21,&
this.dw_20,&
this.dw_19,&
this.dw_18,&
this.dw_17,&
this.dw_16,&
this.dw_15,&
this.dw_14,&
this.dw_13,&
this.dw_12,&
this.dw_11,&
this.dw_10,&
this.dw_9,&
this.dw_8,&
this.dw_7,&
this.dw_6,&
this.dw_5,&
this.dw_4,&
this.dw_3,&
this.dw_2,&
this.dw_1}
end on

on w_aro_calc_dw_references.destroy
destroy(this.dw_22)
destroy(this.dw_21)
destroy(this.dw_20)
destroy(this.dw_19)
destroy(this.dw_18)
destroy(this.dw_17)
destroy(this.dw_16)
destroy(this.dw_15)
destroy(this.dw_14)
destroy(this.dw_13)
destroy(this.dw_12)
destroy(this.dw_11)
destroy(this.dw_10)
destroy(this.dw_9)
destroy(this.dw_8)
destroy(this.dw_7)
destroy(this.dw_6)
destroy(this.dw_5)
destroy(this.dw_4)
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_1)
end on

type dw_22 from datawindow within w_aro_calc_dw_references
integer x = 3054
integer y = 1204
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_21 from datawindow within w_aro_calc_dw_references
integer x = 2912
integer y = 740
integer width = 686
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "dw_cpr_company"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_20 from datawindow within w_aro_calc_dw_references
integer x = 2185
integer y = 1004
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_19 from datawindow within w_aro_calc_dw_references
integer x = 2734
integer y = 304
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_18 from datawindow within w_aro_calc_dw_references
integer x = 2162
integer y = 620
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_17 from datawindow within w_aro_calc_dw_references
integer x = 1678
integer y = 168
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_16 from datawindow within w_aro_calc_dw_references
integer x = 1152
integer y = 1004
integer width = 686
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "dw_aro_accr_dtl_allo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_15 from datawindow within w_aro_calc_dw_references
integer x = 1061
integer y = 928
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_surviving_percentage"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_14 from datawindow within w_aro_calc_dw_references
integer x = 987
integer y = 864
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_reserve_ratios"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_13 from datawindow within w_aro_calc_dw_references
integer x = 914
integer y = 800
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_mortality_curve_points_ret"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_12 from datawindow within w_aro_calc_dw_references
integer x = 841
integer y = 736
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_aro_work_order_summary"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_11 from datawindow within w_aro_calc_dw_references
integer x = 768
integer y = 672
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_aro_wo_charges_ext_acct"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_10 from datawindow within w_aro_calc_dw_references
integer x = 695
integer y = 608
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_aro_wo_charges"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_9 from datawindow within w_aro_calc_dw_references
integer x = 622
integer y = 544
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_aro_transition_month_end"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_8 from datawindow within w_aro_calc_dw_references
integer x = 549
integer y = 480
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_aro_settle_months"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_7 from datawindow within w_aro_calc_dw_references
integer x = 475
integer y = 416
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_aro_reg_activity_insert"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_6 from datawindow within w_aro_calc_dw_references
integer x = 402
integer y = 352
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_aro_layer_work_order"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_5 from datawindow within w_aro_calc_dw_references
integer x = 329
integer y = 288
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_aro_layer_stream_downward_adj_me"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_4 from datawindow within w_aro_calc_dw_references
integer x = 256
integer y = 224
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_aro_layer_liability"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_3 from datawindow within w_aro_calc_dw_references
integer x = 183
integer y = 160
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_aro_est_settle_dates"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_2 from datawindow within w_aro_calc_dw_references
integer x = 110
integer y = 96
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_aro_active_by_comp"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_aro_calc_dw_references
integer x = 37
integer y = 32
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_aro_active_by_comp"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

