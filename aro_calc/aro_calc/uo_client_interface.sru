HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = "ssp_aro_calc.exe"

nvo_cpr_control i_nvo_cpr_control
end variables

forward prototypes
public function longlong uf_read ()
public function boolean of_aro_calc_main ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-27-2014      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
longlong rtn_success, rtn_failure, rtn
string process_msg
w_aro_calc_dw_references w

f_pp_msgs("Begin uf_read...")

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS';

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE';


if not of_aro_calc_main() then
	i_nvo_cpr_control.of_log_failed_companies("PROCESS ARO CALCULATION")
	rtn = rtn_failure
elseif upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
	i_nvo_cpr_control.of_log_failed_companies("PROCESS ARO CALCULATION")
	rtn = rtn_failure
else
	rtn = rtn_success
end if

// Release the concurrency lock
i_nvo_cpr_control.of_releaseProcess(process_msg)
f_pp_msgs("Release Process Status: " + process_msg)

f_pp_msgs("End ARO Processing: " + string(now()))

return rtn
end function

public function boolean of_aro_calc_main ();longlong rtn, fiscal_month, rtn_fm, num_failed_companies
longlong total_company, cc, rc
string comp_str, sqls
uo_ds_top ds_mass_aros
nvo_aro_calc aro_calc

// Construct the nvo
i_nvo_cpr_control.of_constructor()

// Get the ssp parameters
i_nvo_cpr_control.i_company_idx[]  = g_ssp_parms.long_arg[]
//i_nvo_cpr_control.i_month = datetime(g_ssp_parms.date_arg[1])
//i_nvo_cpr_control.i_months[1] = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_original_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return false
end if

// Get the descriptions
rtn = i_nvo_cpr_control.of_getDescriptionsFromIds(i_nvo_cpr_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_cpr_control.of_selectedCompanies(i_nvo_cpr_control.i_company_idx) = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do " + &
				"not line up")
	return false
end if

//Check PP Calendar if row exists check for month 12
// If no row then check for Dec.
select fiscal_month into :fiscal_month
from pp_calendar
where month = :i_nvo_cpr_control.i_month
;

if isnull(fiscal_month) then
	fiscal_month = month(date(i_nvo_cpr_control.i_month))
end if

if fiscal_month = 12 then
	comp_str = f_parseNumArrayIntoString(i_nvo_cpr_control.i_company_idx)
	
	//Verify that there are mass ARO's being processed
	sqls = "select aro_id"
	sqls += " from 	aro"
	sqls += " where company_id in (" + comp_str + ")"
	sqls += " and 	aro_type_id = 2 "/*Mass*/
	sqls += " and 	aro_status_id in (3,6) "/*Active, Active w Pending*/
	
	ds_mass_aros = create uo_ds_top
	f_create_dynamic_ds(ds_mass_aros, "grid", sqls, sqlca, true)
	
	if ds_mass_aros.rowCount() > 0 then 
		f_pp_msgs("Reminder: Have you calculated Mass Year End for AROs?")
	end if
	destroy ds_mass_aros
end if

total_company = upperbound(i_nvo_cpr_control.i_company_idx)

f_pp_msgs( "Begin ARO Processing: " + string(now()))

aro_calc = CREATE nvo_aro_calc
for cc = 1 to total_company
	
	rc = i_nvo_cpr_control.of_setupCompany(cc)
	
	if rc < 1 then continue // next company
	
	rtn = aro_calc.uf_calc_for_company(i_nvo_cpr_control.i_company_idx[cc], i_nvo_cpr_control.i_month)
	
	if rtn = -1 then
		rollback;
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		return false
	elseif rtn = 1 then
		commit;
	end if
	
next//end company loop

DESTROY aro_calc

i_nvo_cpr_control.of_cleanUp(2, 'email cpr close: aro processing', 'ARO Processing')

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppdepr_interface_custom_version nvo_ppdepr_interface_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version

choose case a_pbd_name
	case 'ppdepr_interface_custom.pbd'
		return nvo_ppdepr_interface_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version	
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

