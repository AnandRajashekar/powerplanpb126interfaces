HA$PBExportHeader$nvo_aro_calc.sru
forward
global type nvo_aro_calc from nonvisualobject
end type
end forward

global type nvo_aro_calc from nonvisualobject
end type
global nvo_aro_calc nvo_aro_calc

forward prototypes
public function longlong uf_calc_for_company (longlong a_company_id, datetime a_month)
public function boolean uf_aro_approved (longlong a_company_id, datetime a_month)
public function boolean uf_aro_approved_prev_month (longlong a_company_id, datetime a_month)
public function boolean uf_next_month_open (longlong a_company_id, datetime a_month)
public function longlong uf_update_arc_depr_life (longlong a_company_id, datetime a_month)
public function longlong uf_update_aro_calculated (longlong a_company_id, datetime a_month)
end prototypes

public function longlong uf_calc_for_company (longlong a_company_id, datetime a_month);/*********************************************************************
*
*	uf_calc_for_company(): Runs ARO Calculation for a given company/month
*									No commits or rollbacks
*
*	Parameters: longlong a_company_id: The company to process
*					datetime a_month: The month to process
*	
*	Returns: 1 Success, -1 Failure
*
**********************************************************************/

longlong rtn, pp_stat_id
string comp_descr, rtn_str
nvo_aro_reg reg_aro_engine

// Log some info
f_pp_msgs("Company_id: " + string(a_company_id)+ ' ' + string(now()))
g_stats.set_met_start("ARO Calc for Company")

// Validate ARO hasn't already been approved this month
if uf_aro_approved(a_company_id, a_month) then
	f_pp_msgs("The ARO results have already been approved.")
	return -1
end if

// Validate ARO has been approved last month
if not uf_aro_approved_prev_month(a_company_id, a_month) then
	f_pp_msgs("ARO must be approved from the previous month before ARO can be processed this month.")
	return -1
end if

// Validate next month is open
if not uf_next_month_open(a_company_id, a_month) then
	f_pp_msgs("Next month must be open before ARO's can be processed")
	return -1
end if

select description  
into :comp_descr 
from company 
where company_id = :a_company_id;
f_pp_msgs("")
f_pp_msgs("ARO Processing Started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))

// Start Statistics
pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "ARO Processing: " + comp_descr)

// Update ARC Depr Life
f_pp_msgs("     Updating Remaining Life on CPR Depr...")
g_stats.set_met_start("ARO Calc: Update ARC Depr Life")
rtn = uf_update_arc_depr_life(a_company_id, a_month)
if rtn <> 1 or isNull(rtn) then
	f_pp_msgs("Error During Updating Remaining Life on CPR Depr")
	return -1
end if
g_stats.set_met_end("ARO Calc: Update ARC Depr Life")

// Calc Transition
f_pp_msgs("     Processing Transition..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Calc: Transition")
rtn = f_aro_calc_transition(a_company_id, a_month)
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error During Processing of Transition ARO's")
	return -1
end if
g_stats.set_met_end("ARO Calc: Transition")

// Calc Settlement
f_pp_msgs("     Processing Settlements..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Calc: Settlement")
rtn = f_aro_calc_settlement(a_company_id, a_month, 0, true)
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error During Settlement Process for ARO's")
	return -1
end if
g_stats.set_met_end("ARO Calc: Settlement")

// Calc Accretion
f_pp_msgs("     Processing Accretion..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Calc: Accretion")
rtn = f_aro_calc_accretion(a_company_id, a_month)
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error During Processing of Accretion ARO's")
	return -1
end if
g_stats.set_met_end("ARO Calc: Accretion")

// Calc Reg Entries
f_pp_msgs("     Processing Reg Entries..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Calc: Reg Entries")
rtn = f_reg_entries_calc(a_company_id, a_month, 'ARO')
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error During Processing of Reg Entries ARO's")
	return -1
end if
g_stats.set_met_end("ARO Calc: Reg Entries")

// Calc Regulated ARO's
f_pp_msgs("     Processing Regulated ARO's..."+ ' ' + string(now()))
reg_aro_engine = create nvo_aro_reg
g_stats.set_met_start("ARO Calc: Regulated ARO")
rtn_str = reg_aro_engine.uf_db_calc(a_company_id, a_month)
if rtn_str <> 'OK' or isnull(rtn_str) then
	f_pp_msgs("Error During Processing of Regulated ARO's")
	destroy reg_aro_engine
	return -1
else
	destroy reg_aro_engine
end if
g_stats.set_met_end("ARO Calc: Regulated ARO")

// Update aro_calculated on cpr control
g_stats.set_met_start("ARO Calc: Update CPR Control")
rtn = uf_update_aro_calculated(a_company_id, a_month)
if rtn <> 1 then
	return -1
end if
g_stats.set_met_end("ARO Calc: Update CPR Control")

// End Statistics
if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
g_stats.set_met_end("ARO Calc for Company")

f_pp_msgs("ARO Processing Completed for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
f_pp_msgs("")

return 1
end function

public function boolean uf_aro_approved (longlong a_company_id, datetime a_month);date aro_approved

select aro_approved
into :aro_approved
from cpr_control
where company_id = :a_company_id
and accounting_month = :a_month;

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Checking ARO Approval Status: " + sqlca.sqlErrText)
	return false
end if

if isNull(aro_approved) then
	return false
else
	return true
end if
end function

public function boolean uf_aro_approved_prev_month (longlong a_company_id, datetime a_month);date aro_approved

select aro_approved
into :aro_approved
from cpr_control
where company_id = :a_company_id
and accounting_month = add_months(:a_month, -1);

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Checking ARO Approval Status for previous month: " + sqlca.sqlErrText)
	return false
end if

if isNull(aro_approved) then
	return false
else
	return true
end if
end function

public function boolean uf_next_month_open (longlong a_company_id, datetime a_month);longlong check

select count(1)
into :check
from cpr_control
where company_id = :a_company_id
and accounting_month = add_months(:a_month, 1);

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Checking next month in CPR Control: " + sqlca.sqlErrText)
	return false
end if

if check = 1 then
	return true
else 
	return false
end if
end function

public function longlong uf_update_arc_depr_life (longlong a_company_id, datetime a_month);string sqls, rtn
longlong aro_id, asset_id, i, remaining_life, count, mnths_btwn, layer_id, j
datetime gl_posting_mo_yr, arc_depr_date
uo_ds_top ds_arc_depr
ds_arc_depr = CREATE uo_ds_top

sqls = "select aro.aro_id, aro.asset_id, aro_layer.arc_depr_date,   " + &
"aro_layer.arc_depr_flag, aro_layer.gl_posting_mo_yr, aro.company_id, aro_layer.layer_id   " + &  
"from aro_layer, aro   " + &  
"where (aro_layer.layer_id, aro_layer.aro_id) in   " + &  
	"(select max(layer_id), aro_id   " + &  
	 "from aro_layer   " + &  
	 "where aro_status_id = 3   " + &
	 "group by aro_id)   " + &  
"and aro_layer.aro_id = aro.aro_id   " + &  
"and nvl(aro_layer.arc_depr_flag, 0) = 1   " + &  
"and aro.company_id = " + string(a_company_id) + &
" and aro.aro_status_id = 3 " + &
"and aro.asset_id is not null " + &
"and nvl(aro_layer.arc_depr_date, to_date('01/1900', 'mm/yyyy')) <> to_date('01/1900', 'mm/yyyy')"

rtn = f_create_dynamic_ds(ds_arc_depr, 'grid', sqls, sqlca, true)
if rtn <> 'OK' then
	f_status_box('ARO Processing', 'Error: Could not create ARC Depr Datastore!')
	return -1
end if

count = ds_arc_depr.rowcount()
for i = 1 to count
//#### lkk PB problem with dynamic sql column names

	asset_id = ds_arc_depr.getItemNumber(i, 'asset_id')
	aro_id = ds_arc_depr.getItemNumber(i, 'aro_id')
	layer_id = ds_arc_depr.getItemNumber(i, 'layer_id')
	gl_posting_mo_yr = ds_arc_depr.getItemDateTime(i, 'gl_posting_mo_yr')
	arc_depr_date = ds_arc_depr.getItemDateTime(i, 'arc_depr_date')

	//ZJS: Maint 8924: 10302011: START
	select months_between(:arc_depr_date, :gl_posting_mo_yr) + 1
	into :remaining_life from dual;
	//ZJS: Maint 8924: 10302011: END
	
	//// Update Remaining Life on gl_posting_mo_yr
	update cpr_depr
	set remaining_life = :remaining_life
	where asset_id = :asset_id
	and gl_posting_mo_yr = :gl_posting_mo_yr
	//and set_of_books_id = 1
	;
	
	if sqlca.sqlcode <> 0 then
		f_pp_msgs('Error: Could not Update CPR Depr for Asset ID: ' + string(asset_id) + &
			'~n Error: ' + sqlca.sqlerrtext);
		return -1 
	end if
	
	/// Update Future Months after GL Posting Mo Yr
	select months_between(max_month, :gl_posting_mo_yr)
	into :mnths_btwn
	from 
		(select max(gl_posting_mo_yr) max_month
		from cpr_depr
		where asset_id = :asset_id
		and	set_of_books_id = 1);
	
	if mnths_btwn > 0 then
		for j = 1 to mnths_btwn
			update cpr_depr
			set remaining_life = :remaining_life - :j
			where asset_id = :asset_id
			and gl_posting_mo_yr = add_months(:gl_posting_mo_yr, :j)
			//and set_of_books_id = 1
			;
					
			if sqlca.sqlcode <> 0 then
				f_pp_msgs('Error: Could not Update Future CPR Depr Rows for Asset ID: ' + string(asset_id) + &
					'~n Error: ' + sqlca.sqlerrtext);
				return -1 
			end if
		next
	end if
	
	/// Update arc_depr_flag = 0 
	update aro_layer
	set arc_depr_flag = 0
	where aro_id = :aro_id
	and layer_id = :layer_id;
	if sqlca.sqlcode <> 0 then
		f_pp_msgs('Error: Could not Update arc_depr_flag to 0 for ARO ID: ' + string(aro_id) + &
			'~n Error: ' + sqlca.sqlerrtext);
		return -1 
	end if	

	
next 

f_pp_msgs('    Sucessfully Updated CPR Depr on ' + string(count) + ' ARC assets.')

DESTROY ds_arc_depr
return 1
end function

public function longlong uf_update_aro_calculated (longlong a_company_id, datetime a_month);update cpr_control
set aro_calculated = sysdate
where company_id = :a_company_id
and accounting_month = :a_month;

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Updating aro calculated on cpr control: " + sqlca.sqlerrtext)
	return -1
end if

return 1
end function

on nvo_aro_calc.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_aro_calc.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

