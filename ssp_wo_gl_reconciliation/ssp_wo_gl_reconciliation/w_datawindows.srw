HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_1 from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
string tag = "w_datawindows"
integer width = 3168
integer height = 1320
boolean titlebar = true
string title = "w_datawindows"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_1 dw_1
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_1=create dw_1
this.Control[]={this.dw_1}
end on

on w_datawindows.destroy
destroy(this.dw_1)
end on

type dw_1 from datawindow within w_datawindows
integer x = 114
integer y = 88
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

