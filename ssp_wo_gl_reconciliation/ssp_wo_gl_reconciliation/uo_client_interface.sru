HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_gl_reconciliation.exe'

nvo_wo_control i_nvo_wo_control
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
private function boolean uf_gl_reconciliation ()
public function boolean uf_set_gl_reconciliation_date (longlong a_company_idx, string a_company_descr, datetime a_month)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
w_datawindows w
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Process GL Reconciliation
if not uf_gl_reconciliation() then
	// log the list of companies that failed to perform G/L RECONCILIATION
	i_nvo_wo_control.of_log_failed_companies("G/L RECONCILIATION")
	return rtn_failure
end if

i_nvo_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

private function boolean uf_gl_reconciliation ();//*********************************************************************************************************
//
//	uf_gl_reconciliation()
//	
//	This function corresponds to w_wo_control.cb_gl_reconciliation.clicked() event
// It is called from the ssp_wo_gl_reconciliation ppinterface 
// application object and contains the main business logic for running the GL Reconciliation
// for a list of companies for a specific month
//	
//	Parameters	:	none
//	
//	Returns		:	boolean : true  : All provided companies closed for the provided accounting month
//                          false : All provided companies were NOT closed for the provided accounting month
//
//*********************************************************************************************************
int ndx
longlong ret, pp_stat_id, month_number, rtn
string args[], process_msg

// if any company in the loop has an error, it continues on to the next company but returns a failure status to uf_read
boolean b_all_success
b_all_success = true

i_nvo_wo_control.of_constructor()

i_nvo_wo_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_wo_control.i_month = datetime(g_ssp_parms.date_arg[1])

// lock the process for concurrency purposes
month_number = year(date(i_nvo_wo_control.i_month))*100 + month(date(i_nvo_wo_control.i_month))
if (i_nvo_wo_control.of_lockprocess( i_exe_name,  i_nvo_wo_control.i_company_idx,month_number, process_msg)) = false then
          f_pp_msgs("GL Reconciliation - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return false
end if 

// Get the descriptions
rtn = i_nvo_wo_control.of_getDescriptionsFromIds(i_nvo_wo_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

f_pp_msgs("Attempting GL Reconciliation for: ")
f_pp_msgs("Month: " + string(i_nvo_wo_control.i_month, "mm/dd/yyyy"))
for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	f_pp_msgs("Company ID: " + string(i_nvo_wo_control.i_company_idx[ndx]) + ", Company Name: " + i_nvo_wo_control.i_company_descr[ndx])
next

// check if the multiple companies chosen can be processed  together
if (i_nvo_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return false
end if

longlong num_companies, c
num_companies = upperbound(i_nvo_wo_control.i_company_idx)
for c=1 to num_companies
	//	it's selected, update the data window
	i_nvo_wo_control.of_companychanged(c,i_nvo_wo_control.i_month)
	
	if i_nvo_wo_control.i_ds_wo_control.retrieve(i_nvo_wo_control.i_company_idx[c], i_nvo_wo_control.i_month) = 1 then
		// Make sure the month isn't closed
		if not isnull(i_nvo_wo_control.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
			f_pp_msgs("This month is closed.")
			return false
		end if
	else
		f_pp_msgs("ERROR: retrieving wo_process_control record for company: '" + string(i_nvo_wo_control.i_company_idx[c])  +  "', month '" + string(i_nvo_wo_control.i_month, "mm/dd/yyyy") + "'")
	end if
	
next

for ndx = 1 to upperbound(i_nvo_wo_control.i_company_idx)
	
   // Call f_reconcile_gl to Reconcile PowerPlant with GL ( after GL transactions have
	// been passed to GL and results returned ) .	
	f_pp_msgs("GL Reconcile Started for company " + i_nvo_wo_control.i_company_descr[ndx] +  " at " + String(Today(), "hh:mm:ss"))

	pp_stat_id = f_pp_statistics_start("WO Monthly Close", "G/L Reconciliation: " + i_nvo_wo_control.i_company_descr[ndx])
	
	// call custom function from PBD
	ret = f_reconcile_gl_wo(i_nvo_wo_control.i_month)

	if ret = -999 then
		f_wo_status_box("GL Reconcile","GL Reconciliation process is not set up for company: '" + i_nvo_wo_control.i_company_descr[ndx] + "'")
	elseif ret <> 1 then
		f_wo_status_box("GL Reconcile","Possible that external Journal Entry was posted to GL," )
		f_wo_status_box("GL Reconcile","but NOT to PowerPlant." )
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(ndx)
		// don't set GL_RECONCILED
		if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
		continue
	else
		f_wo_status_box("GL Reconcile","PowerPlant and General Ledger are in Balance. ")
	end if
	
	// Call Dynamic Validation option
	args[1] = string(i_nvo_wo_control.i_month)
	args[2] = string(i_nvo_wo_control.i_company_idx[ndx])
	
	ret = f_wo_validation_control(1058,args)
	
	if ret = -1 then
		f_pp_msgs("Work Order Control G/L Reconciliation (Dynamic Validations) Failed.")
		// keep track of all the companies that failed to unitize so they can be logged later
		b_all_success = false
		i_nvo_wo_control.of_add_to_failed_company_list(ndx)
		// don't set GL_RECONCILED
		if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
		continue
	end if

	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
	// if neither Custom PBD Validation nor SQL Dynamic Validation failed then set GL_RECONCILED
	uf_set_gl_reconciliation_date( i_nvo_wo_control.i_company_idx[ndx], i_nvo_wo_control.i_company_descr[ndx], i_nvo_wo_control.i_month )

next

// email that companies have closed
i_nvo_wo_control.of_cleanup( 1, 'email wo close: g/l reconciliation', 'G/L Reconciliation ended')

return b_all_success
end function

public function boolean uf_set_gl_reconciliation_date (longlong a_company_idx, string a_company_descr, datetime a_month);//*********************************************************************************************************
//
//	uf_set_gl_reconciliation_date()
//	
//	This function corresponds to w_wo_control.cb_close_chage_collection.clicked() event
// It is called from the ssp_wo_close_charge_collection ppinterface application object
// It will set the GL_RECONCILED date column on the wo_process_control table
// for the company provided for the accounting month provided
//	
//	Parameters	:	longlong : a_company_idx : company id
//	               string : a_company_descr : company description
//                datetime   : a_month : accounting month for closing
//	
//	Returns		:	boolean : true  : Provided company GL Reconcliation date set for the provided accounting month
//                          false : Provided company GL Reconcliation date NOT set for the provided accounting month
//
//*********************************************************************************************************

f_pp_msgs("GL Reconciliation started for company: '" + a_company_descr  +  "', month '" + string(a_month, "mm/dd/yyyy") + "' at '" + String(Today(), "hh:mm:ss") + "'")

if i_nvo_wo_control.i_ds_wo_control.retrieve(a_company_idx, a_month) = 1 then
	if (i_nvo_wo_control.i_ds_wo_control.setitem( 1, 'gl_reconciled', today()) = 1) then
		f_pp_msgs("GL Reconciliation date set for company: '" + a_company_descr  +  "', month: '" + string(a_month, "mm/dd/yyyy") + "' at '" + String(Today(), "hh:mm:ss") + "'")
	else
		f_pp_msgs("ERROR: Setting GL Reconciliation date for company: '" + a_company_descr  +  "', month '" + string(a_month, "mm/dd/yyyy") + "'")
	end if

	i_nvo_wo_control.of_updateDW()
else
	f_pp_msgs("ERROR: retrieving wo_process_control record for company: '" + a_company_descr  +  "', month '" + string(a_month, "mm/dd/yyyy") + "'")
end if

return true
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_wo_gl_reconciliation_custom_version nvo_ssp_wo_gl_reconciliation_custom_version

choose case a_pbd_name
	case 'ssp_wo_gl_reconciliation_custom.pbd'
		return nvo_ssp_wo_gl_reconciliation_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

