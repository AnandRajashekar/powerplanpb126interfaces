HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_company, i_output_location, i_where_clause_array[]
string i_stg_table_name, i_description
longlong 	 i_obey_gl_rollup, i_output_method, i_posting_id, i_array_counter, i_posting_id_array[]

uo_ds_top i_ds_posting_audit
string i_budget_version

string		i_exe_name = 'cr_posting_bdg.exe'
end variables

forward prototypes
public function longlong uf_create_file_or_table ()
public function longlong uf_update_summary_values ()
public function longlong uf_cc_id ()
public function longlong uf_read ()
public function longlong uf_insert_budget_to_gl ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_create_file_or_table ();longlong i, rtn, posted_to_gl, num_cols,  j, col_dec
string sqls, create_text_file, gl_column_name, post_to_gl_table, file_extension, &
		col_width, col_type, nines, gl_cols

if i_output_method = 1 then //table

	//	DML: 042006: Build the insert dynamically to include the column lists in case the posting
	//		is against a table over a link and we don't need to insert into every column.
	//		If we don't have to insert into every column our staging table will not be 
	//		shaped the same as the posting table.

	//	sqls = ' insert into ' + i_output_location + ' select * from ' + i_stg_table_name 

	uo_ds_top ds_budget_to_gl
	ds_budget_to_gl = create uo_ds_top

	sqls = " select replace(replace(replace(gl_column_name,' ','_'),'-','_'),'/','_') gl_column_name " + &
			 " from cr_post_to_gl_columns_bdg " + &
			 " where posting_id = " + string(i_posting_id) + "order by " + '"ORDER"'
			 
	f_create_dynamic_ds(ds_budget_to_gl,"grid",sqls,sqlca,true)
	rtn = ds_budget_to_gl.rowcount()
	
	gl_cols = ""
	for i = 1 to rtn 
		gl_column_name = ds_budget_to_gl.getitemstring(i,"gl_column_name")
		gl_column_name = '"' + upper(gl_column_name) + '"'
		gl_cols = gl_cols + gl_column_name + ", "
	next 
	gl_cols = LEFT(gl_cols, LEN(gl_cols) - 2)

	sqls = 	' insert into ' + i_output_location +&
				' ( ' + gl_cols + ' ) select ' + gl_cols +&
				' from ' + i_stg_table_name 
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Inserting into " + i_output_location + " : " + sqlca.sqlerrtext)
		rollback;
		f_pp_msgs(sqls)
		return -1
	else
		destroy ds_budget_to_gl
	end if
	
else	//file
	
	uo_ds_top ds_create_text_file
	ds_create_text_file = create uo_ds_top
	
	file_extension = lower(right(i_output_location,3) )
	
	if file_extension = 'dat' then  // fixed width
		// Get the table column definitions
		sqls = " select replace(replace(replace(gl_column_name,' ','_'),'-','_'),'/','_'), " + &
				' "WIDTH","TYPE","DECIMAL" from CR_POST_TO_GL_COLUMNS_BDG ' + &
				" where posting_id = " + string(i_posting_id) + ' order by "ORDER", gl_column_name'
		uo_ds_top ds_cr_columns
		ds_cr_columns = create uo_ds_top
		f_create_dynamic_ds(ds_cr_columns,"grid",sqls,sqlca,true)
		num_cols = ds_cr_columns.rowcount()
		
		if num_cols = 0 then return 1
		
		sqls = 'select '

		for i = 1 to num_cols
			
			setnull(col_width)
			gl_column_name = ds_cr_columns.getitemstring(i,1)
			col_width = string(ds_cr_columns.getitemnumber(i,2))
			col_type = ds_cr_columns.getitemstring(i,3)
			
			if col_type = "Character" then
				sqls = sqls + "rpad(" + gl_column_name + "," + col_width + ")"
			else
				col_dec = ds_cr_columns.getitemnumber(i,4)
				if not isnull(col_dec) and col_dec > 0 then
					nines = "9"
					for j = 2 to col_dec
						nines = nines + "9"
					next
					sqls = sqls + "lpad(to_char(" + gl_column_name + ",'99999999999999990." + nines + "')," + col_width + ")"
				else
					sqls = sqls + "lpad(to_char(" + gl_column_name + ")," + col_width + ")"
				end if
			end if
			
			if i <> num_cols then
				sqls = sqls + "||"
			else
				sqls = sqls + " from " + i_stg_table_name
			end if
			
		next

	else
		sqls = ' select * from ' + i_stg_table_name
	end if
	
	f_create_dynamic_ds(ds_create_text_file,"grid",sqls,sqlca,true)
	
	choose case file_extension
		case 'txt', 'dat'
			rtn = ds_create_text_file.SaveAs(i_output_location,text!,false)
		case 'xls'
			rtn = ds_create_text_file.SaveAs(i_output_location,excel!,false)
		case 'csv'
			rtn = ds_create_text_file.SaveAs(i_output_location,csv!,false)
	end choose
	
	
	if rtn <> 1 then
		f_pp_msgs("ERROR: Creating the text file. ")
		f_pp_msgs(sqls)
		return -1
	end if
	
end if
	
return 1
end function

public function longlong uf_update_summary_values ();//*****************************************************************************************
//
//  Function  			:  uf_update_summary_values
//
//	 Description      :  Performs an update that changes the detailed code block values
//								in cr_temp_cr_gl_temp to summary code block values.    The results in
//                      cr_temp_cr_gl_temp very well may end up with many records containing
//                      the same summary code block.  They will be summarized in the
//                      uf_insert_post_to_gl function.
//								
//*****************************************************************************************
longlong i, rtn, num_elements
string sqls, element_table, element_column, cr_element

uo_ds_top ds_elements
ds_elements = create uo_ds_top

sqls = " select element_table,element_column,budgeting_element " + &
	" from cr_elements " + &
	" where (gl_actual_or_budget = 0 or gl_actual_or_budget is null) "
f_create_dynamic_ds(ds_elements,"grid",sqls,sqlca,true)

num_elements = ds_elements.rowcount()
for i = 1 to num_elements
	
	element_table 	= upper(ds_elements.getitemstring(i,"element_table"))
	element_column	= upper(ds_elements.getitemstring(i,"element_column"))
	cr_element		= upper(ds_elements.getitemstring(i,"budgeting_element"))
	
	element_table 	= f_cr_clean_string(element_table)
	element_column = f_cr_clean_string(element_column)
	cr_element 		= f_cr_clean_string(cr_element)

//	In case the element is a reserved word user upper and double quotes
//	element_table  = '~""' + element_table  + '~""'
//	element_column = '~""' + element_column + '~""'
	element_table  = '~"' + element_table  + '~"'
	element_column = '~"' + element_column + '~"'
	cr_element 		= '~"' + cr_element 		+ '~"'

	sqls = "update cr_temp_cr_gl_bdg_temp a set " + cr_element + " = ( " + &
		" select b.budget_value from " + element_table + " b " + &
		" where a." + cr_element + " = b." + element_column + &
		" and lower(b.element_type) = 'budget' ) "
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Updating cr_temp_cr_gl_bdg_temp." + element_column + ": " + sqlca.sqlerrtext)
		f_pp_msgs(sqls)
		rollback;
		return -1
	end if

next

return 1
end function

public function longlong uf_cc_id ();//*****************************************************************************************
//
//  Window Function  :  uf_cc_id
//
//	 Description 		: 	1) Inserts any new code block combinations into cr_gl_id
//								2) Update cr_temp_cr_gl_bdg with a gl_id
//							: 	DML: 032105: No longer update the gl_id and batch_id in this function
//								3) Backfill cr_cost_repository.gl_id to indicate it has been sent
//
//*****************************************************************************************
longlong i, temp_rows, gl_id, rtn, struct_num, resp_appl_id, resp_id, &
       user_id, counter, id, max_gl_id, num_rows
string sqls, resp_name, user_name, field, gl_field, obey_approval

setnull(obey_approval)
select upper(trim(control_value)) into :obey_approval from cr_system_control
where upper(trim(control_name)) = 'CR BUDGET POSTING - OBEY APPROVAL';
if isnull(obey_approval) or obey_approval = "" then
	obey_approval = "NO"
end if

max_gl_id = 0
select max(gl_id) into :max_gl_id from cr_gl_id;
if isnull(max_gl_id) then max_gl_id = 0

uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
f_create_dynamic_ds(ds_elements,'grid','select * from cr_elements order by "ORDER"',sqlca,true)
num_rows = ds_elements.rowcount()
//ds_elements.DataObject = "dw_cr_element_definitions"
//ds_elements.SetTransObject(sqlca)
//num_rows = ds_elements.RETRIEVE()

//insert new code block combos into cr_gl_id
sqls = "insert into cr_gl_id ("

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)
	
	if i = num_rows then
		sqls = sqls + '"' + upper(field) + '", gl_id ) '
	else
		sqls = sqls + '"' + upper(field) + '", '
	end if
next

sqls = sqls + " select a." 

for i = 1 to num_rows
	
	field = ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)
	
	if i = num_rows then
		sqls = sqls + '"' + upper(field) + '", '  + string(max_gl_id) + " + rownum as gl_id "
	else
		sqls = sqls + '"' + upper(field) + '", a.'
	end if
	
next

sqls = sqls + " from ( select distinct "

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)

	gl_field = ds_elements.GetItemString(i, "gl_element")
	if isnull(gl_field) or gl_field = "" then
		//not gl field so use space
		field = "' ' as " + upper(field)
	else
		field = '"' + upper(field) + '" '
	end if
	
	if i = num_rows then
		sqls = sqls + field + " from cr_temp_cr_gl_bdg) a, "
	else
		sqls = sqls + field + ', '
	end if
next

sqls = sqls + " ( select " 

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)
	
	if i = num_rows then
		sqls = sqls + '"' + upper(field) + '" ' + ", gl_id from cr_gl_id) b "
	else
		sqls = sqls + '"' + upper(field) + '", '
	end if
next

sqls = sqls + " where a."

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)
	
	sqls = sqls + '"' + upper(field) + '"' + " = b." + '"' + upper(field) + '"' + " (+) " 
	
	if i <> num_rows then
		sqls = sqls + " and a."
	end if
	
next

sqls = sqls + " and b.gl_id is null "

execute immediate :sqls;
if sqlca.sqlcode < 0 then
//	f_write_log(g_log_file, "ERROR: Inserting into cr_gl_id: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Inserting into cr_gl_id: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	f_pp_msgs(sqls)
	return sqlca.sqlcode
end if
	
// Clear out the cr_temp_cr_gl_ids_bdg table
f_pp_msgs(" -- deleting from cr_temp_cr_gl_ids_bdg ...")
sqlca.truncate_table('cr_temp_cr_gl_ids_bdg');
if sqlca.SQLCode < 0 then
//		f_write_log(g_log_file, "Error: truncating cr_temp_cr_gl_ids_bdg: " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		f_pp_msgs("Error: truncating cr_temp_cr_gl_ids_bdg: " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		return sqlca.SQLCode
end if

// Insert into cr_temp_cr_gl_ids_bdg
f_pp_msgs(" -- inserting into cr_temp_cr_gl_ids_bdg ...")

sqls = "insert into cr_temp_cr_gl_ids_bdg (id, gl_id) ( " + &
	"select a.id, b.gl_id from cr_temp_cr_gl_bdg a, cr_gl_id b where "  

for i = 1 to num_rows
	field = ds_elements.GetItemString(i, "description")
	field = f_cr_clean_string(field)
	
	gl_field = ds_elements.GetItemString(i, "gl_element")
	if isnull(gl_field) or gl_field = "" then
		//not gl field so use space
		sqls = sqls + "' ' = b." + '"' + upper(field) + '"' 
	else
		sqls = sqls + 'a."' + upper(field) + '"' + " = b." + '"' + upper(field) + '"' 
	end if
	
	if i <> num_rows then
		sqls = sqls + " and "
	else
		sqls = sqls + ") "
	end if
	
next

execute immediate :sqls;
if sqlca.SQLCode < 0 then
//		f_write_log(g_log_file, "Error: inserting into cr_temp_cr_gl_ids_bdg: " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		f_pp_msgs("Error: inserting into cr_temp_cr_gl_ids_bdg: " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		f_pp_msgs(sqls)
		return sqlca.SQLCode
end if

//	Assign the gl_id to cr_temp_cr_gl_bdg (table containing records to send to the GL) 
//		from cr_temp_cr_gl_ids_bdg (where the new code block combos where inserted)

f_pp_msgs(" -- updating cr_temp_cr_gl_bdg.gl_id ...")
update cr_temp_cr_gl_bdg a set gl_id = (
	select gl_id from cr_temp_cr_gl_ids_bdg c
	 where a.id = c.id);
if sqlca.SQLCode < 0 then
//		f_write_log(g_log_file, "Error: updating cr_temp_cr_gl_bdg.gl_id: " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: updating cr_temp_cr_gl_bdg.gl_id: " + sqlca.SQLErrText)
		f_pp_msgs(" ")
		return sqlca.SQLCode
end if

//// DML: 032105: At Atmos, they need to send the gl_id and batch_id
////	with the posting.  This function is called after all the postings
////	have completed.  The uf_cc_id function will be moved to right after
////	the insert into cr_temp_cr_gl_bdg but without the updates to cr_cost_repository
////	The updates to cr_cost_repository need to stay in their current location
////	so they do not fire until all postings have completed.
//
////  Back fill the cr_cost_repository.batch_id with this id ...
////	DML: 122004: If new transactions are loaded while this process is running
////		then this update could touch more records then desired.  We should 
////		have an id restriction similar to the restriction below on the gl_id update.
f_pp_msgs("Updating cr_temp_cr_gl_bdg.batch_id ...")

if obey_approval = 'YES' then
	update cr_temp_cr_gl_bdg set batch_id = :g_batch_id
	 where batch_id = 999999999999999 and id in (select id from cr_temp_cr_gl_bdg) using sqlca;
else
	update cr_temp_cr_gl_bdg set batch_id = :g_batch_id
	 where batch_id is NULL and id in (select id from cr_temp_cr_gl_bdg) using sqlca;
end if

if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Updating cr_temp_cr_gl_bdg.batch_id: " + sqlca.SQLErrText )
	f_pp_msgs(" ")
	return sqlca.SQLCode
end if
//			 
//
////	Backfill the cr_cost_repository.gl_id 
//f_pp_msgs("Updating cr_cost_repository.gl_id ...")
//update cr_cost_repository a set gl_id = (
//	select gl_id from cr_temp_cr_gl_bdg b
//    where a.id = b.id)
//	where batch_id = :g_batch_id; 
//  	 
// //where batch_id is null
// 
// if sqlca.SQLCode < 0 then
//		f_write_log(g_log_file, "Error: updating cr_cost_repository_gl.gl_id: " + sqlca.SQLErrText)
//		f_pp_msgs("Error: updating cr_cost_repository.gl_id: " + sqlca.SQLErrText)
//		return sqlca.SQLCode
//end if

f_pp_msgs("")

return 0
end function

public function longlong uf_read ();integer  rtn, gl_actual_or_budget, asys_rtn = 1, g
string   sqls, element, company, ferc, prov_area, targ_area, resource, task, sid, &
         work_order, cost_code, location_id, file_name, lookup_element, dataobject, &
			s_date, analyze_sqls, col1, col2, col3, col4, col5, col6, col7, col8, col9, &
			server_name, where_clause, obey_approval, cmd_line_arg[], posting_ids
boolean  write_text_file, view_failure, updated_gl_id, no_wc_found
longlong file_rows, i, num_postings, l, counter, num_active_postings, m, overall_rtn, num_cells, &
	cur_batch_id
date     ddate
time     ttime
datetime started_at
dec {2} debit, credit

overall_rtn = 1

//**********************************************************************************
//
//  The command line argument cannot be NULL ... 
//  budget_version
//
//**********************************************************************************

// ### - SEK - 7078 - 031711: Need to trim g_command_line_args
g_command_line_args = trim(g_command_line_args)

if isnull(g_command_line_args) or g_command_line_args = "" then
	f_pp_msgs(" ")
	f_pp_msgs("The command line argument is NULL.  Cannot run the Interface!")	
	f_pp_msgs(" ")
	rollback;
	return -1
end if

//  Now allowing to filter to only certain posting ids.  For example, a budget
//  system sends us $$$, we run some billing allocations to another company and
//  need to send it back to the external budget system so they can do their work.
//  That is one posting type.  We then get that company's budgeted data back from
//  the external system, run some allocations, and have to post the whole mess to
//  the GL.  That is a second posting type.  While we could manage all of this in 
//  our current app, by simply having separate budget versions, we'd still be creating
//  the files or inserting to staging tables for BOTH posting types each time we ran.
//  That is bad ... e.g. the first run of the posting for company 2 could trigger
//  posting in the GL when they don't want that.
//
//  Assume that the 1st comma-separated arg is the BV so we don't blow up existing
//  clients.  Assume that all args after that are posting ids.  If no posting ids,
//  set a flagg to ALL, so the code behaves as it does now for existing clients.
f_parsestringintostringarray(g_command_line_args, ",", cmd_line_arg)

num_cells = upperbound(cmd_line_arg)

i_budget_version = cmd_line_arg[1] // We've already checked that there is a not null value in g_cmd_line_arg,
	// so at least 1 cell.
// Maint - 10911: force budget version to uppercase to prevent mismatches
i_budget_version = upper(trim(i_budget_version))

posting_ids = ""
for i = 2 to num_cells
	if i = 2 then
		posting_ids = cmd_line_arg[i]
	else
		posting_ids = posting_ids + "," + cmd_line_arg[i]
	end if
next
if isnull(posting_ids) or posting_ids = "" then posting_ids = "ALL"

counter = 0
select count(*) into :counter
from cr_budget_version
where upper(trim(budget_version)) = :i_budget_version;

if counter<=0 then
	f_pp_msgs(" ")
	f_pp_msgs("The command line argument is not valid - not a valid budget version.  Cannot run the Interface!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if

//Set the budget version from the command line.  this was verified in the window open event
//i_budget_version = g_bv // This used to be: g_cmd_line_arg

sqlca.truncate_table('cr_temp_cr_gl_bdg');

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: Truncating cr_temp_cr_gl_bdg: " + &
			 sqlca.SQLErrText )
	rollback;
	return -1
end if

// ### 8194  JAK: 2011-07-27:  Can't truncate in the loop below otherwise posting 1
//	could be saved and then posting 2 error.  Will truncate here to clear the high water mark
//	and then just delete below.
sqlca.truncate_table('cr_temp_cr_gl_bdg_temp');
	
if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: Truncating cr_temp_cr_gl_bdg_temp: " + sqlca.SQLErrText )
	rollback;
	return -1
end if

i_ds_posting_audit = create uo_ds_top

sqls = "select count(*), sum(decode(sign(amount),-1,0,amount)), sum(decode(sign(amount),-1,amount,0)) " + &
	"from cr_temp_cr_gl_bdg where 1 = 0"
f_create_dynamic_ds(i_ds_posting_audit,"grid",sqls,sqlca,true)

//*****************************************************************************************
//
//  Method:  
//			1) Loop over cr_budget_to_gl_columns and build the sql for
//				each required posting
//			2) Based on the output method, create a text file or a table
//
//*****************************************************************************************

//	0 as the posting_id indicates the function was called at the beginning
rtn = f_cr_posting_bdg(0,0)

if rtn < 0 then
	rollback;
	return -1
end if

setnull(obey_approval)
select upper(trim(control_value)) into :obey_approval from cr_system_control
where upper(trim(control_name)) = 'CR BUDGET POSTING - OBEY APPROVAL';
if isnull(obey_approval) or obey_approval = "" then
	obey_approval = "NO"
end if

if obey_approval = 'YES' then
	select count(*), sum(decode(sign(amount),-1,0,amount)), sum(decode(sign(amount),-1,amount,0)) 
	into :g_record_count, :debit, :credit
	from cr_budget_data
	where batch_id = 999999999999999
	and upper(trim(budget_version)) = :i_budget_version;
else
	//	DML: 092605: If the dr_cr_id is incorrect then the abs will cause
	//		an oob journal.  Although the dr_cr_id should be corrected it
	//		shouldn't impact posting to the GL.  So, take the amount as is.
	//select count(*), sum(decode(dr_cr_id,1,amount,0)), sum(decode(dr_cr_id,-1,amount,0)) 
	select count(*), sum(decode(sign(amount),-1,0,amount)), sum(decode(sign(amount),-1,amount,0)) 
	into :g_record_count, :debit, :credit
	from cr_budget_data
	where batch_id is null
	and upper(trim(budget_version)) = :i_budget_version;
end if

if g_record_count = 0 then 
	f_pp_msgs("")
	f_pp_msgs("--------There are no transactions to be posted----------")
	f_pp_msgs("")
	goto the_end
end if

g_total_dollars = debit + credit

//	The total record count/debits/credits eligible to be posted doesn't mean
//		that is what will post since other criteria can be applied during
//		each posting.  Change the message to indicate this.
f_pp_msgs("")
//f_pp_msgs("Posting " + string(g_record_count) + " transactions to the General Ledger")
f_pp_msgs("There are " + string(g_record_count) + " transactions eligible to be posted.")
f_pp_msgs("Total credits = " + string(credit))
f_pp_msgs("Total debits = " + string(debit))
f_pp_msgs("")

//	If there are no active postings terminate.
select count(*) into :num_active_postings
from cr_post_to_gl_control_bdg
where status = 1;
if isnull(num_active_postings) then num_active_postings = 0

if num_active_postings = 0 then
	//	There are no active postings
	f_pp_msgs("  ")
	f_pp_msgs("--------Although there are transactions to post, there are no active postings.----------")
	f_pp_msgs("  ")
	goto the_end
end if

if obey_approval = 'YES' then
	//	Get the unposted (batch_id = 999999999999999) from cr_budget_data
	insert into cr_temp_cr_gl_bdg
	select * from cr_budget_data 
	where batch_id = 999999999999999 and upper(trim(budget_version)) = :i_budget_version;
else
	//	Get the unposted (batch_id = null) from cr_budget_data
	insert into cr_temp_cr_gl_bdg
	select * from cr_budget_data 
	where batch_id is null and upper(trim(budget_version)) = :i_budget_version;
end if

if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Inserting unposted records into cr_temp_cr_gl_bdg : " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//	Analyze cr_temp_cr_gl_bdg
//	DML: 040506: It is ok to have an analyze here.  Firing a commit at this point 
//		should be ok although that might depend on what is being done in 
//		f_cr__budget_posting(0,0) above.  If an error occurs, this table is cleared out
//		in the close event.
//sqls = 'analyze table cr_temp_cr_gl_bdg compute statistics'
//execute immediate :sqls;
sqlca.analyze_table('cr_temp_cr_gl_bdg');

//	Increment the batch_id
g_batch_id = '0'
select max(batch_id) into :g_batch_id 
  from cr_special_processing_dates_bd where id = 11 using sqlca;
if isnull(g_batch_id) then g_batch_id = '0'
cur_batch_id = long(g_batch_id)
cur_batch_id++
g_batch_id = string(cur_batch_id)

//EAG: This process is for actuals only, not needed in the outbound budget interface
////// DML: 032105: At Atmos, they need to send the gl_id and batch_id
//////	with the posting.  Update it after the insert to cr_temp_cr_gl_bdg
//////	so it is available to the posting but do not backfill cr_budget_data 
//////	until after all postings have successfully completed.
//rtn = uf_cc_id()
//if rtn < 0 then
//	error = true
//	rollback;
//	goto clear_stg_table
////	return -1
//end if
//
//	Retrieve the posting information from the posting control table
//	DML: 013006: Only pull active postings (active = 1, inactive = 0)

if posting_ids = "ALL" then
	sqls = ' select posting_id, company, output_method, output_location, ' + &
			 ' obey_gl_rollup, stg_table_name, description from cr_post_to_gl_control_bdg ' + &
			 ' where status = 1 order by description '
else
	sqls = ' select posting_id, company, output_method, output_location, ' + &
			 ' obey_gl_rollup, stg_table_name, description from cr_post_to_gl_control_bdg ' + &
			 ' where status = 1 and posting_id in (' + posting_ids + ') order by description '
end if

uo_ds_top ds_post_to_gl_columns_bdg
ds_post_to_gl_columns_bdg = create uo_ds_top

f_create_dynamic_ds(ds_post_to_gl_columns_bdg,"grid",sqls,sqlca,true)
num_postings = ds_post_to_gl_columns_bdg.rowcount()

// ### 8194  JAK: 2011-07-27:  Can't truncate in the loop below otherwise posting 1
//	could be saved and then posting 2 error.  Will truncate before processing anything to clear the high water mark
//	and then just delete below.
for l = 1 to num_postings
	i_stg_table_name = ds_post_to_gl_columns_bdg.getitemstring(l,"stg_table_name")
	sqlca.truncate_table(i_stg_table_name);
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: Truncating " + i_stg_table_name + ": " + sqlca.SQLErrText )
		rollback;
		return -1
	end if
next

for l = 1 to num_postings
	
	i_description = ds_post_to_gl_columns_bdg.getitemstring(l,"description")
	i_company = ds_post_to_gl_columns_bdg.getitemstring(l,"company")
	i_posting_id = ds_post_to_gl_columns_bdg.getitemnumber(l,"posting_id")
	i_output_method = ds_post_to_gl_columns_bdg.getitemnumber(l,"output_method")
	i_output_location = ds_post_to_gl_columns_bdg.getitemstring(l,"output_location")
	i_obey_gl_rollup = ds_post_to_gl_columns_bdg.getitemnumber(l,"obey_gl_rollup")
	i_stg_table_name = ds_post_to_gl_columns_bdg.getitemstring(l,"stg_table_name")
	
	f_pp_msgs("Posting: " + i_description + " ... " + string(l) + " of " + string(num_postings))

//	g_do_not_write_batch_id = true

	
	// ### 8194  JAK: 2011-07-27:  Can't truncate in the loop otherwise posting 1
	//	could be saved and then posting 2 error.  Will truncate at the top to clear the high water mark
	//	and then just delete here.
	
	//	cr_temp_cr_gl_bdg_temp needs to be refreshed each time through the loop
	//	b/c the ability to roll up elements means this table could be different
	//	for each posting depending on the posting parameters.
//	sqlca.truncate_table('cr_temp_cr_gl_bdg_temp');
	sqls = 'delete from cr_temp_cr_gl_bdg_temp'
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: Deleting cr_temp_cr_gl_bdg_temp: " + sqlca.SQLErrText )
		rollback;
		return -1
	end if
	
//	sqlca.truncate_table(i_stg_table_name);
	sqls = 'delete from ' + i_stg_table_name
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: Deleting " + i_stg_table_name + ": " + sqlca.SQLErrText )
		rollback;
		return -1
	end if
	
	insert into cr_temp_cr_gl_bdg_temp
	select * from cr_temp_cr_gl_bdg;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: Inserting into cr_temp_cr_gl_bdg_temp: " + sqlca.SQLErrText )
		rollback;
		return -1
	end if
	
	//	DML: 032105: Call the custom function before the rollup occurs.
	rtn = f_cr_posting_bdg(0,i_posting_id)

	if rtn < 0 then
		rollback;
		overall_rtn = -1
		goto clear_stg_table
	end if
	
	//	wf_update_summary_values looks up the budget_value (really the GL Value)
	//	from cr_elements and updates cr_temp_cr_gl_bdg_temp with this rollup value.  It is 
	//	then summarized in the uf_insert_post_to_gl function.  Since this 
	//	interface can be used to post to different systems, we should check to 
	//	see if this particular posting should obey the rollup flag.
	
	//	RULES
	//	obey_gl_rollup = 0 means no
	//	obey_gl_rollup = 1 means yes
//EAG: This is not used for the budget since it is going straight from cr_budget_data.
//	if i_obey_gl_rollup = 1 then	
//		
//		rtn = uf_update_summary_values()
//		if rtn < 0 then
//			error = true
//			goto clear_stg_table
////			return -1
//		end if
//		
//	end if
	
//	Build the insert command and insert into the posting stage table
	rtn = uf_insert_budget_to_gl()
	if rtn < 0 then
		rollback;
		overall_rtn = -1
		goto clear_stg_table
	end if
	
	//	Call the custom function now that the data is in the staging table
	//	but before the file is created or the insert into the posting table 
	//	is complete.
	rtn = f_cr_posting_bdg(1,i_posting_id)

	if rtn < 0 then
		rollback;
		overall_rtn = -1
		goto clear_stg_table
	end if
	
	//	Create the file or insert into the posting table
	rtn = uf_create_file_or_table()
	if rtn < 0 then
		rollback;
		overall_rtn = -1
		goto clear_stg_table
	end if
	
	//	uf_create_file_or_table was successful.  Another crack at the custom function
	 rtn = f_cr_posting_bdg(3,i_posting_id)

//	commit; HOLD COMMIT UNTIL ALL SUCCEED (after next)
	
next

commit; // everything good so far so commit the interface rows.

//// DML: 032105: At Atmos, they need to send the gl_id and batch_id
////	with the posting.  This function is called after all the postings
////	have completed.  The uf_cc_id function will be moved to right after
////	the insert into cr_temp_cr_gl_bdg but without the updates to cr_budget_data
////	The updates to cr_budget_data need to stay here
////	so they do not fire until all postings have completed.
//	Backfill the gl_id
//rtn = uf_cc_id()
//if rtn < 0 then
//	error = true
//	goto clear_stg_table
////	return -1
//end if

////	Backfill cr_budget_data.gl_id/batch_id
f_pp_msgs("Updating cr_budget_data.batch_id ...")
//update cr_budget_data set batch_id = :g_batch_id
// where batch_id is NULL and id in (select id from cr_temp_cr_gl_bdg) using sqlca;

//	Loop over the where clause array.  If I find any where_clause = 'nowhereclause' then I 
//	know there is a posting that hit every record eligible.  Therefore, I only have to fire
//	one update against cr_budget_data that hits all the eligible rows.  This would happen
//	if they checked "All Companies" and "All Journal Categories".
for g = 1 to upperbound(i_where_clause_array)
	
	where_clause = i_where_clause_array[g]
	if where_clause = 'nowhereclause' then
		no_wc_found = true
	end if
	
next

if no_wc_found then
	//	At least one of the postings hit all the records eligible to post.
	
	//	The cr_temp_cr_gl_bdg restriction is to make sure we don't update any new 
	//	rows that might have been loaded while this process was executing.
	//	The id restriction is needed to make sure we don't mark any rows that 
	//	were loaded into cr_budget_data while cr_posting is still running.
	if obey_approval = 'YES' then
		sqls = "update cr_budget_data set batch_id = " + string(g_batch_id) + &
		 " where batch_id = 999999999999999 and id in (select id from cr_temp_cr_gl_bdg) " +&
		 " and upper(trim(budget_version)) = '" + i_budget_version + "'"
	else
		sqls = "update cr_budget_data set batch_id = " + string(g_batch_id) + &
		 " where batch_id is NULL and id in (select id from cr_temp_cr_gl_bdg) " +&
		 " and upper(trim(budget_version)) = '" + i_budget_version + "'"
	end if
	 
	execute immediate :sqls;

	if sqlca.SQLCode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Updating cr_budget_data.batch_id: " + &
				 sqlca.SQLErrText + "The interface records WERE created.")
		f_pp_msgs(sqls)
		f_pp_msgs(" ")
		rollback;
		overall_rtn = -1
		goto clear_stg_table
	end if
	
else	//	All postings have a where clause restriction.
	
	for g = 1 to upperbound(i_where_clause_array)
		
		where_clause = i_where_clause_array[g]
		
		if obey_approval = 'YES' then
			sqls = "update cr_budget_data set batch_id = " + string(g_batch_id) + ' ' + &
			 where_clause + " and batch_id = 999999999999999 and id in (select id from cr_temp_cr_gl_bdg) " +&
			 "and upper(trim(budget_version)) = '" + i_budget_version + "'"
		else
			sqls = "update cr_budget_data set batch_id = " + string(g_batch_id) + ' ' + &
			 where_clause + " and batch_id is NULL and id in (select id from cr_temp_cr_gl_bdg) " +&
			 "and upper(trim(budget_version)) = '" + i_budget_version + "'"
		end if
		 
		execute immediate :sqls;
 
		if sqlca.SQLCode < 0 then
			f_pp_msgs(sqls)
			f_pp_msgs("ERROR: Updating cr_budget_data.batch_id: " + &
					 sqlca.SQLErrText + "The interface records WERE created.")
			rollback;
			overall_rtn = -1
			goto clear_stg_table
		end if
		
	next
	
end if
		 
//EAG: This was removed since it is important only to Actuals.
////	Backfill the cr_budget_data.gl_id 
////	A where clause restriction similar to batch_id is not needed for the gl_id because
////		there is a restriction that the batch_id equals what we just filled in above effectively
////		acting as our where clause.
//f_pp_msgs("Updating cr_budget_data.gl_id ...")
//update cr_budget_data a set gl_id = (
//	select gl_id from cr_temp_cr_gl_bdg b
//    where a.id = b.id)
//	where batch_id = :g_batch_id; 
//  	 
// //where batch_id is null
// 
// if sqlca.SQLCode < 0 then
//		f_write_log(g_log_file, "ERROR: Updating cr_budget_data.gl_id: " + &
//	       sqlca.SQLErrText + "The interface records WERE created.")
//	f_pp_msgs("ERROR: Updating cr_budget_data.gl_id: " + &
//	       sqlca.SQLErrText + "The interface records WERE created.")
//	rollback;
//	error = true
//	goto clear_stg_table
//end if


//	Clear out the "main" temp table.
sqlca.truncate_table('cr_temp_cr_gl_bdg');

if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: Truncating cr_temp_cr_gl_bdg : " + sqlca.sqlerrtext)
	rollback;
	overall_rtn = -1
	goto clear_stg_table
end if

clear_stg_table:

destroy i_ds_posting_audit

for l = 1 to num_postings
	
	i_stg_table_name = ds_post_to_gl_columns_bdg.getitemstring(l,"stg_table_name")
	
	sqlca.truncate_table(i_stg_table_name);
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs("ERROR: Truncating " + i_stg_table_name + " : " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

next

if overall_rtn < 0 then
	return overall_rtn
end if

//	Call a custom function for any additional processing.
//	-1 as the posting_id indicates the function was called at the end
//rtn = f_cr_posting_bdg(2,-1)
for m = 1 to num_postings 
	
	i_posting_id = ds_post_to_gl_columns_bdg.getitemnumber(m,"posting_id")
	/*### - MDZ - 30211 - 20130619*/
	rtn = f_cr_posting_bdg(2,i_posting_id)

	if rtn < 0 then
		rollback;
		return -1
	end if

next


commit;
the_end:
return 1
end function

public function longlong uf_insert_budget_to_gl ();string post_to_gl_table, sqls, gl_column_name, cr_column_name, create_text_file, journal_cat, &
	company, company_field, fields[], company_sqls, journal_cat_list, journal_cat_sqls,journal_fields[], &
	log_sqls, log_sqls2, s_date, spec_proc_dates2_sqls
longlong i, rtn, posted_to_gl, cr_column, d, num_trans
dec {2} debits, credits


//  Update batch_id in cr_temp_cr_gl_bdg_temp so we can pass it to the external
//  system if needed.
sqls = "update cr_temp_cr_gl_bdg_temp set batch_id = " + string(g_batch_id)
execute immediate :sqls;
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_temp_cr_gl_bdg_temp.batch_id: " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	f_pp_msgs(sqls)
	f_pp_msgs("  ")
	return -1
end if


uo_ds_top ds_budget_to_gl
ds_budget_to_gl = create uo_ds_top

//sqls = " select replace(replace(replace(gl_column_name,' ','_'),'-','_'),'/','_') gl_column_name, " + &
//	    "replace(cr_column_name,' ','_') cr_column_name, " + &
//	    "cr_column from cr_post_to_gl_columns_bdg " + &
//		 " where posting_id = " + string(i_posting_id) + "order by " + '"ORDER"'

//	DMJ: 10/6/08: DML: 080907: 1370: Don't replace spaces or special characters with underscores if
//		the column is not a CR column.  Any space hardcoding or text with spaces were
//		being turned to underscores.
sqls = " select replace(replace(replace(gl_column_name,' ','_'),'-','_'),'/','_') gl_column_name, " + &
	    "decode(cr_column,0,cr_column_name,replace(cr_column_name,' ','_')) cr_column_name, " + &
	    "cr_column from cr_post_to_gl_columns_bdg " + &
		 " where posting_id = " + string(i_posting_id) + "order by " + '"ORDER"'

f_create_dynamic_ds(ds_budget_to_gl,"grid",sqls,sqlca,true)
rtn = ds_budget_to_gl.rowcount()

sqls = " insert into " + i_stg_table_name + " ( "

for i = 1 to rtn 

	gl_column_name = ds_budget_to_gl.getitemstring(i,"gl_column_name")
	gl_column_name = '"' + upper(gl_column_name) + '"'
	
	if i = rtn then
		sqls = sqls + gl_column_name + ") "
	else
		sqls = sqls + gl_column_name + ", "
	end if
	
next 

//	build the select
sqls = sqls + " select " 

for i = 1 to rtn 

	cr_column_name = ds_budget_to_gl.getitemstring(i,"cr_column_name")
//	company = ds_budget_to_gl.getitemstring(i,"company")
	cr_column = ds_budget_to_gl.getitemnumber(i,"cr_column")
	
	// DMJ: 10/6/08: DMJ: 10/19/2007: abs(amount) would not work with duplicate records.
	gl_column_name = ds_budget_to_gl.getitemstring(i,"gl_column_name")
	
	//	DML: 072903: Blank values are being turned to underscores.  I need the
	//	replace command for the blanks in the column name if it is a CR column.
	//	However, if they just want a blank space don't turn it into an underscore.
	if cr_column_name = "'_'" then cr_column_name = "' '"
	
	if i = 1 then
		
		select company, gl_journal_category into :company, :journal_cat_list
		from cr_post_to_gl_control_bdg
		where posting_id = :i_posting_id;
		
		//	parse the company value out and create the "in" 
		company_sqls = '('
		f_parsestringintostringarray(company,',',fields[])
	
		for d = 1 to upperbound(fields)
			
			company = fields[d]
			company = "'" + company + "'"
			
			if d = upperbound(fields) then
				company_sqls = company_sqls + company + ')'
			else
				company_sqls = company_sqls + company + ','
			end if
			
		next
		
		//	parse the journal category value out and create the "in" 
		journal_cat_sqls = '('
		f_parsestringintostringarray(journal_cat_list,',',journal_fields[])
	
		for d = 1 to upperbound(journal_fields)
			
			journal_cat = journal_fields[d]
			journal_cat = "'" + journal_cat + "'"
			
			if d = upperbound(journal_fields) then
				journal_cat_sqls = journal_cat_sqls + journal_cat + ')'
			else
				journal_cat_sqls = journal_cat_sqls + journal_cat + ','
			end if
			
		next
	end if
	
	//	if this is a CR column then put the upper and double quotes
	if cr_column = 1 and lower(cr_column_name) <> 'amount' and &
		lower(cr_column_name) <> 'quantity' and lower(cr_column_name) <> 'dr_amount' and &
		lower(cr_column_name) <> 'cr_amount' then
		cr_column_name = '"' + upper(cr_column_name) + '"'
	end if
	
	choose case lower(cr_column_name)
		case 'amount'
			//	DML: 092605: If the dr_cr_id is incorrect then the abs will cause
			//		an oob journal.  Although the dr_cr_id should be corrected it
			//		shouldn't impact posting to the GL.  So, take the amount as is.
			//		Make this change for the dr_amount and cr_amount columns too.
//			cr_column_name = 'sum(abs(amount) * dr_cr_id)'
			cr_column_name = 'sum(amount)'
		case 'quantity'
			cr_column_name = 'sum(quantity)'
		//	DML: 092204: When posting to Oracle, we need a separate DR and CR amount
		//		columns.  cr_post_to_gl_columns_bdg will plug in dr_amount or cr_amount
		//		in the CR column name and this code will handle it.
		case 'dr_amount'
//			cr_column_name = 'sum(decode(dr_cr_id,1,amount,0))'
			cr_column_name = 'sum(decode(sign(amount),1,amount,0))'
		case 'cr_amount'
			//cr_column_name = 'sum(decode(dr_cr_id,-1,amount * dr_cr_id,0))'
			cr_column_name = 'sum(decode(sign(amount),-1,amount * -1,0))'
	end choose
	
	// DMJ: 10/6/08: DMJ: 10/19/2007: abs(amount) would not work with duplicate records.
	// DMJ: 10/31/2007: in the 10/19 change I had incorrectly referenced "amount", "quantity", etc.
	// Those values had been changed above to "sum(amount)", "sum(quantity)", etc.
	if lower(gl_column_name) = "amount" and lower(cr_column_name) <> "sum(amount)" and &
		lower(cr_column_name) <> "sum(quantity)" and lower(cr_column_name) <> "sum(decode(sign(amount),1,amount,0))" and &
		lower(cr_column_name) <> "sum(decode(sign(amount),-1,amount * -1,0))" &
	then
		cr_column_name =  "sum(" + cr_column_name + ")"
	end if
	
	if i = rtn then
		sqls = sqls + cr_column_name 
	else
		sqls = sqls + cr_column_name + ", "
	end if
	
next 

//	Get the company value from the system control
select upper(replace(replace(replace(control_value,' ','_'),'-','_'),'/','_')) into :company_field
from cr_system_control
where lower(control_name) = 'company field';

//	DML: 051005: Williams, filter by the journal categories specified in cr_post_to_gl_control_bdg

sqls = sqls + " from cr_temp_cr_gl_bdg_temp "

if company <> "'*'" then
	
	log_sqls = ' where "' + company_field + '"'
	log_sqls = log_sqls + " in " + company_sqls
	sqls = sqls + ' where "' + company_field + '"'
	sqls = sqls + " in " + company_sqls
	
end if

if journal_cat <> "'*'" and not isnull(journal_cat) and journal_cat <> "" then
	if company = "'*'" then
		log_sqls = log_sqls + ' where '
		sqls = sqls + ' where '
	else
		log_sqls = log_sqls + ' and '
		sqls = sqls + ' and '
	end if
	log_sqls = log_sqls + 'gl_journal_category in ' + journal_cat_sqls
	sqls = sqls + 'gl_journal_category in ' + journal_cat_sqls
end if

i_array_counter++
i_posting_id_array[i_array_counter] = i_posting_id
if isnull(log_sqls) or log_sqls = "" then log_sqls = 'nowhereclause'
i_where_clause_array[i_array_counter] = log_sqls

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")

//	Provide an audit for this posting in the log and build the sqls string for cr_special_processing_dates2.
if log_sqls = 'nowhereclause' then
	log_sqls2 = "select count(*), nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0) " + &
		"from cr_temp_cr_gl_bdg_temp " 
	spec_proc_dates2_sqls = "insert into cr_special_processing_dates2_b " + &
		" (id, process_date, posting_id, total_dollars, total_records, total_debits, total_credits, batch_id, budget_version) " + &
		" select 11, to_date('" + s_date + "','yyyy-mm-dd hh24:mi:ss'), " + string(i_posting_id) + ",nvl(sum(amount),0),count(*), " + &
		" nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0), " + string(g_batch_id) + ", '" + i_budget_version + "'" + &
		" from cr_temp_cr_gl_bdg_temp "
else
	log_sqls2 = "select count(*), nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0) " + &
		"from cr_temp_cr_gl_bdg_temp " + log_sqls
	spec_proc_dates2_sqls = "insert into cr_special_processing_dates2_b " + &
		" (id, process_date, posting_id, total_dollars, total_records, total_debits, total_credits, batch_id, budget_version) " + &
		" select 11, to_date('" + s_date + "','yyyy-mm-dd hh24:mi:ss'), " + string(i_posting_id) + ",nvl(sum(amount),0),count(*), " + &
		" nvl(sum(decode(sign(amount),-1,0,amount)),0), nvl(sum(decode(sign(amount),-1,amount,0)),0), " + string(g_batch_id) + ", '" + i_budget_version + "'" + &
		" from cr_temp_cr_gl_bdg_temp " + log_sqls
end if
i_ds_posting_audit.setsqlselect(log_sqls2)	
i_ds_posting_audit.retrieve()
num_trans 	= i_ds_posting_audit.getitemnumber(1,1)
debits 		= i_ds_posting_audit.getitemdecimal(1,2)
credits 		= i_ds_posting_audit.getitemdecimal(1,3)

f_pp_msgs("")
f_pp_msgs("Posting " + string(num_trans) + " transactions.")
f_pp_msgs("Total credits = " + string(credits))
f_pp_msgs("Total debits = " + string(debits))
f_pp_msgs("")


sqls = sqls + " group by "
	

//	build the group by ...if no amount, quantity, group by acts as a distinct
for i = 1 to rtn 

	cr_column_name = ds_budget_to_gl.getitemstring(i,"cr_column_name")
	cr_column = ds_budget_to_gl.getitemnumber(i,"cr_column")
	gl_column_name = ds_budget_to_gl.getitemstring(i,"gl_column_name") // DMJ: 10/1/08
	
	//	DML: 072903: Blank values are being turned to underscores.  I need the
	//	replace command for the blanks in the column name if it is a CR column.
	//	However, if they just want a blank space don't turn it into an underscore.
	if cr_column_name = "'_'" then cr_column_name = "' '"
	
	//	if this is a CR column then put the upper and double quotes
	if cr_column = 1 and lower(cr_column_name) <> 'amount' and &
		lower(cr_column_name) <> 'quantity' and lower(cr_column_name) <> 'dr_amount' and &
		lower(cr_column_name) <> 'cr_amount' then
		cr_column_name = '"' + upper(cr_column_name) + '"'
	end if
	
	choose case lower(cr_column_name)
		//	DML: 092204: When posting to Oracle, we need a separate DR and CR amount
		//		columns.  cr_post_to_gl_columns_bdg will plug in dr_amount or cr_amount
		//		in the CR column name and this code will handle it.
		case 'amount','quantity','dr_amount','cr_amount'
			continue
	end choose
	
	// DMJ: 10/19/2007: abs(amount) would not work with duplicate records.
	if lower(gl_column_name) = "amount" and lower(cr_column_name) <> "amount" and &
		lower(cr_column_name) <> "quantity" and lower(cr_column_name) <> "dr_amount" and &
		lower(cr_column_name) <> "cr_amount" &
	then
		//  We just added a "sum()" above ... don't add this to the group by.
		continue
	end if
	
	// DMJ: 07/21/2008: need to support sub-selects ... if left(1) = "(" then don't
	// add to group by.
	if left(cr_column_name, 1) = "(" then
		continue
	end if
	
	// DMJ: 10/07/2008: need to support summing of non-standard fields ... 
	// e.g. "sum(decode( ... ))".
	if left(upper(cr_column_name), 4) = "SUM(" then
		continue
	end if
	
	if i = rtn then
		sqls = sqls + cr_column_name
	else
		sqls = sqls + cr_column_name + ", "
	end if
	
next 

//	DML: 092204: If an amount/quantity field are last on the table the comma
//		doesn't get trimmed.
if right(trim(sqls),1) = ',' then
	sqls = mid(sqls,1,len(sqls) - 2)
end if

execute immediate :sqls;
if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into " + i_stg_table_name + ": " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	rollback;
	f_pp_msgs(sqls)
	f_pp_msgs("  ")
	return -1
end if

execute immediate :spec_proc_dates2_sqls;
if sqlca.sqlcode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Inserting into cr_special_processing_dates2: " + sqlca.sqlerrtext)
	f_pp_msgs("  ")
	//	Having a rollback here means the posting will also rollback?  For now at least, it
	//		is all or nothing per posting including the audit table.
	rollback;
	f_pp_msgs(spec_proc_dates2_sqls)
	f_pp_msgs("  ")
	return -1
end if

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_posting_bdg_custom_version nvo_cr_posting_bdg_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_posting_bdg_custom.pbd'
		return nvo_cr_posting_bdg_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

