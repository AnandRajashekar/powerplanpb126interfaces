HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong	i_num_elements
string 					i_use_new_structures_table, i_structure_column

uo_ds_top				i_ds_parse_element
uo_ds_top				i_ds_elements
uo_ds_top				i_ds_rules
uo_ds_top				i_ds_ids
uo_ds_top				i_ds_count_sqls
uo_ds_top				i_ds_structure_id

string	i_exe_name = 'cr_build_combos.exe'

end variables

forward prototypes
public function longlong uf_read ()
public function string uf_parse_mult_structure (string a_element_to_parse, longlong a_rule_id, longlong a_id)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();longlong i, structure_id
string use_new_structures_table
longlong num_rules, d, counter, element_counter, num_cols, &
	rule_id, min_id, max_id, id, xx, yy, stge_max_id, &
	structure_array_counter, found, g, gg,  &
	num_ids, selected_column_counter, &
	num_structures, running_session_id, ii, rtn, old_stge_max_id, r, control_status, &
	range_based, range_counter, overall_rtn
string insert_sqls, element, null_sql, structure_sql, hold_sql, count_sqls, &
	element_array[],element_alias_array[], alias_array[], select_sqls, sqls, &
	col, structure_sqls, structure_array[], clear_array[], from_sqls, where_sqls, &
	relational_cols_sqls, element_column, where_sqls1, element1, ids_sqls, &
	element_sqls, col_check, selected_column_array[], final_element_column, &
	description, final_description, reset_array[], parsed_point, &
	user_id, col1, col_hold, relational_cols_sqls1, cv, element_name, aa_where_sqls, &
	ds_ids_sqls, rc, element_for_modify, effmn_cv, point,point_array[], rule_id_in
	
uo_cr_structure_value_ranges uo_cr_ranges
uo_cr_ranges = CREATE uo_cr_structure_value_ranges	

overall_rtn = 1
//*******************************************************************
//
//		  If they are setup using control then there is no reason
//			to ever build combos.  Stop if that is the case
//
//*******************************************************************
setnull(cv)
select upper(control_value) into :cv
from cr_system_control
where upper(control_name) = 'VALIDATIONS - CONTROL OR COMBOS';
if isnull(cv) then cv = 'CONTROL'

if cv = 'CONTROL' then
	f_pp_msgs(" ")
	f_pp_msgs("WARNING: Validations are set to go against the contol table.")
	f_pp_msgs("There is no need to create combos.")
	f_pp_msgs(" ")
	return 1
end if

//*******************************************************************
//
//		  Retrieve applicable system switches 
//
//*******************************************************************
setnull(i_use_new_structures_table)
select upper(trim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(trim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"

setnull(effmn_cv)
select upper(trim(control_value)) into :effmn_cv from cr_system_control
 where upper(control_name) = 'VALIDATIONS: EFFECTIVE MONTH NUMBER';
if isnull(effmn_cv) then effmn_cv = "NO"


//*******************************************************************
//
//		 Build i_ds_parse_element datastore for use later
//
//*******************************************************************
i_ds_parse_element = create uo_ds_top

sqls = "select rpad(' ', 254) from dual "

f_create_dynamic_ds(i_ds_parse_element,'grid',sqls,sqlca,true)


//*******************************************************************
//
//		 Build i_ds_elements datastore for use later
//
//*******************************************************************
i_ds_elements = CREATE uo_ds_top
f_create_dynamic_ds(i_ds_elements,'grid','select * from cr_elements order by "ORDER"',sqlca,true)
i_num_elements = i_ds_elements.rowcount()
//i_ds_elements = CREATE uo_ds_top
//i_ds_elements.DataObject = 'dw_cr_element_definitions'
//i_ds_elements.SetTransObject(sqlca)
//i_num_elements = i_ds_elements.RETRIEVE()


//*******************************************************************
//
//		 Build i_ds_ids datastore for use later
//
//*******************************************************************
 i_ds_ids = CREATE uo_ds_top
f_create_dynamic_ds(i_ds_ids,'grid','SELECT "CR_VALIDATION_CONTROL"."ID" FROM "CR_VALIDATION_CONTROL" WHERE "CR_VALIDATION_CONTROL"."RULE_ID" = -1',sqlca,true)

// i_ds_ids = CREATE uo_ds_top
//i_ds_ids.DataObject = 'dw_cr_validations_ids'
//i_ds_ids.SetTransObject(sqlca)


//*******************************************************************
//
//		 Build i_ds_rules datastore for use later
//
//*******************************************************************
f_pp_msgs("Retrieving control records at " + string(now()))
i_ds_rules = CREATE uo_ds_top

sqls = "select * from cr_validation_control"

f_create_dynamic_ds(i_ds_rules,"grid",sqls,sqlca,true)


//*******************************************************************
//
//		 Build i_ds_count_sqls datastore for use later
//
//*******************************************************************
i_ds_count_sqls = create uo_ds_top

sqls = "select rpad(' ', 254) from dual "

f_create_dynamic_ds(i_ds_count_sqls,'grid',sqls,sqlca,true)


//*******************************************************************
//
//		 Build i_ds_structure_id datastore for use later
//
//*******************************************************************
i_ds_structure_id = create uo_ds_top

sqls = "select rpad(' ', 254) from dual "

f_create_dynamic_ds(i_ds_structure_id,'grid',sqls,sqlca,true)


//*******************************************************************
//
//		  In addition to the CR - Build Combos interface being
//			flagged as running, we also need to watch the 
//			Create Validation Combos process that is used
//			by the application.
//
//*******************************************************************
running_session_id = 0
select running_session_id, executable_file
into :running_session_id, :user_id
from pp_processes
where lower(description) = 'create validation combos';

if isnull(running_session_id) then running_session_id = 0

if running_session_id <> 0 then
	f_pp_msgs(" ")
	f_pp_msgs("WARNING: Combos are currently being created by " + user_id)
	f_pp_msgs(" ")
	return 1
else
	
	select userenv('sessionid'),user
	into :running_session_id, :user_id
	from dual;
	
	update pp_processes
	set running_session_id = :running_session_id,
		executable_file = :user_id
	where lower(description) = 'create validation combos';
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: Updating pp_processes.running_session_id (Create Validation Combos): " + sqlca.sqlerrtext)
		f_pp_msgs(" ")
		rollback;
		return -1
	else
		commit;
	end if
	
end if

//*******************************************************************
//
//		  Truncate the combos table -- the standalone will always
//			build all rules
//
//*******************************************************************
sqlca.truncate_table('cr_validation_combos')

if sqlca.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Truncating cr_validation_combos: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	rollback;
	overall_rtn = -1
	goto clear_user
end if


//	Section 1: Structures are not being used at all.
//  DMJ:  needed id in section 1.
stge_max_id = 0

sqls = " select rule_id, range_based from cr_validation_rules"
uo_ds_top ds_rules
ds_rules = create uo_ds_top
f_create_dynamic_ds(ds_rules,"grid",sqls,sqlca,true)
num_rules = ds_rules.rowcount()

//  DMJ:  needed id in section 1.  Added "id,"
if effmn_cv = "YES" then
	//  Status and effective_month_number get copied to combos.
	//  Changed from "staging".
	sqls = ' insert /*+append*/ into cr_validation_combos (id, status, effective_month_number, rule_id, gl_rollup, effective_month_number2,'
else
	sqls = ' insert /*+append*/ into cr_validation_combos (id, rule_id, gl_rollup '
end if
null_sql = ' ('
structure_sql = ' and '

setpointer(hourglass!)

for i = 1 to i_num_elements
	
	setpointer(hourglass!)
	
	//  Accounting Key columns ...
	element  = lower(i_ds_elements.GetItemString(i, 'description'))
	element = f_cr_clean_string(element)
	//  DMJ:  need squiggles.
	element_for_modify = '~~"' + upper(element) + '~~"'
	element = '"' + upper(element) + '"'
	
	//	Insert sql
	if i = i_num_elements then
		sqls = sqls + element + ') '
	else
		sqls = sqls + element + ', '
	end if
	
	//	'Not is null' sql
	if i = i_num_elements then
		null_sql = null_sql + 'b.' + element + ' is not null )'
	else 
		null_sql = null_sql + 'b.' + element + ' is not null or '
	end if
	
	//	'Not a structure' sql
	if i = i_num_elements then
		structure_sql = structure_sql + "nvl(a." + element + ",' ') not like '{%' "
	else
		structure_sql = structure_sql + "nvl(a." + element + ",' ') not like '{%' and "
	end if
	
	//  DMJ:  while I'm here, I need to append a where clause to i_ds_ids so I do not get
	//        records in the subsequent sections that have no structure values.  This saves
	//        a significant amount of time.
	if i = 1 then
		ds_ids_sqls = " and ("
	end if
	
	if i = i_num_elements then
		ds_ids_sqls = ds_ids_sqls + " " + element_for_modify + " like '{%')"
	else
		ds_ids_sqls = ds_ids_sqls + " " + element_for_modify + " like '{%' or "
	end if
	
next 

//  DMJ:  needed id in section 1.  Added stge_max_id.
if effmn_cv = "YES" then
	//  Status and effective_month_number get copied to combos.
	sqls = sqls + '(select ' + string(stge_max_id) + ' + rownum, a.status, ' + &
		"decode(a.effective_month_number,NULL,to_number(to_char(to_date(to_char(sysdate,'yyyy'),'yyyy'),'yyyymm')),a.effective_month_number)," + &
		"a.rule_id, gl_rollup, "+ &
		"decode(a.effective_month_number2,NULL,to_number(to_char(to_date(to_char(sysdate,'yyyy'),'yyyy'),'yyyymm')),a.effective_month_number2), "
else
	sqls = sqls + '(select ' + string(stge_max_id) + ' + rownum, a.rule_id, gl_rollup, '
end if

for i = 1 to i_num_elements
	
	setpointer(hourglass!)
	
	//  Accounting Key columns ...
	element = lower(i_ds_elements.GetItemString(i, 'description'))
	element = f_cr_clean_string(element)
	element = '"' + upper(element) + '"'
	
	//	DML: The element columns on this window are large enough to 
	//			allow for structures (75) so on direct entry, the column
	//			definition can't be used to restrict the entry to the 
	//			actual element width.  So, trim...
	
	//  Insert sql
	//  07/06/2005:  added decode.
	if i = i_num_elements then
		sqls = sqls + 'decode(trim(a.' + element +     '), null, ' + "' '" + ',trim(a.' + element + ')) '
	else
		sqls = sqls + 'decode(trim(a.' + element +     '), null, ' + "' '" + ',trim(a.' + element + ')), '
	end if
	
next

hold_sql = sqls + ' from cr_validation_control a, cr_validation_rules b '	+ &
	' where ' + null_sql + structure_sql + ' and a.rule_id = b.rule_id and a.rule_id ' + &
	' = ' 
	
for i = 1 to num_rules
	
	rule_id = ds_rules.getitemnumber(i,"rule_id")
	
	range_based = ds_rules.GetItemNumber(i,"range_based")
	if isnull(range_based) then range_based = 0
	
	if range_based = 1 then
		// We don't want to build combos for Range Based rules
		continue
	end if
	
	sqls = hold_sql
	
	setpointer(hourglass!)
	f_pp_msgs('Inserting non-structure combos ' + string(i) + ' of ' + string(num_rules) + ' at ' + string(now()))

	if effmn_cv = "YES" then
		//  Status gets copied to combos.
		sqls = sqls + string(rule_id) + '  ) '
	else
		sqls = sqls + string(rule_id) + ' and status = 1 ) '
	end if
	
	old_stge_max_id = stge_max_id
	
	//  Changed from "staging".
	stge_max_id = 0
	select max(id) into :stge_max_id from cr_validation_combos;
	if isnull(stge_max_id) then stge_max_id = 0
	
	//  Always 0, due to sqls = hold_sql above.
	sqls = f_replace_string(sqls, string(0) + " + rownum", &
		string(stge_max_id) + " + rownum", "all")
	
	execute immediate :sqls;
	if sqlca.sqlcode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs('ERROR: Inserting into cr_validation_combos for Section 1 : ' + sqlca.sqlerrtext)
		f_pp_msgs(sqls)
		f_pp_msgs(" ")
		rollback;
		overall_rtn = -1
		goto clear_user
	else
		commit;
	end if
	
	
next
	
commit;


//	Section 2:	Only structures are being used.
//	Verify that structures are being used.  If not, there is no need to
//	go through this section.
num_structures = 0
select count(*) into :num_structures
from cr_structures;

if isnull(num_structures) or num_structures = 0 then
	goto skip_structure_code
end if

//Build the alias array with letters b - z.  If they have more than 25
//	elements they are validating for, well then... :)
alias_array[1] = 'b'	//'a' is taken already (cr_validation_control)
alias_array[2] = 'c'
alias_array[3] = 'd'
alias_array[4] = 'e'
alias_array[5] = 'f'
alias_array[6] = 'g'
alias_array[7] = 'h'
alias_array[8] = 'i'
alias_array[9] = 'j'
alias_array[10] = 'k'
alias_array[11] = 'l'
alias_array[12] = 'm'
alias_array[13] = 'n'
alias_array[14] = 'o'
alias_array[15] = 'p'
alias_array[16] = 'q'
alias_array[17] = 'r'
alias_array[18] = 's'
alias_array[19] = 't'
alias_array[20] = 'u'
alias_array[21] = 'v'
alias_array[22] = 'w'
alias_array[23] = 'x'
alias_array[24] = 'y'
alias_array[25] = 'z'

num_cols = long(i_ds_rules.Object.DataWindow.Column.Count)
//	Get the max(id) from the staging table (cr_validation_combos_staging)
//  Changed from "staging".
stge_max_id = 0
select max(id) into :stge_max_id from cr_validation_combos;
if isnull(stge_max_id) then stge_max_id = 0


// ### 7209:  ds_ids_sqls has tildes in it that work for the f_add_where_clause_ds used in the application, but it needs to be
//	removed here since we're doing direct SQL.
ds_ids_sqls = f_replace_string(ds_ids_sqls,'~~','','all')

////  DMJ:  Need to skip the "value only" records.  This significantly helps the processing time.
//f_add_where_clause_ds(i_ds_ids, "", ds_ids_sqls, false)

//	Section 3:	A mix of structures and non-structures are being used.

//  DMJ: moved the count into the loop.  NOTE: for rule_id = 1 to ... was wrong.  The 
//       rule_ids are not necessarily sequential.

for r = 1 to num_rules
	
	setpointer(hourglass!)
	
	rule_id = ds_rules.GetItemNumber(r, "rule_id")
	
	range_based = ds_rules.GetItemNumber(r,"range_based")
	if isnull(range_based) then range_based = 0
	
	if range_based = 1 then
		// We don't want to build combos for Range Based rules
		continue
	end if
	
	f_pp_msgs('Inserting mix of structures and non-structures ' + &
		string(r) + ' of ' + string(num_rules) + ' at ' + string(now()))
	
	structure_array_counter = 0
	structure_array[] = clear_array[]
	
	sqls = 'SELECT "CR_VALIDATION_CONTROL"."ID" FROM "CR_VALIDATION_CONTROL" WHERE "CR_VALIDATION_CONTROL"."RULE_ID" = ' + string(rule_id) + ' ' + ds_ids_sqls
	i_ds_ids.SetSQLSelect(sqls)
	num_ids = i_ds_ids.retrieve()
	
	for i = 1 to num_ids
		
		if mod(i,50) = 0 or i = num_ids then f_pp_msgs(' -- Record ' + string(i) + " of " + string(num_ids))
			
		//	Get the max(id) from the staging table (cr_validation_combos_staging)
		//  Changed from "staging".
		stge_max_id = 0
		select max(id) into :stge_max_id from cr_validation_combos;
		if isnull(stge_max_id) then stge_max_id = 0
		
		id = i_ds_ids.getitemnumber(i,"id")
		
		control_status = 0
		select status into :control_status from cr_validation_control where id = :id;
		if isnull(control_status) then control_status = 0
		
		if effmn_cv = "YES" then
			//  Both statuses go to combos.
		else
			//  Previously i_ds_ids had "and status = 1".  That was removed when effmn was added.
			if control_status = 0 then continue
		end if
		
		structure_array_counter = 0
		structure_array[] = clear_array[]
		
		if effmn_cv = "YES" then
			//  Status and effective_month_number get copied to combos.
			//  Changed from "staging".
			insert_sqls = ' insert /*+append*/ into cr_validation_combos (id, status, effective_month_number, rule_id, gl_rollup, effective_month_number2, '
		else
			insert_sqls = ' insert /*+append*/ into cr_validation_combos (id, rule_id, gl_rollup, '
		end if
		
		if effmn_cv = "YES" then
			//  Status and effective_month_number get copied to combos.
			select_sqls = ' select ' + string(stge_max_id) + ' + rownum,aa.* from ' + &
				' (select distinct a.status, ' + &
				"decode(a.effective_month_number,NULL,to_number(to_char(to_date(to_char(sysdate,'yyyy'),'yyyy'),'yyyymm')),a.effective_month_number)," + &
				string(rule_id) + ', gl_rollup, ' + &
				"decode(a.effective_month_number2,NULL,to_number(to_char(to_date(to_char(sysdate,'yyyy'),'yyyy'),'yyyymm')),a.effective_month_number2), "
		else
			select_sqls = ' select ' + string(stge_max_id) + ' + rownum,aa.* from ' + &
				' (select distinct ' + string(rule_id) + ', gl_rollup, ' 
		end if
		
		element_counter = 0
	
		for d = 1 to i_num_elements
			
			setpointer(hourglass!)
			
			//  Accounting Key columns ...
			element  = lower(i_ds_elements.GetItemString(d, 'description'))
			element = f_cr_clean_string(element)
			//	The datawindow.table.select below gets cranky with element the way it is.
			//		Format element1 differently and use it in the select
			element1 = '~~"' + upper(element) + '~~"'
			element = '"' + upper(element) + '"'
			
			count_sqls = '	select to_char(count(*)) from cr_validation_rules ' + &
				'where rule_id = ' + string(rule_id) + ' and ' + element1 + ' is null and ' + &
				element1 + ' <> 0'
			
			count_sqls = 'datawindow.table.select = "' + count_sqls +  ' "'
	
			i_ds_count_sqls.modify(count_sqls)
			i_ds_count_sqls.settransobject(sqlca)
			i_ds_count_sqls.retrieve()
			counter = long(i_ds_count_sqls.getitemstring(1,1))
			if isnull(counter) then counter = 0
			
			if counter = 1 then continue
			
			element_counter++
			element_array[element_counter] = element
			element_alias_array[element_counter] = alias_array[element_counter]
			
			insert_sqls = insert_sqls + element + ', '
			//	I have to prefix the element to avoid the 'column ambiguously 
			//		defined' message.
			
			//  DMJ:  Need to save the element name.
			element_name = element
			
			element = 'decode(' + alias_array[element_counter] + '.' + element + &
				', null, a.' + element + ', ' + alias_array[element_counter] + '.' + element + &
				') '
			
			//	DML: The element columns on this window are large enough to 
			//			allow for structures (75) so on direct entry, the column
			//			definition can't be used to restrict the entry to the 
			//			actual element width.  So, trim...
			
			//select_sqls = select_sqls + element + ', '
			
			//  DMJ CHANGE:
			select_sqls = select_sqls + 'decode(trim(' + element + '), null, ' + "' '" + ', trim(' + element + ')) as ' + element_name + ', '
			
			//  DMJ - Build some extra where clause.  This is to prevent misbehavior when structure
			//  summary points are in the validation control that have no detail points under them.
			if d = 1 then
				aa_where_sqls = " where (aa." + element_name + " not like '{%' or aa." + element_name + " is null) "
			else
				aa_where_sqls = aa_where_sqls + " and (aa." + element_name + " not like '{%' or aa." + element_name + " is null) "
			end if
			
		next  //  for d = 1 to i_num_elements ...
		
		select_sqls = left(select_sqls,len(select_sqls) - 2) 
		insert_sqls = left(insert_sqls,len(insert_sqls) - 2) + ')'
		
		insert_sqls = insert_sqls + select_sqls + ' from cr_validation_control a, ' 
		
		for xx = 1 to upperbound(element_array)
			
			element = element_array[xx]
			//	The double quotes attached to the element above causes the "if" below to
			//		not match up.  Remove the double quotes for this loop.
			element1 = f_replace_string(element,'"','',"all")
			
			for yy = 1 to num_cols
				
				col = i_ds_rules.Describe('#' + string(yy) + '.Name')
				
				if lower(col) = lower(element1) then
					i_structure_column = i_ds_rules.Describe('#' + string(yy + 1) + '.Name')
					
					if effmn_cv = "YES" then
						//  Status gets copied to combos.
						structure_sqls = ' select to_char(' + i_structure_column + ') from cr_validation_control ' + &
							' where rule_id = ' + string(rule_id) + ' and id = ' + string(id)
					else
						structure_sqls = ' select to_char(' + i_structure_column + ') from cr_validation_control ' + &
							' where status = 1 and rule_id = ' + string(rule_id) + ' and id = ' + string(id)
					end if
					
					structure_sqls = 'datawindow.table.select = "' + structure_sqls +  ' "'
			
					i_ds_structure_id.modify(structure_sqls)
					i_ds_structure_id.settransobject(sqlca)
					i_ds_structure_id.retrieve()
					if i_ds_structure_id.rowcount() = 0 then exit
					structure_id = long(i_ds_structure_id.getitemstring(1,1))
					if isnull(structure_id) then structure_id = 0
					
					// SEK 092109: Need to re-apply the ranges in case any values have
					// 				been added to an element table since the structures
					//					were built.  This does not apply to range based rules.
					range_counter = 0
					if i_use_new_structures_table = "YES" then
						select count(*) into :range_counter
						  from cr_structure_values2
						 where structure_id = :structure_id
							and range_start is not null
							and range_end is not null;
					else
						select count(*) into :range_counter
						  from cr_structure_values
						 where structure_id = :structure_id
							and range_start is not null
							and range_end is not null;
					end if
					
					if range_counter > 0 then
						rtn = uo_cr_ranges.uf_apply_all(structure_id)
						
						if rtn = -1 then
							// message in uo
							rollback;
							return -1
						else
							commit;	
						end if
					end if
					
					structure_array_counter++
					structure_array[structure_array_counter] = i_structure_column
					exit
				end if
				
			next  //  for yy = 1 to num_cols ...
			
			if i_use_new_structures_table = "YES" then
				
				//	DML: 102505: Modifications to allow for multiple structure points.
				
				//	Parse out the structure point from the brackets
				point = uf_parse_mult_structure(element,rule_id,id)
				//	The array must be reset each time through the loop
				point_array = reset_array
				f_parsestringintostringarray(point,',',point_array[])
				
				//	If the upperbound > 1 then I know more then one point was used.
				if upperbound(point_array) > 1 then
				
					for g = 1 to upperbound(point_array)
						parsed_point = point_array[g]
						
						insert_sqls = insert_sqls + " (select structure_id, substr(element_value, 1" + &
							", decode(instr(element_value, ':'), 0, length(element_value) + 1, " + &
							" instr(element_value, ':')) - 1) as " 
						insert_sqls = insert_sqls + element 
						insert_sqls = insert_sqls + "	from cr_structure_values2  " + &
							" where structure_id = " + string(structure_id) + " and detail_budget = 1 and status = 1 " + &
							" start with structure_id = " + string(structure_id) + " and element_value in ( select " + & 
						parsed_point + &
						" from cr_validation_control where rule_id = " + string(rule_id) + " and id = " + &
							string(id) + ") connect by prior element_value = parent_value and " + &
							" structure_id = " + string(structure_id) + " "  + &
						" union "	
					next
					
					//	Trim off the last union and close the parens.  The
					//		number of parens depends on the number of structure
					//		points in the element field.
					insert_sqls = left(insert_sqls,len(insert_sqls) - 6)
					for gg = 1 to g - 1
						insert_sqls = insert_sqls + ') '
					next
					insert_sqls = insert_sqls + element_alias_array[xx] +  ','
					
				else
					
					//	Single structure point being used.
				
					insert_sqls = insert_sqls + " (select structure_id, substr(element_value, 1" + &
						", decode(instr(element_value, ':'), 0, length(element_value) + 1, " + &
						" instr(element_value, ':')) - 1) as " 
					insert_sqls = insert_sqls + element 
					insert_sqls = insert_sqls + "	from cr_structure_values2  " + &
						" where structure_id = " + string(structure_id) + " and detail_budget = 1 and status = 1 " + &
						" start with structure_id = " + string(structure_id) + " and element_value in ( " 
					insert_sqls = insert_sqls + ' select replace(replace(' + element + ",'{',''),'}','') "
					insert_sqls = insert_sqls + " from cr_validation_control where rule_id = " + string(rule_id) + " and id = " + &
						string(id) + ") connect by prior element_value = parent_value and " + &
						" structure_id = " + string(structure_id) + ") " + element_alias_array[xx] + ","
						
				end if
				
			else
				
				//	DML: 102505: Modifications to allow for multiple structure points.
				
				//	Parse out the structure point from the brackets
				point = uf_parse_mult_structure(element,rule_id,id)
				//	The array must be reset each time through the loop
				point_array = reset_array
				f_parsestringintostringarray(point,',',point_array[])
				
				//	If the upperbound > 1 then I know more then one point was used.
				if upperbound(point_array) > 1 then
				
					for g = 1 to upperbound(point_array)
						parsed_point = point_array[g]
						
						insert_sqls = insert_sqls + " (select structure_id, substr(element_value, 1" + &
							", decode(instr(element_value, ':'), 0, length(element_value) + 1, " + &
							" instr(element_value, ':')) - 1) as " 
						insert_sqls = insert_sqls + element 
						insert_sqls = insert_sqls + "	from cr_structure_values  " + &
							" where structure_id = " + string(structure_id) + " and detail_budget = 1 and status = 1 " + &
							" start with structure_id = " + string(structure_id) + " and upper(element_value) in ( select " + & 
						parsed_point + &
						" from cr_validation_control where rule_id = " + string(rule_id) + " and id = " + &
							string(id) + ") connect by prior value_id = rollup_value_id and " + &
							" structure_id = " + string(structure_id) + " "  + &
						" union "	
					next
						
						//	Trim off the last union and close the parens.  The
						//		number of parens depends on the number of structure
						//		points in the element field.
						insert_sqls = left(insert_sqls,len(insert_sqls) - 6)
						for gg = 1 to g - 1
							insert_sqls = insert_sqls + ') '
						next
						insert_sqls = insert_sqls + element_alias_array[xx] +  ','
						
					else
						
						//	Single structure point being used.
						
						insert_sqls = insert_sqls + " (select structure_id, substr(element_value, 1" + &
							", decode(instr(element_value, ':'), 0, length(element_value) + 1, " + &
							" instr(element_value, ':')) - 1) as " 
						insert_sqls = insert_sqls + element 
						insert_sqls = insert_sqls + "	from cr_structure_values  " + &
							" where structure_id = " + string(structure_id) + " and detail_budget = 1 and status = 1 " + &
							" start with structure_id = " + string(structure_id) + " and upper(element_value) in ( " 
						insert_sqls = insert_sqls + ' select upper(substr(replace(' + element 
						insert_sqls = insert_sqls + ",'{','' ),1,instr(" 
						insert_sqls = insert_sqls + element 
						insert_sqls = insert_sqls + ",',')-2)) from cr_validation_control where rule_id = " + string(rule_id) + " and id = " + &
							string(id) + ") connect by prior value_id = rollup_value_id and " + &
							" structure_id = " + string(structure_id) + ") " + element_alias_array[xx] + ","
							
					end if
					
			end if
			
		next  //  for xx = 1 to upperbound(element_array) ...
		
		//	Trim the last comma
		insert_sqls = left(insert_sqls,len(insert_sqls) - 1) 
		
		//	Build the where clause
		insert_sqls = insert_sqls + ' where a.' 
		
		for ii = 1 to upperbound(structure_array)
			i_structure_column = structure_array[ii]
			insert_sqls = insert_sqls + i_structure_column + ' = ' + &
				alias_array[ii] + '.structure_id (+) and a.'
		next
		
		insert_sqls = insert_sqls + 'rule_id = ' + string(rule_id) + &
		 	' and a.id = ' + string(id) + ') aa'
		
		//  DMJ:
		insert_sqls = insert_sqls + aa_where_sqls
		
		execute immediate :insert_sqls;
		if sqlca.sqlcode < 0 then
			f_pp_msgs(" ")
			f_pp_msgs('ERROR: Inserting into cr_validation_combos for Section 3 : ' + sqlca.sqlerrtext)
			f_pp_msgs(insert_sqls)
			f_pp_msgs(" ")
			rollback;
			overall_rtn = -1
			goto clear_user
		else
			commit;
		end if	
			
	next	//id = 1 to num_ids
next	//for rule_id = 1 to num_rules


skip_structure_code:


commit;

f_pp_msgs("Analyzing table CR_VALIDATION_COMBOS at " + string(now()))
setpointer(hourglass!)

sqlca.analyze_table('cr_validation_combos');

if sqlca.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("The anaylze function returned the following error: " + sqlca.sqlerrtext)
	f_pp_msgs("Processing WILL continue.")
	f_pp_msgs(" ")
	//  No return ...
end if

clear_user:

//	Set the running_session_id = 0 and executable_file = null so another
//		user can create combos.
update pp_processes
set running_session_id = 0,
	executable_file = null
where lower(description) = 'create validation combos';

if sqlca.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Updating pp_processes.running_session_id: " + sqlca.sqlerrtext)
	f_pp_msgs(" ")
	rollback;
	return -1
else
	commit;
end if

return overall_rtn
end function

public function string uf_parse_mult_structure (string a_element_to_parse, longlong a_rule_id, longlong a_id);//*****************************************************************************************************************
//
//  Function:     uf_parse_mult_structure
//
//  Description:  If more then one structure is used in an element then
//						the points will need to have the brackets parsed out.
//
//*****************************************************************************************************************

string parsed_element,parsed_array[], structure_point, sqls, element_to_parse, &
	reset_array[]
longlong g, gg, num_rows

sqls = "select " + a_element_to_parse + " from cr_validation_control " + &
		" where rule_id = " + string(a_rule_id) + " and id = " + string(a_id)

i_ds_parse_element.setsqlselect(sqls)
i_ds_parse_element.retrieve()

element_to_parse = i_ds_parse_element.getitemstring(1,1)
if isnull(element_to_parse) or element_to_parse = "" then
	parsed_element = a_element_to_parse
	return parsed_element
end if

//	If multi-select is used in the structure window then an extra set
//		of brackets is added around all the points.  Remove those so the 
//		only brackets used are the ones around each individual point.

//select replace(:element_to_parse,'{{','{') into :element_to_parse from dual;
//select replace(:element_to_parse,'}}','}') into :element_to_parse from dual;
element_to_parse = f_replace_string(element_to_parse,'{{','{','all')
element_to_parse = f_replace_string(element_to_parse,'}}','}','all')

f_parsestringintostringarray(element_to_parse,'{',parsed_array[])

//	A '{' will always be first.  Not a real value so skip it.
for g = 2 to upperbound(parsed_array)
	
	structure_point = parsed_array[g]
	
	if i_use_new_structures_table = "YES" then
		structure_point = f_replace_string(structure_point,'}','','all')
	else
		//	If using the old structure technique with the value_id then parse
		//		out the comma.
		select upper(substr(:structure_point,1,instr(:structure_point,',') -1))
		into :structure_point
		from dual;
	end if
	
	parsed_element = parsed_element + "'" + structure_point + "',"
	
next

parsed_element = mid(parsed_element,1,len(parsed_element) -1)

return parsed_element
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

