HA$PBExportHeader$w_custom_rpt_gen_dw.srw
forward
global type w_custom_rpt_gen_dw from window
end type
type dw_funding_just_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_detail_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_fund_wo_prepare_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_fund_wo_detail_workflow_close from datawindow within w_custom_rpt_gen_dw
end type
type dw_fund_wo_detail_workflow from datawindow within w_custom_rpt_gen_dw
end type
type dw_fund_wo_detail_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_twc_funding_just_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_fund_wo_detail_close_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_estimate_report from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_est_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_est_comments_task from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_detail_approvals from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_detail_workflow from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_print from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_print_report from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_unit_est_rev from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_wes_rev from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_wo_detail_rev from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_wo_detail_task from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_wo_rev from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_est_comments from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_wes from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_unit_est from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_wo_detail from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_selected_retire from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_as_built from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_overhead_excluded from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_forecast from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_overhead from datawindow within w_custom_rpt_gen_dw
end type
type dw_wo_auth_class_code from datawindow within w_custom_rpt_gen_dw
end type
type dw_fund_wo_detail_approvals from datawindow within w_custom_rpt_gen_dw
end type
end forward

global type w_custom_rpt_gen_dw from window
integer width = 3566
integer height = 1648
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_funding_just_print dw_funding_just_print
dw_wo_detail_print dw_wo_detail_print
dw_fund_wo_prepare_print dw_fund_wo_prepare_print
dw_fund_wo_detail_workflow_close dw_fund_wo_detail_workflow_close
dw_fund_wo_detail_workflow dw_fund_wo_detail_workflow
dw_fund_wo_detail_print dw_fund_wo_detail_print
dw_twc_funding_just_print dw_twc_funding_just_print
dw_fund_wo_detail_close_print dw_fund_wo_detail_close_print
dw_wo_estimate_report dw_wo_estimate_report
dw_wo_est_print dw_wo_est_print
dw_wo_est_comments_task dw_wo_est_comments_task
dw_wo_auth_detail_approvals dw_wo_auth_detail_approvals
dw_wo_auth_detail_workflow dw_wo_auth_detail_workflow
dw_wo_auth_print dw_wo_auth_print
dw_wo_auth_print_report dw_wo_auth_print_report
dw_wo_auth_unit_est_rev dw_wo_auth_unit_est_rev
dw_wo_auth_wes_rev dw_wo_auth_wes_rev
dw_wo_auth_wo_detail_rev dw_wo_auth_wo_detail_rev
dw_wo_auth_wo_detail_task dw_wo_auth_wo_detail_task
dw_wo_auth_wo_rev dw_wo_auth_wo_rev
dw_wo_est_comments dw_wo_est_comments
dw_wo_auth_wes dw_wo_auth_wes
dw_wo_auth_unit_est dw_wo_auth_unit_est
dw_wo_auth_wo_detail dw_wo_auth_wo_detail
dw_wo_auth_selected_retire dw_wo_auth_selected_retire
dw_wo_auth_as_built dw_wo_auth_as_built
dw_wo_auth_overhead_excluded dw_wo_auth_overhead_excluded
dw_wo_auth_forecast dw_wo_auth_forecast
dw_wo_auth_overhead dw_wo_auth_overhead
dw_wo_auth_class_code dw_wo_auth_class_code
dw_fund_wo_detail_approvals dw_fund_wo_detail_approvals
end type
global w_custom_rpt_gen_dw w_custom_rpt_gen_dw

on w_custom_rpt_gen_dw.create
this.dw_funding_just_print=create dw_funding_just_print
this.dw_wo_detail_print=create dw_wo_detail_print
this.dw_fund_wo_prepare_print=create dw_fund_wo_prepare_print
this.dw_fund_wo_detail_workflow_close=create dw_fund_wo_detail_workflow_close
this.dw_fund_wo_detail_workflow=create dw_fund_wo_detail_workflow
this.dw_fund_wo_detail_print=create dw_fund_wo_detail_print
this.dw_twc_funding_just_print=create dw_twc_funding_just_print
this.dw_fund_wo_detail_close_print=create dw_fund_wo_detail_close_print
this.dw_wo_estimate_report=create dw_wo_estimate_report
this.dw_wo_est_print=create dw_wo_est_print
this.dw_wo_est_comments_task=create dw_wo_est_comments_task
this.dw_wo_auth_detail_approvals=create dw_wo_auth_detail_approvals
this.dw_wo_auth_detail_workflow=create dw_wo_auth_detail_workflow
this.dw_wo_auth_print=create dw_wo_auth_print
this.dw_wo_auth_print_report=create dw_wo_auth_print_report
this.dw_wo_auth_unit_est_rev=create dw_wo_auth_unit_est_rev
this.dw_wo_auth_wes_rev=create dw_wo_auth_wes_rev
this.dw_wo_auth_wo_detail_rev=create dw_wo_auth_wo_detail_rev
this.dw_wo_auth_wo_detail_task=create dw_wo_auth_wo_detail_task
this.dw_wo_auth_wo_rev=create dw_wo_auth_wo_rev
this.dw_wo_est_comments=create dw_wo_est_comments
this.dw_wo_auth_wes=create dw_wo_auth_wes
this.dw_wo_auth_unit_est=create dw_wo_auth_unit_est
this.dw_wo_auth_wo_detail=create dw_wo_auth_wo_detail
this.dw_wo_auth_selected_retire=create dw_wo_auth_selected_retire
this.dw_wo_auth_as_built=create dw_wo_auth_as_built
this.dw_wo_auth_overhead_excluded=create dw_wo_auth_overhead_excluded
this.dw_wo_auth_forecast=create dw_wo_auth_forecast
this.dw_wo_auth_overhead=create dw_wo_auth_overhead
this.dw_wo_auth_class_code=create dw_wo_auth_class_code
this.dw_fund_wo_detail_approvals=create dw_fund_wo_detail_approvals
this.Control[]={this.dw_funding_just_print,&
this.dw_wo_detail_print,&
this.dw_fund_wo_prepare_print,&
this.dw_fund_wo_detail_workflow_close,&
this.dw_fund_wo_detail_workflow,&
this.dw_fund_wo_detail_print,&
this.dw_twc_funding_just_print,&
this.dw_fund_wo_detail_close_print,&
this.dw_wo_estimate_report,&
this.dw_wo_est_print,&
this.dw_wo_est_comments_task,&
this.dw_wo_auth_detail_approvals,&
this.dw_wo_auth_detail_workflow,&
this.dw_wo_auth_print,&
this.dw_wo_auth_print_report,&
this.dw_wo_auth_unit_est_rev,&
this.dw_wo_auth_wes_rev,&
this.dw_wo_auth_wo_detail_rev,&
this.dw_wo_auth_wo_detail_task,&
this.dw_wo_auth_wo_rev,&
this.dw_wo_est_comments,&
this.dw_wo_auth_wes,&
this.dw_wo_auth_unit_est,&
this.dw_wo_auth_wo_detail,&
this.dw_wo_auth_selected_retire,&
this.dw_wo_auth_as_built,&
this.dw_wo_auth_overhead_excluded,&
this.dw_wo_auth_forecast,&
this.dw_wo_auth_overhead,&
this.dw_wo_auth_class_code,&
this.dw_fund_wo_detail_approvals}
end on

on w_custom_rpt_gen_dw.destroy
destroy(this.dw_funding_just_print)
destroy(this.dw_wo_detail_print)
destroy(this.dw_fund_wo_prepare_print)
destroy(this.dw_fund_wo_detail_workflow_close)
destroy(this.dw_fund_wo_detail_workflow)
destroy(this.dw_fund_wo_detail_print)
destroy(this.dw_twc_funding_just_print)
destroy(this.dw_fund_wo_detail_close_print)
destroy(this.dw_wo_estimate_report)
destroy(this.dw_wo_est_print)
destroy(this.dw_wo_est_comments_task)
destroy(this.dw_wo_auth_detail_approvals)
destroy(this.dw_wo_auth_detail_workflow)
destroy(this.dw_wo_auth_print)
destroy(this.dw_wo_auth_print_report)
destroy(this.dw_wo_auth_unit_est_rev)
destroy(this.dw_wo_auth_wes_rev)
destroy(this.dw_wo_auth_wo_detail_rev)
destroy(this.dw_wo_auth_wo_detail_task)
destroy(this.dw_wo_auth_wo_rev)
destroy(this.dw_wo_est_comments)
destroy(this.dw_wo_auth_wes)
destroy(this.dw_wo_auth_unit_est)
destroy(this.dw_wo_auth_wo_detail)
destroy(this.dw_wo_auth_selected_retire)
destroy(this.dw_wo_auth_as_built)
destroy(this.dw_wo_auth_overhead_excluded)
destroy(this.dw_wo_auth_forecast)
destroy(this.dw_wo_auth_overhead)
destroy(this.dw_wo_auth_class_code)
destroy(this.dw_fund_wo_detail_approvals)
end on

type dw_funding_just_print from datawindow within w_custom_rpt_gen_dw
integer x = 1856
integer y = 1284
integer width = 864
integer height = 112
integer taborder = 70
boolean titlebar = true
string title = "dw_funding_just_print"
string dataobject = "dw_funding_just_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_detail_print from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 1148
integer width = 864
integer height = 112
integer taborder = 70
boolean titlebar = true
string title = "dw_wo_detail_print"
string dataobject = "dw_wo_detail_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_fund_wo_prepare_print from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 1016
integer width = 864
integer height = 112
integer taborder = 60
boolean titlebar = true
string title = "dw_fund_wo_prepare_print"
string dataobject = "dw_fund_wo_prepare_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_fund_wo_detail_workflow_close from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 892
integer width = 864
integer height = 112
integer taborder = 60
boolean titlebar = true
string title = "dw_fund_wo_detail_workflow_close"
string dataobject = "dw_fund_wo_detail_workflow_close"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_fund_wo_detail_workflow from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 768
integer width = 864
integer height = 112
integer taborder = 50
boolean titlebar = true
string title = "dw_fund_wo_detail_workflow"
string dataobject = "dw_fund_wo_detail_workflow"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_fund_wo_detail_print from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 640
integer width = 864
integer height = 112
integer taborder = 50
boolean titlebar = true
string title = "dw_fund_wo_detail_print"
string dataobject = "dw_fund_wo_detail_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_twc_funding_just_print from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 512
integer width = 864
integer height = 112
integer taborder = 40
boolean titlebar = true
string title = "dw_twc_funding_just_print"
string dataobject = "dw_twc_funding_just_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_fund_wo_detail_close_print from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 388
integer width = 864
integer height = 112
integer taborder = 40
boolean titlebar = true
string title = "dw_fund_wo_detail_close_print"
string dataobject = "dw_fund_wo_detail_close_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_estimate_report from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 260
integer width = 864
integer height = 112
integer taborder = 30
boolean titlebar = true
string title = "dw_wo_estimate_report"
string dataobject = "dw_wo_estimate_report"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_est_print from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 136
integer width = 864
integer height = 112
integer taborder = 30
boolean titlebar = true
string title = "dw_wo_est_print"
string dataobject = "dw_wo_est_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_est_comments_task from datawindow within w_custom_rpt_gen_dw
integer x = 1861
integer y = 16
integer width = 864
integer height = 112
integer taborder = 20
boolean titlebar = true
string title = "dw_wo_est_comments_task"
string dataobject = "dw_wo_est_comments_task"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_detail_approvals from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 16
integer width = 864
integer height = 112
integer taborder = 10
boolean titlebar = true
string title = "dw_wo_auth_detail_approvals"
string dataobject = "dw_wo_auth_detail_approvals"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_detail_workflow from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 136
integer width = 864
integer height = 112
integer taborder = 20
boolean titlebar = true
string title = "dw_wo_auth_detail_workflow"
string dataobject = "dw_wo_auth_detail_workflow"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_print from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 260
integer width = 864
integer height = 112
integer taborder = 20
boolean titlebar = true
string title = "dw_wo_auth_print"
string dataobject = "dw_wo_auth_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_print_report from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 388
integer width = 864
integer height = 112
integer taborder = 30
boolean titlebar = true
string title = "dw_wo_auth_print_report"
string dataobject = "dw_wo_auth_print_report"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_unit_est_rev from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 512
integer width = 864
integer height = 112
integer taborder = 30
boolean titlebar = true
string title = "dw_wo_auth_unit_est_rev"
string dataobject = "dw_wo_auth_unit_est_rev"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_wes_rev from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 640
integer width = 864
integer height = 112
integer taborder = 40
boolean titlebar = true
string title = "dw_wo_auth_wes_rev"
string dataobject = "dw_wo_auth_wes_rev"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_wo_detail_rev from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 768
integer width = 864
integer height = 112
integer taborder = 40
boolean titlebar = true
string title = "dw_wo_auth_wo_detail_rev"
string dataobject = "dw_wo_auth_wo_detail_rev"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_wo_detail_task from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 892
integer width = 864
integer height = 112
integer taborder = 50
boolean titlebar = true
string title = "dw_wo_auth_wo_detail_task"
string dataobject = "dw_wo_auth_wo_detail_task"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_wo_rev from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 1016
integer width = 864
integer height = 112
integer taborder = 50
boolean titlebar = true
string title = "dw_wo_auth_wo_rev"
string dataobject = "dw_wo_auth_wo_rev"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_est_comments from datawindow within w_custom_rpt_gen_dw
integer x = 955
integer y = 1144
integer width = 864
integer height = 112
integer taborder = 60
boolean titlebar = true
string title = "dw_wo_est_comments"
string dataobject = "dw_wo_est_comments"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_wes from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 1144
integer width = 864
integer height = 112
integer taborder = 50
boolean titlebar = true
string title = "dw_wo_auth_wes"
string dataobject = "dw_wo_auth_wes"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_unit_est from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 1016
integer width = 864
integer height = 112
integer taborder = 50
boolean titlebar = true
string title = "dw_wo_auth_unit_est"
string dataobject = "dw_wo_auth_unit_est"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_wo_detail from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 892
integer width = 864
integer height = 112
integer taborder = 40
boolean titlebar = true
string title = "dw_wo_auth_wo_detail"
string dataobject = "dw_wo_auth_wo_detail"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_selected_retire from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 768
integer width = 864
integer height = 112
integer taborder = 40
boolean titlebar = true
string title = "dw_wo_auth_selected_retire"
string dataobject = "dw_wo_auth_selected_retire"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_as_built from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 640
integer width = 864
integer height = 112
integer taborder = 30
boolean titlebar = true
string title = "dw_wo_auth_as_built"
string dataobject = "dw_wo_auth_as_built"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_overhead_excluded from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 512
integer width = 864
integer height = 112
integer taborder = 30
boolean titlebar = true
string title = "dw_wo_auth_overhead_excluded"
string dataobject = "dw_wo_auth_overhead_excluded"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_forecast from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 388
integer width = 864
integer height = 112
integer taborder = 20
boolean titlebar = true
string title = "dw_wo_auth_forecast"
string dataobject = "dw_wo_auth_forecast"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_overhead from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 260
integer width = 864
integer height = 112
integer taborder = 20
boolean titlebar = true
string title = "dw_wo_auth_overhead"
string dataobject = "dw_wo_auth_overhead"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_auth_class_code from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 136
integer width = 864
integer height = 112
integer taborder = 10
boolean titlebar = true
string title = "dw_wo_auth_class_code"
string dataobject = "dw_wo_auth_class_code"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_fund_wo_detail_approvals from datawindow within w_custom_rpt_gen_dw
integer x = 37
integer y = 16
integer width = 864
integer height = 112
integer taborder = 10
boolean titlebar = true
string title = "dw_fund_wo_detail_approvals"
string dataobject = "dw_fund_wo_detail_approvals"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

