HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_report_gen.exe'
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure
string dw_name, wo_number, file_name, file_path, ext
longlong long_arg[], empty_long[], ret, co_id, fnum, bytes
uo_ds_top dw_attached
uo_ps_interface_approvals uo_ps_appr
blob	docblob
boolean rtn_bln

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// set variables from the ssp parms
// string 1 = datawindow name.  This must be passed to the ssp.
dw_name = g_ssp_parms.string_arg[1]
f_pp_msgs("datawindow name : "+dw_name)
if isnull( dw_name ) or dw_name = "" then
	f_pp_msgs("Error.  No datawindow was passed into the report generation ssp, so no report can be generated.  Exiting...")
	return rtn_failure
end if

long_arg = empty_long

// work order id.
long_arg[1] = g_ssp_parms.long_arg[1]
f_pp_msgs("datawindow argument 1 : "+string(long_arg[1]))

// revision
if not isnull( g_ssp_parms.long_arg2[1] ) then 
	long_arg[2] = g_ssp_parms.long_arg2[1]
	f_pp_msgs("datawindow argument 2 : "+string(long_arg[2]))
end if

// grab the work order number and company id from work order control.
select work_order_number, company_id
into :wo_number, :co_id
from work_order_control
where work_order_id = :long_arg[1];

f_pp_msgs("work order number associated with work order id "+string(long_arg[1])+" : "+wo_number)
f_pp_msgs("company id associated with work order id "+string(long_arg[1])+" : "+string(co_id))

// set up and retrieve the datawindow
f_pp_msgs("setting up and retrieving datawindow.")
dw_attached = create uo_ds_top
dw_attached.dataobject = dw_name
dw_attached.settransobject(sqlca)

f_pp_msgs(string( upperBound( long_arg[] ) )+" arguments passed to ssp.")
choose case upperBound( long_arg[] )
	case	0
		ret = dw_attached.retrieve()
	case 1
		ret = dw_attached.retrieve( long_arg[1] )
	case 2
		ret = dw_attached.retrieve( long_arg[1], long_arg[2] )
end choose
		
if ret = -1 then
	f_pp_msgs("Error retrieving datawindow '"+dw_name+"'. Check approval notification "+&
				"for the row for this notification type and make sure that the DW retrieves with the "+&
				"arguments passed to this SSP as displayed in the online logs.  Exiting...")
	destroy dw_attached
	return rtn_failure
end if
f_pp_msgs("Successfully retrieved datawindow.  Return code : "+string(ret))

// set the file name & empty the file path
file_name = wo_number+'-'+string(long_arg[2])
file_path = ""
f_pp_msgs("File name set to : "+file_name)

// save the dw results to a file.
f_pp_msgs("Printing to file (start)")
uo_ps_appr = create uo_ps_interface_approvals
uo_ps_appr.uf_print_file(dw_attached, co_id, file_name, file_path)
f_pp_msgs("Successfully printed to file : "+file_path)

// saving the file extension
if left( right( file_path, 5 ), 1 ) = "." then
	ext = right( file_path, 4 )
else 
	ext = right( file_path, 3 )
end if
f_pp_msgs("saving extension : "+ext)


//	Read the report into a blob.
f_pp_msgs("Opening file.")
fnum = fileOpen( file_path, StreamMode!, Read! )

if fnum = -1 then
	f_pp_msgs("Error occurred while opening the report for reading.")
	destroy dw_attached
	destroy uo_ps_appr
	return rtn_failure
end if

f_pp_msgs("Reading file.")
bytes = fileReadEx( fnum, docblob )
if bytes <= 0 then
	f_pp_msgs("Error occurred while reading the report.")
	destroy dw_attached
	destroy uo_ps_appr
	return rtn_failure
end if

f_pp_msgs("Closing File.")
fileClose( fnum )

// destroy the objects no longer needed
f_pp_msgs("Destroying dw and interface objects.")
destroy dw_attached
destroy uo_ps_appr

// clear out previous data in ma_report_results
f_pp_msgs("Clearing out previously existing report data.")
delete from ma_report_results
where work_order_id = :long_arg[1]
and revision = :long_arg[2];
if sqlca.SQLCode <> 0 then
	f_pp_msgs("Error occurred while clearing out previously existing report data.  Error: " + sqlca.SQLErrText )
	return rtn_failure
end if

// insert header row into ma_report_results
f_pp_msgs("Inserting the header for the report data.")
insert into ma_report_results
(work_order_id, revision, bytes, file_path, file_ext)
values
(:long_arg[1], :long_arg[2], :bytes, :file_path, :ext);
if sqlca.SQLCode <> 0 then
	f_pp_msgs("Error occurred while inserting the header for the report data.  Error: " + sqlca.SQLErrText )
	return rtn_failure
end if

// update with report data
f_pp_msgs("Updating with blob report data.")
updateblob	ma_report_results
set			report_data = :docblob
where work_order_id = :long_arg[1]
and revision = :long_arg[2];
if sqlca.SQLCode <> 0 then
	f_pp_msgs("Error occurred while saving the report data to the database.  Error: " + sqlca.SQLErrText )
	return rtn_failure
end if

f_pp_msgs("Successfully saved report to the database.")

// attempt to delete the newly created file from the server.  but don't check for errors or keep this last piece from the process finishing (ssp may not have delete rights).
rtn_bln = fileDelete( file_path )
if rtn_bln then
	f_pp_msgs("Successfully deleted the file created by this process.")
else
	f_pp_msgs("Unable to delete the file created by this process.  Continuing without error.")
end if
	

return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_report_gen_custom_version nvo_ssp_report_gen_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version
nvo_ppprojct_custom_version nvo_ppprojct_custom_version

choose case a_pbd_name
	case 'ssp_report_gen_custom.pbd'
		return nvo_ssp_report_gen_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version	
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version	
end choose

return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

