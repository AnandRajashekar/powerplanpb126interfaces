HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_close_powerplant.exe'


nvo_cpr_control i_nvo_cpr_control
end variables

forward prototypes
public function longlong uf_read ()
public function boolean uf_close_powerplant_main ()
public function integer uf_verify_aro ()
public function boolean uf_allocate_rwip ()
public function boolean uf_validate_cpr_control (integer a_index)
public function boolean uf_build_depr_vintage_summary ()
public function boolean uf_build_res_allo_factors ()
public function boolean uf_build_depr_net_salvage_amort ()
public function boolean uf_roll_allo_factors_forward ()
public subroutine uf_check_factor_count ()
public subroutine uf_archive_gl_transaction ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure, rtn
string process_msg
w_datawindows w

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

// Call Close PowerPlant logic
if not uf_close_powerplant_main() then
	i_nvo_cpr_control.of_log_failed_companies("CLOSE POWERPLANT")
	rtn = rtn_failure
elseif upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
	i_nvo_cpr_control.of_log_failed_companies("CLOSE POWERPLANT")
	rtn = rtn_failure
else
	rtn = rtn_success
end if

// Release the concurrency lock
i_nvo_cpr_control.of_releaseProcess(process_msg)
f_pp_msgs("Release Process Status: " + process_msg)

return rtn
end function

public function boolean uf_close_powerplant_main ();/************************************************************************************************************************************************************
**
**	uf_close_powerplant_main()
**	
**	This function corresponds to w_cpr_control.wf_check_system_control(a_button_number).  This determines whether emails should be sent based on the
**	selected companies.
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
longlong ret, rtn, process_id, pp_stat_id
longlong total_company, cc, rc, i, empty_array[], check, rows, curs, tablepos, subl, num_months, num_failed_companies
decimal {8} factor
string company_string, month_string, open_month, null_string, msg, allo_rwip
string original_sql, sqls, subledger, template, replacestring, modret, process_depr, comp_descr, num_months_str
string	find_str, user_id
boolean	vfy_users
uo_ds_top ds_users
datetime open_month_date

string args[]

i_nvo_cpr_control.of_constructor()

// Set instance variables based on the parameters passed in g_ssp_parms
i_nvo_cpr_control.i_company_idx = g_ssp_parms.long_arg
i_nvo_cpr_control.i_original_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_original_col_num = g_ssp_parms.long_arg2[1]
i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return false
end if

// Get the descriptions
rtn = i_nvo_cpr_control.of_getDescriptionsFromIds(i_nvo_cpr_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_cpr_control.of_selectedCompanies(i_nvo_cpr_control.i_company_idx) = -1) then
	g_msg.messagebox("", "Cannot process multiple companies because the open/closed months do " + &
				"not line up")
	return false
end if

SetNull(null_string)

total_company = upperbound(i_nvo_cpr_control.i_company_idx)

// Verify ARO's have been calc'ed and approved if necessary
rtn = uf_verify_aro()
if rtn <> 1 then return false

for cc = 1 to total_company
	
	rc = i_nvo_cpr_control.of_setupCompany(cc)
	
	if rc < 1 then continue // next company
	
   // Validate cpr control fields
	if not uf_validate_cpr_control(cc) then 
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		continue
	end if

   comp_descr = i_nvo_cpr_control.i_company_descr[cc]
	
   f_pp_msgs("CPR PowerPlant Close started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
	
	pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "Close PowerPlant: " + comp_descr)


   process_depr = f_pp_system_control_company( 'post depr', -1)
	if process_depr = 'yes' then 
		// allocate RWIP to depr_ledger
		if not uf_allocate_rwip() then 
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
			continue
		end if
		
		// Build Depr Vintage Summary
		if not uf_build_depr_vintage_summary() then 
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
			return false
		end if
	
		month_string =  String(i_nvo_cpr_control.i_month, "MM/YYYY")
		
		// Build Res Allo Factors
		if not uf_build_res_allo_factors() then 
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
			continue
		end if
			
		// Build depr net salvage amort	
		if not uf_build_depr_net_salvage_amort() then 
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
			continue
		end if
		
		// roll allo factors to open months
		if not uf_roll_allo_factors_forward() then 
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
			continue
		end if
		
		// MAKE SURE the factors got made
		uf_check_factor_count()
	end if
	 
   // backup the gl_transaction table
   uf_archive_gl_transaction()
	
	// call customizable function
	f_pp_msgs("Calling Customized function to close the month ..."+ ' ' + string(now()))
 	
	rtn = f_cpr_control_close_month(i_nvo_cpr_control.i_company,  i_nvo_cpr_control.i_month) 
	 
	if rtn <> 1 then
		f_pp_msgs("ERROR: in f_cpr_control_close_month: " + sqlca.SQLErrText)
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		return false
	end if
	
	// Call Dynamic Validation option
	args[1] = string(i_nvo_cpr_control.i_month)
	args[2] = string(i_nvo_cpr_control.i_company)
	
	ret = f_wo_validation_control(1008,args)
	
	if ret < 0 then
		rollback;
		f_pp_msgs("Calling Custom Dynamic Validations to close the month Failed.")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		return false
	end if
			
	i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'powerplant_closed',today())

	f_pp_msgs("PowerPlant is closed."+ ' ' + string(now()))
	
	i_nvo_cpr_control.of_updateDW()
	
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)

next


f_pp_msgs("Process ended on " + String(Today(), "mm/dd/yyyy") + " at " + String(Today(), "hh:mm:ss"))

// Email users
i_nvo_cpr_control.of_cleanup(8, 'email cpr close: close powerplant', 'Close PowerPlant')

return true
end function

public function integer uf_verify_aro ();/************************************************************************************************************************************************************
**
**	uf_verify_aro()
**	
**	This function corresponds to w_cpr_control.wf_verify_aro().  This function verifies that AROs have been calculated and approved for the
**	selected companies
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
////
////	Make sure that ARO's have been calc'ed and approved for all selected companies that have ARO's
////

longlong check, rtn, i, rowcount
string sqls, rtn_str
uo_ds_top ds_companies

sqls = "select company_id ~r~n"
sqls += " from cpr_control ~r~n"
sqls += " where company_id in (" + i_nvo_cpr_control.of_getCompanyList() + ") ~r~n"
sqls += " and to_char(accounting_month, 'yyyymm') = '" + string(i_nvo_cpr_control.i_month, 'yyyymm') + "' ~r~n"
sqls += " and (aro_calculated is null or aro_approved is null) ~r~n"
sqls += " and exists ( ~r~n"
sqls += " 	select 1 from aro ~r~n"
sqls += " 	where company_id = cpr_control.company_id ~r~n"
sqls += " 	and aro_status_id in (3,6) ~r~n"
sqls += " ) ~r~n"
sqls += " group by company_id ~r~n"

ds_companies = create uo_ds_top
rtn_str = f_create_dynamic_ds(ds_companies, "grid", sqls, sqlca, true)
if rtn_str <> "OK" then
	f_pp_msgs("ERROR: Creating ds_companies in uf_verify_aro: " + rtn_str)
	destroy ds_companies
	return -1
end if

rowcount = ds_companies.rowCount()

//If nothing shows up, then we are free to close
if rowcount = 0 then
	destroy ds_companies
	return 1
end if

//If something does show up, then print out error messages
for i = 1 to ds_companies.rowCount()
	f_pp_msgs("Error: ARO's must be approved and calculated " + &
						"for company " + string(ds_companies.GetItemNumber(i,1)) + " before PowerPlant can be closed." )
next

destroy ds_companies
return -1
end function

public function boolean uf_allocate_rwip ();/************************************************************************************************************************************************************
**
**	uf_allocate_rwip()
**	
**	Calls f_rwip_allo_load to allocate RWIP to the Depr Ledger
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
string allo_rwip, args[]
longlong ret

allo_rwip = f_pp_system_control_company("Allocate RWIP to Depr Ledger", i_nvo_cpr_control.i_company)

if Upper(Trim(allo_rwip)) = "PLANT" or Upper(Trim(allo_rwip)) = "RESERVE" then
	f_pp_msgs("Allocating RWIP to Depr Ledger..." + ' ' + string(now()))
	
	allo_rwip = f_rwip_allo_load(Long(String(i_nvo_cpr_control.i_month,'yyyymm')), i_nvo_cpr_control.i_company)
	
	if allo_rwip <> "" then
		rollback;
		f_pp_msgs("Error Allocating RWIP to Depr Ledger! (" + allo_rwip + ")")
		return false
	end if
	
	// Call Dynamic Validation option
	args[1] = string(i_nvo_cpr_control.i_month,'yyyymm')
	args[2] = string(i_nvo_cpr_control.i_company)
	
	ret = f_wo_validation_control(1008,args)
	
	if ret < 0 then
		rollback;
		f_pp_msgs("Error (Dynamic Validations) Allocating RWIP to Depr Ledger!")
		return false
	end if
end if

return true
end function

public function boolean uf_validate_cpr_control (integer a_index);/************************************************************************************************************************************************************
**
**	uf_validate_cpr_control()
**	
**	This function verifies that the appropriate fields on cpr_control are populated prior to Closing PowerPlant
**	
**	Parameters	:	longlong	:	(a_index) Corresponds to the current index in the company loop
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
longlong ret

if isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'cpr_closed')) then

	f_pp_msgs(" Cannot close PowerPlant without closing the CPR for the month ")
	return false

end if //cpr_closed

if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'powerplant_closed')) then
	
	f_pp_msgs("This month is closed.")
	return false

end if


boolean check_month_end_option

//make sure boolean_arg is populated
if upperbound(g_ssp_parms.boolean_arg) < a_index then 
	check_month_end_option = true
elseif isnull(g_ssp_parms.boolean_arg[a_index]) then //having this as a separate ELSEIF is important. If we used an OR in the IF clause, an array out of bounds exception occurs.
	check_month_end_option = true
else 
	check_month_end_option = false
end if

if check_month_end_option = true then 
	f_pp_msgs("Checking month end option for 'Continue if GL Reconciliation has not been completed?'")	
	try
		if lower(f_pp_month_end_options(i_nvo_cpr_control.i_company, g_process_id, 1)) = 'yes' then  //option_id 1 = 'Continue if GL Reconciliation has not been completed?'
			f_pp_msgs("'Continue if GL Reconciliation has not been completed?' set to Yes - this interface will proceed even if GL Reconciliation has not been completed.")	
			g_ssp_parms.boolean_arg[a_index] = false
		else 
			f_pp_msgs("'Continue if GL Reconciliation has not been completed?' set to No - this interface will NOT proceed if GL Reconciliation has not been completed.")	
			g_ssp_parms.boolean_arg[a_index] = true
		end if
	catch (exception e)
		f_pp_msgs("Could not find value for 'Continue if GL Reconciliation has not been completed?' month end option. Defaulting to 'No'.")
		g_ssp_parms.boolean_arg[a_index] = true	
	end try
end if 

if isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'gl_reconciled')) and g_ssp_parms.boolean_arg[a_index] = true then //boolean_arg tells is if we should check 
	f_status_box("ERROR","PowerPlant and the GL have not been reconciled for the month.")
	return false
end if //gl_recon

return true
end function

public function boolean uf_build_depr_vintage_summary ();/************************************************************************************************************************************************************
**
**	uf_build_depr_vintage_summary()
**	
**	This function calls f_build_depr_vintage_summary.
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
string null_string
longlong ret

setnull(null_string)

f_pp_msgs("Building Depr Vintage Summary...")
	
f_pp_msgs("Building Depr Vintage Summary for company " + string(i_nvo_cpr_control.i_company) +  " at " + String(Today(), "hh:mm:ss"))

ret = f_build_depr_vintage_summary(i_nvo_cpr_control.i_month, i_nvo_cpr_control.i_company, null_string)
if ret <> 1 then
	f_pp_msgs("f_build_depr_vintage_summary() failed.")
	return false
end if

return true
end function

public function boolean uf_build_res_allo_factors ();/************************************************************************************************************************************************************
**
**	uf_build_res_allo_factors()
**	
**	This function builds res allo factors.
**	
**	Parameters	:	longlong	:	(a_button_number) Corresponds to the various buttons on w_cpr_control
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
string msg

uo_depr_processing uo_depr
uo_depr = CREATE uo_depr_processing

f_pp_msgs("Building Res Allo Factors for company " + string(i_nvo_cpr_control.i_company) +  " at " + String(Today(), "hh:mm:ss"))
	
msg = uo_depr.uf_allo_reserve_co(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month)

if msg = "next company" then return false

DESTROY uo_depr

return true
end function

public function boolean uf_build_depr_net_salvage_amort ();/************************************************************************************************************************************************************
**
**	uf_build_depr_net_salvage_amort()
**	
**	This function builds the depr_net_salvage_amort records for the month
**	
**	Parameters	:	(none)
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/

if Month(Date(i_nvo_cpr_control.i_month)) = 12 then
	// create new vintage in depr_net_salvage_amort
	// Maint 2615: include NOT EXISTS to avoid duplicates when using the monthly amort option
	INSERT INTO DEPR_NET_SALVAGE_AMORT
		(SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, GL_POST_MO_YR, 
		COST_OF_REMOVAL_BAL, SALVAGE_BAL)
	SELECT dl.SET_OF_BOOKS_ID, dl.DEPR_GROUP_ID, to_number(to_char(dl.gl_post_mo_yr,'yyyy')), :i_nvo_cpr_control.i_month,
		SUM(COST_OF_REMOVAL), SUM(SALVAGE_CASH + SALVAGE_RETURNS)
	FROM DEPR_LEDGER dl, DEPR_GROUP dg, DEPR_METHOD_RATES dmr
	WHERE dl.DEPR_GROUP_ID = dg.DEPR_GROUP_ID 
	AND dg.COMPANY_ID = :i_nvo_cpr_control.i_company 
	AND TO_CHAR(dl.GL_POST_MO_YR,'YYYY') = TO_CHAR(:i_nvo_cpr_control.i_month, 'YYYY')
	and nvl(dg.subledger_type_id,0) > -1
	and dmr.depr_method_id = dg.depr_Method_id
	and dmr.set_of_books_id = dl.set_of_books_id
	and (lower(dmr.salvage_treatment) <> 'no' or lower(dmr.cor_treatment) <> 'no') 
	and dmr.effective_date = 
		(select max(x.effective_date) from depr_method_Rates x
		where x.depr_method_id = dmr.depr_Method_id
		and x.set_of_books_id = dmr.set_of_books_id
		and x.effective_Date <= :i_nvo_cpr_control.i_month)
	AND NOT EXISTS (
		SELECT 1 FROM DEPR_NET_SALVAGE_AMORT d 
		WHERE d."SET_OF_BOOKS_ID" = dl."SET_OF_BOOKS_ID" 
			AND d."DEPR_GROUP_ID" = dl."DEPR_GROUP_ID"
			AND d."GL_POST_MO_YR" = :i_nvo_cpr_control.i_month
			AND d."VINTAGE" = to_number(to_char(dl.gl_post_mo_yr,'yyyy'))
	)
			group by dl.SET_OF_BOOKS_ID, dl.DEPR_GROUP_ID, to_number(to_char(dl.gl_post_mo_yr,'yyyy')), :i_nvo_cpr_control.i_month
	;
	

	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs("Depr Net Salvage Amort Create Error: " + SQLCA.SQLErrText)
		rollback;
//			goto next_company
		return false
	end if
	
	// copy new vintage in depr_net_salvage_amort
	// Maint 2615: include NOT EXISTS to avoid duplicates when using the monthly amort option
	INSERT INTO DEPR_NET_SALVAGE_AMORT
	(SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, GL_POST_MO_YR, 
	COST_OF_REMOVAL_BAL, SALVAGE_BAL)
	SELECT SET_OF_BOOKS_ID, DEPR_NET_SALVAGE_AMORT.DEPR_GROUP_ID, vintage, add_months(:i_nvo_cpr_control.i_month,1),
	COST_OF_REMOVAL_BAL, SALVAGE_BAL
	FROM DEPR_NET_SALVAGE_AMORT, DEPR_GROUP
	WHERE DEPR_NET_SALVAGE_AMORT.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID 
	AND COMPANY_ID = :i_nvo_cpr_control.i_company 
	AND GL_POST_MO_YR = :i_nvo_cpr_control.i_month
	and vintage = to_number(TO_CHAR(:i_nvo_cpr_control.i_month, 'YYYY'))
	AND NOT EXISTS (
		SELECT 1 FROM DEPR_NET_SALVAGE_AMORT d 
		WHERE d."SET_OF_BOOKS_ID" = DEPR_NET_SALVAGE_AMORT."SET_OF_BOOKS_ID" 
			AND d."DEPR_GROUP_ID" = DEPR_NET_SALVAGE_AMORT."DEPR_GROUP_ID"
			AND d."GL_POST_MO_YR" = add_months(:i_nvo_cpr_control.i_month,1)
			AND d."VINTAGE" = DEPR_NET_SALVAGE_AMORT.VINTAGE
	);
	
	if SQLCA.SqlCode <> 0 then 
		f_pp_msgs("Depr Net Salvage Amort Copy Error: " + SQLCA.SQLErrText)
		rollback;
//			goto next_company
		return false
	end if
end if

//CBS maint 33594 - for monthly amortization, insert missing rows to depr_net_salvage_amort
INSERT INTO depr_net_salvage_amort (set_of_books_id, depr_group_id, gl_post_mo_yr,
	vintage, cor_treatment, salvage_treatment, net_salvage_amort_life, cost_of_removal_bal,
	cost_of_removal_amort, cost_of_removal_reserve, salvage_bal, salvage_amort, salvage_reserve)
SELECT DISTINCT dl.set_of_books_id AS set_of_books_id,
	dl.depr_group_id AS depr_group_id ,
	Add_Months(dl.gl_post_mo_yr,1) AS gl_post_mo_yr,
	To_Number(To_Char(dl.gl_post_mo_yr,'YYYY')) AS vintage,
	dmr.cor_treatment AS cor_treatment,
	dmr.salvage_treatment AS salvage_treatment,
	dmr.net_salvage_amort_life AS net_salvage_amort_life,
	Sum(dl.cost_of_removal) AS cost_of_removal_bal,
	0 AS cost_of_removal_amort,
	0 AS cost_of_removal_reserve, /*Safe to set to zero because if this row existed last month, it will have already been rolled forward when the new month was opened, so the NOT EXISTS will catch it*/
	Sum(dl.salvage_cash + dl.salvage_returns) AS salvage_bal,
	0 AS salvage_amort,
	0 AS salvage_reserve /*Safe to set to zero because if this row existed last month, it will have already been rolled forward when the new month was opened, so the NOT EXISTS will catch it*/
FROM depr_ledger dl, depr_group dg, depr_method_rates dmr
WHERE dl.depr_group_id = dg.depr_group_id
AND dl.depr_group_id = dg.depr_group_id
AND dmr.depr_method_id = dg.depr_method_id
AND dmr.set_of_books_id = dl.set_of_books_id
AND dmr.effective_date = (
	SELECT Max(effective_date)
	FROM depr_method_rates dmr2
	WHERE dmr.depr_method_id = dmr2.depr_method_id
	AND dmr.set_of_books_id = dmr2.set_of_books_id
	AND dmr2.effective_date <= Add_Months(dl.gl_post_mo_yr,1)
)
AND (Upper(Trim(dmr.cor_treatment)) = 'MONTH'
 OR Upper(Trim(dmr.salvage_treatment)) = 'MONTH')
AND dl.gl_post_mo_yr = :i_nvo_cpr_control.i_month
AND COMPANY_ID = :i_nvo_cpr_control.i_company 
AND NOT EXISTS (
	SELECT 1
	FROM depr_net_salvage_amort new_month
	WHERE dl.depr_group_id = new_month.depr_group_id
	AND dl.set_of_books_id = new_month.set_of_books_id
	AND Add_Months(dl.gl_post_mo_yr,1) = new_month.gl_post_mo_yr
	AND To_Number(To_Char(dl.gl_post_mo_yr,'YYYY')) = new_month.vintage
)
GROUP BY dl.set_of_books_id, 
	dl.depr_group_id, 
	Add_Months(dl.gl_post_mo_yr,1), /* gl_post_mo_yr, */
	To_Number(To_Char(dl.gl_post_mo_yr,'YYYY')), /* vintage,*/
	dmr.cor_treatment, 
	dmr.salvage_treatment, 
	dmr.net_salvage_amort_life;
	
if SQLCA.SqlCode <> 0 then 
	f_pp_msgs("Depr Net Salvage Amort Insert Monthly Error: " + SQLCA.SQLErrText)
	rollback;
	return false // goto next_company
end if

//CBS maint 33594 - for monthly amortization, add dollars from the closing month to the next month's record (for correct vintage)
UPDATE depr_net_salvage_amort new_month
SET (salvage_bal, cost_of_removal_bal) = (
	SELECT nvl(closed_month.salvage_bal,0) + dl.salvage_cash + dl.salvage_returns,
		nvl(closed_month.cost_of_removal_bal,0) + dl.cost_of_removal
	FROM depr_ledger dl, depr_net_salvage_amort closed_month
	WHERE dl.depr_group_id = new_month.depr_group_id
	AND dl.set_of_books_id = new_month.set_of_books_id
	AND Add_Months(dl.gl_post_mo_yr,1) = new_month.gl_post_mo_yr
	AND To_Number(To_Char(dl.gl_post_mo_yr,'YYYY')) = new_month.vintage /*this is done after the  month is inserted into depr_net_salvage_amort */
	AND dl.depr_group_id = closed_month.depr_group_id (+)
	AND dl.set_of_books_id = closed_month.set_of_books_id (+)
	AND dl.gl_post_mo_yr = closed_month.gl_post_mo_yr (+)
	AND To_Number(To_Char(dl.gl_post_mo_yr,'YYYY')) = closed_month.vintage (+)
	AND dl.gl_post_mo_yr = :i_nvo_cpr_control.i_month
)
WHERE (new_month.cor_treatment <> 'NO'
 OR new_month.salvage_treatment <> 'NO')
	AND GL_POST_MO_YR = :i_nvo_cpr_control.i_month
AND EXISTS (
	SELECT 1
	FROM depr_ledger dl1
	WHERE dl1.depr_group_id = new_month.depr_group_id
	AND dl1.set_of_books_id = new_month.set_of_books_id
	AND Add_Months(dl1.gl_post_mo_yr,1) = new_month.gl_post_mo_yr
	AND To_Number(To_Char(dl1.gl_post_mo_yr,'YYYY')) = new_month.vintage
	AND dl1.gl_post_mo_yr = :i_nvo_cpr_control.i_month
)
AND new_month.depr_group_id in
(
	SELECT depr_group_id
	FROM depr_group
	WHERE company_id =  :i_nvo_cpr_control.i_company 
)
;
	
if SQLCA.SqlCode <> 0 then 
	f_pp_msgs("Depr Net Salvage Amort Monthly Update Error: " + SQLCA.SQLErrText)
	rollback;
	return false // goto next_company
end if

return true
end function

public function boolean uf_roll_allo_factors_forward ();/************************************************************************************************************************************************************
**
**	uf_roll_allo_factors_forward()
**	
**	This function rolls the allo factors forward to open months
**	
**	Parameters	:	longlong	:	(a_button_number) Corresponds to the various buttons on w_cpr_control
**	
**	Returns		:	boolean	: Success: True; Failure: False
**
************************************************************************************************************************************************************/
longlong i
string open_month
datetime open_month_date

f_pp_msgs("Copying new reserve allo factors to open months for company " + string(i_nvo_cpr_control.i_company) +  " at " + String(Today(), "hh:mm:ss"))
	
for i = 1 to i_nvo_cpr_control.i_col_num - 1 
	open_month = String(i_nvo_cpr_control.i_months[i], "mm/yyyy")
	open_month_date = i_nvo_cpr_control.i_months[i]
	
	delete from depr_vintage_summary where accounting_month = :open_month_date
	and depr_group_id in (
		select depr_group_id from depr_group where company_id = :i_nvo_cpr_control.i_company);

	if f_check_sql_error(sqlca, "Cannot Delete depr_vintage_summary for month: " + open_month) = 1 then
		return false
	end if

	insert into depr_vintage_summary 
		(set_of_books_id, depr_group_id, vintage, accounting_month, accum_cost, combined_depr_group_id)
		select set_of_books_id, depr_group_id, vintage, :open_month_date, accum_cost, combined_depr_group_id
		from   depr_vintage_summary
		where accounting_month = :i_nvo_cpr_control.i_month
		and depr_group_id in (select depr_group_id 
									from depr_group 
									where company_id = :i_nvo_cpr_control.i_company);

	if f_check_sql_error(sqlca, "Cannot Copy depr_vintage_summary to month: " + open_month) = 1 then
		return false
	end if

	delete from depr_res_allo_factors where month = :open_month_date 
		and depr_group_id in (
		select depr_group_id from depr_group where company_id = :i_nvo_cpr_control.i_company);

	if f_check_sql_error(sqlca, "Cannot Delete depr_res_allo_factors for month: " + open_month) = 1 then
		return false
	end if

	insert into depr_res_allo_factors 
		(set_of_books_id, depr_group_id, vintage, month, factor, 
		 theo_factor, remaining_life, life_factor, cor_factor)
		select set_of_books_id, depr_group_id, vintage, :open_month_date, factor, 
				 theo_factor, remaining_life, life_factor, cor_factor
		from   depr_res_allo_factors
		where  month = :i_nvo_cpr_control.i_month
		and depr_group_id in (select depr_group_id 
									from depr_group 
									where company_id = :i_nvo_cpr_control.i_company);

	if f_check_sql_error(sqlca, "Cannot Copy depr_res_allo_factors to month: " + open_month) = 1 then
		return false
	end if

	update depr_ledger curr
	set (rwip_allocation, rwip_cost_of_removal, rwip_salvage_cash, 
		  rwip_salvage_returns, rwip_reserve_credits ) = 
		(select prev.rwip_allocation, prev.rwip_cost_of_removal, prev.rwip_salvage_cash, 
		  prev.rwip_salvage_returns, prev.rwip_reserve_credits
		from depr_ledger prev
		where prev.gl_post_mo_yr=:i_nvo_cpr_control.i_month
		and prev.depr_group_id = curr.depr_group_id
		and prev.set_of_books_id = curr.set_of_books_id
		)
	where (depr_group_id,set_of_books_id) in 
		(select r.depr_group_id,set_of_books_id 
		from depr_ledger r, depr_group g
		where r.gl_post_mo_yr=:i_nvo_cpr_control.i_month
		and r.depr_group_id = g.depr_group_id
		and company_id = :i_nvo_cpr_control.i_company
		)
	and gl_post_mo_yr=:open_month_date
	;

	if f_check_sql_error(sqlca, "Cannot Copy rwip allocation to month: " + open_month) = 1 then
		return false
	end if

	commit ;
next

return true
end function

public subroutine uf_check_factor_count ();/************************************************************************************************************************************************************
**
**	uf_check_factor_count()
**	
**	This function checks to see if factors were created for all depr groups in cpr_ledger
**	
**	Parameters	:	(none)
**	
**	Returns		:	(none)
**
************************************************************************************************************************************************************/
string month_string
longlong check

month_string =  String(i_nvo_cpr_control.i_month, "MM/YYYY")

select count(*) into :check from (
select distinct depr_group_id, to_number(to_char(eng_in_service_year,'yyyy'))
from cpr_ledger
where accum_cost <> 0 and ledger_status < 100 and
company_id = :i_nvo_cpr_control.i_company and subledger_indicator > -1
minus
select distinct depr_group_id, vintage from depr_res_allo_factors
where month = :i_nvo_cpr_control.i_month and
 depr_group_id in (select depr_group.depr_group_id from depr_group , depr_ledger
 where depr_group.depr_group_id = depr_ledger.depr_group_id 
 and company_id = :i_nvo_cpr_control.i_company
 and nvl(subledger_type_id,0) >= 0 )
);

if check > 0 then
	f_pp_msgs("Did not create some depr res allo factors for " + month_string)
end if
end subroutine

public subroutine uf_archive_gl_transaction ();/************************************************************************************************************************************************************
**
**	uf_archive_gl_transaction()
**	
**	This function archives records from gl_transaction based on a system control
**	
**	Parameters	:	(none)
**	
**	Returns		:	(none)
**
************************************************************************************************************************************************************/
string num_months_str, sqls
longlong num_months

num_months_str =  f_pp_system_control_company("Archive GL Transaction - Months", i_nvo_cpr_control.i_company) 
if num_months_str = '' then
	num_months = -1
else
	num_months = long(num_months_str)
	if num_months < 0 then
		num_months  = -1
	end if
end if

if num_months >= 0 then
	
	f_pp_msgs("Archive GL Transaction table " + String(Today(), "hh:mm:ss"))
	
	insert into arc_gl_transaction (gl_trans_id,month,company_number,gl_account,debit_credit_indicator,
		amount,gl_je_code,gl_status_id,description,source,pend_trans_id,
		asset_id,originator,comments, trans_type)
	 
	select gl_trans_id,month,company_number,gl_account,debit_credit_indicator,
		amount,gl_je_code,gl_status_id, gl.description,source,pend_trans_id,
		asset_id,originator,comments, trans_type
	from gl_transaction gl , company
	where trim(gl.company_number) = company.gl_company_no and
	gl_status_id = 3 and month <=    add_months(:i_nvo_cpr_control.i_month, - :num_months)  and
	company_id = :i_nvo_cpr_control.i_company and not exists
	(select  gl_trans_id from arc_gl_transaction arc     
		 where gl.gl_trans_id = arc.gl_trans_id) ; 
		 
  
	
	if sqlca.sqlcode < 0 then
		
		f_pp_msgs("Archive GL Transaction table " + String(Today(), "hh:mm:ss"))
		
	else
		
			f_pp_msgs("Delete from Transaction table " + String(Today(), "hh:mm:ss"))
		 
			
//		//###sjh Maint 7641 
	sqls= "to_date(' " +  mid(string(i_nvo_cpr_control.i_month, 'mm/dd/yyyy'), 1,10) +  "'" + ",'mm/dd/yyyy')"
	

	
	sqls =  "delete from gl_transaction where gl_trans_id in  " + &
				 " (select gl_trans_id from gl_transaction, company " + & 
				 " where trim(gl_transaction.company_number) = company.gl_company_no and " + &
				 " gl_status_id = 3 and month <= add_months(" + sqls + ", -" + string(num_months)  + & 
				") and " + &
				" company_id = " + string(i_nvo_cpr_control.i_company) + ")"
				
	 sqls = f_sql_add_hint(sqls, 'cpr_control.delete_gl_transaction')
	
	execute immediate :sqls;

		
		if sqlca.sqlcode < 0 then
			f_pp_msgs("Error deleting from  the GL transaction table. Process will continue  " + String(Today(), "hh:mm:ss"))
		end if
	end if
  
end if  // end num_months > 0
end subroutine

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ssp_close_powerplant_custom_version nvo_ssp_close_powerplant_custom_version
nvo_ppdepr_interface_custom_version nvo_ppdepr_interface_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version

choose case a_pbd_name
	case 'ssp_close_powerplant_custom.pbd'
		return nvo_ssp_close_powerplant_custom_version.custom_version
	case 'ppdepr_interface_custom.pbd'
		return nvo_ppdepr_interface_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version	
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

