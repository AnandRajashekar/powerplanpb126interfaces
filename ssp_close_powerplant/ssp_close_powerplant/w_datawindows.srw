HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_27 from datawindow within w_datawindows
end type
type dw_26 from datawindow within w_datawindows
end type
type dw_25 from datawindow within w_datawindows
end type
type dw_24 from datawindow within w_datawindows
end type
type dw_23 from datawindow within w_datawindows
end type
type dw_22 from datawindow within w_datawindows
end type
type dw_21 from datawindow within w_datawindows
end type
type dw_20 from datawindow within w_datawindows
end type
type dw_19 from datawindow within w_datawindows
end type
type dw_18 from datawindow within w_datawindows
end type
type dw_17 from datawindow within w_datawindows
end type
type dw_16 from datawindow within w_datawindows
end type
type dw_15 from datawindow within w_datawindows
end type
type dw_14 from datawindow within w_datawindows
end type
type dw_13 from datawindow within w_datawindows
end type
type dw_12 from datawindow within w_datawindows
end type
type dw_11 from datawindow within w_datawindows
end type
type dw_10 from datawindow within w_datawindows
end type
type dw_9 from datawindow within w_datawindows
end type
type dw_8 from datawindow within w_datawindows
end type
type dw_7 from datawindow within w_datawindows
end type
type dw_6 from datawindow within w_datawindows
end type
type dw_5 from datawindow within w_datawindows
end type
type dw_4 from datawindow within w_datawindows
end type
type dw_3 from datawindow within w_datawindows
end type
type dw_2 from datawindow within w_datawindows
end type
type dw_1 from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer x = 837
integer y = 388
integer width = 4407
integer height = 2308
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
dw_27 dw_27
dw_26 dw_26
dw_25 dw_25
dw_24 dw_24
dw_23 dw_23
dw_22 dw_22
dw_21 dw_21
dw_20 dw_20
dw_19 dw_19
dw_18 dw_18
dw_17 dw_17
dw_16 dw_16
dw_15 dw_15
dw_14 dw_14
dw_13 dw_13
dw_12 dw_12
dw_11 dw_11
dw_10 dw_10
dw_9 dw_9
dw_8 dw_8
dw_7 dw_7
dw_6 dw_6
dw_5 dw_5
dw_4 dw_4
dw_3 dw_3
dw_2 dw_2
dw_1 dw_1
end type
global w_datawindows w_datawindows

type variables

end variables

on w_datawindows.create
this.dw_27=create dw_27
this.dw_26=create dw_26
this.dw_25=create dw_25
this.dw_24=create dw_24
this.dw_23=create dw_23
this.dw_22=create dw_22
this.dw_21=create dw_21
this.dw_20=create dw_20
this.dw_19=create dw_19
this.dw_18=create dw_18
this.dw_17=create dw_17
this.dw_16=create dw_16
this.dw_15=create dw_15
this.dw_14=create dw_14
this.dw_13=create dw_13
this.dw_12=create dw_12
this.dw_11=create dw_11
this.dw_10=create dw_10
this.dw_9=create dw_9
this.dw_8=create dw_8
this.dw_7=create dw_7
this.dw_6=create dw_6
this.dw_5=create dw_5
this.dw_4=create dw_4
this.dw_3=create dw_3
this.dw_2=create dw_2
this.dw_1=create dw_1
this.Control[]={this.dw_27,&
this.dw_26,&
this.dw_25,&
this.dw_24,&
this.dw_23,&
this.dw_22,&
this.dw_21,&
this.dw_20,&
this.dw_19,&
this.dw_18,&
this.dw_17,&
this.dw_16,&
this.dw_15,&
this.dw_14,&
this.dw_13,&
this.dw_12,&
this.dw_11,&
this.dw_10,&
this.dw_9,&
this.dw_8,&
this.dw_7,&
this.dw_6,&
this.dw_5,&
this.dw_4,&
this.dw_3,&
this.dw_2,&
this.dw_1}
end on

on w_datawindows.destroy
destroy(this.dw_27)
destroy(this.dw_26)
destroy(this.dw_25)
destroy(this.dw_24)
destroy(this.dw_23)
destroy(this.dw_22)
destroy(this.dw_21)
destroy(this.dw_20)
destroy(this.dw_19)
destroy(this.dw_18)
destroy(this.dw_17)
destroy(this.dw_16)
destroy(this.dw_15)
destroy(this.dw_14)
destroy(this.dw_13)
destroy(this.dw_12)
destroy(this.dw_11)
destroy(this.dw_10)
destroy(this.dw_9)
destroy(this.dw_8)
destroy(this.dw_7)
destroy(this.dw_6)
destroy(this.dw_5)
destroy(this.dw_4)
destroy(this.dw_3)
destroy(this.dw_2)
destroy(this.dw_1)
end on

type dw_27 from datawindow within w_datawindows
integer x = 3639
integer y = 476
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_allo_depr_reserve_theo_list"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_26 from datawindow within w_datawindows
integer x = 3639
integer y = 32
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_allo_depr_resrwip_theo_list"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_25 from datawindow within w_datawindows
integer x = 2926
integer y = 1776
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_messagebox_translate"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_24 from datawindow within w_datawindows
integer x = 2199
integer y = 1776
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_reserve_ratios"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_23 from datawindow within w_datawindows
integer x = 1477
integer y = 1776
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_mortality_curve_points_ret"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_22 from datawindow within w_datawindows
integer x = 754
integer y = 1776
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_surviving_percentage"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_21 from datawindow within w_datawindows
integer x = 50
integer y = 1776
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_temp_dynamic"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_20 from datawindow within w_datawindows
integer x = 1477
integer y = 1352
integer width = 686
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_19 from datawindow within w_datawindows
integer x = 50
integer y = 1364
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_cpr_company"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_18 from datawindow within w_datawindows
integer x = 50
integer y = 924
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_17 from datawindow within w_datawindows
integer x = 1481
integer y = 908
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_fcst_groups_for_theo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_16 from datawindow within w_datawindows
integer x = 754
integer y = 1352
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_fcst_groups_rate_recalc"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_15 from datawindow within w_datawindows
integer x = 754
integer y = 908
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_groups_for_theo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_14 from datawindow within w_datawindows
integer x = 2926
integer y = 1352
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_allo_combined_depr_reserve_theo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_13 from datawindow within w_datawindows
integer x = 2926
integer y = 908
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_allo_combined_depr_reserve"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_12 from datawindow within w_datawindows
integer x = 2199
integer y = 1352
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_allo_combined_depr_resrwip_theo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_11 from datawindow within w_datawindows
integer x = 2199
integer y = 908
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_allo_combined_depr_resrwip"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_10 from datawindow within w_datawindows
integer x = 2930
integer y = 476
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_allo_subl_reserve"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_9 from datawindow within w_datawindows
integer x = 2930
integer y = 32
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_allo_depr_reserve_theo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_8 from datawindow within w_datawindows
integer x = 2203
integer y = 476
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_allo_depr_resrwip_theo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_7 from datawindow within w_datawindows
integer x = 2203
integer y = 32
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_allo_depr_reserve"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_6 from datawindow within w_datawindows
integer x = 1477
integer y = 476
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_allo_depr_resrwip"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_5 from datawindow within w_datawindows
integer x = 1477
integer y = 32
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_combined_depr_res_allo_factors"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_4 from datawindow within w_datawindows
integer x = 782
integer y = 480
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_3 from datawindow within w_datawindows
integer x = 50
integer y = 488
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_2 from datawindow within w_datawindows
integer x = 754
integer y = 36
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_depr_res_allo_factors"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_datawindows
integer x = 37
integer y = 40
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

