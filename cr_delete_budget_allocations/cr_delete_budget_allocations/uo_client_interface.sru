HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$FECR.pbl - Budget Allocations ...
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_budget_version, i_company, i_test_or_actual, i_use_new_structures_table, i_company_field, i_run_by_dept_credits_method
string i_me_validations, i_combo_validations, i_proj_based_validations, i_mn_validations, i_custom_validations
boolean i_suspense_processed
longlong i_actuals_month, i_start_year

string i_exe_name = 'cr_delete_budget_allocations.exe'
end variables

forward prototypes
public function integer uf_read ()
public function longlong uf_delete (string a_budget_version, longlong a_allocation_id)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function integer uf_read ();//*****************************************************************************************
//
//  Object      :  uo_cr_delete_budget_allocations
//  UO Function :  uf_read
//
//	 Returns     :  1 if successful
// 					-1 otherwise
//
//*****************************************************************************************
string sqls, lower_bv, g_command_line_array[], to_user
environment env
longlong num_alloc_ids, i, rtn, cmd_line_arg_elements, locked

any alloc_id[]

rtn = 1
//**********************************************************************************
//
//  The command line argument cannot be NULL ... 
//
//  It will be passed as a comma separated list in the following format:  
//
//  budget_version,company
//
//  i_test_or_actual is hardcoded to ACTUAL since we don't want to delete test
//	 allocations data yet
//
//**********************************************************************************
// ### - SEK - 7078 - 031711: Need to trim g_command_line_args
g_command_line_args = trim(g_command_line_args)

if isnull(g_command_line_args) or g_command_line_args = "" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: The command line argument is NULL.  Cannot run the Budget Allocations!")
	f_pp_msgs(" ")
	rtn = -1
	goto halt_the_app
end if

//  Translate the command line argument into the appropriate
//  variables.  If the 3 arguments are not passed, log an error.
cmd_line_arg_elements = f_parsestringintostringarray(g_command_line_args, ",", g_command_line_array[])

	
if cmd_line_arg_elements < 2 then
	f_pp_msgs("The command line argument contained " + &
		string(cmd_line_arg_elements) + " argument(s).  " + &
		"It must have all 2 arguments that are required.  " + &
		"Cannot run the Budget Allocations!")
	rtn = -1
	goto halt_the_app
end if

i_budget_version = trim(g_command_line_array[1])
i_company		  = trim(g_command_line_array[2])
i_test_or_actual = "ACTUAL"


//
//	 Make sure that the budget_version is not locked in the CR or PowerPlant
//
setnull(locked)
i_actuals_month = 0
i_start_year    = 0

select locked, actuals_month, start_year into :locked, :i_actuals_month, :i_start_year
  from budget_version
 where lower(description) = lower(:i_budget_version);

if isnull(i_actuals_month) then i_actuals_month = 0
if isnull(i_start_year)    then i_start_year    = 0

if locked = 1 then
	f_pp_msgs("  ")
	f_pp_msgs("The budget version (" + i_budget_version + ") is locked for entry.  " + &
				 "Cannot delete the Budget Allocations!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

setnull(locked)
select locked into :locked
  from cr_budget_version
 where lower(budget_version) = lower(:i_budget_version);

if locked = 1 then
	f_pp_msgs("  ")
	f_pp_msgs("The budget version (" + i_budget_version + ") is locked for entry.  " + &
				 "Cannot delete the Budget Allocations!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if



//*****************************************************************************************
//
//  Get the Structures Variable:  New variable to determine which structure values
//                                table is being used.
//
//*****************************************************************************************
setnull(i_use_new_structures_table)
select upper(rtrim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(rtrim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"


//*****************************************************************************************
//
//  Other CR System Control Lookups:  Company Field
//
//*****************************************************************************************
setnull(i_company_field)
select upper(control_value) into :i_company_field
  from cr_system_control 
 where upper(control_name) = 'COMPANY FIELD';

if isnull(i_company_field) or trim(i_company_field) = "" then
	f_pp_msgs("  ")
	f_pp_msgs("No COMPANY FIELD was found in cr_system_control ... " + &
				 "No allocation will be calculated.")
	f_pp_msgs("  ")
	rollback using sqlca;
	rtn = -1
	goto halt_the_app
end if

i_company_field = f_cr_clean_string(trim(i_company_field))


//*****************************************************************************************
//
//  Other CR System Control Lookups:  Run by Dept - Clearing Calc Rate - Credits Method
//
//*****************************************************************************************
setnull(i_run_by_dept_credits_method)
select upper(trim(control_value)) into :i_run_by_dept_credits_method
  from cr_system_control 
 where upper(trim(control_name)) = 'RUN BY DEPT: CALCRATE: OBEY CREDITS';

if isnull(i_run_by_dept_credits_method) or trim(i_run_by_dept_credits_method) = "" then
	i_run_by_dept_credits_method = "NO"
end if



//*****************************************************************************************
//
//  Get the Validations Variables:  System switches determine whether validations are 
// 											enabled or not.  The default is NO.
//
//*****************************************************************************************

//  Switches:
setnull(i_me_validations)
select upper(rtrim(control_value)) into :i_me_validations from cr_system_control 
 where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - ME';
if isnull(i_me_validations) then i_me_validations = "NO"

f_pp_msgs("Master Element Validation = " + i_me_validations)

setnull(i_combo_validations)
select upper(rtrim(control_value)) into :i_combo_validations from cr_system_control 
 where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - COMBO';
if isnull(i_combo_validations) then i_combo_validations = "NO"

f_pp_msgs("Combination Validation    = " + i_combo_validations)

//  N/A FOR BUDGET
//setnull(i_proj_based_validations)
//select upper(rtrim(control_value)) into :i_proj_based_validations from cr_system_control 
// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
//if isnull(i_proj_based_validations) then i_proj_based_validations = "NO"

//f_pp_msgs("Projects-Based Validation = " + i_proj_based_validations)
i_proj_based_validations = "NO"

//  N/A FOR BUDGET
//setnull(i_mn_validations)
//select upper(rtrim(control_value)) into :i_mn_validations from cr_system_control 
// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - MONTH NUMBER';
//if isnull(i_mn_validations) then i_mn_validations = "NO"
//
//f_pp_msgs("Month Number Validation   = " + i_mn_validations)
i_mn_validations = "NO"

setnull(i_custom_validations)
select upper(rtrim(control_value)) into :i_custom_validations from cr_system_control 
 where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - CUSTOM';
if isnull(i_custom_validations) then i_custom_validations = "NO"

f_pp_msgs("Custom Validation         = " + i_custom_validations)


//*****************************************************************************************
//
//  Delete the Budget Allocations:
//
//*****************************************************************************************
i_suspense_processed = false


select lower(:i_budget_version) into :lower_bv from dual;

// Get a list of the allocation ids to loop over for deleting
sqls = "select distinct allocation_id from cr_budget_data " + &
		 " where lower(budget_version) = '" + lower_bv + "' " + &
		 "   and company = '" + i_company + "' " + &
		 "	  and source_id in (" + &
		 "		select source_id from cr_sources where upper(description) in ('ALLOCATIONS', 'INTER-COMPANY'))"
		 
f_get_column(alloc_id, sqls)

num_alloc_ids = upperbound(alloc_id)

if isnull(num_alloc_ids) then num_alloc_ids = 0

if num_alloc_ids = 0 then
	f_pp_msgs("  ")
	f_pp_msgs("There are no allocation results for the budget version '" + i_budget_version + "'")
	f_pp_msgs("Nothing will be deleted.")
	f_pp_msgs("  ")
	goto halt_the_app
end if

for i = 1 to num_alloc_ids
	
	rtn = uf_delete(i_budget_version, alloc_id[i])
	
	if rtn = -1 then
		rollback;
		rtn = -1
		goto halt_the_app
	end if 
	
next

commit;


halt_the_app:

if rtn = 1 then
	f_pp_msgs("  ")
	f_pp_msgs("The budget allocations were SUCCESSFULLY DELETED.")
	f_pp_msgs("  ")
	g_rtn_code = 0
else
	g_rtn_code = -1
	
	//  Mail Logic:
	if upper(i_test_or_actual) <> 'TEST' then
		
		// Find the user (from cr_system_control) to send a failure e-mail to.
		setnull(to_user)
		select trim(control_value) into :to_user from cr_system_control
		 where upper(control_name) = 'ALLOCATIONS ERROR EMAIL RECIPIENT';
		if isnull(to_user) or to_user = "" then to_user = "None"
		
		if upper(to_user) <> "NONE" then
			
			//  Set the correct win API variable.
			getenvironment(env) 
			choose case env.OSType
				case Windows!  
					if env.win16 = true then 
						uo_winapi = create u_external_function_winapi
					else
						uo_winapi = create u_external_function_win32
					end if	
				case WindowsNT!
					if env.win16 = true then 
						uo_winapi = create u_external_function_winapi
					else	 
						uo_winapi = create u_external_function_win32
					end if
			end choose
			
			f_pp_msgs("Sending error email to user: " + to_user)
			g_msmail = create uo_smtpmail
			f_send_mail('pwrplant','CR_DELETE_BUDGET_ALLOCATIONS.EXE: TERMINATED WITHOUT COMPLETING', &
				'The cr_delete_budget_allocations encountered an error and terminated ' + &
				'without completing.  See the PowerPlant Online Logs for detailed information.',to_user)
		
			destroy g_msmail
		
		end if  //  if upper(to_user) <> "NONE" then ...
		
	end if  //  if upper(i_test_or_actual) <> 'TEST' then ...
	
end if

return g_rtn_code
end function

public function longlong uf_delete (string a_budget_version, longlong a_allocation_id);//*******************************************************************************************
//
//  Object      :  uo_cr_delete_budget_allocations
//  UO Function :  uf_delete
//
//  Description :  This script will delete the results of a budget allocation from the cost
//                 repository.  The user selects the allocation type and the month
//                 and period to delete.
//
//	 Returns     :  1 if successful
// 					-1 otherwise
//
//*******************************************************************************************
longlong allocation_row, allocation_id, rtn, month_number, month_period, counter, &
		 month_row, clearing_indicator, reversed, whole_month, locked
string description, budget_version, year_string, me_validations, combo_validations, &
		 custom_validations, proj_based_validations, mn_validations
boolean all_months


select clearing_indicator into :clearing_indicator
  from cr_allocation_control_bdg
 where allocation_id = :a_allocation_id;
 

				
// ---- use global variables set in window open event ---- 
////*****************************************************************************************
////
////  Get the Validations Variables:  System switches determine whether validations are 
//// 											enabled or not.  The default is NO.
////
////*****************************************************************************************
//
////  Switches:
//setnull(me_validations)
//select upper(rtrim(control_value)) into :me_validations from cr_system_control 
// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - ME';
//if isnull(me_validations) then me_validations = "NO"
//
//f_pp_msgs("Master Element Validation = " + me_validations)
//
//setnull(combo_validations)
//select upper(rtrim(control_value)) into :combo_validations from cr_system_control 
// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - COMBO';
//if isnull(combo_validations) then combo_validations = "NO"
//
//f_pp_msgs("Combination Validation    = " + combo_validations)
//
////  N/A FOR BUDGET
////setnull(g_proj_based_validations)
////select upper(rtrim(control_value)) into :g_proj_based_validations from cr_system_control 
//// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
////if isnull(g_proj_based_validations) then i_proj_based_validations = "NO"
//
////f_pp_msgs("Projects-Based Validation = " + i_proj_based_validations)
//proj_based_validations = "NO"
//
////  N/A FOR BUDGET
////setnull(g_mn_validations)
////select upper(rtrim(control_value)) into :g_mn_validations from cr_system_control 
//// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - MONTH NUMBER';
////if isnull(g_mn_validations) then i_mn_validations = "NO"
////
////f_pp_msgs("Month Number Validation   = " + i_mn_validations)
//mn_validations = "NO"
//
//setnull(custom_validations)
//select upper(rtrim(control_value)) into :custom_validations from cr_system_control 
// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - CUSTOM';
//if isnull(custom_validations) then custom_validations = "NO"
//
//f_pp_msgs("Custom Validation         = " + custom_validations)



//  FIRST: delete records from the cr_budget_data table ...
f_pp_msgs("Deleting cr_budget_data records (Reversal) ...")

//  Deleting any reversals that may exist ... 
//  NO, NO, NO - Only perform this if the allocation has been reversed (it is very slow) ...
//  We must now perform this every time, since the "undo" functionality will populate
//  the reversal id ... there is no way to selectively determine if an allocation has
//  been "un-done" like we can with reversals since the cr_alloc_process_control_bdg record
//  gets deleted with an "undo".
if i_test_or_actual = "ACTUAL" then
	reversed = 1
//	reversed = 0
//	select reversed into :reversed
//	  from cr_alloc_process_control_bdg
//	 where month_number    between :i_delete_month_number and :i_delete_mn2 and
//   		 month_period  = :month_period and 
//        budget_version = :budget_version and
//		  	 allocation_id = :allocation_id and
//			 upper(test_or_actual) = 'ACTUAL';
//	if isnull(reversed) then reversed = 0
	
	if reversed = 1 then 	
		if clearing_indicator = 4 or clearing_indicator = 5 then
			delete from cr_budget_data a where exists
				(select * from cr_budget_data b
	   		  where a.reversal_id  = b.id and
	  	      	  	  allocation_id  = :a_allocation_id and
						  budget_version = :a_budget_version and
	                 source_id in
						    (select source_id from cr_sources 
                        where upper(rtrim(description)) = 'INTER-COMPANY')) and
			allocation_id = :a_allocation_id;
		else
			delete from cr_budget_data a where exists
				(select * from cr_budget_data b
	   		  where a.reversal_id  = b.id and
	  	      	  	  allocation_id  = :a_allocation_id and
						  budget_version = :a_budget_version and
	                 source_id in
						    (select source_id from cr_sources 
                        where upper(rtrim(description)) = 'ALLOCATIONS')) and
			allocation_id = :a_allocation_id;
		end if
	end if  //  if reversed = 1 then  ...
else
	if reversed = 1 then 	
		if clearing_indicator = 4 or clearing_indicator = 5 then
			delete from cr_budget_data_test a where exists
				(select * from cr_budget_data_test b
	   		  where a.reversal_id  = b.id and
	  	      	  	  allocation_id  = :a_allocation_id and
						  budget_version = :a_budget_version and
	                 source_id in
						    (select source_id from cr_sources 
                        where upper(rtrim(description)) = 'INTER-COMPANY')) and
			allocation_id = :a_allocation_id;
		else
			delete from cr_budget_data_test a where exists
				(select * from cr_budget_data_test b
	   		  where a.reversal_id  = b.id and 
	  	      	  	  allocation_id  = :a_allocation_id and
						  budget_version = :a_budget_version and
	                 source_id in
						    (select source_id from cr_sources 
                        where upper(rtrim(description)) = 'ALLOCATIONS')) and
			allocation_id = :a_allocation_id;
		end if
	end if  //  if reversed = 1 then  ...
end if
					 
if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting reversals from cr_budget_data: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  Deleting invalid ids records if needed ...
if i_me_validations = "YES" or i_combo_validations = "YES" or i_custom_validations = "YES" or &
	i_proj_based_validations = "YES" or i_mn_validations = "YES" &
then
	//  Continue.
else
	goto after_validations
end if

f_pp_msgs("Deleting validation kickouts ...")

if i_test_or_actual = "ACTUAL" then
	if clearing_indicator = 4 or clearing_indicator = 5 then
		//  First, delete any validation kickouts.
		delete from cr_validations_invalid_ids3 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in 
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'INTER-COMPANY'));
	else
		delete from cr_validations_invalid_ids3 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'ALLOCATIONS'));
	end if
else
	if clearing_indicator = 4 or clearing_indicator = 5 then
		delete from cr_validations_invalid_ids3 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in 
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'INTER-COMPANY'));
	else
		delete from cr_validations_invalid_ids3 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'ALLOCATIONS'));
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting from cr_validations_invalid_ids3: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

if i_test_or_actual = "ACTUAL" then
	if clearing_indicator = 4 or clearing_indicator = 5 then
		//  First, delete any validation kickouts.
		delete from cr_validations_invalid_ids2 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in 
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'INTER-COMPANY'));
	else
		delete from cr_validations_invalid_ids2 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'ALLOCATIONS'));
	end if
else
	if clearing_indicator = 4 or clearing_indicator = 5 then
		delete from cr_validations_invalid_ids2 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in 
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'INTER-COMPANY'));
	else
		delete from cr_validations_invalid_ids2 where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'ALLOCATIONS'));
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting from cr_validations_invalid_ids2: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

if i_test_or_actual = "ACTUAL" then
	if clearing_indicator = 4 or clearing_indicator = 5 then
		//  First, delete any validation kickouts.
		delete from cr_validations_invalid_ids where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in 
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'INTER-COMPANY'));
	else
		delete from cr_validations_invalid_ids where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'ALLOCATIONS'));
	end if
else
	if clearing_indicator = 4 or clearing_indicator = 5 then
		delete from cr_validations_invalid_ids where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in 
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'INTER-COMPANY'));
	else
		delete from cr_validations_invalid_ids where table_name = 'cr_budget_data'
			and id in (select id from cr_budget_data where 
				allocation_id = :a_allocation_id and
				budget_version = :a_budget_version and
				source_id in
					(select source_id from cr_sources 
					  where upper(rtrim(description)) = 'ALLOCATIONS'));
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting from cr_validations_invalid_ids: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

after_validations:


//  Deleting the results of the allocation itself ...
f_pp_msgs("Deleting cr_budget_data records (Allocation Results) ...")
if i_test_or_actual = "ACTUAL" then
	if clearing_indicator = 4 or clearing_indicator = 5 then
		delete from cr_budget_data where 
	      allocation_id = :a_allocation_id and
			budget_version = :a_budget_version and
			source_id in 
				(select source_id from cr_sources 
              where upper(rtrim(description)) = 'INTER-COMPANY');
	else
		delete from cr_budget_data where 
		   allocation_id = :a_allocation_id and
			budget_version = :a_budget_version and
			source_id in
				(select source_id from cr_sources 
              where upper(rtrim(description)) = 'ALLOCATIONS');
	end if
else
	if clearing_indicator = 4 or clearing_indicator = 5 then
		delete from cr_budget_data_test where 
	      allocation_id = :a_allocation_id and
			budget_version = :a_budget_version and
			source_id in 
				(select source_id from cr_sources 
              where upper(rtrim(description)) = 'INTER-COMPANY');
	else
		delete from cr_budget_data_test where 
		   allocation_id = :a_allocation_id and
			budget_version = :a_budget_version and
			source_id in
				(select source_id from cr_sources 
              where upper(rtrim(description)) = 'ALLOCATIONS');
	end if
end if

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting from cr_budget_data: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  SECOND(a): delete the record from cr_alloc_process_control3_bdg for this allocation type and
//             month/period combination ...
delete from cr_alloc_process_control3_bdg
 where budget_version        = :a_budget_version and
       allocation_id         = :a_allocation_id  and
		 upper(test_or_actual) = :i_test_or_actual;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting from cr_alloc_process_control3_bdg: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  SECOND(b): delete the record from cr_alloc_process_control2_bdg for this allocation type and
//             month/period combination ...
delete from cr_alloc_process_control2_bdg
 where budget_version        = :a_budget_version and
       allocation_id         = :a_allocation_id  and
		 upper(test_or_actual) = :i_test_or_actual;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting from cr_alloc_process_control2_bdg: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


//  SECOND: delete the record from cr_alloc_process_control_bdg for this allocation type and
//          month/period combination ...
delete from cr_alloc_process_control_bdg
 where budget_version        = :a_budget_version and
       allocation_id         = :a_allocation_id  and
		 upper(test_or_actual) = :i_test_or_actual;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Deleting from cr_alloc_process_control_bdg: " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
end if


////  Commit the deletes and re-retrieve the bottom 2 DWs, and the reports DW ...
//commit;


return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_delete_budget_allocations_custom_version nvo_cr_delete_budget_allocations_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_delete_budget_allocations_custom.pbd'
		return nvo_cr_delete_budget_allocations_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

