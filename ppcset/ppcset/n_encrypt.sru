HA$PBExportHeader$n_encrypt.sru
$PBExportComments$v10.0.7  **  Fixed encription for Citrix
forward
global type n_encrypt from nonvisualobject
end type
end forward

global type n_encrypt from nonvisualobject autoinstantiate
end type

type prototypes
FUNCTION long CryptAcquireContextA (ref ulong hProv, ref string pszContainer, ref string pszProvider, ulong dwProvType,  ulong dwFlags) LIBRARY "advapi32.dll" alias for "CryptAcquireContextA;Ansi"
FUNCTION long CryptReleaseContext (ulong hProv, ulong dwFlags) LIBRARY "advapi32.dll"
FUNCTION long CryptCreateHash (ulong hProv, uint Algid, ulong hKey, ulong dwFlags, ref ulong phHash) LIBRARY "advapi32.dll"
FUNCTION long CryptHashData (ulong hHash,  ref string pbData,ulong dwDataLen, ulong dwFlags) LIBRARY "advapi32.dll" alias for "CryptHashData;Ansi"
FUNCTION long CryptDestroyHash (ulong hHash) LIBRARY "advapi32.dll"
FUNCTION long CryptGetHashParam (ulong hHash, ulong dwParam, ref blob pbData, ref ulong pdwDataLen, ulong dwFlags) LIBRARY "advapi32.dll"
Function ulong CryptDeriveKey (ulong hProv, ulong Algid, ulong hBaseData, ulong dwFlags, Ref ulong phKey) Library "advapi32.dll" Alias For "CryptDeriveKey"
Function ulong CryptEncrypt (ulong hKey, ulong hHash, ulong Final, ulong dwFlags, Ref blob pbData, Ref ulong pdwDataLen, ulong dwBufLen) Library "advapi32.dll" Alias For "CryptEncrypt"
Function ulong CryptAcquireContext (Ref ulong phProv,long pszContainer, String pszProvider, ulong dwProvType, ulong dwFlags) Library "advapi32.dll" Alias For "CryptAcquireContextA;Ansi"
Function ulong CryptDestroyKey (ulong hKey) Library "advapi32.dll" Alias For "CryptDestroyKey"
Function ulong CryptDecrypt (ulong hKey, ulong hHash, ulong Final, ulong dwFlags, Ref blob pbData, Ref ulong pdwDataLen) Library "advapi32.dll" Alias For "CryptDecrypt"	
Function long FormatMessageA( long flag,long input,long err,long lang,ref string msg,long len,long arg) Library "kernel32.dll" alias for "FormatMessageA;ansi"
function long GetLastError()  library "kernel32.dll"
function long WNetGetUserA(long ptr,ref string user,ref long len) LIBRARY "mpr.dll" alias for "WNetGetUserA;Ansi"


end prototypes

type variables
string i_key_create_userid
string i_key_cdate
string i_key_sso_logid
string i_key_sso_logpass

CONSTANT ULONG PROV_RSA_FULL = 1
CONSTANT ULONG CRYPT_VERIFYCONTEXT  =  4026531840 // 0xF0000000
CONSTANT ULONG CALG_MD5 = 32771 // 4<<13 | 0 | 3
CONSTANT ULONG HP_HASHVAL = 2 //  0x0002
Constant String SERVICE_PROVIDER = "Microsoft Base Cryptographic Provider v1.0"
String KEY_CONTAINER = "Metallica"
Constant ulong PP_NAME = 4
Constant ulong PP_CONTAINER = 6
Constant ulong CRYPT_NEWKEYSET = 8
Constant ulong ENCRYPT_ALGORITHM = 26625

end variables

forward prototypes
public function integer uf_encrypt_decrypt (boolean a_encrypt, ref blob a_blob, ref longlong a_len)
public function integer uf_show_blob (blob a_blob, longlong a_len)
public function longlong uf_decrypt (blob a_blob, longlong a_len)
public function longlong uf_encrypt (ref blob a_blob, string a_createid, string a_date, string a_userid, string a_passwd)
public function longlong uf_get_error (longlong a_err, ref string a_msg)
public function string uf_local_userid ()
public function longlong uf_add_line (ref blob a_b, string a_line)
public function longlong uf_read_section (blob a_blob, ref longlong a_pos, ref blob a_section)
public function integer uf_read_line (blob a_b, ref longlong a_pos, ref string a_line)
public function longlong uf_find_line (blob a_section, string a_name, ref string a_value)
public function integer uf_save_blob (string a_filename, blob a_blob)
public function integer uf_write_section (ref blob a_blob, blob a_section)
public function integer uf_write_line (ref blob a_blob, string a_line)
public function integer uf_read_blob (string a_filename, ref blob a_blob)
public function integer uf_find_section (ref blob a_blob, ref blob a_section, string a_name)
end prototypes

public function integer uf_encrypt_decrypt (boolean a_encrypt, ref blob a_blob, ref longlong a_len);ulong hCryptProv
ulong lLength
blob sTemp
ulong hHash
ulong hKey
longlong err ,err2
string msg,msg2
string ls_ret
string password
password = 'secret'
// Get handle to CSP
KEY_CONTAINER = 'KEY' +uf_local_userid()
If CryptAcquireContext(hCryptProv,0, SERVICE_PROVIDER, PROV_RSA_FULL,  CRYPT_NEWKEYSET + CRYPT_VERIFYCONTEXT) = 0 Then
     err = GetlastError()
	If CryptAcquireContext(hCryptProv, 0, SERVICE_PROVIDER, PROV_RSA_FULL,  CRYPT_VERIFYCONTEXT) = 0 Then
	     err2 = GetlastError()
		uf_get_error(err,msg)
		uf_get_error(err2,msg2)
		MessageBox("DEBUG", "Error during CryptAcquireContext for a new key container." + "~r~n" + &
		"A container with this name probably already exists. Message: First Error:" + msg + " Second Error:" +msg2)
		
		RETURN 0
  End If
End If
// --------------------------------------------------------------------
// The data will be encrypted with a session key derived from the
// password. The session key will be recreated when the data is decrypted
// only if the password used to create the key is available.
// --------------------------------------------------------------------

// Create a hash object.
If CryptCreateHash(hCryptProv, CALG_MD5, 0, 0, hHash) = 0 Then
	err = GetlastError()
	uf_get_error(err,msg)
   MessageBox("DEBUG", "Error during CryptCreateHash! Message: " + msg)
End If

// Hash the password.
blob p
p = blob(password)
If CryptHashData(hHash, password, Len(Password), 0) = 0  Then
  err = GetlastError()
  uf_get_error(err,msg)
  MessageBox("DEBUG", "Error during CryptHashData. Message: " + msg)
End If

// Derive a session key from the hash object.
If CryptDeriveKey(hCryptProv, ENCRYPT_ALGORITHM, hHash, 0, hKey) = 0 Then
	err = GetlastError()
	uf_get_error(err,msg)
  MessageBox("DEBUG", "Error during CryptDeriveKey! Message: " + msg)
End If

// Do the work
sTemp = a_blob
lLength = a_len //Len(a_blob)

If a_Encrypt Then
  // Encrypt data.
  If CryptEncrypt(hKey, 0, 1, 0, sTemp, lLength, lLength) = 0 Then
		err = GetlastError()
		uf_get_error(err,msg)
		MessageBox("DEBUG", "Error during CryptEncrypt. Message: " + msg)
  End If
Else
  // Decrypt data.
  If CryptDecrypt(hKey, 0, 1, 0, sTemp, lLength) = 0 Then
		err = GetlastError()
		uf_get_error(err,msg)
		MessageBox("DEBUG", "Error during CryptDecrypt. Message: " + msg)
  End If
End If

// This is what we return.
//ls_ret = Mid(sTemp, 1, lLength)

// Destroy session key.
If hKey <> 0 Then
  CryptDestroyKey(hKey)
End If

// Destroy hash object.
If hHash <> 0 Then
  CryptDestroyHash(hHash)
End If

// Release provider handle.
If hCryptProv <> 0 Then
  CryptReleaseContext(hCryptProv, 0)
End If
a_blob = stemp
a_len = llength
//uf_show_blob(a_blob,a_len)

RETURN 0
end function

public function integer uf_show_blob (blob a_blob, longlong a_len);longlong len
longlong i
string str,str2
len = len(a_blob)
str = ''
str2 = ''
for i = 1 to a_len
	str = str + string(asc(string(blobmid(a_blob,i,1),EncodingANSI!)),'00')
	str2 = str2 + string(blobmid(a_blob,i,1),EncodingANSI!)
next
messagebox("information","Blob:" + str2 + str)
return 0
end function

public function longlong uf_decrypt (blob a_blob, longlong a_len);int i = 52
int j,k,l
int size
char c
blob b2,charblob
string key_dbuserid
string key_cdate

string data

//uf_show_blob(a_blob,a_len)
uf_encrypt_decrypt(false,a_blob,a_len)
//uf_show_blob(a_blob,a_len)

string num_str
longlong num,pos
blob bkey


//string i_key_create_userid
//string i_key_cdate
//string i_key_sso_logid
//string i_key_sso_logpass

//Key File Creator  User ID
pos = 1
num_str = string(blobmid(a_blob,pos,3),EncodingANSI!)
if isnumber(num_str) then
	num = long(num_str)
	i_key_create_userid = string(blobmid(a_blob,pos + 3,num),EncodingANSI!)
else
	return -1
end if

// Key File Create Date
pos = pos + 3 + num 
num_str = string(blobmid(a_blob,pos,3),EncodingANSI!)
if isnumber(num_str) then
	num = long(num_str)
	i_key_cdate = string(blobmid(a_blob,pos + 3,num),EncodingANSI!)
else
	return -1
end if

// SSO ID
pos = pos + 3 + num 
num_str = string(blobmid(a_blob,pos,3),EncodingANSI!)
if isnumber(num_str) then
	num = long(num_str)
	i_key_sso_logid = string(blobmid(a_blob,pos + 3,num),EncodingANSI!)
else
	return -1
end if


// SSO PW
pos = pos + 3 + num 
num_str = string(blobmid(a_blob,pos,3),EncodingANSI!)
if isnumber(num_str) then
	num = long(num_str)
	i_key_sso_logpass = string(blobmid(a_blob,pos + 3,num),EncodingANSI!)
else
	return -1
end if


return 0
end function

public function longlong uf_encrypt (ref blob a_blob, string a_createid, string a_date, string a_userid, string a_passwd);string local_user
string create_date
string data

longlong key_create_userid_len
longlong key_cdate_len
longlong key_sso_logid_len
longlong key_sso_logpass_len
blob b,b2
longlong pos
longlong status
blob b_key_create_userid 
blob b_key_cdate
blob b_key_sso_logid
blob b_key_sso_logpass

i_key_create_userid = a_createid
i_key_cdate =  a_date
i_key_sso_logid =  a_userid
i_key_sso_logpass =  a_passwd



b_key_create_userid = blob(a_createid,EncodingANSI!)
b_key_cdate =  blob(a_date,EncodingANSI!)
b_key_sso_logid =  blob(a_userid,EncodingANSI!)
b_key_sso_logpass =  blob(a_passwd,EncodingANSI!)

key_create_userid_len = len(i_key_create_userid)
key_cdate_len = len(i_key_cdate)
key_sso_logid_len = len(i_key_sso_logid)
key_sso_logpass_len = len(i_key_sso_logpass) 


pos = 1
b = blob(space(1000))
string str
str = string(key_create_userid_len,'000')
status = blobedit(b,1,string(key_create_userid_len,'000'),EncodingANSI!)
pos += 3
status = blobedit(b,pos,b_key_create_userid)
pos += key_create_userid_len


status = blobedit(b,pos,string(key_cdate_len,'000'),EncodingANSI!)
pos += 3
status = blobedit(b,pos,b_key_cdate)
pos += key_cdate_len


status = blobedit(b,pos,string(key_sso_logid_len,'000'),EncodingANSI!)
pos += 3
status = blobedit(b,pos,b_key_sso_logid)
pos += key_sso_logid_len

status = blobedit(b,pos,string(key_sso_logpass_len,'000'),EncodingANSI!)
pos += 3
status = blobedit(b,pos,b_key_sso_logpass)
pos += key_sso_logpass_len

//n_cst_crypto n_cst_crypto2 
//n_cst_crypto2 = create n_cst_crypto
longlong len
b2 = blobmid(b,1,pos)
//uf_show_blob(b2,pos)
uf_encrypt_decrypt(true,b2,pos)
//uf_show_blob(b2,pos)

a_blob = b2
return pos
end function

public function longlong uf_get_error (longlong a_err, ref string a_msg);string msg
longlong count,langid = 1024
longlong status

msg = space(2000)
count = FormatMessageA( 4096,0, long(a_err),langid,msg,    2000,   0 )
if count = 0 then
   status = GetlastError()
end if

a_msg = msg
return count
end function

public function string uf_local_userid ();string str = space(200)
longlong status
longlong code
string msg
longlong count,langid = 1024
int NO_ERROR = 0 
long len =200
status = WNetGetUserA(0,str,len)
if status <> NO_ERROR then
   code = GetLastError()
	msg = space(2000)
	count = FormatMessageA( 4096,0,code,langid,msg,    2000,   0 )
	if count = 0 then
  		 status = GetlastError()
		 msg = "Code = " + string(code)
	end if
	MessageBox("Error", "Error geting User ID " + msg)
	return ""
end if

return str
end function

public function longlong uf_add_line (ref blob a_b, string a_line);longlong pos = 1
blob b
longlong status,len
string str
string slen
b = blob(space(1000))

len = len(a_line)
slen = string(len,'000')
status = blobedit(b,1,string(slen,'000'),EncodingANSI!)
pos += 3
if len > 0 then
	status = blobedit(b,pos,a_line)
	pos += len
end if

a_b = blobmid(b,1,pos)
return pos
end function

public function longlong uf_read_section (blob a_blob, ref longlong a_pos, ref blob a_section);blob b
longlong pos
longlong num
string num_str
pos = 1
num_str = string(blobmid(a_blob,a_pos,3),EncodingANSI!)
if isnumber(num_str) then
	num = long(num_str)
	a_section = blobmid(a_blob,a_pos + 3,num)
	a_pos = num + 3 + a_pos
else
	return -1
end if

return num + 3
end function

public function integer uf_read_line (blob a_b, ref longlong a_pos, ref string a_line);
blob b
longlong pos
longlong num
string num_str
pos = 1
num_str = string(blobmid(a_b,a_pos,3),EncodingANSI!)
if isnumber(num_str) then
	num = long(num_str)
	a_line = string(blobmid(a_b,a_pos + 3,num),EncodingANSI!)
	a_pos = a_pos + num + 3
else
	return -1
end if

return num
end function

public function longlong uf_find_line (blob a_section, string a_name, ref string a_value);blob section
string line
longlong len,rlen
longlong pos = 1
longlong lpos = 1
blob b
longlong nlen
nlen = len(a_name)
section = a_section
do while 1 = 1 
	len = uf_read_line(section,pos,line)
	if  len > 0 then
			  if mid(line,1,nlen + 1) = a_name + '=' then		
					a_value = mid(line,nlen + 2)	
					return len
			  end if
	else
		return -1    		
	end if
loop



return 0
end function

public function integer uf_save_blob (string a_filename, blob a_blob);int unit
longlong flen,pos,tot,bytes_write
unit = FileOpen(a_filename, StreamMode!, write!, lockwrite! ,Replace!)
flen = len(a_blob)
	
	// Write the file
pos = 1
tot = flen
do while tot > 0
		bytes_write = FileWrite(unit,a_blob)
		tot = tot - bytes_write 
		a_blob = blobmid(a_blob,bytes_write + 1)
loop
FileClose(unit)
return 0
end function

public function integer uf_write_section (ref blob a_blob, blob a_section);longlong len,pos,status
blob b


pos = 1
b = blob(space(1000))
string str
len = len(a_section)
str = string(len,'000')
status = blobedit(b,1,str,EncodingANSI!)
pos += 3
status = blobedit(b,pos,a_section)
pos +=   len

a_blob = a_blob  + blobmid(b,1,pos - 1)
return pos
end function

public function integer uf_write_line (ref blob a_blob, string a_line);
longlong len,pos,status
blob b,b2

b = blob(a_line,EncodingANSI!)

pos = 1
b2 = blob(space(1000))
string str
len = len(a_line)
str = string(len,'000')
status = blobedit(b2,1,str,EncodingANSI!)
pos += 3
status = blobedit(b2,pos,b)
pos +=   len

a_blob = a_blob  + blobmid(b2,1,pos - 1)
return pos
end function

public function integer uf_read_blob (string a_filename, ref blob a_blob);
longlong bytes_read,unit
longlong tot
blob data_temp
tot = 0
bytes_read = 1
unit = FileOpen(a_filename,StreamMode!,Read!)
if unit = -1 then
	 // f_pp_msgs("Error opening file " + a_filename)
	 return -1
end if
do while bytes_read > 0
		bytes_read = FileRead(unit,data_temp)
		if  bytes_read > 0 then
			 a_blob = a_blob + data_temp 
			 tot = tot  + bytes_read 
		end if
loop
fileclose(unit)
return tot
end function

public function integer uf_find_section (ref blob a_blob, ref blob a_section, string a_name);blob section
string line
longlong len,rlen
longlong pos = 1
longlong lpos = 1
blob b
b = a_blob
do while 1 = 1
	len = uf_read_section(b,pos,section)
	if  len > 0 then
		        lpos = 1
			  rlen = uf_read_line(section,lpos,line)
			 if rlen > 0 then
					if  line = '[' + a_name + ']' then
							a_section = section
							return len
					end if
			else
					return -1
			  end if
			  //pos = pos + rlen 
	else
	     return -1		
	end if
loop

end function

on n_encrypt.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_encrypt.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

