HA$PBExportHeader$ppc_programs.sra
forward
global type ppc_programs from application
end type
global uo_sqlca sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
u_prog_interface g_prog_interface
end variables

global type ppc_programs from application
string appname = "ppc_programs"
end type
global ppc_programs ppc_programs

on ppc_programs.create
appname="ppc_programs"
message=create message
sqlca=create uo_sqlca
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on ppc_programs.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;open(w_ppc_program_opt)
end event

