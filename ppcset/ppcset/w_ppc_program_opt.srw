HA$PBExportHeader$w_ppc_program_opt.srw
forward
global type w_ppc_program_opt from window
end type
type cb_login from commandbutton within w_ppc_program_opt
end type
type cb_close from commandbutton within w_ppc_program_opt
end type
type cbx_write_to_file from checkbox within w_ppc_program_opt
end type
type cb_1 from commandbutton within w_ppc_program_opt
end type
type cb_program from commandbutton within w_ppc_program_opt
end type
type cb_cancel from commandbutton within w_ppc_program_opt
end type
type cb_delete from commandbutton within w_ppc_program_opt
end type
type cb_add from commandbutton within w_ppc_program_opt
end type
type cb_oupdate from commandbutton within w_ppc_program_opt
end type
type dw_ppc_program_opt from datawindow within w_ppc_program_opt
end type
end forward

global type w_ppc_program_opt from window
integer x = 1074
integer y = 484
integer width = 3470
integer height = 1740
boolean titlebar = true
string title = "Program Options Version 2.3.7"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cb_login cb_login
cb_close cb_close
cbx_write_to_file cbx_write_to_file
cb_1 cb_1
cb_program cb_program
cb_cancel cb_cancel
cb_delete cb_delete
cb_add cb_add
cb_oupdate cb_oupdate
dw_ppc_program_opt dw_ppc_program_opt
end type
global w_ppc_program_opt w_ppc_program_opt

type prototypes
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long  GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
end prototypes

type variables
longlong i_row
u_prog_interface uo_prog_interface
string i_path
end variables

on w_ppc_program_opt.create
this.cb_login=create cb_login
this.cb_close=create cb_close
this.cbx_write_to_file=create cbx_write_to_file
this.cb_1=create cb_1
this.cb_program=create cb_program
this.cb_cancel=create cb_cancel
this.cb_delete=create cb_delete
this.cb_add=create cb_add
this.cb_oupdate=create cb_oupdate
this.dw_ppc_program_opt=create dw_ppc_program_opt
this.Control[]={this.cb_login,&
this.cb_close,&
this.cbx_write_to_file,&
this.cb_1,&
this.cb_program,&
this.cb_cancel,&
this.cb_delete,&
this.cb_add,&
this.cb_oupdate,&
this.dw_ppc_program_opt}
end on

on w_ppc_program_opt.destroy
destroy(this.cb_login)
destroy(this.cb_close)
destroy(this.cbx_write_to_file)
destroy(this.cb_1)
destroy(this.cb_program)
destroy(this.cb_cancel)
destroy(this.cb_delete)
destroy(this.cb_add)
destroy(this.cb_oupdate)
destroy(this.dw_ppc_program_opt)
end on

event open;longlong hMod
longlong i,j,row,next_section_pos
string name
string filename,path
string subkeys[]
string username 
string password
string server
string mail_server
string mail_to
string mail_from
string debug
string dbms
string disable_mail
string disable_log
string pbtrace_file
string pbtrace_dir
string dbparm
string prog
string log_file
ulong encrypt_type
blob b,b2
longlong len
longlong len2
ulong ulen
n_encrypt n_encrypt 
uo_prog_interface = create u_prog_interface
RegistryKeys ( "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info", subkeys )


filename = space(200)
hMod = GetModuleHandleA("")
GetModuleFileNameA(hMod,filename,200)
	

/* Get the path of the executable */
len =  len(filename)
for i = len to 1 step -1
		if mid(filename,i,1) = '\' then
			path = mid(filename,1,i)
			exit
		end if
next

string line,val
blob bsection
longlong pos,pos2
i_path = path + 'ppcset.dat'
if fileexists(i_path) = true then
                 cbx_write_to_file.checked = true 
			len = n_encrypt.uf_read_blob(i_path,b2)
			if len = -1 then
				 MessageBox('Error','Error reading the ' + path + 'ppcset.dat file')
				 return
			end if
			if n_encrypt.uf_encrypt_decrypt(false,b2,len) <> 0 then
				return
			end if
			pos = 1
			next_section_pos=1
			i = 0
			len = n_encrypt.uf_read_section(b2,next_section_pos,bsection) 	
                  do while len > 0 
				pos = pos + len
				pos2 = 1
				len2 = n_encrypt.uf_read_line(bsection,pos2,line)
				prog = mid(line,2,len2 - 2)
				
			     row = dw_ppc_program_opt.insertrow(0)
			     dw_ppc_program_opt.object.program[row] = prog
				 do while len2 > 0 
						 len2 = n_encrypt.uf_read_line(bsection,pos2,line)
						 pos = pos(line,'=')
						 name = mid(line,1,pos - 1)
						 val = mid(line,pos + 1)
						choose case (name)			
								case 'username'				
										dw_ppc_program_opt.object.database_id[row] = val
								case 'passwd'	
									dw_ppc_program_opt.object.database_password[row] =val
								case 'server'
									 dw_ppc_program_opt.object.database_server[row] =val
								case 'dbms'
									dw_ppc_program_opt.object.dbms[row] = val
								case 'mail_server'
									dw_ppc_program_opt.object.mail_server[row] = val
								case 'mail_to'
									dw_ppc_program_opt.object.mail_to[row] = val
								case 'debug'
									dw_ppc_program_opt.object.debug[row] = long(val)
								case 'disable_mail'
									dw_ppc_program_opt.object.disable_mail[row] = long(val)
								case 'disable_log'
									dw_ppc_program_opt.object.disable_log[row] = long(val)
								case 'log_file'
									dw_ppc_program_opt.object.log_file[row] = val
								case 'pbtrace_file'
									dw_ppc_program_opt.object.pbtrace_file[row] = val
								case 'pbtrace_dir'
									dw_ppc_program_opt.object.pbtrace_dir[row] = val
						            case 'dbparm'
									dw_ppc_program_opt.object.dbparm[row] = val
								end choose
						loop
						len = n_encrypt.uf_read_section(b2,next_section_pos,bsection) 	
				loop
else
		for i = 1 to upperbound(subkeys)
			prog = space(len(subkeys[i]))
			for j = 1 to len(subkeys[i])
				if mid(subkeys[i],j,1) = '/' then
					prog = replace(prog,j,1,'\')
				else
					prog = replace(prog,j,1,mid(subkeys[i],j,1))
				end if
			next
			path = "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info\" + subkeys[i]
			dw_ppc_program_opt.insertrow(0)
			dw_ppc_program_opt.object.program[i] = prog
			RegistryGet(path ,"username", username)
			dw_ppc_program_opt.object.database_id[i] = username
			
			RegistryGet(path ,"passwd",RegBinary!, b)
			RegistryGet(path ,"passwd_len",ReguLong!, ulen)
			
			if RegistryGet(path ,"encrypt_type",ReguLong!, encrypt_type) = -1 then
				encrypt_type = 0
			end if
			if ulen < 1 then
				password = ''
			else
				if encrypt_type = 1 then
					len2 = ulen
					n_encrypt.uf_encrypt_decrypt(false,b,len2)
				else
					uo_prog_interface.uf_decrypt(b,ulen)
				end if
				
				password = string(b)
			end if
			//len = len(password)
			
			dw_ppc_program_opt.setitem(i,'encrypt',encrypt_type)
			
			dw_ppc_program_opt.object.database_password[i] = password
			RegistryGet(path ,"server", server)
			dw_ppc_program_opt.object.database_server[i] = server
			
			RegistryGet(path ,"dbms", dbms)
			dw_ppc_program_opt.object.dbms[i] = dbms
			RegistryGet(path ,"mail_server", mail_server)
			dw_ppc_program_opt.object.mail_server[i] = mail_server
			RegistryGet(path,"mail_to", mail_to)
			dw_ppc_program_opt.object.mail_to[i] = mail_to
			RegistryGet(path ,"mail_from", mail_from)
			dw_ppc_program_opt.object.mail_from[i] = mail_from
			RegistryGet(path,"debug", debug)
			dw_ppc_program_opt.object.debug[i] = long(debug)
			RegistryGet(path,"disable_mail", disable_mail)
			dw_ppc_program_opt.object.disable_mail[i] = long(disable_mail)
			RegistryGet(path,"disable_log", disable_log)
			dw_ppc_program_opt.object.disable_log[i] = long(disable_log)
			RegistryGet(path ,"log_file", log_file)
			dw_ppc_program_opt.object.log_file[i] = log_file
			RegistryGet(path ,"pbtrace_file",pbtrace_file)
			dw_ppc_program_opt.object.pbtrace_file[i] = pbtrace_file
			RegistryGet(path ,"pbtrace_dir", pbtrace_dir)
			dw_ppc_program_opt.object.pbtrace_dir[i] = pbtrace_dir
			RegistryGet(path ,"dbparm", dbparm)
			dw_ppc_program_opt.object.dbparm[i] = dbparm
		next
end if
i_row = 1
dw_ppc_program_opt.selectrow(i_row,true)
end event

type cb_login from commandbutton within w_ppc_program_opt
integer x = 1006
integer y = 1496
integer width = 297
integer height = 88
integer taborder = 30
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Login"
end type

event clicked;uo_sqlca a_sqlca
string dev_role


a_sqlca  = create uo_sqlca
a_sqlca.logid = dw_ppc_program_opt.getitemstring(i_row,'database_id')
a_sqlca.userid = dw_ppc_program_opt.getitemstring(i_row,'database_id')
a_sqlca.servername = dw_ppc_program_opt.getitemstring(i_row,'database_server')
a_sqlca.logpass = dw_ppc_program_opt.getitemstring(i_row,'database_password')
a_sqlca.dbms= dw_ppc_program_opt.getitemstring(i_row,'dbms')

connect using a_sqlca;

if a_sqlca.sqlcode <> 0 then
	MessageBox("Error","Error login to the Database. Msg="  + a_sqlca.sqlerrtext)
	return
end if
dev_role = 'PWRPLANT_ROLE_DEV'
a_sqlca.uf_get_default_roles(dev_role)
a_sqlca.uf_set_role(dev_role)
if a_sqlca.sqlcode <> 0 then
	MessageBox("Error","Error assigning developer role Msg="  + a_sqlca.sqlerrtext)
	return
end if

MessageBox("Information","The login was successful. "  + a_sqlca.sqlerrtext)
disconnect using a_sqlca;
end event

type cb_close from commandbutton within w_ppc_program_opt
integer x = 3035
integer y = 1496
integer width = 297
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Close"
boolean default = true
end type

event clicked;close(parent)
end event

type cbx_write_to_file from checkbox within w_ppc_program_opt
integer x = 1874
integer y = 1508
integer width = 457
integer height = 64
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Write to File"
end type

type cb_1 from commandbutton within w_ppc_program_opt
integer x = 1330
integer y = 1496
integer width = 494
integer height = 88
integer taborder = 16
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Trace Directory"
end type

event clicked;string dir,file
int value
nca_folderbrowse nca_folderbrowse
if i_row > 0 then
	dir = nca_folderbrowse.browseforfolder(parent,"Select Trace Directory")
      dw_ppc_program_opt.setitem(i_row ,"pbtrace_dir",dir)
else
	MessageBox("Error","Please select a row.")
end if
end event

type cb_program from commandbutton within w_ppc_program_opt
integer x = 681
integer y = 1496
integer width = 297
integer height = 88
integer taborder = 6
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Program"
end type

event clicked;string path,file
int value
if i_row > 0 then
 	value = GetFileOpenName("Select File",  &
	          + path, file, "EXE",  &
	         + " Files (*.EXE),*.EXE,"  &
	        + "PBL Executable  Files (*.EXE),*.EXE")
   if value = 1 then
     dw_ppc_program_opt.object.program[i_row] = path
   end if
else
	MessageBox("Error","Please select a row.")
end if
end event

type cb_cancel from commandbutton within w_ppc_program_opt
integer x = 2368
integer y = 1496
integer width = 297
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Cancel"
end type

event clicked;close(parent)
end event

type cb_delete from commandbutton within w_ppc_program_opt
integer x = 357
integer y = 1496
integer width = 297
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Delete"
end type

event clicked;
//string prog,path
//longlong j
//prog = dw_ppc_program_opt.object.program[i_row]
//for j = 1 to len(prog)
//	if mid(prog,j,1) = '\' then
//		prog = replace(prog,j,1,'/')
//	else
//		prog = replace(prog,j,1,mid(prog,j,1))
//	end if
//next
//path = "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info\" + prog
//RegistryDelete ( path, "" )
dw_ppc_program_opt.deleterow(i_row)
i_row = 1
dw_ppc_program_opt.selectrow(i_row,true)
end event

type cb_add from commandbutton within w_ppc_program_opt
integer x = 32
integer y = 1496
integer width = 297
integer height = 88
integer taborder = 30
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Add"
end type

event clicked;longlong row

row = dw_ppc_program_opt.insertrow(0)
if row > 1 then
	row = row - 1
   dw_ppc_program_opt.object.program[row + 1] = dw_ppc_program_opt.object.program[row]
	
	dw_ppc_program_opt.object.database_id[row + 1] = dw_ppc_program_opt.object.database_id[row]
	
	dw_ppc_program_opt.object.database_password[row + 1] = dw_ppc_program_opt.object.database_password[row]
	
	dw_ppc_program_opt.object.database_server[row + 1] = dw_ppc_program_opt.object.database_server[row]
	
	dw_ppc_program_opt.object.dbms[row + 1] = dw_ppc_program_opt.object.dbms[row]
	
	dw_ppc_program_opt.object.mail_server[row + 1] = dw_ppc_program_opt.object.mail_server[row] 
	
	dw_ppc_program_opt.object.mail_to[row + 1] = dw_ppc_program_opt.object.mail_to[row]
	
	dw_ppc_program_opt.object.mail_from[row + 1] = dw_ppc_program_opt.object.mail_from[row]
	
	dw_ppc_program_opt.object.debug[row + 1] = dw_ppc_program_opt.object.debug[row]
	
	dw_ppc_program_opt.object.disable_mail[row + 1] = dw_ppc_program_opt.object.disable_mail[row]
	
	dw_ppc_program_opt.object.disable_log[row + 1] = dw_ppc_program_opt.object.disable_log[row]
end if
end event

type cb_oupdate from commandbutton within w_ppc_program_opt
integer x = 2702
integer y = 1496
integer width = 297
integer height = 88
integer taborder = 50
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Update"
boolean default = true
end type

event clicked;longlong hMod
longlong i,j
longlong len
string filename,path
string subkeys[]
string username 
string password
string server
string mail_server
string mail_to
string mail_from
string debug
string disable_mail
string disable_log
string log_file
string prog
string dbms
string pbtrace_file
string pbtrace_dir
string dbparm
longlong status 
longlong encrypt_type
n_encrypt n_encrypt
blob b,bsection


dw_ppc_program_opt.accepttext()

if cbx_write_to_file.checked = true then
	for i = 1 to dw_ppc_program_opt.rowcount()
		     bsection = blob('')
			  
			prog = lower(dw_ppc_program_opt.object.program[i])
		      n_encrypt.uf_write_line(bsection,'[' + prog +']') 			
				
			username = dw_ppc_program_opt.object.database_id[i]
			if isnull(username) then username = ' '		
			  n_encrypt.uf_write_line(bsection,'username=' + username ) 	
			 
			password = dw_ppc_program_opt.object.database_password[i]
			if isnull(password) then password = ' '
			 n_encrypt.uf_write_line(bsection,'passwd=' + password ) 	
			
			server = dw_ppc_program_opt.object.database_server[i]
			if isnull(server) then server = ' '
			 n_encrypt.uf_write_line(bsection,'server=' +server ) 	
			
			dbms = dw_ppc_program_opt.object.dbms[i]
			if isnull(dbms) then dbms = ' '
			 n_encrypt.uf_write_line(bsection,'dbms=' + dbms ) 	
			
			mail_server = dw_ppc_program_opt.object.mail_server[i]
			if isnull(mail_server) then mail_server = ' '
			 n_encrypt.uf_write_line(bsection,'mail_server=' + mail_server ) 	
			
			mail_to = dw_ppc_program_opt.object.mail_to[i]
			if isnull(mail_to) then mail_to = ' '
			 n_encrypt.uf_write_line(bsection,'mail_to=' + mail_to ) 	
			
			mail_from = dw_ppc_program_opt.object.mail_from[i]
			if isnull(mail_from) then mail_from = ' '
			 n_encrypt.uf_write_line(bsection,'mail_from=' + mail_from ) 	
			
			if isnull(dw_ppc_program_opt.object.debug[i]) then 
				debug = '0'
			else
				debug = string(dw_ppc_program_opt.object.debug[i])
			end if
			 n_encrypt.uf_write_line(bsection,'debug=' + debug) 	
			
				
			if isnull(dw_ppc_program_opt.object.disable_mail[i]) then 
				disable_mail = '0'
			else
				disable_mail = string(dw_ppc_program_opt.object.disable_mail[i])
			end if
			 n_encrypt.uf_write_line(bsection,'disable_mail =' + disable_mail ) 
			
		
			if isnull(dw_ppc_program_opt.object.disable_log[i]) then 
				disable_log  = '0'
			else
				disable_log = string(dw_ppc_program_opt.object.disable_log[i])
			end if
			 n_encrypt.uf_write_line(bsection,'disable_log=' + disable_log ) 	
			
			log_file = dw_ppc_program_opt.object.log_file[i]
			if isnull(log_file) then log_file = ' '
			 n_encrypt.uf_write_line(bsection,'log_file=' + log_file ) 	
			
			pbtrace_file = dw_ppc_program_opt.object.pbtrace_file[i] 	
			if isnull(pbtrace_file) then pbtrace_file = ' '
			 n_encrypt.uf_write_line(bsection,'pbtrace_file=' + pbtrace_file ) 	
			
			pbtrace_dir = dw_ppc_program_opt.object.pbtrace_dir[i] 
			if isnull(pbtrace_dir) then pbtrace_dir = ' '
			n_encrypt.uf_write_line(bsection,'pbtrace_dir=' + pbtrace_dir ) 	
			
			dbparm = dw_ppc_program_opt.object.dbparm[i] 
			if isnull(pbtrace_dir) then dbparm = ' '
			n_encrypt.uf_write_line(bsection,'dbparm=' +dbparm) 	
			
			n_encrypt.uf_write_section(b,bsection)
	next
	len = len(b)
	n_encrypt.uf_encrypt_decrypt(true,b,len)
	n_encrypt.uf_save_blob(i_path,b)
else
		RegistryKeys ( "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info", subkeys )
		for i = 1 to upperbound(subkeys)
				  path = "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info\"  + subkeys[i]
				  status = RegistryDelete ( path, "" )
		next
		for i = 1 to dw_ppc_program_opt.rowcount()
			prog = lower(dw_ppc_program_opt.object.program[i])
			for j = 1 to len(prog)
				if mid(prog,j,1) = '\' then
					prog = replace(prog,j,1,'/')
				else
					prog = replace(prog,j,1,mid(prog,j,1))
				end if
			next
			path = "HKEY_LOCAL_MACHINE\Software\PowerPlan\Programs\Info\" + prog
			
			
			username = dw_ppc_program_opt.object.database_id[i]
			if isnull(username) then username = ' '
			RegistrySet(path ,"username", username)
			
			
			password = dw_ppc_program_opt.object.database_password[i]
			if isnull(password) then password = ' '

			
			encrypt_type = dw_ppc_program_opt.getitemnumber(i,'encrypt')
//			if encrypt_type = 1 then
//				len = len(password)
//				n_encrypt.uf_encrypt_decrypt(true,b,len)
//				Registryset(path ,"encrypt_type", Regulong!,1)
//			else
                        b = blob(password)
				uo_prog_interface.uf_encrypt(b)
				Registryset(path ,"encrypt_type", Regulong!,0)
//			end if
			RegistrySet(path ,"passwd", RegBinary!,b)
		
			Registryset(path ,"passwd_len", Regulong!,len(password))
			
			server = dw_ppc_program_opt.object.database_server[i]
			if isnull(server) then server = ' '
			RegistrySet(path ,"server", server)
			
			dbms = dw_ppc_program_opt.object.dbms[i]
			if isnull(dbms) then dbms = ' '
			RegistrySet(path ,"dbms", dbms)
			
			mail_server = dw_ppc_program_opt.object.mail_server[i]
			if isnull(mail_server) then mail_server = ' '
			RegistrySet(path ,"mail_server", mail_server)
			
			mail_to = dw_ppc_program_opt.object.mail_to[i]
			if isnull(mail_to) then mail_to = ' '
			RegistrySet(path,"mail_to", mail_to)
			
			mail_from = dw_ppc_program_opt.object.mail_from[i]
			if isnull(mail_from) then mail_from = ' '
			RegistrySet(path ,"mail_from", mail_from)
			
			
			if isnull(dw_ppc_program_opt.object.debug[i]) then 
				debug = '0'
			else
				debug = string(dw_ppc_program_opt.object.debug[i])
			end if
			RegistrySet(path,"debug", debug)
			
			disable_mail = string(dw_ppc_program_opt.object.disable_mail[i])
			
			if isnull(dw_ppc_program_opt.object.disable_mail[i]) then 
				disable_mail = '0'
			else
				disable_mail = string(dw_ppc_program_opt.object.disable_mail[i])
			end if
			RegistrySet(path,"disable_mail", disable_mail)
			
		
			if isnull(dw_ppc_program_opt.object.disable_log[i]) then 
				disable_log  = '0'
			else
				disable_log = string(dw_ppc_program_opt.object.disable_log[i])
			end if
			RegistrySet(path,"disable_log", disable_log)
			
			log_file = dw_ppc_program_opt.object.log_file[i]
			if isnull(log_file) then log_file = ' '
			RegistrySet(path,"log_file", log_file)
			
			
			pbtrace_file = dw_ppc_program_opt.object.pbtrace_file[i] 
			if isnull(pbtrace_file) then pbtrace_file = ' '
			RegistrySet(path,"pbtrace_file", pbtrace_file)
			
			
			pbtrace_dir = dw_ppc_program_opt.object.pbtrace_dir[i] 
			if isnull(pbtrace_dir) then pbtrace_dir = ' '
			RegistrySet(path,"pbtrace_dir", pbtrace_dir)
			
			dbparm = dw_ppc_program_opt.object.dbparm[i] 
			if isnull(pbtrace_dir) then dbparm = ' '
			RegistrySet(path,"dbparm", dbparm)
		next
end if

end event

type dw_ppc_program_opt from datawindow within w_ppc_program_opt
event ue_key pbm_dwnkey
integer x = 18
integer y = 16
integer width = 3346
integer height = 1424
integer taborder = 40
string dataobject = "dw_ppc_program_opt"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event ue_key;string rc
longlong i
IF keyflags = 2 and key = KeyP!THEN
	rc = this.modify("database_password.edit.Password='no'")
	i = 2
end if
end event

event clicked;if row > 0 then
	  selectrow(i_row,false)
	 i_row = row
    selectrow(row,true)
end if
end event

