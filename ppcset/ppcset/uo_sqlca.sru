HA$PBExportHeader$uo_sqlca.sru
$PBExportComments$v10.0.7 Online logs Not Working for Novel networks
forward
global type uo_sqlca from transaction
end type
end forward

global type uo_sqlca from transaction
string dbparm = "CommitOnDisconnect=~'No~'"
event uf_callback pbm_custom02
end type
global uo_sqlca uo_sqlca

type prototypes
subroutine truncate_table( string name) RPCFUNC ALIAS FOR "truncate_table"
function  longlong analyze_table(string name ) RPCFUNC ALIAS FOR "analyze_table"
function  longlong analyze_table_pct(string name,longlong pct ) RPCFUNC ALIAS FOR "analyze_table"
function longlong alter_table(string name,string cmd, string sql ) RPCFUNC ALIAS FOR "alter_table"
FUNCTION double fopen(string path,string filename,string mode) RPCFUNC ALIAS FOR "utl_file.fopen"
SUBROUTINE fclose(longlong unitx) RPCFUNC ALIAS FOR "utl_file.fclose"
SUBROUTINE  set_sql_trace_in_session(ref double sid, ref double serial,ref int state)  RPCFUNC ALIAS FOR "sys.dbms_system.set_sql_trace_in_session"
subroutine set_sqltrace_on(int sed,int serail)  RPCFUNC ALIAS FOR "system.set_sqltrace_on"
subroutine describe_procedure (string object_name ,string reserved1 , string reserved2 ,ref double overload[] ,ref double position[],ref double level[],ref string argument_name[] ,ref double datatype[],ref double default_value[],ref double n_out[],ref double length_out[],ref double precision[],ref double scale[],ref double radixx[],ref double spare[]) RPCFUNC ALIAS FOR "sys.dbms_describe.describe_procedure"
subroutine open_file(string path,string file,string mode,ref double unit) RPCFUNC ALIAS FOR "pp_file.open_file"
SUBROUTINE read_file(double unit, ref string buf)  RPCFUNC ALIAS FOR "pp_file.read_file"
SUBROUTINE close_file(double unit)  RPCFUNC ALIAS FOR "pp_file.close_file"
subroutine pack_message(string msg) rpcfunc alias for "dbms_pipe.pack_message"
subroutine pack_message(longlong msg) rpcfunc alias for "dbms_pipe.pack_message"
function longlong send_message(string msg,longlong timeout) rpcfunc alias for "dbms_pipe.send_message"
function string unique_session_name() rpcfunc alias for "dbms_pipe.unique_session_name"
function longlong receive_message(string msg,longlong timeout) rpcfunc alias for "dbms_pipe.receive_message"
subroutine unpack_message(ref string msg) rpcfunc alias for "dbms_pipe.unpack_message"
subroutine unpack_message(ref longlong status) rpcfunc alias for "dbms_pipe.unpack_message"
function longlong submit_tax( longlong version,ref longlong code,ref string err_msg) rpcfunc alias for "submit_tax"
subroutine submit(ref longlong jobid,string what,datetime startdate,string interval) rpcfunc alias for "dbms_job.submit"
subroutine remove( longlong jobid) rpcfunc alias for "dbms_job.remove"
subroutine user_export(longlong jobid,ref string what) rpcfunc alias for  "dbms_job.user_export"
subroutine run(longlong jobid) rpcfunc alias for  "dbms_job.run"
function double get_tax_version( ) rpcfunc alias for "archive_tax.get_version"
function longlong get_tax_vparameter( ) rpcfunc alias for "archive_tax.get_vparameter"
function longlong get_plant_vparameter( ) rpcfunc alias for "archive_plant.get_vparameter"
function double get_plant_version( ) rpcfunc alias for "archive_plant.get_version"
subroutine set_context (string namespace2, string attribute ,string value ,string client,string client_id) rpcfunc alias for "dbms_session.set_context"
subroutine set_module(string mod,string action) rpcfunc alias for "DBMS_APPLICATION_INFO.SET_MODULE";
subroutine SET_CLIENT_INFO (string info)  rpcfunc alias for "DBMS_APPLICATION_INFO.SET_CLIENT_INFO";
function longlong pwrplant_admin(string cmd ,string user, ref string passwd ,string tablespace, string temp_tablespace,string user_role , string dev_role)   rpcfunc alias for "pwrplant_admin";
subroutine set_role(string role) rpcfunc alias for "dbms_session.set_role";
function longlong drop_tax_partition(string table,longlong vers) rpcfunc alias for "drop_tax_partition";
function longlong SetContext(string parm ,string val) rpcfunc alias for "audit_table_pkg.SetContext";
function longlong SetWindowContext(string winparm ,string windowtitle, string auditparm ) rpcfunc alias for "audit_table_pkg.SetWindowContext";
function longlong CreatePackage(string a_name ) rpcfunc alias for "audit_table_pkg.CreatePackage";
function string GetErrorText(longlong code ) rpcfunc alias for "audit_table_pkg.GetErrorText";
function longlong exec_sql(string sql,ref string msg) rpcfunc alias for 'archive_tax.exec_sql'
function string pp_sso_login(string keystr ) rpcfunc alias for 'pp_sso_login'
function longlong pp_send_mail(string a_fromuser,string a_smtp_userid, string a_smtp_passwd, &
          string a_subject,string a_text,string a_user,string a_server,string a_filename) rpcfunc alias for "PP_SEND_MAIL";
function string pp_get_error_msg(longlong code) rpcfunc alias for 'pp_get_error_msg';
subroutine put_line(string outstr) rpcfunc alias for "dbms_output.put_line"
subroutine get_line(ref string buf,ref int status ) rpcfunc alias for "dbms_output.get_line"
subroutine enable(longlong buflen) rpcfunc alias for "dbms_output.enable"
subroutine disable() rpcfunc alias for "dbms_output.disable"
function string pp_ldap_login(string a_user,string a_passwd,string a_host,longlong a_port ) rpcfunc alias for "pp_ldap.login"
function longlong pp_ldap_search(string a_session ,string a_class ,string attrib ,string a_ldap_path, longlong a_level ) rpcfunc alias for "pp_ldap.search"
function longlong pp_ldap_search_list(string a_session ,string a_class ,string a_ldap_path, longlong a_level ) rpcfunc alias for "pp_ldap.search_list"
function longlong pp_ldap_logout(string session)rpcfunc alias for "pp_ldap.logout"
function longlong pp_ldap_update_ad_user(string user,string pw,string server,string dn ,string server_dn ,longlong port) rpcfunc alias for "pp_ldap.update_ad_user"
function string verify_user(string a_verify_user ,string  a_verify_passwd ) rpcfunc alias for "pp_verify_user"
function string pp_ldap_getpasswd( ) rpcfunc alias for "pp_ldap.getpasswd"
function string pp_ldap_profile_passwd(string a_name ) rpcfunc alias for "pp_ldap.profile_passwd"
function  string pp_ldap_update_profile( string a_name , string a_user , string a_pw , string a_server , string a_dn , string a_server_dn ,longlong a_portr) rpcfunc alias for "pp_ldap.update_profile" 
function  longlong pp_ldap_update_user(string a_session ,string a_user ,string a_server_dn ) rpcfunc alias for "pp_ldap.update_user"
//
Function long GetEnvironmentVariable(string var,ref string str,long len)  Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long SetEnvironmentVariable(string var,string val)  Library "kernel32.dll" alias for "SetEnvironmentVariableA;Ansi"
Function long StartTrace(long hwnd,long msg,long stmt_type,long entry_type)  Library "ppctrace.dll" 
Function long EndTrace()  Library "ppctrace.dll"
Function long Reset()  Library "ppctrace.dll"
Function long GetModuleHandle(long modname) Library "KERNEL32.DLL"  alias for "GetModuleHandleA"
Function long GetModuleFileName(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
Function long LoadLibrary(string libname) Library "KERNEL32.DLL" alias for "LoadLibraryA;Ansi"
Function long GetLastError() Library "kernel32.dll"
Function long FormatMessage( long flag,long input,long err,long lang,ref string msg,long len,long arg) Library "kernel32.dll" alias for "FormatMessageA;Ansi"
end prototypes

type variables
string table_owner
window i_callback_window
string i_dev_role

boolean trace_enabled = false
boolean trace_loaded = false

/*--------------------------- User Callback Constants -----------------------*/
longlong OCI_UCBTYPE_ENTRY	= 1                          /* entry callback */
longlong OCI_UCBTYPE_EXIT	= 2                           /* exit callback */
longlong OCI_UCBTYPE_REPLACE	= 3                    /* replacement callback */


longlong OCI_FNCODE_INITIALIZE  =   1                         /* OCIInitialize */
longlong OCI_FNCODE_HANDLEALLOC = 2                          /* OCIHandleAlloc */
longlong OCI_FNCODE_HANDLEFREE = 3                            /* OCIHandleFree */
longlong OCI_FNCODE_DESCRIPTORALLOC = 4                  /* OCIDescriptorAlloc */
longlong OCI_FNCODE_DESCRIPTORFREE=  5                    /* OCIDescriptorFree */
longlong OCI_FNCODE_ENVINIT  = 6                                 /* OCIEnvInit */
longlong OCI_FNCODE_SERVERATTACH  = 7                       /* OCIServerAttach */
longlong OCI_FNCODE_SERVERDETACH =  8                       /* OCIServerDetach */
/* unused         9 */ 
longlong OCI_FNCODE_SESSIONBEGIN = 10                       /* OCISessionBegin */
longlong OCI_FNCODE_SESSIONEND =  11                          /* OCISessionEnd */
longlong OCI_FNCODE_PASSWORDCHANGE  = 12                  /* OCIPasswordChange */
longlong OCI_FNCODE_STMTPREPARE =  13                        /* OCIStmtPrepare */
                                                      /* unused       14- 16 */
longlong OCI_FNCODE_BINDDYNAMIC  = 17                        /* OCIBindDynamic */
longlong OCI_FNCODE_BINDOBJECT = 18                           /* OCIBindObject */
                                                                /* 19 unused */
longlong OCI_FNCODE_BINDARRAYOFSTRUCT =  20            /* OCIBindArrayOfStruct */
longlong OCI_FNCODE_STMTEXECUTE = 21                         /* OCIStmtExecute */
                                                             /* unused 22-24 */
longlong OCI_FNCODE_DEFINEOBJECT = 25                       /* OCIDefineObject */
longlong OCI_FNCODE_DEFINEDYNAMIC =  26                    /* OCIDefineDynamic */
longlong OCI_FNCODE_DEFINEARRAYOFSTRUCT = 27         /* OCIDefineArrayOfStruct */
longlong OCI_FNCODE_STMTFETCH =  28                            /* OCIStmtFetch */
longlong OCI_FNCODE_STMTGETBIND =  29                    /* OCIStmtGetBindInfo */
                                                            /* 30, 31 unused */
longlong OCI_FNCODE_DESCRIBEANY =  32                         /* OCIDescribeAny */
longlong OCI_FNCODE_TRANSSTART = 33                           /* OCITransStart */
longlong OCI_FNCODE_TRANSDETACH = 34                         /* OCITransDetach */
longlong OCI_FNCODE_TRANSCOMMIT = 35                         /* OCITransCommit */
                                                                /* 36 unused */
longlong OCI_FNCODE_ERRORGET =  37                              /* OCIErrorGet */
longlong OCI_FNCODE_LOBOPENFILE = 38                         /* OCILobFileOpen */
longlong OCI_FNCODE_LOBCLOSEFILE=  39                       /* OCILobFileClose */
                                             /* 40 was LOBCREATEFILE, unused */
                                         /* 41 was OCILobFileDelete, unused  */
longlong OCI_FNCODE_LOBCOPY = 42                                 /* OCILobCopy */
longlong OCI_FNCODE_LOBAPPEND = 43                             /* OCILobAppend */
longlong OCI_FNCODE_LOBERASE = 44                               /* OCILobErase */
longlong OCI_FNCODE_LOBLENGTH = 45                          /* OCILobGetLength */
longlong OCI_FNCODE_LOBTRIM = 46                                 /* OCILobTrim */
longlong OCI_FNCODE_LOBREAD  =47                                 /* OCILobRead */
longlong OCI_FNCODE_LOBWRITE = 48                               /* OCILobWrite */
                                                                /* 49 unused */
longlong OCI_FNCODE_SVCCTXBREAK =50                                /* OCIBreak */
longlong OCI_FNCODE_SERVERVERSION = 51                     /* OCIServerVersion */
/* unused 52, 53 */
longlong OCI_FNCODE_ATTRGET = 54                                  /* OCIAttrGet */
longlong OCI_FNCODE_ATTRSET = 55                                  /* OCIAttrSet */
longlong OCI_FNCODE_PARAMSET =56                                /* OCIParamSet */
longlong OCI_FNCODE_PARAMGET =57                                /* OCIParamGet */
longlong OCI_FNCODE_STMTGETPIECEINFO  = 58              /* OCIStmtGetPieceInfo */
longlong OCI_FNCODE_LDATOSVCCTX =59                          /* OCILdaToSvcCtx */
                                                                /* 60 unused */
longlong OCI_FNCODE_STMTSETPIECEINFO   =61              /* OCIStmtSetPieceInfo */
longlong OCI_FNCODE_TRANSFORGET =62                          /* OCITransForget */
longlong OCI_FNCODE_TRANSPREPARE = 63                        /* OCITransPrepare */
longlong OCI_FNCODE_TRANSROLLBACK = 64                     /* OCITransRollback */
longlong OCI_FNCODE_DEFINEBYPOS =65                          /* OCIDefineByPos */
longlong OCI_FNCODE_BINDBYPOS =66                              /* OCIBindByPos */
longlong OCI_FNCODE_BINDBYNAME= 67                            /* OCIBindByName */
longlong OCI_FNCODE_LOBASSIGN  =68                             /* OCILobAssign */
longlong OCI_FNCODE_LOBISEQUAL = 69                           /* OCILobIsEqual */
longlong OCI_FNCODE_LOBISINIT  =70                      /* OCILobLocatorIsInit */
/* 71 was lob locator size in beta2 */
longlong OCI_FNCODE_LOBENABLEBUFFERING = 71           /* OCILobEnableBuffering */
longlong OCI_FNCODE_LOBCHARSETID = 72                       /* OCILobCharSetID */
longlong OCI_FNCODE_LOBCHARSETFORM = 73                   /* OCILobCharSetForm */
longlong OCI_FNCODE_LOBFILESETNAME  =74                   /* OCILobFileSetName */
longlong OCI_FNCODE_LOBFILEGETNAME = 75                   /* OCILobFileGetName */
longlong OCI_FNCODE_LOGON =76                                      /* OCILogon */
longlong OCI_FNCODE_LOGOFF =77                                    /* OCILogoff */
longlong OCI_FNCODE_LOBDISABLEBUFFERING =78          /* OCILobDisableBuffering */
longlong OCI_FNCODE_LOBFLUSHBUFFER =79                    /* OCILobFlushBuffer */
longlong OCI_FNCODE_LOBLOADFROMFILE =80                  /* OCILobLoadFromFile */

longlong OCI_FNCODE_LOBOPEN  =81                                 /* OCILobOpen */
longlong OCI_FNCODE_LOBCLOSE  =82                               /* OCILobClose */
longlong OCI_FNCODE_LOBISOPEN  =83                             /* OCILobIsOpen */
longlong OCI_FNCODE_LOBFILEISOPEN = 84                     /* OCILobFileIsOpen */
longlong OCI_FNCODE_LOBFILEEXISTS = 85                     /* OCILobFileExists */
longlong OCI_FNCODE_LOBFILECLOSEALL = 86                 /* OCILobFileCloseAll */
longlong OCI_FNCODE_LOBCREATETEMP = 87                /* OCILobCreateTemporary */
longlong OCI_FNCODE_LOBFREETEMP = 88                    /* OCILobFreeTemporary */
longlong OCI_FNCODE_LOBISTEMP= 89                        /* OCILobIsTemporary */

longlong OCI_FNCODE_AQENQ = 90                                     /* OCIAQEnq */
longlong OCI_FNCODE_AQDEQ = 91                                     /* OCIAQDeq */
longlong OCI_FNCODE_RESET = 92                                     /* OCIReset */
longlong OCI_FNCODE_SVCCTXTOLDA = 93                         /* OCISvcCtxToLda */
longlong OCI_FNCODE_LOBLOCATORASSIGN =94                /* OCILobLocatorAssign */

longlong OCI_FNCODE_UBINDBYNAME =95

longlong OCI_FNCODE_AQLISTEN = 96                               /* OCIAQListen */

longlong OCI_FNCODE_SVC2HST =97                                    /* reserved */
longlong OCI_FNCODE_SVCRH  = 98                                    /* reserved */
                           /* 97 and 98 are reserved for Oracle internal use */

longlong OCI_FNCODE_TRANSMULTIPREPARE  = 99            /* OCITransMultiPrepare */

longlong OCI_FNCODE_CPOOLCREATE  =100               /* OCIConnectionPoolCreate */
longlong OCI_FNCODE_CPOOLDESTROY =101              /* OCIConnectionPoolDestroy */
longlong OCI_FNCODE_LOGON2 =102                                   /* OCILogon2 */
longlong OCI_FNCODE_ROWIDTOCHAR  =103                        /* OCIRowidToChar */

longlong OCI_FNCODE_SPOOLCREATE  =104                  /* OCISessionPoolCreate */
longlong OCI_FNCODE_SPOOLDESTROY =105                 /* OCISessionPoolDestroy */
longlong OCI_FNCODE_SESSIONGET   =106                         /* OCISessionGet */
longlong OCI_FNCODE_SESSIONRELEASE =107                   /* OCISessionRelease */
longlong OCI_FNCODE_STMTPREPARE2 =108                       /* OCIStmtPrepare2 */
longlong OCI_FNCODE_STMTRELEASE =109                         /* OCIStmtRelease */

longlong OCI_FNCODE_MAXFCN =109                   /* maximum OCI function code */

end variables

forward prototypes
public function integer wf_create_truncate ()
public function string uf_get_default_roles (string a_dev_role)
public function longlong uf_set_role (string a_dev_role)
public function longlong uf_get_dn (ref string a_dn, string a_ad_server)
public function integer uf_start_error_trace (longlong a_hwnd)
public function integer uf_load_library (string a_lib)
public function integer uf_start_trace (integer a_hwnd)
public function longlong uf_stop_trace ()
public function integer uf_enable_trace ()
public function longlong uf_start_error_callback ()
end prototypes

event uf_callback;longlong errno

errno = lparam
if wparam = 1 and lparam <> 0  then
   this.SQLErrText	= 'test'
end if
return 0
end event

public function integer wf_create_truncate ();string sql
		
sql =	     " create or replace procedure truncate_table(  " + &  
	     "      table_name   in varchar2) AS  " + &  
	     " rows integer;  " + &  
	     " counts number;  " + &  
	     " user_cursor integer;  " + &  
	     " sql_stmt long(4000);  " + &  
	     " status     long(2000);  " + &  
	     "   " + &  
	     "   begin  " + &  
	     " dbms_output.enable (200000);   /* enable the output package */  " + &  
	     " status := 'Enabled dbms_output.';  " + &  
	     " dbms_output.put_line (status);   " + &  
	     "   " + &  
	     " /* Truncate table */     " + &  
	     "   " + &  
	     " sql_stmt := 'truncate table ' ||  table_name ;  " + &  
	     " dbms_output.put_line (sql_stmt);   " + &      
	     " user_cursor := dbms_sql.open_cursor;  " + &  
        " dbms_sql.parse(user_cursor,sql_stmt,dbms_sql.v7);   " + &         
        " dbms_output.put_line ('execute');     " + &    
        " rows := dbms_sql.execute(user_cursor);  " + &  
        " dbms_sql.close_cursor(user_cursor);  " + &  
        " end;  " 
execute immediate :sql;

sql = 'grant execute on truncate_table to pwrplant_role_dev'
execute immediate :sql;

sql = 'create public synonym truncate_table for pwrplant.truncate_table'
execute immediate :sql;

return 0
end function

public function string uf_get_default_roles (string a_dev_role);string role,default_role,roles

roles = ''
if a_dev_role = '' then
	a_dev_role = 'PWRPLANT_ROLE_DEV'
end if
DECLARE role_cur  CURSOR  for SELECT granted_role,default_role 
           FROM user_role_privs where username <> 'PUBLIC' and granted_role <> upper(:a_dev_role) using this;
OPEN role_cur ;
if this.SQLCode <> 0 then
	return roles
end if
FETCH role_cur INTO :role,:default_role ;
do while (this.SQLCode = 0) 
  if(this.SQLCode = 0 and default_role = 'YES') then
			 roles = roles + role + ','
  end if
  FETCH role_cur INTO :role,:default_role ;
  //if(SQLCA.SQLCode = 0 and default_role = 'YES') then
	//		 roles = roles + ','
  //end if
loop
close role_cur;
return roles
end function

public function longlong uf_set_role (string a_dev_role);string key,msg
string roles,cmd,schema,sqls
longlong code,errcode
string passwd = space(1024)
string all_roles
string tablespace = ''
string temp_tablespace = ''
string user_role = ''
string dev_role= ''
string output = space(1024)
string user_dn,ad_server
string ad_passwd

cmd = 'getrole'
roles = uf_get_default_roles(a_dev_role)
key = 'PWRPLANT42PPC'
code = 0
passwd = space(1024)
ad_server = ' '
user_dn = ' ' 

if  isnull(this.userid) or this.userid = '' then goto no_domain
 
 if  this.userid <> this.logid then
 if uf_get_dn(user_dn,ad_server)  = -1 then
   MessageBox("Error","Error, no domain found! " )
   return -1
 end if
end if
 
no_domain:


ad_passwd = this.dbpass
this.pwrplant_admin(cmd,key,passwd,ad_passwd,user_dn,ad_server,'')
if this.sqlcode <> 0 then
	  msg = this.sqlerrtext + " Code=" + string(this.sqldbcode)
	  code = -1
else
	 all_roles =  roles  + a_dev_role 
    this.set_role( all_roles + " identified by " + passwd )
	 if this.sqlcode <> 0 then
	     msg = this.sqlerrtext + " Code=" + string(this.sqldbcode)
	     code = -1
    end if
end if
if code <> 0 then // try it the old way
	all_roles =  roles  + a_dev_role 
	cmd = "begin dbms_session.set_role(~'" + all_roles + " identified by Tr9uI22k~'); end;"
	execute immediate :cmd using this;
	if this.sqlcode = -1 then 
        messagebox("ORACLE Error Setting Roles ", "Error dbms_session.set_role = " + this.SQLErrText + &
		                         "~r~n ppc_set_role code=" + msg)
		   
		  return this.sqldbcode
	end if
//	schema = ProfileString(i_ini_file,   i_database, "SCHEMA", "")
//	if schema <> "" then   
//		sqls = "ALTER SESSION SET CURRENT_SCHEMA=" + schema; 
//      execute immediate :sqls;
//		if this.sqlcode = -1 then 
//        messagebox("ORACLE Error Alter Session Schema ", this.SQLErrText)
//	   end if
//  end if
	
end if

return 0
end function

public function longlong uf_get_dn (ref string a_dn, string a_ad_server);
//n_oleobject ole_ad
//string site 
//longlong result,i
//
//
//ole_ad = CREATE n_oleobject
//
//result = ole_ad.ConnectToNewObject("ADSystemInfo") 
//if result = 0 then
//
//			 site = ole_ad.SiteName
//			 if isnull(ole_ad.UserName) then // No domain
//		        a_dn = ' '
//			     a_ad_server = ' '
//				  return -1
//			 end if
//		 a_dn = ole_ad.UserName
//		 a_ad_server = ole_ad.GetAnyDCName()
//		 return 1
//	
//end if
//a_dn = ' '
return -1
end function

public function integer uf_start_error_trace (longlong a_hwnd);
longlong code
if trace_enabled = true then
	
	code = starttrace(long(a_hwnd),1025,OCI_FNCODE_STMTPREPARE,OCI_UCBTYPE_EXIT)
	code = starttrace(long(a_hwnd),1025,OCI_FNCODE_STMTEXECUTE,OCI_UCBTYPE_EXIT)
	code = starttrace(long(a_hwnd),1025,OCI_FNCODE_STMTFETCH,OCI_UCBTYPE_EXIT)

	return 1
else
	return -1
end if
return 1
end function

public function integer uf_load_library (string a_lib);string ini_file,str
string path
string err
string exe_path
longlong ret
integer i
integer str_len
integer not_found = 1
environment env
longlong mod
longlong status,len

if  trace_loaded = true then
	return 1
end if

exe_path=''
mod =  GetModuleHandle(0)
if mod <> 0 then
	path = space(601)
	status = GetModuleFileName(mod,path,600)
	if status <> 0 then
		len = status
		for i = len to 1 step -1
			if mid(path,i,1) = '\' then
				exe_path = mid(path,1,i)	
				exit
			end if
		next
	end if
else
	return -1
end if

 if FileExists(exe_path + a_lib) = false then
	  return -1
 end if
 str = exe_path + a_lib
 // load_lib:
  //ret = uo_winapi.uf_loadLibrary(str)
  choose case ret
        case 0
           	err = 'System was out of memory, executable file was corrupt, or relocations ' + &
                  'were invalid.' 
	     case 2
            err = 'File was not found.' 
        case 3	
            err = 'Path was not found.' 
        case 5	
            err = 'Attempt was made to dynamically link to a task, or there ' + &
                  'was a sharing or network-protection error.' 
        case 6	
            err = 'Library required separate data segments for each task.' 
        case 8	
            err = 'There was insufficient memory to start the application.' 
        case 10	
            err = 'Windows version was incorrect.' 
        case 11	
            err = 'Executable file was invalid. Either it was not a ' + &
                   'Windows application or there was an error in the .EXE image.' 
        case 12	
            err = 'Application was designed for a different operating system.' 
        case 13	
            err = 'Application was designed for MS-DOS 4.0.' 
        case 14	
            err = 'Type of executable file was unknown.' 
        case 15	
            err = 'Attempt was made to load a real-mode application (developed for an ' + &
                  'earlier version of Windows).' 
        case 16	
            err = 'Attempt was made to load a second instance of an executable file containing ' + &
                  'multiple data segments that were not marked read-only.' 
        case 19	
            err = 'Attempt was made to load a compressed executable file. The file must ' + &
                  'be decompressed before it can be loaded.' 
        case 20	
            err = 'Dynamic-link library (DLL) file was invalid. One of the DLLs required ' + &
                  'to run this application was corrupt.' 
        case 21	
            err = 'Application requires Microsoft Windows 32-bit extensions. '
        case is > 32 
			trace_loaded = true
            return 1
        case is < 0
			 trace_loaded = true
            return 1 
        case else
            err = 'Unknown Error Code = ' + string(ret)
     end choose
MessageBox("Error",err)
return -1  


return 0
end function

public function integer uf_start_trace (integer a_hwnd);longlong code
if trace_enabled = true then
	code = StartTrace(a_hwnd,1024,OCI_FNCODE_STMTPREPARE,OCI_UCBTYPE_ENTRY)
	code = StartTrace(a_hwnd,1024,OCI_FNCODE_STMTEXECUTE,OCI_UCBTYPE_EXIT)
	if code <> 0 then
		//code = GetLastError()
		MessageBox("Starting Trace","Code = " + string(code))
	end if
	return 1
else
	return -1
end if
end function

public function longlong uf_stop_trace ();if trace_enabled = true then
	return endtrace()
else
	return -1
end if
end function

public function integer uf_enable_trace ();string var,val
var = 'ORA_OCI_UCBPKG'
val = 'ppctrace'
if lower(mid(sqlca.dbms,1,3)) = 'o73' then
	MessageBox("Trace Error","Trace does not work with o73.")
	return -1
end if
	
if uf_load_library('ppctrace.dll') = -1 then
	MessageBox("Trace Error","Failed to load library ppctrace.dll.")
	return -1
end if
Reset()
trace_enabled=true
SetEnvironmentVariable(var,val)

return 0
end function

public function longlong uf_start_error_callback ();longlong hwnd

//hwnd = handle(w_top_frame)
uf_start_error_trace(hwnd)

return 0
end function

on uo_sqlca.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_sqlca.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

