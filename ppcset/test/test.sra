HA$PBExportHeader$test.sra
forward
global type test from application
end type
global uo_sqlca sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
u_prog_interface g_prog_interface
end variables

global type test from application
string appname = "test"
end type
global test test

type prototypes

end prototypes

type variables

end variables

on test.create
appname="test"
message=create message
sqlca=create uo_sqlca
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on test.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;string str
str = commandparm()
messagebox("test",str)

g_prog_interface = create  u_prog_interface

// Get the database interface parameters for this program
g_prog_interface.uf_get_prog_params("")


sqlca.dbms = g_prog_interface.i_dbms
sqlca.servername = g_prog_interface.i_server
sqlca.logid= g_prog_interface.i_username
sqlca.logpass = g_prog_interface.i_password

connect;

if sqlca.sqlcode <> 0 then
	g_prog_interface.uf_write_log(sqlca.sqlerrtext)
end if




open(w_test)

end event

event close;g_prog_interface.uf_exit(0)

end event

