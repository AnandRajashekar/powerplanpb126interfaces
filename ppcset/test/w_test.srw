HA$PBExportHeader$w_test.srw
forward
global type w_test from Window
end type
type st_3 from statictext within w_test
end type
type st_command from statictext within w_test
end type
type st_passwd from statictext within w_test
end type
type st_id from statictext within w_test
end type
type st_2 from statictext within w_test
end type
type st_1 from statictext within w_test
end type
type cb_1 from commandbutton within w_test
end type
end forward

global type w_test from Window
int X=1075
int Y=477
int Width=1646
int Height=841
boolean TitleBar=true
string Title="Test Window"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
st_3 st_3
st_command st_command
st_passwd st_passwd
st_id st_id
st_2 st_2
st_1 st_1
cb_1 cb_1
end type
global w_test w_test

on w_test.create
this.st_3=create st_3
this.st_command=create st_command
this.st_passwd=create st_passwd
this.st_id=create st_id
this.st_2=create st_2
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={ this.st_3,&
this.st_command,&
this.st_passwd,&
this.st_id,&
this.st_2,&
this.st_1,&
this.cb_1}
end on

on w_test.destroy
destroy(this.st_3)
destroy(this.st_command)
destroy(this.st_passwd)
destroy(this.st_id)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_1)
end on

event open;st_passwd.text = g_prog_interface.i_password

st_id.text = g_prog_interface.i_username

st_command.text = commandparm()
end event

type st_3 from statictext within w_test
int X=78
int Y=433
int Width=417
int Height=77
boolean Enabled=false
string Text="Command"
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_command from statictext within w_test
int X=613
int Y=421
int Width=709
int Height=85
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_passwd from statictext within w_test
int X=613
int Y=309
int Width=709
int Height=77
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_id from statictext within w_test
int X=613
int Y=205
int Width=709
int Height=77
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_test
int X=69
int Y=309
int Width=417
int Height=77
boolean Enabled=false
string Text="Password"
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_test
int X=238
int Y=205
int Width=247
int Height=77
boolean Enabled=false
string Text="ID "
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_test
int X=1226
int Y=629
int Width=270
int Height=85
int TabOrder=1
string Text="&Close"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;


g_prog_interface.uf_write_log("this is a test")



close(parent)
end event

