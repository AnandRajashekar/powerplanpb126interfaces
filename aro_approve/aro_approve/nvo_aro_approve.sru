HA$PBExportHeader$nvo_aro_approve.sru
forward
global type nvo_aro_approve from nonvisualobject
end type
end forward

global type nvo_aro_approve from nonvisualobject
end type
global nvo_aro_approve nvo_aro_approve

forward prototypes
public function longlong uf_approve_for_company (longlong a_company_id, datetime a_month)
public function boolean uf_aro_approved (longlong a_company_id, datetime a_month)
public function boolean uf_next_month_open (longlong a_company_id, datetime a_month)
public function boolean uf_aro_calculated (longlong a_company_id, datetime a_month)
public function longlong uf_update_aro_approved (longlong a_company_id, datetime a_month)
end prototypes

public function longlong uf_approve_for_company (longlong a_company_id, datetime a_month);/*********************************************************************
*
*	uf_approve_for_company(): Runs ARO Approval for a given company/month
*									No commits or rollbacks
*
*	Parameters: longlong a_company_id: The company to process
*					datetime a_month: The month to process
*	
*	Returns: 1 Success, -1 Failure
*
**********************************************************************/

longlong rtn, pp_stat_id
string comp_descr, rtn_str
nvo_aro_reg reg_aro_engine

// Log some info
f_pp_msgs("Company_id: " + string(a_company_id)+ ' ' + string(now()))
g_stats.set_met_start("ARO Approval for Company")

// Validate ARO has already been calculated this month
if not uf_aro_calculated(a_company_id, a_month) then
	f_pp_msgs("The ARO results must be calculated in order to be approved.")
	return -1
end if

// Validate ARO hasn't already been approved this month
if uf_aro_approved(a_company_id, a_month) then
	f_pp_msgs("The ARO results have already been approved.")
	return -1
end if

// Validate next month is open
if not uf_next_month_open(a_company_id, a_month) then
	f_pp_msgs("Next month must be open before approving the ARO results.")
	return -1
end if

select description  
into :comp_descr 
from company 
where company_id = :a_company_id;
f_pp_msgs("")
f_pp_msgs("ARO Approval Processing Started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))

// Start Statistics
pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "ARO Approval: " + comp_descr)

// Approve accretion
f_pp_msgs( "    Creating Journal Entries..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Approval: Creating Journal Entries")
rtn = f_aro_approve_accretion(a_company_id, a_month)
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error during ARO Approval function")
	return -1
end if
g_stats.set_met_end("ARO Approval: Creating Journal Entries")

// Approve reg entries
f_pp_msgs( "     Approving Reg Entries..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Approval: Approving Reg Entries")
rtn = f_reg_entries_approve(a_company_id, a_month, 'ARO')
if rtn <> 1 or isnull(rtn) then
	f_pp_msgs("Error During Approving of Reg Entries ARO's")
	return -1
end if
g_stats.set_met_end("ARO Approval: Approving Reg Entries")

// Approved regulated ARO's
f_pp_msgs( "     Approving Regulated ARO's..."+ ' ' + string(now()))
g_stats.set_met_start("ARO Approval: Approving Regulated ARO's")
reg_aro_engine = create nvo_aro_reg
rtn_str = reg_aro_engine.uf_db_approve(a_company_id, a_month)
if rtn_str <> 'OK' or isnull(rtn_str) then
	f_pp_msgs("Error During Approving of Regulated ARO's")
	return -1
else
	DESTROY reg_aro_engine
end if
g_stats.set_met_end("ARO Approval: Approving Regulated ARO's")

// Update ARO approved on CPR Control
g_stats.set_met_start("ARO Approval: Update CPR Control")
rtn = uf_update_aro_approved(a_company_id, a_month)
if rtn <> 1 then
	return -1
end if
g_stats.set_met_end("ARO Approval: Update CPR Control")

// End Statistics
if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
g_stats.set_met_end("ARO Calc for Company")

f_pp_msgs("ARO Processing Completed for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
f_pp_msgs("")

return 1
end function

public function boolean uf_aro_approved (longlong a_company_id, datetime a_month);date aro_approved

select aro_approved
into :aro_approved
from cpr_control
where company_id = :a_company_id
and accounting_month = :a_month;

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Checking ARO Approval Status: " + sqlca.sqlErrText)
	return false
end if

if isNull(aro_approved) then
	return false
else
	return true
end if
end function

public function boolean uf_next_month_open (longlong a_company_id, datetime a_month);longlong check

select count(1)
into :check
from cpr_control
where company_id = :a_company_id
and accounting_month = add_months(:a_month, 1);

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Checking next month in CPR Control: " + sqlca.sqlErrText)
	return false
end if

if check = 1 then
	return true
else 
	return false
end if
end function

public function boolean uf_aro_calculated (longlong a_company_id, datetime a_month);date aro_calculated

select aro_calculated
into :aro_calculated
from cpr_control
where company_id = :a_company_id
and accounting_month = :a_month;

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Checking ARO Calculation Status: " + sqlca.sqlErrText)
	return false
end if

if isNull(aro_calculated) then
	return false
else
	return true
end if
end function

public function longlong uf_update_aro_approved (longlong a_company_id, datetime a_month);update cpr_control
set aro_approved = sysdate
where company_id = :a_company_id
and accounting_month = :a_month;

if sqlca.sqlcode <> 0 then
	f_pp_msgs("ERROR: Updating aro approved on cpr control: " + sqlca.sqlerrtext)
	return -1
end if

return 1
end function

on nvo_aro_approve.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_aro_approve.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

