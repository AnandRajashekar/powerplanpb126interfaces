HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string   i_company[], i_company_field
longlong 		i_start_record, i_max_month, i_open_month, i_num_comps, i_min_month
longlong	i_mn_to_process, i_min_month_number, i_max_month_number

longlong i_min_commitment_id_loaded, i_min_commitment_id

uo_ds_top i_ds_detail_tables
longlong	i_num_detail_tables

uo_ds_top i_ds_cc_errors, i_ds_trans_clause, i_ds_trans_tables, i_ds_cwip_mapping, i_ds_audit
decimal {2} i_amount_to_process
string i_amount_types, i_debug, i_wo_field, i_obey_approval
boolean i_kickouts
longlong i_records_to_process

string i_exe_name = 'cr_to_commitments.exe'
end variables

forward prototypes
public function longlong uf_balancing ()
public function longlong uf_validate_pre_load ()
public function longlong uf_company ()
public function longlong uf_read ()
public function integer uf_validate_pre_load_update (longlong a_source_id, string a_table_name)
public function longlong uf_cwip_charge_status (longlong a_type)
public function longlong uf_insert_stg_translate ()
public function longlong uf_insert_stg_translate2 (longlong a_source, string a_table)
public function integer uf_update_charge_id ()
public function longlong uf_insert_final ()
public function integer uf_insert_stg_sum ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_balancing ();//*********************************************************************************************************************
//
//  User Object Function  :  uf_balancing
//
//  Description  :  Balance the cr_to_comm_translate to cr_to_comm_sum
//
//*********************************************************************************************************************
decimal {2} ps1, temp1

f_pp_msgs( "Balancing cr_commitments_translate to cr_commitments_sum at " + string(now()))

ps1   = 0
temp1 = 0

select sum(amount) into :ps1 from cr_commitments_translate;

select sum(amount) into :temp1 from cr_commitments_sum;

if isnull(ps1)   then ps1   = 0
if isnull(temp1) then temp1 = 0

if ps1 <> temp1 or ps1 <> i_amount_to_process then
	f_pp_msgs(" ")
	f_pp_msgs( "  ERROR:  cr_commitments_translate and cr_commitments_sum are out of balance.")
	f_pp_msgs("CR = " + string(i_amount_to_process) + ", cr_commitments_translate = " + string(ps1) + ", cr_commitments_sum = " + string(temp1))
	f_pp_msgs( "  The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if

f_pp_msgs( "cr_commitments_translate and cr_commitments_sum are in balance.")

return 1
end function

public function longlong uf_validate_pre_load ();// I need to loop over this as well and make sure it I validate the
// detail tables (based off of source_id in the control table)
string table_name
longlong i, ret, source_id, needs_processing

ret = uf_validate_pre_load_update(0, 'cr_cost_repository')
if ret = -1 then
	return -1
end if

for i = 1 to i_num_detail_tables
	table_name = i_ds_detail_tables.getitemstring(i,'table_name')
	source_id = i_ds_detail_tables.getitemnumber(i,'source_id')
		
	// If we didn't find records to process above, no need to do any of the extra processing (validations, inserts etc)
	needs_processing = i_ds_detail_tables.getitemnumber(i, 'needs_processing')
	if needs_processing = 0 then continue
	
	ret = uf_validate_pre_load_update(source_id, table_name)
	if ret = -1 then
		return -1
	end if
next

return 1
end function

public function longlong uf_company ();//*********************************************************************************************************************
//
//  User Object Function  :  uf_company
//
//  Description  :  Get the distinct list of companies (entities) from CR_COST_REPOSITORY.  Put them
//                  into an array.  They will be translated later to company_ids for the rebuild of the 
//						  summary tables.
//
//*********************************************************************************************************************
string sqls
longlong i, num_rows, j
boolean comp_exists
any results[]

// ### 6908:  Missing equals sign in the SQL
sqls = &
	"select distinct " + i_company_field +&
	" from cr_cost_repository " +&
	" where cwip_charge_status in (1, 977) " + &
	" and month_number = " + string(i_mn_to_process) + &
	" and amount_type in (" + i_amount_types + ")"
 
f_get_column(results,sqls)

num_rows = upperbound(results)

for i = 1 to num_rows
	comp_exists = false
	
	// loop over the companies and check to see if the company is already in the array
	for j = 1 to i_num_comps
		if trim(results[i]) = trim(i_company[j]) then
			comp_exists = true
			exit
		end if
	next
	if comp_exists = false then
		i_num_comps++
		i_company[i_num_comps] = results[i]	
	end if
next

return 1
end function

public function longlong uf_read ();//********************************************************************************************************************
//
//  User Object Function  :  uf_read
//
//  Description  :  The "driving" function of the charge interface.  Some code exists in here and some other
//                  user object functions are called to perform certain processing.
//
//********************************************************************************************************************
longlong counter, counter2, rtn,ct,gl_ct, num_months, i, success, num_to_proc, ana_ret
string sqls, interface_type, mn_sqls, analyze_cwip
any results[]

i_kickouts = false

//*****************************************************************************************
//   CHECK THE DEBUG VARIABLE.
//*****************************************************************************************
setnull(i_debug)
select upper(trim(control_value)) into :i_debug from cr_system_control
 where upper(trim(control_name)) = 'CR TO COMMITMENTS EXE - DEBUG';
if isnull(i_debug) or i_debug = "" then i_debug = "NO"

if i_debug = "YES" then
	f_pp_msgs("i_debug = 'YES'")
	f_pp_msgs("  ")
end if


//*****************************************************************************************
//   ### 6202: Amount Type System Control
//*****************************************************************************************
select control_value
	into :i_amount_types
	from cr_system_control 
	where upper(control_name) = 'CR TO COMMITMENTS: AMOUNT TYPES';
if isnull(i_amount_types) or i_amount_types = '' then i_amount_types = '4'

if i_debug = "YES" then
	f_pp_msgs("i_amount_types = '" + i_amount_types + "'")
end if

//*****************************************************************************************
//   Company / Work Order Fields
//*****************************************************************************************
select upper(trim(control_value)) into :i_company_field from cr_system_control
 where upper(trim(control_name)) = 'COMPANY FIELD';
i_company_field = f_cr_clean_string(i_company_field)
if isnull(i_company_field) or i_company_field = "" then i_company_field = "NO_COMPANY_SYSTEM_SWITCH"

select upper(trim(control_value)) into :i_wo_field from cr_system_control
where upper(trim(control_name)) = 'WORK ORDER FIELD';
i_wo_field = f_cr_clean_string(i_wo_field)
if isnull(i_wo_field) or i_wo_field = "" then i_wo_field = "NO_WO_SYSTEM_SWITCH"

if i_debug = "YES" then
	f_pp_msgs("i_company_field = '" + i_company_field + "'")
	f_pp_msgs("i_wo_field = '" + i_wo_field + "'")
end if
i_num_comps = 0


////****************************************************************************
////  EVALUATE APPROVALS FOR POSTING TO CR_TO_COMMITMENTS:
////****************************************************************************
//setnull(i_obey_approval)
//select upper(trim(control_value)) into :i_obey_approval from cr_system_control
//where upper(trim(control_name)) = 'CWIP CHARGE - OBEY APPROVAL';
//if isnull(i_obey_approval) or i_obey_approval = "" then
//	i_obey_approval = "NO"
//end if
//
//if i_debug = "YES" then
//	f_pp_msgs("i_obey_approval = '" + i_obey_approval + "'")
//end if
i_obey_approval = 'NO'


//****************************************************************************
//  Get the max and min month_numbers for the summary re-builds below ...
//	### 7025:  New controls for start and end month number for commitments
//****************************************************************************
i_min_month_number = 0
i_max_month_number = 0

i_max_month = 0
select control_value into :i_max_month
 from cr_alloc_system_control where lower(control_name) = 'cr to commitments end month';
 
if i_max_month < 200400 or isnull(i_max_month) then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: getting current open month: " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs("  ")
	rollback;
	return -1
end if

i_min_month = 0
select control_value into :i_min_month
 from cr_alloc_system_control where lower(control_name) = 'cr to commitments start month';
if isnull(i_min_month) then i_min_month = 190001

if i_debug = "YES" then
	f_pp_msgs("i_min_month = '" + string(i_min_month) + "'")
	f_pp_msgs("i_max_month = '" + string(i_max_month) + "'")
end if

//****************************************************************************
//  The detail posting setup in cr_to_comm_detail_tables allows for multiple records by table as the 
//		primary key is table_name and gl_journal_category.  This causes bad processing because of
//		the way the logic is written below.
//
//	Audit that here, and if they have 2 records, force them to update it before continuing.  This is normally
//		config done by PPC anyway.
//
//	Bad Data example:
//		TABLE NAME		GL JOURNAL_CATEGORY
//		1						'AP'
//		1						'INVOICES'
//		2						 
//
//	Good Data example:
//		TABLE NAME		GL JOURNAL_CATEGORY
//		1						'AP','INVOICES'
//		2						 
//****************************************************************************
sqls = 'select table_name '
sqls += 'from cr_to_comm_detail_tables '
sqls += 'group by table_name ' 
sqls += 'having count(*) > 1'

f_get_column(results[],sqls)

if upperbound(results) > 0 then
	f_pp_msgs(" ")
	f_pp_msgs("The following sources have multiple records in cr_to_comm_detail_tables!")
	f_pp_msgs("Please consolidate these to a single record before continuing:")
	for i = 1 to upperbound(results)
		f_pp_msgs(string(results[i]))
	next
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if

//****************************************************************************
//  Initialize datastores...
//****************************************************************************
// we want to loop over any detail sources.
sqls = "select a.table_name source_id, lower(b.table_name) table_name, a.gl_jrnl_category gl_jrnl_category, 0 needs_processing "
sqls += " from cr_to_comm_detail_tables a, cr_sources b where a.table_name = b.source_id "
i_ds_detail_tables = create uo_ds_top
f_create_dynamic_ds(i_ds_detail_tables, 'grid', sqls, sqlca, true)
i_num_detail_tables = i_ds_detail_tables.rowCount()

// Datastore for wheres and error codes
sqls = "select where_id, error_code " +&
	" from cr_to_comm_control crtcc, " +&
	" cr_to_comm_translate crtct " +&
	" where crtcc.translate_id = crtct.translate_id " +&
	" and error_code is not null " +&
	" and crtcc.source_id = -1 " +&
	" order by error_order "
i_ds_cc_errors = create uo_ds_top
f_create_dynamic_ds(i_ds_cc_errors,'grid',sqls,sqlca,true)

// Datastore for translate clause
sqls = "select * from cr_to_comm_translate_clause where where_id = -1 order by row_id"
i_ds_trans_clause = create uo_ds_top
f_create_dynamic_ds(i_ds_trans_clause,'grid',sqls,sqlca,true)

// Datastore for translate tables
sqls =	"select distinct table_name || ' ' || table_alias" +&
	" from cr_to_comm_translate_clause ctc, cr_to_comm_table_list tl" +&
	" where where_id = -1 and " +&
	" (table_id = table1 OR table_id = table2)" +&
	" and table_alias <> 'cr' "
i_ds_trans_tables = create uo_ds_top
f_create_dynamic_ds(i_ds_trans_tables,'grid',sqls,sqlca,true)
	
// Datastore for record counts
sqls = "select count(*), sum(amount) from cr_cost_repository where -1 = 0"
i_ds_audit = create uo_ds_top
f_create_dynamic_ds(i_ds_audit,'grid',sqls,sqlca,true)

// Datastore for mappings
sqls = "select * from cr_to_comm_control where -1 = 0"
i_ds_cwip_mapping = create uo_ds_top
f_create_dynamic_ds(i_ds_cwip_mapping,'grid',sqls,sqlca,true)

//****************************************************************************
//  RESET CWIP_CHARGE_STATUS FOR VALIDATION ERRORS:
//    Any 9xx series was a prior validation kickout.  Reset to NULL
//****************************************************************************
// call a function to reset all of the cwip_charge_statuses
//uf_reset_cwip_charge_status_loop()

f_pp_msgs( "Reseting cwip_charge_status at " + string(now()))
//null - means it hasn't processed. If approvals are turned on null means it isn't approved
//0-999 - Means some sort of error (invalid, cost element, department, etc.)
//999999999999999 - means approved if approvals are being used

// Mark transactions that are prior kickouts or are approved (if turned on)
// ### 6202: Amount type flexibility
//	Also just going to update records -- then I don't have to do a couple below, I can just check SQLCA.SQLNRows
sqls = "update cr_cost_repository set cwip_charge_status = 999 "
if i_obey_approval = "YES" then
	sqls += "where ( nvl(cwip_charge_status, -1) between 0 and 999 or cwip_charge_status = 999999999999999 ) "
else
	sqls += "where nvl(cwip_charge_status, 0) between 0 and 999 "
end if
sqls += "and month_number between " + string(i_min_month) + " and " + string(i_max_month)
sqls += " and amount_type in (" + string(i_amount_types) + ")"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: updating cr_cost_repository.cwip_charge_status (900's) to 999: " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if

//f_pp_msgs( "Counting the records to be read in CR_COST_REPOSITORY at " + string(now()))
//select count(*) into :ct
//	from CR_COST_REPOSITORY
//	where cwip_charge_status = 999
//	  and amount_type = 1;
ct = SQLCA.SQLNRows
	
f_pp_msgs( "CR_COST_REPOSITORY has " + string(ct) + " records to process.")

if ct > 0 then
	// ### 6202
	mn_sqls = "select distinct month_number from cr_cost_repository " 
	mn_sqls += " where cwip_charge_status = 999 "
	mn_sqls += "and month_number between " + string(i_min_month) + " and " + string(i_max_month)
	mn_sqls += " and amount_type in (" + string(i_amount_types) + ") "
	mn_sqls += "order by 1 asc"

	f_get_column(results[], mn_sqls)
	num_months = upperbound(results)
else
	// Nothing to do...stop
	commit;
	return 1
end if

if num_months > 0 then
	i_max_month_number = results[num_months]
	i_min_month_number = results[1]
end if

// Moved this before the loop to save time...
// Omit certain companies from cwip_charge.
f_pp_msgs("Updating cr_cost_repository.cwip_charge_status where do_not_post_to_projects = 1 at " + string(now()))
sqls = "update cr_cost_repository a set cwip_charge_status = -1 * id " 
sqls += "where a.cwip_charge_status = 999 "
sqls += "and month_number between " + string(i_min_month_number) + " and " + string(i_max_month_number)
sqls += " and amount_type in (" + string(i_amount_types) + ")"
sqls += "and exists (select 1 from cr_company where a." + i_company_field + " = external_company_id and do_not_post_to_projects = 1)"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs( "ERROR: updating cwip_charge_status to -id (do not post to projects): " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if


//  Mark records that have no work order.
// ### 6202: Amount Types
f_pp_msgs("Updating cr_cost_repository.cwip_charge_status where proj = ' ' at " + string(now()))
sqls = " update cr_cost_repository set cwip_charge_status = -id"
sqls += " where cwip_charge_status = 999"
sqls += " and month_number between " + string(i_min_month_number) + " and " + string(i_max_month_number)
sqls += " and amount_type in (" + string(i_amount_types) + ") "
sqls += ' and "' + i_wo_field + '" = ' + "' '"
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: Updating cr_cost_repository.cwip_charge_statuts where proj = ' ': " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs("  ")
	rollback;
	return -1
end if
	
for i = 1 to num_months
	i_mn_to_process = results[i]
	g_mn_to_process = i_mn_to_process
	
	f_pp_msgs("  ")
	f_pp_msgs("Processing month " + string(i_mn_to_process) + " at " + string(now()))

	// This gets updated during the validation logic...if there are records that pass validations, then it goes through the 
	//	rest of the code, otherwise it goes to the next month.  This takes the place of a count from cr_cost_repository
	//	below.  Currently, if it's only processing detail tables there is no check to know if it needs to process or not.
	i_records_to_process = 0
	i_amount_to_process = 0
	
	//EAG, moved this up because there were times that the RJO piece below (line 162) was getting skipped.
	i_min_commitment_id_loaded = 0
	select max(commitment_id) into :i_min_commitment_id_loaded from commitments;
	if isnull(i_min_commitment_id_loaded) then i_min_commitment_id_loaded = 0
	i_min_commitment_id_loaded = i_min_commitment_id_loaded + 1
	
	// RJO 
	// Save off the min id of the first month's load
	// will use this in building the summary tables
	if i = 1 then
		i_min_commitment_id = i_min_commitment_id_loaded
	end if
	
	// CALL A CUSTOM FUNCTION FOR ADDITIONAL VALIDATIONS
	// THAT ARE NOT SET UP THROUGH THE TABLES
	rtn = f_cr_to_commitments_custom(-1)
	if rtn = -1 then
		return -1
	end if
	
	//****************************************************************************
	//  CWIP_CHARGE_STATUS:
	//    Update the cwip_charge_status field with a 1 to denote the charges to be read.
	//****************************************************************************
	f_pp_msgs("Updating cwip_charge_status at " + string(now()))
	rtn = uf_cwip_charge_status(0)
	
	if rtn < 0 then
		return -1
	end if
	
	if rtn = 2 then
		f_pp_msgs("  ")
		f_pp_msgs("There are no records to process in this block.")
		f_pp_msgs("The interface will move to the next block of records.")
		f_pp_msgs("  ")
		commit;
		continue
	end if
	
	// PERFORM PRE LOADING VALIDATION
	f_pp_msgs("Validating mappings at " + string(now()))
	rtn = uf_validate_pre_load()
	if rtn = -1 then
		return -1
	end if
	
//	select count(*) 
//	into :num_to_proc
//	from cr_cost_repository
//	where cwip_charge_status in (0, 977)
//	and amount_type = 1
//	;
	
	if i_records_to_process = 0 then
		f_pp_msgs("  ")
		f_pp_msgs("There are no records to process in this block.")
		f_pp_msgs("The interface will move to the next block of records.")
		f_pp_msgs("  ")
		commit;
		continue
	else
		f_pp_msgs("Processing: " + string(i_records_to_process) + " records from the CR.")
	end if
	
	//***********************************************************************
	//  Get the distinct list of companies (BU'S) from CR_COST_REPOSITORY.
	//  These will be used later to rebuild the summary tables.
	//***********************************************************************
	f_pp_msgs( "Getting the distinct list of companies at " + string(now()))
	rtn = uf_company()
	
	if rtn < 0 then
		return -1
	end if
	
	
	//*********************************************************************************
	//  Make sure that the CR_TEMP_CWIP_CHARGE table is empty ... previous runs
	//  should clear out this table, but, just in case ...
	//*********************************************************************************
	f_pp_msgs( "Truncating cr_commitments_sum at " + string(now()))
	sqlca.truncate_table('cr_commitments_sum')
	if sqlca.SQLCode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: truncating the cr_commitments_sum table " + sqlca.SQLErrText)
		f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
		f_pp_msgs(" ")
		rollback;
		return -1
	end if
	
	f_pp_msgs( "Truncating cr_commitments_translate at " + string(now()))
	sqlca.truncate_table('cr_commitments_translate')
	if sqlca.SQLCode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: truncating the cr_commitments_translate table " + sqlca.SQLErrText)
		f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
		f_pp_msgs(" ")
		rollback;
		return -1
	end if
	
	
	//*********************************************************************************
	//  Load the cr_commitments_translate AND CR_TO_COM_SUM table.
	//*********************************************************************************
	f_pp_msgs("Inserting into cr_commitments_translate at " + string(now()))
	rtn = uf_insert_stg_translate()
	
	if rtn < 0 then
		return -1
	end if

	// DO POST LOAD VALIDATION BEFORE SUMMARIZING INTO CR_CWIP_CHARGE_SUM
	// THIS IS A CUSTOM FUNCTION FOR UPDATES OR VALIDATIONS
	rtn = f_cr_to_commitments_custom(1)
	if rtn = -1 then
		rollback;
		return -1
	end if

//	//*********************************************************************************
//	//  If there are any new journal codes, they must be in cr_cwip_charge_drill
//	//		so that drilling from cwip_charge to the CR will work correctly.
//	//*********************************************************************************
//	insert into cr_cwip_charge_drill (journal_code, table_name)
//	select distinct journal_code, 'cr_cost_repository'
//		from cr_cwip_charge_translate a
//		where journal_code is not null
//		and a.source_id = 0
//		and not exists (select journal_code from cr_cwip_charge_drill c where a.journal_code = c.journal_code);
//		
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs(" ")
//		f_pp_msgs("ERROR: Inserting into cr_cwip_charge_drill (summary): " + sqlca.sqlerrtext)
//		f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
//		f_pp_msgs(" ")
//		rollback;
//		return -1
//	end if
//	
//	insert into cr_cwip_charge_drill (journal_code, table_name)
//	select distinct journal_code, lower(b.description)
//		from cr_cwip_charge_translate a, cr_sources b
//		where journal_code is not null
//		and a.source_id = b.source_id
//		and not exists (select journal_code from cr_cwip_charge_drill c where a.journal_code = c.journal_code);
//		
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs(" ")
//		f_pp_msgs("ERROR: Inserting into cr_cwip_charge_drill (detail): " + sqlca.sqlerrtext)
//		f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
//		f_pp_msgs(" ")
//		rollback;
//		return -1
//	end if
	
	//*********************************************************************************
	//  Load the CR_CWIP_CHARGE_SUM table.
	//*********************************************************************************
	f_pp_msgs( "Inserting into cr_commitments_sum at " + string(now()))
	rtn = uf_insert_stg_sum()
	
	if rtn < 0 then
		return -1
	end if
	
	//*********************************************************************************
	//  BALANCING:
	//    Make sure the amounts in cr_commitments_translate match the amounts in cr_commitments_sum.
	//*********************************************************************************
	rtn = uf_balancing()
	
	if rtn < 0 then
		return -1
	end if
	
	//*********************************************************************************
	//  CHARGE_ID:
	//    Fill in the commitment_id on CR_TO_COM_SUM ... it is currently NULL.
	//*********************************************************************************
	f_pp_msgs("Updating commitment_id at " + string(now()))
	rtn = uf_update_charge_id()
	
	if rtn < 0 then
		return -1
	end if
	
	// CALL A CUSTOM FUNCTION FOR ANY UPDATES / BACKFILLS TO cr_commitments_sum
	rtn = f_cr_to_commitments_custom(50)
	if rtn = -1 then
		return -1
	end if
	
	//*********************************************************************************
	//  COMMITMENTS:
	//    Insert data into commitments from cr_commitments_sum.
	//*********************************************************************************
	f_pp_msgs("Inserting into the commitments table at " + string(now()))
	rtn = uf_insert_final()
	
	if rtn < 0 then
		return -1
	end if
	
	
	//*********************************************************************************
	//  CWIP_CHARGE_STATUS:
	//    Mark the cwip_charge_status field with the month_number to denote the charges
	//    that have been read.
	//*********************************************************************************
	f_pp_msgs("Backfilling cwip_charge_status at " + string(now()))
	rtn = uf_cwip_charge_status(1)
	
	if rtn < 0 then
		return -1
	end if
	
	// CALL A CUSTOM FUNCTION FOR ANY UPDATES / BACKFILLS TO COMMITMENTS
	rtn = f_cr_to_commitments_custom(100)
	if rtn = -1 then
		return -1
	end if
	
	
	//*********************************************************************************
	//  DO WE NEED TO LOAD MORE CHARGES ?
	//    This interface loads records in increments.  Count the number of cr_cost_repository
	//    records whose cwip_charge_status is NULL.  If there are any, reload.
	//*********************************************************************************
	
	// RJO - Commit here after finishing a month of data.
	//       The only danger of committing here is that the interface 
	//       could fail before the summary tables are built.
	commit;

next


//*********************************************************************************
//  TRUNCATE TEMP TABLES
//*********************************************************************************
f_pp_msgs( "Truncating cr_commitments_sum at " + string(now()))
sqlca.truncate_table('cr_commitments_sum')
if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: truncating the cr_commitments_sum table " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if

f_pp_msgs( "Truncating cr_commitments_translate at " + string(now()))
sqlca.truncate_table('cr_commitments_translate')
if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: truncating the cr_commitments_translate table " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs(" ")
	rollback;
	return -1
end if


////THIS CODE TO ANALYZE AND UPDATE SUMMARIES WAS FROM CWIP_CHARGE.EXE.  DO NOT NEED.
////*********************************************************************************
////  Analyze cwip_charge:
////*********************************************************************************
//analyze_cwip = 'NO'
//select upper(trim(control_value)) into :analyze_cwip from cr_system_control
//where upper(trim(control_name)) = 'ANALYZE CWIP CHARGE';
//if isnull(analyze_cwip) then analyze_cwip = 'NO'
//
//if analyze_cwip = 'YES' then
//	f_pp_msgs( "Analyzing cwip_charge at " + string(now()))
//
//	ana_ret = sqlca.analyze_table('cwip_charge');	
//	if ana_ret <> 0 then
//		f_pp_msgs("ERROR: analyzing the cwip_charge table: ")
//		f_pp_msgs("THE CWIP_CHARGE WAS LOADED WITH ALL CHARGES.")
//		f_pp_msgs("CWIP_CHARGE SHOULD BE ANALYZED BY HAND.")
//	end if
//end if


//*********************************************************************************
// CALL A CUSTOM FUNCTION OUTSIDE THE LOOP FOR ANY LAST UPDATES
//*********************************************************************************
rtn = f_cr_to_commitments_custom(101)
if rtn = -1 then
   return -1
end if  


////*********************************************************************************
////  REBUILD SUMMARIES:
////*********************************************************************************
//if isnull(i_min_charge_id) or i_min_charge_id = 0 then
//	//  DMJ: 10/9/07:
//	//  We have not loaded any charges.  Building the summaries will take forever
//	//  if we drive it off of a i_min_charge_id = 0.  I used this technique instead
//	//  of something like "if ct = 0" since this should hold up even if we change
//	//  code in this function over time.  I'm guessing that i_min_charge_id will
//	//  always be set if we load any charges.
//else
//	rtn = uf_update_summaries()
//	
//	if rtn < 0 then
//		f_pp_msgs("The current month should not be closed until the summary tables are fixed and balanced.")
//		return -1
//	else
//		// RJO - commit after summaries are re-built
//		commit;
//	end if
//end if
//
//if i_kickouts then
//	f_pp_msgs(" ")
//	f_pp_msgs("Validation kickouts exists and should be reviewed in the logs above.")
//	f_pp_msgs(" ")
//end if

// This doesn't work for drills that need to goto detail tables...
// Moved above to handle taht
////*********************************************************************************
////  If there are any new journal codes, they must be in cr_cwip_charge_drill
////		so that drilling from cwip_charge to the CR will work correctly.
////*********************************************************************************
//insert into cr_cwip_charge_drill (journal_code, table_name)
//select journal_code, 'cr_cost_repository' from (
//	select journal_code 
//	from cwip_charge 
//	where month_number <= :i_max_month
//	and journal_code is not null
//	minus
//	select journal_code
//	from cr_cwip_charge_drill);
//	
//if sqlca.sqlcode < 0 then
//	f_pp_msgs("ERROR: Inserting into cr_cwip_charge_drill: " + sqlca.sqlerrtext)
//	rollback;
//	return -1
//end if

commit;

// ### 8142: JAK: 2011-11-01:  Consistent return codes
if i_kickouts then
	return -2
else 
	return 1
end if
end function

public function integer uf_validate_pre_load_update (longlong a_source_id, string a_table_name);string sqls, s_from, s_where,  descr
longlong num_rows, num_where, i, where_id, error_code, j
string lp, cn, op, v1, ba, v2, ao, rp
longlong records_to_process, num_errors
double amount_to_process

// VALIDATE ROWS IN CR THAT MIGHT FAIL A JOIN
// LOOK IN THE CR_TO_COMM_TRANSLATE TABLE AND ONLY VALIDATE THE
// COLUMNS WITH AN ERROR_CODE NOT NULL

// create the datastore
sqls = "select where_id, error_code" +&
	" from cr_to_comm_control crtcc," +&
	" cr_to_comm_translate crtct" +&
	" where crtcc.translate_id = crtct.translate_id" +&
	" and error_code is not null" +&
	" and crtcc.source_id = " + string(a_source_id) +&
	" order by error_order"
i_ds_cc_errors.SetSQLSelect(sqls)
num_rows = i_ds_cc_errors.Retrieve()

for i = 1 to num_rows
	where_id = i_ds_cc_errors.getItemNumber(i, 1)
	error_code = i_ds_cc_errors.getItemNumber(i, 2)
	
	sqls = "select * from cr_to_comm_translate_clause where where_id = " +&
		string(where_id) + " order by row_id"
	
	i_ds_trans_clause.SetSQLSelect(sqls)
	i_ds_trans_clause.Retrieve()
	
	sqls =	"select distinct table_name || ' ' || table_alias" +&
		" from cr_to_comm_translate_clause ctc, cr_to_comm_table_list tl" +&
		" where where_id = " + string(where_id) + " and " +&
		" (table_id = table1 OR table_id = table2)" +&
		" and table_alias <> 'cr' "
		
	i_ds_trans_tables.SetSQLSelect(sqls)	
	num_where = i_ds_trans_tables.Retrieve()
	
	//  Before the "detail table" code was added to this exe, the s_from variable looked like:
	//  " from cr_cost_repository cr, "
	//  Since the final validation update below changed to a straight update (as opposed to an
	//  update with a subselect/minus), this code had been changed to s_from = " from ".  That
	//  was not robust enough to handle mapping cases like job_task_id where there is only a 
	//  single line in the mapping, table1 is "cr" and table2 is null (which would always be)
	//  the case when you are not actually "mapping" and instead using something like an 
	//  in-list.  In these cases, the s_from variable was being trimmed off to " fro " and 
	//  bombing the SQL.  We placed an if block here to add the "dual" to the s_from.  Note
	//  you cannot add the actual a_table_name to the from in these cases (or any cases) since
	//  the final update below was changed to be an "un-joined" update.  You would end up getting
	//  a single-row subquery error.
	if num_where = 0 then
		s_from = " from dual, "
	else
		s_from = " from "
	end if
	
	// loop through the tables and build the from clause
	for j = 1 to num_where
		s_from += i_ds_trans_tables.getItemString(j, 1) + ", "
	next
	s_from = LEFT(s_from, LEN(s_from) - 2)
	
	// loop through the where claus and build the where
	num_where = i_ds_trans_clause.rowcount()
	s_where = " where "
	for j = 1 to num_where
		lp = i_ds_trans_clause.getitemstring(j, 'left_paren')
		cn = i_ds_trans_clause.getitemstring(j, 'column_name')
		op = i_ds_trans_clause.getitemstring(j, 'operator')
		v1 = i_ds_trans_clause.getitemstring(j, 'value1')
		ba = i_ds_trans_clause.getitemstring(j, 'between_and')
		v2 = i_ds_trans_clause.getitemstring(j, 'value2')
		ao = i_ds_trans_clause.getitemstring(j, 'and_or')
		rp = i_ds_trans_clause.getitemstring(j, 'right_paren')
		
		if isnull(lp) then lp = ""
		if isnull(cn) then cn = ""
		if isnull(op) then op = ""
		if isnull(v1) then v1 = ""
		if isnull(ba) then ba = ""
		if isnull(v2) then v2 = ""
		if isnull(rp) then rp = ""
		if isnull(ao) then ao = ""
		
		s_where +=  lp + " " + cn + " " + op + " " + v1 + " " + ba + " " + v2 + " " + rp + " " + ao	 + " "
	next
			
	sqls = "update " + a_table_name + " set cwip_charge_status = " + string(error_code)
	sqls += " where month_number = " + string(i_mn_to_process)
	sqls += " and amount_type in (" + i_amount_types + ") "
	sqls += " and cwip_charge_status = 1 "
	sqls += " and not exists "
	sqls += "	(select 1 "
	sqls += 	s_from + f_replace_string(s_where, "cr.", a_table_name + ".", "all")
	sqls += "	)"
	
	// RJO 2-7-2006
//	sqls = "update " + a_table_name + " set cwip_charge_status = " +&
//	 			" nvl( (" +& 
//				   " select " + a_table_name + ".cwip_charge_status " +&
//					s_from + f_replace_string(s_where, "cr.", a_table_name + ".", "all") +&
//				 + "), " + string(error_code) + ") " +&
//				" where cwip_charge_status = 1 "
				
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("  ERROR:  There are some errors with the where clause (id) " + string(where_id) + " for the source " + a_table_name)
		f_pp_msgs("  SQL ERROR: " + sqlca.SQLErrText)
		f_pp_msgs("  SQL: " + sqls)
		f_pp_msgs(" ")
		rollback;
		return -1
	elseif SQLCA.SQLNRows > 0 then
		num_errors = SQLCA.SQLNRows
		descr = ''
		select 'Invalid ' || long_description into :descr from cr_to_comm_translate
		 where error_code = :error_code;
		
		f_pp_msgs("  ")
		f_pp_msgs("*****  VALIDATION ERRORS EXIST: (" + upper(a_table_name) + ":" + string(num_errors) + " records:" + string(error_code) + "): " + descr)
		i_kickouts = true
		f_pp_msgs("  ")	
	end if
next
		
// Update i_records_to_process and i_amount_to_process
sqls = "select count(*), sum(amount) from " + a_table_name
sqls += " where month_number = " + string(i_mn_to_process)
sqls += "and cwip_charge_status = 1 "
sqls += " and amount_type in (" + i_amount_types + ") "
i_ds_audit.SetSQLSelect(sqls)
i_ds_audit.Retrieve()

records_to_process = i_ds_audit.getitemnumber(1,1)
amount_to_process = i_ds_audit.getitemnumber(1,2)

if isnull(records_to_process) then records_to_process = 0
if isnull(amount_to_process) then amount_to_process = 0

i_records_to_process += records_to_process
i_amount_to_process += amount_to_process

return 1
end function

public function longlong uf_cwip_charge_status (longlong a_type);//*********************************************************************************************************************
//
//  User Object Function  :  uf_cwip_charge_status
//
//  Description  :  Update the cwip_charge_status field with a month_number or a 0.
//						    0 means "ready to be read".
//							 A month_number means "processed".
//
//*********************************************************************************************************************
string sqls
longlong num_rows, i, cnt, source_id, needs_processing
string up_sql, gl_cats, ck_sql, table_name
any results[]

if a_type = 0 then 	
	//  Mark the records as "ready to be read" ...
	// ### 6202: Amount Type
	f_pp_msgs( "Updating cr_cost_repository.cwip_charge_status with 1 at " + string(now()))
	sqls = "update cr_cost_repository set cwip_charge_status = 1 "
	sqls += "	 where cwip_charge_status = 999 "
	sqls += "	 and month_number =  " + string(i_mn_to_process) 
	sqls += "	 and amount_type in (" + i_amount_types + ")"
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: updating cr_cost_repository.cwip_charge_status with 1: " + sqlca.SQLErrText)
		f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
		f_pp_msgs("SQL: " + sqls)
		f_pp_msgs(" ")
		rollback;
		return -1
	else
		if sqlca.SQLNRows = 0 then
			//if there are no records in this "block" updated to 0, there is no sense in 
			//running through the rest of the code to do nothing.
			return 2
		end if
	end if	
	
	// we want to loop over any detail sources.
	for i = 1 to i_num_detail_tables
		source_id = i_ds_detail_tables.getitemnumber(i, 'source_id')
		table_name = i_ds_detail_tables.getitemstring(i, 'table_name')
		gl_cats = i_ds_detail_tables.getitemstring(i, 'gl_jrnl_category')
				
		// SEK 050609: Added the exists clause to prevent picking up detail records
		//					whose corresponding summary records have not been selected
		//					to be sent, i.e. a summary record that has not been approved
		//					if they are not obeying approval.
		up_sql = "update " + table_name + " set cwip_charge_status = 999" +&
	 				" where nvl(cwip_charge_status,0) between 0 and 998 " +&
					" and month_number = " + string(i_mn_to_process) + &
					" and amount_type in (" + i_amount_types + ") " + &
					" and exists (" + &
					"		select 1 from cr_cost_repository" + &
					"		 where source_id = " + string(source_id) + &
					"			and cwip_charge_status = 1" + &
					"			and cr_cost_repository.drilldown_key = " + table_name + ".drilldown_key" + &
					"			and cr_cost_repository.month_number = " + table_name + ".month_number)"
					
		if not (trim(gl_cats) = "''" or isnull(trim(gl_cats)) or trim(gl_cats) = "") then
			// DMJ: 12/5/07: There are no single-quotes in the in-list here.  You must enter them
			// in the data setup.  This is intended to be a list of GL JCats if needed.
			up_sql += " and gl_journal_category in (" + gl_cats + ")"
		end if
		execute immediate :up_sql;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: Updating " + table_name + ".cwip_charge_statuts =999  " + sqlca.sqlerrtext)
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
		//  Updating detail records where summary records are marked as Do Not Post 
		// ### 6202
		f_pp_msgs( "Updating detail records where summary records are marked as Do Not Post at " + string(now()))
		up_sql = &
			" update " + table_name + " a set cwip_charge_status = -1 * id " + &
			" where a.month_number = " + string(i_mn_to_process) + &
			" and amount_type in (" + i_amount_types + ") " + &
			" and cwip_charge_status = 999 " + &
			" and exists (select 1 from cr_cost_repository b where a.drilldown_key = b.drilldown_key and b.cwip_charge_status < 0 " +&
				" and b.month_number = " + string(i_mn_to_process) + ")"
		execute immediate :up_sql;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: updating " + table_name + ".cwip_charge_status with -id (Do Not Post): " + sqlca.SQLErrText)
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs("SQL: " + up_sql)
			f_pp_msgs(" ")
			rollback;
			return -1
		end if		
		
		//  Mark the records as "ready to be read" ...
		f_pp_msgs( "Updating " + table_name + ".cwip_charge_status with 1 at " + string(now()))
		
		up_sql = &
			" update " + table_name + " set cwip_charge_status = 1" +&
			" where cwip_charge_status = 999 " +&
			" and month_number = " + string(i_mn_to_process) +&
			" and amount_type in (" + i_amount_types + ") "
		execute immediate :up_sql;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: updating " + table_name + ".cwip_charge_status with 1: " + sqlca.SQLErrText)
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs("SQL: " + up_sql)
			f_pp_msgs(" ")
			rollback;
			return -1
		else
			if sqlca.SQLNRows > 0 then
				// There are detail records we need to process
				i_ds_detail_tables.setitem(i, 'needs_processing',1)
			else
				i_ds_detail_tables.setitem(i, 'needs_processing',0)
			end if
		end if
		
		// now we want to update the cr_cost_repository record with 977
		// as the cwip_charge_status (this means it comes from the detail table
		up_sql = "update cr_cost_repository b" +&
			" set cwip_charge_status = 977" +&
			" where source_id = " + string(source_id) + &
			" and month_number = " + string(i_mn_to_process) + &
			" and amount_type in (" + i_amount_types + ") " + &
			" and cwip_charge_status = 1 "
		
		if not (trim(gl_cats) = "''" or isnull(trim(gl_cats)) or trim(gl_cats) = "") then
			// DMJ: 12/5/07: There are no single-quotes in the in-list here.  You must enter them
			// in the data setup.  This is intended to be a list of GL JCats if needed.
			up_sql += " and gl_journal_category in (" + gl_cats + ")"
		end if
	
		execute immediate :up_sql;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: updating cr_cost_repository.cwip_charge_status with 977: " + sqlca.SQLErrText)
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs("SQL: " + up_sql)
			f_pp_msgs(" ")
			rollback;
			return -1
		end if
		
		// in the case that the detail records get ignored, we need to update cr_cost_repository to reflect that.
		up_sql = "update cr_cost_repository a" +&
			" set a.cwip_charge_status = " + &
				" (select case when min(b.cwip_charge_status) < 0 then -977 else min(b.cwip_charge_status) end " +&
				" from " + table_name + " b" +&
				" where b.drilldown_key = a.drilldown_key " + &
				" and b.month_number = " + string(i_mn_to_process) + ") " + &
			" where cwip_charge_status = 977 " +&
			" and source_id = " + string(source_id) + &
			" and month_number = " + string(i_mn_to_process) + &
			" and amount_type in (" + i_amount_types + ") " + &
			" and not exists  (select 1 " +&
				" from " + table_name + " b" +&
				" where b.drilldown_key = a.drilldown_key " + &
				" and b.month_number = " + string(i_mn_to_process) + " and b.cwip_charge_status between 0 and 1000) "
		execute immediate :up_sql;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: updating cr_cost_repository.cwip_charge_status with detail charge_id: " + sqlca.SQLErrText)
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs("SQL: " + up_sql)
			f_pp_msgs(" ")
			rollback;
			return -1
		end if
	next
	
	f_pp_msgs( "Successfully updated cr_cost_repository.cwip_charge_status with 1 at " + string(now()))
	return 1

else	
	//  cr_cost_repository records with a cwip_charge_status or 1 have now been loaded into commitments.
	//  Update cwip_charge_status with the commitment_id to enable drilling back.
	// ### 7849: JAK: 2011-06-14:  Change the journal code join from:
	//		and nvl(crsum.journal_code, 'sqrtpi') = nvl(crxlate.journal_code, '-011181')
	//		to
	//		and nvl(crsum.journal_code, '-011181') = nvl(crxlate.journal_code, '-011181')
	f_pp_msgs( "Updating cr_cost_repository.cwip_charge_status with the commitment_id at " + string(now()))
	
	update cr_cost_repository set cwip_charge_status =
 		nvl(( select crsum.commitment_id 
		from cr_commitments_sum crsum, cr_commitments_translate crxlate 
		where cr_cost_repository.id = crxlate.commitment_id 		
		and nvl(crsum.charge_mo_yr, to_date(1776, 'yyyy')) = nvl(crxlate.charge_mo_yr, to_date(1776, 'yyyy'))
		and crsum.commitment_type_id = crxlate.commitment_type_id 
		and crsum.company_id = crxlate.company_id 
		and nvl(crsum.cost_element_id, -11181) = nvl(crxlate.cost_element_id, -11181) 
		and crsum.department_id = crxlate.department_id 
		and crsum.description = crxlate.description
		and crsum.expenditure_type_id = crxlate.expenditure_type_id 
		and nvl(crsum.gl_account_id, -11181) = nvl(crxlate.gl_account_id, -11181)
		and nvl(crsum.month_number, -11181) = nvl(crxlate.month_number, -11181)
		and crsum.work_order_id = crxlate.work_order_id 
		and nvl(crsum.bus_segment_id, -11181) = nvl(crxlate.bus_segment_id, -11181)
		and nvl(crsum.closed_month_number, -11181) = nvl(crxlate.closed_month_number, -11181)
		and nvl(crsum.exclude_from_overheads, -11181) = nvl(crxlate.exclude_from_overheads, -11181)
		and nvl(crsum.external_gl_account, '-011181') = nvl(crxlate.external_gl_account, '-011181') 
		and nvl(crsum.id_number, '-011181') = nvl(crxlate.id_number, '-011181')
		and nvl(crsum.job_task_id, '-011181') = nvl(crxlate.job_task_id, '-011181')
		and nvl(crsum.journal_code, '-011181') = nvl(crxlate.journal_code, '-011181')
		and nvl(crsum.loading_amount, -11181) = nvl(crxlate.loading_amount, -11181)
		and nvl(crsum.non_unitized_status, -11181) = nvl(crxlate.non_unitized_status, -11181)
		and nvl(crsum.notes, '-011181') = nvl(crxlate.notes, '-011181')
		and nvl(crsum.original_amount, -11181) = nvl(crxlate.original_amount, -11181)
		and nvl(crsum.original_currency, -11181) = nvl(crxlate.original_currency, -11181)
		and nvl(crsum.payment_date, to_date(1776, 'yyyy')) = nvl(crxlate.payment_date, to_date(1776, 'yyyy'))
		and nvl(crsum.po_number, '-011181') = nvl(crxlate.po_number, '-011181')
		and nvl(crsum.reference_number, '-011181') = nvl(crxlate.reference_number, '-011181')
		and nvl(crsum.retirement_unit_id, -11181) = nvl(crxlate.retirement_unit_id,-11181)
		and nvl(crsum.serial_number, '-011181') = nvl(crxlate.serial_number, '-011181')
		and nvl(crsum.status, -11181) = nvl(crxlate.status, -11181)
		and nvl(crsum.stck_keep_unit_id, -11181) = nvl(crxlate.stck_keep_unit_id, -11181)
		and nvl(crsum.sub_account_id, -11181) = nvl(crxlate.sub_account_id, -11181)
		and nvl(crsum.units, '-011181') = nvl(crxlate.units, '-011181')
		and nvl(crsum.utility_account_id, -11181) = nvl(crxlate.utility_account_id, -11181) 
		and nvl(crsum.vendor_information, '-011181') = nvl(crxlate.vendor_information, '-011181')
		
		and nvl(crsum.est_start_date, to_date(1776, 'yyyy')) = nvl(crxlate.est_start_date, to_date(1776, 'yyyy'))
		and nvl(crsum.est_comp_date, to_date(1776, 'yyyy')) = nvl(crxlate.est_comp_date, to_date(1776, 'yyyy'))
		and nvl(crsum.est_chg_type_id, -11181) = nvl(crxlate.est_chg_type_id, -11181)
		),988)
	where cr_cost_repository.cwip_charge_status = 1 
	and month_number = :i_mn_to_process;

	if sqlca.SQLCode < 0 then
		// The rollback prevents the records from getting loaded to CWIP Charge...  the interface still has to be fixed, or it will continue to error, but at least it's not duplicating data...
		f_pp_msgs(" ")
		f_pp_msgs("ERROR: updating cr_cost_repository.cwip_charge_status with the charge_id " + sqlca.SQLErrText)
		f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
		f_pp_msgs(" ")
		rollback;
		return -1
	else
		select count(*) into :cnt from cr_cost_repository
		 where cr_cost_repository.cwip_charge_status = 988 
			and month_number = :i_mn_to_process;

		if cnt > 0 then
			f_pp_msgs("  ")
			f_pp_msgs( "ERROR: updating cr_cost_repository.cwip_charge_status with the commitment_id ")
			f_pp_msgs( string(cnt) + " records failed the cwip charge status backfill in month " + string(i_mn_to_process))
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
	end if
	
	// we want to loop over any detail sources.
	for i = 1 to i_num_detail_tables
		source_id = i_ds_detail_tables.getitemnumber(i, 'source_id')
		table_name = i_ds_detail_tables.getitemstring(i, 'table_name')
		gl_cats = i_ds_detail_tables.getitemstring(i, 'gl_jrnl_category')
		
		// If we didn't find records to process above, no need to do any of the extra processing (validations, inserts etc)
		needs_processing = i_ds_detail_tables.getitemnumber(i, 'needs_processing')
		if needs_processing = 0 then continue
		
		f_pp_msgs("Updating " + table_name + ".cwip_charge_status with commitment_id at " + string(now()))
	
		// ### 7849: JAK: 2011-06-14:  Change the journal code join from:
		//		and nvl(crsum.journal_code, 'sqrtpi') = nvl(crxlate.journal_code, '-011181')
		//		to
		//		and nvl(crsum.journal_code, '-011181') = nvl(crxlate.journal_code, '-011181')
		up_sql = &
		"update " + table_name + " set cwip_charge_status =" +&
			" nvl(( select crsum.charge_audit_id " +&
			"	from cr_commitments_sum crsum, cr_commitments_translate crxlate " +&
			"	where " + table_name + ".id = crxlate.commitment_id " +&
			"	and nvl(crsum.charge_mo_yr, to_date(1776, 'yyyy')) = nvl(crxlate.charge_mo_yr, to_date(1776, 'yyyy')) " + &
			"	and crsum.commitment_type_id = crxlate.commitment_type_id  " + &
			"	and crsum.company_id = crxlate.company_id  " + &
			"	and nvl(crsum.cost_element_id, -11181) = nvl(crxlate.cost_element_id, -11181)  " + &
			"	and crsum.department_id = crxlate.department_id  " + &
			"	and crsum.description = crxlate.description " + &
			"	and crsum.expenditure_type_id = crxlate.expenditure_type_id  " + &
			"	and nvl(crsum.gl_account_id, -11181) = nvl(crxlate.gl_account_id, -11181) " + &
			"	and nvl(crsum.month_number, -11181) = nvl(crxlate.month_number, -11181) " + &
			"	and crsum.work_order_id = crxlate.work_order_id  " + &
			"	and nvl(crsum.bus_segment_id, -11181) = nvl(crxlate.bus_segment_id, -11181) " + &
			"	and nvl(crsum.closed_month_number, -11181) = nvl(crxlate.closed_month_number, -11181) " + &
			"	and nvl(crsum.exclude_from_overheads, -11181) = nvl(crxlate.exclude_from_overheads, -11181) " + &
			"	and nvl(crsum.external_gl_account, '-011181') = nvl(crxlate.external_gl_account, '-011181')  " + &
			"	and nvl(crsum.id_number, '-011181') = nvl(crxlate.id_number, '-011181') " + &
			"	and nvl(crsum.job_task_id, '-011181') = nvl(crxlate.job_task_id, '-011181') " + &
			"	and nvl(crsum.journal_code, '-011181') = nvl(crxlate.journal_code, '-011181') " + &
			"	and nvl(crsum.loading_amount, -11181) = nvl(crxlate.loading_amount, -11181) " + &
			"	and nvl(crsum.non_unitized_status, -11181) = nvl(crxlate.non_unitized_status, -11181) " + &
			"	and nvl(crsum.notes, '-011181') = nvl(crxlate.notes, '-011181') " + &
			"	and nvl(crsum.original_amount, -11181) = nvl(crxlate.original_amount, -11181) " + &
			"	and nvl(crsum.original_currency, -11181) = nvl(crxlate.original_currency, -11181) " + &
			"	and nvl(crsum.payment_date, to_date(1776, 'yyyy')) = nvl(crxlate.payment_date, to_date(1776, 'yyyy')) " + &
			"	and nvl(crsum.po_number, '-011181') = nvl(crxlate.po_number, '-011181') " + &
			"	and nvl(crsum.reference_number, '-011181') = nvl(crxlate.reference_number, '-011181') " + &
			"	and nvl(crsum.retirement_unit_id, -11181) = nvl(crxlate.retirement_unit_id,-11181) " + &
			"	and nvl(crsum.serial_number, '-011181') = nvl(crxlate.serial_number, '-011181') " + &
			"	and nvl(crsum.status, -11181) = nvl(crxlate.status, -11181) " + &
			"	and nvl(crsum.stck_keep_unit_id, -11181) = nvl(crxlate.stck_keep_unit_id, -11181) " + &
			"	and nvl(crsum.sub_account_id, -11181) = nvl(crxlate.sub_account_id, -11181) " + &
			"	and nvl(crsum.units, '-011181') = nvl(crxlate.units, '-011181') " + &
			"	and nvl(crsum.utility_account_id, -11181) = nvl(crxlate.utility_account_id, -11181)  " + &
			"	and nvl(crsum.vendor_information, '-011181') = nvl(crxlate.vendor_information, '-011181') " + &
			"	and nvl(crsum.est_start_date, to_date(1776, 'yyyy')) = nvl(crxlate.est_start_date, to_date(1776, 'yyyy')) " + &
			"	and nvl(crsum.est_comp_date, to_date(1776, 'yyyy')) = nvl(crxlate.est_comp_date, to_date(1776, 'yyyy')) " + &
			"	and nvl(crsum.est_chg_type_id, -11181) = nvl(crxlate.est_chg_type_id, -11181) " + &
			"	),988) " + &
		" where cwip_charge_status = 1  " +&
		" and month_number = " + string(i_mn_to_process)
			
		execute immediate :up_sql;
		
		if sqlca.SQLCode < 0 then
			// The rollback prevents the records from getting loaded to CWIP Charge...  the interface still has to be fixed, or it will continue to error, but at least it's not duplicating data...
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: updating " + table_name + ".cwip_charge_status with the commitment_id " + sqlca.SQLErrText)
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs(" ")
			rollback;
			return -1
		else
			ck_sql = ' select count(*) from ' + table_name + ' where cwip_charge_status = 988 '
			f_get_column(results,ck_sql)
			if upperbound(results) > 0 then
				if results[1] > 0 then 
						f_pp_msgs("  ")
						f_pp_msgs( "ERROR: updating " + table_name + ".cwip_charge_status with the commitment_id ")
						f_pp_msgs( string(results[1]) + " records failed the cwip charge status backfill " + string(i_mn_to_process))
						f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
						f_pp_msgs("  ")
						rollback;
						return -1
				end if
			end if
		end if
		
		// we want to fill in the cr_cost_repository record with the cwip_charge_status
		//		only do this update if the underlying records have been completely processed.
		//		If there are in process (CCS between 0 and 1000) leave it as 977 so the detail gets
		//		processed next time.
		up_sql = "update cr_cost_repository a" +&
			" set a.cwip_charge_status = " + &
				" (select case when min(b.cwip_charge_status) < 0 then -977 else min(b.cwip_charge_status) end " +&
				" from " + table_name + " b" +&
				" where b.drilldown_key = a.drilldown_key " + &
				" and b.month_number = " + string(i_mn_to_process) + ") " + &
			" where cwip_charge_status = 977 " +&
			" and source_id = " + string(source_id) + &
			" and month_number = " + string(i_mn_to_process) + &
			" and amount_type in (" + i_amount_types + ") " + &
			" and not exists  (select 1 " +&
				" from " + table_name + " b" +&
				" where b.drilldown_key = a.drilldown_key " + &
				" and b.month_number = " + string(i_mn_to_process) + " and b.cwip_charge_status between 0 and 1000) "
		execute immediate :up_sql;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: updating cr_cost_repository.cwip_charge_status with detail cwip_charge_status: " + sqlca.SQLErrText)
			f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
			f_pp_msgs("SQL: " + up_sql)
			f_pp_msgs(" ")
			rollback;
			return -1
		end if
	next 
	
end if

return 1
end function

public function longlong uf_insert_stg_translate ();longlong ret, i, source_id, needs_processing
string table_name

// Insert into the translation table for the summary table
ret = uf_insert_stg_translate2(0, 'cr_cost_repository')
if ret < 0 then
	f_pp_msgs( "ERROR: uf_insert_stg_translate2(0, 'cr_cost_repository')")
	return -1
end if

// Insert into the translation table for the detail tables
for i = 1 to i_num_detail_tables
	source_id = i_ds_detail_tables.getitemnumber(i, 'source_id')
	table_name = i_ds_detail_tables.getitemstring(i, 'table_name')
		
	// If we didn't find records to process above, no need to do any of the extra processing (validations, inserts etc)
	needs_processing = i_ds_detail_tables.getitemnumber(i, 'needs_processing')
	if needs_processing = 0 then continue
	
	ret = uf_insert_stg_translate2(source_id, table_name)
	if ret < 0 then
		f_pp_msgs( "ERROR: uf_insert_stg_translate2(" + string(source_id) + ", '" + table_name + "')")
		return -1
	end if
next

return 1
end function

public function longlong uf_insert_stg_translate2 (longlong a_source, string a_table);//*********************************************************************************************************************
//
//  User Object Function  :  uf_insert_stg_translate2
//
//  Description  :  Get the valid records from CR_COST_REPOSITORY and load them into CR_COMMITMENTS_TRANSLATE.
//
//*********************************************************************************************************************
string s_insert_cols, s_selects, s_group, s_from, sqls, s_insert, s_where, comm_col, cr_col, cr_col_trans
longlong num_rows, i, trans_id, where_id, num_where, j, rtn
string lp, cn, op, v1, ba, v2, ao, rp

s_insert_cols = "( "
s_selects = "select "
s_group = ""
s_insert = "insert into cr_commitments_translate "

// have to use alias cr here because that is the alias used in the cr_to_comm_control table
s_from = "from " + a_table + " cr " 

// build the rest of the from clause
sqls =	"select distinct table_name || ' ' || table_alias" +&
	" from cr_to_comm_translate_clause ctc," +&
	" cr_to_comm_table_list tl, cr_to_comm_control cc" +&
	" , cr_to_comm_translate ct" +&
	" where ctc.where_id = ct.where_id and " +&
	" (tl.table_id = ctc.table1 OR tl.table_id = ctc.table2)" +&
	" and tl.table_alias <> 'cr'" +&
	" and cc.translate_id = ct.translate_id " +&
	" and cc.active_mapping = 1" +&
	" and cc.source_id = " + string(a_source)
i_ds_trans_tables.SetSQLSelect(sqls)
num_rows = i_ds_trans_tables.Retrieve()
for i = 1 to num_rows
	s_from += ", " + i_ds_trans_tables.GetItemString(i,1) + " "
next

// ### 6202: Amount types
s_where = "where cr.cwip_charge_status = 1 and month_number = " + string(i_mn_to_process) + " and cr.amount_type in (" + i_amount_types + ") and "

sqls = "select * from cr_to_comm_control " +&
			" where active_mapping = 1 " +&
			" and source_id = " + string(a_source) +&
			" order by control_id"
i_ds_cwip_mapping.SetSQLSelect(sqls)
num_rows = i_ds_cwip_mapping.Retrieve()

for i = 1 to num_rows
	setnull(trans_id)
	trans_id = i_ds_cwip_mapping.GetItemNumber(i, 'translate_id')
	//### JDS 2011-03-23: MAINT 7188 - item 2. Changed this to comm_col and updated the variable throughout.
	//cwip_col = i_ds_cwip_mapping.GetItemString(i, 'cwip_col')
	comm_col = i_ds_cwip_mapping.GetItemString(i, 'comm_col')
	cr_col = i_ds_cwip_mapping.GetItemString(i, 'val_to_insert')
	s_insert_cols = s_insert_cols + comm_col + ", "
	if isnull(trans_id) then 
		// not a translation
		s_selects = s_selects + cr_col + " as " + comm_col + ", "
	else
		// translation exists either by id or by external
		select  where_id, val_to_insert
			into :where_id, :cr_col_trans
			from cr_to_comm_translate
			where translate_id = :trans_id;
			
		s_selects = s_selects + cr_col_trans + " as " + comm_col + ", "
		
		// build the where clause for this translation
		sqls = "select * from cr_to_comm_translate_clause where where_id = " +&
			string(where_id) + " order by row_id"
		
		i_ds_trans_clause.SetSQLSelect(sqls)
		num_where = i_ds_trans_clause.Retrieve()
		
		for j = 1 to num_where
			lp = i_ds_trans_clause.getitemstring(j, 'left_paren')
			cn = i_ds_trans_clause.getitemstring(j, 'column_name')
			op = i_ds_trans_clause.getitemstring(j, 'operator')
			v1 = i_ds_trans_clause.getitemstring(j, 'value1')
			ba = i_ds_trans_clause.getitemstring(j, 'between_and')
			v2 = i_ds_trans_clause.getitemstring(j, 'value2')
			ao = i_ds_trans_clause.getitemstring(j, 'and_or')
			rp = i_ds_trans_clause.getitemstring(j, 'right_paren')
			
			if isnull(lp) then lp = ""
			if isnull(cn) then cn = ""
			if isnull(op) then op = ""
			if isnull(v1) then v1 = ""
			if isnull(ba) then ba = ""
			if isnull(v2) then v2 = ""
			if isnull(rp) then rp = ""
			if isnull(ao) then ao = ""
			
			s_where = s_where + lp + " " + cn + " " + op + " " + v1 + " " + ba + " " + v2 + " " + rp + " " + ao	 + " "
		next
		
		s_where = s_where + " and "
	end if
next

s_insert_cols = s_insert_cols + " source_id ) "
s_selects = s_selects + " " + string(a_source)
s_where = left(s_where, len(s_where) - 4)

f_pp_msgs( "Loading cr_commitments_translate from " + a_table + " at " + string(now()))

sqls = s_insert + " " + s_insert_cols + " ( " + s_selects + " " + s_from + " " + s_where + " ) "

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: inserting into cr_commitments_translate: " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
else
	// log the number of records written 
	f_pp_msgs( "Inserted " + string(sqlca.sqlnrows) + " records into cr_commitments_translate")
end if

if i_debug = "YES" then
	f_write_log(g_log_file, "***DEBUG*** sqls = " + sqls)
end if

//f_pp_msgs( "Analyzing cr_commitments_translate at " + string(now()))
sqlca.analyze_table('cr_commitments_translate')

if sqlca.sqlcode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: analyzing cr_commitments_translate:")
	f_pp_msgs("The interface will continue.")
	f_pp_msgs(" ")
end if

return 1
end function

public function integer uf_update_charge_id ();//******************************************************************************************
//
//  Function  :  uf_update_charge_id
//  
//  Updates the commitment_id field on cr_to_comm_sum.  This field is the
//  primary key on the commitment table.  
//
//  Returns  :    1 if all is successful
//               -1 on error
//
//******************************************************************************************
//  DMJ: THIS UPDATE ON CHARGE_AUDIT_ID MUST BE HERE !!!  I DON'T CARE WHAT KIND OF
//       COCKAMAMIE TABLE STRUCTURE HAS BEEN PUT IN PLACE FOR THE WORK ORDER COMMITMENTS.
//       MY ONLY CONCERN IS PREVENTING CR COMMITMENT TRANSACTIONS FROM POSTING OVER AND
//       OVER AGAIN TO PROJECTS.  IF THEY DON'T HANG TOGETHER PROPERLY IN THE WORK ORDER
//       SCREENS, SO BE IT.  I WILL ATTEMPT A BACKFILL BELOW THIS AND THERE IS A CUSTOM
//       FUNCTION TO HANDLE MORE DIFFICULT CASES.
//
//       DON'T EVEN THINK ABOUT REMOVING THIS CODE !!!
//
//       THE UPDATE WITH COMMITMENT_ID HAS BEEN MOVED BELOW.  "ORIGINALS" WILL GET 
//       CAUGHT SINCE THEY WILL STILL BE NULL AFTER BACKFILLS.  DO NOT CHANGE THIS
//       "NULL" UPDATE SINCE CODE BELOW KEYS OFF OF IT.
//

String sqls, column, column_list, updt_sql, where_sql1, sqls1, sqls2, sqls3, sqls4, where_sql2
longlong row, rowcount 
uo_ds_top ds_col

update cr_commitments_sum
   set commitment_id       = pwrplant1.nextval,
		 charge_audit_id     = NULL;
	
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs( "ERROR: updating cr_commitments_sum.commitment_id: " + &
	            sqlca.SQLErrText)
	f_pp_msgs( "The interface will terminate without loading commitments.")
	f_pp_msgs("  ")
	rollback;
	return -1
//else
//	commit;
end if


//
//  DMJ: HERE'S MY ATTEMPT AT A BACKFILL.  PO_NUMBER AND THE MIN(COMMITMENT_ID) SEEMS
//       LIKE A DECENT ATTEMPT AT IDENTIFYING AN UNIDENTIFIABLE PARENT.  AGAIN, THE
//       "NET" SCHEME IS NOT MY PROBLEM.
//
//       ONLY UPDATE 3 AND 4 HERE SINCE IT APPEARS THAT THEY ARE THE ONLY THINGS THAT
//       ARE SUPPOSED TO BE "ADJUSTMENTS" TO THE ORGINAL PO OR REQ.
//
//       ONLY LOOK AT THE 1'S AND 2'S IN THE INNER VIEW SINCE THEY SEEM TO BE THE ONLY
//       ONES THAT ARE ELIGIBLE TO BE THE "PARENT".
//
//       YOU HAD BETTER GET THE COMMITMENT_TYPE_ID FILLED IN USING THE MAPPINGS OR
//       THE CUSTOM FUNCTION.  I'M FILLING IN NULLS HERE WITH 1.
//
f_pp_msgs("Updating charge_audit_id at " + string(now()))

update cr_commitments_sum set commitment_type_id = 1 where commitment_type_id is null;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs( "  ERROR: Updating commitment_type_id where NULL: " + sqlca.SQLErrText)
	f_pp_msgs( "  The interface will terminate without loading the commitments table !!!")
	f_pp_msgs("  ")
	rollback;
	return -1
end if

/* Modified by Cathy for Maint 7942 (7/29/11) 
    To add an extension for client to customized their backfilling of commitment offsets to present an accurate net view 
*/
//  VS. THE ALREADY POSTED COMMITMENTS.
//update cr_commitments_sum sum
//	set charge_audit_id = 
//		(select min(commitments.commitment_id) from commitments
//		  where commitments.work_order_id = sum.work_order_id
//		  	 and commitments.po_number = sum.po_number 
//			 and commitments.commitment_type_id in (1, 2))
// where commitment_type_id in (3, 4)
// 	and (po_number, work_order_id) in
//		(select po_number, work_order_id from commitments)
//	and charge_audit_id is null;

ds_col = create uo_ds_top
sqls = "select comm_col from cr_to_comm_control where net_comm_view = 1" 
f_create_dynamic_ds(ds_col,"grid",sqls,sqlca,true) 		 

rowcount = ds_col.rowcount()
column_list = ''
where_sql1 = ''
where_sql2 = ''
FOR row = 1 TO rowcount
	column = ds_col.GetItemString(row, 'comm_col')
	
	IF row = 1 THEN 
		column_list = column 
	ELSE
		column_list += ", " + column 
	END IF
	
	where_sql1 += "and commitments." + column + " = sum." + column + " " 
	where_sql2 += "and sum2." + column + " = sum." + column + " " 
NEXT

sqls1 = "update cr_commitments_sum sum " +&
		 "set charge_audit_id = " +&
		 "(select min(commitments.commitment_id) from commitments " +&
		 "where commitments.work_order_id = sum.work_order_id " +&
		 "and commitments.po_number = sum.po_number " 

sqls2 = "and commitments.commitment_type_id in (1, 2)) " +&
		 "where commitment_type_id in (3, 4) " +&
 		"and (po_number, work_order_id "

sqls3 = ") in (select po_number, work_order_id " 

sqls4 = " from commitments)  " +&
		   "and charge_audit_id is null " 

IF column_list <> '' THEN column_list = ", " + column_list 

updt_sql = sqls1 + where_sql1 + sqls2 + column_list + sqls3 + column_list + sqls4 	

execute immediate :updt_sql;
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs( "  ERROR: Updating charge_audit_id : " + sqlca.SQLErrText)
	f_pp_msgs( "  The interface will terminate without loading the commitments table !!!")
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  VS. COMMITMENTS IN THIS RUN (IN CASE THEY HAVE AN ORIGINAL AND ADJUSTMENT TOGETHER
//  IN THIS RUN.
//update cr_commitments_sum sum
//	set charge_audit_id = 
//		(select min(sum2.commitment_id) from cr_commitments_sum sum2
//		  where sum2.work_order_id = sum.work_order_id
//		  	 and sum2.po_number = sum.po_number 
//			 and sum2.commitment_type_id in (1, 2))
// where commitment_type_id in (3, 4)
// 	and (po_number, work_order_id) in
//		(select po_number, work_order_id from cr_commitments_sum)
//	and charge_audit_id is null;

sqls1 = "update cr_commitments_sum sum " +&
	     "set charge_audit_id = " +&
		"(select min(sum2.commitment_id) from cr_commitments_sum sum2 " +&
		"where sum2.work_order_id = sum.work_order_id " +&
		"and sum2.po_number = sum.po_number " 
		
sqls2 = "and sum2.commitment_type_id in (1, 2)) " +&
         "where commitment_type_id in (3, 4) " +&
 	    "and (po_number, work_order_id"

sqls4 = " from cr_commitments_sum) " +&
		"and charge_audit_id is null " 

updt_sql = sqls1 + where_sql2 + sqls2 + column_list + sqls3 + column_list + sqls4 	
execute immediate :updt_sql;
if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs( "  ERROR: Updating charge_audit_id (2) : " + sqlca.SQLErrText)
	f_pp_msgs( "  The interface will terminate without loading the commitments table !!!")
	f_pp_msgs("  ")
	rollback;
	return -1
end if

//  This will catch the "originals" and anything that fell out above (which of course will
//  end up looking like an "original".
update cr_commitments_sum set charge_audit_id = commitment_id where charge_audit_id is null;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs( "  ERROR: Updating charge_audit_id where NULL: " + sqlca.SQLErrText)
	f_pp_msgs( "  The interface will terminate without loading the commitments table !!!")
	f_pp_msgs("  ")
	rollback;
	return -1
end if

return 1
end function

public function longlong uf_insert_final ();//*********************************************************************************************************************
//
//  User Object Function  :  uf_insert_final
//
//  Description  :  Inserts the final records into commitments
//
//*********************************************************************************************************************
string sqls
any columns[]
longlong i, num_cols

string to_table_name, from_table_name

to_table_name = 'COMMITMENTS'
from_table_name = 'CR_COMMITMENTS_SUM'

f_pp_msgs( "Inserting records from "+ lower(from_table_name) + " to " + lower(to_table_name) + " at " + string(now()))

sqls = &
	"select column_name from all_tab_columns where table_name = '" + to_table_name + "' and owner = 'PWRPLANT' " + &
	" intersect " + &
	"select column_name from all_tab_columns where table_name = '" + from_table_name + "' and owner = 'PWRPLANT' "
	
f_get_column(columns[],sqls)
	
num_cols = upperbound(columns[])

sqls = ''
for i = 1 to num_cols
	sqls += columns[i] + ','
next

sqls = mid(sqls,1,len(sqls) - 1)

sqls = 'insert into ' + to_table_name + ' (' + sqls + ') select ' + sqls + ' from ' + from_table_name

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: inserting into " + to_table_name + ": " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs(" ")
	rollback;
	return -1
else
	// log the number of records written 
	f_pp_msgs( "Inserted " + string(sqlca.sqlnrows) + " records into " + lower(to_table_name))
end if

return 1
end function

public function integer uf_insert_stg_sum ();string sqls, sqls_col_list, sqls_select, sqls_group, col
longlong i
any results[]

string to_table_name, from_table_name
to_table_name = 'CR_COMMITMENTS_SUM'
from_table_name = 'CR_COMMITMENTS_TRANSLATE'

///This pulls all the columns from all tab columns and assumes the tables (translate and sum are exactly the same columns wise)
sqls = &
	"select column_name from all_tab_columns where table_name = '" + to_table_name + "' and owner = 'PWRPLANT' " + &
	" intersect " + &
	"select column_name from all_tab_columns where table_name = '" + from_table_name + "' and owner = 'PWRPLANT' "

f_get_column(results[],sqls)

sqls_col_list = ' ( '
for i = 1 to upperbound(results)
	col = results[i]
	if col = 'TIME_STAMP' or col = 'USER_ID' then continue
	sqls_col_list += col + ', '
	if col = 'CHARGE_ID' or col = 'CHARGE_AUDIT_ID' or col = 'COMMITMENT_ID' then
		sqls_select += " 0 as " + col + ", "
	elseif col = 'QUANTITY' or col = 'AMOUNT' or col = 'HOURS' then
		sqls_select += " sum(crcct." + col + ") as " + col + ", "
	else
		sqls_select += " crcct." + col + ", "
		sqls_group += "crcct." + col + ", "
	end if
next

sqls_col_list = left(sqls_col_list,len(sqls_col_list)-2)
sqls_select = left(sqls_select,len(sqls_select)-2)
sqls_group = left(sqls_group,len(sqls_group)-2)
sqls_col_list = sqls_col_list +' ) '

// SUMMARIZE
sqls = "insert into " + to_table_name + " "
sqls += sqls_col_list
sqls += " select " + sqls_select
sqls += " from " + from_table_name + " crcct "
sqls += " group by " + sqls_group

execute immediate :sqls;

if i_debug = "YES" then
	f_write_log(g_log_file, "***DEBUG*** sqls = " + sqls)
end if
 
if sqlca.SQLCode < 0 then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: inserting into " + to_table_name + ": " + sqlca.SQLErrText)
	f_pp_msgs("The interface will terminate without loading COMMITMENTS !!!")
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs(" ")
	rollback;
	return -1
else
	// log the number of records written 
	f_pp_msgs( "Inserted " + string(sqlca.sqlnrows) + " records into " + lower(to_table_name))
end if

//f_pp_msgs( "Analyzing " + lower(to_table_name) + " at " + string(now()))
sqlca.analyze_table(to_table_name)
if sqlca.sqlcode < 0 then
	f_pp_msgs("ERROR: analyzing " + to_table_name + ":")
	f_pp_msgs("The interface will continue.")
end if

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_to_commitments_custom_version nvo_cr_to_commitments_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_to_commitments_custom.pbd'
		return nvo_cr_to_commitments_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

