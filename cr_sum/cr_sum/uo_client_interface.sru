HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
uo_ds_top i_ds_elements
longlong i_num_elements

string	i_exe_name = 'cr_sum.exe'
end variables

forward prototypes
public function longlong uf_step1 ()
public function longlong uf_read ()
public function longlong uf_step3 ()
public function longlong uf_step2 ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_step1 ();//*********************************************************************************************
//
//  Step 1  :  Insert the records into cr_cr_id
//
//  NOTES   :  This script is best performed on conversion with a company/MN index and
//					using where clauses in the code below.
//
//*********************************************************************************************
longlong i, max_id
string sqls, element_string, element

f_pp_msgs("Starting uf_step1 at: " + string(now()))

//**********************************************************
//  SETUP:
//**********************************************************
f_pp_msgs("Updating cr_id to 0 where cr_id is NULL at " + string(now()))

////  CONVERSION EXAMPLE WITH MONTH_NUMBER:
//update cr_cost_repository set cr_id = 0 
// where cr_id is NULL and month_number between 200102 and 200103;

//  PRODUCTION:
update cr_cost_repository set cr_id = 0 
 where cr_id is NULL;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_cost_repository: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if

max_id = 0
select max(cr_id) into :max_id from cr_cr_id;
if isnull(max_id) then 
	max_id = 0
else
	max_id++
end if

//**********************************************************
//  INSERT SQL:
//**********************************************************
f_pp_msgs("Inserting into cr_cr_id at " + string(now()))

sqls           = "insert into cr_cr_id (cr_id,"
element_string = ""

for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element  = lower(i_ds_elements.GetItemString(i, "description"))
	element  = f_cr_clean_string(element)
	
	if i = i_num_elements then
		element_string = element_string + '"' + upper(element) + '"'
	else
		element_string = element_string + '"' + upper(element) + '", '
	end if
	
next

sqls = sqls + element_string


//**********************************************************
//  SELECT SQL:
//**********************************************************
////  CONVERSION EXAMPLE WITH MONTH_NUMBER:
//sqls = sqls + ") (" + &
//	"select " + string(max_id) + " + rownum as cr_id, " + element_string + " from (" + &
//	"select " + element_string + &
//	 " from cr_cost_repository " + &
//	 "where cr_id = 0 and month_number between 200102 and 200103 minus " + &
//	"select " + element_string + " from cr_cr_id ))"

//  PRODUCTION:
sqls = sqls + ") (" + &
	"select " + string(max_id) + " + rownum as cr_id, " + element_string + " from (" + &
	"select " + element_string + &
	 " from cr_cost_repository " + &
	 "where cr_id = 0 minus " + &
	"select " + element_string + " from cr_cr_id ))"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: " + sqlca.SQLErrText)
	f_pp_msgs("sqls : " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

commit;

f_pp_msgs("uf_step1 finished at: " + string(now()))

return 1
end function

public function longlong uf_read ();longlong rtn
string   s_date, sqls
date     ddate
time     ttime

//************************************************************************
//  Setup
//************************************************************************
i_ds_elements = CREATE uo_ds_top
sqls = 'select * from cr_elements order by "ORDER" '
f_create_dynamic_ds(i_ds_elements, "grid", sqls, sqlca, true)
i_num_elements = i_ds_elements.RowCount()


//************************************************************************
//  Call step 1:  Insert the records into cr_cr_id.
//************************************************************************
f_pp_msgs("--------------------------------")
rtn = uf_step1()
if rtn <> 1 then return -1


//************************************************************************
//  Call step 2:  Update the cr_id field on cr_cost_repository.
//************************************************************************
f_pp_msgs("--------------------------------")
rtn = uf_step2()
if rtn <> 1 then return -1


//************************************************************************
//  Call step 3:  Insert into cr_sum.
//************************************************************************
f_pp_msgs("--------------------------------")
rtn = uf_step3()
if rtn <> 1 then return -1

f_pp_msgs(" ")
f_pp_msgs("The CR_SUM table was successfully built.")
return 1
end function

public function longlong uf_step3 ();//*********************************************************************************************
//
//  Step 3  :  Insert into cr_sum.
//
//  NOTES   :  
//
//    py_curr_qtr ... 
//      * ytd_ind03 * the_qtr_ind01 ??? to prevent 03 from entering curr qtr if 199902 ???
//
//*********************************************************************************************
longlong current_month, beg_12_month, i, current_pd, &
		  max_cr_id, beg_bdg_month, end_bdg_month, cr_sum_count, initial_fy_month
integer curr_pd_ind01, curr_pd_ind02, curr_pd_ind03, curr_pd_ind04, &
        curr_pd_ind05, curr_pd_ind06, curr_pd_ind07, curr_pd_ind08, &
		  curr_pd_ind09, curr_pd_ind10, curr_pd_ind11, curr_pd_ind12, &
		  ytd_ind01, ytd_ind02, ytd_ind03, ytd_ind04, &
        ytd_ind05, ytd_ind06, ytd_ind07, ytd_ind08, &
		  ytd_ind09, ytd_ind10, ytd_ind11, ytd_ind12, &
		  the_qtr_ind01, the_qtr_ind02, the_qtr_ind03, the_qtr_ind04
string  sqls, sqls_bdg, element, element_string, ix_sqls, ix_sqls_bdg, &
		  element_bdg, element_string_bdg, original_bv, revised_bv, &
		  ix_tablespace, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, &
		  prior_m1, prior_m2, prior_m3, prior_m4, prior_m5, prior_m6, &
		  prior_m7, prior_m8, prior_m9, prior_m10, prior_m11, prior_m12, &
		  mmm_array[]

f_pp_msgs("Starting uf_step3 at: " + string(now()))


//*****************************************************************
//  0:  Setup stuff.
//*****************************************************************
f_pp_msgs("Performing setup at " + string(now()))
//-------  Get the current open month.     --------------
current_month = 0
select to_number(rtrim(control_value)) into :current_month
  from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'CURRENT OPEN MONTH';

if isnull(current_month) or current_month = 0 then
	f_pp_msgs("ERROR (0): could not find the CURRENT OPEN MONTH.")
	return -1
end if

initial_fy_month = 1
select to_number(rtrim(control_value)) into :initial_fy_month
  from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'INITIAL MONTH OF FISCAL YEAR';

if isnull(initial_fy_month) or initial_fy_month < 1 then
	initial_fy_month = 1
end if

if initial_fy_month > 12 then
	initial_fy_month = 12
end if


//-------  Set up the current_pd variable  --------------
//-------  FISCAL:  Need code here to xlate the period to a fiscal period (e.g. pd 7 xlates to 1)
//-------           Then the indicators will get build correctly.
//-------           Below, the indicators will not be able to be applied directly to the 
//-------           hardcoded months.  Rather, we need 12 string variable (m1, m2, etc.)
//-------           where m1 would be July ... and change the code to be of the form: 
//-------           m1 + " * " + string(ytd_ind01) + &
current_pd = long(right(string(current_month),2))

mmm_array[1]  = "JAN"
mmm_array[2]  = "FEB"
mmm_array[3]  = "MAR"
mmm_array[4]  = "APR"
mmm_array[5]  = "MAY"
mmm_array[6]  = "JUN"
mmm_array[7]  = "JUL"
mmm_array[8]  = "AUG"
mmm_array[9]  = "SEP"
mmm_array[10] = "OCT"
mmm_array[11] = "NOV"
mmm_array[12] = "DEC"

m1       =            mmm_array[initial_fy_month]
prior_m1 = "prior_" + mmm_array[initial_fy_month]

if initial_fy_month + 1 <= 12 then
	m2       =            mmm_array[initial_fy_month + 1]
	prior_m2 = "prior_" + mmm_array[initial_fy_month + 1]
else
	m2       =            mmm_array[(initial_fy_month + 1) - 12]
	prior_m2 = "prior_" + mmm_array[(initial_fy_month + 1) - 12]
end if

if initial_fy_month + 2 <= 12 then
	m3       =            mmm_array[initial_fy_month + 2]
	prior_m3 = "prior_" + mmm_array[initial_fy_month + 2]
else
	m3       =            mmm_array[(initial_fy_month + 2) - 12]
	prior_m3 = "prior_" + mmm_array[(initial_fy_month + 2) - 12]
end if

if initial_fy_month + 3 <= 12 then
	m4       =            mmm_array[initial_fy_month + 3]
	prior_m4 = "prior_" + mmm_array[initial_fy_month + 3]
else
	m4       =            mmm_array[(initial_fy_month + 3) - 12]
	prior_m4 = "prior_" + mmm_array[(initial_fy_month + 3) - 12]
end if

if initial_fy_month + 4 <= 12 then
	m5       =            mmm_array[initial_fy_month + 4]
	prior_m5 = "prior_" + mmm_array[initial_fy_month + 4]
else
	m5       =            mmm_array[(initial_fy_month + 4) - 12]
	prior_m5 = "prior_" + mmm_array[(initial_fy_month + 4) - 12]
end if

if initial_fy_month + 5 <= 12 then
	m6       =            mmm_array[initial_fy_month + 5]
	prior_m6 = "prior_" + mmm_array[initial_fy_month + 5]
else
	m6       =            mmm_array[(initial_fy_month + 5) - 12]
	prior_m6 = "prior_" + mmm_array[(initial_fy_month + 5) - 12]
end if

if initial_fy_month + 6 <= 12 then
	m7       =            mmm_array[initial_fy_month + 6]
	prior_m7 = "prior_" + mmm_array[initial_fy_month + 6]
else
	m7       =            mmm_array[(initial_fy_month + 6) - 12]
	prior_m7 = "prior_" + mmm_array[(initial_fy_month + 6) - 12]
end if

if initial_fy_month + 7 <= 12 then
	m8       =            mmm_array[initial_fy_month + 7]
	prior_m8 = "prior_" + mmm_array[initial_fy_month + 7]
else
	m8       =            mmm_array[(initial_fy_month + 7) - 12]
	prior_m8 = "prior_" + mmm_array[(initial_fy_month + 7) - 12]
end if

if initial_fy_month + 8 <= 12 then
	m9       =            mmm_array[initial_fy_month + 8]
	prior_m9 = "prior_" + mmm_array[initial_fy_month + 8]
else
	m9       =            mmm_array[(initial_fy_month + 8) - 12]
	prior_m9 = "prior_" + mmm_array[(initial_fy_month + 8) - 12]
end if

if initial_fy_month + 9 <= 12 then
	m10       =            mmm_array[initial_fy_month + 9]
	prior_m10 = "prior_" + mmm_array[initial_fy_month + 9]
else
	m10       =            mmm_array[(initial_fy_month + 9) - 12]
	prior_m10 = "prior_" + mmm_array[(initial_fy_month + 9) - 12]
end if

if initial_fy_month + 10 <= 12 then
	m11       =            mmm_array[initial_fy_month + 10]
	prior_m11 = "prior_" + mmm_array[initial_fy_month + 10]
else
	m11       =            mmm_array[(initial_fy_month + 10) - 12]
	prior_m11 = "prior_" + mmm_array[(initial_fy_month + 10) - 12]
end if

if initial_fy_month + 11 <= 12 then
	m12       =            mmm_array[initial_fy_month + 11]
	prior_m12 = "prior_" + mmm_array[initial_fy_month + 11]
else
	m12       =            mmm_array[(initial_fy_month + 11) - 12]
	prior_m12 = "prior_" + mmm_array[(initial_fy_month + 11) - 12]
end if


//-------  The current_pd variable depends on the intial FY month.  --------------
if current_pd >= initial_fy_month then
	current_pd = (current_pd - initial_fy_month) + 1
else
	current_pd = current_pd + ( (12 - initial_fy_month) + 1 )
end if


//-------  Set the beg_12_month variable.  --------------
if right(string(current_month), 2) = "12" then
	beg_12_month = current_month - 11
else
	beg_12_month = current_month - 99
end if


f_pp_msgs("  -- current_month = " + string(current_month))
f_pp_msgs("  -- beg_12_month  = " + string(beg_12_month))


//-------  Get the Original Budget Version.  --------------
select control_value into :original_bv from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'ORIGINAL BUDGET VERSION';
 

//-------  Get the Revised Budget Version.  --------------
select control_value into :revised_bv from cr_alloc_system_control
 where upper(rtrim(control_name)) = 'REVISED BUDGET VERSION';


//-------  Truncate the cr_sum table.      --------------
f_pp_msgs("truncating cr_sum at " + string(now()))

g_sqlca_logs.truncate_table("cr_sum")

select count(*) into :cr_sum_count from cr_sum;

if cr_sum_count > 0 then
	f_pp_msgs("ERROR: (0) truncating table cr_sum: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if


//-------  Indicators  ----------------------------------
select 1 - abs(sign( 1 - :current_pd)) into :curr_pd_ind01 from dual;
select 1 - abs(sign( 2 - :current_pd)) into :curr_pd_ind02 from dual;
select 1 - abs(sign( 3 - :current_pd)) into :curr_pd_ind03 from dual;
select 1 - abs(sign( 4 - :current_pd)) into :curr_pd_ind04 from dual;
select 1 - abs(sign( 5 - :current_pd)) into :curr_pd_ind05 from dual;
select 1 - abs(sign( 6 - :current_pd)) into :curr_pd_ind06 from dual;
select 1 - abs(sign( 7 - :current_pd)) into :curr_pd_ind07 from dual;
select 1 - abs(sign( 8 - :current_pd)) into :curr_pd_ind08 from dual;
select 1 - abs(sign( 9 - :current_pd)) into :curr_pd_ind09 from dual;
select 1 - abs(sign(10 - :current_pd)) into :curr_pd_ind10 from dual;
select 1 - abs(sign(11 - :current_pd)) into :curr_pd_ind11 from dual;
select 1 - abs(sign(12 - :current_pd)) into :curr_pd_ind12 from dual;

select sign(1 + sign( 1 - :current_pd) * -1) into :ytd_ind01 from dual;
select sign(1 + sign( 2 - :current_pd) * -1) into :ytd_ind02 from dual;
select sign(1 + sign( 3 - :current_pd) * -1) into :ytd_ind03 from dual;
select sign(1 + sign( 4 - :current_pd) * -1) into :ytd_ind04 from dual;
select sign(1 + sign( 5 - :current_pd) * -1) into :ytd_ind05 from dual;
select sign(1 + sign( 6 - :current_pd) * -1) into :ytd_ind06 from dual;
select sign(1 + sign( 7 - :current_pd) * -1) into :ytd_ind07 from dual;
select sign(1 + sign( 8 - :current_pd) * -1) into :ytd_ind08 from dual;
select sign(1 + sign( 9 - :current_pd) * -1) into :ytd_ind09 from dual;
select sign(1 + sign(10 - :current_pd) * -1) into :ytd_ind10 from dual;
select sign(1 + sign(11 - :current_pd) * -1) into :ytd_ind11 from dual;
select sign(1 + sign(12 - :current_pd) * -1) into :ytd_ind12 from dual;

the_qtr_ind01 = curr_pd_ind01 + curr_pd_ind02 + curr_pd_ind03
the_qtr_ind02 = curr_pd_ind04 + curr_pd_ind05 + curr_pd_ind06
the_qtr_ind03 = curr_pd_ind07 + curr_pd_ind08 + curr_pd_ind09
the_qtr_ind04 = curr_pd_ind10 + curr_pd_ind11 + curr_pd_ind12


////*****************************************************************
////  1:  Insert 0 dollar records from CR_CR_ID.
////*****************************************************************
//f_pp_msgs("Inserting initial records into cr_sum.")
//
//insert into cr_sum 
//	select cr_cr_id.*,
//	       :current_month as month_number,
//	       0 as cy_beg_bal,
//			 0 as cy_curr_mo,
//			 0 as cy_prior_mo,
//			 0 as cy_curr_qtr,
//			 0 as cy_ytd,
//			 0 as cy_itd,
//			 0 as cy_tme,
//			 0 as cy_q1,
//			 0 as cy_q2,
//			 0 as cy_q3,
//			 0 as cy_q4,
//			 0 as cy_curr_mo_original_bdg,
//			 0 as cy_ytd_original_bdg,
//			 0 as cy_curr_mo_revised_bdg,
//			 0 as cy_ytd_revised_bdg,
//			 0 as py_curr_mo,
//			 0 as py_prior_mo,
//			 0 as py_curr_qtr,
//			 0 as py_ytd,
//			 0 as py_itd,
//			 0 as py_tme,
//			 0 as py_q1,
//			 0 as py_q2,
//			 0 as py_q3,
//			 0 as py_q4
//	  from cr_cr_id;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: (1) inserting into cr_sum: " + sqlca.SQLErrText)
//	rollback;
//	return
//else
//	commit;
//end if


//*****************************************************************
//  2:  Create a "de-normalized" copy of cr_cost_repository,
//      throwing the monthly amounts out accross the table.
//      This table contains a range of dollars that is the
//      12 months of history.
//*****************************************************************
f_pp_msgs("Creating the cr_sum_temp table at " + string(now()))

setnull(ix_tablespace)
select trim(control_value) into :ix_tablespace
  from cr_system_control where control_name = 'CR Indexes Tablespace';
if isnull(ix_tablespace) or ix_tablespace = "" then
	ix_tablespace = "pwrplant_idx"
end if

sqls     = "drop table cr_sum_temp"
execute immediate :sqls;

if sqlca.SQLCode < 0 then
	//  The table did not exist ... simply rollback to reset 
	//  the sqlca.SQLCode.
	rollback;
end if

sqls_bdg = "drop table cr_sum_temp_bdg"
execute immediate :sqls_bdg;

if sqlca.SQLCode < 0 then
	//  The table did not exist ... simply rollback to reset 
	//  the sqlca.SQLCode.
	rollback;
end if

sqls               = "create table cr_sum_temp storage (initial 10M next 1M) as select "
sqls_bdg           = "create table cr_sum_temp_bdg storage (initial 5M next 1M) as select "
ix_sqls            = 'create unique index cr_sum_temp_ix on cr_sum_temp (' 
ix_sqls_bdg        = 'create unique index cr_sum_temp_bdg_ix on cr_sum_temp_bdg (' 
element_string     = ""
element_string_bdg = ""

for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element     = lower(i_ds_elements.GetItemString(i, "description"))
	element     = f_cr_clean_string(element)
	element_bdg = lower(i_ds_elements.GetItemString(i, "budgeting_element"))
	element_bdg = f_cr_clean_string(element_bdg)
		
	element_string     = element_string     + '"' + upper(element)     + '", '
	element_string_bdg = element_string_bdg + '"' + upper(element_bdg) + '", '
	
	if i = i_num_elements then
		ix_sqls     = ix_sqls     + '"' + upper(element)     + '" '
		ix_sqls_bdg = ix_sqls_bdg + '"' + upper(element_bdg) + '" '
	else
		ix_sqls     = ix_sqls     + '"' + upper(element)     + '", '
		ix_sqls_bdg = ix_sqls_bdg + '"' + upper(element_bdg) + '", '
	end if
	
next

ix_sqls     = ix_sqls     + ") tablespace " + ix_tablespace
ix_sqls_bdg = ix_sqls_bdg + ") tablespace " + ix_tablespace

//  I used to have "to_number(substr(month_number,1,4)) as year, " + & in the SQL for the
//  year field.  It is not clear what I was thinking.  By definition, to get amounts like TME,
//  the Jan-Dec amounts must cross years.  This has been changed to 0 below to avoid errors
//  in subsequent code that multiple records for two years was causing.
sqls = sqls + element_string + &
	 "sum(prior_jan) prior_jan,sum(prior_feb) prior_feb,sum(prior_mar) prior_mar," + &
	 "sum(prior_apr) prior_apr,sum(prior_may) prior_may,sum(prior_jun) prior_jun," + &
	 "sum(prior_jul) prior_jul,sum(prior_aug) prior_aug,sum(prior_sep) prior_sep," + &
	 "sum(prior_oct) prior_oct,sum(prior_nov) prior_nov,sum(prior_dec) prior_dec," + &
	 "sum(jan) jan,sum(feb) feb,sum(mar) mar,sum(apr) apr,sum(may) may,sum(jun) jun," + &
	 "sum(jul) jul,sum(aug) aug,sum(sep) sep,sum(oct) oct,sum(nov) nov,sum(dec) dec from (" + &
	 " select " + element_string + &
	 "0 as year, " + &
	 "0 as prior_jan, " + &
	 "0 as prior_feb, " + &
	 "0 as prior_mar, " + &
	 "0 as prior_apr, " + &
	 "0 as prior_may, " + &
	 "0 as prior_jun, " + &
	 "0 as prior_jul, " + &
	 "0 as prior_aug, " + &
	 "0 as prior_sep, " + &
	 "0 as prior_oct, " + &
	 "0 as prior_nov, " + &
	 "0 as prior_dec, " + &
	 "sum(decode(substr(month_number,5,2), '01', amount, 0)) as jan, " + &
	 "sum(decode(substr(month_number,5,2), '02', amount, 0)) as feb, " + &
	 "sum(decode(substr(month_number,5,2), '03', amount, 0)) as mar, " + &
	 "sum(decode(substr(month_number,5,2), '04', amount, 0)) as apr, " + &
	 "sum(decode(substr(month_number,5,2), '05', amount, 0)) as may, " + &
	 "sum(decode(substr(month_number,5,2), '06', amount, 0)) as jun, " + &
	 "sum(decode(substr(month_number,5,2), '07', amount, 0)) as jul, " + &
	 "sum(decode(substr(month_number,5,2), '08', amount, 0)) as aug, " + &
	 "sum(decode(substr(month_number,5,2), '09', amount, 0)) as sep, " + &
	 "sum(decode(substr(month_number,5,2), '10', amount, 0)) as oct, " + &
	 "sum(decode(substr(month_number,5,2), '11', amount, 0)) as nov, " + &
	 "sum(decode(substr(month_number,5,2), '12', amount, 0)) as dec " + &
"from cr_cost_repository where amount_type = 1 and month_number between " + &
											string(beg_12_month) + " and " + &
											string(current_month) + " " + &
"group by " + element_string + "0 " + &
" union " + &
"select " + element_string + &
	 "-1 as year, " + &
	 "sum(decode(substr(month_number,5,2), '01', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '02', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '03', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '04', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '05', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '06', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '07', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '08', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '09', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '10', amount, 0))," + &
	 "sum(decode(substr(month_number,5,2), '11', amount, 0))," + &
	 "sum(decode(substr(month_number,5,2), '12', amount, 0))," + &
    "0,0,0,0,0,0,0,0,0,0,0,0 " + &
"from cr_cost_repository where amount_type = 1 and month_number between " + &
											string(beg_12_month - 100) + " and " + &
											string(current_month - 100) + " " + &
"group by " + element_string + "-1) " + &
"group by " + element_string

//  Trim final comma:
sqls = left(sqls, len(sqls) - 2)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: (2) creating cr_sum_temp: " + sqlca.SQLErrText)
	f_pp_msgs("sqls: " + sqls)
	rollback;
	return -1
else
	commit;
end if



f_pp_msgs("Creating the cr_sum_temp_bdg table at " + string(now()))

select round(:current_month / 100) * 100 + 1  into :beg_bdg_month from dual;
select round(:current_month / 100) * 100 + 12 into :end_bdg_month from dual;

if isnull(beg_bdg_month) then beg_bdg_month = 0
if isnull(end_bdg_month) then end_bdg_month = 0

sqls_bdg = sqls_bdg + element_string_bdg + &
	 "sum(prior_jan) prior_jan,sum(prior_feb) prior_feb,sum(prior_mar) prior_mar," + &
	 "sum(prior_apr) prior_apr,sum(prior_may) prior_may,sum(prior_jun) prior_jun," + &
	 "sum(prior_jul) prior_jul,sum(prior_aug) prior_aug,sum(prior_sep) prior_sep," + &
	 "sum(prior_oct) prior_oct,sum(prior_nov) prior_nov,sum(prior_dec) prior_dec," + &
	 "sum(jan) jan,sum(feb) feb,sum(mar) mar,sum(apr) apr,sum(may) may,sum(jun) jun," + &
	 "sum(jul) jul,sum(aug) aug,sum(sep) sep,sum(oct) oct,sum(nov) nov,sum(dec) dec from (" + &
	 " select " + element_string_bdg + &
	 "0 as year, " + &
	 "0 as prior_jan, " + &
	 "0 as prior_feb, " + &
	 "0 as prior_mar, " + &
	 "0 as prior_apr, " + &
	 "0 as prior_may, " + &
	 "0 as prior_jun, " + &
	 "0 as prior_jul, " + &
	 "0 as prior_aug, " + &
	 "0 as prior_sep, " + &
	 "0 as prior_oct, " + &
	 "0 as prior_nov, " + &
	 "0 as prior_dec, " + &
	 "sum(decode(substr(month_number,5,2), '01', amount, 0)) as jan, " + &
	 "sum(decode(substr(month_number,5,2), '02', amount, 0)) as feb, " + &
	 "sum(decode(substr(month_number,5,2), '03', amount, 0)) as mar, " + &
	 "sum(decode(substr(month_number,5,2), '04', amount, 0)) as apr, " + &
	 "sum(decode(substr(month_number,5,2), '05', amount, 0)) as may, " + &
	 "sum(decode(substr(month_number,5,2), '06', amount, 0)) as jun, " + &
	 "sum(decode(substr(month_number,5,2), '07', amount, 0)) as jul, " + &
	 "sum(decode(substr(month_number,5,2), '08', amount, 0)) as aug, " + &
	 "sum(decode(substr(month_number,5,2), '09', amount, 0)) as sep, " + &
	 "sum(decode(substr(month_number,5,2), '10', amount, 0)) as oct, " + &
	 "sum(decode(substr(month_number,5,2), '11', amount, 0)) as nov, " + &
	 "sum(decode(substr(month_number,5,2), '12', amount, 0)) as dec " + &
"from cr_budget_data where budget_version = '" + original_bv + "' " + &
						   " and month_number between " + &
											string(beg_bdg_month) + " and " + &
											string(end_bdg_month) + " " + &
"group by " + element_string_bdg + "0 " + &
" union " + &
"select " + element_string_bdg + &
	 "-1 as year, " + &
	 "sum(decode(substr(month_number,5,2), '01', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '02', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '03', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '04', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '05', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '06', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '07', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '08', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '09', amount, 0)), " + &
	 "sum(decode(substr(month_number,5,2), '10', amount, 0))," + &
	 "sum(decode(substr(month_number,5,2), '11', amount, 0))," + &
	 "sum(decode(substr(month_number,5,2), '12', amount, 0))," + &
    "0,0,0,0,0,0,0,0,0,0,0,0 " + &
"from cr_budget_data where budget_version = '" + revised_bv + "' " + &
						   " and month_number between " + &
											string(beg_bdg_month) + " and " + &
											string(end_bdg_month) + " " + &
"group by " + element_string_bdg + "-1) " + &
"group by " + element_string_bdg

//  Trim final comma:
sqls_bdg = left(sqls_bdg, len(sqls_bdg) - 2)

execute immediate :sqls_bdg;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: (2) creating cr_sum_temp_bdg: " + sqlca.SQLErrText)
	f_pp_msgs("sqls_bdg: " + sqls_bdg)
	rollback;
	return -1
else
	commit;
end if



//  DMJ:  ADDED CODE TO DELETE ALL-ZERO RECORDS FOR PERFORMANCE.
f_pp_msgs("Deleting from cr_sum_temp where 0 at " + string(now()))

delete from cr_sum_temp
 where prior_jan = 0 and prior_feb = 0 and prior_mar = 0 
 	and prior_apr = 0 and prior_may = 0 and prior_jun = 0
	and prior_jul = 0 and prior_aug = 0 and prior_sep = 0
	and prior_oct = 0 and prior_nov = 0 and prior_dec = 0
	and jan = 0 and feb = 0 and mar = 0 
	and apr = 0 and may = 0 and jun = 0
	and jul = 0 and aug = 0 and sep = 0
	and oct = 0 and nov = 0 and "DEC" = 0;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: deleting form cr_sum_temp: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if

f_pp_msgs("Deleting from cr_sum_temp_bdg where 0 at " + string(now()))

delete from cr_sum_temp_bdg
 where prior_jan = 0 and prior_feb = 0 and prior_mar = 0 
 	and prior_apr = 0 and prior_may = 0 and prior_jun = 0
	and prior_jul = 0 and prior_aug = 0 and prior_sep = 0
	and prior_oct = 0 and prior_nov = 0 and prior_dec = 0
	and jan = 0 and feb = 0 and mar = 0 
	and apr = 0 and may = 0 and jun = 0
	and jul = 0 and aug = 0 and sep = 0
	and oct = 0 and nov = 0 and "DEC" = 0;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: deleting form cr_sum_temp_bdg: " + sqlca.SQLErrText)
	rollback;
	return -1
else
	commit;
end if



f_pp_msgs("Creating unique index on cr_sum_temp table at " + string(now()))

execute immediate :ix_sqls;
		
if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("Error: cr_sum_temp: creating (unique IX): " + sqlca.SQLErrText)
	f_pp_msgs("ix_sqls: " + ix_sqls)
	rollback;
	return -1
end if



f_pp_msgs("Creating unique index on cr_sum_temp_bdg table at " + string(now()))

execute immediate :ix_sqls_bdg;
		
if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("Error: cr_sum_temp_bdg: creating (unique IX): " + sqlca.SQLErrText)
	f_pp_msgs("ix_sqls_bdg: " + ix_sqls_bdg)
	rollback;
	return -1
end if


//*****************************************************************
//  3:  Insert into cr_sum.
//*****************************************************************
f_pp_msgs("Inserting into cr_sum at " + string(now()))

sqls           = "insert into cr_sum (cr_id,month_number,"
element_string = ""

for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element  = lower(i_ds_elements.GetItemString(i, "description"))
	element  = f_cr_clean_string(element)
	
	sqls = sqls + '"' + upper(element) + '", '
	element_string = element_string + 'a."' + upper(element) + '", '
		
next

sqls = sqls + " cy_curr_mo, cy_prior_mo, cy_curr_qtr, cy_ytd, cy_itd, cy_tme, " + &
					"cy_q1, cy_q2, cy_q3, cy_q4, " + &
					"py_curr_mo, py_prior_mo, py_curr_qtr, py_ytd, py_itd, py_tme, " + &
					"py_q1, py_q2, py_q3, py_q4, cy_curr_mo_original_bdg, cy_ytd_original_bdg, " + &
					"cy_tot_yr_original_bdg, cy_curr_mo_revised_bdg, cy_ytd_revised_bdg, " + &
					"cy_tot_yr_revised_bdg) " + &
" (select b.cr_id," + string(current_month) + "," + element_string + &
'   "' +  m1 + '" * ' + string(curr_pd_ind01) + &
' + "' +  m2 + '" * ' + string(curr_pd_ind02) + &
' + "' +  m3 + '" * ' + string(curr_pd_ind03) + &
' + "' +  m4 + '" * ' + string(curr_pd_ind04) + &
' + "' +  m5 + '" * ' + string(curr_pd_ind05) + &
' + "' +  m6 + '" * ' + string(curr_pd_ind06) + &
' + "' +  m7 + '" * ' + string(curr_pd_ind07) + &
' + "' +  m8 + '" * ' + string(curr_pd_ind08) + &
' + "' +  m9 + '" * ' + string(curr_pd_ind09) + &
' + "' + m10 + '" * ' + string(curr_pd_ind10) + &
' + "' + m11 + '" * ' + string(curr_pd_ind11) + &
' + "' + m12 + '" * ' + string(curr_pd_ind12) + ' as cy_curr_mo, ' + &
' + "' +  m1 + '" * ' + string(curr_pd_ind02) + &
' + "' +  m2 + '" * ' + string(curr_pd_ind03) + &
' + "' +  m3 + '" * ' + string(curr_pd_ind04) + &
' + "' +  m4 + '" * ' + string(curr_pd_ind05) + &
' + "' +  m5 + '" * ' + string(curr_pd_ind06) + &
' + "' +  m6 + '" * ' + string(curr_pd_ind07) + &
' + "' +  m7 + '" * ' + string(curr_pd_ind08) + &
' + "' +  m8 + '" * ' + string(curr_pd_ind09) + &
' + "' +  m9 + '" * ' + string(curr_pd_ind10) + &
' + "' + m10 + '" * ' + string(curr_pd_ind11) + &
' + "' + m11 + '" * ' + string(curr_pd_ind12) + &
' + "' + m12 + '" * ' + string(curr_pd_ind01) + ' as cy_prior_mo, ' + &
' + "' +  m1 + '" * ' + string(ytd_ind01 * the_qtr_ind01) + &
' + "' +  m2 + '" * ' + string(ytd_ind02 * the_qtr_ind01) + &
' + "' +  m3 + '" * ' + string(ytd_ind03 * the_qtr_ind01) + &
' + "' +  m4 + '" * ' + string(ytd_ind04 * the_qtr_ind02) + &
' + "' +  m5 + '" * ' + string(ytd_ind05 * the_qtr_ind02) + &
' + "' +  m6 + '" * ' + string(ytd_ind06 * the_qtr_ind02) + &
' + "' +  m7 + '" * ' + string(ytd_ind07 * the_qtr_ind03) + &
' + "' +  m8 + '" * ' + string(ytd_ind08 * the_qtr_ind03) + &
' + "' +  m9 + '" * ' + string(ytd_ind09 * the_qtr_ind03) + &
' + "' + m10 + '" * ' + string(ytd_ind10 * the_qtr_ind04) + &
' + "' + m11 + '" * ' + string(ytd_ind11 * the_qtr_ind04) + &
' + "' + m12 + '" * ' + string(ytd_ind12 * the_qtr_ind04) + ' as cy_curr_qtr, ' + &
' + "' +  m1 + '" * ' + string(ytd_ind01) + &
' + "' +  m2 + '" * ' + string(ytd_ind02) + &
' + "' +  m3 + '" * ' + string(ytd_ind03) + &
' + "' +  m4 + '" * ' + string(ytd_ind04) + &
' + "' +  m5 + '" * ' + string(ytd_ind05) + &
' + "' +  m6 + '" * ' + string(ytd_ind06) + &
' + "' +  m7 + '" * ' + string(ytd_ind07) + &
' + "' +  m8 + '" * ' + string(ytd_ind08) + &
' + "' +  m9 + '" * ' + string(ytd_ind09) + &
' + "' + m10 + '" * ' + string(ytd_ind10) + &
' + "' + m11 + '" * ' + string(ytd_ind11) + &
' + "' + m12 + '" * ' + string(ytd_ind12) + ' as cy_ytd, ' + &
' + "' +  m1 + '" * ' + string(ytd_ind01) + &
' + "' +  m2 + '" * ' + string(ytd_ind02) + &
' + "' +  m3 + '" * ' + string(ytd_ind03) + &
' + "' +  m4 + '" * ' + string(ytd_ind04) + &
' + "' +  m5 + '" * ' + string(ytd_ind05) + &
' + "' +  m6 + '" * ' + string(ytd_ind06) + &
' + "' +  m7 + '" * ' + string(ytd_ind07) + &
' + "' +  m8 + '" * ' + string(ytd_ind08) + &
' + "' +  m9 + '" * ' + string(ytd_ind09) + &
' + "' + m10 + '" * ' + string(ytd_ind10) + &
' + "' + m11 + '" * ' + string(ytd_ind11) + &
' + "' + m12 + '" * ' + string(ytd_ind12) + ' as cy_itd, ' + &
'   "' + m1 + '" + "' +  m2 + '" + "' +  m3 + '" + "' +  m4 + '" ' + &
' + "' + m5 + '" + "' +  m6 + '" + "' +  m7 + '" + "' +  m8 + '" ' + &
' + "' + m9 + '" + "' + m10 + '" + "' + m11 + '" + "' + m12 + '" ' + &
											   ' as cy_tme, ' + &
' + "' +  m1 + '" * ' + string(ytd_ind01) + &
' + "' +  m2 + '" * ' + string(ytd_ind02) + &
' + "' +  m3 + '" * ' + string(ytd_ind03) + ' as cy_q1, ' + &
' + "' +  m4 + '" * ' + string(ytd_ind04) + &
' + "' +  m5 + '" * ' + string(ytd_ind05) + &
' + "' +  m6 + '" * ' + string(ytd_ind06) + ' as cy_q2, ' + &
' + "' +  m7 + '" * ' + string(ytd_ind07) + &
' + "' +  m8 + '" * ' + string(ytd_ind08) + &
' + "' +  m9 + '" * ' + string(ytd_ind09) + ' as cy_q3, ' + &
' + "' + m10 + '" * ' + string(ytd_ind10) + &
' + "' + m11 + '" * ' + string(ytd_ind11) + &
' + "' + m12 + '" * ' + string(ytd_ind12) + ' as cy_q4, ' + &
"   " +  prior_m1 + " * " + string(curr_pd_ind01) + &
" + " +  prior_m2 + " * " + string(curr_pd_ind02) + &
" + " +  prior_m3 + " * " + string(curr_pd_ind03) + &
" + " +  prior_m4 + " * " + string(curr_pd_ind04) + &
" + " +  prior_m5 + " * " + string(curr_pd_ind05) + &
" + " +  prior_m6 + " * " + string(curr_pd_ind06) + &
" + " +  prior_m7 + " * " + string(curr_pd_ind07) + &
" + " +  prior_m8 + " * " + string(curr_pd_ind08) + &
" + " +  prior_m9 + " * " + string(curr_pd_ind09) + &
" + " + prior_m10 + " * " + string(curr_pd_ind10) + &
" + " + prior_m11 + " * " + string(curr_pd_ind11) + &
' + ' + prior_m12 + ' * ' + string(curr_pd_ind12) + ' as py_curr_mo, ' + &
"   " +  prior_m1 + " * " + string(curr_pd_ind02) + &
" + " +  prior_m2 + " * " + string(curr_pd_ind03) + &
" + " +  prior_m3 + " * " + string(curr_pd_ind04) + &
" + " +  prior_m4 + " * " + string(curr_pd_ind05) + &
" + " +  prior_m5 + " * " + string(curr_pd_ind06) + &
" + " +  prior_m6 + " * " + string(curr_pd_ind07) + &
" + " +  prior_m7 + " * " + string(curr_pd_ind08) + &
" + " +  prior_m8 + " * " + string(curr_pd_ind09) + &
" + " +  prior_m9 + " * " + string(curr_pd_ind10) + &
" + " + prior_m10 + " * " + string(curr_pd_ind11) + &
" + " + prior_m11 + " * " + string(curr_pd_ind12) + &
' + ' + prior_m12 + ' * ' + string(curr_pd_ind01) + ' as py_prior_mo, ' + &
"   " +  prior_m1 + " * " + string(ytd_ind01 * the_qtr_ind01) + &
" + " +  prior_m2 + " * " + string(ytd_ind02 * the_qtr_ind01) + &
" + " +  prior_m3 + " * " + string(ytd_ind03 * the_qtr_ind01) + &
" + " +  prior_m4 + " * " + string(ytd_ind04 * the_qtr_ind02) + &
" + " +  prior_m5 + " * " + string(ytd_ind05 * the_qtr_ind02) + &
" + " +  prior_m6 + " * " + string(ytd_ind06 * the_qtr_ind02) + &
" + " +  prior_m7 + " * " + string(ytd_ind07 * the_qtr_ind03) + &
" + " +  prior_m8 + " * " + string(ytd_ind08 * the_qtr_ind03) + &
" + " +  prior_m9 + " * " + string(ytd_ind09 * the_qtr_ind03) + &
" + " +  prior_m10 + " * " + string(ytd_ind10 * the_qtr_ind04) + &
" + " +  prior_m11 + " * " + string(ytd_ind11 * the_qtr_ind04) + &
" + " +  prior_m12 + " * " + string(ytd_ind12 * the_qtr_ind04) + &
' + "' +  m1 + '" * ' + string((1 - ytd_ind01) * the_qtr_ind01) + &
' + "' +  m2 + '" * ' + string((1 - ytd_ind02) * the_qtr_ind01) + &
' + "' +  m3 + '" * ' + string((1 - ytd_ind03) * the_qtr_ind01) + &
' + "' +  m4 + '" * ' + string((1 - ytd_ind04) * the_qtr_ind02) + &
' + "' +  m5 + '" * ' + string((1 - ytd_ind05) * the_qtr_ind02) + &
' + "' +  m6 + '" * ' + string((1 - ytd_ind06) * the_qtr_ind02) + &
' + "' +  m7 + '" * ' + string((1 - ytd_ind07) * the_qtr_ind03) + &
' + "' +  m8 + '" * ' + string((1 - ytd_ind08) * the_qtr_ind03) + &
' + "' +  m9 + '" * ' + string((1 - ytd_ind09) * the_qtr_ind03) + &
' + "' + m10 + '" * ' + string((1 - ytd_ind10) * the_qtr_ind04) + &
' + "' + m11 + '" * ' + string((1 - ytd_ind11) * the_qtr_ind04) + &
' + "' + m12 + '" * ' + string((1 - ytd_ind12) * the_qtr_ind04)	+ ' as py_curr_qtr, ' + &
"   " +  prior_m1 + " * " + string(ytd_ind01) + &
" + " +  prior_m2 + " * " + string(ytd_ind02) + &
" + " +  prior_m3 + " * " + string(ytd_ind03) + &
" + " +  prior_m4 + " * " + string(ytd_ind04) + &
" + " +  prior_m5 + " * " + string(ytd_ind05) + &
" + " +  prior_m6 + " * " + string(ytd_ind06) + &
" + " +  prior_m7 + " * " + string(ytd_ind07) + &
" + " +  prior_m8 + " * " + string(ytd_ind08) + &
" + " +  prior_m9 + " * " + string(ytd_ind09) + &
" + " + prior_m10 + " * " + string(ytd_ind10) + &
" + " + prior_m11 + " * " + string(ytd_ind11) + &
' + ' + prior_m12 + ' * ' + string(ytd_ind12) + ' as py_ytd, ' + &
"   " +  prior_m1 + " * " + string(ytd_ind01) + &
" + " +  prior_m2 + " * " + string(ytd_ind02) + &
" + " +  prior_m3 + " * " + string(ytd_ind03) + &
" + " +  prior_m4 + " * " + string(ytd_ind04) + &
" + " +  prior_m5 + " * " + string(ytd_ind05) + &
" + " +  prior_m6 + " * " + string(ytd_ind06) + &
" + " +  prior_m7 + " * " + string(ytd_ind07) + &
" + " +  prior_m8 + " * " + string(ytd_ind08) + &
" + " +  prior_m9 + " * " + string(ytd_ind09) + &
" + " + prior_m10 + " * " + string(ytd_ind10) + &
" + " + prior_m11 + " * " + string(ytd_ind11) + &
' + ' + prior_m12 + ' * ' + string(ytd_ind12) + ' as py_itd, ' + &
"   " + prior_m1 + " + " +  prior_m2 + " + " +  prior_m3 + " + " +  prior_m4 + " " + &
" + " + prior_m5 + " + " +  prior_m6 + " + " +  prior_m7 + " + " +  prior_m8 + " " + &
' + ' + prior_m9 + ' + ' + prior_m10 + ' + ' + prior_m11 + ' + ' + prior_m12 + '  as py_tme, ' + &
" " + &
"   "  + prior_m1  + "  * " + string(ytd_ind01) + &
' + "' + m1        + '" * ' + string(1 - ytd_ind01) + &
" + "  + prior_m2  + "  * " + string(ytd_ind02) + &
' + "' + m2        + '" * ' + string(1 - ytd_ind02) + &
" + "  + prior_m3  + "  * " + string(ytd_ind03) + &
' + "' + m3        + '" * ' + string(1 - ytd_ind03) + ' as py_q1, ' + &
"   "  + prior_m4  + "  * " + string(ytd_ind04) + &
' + "' + m4        + '" * ' + string(1 - ytd_ind04) + &
" + "  + prior_m5  + "  * " + string(ytd_ind05) + &
' + "' + m5        + '" * ' + string(1 - ytd_ind05) + &
" + "  + prior_m6  + "  * " + string(ytd_ind06) + &
' + "' + m6        + '" * ' + string(1 - ytd_ind06) + ' as py_q2, ' + &
" " + &
"   "  + prior_m7  + "  * " + string(ytd_ind07) + &
' + "' + m7        + '" * ' + string(1 - ytd_ind07) + &
" + "  + prior_m8  + "  * " + string(ytd_ind08) + &
' + "' + m8        + '" * ' + string(1 - ytd_ind08) + &
" + "  + prior_m9  + "  * " + string(ytd_ind09) + &
' + "' + m9        + '" * ' + string(1 - ytd_ind09) + ' as py_q3, ' + &
" " + &
"   "  + prior_m10 + "  * " + string(ytd_ind10) + &
' + "' + m10       + '" * ' + string(1 - ytd_ind10) + &
" + "  + prior_m11 + "  * " + string(ytd_ind11) + &
' + "' + m11       + '" * ' + string(1 - ytd_ind11) + &
' + '  + prior_m12 + '  * ' + string(ytd_ind12) + &
' + "' + m12       + '" * ' + string(1 - ytd_ind12) + ' as py_q4, ' + &
" 0, 0, 0, 0, 0, 0 " + &
"from cr_sum_temp a, cr_cr_id b  " + &
"where  "


for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element  = lower(i_ds_elements.GetItemString(i, "description"))
	element  = f_cr_clean_string(element)
	
	if i = i_num_elements then
		sqls = sqls + 'a."' + upper(element) + '" = b."' + upper(element) + '" )'
	else
		sqls = sqls + 'a."' + upper(element) + '" = b."' + upper(element) + '" and '
	end if
		
next

execute immediate :sqls;
		
if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("Error: inserting into cr_sum: " + sqlca.SQLErrText)
	f_pp_msgs("sqls: " + sqls)
	rollback;
	return -1
end if



f_pp_msgs("Inserting into cr_sum (budget) at " + string(now()))

sqls_bdg           = "insert into cr_sum (cr_id,month_number,"
element_string_bdg = ""

max_cr_id = 0
select max(cr_id) into :max_cr_id from cr_sum;
if isnull(max_cr_id) then max_cr_id = 0

for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element      = lower(i_ds_elements.GetItemString(i, "description"))
	element      = f_cr_clean_string(element)
	element_bdg  = lower(i_ds_elements.GetItemString(i, "budgeting_element"))
	element_bdg  = f_cr_clean_string(element_bdg)
	
	sqls_bdg = sqls_bdg + '"' + upper(element) + '", '
	element_string_bdg = element_string_bdg + 'a."' + upper(element_bdg) + '", '
		
next

sqls_bdg = sqls_bdg + &
	" cy_curr_mo_original_bdg, cy_ytd_original_bdg, cy_tot_yr_original_bdg, " + &
	" cy_curr_mo_revised_bdg, cy_ytd_revised_bdg, cy_tot_yr_revised_bdg, " + &
	" cy_curr_mo, cy_prior_mo, cy_curr_qtr, cy_ytd, cy_itd, " + &
	" cy_tme, cy_q1, cy_q2, cy_q3, cy_q4, " + &
	" py_curr_mo, py_prior_mo, py_curr_qtr, py_ytd, py_itd, " + &
	" py_tme, py_q1, py_q2, py_q3, py_q4) " + &
" (select " + string(max_cr_id) + " + rownum," + string(current_month) + "," + element_string_bdg + &
'   "' +  m1 + '" * ' + string(curr_pd_ind01) + &
' + "' +  m2 + '" * ' + string(curr_pd_ind02) + &
' + "' +  m3 + '" * ' + string(curr_pd_ind03) + &
' + "' +  m4 + '" * ' + string(curr_pd_ind04) + &
' + "' +  m5 + '" * ' + string(curr_pd_ind05) + &
' + "' +  m6 + '" * ' + string(curr_pd_ind06) + &
' + "' +  m7 + '" * ' + string(curr_pd_ind07) + &
' + "' +  m8 + '" * ' + string(curr_pd_ind08) + &
' + "' +  m9 + '" * ' + string(curr_pd_ind09) + &
' + "' + m10 + '" * ' + string(curr_pd_ind10) + &
' + "' + m11 + '" * ' + string(curr_pd_ind11) + &
' + "' + m12 + '" * ' + string(curr_pd_ind12) + ' as cy_curr_mo_original_bdg, ' + &
'   "' +  m1 + '" * ' + string(ytd_ind01) + &
' + "' +  m2 + '" * ' + string(ytd_ind02) + &
' + "' +  m3 + '" * ' + string(ytd_ind03) + &
' + "' +  m4 + '" * ' + string(ytd_ind04) + &
' + "' +  m5 + '" * ' + string(ytd_ind05) + &
' + "' +  m6 + '" * ' + string(ytd_ind06) + &
' + "' +  m7 + '" * ' + string(ytd_ind07) + &
' + "' +  m8 + '" * ' + string(ytd_ind08) + &
' + "' +  m9 + '" * ' + string(ytd_ind09) + &
' + "' + m10 + '" * ' + string(ytd_ind10) + &
' + "' + m11 + '" * ' + string(ytd_ind11) + &
' + "' + m12 + '" * ' + string(ytd_ind12) + ' as cy_ytd_original_bdg, ' + &
'   "' +  m1 + '" + "' + &
			 m2 + '" + "' + &
			 m3 + '" + "' + &
			 m4 + '" + "' + &
			 m5 + '" + "' + &
			 m6 + '" + "' + &
			 m7 + '" + "' + &
			 m8 + '" + "' + &
			 m9 + '" + "' + &
			m10 + '" + "' + &
			m11 + '" + "' + &
			m12 + '" as cy_tot_yr_original_bdg, ' + &
"   " +  prior_m1 + " * " + string(curr_pd_ind01) + &
" + " +  prior_m2 + " * " + string(curr_pd_ind02) + &
" + " +  prior_m3 + " * " + string(curr_pd_ind03) + &
" + " +  prior_m4 + " * " + string(curr_pd_ind04) + &
" + " +  prior_m5 + " * " + string(curr_pd_ind05) + &
" + " +  prior_m6 + " * " + string(curr_pd_ind06) + &
" + " +  prior_m7 + " * " + string(curr_pd_ind07) + &
" + " +  prior_m8 + " * " + string(curr_pd_ind08) + &
" + " +  prior_m9 + " * " + string(curr_pd_ind09) + &
" + " + prior_m10 + " * " + string(curr_pd_ind10) + &
" + " + prior_m11 + " * " + string(curr_pd_ind11) + &
' + ' + prior_m12 + ' * ' + string(curr_pd_ind12) + ' as cy_curr_mo_revised_bdg, ' + &
"   " +  prior_m1 + " * " + string(ytd_ind01) + &
" + " +  prior_m2 + " * " + string(ytd_ind02) + &
" + " +  prior_m3 + " * " + string(ytd_ind03) + &
" + " +  prior_m4 + " * " + string(ytd_ind04) + &
" + " +  prior_m5 + " * " + string(ytd_ind05) + &
" + " +  prior_m6 + " * " + string(ytd_ind06) + &
" + " +  prior_m7 + " * " + string(ytd_ind07) + &
" + " +  prior_m8 + " * " + string(ytd_ind08) + &
" + " +  prior_m9 + " * " + string(ytd_ind09) + &
" + " + prior_m10 + " * " + string(ytd_ind10) + &
" + " + prior_m11 + " * " + string(ytd_ind11) + &
' + ' + prior_m12 + ' * ' + string(ytd_ind12) + ' as cy_ytd_revised_bdg, ' + &
" " + &
 prior_m1 + " + " + &
 prior_m2 + " + " + &
 prior_m3 + " + " + &
 prior_m4 + " + " + &
 prior_m5 + " + " + &
 prior_m6 + " + " + &
 prior_m7 + " + " + &
 prior_m8 + " + " + &
 prior_m9 + " + " + &
prior_m10 + " + " + &
prior_m11 + " + " + &
prior_m12 + " as cy_tot_yr_revised_bdg, " + &
" 0, 0, 0, 0, 0, " + &
" 0, 0, 0, 0, 0, " + &
" 0, 0, 0, 0, 0, " + &
" 0, 0, 0, 0, 0 " + &
"from cr_sum_temp_bdg a ) "

//  NOT NEEDED FOR BUDGET ...
//", cr_cr_id b  " + &
//"where  "
//for i = 1 to i_num_elements
//	
//	//  Create the Accounting Key string ...
//	//
//	element_bdg  = lower(i_ds_elements.GetItemString(i, "budgeting_element"))
//	element_bdg  = f_cr_clean_string(element_bdg)
//	
//	if i = i_num_elements then
//		sqls_bdg = sqls_bdg + 'a."' + upper(element_bdg) + '" = b."' + upper(element_bdg) + '" )'
//	else
//		sqls_bdg = sqls_bdg + 'a."' + upper(element_bdg) + '" = b."' + upper(element_bdg) + '" and '
//	end if
//		
//next

execute immediate :sqls_bdg;
		
if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("Error: inserting into cr_sum (budget): " + sqlca.SQLErrText)
	f_pp_msgs("sqls_bdg: " + sqls_bdg)
	rollback;
	return -1
end if


f_pp_msgs("Updating cr_sum.cy_beg_bal to 0 (where cy_beg_bal is null) at " + string(now()))

update cr_sum set cy_beg_bal = 0 where cy_beg_bal is null;

if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("Error: updating cr_sum.cy_beg_bal (where cy_beg_bal is null): " + sqlca.SQLErrText)
	rollback;
	return -1
end if


//*****************************************************************
//  4:  Clean Up.
//*****************************************************************
f_pp_msgs("Dropping table cr_sum_temp at " + string(now()))

sqls = "drop table cr_sum_temp"
execute immediate :sqls;

if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("Error: dropping cr_sum_temp: " + sqlca.SQLErrText)
	rollback;
end if

f_pp_msgs("Dropping table cr_sum_temp_bdg at " + string(now()))

sqls = "drop table cr_sum_temp_bdg"
execute immediate :sqls;

if sqlca.SQLCode = 0 then
	commit;
else
	f_pp_msgs("Error: dropping cr_sum_temp_bdg: " + sqlca.SQLErrText)
	rollback;
end if


f_pp_msgs("uf_step3 finished at: " + string(now()))

return 1
end function

public function longlong uf_step2 ();//*********************************************************************************************
//
//  Step 2  :  Update the cr_id field on cr_cost_repository
//
//  NOTES   :  This script is best performed on conversion with a company/MN index and
//					using where clauses in the code below.  And in fact, while script 1 might run
//					without the indexes, this one will probably grind to a halt.
//
//*********************************************************************************************
longlong i
string sqls, element

f_pp_msgs("Starting uf_step2 at: " + string(now()))

f_pp_msgs("Analyzing table cr_cr_id at " + string(now()))
sqlca.analyze_table('cr_cr_id');

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: analyzing cr_cr_id" + sqlca.SQLErrText)
	rollback;
	return -1
end if


f_pp_msgs("Updating cr_cost_repository at " + string(now()))
sqls = "update cr_cost_repository a set cr_id = ( select b.cr_id from cr_cr_id b " + &
	"where  a.cr_id = 0 and "

for i = 1 to i_num_elements
	
	//  Create the Accounting Key string ...
	//
	element  = lower(i_ds_elements.GetItemString(i, "description"))
	element  = f_cr_clean_string(element)
	
	if i = i_num_elements then
		sqls = sqls + 'a."' + upper(element) + '" = b."' + upper(element) + '" )'
	else
		sqls = sqls + 'a."' + upper(element) + '" = b."' + upper(element) + '" and '
	end if
		
next

//  PRODUCTION:
sqls = sqls + " where a.cr_id = 0 "

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_cost_repository" + sqlca.SQLErrText)
	f_pp_msgs("sqls = " + sqls)
	rollback;
	return -1
end if

commit;

f_pp_msgs("uf_step2 finished at: " + string(now()))

return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_sum_custom_version nvo_cr_sum_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_sum_custom.pbd'
		return nvo_cr_sum_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

