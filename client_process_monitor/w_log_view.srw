HA$PBExportHeader$w_log_view.srw
forward
global type w_log_view from window
end type
type cb_size from commandbutton within w_log_view
end type
type st_kill from statictext within w_log_view
end type
type hpb_progress from hprogressbar within w_log_view
end type
type dw_logs from datawindow within w_log_view
end type
type sle_logs from singlelineedit within w_log_view
end type
type cb_kill from commandbutton within w_log_view
end type
type cb_close from commandbutton within w_log_view
end type
end forward

global type w_log_view from window
boolean visible = false
integer width = 3822
integer height = 1940
boolean titlebar = true
string title = "Process Monitor"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "48_planet.ico"
boolean center = true
cb_size cb_size
st_kill st_kill
hpb_progress hpb_progress
dw_logs dw_logs
sle_logs sle_logs
cb_kill cb_kill
cb_close cb_close
end type
global w_log_view w_log_view

type variables
longlong i_process_identifier, i_process_id, i_occurrence_id, i_max_timeout = 15
double i_timeout, i_wait_time
boolean i_started, i_progress_bar = false, i_killable = false, i_smart_bar = false, i_synchronous = false, i_show_less = false
string i_description
end variables

on w_log_view.create
this.cb_size=create cb_size
this.st_kill=create st_kill
this.hpb_progress=create hpb_progress
this.dw_logs=create dw_logs
this.sle_logs=create sle_logs
this.cb_kill=create cb_kill
this.cb_close=create cb_close
this.Control[]={this.cb_size,&
this.st_kill,&
this.hpb_progress,&
this.dw_logs,&
this.sle_logs,&
this.cb_kill,&
this.cb_close}
end on

on w_log_view.destroy
destroy(this.cb_size)
destroy(this.st_kill)
destroy(this.hpb_progress)
destroy(this.dw_logs)
destroy(this.sle_logs)
destroy(this.cb_kill)
destroy(this.cb_close)
end on

event timer;longlong row, count, old_count, num_rows, current_row, ended, started, pct_complete
longlong past_count, past_occurrence, cpmWaitTime
boolean scroll_up
datetime end_time, past_start, past_end, past_current
string ini_file, launch_local, launch_cpm, db_name

ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "xpwrplant.ini")
launch_local = Profilestring(ini_file, "SSP", "LaunchLocal", "not specified")
launch_cpm = Profilestring(ini_file, "SSP", "LaunchCPM", "not specified")
cpmWaitTime = longlong(Profilestring(ini_file, "SSP", "CPMWaitTime", "not specified"))
db_name = sqlca.ServerName

if isnull(cpmWaitTime) or cpmWaitTime = 0 then
	// Default to two minutes
	cpmWaitTime = 120
end if

if i_description = '' and i_process_id > 0 then
	select description into :i_description from pp_processes where process_id = :i_process_id;
end if

if i_started then
	i_wait_time = 2
	
	select nvl2(max(end_time),1,0), max(end_time), count(*) into :ended, :end_time, :count
		from pp_processes_messages a, pp_processes_occurrences b
		where a.process_id = :i_process_id
		and a.occurrence_id = :i_occurrence_id
		and b.process_id = :i_process_id
		and b.occurrence_id = :i_occurrence_id;
	
	old_count = dw_logs.rowcount()
	
	if count = 0 and i_process_identifier > 0 then
		// Did the process move??
		select nvl(process_id, 0), nvl(occurrence_id, 0) into :i_process_id, :i_occurrence_id
		from pp_job_request
		where process_identifier = :i_process_identifier;
	end if
	
	// If the count has changed, the retrieve the logs again
	if count <> old_count then
		current_row = long(dw_logs.Describe("DataWindow.FirstRowOnPage"))
		if current_row <> dw_logs.ScrollToRow(old_count) then
			// They have scrolled up ... will return to this scrolled position
			scroll_up = true
		else 
			scroll_up = false
		end if
		
		num_rows = dw_logs.retrieve(i_process_id, i_occurrence_id)
		if num_rows < 0 then 
			// Error...stop auto retrieving...
			disconnect using sqlca;
			timer(0)
			messagebox("Error","Error with process "+i_description+", occurrence "+string(i_occurrence_id)+". No rows retrieved.")
			close(this)
		else
			this.visible = true
			this.bringtotop = true
		end if
		dw_logs.ScrollToRow(num_rows)
		
		sle_logs.text = dw_logs.getitemstring(num_rows, 'msg')
		
		if scroll_up then	dw_logs.ScrollToRow(current_row)
	end if

	select nvl(pct_complete, 0) into :pct_complete from pp_processes_occurrences where process_id = :i_process_id and occurrence_id = :i_occurrence_id;
	if pct_complete > 0 then
		if not i_progress_bar then
			i_progress_bar = true
			this.height = this.height + 100
		end if
		hpb_progress.position = pct_complete
	elseif i_smart_bar then

		select count(*), max(occurrence_id)
		  into :past_count, :past_occurrence
		  from pp_processes_messages
		 where process_id = :i_process_id
			and occurrence_id =
				 (select max(occurrence_id)
					 from pp_processes_messages
					where process_id = :i_process_id
					  and occurrence_id <> :i_occurrence_id); //would be better to merge all historic logs instead of just pulling one (you could save it in a cache as well)

		if past_count > 0 then
			if not i_progress_bar then
				i_progress_bar = true
				this.height = this.height + 100
			end if

			select min(time_stamp), max(time_stamp)
			  into :past_start, :past_end
			  from pp_processes_messages
			 where process_id = :i_process_id
				and occurrence_id = :past_occurrence;
				
			select time_stamp
			  into :past_current
			  from pp_processes_messages
			 where process_id = :i_process_id
				and occurrence_id = :past_occurrence
				and msg_order = (select max(msg_order)
										 from pp_processes_messages
										where process_id = :i_process_id
										  and occurrence_id = :i_occurrence_id); //would be better to join on semantic pairs instead of message number

			pct_complete = 100 * secondsafter(time(past_start), time(past_current)) / secondsafter(time(past_start), time(past_end))
			hpb_progress.position = pct_complete
		end if
	end if

	if ended = 1 then
		// Process being retrieved has an end time...stop checking but let this run finish
		disconnect using sqlca;
		timer(0)
		if i_progress_bar then
			hpb_progress.position = 100
		end if
		if not i_synchronous then
			messagebox("Process Complete","Process "+i_description+", occurrence "+string(i_occurrence_id)+" complete.")
		elseif i_show_less then
			sle_logs.text = "Process "+i_description+", occurrence "+string(i_occurrence_id)+" complete."
		end if
		if i_killable then
			i_killable = false
			this.height = this.height - 1
		end if
		this.controlmenu = true
		cb_close.visible = true
		cb_close.default = true
		return
	end if
	
	if i_timeout >= i_max_timeout then
		longlong rs_id, rs_count
		
		select running_session_id, pp_misc_pkg.pp_count_audsid(nvl(running_session_id,-999)) into :rs_id, :rs_count 
		from pp_processes where process_id = :i_process_id;
				
		if (rs_id <> 0 and not isnull(rs_id)) and (isnull(rs_count) or rs_count <= 0) then
			// Error...stop auto retrieving...
			disconnect using sqlca;
			timer(0)
			messagebox("Error","Error with process "+i_description+", occurrence "+string(i_occurrence_id)+". Process failed to complete.")
			this.controlmenu = true
			return
		end if
		
		if (not i_killable) and i_synchronous then
			i_killable = true
			this.height = this.height + 1
		end if
		
		i_timeout = 0
	end if
	i_timeout += i_wait_time
else
	if i_process_id > 0 and i_occurrence_id > 0 then
		select nvl2(max(start_time),1,0) into :started
		from pp_processes_messages a, pp_processes_occurrences b
			where a.process_id = :i_process_id
			and a.occurrence_id = :i_occurrence_id
			and b.process_id = :i_process_id
			and b.occurrence_id = :i_occurrence_id;
	
		i_timeout += i_wait_time
	
		if started = 1 then
			i_started = true
			i_timeout = 0
		end if
	else
		select nvl(process_id, 0), nvl(occurrence_id, 0) into :i_process_id, :i_occurrence_id
		from pp_job_request
		where process_identifier = :i_process_identifier;
		
		i_timeout += i_wait_time
	end if
	
	if i_timeout >= cpmWaitTime then
		disconnect using sqlca;
		timer(0)
		if i_process_id > 0 and i_occurrence_id > 0 then
			messagebox("Process Timeout","Process "+i_description+", occurrence "+string(i_occurrence_id)+" failed to start within the 2 minute waiting period for the CPM." + &
						  "~r~n~r~nPlease manually check the online logs for updates on the process." + &
						  "~r~n~r~nLaunch local is "+launch_local+".~r~nLaunch CPM is "+launch_cpm+".~r~nThe database is "+db_name+".~r~nThe ini file is "+ini_file)
		else 
			messagebox("Process Timeout","Process "+i_description+", request " + string(i_process_identifier)+" failed to start within the 2 minute waiting period for the CPM." + &
						  "~r~n~r~nPlease manually check the online logs for updates on the process." + &
						  "~r~n~r~nLaunch local is "+launch_local+".~r~nLaunch CPM is "+launch_cpm+".~r~nThe database is "+db_name+".~r~nThe ini file is "+ini_file)
		end if
		close(this)
	end if
end if
// Want it to always wait 2 seconds between retrieves...if this isn't here, if the retrieve above takes 2 seconds, it will immediately start again
//	which will lock the application!
timer(i_wait_time)

end event

event open;string args, description
longlong started, synch_process

args = Message.StringParm
if pos(args, ',') > 0 then
	i_process_id = long(trim(mid(args, 1, pos(args, ',') - 1)))
	i_occurrence_id = long(trim(mid(args, pos(args, ',') + 1)))
	
	select nvl(synchronous, 0) into :synch_process 
	from pp_job_request 
	where process_id = :i_process_id
	and occurrence_id = :i_occurrence_id;
else
	i_process_identifier = long(trim(args))
	
	select process_id, process_descr, occurrence_id, nvl(synchronous, 0) into :i_process_id, :description, :i_occurrence_id, :synch_process
	from pp_job_request 
	where	process_identifier = :i_process_identifier;
	
	if sqlca.sqlcode <> 0 then
		messagebox('SQL Error','Error pulling Process ID and/or Occurrence ID from the database: ' + sqlca.sqlerrtext)
		close(this)
	end if
end if

if not isnull(description) and trim(description) <> '' then this.title += ' - ' + description

i_started = false
i_timeout = 0
i_wait_time = 0.5
i_description = description

if synch_process <> 0 then 
	i_synchronous = true
	i_show_less = true
	cb_size.text = 'Show More'
	controlmenu = false
	cb_close.visible = false
	this.height = 500
	visible = true
	i_wait_time = 0.01
end if

dw_logs.SetTransObject(SQLCA)
timer(i_wait_time)
end event

event close;disconnect using sqlca;
halt close
end event

event resize;longlong buffer

buffer = 36

dw_logs.x = buffer
dw_logs.y = buffer
dw_logs.width = newwidth - (2 * buffer)
sle_logs.x = buffer
sle_logs.y = buffer
sle_logs.width = newwidth - (2 * buffer)
hpb_progress.x = buffer
hpb_progress.width = newwidth - (2 * buffer)
cb_size.x = buffer
cb_kill.x = newwidth - (cb_kill.width + buffer)
cb_kill.height = cb_size.height
cb_close.x = newwidth - (cb_close.width + buffer)
cb_close.height = cb_size.height

if i_show_less then
	sle_logs.visible = true
	dw_logs.visible = false
else
	sle_logs.visible = false
	dw_logs.visible = true
end if

if i_progress_bar and not i_killable then //Show progress bar, but hide button
	dw_logs.height = newheight - (4 * buffer + cb_size.height + hpb_progress.height)
	hpb_progress.y = newheight - (2 * buffer + cb_size.height + hpb_progress.height)
	cb_size.y = newheight - (buffer + cb_size.height)
	cb_close.y = newheight - (buffer + cb_size.height)
	cb_kill.y = newheight
	st_kill.y = newheight
elseif not i_killable then //Hide progress bar and hide button
	dw_logs.height = newheight - (3 * buffer + cb_size.height)
	hpb_progress.y = newheight
	cb_size.y = newheight - (buffer + cb_size.height)
	cb_close.y = newheight - (buffer + cb_size.height)
	cb_kill.y = newheight
	st_kill.y = newheight
elseif not i_progress_bar then //Hide progress bar, but show button
	dw_logs.height = newheight - (3 * buffer + cb_size.height)
	hpb_progress.y = newheight
	cb_size.y = newheight - (buffer + cb_size.height)
	cb_close.y = newheight - (buffer + cb_size.height)
	cb_kill.y = newheight - (buffer + cb_size.height)
	st_kill.y = newheight - (buffer + cb_size.height - 24)
	st_kill.text = 'This process is taking longer than '+string(i_max_timeout)+' seconds to complete. You may terminate the process early.'
else //Show progress bar and show button
	dw_logs.height = newheight - (4 * buffer + cb_size.height + hpb_progress.height)
	hpb_progress.y = newheight - (2 * buffer + cb_size.height + hpb_progress.height)
	cb_size.y = newheight - (buffer + cb_size.height)
	cb_close.y = newheight - (buffer + cb_size.height)
	cb_kill.y = newheight - (buffer + cb_size.height)
	st_kill.y = newheight - (buffer + cb_size.height - 24)
	st_kill.text = 'This process is taking longer than '+string(i_max_timeout)+' seconds to complete. You may terminate the process early.'	
end if
dw_logs.ScrollToRow(dw_logs.rowcount())
end event

type cb_size from commandbutton within w_log_view
integer x = 37
integer y = 1700
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Show Less"
end type

event clicked;if i_show_less then
	i_show_less = false
	this.text = 'Show Less'
	parent.height = 1800
else
	i_show_less = true
	this.text = 'Show More'
	if i_progress_bar then
		parent.height = 600
	else
		parent.height = 500
	end if
end if
end event

type st_kill from statictext within w_log_view
integer x = 471
integer y = 1724
integer width = 2725
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "This process is taking longer than XX seconds to complete. You may terminate the process early."
boolean focusrectangle = false
end type

type hpb_progress from hprogressbar within w_log_view
integer x = 37
integer y = 1612
integer width = 3707
integer height = 68
unsignedinteger maxposition = 100
unsignedinteger position = 50
integer setstep = 10
boolean smoothscroll = true
end type

type dw_logs from datawindow within w_log_view
integer x = 37
integer y = 36
integer width = 3707
integer height = 1540
integer taborder = 10
string title = "none"
string dataobject = "dw_pp_processes_messages_clock"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;string line, arg
longlong start_p, end_p, pid
longlong start_o, end_o, oid
string dwobjectname

dwobjectname = GetObjectAtPointer()
if left(dwobjectname,3) = 'b_1' then
	line = getitemstring(row,'msg')
	
	start_p = pos(line,'process ')+8
	end_p = pos(line,' ',start_p)
	start_o = pos(line,'occurrence ')+11
	end_o = pos(line,' ',start_o)
	
	arg = 'cpm.exe -s ' + sqlca.servername + ' -dbms ' + sqlca.dbms + ' -uid ' + sqlca.logid + ' -pass ' + sqlca.logpass +&
			' -pid ' + mid(line, start_p, end_p - start_p) + ' -oid ' + mid(line, start_o, end_o - start_o)
	run(arg)
end if
end event

type sle_logs from singlelineedit within w_log_view
boolean visible = false
integer x = 37
integer y = 36
integer width = 3707
integer height = 112
integer taborder = 30
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "Starting Process..."
borderstyle borderstyle = stylelowered!
end type

type cb_kill from commandbutton within w_log_view
integer x = 3200
integer y = 1700
integer width = 544
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Terminate Process"
end type

event clicked;longlong pid

if 2 = messagebox('Confirm Kill?','Are you sure you want to terminate the process early?',Question!,YesNo!,2) then
	return
end if

select nvl(windows_process_id,-1) into :pid from pp_job_request where process_id = :i_process_id and occurrence_id = :i_occurrence_id;
if pid > 0 then
	run('taskkill /pid '+string(pid)+' /f')
	
	update pp_processes set running_session_id = 0 where process_id = :i_process_id;
	update pp_job_request set status = 'K' where process_id = :i_process_id and occurrence_id = :i_occurrence_id;
	commit;

	messagebox('Process Terminated',"Process "+i_description+", occurrence "+string(i_occurrence_id)+" failed to complete.")
else
	messagebox('Unable to Terminate Process',"Process "+i_description+", occurrence "+string(i_occurrence_id)+" does not have a valid windows process id. Please check the pp_job_request table.")
end if

close(parent)
end event

type cb_close from commandbutton within w_log_view
integer x = 3200
integer y = 1700
integer width = 544
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Close"
boolean default = true
end type

event clicked;close(parent)
end event

