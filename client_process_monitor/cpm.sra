HA$PBExportHeader$cpm.sra
$PBExportComments$Generated Application Object
forward
global type cpm from application
end type
global uo_sqlca sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
longlong g_process_id, g_occurrence_id, g_msg_order, g_rtn_code, g_interface_id, &
		   g_source_id, g_record_count, g_mn_to_process
string   g_log_file, g_batch_id, g_balancing_timestamp, g_command_line_args, g_debug
boolean  g_do_not_write_batch_id
datetime g_finished_at
u_prog_interface g_prog_interface
uo_sqlca_logs    g_sqlca_logs
uo_mail g_msmail
u_external_function uo_winapi
dec{2} g_amount, g_total_dollars, g_total_credits, g_total_debits

////	////	added for uo_log
boolean	g_of_log_add_time, g_of_log_use_ppmsg, g_of_log_use_writefile

////	////	added for uo_check_sql
boolean g_of_check_sql_write_success
boolean g_main_application

// Use this variable to be the OS return value in case we encounter an error 
// before we can connect to the database and establish the online logs
longlong g_rtn_failure = -1 

uo_client_interface g_uo_client
uo_ppinterface g_uo_ppint
boolean g_db_connected, g_db_logs_connected

nvo_runtimeerror g_rte
nvo_runtimeerror_app g_rte_app

nvo_stats g_stats


s_user_info                s_user_info
s_sys_info				s_sys_info
nvo_func_string		g_string_func
nvo_func_database	g_db_func
nvo_func_datastore	g_ds_func
nvo_pp_func_io			g_io_func
uo_messagebox                          g_msg  //Added by appeon (8/15/2012  )
uo_ds_top g_ds_messagebox_translate

// ### 29892: JAK: server side processing
s_file_info				g_exe_info   // global variable with exe attributes
boolean								g_ssp
string									g_ssp_user, g_ssp_process, g_ssp_return
longlong	g_ssp_process_identifier
s_ppbase_parm_arrays   			g_ssp_parms
s_ppbase_parm_arrays_labels	g_ssp_labels
nvo_server_side_request			g_ssp_nvo
uo_system_cache					g_cache
uo_cr_cache 						g_cr

// autogen je accounts
uo_ds_top  g_ds_cr_element_definitions
uo_autogen_je_account g_uo_autogen_je_account

//Holder for PP Constants
nvo_pp_constants g_pp_constants

end variables
global type cpm from application
string appname = "cpm"
end type
global cpm cpm

type prototypes
Function long GetFileTime( long unit , ref s_filedate  create_time,  ref s_filedate last_access ,ref  s_filedate last_write) library "kernel32.dll" alias for "GetFileTime;Ansi"
Function Long FileTimeToSystemTime(s_filedate file_date ,ref  s_system_time system_date)  library "kernel32.dll" alias for "FileTimeToSystemTime;Ansi"
Function Long _lopen( string path ,long mode )  library "kernel32.dll" alias for "_lopen;Ansi"
Function Long _lclose( long unit )  library "kernel32.dll"
Function long GetModuleHandleA(string modname) Library "KERNEL32.DLL" alias for "GetModuleHandleA;Ansi"
Function long GetModuleFileNameA(long hModule, ref string  lpFilename, long Size ) Library "KERNEL32.DLL" alias for "GetModuleFileNameA;Ansi"
Function Long SystemTimeToTzSpecificLocalTime(s_TIME_ZONE_INFORMATION timezone, s_system_time utc_date, ref s_system_time local_date)  library "kernel32.dll" alias for "SystemTimeToTzSpecificLocalTime;Ansi"
Function Long GetTimeZoneInformation(ref s_TIME_ZONE_INFORMATION timezone)  library "kernel32.dll" alias for "GetTimeZoneInformation;Ansi"
Function uLong FindFirstFile(string lpFileName, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindFirstFileA;Ansi"
Function uLong FindNextFile(ulong hFindFile, ref s_finddata lpFindFileData) Library "kernel32.dll" ALIAS FOR "FindNextFileA;Ansi"
Function uLong FindClose(ulong hFindFile) Library "kernel32.dll"

// To get environment variables
Function long GetEnvironmentVariableA(string var,ref string str,long len) Library "kernel32.dll" alias for "GetEnvironmentVariableA;Ansi"
Function long GetLastError() Library "kernel32.dll"

//File version functions
function long GetFileVersionInfoSizeA( string filename  ,ref long  size  ) library "version.dll" alias for "GetFileVersionInfoSizeA;Ansi"
function long GetFileVersionInfoA(string filename, long  dwHandle,long dwLen, ref blob  lpData) library "version.dll" alias for "GetFileVersionInfoA;Ansi"
function long VerQueryValueA( blob pBlock,string lpSubBlock,ref s_string lplpBuffer,ref long puLen ) library "version.dll" alias for "VerQueryValueA;Ansi"
function long GetUserDefaultLangID() library "kernel32.dll"

// addiitonal functions for system and user info
function long GetVersionExA( REF s_osversioninfoex OS) library "kernel32.dll" alias for "GetVersionExA;Ansi"
function long WNetGetUserA(long ptr,ref string user,ref long len) LIBRARY "mpr.dll" alias for "WNetGetUserA;Ansi"
function long gethostname(ref string host,long len) library "Ws2_32.dll" alias for "gethostname;Ansi"
function int WSAGetLastError ( ) library "Ws2_32.dll"
function int WSAStartup( uint UIVersionRequested, ref s_WSAData lpWSAData )library "Ws2_32.dll"

// to get UNC file names
//Function long WNetGetUniversalName(string lpLocalPath,long dwInfoLevel,ref s_universal_name_info lpBuffer,ref long lpBufferSize)   Library "mpr.DLL" alias for "WNetGetUniversalNameA;Ansi"
Function long WNetGetConnection(string lpLocalName, ref string lpRemoteName, ref long lpnLength)   Library "mpr.DLL" alias for "WNetGetConnectionA;Ansi"



end prototypes

on cpm.create
appname="cpm"
message=create message
sqlca=create uo_sqlca
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on cpm.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;string server_name, dbms, log_id, log_pass
int start, stop
longlong process_id, occurence_id, process_identifier


//*****************************************************************************************
//
//  Get login credentials from PPCSET.
//		Maint-38445.  Will always get credentials from ppcset.  Since this functionality does not perform any database 
//		changes the batch processing ID can be used.
//		Credentials include the server, DBMS, username, and password.  Those arguments will no longer be read from the
//		command line.
//
//*****************************************************************************************
// must instantiate uo_winapi before calling f_get_temp_dir() below
// this code was taken (and modified) from the PowerPlant application open() event
environment env
getenvironment(env) 
CHOOSE CASE env.OSType
CASE Windows!  
	 if env.win16 = true then 
		  uo_winapi = create u_external_function_winapi
	else
		  uo_winapi = create u_external_function_win32
	end if	
CASE WindowsNT!
	 if env.win16 = true then 
		 uo_winapi = create u_external_function_winapi
	 else	 
		 uo_winapi = create u_external_function_win32
	end if
END CHOOSE 

g_prog_interface = CREATE u_prog_interface
g_prog_interface.uf_get_prog_params("")

// For logging in case we have issues during login / grants
g_io_func = create nvo_pp_func_io
g_string_func = create nvo_func_string

g_pp_constants = create nvo_pp_constants

s_sys_info.dbms = 'oracle'

g_main_application = true

//*****************************************************************************************
//
//  Which occurrence are we after.
//
//*****************************************************************************************
//###For testing
//commandline = '-ssp 1' //try GetModuleHandleA
//###
commandline = f_replace_string(commandline,'/pid','-pid','all')
commandline = f_replace_string(commandline,'/oid','-oid','all')
commandline = f_replace_string(commandline,'/ssp','-ssp','all')

start = pos(commandline, '-pid')
if start > 0 then
	stop = pos(commandline, ' -', start + 1)
	if stop > 0 then
		process_id = long(trim(mid(commandline, start + 5, stop - (start + 5))))
	else
		process_id = long(trim(mid(commandline, start + 5)))
	end if
	
	start = pos(commandline, '-oid')
	if start > 0 then
		stop = pos(commandline, ' -', start + 1)
		if stop > 0 then
			occurence_id = long(trim(mid(commandline, start + 5, stop - (start + 5))))
		else
			occurence_id = long(trim(mid(commandline, start + 5)))
		end if
	else
		messagebox("Error","If you are using the process id flag, you must also use '-oid' to specify an occurrence id.")
		return
	end if
else
	start = pos(commandline, '-ssp')
	if start > 0 then
		stop = pos(commandline, ' -', start + 1)
		if stop > 0 then
			process_identifier = long(trim(mid(commandline, start + 5, stop - (start + 5))))
		else
			process_identifier = long(trim(mid(commandline, start + 5)))
		end if
	else
		messagebox("Error","You must specify either a server side process identifier (-ssp) or process id (-pid).")
		return
	end if
end if

//*****************************************************************************************
//
//  Are we using command line login credentials.
//		Command Line will be of the form "/connection=encryptedconnectionstring" such as "/connection=436883EB6CAED91A"
// 		### 40102: JAK
//
//*****************************************************************************************
longlong start_pos, end_pos
string conn, conn_array[]
longlong i
string conn_logid, conn_logpass, conn_userid, conn_dbms, conn_servername, conn_sspid

if pos(commandline,"/connection=") > 0 then
	start_pos = pos(commandline,"/connection=") + len('/connection=')
	end_pos = pos(f_replace_string(commandline,'	',' ','all') + ' ',' ',start_pos)	
		
	if end_pos <= 0 then
		// error
	else
		// get the connection string
		conn = mid(commandline, start_pos, end_pos - start_pos)
		
		// remove all of the info from the logs
		g_command_line_args = f_replace_string(commandline,conn,'...','first')
		
		// decrypt the connection string.
		nvo_web_crypto web_crptyo
		web_crptyo = create nvo_web_crypto
		conn = web_crptyo.uf_rc2_decrypt(conn, 'b@n@n@password$plit')
		
		f_parsestringintostringarray(conn,';_',conn_array)
		
		for i = 1 to upperbound(conn_array)
			if mid(conn_array[i],1,4) = 'LID=' then conn_LogID = mid(conn_array[i],5)
			if mid(conn_array[i],1,3) = 'LP=' then conn_LogPass = mid(conn_array[i],4)
			if mid(conn_array[i],1,4) = 'UID=' then  conn_UserID = mid(conn_array[i],5)
			if mid(conn_array[i],1,5) = 'DBMS=' then  conn_DBMS = mid(conn_array[i],6)
			if mid(conn_array[i],1,3) = 'SN=' then  conn_ServerName = mid(conn_array[i],4)
		next 
	end if
end if


//*****************************************************************************************
//
//  Create the connection to the POWERPLANT instance.
//
//*****************************************************************************************
//sqlca.ServerName	= server_name
//sqlca.DBMS       	= dbms
//sqlca.LogID			= log_id
//sqlca.LogPass    	= log_pass
sqlca.ServerName = g_prog_interface.i_server
sqlca.DBMS       	= g_prog_interface.i_dbms
sqlca.LogID      	= g_prog_interface.i_username
sqlca.LogPass    	= g_prog_interface.i_password
if trim(conn_LogID) <> '' then sqlca.LogID = conn_LogID
if trim(conn_LogPass) <> '' then sqlca.LogPass = conn_LogPass
if trim(conn_UserID) <> '' then sqlca.UserID = conn_UserID
if trim(conn_ServerName) <> '' then sqlca.ServerName = conn_ServerName
 if trim(sqlca.DBMS) = '' then sqlca.DBMS = conn_DBMS  // only use the DBMS if there wasn't an entry in ppcset at all.


if pos(sqlca.UserID,'[') > 0 then
	// trying to use SSO.
	// For login,    LogID has to be proxy[username] and UserID has to be proxy
	//	After login, it will be changed to UserID = proxy[username] and LogID = username
	sqlca.LogID = sqlca.UserID
	sqlca.UserID = mid(sqlca.LogID,1,pos(sqlca.LogID,'[') - 1)
end if
	
connect using sqlca;
	
if sqlca.SQLCode = 0 then
	if pos(sqlca.LogID,'[') > 0 then
		// using SSO.  Need to flip the user ids around.
		sqlca.UserID = sqlca.LogID
		sqlca.LogID = mid(sqlca.LogID,pos(sqlca.LogID,'[') + 1, len(sqlca.LogID) - pos(sqlca.LogID,'[') - 1)
	else
		sqlca.UserID = sqlca.LogID
	end if
	
	// ### 6451:  JAK: 2011-01-13:  Starting in Oracle 11g, must set the role even if it is the default role.  
	sqlca.uf_set_role('pwrplant_role_dev')
	
	if process_identifier > 0 then
		OpenWithParm(w_log_view, string(process_identifier))
	else
		OpenWithParm(w_log_view, string(process_id) + ", " + string(occurence_id))
	end if
else
	messagebox("Error","Error connecting to PowerPlan instance: " + sqlca.SQLErrText)
end if

//f_write_log(g_log_file, "    Server: " + sqlca.ServerName)
//f_write_log(g_log_file, "    DBMS:   " + sqlca.DBMS)
//f_write_log(g_log_file, "    LogID:  " + sqlca.LogID)
//f_write_log(g_log_file, "    DBParm: "+ sqlca.dbparm)
//f_write_log(g_log_file, "    Code Last Modified:  " + string(file_written))
//f_write_log(g_log_file, "    Interface Path: " + string(path))
//f_write_log(g_log_file, "    Interface Path UNC: " + string(path_unc))
//f_write_log(g_log_file, "    Log File Path: " + string(g_log_file))
//f_write_log(g_log_file, "    Log File Path UNC: " + string(log_file_unc))
//f_write_log(g_log_file, "    OS Version: " + string(os_version))
//f_write_log(g_log_file, "    OS User: " + string(user))
//f_write_log(g_log_file, "    OS Hostname: " + string(hostname))
end event

