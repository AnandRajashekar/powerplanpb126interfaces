HA$PBExportHeader$uo_client_interface.sru
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string	i_exe_name = 'cr_man_jrnl_reversals.exe'
end variables

forward prototypes
public function longlong uf_read ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//*****************************************************************************************
//
//  User Object Function  :  uf_read
//
//  Description  :  Reverses manual journals that are flagged to auto-reverse.
//
//  Notes  :  The reversal_lag is a "suggestion" only !!!  It is the amount of time to
//            wait until a journal entry is ELIGIBLE to reverse.  It is NOT the 
//            necessarily the actual month when it WILL reverse.  For example, if a JE is
//            posted in 200601 and has a reversal lag of 1 (1 month).  It is ELIGIBLE to
//            reverse in 200602 --- which it WILL if they run this program when that month
//            is open.  If however, they forget to run in 200602 business and instead,
//            close 200602 and open 200603 before running this program, the JE will be
//            reversed in 200603.
//
//*****************************************************************************************
longlong current_open_month, num_jrnls, l, journal_id, source_id, num_elements, &
		i, num_fields, ii, rtn, counter, lvalidate_rtn, validation_counter, post_to_gl, post_to_projects, overall_rtn
string sqls, element, table_name, description, enable_validations, cv_validations, sqls2, to_user, rtn_str
boolean validation_kickouts, validation_kickouts_total, invalid_mn
string batch_id, gl_id, ccs, where_clause, co_field
any results[]

// ### - SEK - 7078 - 031811: Need to trim g_command_line_args
g_command_line_args = trim(g_command_line_args)

//JAK: 12/04/06:  Get control value for memo validations.
select upper(trim(control_value)) into :enable_validations from cr_system_control 
	where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MANJE REVERSAL';

if isnull(enable_validations) then enable_validations = "NO"

if enable_validations = 'YES' then
	uo_cr_validation l_uo_cr_validation
	l_uo_cr_validation = CREATE uo_cr_validation
	validation_kickouts_total = false
end if

uo_cr_cost_repository uo_cr
uo_cr = create uo_cr_cost_repository

//	Get the source_id for cr_journals
select source_id, table_name
into :source_id, :table_name
from cr_sources
where lower(description) = 'journal lines';

// DML: 033104: Get the open month for the journal lines source from cr_open_month_number (v9.0).
// ### JAK : 2010-11-16:  Allow a command line argument to only reverse for a single company
sqls = "select min(month_number) "
sqls += "from cr_open_month_number "
sqls += "where source_id = " + string(source_id) + " "
sqls += "and status = 1 "

if isnull(g_command_line_args) or trim(g_command_line_args) = '' then
	// All companies
	f_pp_msgs("Processing reversals for all companies.")
else
	// Specific company
	// Look up the company field
	setnull(co_field)
	select upper(trim(control_value)) into :co_field from cr_system_control
	 where upper(trim(control_name)) = 'COMPANY FIELD';
	 
	if isnull(co_field) or co_field = "" then
		f_pp_msgs("ERROR: could not find the COMPANY FIELD in cr_system_control.")
		rollback;
		overall_rtn = -1
		goto exit_and_return
	end if
	co_field = f_cr_clean_string(co_field)
	
	f_pp_msgs("Processing reversals for company '" + g_command_line_args + "'.")
	sqls += "and " + co_field + " = '" + g_command_line_args + "' "
end if

f_get_column(results,sqls)
current_open_month = results[1]

if isnull(current_open_month) or current_open_month = 0 then 
	f_pp_msgs("ERROR: Could not find the open month")
	rollback;
	overall_rtn = -1
	goto exit_and_return
end if

f_pp_msgs("Retrieving journals to reverse in " + string(current_open_month) + " at " + string(now()))
////	Pull all the journals that are eligible to be reversed.
////	DML: 042805: If a user puts in a month_number in the reversal lag
////		the sql above blows up.
//// JAK: 20090922: Add logic so that period 00 and period 13+ entries can be auto reversed.
////	Previously, these caused the SQL to fail and then nothing would be reversed.
////	All of the non standard period will be reversed like December transactions.
sqls = "select journal_id, description from cr_journals where reversal_date is null " + &
	" and reversing_journal = 1 and posted_date is not null " + &
	" and to_number(to_char(add_months(to_date(" + &
		"case when substr(month_number,5,2) = '00' then month_number - 88 " + &
		"when substr(month_number,5,2) >= '13' then to_number(substr(month_number,1,4) || '12') " + &
		"else month_number end " + &
	",'yyyymm'),decode( sign(reversal_lag-200000), 1, 0, reversal_lag )" + &
	" ),'yyyymm')) <= " + string(current_open_month) + &
	"   and reversal_lag < 200000 "

if isnull(g_command_line_args) or trim(g_command_line_args) = '' then
	// All companies
else
	// Specific company	
	sqls += "and company_value = '" + g_command_line_args + "' "
end if

uo_ds_top ds_jrnls_to_reverse
ds_jrnls_to_reverse = create uo_ds_top

rtn_str = f_create_dynamic_ds(ds_jrnls_to_reverse,"grid",sqls,sqlca,true)
if rtn_str <> "OK" then
	f_pp_msgs(" ")
	f_pp_msgs("ERROR: Retrieving journals to reverse: " + rtn_str + ": " + ds_jrnls_to_reverse.i_sqlca_sqlerrtext)
	f_pp_msgs("SQL: " + sqls)
	f_pp_msgs(" ")
	overall_rtn = -1
	goto exit_and_return
end if
num_jrnls = ds_jrnls_to_reverse.rowcount()

// Elements
uo_ds_top ds_elements
ds_elements = CREATE uo_ds_top
sqls = 'select * from cr_elements order by "ORDER" '
f_create_dynamic_ds(ds_elements, "grid", sqls, sqlca, true)
num_elements = ds_elements.RowCount()

//  Source specific fields ...
datastore ds_fields
ds_fields = CREATE datastore
sqls = 'select * from cr_sources_fields where source_id = ' + string(source_id)
f_create_dynamic_ds(ds_fields,'grid',sqls,sqlca,true)
num_fields = ds_fields.rowcount()

f_pp_msgs("")
f_pp_msgs("There are " + string(num_jrnls) + " journals to reverse.")
f_pp_msgs("")

for l = 1 to num_jrnls
	
	sqls = "insert into cr_journal_lines (id,"
	
	journal_id = ds_jrnls_to_reverse.getitemnumber(l,"journal_id")
	description = ds_jrnls_to_reverse.getitemstring(l,"description")
	
	f_pp_msgs("")
	f_pp_msgs("Reversing Journal : " + string(journal_id) + " : " + description )
	f_pp_msgs(" ... Journal " + string(l) + " of " + string(num_jrnls))
	f_pp_msgs("")
	
	//	DML: 071604: If a reclass journal was marked to reverse this code will
	//		give the appearance the journal was being reversed when it really
	//		was not.  Don't include the restriction in the sql b/c the user might
	//		be confused that they have the journal checked to reverse but it is not
	//		reversing.  Put it in the log during this loop.
	counter = 0
	select count(*) into :counter
	from cr_journal_lines
	where journal_id = :journal_id;
	
	if isnull(counter) then counter = 0
	
	if counter = 0 then
		f_pp_msgs(" ... THIS IS A RECLASS JOURNAL AND WILL NOT BE REVERSED!")
		f_pp_msgs(" ... ... ONLY MANUAL JOURNALS ORIGINATING IN THE CR CAN BE REVERSED.")
		f_pp_msgs("")
		continue
	end if
	
	//
	//	Build the insert piece of the string
	//
	
	for i = 1 to num_elements
		element = ds_elements.GetItemString(i, "description")
		element = f_cr_clean_string(element)
		sqls = sqls + '"' + upper(element) + '", '
	next
	
	sqls = sqls + "dr_cr_id, ledger_sign, month_number, month_period, gl_journal_category, " + &
			  "amount_type, drilldown_key, quantity, amount,"
		
	//
	//  Source specific fields ...
	//
	for ii = 1 to num_fields
		
		element  = lower(ds_fields.GetItemString(ii, "description"))
		element = f_cr_clean_string(element)
		
		if ii = num_fields then
			sqls = sqls + '"' + upper(element) + '") select crdetail.nextval, ' 
		else
			sqls = sqls + '"' + upper(element) + '", ' 
		end if
		
	next	
	
	//
	// Build the select piece of the string
	//
	for i = 1 to num_elements
		element = ds_elements.GetItemString(i, "description")
		element = f_cr_clean_string(element)
		sqls = sqls + '"' + upper(element) + '", '
	next
	
	sqls = sqls + "dr_cr_id * -1, ledger_sign, " + string(current_open_month) + ", month_period, gl_journal_category, " + &
			  "amount_type, null drilldown_key, quantity * -1, amount * -1, "
	
	//
	//  Source specific fields ...
	//
	for ii = 1 to num_fields
		
		element  = lower(ds_fields.GetItemString(ii, "description"))
		element = f_cr_clean_string(element)
		
		choose case element
			case 'cr_reversal_code' 
				sqls += "'R', "
			case 'cr_derivation_status'
				sqls += "null, "
			case else
				sqls += '"' + upper(element) + '", ' 
		end choose
	next
	sqls = mid(sqls,1,len(sqls) - 2)
	sqls = sqls + " from cr_journal_lines where journal_id = " + string(journal_id)
	//	DML: 120204: If a reclass is done on a journal before it is reversed
	//		it will hit the journal lines source.  The journal_id is a detail
	//		attribute so it is retained.  However, a new interface_batch_id is
	//		assigned.  To indentify only the original entry for reversal add an
	//		additional restriction to check that the journal_id = interface_batch_id
	sqls = sqls + " and to_char(journal_id) = interface_batch_id "
	
	execute immediate :sqls;
	
	if sqlca.sqlcode < 0 then
		destroy ds_elements
		destroy ds_jrnls_to_reverse
		f_pp_msgs("")
		f_pp_msgs("ERROR: Inserting into cr_journal_lines (journal_id = " + string(journal_id) + &
			") : " + sqlca.sqlerrtext)
		f_pp_msgs(sqls)	
		f_pp_msgs("")	
		rollback;
		overall_rtn = -1
		goto exit_and_return
//		return -1
	end if
	
	//  DML: 010509: 2561: Obey the "Post to" flags on JE reversals
	post_to_gl = 0
	post_to_projects = 0
	select post_to_gl, post_to_projects into :post_to_gl, :post_to_projects
	 from cr_journals
	where journal_id = :journal_id;
	
	if isnull(post_to_gl) then post_to_gl = 1
	if isnull(post_to_projects) then post_to_projects = 1
	
	// DMJ: 9/4/08: Needed this to tag an accounting key field with a special value on reversals
	//    of JE's.
	rtn = f_cr_man_jrnl_reversals_custom(1, journal_id)
	
	if rtn < 0 then
		destroy ds_elements
		destroy ds_jrnls_to_reverse
		//  No msgs ... handle it in the custom function.
		rollback;
		overall_rtn = -1
		goto exit_and_return
//		return -1
	end if
	
	//	Build the summary
//	rtn = uf_cr_cost_repository(journal_id,current_open_month,source_id,post_to_gl,post_to_projects)
	// Update to use the general function of the base UO
	where_clause = " where journal_id = " + string(journal_id)
	where_clause += " and month_number = " + string(current_open_month)
	where_clause += " and to_char(journal_id) = interface_batch_id "
	where_clause += " and cr_reversal_code = 'R' " 
	where_clause += " and drilldown_key is null"
	
	if post_to_gl = 1 then
		//  OK to post.
		batch_id = ''
		gl_id = ''
	else
		//  DO NOT POST to the GL.
		batch_id = '-id'
		gl_id = '-id'
	end if
	
	if post_to_projects = 1 then
		//  OK to post.
		ccs = ''
	else
		//  DO NOT POST to the GL.
		ccs = '-id'
	end if
		
		
	rtn = uo_cr.uf_insert_to_cr_general(source_id,where_clause,gl_id,batch_id,ccs)
	
	if rtn = 1 then
	else
		choose case rtn
			case -1
				f_pp_msgs("ERROR: inserting into cr_temp_cr: " + sqlca.SQLErrText)
			case -2
				f_pp_msgs("ERROR: cr_temp_cr.id UPDATE failed: " + sqlca.SQLErrText)			
			case -3
				f_pp_msgs("ERROR: cr_temp_cr.drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
			case -4
				f_pp_msgs("ERROR: cr_general_ledger.drilldown_key UPDATE failed: " + sqlca.SQLErrText)			
			case -5
				f_pp_msgs("ERROR: inserting into cr_cost_repository: " + sqlca.SQLErrText)			
			case -6
				f_pp_msgs("ERROR: deleting from cr_temp_cr UPDATE failed: " + sqlca.SQLErrText)			
			case else
				f_pp_msgs("ERROR: unknown error in uo_cr_cost_repository: " + sqlca.SQLErrText)			
		end choose
		rollback;
		overall_rtn = -1
		goto exit_and_return
	end if

//	if sqlca.sqlcode < 0 then
//		destroy ds_elements
//		destroy ds_jrnls_to_reverse
//		f_pp_msgs("")
//		f_pp_msgs("ERROR: Inserting into cr_cost_repository (journal_id = " + string(journal_id) + &
//			") : " + sqlca.sqlerrtext)
//		f_pp_msgs("Function Return Code = : " + string(rtn))	
//		f_pp_msgs("")	
//		rollback;
//		overall_rtn = -1
//		goto exit_and_return
////		return -1
//	end if
	
	// DMJ: 10/07/08: Needed this to mark the batch_id to 15 nines so these would be
	//    auto-approved for posting at Southern.
	rtn = f_cr_man_jrnl_reversals_custom(2, journal_id)
	
	if rtn < 0 then
		destroy ds_elements
		destroy ds_jrnls_to_reverse
		//  No msgs ... handle it in the custom function.
		rollback;
		overall_rtn = -1
		goto exit_and_return
//		return -1
	end if
	
	//
	//	Now, Update the reversal_date on the journal header
	//
	update cr_journals
	set reversal_date = sysdate
	where journal_id = :journal_id;
	
	if sqlca.sqlcode < 0 then
		destroy ds_elements
		destroy ds_jrnls_to_reverse
		f_pp_msgs("")
		f_pp_msgs("ERROR: Updating cr_journals.reversal_date (journal_id = " + string(journal_id) + &
			") : " + sqlca.sqlerrtext)
		f_pp_msgs("")	
		rollback;
		overall_rtn = -1
		goto exit_and_return
//		return -1
	end if
	
	//	Commit after each journal is successfully reversed in the detail and the summary
	commit;
	
	// JAK 12/04/2006 - Added code to memo validation the records that were posted by the 
	//		man je reversal.
	if enable_validations = 'YES' then
		//*****************************************************************************************
		//
		//	 VALIDATIONS
		//
		//*****************************************************************************************
		sqls2 = "analyze table pwrplant.cr_validations_invalid_ids compute statistics"
		
		execute immediate :sqls2;
		
		f_pp_msgs("Running validations at: " + string(now()))
		
		f_pp_msgs(" -- Deleting from cr_validations_invalid_ids at: " + string(now()))
		
		lvalidate_rtn = l_uo_cr_validation.uf_delete_invalid_ids("cr_journal_lines", string(journal_id))
		
		if lvalidate_rtn <> 1 then
			overall_rtn = -1
			goto exit_and_return
	//		return -1
		end if
		
		validation_kickouts = false
		invalid_mn          = false
		
		setnull(cv_validations)
		select upper(trim(control_value)) into :cv_validations from cr_system_control 
		 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - COMBO';
		if isnull(cv_validations) then cv_validations = "NO"
		
		if cv_validations <> "YES" then goto after_combo_validations
		
		f_pp_msgs(" -- Validating the Batch (Combos) at: " + string(now()))
		
		//	 Which function is this client using?
		setnull(cv_validations)
		select upper(trim(control_value)) into :cv_validations from cr_system_control 
		 where upper(trim(control_name)) = 'VALIDATIONS - CONTROL OR COMBOS';
		if isnull(cv_validations) then cv_validations = "CONTROL"
		
		if cv_validations = 'CONTROL' then
			lvalidate_rtn = l_uo_cr_validation.uf_validate_control("cr_journal_lines", string(journal_id))
		else
			lvalidate_rtn = l_uo_cr_validation.uf_validate_combos("cr_journal_lines", string(journal_id))
		end if
		
		if lvalidate_rtn = -1 then
			//  Error in uf_validate_(x).  Logged by the function.
			overall_rtn = -1
			goto exit_and_return
	//		return -1
		end if
		
		if lvalidate_rtn = -2 then
			//  Validation Kickouts.
			validation_kickouts = true
		end if
		
		after_combo_validations:
		
		setnull(cv_validations)
		select upper(trim(control_value)) into :cv_validations from cr_system_control 
		 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
		if isnull(cv_validations) then cv_validations = "NO"
		
		if cv_validations <> "YES" then goto after_project_validations
		
		f_pp_msgs(" -- Validating the Batch (Projects-Based) at: " + string(now()))
		
		lvalidate_rtn = l_uo_cr_validation.uf_validate_projects("cr_journal_lines", string(journal_id))
		
		if lvalidate_rtn = -1 then
			//  Error in uf_validate_(x).  Logged by the function.
			overall_rtn = -1
			goto exit_and_return
	//		return -1
		end if
		
		if lvalidate_rtn = -2 then
			//  Validation Kickouts.
			validation_kickouts = true
		end if
		
		after_project_validations:
		
		setnull(cv_validations)
		select upper(trim(control_value)) into :cv_validations from cr_system_control 
		 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - ME';
		if isnull(cv_validations) then cv_validations = "NO"
		
		if cv_validations <> "YES" then goto after_me_validations
		
		f_pp_msgs(" -- Validating the Batch (Element Values) at: " + string(now()))
		
		lvalidate_rtn = l_uo_cr_validation.uf_validate_me("cr_journal_lines", string(journal_id))
		
		if lvalidate_rtn = -1 then
			//  Error in uf_validate.  Logged by the function.
			overall_rtn = -1
			goto exit_and_return
	//		return -1
		end if
		
		if lvalidate_rtn = -2 then
			//  Validation Kickouts.
			validation_kickouts = true
		end if
		
		after_me_validations:
		
		setnull(cv_validations)
		select upper(trim(control_value)) into :cv_validations from cr_system_control 
		 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MONTH NUMBER';
		if isnull(cv_validations) then cv_validations = "NO"
		
		if cv_validations <> "YES" then goto after_mn_validations
		
		f_pp_msgs(" -- Validating the Batch (Month Number) at: " + string(now()))
		// SEK 100209: Set i_called_from_validator to true to skip the month number validations
		//     and only perform the acct range validations.  It's pointless to validate
		//     month numbers because the month number for the original journal entries
		//     will always be closed if it is being reversed.
		l_uo_cr_validation.i_called_from_validator = true

		lvalidate_rtn = l_uo_cr_validation.uf_validate_month_number("cr_journal_lines", string(journal_id))
		
		if lvalidate_rtn = -1 then
			//  Error in uf_validate.  Logged by the function.
			overall_rtn = -1
			goto exit_and_return
	//		return -1
		end if
		
		if lvalidate_rtn = -2 or lvalidate_rtn = -3 then
			//  Validation Kickouts.
			validation_kickouts = true
		end if
		
		after_mn_validations:
		
		// Can skip month number validations as the month has to be open for the process to be 
		//		attempting to post to it.
		
		setnull(cv_validations)
		select upper(trim(control_value)) into :cv_validations from cr_system_control 
		 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - CUSTOM';
		if isnull(cv_validations) then cv_validations = "NO"
		
		if cv_validations <> "YES" then goto after_custom_validations
		
		f_pp_msgs(" -- Validating the Batch (Custom Validations) at: " + string(now()))
		
		lvalidate_rtn = l_uo_cr_validation.uf_validate_custom("cr_journal_lines", string(journal_id))
		
		if lvalidate_rtn = -1 then
			//  Error in uf_validate.  Logged by the function.
			overall_rtn = -1
			goto exit_and_return
	//		return -1
		end if
		
		if lvalidate_rtn = -2 then
			//  Validation Kickouts.
			validation_kickouts = true
		end if
		
		after_custom_validations:
	
		if validation_kickouts then
				// Since we could be running through validations multiple times.  Will save these messages
				//		until the very end.
				validation_kickouts_total = true
		end if
		
		// Have to clear these tables so that the next run processes properly
		lvalidate_rtn = l_uo_cr_validation.uf_delete_invalid_ids("cr_journal_lines", string(journal_id))
		if lvalidate_rtn <> 1 then 
			overall_rtn = -1
			goto exit_and_return
	//		return -1
		end if
		
		f_pp_msgs("Validations complete at: " + string(now()))
		
	end if
	
next

if enable_validations = 'YES' then
	destroy l_uo_cr_validation
	
	if validation_kickouts_total then
		f_pp_msgs("  ")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("   *****  MEMO VALIDATION ERRORS EXIST!  " + &
			"THE REVERSALS HAVE BEEN POSTED!  REVIEW ABOVE FOR THE ERRORS!  *****")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"-------------------------------------------------------------")
		f_pp_msgs("  ")
		
		// ### 8142: JAK: 2011-11-01:  Consistent return codes
		overall_rtn = -2
	end if
end if

exit_and_return:
// ### 8142: JAK: 2011-11-01:  Consistent return codes
if overall_rtn < 0  and overall_rtn <> -2 then
	//	Find the user (from cr_system_control) to send a failure e-mail to.
	select upper(control_value) into :to_user
	from cr_system_control
	where upper(control_name) = 'INTERFACE ERROR EMAIL ADDRESS';
	
	if isnull(to_user) or to_user = "" or to_user = 'NONE' then
	else
		g_msmail = create uo_smtpmail
		uo_winapi = create u_external_function_win32
		f_send_mail('pwrplant', 'INTERFACE FAILURE', 'The CR_MAN_JRNL_REVERSALS ' + &
			'encountered an error and terminated.', to_user)
	end if
	rollback;
end if

return overall_rtn
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_cr_man_jrnl_reversals_custom_version nvo_cr_man_jrnl_reversals_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'cr_man_jrnl_reversals_custom.pbd'
		return nvo_cr_man_jrnl_reversals_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

