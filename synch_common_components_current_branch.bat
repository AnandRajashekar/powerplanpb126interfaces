@echo off

REM Pulls common objects from the CURRENT BRANCH IN THE POWERPLANT repository
REM		into the current branch in the CR Repository

REM TO DO LIST
REM    - uo_wo_cr_derivations to build deriver
REM    - branch not linked to dev branch in plastic GUI


REM **********************************************************************
REM  MAIN FUNCTION
REM **********************************************************************
REM -- SETUP
set location=1
set app_rep=Powerplant_Master
set app_branch_name=/main/develop
set int_branch_name=/main/develop

REM ---- CR
cd "C:\PlasticWks\ppinterfaces\"


REM -- Switch to the appropriate, app_branch_name, in application repository, app_rep
REM -- and get the latest files
REM cm switchtobranch br:%app_branch_name% --repository=%app_rep%

goto %location%

REM **********************************************************************
:1
REM -- PPCOSTRP_INTERFACE - pull from ppcostrp
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppcostrp_interface
set app_dir=C:\PlasticWks\powerplant\src\ppcostrp
goto copy_files

REM **********************************************************************
:2
REM -- PPCOSTRP_INTERFACE - pull from PPEXTENSIONS
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppcostrp_interface
set app_dir=C:\PlasticWks\powerplant\src\ppextensions
goto copy_files

REM **********************************************************************
:3
REM -- PPSYSTEM_INTERFACE - pull from PPSYSTEM
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppsystem_interface
set app_dir=C:\PlasticWks\powerplant\src\ppsystem
goto copy_files


REM **********************************************************************
:4
REM -- PPSYSTEM_INTERFACE - pull from PPBASELOGIC
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppsystem_interface
set app_dir=C:\PlasticWks\powerplant\src\ppbaselogic
goto copy_files


REM **********************************************************************
:5
REM -- ppprojct_interface - pull from ppprojct
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppprojct_interface
set app_dir=C:\PlasticWks\powerplant\src\ppprojct
goto copy_files


REM **********************************************************************
:6
REM -- ppsystem_interface - pull from ppbasegui
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppsystem_interface
set app_dir=C:\PlasticWks\powerplant\src\ppbasegui
goto copy_files


REM **********************************************************************
:7
REM -- ppprojct_interface - pull from ppprojct
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppprojct_interface
set app_dir=C:\PlasticWks\powerplant\src\ppbudget
goto copy_files


REM **********************************************************************
:8
REM -- ppdepr_interface - pull from ppdepr
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppdepr_interface\ppdepr_interface
set app_dir=C:\PlasticWks\PowerPlant\src\ppdepr
goto copy_files


REM **********************************************************************
:9
REM -- ppcpr_interface - pull from ppcpr
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppcpr_interface\ppcpr_interface
set app_dir=C:\PlasticWks\PowerPlant\src\ppcpr
goto copy_files


REM **********************************************************************
:10
REM -- ppcprdw_interface - pull from ppcprdw
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppcprdw_interface\ppcprdw_interface
set app_dir=C:\PlasticWks\PowerPlant\src\ppcprdw
goto copy_files


REM **********************************************************************
:11
REM -- pparo_interface - pull from pparo
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\pparo_interface\pparo_interface
set app_dir=C:\PlasticWks\PowerPlant\src\pparo
goto copy_files


REM **********************************************************************
:12
REM -- ppstudy_interface - pull from ppstudy
set int_rep=Interfaces_CR
set int_dir=C:\PlasticWks\ppinterfaces\ppstudy_interface\ppstudy_interface
set app_dir=C:\PlasticWks\PowerPlant\src\ppstudy
goto copy_files


REM **********************************************************************
:13

REM -- Remove branches that are not needed.
cd "C:\PlasticWks\ppinterfaces"

echo Processing Complete.
pause
goto :eof
















:copy_files
REM **********************************************************************
REM  SUBROUTINE TO FIND, CHECK OUT, COPY, AND CHECK IN
REM **********************************************************************

cd %int_dir%
echo .

ECHO -- Pulling latest files from plastic

REM -- Switch to the appropriate, app_branch_name, in application repository, app_rep
REM -- and get the latest files
cm switchtobranch br:%app_branch_name% --workspace=%app_dir% > NUL

REM Update Forced
cm update --forced --override %app_dir% > NUL


echo test 
for %%F in (*.sr*) DO (
	
	
	if exist %app_dir%\%%F (
	
		REM EXCEPTIONS
		REM		- w_mail_print  - remove inheritance from w_top 
		REM		- uo_ds_top (contains reference to cancel query functions.  Hints also?)
		REM		- f_progressbar (stubbed in interface)
		REM		- f_set_datastore_hint (can't set hints)
		REM		- f_status_box (stubbed in interface)
		REM		- f_wo_validation_modify (stubbed in interface)
		REM		- uo_ppbase_datawindow_dropdown (stubbed in interface)
		if not %%F==uo_ppbase_datawindow_dropdown.sru (
		if not %%F==w_mail_print.srw (
		if not %%F==uo_ds_top.sru (
		if not %%F==uo_ppbase_workspace.sru (
		REM if not %%F==f_progressbar.srf (
		if not %%F==f_set_datastore_hint.srf (
		if not %%F==f_status_box.srf (
		if not %%F==f_wo_validation_modify.srf (
		REM CBS 2014/11/12 - for synced components that use f_pp_msgs_box, I had to pull
		REM 		f_pp_msgs_box into ppsystem_interface.pbl and modify it to ignore the 
		REM			messagebox part and only do the online logs. 
		if not %%F==f_pp_msgs_box.srf (
		if not %%F==f_setmicrohelp_w_top.srf (
		if not %%F==f_is_status_box_enabled.srf (
		
			REM PROCESS THE FILE
			Echo  **************************************************************
			Echo  ---- %%F ----
			
			Echo  ---- To:   %int_dir%
			echo  ---- From: %app_dir%
			Echo  **************************************************************
			cm checkout %%F
			copy %app_dir%\%%F %int_dir%\%%F
			cm uncounchanged
			
			REM TEMP DISABLE AUTO CHECKIN
			REM TEMP cm checkin %%F
			
			echo .
			
		REM 10 exceptions above = 10 parens here
		))))))))))
	)
)

REM RETURN TO THE MAIN FUNCTION ABOVE
set /a location=location+1
goto %location%