HA$PBExportHeader$uo_pp_int_imp_exp_files.sru
$PBExportComments$1) Substr to width~r~n2) Check formulated sql~r~n3) move the delete (corruption) of the bat file to right after it is being ran. See if it work that way.
forward
global type uo_pp_int_imp_exp_files from nonvisualobject
end type
end forward

global type uo_pp_int_imp_exp_files from nonvisualobject
end type
global uo_pp_int_imp_exp_files uo_pp_int_imp_exp_files

forward prototypes
public function long of_import_file (string a_batch_name)
protected subroutine uf_msgs (string a_title, string a_msg)
public function string of_get_arc_file_name (string a_batch_name, string a_orig_file_name, string a_arc_file_name)
public function long of_export_file (string a_batch_name)
public function boolean of_check_table_exists (string a_table_name)
public function boolean of_import_xml_file (string a_filename, string a_filepath, string a_table, string a_tablecolumn[], string a_xmltag[], string a_arcfilename, string a_arcfilepath, string a_transtag)
public function boolean of_xml_populate_structure (long a_index, string a_name[], string a_value[], ref s_xml_element a_s_name[], ref s_xml_element a_s_value[])
public function boolean of_xml_insert_records (string a_table_name, string a_field_name[], string a_xml_tag_name[], s_xml_element a_s_name[], s_xml_element a_s_value[], ref string a_message)
public function string of_xml_get_elements_value (string a_match_string, s_xml_element a_s_name, s_xml_element a_s_value)
public function long of_export_xml (string a_batch_name)
public function boolean of_xml_add_node (ref pbdom_element a_parent_node, string a_name, string a_attrib, string a_attrib_value, string a_value)
public function long of_import_xml (string a_batch_name)
public function boolean of_xml_get_elements_and_values (pbdom_element a_node, ref string a_element_name[], ref string a_element_value[])
public function boolean of_xml_build_transaction_data (ref pbdom_element a_parent_node, string a_def_node_name, datastore a_ds, string a_def_name[], string a_def_value[])
public function boolean of_xml_add_transaction_data (ref pbdom_element a_parent_node, string a_def_node_name, datastore a_ds, long a_index, string a_def_name[], string a_def_type[])
end prototypes

public function long of_import_file (string a_batch_name);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   uo_pp_int_imp_exp_files.of_import_file
//
//   Purpose  :  Import files base on the setup in pp_int_file_import and pp_int_file_import_col
// 
//   Scope    :   global
//
//   Arguments:		(v) a_batch_name		:  String			: The batch name used in pp_int_file_import
//
//   Returns :   1 = success ; -1 = failure
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************

longlong rtn, i
string sqls,str_rtn, str_file_path, str_file_name, str_table_name, str_delimiter, str_arc_file_path, str_arc_file_format, str_arc_file_name
longlong lng_start_row, lng_direct, lng_append, lng_fixed_width, file_col_len[]
boolean is_delimiter, is_direct, is_append
string file_name_path_found[], file_name_found[]
string file_col_order[]
s_sqlldr_col_pos s_col_pos, s_file_col_order[]

uo_ds_top ds_temp
ds_temp = create uo_ds_top

nvo_import_export_files uo_files
uo_files = create nvo_import_export_files

//// Default values
is_delimiter = true 
is_direct = false
is_append = true

select count(1) into :rtn
from pp_int_file_import 
where batch_name = :a_batch_name;
if rtn < 1 then
	uf_msgs(a_batch_name, "Error:  No batch setup found for " + a_batch_name + "!")
	destroy uo_files
	destroy ds_temp
	return -1
end if

select file_path, file_name, table_name, delimiter, decode(nvl(data_start_row,1),0,1,nvl(data_start_row,1)), nvl(direct, 0), nvl(append,0), nvl(is_fixed_width,0), 
		nvl(archive_file_path, file_path), nvl(archive_file_name, '{sysdate}_{file_name}.{ext}')
into :str_file_path, :str_file_name, :str_table_name, :str_delimiter, :lng_start_row, :lng_direct, :lng_append, :lng_fixed_width, 
		:str_arc_file_path, :str_arc_file_format
from pp_int_file_import
where batch_name = :a_batch_name;

if lng_fixed_width = 0 then
	is_delimiter = true
else
	is_delimiter = false
end if

if lng_append = 0 then
	is_append = false
else
	is_append = true
end if

if lng_direct = 0 then
	is_direct = false
else
	is_direct = true
end if

//MAINT-44727 JAW: make sure only one of direct/append is selected and if direct
//		is selected, empty the specified table
if is_direct and is_append then
	is_direct = false
end if

if is_direct then
	sqls = "delete from " + str_table_name
	
	execute immediate :sqls;
	commit;
end if
//END MAINT-44727

sqls = + &
" select column_name, start_pos, end_pos " + "~r~n" + &
" from pp_int_file_import_col " + "~r~n" + &
" where batch_name = '"+a_batch_name+"' " + "~r~n" + &
" order by column_order " 

str_rtn = f_create_dynamic_ds(ds_temp, 'grid', sqls, sqlca, true)
if str_rtn <> "OK" then
	uf_msgs(a_batch_name, "Error:  Creating Column DS !")
	uf_msgs(a_batch_name, "  " + sqls)
	destroy uo_files
	destroy ds_temp
	return -1
end if

if is_delimiter then
////
//// Delimited File 
//// Get the columns
////
	for i = 1 to ds_temp.rowCount() 
		file_col_order[i] = ds_temp.getItemString(i, 'column_name')
//JAW PP-45168: changed from user_tab_columns to all_tab_columns to allow functionality for non-pwrplant users
//BGT:06302015:adding column length to be passed to uf_run_sqlldr_delimited as argument
		select to_number(data_length) into :file_col_len[i] from all_tab_columns where owner='PWRPLANT' and table_name=upper(:str_table_name) and column_name=upper(:file_col_order[i]);
//BGT:END
next
	if upperbound(file_col_order) < 1 then
		uf_msgs(a_batch_name, "Error:  No Column Setup for " + a_batch_name + "!")
		destroy uo_files
		destroy ds_temp
		return -1
	end if
	////	Get the number of files to load
	rtn = uo_files.uf_get_file_names(str_file_path, str_file_name, file_name_found[], file_name_path_found[])
	if rtn < 1 then 
		uf_msgs(a_batch_name, "No file found for " + str_file_name + " in " + str_file_path)
		destroy uo_files
		destroy ds_temp
		return 1//// Not an error
	end if
	
	////	For each file load into table
	for i = 1 to rtn
		//// If more than 1 file is found always append to table and turn off direct load
		if i > 1 then
			is_direct = false
			is_append = true				
		end if
		
		if not uo_files.uf_run_sqlldr_delimited(file_name_path_found[i], str_table_name, str_delimiter, file_col_order[], lng_start_row, is_direct, is_append, file_col_len[]) then
			uf_msgs(a_batch_name, "Error: Sql Loader Failed!")
			uf_msgs(a_batch_name, file_name_path_found[i])
			destroy uo_files
			destroy ds_temp
			return -1
		else 
			uf_msgs(a_batch_name, "Success: "+file_name_found[i]+" Loaded!")
		end if
		
		//// Set Archive File Name
		str_arc_file_name = of_get_arc_file_name(a_batch_name, file_name_found[i], str_arc_file_format)
		uf_msgs(a_batch_name, "Archive File: "+file_name_found[i]+" to " + str_arc_file_path+str_arc_file_name)
		rtn = f_file_rename(file_name_path_found[i],str_arc_file_path+str_arc_file_name)
		if rtn <> 1 then
			//// The file is loaded, display warning and move on.
			uf_msgs(a_batch_name, "WARNING ARCHIVING FILE FAILED! PLEASE MOVE FILE MANUALLY!!!")
		end if
	next
	
else
////
//// Fixed width File
//// Get the column name, start position and end position
////
	for i = 1 to ds_temp.rowCount() 
		s_col_pos.column_name = ds_temp.getItemString(i, 'column_name')
		s_col_pos.start_pos= ds_temp.getItemNumber(i, 'start_pos')
		s_col_pos.end_pos = ds_temp.getItemNumber(i, 'end_pos')
		s_file_col_order[i] = s_col_pos
	next
	
	if upperbound(s_file_col_order) < 1 then
		uf_msgs(a_batch_name, "Error:  No Column Setup for " + a_batch_name + "!")
		destroy uo_files
		destroy ds_temp
		return -1
	end if	
	
	////	Get the number of files to load
	rtn = uo_files.uf_get_file_names(str_file_path, str_file_name, file_name_found[], file_name_path_found[])
	if rtn < 1 then 
		uf_msgs(a_batch_name, "No file found for " + str_file_name + " in " + str_file_path)
		destroy uo_files
		destroy ds_temp
		return 1//// Not an error
	end if
	
	////	For each file load into table
	for i = 1 to rtn
		//// If more than 1 file is found always append to table and turn off direct load
		if i > 1 then
			is_direct = false
			is_append = true				
		end if
		
		if not uo_files.uf_run_sqlldr_fixed_width(file_name_path_found[i], str_table_name, s_file_col_order[], lng_start_row, is_direct, is_append) then
			uf_msgs(a_batch_name, "Error: Sql Loader Failed! ")
			uf_msgs(a_batch_name, file_name_path_found[i])
			destroy uo_files
			destroy ds_temp
			return -1
		else 
			uf_msgs(a_batch_name, "Success: "+file_name_found[i]+" Loaded!")
		end if	

		//// Set Archive File Name
		str_arc_file_name = of_get_arc_file_name(a_batch_name, file_name_found[i], str_arc_file_format)
		uf_msgs(a_batch_name, "Archive File: "+file_name_found[i]+" to " + str_arc_file_path+str_arc_file_name)
		//// Rename/Move the file to archive.
		rtn = f_file_rename(file_name_path_found[i],str_arc_file_path+str_arc_file_name)
		if rtn <> 1 then
			//// The file is loaded, display warning and move on.
			uf_msgs(a_batch_name, "WARNING ARCHIVING FILE FAILED! PLEASE MOVE FILE MANUALLY!!!")
		end if
	next
end if

	

destroy uo_files
destroy ds_temp
return 1
end function

protected subroutine uf_msgs (string a_title, string a_msg);if g_main_application then 
	f_status_box(a_title,a_msg)
else
	f_pp_msgs(a_msg)
end if
end subroutine

public function string of_get_arc_file_name (string a_batch_name, string a_orig_file_name, string a_arc_file_name);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   uo_pp_int_imp_exp_files.of_get_file_name
//
//   Purpose  :	Replace keywords with actual value
//					identified keywords:
//						{file_name}		= Original file name
//						{ext}				= Original extention of the file
//						{sysdate}		= system date formated as YYYYMMDD_H24MISS
//						{proc_id}		= the process id of the process
//						{occ_id}			= the occurrence id of this process
//						{batch_name}	= the batch name of this import
// 
//   Scope    :   global
//
//   Arguments:		(v) a_batch_name		:  String			: The batch name used in pp_int_file_import
//						(v) a_orig_file_name	:  String			: The original file name
//						(v) a_arc_file_name	:  String			: The archive file name/format
//
//   Returns :   1 = success ; -1 = failure
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
string str_rtn, str_file_name, str_ext, str_sysdate

str_rtn = a_arc_file_name

/*Get all variables....*/
str_sysdate = String(Today(), "YYYYMMDD_hhmmss")
str_file_name = left(a_orig_file_name,lastPos(a_orig_file_name,'.')-1)
str_ext = right(a_orig_file_name,len(a_orig_file_name) - lastPos(a_orig_file_name,'.'))

str_rtn = f_replace_string(str_rtn,'{file_name}',str_file_name,'all')
str_rtn = f_replace_string(str_rtn,'{ext}',str_ext,'all')
str_rtn = f_replace_string(str_rtn,'{sysdate}',str_sysdate,'all')
str_rtn = f_replace_string(str_rtn,'{proc_id}',string(g_process_id),'all')
str_rtn = f_replace_string(str_rtn,'{occ_id}',string(g_occurrence_id),'all')
str_rtn = f_replace_string(str_rtn,'{batch_name}',a_batch_name,'all')

if isnull(trim(str_rtn)) or trim(str_rtn) = "" then
	return a_arc_file_name
end if

return str_rtn
end function

public function long of_export_file (string a_batch_name);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   uo_pp_int_imp_exp_files.of_export_file
//
//   Purpose  :  Export files base on the setup in pp_int_file_export and pp_int_file_export_col
// 
//   Scope    :   global
//
//   Arguments:		(v) a_batch_name		:  String			: The batch name used in pp_int_file_import
//
//   Returns :   1 = success ; -1 = failure
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
longlong rtn, i
string str_file_path, str_file_name, str_table_name, str_sql, str_delimiter, data_type
longlong lng_header_row, lng_fixed_width, lng_width, ttl_col
boolean has_header_row, is_fixed_width, uses_table

s_sqlldr_col_pos s_col_pos, s_file_col_order[]

string sqls, str_rtn
uo_ds_top ds_temp
ds_temp = create uo_ds_top

//// Make sure we have a batch with the name passed in
select count(1) into :rtn
from pp_int_file_export
where batch_name = :a_batch_name;
if rtn < 1 then
	uf_msgs(a_batch_name, "Error:  No batch setup found for " + a_batch_name + "!")
	destroy ds_temp
	return -1
end if
//// get all configuration
////Defaults
////	File_name
//// 		If file_name is empty use table_name + sysdate and batch name, 
//// 		if table name is empty (straight sql select) then just use sysdate and batch_name 
////	SQL: Removes all carriage returns
////	include header Defaults 0
////	is_fixed_width default to 0
////	delimiter defaults to "|"
select file_path, nvl(file_name, nvl(table_name,'')||'_{sysdate}_{batch_name}.txt'), table_name, replace(replace(replace(nvl(sql,' '),chr(10),' '),chr(13),' '), Chr(9), ' '), nvl(include_header_row,0), nvl(is_fixed_width,0), nvl(delimiter,'|')
into :str_file_path, :str_file_name, :str_table_name, :str_sql, :lng_header_row, :lng_fixed_width, :str_delimiter
from pp_int_file_export
where batch_name = :a_batch_name;

if isnull(trim(str_file_path)) or trim(str_file_path) = "" then
	uf_msgs(a_batch_name, "Error:  No File path setup for " + a_batch_name + "!")
	destroy ds_temp
	return -1
end if

//// set boolean for header row and fixed width
if lng_header_row = 0 then
	has_header_row = false
else
	has_header_row = true
end if

if lng_fixed_width = 0 then
	is_fixed_width = false
else
	is_fixed_width = true
end if

//// Find out if it is using table or straight SQL.
//// Not sure if this is the best way.... BUT
//// If the sql is used and starts with "WHERE" then 
//// Make sure that the table_name is populated.
//// Also make sure that the table exists
rtn = pos(lower(trim(str_sql)),'where') 
if rtn = 1 then
	uses_table = true
elseif not isnull(str_table_name) and isnull(str_sql) or trim(str_sql) = "" then
	uses_table = true
else
	uses_table = false
end if

if uses_table then
	if isnull(trim(str_table_name)) or trim(str_table_name) = "" then
		uf_msgs(a_batch_name, "Error:  No table name setup for " + a_batch_name + "!")
		destroy ds_temp
		return -1		
	end if
	if not of_check_table_exists(str_table_name) then
		uf_msgs(a_batch_name, "Error:  Table "+ str_table_name + " setup for " + a_batch_name + " does not exists!")
		destroy ds_temp
		return -1		
	end if
end if

//// Get file file name 
str_file_name = of_get_arc_file_name(a_batch_name, a_batch_name+'.txt', str_file_name)
uf_msgs(a_batch_name, "File will be exported to : "+str_file_path + str_file_name)

//// Get the columns
sqls = + &
" select column_name, start_pos, end_pos " + "~r~n" + &
" from pp_int_file_export_col " + "~r~n" + &
" where batch_name = '"+a_batch_name+"' " + "~r~n" + &
" order by column_order " 

str_rtn = f_create_dynamic_ds(ds_temp, 'grid', sqls, sqlca, true)
if str_rtn <> "OK" then
	uf_msgs(a_batch_name, "Error:  Creating Column DS !")
	uf_msgs(a_batch_name, "  " + sqls)
	destroy ds_temp
	return -1
end if

for i = 1 to ds_temp.rowCount() 
	s_col_pos.column_name = ds_temp.getItemString(i, 'column_name')
	s_col_pos.start_pos= ds_temp.getItemNumber(i, 'start_pos')
	s_col_pos.end_pos = ds_temp.getItemNumber(i, 'end_pos')
	s_file_col_order[i] = s_col_pos
next

ttl_col = upperbound(s_file_col_order)
if ttl_col < 1 then
	uf_msgs(a_batch_name, "Error:  No column setup for " + a_batch_name + "!")
	destroy ds_temp
	return -1 
end if

sqls = ""

if has_header_row then
	sqls = sqls + "SELECT ROW_ORDER, EXPORT_STRING FROM ( " + "~r~n"
	sqls = sqls + "select 1 row_order, " + "~r~n"
	for i = 1 to ttl_col
		if is_fixed_width then
			lng_width = (s_file_col_order[i].end_pos - s_file_col_order[i].start_pos) + 1 
			
			if i = ttl_col then
				sqls = sqls + "rpad('" + s_file_col_order[i].column_name + "', "+string(lng_width)+", ' ') "
			else
				sqls = sqls + "rpad('" + s_file_col_order[i].column_name + "', "+string(lng_width)+", ' ')|| "
			end if
		else
			if i = ttl_col then
				sqls = sqls + s_file_col_order[i].column_name+"'"
			elseif i = 1 then
				sqls = sqls + "'" + s_file_col_order[i].column_name + "'" + str_delimiter + "'"
			else
				sqls = sqls + s_file_col_order[i].column_name + "'" + str_delimiter + "'"
			end if
		end if
	next
	sqls = sqls + " as export_string from dual " + "~r~n" 
	sqls = sqls + " UNION ALL " + "~r~n" 
end if

sqls = sqls + "select 1 + rownum, " + "~r~n"
for i = 1 to ttl_col
	if is_fixed_width then		
		//// From Joseph: Substr to width
		lng_width = (s_file_col_order[i].end_pos - s_file_col_order[i].start_pos) + 1 
		select data_type 
		into :data_type
		from all_tab_columns 
		where TABLE_NAME = upper(:str_table_name)
		and column_name = upper(:s_file_col_order[i].column_name);
	
		if i = ttl_col then
			if upper(trim(data_type)) = 'NUMBER' or upper(trim(data_type)) = 'FLOAT' then
				sqls = sqls + 'lpad(substr("' + upper(s_file_col_order[i].column_name) + '",1,' + string(lng_width) + '), ' + string(lng_width)+", '0') "
			else				
				sqls = sqls + 'rpad(substr(to_char("' + upper(s_file_col_order[i].column_name) + '"),1,' + string(lng_width) + '), ' + string(lng_width)+", ' ') "
			end if
		else
			if upper(trim(data_type)) = 'NUMBER' or upper(trim(data_type)) = 'FLOAT' then
				sqls = sqls + 'lpad("' + upper(s_file_col_order[i].column_name) + '",1,' + string(lng_width) + '), ' + string(lng_width)+", '0')|| "
			else		
				sqls = sqls + 'rpad("' + upper(s_file_col_order[i].column_name) + '",1,' + string(lng_width) + '), ' + string(lng_width)+", ' ')|| "
			end if
		end if
	else
		if i = ttl_col then
			sqls = sqls + '"' + upper(s_file_col_order[i].column_name) + '"'
		else
			sqls = sqls + '"' + upper(s_file_col_order[i].column_name) + '"' + "'" + str_delimiter + "'"
		end if
	end if
next

if uses_table then
	sqls = sqls + ' as export_string from "'+str_table_name+'" ' + "~r~n"
	sqls = sqls + str_sql
else
	sqls = sqls + ' as export_string from ('+ str_sql +') ' + "~r~n"
end if

if has_header_row then
	sqls = sqls + ") ORDER BY ROW_ORDER"
end if

sqls = ""

if has_header_row then
	sqls = sqls + "SELECT ROW_ORDER, EXPORT_STRING FROM ( " + "~r~n"
	sqls = sqls + "select 1 row_order, " + "~r~n"
	for i = 1 to ttl_col
		if is_fixed_width then
			lng_width = (s_file_col_order[i].end_pos - s_file_col_order[i].start_pos) + 1 
			if i = ttl_col then
				sqls = sqls + "rpad('" + s_file_col_order[i].column_name + "', "+string(lng_width)+", ' ') "
			else
				sqls = sqls + "rpad('" + s_file_col_order[i].column_name + "', "+string(lng_width)+", ' ')|| "
			end if
		else
			if i = ttl_col then
				sqls = sqls + s_file_col_order[i].column_name+"'"
			elseif i = 1 then
				sqls = sqls + "'" + s_file_col_order[i].column_name + str_delimiter
			else
				sqls = sqls + s_file_col_order[i].column_name + str_delimiter
			end if
		end if
	next
	sqls = sqls + " as export_string from dual " + "~r~n" 
	sqls = sqls + " UNION ALL " + "~r~n" 
end if

sqls = sqls + "select 1 + rownum, " + "~r~n"
for i = 1 to ttl_col
	if is_fixed_width then
		lng_width = (s_file_col_order[i].end_pos - s_file_col_order[i].start_pos) + 1 
		if i = ttl_col then
			sqls = sqls + 'rpad(nvl(to_char("' + upper(s_file_col_order[i].column_name) + '"),'+"' '), " + string(lng_width)+", ' ') "
		else
			sqls = sqls + 'rpad(nvl(to_char("' + upper(s_file_col_order[i].column_name) + '"),' +"' '), " + string(lng_width)+", ' ')|| "
		end if
	else
		if i = ttl_col then
			sqls = sqls + '"' + upper(s_file_col_order[i].column_name) + '"'
		else
			sqls = sqls + '"' + upper(s_file_col_order[i].column_name) + '"||' + "'" + str_delimiter + "'||"
		end if
	end if
next

if uses_table then
	sqls = sqls + ' as export_string from "'+upper(str_table_name)+'" ' + "~r~n"
	sqls = sqls + str_sql
else
	sqls = sqls + ' as export_string from ('+ str_sql +') ' + "~r~n"
end if

if has_header_row then
	sqls = sqls + ") ORDER BY ROW_ORDER"
end if

////Export The file
nvo_import_export_files uo_files
uo_files = create nvo_import_export_files
if not uo_files.uf_run_export(str_file_path+str_file_name, sqls) then
	destroy uo_files
	destroy ds_temp
	return -1
end if

destroy uo_files
destroy ds_temp
return 1
end function

public function boolean of_check_table_exists (string a_table_name);string sqls

sqls = "SELECT COUNT(1) FROM "+a_table_name+" where 1 = -1"
execute immediate :sqls;
if sqlca.sqlcode < 0 then
	return false
end if
return true
end function

public function boolean of_import_xml_file (string a_filename, string a_filepath, string a_table, string a_tablecolumn[], string a_xmltag[], string a_arcfilename, string a_arcfilepath, string a_transtag);longlong 					li_count, li_index, rtn
string					str_rtn
string					ls_attribs_name[], ls_attribs_value[], empty_string_array[]
Exception			l_ex
s_xml_element		s_name[], s_value[]
pbdom_builder		builder       
pbdom_document	document  
pbdom_element	root
pbdom_element	dom_childelements[] 
pbdom_element	dom_childelements_child[] 

uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ': Read Transactions from documents')

try
	builder = CREATE PBDOM_BUILDER  
	document = create pbdom_document
	root = document.GetRootElement()
	uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ': Read Transactions from documents')
	document = builder.BuildFromFile ( a_filePath+a_fileName)
	uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ': Read '+a_transTag+' from documents')	
	document.GetElementsByTagName ( a_transTag, dom_childelements[] ) 
	
	li_count = UpperBound ( dom_childelements[] ) 
	uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ': Processing '+string(li_count)+' Transactions')
	for li_index = 1 to li_count
		dom_childelements[li_index].GetChildElements(dom_childelements_child[])
		if upperbound(dom_childelements_child[]) > 0 then
			of_xml_get_elements_and_values(dom_childelements[li_index], ls_attribs_name[], ls_attribs_value[])
		end if
		of_xml_populate_structure(li_index, ls_attribs_name,ls_attribs_value,s_name,s_value)
		ls_attribs_name[] = empty_string_array[]
		ls_attribs_value[] = empty_string_array[]
	next

	uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ': Inserting into Staging Table')
	if of_xml_insert_records(a_table, a_tableColumn,  a_xmlTag, s_name, s_value, str_rtn) then
		commit;
	else
		l_ex = create Exception
		l_ex.SetMessage(str_rtn)
		throw l_ex		
	end if
	
	rtn = f_file_rename(a_filepath + a_filename,a_arcfilepath + a_arcfilename)
	if rtn <> 1 then
		//// The file is loaded, display warning and move on.
		uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ": WARNING ARCHIVING FILE FAILED! PLEASE MOVE FILE MANUALLY!!!")
	else
		uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ": File moved to '"+a_arcfilepath + a_arcfilename+"'")
	end if
catch (Exception e)	
	uf_msgs("OF_IMPORT_XML_FILE", a_fileName + ': ' + e.getMessage())
	return false
finally
	destroy builder	
	destroy document
end try




return true
end function

public function boolean of_xml_populate_structure (long a_index, string a_name[], string a_value[], ref s_xml_element a_s_name[], ref s_xml_element a_s_value[]);longlong i

Try
	for i = 1 to 100
		if i <= upperbound(a_name) then
			Choose case i
				case 1
					a_s_value[a_index].value_1= a_value[i]
					a_s_name[a_index].value_1=a_name[i]
				case 2
					a_s_value[a_index].value_2= a_value[i]
					a_s_name[a_index].value_2=a_name[i]
				case 3
					a_s_value[a_index].value_3= a_value[i]
					a_s_name[a_index].value_3=a_name[i]
				case 4
					a_s_value[a_index].value_4= a_value[i]
					a_s_name[a_index].value_4=a_name[i]
				case 5
					a_s_value[a_index].value_5= a_value[i]
					a_s_name[a_index].value_5=a_name[i]
				case 6
					a_s_value[a_index].value_6= a_value[i]
					a_s_name[a_index].value_6=a_name[i]
				case 7
					a_s_value[a_index].value_7= a_value[i]
					a_s_name[a_index].value_7=a_name[i]
				case 8
					a_s_value[a_index].value_8= a_value[i]
					a_s_name[a_index].value_8=a_name[i]
				case 9
					a_s_value[a_index].value_9= a_value[i]
					a_s_name[a_index].value_9=a_name[i]
				case 10
					a_s_value[a_index].value_10= a_value[i]
					a_s_name[a_index].value_10=a_name[i]
				case 11
					a_s_value[a_index].value_11= a_value[i]
					a_s_name[a_index].value_11=a_name[i]
				case 12
					a_s_value[a_index].value_12= a_value[i]
					a_s_name[a_index].value_12=a_name[i]
				case 13
					a_s_value[a_index].value_13= a_value[i]
					a_s_name[a_index].value_13=a_name[i]
				case 14
					a_s_value[a_index].value_14= a_value[i]
					a_s_name[a_index].value_14=a_name[i]
				case 15
					a_s_value[a_index].value_15= a_value[i]
					a_s_name[a_index].value_15=a_name[i]
				case 16
					a_s_value[a_index].value_16= a_value[i]
					a_s_name[a_index].value_16=a_name[i]
				case 17
					a_s_value[a_index].value_17= a_value[i]
					a_s_name[a_index].value_17=a_name[i]
				case 18
					a_s_value[a_index].value_18= a_value[i]
					a_s_name[a_index].value_18=a_name[i]
				case 19
					a_s_value[a_index].value_19= a_value[i]
					a_s_name[a_index].value_19=a_name[i]
				case 20
					a_s_value[a_index].value_20= a_value[i]
					a_s_name[a_index].value_20=a_name[i]
				case 21
					a_s_value[a_index].value_21= a_value[i]
					a_s_name[a_index].value_21=a_name[i]
				case 22
					a_s_value[a_index].value_22= a_value[i]
					a_s_name[a_index].value_22=a_name[i]
				case 23
					a_s_value[a_index].value_23= a_value[i]
					a_s_name[a_index].value_23=a_name[i]
				case 24
					a_s_value[a_index].value_24= a_value[i]
					a_s_name[a_index].value_24=a_name[i]
				case 25
					a_s_value[a_index].value_25= a_value[i]
					a_s_name[a_index].value_25=a_name[i]
				case 26
					a_s_value[a_index].value_26= a_value[i]
					a_s_name[a_index].value_26=a_name[i]
				case 27
					a_s_value[a_index].value_27= a_value[i]
					a_s_name[a_index].value_27=a_name[i]
				case 28
					a_s_value[a_index].value_28= a_value[i]
					a_s_name[a_index].value_28=a_name[i]
				case 29
					a_s_value[a_index].value_29= a_value[i]
					a_s_name[a_index].value_29=a_name[i]
				case 30
					a_s_value[a_index].value_30= a_value[i]
					a_s_name[a_index].value_30=a_name[i]
				case 31
					a_s_value[a_index].value_31= a_value[i]
					a_s_name[a_index].value_31=a_name[i]
				case 32
					a_s_value[a_index].value_32= a_value[i]
					a_s_name[a_index].value_32=a_name[i]
				case 33
					a_s_value[a_index].value_33= a_value[i]
					a_s_name[a_index].value_33=a_name[i]
				case 34
					a_s_value[a_index].value_34= a_value[i]
					a_s_name[a_index].value_34=a_name[i]
				case 35
					a_s_value[a_index].value_35= a_value[i]
					a_s_name[a_index].value_35=a_name[i]
				case 36
					a_s_value[a_index].value_36= a_value[i]
					a_s_name[a_index].value_36=a_name[i]
				case 37
					a_s_value[a_index].value_37= a_value[i]
					a_s_name[a_index].value_37=a_name[i]
				case 38
					a_s_value[a_index].value_38= a_value[i]
					a_s_name[a_index].value_38=a_name[i]
				case 39
					a_s_value[a_index].value_39= a_value[i]
					a_s_name[a_index].value_39=a_name[i]
				case 40
					a_s_value[a_index].value_40= a_value[i]
					a_s_name[a_index].value_40=a_name[i]
				case 41
					a_s_value[a_index].value_41= a_value[i]
					a_s_name[a_index].value_41=a_name[i]
				case 42
					a_s_value[a_index].value_42= a_value[i]
					a_s_name[a_index].value_42=a_name[i]
				case 43
					a_s_value[a_index].value_43= a_value[i]
					a_s_name[a_index].value_43=a_name[i]
				case 44
					a_s_value[a_index].value_44= a_value[i]
					a_s_name[a_index].value_44=a_name[i]
				case 45
					a_s_value[a_index].value_45= a_value[i]
					a_s_name[a_index].value_45=a_name[i]
				case 46
					a_s_value[a_index].value_46= a_value[i]
					a_s_name[a_index].value_46=a_name[i]
				case 47
					a_s_value[a_index].value_47= a_value[i]
					a_s_name[a_index].value_47=a_name[i]
				case 48
					a_s_value[a_index].value_48= a_value[i]
					a_s_name[a_index].value_48=a_name[i]
				case 49
					a_s_value[a_index].value_49= a_value[i]
					a_s_name[a_index].value_49=a_name[i]
				case 50
					a_s_value[a_index].value_50= a_value[i]
					a_s_name[a_index].value_50=a_name[i]
				case 51
					a_s_value[a_index].value_51= a_value[i]
					a_s_name[a_index].value_51=a_name[i]
				case 52
					a_s_value[a_index].value_52= a_value[i]
					a_s_name[a_index].value_52=a_name[i]
				case 53
					a_s_value[a_index].value_53= a_value[i]
					a_s_name[a_index].value_53=a_name[i]
				case 54
					a_s_value[a_index].value_54= a_value[i]
					a_s_name[a_index].value_54=a_name[i]
				case 55
					a_s_value[a_index].value_55= a_value[i]
					a_s_name[a_index].value_55=a_name[i]
				case 56
					a_s_value[a_index].value_56= a_value[i]
					a_s_name[a_index].value_56=a_name[i]
				case 57
					a_s_value[a_index].value_57= a_value[i]
					a_s_name[a_index].value_57=a_name[i]
				case 58
					a_s_value[a_index].value_58= a_value[i]
					a_s_name[a_index].value_58=a_name[i]
				case 59
					a_s_value[a_index].value_59= a_value[i]
					a_s_name[a_index].value_59=a_name[i]
				case 60
					a_s_value[a_index].value_60= a_value[i]
					a_s_name[a_index].value_60=a_name[i]
				case 61
					a_s_value[a_index].value_61= a_value[i]
					a_s_name[a_index].value_61=a_name[i]
				case 62
					a_s_value[a_index].value_62= a_value[i]
					a_s_name[a_index].value_62=a_name[i]
				case 63
					a_s_value[a_index].value_63= a_value[i]
					a_s_name[a_index].value_63=a_name[i]
				case 64
					a_s_value[a_index].value_64= a_value[i]
					a_s_name[a_index].value_64=a_name[i]
				case 65
					a_s_value[a_index].value_65= a_value[i]
					a_s_name[a_index].value_65=a_name[i]
				case 66
					a_s_value[a_index].value_66= a_value[i]
					a_s_name[a_index].value_66=a_name[i]
				case 67
					a_s_value[a_index].value_67= a_value[i]
					a_s_name[a_index].value_67=a_name[i]
				case 68
					a_s_value[a_index].value_68= a_value[i]
					a_s_name[a_index].value_68=a_name[i]
				case 69
					a_s_value[a_index].value_69= a_value[i]
					a_s_name[a_index].value_69=a_name[i]
				case 70
					a_s_value[a_index].value_70= a_value[i]
					a_s_name[a_index].value_70=a_name[i]
				case 71
					a_s_value[a_index].value_71= a_value[i]
					a_s_name[a_index].value_71=a_name[i]
				case 72
					a_s_value[a_index].value_72= a_value[i]
					a_s_name[a_index].value_72=a_name[i]
				case 73
					a_s_value[a_index].value_73= a_value[i]
					a_s_name[a_index].value_73=a_name[i]
				case 74
					a_s_value[a_index].value_74= a_value[i]
					a_s_name[a_index].value_74=a_name[i]
				case 75
					a_s_value[a_index].value_75= a_value[i]
					a_s_name[a_index].value_75=a_name[i]
				case 76
					a_s_value[a_index].value_76= a_value[i]
					a_s_name[a_index].value_76=a_name[i]
				case 77
					a_s_value[a_index].value_77= a_value[i]
					a_s_name[a_index].value_77=a_name[i]
				case 78
					a_s_value[a_index].value_78= a_value[i]
					a_s_name[a_index].value_78=a_name[i]
				case 79
					a_s_value[a_index].value_79= a_value[i]
					a_s_name[a_index].value_79=a_name[i]
				case 80
					a_s_value[a_index].value_80= a_value[i]
					a_s_name[a_index].value_80=a_name[i]
				case 81
					a_s_value[a_index].value_81= a_value[i]
					a_s_name[a_index].value_81=a_name[i]
				case 82
					a_s_value[a_index].value_82= a_value[i]
					a_s_name[a_index].value_82=a_name[i]
				case 83
					a_s_value[a_index].value_83= a_value[i]
					a_s_name[a_index].value_83=a_name[i]
				case 84
					a_s_value[a_index].value_84= a_value[i]
					a_s_name[a_index].value_84=a_name[i]
				case 85
					a_s_value[a_index].value_85= a_value[i]
					a_s_name[a_index].value_85=a_name[i]
				case 86
					a_s_value[a_index].value_86= a_value[i]
					a_s_name[a_index].value_86=a_name[i]
				case 87
					a_s_value[a_index].value_87= a_value[i]
					a_s_name[a_index].value_87=a_name[i]
				case 88
					a_s_value[a_index].value_88= a_value[i]
					a_s_name[a_index].value_88=a_name[i]
				case 89
					a_s_value[a_index].value_89= a_value[i]
					a_s_name[a_index].value_89=a_name[i]
				case 90
					a_s_value[a_index].value_90= a_value[i]
					a_s_name[a_index].value_90=a_name[i]
				case 91
					a_s_value[a_index].value_91= a_value[i]
					a_s_name[a_index].value_91=a_name[i]
				case 92
					a_s_value[a_index].value_92= a_value[i]
					a_s_name[a_index].value_92=a_name[i]
				case 93
					a_s_value[a_index].value_93= a_value[i]
					a_s_name[a_index].value_93=a_name[i]
				case 94
					a_s_value[a_index].value_94= a_value[i]
					a_s_name[a_index].value_94=a_name[i]
				case 95
					a_s_value[a_index].value_95= a_value[i]
					a_s_name[a_index].value_95=a_name[i]
				case 96
					a_s_value[a_index].value_96= a_value[i]
					a_s_name[a_index].value_96=a_name[i]
				case 97
					a_s_value[a_index].value_97= a_value[i]
					a_s_name[a_index].value_97=a_name[i]
				case 98
					a_s_value[a_index].value_98= a_value[i]
					a_s_name[a_index].value_98=a_name[i]
				case 99
					a_s_value[a_index].value_99= a_value[i]
					a_s_name[a_index].value_99=a_name[i]
				case 100
					a_s_value[a_index].value_100= a_value[i]
					a_s_name[a_index].value_100=a_name[i]				
			end choose
		end if			
	next
catch (exception e)
	uf_msgs("","OF_XML_POPULATE_STRUCTURE: " + e.getMessage())
	uf_msgs("",string(a_index))
	return false
end try
return true
end function

public function boolean of_xml_insert_records (string a_table_name, string a_field_name[], string a_xml_tag_name[], s_xml_element a_s_name[], s_xml_element a_s_value[], ref string a_message);longlong counter, i, m
string sqls, sqls_values, str_rtn


for i = 1 to upperbound(a_s_name) 
	
	sqls = "insert into " + a_table_name + "("
	sqls_values = ""
	for m = 1 to upperbound(a_field_name)
		if m = upperbound(a_field_name) then
			sqls = sqls + a_field_name[m] + ") values ("
		else
			sqls = sqls + a_field_name[m] + ", "
		end if
		
		str_rtn = of_xml_get_elements_value(a_xml_tag_name[m], a_s_name[i], a_s_value[i])
		if isnull(str_rtn) then
			a_message = "ERROR - Cannot find field name in XML that matches "+upper(a_field_name[m])	
			return false
		else
			if m = upperbound(a_field_name) then
				sqls_values = sqls_values + "'" + str_rtn + "')"
			else
				sqls_values = sqls_values + "'" + str_rtn + "', "
			end if
			
		end if
		
	next
	
	sqls = sqls + sqls_values
			
	execute immediate :sqls;
	if sqlca.sqlcode <> 0 then
		a_message = "ERROR - "+ sqlca.SQLErrText 
		return false
	end if
next

return true
end function

public function string of_xml_get_elements_value (string a_match_string, s_xml_element a_s_name, s_xml_element a_s_value);string l_value
setNull(l_value)

if upper(trim(a_s_name.value_1)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_1
end if
if upper(trim(a_s_name.value_2)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_2
end if
if upper(trim(a_s_name.value_3)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_3
end if
if upper(trim(a_s_name.value_4)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_4
end if
if upper(trim(a_s_name.value_5)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_5
end if
if upper(trim(a_s_name.value_6)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_6
end if
if upper(trim(a_s_name.value_7)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_7
end if
if upper(trim(a_s_name.value_8)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_8
end if
if upper(trim(a_s_name.value_9)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_9
end if
if upper(trim(a_s_name.value_10)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_10
end if
if upper(trim(a_s_name.value_11)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_11
end if
if upper(trim(a_s_name.value_12)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_12
end if
if upper(trim(a_s_name.value_13)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_13
end if
if upper(trim(a_s_name.value_14)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_14
end if
if upper(trim(a_s_name.value_15)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_15
end if
if upper(trim(a_s_name.value_16)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_16
end if
if upper(trim(a_s_name.value_17)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_17
end if
if upper(trim(a_s_name.value_18)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_18
end if
if upper(trim(a_s_name.value_19)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_19
end if
if upper(trim(a_s_name.value_20)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_20
end if
if upper(trim(a_s_name.value_21)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_21
end if
if upper(trim(a_s_name.value_22)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_22
end if
if upper(trim(a_s_name.value_23)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_23
end if
if upper(trim(a_s_name.value_24)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_24
end if
if upper(trim(a_s_name.value_25)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_25
end if
if upper(trim(a_s_name.value_26)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_26
end if
if upper(trim(a_s_name.value_27)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_27
end if
if upper(trim(a_s_name.value_28)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_28
end if
if upper(trim(a_s_name.value_29)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_29
end if
if upper(trim(a_s_name.value_30)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_30
end if
if upper(trim(a_s_name.value_31)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_31
end if
if upper(trim(a_s_name.value_32)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_32
end if
if upper(trim(a_s_name.value_33)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_33
end if
if upper(trim(a_s_name.value_34)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_34
end if
if upper(trim(a_s_name.value_35)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_35
end if
if upper(trim(a_s_name.value_36)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_36
end if
if upper(trim(a_s_name.value_37)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_37
end if
if upper(trim(a_s_name.value_38)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_38
end if
if upper(trim(a_s_name.value_39)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_39
end if
if upper(trim(a_s_name.value_40)) = upper(trim(a_match_string)) then
	l_value = a_s_value.value_40
end if
if upper(trim(a_s_name.value_41)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_41
end if
if upper(trim(a_s_name.value_42)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_42
end if
if upper(trim(a_s_name.value_43)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_43
end if
if upper(trim(a_s_name.value_44)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_44
end if
if upper(trim(a_s_name.value_45)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_45
end if
if upper(trim(a_s_name.value_46)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_46
end if
if upper(trim(a_s_name.value_47)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_47
end if
if upper(trim(a_s_name.value_48)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_48
end if
if upper(trim(a_s_name.value_49)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_49
end if
if upper(trim(a_s_name.value_50)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_50
end if
if upper(trim(a_s_name.value_51)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_51
end if
if upper(trim(a_s_name.value_52)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_52
end if
if upper(trim(a_s_name.value_53)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_53
end if
if upper(trim(a_s_name.value_54)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_54
end if
if upper(trim(a_s_name.value_55)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_55
end if
if upper(trim(a_s_name.value_56)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_56
end if
if upper(trim(a_s_name.value_57)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_57
end if
if upper(trim(a_s_name.value_58)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_58
end if
if upper(trim(a_s_name.value_59)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_59
end if
if upper(trim(a_s_name.value_60)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_60
end if
if upper(trim(a_s_name.value_61)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_61
end if
if upper(trim(a_s_name.value_62)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_62
end if
if upper(trim(a_s_name.value_63)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_63
end if
if upper(trim(a_s_name.value_64)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_64
end if
if upper(trim(a_s_name.value_65)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_65
end if
if upper(trim(a_s_name.value_66)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_66
end if
if upper(trim(a_s_name.value_67)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_67
end if
if upper(trim(a_s_name.value_68)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_68
end if
if upper(trim(a_s_name.value_69)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_69
end if
if upper(trim(a_s_name.value_70)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_70
end if
if upper(trim(a_s_name.value_71)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_71
end if
if upper(trim(a_s_name.value_72)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_72
end if
if upper(trim(a_s_name.value_73)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_73
end if
if upper(trim(a_s_name.value_74)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_74
end if
if upper(trim(a_s_name.value_75)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_75
end if
if upper(trim(a_s_name.value_76)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_76
end if
if upper(trim(a_s_name.value_77)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_77
end if
if upper(trim(a_s_name.value_78)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_78
end if
if upper(trim(a_s_name.value_79)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_79
end if
if upper(trim(a_s_name.value_80)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_80
end if
if upper(trim(a_s_name.value_81)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_81
end if
if upper(trim(a_s_name.value_82)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_82
end if
if upper(trim(a_s_name.value_83)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_83
end if
if upper(trim(a_s_name.value_84)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_84
end if
if upper(trim(a_s_name.value_85)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_85
end if
if upper(trim(a_s_name.value_86)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_86
end if
if upper(trim(a_s_name.value_87)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_87
end if
if upper(trim(a_s_name.value_88)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_88
end if
if upper(trim(a_s_name.value_89)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_89
end if
if upper(trim(a_s_name.value_90)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_90
end if
if upper(trim(a_s_name.value_91)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_91
end if
if upper(trim(a_s_name.value_92)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_92
end if
if upper(trim(a_s_name.value_93)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_93
end if
if upper(trim(a_s_name.value_94)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_94
end if
if upper(trim(a_s_name.value_95)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_95
end if
if upper(trim(a_s_name.value_96)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_96
end if
if upper(trim(a_s_name.value_97)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_97
end if
if upper(trim(a_s_name.value_98)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_98
end if
if upper(trim(a_s_name.value_99)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_99
end if
if upper(trim(a_s_name.value_100)) = upper(trim(a_match_string)) then
     l_value = a_s_value.value_100
end if

l_value = g_string_func.of_replace_string( l_value, "'", "''", "all")
return l_value
end function

public function long of_export_xml (string a_batch_name);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   uo_pp_int_imp_exp_files.of_export_xml
//
//   Purpose  :  Export XML files base on the setup in pp_int_xml_export and pp_int_xml_export_col
// 
//   Scope    :   global
//
//   Arguments:		(v) a_batch_name		:  String			: The batch name used in pp_int_file_import
//
//   Returns :   1 = success ; -1 = failure
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
string str_rtn
longlong i
Exception l_ex

string root_node_name, definition_root_name
string sqls, filePath, fileName, field_def_name[], field_def_type[]
uo_ds_top ds_temp
ds_temp = create uo_ds_top

uo_ds_top ds_col
ds_col = create uo_ds_top

PBDOM_Document l_dom_doc
PBDOM_ELEMENT node_root, node_definition, node_field_type, node_msg_data, node_template, node_detail

/*Stored configuration Variables*/
select file_path, nvl(file_name, :a_batch_name||'_{sysdate}_{batch_name}.xml') file_name, SQL, rootNode, DefinitionNode
into :filePath, :fileName, :sqls, :root_node_name, :definition_root_name
from pp_int_xml_export 
where batch_name = :a_batch_name;

str_rtn = f_create_dynamic_ds(ds_col,'grid','select column_name, column_type from pp_int_xml_export_col where batch_name = ~'' + a_batch_name + '~' order by column_order ',sqlca,true)
if str_rtn <> "OK" then
	uf_msgs(a_batch_name,"OF_EXPORT_XML: DS Create Fail")
	return -1
end if

if ds_col.rowCount() < 1 then
	uf_msgs(a_batch_name,"OF_EXPORT_XML: Missing Column Setup")
	return -1
end if

for i = 1 to ds_col.rowCount()
	field_def_name[i] = ds_col.getItemString(i,1)
	field_def_type[i] = ds_col.getItemString(i,2)
next

try
	
	filename = of_get_arc_file_name(a_batch_name, a_batch_name+'.xml', filename)
	
	str_rtn = f_create_dynamic_ds(ds_temp, 'grid', sqls, sqlca, true)
	if str_rtn <> "OK" then
		uf_msgs(a_batch_name,"OF_EXPORT_XML: Error: DS Create Fail")
		return -1
	end if
	
	/*create document*/	
	l_dom_doc = CREATE PBDOM_Document
	l_dom_doc.NewDocument(root_node_name)
	node_root = l_dom_doc.GetRootElement()
	
	/*create definition name*/
	node_field_type = create PBDOM_Element
	node_field_type.setName("FieldTypes")
	node_root.addContent(node_field_type)
	
	/*create msgData name*/
	node_msg_data = create PBDOM_Element
	node_msg_data.setName("MsgData")
	node_root.addContent(node_msg_data)
	
	/* Add Field Type definition type*/
	node_definition = create PBDOM_Element
	node_definition.setName(definition_root_name)
	node_field_type.addContent(node_definition)

	for i = 1 to upperbound(field_def_name)
		setNull(str_rtn)
		of_xml_add_node(node_definition,field_def_name[i],"Type",field_def_type[i], str_rtn)
	next

	if not of_xml_build_transaction_data(node_msg_data, definition_root_name, ds_temp,field_def_name,field_def_type ) then
		l_ex = create Exception
		l_ex.SetMessage("Build Transactions Failed")
		throw l_ex
	end if	
	
//	node_template = create  PBDOM_Element
//	node_template.setName("Transaction")
//	node_msg_data.addContent(node_template)
//
//	if not of_xml_build_transaction_data(node_template, definition_root_name, ds_temp,field_def_name,field_def_type ) then
//		l_ex = create Exception
//		l_ex.SetMessage("Build Transactions Failed")
//		throw l_ex
//	end if
	

	/* Save File */	
	l_dom_doc.SaveDocument(filepath + fileName)

	
catch (Exception E)	
	uf_msgs("Error",e.GetMessage())
finally
	destroy ds_temp
	destroy node_field_type
	destroy node_definition
	destroy l_dom_doc	
end try

return 1
end function

public function boolean of_xml_add_node (ref pbdom_element a_parent_node, string a_name, string a_attrib, string a_attrib_value, string a_value);pbdom_element l_node

try
	l_node = create pbdom_element
	l_node.setName(a_name)
	a_parent_node.addContent(l_node)
	if not isNull(a_attrib) and not isNull(a_attrib_value) then
		l_node.setAttribute(a_attrib,a_attrib_value)
	end if
	if not isNull(a_value) then
		l_node.addContent(a_value)
	end if
catch (Exception e)
	uf_msgs("","OF_ADD_NODE: " + e.getMessage())
	return false
finally
	destroy l_node
end try

return true
end function

public function long of_import_xml (string a_batch_name);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   uo_pp_int_imp_exp_files.of_import_xml
//
//   Purpose  :  Import XML files base on the setup in pp_int_xml_import and pp_int_xml_import_col
// 
//   Scope    :   global
//
//   Arguments:		(v) a_batch_name		:  String			: The batch name used in pp_int_file_import
//
//   Returns :   boolean
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
longlong 							rtn, i
string							sqls, str_rtn, tempFileName
string 						tableName, filePath, fileName, ArcFilePath, ArcFileName, TransactionTag
string 						tableColumn[], XMLTag[], fileNameFound[], fileNamePathFound[]
Exception					l_ex
uo_ds_top 					ds_temp 
nvo_import_export_files	uo_files

select count(1) into :rtn
from pp_int_xml_import 
where batch_name = :a_batch_name;
if rtn < 1 then
	uf_msgs(a_batch_name, "Error:  No batch setup found for " + a_batch_name + "!")
	return -1
end if


Try
	/*Create Objects*/
	ds_temp = create uo_ds_top
	uo_files = create nvo_import_export_files
	/* Get Batch Configuration */
	select table_name,  file_path, file_name, nvl(archive_file_path, file_path), nvl(archive_file_name, '{sysdate}_{file_name}.{ext}'), transaction_tag
	into :tableName, :filePath, :fileName, :ArcFilePath, :ArcFileName, :TransactionTag
	from PP_INT_XML_IMPORT
	where batch_name = :a_batch_name;
	
	sqls = + &
	"select column_name, xml_tag_name" + " ~r~n" + &
	"from pp_int_xml_import_col" + " ~r~n" + &
	"where batch_name = '"+a_batch_name+"'" + " ~r~n" + &
	"order by column_order"
	
	str_rtn = f_create_dynamic_ds(ds_temp, 'grid', sqls, sqlca,true)
	if str_rtn <> "OK" then
		l_ex = create Exception
		l_ex.SetMessage("Create DS for Columns Mapping Failed!")
		throw l_ex
	end if
	
	for i = 1 to ds_temp.rowCount()
		tableColumn[i] = ds_temp.getItemString(i,1)
		XMLTag[i] = ds_temp.getItemString(i,2)
	next
	
	/* Get the number of files to load */
	rtn = uo_files.uf_get_file_names(filePath, fileName, fileNameFound[], fileNamePathFound[])
	if rtn < 1 then 
		uf_msgs(a_batch_name, "No file found for " + fileName + " in " + filePath)
		destroy uo_files
		destroy ds_temp
		return 1//// Not an error
	end if		

	/* Process Each File*/
	for i = 1 to upperBound(fileNameFound)
		tempFileName = of_get_arc_file_name(a_batch_name, fileNameFound[i], arcFileName)
		if not of_import_xml_file(fileNameFound[i], filePath, tableName, tableColumn, XMLTag, tempFileName, ArcFilePath, TransactionTag) then
			l_ex = create Exception
			l_ex.SetMessage("Create DS for Columns Mapping Failed!")
			throw l_ex
		end if
	next
	
	
catch (Exception e)
	uf_msgs(a_batch_name,"Error: " + e.getMessage())
	return -1
finally
	destroy ds_temp
	destroy uo_files
end try

return 1
end function

public function boolean of_xml_get_elements_and_values (pbdom_element a_node, ref string a_element_name[], ref string a_element_value[]);pbdom_element	l_dom_childelements[], temp_chidElements[]
pbdom_element	temp_element
string 				str_rtn
longlong 					i, counter

a_node.GetChildElements(l_dom_childelements[])

for i = 1 to upperbound(l_dom_childElements)
	str_rtn = l_dom_childElements[i].GetName()
	temp_element = a_node.getchildelement( str_rtn)  
	counter = upperbound(a_element_value)
	a_element_name[counter + 1] = str_rtn
	a_element_value[counter + 1] = temp_element.getText()
	
	l_dom_childElements[i].GetChildElements(temp_chidElements)
	if upperBound(temp_chidElements) > 0 then
		of_xml_get_elements_and_values(l_dom_childElements[i], a_element_name[], a_element_value[])
	end if
next


return true
end function

public function boolean of_xml_build_transaction_data (ref pbdom_element a_parent_node, string a_def_node_name, datastore a_ds, string a_def_name[], string a_def_value[]);longlong i
boolean is_success
Exception l_ex
try
	for i = 1 to a_ds.rowCount()
		is_success = of_xml_add_transaction_data(a_parent_node, a_def_node_name, a_ds, i, a_def_name, a_def_value)
		if not is_success then
			l_ex = create Exception
			l_ex.SetMessage("Add Transaction to MsgData Failed!")
			throw l_ex			
		end if
	Next	
catch (Exception e)
	uf_msgs("Error", "of_build_transaction_data : " + e.getMessage())
	return false
Finally
	
End Try
return true
end function

public function boolean of_xml_add_transaction_data (ref pbdom_element a_parent_node, string a_def_node_name, datastore a_ds, long a_index, string a_def_name[], string a_def_type[]);longlong i
string str_rtn, strNull

Exception l_ex
pbdom_element l_trans_node, l_trans_wrappper

setNull(strNull)
try
	l_trans_wrappper= create pbdom_element
	l_trans_wrappper.setName('Transaction')
	
	l_trans_node = create pbdom_element
	l_trans_node.setName(a_def_node_name)
	
	for i = 1 to upperbound(a_def_name)
		choose case upper(trim(a_def_type[i]))
			case "CHAR"
				str_rtn = a_ds.getItemString(a_index,i)
			case "NUMBER"
				str_rtn = string(a_ds.getItemNumber(a_index,i))
			case "DATE"
				str_rtn = string(a_ds.getItemDateTime(a_index,i))
			case else
				l_ex = create Exception
				l_ex.SetMessage("Undefined Data Type ("+a_def_type[i]+")used in configuration!")
				throw l_ex
		end choose 
		if isNull(str_rtn) then
			str_rtn = ""
		end if 
		
		of_xml_add_node(l_trans_node, a_def_name[i],strNull,strNull,str_rtn)				
	next
	
	l_trans_wrappper.AddContent(l_trans_node)
	a_parent_node.AddContent(l_trans_wrappper)
	
catch (Exception e)
	uf_msgs("Error", "of_add_transaction_data : " + e.getMessage())
	return false
finally
	destroy l_trans_node
	destroy l_ex
end try

return true
end function

on uo_pp_int_imp_exp_files.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_pp_int_imp_exp_files.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

