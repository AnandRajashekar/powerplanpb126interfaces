HA$PBExportHeader$nvo_import_export_files.sru
forward
global type nvo_import_export_files from nonvisualobject
end type
end forward

global type nvo_import_export_files from nonvisualobject
end type
global nvo_import_export_files nvo_import_export_files

forward prototypes
public function long uf_get_file_names (string a_file_path, string a_file_search_string, ref string a_file_names[], ref string a_file_path_names[])
public function boolean uf_run_sqlldr_fixed_width (string a_file_path_name, string a_table_name, s_sqlldr_col_pos a_file_col_order[], long a_row_data_starts, boolean a_is_direct, boolean a_is_append)
public function boolean uf_run_export (string a_file_path_name, string a_sqls)
public function boolean uf_run_sqlldr_delimited (string a_file_path_name, string a_table_name, string a_delimeter, string a_file_col_order[], long a_row_data_starts, boolean a_is_direct, boolean a_is_append, long a_file_col_len[])
end prototypes

public function long uf_get_file_names (string a_file_path, string a_file_search_string, ref string a_file_names[], ref string a_file_path_names[]);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   nvo_import_export_files.uf_get_file_names
//
//   Purpose  :   Find file names in directory. Accept * as wild card for the search
//					Returns the number of file found and passes back the file names
//					 and file path with file name in array by reference
// 
//   Scope    :   global
//
//   Arguments:		(v) a_file_path				:  String			: The file path to search in
//                		(v) a_file_search_string	:  String			: The string to search for in a_file_path
//                		(R) a_file_names			:  String Array	: This function will populate this array with the file names
//                		(R) a_file_path_names	:  String Array	: This function will populate this array with the file path with file names
//
//   Returns :   number of files found. Returns -1 if error occurs
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
longlong i, num_items
string file_names[], str_empty_array[]

u_external_function_win32 nvo_files
nvo_files = create u_external_function_win32
/************************************/
//// Reset the arrays passed in
/************************************/
a_file_names = str_empty_array
a_file_path_names = str_empty_array

/************************************/
//// Search for files
/************************************/
num_items = nvo_files.uf_find_files(a_file_path + a_file_search_string, file_names)
if num_items < 0 then
	destroy nvo_files
	return -1
end if

/************************************/
//// Processes Returns into reference arrays
/************************************/
if upperbound(file_names) = 0 then
	destroy nvo_files
	return 0
end if
num_items = upperbound(file_names)
for i = 1 to num_items
	a_file_names[i] = file_names[i]
	a_file_path_names[i] = a_file_path + file_names[i]
next

destroy nvo_files
return num_items



end function

public function boolean uf_run_sqlldr_fixed_width (string a_file_path_name, string a_table_name, s_sqlldr_col_pos a_file_col_order[], long a_row_data_starts, boolean a_is_direct, boolean a_is_append);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   nvo_import_export_files.uf_run_sqlldr_fixed_width
//
//   Purpose  :   Run sql loader to import Fixed width file
// 
//   Scope    :   global
//
//   Arguments:		(v) a_file_path_name		:  String				: The path and file name to load
//                		(v) a_table_name			:  String				: The table to load the file to
//                		(v) a_file_col_order		:  structure Array	: Structure s_sqlldr_col_pos array. This contains the column name, start and end pos of the column
//                		(v) a_row_data_starts	:  long				: The first row of data (excluding header)
//                		(v) a_is_direct				:  Boolean			: True to load direct which is faster but does not allow rollbacks or concatenates
//                		(v) a_is_append			:  Boolean			: True to append to the table or false to delete all rows, DIRECT MUST BE FALSE TO APPEND
//
//   Returns :   Boolean - True for success else false
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
string path, cntrl_file_name, sqlldrstr, sqlldrstr_debug, line
string control_file[]
longlong rtn, i, k = 1
longlong size, sleep_time, h, counter, rows_to_skip, num_col
time start, finish
boolean rtn_b
/*************************************************************/
//// Format the file path with \ instead of /
//// Extract the file path
/*************************************************************/
a_file_path_name = f_replace_string(a_file_path_name, '/', '\', 'all')
path = mid(a_file_path_name,1, lastpos(a_file_path_name, '\'))
//// cntrl File Name
cntrl_file_name = a_table_name + f_replace_string(f_replace_string(f_replace_string(f_replace_string(string(now()),'/', '','all'),',','','all'),':','','all'),' ','','all')
rows_to_skip = a_row_data_starts - 1
if rows_to_skip < 0 then
	rows_to_skip = 0
end if

f_pp_msgs("File: " + a_file_path_name)
f_pp_msgs("Path: " + path)
f_pp_msgs("File Name: " + cntrl_file_name)


control_file[k] = "OPTIONS (SKIP="+string(rows_to_skip)+")"
k++
control_file[k] = "load data "
k++
control_file[k] = "infile '" + a_file_path_name + "'"
k++

if a_is_append then
	control_file[k] = 'APPEND'
	k++
end if

control_file[k] = "into table " +  a_table_name + "("

num_col = upperbound(a_file_col_order[])
for i = 1 to num_col
	k++
	if i = num_col then
		control_file[k] = a_file_col_order[i].column_name + " position ("  + string( a_file_col_order[i].start_pos) + " : " + string( a_file_col_order[i].end_pos) + ") Char) " 
	else
		if num_col = 1 then
			control_file[k] = a_file_col_order[i].column_name + " position ("  + string( a_file_col_order[i].start_pos) + " : " + string( a_file_col_order[i].end_pos) + ") Char) " 
		else
			control_file[k] = a_file_col_order[i].column_name + " position ("  + string( a_file_col_order[i].start_pos) + " : " + string( a_file_col_order[i].end_pos) + ") Char, " 
		end if
	end if
next


//JKH 20110921 It is necessary to delete any existing control file so that the old file does not interfere with the current process
if filedelete(path + cntrl_file_name + '.ctl') then
	f_pp_msgs('Old control file found.  Deleting old control file and proceeding with current control file write....')
end if

for i = 1 to upperbound(control_file)
	f_write_log(path + cntrl_file_name + '.ctl', control_file[i])
next

//get file size
size = filelength64(a_file_path_name)

//JAW PP-45168: add double-quotes on either end of the path allow spaces in the path entered
//run the sql loader
sqlldrstr = "sqlldr " + sqlca.logid + "/" + sqlca.logpass +"@" + sqlca.servername + " control='" +'"'+ path + cntrl_file_name + '.ctl"' +"' " + "log='" + '"' + path + cntrl_file_name + '.log"' + "' >> sqlldr.log"
sqlldrstr_debug = "sqlldr " + sqlca.logid + "/********@" + sqlca.servername + " control='" + '"' + path + cntrl_file_name + '.ctl"' +"' " + "log='" + '"' + path + cntrl_file_name + '.log"' + "' >> sqlldr.log"
//###CWB PC-5544
// test the path strings generated then delete these lines
//sqlldrstr = "sqlldr " + sqlca.logid + "/" + sqlca.logpass +"@" + sqlca.servername + " control='" + path + cntrl_file_name + ".ctl' log='" + path + cntrl_file_name + ".log' >> sqlldr.log"
//sqlldrstr_debug = "sqlldr " + sqlca.logid + "/********@" + sqlca.servername + " control='" + path + cntrl_file_name + ".ctl' log='" + path + cntrl_file_name + ".log' >> sqlldr.log"
f_pp_msgs(sqlldrstr_debug)
start = now()

rtn_b = false
f_pp_msgs("Bat File Location = " + path + cntrl_file_name + '.bat')
rtn_b = f_write_file(path + cntrl_file_name + '.bat',sqlldrstr,replace!)
f_pp_msgs("Write Variable = " + string(rtn_b))
rtn = run(path + cntrl_file_name+ '.bat')
f_pp_msgs("Run Variable = " + string(rtn))


if rtn < 0 then
	f_pp_msgs("Run on sqlldr failed.")
	return false
end if


///*Sleep 60 seconds per file.*/
sleep_time = 10
f_pp_msgs("Wait for file to load for " + string(sleep_time) + " seconds")
sleep(sleep_time)

if size < 900000 then size = 900000

do while true
	
	if fileexists(path + cntrl_file_name + ".log") then
		h = FileOpen(path + cntrl_file_name + ".log", LineMode!, Read!, Shared!)
		do while FileRead(h,line) > -1
			if Pos(line, 'MAXIMUM ERROR COUNT EXCEEDED') > 0 & 
				or Pos(line, 'SQL*Loader-') > 0 then
				f_pp_msgs('ERROR uf_run_sqlldr_fixed_width: Max errors exceeded for sql loader please check file ' + path + cntrl_file_name + ".log")
				FileClose(h)
				return false
			end if
			
			if Pos(line, '0 Rows not loaded due to data errors.') > 0 then
				FileClose(h)
				return true
			end if
		loop
		fileClose(h)
	
		//JKH 20120330 alter denominator from 300,000 to 3,000 in order to allow more time for SQL loader to process the incoming fil
		if secondsafter(start, now()) > size / 3000 then
			f_pp_msgs('ERROR uf_run_sqlldr_fixed_width: SQL Loader Timed out. please check file '+ path + cntrl_file_name + ".log")

			
			if sqlca.sqlcode < 0 then
				f_pp_msgs("SQL error in uf_run_sqlldr_fixed_width: " + sqlca.sqlerrtext)
				f_pp_msgs("Truncation of " + a_table_name + " failed!")
				return false
			end if
			
			return false
		end if
	else
		// File doesn't exist...
		counter++
		
		if counter >= 2 then
			f_pp_msgs('ERROR uf_run_sqlldr_fixed_width: SQL Loader failed to start.')
			f_pp_msgs("Command=" + sqlldrstr_debug)
			
			return false
		end if
	end if
		
	
	sleep(30)

loop


/*Deleting SQL Loader files*/
f_pp_msgs("Delete SQL Loader Files... " + string(now()))
rtn_b = true
f_pp_msgs("Delete " + path + cntrl_file_name + ".ctl")
rtn_b = filedelete(path + cntrl_file_name + ".ctl")

if not rtn_b then
	f_pp_msgs ("Failed to delete ctl file: " + path + cntrl_file_name + ".ctl")
end if

f_pp_msgs("Delete " + path + cntrl_file_name + ".log")
rtn_b = filedelete(path + cntrl_file_name + ".log")

if not rtn_b then
	f_pp_msgs ("Failed to delete log file: " + path + cntrl_file_name + ".log")
end if

f_pp_msgs("Delete " + path + cntrl_file_name + ".bat")
rtn_b = filedelete(path + cntrl_file_name + ".bat")

if not rtn_b then
	f_pp_msgs ("Failed to delete bat file: " + path + cntrl_file_name + ".bat")
end if

f_pp_msgs("Delete " + path + "sqlldr.log")
rtn_b = filedelete(path + "sqlldr.log")

if not rtn_b then
	f_pp_msgs ("Failed to delete sqlldr.log file: " + path + "sqlldr.log")
end if


return true
end function

public function boolean uf_run_export (string a_file_path_name, string a_sqls);longlong numrows, fn
string string_add

if fileExists(a_file_path_name) then
	f_pp_msgs("File " + a_file_path_name + " exists")
	f_pp_msgs("Existing file will be deleted ")
	if not fileDelete(a_file_path_name) then
		f_pp_msgs("Error Deleting File")
		return false
	end if
end if

DECLARE my_cursor DYNAMIC CURSOR FOR SQLSA;

PREPARE SQLSA FROM  :a_sqls;
OPEN DYNAMIC my_cursor ;

If SQLCA.SQLCODE <> 0 then 
	f_pp_msgs("Error Running Cursor " + sqlca.SQLerrText)
	f_pp_msgs("        " + a_sqls)
	return false
End If


/*****/

if fileExists(a_file_path_name) then
                f_pp_msgs("File " + a_file_path_name + " exists")
                f_pp_msgs("Existing file will be deleted ")
                if not fileDelete(a_file_path_name) then
                                f_pp_msgs("Error Deleting File")
                                return false
                end if
end if

If SQLCA.SQLCODE <> 0 then 
                f_pp_msgs("Error Running Cursor " + sqlca.SQLerrText)
                f_pp_msgs("        " + a_sqls)
                return false
End If

fn =fileopen(a_file_path_name,linemode!,write!,lockreadwrite!,append!)

if fn = -1 then
                f_pp_msgs("Error Opening file" + sqlca.SQLerrText)
                return false
end if

FETCH my_cursor INTO :numrows, :string_add ;

Do while SQLCA.SQLCODE = 0
                
                if SQLCA.SQLCODE = 0 then
                                if  filewriteex(fn, string_add)  = -1 then
                                                f_pp_msgs("ERROR: Insert Header to file failed writing to: " + a_file_path_name)
                                                CLOSE my_cursor ;
                                                fileclose(fn)
                                                return false
                                end if
                end if
                
                FETCH my_cursor INTO :numrows, :string_add ;

Loop 

fileclose(fn)

CLOSE my_cursor ;

return true
/******/

fn =fileopen(a_file_path_name,linemode!,write!,lockreadwrite!,append!)
if fn = -1 then
	f_pp_msgs("Error Opening file" + sqlca.SQLerrText)
	return false
end if


Do while SQLCA.SQLCODE <> 100
	
	FETCH my_cursor INTO :numrows, :string_add ;
	
	if  filewriteex(fn,string_add)  = -1 then
		f_pp_msgs("ERROR: Insert Header to file failed writing to: " + a_file_path_name)
		CLOSE my_cursor ;
		fileclose(fn)
		return false
	end if	
	
Loop
fileclose(fn)

CLOSE my_cursor ;

return true
end function

public function boolean uf_run_sqlldr_delimited (string a_file_path_name, string a_table_name, string a_delimeter, string a_file_col_order[], long a_row_data_starts, boolean a_is_direct, boolean a_is_append, long a_file_col_len[]);//*****************************************************************************************
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:   system
//
//   Function :   nvo_import_export_files.uf_run_sqlldr_delimited
//
//   Purpose  :   Run sql loader to import delimited file
// 
//   Scope    :   global
//
//   Arguments:		(v) a_file_path_name		:  String			: The path and file name to load
//                		(v) a_table_name			:  String			: The table to load the file to
//                		(v) a_delimeter				:  String 			: The delimeter
//                		(v) a_file_col_order		:  String Array	: The column order of the file
//                		(v) a_row_data_starts	:  long			: The first row of data (excluding header)
//                		(v) a_is_direct				:  Boolean		: True to load direct which is faster but does not allow rollbacks or concatenates
//                		(v) a_is_append			:  Boolean		: True to append to the table or false to delete all rows, DIRECT MUST BE FALSE TO APPEND
//BGT:06302015	(v) a_file_col_len			:  long			: The length of each column by column order as defined by the staging table
//
//   Returns :   Boolean - True for success else false
//
//	DATE				NAME			REVISION	CHANGES
//	--------------		--------		-----------	-----------------------------------------------
//	07/31/2013		PowerPlan	1.0			Initial version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED
//******************************************************************************************
string path, cntrl_file_name, sqlldrstr, sqlldrstr_debug, line
string control_file[]
longlong rtn, i, k = 1
longlong size, sleep_time, h, counter, rows_to_skip
time start, finish
boolean rtn_b
/*************************************************************/
//// Format the file path with \ instead of /
//// Extract the file path
/*************************************************************/
a_file_path_name = f_replace_string(a_file_path_name, '/', '\', 'all')
path = mid(a_file_path_name,1, lastpos(a_file_path_name, '\'))
//// cntrl File Name
cntrl_file_name = a_table_name + f_replace_string(f_replace_string(f_replace_string(f_replace_string(string(now()),'/', '','all'),',','','all'),':','','all'),' ','','all')
rows_to_skip = a_row_data_starts - 1
if rows_to_skip < 0 then
	rows_to_skip = 0
end if

f_pp_msgs("File: " + a_file_path_name)
f_pp_msgs("Path: " + path)
f_pp_msgs("File Name: " + cntrl_file_name)


control_file[k] = "OPTIONS (SKIP="+string(rows_to_skip)+")"
k++
control_file[k] = "load data "
k++
control_file[k] = "infile '" + a_file_path_name + "'"
k++

if a_is_append then
	control_file[k] = 'APPEND'
	k++
end if

control_file[k] = "into table " +  a_table_name
k++

control_file[k] = "fields terminated by '"+ a_delimeter +"' optionally enclosed by " + "'" + '"' + "'"
k++
control_file[k] = "TRAILING NULLCOLS"

for i = 1 to upperbound(a_file_col_order)
	k++
	if i = 1 then
		if upperbound(a_file_col_order) = 1 then 
//BGT:06302015:added a_file_col_len
			control_file[k] = "(" + a_file_col_order[i] + " char(" + string(a_file_col_len[i]) + ")" + ")"
		else
			control_file[k] = "(" + a_file_col_order[i] + " char(" + string(a_file_col_len[i]) + ")"  + ","
		end if		
	elseif i = upperbound(a_file_col_order) then
		control_file[k] = a_file_col_order[i] + " char(" + string(a_file_col_len[i]) + ")"  + ")"
	else
		control_file[k] = a_file_col_order[i] + " char(" + string(a_file_col_len[i]) + ")" + ","
//BGT:END
	end if
next

//JKH 20110921 It is necessary to delete any existing control file so that the old file does not interfere with the current process
if filedelete(path + cntrl_file_name + '.ctl') then
	f_pp_msgs('Old control file found.  Deleting old control file and proceeding with current control file write....')
end if

for i = 1 to upperbound(control_file)
	f_write_log(path + cntrl_file_name + '.ctl', control_file[i])
next

//get file size
size = filelength64(a_file_path_name)

//JAW PP-45168: add double-quotes on either end of the path allow spaces in the path entered
//run the sql loader
sqlldrstr = "sqlldr " + sqlca.logid + "/" + sqlca.logpass +"@" + sqlca.servername + " control='" +'"'+ path + cntrl_file_name + '.ctl"' +"' " + "log='" + '"' + path + cntrl_file_name + '.log"' + "' >> sqlldr.log"
sqlldrstr_debug = "sqlldr " + sqlca.logid + "/********@" + sqlca.servername + " control='" + '"' + path + cntrl_file_name + '.ctl"' +"' " + "log='" + '"' + path + cntrl_file_name + '.log"' + "' >> sqlldr.log"
//###CWB PC-5544
// test the path strings generated then delete these lines
//sqlldrstr = "sqlldr " + sqlca.logid + "/" + sqlca.logpass +"@" + sqlca.servername + " control='" + path + cntrl_file_name + ".ctl' log='" + path + cntrl_file_name + ".log' >> sqlldr.log"
//sqlldrstr_debug = "sqlldr " + sqlca.logid + "/********@" + sqlca.servername + " control='" + path + cntrl_file_name + ".ctl' log='" + path + cntrl_file_name + ".log' >> sqlldr.log"
f_pp_msgs(sqlldrstr_debug)
start = now()

rtn_b = false
f_pp_msgs("Bat File Location = " + path + cntrl_file_name + '.bat')
rtn_b = f_write_file(path + cntrl_file_name + '.bat',sqlldrstr,replace!)
f_pp_msgs("Write Variable = " + string(rtn_b))
rtn = run(path + cntrl_file_name+ '.bat')
f_pp_msgs("Run Variable = " + string(rtn))


if rtn < 0 then
	f_pp_msgs("Run on sqlldr failed.")
	return false
end if


///*Sleep 60 seconds per file.*/
sleep_time = 10
f_pp_msgs("Wait for file to load for " + string(sleep_time) + " seconds")
sleep(sleep_time)

if size < 900000 then size = 900000

do while true
	
	if fileexists(path + cntrl_file_name + ".log") then
		h = FileOpen(path + cntrl_file_name + ".log", LineMode!, Read!, Shared!)
		do while FileRead(h,line) > -1
			if Pos(line, 'MAXIMUM ERROR COUNT EXCEEDED') > 0 & 
				or Pos(line, 'SQL*Loader-') > 0 then
				f_pp_msgs('ERROR uf_run_sqlldr_fixed_width: Max errors exceeded for sql loader please check file ' + path + cntrl_file_name + ".log")
				FileClose(h)
				return false
			end if
			
			if Pos(line, '0 Rows not loaded due to data errors.') > 0 then
				FileClose(h)
				return true
			end if
		loop
		fileClose(h)
	
		//JKH 20120330 alter denominator from 300,000 to 3,000 in order to allow more time for SQL loader to process the incoming fil
		if secondsafter(start, now()) > size / 3000 then
			f_pp_msgs('ERROR uf_run_sqlldr_fixed_width: SQL Loader Timed out. please check file '+ path + cntrl_file_name + ".log")
			//JKH 20120330 In the case of a SQL loader timeout, truncate staging table to ensure the file is not processed in a partial manner downstream.
			sqlca.truncate_table(a_table_name)
			
			if sqlca.sqlcode < 0 then
				f_pp_msgs("SQL error in uf_run_sqlldr_fixed_width: " + sqlca.sqlerrtext)
				f_pp_msgs("Truncation of " + a_table_name + " failed!")
				return false
			end if
			
			return false
		end if
	else
		// File doesn't exist...
		counter++
		
		if counter >= 30 then
			f_pp_msgs('ERROR uf_run_sqlldr_fixed_width: SQL Loader failed to start.')
			f_pp_msgs("Command=" + sqlldrstr_debug)
			
			return false
		end if
	end if
		
	
	sleep(2)

loop


/*Deleting SQL Loader files*/
f_pp_msgs("Delete SQL Loader Files... " + string(now()))
rtn_b = true
f_pp_msgs("Delete " + path + cntrl_file_name + ".ctl")
rtn_b = filedelete(path + cntrl_file_name + ".ctl")

if not rtn_b then
	f_pp_msgs ("Failed to delete ctl file: " + path + cntrl_file_name + ".ctl")
end if

f_pp_msgs("Delete " + path + cntrl_file_name + ".log")
rtn_b = filedelete(path + cntrl_file_name + ".log")

if not rtn_b then
	f_pp_msgs ("Failed to delete log file: " + path + cntrl_file_name + ".log")
end if

f_pp_msgs("Delete " + path + cntrl_file_name + ".bat")
rtn_b = filedelete(path + cntrl_file_name + ".bat")

if not rtn_b then
	f_pp_msgs ("Failed to delete bat file: " + path + cntrl_file_name + ".bat")
end if

f_pp_msgs("Delete " + path + "sqlldr.log")
rtn_b = filedelete(path + "sqlldr.log")

if not rtn_b then
	f_pp_msgs ("Failed to delete sqlldr.log file: " + path + "sqlldr.log")
end if


return true
end function

on nvo_import_export_files.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_import_export_files.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

