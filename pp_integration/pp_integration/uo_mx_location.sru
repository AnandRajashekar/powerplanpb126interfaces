HA$PBExportHeader$uo_mx_location.sru
forward
global type uo_mx_location from nonvisualobject
end type
end forward

global type uo_mx_location from nonvisualobject
end type
global uo_mx_location uo_mx_location

type variables
string i_exe_name = 'mx_location.exe'
string	il_batchid

end variables

forward prototypes
public function integer uf_check_success ()
public function longlong uf_load_location (string a_batchid)
end prototypes

public function integer uf_check_success ();//*****************************************************************************************
//
//  Object      :  uo_client_interface
//  Function	 :  uf_check_success
//
//	Returns   :  1 if successful
// 					-1 otherwise
//
//  Notes       :  Error if sql statement failed, otherwise success.
//
//*****************************************************************************************


if sqlca.sqlcode < 0 then
                f_pp_msgs('')
                f_pp_msgs(": Error:" + sqlca.sqlerrtext)
                f_pp_msgs('')
                f_pp_msgs('~~~~~~~END : FAILURE ~~~~~~~~')
                rollback;
                return -1
else
                f_pp_msgs(": Success: " + string(sqlca.sqlnrows) + " rows affected.")
                return 1
end if
end function

public function longlong uf_load_location (string a_batchid);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_mx_location.uf_load_location
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//	03-29-2012		PowerPlan										Modified Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure, i, j, rtn
string col_sqls, col_list, sqls, rtns

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

//Retrieving batch id 
//select max(occurrence_id) into :v_batchid from pp_processes_occurrences where process_id = :g_process_id;
il_batchid = a_batchid


//if there are two records for the same location, only keep the record with the latest timestamp
f_pp_msgs(" ")
f_pp_msgs("Updating error_code for records that have more than one update:")

update mx_ppiface_locations s 
set /*batch_id = -1,*/upload_indicator = 'D', error_code = 'Not the latest record update in this batch.'
//where exists (select 0
//                    from mx_ppiface_locations t
//				  where t.location = s.location
//				  and s.time_stamp < t.time_stamp
//				  and s.batch_id = :il_batchid
//				  and s.batch_id = t.batch_id);
where s.batch_id = :il_batchid
and upload_indicator not in ('D')
and nvl(s.transid,0) < (
	select nvl(max(t.transid),0)
	from mx_ppiface_locations t
	where t.location = s.location
	and t.batch_id = :il_batchid
	);

if uf_check_success() < 0 then return rtn_failure 

// CDM - move this lower so that resetting the upload_indicator to 'N' does not get overwritten by updates to 'I' and 'D' below
////Error: where parent does not exist in mx_location
//f_pp_msgs(" ")
//f_pp_msgs("Updating error_code for records where parent does not exist in mx_location:")
//update mx_ppiface_locations  
//set /*batch_id = -2,*/upload_indicator = 'E', error_code = 'ERROR: Parent does not exist in mx_location or in this batch.'
//where "PARENT" not in  (select "LOCATION" from mx_location) AND  "PARENT" not in  (select "LOCATION" from mx_ppiface_locations where batch_id = :il_batchid)
//and batch_id = :il_batchid;
//
//if uf_check_success() < 0 then return rtn_failure

//Updating upload_indicator to 'U'
f_pp_msgs("  ")
f_pp_msgs("Updating upload_indicator to 'U'[rows to Update]:")
//Verify if record already exists in the pwrplant table, if so, process for UPDATE
update mx_ppiface_locations t 
set t.upload_indicator = 'U'
where exists (select 0
				  from mx_location w
				  where w.location = t.location)
and (t.upload_indicator not in ('D') or t.upload_indicator is null)
and t.batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure   

//Updating upload_indicator to 'I'
f_pp_msgs("  ")
f_pp_msgs("Updating upload_indicator to 'I'[new rows to Insert]:")
update mx_ppiface_locations t
set t.upload_indicator = 'I'
where (t.upload_indicator not in ('U','D') or t.upload_indicator is null)
and t.batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

//Error: where parent does not exist in mx_location
f_pp_msgs(" ")
f_pp_msgs("Updating error_code for records where parent does not exist in mx_location:")
update mx_ppiface_locations  
set /*batch_id = -2,*/upload_indicator = 'N', error_code = 'ERROR: Parent does not exist in mx_location or in this batch.'
where "PARENT" not in  (select "LOCATION" from mx_location) AND  "PARENT" not in  (select "LOCATION" from mx_ppiface_locations where batch_id = :il_batchid)
and batch_id = :il_batchid
and upload_indicator not in ('D');

if uf_check_success() < 0 then return rtn_failure

//Retrieving column names from table	
uo_ds_top ds_all_tab_col
ds_all_tab_col = create uo_ds_top
	
col_sqls = "select lower(column_name)||',' from all_tab_columns" + &
			  " where table_name = upper('mx_ppiface_locations') " +&
			  " and upper(column_name) not in ('USER_ID','UPLOAD_INDICATOR', 'BATCH_ID', 'ERROR_CODE','TIME_STAMP','PARENT') "
			  
rtns = f_create_dynamic_ds(ds_all_tab_col,"grid",col_sqls,sqlca,true)
		
		if rtns <> 'OK' then // ---
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: Failure running f_create_dynamic_ds: " + rtns + ds_all_tab_col.i_sqlca_sqlerrtext)  
			f_pp_msgs(" ")
			rollback;
			return rtn_failure
		end if	

col_list = ""
for j = 1 to ds_all_tab_col.rowcount()
	col_list += ds_all_tab_col.GetItemString(j,1)
next
col_list = left(col_list,len(col_list)-1)

f_pp_msgs(" ")
f_pp_msgs("Updating locations in mx_location table:")

sqls =  "update mx_location a set ("
sqls += col_list + ") = "
sqls += " (select " + col_list + " from mx_ppiface_locations b " 
sqls += " where b.upload_indicator = 'U' and b.batch_id = '" + il_batchid + "' "
sqls += " and a.location = b.location) " 
sqls += " where exists ( select 1 from mx_ppiface_locations b"
sqls += " where b.upload_indicator = 'U' and b.batch_id = '" + il_batchid + "' "
sqls += " and a.location = b.location) " 

execute immediate :sqls;

if uf_check_success() < 0 then return rtn_failure

f_pp_msgs(" ")
f_pp_msgs("Inserting new locations to mx_location table:")
//Perform insert
sqls =  "insert into mx_location  ("
sqls += col_list + ") "
sqls += "select " + col_list + " from mx_ppiface_locations " 
sqls += " where upload_indicator = 'I' and batch_id = '" + il_batchid + "' "

execute immediate :sqls;

if uf_check_success() < 0 then return rtn_failure


f_pp_msgs("  ")
f_pp_msgs("Inserting new locations to mx_loc_hierarchy:")
insert into mx_loc_hierarchy ("LOCATION", "PARENT")
select "LOCATION", "PARENT" from mx_ppiface_locations
where upload_indicator = 'I' and batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

f_pp_msgs("  ")
f_pp_msgs("Updating mx_loc_hierarchy:")
update mx_loc_hierarchy a
set ("LOCATION", "PARENT") = (
	select "LOCATION", "PARENT" from mx_ppiface_locations b
	where b.upload_indicator = 'U' and b.batch_id = :il_batchid
	and a.location = b.location)
where exists (
	select 1 from mx_ppiface_locations b
	where b.upload_indicator = 'U' and b.batch_id = :il_batchid
	and a.location = b.location);
	
if uf_check_success() < 0 then return rtn_failure

//Marking records as completed - Done - "D"
f_pp_msgs(" ")
f_pp_msgs("Marking records as processed:")

update mx_ppiface_locations
set upload_indicator = 'D'
where upload_indicator in ('I','U')
and batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

f_pp_msgs(" ")
f_pp_msgs("Archiving processed records:")

insert into mx_ppiface_locations_arc
(autowogen,billtoaddresscode,billtolaborcode,calnum,changeby,changedate,children,controlacc,curvaracc,description,description_longdescription,description_longdescription2,
disabled,externalrefid,failurecode,gisparam1,gisparam2,gisparam3,glaccount,groupname,haschildren,hasparent,hierarchypath,intlabrec,invcostadjacc,invoicevaracc,invowner,                    
isdefault,isrepairfacility,itemnum,itemsetid,location,locationsid,locpriority,oldcontrolacc,oldinvcostadjacc,oldshrinkageacc,orgid,ownersysid,parent,pluscduedate,pluscloop,                   
pluscpmextdate,purchvaracc,receiptvaracc,sendersysid,serviceaddresscode,shiftnum,shiptoaddresscode,shiptolaborcode,shrinkageacc,siteid,sourcesysid,status,statusdate,statusiface,systemid,                    
toolcontrolacc,type,useinpopr,warrantyexpdate,transid,transseq,upload_indicator,user_id,time_stamp,archive_time_stamp,error_code,batch_id)
select autowogen,billtoaddresscode,billtolaborcode,calnum,changeby,changedate,children,controlacc,curvaracc,description,description_longdescription,description_longdescription2,
disabled,externalrefid,failurecode,gisparam1,gisparam2,gisparam3,glaccount,groupname,haschildren,hasparent,hierarchypath,intlabrec,invcostadjacc,invoicevaracc,invowner,                    
isdefault,isrepairfacility,itemnum,itemsetid,location,locationsid,locpriority,oldcontrolacc,oldinvcostadjacc,oldshrinkageacc,orgid,ownersysid,parent,pluscduedate,pluscloop,                   
pluscpmextdate,purchvaracc,receiptvaracc,sendersysid,serviceaddresscode,shiftnum,shiptoaddresscode,shiptolaborcode,shrinkageacc,siteid,sourcesysid,status,statusdate,statusiface,systemid,                    
toolcontrolacc,type,useinpopr,warrantyexpdate,transid,transseq,upload_indicator,user_id,time_stamp,sysdate,error_code,batch_id
from mx_ppiface_locations
where upload_indicator = 'D'
and batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

delete from mx_ppiface_locations
where upload_indicator = 'D'
and batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

commit;
return rtn_success

end function

on uo_mx_location.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mx_location.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

