HA$PBExportHeader$uo_pp_translate.sru
forward
global type uo_pp_translate from nonvisualobject
end type
end forward

global type uo_pp_translate from nonvisualobject
end type
global uo_pp_translate uo_pp_translate

type variables
string   i_company[]
longlong 		i_start_record, i_max_month, i_open_month, i_num_comps, i_min_month
longlong	i_mn_to_process, i_min_month_number, i_max_month_number
string g_debug
longlong g_min_charge_id


// pp translate original

string i_to_table, i_from_table, i_status_field

LongLong i_pp_translate_id
end variables

forward prototypes
public function longlong uf_read ()
public function integer uf_validate ()
public function longlong uf_load_destination ()
public function integer uf_preprocess ()
public subroutine uf_set_pp_translate_id (longlong a_pp_translate_id)
public function integer uf_process (longlong a_pp_translate_id)
end prototypes

public function longlong uf_read ();//********************************************************************************************************************
//
//  User Object Function  :  uf_read
//
//  Description  :  The "driving" function of the charge interface.  Some code exists in here and some other
//                  user object functions are called to perform certain processing.
//
//********************************************************************************************************************
//longlong counter, counter2, rtn,ct,gl_ct, num_months, i, success, num_to_proc, ana_ret
//string sqls, interface_type, mn_sqls, analyze_cwip
//
//i_num_comps = 0
//
//
// change this to the appropriate column
//uf_set_pp_translate_id(1)
//
//
//
//
//f_pp_msgs( "Reseting " + i_from_table + " at " + string(now()))
//
//sqls = "update " + i_from_table + " set " + i_status_field + " = 999 where nvl("+i_status_field+", 0) between 0 and 998"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs( "  ERROR: updating  " + i_from_table + "."+i_status_field+" (900's) to 999: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs( "  The interface will terminate without loading  " + i_to_table + " !!!")
//	rollback;
//	return -1
//end if
//
//f_pp_msgs( "Counting the records to be read in " + i_from_table + " at " + string(now()))
//
//
//sqls = "select * from " + i_from_table + " where " +i_status_field+ " = 999";
//
//execute immediate :sqls;
//
//ct = sqlca.sqlnrows
//
//
//	
//f_pp_msgs( i_from_table + " has " + string(ct) + " records to process.")
//
//	
//
//	
//	// PERFORM PRE LOADING VALIDATION
//	f_pp_msgs("Validating mappings at " + string(now()))
//	rtn = uf_validate_pre_load()
//	if rtn = -1 then
//		return -1
//	end if
//	
//	
//
//	
//	//*********************************************************************************
//	//  Load the CR_TEMP_CWIP_CHARGE table.
//	//*********************************************************************************
//	f_pp_msgs( "Inserting into cr_cwip_charge_translate at " + string(now()))
//	rtn = uf_load_temp_cwip_charges()
//	
//	if rtn < 0 then
//		return -1
//	end if
//	
//	
//	
//	//*********************************************************************************
//	//  CHARGE_ID:
//	//    Fill in the charge_id on cr_temp_cwip_charge ... it is currently NULL.
//	//*********************************************************************************
//	f_pp_msgs( "Updating the charge_id at " + string(now()))
//	rtn = uf_update_charge_id()
//	
//	if rtn < 0 then
//		return -1
//	end if
//	
//	//*********************************************************************************
//	//  CWIP_CHARGE:
//	//    Insert data into cwip_charge from cr_cwip_charge_sum.
//	//*********************************************************************************
//	f_pp_msgs( "Inserting into cwip_charge at " + string(now()))
//	rtn = uf_insert_cwip_charge()
//	
//	if rtn < 0 then
//		return -1
//	end if
	
	

	
	
	




	

return 1
end function

public function integer uf_validate ();//longlong rtn, i, num_rows, error_code, where_id
//longlong j, num_where, tab1, tab2
//string lp, cn, op, v1, ba, v2, rp, ao
//datastore ds_t, ds_where, ds_table, ds_check
//string ds_sql, ds_wh_sql, ds_tab_sql
//string s_w, s_where, s_from, s_sql, sql_check, descr
//longlong check
//
//// VALIDATE ROWS IN CR THAT MIGHT FAIL A JOIN
//// LOOK IN THE pp_translate_map TABLE AND ONLY VALIDATE THE
//// COLUMNS WITH AN ERROR_CODE NOT NULL
//
//// create the datastore
//ds_t = create datastore
//ds_sql = "select where_id, error_code" +&
//			" from pp_translate_control crtcc," +&
//			" pp_translate_map crtct" +&
//			" where crtcc.translate_id = crtct.translate_id" +&
//			" and error_code is not null" +&
//			" and crtcc.source_id = " + string(a_source_id) +&
//			" order by error_order"
//f_create_dynamic_ds(ds_t, 'grid', ds_sql, sqlca, true)
//num_rows = ds_t.rowcount()
//for i = 1 to num_rows
//	where_id = ds_t.getItemNumber(i, 'where_id')
//	error_code = ds_t.getItemNumber(i, 'error_code')
//	
//	ds_wh_sql = "select * from pp_translate_map_clause where where_id = " +&
//						string(where_id) + " order by row_id"
//	ds_tab_sql =	"select distinct table_name, table_alias" +&
//						" from pp_translate_map_clause ctc, pp_translate_table_list tl" +&
//						" where where_id = " + string(where_id) + " and " +&
//						" (table_id = table1 OR table_id = table2)" +&
//						" and table_alias <> 'cr'"
//	ds_where = create datastore
//	ds_table = create datastore
//	f_create_dynamic_ds(ds_where, 'grid', ds_wh_sql, sqlca, true)
//	f_create_dynamic_ds(ds_table, 'grid', ds_tab_sql, sqlca, true)
//	num_where = ds_table.rowcount()
//	
//
//	if num_where = 0 then
//		s_from = " from dual, "
//	else
//		s_from = " from "
//	end if
//	
//	// loop through the tables and build the from clause
//	for j = 1 to num_where
//		s_from = s_from + ds_table.getItemString(j, 'table_name') +&
//					" " + ds_table.getItemString(j, 'table_alias') + ", "
//	next
//	s_from = LEFT(s_from, LEN(s_from) - 2)
//	// loop through the where claus and build the where
//	num_where = ds_where.rowcount()
//	s_where = " where "
//	for j = 1 to num_where
//		lp = ds_where.getitemstring(j, 'left_paren')
//		cn = ds_where.getitemstring(j, 'column_name')
//		op = ds_where.getitemstring(j, 'operator')
//		v1 = ds_where.getitemstring(j, 'value1')
//		ba = ds_where.getitemstring(j, 'between_and')
//		v2 = ds_where.getitemstring(j, 'value2')
//		ao = ds_where.getitemstring(j, 'and_or')
//		rp = ds_where.getitemstring(j, 'right_paren')
//		
//		if isnull(lp) then
//			lp = ""
//		end if
//		if isnull(cn) then
//			cn = ""
//		end if
//		if isnull(op) then
//			op = ""
//		end if
//		if isnull(v1) then
//			v1 = ""
//		end if
//		if isnull(ba) then
//			ba = ""
//		end if
//		if isnull(v2) then
//			v2 = ""
//		end if
//		if isnull(rp) then
//			rp = ""
//		end if
//		if isnull(ao) then
//			ao = ""
//		end if
//		
//	
//		s_w = lp + " " + cn + " " + op + " " + v1 + " " + ba + " " + v2 + " " + rp + " " + ao	
//		s_where = s_where + s_w + " "
//	next
//
//	destroy ds_where
//	destroy ds_table
//	
//	
//	s_sql = "update " + a_table_name + " set " + i_status_field + " = " +&
//	 			" nvl( (" +& 
//				   " select " + a_table_name + "." + i_status_field + " " +&
//					s_from + f_replace_string(s_where, "cr.", a_table_name + ".", "all") +&
//				 + "), " + string(error_code) + ") " +&
//				" where " + i_status_field + " = 0  "
//				
//	execute immediate :s_sql;
//	
//	if sqlca.sqlcode < 0 then
//		f_pp_msgs("  ERROR:  There are some errors with the where clause (id)" + string(where_id) +&
//							" for the source " + a_table_name)
//		f_pp_msgs("  SQL ERROR: " + sqlca.SQLErrText)
//		f_pp_msgs("  SQL = " + s_sql)
//		rollback;
//		return -1
//	end if
//	
//	// $$$ BSB 20060523
//	// count to see if there are any errors 
//	ds_check = create datastore
//	
//	sql_check = "select count(*) from " + a_table_name +&
//					" where " + i_status_field + " = " + string(error_code)
//	f_create_dynamic_ds(ds_check, 'grid', sql_check, sqlca, true)
//	if ds_check.rowCount() > 0 then
//		check = 0
//		check = ds_check.getItemNumber(1, 1)
//		if check > 0 then
//			
//			descr = ''
//			select 'Invalid ' || long_description into :descr from pp_translate_map
//			 where error_code = :error_code;
//			
//			f_pp_msgs("  ")
//			f_pp_msgs("*****  VALIDATION ERRORS EXIST: (" +&
//						string(error_code) + "): " + descr)
//			f_pp_msgs("  ")
//			
//		end if
//	end if
//	destroy ds_check
//	// $$$ END BSB
//next
//destroy ds_t
return 1
end function

public function longlong uf_load_destination ();//*********************************************************************************************************************
//
//  User Object Function  :  uf_load_destination
//
//  Description  :  Get the valid records from FROM_TABLE and load them into TO_TABLE.
//
//*********************************************************************************************************************
string sqls, s_insert, s_insert_cols, s_selects, s_from, s_where, s_sum_select
string s_group
datastore ds_t
datastore ds_trans
datastore ds_t_crit
datastore ds_tab
string s_ds_crit, s_ds_trans, ds_tab_sql, s_ds
longlong num, i, rtn, num_where, j, trans_id
string cwip_col, cr_col, cr_col_trans
string lp, cn, op, v1, ba, v2, ao, rp, s_w, s_insert_sum
longlong ana_ret
string subbed_sql


g_debug = "YES"



s_insert_cols = "( "
s_selects = "select "
s_sum_select = ""
s_group = ""
//s_insert = "insert into cr_cwip_charge_translate "
s_insert = "insert into " + i_to_table + " "

// have to use alias cr here because that is the alias used in the pp_translate_control table
s_from = "from " + i_from_table + " cr " 
// build the rest of the from clause
ds_tab_sql =	"select distinct table_name, table_alias" +&
					" from pp_translate_map_clause ctc," +&
					" pp_translate_table_list tl, pp_translate_control cc" +&
					" , pp_translate_map ct" +&
					" where ctc.where_id = ct.where_id and " +&
					" (tl.table_id = ctc.table1 OR tl.table_id = ctc.table2)" +&
					" and tl.table_alias <> 'cr'" +&
					" and cc.translate_id = ct.translate_id " +&
					" and cc.active_mapping = 1" + &
					 " and cc.pp_translate_id = " + String(i_pp_translate_id)

					
ds_tab = create datastore
f_create_dynamic_ds(ds_tab, 'grid', ds_tab_sql, sqlca, true)
num = ds_tab.rowcount()
for i = 1 to num
	s_from = s_from + ", " + ds_tab.getItemString(i, 'table_name') +&
				" " + ds_tab.getItemString(i, 'table_alias') + " "
next
destroy ds_tab
s_where = "where cr." + i_status_field + " = 0  and "

ds_t = create datastore
s_ds = "select * from pp_translate_control " +&
			" where active_mapping = 1 " +&
			" and pp_translate_id = " + String(i_pp_translate_id)

f_create_dynamic_ds(ds_t, 'grid', s_ds, sqlca, true)
num = ds_t.rowcount()

for i = 1 to num
	setnull(trans_id)
	trans_id = ds_t.GetItemNumber(i, 'translate_id')
	cwip_col = ds_t.GetItemString(i, 'to_table_col')
	cr_col = ds_t.GetItemString(i, 'val_to_insert')
	s_insert_cols = s_insert_cols + cwip_col + ", "
	if isnull(trans_id) then 
		// not a translation
		s_selects = s_selects + cr_col + " as " + cwip_col + ", "
	else
		// translation exists either by id or by external
		ds_trans = create datastore
		s_ds_trans = 	"select where_id, val_to_insert from pp_translate_map" +&
							" where translate_id = " + string(trans_id)
		f_create_dynamic_ds(ds_trans, 'grid', s_ds_trans, sqlca, true)
		cr_col_trans = ds_trans.GetItemString(1, 'val_to_insert')
		s_selects = s_selects + cr_col_trans + " as " + cwip_col + ", "
		
		// build the where clause for this translation
		ds_t_crit = create datastore
		s_ds_crit = "select * from pp_translate_map_clause" +&
						" where where_id = " + string(ds_trans.GetItemNumber(1, 'where_id')) +&
						" order by row_id"
		f_create_dynamic_ds(ds_t_crit, 'grid', s_ds_crit, sqlca, true)
		num_where = ds_t_crit.rowcount()
		for j = 1 to num_where
			lp = ds_t_crit.getitemstring(j, 'left_paren')
			cn = ds_t_crit.getitemstring(j, 'column_name')
			op = ds_t_crit.getitemstring(j, 'operator')
			v1 = ds_t_crit.getitemstring(j, 'value1')
			ba = ds_t_crit.getitemstring(j, 'between_and')
			v2 = ds_t_crit.getitemstring(j, 'value2')
			ao = ds_t_crit.getitemstring(j, 'and_or')
			rp = ds_t_crit.getitemstring(j, 'right_paren')
			
			if isnull(lp) then
				lp = ""
			end if
			if isnull(cn) then
				cn = ""
			end if
			if isnull(op) then
				op = ""
			end if
			if isnull(v1) then
				v1 = ""
			end if
			if isnull(ba) then
				ba = ""
			end if
			if isnull(v2) then
				v2 = ""
			end if
			if isnull(rp) then
				rp = ""
			end if
			if isnull(ao) then
				ao = ""
			end if
			
			// RJO - 4/5/2006 - Right Paren must be before AND/OR!!!
			//s_w = lp + " " + cn + " " + op + " " + v1 + " " + ba + " " + v2 + " " + ao + " " + rp	
			s_w = lp + " " + cn + " " + op + " " + v1 + " " + ba + " " + v2 + " " + rp + " " + ao	
			s_where = s_where + s_w + " "
		next
		
		s_where = s_where + " and "
		destroy ds_t_crit
		destroy ds_trans
	end if
	
	// below is used to build the select and group by
	// for the summarization of the 
	// cr_to_cwip_charge_translate table to cr_to_cwip_charge_sum table

		s_sum_select = s_sum_select + "crcct." + cwip_col + ", "
		s_group = s_group + "crcct." + cwip_col + ", "

next
destroy ds_t

//s_insert_sum = left(s_insert_cols, len(s_insert_cols) - 2) + " ) "


s_insert_cols = left(s_insert_cols, len(s_insert_cols) - 2) + " ) "
s_selects = left(s_selects, len(s_selects) - 2)

//s_sum_select = left(s_sum_select, len(s_sum_select) - 2)

s_group = left(s_group, len(s_group) - 2)
s_where = left(s_where, len(s_where) - 4)

f_pp_msgs( "Loading " + i_to_table + " at " + string(now()))

sqls = s_insert + " " + s_insert_cols + " ( " + s_selects + " " +&
			s_from + " " + s_where + " ) "

execute immediate :sqls;

if g_debug = "YES" then
	f_write_log(g_log_file, "***DEBUG*** sqls = " + sqls)
end if

// $$$ BSB 20060523
// write to online logs
subbed_sql = left(trim(sqls), 2000)
//f_pp_msgs(subbed_sql)
// $$$ END BSB

if g_debug = "YES" then
	f_pp_msgs(left("***DEBUG*** subbed_sql = " + subbed_sql, 2000))
end if

if sqlca.SQLCode < 0 then
	f_pp_msgs( "  ERROR: inserting into " + i_to_table + ": " + &
		sqlca.SQLErrText)
	f_pp_msgs( "  The interface will terminate without loading " + i_to_table + " !!!")
	rollback;
	return -1
end if

// log the number of records written 
f_pp_msgs( "Inserted " + string(sqlca.sqlnrows) + " records into " + i_to_table + "")

//commit;
f_pp_msgs( "Analyzing " + i_to_table + " at " + string(now()))
sqlca.analyze_table(i_to_table)

if sqlca.sqlcode < 0 then
	f_pp_msgs( "  ERROR: analyzing " + i_to_table + ":")
	f_pp_msgs( "  The interface will continue.")
end if

// DO POST LOAD VALIDATION BEFORE SUMMARIZING INTO CR_CWIP_CHARGE_SUM
// THIS IS A CUSTOM FUNCTION FOR UPDATES OR VALIDATIONS
//rtn = f_cwip_charge_custom(1)
//if rtn = -1 then
//	rollback;
//	return -1
//end if


//// SUMMARIZE
//sqls = "insert into cr_cwip_charge_sum"
//sqls = sqls + " " + s_insert_sum + " "
//sqls = sqls + " ( select "
//sqls = sqls + s_sum_select
//sqls = sqls + " from cr_cwip_charge_translate crcct, " + i_from_table + " crcr "
//sqls = sqls + " where crcr.id = crcct.charge_id "
//sqls = sqls + " and crcr." + i_status_field + " = 0 "
//sqls = sqls + " and crcr.amount_type = 1 "
//sqls = sqls + " and crcct.source_id = " + string(a_source)
//sqls = sqls + " group by " + s_group
//sqls = sqls + " )"
//execute immediate :sqls;
//
//f_write_log(g_log_file, sqls)
// 
//if sqlca.SQLCode < 0 then
//	f_pp_msgs( "  ERROR: inserting into cr_cwip_charge_sum: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs( "  The interface will terminate without loading CWIP_CHARGE !!!")
//	rollback;
//	return -1
//end if
//
//// log the number of records written 
//f_pp_msgs( "Inserted " + string(sqlca.sqlnrows) + " records into cr_cwip_charge_sum")

//commit;

return 1
end function

public function integer uf_preprocess ();String sqls;

f_pp_msgs( "Reseting " + i_from_table + " at " + string(now()))

sqls = "update " + i_from_table + " set " + i_status_field + " = 999 where nvl("+i_status_field+", 0) between 0 and 998"

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs( "  ERROR: updating  " + i_from_table + "."+i_status_field+" (900's) to 999: " + &
		sqlca.SQLErrText)
	f_pp_msgs( "  The interface will terminate without loading  " + i_to_table + " !!!")
	rollback;
	return -1
end if

f_pp_msgs( "Counting the records to be read in " + i_from_table + " at " + string(now()))


sqls = "select * from " + i_from_table + " where " +i_status_field+ " = 999";

execute immediate :sqls;


f_pp_msgs( i_from_table + " has " + string(sqlca.sqlnrows) + " records to process.")


return 0
end function

public subroutine uf_set_pp_translate_id (longlong a_pp_translate_id);i_pp_translate_id = a_pp_translate_id

// read the master data
select to_table into :i_to_table
from pp_translate
where pp_translate_id = :i_pp_translate_id;


select to_table into :i_from_table
from pp_translate
where pp_translate_id = :i_pp_translate_id;


select status_field into :i_status_field
from pp_translate
where pp_translate_id = :i_pp_translate_id;

end subroutine

public function integer uf_process (longlong a_pp_translate_id);


uf_set_pp_translate_id(a_pp_translate_id)


if (uf_preprocess() < 0) then
	return -1
end if



if (uf_validate() < 0) then
	return -2
end if



if (uf_load_destination() < 0) then
	return -3
end if


return 0
end function

on uo_pp_translate.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_pp_translate.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

