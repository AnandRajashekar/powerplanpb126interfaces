HA$PBExportHeader$uo_mx_cu.sru
forward
global type uo_mx_cu from nonvisualobject
end type
end forward

global type uo_mx_cu from nonvisualobject
end type
global uo_mx_cu uo_mx_cu

type variables
string i_exe_name = 'mx_cu.exe'
string il_batchid
end variables

forward prototypes
public function integer uf_check_success ()
public function longlong uf_load_cu (string al_batchid)
end prototypes

public function integer uf_check_success ();//*****************************************************************************************
//
//  Object      :  uo_mx_cu
//  Function	 :  uf_check_success
//
//	Returns   :  1 if successful
// 					-1 otherwise
//
//  Notes       :  Error if sql statement failed, otherwise success.
//
//*****************************************************************************************


if sqlca.sqlcode < 0 then
                f_pp_msgs('')
                f_pp_msgs(": Error:" + sqlca.sqlerrtext)
                f_pp_msgs('')
                f_pp_msgs('~~~~~~~END : FAILURE ~~~~~~~~')
                rollback;
                return -1
else
                f_pp_msgs(": Success: " + string(sqlca.sqlnrows) + " rows affected.")
                return 1
end if
end function

public function longlong uf_load_cu (string al_batchid);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
//
//   Subsystem:  system
//
//   Event    :  uo_mx_cu.uf_load_cu(al_batchid)
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			--------------------
//  02-22-2012      PowerPlan                 Version 1.0            Modified Version for CU [Compatible Unit]
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure, i, j, rtn
string col_sqls, col_list, sqls, rtns

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////Intend to pass the batch ID into uo_mx_cu.uf_load_cu/////////////////////////////////
/////////////////And use that batch ID to update/insert records for processing/////////////////03.22.2012/////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Retrieving batch id 
//select max(occurrence_id) into :v_batchid from pp_processes_occurrences where process_id = :g_process_id;
il_batchid = al_batchid
//
//Update batchid
//f_pp_msgs(" ")
//f_pp_msgs("Updating batchid in staging table:")
//
//update mx_ppiface_cu 
//set batch_id = :il_batchid
//where (batch_id is null or batch_id = '' or batch_id = 0);
//DNB: 03062012 - Added empty string to where clause

//if uf_check_success() < 0 then return rtn_failure
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//if there are two records for the same cu, only keep the record with the latest timestamp
f_pp_msgs(" ")
f_pp_msgs("Updating error_code for records that have more than one update:")

update mx_ppiface_cu s 
set /*batch_id = -1,*/upload_indicator = 'D', error_code = 'Not the latest record update in this batch.'
/*where exists (select 0
                    from mx_ppiface_cu t
				  where t.plusdcuid = s.plusdcuid
				  and s.time_stamp < t.time_stamp
				  and s.batch_id = :il_batchid
				  and s.batch_id = t.batch_id)*/
where s.batch_id = :il_batchid
and nvl(s.transid,0) < (
	select nvl(max(t.transid),0)
	from mx_ppiface_cu t
	where t.plusdcuid = s.plusdcuid
	and t.batch_id = :il_batchid
	);

if uf_check_success() < 0 then return rtn_failure 
//
//Updating upload_indicator to 'U'
f_pp_msgs("  ")
f_pp_msgs("Updating upload_indicator to 'U'[rows to Update]:")
//Verify if record already exists in the pwrplant table, if so, process for UPDATE
update mx_ppiface_cu t 
set t.upload_indicator = 'U'
where exists (select 0
				  from mx_cu w
				  where w.plusdcuid = t.plusdcuid)
and (t.upload_indicator not in ('D') or t.upload_indicator is null)
and t.batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure   

//Updating upload_indicator to 'I'
f_pp_msgs("  ")
f_pp_msgs("Updating upload_indicator to 'I'[new rows to Insert]:")
//Set remaining records pending processing into the pwrplant table, to INSERT
update mx_ppiface_cu t
set t.upload_indicator = 'I'
where (t.upload_indicator not in ('U','D') or t.upload_indicator is null)
and t.batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

//Retrieving column names from table	
uo_ds_top ds_all_tab_col
ds_all_tab_col = create uo_ds_top
	
col_sqls = "select lower(column_name)||',' from all_tab_columns" + &
			  " where table_name = upper('mx_ppiface_cu') " +&
			  " and upper(column_name) not in ('USER_ID','UPLOAD_INDICATOR', 'BATCH_ID', 'ERROR_CODE','TIME_STAMP') "
			  
rtns = f_create_dynamic_ds(ds_all_tab_col,"grid",col_sqls,sqlca,true)
		
		if rtns <> 'OK' then // ---
			f_pp_msgs(" ")
			f_pp_msgs("ERROR: Failure running f_create_dynamic_ds: " + rtns + ds_all_tab_col.i_sqlca_sqlerrtext)  
			f_pp_msgs(" ")
			rollback;
			return rtn_failure
		end if		  

col_list = ""
for j = 1 to ds_all_tab_col.rowcount()
	col_list += ds_all_tab_col.GetItemString(j,1)
next
col_list = left(col_list,len(col_list)-1)

f_pp_msgs(" ")
f_pp_msgs("Updating cu(s) in mx_cu table:")

sqls =  "update mx_cu a set ("
sqls += col_list + ") = "
sqls += " (select " + col_list + " from mx_ppiface_cu b " 
sqls += " where b.upload_indicator = 'U' and b.batch_id = '" + il_batchid + "' "
sqls += " and a.plusdcuid = b.plusdcuid) "
sqls += " where exists ( select 1 from mx_ppiface_cu b"
sqls += " where b.upload_indicator = 'U' and b.batch_id = '" + il_batchid + "' "
sqls += " and a.plusdcuid = b.plusdcuid) " 

execute immediate :sqls;

if uf_check_success() < 0 then return rtn_failure

f_pp_msgs(" ")
f_pp_msgs("Updating cu(s) in compatible_unit table:")

// PP-41920
sqls =  "update compatible_unit a set (time_stamp, user_id, description, long_description,external_comp_unit)"
sqls += " = (select sysdate, user, substr(description,1,35), description, plusdcuid from mx_ppiface_cu b " 
sqls += " where b.upload_indicator = 'U' and b.batch_id = '" + il_batchid + "' "
sqls += " and a.external_comp_unit = to_char(b.plusdcuid)) "
sqls += " where exists ( select 1 from mx_ppiface_cu b"
sqls += " where b.upload_indicator = 'U' and b.batch_id = '" + il_batchid + "' "
sqls += " and a.external_comp_unit = to_char(b.plusdcuid)) " 

execute immediate :sqls;

if uf_check_success() < 0 then return rtn_failure

f_pp_msgs(" ")
f_pp_msgs("Inserting new cu(s) to mx_cu table:")

sqls =  "insert into mx_cu  ("
sqls += col_list + ") "
sqls += "select " + col_list + " from mx_ppiface_cu " 
sqls += " where upload_indicator = 'I' and batch_id = '" + il_batchid + "' "

execute immediate :sqls;

if uf_check_success() < 0 then return rtn_failure

//insert into compatible_unit table
f_pp_msgs(" ")
f_pp_msgs("Inserting new cu(s) to compatible_unit table:")

sqls =  "insert into compatible_unit  (comp_unit_id, time_stamp, user_id, description, long_description,external_comp_unit)"
sqls += "select pwrplant1.nextval, sysdate, user, substr(description,1,35), description, plusdcuid from mx_ppiface_cu " 
sqls += " where upload_indicator = 'I' and batch_id = '" + il_batchid + "' "

execute immediate :sqls;

if uf_check_success() < 0 then return rtn_failure

////Marking records as completed - Done - "D"
f_pp_msgs(" ")
f_pp_msgs("Marking records as processed:")

update mx_ppiface_cu
set upload_indicator = 'D', time_stamp = sysdate, user_id = user
where upload_indicator in ('I','U')
and batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

f_pp_msgs(" ")
f_pp_msgs("Archiving processed records:")

insert into mx_ppiface_cu_arc
(acctcode,assetcode,autoexpandflag,bidunitflag,budgetflag,changeby,changedate,classstructureid,cudetailflag,culevelnum,cuname,cuupdateset,cuversion,                   
description,expandableflag,financialwo,historyflag,jobinstructionflag,meainput,orgid,plusdcuid,sketchfeatureflag,           
spanflag,standardssheet,stationdetailflag,statisticalcode,status,statusdate,supportflag,tasktype,uom,wfactive,wfid,                        
worktype,yearflag,transid,transseq,upload_indicator,user_id,time_stamp,archive_time_stamp,batch_id,error_code)
select acctcode,assetcode,autoexpandflag,bidunitflag,budgetflag,changeby,changedate,classstructureid,cudetailflag,culevelnum,cuname,cuupdateset,cuversion,                   
description,expandableflag,financialwo,historyflag,jobinstructionflag,meainput,orgid,plusdcuid,sketchfeatureflag,           
spanflag,standardssheet,stationdetailflag,statisticalcode,status,statusdate,supportflag,tasktype,uom,wfactive,wfid,                        
worktype,yearflag,transid,transseq,upload_indicator,user_id,time_stamp,sysdate,batch_id,error_code
from mx_ppiface_cu
where upload_indicator = 'D'
and batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

delete from mx_ppiface_cu
where upload_indicator = 'D'
and batch_id = :il_batchid;

if uf_check_success() < 0 then return rtn_failure

commit;
return rtn_success

end function

on uo_mx_cu.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mx_cu.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

