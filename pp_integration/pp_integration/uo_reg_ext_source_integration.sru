HA$PBExportHeader$uo_reg_ext_source_integration.sru
forward
global type uo_reg_ext_source_integration from nonvisualobject
end type
type ls_fields from structure within uo_reg_ext_source_integration
end type
type ls_combos from structure within uo_reg_ext_source_integration
end type
end forward

type ls_fields from structure
	longlong		field_id
	string		column_name
end type

type ls_combos from structure
	longlong	combo_id
	longlong	reg_acct_id
end type

global type uo_reg_ext_source_integration from nonvisualobject
end type
global uo_reg_ext_source_integration uo_reg_ext_source_integration

type variables
String i_tableName, i_versionField, i_amtCol, i_extSrcTable

longlong i_versionId, i_reg_acct_src_id

private ls_fields i_ls_fields[]

private ls_combos i_ls_combos[]

uo_ds_top i_ds_combos_fields, i_ds_combos_map, i_ds_wc
end variables

forward prototypes
public function boolean of_buildextcombolist (longlong a_histversion, longlong a_foreversion, longlong a_source, longlong a_company)
public function string of_buildwhereclause (longlong a_histversion, longlong a_foreversion, longlong a_combo)
public function longlong of_getfieldidx (longlong a_fieldid)
public function boolean of_buildextfieldlist (longlong a_sourceid)
public function boolean of_setupextint (integer a_histversion, integer a_fcstversion, integer a_sourceid, longlong a_start, longlong a_end, integer a_company)
public function boolean of_loadregledger (longlong a_company, longlong a_histversion, integer a_foreversion, longlong a_sourceid, longlong a_start, longlong a_end)
public function boolean of_inserttoregledger (longlong a_histversion, longlong a_foreversion, longlong a_source, longlong a_company, longlong a_start, longlong a_end)
public function boolean of_loadexttemp (longlong a_histversion, longlong a_foreversion, longlong a_combo, longlong a_company, longlong a_start, longlong a_end, longlong a_sourceid)
public function string of_getfieldname (longlong a_sourceid, string a_field)
public function string of_builddrillbackwhereclause (longlong a_version_id, boolean ab_historic, string a_reg_acct_list, longlong a_reg_source_id)
public function boolean of_buildledgerbalances (longlong a_histversion, longlong a_fcstversion, longlong a_sourceid, longlong a_company, longlong a_startmonth, longlong a_endmonth)
end prototypes

public function boolean of_buildextcombolist (longlong a_histversion, longlong a_foreversion, longlong a_source, longlong a_company);/**********************************************************************
** 
**		Method: of_BuildExtComboList
**		Retrieves list in a local structure (i_ls_combos) of external source combos to process
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/

string sqls, rtns, versionField
longlong i, version
uo_ds_top ds_combos

ls_combos empty[]

if a_histVersion <> 0 then
	version = a_histVersion
	versionField = "HISTORIC_VERSION_ID"
else 
	version = a_foreVersion
	versionField = "FORECAST_VERSION_ID"
end if

sqls = "select c.ext_combo_id, reg_acct_id ~r~n"
sqls += " from reg_ext_combos_map m, reg_ext_combos c  ~r~n"
sqls += " where c.ext_source_id = " + string(a_source) + "~r~n"
sqls += " and m." + versionField + " = " + string(version) + "~r~n"
sqls += " and m.ext_combo_id = c.ext_combo_id ~r~n"


ds_combos = create uo_ds_top
rtns = f_create_dynamic_ds(ds_combos, 'grid', sqls, sqlca, true)

if rtns <> 'OK' then
	messagebox("Build LS Combos", "ERROR: Creating ds_combos: " + rtns)
	return false
end if

ds_combos.SetSort("#1 A")
ds_combos.Sort()

for i = 1 to ds_combos.rowCount()
	i_ls_combos[i].combo_id = ds_combos.getitemNumber(i, 1)
	i_ls_combos[i].reg_acct_id = ds_combos.getitemNumber(i, 2)
next

return true
end function

public function string of_buildwhereclause (longlong a_histversion, longlong a_foreversion, longlong a_combo);/**********************************************************************
** 
**		Method: of_BuildWhereClause()
**		Build the where clause string for an external source combo to use when loading
**				the reg_ext_load_temp table
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/

string sqls, where_clause, lowerVal, upperVal, not_Str, rtns
longlong elementId, sourceId, fieldId, notInd, index, i, source_id, field_id, not_ind 

sqls = "select ext_source_id, field_id, not_indicator, lower_value, upper_value from reg_ext_combos_fields " + &
		 " where ext_combo_id = " + string(a_combo)+ &
		 "   and historic_version_id = " + string(a_histVersion)+ &
		 "	 and forecast_version_id = " + string(a_foreVersion)
		 
i_ds_combos_fields = create uo_ds_top		 

rtns = f_create_dynamic_ds(i_ds_combos_fields, 'grid', sqls, sqlca, true)

if rtns <> 'OK' then
	messagebox("Build DS Combos Fields", "ERROR: Creating ds_combos_fields: " + rtns)
	return ""
end if

where_clause = " and "
for i = 1 to i_ds_combos_fields.rowcount()
	source_id  = i_ds_combos_fields.GetItemNumber(i,1)
	field_id   = i_ds_combos_fields.GetItemNumber(i,2)
	notInd		  = i_ds_combos_fields.GetItemNumber(i,3)
	lowerVal   = i_ds_combos_fields.GetItemString(i,4)
	upperVal   = i_ds_combos_fields.GetItemString(i,5)
	
	if isnull(source_id)  then source_id = 0
	if isnull(field_id)   then field_id = 0
	if isnull(notInd) 	    then not_ind = 0
	if isnull(lowerVal)   then lowerVal = ""
	if isnull(upperVal)   then upperVal = ""
	
	//Should we keep this?
	if lowerVal = "" and upperVal = "" then continue
	
	if lowerVal <> "" then lowerVal = trim(lowerVal)
	if upperVal <> "" then upperVal = trim(upperVal)
	
	if field_id <> 0 then
		// look up the field
		index = of_getFieldIdx(field_id)
		
		if index = 0 then continue
		
		if not_ind = 1 then
			not_str = " not "
		else
			not_str = ""
		end if
		
	end if
	
	if trim(upperVal) = "" then upperVal = lowerVal
	
	where_clause += "to_char(extSrc." + i_ls_fields[index].column_name+ ") " + not_str + " between '" + lowerVal + "' and '" + upperVal + "'~r~n"
	
	if i <> i_ds_combos_fields.rowcount() then
		where_clause += " and "
	end if
next

if right(where_clause, 4) = "and " then
	where_clause = left(where_clause, len(where_clause) - 4)
end if

if isNull(where_clause) then
		messageBox("Build Where Clause", "Error: Where Clause is null")
end if

destroy i_ds_combos_fields

return where_clause
end function

public function longlong of_getfieldidx (longlong a_fieldid);/**********************************************************************
** 
**		Method: of_GetFieldIdx()
**		Determine where in the array the field is located
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/

longlong i, idx

for i = 1 to upperBound (i_ls_fields)
	if i_ls_fields[i].field_id = a_fieldId then
		idx = i
		exit
	end if
next

return idx
end function

public function boolean of_buildextfieldlist (longlong a_sourceid);/**********************************************************************
** 
**		Method: of_BuildExtFieldList()
**		Builds array of the external fields used in mapping to regulatory accounts
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/

string sqls, rtns
longlong i
ls_fields empty[]
uo_ds_top ds_fields

sqls = "select field_id, column_name ~r~n"
sqls += " from reg_ext_source_field ~r~n"
sqls += " where ext_source_id = " + string(a_sourceId) + "~r~n"
sqls += " and translate_use > 0 "

ds_fields = create uo_ds_top

rtns = f_create_dynamic_ds(ds_fields, 'grid', sqls, sqlca, true)

if rtns <> 'OK' then
	messagebox("Build LS Fields", "ERROR: Creating ds_fields: " + rtns)
	return false
end if

ds_fields.SetSort("#1 A")
ds_fields.Sort()

i_ls_fields = empty
for i = 1 to ds_fields.rowCount()
	i_ls_fields[i].field_id = ds_fields.GetItemNumber(i,1)
	i_ls_fields[i].column_name = ds_fields.getItemString(i,2)
next

destroy ds_fields

return true


end function

public function boolean of_setupextint (integer a_histversion, integer a_fcstversion, integer a_sourceid, longlong a_start, longlong a_end, integer a_company);/**********************************************************************
** 
**		Method: of_SetupExtInt()
**		Performs validation on inputs and sets up additional processing variables
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/
string sqls, rtns, invalidString
longlong reg_source_id, invalid_combos, i
uo_ds_top ds_invalid

//Verify Parms
if a_histVersion = 0 and a_fcstVersion = 0 then
	MessageBox("Setup External", "Historic and Forecast Version are zero.")
	return false
end if

if a_start = 0 and a_end = 0 then
	MessageBox("Setup External", "Start and End Months are zero.")
	return false
end if

//Validate unique pull type per reg account
sqls = "SELECT m.Reg_Acct_Id, COUNT(DISTINCT c.Pull_Type_Id), am.description ~r~n"
sqls += "FROM Reg_Ext_Combos c, Reg_Ext_Combos_Map m, reg_acct_master am ~r~n"
sqls += "WHERE c.Ext_Combo_Id = m.Ext_Combo_Id ~r~n"
sqls += "AND m.reg_acct_id = am.reg_acct_id ~r~n"
sqls += "AND c.Ext_Source_Id = "+string(a_sourceId)+" ~r~n"
sqls += "AND m.Historic_Version_Id = "+string(a_HistVersion)+" ~r~n"
sqls += "AND m.Forecast_Version_Id = "+string(a_Fcstversion)+" ~r~n"
sqls += "GROUP BY m.Reg_Acct_Id, am.description ~r~n"
sqls += "HAVING COUNT(DISTINCT c.Pull_Type_Id) > 1 ~r~n"

if isNull(Sqls) then
	messagebox("Setup External", "ERROR: Sqls string is null ds_invalid")
	return false
end if		

ds_invalid = create uo_ds_top

rtns = f_create_dynamic_ds(ds_invalid,'grid',sqls,sqlca,true)

if rtns <> 'OK' then
	messagebox("Setup External", "ERROR: Creating ds_invalid: " + rtns)
	return false
end if		

//If invalid combos exist then return message with which ones are bad
if ds_invalid.RowCount() > 0 then
	invalidString = " "
	for i = 1 to ds_invalid.rowCount()
		invalidString += ds_invalid.getItemString(i, 3) + ", "	
	next
	//Take off last comma
	invalidString = left(invalidString, len(invalidString) - 2)
	
	MessageBox("Setup External", "ERROR: The following accounts have multiple pull types: "+invalidString+".")
	
	return false
end if

if a_histVersion <> 0 then
	i_tableName = 'REG_HISTORY_LEDGER'
	i_versionField = 'HISTORIC_VERSION_ID'
	i_amtCol = 'ACT_AMOUNT'
	i_versionId = a_histVersion
elseif a_fcstVersion <> 0 then
	i_tableName = 'REG_FORECAST_LEDGER'
	i_versionField = 'FORECAST_VERSION_ID'
	i_amtCol = 'FCST_AMOUNT'
	i_versionId = a_fcstVersion
end if

if a_start = 0 then a_start = a_end

if a_end = 0 then a_end = a_start

select table_name, reg_acct_source_id, reg_source_id
into :i_extSrcTable, :i_reg_acct_src_id, :reg_source_id
from reg_ext_source 
where ext_source_id = :a_sourceId;

if sqlca.sqlcode <> 0 then
	MessageBox("Setup External", "Error retrieving table name for Source Id ("+string(a_sourceId)+"): "+sqlca.sqlerrText)
	return false
end if

sqls = "truncate table reg_ext_load_temp"

execute immediate :sqls;


//Check that company_id is not reg_company_id
//Should be limited by source as well
sqls = "Delete from " + i_tableName 
sqls += " where " + i_versionField + " = " + string(i_versionId) +"~r~n"
if a_company <> 0 then
	sqls += "	and reg_company_id = " + string(a_Company) + " ~r~n"
end if
sqls += "	and gl_month between " + string(a_start) + " and " + string(a_end) + " ~r~n"
sqls += "   and nvl(reg_source_id,0) = " + string(reg_source_id) + " ~r~n"
sqls += "	and reg_acct_id in (~r~n"
sqls += "				Select reg_acct_id from reg_acct_master~r~n"
sqls += "				where reg_source_id = "+string(i_reg_acct_src_id)+")~r~n"

execute immediate :sqls;

if sqlca.sqlcode <> 0 then
	MessageBox("Setup External", "Error deleting from "+i_tableName+ ": "+sqlca.sqlerrText)
	return false
end if

return true



end function

public function boolean of_loadregledger (longlong a_company, longlong a_histversion, integer a_foreversion, longlong a_sourceid, longlong a_start, longlong a_end);/**********************************************************************
** 
**		Method: of_LoadRegLedger()
**		Primary controlling function for loading external source transaction to reg ledger
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/
boolean successFlag
longlong i, comboId, ext_source_id

// look up the external source id based on the reg source id that was passed
select ext_source_id
  into :ext_source_id
  from reg_ext_source
 where reg_source_id = :a_sourceId;

if isnull(ext_source_id) then
	return false
end if

successFlag = false

//Set up Processing tables and variables
if not of_setupextint( a_histversion, a_foreVersion,  ext_source_id, a_start,  a_end, a_company) then
	return false
end if

//Get list of source specific fields
if not of_buildextfieldlist(ext_source_id) then
	return false
end if

//Get list of combos to process
if not of_buildextcombolist( a_histVersion, a_foreversion, ext_source_id, a_company) then
	return false
end if

//Process each combo
for i = 1 to upperBound(i_ls_combos)
	comboId = i_ls_combos[i].combo_Id
	of_loadExtTemp( a_histversion, a_foreversion, comboId, a_company, a_start, a_end, ext_source_id )
next

//Build balance amounts for accounts needing balances
if not of_buildledgerbalances( a_histVersion, a_foreversion, a_sourceid, a_company, a_start, a_end) then
	return false
end if

//Insert records to reg ledger
if of_inserttoregledger( a_histversion, a_foreversion, a_sourceId, a_company, a_start,  a_end ) then
	commit;
	successFlag = true
else
	rollback;
end if

return successFlag

end function

public function boolean of_inserttoregledger (longlong a_histversion, longlong a_foreversion, longlong a_source, longlong a_company, longlong a_start, longlong a_end);/**********************************************************************
** 
**		Method: of_InsertToRegLedger()
**		Load translated temp data to regulatory ledger
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/

string sqls

sqls =   "Insert into " + i_tableName + " ~r~n"
sqls += "  (reg_company_id, reg_acct_id, " + i_versionfield + " , gl_month, " + i_amtCol + ", reg_source_id) ~r~n"
sqls += "  select reg_company_id, ~r~n"
sqls += "         reg_acct_id, ~r~n"
sqls += "         "+i_versionField+", ~r~n"
sqls += "         month, ~r~n"
sqls += "         sum(amount), ~r~n"
sqls += "         " + string(a_source) + " ~r~n"
sqls += "    from (SELECT Reg_Company_Id, ~r~n"
sqls += "				 Reg_Acct_Id, ~r~n"
sqls += "				 "+string(i_Versionfield)+", ~r~n"
sqls += "				 MONTH, ~r~n"
sqls += "				 Amount, ~r~n"
sqls += "				 " + string(a_source) +" ~r~n"
sqls += "	FROM (SELECT Reg_Company_Id, ~r~n"
sqls += "							 Reg_Acct_Id, ~r~n"
sqls += "							 " + string(i_Versionfield) + ", ~r~n"
sqls += "							 MONTH, ~r~n"
sqls += "							 Amount ~r~n"
sqls += "				FROM Reg_Ext_Load_Temp t, Reg_Ext_Combos c ~r~n"
sqls += "				WHERE t.Ext_Combo_Id = c.Ext_Combo_Id ~r~n"
sqls += "				AND c.Pull_Type_Id = 1 ~r~n"
sqls += "				UNION ~r~n"
sqls += "				SELECT Reg_Company_Id, ~r~n"
sqls += "							 Reg_Acct_Id, ~r~n"
sqls += "							 " + string(i_Versionfield) + ", ~r~n"
sqls += "							 MONTH, ~r~n"
sqls += "							 SUM(Amount) ~r~n"
sqls += "				Over(PARTITION BY Reg_Company_Id, ~r~n"
sqls += "									Reg_Acct_Id, ~r~n"
sqls += "									"+string(i_Versionfield)+" ~r~n"
sqls += "									ORDER BY MONTH RANGE Unbounded Preceding) Balance ~r~n"
sqls += "				FROM Reg_Ext_Load_Bal_Temp)) ~r~n"
sqls += "	WHERE MONTH >= "+string(a_start)+" ~r~n"
sqls += "	GROUP BY reg_company_id, reg_acct_id, "+i_versionField+", month"

if isNull(sqls) then
	messagebox("Insert to Ledger", "Error: Insert to " + i_tableName + " SQLs is null")
	return false
end if

execute immediate :sqls;

if sqlca.sqlcode <> 0 then
	messagebox("Insert to Ledger", "Error: Inserting to " + i_tableName + ": " +sqlca.sqlerrText)
	return false
end if

return true
end function

public function boolean of_loadexttemp (longlong a_histversion, longlong a_foreversion, longlong a_combo, longlong a_company, longlong a_start, longlong a_end, longlong a_sourceid);/**********************************************************************
** 
**		Method: of_LoadExtTemp()
**		Load the processing temp table used to translate and summarize data from
**			external source to regulatory ledger
**
**		
**		Built by Dr. Shane Ward, King of PS, Esquire
**********************************************************************/

string sqls, whereclause, month, amount, company

whereClause = of_buildWhereClause(a_histVersion, a_foreVersion, a_combo)

month = of_getfieldname( a_sourceId, 'MONTH_NUMBER')
if isNull(month) then
	MessageBox("Load External Temp Table", "Error retrieving month column from reg_ext_source_field for source "+i_extsrctable)
end if
amount = of_getfieldname( a_sourceId, 'AMOUNT')
if isNull(amount) then
	MessageBox("Load External Temp Table", "Error retrieving amount column from reg_ext_source_field for source "+i_extsrctable)
end if
company = of_getfieldname( a_sourceId, 'COMPANY')
if isNull(company) then
	MessageBox("Load External Temp Table", "Error retrieving company column from reg_ext_source_field for source "+i_extsrctable)
end if

sqls += "INSERT INTO reg_ext_load_temp ~r~n"
sqls += "  (reg_company_id, ~r~n"
sqls += "   reg_acct_id, ~r~n"
sqls += "   ext_combo_id, ~r~n"
sqls += "   historic_version_id, ~r~n"
sqls += "   forecast_version_id, ~r~n"
sqls += "   month, ~r~n"
sqls += "   amount) ~r~n"
sqls += "  select comap.reg_company_id, ~r~n"
sqls += "         ecmap.reg_acct_id, ~r~n" 
sqls += "         ecmap.ext_combo_Id, ~r~n"
sqls += "         ecmap.historic_version_id, ~r~n"
sqls += "         ecmap.forecast_version_id, ~r~n"
sqls += "         extsrc."+month+", ~r~n"
sqls += "         sum(extsrc."+amount+") ~r~n"
sqls += "    from reg_ext_combos_map  ecmap, ~r~n"
sqls += "         reg_ext_combos      ec, ~r~n"
sqls += "         reg_ext_company_map comap, ~r~n"
sqls += "         "+i_extSrcTable+"      extsrc ~r~n"
sqls += "   where ecmap.historic_version_id = " + string(a_histVersion) + " ~r~n"
sqls += "     and ecmap.forecast_version_id = " + string(a_foreVersion) + " ~r~n"
sqls += "     and extSrc."+month+" between "+ string(a_start) + " and "+ string(a_end) + " ~r~n"
sqls += "	  and coMap.ext_source_id = " + string(a_sourceId) + " ~r~n"
if a_company <> 0 then
	sqls += "     and coMap.reg_company_id = "+ string(a_company) + " ~r~n"
end if
sqls += "     and ec.ext_combo_id = "+ string(a_combo) + " ~r~n"
sqls += "	  and ec.ext_source_id = " + string(a_sourceId) + " ~r~n"
sqls += "     and ec.ext_combo_id = ecMap.ext_combo_id ~r~n"
sqls += "     and extSrc."+company+" = comap.company ~r~n"
sqls +=     whereClause
sqls += "   group by comap.reg_company_id, ~r~n"
sqls += "            ecMap.reg_acct_id, ~r~n"
sqls += "            ecMap.ext_combo_id, ~r~n"
sqls += "            ecMap.historic_version_id, ~r~n"
sqls += "            ecMap.forecast_version_id, ~r~n"
sqls += "            extSrc."+month+" ~r~n"

if isNull(sqls) then
	messageBox("Load Ext Temp", "Error: Load External Temp Table SQLs is null")
	return false
end if

execute immediate :sqls;

if sqlca.sqlcode <> 0 then 
	messageBox("Load Ext Temp", "Error: Load External Temp Table: " + sqlca.sqlerrText)
	return false
end if

return true

end function

public function string of_getfieldname (longlong a_sourceid, string a_field);string fieldName

select column_name
into :fieldName
from reg_ext_source_field
where ext_source_id = :a_sourceID
and stage_field = :a_field;

if sqlca.sqlcode <> 0 then
	Messagebox("Get Column Name", "Error retrieving column name for field("+a_field+") and source("+i_extSrcTable+"): "+sqlca.sqlerrText)
end if
return fieldName
end function

public function string of_builddrillbackwhereclause (longlong a_version_id, boolean ab_historic, string a_reg_acct_list, longlong a_reg_source_id);string sqls, where_clause, lowerVal, upperVal, not_Str, rtns
longlong elementId, sourceId, fieldId, notInd, index, i, source_id, field_id, not_ind, hist_version_id, fcst_version_id, combo_id, prior_combo_id
uo_ds_top ds_combos_fields

select ext_source_id into :sourceId
  from reg_ext_source
 where reg_source_id = :a_reg_source_id;
 
if isnull(sourceId) or sourceId = 0 then
	return 'ERROR'
end if

of_buildExtFieldList(sourceId)

if ab_historic then
	hist_version_id = a_version_id
	fcst_version_id = 0
else
	hist_version_id = 0
	fcst_version_id = a_version_id
end if

sqls  = "select f.ext_source_id, f.field_id, f.not_indicator, f.lower_value, f.upper_value, f.ext_combo_id "
sqls += "  from reg_ext_combos_fields f, reg_ext_combos_map m"
sqls += " where f.ext_combo_id = m.ext_combo_id "
sqls += "	and f.historic_version_id = m.historic_version_id "
sqls += "	and f.forecast_version_id = m.forecast_version_id "
sqls += "   and f.historic_version_id = " + string(hist_version_id)
sqls += "	and f.forecast_version_id = " + string(fcst_version_id)
sqls += "	and m.reg_acct_id in (" + a_reg_acct_list + ")"

ds_combos_fields = create uo_ds_top		 

rtns = f_create_dynamic_ds(ds_combos_fields, 'grid', sqls, sqlca, true)

if rtns <> 'OK' then
	messagebox("Build DS Combos Fields", "ERROR: Creating ds_combos_fields: " + rtns)
	return ""
end if

ds_combos_fields.SetSort("#6 A, #2 A")
ds_combos_fields.Sort()

prior_combo_id = 0
for i = 1 to ds_combos_fields.rowcount()
	source_id = ds_combos_fields.GetItemNumber(i,1)
	field_id  = ds_combos_fields.GetItemNumber(i,2)
	notInd	 = ds_combos_fields.GetItemNumber(i,3)
	lowerVal  = ds_combos_fields.GetItemString(i,4)
	upperVal  = ds_combos_fields.GetItemString(i,5)
	combo_id  = ds_combos_fields.GetItemNumber(i,6)
	
	if isnull(source_id) then source_id = 0
	if isnull(field_id)  then field_id = 0
	if isnull(notInd) 	then not_ind = 0
	if isnull(lowerVal)  then lowerVal = ""
	if isnull(upperVal)  then upperVal = ""
	if isnull(combo_id)  then combo_id = 0
	
	if field_id = 0 then continue
	if lowerVal = "" and upperVal = "" then continue
	
	if lowerVal <> " " then lowerVal = trim(lowerVal)
	if upperVal <> " " then upperVal = trim(upperVal)
	
	// look up the field
	index = of_getFieldIdx(field_id)
	
	if index = 0 then continue
	
	if i = 1 or where_clause = "" then
		where_clause = "("
	elseif combo_id <> prior_combo_id then 
		where_clause += ") or ("
	else
		where_clause += " and "
	end if
	
	if not_ind = 1 then
		not_str = " not "
	else
		not_str = ""
	end if
	
	if trim(upperVal) = "" then upperVal = lowerVal
	
	where_clause += "to_char(" + i_ls_fields[index].column_name+ ") " + not_str + " between '" + lowerVal + "' and '" + upperVal + "'~r~n"
	
	prior_combo_id = combo_id
next

if isnull(where_clause) or where_clause = "" or where_clause = " and " then 
	where_clause = "ERROR"
else
	where_clause += ")"
end if

destroy ds_combos_fields

return where_clause
end function

public function boolean of_buildledgerbalances (longlong a_histversion, longlong a_fcstversion, longlong a_sourceid, longlong a_company, longlong a_startmonth, longlong a_endmonth);/*******************************************************************
**		Method: Of_buildLedgerBalances
**	
**		Description: for accounts that require balances we must create that from the
**		activity provided. The combo id is removed and it must make sure that accounts
**		with no activity are still included in the balance rollforward.
**
**		Built by Dr. Shane Ward, King of PS, Esquire
*******************************************************************/
string sqls, monthColumn, tableName

//Summarize data from Reg_Ext_Load_Temp

sqls =   "INSERT INTO Reg_Ext_Load_Bal_Temp ~r~n"
sqls += "	(Reg_Company_Id, ~r~n"
sqls += "	 Reg_Acct_Id, ~r~n"
sqls += "	 Historic_Version_Id, ~r~n"
sqls += "	 Forecast_Version_Id, ~r~n"
sqls += "	 MONTH, ~r~n"
sqls += "	 Amount) ~r~n"
sqls += "	SELECT Reg_Company_Id, ~r~n"
sqls += "				 Reg_Acct_Id, ~r~n"
sqls += "				 Historic_Version_Id, ~r~n"
sqls += "				 Forecast_Version_Id, ~r~n"
sqls += "				 MONTH, ~r~n"
sqls += "				 SUM(Amount) ~r~n"
sqls += "	FROM Reg_Ext_Load_Temp t, Reg_Ext_Combos c ~r~n"
sqls += "	WHERE c.Ext_Combo_Id = t.Ext_Combo_Id ~r~n"
sqls += "	AND c.Pull_Type_Id = 2 ~r~n"
sqls += "	GROUP BY Reg_Company_Id, ~r~n"
sqls += "					 Reg_Acct_Id, ~r~n"
sqls += "					 Historic_Version_Id, ~r~n"
sqls += "					 Forecast_Version_Id, ~r~n"
sqls += "					 MONTH ~r~n"

if isNull(sqls) then
	MessageBox("Build Ledger Balances", "ERROR: Summarize data SQL string is empty")
	return false
end if

execute immediate :sqls;

if sqlca.sqlcode <> 0 then
	MessageBox("Build Ledger Balances", "ERROR: Unable to insert into REG_EXT_LOAD_BAL_TEMP: " + Sqlca.sqlerrtext)
	return false
end if

//clear out sqls
sqls = " "

//Prime Starting Balance
sqls =  "INSERT INTO Reg_Ext_Load_Bal_Temp ~r~n"
sqls += "	(Reg_Company_Id, ~r~n"
sqls += "	 Reg_Acct_Id, ~r~n"
sqls += "	 Historic_Version_Id, ~r~n"
sqls += "	 Forecast_Version_Id, ~r~n"
sqls += "	 MONTH, ~r~n" 
sqls += "	 Amount) ~r~n"
sqls += "	SELECT Reg_Company_Id, ~r~n"
sqls += "				 Reg_Acct_Id, ~r~n"
sqls += "				 Historic_Version_Id, ~r~n"
sqls += "				 Forecast_Version_Id, ~r~n"
sqls += "				 MONTH, ~r~n"
sqls += "				 SUM(Amount) ~r~n"
sqls += "	FROM (SELECT l.Reg_Company_Id, ~r~n"
sqls += "							 l.Reg_Acct_Id, ~r~n"
sqls += "							 l.Historic_Version_Id, ~r~n"
sqls += "							 0                     Forecast_Version_Id, ~r~n"
sqls += "							 l.Gl_Month month, ~r~n"
sqls += "							 l.Act_Amount          Amount ~r~n"
sqls += "				FROM Reg_History_Ledger l, ~r~n"
sqls += "						 Reg_Ext_Combos     c, ~r~n"
sqls += "						 Reg_Ext_Combos_Map m, ~r~n"
sqls += "						 Reg_Ext_Source     s ~r~n"
sqls += "				WHERE m.Reg_Acct_Id = l.Reg_Acct_Id ~r~n"
sqls += "				AND m.Historic_Version_Id = l.Historic_Version_Id ~r~n"
sqls += "				AND m.Forecast_Version_Id = 0 ~r~n"
sqls += "				AND c.Ext_Combo_Id = m.Ext_Combo_Id ~r~n"
sqls += "				AND c.Pull_Type_Id = 2 ~r~n"
sqls += "				AND c.Ext_Source_Id = s.Ext_Source_Id ~r~n"
sqls += "				AND s.Reg_Source_Id = " + string(a_sourceId) + " ~r~n"
sqls += "				AND l.Reg_Source_Id = s.Reg_Source_Id ~r~n"
sqls += "				AND l.Gl_Month = ~r~n"
sqls += "							To_Number(To_Char(Add_Months(To_Date("+string(a_startMonth)+", 'YYYYMM'), - 1), ~r~n"
sqls += "														 'YYYYMM')) ~r~n"
sqls += "				AND l.Historic_Version_Id = " + string(a_histVersion) + " ~r~n"
sqls += "				UNION ~r~n"
sqls += "				SELECT l.Reg_Company_Id, ~r~n"
sqls += "							 l.Reg_Acct_Id, ~r~n"
sqls += "							 0 Historic_Version_Id, ~r~n"
sqls += "							 l.Forecast_Version_Id, ~r~n"
sqls += "							 l.Gl_Month month, ~r~n"
sqls += "							 l.Fcst_Amount ~r~n"
sqls += "				FROM Reg_Forecast_Ledger l, ~r~n"
sqls += "						 Reg_Ext_Combos c, ~r~n"
sqls += "						 Reg_Ext_Combos_Map  m, ~r~n"
sqls += "						 Reg_Ext_Source      s ~r~n"
sqls += "				WHERE m.Reg_Acct_Id =l.Reg_Acct_Id ~r~n"
sqls += "				AND m.Historic_Version_Id = 0 ~r~n"
sqls += "				AND m.Forecast_Version_Id = l.Forecast_Version_Id ~r~n"
sqls += "				AND c.Ext_Combo_Id = m.Ext_Combo_Id ~r~n"
sqls += "				AND c.Pull_Type_Id = 2 ~r~n"
sqls += "				AND c.Ext_Source_Id = s.Ext_Source_Id ~r~n"
sqls += "				AND s.Ext_Source_Id = " + string(a_sourceId) + " ~r~n"
sqls += "				AND l.Reg_Source_Id = s.Reg_Source_Id ~r~n"
sqls += "				AND l.Gl_Month = ~r~n"
sqls += "							To_Number(To_Char(Add_Months(To_Date(" + string(a_StartMonth) + ", 'YYYYMM'), -2), ~r~n"
sqls += "														 'YYYYMM')) ~r~n"
sqls += "				AND l.Forecast_Version_Id = "+string(a_fcstVersion)+") t ~r~n"
sqls += "	GROUP BY Reg_Company_Id, ~r~n"
sqls += "					 Reg_Acct_Id, ~r~n"
sqls += "					 Historic_Version_Id, ~r~n"
sqls += "					 Forecast_Version_Id, ~r~n"
sqls += "					 Month ~r~n"


if isNull(sqls) then
	MessageBox("Build Ledger Balances", "ERROR: Prime Starting Balance SQL string is empty")
	return false
end if

execute immediate :sqls;

if sqlca.sqlcode <> 0 then
	MessageBox("Build Ledger Balances", "ERROR: Unable to insert into REG_EXT_LOAD_BAL_TEMP for starting balance: " + Sqlca.sqlerrtext)
	return false
end if

sqls = " "

//Get name of source's MONTH_NUMBER field from Reg_Ext_Source_Field

select column_name, s.table_name 
into :monthColumn, :tableName
from reg_ext_source_field f, reg_ext_source s
where stage_field = 'MONTH_NUMBER'
and s.reg_source_id = :a_sourceId
and f.ext_source_id = s.ext_source_id;

if sqlca.sqlcode <> 0 then
	MessageBox("Build Ledger Balances", "ERROR: Unable to find 'MONTH_NUMBER' stage field for external source("+string(a_sourceId)+") ~r~n"+sqlca.sqlerrText)
	return false
end if

//Add missing months (zero activity)
sqls = "INSERT INTO Reg_Ext_Load_Bal_Temp ~r~n"
sqls += "	(Reg_Company_Id, ~r~n"
sqls += "	 Reg_Acct_Id, ~r~n"
sqls += "	 Historic_Version_Id, ~r~n"
sqls += "	 Forecast_Version_Id, ~r~n"
sqls += "	 MONTH, ~r~n"
sqls += "	 Amount) ~r~n"
sqls += "	SELECT Reg_Company_Id, ~r~n"
sqls += "				 Reg_Acct_Id, ~r~n"
sqls += "				 Historic_Version_Id, ~r~n"
sqls += "				 Forecast_Version_Id, ~r~n"
sqls += "				 mn month, ~r~n"
sqls += "				 0 ~r~n"
sqls += "	FROM (SELECT DISTINCT Reg_Company_Id, ~r~n"
sqls += "												Reg_Acct_Id, ~r~n"
sqls += "												Historic_Version_Id, ~r~n"
sqls += "												Forecast_Version_Id ~r~n"
sqls += "				FROM Reg_Ext_Load_Bal_Temp) a, ~r~n"
sqls += "			 (SELECT To_Number(To_Char(Add_Months(Start_Date, LEVEL - 1), 'YYYYMM')) Mn ~r~n"
sqls += "					FROM (SELECT To_Date('"+string(a_startMonth)+"', 'YYYYMM') Start_Date, ~r~n"
sqls += "										 To_Date('"+string(a_endMonth)+"', 'YYYYMM') End_Date ~r~n"
sqls += "																FROM Dual) ~r~n"
sqls += "CONNECT BY LEVEL <= Months_Between(End_Date, Start_Date) + 1) b ~r~n"
sqls += "	MINUS ~r~n"
sqls += "	SELECT Reg_Company_Id, ~r~n"
sqls += "				 Reg_Acct_Id, ~r~n"
sqls += "				 Historic_Version_Id, ~r~n"
sqls += "				 Forecast_Version_Id, ~r~n"
sqls += "				 MONTH, ~r~n"
sqls += "				 0 ~r~n"
sqls += "	FROM Reg_Ext_Load_Bal_Temp ~r~n"

if isNull(sqls) then
	MessageBox("Build Ledger Balances", "ERROR: Add missing months SQL string is empty")
	return false
end if

execute immediate :sqls;

if sqlca.sqlcode <> 0 then
	MessageBox("Build Ledger Balances", "ERROR: Unable to insert into REG_EXT_LOAD_BAL_TEMP for missing months: " + Sqlca.sqlerrtext)
	return false
end if



return true
end function

on uo_reg_ext_source_integration.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_reg_ext_source_integration.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

