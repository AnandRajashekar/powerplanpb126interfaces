HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'pp_integration.exe'
string i_batch[], i_args[]

longlong i_batch_cnt, i_args_cnt

longlong i_ppint_process_id, i_prior_process_id, i_prior_occurrence_id, i_prior_msg_order, i_session_id
string i_process_descr, i_exe_version
end variables

forward prototypes
public function longlong uf_read ()
public function string uf_get_arguments_resolve (string a_arg)
public subroutine uf_get_arguments (longlong a_num_args, longlong a_row, uo_ds_top a_ds_components, ref string a_args[])
public function any uf_cast (string a_str, string a_datatype)
protected function longlong uf_setup_batch (string a_batch, string a_start_stop)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN, INC., ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read (pp_integration)
//
//   Purpose  :  Wrapper interface to call various base components via one interface.
//					This interface takes in a batch name via a command line argument
//					that is then looked up in pp_integration and the individual components 
//					are then run in the defined order.  
//
//	The following fields define the process in pp_integration for a batch:
//		Priority					The order the calls are made in ascending order
//		Component				The supported component listed below
//		Argument1				Either a hardcoded value, SQL to lookup a value, or <arg1> etc keyword to pull from command line arguments
//		...							"
//		Argument10				"
//
//	Command Line Argument:
//		batch,batch2,batch3|arg1|arg2|arg3
//			* Multiple batches can be run at once by passing them in comma seperated as argument 1
//			* Arguments can be passed in after the batch(es) seperated by pipes |
//
//	Currently supported:
//		Component 				Launches																		Arguments
//		Data Mover Batch		uo_cr_data_mover.uf_run_data_mover									1	(string)
//		Data Mover Where		uo_cr_data_mover.uf_run_data_mover_where_clause			1	(string)	
//		Create Work Order	uo_wo_interface.uf_create_wos												2	(long, string)
//		Create Job Task		uo_job_task_interface.uf_create_job_task								2	(long, string)
//		OCR Process			uo_original_cost_retirement.uf_process_ocr								1	(string)
//		OCR Process Est		uo_original_cost_retirement.uf_process_ocr_from_estimate		1	(long)
//		Load Monthly Estimates	uo_wo_interface.uf_load_monthly_est									5	(long, boolean, string, boolean, long)				
//		PP Translate			uo_pp_translate.uf_process													1	(long)
//		CR Transactions		uo_cr_txn_api.of_read															2	(string, long)  (assumes 3rd-5th argument are true)
//																													or 		3	(string, long, boolean)	(assumes 4th & 5th arguments are true)
//																													or		5	(string, long, boolean, boolean, boolean)
//		Create Funding Project	uo_wo_interface.uf_create_fps               								2	(long,string)
//		Reg External Source	uo_reg_ext_source_integration.of_loadRegLedger						6  (long, long, long, long, long, long)
//
//
//  DATE			NAME				REVISION		CHANGES
//  ------			-------				------------		------------
//  03/09/2012	Joseph King		1.0				Initial Version
//
//  PROPRIETARY INFORMATION OF POWERPLAN, INC., ALL RIGHTS RESERVED 
//*****************************************************************************************
longlong batch_count, i, j, component_cnt, rtn, component_args_cnt, k, long_array[]
uo_ds_top ds_components
string sqls, component, component_args[]
/*###CWB PC3085 Add File Import Component*/
uo_pp_int_imp_exp_files uo_int_files

// Capture original logging information.  As we switch batches, will be changing the process / occurrence etc.
i_prior_process_id = g_process_id
i_ppint_process_id = g_process_id
i_prior_occurrence_id = g_occurrence_id
i_prior_msg_order = g_msg_order

select version into :i_exe_version 
	from pp_processes_occurrences
	where process_id = :g_process_id
	and occurrence_id = :g_occurrence_id;

select description into :i_process_descr
	from pp_processes where process_id = :g_process_id;
	
if trim(i_process_descr) = '' or isnull(i_process_descr) then
	i_process_descr = 'PP Integration'
end if

// Declare components
uo_cr_data_mover uo_cr_dm
uo_wo_interface uo_wo
uo_job_task_interface uo_jt
uo_mx_cu uo_cu
uo_mx_location uo_loc
uo_original_cost_retirement uo_ocr
uo_pp_translate uo_pp_trans
uo_cr_txn_api uo_cr_txn_api_l
uo_reg_ext_source_integration uo_reg


if g_ssp then
	// then ignore g_command_line_args and rebuild from SSP
	g_command_line_args = ''
	
	if upperbound(g_ssp_parms.string_arg[]) = 0 then g_ssp_parms.string_arg[1] = ''
	
	if upper(g_ssp_labels.string_label) = 'COMPONENT' or mid(upper(g_ssp_parms.string_arg[1]),1,10) = 'COMPONENT-' then
		// if upper(g_ssp_labels.string_label) = 'COMPONENT' then
		// Batch using keyword COMPONENTONLY
		for i = 1 to upperbound(g_ssp_parms.string_arg)
			if mid(upper(g_ssp_parms.string_arg[i]),1,10) = 'COMPONENT-' then
				// already formatted correctly.
			else
				g_ssp_parms.string_arg[i] = 'COMPONENT-' + g_ssp_parms.string_arg[i]
			end if
		next
		
		g_command_line_args = f_parsestringarrayintostring(g_ssp_parms.string_arg)
		
		// args from SSP
		if upperbound(g_ssp_parms.string_arg2) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg2)
		if upperbound(g_ssp_parms.string_arg3) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg3)
		if upperbound(g_ssp_parms.string_arg4) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg4)
		if upperbound(g_ssp_parms.string_arg5) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg5)
		if upperbound(g_ssp_parms.string_arg6) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg6)
		if upperbound(g_ssp_parms.string_arg7) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg7)
		if upperbound(g_ssp_parms.string_arg8) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg8)
		if upperbound(g_ssp_parms.string_arg9) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg9)
		if upperbound(g_ssp_parms.string_arg10) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg10)
		if upperbound(g_ssp_parms.string_arg11) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg11)
	else    
		// They are providing batches... use those first.
		g_command_line_args = f_parsestringarrayintostring(g_ssp_parms.string_arg)

		// For args
		if upperbound(g_ssp_parms.string_arg2) >= 1 then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg2)
		if upperbound(g_ssp_parms.string_arg3) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg3)
		if upperbound(g_ssp_parms.string_arg4) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg4)
		if upperbound(g_ssp_parms.string_arg5) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg5)
		if upperbound(g_ssp_parms.string_arg6) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg6)
		if upperbound(g_ssp_parms.string_arg7) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg7)
		if upperbound(g_ssp_parms.string_arg8) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg8)
		if upperbound(g_ssp_parms.string_arg9) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg9)
		if upperbound(g_ssp_parms.string_arg10) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg10)
		if upperbound(g_ssp_parms.string_arg11) >= 1  then g_command_line_args += '|' + f_parsestringarrayintostring(g_ssp_parms.string_arg11)
		
	end if
end if


// Translate command line parameters.
//	Command Line Argument:
//		batch,batch2,batch3|arg1|arg2|arg3
//			* Multiple batches can be run at once by passing them in comma seperated as argument 1
//			* Arguments can be passed in after the batch(es) seperated by pipes |
g_command_line_args = trim(g_command_line_args)

if trim(g_command_line_args) = '' or isnull(g_command_line_args) then
	g_rte.msg(-1,'Command line argument is required.')
	throw g_rte
end if

if pos(g_command_line_args,'|') > 0 then
	// We have arguments as well
	i_batch[1] = mid(g_command_line_args,1,pos(g_command_line_args,'|') - 1)
	g_command_line_args = mid(g_command_line_args,pos(g_command_line_args,'|') + 1)
	
	i_batch_cnt = 	f_parsestringintostringarray(i_batch[1],',',i_batch)
	i_args_cnt = 	f_parsestringintostringarray(g_command_line_args,'|',i_args)
else
	// We have no arguments
	i_batch_cnt = 	f_parsestringintostringarray(g_command_line_args,',',i_batch)
end if

if i_batch_cnt = 0 then
	g_rte.msg(-1,'Command line argument is required.')
	throw g_rte
end if

// Process the batches one at a time
for i = 1 to i_batch_cnt	
	// Switch to a seperate process.
	// all errors handled via throws...
	uf_setup_batch(i_batch[i], 'START')
	
	if i_batch_cnt > 1 then
		f_pp_msgs("Starting batch " + string(i) + " of " + string(i_batch_cnt) + ": '" + i_batch[i] + "'")
	else
		f_pp_msgs("Starting batch: '" + i_batch[i] + "'")
	end if
	
	if mid(i_batch[i],1,10) = 'COMPONENT-' then
		sqls = "select 1 priority, '" + mid(i_batch[i],11) + "' component, '<arg1>' argument1, '<arg2>' argument2, '<arg3>' argument3, '<arg4>' argument4, '<arg5>' argument5,"
		sqls += "'<arg6>' argument6, '<arg7>' argument7, '<arg8>' argument8, '<arg9>' argument9, '<arg10>' argument10 from dual"
	else
		sqls = "select priority, component, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9, argument10  from pp_integration"
		sqls += " where batch = '" + i_batch[i] + "' and nvl(status,1) = 1 order by priority"
	end if
	
	if i = 1 then
		ds_components = create uo_ds_top
		f_create_dynamic_ds(ds_components,'grid',sqls,sqlca,true)
	else
		ds_components.SetSQLSelect(sqls)
		ds_components.Retrieve()
	end if		
		
	if ds_components.i_sqlca_SQLCode < 0 then
		g_rte.msg(-1,'Retrieving component list.',{ds_components.i_sqlca_SQLErrText})
		throw g_rte
	end if
	
	component_cnt = ds_components.rowcount()
	if component_cnt = 0 then
		f_pp_msgs("-- No record found in pp_integration for batch '" + i_batch[i] + "'")
		f_pp_msgs('-- Continuing to the next batch.')
		f_pp_msgs(" ")
		continue
	end if
	
	// Process the components one at a time
	for j = 1 to component_cnt
		component = ds_components.GetItemString(j,'component')
		f_pp_msgs("-- Starting component '" + component + "'")
		choose case lower(trim(component))
			case 'data mover batch'
				//		Data Mover Batch		uo_cr_data_mover.uf_run_data_mover							1	(string)
				if not isvalid(uo_cr_dm) then uo_cr_dm = create uo_cr_data_mover
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_cr_dm.uf_run_data_mover(component_args[1])
				
			case 'data mover where'
				//		Data Mover Where	uo_cr_data_mover.uf_run_data_mover_where_clause		1	(string)
				if not isvalid(uo_cr_dm) then uo_cr_dm = create uo_cr_data_mover
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_cr_dm.uf_run_data_mover_where_clause(component_args[1])
				
			case 'create work order'
				//		Create Work Order	uo_wo_interface.uf_create_wos									2	(long, string)
				if not isvalid(uo_wo) then uo_wo = create uo_wo_interface
				uf_get_arguments(2,j,ds_components,component_args)
				rtn = uo_wo.uf_create_wos(uf_cast(component_args[1],'long'),component_args[2])
				
			case 'create job task'
				//		Create Job Task		uo_job_task_interface.uf_create_job_task						2	(long, string)
				if not isvalid(uo_jt) then uo_jt = create uo_job_task_interface
				uf_get_arguments(2,j,ds_components,component_args)
				rtn = uo_jt.uf_create_job_task(uf_cast(component_args[1],'long'),component_args[2])
				
			case 'create mx cu'
				//		Create MX CU		uo_mx_cu.uf_load_cu						1 (string)
				if not isvalid(uo_cu) then uo_cu = create uo_mx_cu
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_cu.uf_load_cu(component_args[1])
				
			case 'create mx locations'
				//		Create MX Locations		uo_mx_location.uf_load_				1 (string)
				if not isvalid(uo_loc) then uo_loc = create uo_mx_location
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_loc.uf_load_location(component_args[1])
				
			case 'load unit est'
				//		Load Unit Estimates	uo_wo_interface.uf_load_unit_est									4	(long, boolean, boolean, string)
				if not isvalid(uo_wo) then uo_wo = create uo_wo_interface
				uf_get_arguments(4,j,ds_components,component_args)
				rtn = uo_wo.uf_load_unit_est(uf_cast(component_args[1],'long'),uf_cast(component_args[2],'boolean'),uf_cast(component_args[3],'boolean'),component_args[4])
			
			case 'ocr process'
				//		OCR Process			uo_original_cost_retirement.uf_process_ocr					1	(string)
				if not isvalid(uo_ocr) then uo_ocr = create uo_original_cost_retirement
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_ocr.uf_process_ocr(component_args[1])
				
			case 'ocr process est'
				//		OCR Process Est		uo_original_cost_retirement.uf_process_ocr_from_estimate			1	(long)
				if not isvalid(uo_ocr) then uo_ocr = create uo_original_cost_retirement
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_ocr.uf_process_ocr_from_estimate(uf_cast(component_args[1],'long'))
			
			case 'load monthly estimates'
				//		Load Monthly Estimates	uo_wo_interface.uf_load_monthly_est							5	(long, boolean, string, boolean, long)
				if not isvalid(uo_wo) then uo_wo = create uo_wo_interface
				uf_get_arguments(5,j,ds_components,component_args)
				rtn = uo_wo.uf_load_monthly_est(uf_cast(component_args[1],'long'),uf_cast(component_args[2],'boolean'),component_args[3],uf_cast(component_args[4],'boolean'),uf_cast(component_args[5],'long'))
				
			case 'pp translate'
				//		PP Translate			uo_pp_translate.uf_process						1	(long)
				if not isvalid(uo_pp_trans) then uo_pp_trans = create uo_pp_translate
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_pp_trans.uf_process(uf_cast(component_args[1],'long'))
				
			case 'cr transactions'
				//		CR Transactions		uo_cr_txn_api.of_read												2	(string, long)
				if not isvalid(uo_cr_txn_api_l) then uo_cr_txn_api_l = create uo_cr_txn_api
				uf_get_arguments(5,j,ds_components,component_args)
				
				// Default argument 3-5 if it's not a valid value.
				// Balanced entries to TRUE
				if upper(trim(component_args[3])) = 'NO' or upper(trim(component_args[3])) = '0' or upper(trim(component_args[3])) = 'FALSE' or upper(trim(component_args[3])) = 'F' then
					component_args[3] = 'FALSE'
				else
					component_args[3] = 'TRUE'
				end if
				
				// CCS to TRUE
				if upper(trim(component_args[4])) = 'NO' or upper(trim(component_args[4])) = '0' or upper(trim(component_args[4])) = 'FALSE' or upper(trim(component_args[4])) = 'F' then
					component_args[4] = 'FALSE'
				else
					component_args[4] = 'TRUE'
				end if
				
				// Post to GL to TRUE
				if upper(trim(component_args[5])) = 'NO' or upper(trim(component_args[5])) = '0' or upper(trim(component_args[5])) = 'FALSE' or upper(trim(component_args[5])) = 'F' then
					component_args[5] = 'FALSE'
				else
					component_args[5] = 'TRUE'
				end if
				
				rtn = uo_cr_txn_api_l.of_read(component_args[1], uf_cast(component_args[2],'long'), uf_cast(component_args[3],'boolean'), uf_cast(component_args[4],'boolean'), uf_cast(component_args[5],'boolean'))

			case 'create funding project'
				//		Create Funding Project	uo_wo_interface.uf_create_fps							2	(long, string)
				if not isvalid(uo_wo) then uo_wo = create uo_wo_interface
				uf_get_arguments(2,j,ds_components,component_args)
				rtn = uo_wo.uf_create_fps(uf_cast(component_args[1],'long'),component_args[2])
				
			case 'reg external source'
				//		Load Reg External Source	uo_reg.of_LoadRegLedger				6 (long, long, long, long, long, long)
				if not isValid (uo_reg) then uo_reg = create uo_reg_ext_source_integration
				uf_get_arguments (6, j, ds_components, component_args)
				if not uo_reg.of_loadRegLedger(uf_cast(component_args[1], 'long'), uf_cast(component_args[2], 'long'), uf_cast(component_args[3], 'long') ,+&
															uf_cast(component_args[4], 'long'), uf_cast(component_args[5], 'long'), uf_cast(component_args[6], 'long')) then
					rtn = -1 
				end if
 			/*###CWB PC3085 Add File Import Calls*/
			case 'pp text file import'
				if not isvalid(uo_int_files) then uo_int_files = create uo_pp_int_imp_exp_files
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_int_files.of_import_file(component_args[1])
			case 'pp text file export'
				if not isvalid(uo_int_files) then uo_int_files = create uo_pp_int_imp_exp_files
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_int_files.of_export_file(component_args[1])	
			case 'pp xml import'
				if not isvalid(uo_int_files) then uo_int_files = create uo_pp_int_imp_exp_files
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_int_files.of_import_xml(component_args[1])
			case 'pp xml export'
				if not isvalid(uo_int_files) then uo_int_files = create uo_pp_int_imp_exp_files
				uf_get_arguments(1,j,ds_components,component_args)
				rtn = uo_int_files.of_export_xml(component_args[1])
 

			case else
				g_rte.msg(-1,"Unknown component '" + component + "'")
				throw g_rte
		end choose
		
		if rtn < 0 then
			g_rte.msg(rtn,"During component '" + component + "'")
			throw g_rte
		end if
		f_pp_msgs(" ")
	next			
next

commit;

return 0
end function

public function string uf_get_arguments_resolve (string a_arg);//		Argument1				Either a hardcoded value, SQL to lookup a value, or <arg1> etc keyword to pull from command line arguments
//		...							"
//		Argument10				"
any value[]
longlong j, arg_num
string arg_str

if mid(lower(a_arg),1,4) = '<arg' and right(a_arg,1) = '>' then
	arg_str = mid(a_arg,5,2)
	if right(arg_str,1) = '>' then arg_str = mid(arg_str,1,1)
	if isnumber(arg_str) then 
		arg_num = long(arg_str)
	else
		g_rte.msg(-1,"Unable to determine argument number from '" + a_arg + "'")
		throw g_rte
	end if
	
	if arg_num > i_args_cnt then
		g_rte.msg(-1,"Argument number from '" + a_arg + "' exceeds the number of command arguments")
		throw g_rte
	else
		return i_args[arg_num]
	end if
elseif pos(upper(a_arg),'SELECT') > 0 then
	// Scalar select...go get the value
	f_get_column(value,a_arg)
	
	if SQLCA.SQLCode < 0 then
		g_rte.msg(-1,"Resolving argument '" + a_arg + "'")
		throw g_rte
	end if
	
	a_arg = ''
	for j = 1 to upperbound(value)
		a_arg += string(value[j]) + '|'
	next
	a_arg = mid(a_arg,1, len(a_arg) - 1)
end if

return a_arg
end function

public subroutine uf_get_arguments (longlong a_num_args, longlong a_row, uo_ds_top a_ds_components, ref string a_args[]);string null_array[]
longlong i

if a_row > a_ds_components.rowcount() then
	g_rte.msg("Row greater than the number of components being processed!")
	throw g_rte
end if

a_args = null_array

if a_num_args > 10 then a_num_args = 10
for i = 1 to a_num_args
	a_args[i] = a_ds_components.GetItemString(a_row,'argument' + string(i))
	a_args[i] = uf_get_arguments_resolve(a_args[i])
	f_pp_msgs('---- Argument' + string(i) + ':   ' + a_args[i])
next

end subroutine

public function any uf_cast (string a_str, string a_datatype);choose case lower(a_datatype)
	case 'boolean'
		if upper(trim(a_str)) = 'YES' or upper(trim(a_str)) = '1' or upper(trim(a_str)) = 'TRUE' or upper(trim(a_str)) = 'T' then
			return true
		elseif upper(trim(a_str)) = 'NO' or upper(trim(a_str)) = '0' or upper(trim(a_str)) = 'FALSE' or upper(trim(a_str)) = 'F' then
			return false
		else
			g_rte.msg(-1,"Unable to cast argument '" + a_str + "' to " + a_datatype)
			throw g_rte
		end if
			
	case 'long','int'
		if isnumber(a_str) then
			return long(a_str)
		else
			g_rte.msg(-1,"Unable to cast argument '" + a_str + "' to " + a_datatype)
			throw g_rte
		end if
		
	case'double','decimal'
		if isnumber(a_str) then
			return double(a_str)
		else
			g_rte.msg(-1,"Unable to cast argument '" + a_str + "' to " + a_datatype)
			throw g_rte
		end if
		
	case'date'
		if isdate(a_str) then
			return date(a_str)
		else
			g_rte.msg(-1,"Unable to cast argument '" + a_str + "' to " + a_datatype)
			throw g_rte
		end if
		
	case 'string'
		return a_str
		
	case else
		g_rte.msg(-1,"Unrecognized datatype '" + a_datatype + "'")
		
end choose
end function

protected function longlong uf_setup_batch (string a_batch, string a_start_stop);longlong process_id, running_session_id, occurrence_id

// If running a single component, don't create a seperate log
a_batch = f_replace_string(a_batch,'COMPONENT-','COMP-','first')

// Starting a new batch.  Switch to that batch
a_start_stop = upper(trim(a_start_stop))	

if isnull(i_session_id) or i_session_id = 0 then
	select userenv('sessionid') into :i_session_id from dual;
end if

if a_start_stop = 'START' then
	// Go find the new process_id for the batch
	select process_id
		into :process_id
		from pp_processes
		where executable_file = 'pp_integration.exe ' || :a_batch
		using g_sqlca_logs;
		
	if g_sqlca_logs.sqlnrows = 0 then
		// need to create a new pp_processes record
		//	- get the new process id first.
		select max(process_id) into :process_id from pp_processes using g_sqlca_logs;
		if isnull(process_id) then process_id = 0
		process_id++
		
		insert into pp_processes
			(process_id, description, long_description, executable_file, allow_concurrent)
			values
			(:process_id, substr(:i_process_descr || ': ' || :a_batch,1,35), substr(:i_process_descr || ': ' || :a_batch,1,254), 
				'pp_integration.exe ' || :a_batch, 0)
			using g_sqlca_logs;
		
		if g_sqlca_logs.SQLCode < 0 then
			g_rte.msg(-1,'Creating new pp_processes record.',{i_process_descr + ': ' + a_batch,g_sqlca_logs.sqlerrtext})
			throw g_rte
		end if
		
		insert into pp_processes_return_values
			(process_id, return_value, description, long_description, os_return_value, email_recipients, email_subject, email_body)
			select :process_id, return_value, description, long_description, os_return_value, email_recipients, email_subject, email_body
			from pp_processes_return_values
			where process_id = :i_ppint_process_id
			using g_sqlca_logs;
		
		if g_sqlca_logs.SQLCode < 0 then
			g_rte.msg(-1,'Creating new pp_processes_return_values records.',{i_process_descr + ': ' + a_batch,g_sqlca_logs.sqlerrtext})
			throw g_rte
		else
			commit using g_sqlca_logs;
		end if
	end if

	if i_prior_process_id <> i_ppint_process_id then
		// Switching from one batch to another.  Need to shut down the logs for the first batch
		//	before starting up the next.
		update pp_processes set running_session_id = null
		where process_id = :g_process_id
		and running_session_id = :i_session_id
		using g_sqlca_logs;
				
		f_pp_msgs_end_log()
		
		// Log a message before we switch to the new indicating the process continues with another session.
		select max(occurrence_id) into :occurrence_id
			from pp_processes_occurrences
			where process_id = :process_id using g_sqlca_logs;
		if isnull(occurrence_id) then occurrence_id = 0
		occurrence_id++
		
		f_pp_msgs("Logs continue under process '" + i_process_descr + ': ' + a_batch + "' / occurrence " + string(occurrence_id) + ".  Overall returns are handled by that occurrence.")
		f_pp_msgs("")
	end if
	
	// Found the process_id, now get the occurrence_id
	//	f_pp_msgs_start_log sets g_process_id, g_msg_order, g_occurrence_id
	f_pp_msgs_start_log(process_id)
	
	// Log the exe version
	if trim(i_exe_version) <> '' and not isnull(i_exe_version) then
		update pp_processes_occurrences
			set version = :i_exe_version 
			where process_id = :g_process_id
			and occurrence_id = :g_occurrence_id
		using g_sqlca_logs;
	end if
	
	// Copy the header logs from the old process to the new process.
	insert into pp_processes_messages
		(process_id, occurrence_id, msg, msg_order)
		select :g_process_id, :g_occurrence_id, msg, msg_order
		from pp_processes_messages 
		where process_id = :i_prior_process_id
		and occurrence_id = :i_prior_occurrence_id
		and msg_order < :i_prior_msg_order
	using g_sqlca_logs;
		
	if g_sqlca_logs.SQLCode < 0 then
		g_rte.msg(-1,'Copying header logs to new occurrence',{g_sqlca_logs.SQLErrText})
		throw g_rte
	else
		commit using g_sqlca_logs;
		g_msg_order = i_prior_msg_order + 1
	end if

	// if this is the first batch, the old process is the raw pp_integration.exe.  Those logs are useless now so delete them.
	if i_prior_process_id = i_ppint_process_id then
		// Delete the occurrence from the old process id.
		delete from pp_processes_messages 
			where process_id = :i_prior_process_id
			and occurrence_id = :i_prior_occurrence_id
			using g_sqlca_logs;
		
		if g_sqlca_logs.SQLCode < 0 then
			g_rte.msg(-1,'Deleting occurrence from the non-batch specific pp_integration.exe for pp_processes_messages',{g_sqlca_logs.SQLErrText})
			throw g_rte
		end if	
		
		delete from pp_processes_occurrences 
			where process_id = :i_prior_process_id
			and occurrence_id = :i_prior_occurrence_id
			using g_sqlca_logs;
			
		if g_sqlca_logs.SQLCode < 0 then
			g_rte.msg(-1,'Deleting occurrence from the non-batch specific pp_integration.exe for pp_processes_occurrences',{g_sqlca_logs.SQLErrText})
			throw g_rte
		else
			commit using g_sqlca_logs;
		end if
	end if
	
	// Check the running session id now.
	select running_session_id into :running_session_id from pp_processes
		where process_id = :g_process_id
		for update wait 60
		using g_sqlca_logs;
	
	if isnull(running_session_id) or running_session_id = 0 then
		//  NOT CURRENTLY RUNNING:  update the record with this session_id.		
		update pp_processes 
		set running_session_id = :i_session_id
		where process_id = :g_process_id
		using g_sqlca_logs;
		
		if g_sqlca_logs.SQLCode < 0 then
			g_rte.msg(-1,'updating pp_processes.running_session_id',{g_sqlca_logs.SQLErrText})
			throw g_rte
		else
			commit using g_sqlca_logs;
		end if			
	else
		g_rte.msg(-1,'Another session is currently running this process.',{i_process_descr + ': ' + a_batch,"Session ID = " + string(running_session_id)})
		throw g_rte
	end if
	
	i_prior_process_id = g_process_id
	i_prior_occurrence_id = g_occurrence_id
	
	// If running SSP, need to update the process id and occurrence id on PP_JOB_REQUEST
	if g_ssp then
		update PP_JOB_REQUEST set
			process_id = :g_process_id,
			occurrence_id = :g_occurrence_id
		where process_identifier = :g_ssp_process_identifier
		and status = 'I'
		and request_time >= (sysdate - .25)
		using g_sqlca_logs;
		
		if g_sqlca_logs.SQLCode < 0 then
			g_rte.msg(-1,'updating pp_job_request.process and occurrence id',{g_sqlca_logs.SQLErrText})
			throw g_rte
		else
			commit using g_sqlca_logs;
		end if		
	end if
end if

return 0
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_pp_integration_custom_version nvo_pp_integration_custom_version
nvo_ppprojct_custom_version nvo_ppprojct_custom_version
nvo_ppsystem_interface_custom_version nvo_ppsystem_interface_custom_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'pp_integration_custom.pbd'
		return nvo_pp_integration_custom_version.custom_version
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
	case 'ppsystem_interface_custom.pbd'
		return nvo_ppsystem_interface_custom_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

