HA$PBExportHeader$w_logview.srw
forward
global type w_logview from w_top
end type
type cb_refresh from commandbutton within w_logview
end type
type cb_close from commandbutton within w_logview
end type
type dw_logview from uo_dw_top within w_logview
end type
type ws_log_job_no from structure within w_logview
end type
end forward

type ws_log_job_no from structure
	string		log_no
	longlong	job_no
end type

global type w_logview from w_top
integer width = 4599
integer height = 1992
string title = "Log View"
long backcolor = 12632256
string icon = "Report5!"
event ue_stop pbm_custom02
cb_refresh cb_refresh
cb_close cb_close
dw_logview dw_logview
end type
global w_logview w_logview

type variables
longlong i_orig_width
longlong i_orig_height
longlong i_orig_dw_width
longlong i_orig_dw_height
longlong i_cb_close_x
longlong i_cb_close_y
longlong i_cb_refresh_x 
longlong i_cb_refresh_y 
longlong i_cb_job_status_x 
longlong i_cb_job_status_y 
longlong i_cb_kill_job_x 
longlong i_cb_kill_job_y 
longlong i_log_no
longlong i_job_no
longlong i_hwnd

boolean ib_sizewindow
end variables

event ue_stop;halt

end event

on w_logview.create
int iCurrent
call super::create
this.cb_refresh=create cb_refresh
this.cb_close=create cb_close
this.dw_logview=create dw_logview
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_refresh
this.Control[iCurrent+2]=this.cb_close
this.Control[iCurrent+3]=this.dw_logview
end on

on w_logview.destroy
call super::destroy
destroy(this.cb_refresh)
destroy(this.cb_close)
destroy(this.dw_logview)
end on

event open;call super::open;i_orig_width =   4622//    this.width
i_orig_height =  1924// this.height
i_orig_dw_width = 4475 //dw_logview.width 
i_orig_dw_height = 1684 - 200  //dw_logview.height 
ib_sizewindow = false
i_cb_close_x = 4142 // cb_close.x
i_cb_close_y = 1548 // cb_close.y 

i_cb_refresh_x   = cb_refresh.x 
i_cb_refresh_y   = 1548 //cb_refresh.y 

string msg
longlong pos
msg =  message.stringparm
pos = pos(msg,',')
if pos > 0 then
    i_log_no = long(mid(msg,1,pos -1))
	i_job_no = long(mid(msg,pos + 1))
	
else
	 i_log_no = 0
	i_job_no =0
end if



dw_logview.settransobject(sqlca)
dw_logview.retrieve(i_log_no)
longlong row
row = dw_logview.rowcount()
dw_logview.scrolltorow(row)
timer(3)


end event

event resize;
longlong diff_width
longlong diff_height
string logno
diff_width = this.width - i_orig_width 
diff_height = this.height -  i_orig_height 

dw_logview.width = long(i_orig_dw_width + diff_width)
dw_logview.height = long(i_orig_dw_height + diff_height)

cb_close.x = long(i_cb_close_x + diff_width)
cb_close.y = long(i_cb_close_y + diff_height)

//cb_refresh.x = i_cb_refresh_x   + diff_width
cb_refresh.y = long(i_cb_refresh_y   + diff_height)
end event

event timer;call super::timer;

longlong row
string msg

dw_logview.setredraw( false )
row = dw_logview.retrieve( i_log_no )
dw_logview.scrolltorow( row )
dw_logview.setredraw( true )

msg= dw_logview.getitemstring( row ,'msg')

if pos(msg, 'Finished') > 0  then
	select hwnd into :i_hwnd from tax_job_params where job_no = :i_log_no and rownum = 1;
	send(i_hwnd,1025,1,0)
	
	timer(0)
end if

end event

type cb_refresh from commandbutton within w_logview
integer x = 59
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Refresh"
end type

event clicked;

dw_logview.retrieve(i_log_no)

longlong row
row = dw_logview.rowcount()
dw_logview.scrolltorow(row)

end event

type cb_close from commandbutton within w_logview
integer x = 4091
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Close"
end type

event clicked;close(parent)
end event

type dw_logview from uo_dw_top within w_logview
integer x = 46
integer y = 40
integer width = 4439
integer height = 1684
integer taborder = 10
string dataobject = "dw_tax_processing_log_grid"
boolean hscrollbar = true
boolean vscrollbar = true
end type

