HA$PBExportHeader$w_top.srw
forward
global type w_top from window
end type
end forward

global type w_top from window
integer width = 3584
integer height = 1656
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
end type
global w_top w_top

on w_top.create
end on

on w_top.destroy
end on

