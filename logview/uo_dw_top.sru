HA$PBExportHeader$uo_dw_top.sru
forward
global type uo_dw_top from datawindow
end type
end forward

global type uo_dw_top from datawindow
integer width = 1879
integer height = 1060
integer taborder = 1
boolean livescroll = true
event ue_save_as pbm_custom64
event ue_mail pbm_custom65
event ue_mousemove pbm_dwnmousemove
event ue_dropdown pbm_dwndropdown
event ue_mouse_down pbm_lbuttondown
event ue_paste_to_excel pbm_custom52
end type
global uo_dw_top uo_dw_top

type variables
uo_ps_interface u_ps_interface
m_popup i_m_popup
string is_colname


string i_default_color
longlong i_mouse_down_y  = 0
longlong i_mouse_down_x  = 0
longlong i_mouse_up_y  = 0
longlong i_mouse_up_x = 0
boolean i_mouse_down = false

string i_selected_columns[]
longlong i_mouse_down_row = 0
string i_mouse_down_column = ''
longlong i_mouse_up_row = 0
string i_mouse_up_column = ''
longlong i_start_selected_row
longlong i_end_selected_row
end variables

event ue_save_as;string opt
setpointer(hourglass!)
u_ps_interface.redraw=false
setredraw(false)
opt = this.object.DataWindow.Print.Preview
u_ps_interface.uf_save_as(this)
this.object.DataWindow.Print.Preview = opt
setredraw(true)
u_ps_interface.redraw=true
end event

event ue_mail;//string opt
//setpointer(hourglass!)
//u_ps_interface.redraw=false
//setredraw(false)
//opt = this.object.DataWindow.Print.Preview
//u_ps_interface.uf_mail_print(this,"Report")
//this.object.DataWindow.Print.Preview = opt
//setredraw(true)
//u_ps_interface.redraw=true
//
//
//
end event

event ue_mousemove;integer li_X, li_Y, li_W, li_H
string ls_Text



Return 0
end event

event ue_mouse_down;STRING ls_create 
string str
longlong x,y,i,pos
longlong count
longlong status
longlong startx, endx
longlong starty,endy
longlong start_row,end_row
string cols[]
longlong select_row
string start_col
string end_col


return 0

end event

event ue_paste_to_excel;
//uo_paste_to_excel uo_paste_to_excel 

//if upperbound(i_selected_columns) < 1 then
//	MessageBox("Warning","Please select rows.")
//	return
//end if
//uo_paste_to_excel.uf_export_to_excel(this,i_selected_columns,i_start_selected_row,i_end_selected_row)
//
return 0
end event

event rbuttondown;

message.PowerObjectParm = this


i_m_popup.PopMenu(w_logview.PointerX(),w_logview.PointerY())


end event

event constructor;u_ps_interface = create uo_ps_interface
i_m_popup = create m_popup
i_m_popup.i_object = this
if u_ps_interface.i_initialize_dll = true then
  i_m_popup.item[5].visible   = TRUE
  i_m_popup.item[6].visible   = TRUE

end if


return
end event

on uo_dw_top.create
end on

on uo_dw_top.destroy
end on

event doubleclicked;string band
string ctype
longlong posX,PosX2
longlong pos,start_pos,colpos
string col_name,col_type,objects
string prev_sort
string data_type,str
longlong num
date d
datetime dt

if dwo.Type <> 'text' then
	return
end if
 
if isnull(dwo) or not isvalid(dwo) then
	 return
end if
band = dwo.band
If dwo.Type = "text"  and dwo.band = 'header' Then
    posX = long(dwo.x)
	 PosX2 = long(dwo.x) + long(dwo.width)
	 objects = this.describe( "datawindow.objects")
	 pos = 1
	 start_pos = 1
	 do while pos > 0
			pos = pos(objects,"~t",start_pos)
			if pos = 0 then
				 col_name = mid(objects,start_pos)
			else 
				 col_name = mid(objects,start_pos,pos - start_pos)
			end if
			start_pos = pos + 1
			col_type = this.describe(col_name + ".type")
			if col_type = 'column' or col_type = 'compute' then
				colpos = long(this.describe(col_name + ".x"))
				if colpos >= posX and colpos <= PosX2 then
					tag = this.describe(col_name + ".tag")
					if isnull(tag) or tag = '' then
						tag = 'a'
					end if
					if Keydown(KeyControl!) then
						prev_sort = describe("DataWindow.Table.sort")
						if len(prev_sort) > 0 then
							prev_sort = prev_sort + ','
							if pos(prev_sort,col_name) > 0 then
								
							end if
						end if
					else 
						prev_sort =''
					end if 
					if tag = 'a' then
					    setsort(prev_sort + col_name)
					    tag = 'd'
					else
						setsort(prev_sort + col_name + ' d')
						tag = 'a'
					end if
					sort()
					this.modify(col_name + ".tag='" + tag + "'")
				end if
			end if
	loop
	return
end if
If dwo.Type = "column"  or dwo.band = 'compute' Then
	if Keydown(KeyControl!) and Keydown(KeyShift!) then
		 if row > 0 then
			data_type = dwo.ColType
			col_name = dwo.name
			choose case data_type 
				  case "long","number","real","int"
					      num = dwo.primary[row]
							setfilter(col_name + ' = ' + string(num) )
							
				  case "date"
						d = dwo.primary[row]
						setfilter(col_name + " = '" + string(d) + "'")
				  case "datetime"
						dt = dwo.primary[row]
						setfilter(col_name + " = '" + string(dt) + "'")
				  case else
					  if mid(data_type,1,4) = "char" then
						 str = dwo.primary[row]
						 setfilter(col_name + " = '" + str + "'")
					  elseif mid(data_type,1,7) = "decimal" then
						  num = dwo.primary[row]
						  setfilter(col_name + " = " + string(num))
					  end if
				end choose
				filter()

		 
	    end if
	end if	
end if
end event

