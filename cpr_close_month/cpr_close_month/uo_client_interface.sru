HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_cpr_close_month.exe'

nvo_cpr_control i_nvo_cpr_control

boolean i_bal_depreciation
boolean i_bal_summary
boolean i_cpr_balanced
boolean i_depr_balanced
boolean i_bal_act_actbasis
boolean i_bal_ledger_ldgbasis
boolean i_bal_ledger_act
boolean i_bal_ledger_subl	
boolean i_bal_quantities
boolean i_bal_subl
boolean i_bal_depr_subl

boolean i_show_list
longlong i_num_basis
end variables

forward prototypes
public function longlong uf_read ()
public subroutine uf_balancedeprledger ()
public subroutine uf_balancesummaryledger ()
public subroutine uf_balancesubledger ()
public function longlong uf_balancecpr ()
public function boolean of_close_cpr_main ()
public function integer uf_required_for_closing (longlong a_company_id)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************
//	
//	
//	
longlong rtn_success, rtn_failure, rtn, i, num
string process_msg
w_dws w

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE';

select count(*) into :i_num_basis from book_summary;
i_show_list = false; rtn = 1

if not of_close_cpr_main() then
	i_nvo_cpr_control.of_log_failed_companies("CLOSE/REOPEN CPR")
	rtn = rtn_failure
elseif upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
	i_nvo_cpr_control.of_log_failed_companies("CLOSE/REOPEN CPR")
	rtn = rtn_failure
else
	rtn = rtn_success
end if

// Release the concurrency lock
i_nvo_cpr_control.of_releaseProcess(process_msg)
f_pp_msgs("Release Process Status: " + process_msg)

return rtn
end function

public subroutine uf_balancedeprledger ();longlong ret
string save_dir
uo_ds_top ds_balance_cpr_depr


// check if this is the last month with activity. If so, there is no need to back down
// activities

ds_balance_cpr_depr = create uo_ds_top 
save_dir = f_get_temp_dir()
	
if save_dir = '' then save_dir = 'c:'

ds_balance_cpr_depr.dataobject = "dw_balance_cpr_depr"
ds_balance_cpr_depr.settransobject(sqlca)

ret = ds_balance_cpr_depr.retrieve(i_nvo_cpr_control.i_company)
	
ds_balance_cpr_depr.setfilter("depr_diff <> 0")
ds_balance_cpr_depr.filter()
	
ret = ds_balance_cpr_depr.rowcount()

if ret > 0  then
	
	i_depr_balanced = false
	f_pp_msgs(" ")	
	f_pp_msgs( " ")	
	f_pp_msgs("********************************************************** ")	
	f_pp_msgs("Depr Ledger is Not in Balance with CPR Ledger.")		
	f_pp_msgs("See differences in file : " + &
	 save_dir + '\deprbal' + '_' + string(i_nvo_cpr_control.i_company) + '.txt')	
	f_pp_msgs("********************************************************** ")	
	f_pp_msgs(" ")	
	f_pp_msgs(" ")	
	
		
	ds_balance_cpr_depr.saveas(save_dir + '\deprbal' + '_' + string(i_nvo_cpr_control.i_company) + '.txt',Text!,true)
end if
	
 
end subroutine

public subroutine uf_balancesummaryledger ();datetime  last_run, max_act_month
decimal {2} difference
longlong rows, ret
string save_dir, month
integer month_num, year_num
uo_ds_top ds_balance_acct_summ
 
save_dir = f_get_temp_dir()

if save_dir = '' then save_dir = 'c:'

ds_balance_acct_summ = create uo_ds_top
ds_balance_acct_summ.dataobject = "dw_balance_account_summ"
ds_balance_acct_summ.settransobject(sqlca)

ret = ds_balance_acct_summ.retrieve(i_nvo_cpr_control.i_company)
	
ds_balance_acct_summ.setfilter("diff <> 0")
ds_balance_acct_summ.filter()
	
ret = ds_balance_acct_summ.rowcount()
	
if ret > 0  then 
	
   i_cpr_balanced = false
	f_pp_msgs(" ")	
	f_pp_msgs(" ")	
	f_pp_msgs("********************************************************** ")	
	f_pp_msgs("Account Summary is Not in Balance with CPR Ledger.")		
	f_pp_msgs("See differences in file : " + &
	  save_dir + '\acctbal' + '_' + string(i_nvo_cpr_control.i_company) + '.txt')		
		
	f_pp_msgs("********************************************************** ")	
	f_pp_msgs(" ")	
	f_pp_msgs(" ")
	ds_balance_acct_summ.saveas(save_dir + '\acctbal' + '_' + string(i_nvo_cpr_control.i_company) + '.txt',Text!,true)
	return	
end if
	
 
end subroutine

public subroutine uf_balancesubledger ();longlong subledger_count, rows, cc, total_company, subledger_type_id
int i, rtn
string subledger_name, sqls, new_sql, original_sql, rc
string template, replacestring
int curs, tablepos
int balanced
int ret
decimal{2} activity_cost, accum_cost, cpr_ldg_basis, cpr_act_basis
double qty1, qty2
string filename, filepath, savedir, sub_view, sub_bal_dollar_only
longlong sub_num, session_id
int basis_ind[]
boolean first_one
uo_ds_top ds_subledger_name, ds_balance_cpr_subledger


sub_bal_dollar_only = lower(f_pp_system_control_company('CPR CLOSE BALANCE SUBL DOLLAR ONLY', i_nvo_cpr_control.i_company))

select userenv('sessionid') into :session_id from dual;

f_set_large_rollback_segment()

savedir = f_get_temp_dir()

if savedir = "" then savedir = 'c:'

// depr ledger  to subledgers
f_pp_msgs("Balancing Depr Ledger with Subledgers...")
ds_subledger_name = create uo_ds_top
ds_subledger_name.dataObject = 'dw_subledger_name'
ds_subledger_name.SetTransObject(sqlca)
ds_subledger_name.reset()
ds_subledger_name.Retrieve()
subledger_count = ds_subledger_name.rowcount()

for i = 1 to subledger_count

	subledger_name = ds_subledger_name.GetItemString(i, 'subledger_name')
	subledger_type_id = ds_subledger_name.getitemnumber(i, 'subledger_type_id')
		
	if ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 0 or ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 3 then continue
		
	if sub_bal_dollar_only = 'no' and ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 2 then continue
		
	if subledger_name <> "None" then
		f_pp_msgs("Balancing Depr Ledger (book) with " + subledger_name + "...")
		
		if not isValid(ds_balance_cpr_subledger) then
			ds_balance_cpr_subledger = create uo_ds_top
		end if
		
		ds_balance_cpr_subledger.dataobject = "dw_balance_depr_subledger"
	   ds_balance_cpr_subledger.settransobject(sqlca)
	
		//Get SQL for datawindow
		sqls = ds_balance_cpr_subledger.DESCRIBE("datawindow.table.select")
		original_sql = "datawindow.table.select = '" + sqls + " '"

		sqls = f_replace_string(sqls, "TO_NUMBER('DEPR_COMPANY')", string(i_nvo_cpr_control.i_company), 'all')
		sqls = f_replace_string(sqls, "TO_NUMBER('DEPR_SUBLEDGER_TYPE_ID')", &
				string(subledger_type_id), 'all')
		
		sqls = f_replace_string(sqls,'DEPR_SUBLEDGER_TEMPLATE',& 
							 UPPER(subledger_name)+'_DEPR' ,'all')
							 
		//Prepare SQL for Retrieve
		new_sql = "datawindow.table.select = '" + sqls + " '"
		
		rc = ds_balance_cpr_subledger.Modify(new_sql)
		if rc = "" then
			ds_balance_cpr_subledger.settransobject(sqlca)
			rows = ds_balance_cpr_subledger.Retrieve()
			
			ds_balance_cpr_subledger.setfilter("diff <> 0")
			ds_balance_cpr_subledger.filter()
			
			rows = ds_balance_cpr_subledger.rowcount()
			
			if rows > 0 then
				i_depr_balanced = false
				filename = "\depr_book_" + string(i_nvo_cpr_control.i_company) + '_' + left(subledger_name, 4)
				filepath = savedir + filename + ".txt"

				f_pp_msgs("   " + subledger_name +  " out of Balance with Depr Ledger")
				f_pp_msgs("    --See file '" + filepath +"'")
	
				ret = ds_balance_cpr_subledger.saveas(filepath, Text!, True)
				if ret = -1 then
					f_pp_msgs("saveas failed.")
				end if
			end if
		else
			f_pp_msgs("Modify Failed" + rc)
		end if

		//restore original template
      ds_balance_cpr_subledger.setfilter("")
		ds_balance_cpr_subledger.filter()
	end if
next

for i = 1 to subledger_count

	subledger_name = ds_subledger_name.GetItemString(i, 'subledger_name')
	subledger_type_id = ds_subledger_name.getitemnumber(i, 'subledger_type_id')
		
	if ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 0 or ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 3 then continue
		
	if subledger_name <> "None" then
		f_pp_msgs("Balancing Depr Ledger (reserve) with " + subledger_name + "...")
		
		if not isValid(ds_balance_cpr_subledger) then
			ds_balance_cpr_subledger = create uo_ds_top
		end if
		
		ds_balance_cpr_subledger.dataobject = "dw_balance_depr_subledger_res"
	   ds_balance_cpr_subledger.settransobject(sqlca)
				
		//Get SQL for datawindow
		sqls = ds_balance_cpr_subledger.DESCRIBE("datawindow.table.select")
		original_sql = "datawindow.table.select = '" + sqls + " '"

		sqls = f_replace_string(sqls, "TO_NUMBER('DEPR_COMPANY')", string(i_nvo_cpr_control.i_company), 'all')
		sqls = f_replace_string(sqls, "TO_NUMBER('DEPR_SUBLEDGER_TYPE_ID')", &
				string(subledger_type_id), 'all')
			
		sqls = f_replace_string(sqls,'DEPR_SUBLEDGER_TEMPLATE',& 
							 UPPER(subledger_name)+'_DEPR' ,'all')
	
		//Prepare SQL for Retrieve
		new_sql = "datawindow.table.select = '" + sqls + " '"
			
		rc = ds_balance_cpr_subledger.Modify(new_sql)
		if rc = "" then
			ds_balance_cpr_subledger.settransobject(sqlca)
			rows = ds_balance_cpr_subledger.Retrieve()
			
			ds_balance_cpr_subledger.setfilter("depr_diff <> 0")
			ds_balance_cpr_subledger.filter()
			
			rows = ds_balance_cpr_subledger.rowcount()
			
			if rows > 0 then
				i_depr_balanced = false
				filename = "\depr_res_" + string(i_nvo_cpr_control.i_company) + '_' + left(subledger_name, 4)
				filepath = savedir + filename + ".txt"

				f_pp_msgs("   " + subledger_name +  " out of Balance with Depr Ledger")
				f_pp_msgs("    --See file '" + filepath +"'")
	
				ret = ds_balance_cpr_subledger.saveas(filepath, Text!, True)
				if ret = -1 then
					f_pp_msgs("saveas failed.")
				end if
			end if
		else
			f_pp_msgs("Modify Failed" + rc)
		end if

		//restore original template
      ds_balance_cpr_subledger.setfilter("")
		ds_balance_cpr_subledger.filter()
	end if
next

f_pp_msgs("Finished balancing depr ledger to subledgers.")




end subroutine

public function longlong uf_balancecpr ();longlong subledger_count, rows, kk
int i, curs, tablepos, balanced, ret
string subledger_name, sqls, new_sql, original_sql, rc
string template, replacestring
decimal{2} activity_cost, accum_cost, cpr_ldg_basis, cpr_act_basis
double qty1, qty2
string filename, filepath, savedir, sub_view
longlong sub_num, session_id
int basis_ind[]
boolean first_one
uo_ds_top ds_subledger_name


select userenv('sessionid') into :session_id from dual;

f_set_large_rollback_segment()

savedir = f_get_temp_dir()

if savedir = "" then savedir = 'c:'

if not i_show_list then
	i_bal_ledger_act      = TRUE
	i_bal_ledger_ldgbasis = TRUE
	i_bal_act_actbasis    = TRUE
	i_bal_quantities      = TRUE
	i_bal_ledger_subl     = TRUE
	i_bal_subl            = FALSE
	i_bal_depreciation    = FALSE
	i_bal_depr_subl       = FALSE
	i_bal_summary         = TRUE
end if

i_cpr_balanced  = true
i_depr_balanced = true


sqls = 'select '

for kk = 1 to i_num_basis
	if kk < i_num_basis then
		sqls  = sqls + ' basis_'+ string(kk) + '_indicator, '  
	else
		sqls  = sqls + ' basis_'+ string(kk) + '_indicator '  
	end if
next 

sqls = sqls + '  from set_of_books where set_of_books_id = 1'
 
uo_ds_top ds_select1
ds_select1 = CREATE uo_ds_top
f_create_dynamic_ds(ds_select1, 'grid', sqls, sqlca, true)
 
for kk = 1  to i_num_basis
   basis_ind[kk] = ds_select1.getitemnumber(1, kk)
next

//*** Balance CPR
if i_bal_ledger_act = TRUE then
	f_pp_msgs("Balancing CPR with CPR Activity..."+ ' ' + string(now()))
	select sum(activity_cost) into :activity_cost from cpr_activity;
	select sum(accum_cost)    into :accum_cost from cpr_ledger;
	
	
	if accum_cost <> activity_cost then
		f_pp_msgs('CPR_ACTIVITY and CPR_LEDGER are out of balance. Check accumulated cost/activty cost reports to find the errant company.' )
	end if
end if

if i_bal_ledger_ldgbasis = TRUE then
	f_pp_msgs("Balancing CPR Ledger with CPR Ledger Basis..."+ ' ' + string(now()))
	
	sqls =  'SELECT sum(  ' 

	first_one = true

	for i = 1 to i_num_basis
		if basis_ind[i] = 1 then
			if first_one then
				first_one = false
				sqls = sqls + '   NVL(BASIS_' + string(i)+ ' ,0) '  
			else
				sqls = sqls + ' + NVL(BASIS_' + string(i)+ ' ,0) '  
			end if
		end if	
	next

	sqls = sqls + ') FROM CPR_LDG_BASIS '
 
	DECLARE get_basis  DYNAMIC CURSOR FOR SQLSA ;
  
	PREPARE SQLSA FROM :sqls;

	OPEN DYNAMIC get_basis ;

	FETCH get_basis  INTO :cpr_ldg_basis;

	CLOSE get_basis ;

	 
	if accum_cost = 0 then
		select sum(accum_cost) into :accum_cost from cpr_ledger;
	end if

	if cpr_ldg_basis <> accum_cost then
		f_pp_msgs("CPR_LEDGER and CPR_LDG_BASIS are out of balance. Check CPR Ledger basis reports to find the errant company.")
	end if
end if

if i_bal_act_actbasis = TRUE then
	f_pp_msgs("Balancing CPR Activity with CPR Activity Basis..."+ ' ' + string(now()))
	
	sqls =  'SELECT sum(  ' 

	first_one = true

	for i = 1 to i_num_basis
		if basis_ind[i] = 1 then
			if first_one then
				first_one = false
				sqls = sqls + '   NVL(BASIS_' + string(i)+ ' ,0) '  
			else
				sqls = sqls + ' + NVL(BASIS_' + string(i)+ ' ,0) '  
			end if
		end if	
	next

	sqls = sqls + ') FROM CPR_ACT_BASIS '
 
	DECLARE get_act_basis  DYNAMIC CURSOR FOR SQLSA ;
  
	PREPARE SQLSA FROM :sqls;
	
	OPEN DYNAMIC get_act_basis ;

	FETCH get_act_basis  INTO :cpr_act_basis;

	CLOSE get_act_basis ;

	if activity_cost = 0 then
		select sum(activity_cost) into :activity_cost from cpr_activity;
	end if


	if cpr_act_basis <> activity_cost then
		f_pp_msgs("CPR_LEDGER and CPR_ACTI_BASIS are out of balance. Check CPR Activity Basis reports to find the errant company.")
	end if
end if




// *** Balance quantity
if i_bal_quantities = TRUE then
	f_pp_msgs("Balancing CPR with CPR activity quantities..."+ ' ' + string(now())) 
	
	select sum(activity_quantity) into :qty1 from cpr_activity;
	select sum(accum_quantity)    into :qty2 from cpr_ledger;
	
	if qty1 <> qty2 then  
		uo_ds_top ds_cpr_control_bal_quantity
		ds_cpr_control_bal_quantity = create uo_ds_top
		ds_cpr_control_bal_quantity.dataObject = 'dw_cpr_control_bal_quantity'
		ds_cpr_control_bal_quantity.settransobject(sqlca)
		ds_cpr_control_bal_quantity.reset()
		f_pp_msgs("Quantites on CPR_ACTIVITY and CPR_LEDGER are out of balance for some company. "+&
						"Checking current company..."+ ' ' + string(now()))

		rows = ds_cpr_control_bal_quantity.retrieve(i_nvo_cpr_control.i_company)
		if rows > 0 then
			i_cpr_balanced = false
			filename = "\cpr_qty_" + string(i_nvo_cpr_control.i_company)  
			filepath = savedir + filename + ".txt"

			f_pp_msgs("Quantity is out of balance for company_id = "+string(i_nvo_cpr_control.i_company)+" at "+ string(now()))
			f_pp_msgs("       --See file '" + filepath +"'")

			ret = ds_cpr_control_bal_quantity.saveas(filepath, Text!, True)
			if ret = -1 then
				f_pp_msgs("saveas failed.")
			end if
		elseif rows = 0 then
			f_pp_msgs("Quantity is OK for this Company"+ ' ' + string(now()))
		else
			f_pp_msgs('Datawindow error: '+sqlca.sqlErrText)
		end if
	end if
end if

if i_bal_ledger_subl = TRUE then
	//**** Check if CPR_Ledger in balance with subledger depr tables
	f_pp_msgs("Balancing CPR Ledger with Subledgers..."+ ' ' + string(now()))
	
	ds_subledger_name = create uo_ds_top
	ds_subledger_name.dataObject = 'dw_subledger_name'
	ds_subledger_name.SetTransObject(sqlca)
	
	ds_subledger_name.reset()
	ds_subledger_name.Retrieve()
	subledger_count = ds_subledger_name.rowcount()

	for i = 1 to subledger_count
		subledger_name = ds_subledger_name.GetItemString(i, 'subledger_name')
		
		if ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 0 or ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 3 then continue
		
		if subledger_name <> "None" then
			f_pp_msgs("Balancing CPR with " + subledger_name + "..."+ ' ' + string(now()))
			
			uo_ds_top ds_balance_cpr_subledger
			ds_balance_cpr_subledger = create uo_ds_top
			
			ds_balance_cpr_subledger.dataobject = "dw_balance_cpr_subledger"
	      ds_balance_cpr_subledger.settransobject(sqlca)
		
			//Get SQL for datawindow
			sqls = ds_balance_cpr_subledger.DESCRIBE("datawindow.table.select")
			sqls = f_replace_string(sqls, "USERENV('SESSIONID')", string(session_id), 'all')
			original_sql = "datawindow.table.select = '" + sqls + " '"

   		//First Find 'DEPR_SUBLEDGER_COMP_VIEW' table throughout SQL and replace; 
			
         sqls = f_replace_string(sqls,'TEMPLATE_DOLLARS_VIEW',& 
                         UPPER(subledger_name)+'_DOLLARS_VIEW' ,'all')
							 
			sqls = f_replace_string(sqls,'SUBLEDGER_TEMPLATE',& 
                         UPPER(subledger_name) ,'all')
					 

			//Prepare SQL for Retrieve
			new_sql = "datawindow.table.select = '" + sqls + " '"
			
			rc = ds_balance_cpr_subledger.Modify(new_sql)
			if rc = "" then
				ds_balance_cpr_subledger.settransobject(sqlca)
				rows = ds_balance_cpr_subledger.Retrieve()
				
				ds_balance_cpr_subledger.setfilter("diff <> 0")
				ds_balance_cpr_subledger.filter()
				
				rows = ds_balance_cpr_subledger.rowcount()
				
				if rows > 0 then
					i_cpr_balanced = false
					filename = "\cpr_" + string(i_nvo_cpr_control.i_company) + '_' + left(subledger_name, 4)
					filepath = savedir + filename + ".txt"

					f_pp_msgs("   " + subledger_name +  " Assets out of Balance")
					f_pp_msgs("    --See file '" + filepath +"'")
		
					ret = ds_balance_cpr_subledger.saveas(filepath, Text!, True)
					if ret = -1 then
						f_pp_msgs("saveas failed.")
					end if

				end if
			else
   			f_pp_msgs("Modify Failed" + rc)
			end if

			//restore original template
         ds_balance_cpr_subledger.setfilter("")
			ds_balance_cpr_subledger.filter()
		end if
	next
end if




if i_bal_subl = TRUE then
	//**** Check if subledgers are in balance
	f_pp_msgs("Balancing Subledgers..."+ ' ' + string(now()))
	
	if not isValid(ds_subledger_name) then
		ds_subledger_name = create uo_ds_top
		ds_subledger_name.dataObject = 'dw_subledger_name'
	end if
	
	ds_subledger_name.SetTransObject(sqlca)
	ds_subledger_name.reset()
	ds_subledger_name.Retrieve()
	subledger_count = ds_subledger_name.rowcount()
	for i = 1 to subledger_count
		
		if ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 0 or ds_subledger_name.GetItemnumber(i, 'depreciation_indicator') = 3 then continue

		subledger_name = ds_subledger_name.GetItemString(i, 'subledger_name')
		
		if subledger_name <> "None" then
			f_pp_msgs("Balancing '" + subledger_name +"' subledger...")
			
			uo_ds_top ds_balance_subl_depr_basis
			ds_balance_subl_depr_basis = create uo_ds_top

         ds_balance_subl_depr_basis.dataobject = 'dw_balance_subl_depr_basis'
			ds_balance_subl_depr_basis.settransobject(sqlca)
			
			//Get SQL for datawindow
			sqls = ds_balance_subl_depr_basis.Describe("datawindow.table.select")
			original_sql = "datawindow.table.select = '" + sqls + " '"
			sqls = f_replace_string(sqls, "USERENV('SESSIONID')", string(session_id), 'all')
					
			//First Find 'DEPR_SUBLEDGER_TEMPLATE' table throughout SQL and replace; 
		   template = upper("depr_subledger_template")
		   replacestring = upper(subledger_name + '_depr')

   		curs = 1
   		tablepos = pos(sqls, template, curs)
   		do until tablepos = 0
	   		sqls = replace(sqls, tablepos, 23, replacestring)		
	   		curs = curs + len(replacestring)
	   		tablepos = pos(sqls, template, curs)
  			loop
 
			//Find "DEPR_SUBLEDGER_BASIS" throughout SQL and replace
			template = "depr_subledger_basis"
			template = lower(template)
			replacestring = lower(subledger_name + '_basis')

			curs = 1
			tablepos = pos(sqls, template, curs)
			do until tablepos = 0
				sqls = replace(sqls, tablepos, 20, replacestring)		
				curs = curs + len(replacestring)
				tablepos = pos(sqls, template, curs)
			loop

			//Prepare SQL for Retrieve
			new_sql = "datawindow.table.select = '" + sqls + " '"

			rc = ds_balance_subl_depr_basis.Modify(new_sql)
			if rc = "" then
				ds_balance_subl_depr_basis.SetTransObject(sqlca)
				rows = ds_balance_subl_depr_basis.Retrieve()
				if rows > 0 then
               i_cpr_balanced = false
					filename = left(subledger_name, 8) + '_' + string(i_nvo_cpr_control.i_company) 
					filepath = savedir + '\' + filename + ".txt"

					f_pp_msgs(subledger_name + " Assets out of Balance")
					f_pp_msgs("    --See file '" + filepath +"'")
		
					ret = ds_balance_subl_depr_basis.saveas(filepath, Text!, True)
					if ret = -1 then
						f_pp_msgs("saveas failed.")
					end if


				end if
			else
   			f_pp_msgs("dwModify Failed" + rc)
			end if

			//restore original template
			ds_balance_subl_depr_basis.Modify(original_sql)
			ds_balance_subl_depr_basis.setfilter("")
			ds_balance_subl_depr_basis.filter()
		end if
	next
end if



if i_bal_depreciation = TRUE then
	f_pp_msgs("Balancing CPR to Depreciation Ledger..." + string(i_nvo_cpr_control.i_company)+ ' ' + string(now()))	
	uf_balanceDeprLedger()
end if
	
	
if i_bal_summary = TRUE then
	f_pp_msgs("Balancing CPR to Summary Ledger..." + string(i_nvo_cpr_control.i_company)+ ' ' + string(now()))
	uf_balanceSummaryLedger()
end if

update CPR_CONTROL
set CPR_BALANCED = sysdate
where COMPANY_ID = :i_nvo_cpr_control.i_company
and ACCOUNTING_MONTH = :i_nvo_cpr_control.i_month;
if SQLCA.sqlcode <> 0 then
	f_pp_msgs('Error updating CPR_BALANCED date on CPR_CONTROL. Terminating interface.')
	return -1
end if

// depr ledger  to subledgers

if i_bal_depr_subl = true then
	uf_balanceSubLedger()
end if

f_pp_msgs("Finished balancing CPR."+ ' ' + string(now()))

return 1
end function

public function boolean of_close_cpr_main ();datetime closed, month, nullval
longlong mo, yr, ret, i, num_cols, rc, depr, rtn, check
string title1, sub_string, sqls, subledger, template, replacestring, sql_dollar, original_sql
longlong year, month1, month2, life_month, curve_month
string month1_str, month2_str, balance_cpr, search_date, company, je_code, company_name
longlong num_pend, cc, total_company, rows, curs, tablepos, num_groups, group_rows[], k, j
longlong group_id, books_id, num_rows, je_id, company_id
decimal {2} adds, retire, gain_loss, salvage, depr_exp, end_res, trans_in, trans_out
datetime auto_month, max_month
date cpr_closed, check_date
string msg , je_code2, close_or_reopen, comp, ret_str,balance_depr, comp_descr
longlong cpr_depr_rtn, closed_companies, open_companies, process_id, pp_stat_id 
string   find_str, user_id
boolean   vfy_users
uo_ds_top ds_users
boolean   required_not_run = false, brtn = true
longlong num_failed_companies

// Construct the nvo
i_nvo_cpr_control.of_constructor()

// Get the ssp parameters
close_or_reopen = upper(g_ssp_parms.string_arg[1])
i_nvo_cpr_control.i_company_idx[]  = g_ssp_parms.long_arg[]
i_nvo_cpr_control.i_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_original_month = datetime(g_ssp_parms.date_arg[1])
i_nvo_cpr_control.i_month_number	= year(g_ssp_parms.date_arg[1])*100 + month(g_ssp_parms.date_arg[1])

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return false
end if

// Get the descriptions
rtn = i_nvo_cpr_control.of_getDescriptionsFromIds(i_nvo_cpr_control.i_company_idx)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return false
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_cpr_control.of_selectedCompanies(i_nvo_cpr_control.i_company_idx) = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do " + &
				"not line up")
	return false
end if

setnull(nullval)

total_company = upperbound(i_nvo_cpr_control.i_company_idx)

// make sure all companies selected are all closed or all open, as the case may be.
closed_companies  = 0
open_companies    = 0

for cc = 1 to total_company
  
   select count(*) into :check 
     from cpr_control 
	 where company_id = :i_nvo_cpr_control.i_company_idx[cc] 
	 	and accounting_month = :i_nvo_cpr_control.i_month 
		and cpr_closed is null;
 
  if check = 0 then
     closed_companies = closed_companies + 1
  else
     open_companies = open_companies + 1
  end if
   
next 

if pos(close_or_reopen,'OPEN',1) > 0 then
   
   if closed_companies <> total_company then
      f_pp_msgs("Cannot reopen the companies together because some companies are already open")
      return false
   end if

else
   
   if open_companies <> total_company then
      f_pp_msgs("Cannot close the companies together because some companies are already closed")
      return false
   end if
   
end if

for cc = 1 to total_company
   
   rc = i_nvo_cpr_control.of_setupCompany(cc)
   
   if rc < 1 then continue // next company
   
   if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'powerplant_closed')) then
      f_pp_msgs("You cannot re-open the month after PowerPlant has been closed.")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
      continue // next company
   end if
	
   // Make sure that all the required interfaces have been run
   if uf_required_for_closing(i_nvo_cpr_control.i_company) <> 0 then 
      comp_descr = i_nvo_cpr_control.i_company_descr[cc]
      f_pp_msgs("WARNING: Company " + comp_descr + "was not processed because some of the required interfaces have not been run. Please run the required interfaces first.")
      required_not_run = true
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
      goto next_company
   end if
   
   month1 = month(date(i_nvo_cpr_control.i_month))
   year   =  year(date(i_nvo_cpr_control.i_month))
      
   month1_str = String(i_nvo_cpr_control.i_month, "MM/YYYY")
      
   if pos(close_or_reopen,'OPEN',1) > 0 then
   	
		comp_descr = i_nvo_cpr_control.i_company_descr[cc]
      
      f_pp_msgs("Reopen month started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
      
      pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "Close CPR: " + comp_descr)
         
      // setup other fields   
      SetNull(company)
      Setnull(je_id)
      Setnull(je_code)
      select gl_company_no into :company from company where company_id = :i_nvo_cpr_control.i_company;
      if sqlca.sqlcode <> 0 then
         f_pp_msgs("Error getting Company Number: " + sqlca.sqlerrtext)
			
         goto next_company
      end if
      if IsNull(company) then
         f_pp_msgs("Company Number not found for ID: " + String(i_nvo_cpr_control.i_company))
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if      
      
      select je_id into :je_id from gl_je_control where process_id = 'Depreciation Expense';
      if sqlca.sqlcode <> 0 then
         f_pp_msgs("Error getting Journal ID: " + sqlca.sqlerrtext)
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if
      if IsNull(je_id) then
         f_pp_msgs("Journal ID not found for 'Depreciation Expense'")
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if      
      
      select gl_je_code into :je_code from standard_journal_entries where je_id = :je_id;
      if sqlca.sqlcode <> 0 then
         f_pp_msgs("Error getting GL Journal Code: " + sqlca.sqlerrtext)
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if
      if IsNull(je_code) then
         f_pp_msgs("GL Journal Code not found for ID: " + String(je_id))
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if      
      
      // reset dates
      num_cols = long(i_nvo_cpr_control.i_ds_cpr_control.describe("datawindow.column.count"))
   
      for i = 3 to num_cols
   
         //###  ARE 2/6/2007
         // if i = 5  or i = 10 or i = 11 then continue // don't null depr calc or aro calc
          if i = 5  or i = 10   then continue // don't null depr calc or retirements amortized 
          
          i_nvo_cpr_control.i_ds_cpr_control.setitem(1,i,nullval)
   
      next
   
      check_date = date(i_nvo_cpr_control.i_month)
      month1 = month(date(i_nvo_cpr_control.i_month))
      year   =  year(date(i_nvo_cpr_control.i_month))
         
      month1_str = String(i_nvo_cpr_control.i_month, "MM/YYYY")
   
      update depr_ledger   set depr_ledger_status = 8
      where to_char(depr_ledger.gl_post_mo_yr, 'mm/yyyy') = :month1_str and
      depr_group_id in (select depr_group_id from depr_group where
      company_id = :i_nvo_cpr_control.i_company);
      
      f_check_sql_error(sqlca, "Update Depr Ledger status 8 ")
     
      month2 = month1 + 1
      if month2 = 13 then 
         month2 = 1
         year   = year + 1
      end if
      
      month2_str = String(month2, "00") + "/" + String(year,"0000")
   
      update depr_ledger   set depr_ledger_status = 9
      where depr_ledger.gl_post_mo_yr >= to_date(:month2_str,'mm/yyyy')
      and depr_group_id in 
         (select depr_group_id 
         from depr_group 
         where   company_id = :i_nvo_cpr_control.i_company);

     f_check_sql_error(sqlca, "Update depr Ledger Status 9 ")

      update depr_method_rates
         set rate_used_code = 0
         where rate_used_code = 1
         and to_char(effective_date, 'mm/yyyy') = :month1_str
         and depr_method_id in 
            (select depr_method_id 
             from   depr_group
             where  company_id = :i_nvo_cpr_control.i_company
               and   nvl(subledger_type_id,0) > -1)   ;
          
     f_check_sql_error(sqlca, "Update Rate Used Code ")

 
    select count(*)   into :check
    from cpr_control
    where company_id = :i_nvo_cpr_control.i_company and
    accounting_month = :i_nvo_cpr_control.i_month  and      depr_approved  is not null;

	if  check > 0 then // depr has  been approved
	
		select count(*) into :check from 
		gl_transaction
		where company_number = :company
		and   month = :i_nvo_cpr_control.i_month 
		and source in ( 'DEPR APPROVAL', 'DEPR RECUR');
		
		if check = 0 then
			g_msg.messagebox("", "CPR cannot be reopened. The Depreciation Journal entries to reverse are not in the GL transaction table")
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
			return false
		end if
	
	end if
 
	insert into gl_transaction
      (gl_trans_id, month, company_number, gl_account, amount, gl_je_code,
      gl_status_id, description, debit_credit_indicator, source,
	originator, comments, pend_trans_id, asset_id,
	je_method_id, amount_type,TAX_ORIG_MONTH_NUMBER, trans_type)
      select pwrplant1.nextval, month, company_number, gl_account, amount, gl_je_code,
      1, SUBSTR(description || ':Reversal',1,254),
      DECODE(debit_credit_indicator, 0, 1, 1, 0),source,
	originator, comments, pend_trans_id, asset_id,
	je_method_id, amount_type,TAX_ORIG_MONTH_NUMBER, trans_type
      from gl_transaction
      where company_number = :company
      and   month = :i_nvo_cpr_control.i_month 
      and source in ( 'DEPR APPROVAL', 'DEPR RECUR' ) ;
      
      if sqlca.sqlcode <> 0 then
         f_pp_msgs("Error inserting GL Transaction Reversal: " + sqlca.sqlerrtext)
         rollback;
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if         
      
      
      rtn = f_aro_reopen_month(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month)
      if rtn <> 1 or isnull(rtn) then
         f_pp_msgs("Error during ARO Reopen Month")
         rollback;
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if
      
      rtn = f_reg_entries_reopen_month(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month)
      if rtn <> 1 or isnull(rtn) then
         f_pp_msgs("Error during ARO Reopen Month")
         rollback;
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         goto next_company
      end if
      
      i_nvo_cpr_control.of_updateDW()
       
      continue // next company
   
   else
		
		select max(accounting_month)
		  into :max_month
		  from cpr_control
		 where company_id = :i_nvo_cpr_control.i_company;
		
//      if i_col_num = 1 then
   	if max_month = i_nvo_cpr_control.i_month then
         f_pp_msgs("Next month must be open before closing the CPR Ledger.")
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         continue // next company   
      end if
   
      // make sure the previous month is closed

         
      select count(*)   into :check
      from cpr_control
      where company_id = :i_nvo_cpr_control.i_company and
      to_char(accounting_month, 'yyyymm') = to_char(add_months(:i_nvo_cpr_control.i_month, -1), 'yyyymm') and
      cpr_closed is null;
   
      if check = 1 then
         f_pp_msgs("You cannot close the month if the previous month is open.")
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
         continue // next company
      end if
      
      // checkthere are no pending transactions for this month
    
      month1_str = String(i_nvo_cpr_control.i_month, "MM/YYYY")
   
      select count(*) into :num_pend from pend_transaction where 
         to_char(gl_posting_mo_yr, 'mm/yyyy') = :month1_str and
         company_id = :i_nvo_cpr_control.i_company;
      
      if  num_pend > 0   then
      	f_pp_msgs("Cannot Close the Month While There are Still Unposted Pending " &
            + " Transactions for the Month")
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
				
				
         continue // next company   
      end if
      
      ///// $$$ARE added 6/1/2004.  If depr approved, make sure all depr activity posted /////
      if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'depr_approved')) then
         
         check = 0 
			//Maint 8604
			select 1 into :check
			from pend_depr_activity a, depr_group g
			where (gl_post_mo_yr = :i_nvo_cpr_control.i_month or current_mo_yr = :i_nvo_cpr_control.i_month)
			and a.depr_group_id = g.depr_group_id
			and g.company_id = :i_nvo_cpr_control.i_company
			and rownum = 1
			;
					
         if check = 1 then
            f_pp_msgs("Cannot CLOSE CPR for the Month While There are Still Unposted Pending " &
                  + " Reserve Transactions for the Month and Depr has already been approved")
			
				num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
				i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
						
            goto next_company   
         end if
      end if
         
      // Check if automatic transactions should have been created for this month
            
     
      select auto_life_month  into :life_month from company where company_id   = :i_nvo_cpr_control.i_company;
      select auto_curve_month into :curve_month from company where company_id = :i_nvo_cpr_control.i_company;
    
      if life_month  = month1 or curve_month = month1 then
      
         select retirements_amortized into :auto_month from cpr_control where
          to_char(accounting_month, 'mm/yyyy') = :month1_str and
         company_id = :i_nvo_cpr_control.i_company;
          
         if isnull(auto_month) then
            f_pp_msgs("The Automatic Transactions have not been Created. " + &
            " This company cannot be closed until the Auto Transactions have been generated. " + &
            " Click the 'Auto Transactions' button on the right to initiate this process. ")
			
				num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
				i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
				
            continue // next company   
         end if 
      end if
      
      closed =  i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'cpr_closed') 
   
      if not isnull(closed) then                    
   
         f_pp_msgs("CPR is already closed for this month.")
			
         continue // next company   
   
      else
   		// Roll balances forward on   Account Summary 
			comp_descr = i_nvo_cpr_control.i_company_descr[cc]
      
         f_pp_msgs("Closing CPR and rolling balances forward for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
         f_pp_msgs("Closing CPR and rolling balances forward..."+ ' ' + string(now()))
      
         select description  into :comp_descr from company where company_id = :i_nvo_cpr_control.i_company;
         pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "Close CPR: " + comp_descr)
         f_pp_msgs("Close CPR started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
      
         mo = month(date(i_nvo_cpr_control.i_month))
   
         yr = year(date(i_nvo_cpr_control.i_month))
   
         if mo = 12 then
            mo = 1
            yr = yr + 1
         else
            mo = mo + 1
         end if
   
         month = datetime(date((string(mo) + '-01-' + string(yr))))
   
               
         update account_summary   set    ending_balance =
                                 beginning_balance
                              +   additions
                              +   retirements
                              +   transfers_in
                              +   transfers_out
                              +   adjustments    
         where 
          to_char(:i_nvo_cpr_control.i_month, 'mm/yyyy')  =
              to_char(account_summary.gl_posting_mo_yr, 'mm/yyyy') and
              company_id = :i_nvo_cpr_control.i_company;
     
   
         ret = f_check_sql_error(sqlca,"Error trying to update Ending Balance in " & 
                                 + "the Account Summary table. ")
   
         commit;
   
         update account_summary a  set beginning_balance =
            (select b.ending_balance from  account_summary b 
            where a.set_of_books_id    = b.set_of_books_id and
             a.company_id         = b.company_id   and 
             a.bus_segment_id     = b.bus_segment_id   and 
             a.gl_account_id      = b.gl_account_id   and
             a.utility_account_id = b.utility_account_id   and
             a.sub_account_id     = b.sub_account_id   and 
             a.major_location_id  = b.major_location_id   and   
             (to_number(to_char(a.gl_posting_mo_yr, 'J')) - 33) <
              to_number(to_char(b.gl_posting_mo_yr , 'J')) and
             (to_number(to_char(a.gl_posting_mo_yr, 'J')) - 27) >
              to_number(to_char(b.gl_posting_mo_yr , 'J')) and
              to_char(:month, 'mm/yyyy') =
              to_char(a.gl_posting_mo_yr, 'mm/yyyy') and
              b.company_id  = :i_nvo_cpr_control.i_company) 
         where 
            to_char(:month, 'mm/yyyy' ) =
               to_char(a.gl_posting_mo_yr, 'mm/yyyy') and
               a.company_id = :i_nvo_cpr_control.i_company
	 
	and exists (select 1
	from  account_summary  b 
				where a.set_of_books_id    = b.set_of_books_id and
				 a.company_id         = b.company_id   and 
				 a.bus_segment_id     = b.bus_segment_id   and 
				 a.gl_account_id      = b.gl_account_id   and
				 a.utility_account_id = b.utility_account_id   and
				 a.sub_account_id     = b.sub_account_id   and 
				 a.major_location_id  = b.major_location_id   and   
				 (to_number(to_char(a.gl_posting_mo_yr, 'J')) - 33) <
				  to_number(to_char(b.gl_posting_mo_yr , 'J')) and
				 (to_number(to_char(a.gl_posting_mo_yr, 'J')) - 27) >
				  to_number(to_char(b.gl_posting_mo_yr , 'J')) and
				 to_char(:month,'mm/yyyy') =
              to_char(a.gl_posting_mo_yr, 'mm/yyyy') and
              b.company_id  = :i_nvo_cpr_control.i_company) ;
     
   
         ret = f_check_sql_error(sqlca,"Error trying to update Beginning Balance in " & 
                                       + "the Account Summary table. ")
   
         commit;

         // See if Depr Approval already run
         if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'depr_approved')) then
            ret_str = f_depr_ledger_rollfwd(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month)
            
            update depr_ledger set depr_ledger_status = 8
            where to_char(gl_post_mo_yr, 'mm/yyyy') =  to_char(:month, 'mm/yyyy' ) and
            depr_group_id in (select depr_group_id from depr_group where company_id = :i_nvo_cpr_control.i_company and
            nvl(subledger_type_id,0) > -1);
            
            if ret_str <> "" then
            
               f_pp_msgs("Balancing CPR to Depreciation Ledger..."+ ' ' + string(now()))
      
               i_depr_balanced = true
                balance_depr = f_pp_system_control_company("Balance DEPR on Close", i_nvo_cpr_control.i_company)
   
               if balance_depr = "" or balance_depr = "yes" then
                  uf_balanceDeprLedger()
             
                  if i_depr_balanced = false then
                     rollback; 
                     f_pp_msgs("Depreciation not Balanced to CPR. Notify PPC.")
			
							num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
							i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
							
                     return false
                  end if
               else
                  f_pp_msgs("Not Running the DEPR Balancing Routines this Month.")
               end if
          
               goto next_company
            end if
   
         end if // depr_approved not null
      
      end if 
   
   end if
   
   // set the cpr_closed date so the bal  routines will use
   // the ending balfor this month. On error, the cpr_close date
   // is made null
   
   
   i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'cpr_closed',today()) 
   
   i_nvo_cpr_control.of_updateDW()
   

   balance_cpr = f_pp_system_control_company("Balance CPR on Close", company_id)

   if  balance_cpr = "" or balance_cpr = "yes" then
   
      ret = uf_balanceCPR()
		
		if ret < 0 then
			brtn = false
		end if
   
      if i_cpr_balanced = true then
      
			i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'cpr_closed',today()) 

			i_nvo_cpr_control.of_updateDW()
		 
			f_pp_msgs("Finished Closing the CPR"+ ' ' + string(now()))
      else
         i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'cpr_closed',nullval) 
   
         i_nvo_cpr_control.of_updateDW()
          
         f_pp_msgs(" ERROR !!! CPR Not Balanced. Notify PPC")
			
			// make sure the interface returns the failure return code
			brtn = false
      end if   
   else
      f_pp_msgs("Not Running the CPR Balancing Routines This Month.")
      f_pp_msgs("Finished Closing the CPR."+ ' ' + string(now()))
         
   end if

	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)
next_company:
next


i_nvo_cpr_control.of_cleanUp(1, 'email cpr close: close cpr', 'Close CPR')

if required_not_run then
   f_pp_msgs("Some of the companies were not processed because required interfaces were not run. Please review the log and hit OK to continue.")
end if

if not brtn then return false

return true

end function

public function integer uf_required_for_closing (longlong a_company_id);// Maintenance 450
// return values:
//	1 if required_for_closing is true
//	0 if required_for_closing is false
//	-1 if an error occured or companies do not line up

long	c, i, company_id, interface_id,  required_for_closing
string sqls, text, description, month_str, company_descr
uo_ds_top ds
datetime last_run 

month_str = String(i_nvo_cpr_control.i_month, "yyyymm")
company_id = a_company_id
ds = create uo_ds_top
		
		  
	sqls = "select PP_INTERFACE.interface_id, nvl(required_for_closing, 0) required_for_closing from pp_interface, pp_interface_dates " + &
			"where needed_for_closing = 1 and lower(subsystem) = 'asset management' and PP_INTERFACE.company_id = " + String(company_id) + &
			" and PP_INTERFACE_DATES.INTERFACE_ID = PP_INTERFACE.INTERFACE_ID" + &  
			" and PP_INTERFACE_DATES.COMPANY_ID = PP_INTERFACE.COMPANY_ID"  + & 
			" and to_char(PP_INTERFACE_DATES.ACCOUNTING_MONTH, 'yyyymm') = " + month_str  
	if f_create_dynamic_ds( ds, 'grid', sqls, sqlca, true) = 'ERROR' then 
		f_pp_msgs("Could not create datastore")
		goto quit
	end if
	
	for i = 1 to ds.rowCount()
		interface_id = 		ds.getItemNumber(i, 1)
		required_for_closing = ds.getItemNumber(i, 2)

		if required_for_closing = 1 then
				
			select last_run into :last_run  
			from pp_interface_dates
			where interface_id = :interface_id
				and company_id = :company_id
				and to_char(accounting_month, 'yyyymm') = :month_str;
			
			if isnull(last_run) then
				select description into :company_descr from company where company_id = :company_id;
				
				select description into :description from pp_interface where interface_id = :interface_id and company_id = :company_id;
				f_pp_msgs("Interface " + description + " has to be run for company " +  company_descr + " before closing CPR.")
				destroy ds
				return 1
			end if
		end if //required_for_closing = 1
	next		
	destroy ds

return 0

quit: 
		destroy ds
		return -1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppdepr_interface_custom_version nvo_ppdepr_interface_custom_version

choose case a_pbd_name
	case 'ppdepr_interface_custom.pbd'
		return nvo_ppdepr_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

