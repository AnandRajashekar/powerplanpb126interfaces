HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_new_month.exe'

nvo_wo_control i_wo_control

longlong i_company_ids[]
boolean i_check_oh[], i_check_accruals[], i_check_ret[], i_check_non_unit[], i_check_unit[]
date i_old_month, i_new_month
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function longlong uf_do_warnings (longlong a_company_id, date a_month, boolean a_check_oh, boolean a_check_accrual, boolean a_check_auto_ret, boolean a_check_auto_non_unit, boolean a_check_auto_unit)
public function longlong uf_warning_messaging (string a_process_description, boolean a_done, boolean a_hard_error)
public function longlong uf_new_month_company (longlong a_company_id, date a_old_month, boolean a_check_oh, boolean a_check_accruals, boolean a_check_ret, boolean a_check_non_unit, boolean a_check_unit)
public subroutine uf_check_option_array (longlong a_company_ids[], ref boolean a_option_values[], longlong a_option_id, string a_option_description)
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

// Declare variables
longlong rtn_success, rtn_failure, c, pp_stat_id, month, year, last_month_number, new_month_number, i, company_id, rc, &
	rtn, month_number
string comp_descr, rollback_segment, sqls, month_string, user_id, process_msg
date nulldate
boolean vfy_users

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

//instantiate any objects needed
uo_ds_top ds_users

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

//call constructor on i_wo_control
i_wo_control.of_constructor()
 
//Populate instance variables with arguments from SSP parms
i_company_ids = g_ssp_parms.long_arg
i_old_month = g_ssp_parms.date_arg[1]
i_check_oh = g_ssp_parms.boolean_arg
i_check_accruals = g_ssp_parms.boolean_arg2
i_check_ret = g_ssp_parms.boolean_arg3
i_check_non_unit = g_ssp_parms.boolean_arg4
i_check_unit = g_ssp_parms.boolean_arg5

//setup i_wo_control
rtn = i_wo_control.of_setupfromcompaniesandmonth(i_company_ids, i_old_month)

if rtn <> 1 then 
	f_pp_msgs("An error occurred when setting up the interface. Review logs for additional information.")
	return rtn_failure
end if

//get new month based on old month + 1 month
if month(i_old_month) = 12 then 
	//Our old month is december, which means that the new month is Jan of the next year
	i_new_month = date(year(i_old_month) + 1, 1, 1) 
else 
	//Not december - just increment month
	i_new_month = date(year(i_old_month), month(i_old_month) + 1, 1)
end if
 
 // lock the process for concurrency purposes
month_number = year(date(i_wo_control.i_month))*100 + month(date(i_wo_control.i_month))
if (i_wo_control.of_lockprocess( i_exe_name,  i_company_ids,month_number, process_msg)) = false then
          f_pp_msgs("New Month - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return rtn_failure
end if 
 
// check if the multiple companies chosen can be processed  together
if (i_wo_control.of_selectedcompanies() = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	return rtn_failure
end if

//Check boolean inputs to see if they were included. 
uf_check_option_array(i_company_ids,i_check_oh,1,'Continue if Overheads and AFUDC have not been approved?')
uf_check_option_array(i_company_ids,i_check_accruals,2,'Continue if Accruals have not been approved?')
uf_check_option_array(i_company_ids,i_check_ret,3,'Continue if Automatic Retirements have not completed?')
uf_check_option_array(i_company_ids,i_check_non_unit,4,'Continue if Auto Non-Unitization has not completed?')
uf_check_option_array(i_company_ids,i_check_unit,5,'Continue if Auto Unitization has not completed?')

//	loop over the array of IDs, and react to the selected rows
longlong num_elements
num_elements = upperbound(i_company_ids) 
for c = 1 to num_elements 
	company_id = i_company_ids[c]

	//	it's selected, update the rest of the window
	i_wo_control.of_companychanged(c, datetime(i_old_month))

	if uf_new_month_company(i_wo_control.i_company, i_new_month, i_check_oh[c], i_check_accruals[c], i_check_ret[c], i_check_non_unit[c], i_check_unit[c]) = 1 then 
		commit;
	
		i_wo_control.of_retrievecompany()	
	else
		rollback using sqlca;
		f_pp_msgs("Error adding a new month")
		return rtn_failure
	end if 
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)

next  //End loop over company (begins around line 70)
		
if i_wo_control.of_checksystemcontrol( 10 ) = true then
	ds_users = create uo_ds_top
	vfy_users = false
	select nvl(user_sql, '') into :sqls from pp_verify where lower(description) = 'email wo close: new month';

	if sqls <> "" then
		if f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true) = "OK" then
			if ds_users.RowCount() > 0 then
				vfy_users = true
			end if
		end if
	end if
	
	if vfy_users = true then
		for i=1 to ds_users.RowCount()
			user_id = ds_users.GetItemString( i, 1)
			f_send_mail( user_id, "New Month completed", &
				"New Month completed on " + String(Today(), "mm/dd/yyyy") + &
				" at " + String(Today(), "hh:mm:ss"), user_id)				
		next
	else
		f_send_mail( s_user_info.user_id, "New Month completed", &
		"New Month completed on " + String(Today(), "mm/dd/yyyy") + &
		" at " + String(Today(), "hh:mm:ss"), s_user_info.user_id)
	end if

end if

i_wo_control.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function longlong uf_do_warnings (longlong a_company_id, date a_month, boolean a_check_oh, boolean a_check_accrual, boolean a_check_auto_ret, boolean a_check_auto_non_unit, boolean a_check_auto_unit);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_do_warnings
//
//   Purpose  :  This function is specific to the Work Order New Month interface. It validates whether or not certain other 
//					processes have already been run for the company and month, and either prints a warning message
//					to the online logs or halts execution completely depending on the value of the booleans passed in. 
// 
//	Return Values	:	-1 - hard stop - do not continue processing
//							 1 - continue processing
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  01-06-2015      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

date d_overhead_approval, d_accrual_approval, d_auto_retirements, d_auto_non_unitized, d_auto_close
boolean overhead_approval_done, accrual_approval_done, auto_retirements_done, auto_non_unitized_done, auto_close_done
longlong rtn_oh, rtn_accrual, rtn_ret, rtn_non_unit, rtn_unit
string wo_accr_process_ans
w_datawindows w_dw


//initialize all dates to null
setnull(d_overhead_approval)
setnull(d_accrual_approval)
setnull(d_auto_retirements)
setnull(d_auto_non_unitized)
setnull(d_auto_close)

//initialize booleans to true
overhead_approval_done = true
accrual_approval_done = true 
auto_retirements_done = true 
auto_non_unitized_done = true 
auto_close_done = true

//initialize return values for each validation to 1 
rtn_oh = 1
rtn_accrual = 1
rtn_ret = 1
rtn_non_unit = 1
rtn_unit = 1

//retrieve dates from database 
SELECT overhead_approval, 
	accrual_approval, 
	auto_retirements, 
	auto_non_unitized, 
	auto_close
INTO :d_overhead_approval, 
	:d_accrual_approval, 
	:d_auto_retirements, 
	:d_auto_non_unitized, 
	:d_auto_close
FROM wo_process_control wpc
WHERE wpc.company_id = :a_company_id
AND wpc.accounting_month = :a_month;

//perform validations
if isnull(d_overhead_approval) then overhead_approval_done = false
if isnull(d_accrual_approval) then accrual_approval_done = false
if isnull(d_auto_retirements) then auto_retirements_done = false
if isnull(d_auto_non_unitized) then auto_non_unitized_done = false
if isnull(d_auto_close) then auto_close_done = false

//Do messaging and set return_value
if overhead_approval_done = false &
	or accrual_approval_done = false & 
	or auto_retirements_done = false &
	or auto_non_unitized_done = false &
	or auto_close_done = false &
then 
	f_pp_msgs("The following processes have not been completed: ")
	
	//Overheads and AFUDC Approval
	rtn_oh = uf_warning_messaging("Overheads and AFUDC Approval", overhead_approval_done, a_check_oh)
	
	//Approve Accruals - need to check system control first to see if we need to even do the check. 
	wo_accr_process_ans = f_pp_system_control_company("Work Order Accrual Calculation",a_company_id)
	if isnull(wo_accr_process_ans) or wo_accr_process_ans = "" then wo_accr_process_ans = 'no'
		
	if wo_accr_process_ans = 'yes' then 
		rtn_accrual = uf_warning_messaging("Approve Accruals", accrual_approval_done, a_check_accrual)
	else 
		f_pp_msgs('Approve Accruals not validated because the "Work Order Accrual Calculation" system control is not set to "Yes"')
		rtn_accrual = 1 //hardcode to success status
	end if
	
	//Retirement Transactions
	rtn_ret = uf_warning_messaging("Retirement Transactions", auto_retirements_done, a_check_auto_ret)

	//Automatic Non-Unitized
	rtn_non_unit = uf_warning_messaging("Automatic Non-Unitized", auto_non_unitized_done, a_check_auto_non_unit)	
	
	//Automatic Unitization
	rtn_unit = uf_warning_messaging("Automatic Unitization", auto_close_done, a_check_auto_unit)
end if 

if rtn_oh <> 1 or rtn_accrual <> 1 or rtn_ret <> 1 or rtn_non_unit <> 1 or rtn_unit <> 1 then
	return -1
else
	return 1
end if
end function

public function longlong uf_warning_messaging (string a_process_description, boolean a_done, boolean a_hard_error);//Utility function to avoid copy and pasting this messaging and validation logic a bunch of times

longlong return_value 

return_value = 1

if a_done = false then 
	if a_hard_error = true then 
		//hard stop - message and set return_value to -1
		f_pp_msgs("	" + a_process_description + " (hard stop - New Month process cannot continue)")
		return_value = -1
	else 
		//soft warning - message only
		f_pp_msgs("	" + a_process_description + " (warning - New Month process will continue)")
	end if 
end if // No else - if the process was completed, nothing to do

return return_value
end function

public function longlong uf_new_month_company (longlong a_company_id, date a_old_month, boolean a_check_oh, boolean a_check_accruals, boolean a_check_ret, boolean a_check_non_unit, boolean a_check_unit);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_new_month_company
//
//   Purpose  :  This function does the meat of the work for a 
// 
//	Arguments		: 	(long)		a_company_id	:	The company to open the new month for 
//							(date)	 	a_old_month	:	The last current month prior to the new month being opened
//
//	Return Values	:	-1 - error
//							 1 - no error
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  01-06-2015      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

longlong rtn_failure, rtn_success, pp_stat_id, rtn
string comp_descr, rollback_segment, sqls

//initialize rtn values
rtn_failure = -1
rtn_success = 1


f_pp_msgs("  " + string(a_company_id))

//f_wo_status_box("Add New Month ", "Month: " + string(i_month_number) + " Company:  " + a_company_id + " Comp Id: " + string(a_company_id))

select description  into :comp_descr from company where company_id = :a_company_id;

f_pp_msgs("Add New Month started  for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))

pp_stat_id = f_pp_statistics_start("Add new Month", "Close PowerPlant: " + comp_descr)

if uf_do_warnings(a_company_id, i_old_month, a_check_oh, a_check_accruals, a_check_ret, a_check_non_unit, a_check_unit) = -1 then 
	//One or more of our warnings ended up being a hard-stop. Logging done within the uf_do_warnings button - none needed here
	return rtn_failure
end if

//Check to make sure that our new month does not already exist in wo_process_control
longlong new_month_check
SELECT 1
INTO :new_month_check
FROM wo_process_control
WHERE company_id = :a_company_id
AND accounting_month = :i_new_month;

if sqlca.sqlcode <> 100 then 
	f_pp_msgs("ERROR: The month that the interface is trying to open already exists in WO_PROCESS_CONTROL.")
	return rtn_failure
end if 

//Check to make sure that our new month does not already exist in pp_interface_dates
SELECT count(1)
INTO :new_month_check
FROM pp_interface_dates pid, pp_interface pi
WHERE pid.company_id = pi.company_id
AND pid.interface_id = pi.interface_id 
AND pid.company_id = :a_company_id
AND pid.accounting_month = :i_new_month
AND pi.subsystem = 'Project Management'
AND pi.needed_for_closing = 1;

if new_month_check <> 0 then 
	f_pp_msgs("ERROR: The month that the interface is trying to open already exists in PP_INTERFACE_DATES. Company_id: " + string(a_company_id) + " Month: " + string(i_new_month))
	return rtn_failure
end if 

//
//  Set the large rollback segment, if one is defined ...
//
rollback_segment = f_pp_system_control_company('rollback segment', a_company_id)

if isnull(rollback_segment) or rollback_segment = "" then
else
	sqls = 'Set transaction use rollback segment ' + rollback_segment 
	execute immediate :sqls;
end if	
	
	//CBS - all this commented out logic is not needed on the interface side. 
//	dw_cpr_control_hide.settransobject(sqlca)
//	
//	// copies a row from one datawindow to another
//	for i = 1 to dw_interface_dates1.rowcount()
//		f_copy_info(dw_interface_dates3,dw_interface_dates4,i,i)
//		f_copy_info(dw_interface_dates2,dw_interface_dates3,i,i)
//		f_copy_info(dw_interface_dates1,dw_interface_dates2,i,i)
//	next
//	
//	f_copy_info(dw_wo_process_control3,dw_interface_dates4,1,1)
//	f_copy_info(dw_wo_process_control2,dw_interface_dates3,1,1)
//	f_copy_info(dw_wo_process_control1,dw_interface_dates2,1,1)
//	
//	dw_wo_process_control1.reset()
//	dw_wo_process_control1.insertrow(0)
//	
//	f_copy_info(dw_interface_dates2,dw_interface_dates1,1,1)
//	
//	old_month = i_wo_control.i_months[1]
//	
//	month = month(date(i_wo_control.i_months[1]))
//	year  = year(date(i_wo_control.i_months[1]))
//	
//	last_month_number = (year * 100) + month
//	
//	month = month + 1
//	
//	if month = 13 then 
//		month = 1
//		year = year + 1
//	end if
//	
//	new_month_number = (year * 100) + month
//	
//	new_month = datetime(date(string(month) + '/1/' + string(year)))
	
//	Select month into :month_string from pp_table_months where month_num = :month;

//CBS TODO: add records to WO_PROCESS_CONTROL and PP_INTERFACE_DATES using SQL instead of DWs. 
INSERT INTO wo_process_control (company_id, accounting_month)
VALUES (:a_company_id, :i_new_month);

if sqlca.sqlcode < 0 then 
	//SQL error
	f_pp_msgs("ERROR: Could not insert new record into WO_PROCESS_CONTROL for company_id " + &
						string(a_company_id) + " and accounting_month " + string(i_new_month) + ". SQL Error: " + sqlca.sqlerrtext)
	return rtn_failure
end if 

INSERT INTO pp_interface_dates (interface_id, company_id, accounting_month)
SELECT pid.interface_id, pid.company_id, :i_new_month
FROM pp_interface_dates pid, pp_interface pi
WHERE pid.interface_id = pi.interface_id
AND pid.company_id = pi.company_id
AND pi.needed_for_closing = 1
AND pi.subsystem = 'Project Management'
AND pid.company_id = :a_company_id
AND pid.accounting_month = :i_old_month; 

if sqlca.sqlcode < 0 then 
	//SQL error
	f_pp_msgs("ERROR: Could not insert new record into PP_INTERFACE_DATES for company_id " + &
						string(a_company_id) + " and accounting_month " + string(i_new_month) + ". SQL Error: " + sqlca.sqlerrtext)
	return rtn_failure
end if 

//CBS - Leave this in because I don't know if nvo_wo_control needs the dates moved in the background.  
i_wo_control.i_months[4] = i_wo_control.i_months[3]
i_wo_control.i_months[3] = i_wo_control.i_months[2]
i_wo_control.i_months[2] = i_wo_control.i_months[1]
i_wo_control.i_months[1] = datetime(i_new_month)
i_wo_control.i_month     = datetime(i_new_month)


//*********************************************
//  Re-build the summaries ...
//*********************************************
rtn = f_update_wo_summary_month(a_company_id, year(a_old_month)*100 + month(a_old_month))

if rtn = -1 then
	f_pp_msgs("ERROR: updating wo_summary ")
	return rtn_failure
end if

insert into clearing_wo_rate (clearing_id,accounting_month,input_rate,calc_rate)
(select clearing_wo_rate.clearing_id, :i_new_month, 
 nvl(clearing_wo_rate.input_rate,0.00000),0.00000
 from clearing_wo_rate,  clearing_wo_control 
 where clearing_wo_rate.clearing_id = clearing_wo_control.clearing_id and 
 to_char(accounting_month,'mmyyyy') = to_char(:i_old_month,'mmyyyy') and 
 ((clearing_department in (
	 select 	department_id from department 
	 where division_id in (select division_id from division where company_id = :a_company_id)))
	 or clearing_wo_control.company_id = :a_company_id)
);

if sqlca.SQLCode < 0 then
	f_pp_msgs( "Error rolling prior month forward in clearing_wo_rate. ~n" +&
				string(sqlca.SQLDBCode) + ", " + sqlca.SQLErrText)
	return rtn_failure
end if 


return rtn_success 
end function

public subroutine uf_check_option_array (longlong a_company_ids[], ref boolean a_option_values[], longlong a_option_id, string a_option_description);boolean check_month_end_option
longlong i

for i = 1 to upperbound(a_company_ids)
	//make sure boolean_arg is populated
	if upperbound(a_option_values) < i then 
		check_month_end_option = true
	elseif isnull(a_option_values[i]) then //having this as a separate ELSEIF is important. If we used an OR in the IF clause, an array out of bounds exception occurs.
		check_month_end_option = true
	else 
		check_month_end_option = false
	end if
	
	if check_month_end_option = true then 
		f_pp_msgs("Checking month end option for '" + a_option_description + "'")	
		try
			if lower(f_pp_month_end_options(a_company_ids[i], g_process_id, a_option_id)) = 'yes' then
				f_pp_msgs("'" + a_option_description + "' set to Yes.")	
				a_option_values[i] = false
			else 
				f_pp_msgs("'" + a_option_description + "' set to No.")	
				a_option_values[i] = true
			end if
		catch (exception e)
			f_pp_msgs("Could not find value for '" + a_option_description + "' month end option. Defaulting to 'No'.")
			a_option_values[i] = true	
		end try
	end if 
next
end subroutine

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_ppprojct_custom_version nvo_ppprojct_custom_version

choose case a_pbd_name
	case 'ppprojct_custom.pbd'
		return nvo_ppprojct_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

