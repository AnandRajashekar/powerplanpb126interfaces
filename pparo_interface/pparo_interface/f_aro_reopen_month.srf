HA$PBExportHeader$f_aro_reopen_month.srf
$PBExportComments$Maint 6577: added JE_METHOD_ID
global type f_aro_reopen_month from function_object
end type

forward prototypes
global function any f_aro_reopen_month (longlong a_company_id, datetime a_month)
end prototypes

global function any f_aro_reopen_month (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	f_aro_reopen_month()
 **	
 **	Reopens ARO processing for a company and month.
 **	
 **	This function is currently called from 
 **		- w_cpr_control - cb_close_cpr.clicked()
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to reopen.
 **						datetime		:	( a_month ) The month to reopen.
 **	
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong	month_number
string			je_code, company_num
datetime		gl_posting_mo_yr

////
////	Get the company number.
////
		setNull( company_num )
		
		select 	gl_company_no 
		into 		:company_num 
		from 		company 
		where 	company_id = :a_company_id;

////
////	Set the GL Posting Mo Yr and convert it to a month number.
////
		gl_posting_mo_yr = a_month
		
		month_number = year( date( gl_posting_mo_yr ) ) * 100 + month( date( gl_posting_mo_yr ) )

////
////	Get the JE code for ARO.
////
		setNull( je_code )
		
		select 	standard_journal_entries.gl_je_code 
		into 		:je_code
		from 		standard_journal_entries, 
					gl_je_control
		where 	gl_je_control.process_id = 'ARO'
		and 		gl_je_control.je_id = standard_journal_entries.je_id;
	
		if isNull( je_code ) or je_code = "" then
			f_status_box( "ARO Approval", "Cannot Find GL JE CODE for the ARO Process." )
			return -1
		end if

////
////	Insert reversals into GL transaction.
////
 		insert into gl_transaction
		( gl_trans_id, month, company_number, gl_account, amount, gl_je_code, gl_status_id, description, debit_credit_indicator, source, originator, comments, pend_trans_id, asset_id, amount_type, tax_orig_month_number, je_method_id, trans_type )
		select 	pwrplant1.nextval gl_trans_id, 
					month month, 
					company_number company_number, 
					gl_account gl_account, 
					amount amount, 
					gl_je_code gl_je_code,
					1 gl_status_id, 
					ltrim( description || ':Reversal', 254 ) description,
					decode( debit_credit_indicator, 0, 1, 1, 0 ) debit_credit_indicator,
					source source,
					originator originator,
					comments comments,
					pend_trans_id pend_trans_id,
					asset_id asset_id,
					amount_type amount_type,
					tax_orig_month_number tax_orig_month_number, 
					je_method_id je_method_id, 
					trans_type trans_type
		from 		gl_transaction
		where 	company_number = :company_num
		and 		month = :a_month 
		and 		source = 'ARO CURRENT';
		
		if sqlca.SQLCode <> 0 then
			f_status_box( "Information", "Error occurred while inserting reversals into GL Transaction for Current Month.  Error: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if			
		
		insert into gl_transaction
		( gl_trans_id, month, company_number, gl_account, amount, gl_je_code, gl_status_id, description, debit_credit_indicator, source, originator, comments, pend_trans_id, asset_id, amount_type, tax_orig_month_number, je_method_id, trans_type )
		select 	pwrplant1.nextval gl_trans_id, 
					month month, 
					company_number company_number, 
					gl_account gl_account, 
					amount amount, 
					gl_je_code gl_je_code,
					1 gl_status_id, 
					ltrim( description || ':Reversal', 254 ) description,
					decode( debit_credit_indicator, 0, 1, 1, 0 ) debit_credit_indicator,
					source source,
					originator originator,
					comments comments,
					pend_trans_id pend_trans_id,
					asset_id asset_id,
					amount_type amount_type,
					tax_orig_month_number tax_orig_month_number, 
					je_method_id je_method_id, 
					trans_type trans_type
		from 		gl_transaction
		where 	company_number = :company_num
		and 		month = add_months( :a_month, 1 )
		and 		source = 'ARO FUTURE'; 
		
		if sqlca.SQLCode <> 0 then
			f_status_box( "Information", "Error occurred while inserting reversals into GL Transaction for Next Month.  Error: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if			
		
////
////	Update the status on ARO Liability.
////
		update 	aro_liability
		set 		status = 8
		where 	aro_id in
					( select aro_id from aro where company_id = :a_company_id )
		and 		to_char( month_yr, 'yyyymm' ) = to_char( :a_month, 'yyyymm' );
		
		if sqlca.SQLCode < 0 then
			f_status_box( "ARO Processing", "Error occurred while setting the status on ARO Liability for Current Month.  Error: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if
			
		update 	aro_liability
		set 		status = 9 
		where 	aro_id in 
					( select aro_id from aro where company_id = :a_company_id )
		and 		to_char( month_yr, 'yyyymm' ) = to_char( add_months( :a_month, 1 ), 'yyyymm' );
		
		if sqlca.SQLCode < 0 then
			f_status_box( "ARO Processing", "Error occurred while setting the status on ARO Liability for Next Month.  Error: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if
		
////
////	Function completed successfully.
////
		return 1
end function

