HA$PBExportHeader$nvo_aro_reg.sru
forward
global type nvo_aro_reg from nonvisualobject
end type
end forward

global type nvo_aro_reg from nonvisualobject
end type
global nvo_aro_reg nvo_aro_reg

type variables
datetime i_month
longlong i_company_id
boolean i_pp_msgs
end variables

forward prototypes
public function longlong uf_calc_aro_reg (longlong a_company_id, datetime a_month)
public function longlong uf_get_factors (ref datastore a_ds)
public function longlong uf_calc_aro_reg_for_sob (ref datastore a_ds_factors, ref datastore a_ds_pend_act, longlong a_sob)
public function longlong uf_update_depr_ledger ()
public function longlong uf_insert_pend_act (ref datastore a_ds_pend_act)
public function longlong uf_validate ()
public function longlong uf_close_to_depr_ledger (longlong a_company_id, datetime a_month)
public function longlong uf_flag_used_trans_sets ()
public function longlong uf_create_jes (longlong a_company_id, datetime a_month)
public function string uf_pp_gl_trans (longlong a_activity, longlong a_ldg_asset_id, longlong a_ldg_asset_act_id, longlong a_ldg_depr_group_id, longlong a_ldg_work_order_id, longlong a_ldg_gl_account_id, longlong a_gain_loss_ind, longlong a_ldg_pend_trans_id, longlong a_je_method_id, longlong a_je_set_of_books_id, longlong a_je_reversal, longlong a_je_amount_type)
public function string uf_db_calc (longlong a_company_id, datetime a_month)
public function string uf_db_approve (longlong a_company_id, datetime a_month)
public function boolean uf_can_approve_depr (longlong a_company_id, datetime a_month, ref string a_msg)
end prototypes

public function longlong uf_calc_aro_reg (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	uf_calc_aro_reg()
 **	
 **	Calculates the amount to be inserted into depr_ledger (from Regulated ARO's) for a company and month
 **	No commits in this function
 **	
 **	This function is currently called from 
 **		- w_cpr_control - cb_aro_calc.clicked()
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	longlong	:	1		:	The calculation was successful
 **										:	-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
longlong rtn, i, j, sob_ids[], num_factors, num_sobs
longlong aro_id, new_row, max_row, num_act
decimal factor, sum_factor, aro_amount, aro_amount_accounted_for, ledger_amount, max_amount, dbug[]
string sqls, rtn_str
any results[]
uo_ds_top ds_factors, ds_pend_act
datawindow dw_pend_act

i_company_id = a_company_id
i_month = a_month

//
//	Validation
//
rtn = uf_validate()
if rtn <> 1 then
	return -1
end if

//
//	Setup a datastore to hold new pending activities which we will insert at the end
//
ds_pend_act = create uo_ds_top
ds_pend_act.DataObject = "dw_aro_reg_activity_insert"
ds_pend_act.SetTransObject(sqlca)

//
//	Retrieve the allocation factors for all ARO's in this company/month
//
ds_factors = create uo_ds_top
rtn = uf_get_factors(ds_factors)
if rtn <> 1 then
	destroy ds_pend_act
	destroy ds_factors
	return -1
end if
if ds_factors.rowcount() = 0 then
	destroy ds_pend_act
	destroy ds_factors
	return 1
end if

//
//	Run the calculation for each set of books
//
num_sobs = f_get_column(results, "select set_of_books_id from set_of_books order by set_of_books_id asc")
sob_ids = results
for i = 1 to num_sobs
	rtn = uf_calc_aro_reg_for_sob(ds_factors, ds_pend_act, sob_ids[i])
	if rtn <> 1 then
		destroy ds_pend_act
		destroy ds_factors
		return -1
	end if
next

//
//	Insert pending activities
//
num_act = ds_pend_act.Rowcount()
rtn = uf_insert_pend_act(ds_pend_act)
if rtn <> 1 then
	destroy ds_pend_act
	destroy ds_factors
	return -1
end if

//
// Flag any used depr trans sets
//
rtn = uf_flag_used_trans_sets()
if rtn <> 1 then
	destroy ds_pend_act
	destroy ds_factors
	return -1
end if

destroy ds_pend_act
destroy ds_factors
return 1
end function

public function longlong uf_get_factors (ref datastore a_ds);/*************************************
*
*	Notes:
*	- The logic for determining the factor amount came from dw_depr_trans_set_dg_update
*	- If you change the decode function for the "factor" column, make sure you change it for "sum_factor" as well
*	- For 10.4.2.0, we only allow Manual and Current Asset Balance.  The other 3 options are not allowed because COR fields should be empty.
*		These options may be added in a future release.
*
**************************************/

string sqls, rtn_str
longlong i, rowcount

sqls = "select aro.aro_id aro_id, ts.depr_trans_set_id depr_trans_set_id, tsdg.depr_group_id depr_group_id, dl.set_of_books_id set_of_books_id,"
sqls += "			decode( ts.depr_trans_allo_method_id, "
sqls += "                1, tsdg.factor, " //Manual
sqls += "                2, dl.begin_balance * (1 - dmr.net_salvage_pct + dmr.cost_of_removal_pct) - dl.begin_reserve - dl.cor_beg_reserve," //Current Asset NBV
sqls += "                3, begin_balance," //Current Asset Balance
sqls += "                4, cor_beg_reserve," //Current Reserve Balance
sqls += "                5, dl.cor_expense + dl.cor_exp_adjust + dl.cor_exp_alloc_adjust " //Current Month Expense
sqls += "				) factor,"
sqls += "			sum(decode( ts.depr_trans_allo_method_id, "
sqls += "                1, tsdg.factor, "
sqls += "                2, dl.begin_balance * (1 - dmr.net_salvage_pct + dmr.cost_of_removal_pct) - dl.begin_reserve - dl.cor_beg_reserve,"
sqls += "                3, begin_balance,"
sqls += "                4, cor_beg_reserve,"
sqls += "                5, dl.cor_expense + dl.cor_exp_adjust + dl.cor_exp_alloc_adjust "
sqls += "				)) over (partition by aro.aro_id, dl.set_of_books_id) sum_factor,"
sqls += "			count(1) over (partition by aro.aro_id, dl.set_of_books_id) factor_cnt,"
sqls += "			liab.settled - ("
sqls += "				select nvl(sum(settlement_adjust),0)"
sqls += "				from aro_liability_adj"
sqls += "				where aro_id = aro.aro_id"
sqls += "				and depr_ledger_include = 0"
sqls += "			) aro_amount"
sqls += " from aro, "
sqls += " 	depr_trans_set ts, "
sqls += " 	depr_trans_set_dg tsdg, "
sqls += " 	depr_group dg, "
sqls += " 	depr_ledger dl, "
sqls += " 	depr_method_rates dmr, "
sqls += " 	aro_liability liab, "
sqls += " 	aro_sob_view asob"
sqls += " where ts.depr_trans_set_id = aro.depr_trans_set_id"
sqls += " and tsdg.depr_trans_set_id = ts.depr_trans_set_id"
sqls += " and dg.depr_group_id = tsdg.depr_group_id"
sqls += " and dl.depr_group_id = dg.depr_group_id"
sqls += " and dl.gl_post_mo_yr = to_date('" + string(i_month, 'yyyymm') + "', 'yyyymm')"
sqls += " and dmr.depr_method_id = dg.depr_method_id"
sqls += " and dmr.set_of_books_id = dl.set_of_books_id"
sqls += " and dmr.effective_date = (select max(x.effective_date) from depr_method_rates x"
sqls += "              where x.depr_method_id = dmr.depr_method_id "
sqls += "              and x.set_of_books_id = dmr.set_of_books_id"
sqls += "              and x.effective_date <= dl.gl_post_mo_yr)"
sqls += " and liab.aro_id = aro.aro_id"
sqls += " and liab.month_yr = dl.gl_post_mo_yr"
sqls += " and aro.regulatory_obligation = 1"
sqls += " and aro.company_id = " + string(i_company_id)
sqls += " and asob.aro_id = aro.aro_id"
sqls += " and asob.set_of_books_id = dl.set_of_books_id"
sqls += " order by aro.aro_id, dl.set_of_books_id, dg.depr_group_id"

rtn_str = f_create_dynamic_ds(a_ds, "grid", sqls, sqlca, true)
if f_pp_nvl(rtn_str, "") <> "OK" then
	return -1
end if

//
//	Do an even split if all the factors are zero
//
rowcount = a_ds.rowCount()
for i = 1 to rowcount
	if a_ds.getItemNumber(i, "sum_factor") = 0 then
		a_ds.setItem(i, "factor", 1)
		a_ds.setItem(i, "sum_factor", a_ds.getItemNumber(i, "factor_cnt"))
	end if
next

return 1
end function

public function longlong uf_calc_aro_reg_for_sob (ref datastore a_ds_factors, ref datastore a_ds_pend_act, longlong a_sob);longlong i, num_factors, aro_id, new_row, max_row
decimal factor, sum_factor, aro_amount, ledger_amount, max_amount, aro_amount_accounted_for
boolean last_row_for_aro

//
//	Filter to this set of books and ignore zero values
//
a_ds_factors.SetFilter("set_of_books_id = " + string(a_sob) + " and aro_amount <> 0")
a_ds_factors.Filter()
a_ds_factors.SetSort("aro_id, set_of_books_id, depr_group_id")
a_ds_factors.Sort()

//
//	Loop over a_ds_factors and insert into a_ds_pend_act
//	a_ds_pend_act.Update() will be called in another function to insert new activities
//
num_factors = a_ds_factors.rowcount()
for i = 1 to num_factors
	aro_id = a_ds_factors.GetItemNumber(i, "aro_id")
	
	factor = a_ds_factors.GetItemNumber(i, "factor")
	sum_factor = a_ds_factors.GetItemNumber(i, "sum_factor")
	aro_amount = a_ds_factors.GetItemNumber(i, "aro_amount")
	ledger_amount = aro_amount * factor / sum_factor
	aro_amount_accounted_for += round(ledger_amount,2)

	new_row = a_ds_pend_act.InsertRow(0)
	a_ds_pend_act.SetItem(new_row, "aro_id", a_ds_factors.GetItemNumber(i, "aro_id"))
	a_ds_pend_act.SetItem(new_row, "set_of_books_id", a_ds_factors.GetItemNumber(i, "set_of_books_id"))
	a_ds_pend_act.SetItem(new_row, "depr_group_id", a_ds_factors.GetItemNumber(i, "depr_group_id"))
	a_ds_pend_act.SetItem(new_row, "gl_post_mo_yr", i_month)
	a_ds_pend_act.SetItem(new_row, "amount", ledger_amount)
	
	if abs(ledger_amount) >= abs(max_amount) then
		max_amount = ledger_amount
		max_row = new_row
	end if
	
	//Make sure we account for potential rounding errors, and add pennies to the largest charge
	last_row_for_aro = false
	if i = num_factors then
		last_row_for_aro = true
	else
		if aro_id <> a_ds_factors.GetItemNumber(i+1, "aro_id") then
			last_row_for_aro = true
		else
			last_row_for_aro = false
		end if
	end if
	if last_row_for_aro then
		if aro_amount_accounted_for <> aro_amount then
			a_ds_pend_act.SetItem(max_row, "amount", max_amount + (aro_amount - aro_amount_accounted_for))
		end if
		
		//We are at the last row for this aro, so we need to reset these variables
		aro_amount_accounted_for = 0.00
		max_amount = 0.00
	end if
	
next

return 1
end function

public function longlong uf_update_depr_ledger ();//FUNCTION UNDER CONSTRUCTION
//DO NOT USE YET

/*
For now, I'm assuming this will update depr_ledger
Still not sure where this will get called
*/

update depr_ledger dl
set dl.cost_of_removal_base = dl.cost_of_removal_base + (
	select sum(pend.amount)
	from pend_aro_reg_activity pend
	where pend.set_of_books_id = dl.set_of_books_id
		and pend.depr_group_id = dl.depr_group_id
		and pend.gl_post_mo_yr = dl.gl_post_mo_yr
)
where exists (
	select 1
	from pend_aro_reg_activity pend
	where pend.set_of_books_id = dl.set_of_books_id
		and pend.depr_group_id = dl.depr_group_id
		and pend.gl_post_mo_yr = dl.gl_post_mo_yr
);
if sqlca.sqlcode <> 0 then
	return -1
end if

delete from pend_aro_reg_activity;
if sqlca.sqlcode <> 0 then
	return -1
end if

return 1
end function

public function longlong uf_insert_pend_act (ref datastore a_ds_pend_act);/************************************************************************************************************************************************************
 **
 **	uf_insert_pend_act()
 **	
 **	Inserts/Updates the pending activities in the datastore a_ds_pend_act
 **	This should just be a_ds_pend_act.update(), but that didn't work for some reason
 **	
 ************************************************************************************************************************************************************/
longlong i, num_act, rtn
string sqls, dbug

//
//	Delete all pending activities for this company and month
// The datawindow isn't smart enough to run an update.  We could have tricked it, but...
//
delete from pend_aro_reg_activity
where gl_post_mo_yr = :i_month
and aro_id in (
	select aro_id
	from aro
	where company_id = :i_company_id
);
if sqlca.sqlcode <> 0 then
	if i_pp_msgs then f_pp_msgs("     Database error while deleting old pending activities: " + sqlca.sqlerrtext)
	return -1
end if

//
//	Ideally we would just do this update statement...
//
rtn = a_ds_pend_act.Update()
if rtn <> 1 then
	dbug = sqlca.sqlerrtext
	return -1
end if


return 1
end function

public function longlong uf_validate ();/************************************************************************************************************************************************************
 **
 **	uf_validate()
 **	
 **	Validates that regulated ARO's are setup correctly
 **	
 **	This function is currently called from 
 **		- nvo_aro_reg.uf_calc_aro_reg
 **	
 **	Parameters	:	(None)
 **	
 **	Returns		:	longlong	:	1		:	The validation was successful
 **										:	-1		:	An error occurred/was found
 **
 ************************************************************************************************************************************************************/
longlong rtn, i, return_code
string msgs[], rtn_str

return_code = 1

//
//	Validate that all regulated ARO's have trans sets
//
rtn = 0

select count(1) into :rtn
from aro
where regulatory_obligation = 1
	and depr_trans_set_id is null
	and company_id = :i_company_id;
	
if sqlca.sqlcode <> 0 then
	msgs[upperBound(msgs)+1] = "An error occured while validating trans set relationships: " + sqlca.sqlerrtext
	return_code = -1
end if
if rtn > 0 then
	msgs[upperBound(msgs)+1] = "There are " + string(rtn) + " Regulated ARO's with no Depreciation Trans Set."
	msgs[upperBound(msgs)+1] = "    This issue must be fixed before ARO's can be processed."
	return_code = -1
end if

//
//	Validate there are depr_ledger rows for this company/month, so that we can foreign key to them
//
rtn = 0

select count(1)
into :rtn
from depr_ledger dl, depr_group dg
where dg.depr_group_id = dl.depr_group_id
and dg.company_id = :i_company_id
and dl.gl_post_mo_yr = :i_month;

if sqlca.sqlcode <> 0 then
	msgs[upperBound(msgs)+1] = "An error occured while validating depr ledger: " + sqlca.sqlerrtext
	return_code = -1
elseif rtn = 0 then
	msgs[upperBound(msgs)+1] = "There are no records in the depr ledger for this company and month."
	msgs[upperBound(msgs)+1] = "    Depr Ledger records are required for processing regulated ARO's."
	return_code = -1
end if

//
//	Validate that we only use Manual and Current Asset Balance trans sets
// This validation may be removed or modified if we add support for more trans set methods
//
rtn = 0

select count(1), min(aro.description)
into :rtn, :rtn_str
from aro, depr_trans_set ts
where ts.depr_trans_set_id = aro.depr_trans_set_id
and aro.company_id = :i_company_id
and aro.regulatory_obligation = 1
and ts.depr_trans_allo_method_id not in (1,3);

if sqlca.sqlcode <> 0 then
	msgs[upperBound(msgs)+1] = "An error occured while validating depr trans set types: " + sqlca.sqlerrtext
	return_code = -1
end if
if rtn > 0 then
	msgs[upperBound(msgs)+1] = "ARO '" + rtn_str + "' "
	if rtn > 1 then
		msgs[upperBound(msgs)] += "and " + string(rtn - 1) + " others are "
	else
		msgs[upperBound(msgs)] += "is "
	end if
	msgs[upperBound(msgs)] += "related to a Depr Trans Set with an invalid allocation method."
	msgs[upperBound(msgs)+1] = "    Only 'Manual' and 'Current Asset Balance' are allowed."
	return_code = -1
end if

//
//	Validate that all work orders on Reg ARO's have a Closing Option of "Clearing" (9)
//
rtn = 0
rtn_str = ""

select count(1), min(aro.description)
into :rtn, :rtn_str
from work_order_account woa, aro_work_order arowo, aro
where arowo.aro_id = aro.aro_id
and woa.work_order_id = arowo.work_order_id
and aro.regulatory_obligation = 1
and aro.company_id = :i_company_id
and woa.closing_option_id <> 9;

if sqlca.sqlcode <> 0 then
	msgs[upperBound(msgs)+1] = "An error occured while validating work order closing types: " + sqlca.sqlerrtext
	return_code = -1
end if
if rtn > 0 then
	msgs[upperBound(msgs)+1] = "ARO '" + rtn_str + "' "
	if rtn > 1 then
		msgs[upperBound(msgs)] += "and " + string(rtn - 1) + " others are "
	else
		msgs[upperBound(msgs)] += "is "
	end if
	msgs[upperBound(msgs)] += "related to a Work Order with an invalid closing option."
	msgs[upperBound(msgs)+1] = "    Regulated ARO work orders must have a closing option of 'Clearing.'"
	return_code = -1
end if

//
//	Print out error messages, if any
//
if i_pp_msgs then
	for i = 1 to upperBound(msgs)
		f_pp_msgs("          " + msgs[i])
	next
end if

return return_code
end function

public function longlong uf_close_to_depr_ledger (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	uf_close_to_depr_ledger()
 **	
 **	Updates the cost_of_removal on depr_ledger for a company and month
 **	Creates gl transactions
 **	No commits in this function
 **	
 **	This function is currently called from 
 **		- w_cpr_control.cb_aro_approval.clicked
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	longlong	:	1		:	The calculation was successful
 **										:	-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
longlong rtn

//
//	Update the depr_ledger with amounts from pend_aro_reg_activity
//
update depr_ledger dl
set cost_of_removal = cost_of_removal - (
	select nvl(sum(amount),0)
	from pend_aro_reg_activity pend
	where pend.set_of_books_id = dl.set_of_books_id
		and pend.depr_group_id = dl.depr_group_id
		and pend.gl_post_mo_yr = dl.gl_post_mo_yr
)
where gl_post_mo_yr = :a_month
and depr_group_id in (select depr_group_id from depr_group where company_id = :a_company_id)
and exists (
	select 1
	from pend_aro_reg_activity pend2
	where pend2.set_of_books_id = dl.set_of_books_id
		and pend2.depr_group_id = dl.depr_group_id
		and pend2.gl_post_mo_yr = dl.gl_post_mo_yr
);
if sqlca.sqlcode <> 0 then
	return -1
end if

//
//	Create Journal Entries
//
rtn = uf_create_jes(a_company_id, a_month)
if rtn <> 1 then
	return -1
end if


//
//	Clear the archived activities
//
delete from pend_aro_reg_activity_arc
where gl_post_mo_yr = :a_month
and depr_group_id in (select depr_group_id from depr_group where company_id = :a_company_id);
if sqlca.sqlcode <> 0 then
	return -1
end if

//
//	Archive the current pending activities
//
insert into pend_aro_reg_activity_arc (ARO_ID, SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR, AMOUNT)
select ARO_ID, SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR, AMOUNT
from pend_aro_reg_activity
where gl_post_mo_yr = :a_month
and depr_group_id in (select depr_group_id from depr_group where company_id = :a_company_id);
if sqlca.sqlcode <> 0 then
	return -1
end if

//
//	Clear the current activities
//
delete from pend_aro_reg_activity
where gl_post_mo_yr = :a_month
and depr_group_id in (select depr_group_id from depr_group where company_id = :a_company_id);
if sqlca.sqlcode <> 0 then
	return -1
end if

return 1
end function

public function longlong uf_flag_used_trans_sets ();//
//	Write update statement 
//
update depr_trans_set
set used_in_aro_calc = 1
where depr_trans_set_id in (
	select aro.depr_trans_set_id
	from aro, pend_aro_reg_activity pend
	where pend.aro_id = aro.aro_id
		and aro.company_id = :i_company_id
		and pend.gl_post_mo_yr = :i_month
)
and nvl(used_in_aro_calc,0) = 0;

if sqlca.sqlcode <> 0 then
	return -1
end if

return 1
end function

public function longlong uf_create_jes (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	uf_create_jes()
 **	
 **	Runs through pending regulated ARO activities and creates journal entries
 **	No commits in this function
 **	
 **	This function is currently called from 
 **		- nvo_aro_reg.uf_close_to_depr_ledger()
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	longlong	:	1		:	The calculation was successful
 **										:	-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
longlong rtn, i, rowcount
longlong asset_id, asset_act_id, depr_group_id, wo_id, credit_acc, debit_acc, gain_loss, pend_trans_id, je_method_id, sob_id, reversal, amount_type, credit_trans_type, debit_trans_type
decimal amount
string sqls, gl_account_str_credit, gl_account_str_debit
uo_ds_top ds_pend_act

//
//	Initialize datastore for processing
//
ds_pend_act = create uo_ds_top
sqls = "select pend.set_of_books_id,"
sqls+= "	  pend.depr_group_id,"
sqls+= "	  dg.cor_reserve_acct_id,"
sqls+= "	  aro.cor_credit_acct_id,"
sqls+= "	  pend.amount,"
sqls+= "	  aro.asset_id,"
sqls+= "	  msob.reversal_convention,"
sqls+= "	  m.amount_type,"
sqls+= "	  m.je_method_id"
sqls+= "	from pend_aro_reg_activity pend,"
sqls+= "	  aro,"
sqls+= "	  depr_group dg,"
sqls+= "	  je_method m,"
sqls+= "	  je_method_set_of_books msob,"
sqls+= "	  je_method_trans_type mtype"
sqls+= "	where aro.aro_id = pend.aro_id"
sqls+= "	and dg.depr_group_id = pend.depr_group_id"
sqls+= "	and m.je_method_id = mtype.je_method_id"
sqls+= "	and msob.set_of_books_id = pend.set_of_books_id"
sqls+= "	and msob.je_method_id = m.je_method_id"
sqls+= "	and mtype.trans_type = 41"
sqls+= "	and aro.company_id = " + string(a_company_id)
sqls+= "	and pend.gl_post_mo_yr = to_date(" + string(a_month, "yyyymm") + ", 'yyyymm')"
f_create_dynamic_ds(ds_pend_act, "grid", sqls, sqlca, true)

//
//	For each pending activity, create the gl_account_str and insert
//
rowcount = ds_pend_act.rowCount()
for i = 1 to rowcount
	asset_id = ds_pend_act.GetItemNumber(i, "asset_id")
	depr_group_id = ds_pend_act.GetItemNumber(i, "depr_group_id")
	debit_acc = ds_pend_act.GetItemNumber(i, "cor_reserve_acct_id")
	credit_acc = ds_pend_act.GetItemNumber(i, "cor_credit_acct_id")
	je_method_id = ds_pend_act.GetItemNumber(i, "je_method_id")
	sob_id = ds_pend_act.GetItemNumber(i, "set_of_books_id")
	reversal = ds_pend_act.GetItemNumber(i, "reversal_convention")
	amount_type = ds_pend_act.GetItemNumber(i, "amount_type")
	amount = ds_pend_act.GetItemNumber(i, "amount")
	
	gain_loss = 0
	wo_id = 0
	pend_trans_id = 0
	asset_act_id = 0
	
	debit_trans_type = 41
	credit_trans_type = 42
	
	gl_account_str_debit = uf_pp_gl_trans(debit_trans_type, asset_id, asset_act_id, depr_group_id, wo_id, debit_acc, gain_loss, pend_trans_id, je_method_id, sob_id, reversal, amount_type)
	if mid(gl_account_str_debit, 1, 5) = 'ERROR' then
		return -1
	end if
	
	gl_account_str_credit = uf_pp_gl_trans(credit_trans_type, asset_id, asset_act_id, depr_group_id, wo_id, credit_acc, gain_loss, pend_trans_id, je_method_id, sob_id, reversal, amount_type)
	if mid(gl_account_str_credit, 1, 5) = 'ERROR' then
		return -1
	end if
	
	//Debit Insert
	insert into gl_transaction
		(gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount, 
		gl_je_code, gl_status_id, description, source, asset_id, amount_type, je_method_id, trans_type)
	select
		pwrplant1.nextval, :a_month, c.gl_company_no, :gl_account_str_debit, 1, :amount,
		'ARO', 1, 'Regulated ARO Debit', 'ARO APPROVAL', :asset_id, :amount_type, :je_method_id, :debit_trans_type
	from company c
	where c.company_id = :a_company_id;
	
	//Credit Insert
	insert into gl_transaction
		(gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount, 
		gl_je_code, gl_status_id, description, source, asset_id, amount_type, je_method_id, trans_type)
	select
		pwrplant1.nextval, :a_month, c.gl_company_no, :gl_account_str_credit, 0, :amount,
		'ARO', 1, 'Regulated ARO Credit', 'ARO APPROVAL', :asset_id, :amount_type, :je_method_id, :credit_trans_type
	from company c
	where c.company_id = :a_company_id;
next


return 1


/*
NOTES!

JE Trans Types
	41 - Regulatory ARO DR
	42 - Regulatory ARO CR

*/
end function

public function string uf_pp_gl_trans (longlong a_activity, longlong a_ldg_asset_id, longlong a_ldg_asset_act_id, longlong a_ldg_depr_group_id, longlong a_ldg_work_order_id, longlong a_ldg_gl_account_id, longlong a_gain_loss_ind, longlong a_ldg_pend_trans_id, longlong a_je_method_id, longlong a_je_set_of_books_id, longlong a_je_reversal, longlong a_je_amount_type);/************************************************************************************************************************************************************
 **
 **	uf_pp_gl_trans()
 **	
 **	Wrapper for the PP_GL_TRANSACTION PL/SQL function
 **	No commits in this function
 **	
 **	This function is currently called from 
 **		- nvo_aro_reg.uf_create_jes()
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	longlong	:	1		:	The calculation was successful
 **										:	-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
string str_ret

DECLARE F_PP_GL_TRANSACTION PROCEDURE FOR 
	PP_GL_TRANSACTION (:a_activity, :a_ldg_asset_id, :a_ldg_asset_act_id, 
							:a_ldg_depr_group_id, :a_ldg_work_order_id, :a_ldg_gl_account_id, 
							:a_gain_loss_ind, :a_ldg_pend_trans_id, :a_je_method_id, 
							:a_je_set_of_books_id, :a_je_reversal, :a_je_amount_type);

execute F_PP_GL_TRANSACTION;
if sqlca.sqlCode <> 0 then
	messageBox("ERROR", "Error creating ORACLE function F_PP_GL_TRANSACTION: " + sqlca.sqlErrText)
	return "ERROR"
end if

FETCH F_PP_GL_TRANSACTION INTO :str_ret;
if sqlca.sqlCode <> 0 then
	messageBox("ERROR", "Error executing ORACLE function F_PP_GL_TRANSACTION: " + sqlca.sqlErrText)
	return "ERROR"
end if

close F_PP_GL_TRANSACTION;

return str_ret
end function

public function string uf_db_calc (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	uf_db_calc()
 **	
 **	Calculates the amount to be inserted into depr_ledger (from Regulated ARO's) for a company and month
 **	No commits in this function
 **	
 **	This function is currently called from 
 **		- w_cpr_control - cb_aro_calc.clicked()
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	longlong	:	1		:	The calculation was successful
 **										:	-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
 string str_ret

DECLARE F_CALC_REGULATED PROCEDURE FOR 
	PKG_ARO.F_CALC_REGULATED (:a_company_id, :a_month);

execute F_CALC_REGULATED;
if sqlca.sqlCode <> 0 then
	messageBox("ERROR", "Error creating ORACLE function F_CALC_REGULATED: " + sqlca.sqlErrText)
	return "ERROR"
end if

FETCH F_CALC_REGULATED INTO :str_ret;
if sqlca.sqlCode <> 0 then
	messageBox("ERROR", "Error executing ORACLE function F_CALC_REGULATED: " + sqlca.sqlErrText)
	return "ERROR"
end if

close F_CALC_REGULATED;

return str_ret
end function

public function string uf_db_approve (longlong a_company_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	uf_db_approve()
 **	
 **	Updates the cost_of_removal on depr_ledger for a company and month
 **	Creates gl transactions
 **	No commits in this function
 **	
 **	This function is currently called from 
 **		- w_cpr_control.cb_aro_approval.clicked
 **	
 **	Parameters	:	longlong	:	( a_company_id ) The company to calculate.
 **						datetime		:	( a_month ) The month to calculate.
 **	
 **	Returns		:	longlong	:	1		:	The calculation was successful
 **										:	-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
 string str_ret

DECLARE F_APPROVE_REGULATED PROCEDURE FOR 
	PKG_ARO.F_APPROVE_REGULATED (:a_company_id, :a_month);

execute F_APPROVE_REGULATED;
if sqlca.sqlCode <> 0 then
	messageBox("ERROR", "Error creating ORACLE function F_APPROVE_REGULATED: " + sqlca.sqlErrText)
	return "ERROR"
end if

FETCH F_APPROVE_REGULATED INTO :str_ret;
if sqlca.sqlCode <> 0 then
	messageBox("ERROR", "Error executing ORACLE function F_APPROVE_REGULATED: " + sqlca.sqlErrText)
	return "ERROR"
end if

close F_APPROVE_REGULATED;

return str_ret
end function

public function boolean uf_can_approve_depr (longlong a_company_id, datetime a_month, ref string a_msg);longlong reg_obligation
datetime aro_appr_date, depr_calc_date

SELECT aro.regulatory_obligation, nvl(cc.aro_approved,to_date(190001,'yyyymm')), cc.depr_calculated
INTO :reg_obligation, :aro_appr_date, :depr_calc_date
FROM cpr_control cc, 
	(SELECT company_id, Max(Nvl(regulatory_obligation,0)) AS regulatory_obligation
	FROM aro
	GROUP BY company_id) aro
WHERE cc.company_id = aro.company_id (+)
AND cc.company_id = :a_company_id
AND cc.accounting_month = :a_month;

if sqlca.sqlcode < 0 then 
	a_msg = "SQL Error from nvo_aro_reg.uf_can_approve_depr: " + sqlca.SQLErrText
	return false
elseif reg_obligation > 0 and not isnull(depr_calc_date) and depr_calc_date < aro_appr_date then 
	a_msg = "The depreciation calculation was executed before AROs were approved. ARO Approval must be done before the depreciation calculation when regulatory AROs exist for the company."
	return false
elseif isnull(depr_calc_date) then 
	a_msg = "Depreciation cannot be approved until after the depreciation calculation has been executed."
	return false
else 
	a_msg = "Success!" 
	return true
end if
end function

on nvo_aro_reg.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_aro_reg.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;//
//	Assume pp_msgs is true
//
i_pp_msgs = true
end event

