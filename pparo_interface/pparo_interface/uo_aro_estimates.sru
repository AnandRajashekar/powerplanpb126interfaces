HA$PBExportHeader$uo_aro_estimates.sru
$PBExportComments$v10.2.1.5 maint 5349:   use the time average methodology to find a single discount rate for all prior layers, rather than simple weighted average, to include in the overall time average calc.
forward
global type uo_aro_estimates from nonvisualobject
end type
end forward

global type uo_aro_estimates from nonvisualobject
end type
global uo_aro_estimates uo_aro_estimates

type variables
////
////	The current ARO and layer and its attributes.
////
		longlong 	i_aro_id
		longlong	i_layer_id
		longlong	i_aro_type
		longlong 	i_detailed_rates
		longlong	i_skip_layer_adj
		
////
////	The rate type fo the current ARO.
////
		longlong 	i_weighted_avg
		
////
////	A datawindow used for multiple functions.
////
		uo_ds_top i_ds_aro_streams
		
////
////	The accounting month being processed.
////
		datetime i_accounting_month

////
////	Indicates if the ARO has been approved.
////	This gets set from w_aro_pp_approval.
////		
		longlong i_pp_approve

////
////	ARO types
////
		longlong	i_type_component = 1
		longlong	i_type_mass = 2
		longlong	i_type_site = 3
		
////
////	ARO statuses
////
		longlong	i_status_setup = 1
		longlong	i_status_pending = 2
		longlong	i_status_active = 3
		longlong	i_status_inactive = 4
		longlong	i_status_transition = 5
		longlong	i_status_active_pending = 6
		longlong	i_status_forecast = 7

////
////	ARO Rate Types
////
		longlong	i_rate_type_standard = 0	/* Standard */
		longlong	i_rate_type_weighted = 1	/* Weighted Average */
		longlong	i_rate_type_new = 2			/* New Rate */
		longlong	i_rate_type_time = 3			/* Time Average */
		
////
////	Expenditure types
////
		longlong	i_expend_adds = 1
		longlong	i_expend_rets = 2
		longlong	i_expend_rev = 3
		longlong	i_expend_job = 4
		longlong	i_expend_wip = 5
		
////
////	ARO Transaction Types
////
		longlong	i_trans_aro_settle_liab = 26		/* 26 - ARO Settlement Debit to Liability */
		longlong	i_trans_aro_settle_wo = 27		/* 27 - ARO Settlement Credit to Work Order */
		longlong	i_trans_aro_settle_ext_gl = 28	/* 28 - ARO Settlement Credit external_gl_account */
end variables

forward prototypes
public function integer uf_settlement_aro (longlong a_aro_id, datetime a_month)
public function integer uf_retire_arc (longlong a_aro_id, datetime a_month, string a_wo)
public function integer uf_retire_arc_settle (longlong a_aro_id, string a_wo_num, datetime a_month, decimal a_settle_amount)
public function decimal uf_set_old_liability ()
public function decimal uf_wam_detail_rate (datetime a_month_yr)
public function decimal uf_set_new_liability (decimal a_discount_rate, datetime a_start_date)
public function decimal uf_calc_weighted_avg_rate (string a_up_down)
public function integer uf_calc_settle_adj (ref datawindow a_dw_streams, longlong a_aro_id, longlong a_layer_id)
public function longlong uf_add_settlements ()
public function integer uf_approve_layer (longlong a_aro_id, longlong a_layer_id)
public function integer uf_book_layer (longlong a_aro_id, longlong a_layer_id, longlong a_trans_forcst)
public function integer uf_calc_layer_balance (longlong a_aro_id, longlong a_layer_id, longlong a_trans_forcst)
public function integer uf_mass_layer_calc (longlong a_aro_id, longlong a_layer_id, longlong a_stream_id, decimal a_prob, decimal a_cost_per_unit, decimal a_inf_rate, datetime a_gl_posting_mo_yr)
public function integer uf_stream_update_mass (longlong a_stream_id, date a_month_yr, decimal a_cost_per_unit, decimal a_in_rate, decimal a_gross_cash_flow, string a_comments, decimal a_cash_flow, decimal a_remain_cf, decimal a_qty)
public function decimal uf_calc_rate_trial (decimal a_gross_cash_flow[], decimal a_ann_disc_rate[], longlong a_months_between[], decimal a_starting_rate, string a_same_or_prior)
end prototypes

public function integer uf_settlement_aro (longlong a_aro_id, datetime a_month);/************************************************************************************************************************************************************
 **
 **	uf_settlement_aro()
 **	
 **	Updates ARO Liability with the settlement amount.
 **
 **	This function is currently called from:
 **		- w_aro_mass_ye - cb_new_layers.clicked() - It gets called from a loop, so it is called for each ARO. The month will be the same for each call.
 **	
 **	Parameters	:	longlong	:	( a_aro_id ) The ARO to process.
 **						datetime		:	( a_month ) The month to process.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			m, amount, check, aro_id, cost_element, company, aro_rows, p, crows
longlong 			bs_id, je_method_id, je_method_count, reversal_convention
longlong 			i, j, wo_id, wo_rows,k, layer_rows, layer_id, month_number, liab_acct
longlong	gain_acct, loss_acct, expend_id, bus_seg, gl_acct, charge_id, acct, aro_charge_type, dept
decimal{2} 	settlement[], alloc_amt, current_liab, input_gain_loss, aro_charge, empty_array[], settle_amount
datetime 	month_yr, gl_posting_mo_yr, max_month
string 		layer_string, description, ext_gl_acct, je_code, company_num, sqls, s_rtn

uo_ds_top 	ds_aro_je_method
uo_ds_top 	ds_aro_wo_charges
uo_ds_top 	ds_aro_settlement_header
uo_ds_top 	ds_aro_work_order_summary
uo_ds_top 	ds_aro_settlement_allo
uo_ds_top 	ds_aro_layer_liability
uo_ds_top 	ds_aro_layer_wo

////
////	Set up the datastores.
////
		ds_aro_wo_charges = create uo_ds_top
		ds_aro_wo_charges.dataobject = "dw_aro_wo_charges"
		ds_aro_wo_charges.setTransObject( sqlca )
		
		ds_aro_settlement_header = create uo_ds_top
		ds_aro_settlement_header.dataobject = "dw_aro_active_by_aro"
		ds_aro_settlement_header.setTransObject( sqlca )
		
		ds_aro_work_order_summary = create uo_ds_top
		ds_aro_work_order_summary.dataobject = "dw_aro_work_order_summary"
		ds_aro_work_order_summary.setTransObject( sqlca )
		
		ds_aro_settlement_allo = create uo_ds_top
		ds_aro_settlement_allo.dataobject = "dw_aro_settlement_allo"
		ds_aro_settlement_allo.setTransObject( sqlca )
		
		ds_aro_layer_liability = create uo_ds_top
		ds_aro_layer_liability.dataobject = "dw_aro_layer_liability"
		ds_aro_layer_liability.setTransObject( sqlca )
		
		ds_aro_layer_wo = create uo_ds_top
		ds_aro_layer_wo.dataobject = "dw_aro_layer_work_order"
		ds_aro_layer_wo.setTransObject( sqlca )
		
		ds_aro_je_method = create uo_ds_top
		
////
////	Retrieve the ARO information.  This dataobject restricts to "Active" and "Active with Pending" status.
////
		aro_rows = ds_aro_settlement_header.retrieve( a_aro_id )

		if aro_rows <= 0 then 
			f_wo_status_box( "ARO Processing", "No AROs to process for settlement." )
			destroy ds_aro_wo_charges
			destroy ds_aro_settlement_header
			destroy ds_aro_work_order_summary
			destroy ds_aro_settlement_allo
			destroy ds_aro_layer_liability
			destroy ds_aro_layer_wo
			destroy ds_aro_je_method
			return 1
		end if

////
////	Check to make sure there aren't any pending layers.
////
		setNull( check )
		
		select 	count(*) 
		into 		:check
		from 		aro_layer, 
					aro
		where 	aro_layer.aro_status_id in ( :i_status_pending )
		and 		aro.aro_id = aro_layer.aro_id
		and		aro.aro_id = :a_aro_id
		and		aro_layer.gl_posting_mo_yr = :a_month;
		
		if isNull( check ) then check = 0
		
		if check > 0 then
			f_wo_status_box( "ARO Processing", " Pending Layers exist.  Please process the pending layers before closing this ARO." )
			destroy ds_aro_wo_charges
			destroy ds_aro_settlement_header
			destroy ds_aro_work_order_summary
			destroy ds_aro_settlement_allo
			destroy ds_aro_layer_liability
			destroy ds_aro_layer_wo
			destroy ds_aro_je_method
			return -1 
		end if

////
////	Loop through the AROs.
////	(Based on the dataobject, only one row will be returned, so I'm not sure why we're looping.)
////
		for p = 1 to aro_rows
		////
		////	Get the data from the row.
		////
				aro_id = ds_aro_settlement_header.getItemNumber( p, "aro_id" )
				liab_acct = ds_aro_settlement_header.getItemNumber( p, "liability_account" )
				gain_acct = ds_aro_settlement_header.getItemNumber( p, "gain_account" )
				loss_acct = ds_aro_settlement_header.getItemNumber( p, "loss_account" )
				description = ds_aro_settlement_header.getItemString( p, "description" )
				company = ds_aro_settlement_header.getItemNumber( p, "company_id" )
				cost_element = ds_aro_settlement_header.getItemNumber( p, "cost_element_id" )
				
		////
		////	Initialize some variables.
		////
				settlement = empty_array
				setNull( acct )
			
		////	
		////	Check for estimates past this month.  If none, then give warning in the status box.
		////
				setNull( max_month )
				
				select 	max( month_yr ) 
				into 		:max_month
				from 		aro_layer_stream
				where 	aro_id = :aro_id;
			
				if max_month < a_month then
					f_wo_status_box( "ARO Processing", " WARNING: '" + description + "' is past all estimate cash flows." )
				end if
				
		////
		////	Set the GL Posting Mo Yr and convert it to a month number.
		////
				gl_posting_mo_yr = a_month
			
				month_number = year( date( gl_posting_mo_yr ) ) * 100 + month( date( gl_posting_mo_yr ) )
				
		////
		////	Determine the set of books.
		////					
				bs_id = f_aro_determine_sob( aro_id )
				
				if bs_id < 0 then
					f_wo_status_box( "ARO Processing", "Error occurred while determining the set of books." )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
				
		////
		////	Get the charge type for CWIP Charge.
		////
				setNull( aro_charge_type )
				
				select 	charge_type_id 
				into 		:aro_charge_type
				from 		cost_element
				where 	cost_element_id = :cost_element;
				
				if isNull( aro_charge_type ) then
					f_wo_status_box( "ARO Processing", "Unable to retrieve charge type." )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
				
		////
		////	Get GL JE Code.
		////
				setNull( je_code )
				
				select 	standard_journal_entries.gl_je_code 
				into 		:je_code
				from 		standard_journal_entries, 
							gl_je_control
				where 	upper( gl_je_control.process_id ) = 'ARO'
				and 		gl_je_control.je_id = standard_journal_entries.je_id;
				
				if isNull( je_code ) or je_code= "" then
					f_wo_status_box( "ARO Processing", "Cannot Find GL JE CODE for the ARO Process." )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
		
		////
		////	Populate ARO Delete Charge with charges that should be deleted.
		////	Then delete from ARO Charge and CWIP Charge.
		////
				delete from aro_delete_charge;
				
				if sqlca.SQLCode < 0 then
					f_wo_status_box( "ARO Processing", "Error deleting from ARO Delete Charge: " + sqlca.SQLErrText )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
			
				insert into aro_delete_charge 
				( aro_id, charge_id )
				( 	select 	aro_charge.aro_id, 
								aro_charge.charge_id
					from 		aro_charge, 
								cwip_charge
					where 	aro_charge.aro_id = :aro_id
					and 		aro_charge.charge_id = cwip_charge.charge_id
					and 		cwip_charge.month_number = :month_number 
				);
				
				if sqlca.SQLCode < 0 then
					f_wo_status_box( "ARO Processing", "Error inserting into ARO Delete Charge: " + sqlca.SQLErrText )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
			
				delete from aro_charge
				where 	aro_id = :aro_id
				and 		charge_id in 
							( select charge_id from aro_delete_charge );
							
				if sqlca.SQLCode < 0 then
					f_wo_status_box( "ARO Processing", "Error deleting from ARO Charge: " + sqlca.SQLErrText )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
				
				delete from cwip_charge
				where 	charge_id in 
							( select charge_id from aro_delete_charge );
							
				if sqlca.SQLCode < 0 then
					f_wo_status_box( "ARO Processing", "Error deleting from CWIP Charge: " + sqlca.SQLErrText )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
				
				delete from aro_delete_charge;
				
				if sqlca.SQLCode < 0 then
					f_wo_status_box( "ARO Processing", "Error deleting from ARO Delete Charge: " + sqlca.SQLErrText )
					destroy ds_aro_wo_charges
					destroy ds_aro_settlement_header
					destroy ds_aro_work_order_summary
					destroy ds_aro_settlement_allo
					destroy ds_aro_layer_liability
					destroy ds_aro_layer_wo
					destroy ds_aro_je_method
					return -1
				end if
			
		////
		////	Retrieve the work orders for this ARO and month.
		////
				wo_rows = ds_aro_work_order_summary.retrieve( aro_id, month_number )
				if wo_rows <= 0 then continue
		
		////
		////	Calculate the settlement for all work orders for this ARO.
		////	Loop through the rows
		////
				for i = 1 to wo_rows
				////
				////	Get the data from the row.
				////
						wo_id = ds_aro_work_order_summary.getItemNumber( i, "work_order_id" )
						alloc_amt = ds_aro_work_order_summary.getItemNumber( i, "to_settle_amount" )
						
				////
				////	Get the department for this work order.
				////
						setNull( dept )
						setNull( bus_seg )
						
						select 	department_id,
									bus_segment_id
						into 		:dept,
									:bus_seg
						from 		work_order_control
						where 	work_order_id = :wo_id;
						
						if isNull( dept ) then
							f_wo_status_box( "ARO Processing", "No department retrieved for work order ID " + string( wo_id ) + "." )
							destroy ds_aro_wo_charges
							destroy ds_aro_settlement_header
							destroy ds_aro_work_order_summary
							destroy ds_aro_settlement_allo
							destroy ds_aro_layer_liability
							destroy ds_aro_layer_wo
							destroy ds_aro_je_method
							return -1
						end if
						
				////
				////	Retrieve the data from aro_layer_work_order.   This will tell us which layers
				////	are related to this work order.
				////
						ds_aro_layer_wo.retrieve( wo_id, aro_id )
						
				////
				////	Build a string with all the layer IDs.
				////
						layer_string = ""
						for j = 1 to ds_aro_layer_wo.rowCount()
							if j = 1 then
								layer_string = layer_string + "layer_id = " + string( ds_aro_layer_wo.getItemNumber( j, "layer_id" ) )
							else
								layer_string = layer_string + " or layer_id = " + string( ds_aro_layer_wo.getItemNumber( j, "layer_id" ) )
							end if
						next
				
				////
				////	Retrieve the liability amount to settle.
				////	Filter for the layers for this WO.
				////		
						ds_aro_settlement_allo.retrieve( aro_id, alloc_amt )
						ds_aro_settlement_allo.setFilter( layer_string )
						ds_aro_settlement_allo.filter()
						
				////
				////	If there are no liability rows, skip this row.
				////
						if ds_aro_settlement_allo.rowCount() <= 0 then continue
						
				////
				////	Loop through and store the settlement amount in the array.
				////	Each index corresponds to a layer ID.
				////
						for j = 1 to ds_aro_settlement_allo.rowCount()
							layer_id = ds_aro_settlement_allo.getItemNumber( j, "layer_id" )
							settlement[layer_id] = settlement[layer_id] + ds_aro_settlement_allo.getItemNumber( j, "settlement" )
						next
						
				////
				////	Retrieve the CWIP Charge rows and loop through.
				////
						crows = ds_aro_wo_charges.retrieve( wo_id, aro_id, month_number )
					
						if crows <= 0 then continue		
						
						for m = 1 to crows
						////
						////	Get the data from the row.
						////
								expend_id = ds_aro_wo_charges.getItemNumber( m, "expenditure_type_id" )
								settle_amount = ds_aro_wo_charges.getItemNumber( m, "settle_amount" )
								
						////
						////	Retrieve the GL account based on the expenditure type.
						////
								setNull( gl_acct )
								choose case expend_id
									case i_expend_adds
										select		cwip_gl_account 
										into 		:gl_acct 
										from 		work_order_account 
										where 	work_order_id = :wo_id;
									case i_expend_rets
										select 	removal_gl_account 
										into 		:gl_acct 
										from 		work_order_account 
										where 	work_order_id = :wo_id;
									case i_expend_rev
										select 	expense_gl_account 
										into 		:gl_acct 
										from 		work_order_account 
										where 	work_order_id = :wo_id;
									case i_expend_job
										select 	jobbing_gl_account 
										into 		:gl_acct 
										from 		work_order_account 
										where 	work_order_id = :wo_id;
								end choose
							
								if isNull( gl_acct ) then
									f_wo_status_box( "ARO Processing", "No GL Account retrieved for work order ID " + string( wo_id ) + " and expenditure type ID " + string( expend_id ) + "." )
									destroy ds_aro_wo_charges
									destroy ds_aro_settlement_header
									destroy ds_aro_work_order_summary
									destroy ds_aro_settlement_allo
									destroy ds_aro_layer_liability
									destroy ds_aro_layer_wo
									destroy ds_aro_je_method
									return -1
								end if
									
						////
						////	TRANSACTION TYPE: 28 - ARO Settlement Credit external_gl_account
						////
						
						////
						////	Retrieve the JE methods.
						////
								sqls =	"	select 	je_method_set_of_books.je_method_id, " + "~r~n" + &
										"				je_method_set_of_books.reversal_convention " + "~r~n" + &
										" 	from 		je_method_set_of_books, " + "~r~n" + &
										"				je_method_trans_type " + "~r~n" + &
										" 	where 	je_method_set_of_books.set_of_books_id = " + string( bs_id ) + " " + "~r~n" + &
										" 	and 		je_method_set_of_books.je_method_id = je_method_trans_type.je_method_id " + "~r~n" + &
										" 	and 		je_method_trans_type.trans_type = " + string( i_trans_aro_settle_ext_gl ) + " "
							
								s_rtn = f_create_dynamic_ds( ds_aro_je_method, "grid", sqls, sqlca, true )
								
								if s_rtn <> "OK" then
									f_wo_status_box( "ARO Processing", "Error occurred while creating the dynamic datastore for JE Methods (Trans Type 28).  Error: " + s_rtn )
									destroy ds_aro_wo_charges
									destroy ds_aro_settlement_header
									destroy ds_aro_work_order_summary
									destroy ds_aro_settlement_allo
									destroy ds_aro_layer_liability
									destroy ds_aro_layer_wo
									destroy ds_aro_je_method
									return -1
								end if
									
						////
						////	Loop through the JE methods.
						////
								for je_method_count = 1 to ds_aro_je_method.rowCount()
								////
								////	Get the data from the row.
								////
										je_method_id = ds_aro_je_method.getItemNumber( je_method_count, "je_method_id" )
										reversal_convention = ds_aro_je_method.getItemNumber( je_method_count, "reversal_convention" )
				
								////
								////	Compute the charge.
								////
										aro_charge = settle_amount * ( -1 ) * reversal_convention
						
								////
								////	Get the next charge ID.
								////
										//charge_id = f_topkey_no_dw( "cwip_charge", "charge_id" )
										select pwrplant3.nextval into :charge_id from dual; 
						
								////
								////	Get the external GL account.
								////
										setNull( ext_gl_acct )
										ext_gl_acct = f_autogen_je_account( ds_aro_wo_charges, m, i_trans_aro_settle_ext_gl, gl_acct, je_method_id )										
										////	maint 43874 - hard stop if string starts with 'ERROR'.  String provided enough details specifying trans type and error, so keep message simple.
										if mid(ext_gl_acct,1,5) = 'ERROR' then
											f_wo_status_box( "ARO Processing", "Error occurred while generating the transaction's gl account.  Generated value : " + ext_gl_acct)
											destroy ds_aro_wo_charges
											destroy ds_aro_settlement_header
											destroy ds_aro_work_order_summary
											destroy ds_aro_settlement_allo
											destroy ds_aro_layer_liability
											destroy ds_aro_layer_wo
											destroy ds_aro_je_method
											return -1
										end if
						
								////
								////	Insert into CWIP Charge and ARO Charge.
								////
										insert into cwip_charge 
										( charge_id, expenditure_type_id, work_order_id, charge_type_id, charge_mo_yr, description, quantity, amount, hours, payment_date, department_id, 
											cost_element_id, utility_account_id, gl_account_id, charge_audit_id, month_number, company_id, external_gl_account, bus_segment_id, journal_code ) 
										values 
										( :charge_id, :expend_id, :wo_id, :aro_charge_type, :gl_posting_mo_yr, :description, 0, :aro_charge, 0, :gl_posting_mo_yr, :dept, 
											:cost_element, :acct, :gl_acct, :charge_id, :month_number, :company, :ext_gl_acct, :bus_seg, :je_code );
				
										if sqlca.SQLCode < 0 then
											f_wo_status_box( "ARO Processing", "Error inserting into CWIP Charge for the settlement charges: " + sqlca.SQLErrText )
											destroy ds_aro_wo_charges
											destroy ds_aro_settlement_header
											destroy ds_aro_work_order_summary
											destroy ds_aro_settlement_allo
											destroy ds_aro_layer_liability
											destroy ds_aro_layer_wo
											destroy ds_aro_je_method
											return -1
										end if
										
										insert into aro_charge
										( aro_id, charge_id ) 
										values 
										( :aro_id, :charge_id );
										
										if sqlca.SQLCode < 0 then
											f_wo_status_box( "ARO Processing", "Error inserting into ARO Charge for the settlement charges: " + sqlca.SQLErrText )
											destroy ds_aro_wo_charges
											destroy ds_aro_settlement_header
											destroy ds_aro_work_order_summary
											destroy ds_aro_settlement_allo
											destroy ds_aro_layer_liability
											destroy ds_aro_layer_wo
											destroy ds_aro_je_method
											return -1
										end if
								next //JE methods - trans type 28
						next //CWIP charge rows (ds_aro_wo_charges )
				next //Work orders for this ARO (ds_aro_work_order_summary)
			
		////
		////	Retrieve the layer liabilities.  Loop through and update with settlement amounts.
		////
				layer_rows = ds_aro_layer_liability.retrieve( aro_id )
			
				for k = 1 to layer_rows
				////
				////	Get the data from the row.
				////
						layer_id = ds_aro_layer_liability.getItemNumber( k, "layer_id" )
						current_liab = ds_aro_layer_liability.getItemNumber( k, "current_liab" )
						input_gain_loss = ds_aro_layer_liability.getItemNumber( k, "input_gain_loss" )	
						
				////
				////	Set the month yr.
				////
						month_yr = a_month
						
				////
				////	Update the settlement and gain/loss amounts on ARO Liability.
				////
						if k > upperBound( settlement[] ) then
						////
						////	Update the settlement and gain/loss to zero.
						////
								update 	aro_liability 
								set 		settled = 0,
											gain_loss = 0
								where 	aro_id = :aro_id
								and 		layer_id = :layer_id
								and 		month_yr = :month_yr;
				
								if sqlca.SQLCode < 0 then
									f_wo_status_box( "ARO Processing", "Error updating Settled and Gain/Loss on ARO Liability (1): " + sqlca.SQLErrText )
									destroy ds_aro_wo_charges
									destroy ds_aro_settlement_header
									destroy ds_aro_work_order_summary
									destroy ds_aro_settlement_allo
									destroy ds_aro_layer_liability
									destroy ds_aro_layer_wo
									destroy ds_aro_je_method
									return -1
								end if
						elseif isNull( settlement[k] ) then
						////
						////	Update the settlement to zero.  Also, compute the gain/loss.
						////
								update 	aro_liability 
								set 		settled = 0,
											gain_loss = 0
								where 	aro_id = :aro_id
								and 		layer_id = :layer_id
								and 		month_yr = :month_yr;
								
								if sqlca.SQLCode < 0 then
									f_wo_status_box( "ARO Processing", "Error updating Settled and Gain/Loss on ARO Liability (2): " + sqlca.SQLErrText )
									destroy ds_aro_wo_charges
									destroy ds_aro_settlement_header
									destroy ds_aro_work_order_summary
									destroy ds_aro_settlement_allo
									destroy ds_aro_layer_liability
									destroy ds_aro_layer_wo
									destroy ds_aro_je_method
									return -1
								end if
						elseif settlement[k] > current_liab + input_gain_loss and current_liab > 0 then
						////
						////	Update the settlement to negative the value in the array.  Also, compute the gain/loss.
						////
								update 	aro_liability 
								set 		settled = -1 * :settlement[k],
											gain_loss = :settlement[k] - :current_liab
								where 	aro_id = aro_id
								and 		layer_id = :layer_id
								and 		month_yr = :month_yr;
						
								if sqlca.SQLCode < 0 then
									f_wo_status_box( "ARO Processing", "Error updating Settled and Gain/Loss on ARO Liability (3): " + sqlca.SQLErrText )
									destroy ds_aro_wo_charges
									destroy ds_aro_settlement_header
									destroy ds_aro_work_order_summary
									destroy ds_aro_settlement_allo
									destroy ds_aro_layer_liability
									destroy ds_aro_layer_wo
									destroy ds_aro_je_method
									return -1
								end if
						elseif settlement[k] < current_liab + input_gain_loss and current_liab < 0 then
						////
						////	Update the settlement to negative the value in the array.  Also, compute the gain/loss.
						////
								update 	aro_liability 
								set 		settled = -1 * :settlement[k],
											gain_loss = :settlement[k] - :current_liab
								where 	aro_id = aro_id
								and 		layer_id = :layer_id
								and 		month_yr = :month_yr;
						
								if sqlca.SQLCode < 0 then
									f_wo_status_box( "ARO Processing", "Error updating Settled and Gain/Loss on ARO Liability (4): " + sqlca.SQLErrText )
									destroy ds_aro_wo_charges
									destroy ds_aro_settlement_header
									destroy ds_aro_work_order_summary
									destroy ds_aro_settlement_allo
									destroy ds_aro_layer_liability
									destroy ds_aro_layer_wo
									destroy ds_aro_je_method
									return -1
								end if
						else
						////
						////	Update the settlement to negative the value in the array.  Also, set the gain/loss to zero.
						////
								update 	aro_liability 
								set 		settled = -1 * :settlement[k],
											gain_loss = 0
								where 	aro_id = aro_id
								and 		layer_id = :layer_id
								and 		month_yr = :month_yr;
						
								if sqlca.SQLCode < 0 then
									f_wo_status_box( "ARO Processing", "Error updating Settled and Gain/Loss on ARO Liability (5): " + sqlca.SQLErrText )
									destroy ds_aro_wo_charges
									destroy ds_aro_settlement_header
									destroy ds_aro_work_order_summary
									destroy ds_aro_settlement_allo
									destroy ds_aro_layer_liability
									destroy ds_aro_layer_wo
									destroy ds_aro_je_method
									return -1
								end if
						end if
				next //layers (ds_aro_layer_liability)
		next //AROs (ds_aro_settlement_header )

////
////	Function completed successfully.
////
		destroy ds_aro_wo_charges
		destroy ds_aro_settlement_header
		destroy ds_aro_work_order_summary
		destroy ds_aro_settlement_allo
		destroy ds_aro_layer_liability
		destroy ds_aro_layer_wo
		destroy ds_aro_je_method
		
		return 1
end function

public function integer uf_retire_arc (longlong a_aro_id, datetime a_month, string a_wo);/************************************************************************************************************************************************************
 **
 **	uf_retire_arc()
 **	
 **	Creates a pending transaction for the retirement.
 **
 **	This function is currently called from:
 **		- w_aro_mass_ye - cb_new_layers.clicked() - It gets called from a loop, so it is called for each ARO.  The month and WO will be the same for each call.
 **	
 **	Parameters	:	longlong	:	( a_aro_id ) The ARO to process.
 **						datetime		:	( a_month ) The month of the transaction.
 **						string			:	( a_wo ) The work order to process.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong	month_number, asset_id, count, je_id, pend_trans_id, basis
decimal{2}	discounted_flow, posting_amount
string			gl_je_code, sqls
datetime		gl_posting_mo_yr
 
////
////	Set the GL Posting Mo Yr and convert it to a month number.
////
		gl_posting_mo_yr = a_month
	
		month_number = year( date( gl_posting_mo_yr ) ) * 100 + month( date( gl_posting_mo_yr ) )

////	
////	Get the asset ID for this ARO.
////
		setNull( asset_id ) 
		
		select 	asset_id 
		into 		:asset_id 
		from 		aro
		where 	aro_id = :a_aro_id;
		
		if isNull( asset_id ) then
			f_wo_status_box( "Error", "No asset is related to the ARO." )
			return -1
		end if
		
////
////	Make sure there are entries in ARO Layer Discounting.
////
		setNull( count )
		
		select 	count(*)
		into 		:count
		from 		aro_layer_discounting
		where 	aro_id = :a_aro_id;
		
		if isNull( count ) then count = 0

		if count = 0 then
			f_wo_status_box( "Error", "No entries exist in ARO Layer Discounting.  Retire ARC by hand." )
			return 1
		end if
	
////
////	Get the Posting Amount.
////
		setNull( discounted_flow )
		
		select 	sum( discounted_flow ) 
		into 		:discounted_flow
		from 		aro_layer_discounting
		where 	aro_id = :a_aro_id
		and 		month_yr between add_months( :gl_posting_mo_yr, -11 ) and :gl_posting_mo_yr;
		
		if isNull( discounted_flow ) then discounted_flow = 0

		if discounted_flow = 0 then
			f_wo_status_box( "Error", "There are no discounted cash flows for the past 12 months." )
			return 1
		end if
		
		posting_amount = discounted_flow * -1

////
////	Get GL JE Code.
////
		setNull( je_id )
		setNull( gl_je_code )
		
		select 	je_id 
		into 		:je_id 
		from 		gl_je_control
		where 	upper( process_id ) = 'ARO';
		
		if isNull( je_id ) then
			messageBox( "Error", "Unable to find the Journal Entry ID for ARO." )
			return -1
		end if
		
		select 	gl_je_code 
		into 		:gl_je_code 
		from 		standard_journal_entries
		where 	je_id = :je_id;
		
		if isNull( gl_je_code ) or gl_je_code= "" then
			messageBox( "Error", "Unable to find the GL JE Code for ARO." )
			return -1
		end if

////
////	Get the next transaction ID.
////
		//pend_trans_id = f_topkey_no_dw( "pend_transaction", "pend_trans_id" )
		select pwrplant1.nextval into :pend_trans_id from dual; 

////
////	Insert the transaction.
////
		insert into pend_transaction
		( pend_trans_id, ldg_asset_id, ldg_depr_group_id, books_schema_id, retirement_unit_id, utility_account_id, bus_segment_id,
			func_class_id, sub_account_id, asset_location_id, gl_account_id, company_id, gl_posting_mo_yr, subledger_indicator, activity_code,
			gl_je_code, work_order_number, posting_quantity, user_id1, posting_amount, in_service_year, description, long_description,
			property_group_id, retire_method_id, posting_status, cost_of_removal, salvage_cash, salvage_returns, gain_loss, reserve,
			misc_description, ferc_activity_code, serial_number, reserve_credits )
		select 	:pend_trans_id, 
					asset_id, 
					depr_group_id, 
					1 books_schema_id, 
					retirement_unit_id,
					utility_account_id, 
					bus_segment_id,
					func_class_id,
					sub_account_id,
					asset_location_id,
					gl_account_id,
					company_id,
					:gl_posting_mo_yr, 
					subledger_indicator,
					'URGL' activity_code,
					:gl_je_code,
					:a_wo,
					0 posting_quantity,
					user user_id1,
					:posting_amount,
					:gl_posting_mo_yr,
					'ARC YE Retirement' description,
					'Retire ARC Asset for the current year, Auto generated' long_description,
					property_group_id, 
					8 retire_method_id,
					1 posting_status, 
					0 cost_of_removal,
					0 salvage_cash, 
					0 salvage_returns,
					0 gain_loss,
					:posting_amount * -1 reserve,
					long_description misc_description, 
					2 ferc_activity_code,
					serial_number, 
					0 reserve_credits
		from 		cpr_ledger 
		where 	asset_id = :asset_id;
		
		if sqlca.SQLCode < 0 then
			messageBox( "Error", "Error occurred while inserting into Pending Transaction.~n~nError: " + sqlca.SQLErrText )
			return -1
		end if
		
////
////	Get the book summary override.  If there isn't one,
////	get the minimum book summary ID for ARO.
////  Also get the liability account while we're in there.
////
		setNull( basis )
		select 	nvl( book_summary_override, 0 )
		into 		:basis
		from 		aro
		where 	aro_id = :a_aro_id;
		
		if isNull( basis ) or basis = 0 then 
			select 	min( book_summary_id ) 
			into 		:basis
			from 		book_summary
			where 	book_summary_type = 'ARO';
			
			if isNull( basis ) or basis = 0 then
				messageBox( "Error", "No book summary has a book summary type of 'ARO'.  This transaction cannot be completed." )
				return -1
			end if
		end if
		
////
////	Insert into pend_basis.
////
		sqls =	"insert into pend_basis ( pend_trans_id, basis_" + string( basis ) + " ) " + "~r~n" +  &
				"values ( " + string( pend_trans_id ) + ", " + string( posting_amount ) + " ) "
				
		execute immediate :sqls;
		
		if sqlca.SQLCode <> 0 then
			messageBox( "SQL Error", "Error occurred while inserting into Pending Basis.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if

////
////	Function completed successfully.
////
		return 1
end function

public function integer uf_retire_arc_settle (longlong a_aro_id, string a_wo_num, datetime a_month, decimal a_settle_amount);/************************************************************************************************************************************************************
 **
 **	uf_retire_arc_settle()
 **	
 **	Creates a pending transaction for the retirement settlement.
 **
 **	This function is currently called from:
 **		- f_aro_calc_settlement - It gets called from a loop, so it is called for each ARO/work order combination.
 **	
 **	Parameters	:	longlong	:	( a_aro_id ) The ARO to process.
 **						string			:	( a_wo_num ) The work order to process.
 **						datetime		:	( a_month ) The month of the transaction.
 **						decimal		:	( a_settle_amount ) The settlement amount.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong	month_number, asset_id, je_id, pend_trans_id, basis
decimal{2}	posting_amount, asset_cost, current_liab
string			gl_je_code, sqls, aro_desc, comp_desc
datetime		gl_posting_mo_yr
 
////
////	Set the GL Posting Mo Yr and convert it to a month number.
////
		select 	add_months( :a_month,1) 
		into 		:gl_posting_mo_yr
		from 		dual;
	
		month_number = year( date( gl_posting_mo_yr ) ) * 100 + month( date( gl_posting_mo_yr ) )

////	
////	Get the asset ID and its cost for this ARO.
////
		setNull( asset_id ) 
		setNull( asset_cost )
		
		select 	cpr_ledger.asset_id,
					cpr_ledger.accum_cost,
					aro.description,
					comp.description
		into 		:asset_id,
					:asset_cost,
					:aro_desc,
					:comp_desc
		from 		aro,
					cpr_ledger,
					company comp
		where 	aro.aro_id = :a_aro_id
		and		aro.asset_id = cpr_ledger.asset_id
		and		comp.company_id = aro.company_id;
		
		if isNull( asset_id ) then
			f_wo_status_box( "Error", "No asset is related to the ARO." )
			f_wo_status_box( "Error", "    Company: " + comp_desc)
			f_wo_status_box( "Error", "    ARO: " + aro_desc)
			return -1
		end if
		
		if isNull( asset_cost ) then asset_cost = 0

////
////	Get the liability.
////
		setNull( current_liab )
		
		select 	sum( liab.beg_liability + liab.accreted ), max(aro.description), max(comp.description)
		into 		:current_liab, :aro_desc, :comp_desc
		from 		aro_liability liab, aro, company comp
		where 	aro.aro_id = :a_aro_id
		and 		liab.aro_id = aro.aro_id
		and		liab.month_yr = :a_month
		and		comp.company_id = aro.company_id;
		
		if isNull( current_liab ) then current_liab = 0

		if current_liab = 0 then
			f_wo_status_box( "Error", "Cannot find the ARO Liability.  Retire ARC by hand." )
			f_wo_status_box( "Error", "    Company: " + comp_desc)
			f_wo_status_box( "Error", "    ARO: " + aro_desc)
			return 1
		end if
		
////
////	Calculate the posting amount.
////
		if a_settle_amount = 0 then
			return 1
		elseif asset_cost = 0 then
			return 1
		elseif current_liab = 0 then
			return 1
		else
			posting_amount = asset_cost * ( a_settle_amount / current_liab) * -1
		end if
		
////
////	Get GL JE Code.
////
		setNull( je_id )
		setNull( gl_je_code )
		
		select 	je_id 
		into 		:je_id 
		from 		gl_je_control
		where 	upper( process_id ) = 'ARO';
		
		if isNull( je_id ) then
			messageBox( "Error", "Unable to find the Journal Entry ID for ARO." )
			return -1
		end if
		
		select 	gl_je_code 
		into 		:gl_je_code 
		from 		standard_journal_entries
		where 	je_id = :je_id;
		
		if isNull( gl_je_code ) or gl_je_code= "" then
			messageBox( "Error", "Unable to find the GL JE Code for ARO." )
			return -1
		end if

////
////	Get the next transaction ID.
////
		//pend_trans_id = f_topkey_no_dw( "pend_transaction", "pend_trans_id" )
		select pwrplant1.nextval into :pend_trans_id from dual; 

////
////	Insert the transaction.
////
		insert into pend_transaction
		( pend_trans_id, ldg_asset_id, ldg_depr_group_id, books_schema_id, retirement_unit_id, utility_account_id, bus_segment_id,
			func_class_id, sub_account_id, asset_location_id, gl_account_id, company_id, gl_posting_mo_yr, subledger_indicator, activity_code,
			gl_je_code, work_order_number, posting_quantity, user_id1, posting_amount, in_service_year, description, long_description,
			property_group_id, retire_method_id, posting_status, cost_of_removal, salvage_cash, salvage_returns, gain_loss, reserve,
			misc_description, ferc_activity_code, serial_number, reserve_credits )
		select 	:pend_trans_id, 
					asset_id, 
					depr_group_id, 
					1 books_schema_id, 
					retirement_unit_id,
					utility_account_id, 
					bus_segment_id,
					func_class_id,
					sub_account_id,
					asset_location_id,
					gl_account_id,
					company_id,
					:gl_posting_mo_yr, 
					subledger_indicator,
					'URGL' activity_code,
					:gl_je_code,
					:a_wo_num,
					0 posting_quantity,
					user user_id1,
					:posting_amount,
					:gl_posting_mo_yr,
					'ARC Auto Retirement' description,
					'Retire ARC Asset for the Settlement of '||to_char(:a_settle_amount) long_description,
					property_group_id, 
					8 retire_method_id,
					1 posting_status, 
					0 cost_of_removal,
					0 salvage_cash, 
					0 salvage_returns,
					0 gain_loss,
					:posting_amount * -1 reserve,
					long_description misc_description, 
					2 ferc_activity_code,
					serial_number, 
					0 reserve_credits
		from 		cpr_ledger 
		where 	asset_id = :asset_id;

		if sqlca.SQLCode < 0 then
			messageBox( "Error", "Error occurred while inserting into Pending Transaction.~n~nError: " + sqlca.SQLErrText )
			return -1
		end if
		
////
////	Get the book summary override.  If there isn't one,
////	get the minimum book summary ID for ARO.
////  Also get the liability account while we're in there.
////
		setNull( basis )
		select 	nvl( book_summary_override, 0 )
		into 		:basis
		from 		aro
		where 	aro_id = :a_aro_id;
		
		if isNull( basis ) or basis = 0 then 
			select 	min( book_summary_id ) 
			into 		:basis
			from 		book_summary
			where 	book_summary_type = 'ARO';
			
			if isNull( basis ) or basis = 0 then
				messageBox( "Error", "No book summary has a book summary type of 'ARO'.  This transaction cannot be completed." )
				return -1
			end if
		end if
		
////
////	Insert into pend_basis.
////
		sqls =	"insert into pend_basis ( pend_trans_id, basis_" + string( basis ) + " ) " + "~r~n" +  &
				"values ( " + string( pend_trans_id ) + ", " + string( posting_amount ) + " ) "
				
		execute immediate :sqls;
		
		if sqlca.SQLCode <> 0 then
			messageBox( "SQL Error", "Error occurred while inserting into Pending Basis.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if

////
////	Function completed successfully.
////
		return 1
end function

public function decimal uf_set_old_liability ();/************************************************************************************************************************************************************
 **
 **	uf_set_old_liability()
 **	
 **	Retrieve the current liability for the current ARO and month ( stored as instance variables ).
 **
 **	This function is currently called from:
 **		- uf_calc_layer_balance()
 **		- w_aro_pp_approval - open()
 **	
 **						
 **	Returns		:	decimal		:	1		:	The calculated old liability.
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			check
decimal{2} 	old_liability

////
////	Check the status.  If it's not "Setup", return.
////
		setNull( check )
		
		select 	aro_status_id
		into 		:check
		from 		aro_layer
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if isNull( check ) then check = i_status_setup
		
		if check <> i_status_setup then return 1

////
////	Retrieve the liability.
////
		setNull( old_liability )
		
		select 	sum( nvl( beg_liability, 0 ) + nvl( new_incurred, 0 ) + nvl( settled, 0 ) + nvl( accreted, 0 ) + nvl( revised, 0 ) + nvl( gain_loss, 0 ) + nvl( input_gain_loss, 0 ) + nvl( accretion_adjust, 0 ) + nvl( liability_adjust, 0 ) )
		into 		:old_liability
		from 		aro_liability 
		where 	month_yr = :i_accounting_month
		and 		aro_id = :i_aro_id;

		if isNull( old_liability ) then old_liability = 0
		
		//RO 2013-10-28: Need to include previous layers' value, even if they are in pending status
		select :old_liability + nvl(sum(layer_asset_value),0)
		into :old_liability
		from aro_layer
		where aro_id = :i_aro_id
		and layer_id < :i_layer_id
		and aro_status_id = 2;

////
////	Function completed successfully.
////
		return old_liability
end function

public function decimal uf_wam_detail_rate (datetime a_month_yr);/************************************************************************************************************************************************************
 **
 **	uf_settlement_aro()
 **	
 **	Computes the new weighted average rate for the current ARO and layer ( stored as instance variables ).
 **
 **	This function is currently called from:
 **		- uf_calc_layer_balance()
 **	
 **	Parameters	:	datetime		:	( a_month_yr ) The month to process.
 **						
 **	Returns		:	decimal		:	1		:	The calculated rate.
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			i , tot_cash_flow
decimal{2} 	total_pos_cash_flow, total_neg_cash_flow, rcf_difference
decimal{8} 	total_rate, date_discount_rate, percent_diff

////
////	Delete any existing rows in the calculation table.
////
		delete from aro_wam_rate_calc_detailed
		where 	aro_id = :i_aro_id 
		and 		layer_id = :i_layer_id
		and 		month_yr = :a_month_yr;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while deleting from ARO WAM Rate Calc Detailed.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if 

////
////	Loop through the layers.
////
		for i = 1 to ( i_layer_id - 1 )
		////
		////	Insert old layers and rates into the calc table.  Only use stream 0.
		////
				insert into aro_wam_rate_calc_detailed 
				( aro_id, layer_id, month_yr, orig_layer_id, date_disc_rate, remaining_cash_flow, total_cash_flow, portion_rate, total_wam_rate )
				( 	select 	:i_aro_id,
								:i_layer_id,
								:a_month_yr,
								:i,
								date_disc_rate,
								sum( remaining_cash_flow ),
								0,
								0,
								0
					from 		aro_layer_stream
					where 	aro_id = :i_aro_id
					and 		layer_id = :i
					and 		month_yr = :a_month_yr
					and 		stream_id = 0
					and 		remaining_cash_flow <> 0
					group by	layer_id,date_disc_rate
				 );
				
				if sqlca.SQLCode < 0 then 
					messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc Detailed for prior layer ID " + string( i ) + ".~n~nError: " + sqlca.SQLErrText )
					rollback;
					return -1
				end if 
		
		////
		////	Get the date discount rate.  If it's null, get the annual effective rate from the layer.
		////
				setNull( date_discount_rate )
				
				select 	date_disc_rate
				into 		:date_discount_rate
				from 		aro_wam_rate_calc_detailed
				where 	aro_id = :i_aro_id
				and 		layer_id = :i_layer_id
				and 		orig_layer_id = :i
				and 		month_yr = :a_month_yr;
			
				if isNull( date_discount_rate ) then
					select 	annual_eff_rate
					into 		:date_discount_rate
					from 		aro_layer
					where 	aro_id = :i_aro_id
					and 		layer_id = :i;
			 
					update 	aro_wam_rate_calc_detailed
					set 		date_disc_rate = :date_discount_rate
					where 	aro_id = :i_aro_id
					and 		layer_id = :i_layer_id
					and 		orig_layer_id = :i
					and		month_yr = :a_month_yr;
					
					if sqlca.SQLCode < 0 then 
						messageBox( "Error", "Error occurred while updating Date Discount Rate on ARO WAM Rate Calc Detailed for prior layer ID " + string( i ) + ".~n~nError: " + sqlca.SQLErrText )
						rollback;
						return -1
					end if 
				end if 
		
		////
		////	If we're on the last iteration of the loop (the layer right before the current layer), update total dollars on the current layer.
		////	Only include dollars if the discount rate is not zero for the layer.
		////
				 if i = ( i_layer_id -1 ) then
					update 	aro_wam_rate_calc_detailed
					set 		total_cash_flow = 
					 			( 	select 	sum( nvl( remaining_cash_flow, 0 ) ) 
								 	from 		aro_wam_rate_calc_detailed
									where 	aro_id = :i_aro_id
									and 		layer_id = :i_layer_id
									and 		month_yr = :a_month_yr
									and		date_disc_rate <> 0 
								)
					where 	aro_id = :i_aro_id
					and 		layer_id = :i_layer_id
					and 		month_yr = :a_month_yr;
					
					if sqlca.SQLCode < 0 then 
						messageBox( "Error", "Error occurred while updating Total Cash Flow on ARO WAM Rate Calc Detailed for current layer ID " + string( i ) + ".~n~nError: " + sqlca.SQLErrText )
						rollback;
						return -1
					end if
				end if 
		next //layer loop
			
////
////	After the loop ends, we have remaining cash flow, disc rate, and total cash flow for each layer prior to current layer.
////	For example, if i_layer_id is 7, we will have 6 entries, with layer_id 1 through 6 in the table.
////	Next we must calculate the rate for each layer, then sum to get the new number.
////

////
////	Loop through and update the portion rate.
////
		for i = 1 to ( i_layer_id - 1 )
			update 	aro_wam_rate_calc_detailed
			set 		cash_flow_portion = decode( total_cash_flow, 0, 0, nvl( remaining_cash_flow, 0 ) / total_cash_flow ),
						portion_rate = decode( total_cash_flow, 0, 0, nvl( remaining_cash_flow, 0 ) / total_cash_flow ) * date_disc_rate
			where 	aro_id = :i_aro_id
			and 		layer_id = :i_layer_id
			and 		orig_layer_id = :i
			and 		month_yr = :a_month_yr;
			
			if sqlca.SQLCode < 0 then 
				messageBox( "Error", "Error occurred while updating Cash Flow Portion and Portion Rate on ARO WAM Rate Calc Detailed for original layer ID " + string( i ) + " (1).~n~nError: " + sqlca.SQLErrText )
				rollback;
				return -1
			end if
			
			update 	aro_wam_rate_calc_detailed
			set 		cash_flow_portion = decode( total_cash_flow, 0, 0, nvl( remaining_cash_flow, 0 ) / total_cash_flow ),
						portion_rate = nvl( remaining_cash_flow, 0 ) * date_disc_rate
			where 	aro_id = :i_aro_id
			and 		layer_id = :i_layer_id
			and 		orig_layer_id = :i
			and 		month_yr = :a_month_yr;
			
			if sqlca.SQLCode < 0 then 
				messageBox( "Error", "Error occurred while updating Cash Flow Portion and Portion Rate on ARO WAM Rate Calc Detailed for original layer ID " + string( i ) + " (2).~n~nError: " + sqlca.SQLErrText )
				rollback;
				return -1
			end if
		next

////
////	Update the total rate.
////
		update 	aro_wam_rate_calc_detailed
		set 		total_wam_rate = 
					( 	select 	sum( nvl( portion_rate, 0 ) ) 
						from 		aro_wam_rate_calc_detailed
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id
						and 		month_yr = :a_month_yr 
					)
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id 
		and 		month_yr = :a_month_yr;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while updating Total WAM Rate on ARO WAM Rate Calc Detailed for current layer ID " + string( i ) + ".~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if
		
////
////	Get the total cash flow.
////
		setNull( tot_cash_flow )
		
		select 	distinct nvl( total_cash_flow, 0 )
		into 		:tot_cash_flow
		from 		aro_wam_rate_calc_detailed
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id 
		and 		month_yr = :a_month_yr;
		
		if isNull( tot_cash_flow ) then tot_cash_flow = 0

////
////	Calculate the total rate.
////
		setNull( total_rate )

		if tot_cash_flow <> 0 then
			select 	distinct total_wam_rate / :tot_cash_flow
			into 		:total_rate
			from 		aro_wam_rate_calc_detailed
			where 	aro_id = :i_aro_id
			and 		layer_id = :i_layer_id 
			and 		month_yr = :a_month_yr;
		end if

		if isNull( total_rate ) then total_rate = 0

////
////	If the total rate is negative, make it zero.
////
		if total_rate <> 0 then
			if ( abs( total_rate ) / total_rate ) = -1 then
				total_rate = 0 
			end if 
		end if

////
////	Function completed successfully.
////
		return total_rate
end function

public function decimal uf_set_new_liability (decimal a_discount_rate, datetime a_start_date);/************************************************************************************************************************************************************
 **
 **	uf_set_new_liability()
 **	
 **	Calculates the new liability for the current ARO and layer ( stored as instance variables ).
 **
 **	This function is currently called from:
 **		- uf_calc_layer_balance()
 **	
 **	Parameters	:	decimal		:	( a_discount_rate ) The discount rate.
 **						datetime		:	( a_start_date ) The start date.
 **						
 **	Returns		:	decimal		:	1		:	The calculated new liability.
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			i, periods
decimal{2} 	new_liability, weighted_gross, stream_discounted, stream1, stream2, stream3
decimal{8}	prob1, prob2, prob3
datetime 	month_yr

////
////	Loop through the datastore.
////
		for i = 1 to i_ds_aro_streams.rowcount()
		////
		////	Set the microhelp.
		////
				////w_top_frame.setMicroHelp( "Calculating for Month ( " + string( i ) + "/" + string( i_ds_aro_streams.RowCount() ) + " )..." )
			
		////
		////	Calculate the weighted gross amount.
		////
				stream1 = i_ds_aro_streams.getItemDecimal( i, "stream1" )
				stream2 = i_ds_aro_streams.getItemDecimal( i, "stream2" )
				stream3 = i_ds_aro_streams.getItemDecimal( i, "stream3" )
				prob1 = i_ds_aro_streams.getItemDecimal( i, "prob1" )
				prob2 = i_ds_aro_streams.getItemDecimal( i, "prob2" )
				prob3 = i_ds_aro_streams.getItemDecimal( i, "prob3" )
				
				if isNull( stream1 ) then stream1 = 0
				if isNull( stream2 ) then stream2 = 0
				if isNull( stream3 ) then stream3 = 0
				if isNull( prob1 ) then prob1 = 0
				if isNull( prob2 ) then prob2 = 0
				if isNull( prob3 ) then prob3 = 0
				
				weighted_gross = stream1 * prob1 + stream2 * prob2 + stream3 * prob3
			
				if isNull( weighted_gross ) then weighted_gross = 0
			
		////
		////	Get the month yr.
		////
				month_yr = i_ds_aro_streams.getItemDateTime( i, "month_yr" )
				
				if isnull( month_yr ) then continue
				
		////
		////	Calculate the periods.
		////
				select 	months_between( :month_yr, :a_start_date ) 
				into 		:periods 
				from 		dual;
				
		////
		////	Calculate the new liability and stream discounted.
		////			
				new_liability = new_liability + round( ( weighted_gross / ( ( 1 + a_discount_rate / 12 ) ^ periods ) ), 2 )
				stream_discounted = round( ( weighted_gross / ( ( 1 + a_discount_rate / 12 ) ^ periods ) ), 2 )
		
		////
		////	Insert into ARO Layer Discounting.
		////
				insert into aro_layer_discounting
				( aro_id, layer_id, stream_id, month_yr, start_month, periods, cash_flow, discount_rate, discounted_flow, init_bal )
				values 
				( :i_aro_id, :i_layer_id, 0, :month_yr, :a_start_date, :periods, :weighted_gross, :a_discount_rate, :stream_discounted, 0 );
				
				if sqlca.SQLCode < 0 then
					messageBox( "Error", "Error occurred while inserting into ARO Layer Discounting.~n~nError: " + sqlca.SQLErrText )
					rollback;
					return -1
				end if
		next

////
////	Return the new liability.
////
		return new_liability
end function

public function decimal uf_calc_weighted_avg_rate (string a_up_down);/************************************************************************************************************************************************************
 **
 **	uf_calc_weighted_avg_rate()
 **	
 **	Calculates the weighted average rate to be used when calculating the layer balance.
 **	The calling function sets the instance variables.
 **
 **	This function is currently called from:
 **		- uf_calc_layer_balance()
 **	
 **	Parameters	:	string			:	( a_up_down ) Whether we're calculating "up" or "down".
 **						
 **	Returns		:	decimal		:	1		:	The calculated rate.
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			i, layer_id, num_months, months_between[], null_array[], check, month_number, counter
decimal{2} 	rem_cash_flow[], total_rem_cash_flow, total_rem_cash_flow_new, null_array2[], cash_flow, check_cash_flow, stream1, stream2, stream3, settle_adj
decimal{8} 	disc_rate[], weighted_ann_rate, discount_rate, null_array8[], annual_eff_rate, prob1, prob2, prob3
string 		sqls, s_rtn
datetime 	gl_posting_mo_yr, month_yr
boolean 		b_old_is_satisfied

uo_ds_top 	ds_cash_flows
uo_ds_top 	ds_cash_flows_time_avg 
uo_ds_top 	ds_old_layer_loop
uo_ds_top 	ds_rcf_loop
	
////
////	Get the GL Posting Mo Yr from the Estimate window.
////
//		if isValid( w_aro_estimate ) then
//			gl_posting_mo_yr = w_aro_estimate.dw_aro_layer.getItemDateTime( 1, "gl_posting_mo_yr" )
//		end if

		select gl_posting_mo_yr into :gl_posting_mo_yr
		from aro_layer
		where aro_id = :i_aro_id
		and layer_id = :i_layer_id;
		if sqlca.SQLCode <> 0 then
			messageBox( "Error", "Error occurred while retrieving the month for this ARO Layer.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if
		

////
////	Delete any existing records from the calculation table.
////
		delete from aro_wam_rate_calc
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if sqlca.SQLCode <> 0 then
			messageBox( "Error", "Error occurred while deleting from ARO WAM Rate Calc while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if

////
////	Create a datastore to retrieve the cash flows.  Use different SQL based on the rate type.
////
		if i_weighted_avg = i_rate_type_weighted then 
			sqls =	"	select 	aro_layer.aro_id aro_id, " + "~r~n" + &
					"				aro_layer.layer_id layer_id, " + "~r~n" + &
					"				aro_layer.annual_eff_rate annual_eff_rate, " + "~r~n" + &
					"				sum( aro_layer_stream.remaining_cash_flow ) remaining_cash_flow " + "~r~n" + &
					"	from 		aro_layer, " + "~r~n" + &
					"				aro_layer_stream " + "~r~n" + &
					"	where 	aro_layer.aro_id = aro_layer_stream.aro_id " + "~r~n" + &
					"	and 		aro_layer.layer_id = aro_layer_stream.layer_id " + "~r~n" + &
					"	and 		aro_layer.aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
					" 	and 		aro_layer.aro_status_id not in ( " + string( i_status_setup ) + ", " + string( i_status_inactive ) + " ) " + "~r~n" + &
					"	and 		aro_layer_stream.remaining_cash_flow <> 0 " + "~r~n" + &
					"	group by	aro_layer.aro_id, " + "~r~n" + &
					"				aro_layer.layer_id, " + "~r~n" + &
					"				aro_layer.annual_eff_rate "
		elseif i_weighted_avg = i_rate_type_time then
			if isNull( gl_posting_mo_yr ) then
				messageBox( "Error", "Could not determine the GL Posting Mo Yr on the current layer for calculating the weighted average rate." )
				rollback;
				return -1
			end if
			
			sqls =	"	select 	aro_layer.aro_id aro_id, " + "~r~n" + &
					"				aro_layer.layer_id layer_id, " + "~r~n" + &
					"				aro_layer.annual_eff_rate annual_eff_rate, " + "~r~n" + &
					"				sum( aro_layer_stream.remaining_cash_flow ) remaining_cash_flow, " + "~r~n" + &
					"				aro_layer_stream.month_yr month_yr " + "~r~n" + &
					"	from 		aro_layer, " + "~r~n" + &
					"				aro_layer_stream " + "~r~n" + &
					"	where 	aro_layer.aro_id = aro_layer_stream.aro_id " + "~r~n" + &
					"	and 		aro_layer.layer_id = aro_layer_stream.layer_id " + "~r~n" + &
					"	and 		aro_layer.aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
					" 	and 		aro_layer.aro_status_id not in ( " + string( i_status_setup ) + ", " + string( i_status_inactive ) + " ) " + "~r~n" + &
					"	and 		aro_layer_stream.remaining_cash_flow <> 0 " + "~r~n" + &
					"	group by	aro_layer.aro_id, " + "~r~n" + &
					"				aro_layer.layer_id, " + "~r~n" + &
					"				aro_layer.annual_eff_rate, " + "~r~n" + &
					"				aro_layer_stream.month_yr "
		end if

		ds_cash_flows = create uo_ds_top
		
		s_rtn = f_create_dynamic_ds( ds_cash_flows, "grid", sqls, sqlca, true )
		if s_rtn <> "OK" then
			messageBox( "Error", "Error occurred while creating the cash flows datastore.~n~nError: "+s_rtn )
			destroy ds_cash_flows
			rollback;
			return -1
		end if
				
////
////	Loop through the cash flows and populate the calculation table with the remaining cash flows.
////
		total_rem_cash_flow = 0
		for i = 1 to ds_cash_flows.rowCount()
		////
		////	Get the data from the row.
		////
				layer_id = ds_cash_flows.getItemNumber( i, "layer_id" )
				rem_cash_flow[i] = ds_cash_flows.getItemDecimal( i, "remaining_cash_flow" )
				disc_rate[i] = ds_cash_flows.getItemDecimal( i, "annual_eff_rate" )
				
				if isNull( rem_cash_flow[i] ) then rem_cash_flow[i] = 0
				if isNull( disc_rate[i] ) then disc_rate[i] = 0
			
		////
		////	If this ARO uses the time average rate type, get the number of months.
		////
				if i_weighted_avg = i_rate_type_time then
					month_yr = ds_cash_flows.getItemDateTime( i, "month_yr" )
					
					select 	months_between( :month_yr, :gl_posting_mo_yr ) 
					into 		:num_months 
					from 		dual;
					
					if isNull( num_months ) then num_months = 0
					months_between[i] = num_months
				end if		
			
		////
		////	Add to the total remaining cash flow.
		////
				total_rem_cash_flow += rem_cash_flow[i]	
			
		////
		////	Insert into the calculation table.
		////
				insert into aro_wam_rate_calc 
				( aro_id, layer_id, calc_layer_id, cash_flow, annual_eff_rate, month_yr, num_months )
				values 
				( :i_aro_id, :i_layer_id, :layer_id, :rem_cash_flow[i], :disc_rate[i], nvl( :month_yr, to_date( 190001, 'yyyymm' ) ), nvl( :num_months, 0 ) );
				
				if sqlca.SQLCode <> 0 then
					messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc while calculating the weighted average rate (for layer ID " + string( layer_id ) + ".~n~nError: " + sqlca.SQLErrText )
					destroy ds_cash_flows
					rollback;
					return -1
				end if
		next
		
////
////	We're finished with the datastore.
////
		destroy ds_cash_flows

////
////	Calculate the discount rate.
////	
		for i = 1 to upperBound( rem_cash_flow )
			discount_rate += ( rem_cash_flow[i] / total_rem_cash_flow ) * disc_rate[i]
		next

////
////	If this ARO uses the time average rate type, use the appropriate methodology for prior layers to get the discount rate.
////
		if i_weighted_avg = i_rate_type_time then 
			if i_layer_id = 1 then
				discount_rate = 0
			else
				discount_rate = uf_calc_rate_trial( rem_cash_flow[], disc_rate[], months_between[], discount_rate, "prior" )
			end if
		end if

////
////	If this ARO  uses the weighted average rate type, insert into the calculation table for the current layer.
////
		if i_weighted_avg = i_rate_type_weighted then 
			insert into aro_wam_rate_calc 
			( aro_id, layer_id, calc_layer_id, month_yr )
			values 
			( :i_aro_id, :i_layer_id, :i_layer_id, to_date( 190001, 'yyyymm' ) );
			
			if sqlca.SQLCode <> 0 then
				messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc while calculating the weighted average rate (for current layer).~n~nError: " + sqlca.SQLErrText )
				rollback;
				return -1
			end if
		end if
		
////
////	Discount Rate is equal to weighted average for all prior layers ( aka "Old Rate" ).
////
		update 	aro_wam_rate_calc
		set 		prior_wam_rate = :discount_rate
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if sqlca.SQLCode <> 0 then
			messageBox( "Error", "Error occurred while updating Prior WAM Rate on ARO WAM Rate Calc while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if
		
////
////	If this ARO uses the time average rate type, update the annual effective rate to be the discount rate.
////
		if i_weighted_avg = i_rate_type_time then
			update 	aro_wam_rate_calc
			set 		annual_eff_rate = :discount_rate
			where 	aro_id = :i_aro_id
			and 		layer_id = :i_layer_id;
			
			if sqlca.SQLCode <> 0 then
				messageBox( "Error", "Error occurred while updating Annual Effective Rate on ARO WAM Rate Calc for prior layers while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
				rollback;
				return -1
			end if
		end if

////
////	Clear out the arrays.
////
		rem_cash_flow[] = null_array2[]
		disc_rate[] = null_array8[]

////
////	Upward Estimate - Find Weighted Average Rates with possibility of multiple existing layers with Remaining Cash Flows
////	Get the weighted annual rate.
////	
		setNull( weighted_ann_rate )
		
		select 	weighted_ann_rate 
		into 		:weighted_ann_rate
		from 		aro_layer
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;

		if isNull( weighted_ann_rate ) then weighted_ann_rate = 0

////
////	Process based on the rate type.
////
		if i_weighted_avg = i_rate_type_weighted then
		////
		////	WEIGHTED AVERAGE
		////
		
		////
		////	Loop through the streams DW and store the remaining cash flows.
		////
				for i = 1 to i_ds_aro_streams.rowCount()
					stream1 = i_ds_aro_streams.getItemDecimal( i, "stream1" )
					stream2 = i_ds_aro_streams.getItemDecimal( i, "stream2" )
					stream3 = i_ds_aro_streams.getItemDecimal( i, "stream3" )
					prob1 = i_ds_aro_streams.getItemDecimal( i, "prob1" )
					prob2 = i_ds_aro_streams.getItemDecimal( i, "prob2" )
					prob3 = i_ds_aro_streams.getItemDecimal( i, "prob3" )
					settle_adj = i_ds_aro_streams.getItemDecimal( i, "settle_adj" )
					
					if isNull( stream1 ) then stream1 = 0
					if isNull( stream2 ) then stream2 = 0
					if isNull( stream3 ) then stream3 = 0
					if isNull( prob1 ) then prob1 = 0
					if isNull( prob2 ) then prob2 = 0
					if isNull( prob3 ) then prob3 = 0
					if isNull( settle_adj ) then settle_adj = 0
					
					rem_cash_flow[i] = stream1 * prob1 + stream2 * prob2 + stream3 * prob3 - settle_adj
					
					if isNull( rem_cash_flow[i] ) then rem_cash_flow[i] = 0
						
					total_rem_cash_flow_new += rem_cash_flow[i]
				next
		elseif i_weighted_avg = i_rate_type_time then
		////
		////	TIME AVERAGE
		////
		
		////
		//// 	First Loop - Find the Total RCF and store the New Layer's RCF and Dates into ARO WAM RATE CALC.
		////			
				for i = 1 to i_ds_aro_streams.rowCount()
				////
				////	Calculate the remaining cash flow for the row.
				////
				// maint 11150: BSB $$$: use i_ds_aro_streams
						stream1 = i_ds_aro_streams.getItemDecimal( i, "stream1" )
						stream2 =i_ds_aro_streams.getItemDecimal( i, "stream2" )
						stream3 = i_ds_aro_streams.getItemDecimal( i, "stream3" )
						prob1 = i_ds_aro_streams.getItemDecimal( i, "prob1" )
						prob2 = i_ds_aro_streams.getItemDecimal( i, "prob2" )
						prob3 = i_ds_aro_streams.getItemDecimal( i, "prob3" )
						settle_adj = i_ds_aro_streams.getItemDecimal( i, "settle_adj" )
						
						if isNull( stream1 ) then stream1 = 0
						if isNull( stream2 ) then stream2 = 0
						if isNull( stream3 ) then stream3 = 0
						if isNull( prob1 ) then prob1 = 0
						if isNull( prob2 ) then prob2 = 0
						if isNull( prob3 ) then prob3 = 0
						if isNull( settle_adj ) then settle_adj = 0
						
						rem_cash_flow[i] = stream1 * prob1 + stream2 * prob2 + stream3 * prob3 - settle_adj
						
						if isNull( rem_cash_flow[i] ) then rem_cash_flow[i] = 0
							
						total_rem_cash_flow_new += rem_cash_flow[i]
						disc_rate[i] = weighted_ann_rate
					
				////
				////	Get the month year and compute the number of months.
				////
						month_yr = i_ds_aro_streams.getItemDateTime( i, "month_yr" )
						
						select 	months_between( :month_yr, :gl_posting_mo_yr ) 
						into 		:num_months 
						from 		dual;
						
				////
				////	Prior periods should reduce remaining cash flow on prior layer.
				////
						if num_months < 0 then
							total_rem_cash_flow_new = total_rem_cash_flow_new - rem_cash_flow[i]
							disc_rate[i] = discount_rate
							total_rem_cash_flow = total_rem_cash_flow - rem_cash_flow[i]			
					
							setNull( check )
							
							select 	count(*)
							into 		:check
							from 		aro_wam_rate_calc
							where 	aro_id = :i_aro_id
							and 		layer_id = :i_layer_id
							and 		calc_layer_id = :i_layer_id - 1
							and 		month_yr = :month_yr;
							
							if isNull( check ) then check = 0
				
							if check > 0 then
								update 	aro_wam_rate_calc
								set 		cash_flow = cash_flow - :rem_cash_flow[i]
								where 	aro_id = :i_aro_id
								and 		layer_id = :i_layer_id
								and 		calc_layer_id = :i_layer_id - 1
								and 		month_yr = :month_yr;
								
								if sqlca.SQLCode <> 0 then
									messageBox( "Error", "Error occurred while updating Cash Flow on ARO WAM Rate Calc for prior settlements while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
									rollback;
									return -1
								end if
							else
								insert into aro_wam_rate_calc 
								( aro_id, layer_id, calc_layer_id, cash_flow, annual_eff_rate, month_yr, num_months )
								values 
								( :i_aro_id, :i_layer_id, :i_layer_id -1, -1 * :rem_cash_flow[i], :discount_rate, :month_yr, :num_months );
							
								if sqlca.SQLCode <> 0 then
									messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc for prior settlements while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
									rollback;
									return -1
								end if
							end if
						else	
							insert into aro_wam_rate_calc 
							( aro_id, layer_id, calc_layer_id, cash_flow, annual_eff_rate, month_yr, num_months )
							values 
							( :i_aro_id, :i_layer_id, :i_layer_id, :rem_cash_flow[i], :weighted_ann_rate, :month_yr, :num_months );
							
							if sqlca.SQLCode <> 0 then
								messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc while calculating the weighted average rate (current).~n~nError: " + sqlca.SQLErrText )
								rollback;
								return -1
							end if
						end if
				next //i_ds_aro_streams loop
	
				if a_up_down = "down" then 
					goto the_end
				end if

		////
		//// 	Now all the data that we need is in ARO WAM RATE CALC, do 2 More loops
		//// 	1a ) Loop through all Prior Layers' Time Periods and determine if the date is in the New Layer. If so, then take the min( prior_layer_rcf, new_layer_rcf ) and reduce the total RCF for the prior layers
		//// 	1b ) If Date is not in New Layer then skip it and don"t reduce the total Prior RCF.		
		//// 	2 ) Loop Through New Layer"s Time Periods startest with the Furthest Date and Satisfy the Total RCF
		////
	
		////
		////	Change New Cash Flow to only be incremental portion.
		////
				total_rem_cash_flow_new = total_rem_cash_flow_new - total_rem_cash_flow
	
		////
		////	Create a datastore to retrieve the prior layers.
		////
				ds_old_layer_loop = create uo_ds_top
				
				sqls =	"	select 	sum( cash_flow ) cash_flow, " + "~r~n" + &
						"				to_number( to_char( month_yr, 'yyyymm' ) ) month_number, " + "~r~n" + &
						"				annual_eff_rate " + "~r~n" + &
						" 	from 		aro_wam_rate_calc " + &
						" 	where 	aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
						" 	and 		layer_id = " + string( i_layer_id ) + " " + "~r~n" + &
						" 	and 		calc_layer_id < " + string( i_layer_id ) + " " + "~r~n" + &
						" 	group by	to_number( to_char( month_yr, 'yyyymm' ) ), " + "~r~n" + &
						"				annual_eff_rate " + "~r~n" + &
						" 	having 	sum( cash_flow ) <> 0 "
						
				s_rtn = f_create_dynamic_ds( ds_old_layer_loop, "grid", sqls, sqlca, true )
				
				if s_rtn <> "OK" then
					messageBox( "Error", "Error occurred while creating the prior layer datastore.~n~nError: "+s_rtn )
					destroy ds_old_layer_loop
					rollback;
					return -1
				end if
				
		////
		////	Loop through the prior layers.
		////	
				for i = 1 to ds_old_layer_loop.rowCount()
				////
				////	Get the data from the row.
				////
						check_cash_flow = 0
						cash_flow = ds_old_layer_loop.getItemNumber( i, "cash_flow" )
						month_number = ds_old_layer_loop.getItemNumber( i, "month_number" )		
						annual_eff_rate = ds_old_layer_loop.getItemDecimal( i, "annual_eff_rate" )	
					
				////
				////	Check to make sure there is a cash flow.  If not, skip this row.
				////
						setNull( check_cash_flow)
						
						select 	least( least( cash_flow, :cash_flow ), :total_rem_cash_flow )
						into 		:check_cash_flow
						from 		aro_wam_rate_calc
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id
						and 		calc_layer_id = :i_layer_id
						and 		to_char( month_yr, 'yyyymm' ) = :month_number
						and 		cash_flow <> 0;
						
						if isNull( check_cash_flow ) then check_cash_flow = 0
						if check_cash_flow = 0 then continue
						
				////
				////	Update the total remaining cash flow.
				////
						total_rem_cash_flow = total_rem_cash_flow - check_cash_flow
						
				////
				////	Insert into the calculation table.
				////
						insert into aro_wam_rate_calc 
						( aro_id, layer_id, calc_layer_id, cash_flow, annual_eff_rate, month_yr, num_months )
						select 	:i_aro_id, 
									:i_layer_id, 
									0, 
									:check_cash_flow, 
									:annual_eff_rate, 
									to_date( :month_number, 'yyyymm' ), 	
									months_between( to_date( :month_number, 'yyyymm' ), :gl_posting_mo_yr )
						from 		dual;
						
						if sqlca.SQLCode <> 0 then
							messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc for Prior Layers (Loop 1) while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
							destroy ds_old_layer_loop
							rollback;
							return -1
						end if
				next //ds_old_layer_loop loop
				
		////
		////	We're finished with the datastore.
		////
				destroy ds_old_layer_loop
	
		////
		//// 	Now total_rem_cash_flow = the RCF to be discounted at the Old Rate, and total_rem_Cash_flow_new = the RCF to be discounted at the New Rate
		//// 	Start from furthest date and new Rate to satisfy Remaining Cash Flow, then move to old rate.
		////
	
		////
		////	Create a datastore of layers with remaining cash flows.
		////
				ds_rcf_loop = create uo_ds_top
				
				sqls =	"	select 	sum( decode( calc_layer_id, 0, -cash_flow, cash_flow ) ) cash_flow, " + "~r~n" + &
						"				to_number( to_char( month_Yr, 'yyyymm' ) ) month_number " + "~r~n" + &
						" 	from 		aro_wam_rate_calc " + "~r~n" + &
						" 	where 	aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
						" 	and 		layer_id = " + string( i_layer_id ) + " " + "~r~n" + &
						" 	and 		( calc_layer_id = " + string( i_layer_id ) + " or calc_layer_id = 0 ) " + "~r~n" + &
						" 	group by	to_number( to_char( month_yr, 'yyyymm' ) ) " + "~r~n" + &
						" 	having 	sum( decode( calc_layer_id, 0, -cash_flow, cash_flow ) ) <> 0 " + "~r~n" + &
						" 	order by	to_number( to_char( month_yr, 'yyyymm' ) ) desc "
				
				s_rtn = f_create_dynamic_ds( ds_rcf_loop, "grid", sqls, sqlca, true )
				
				if s_rtn <> "OK" then
					messageBox( "Error", "Error occurred while creating the prior layer datastore for remaining cash flows.~n~nError: "+s_rtn )
					destroy ds_rcf_loop
					rollback;
					return -1
				end if
	
		////
		////	Loop through the remaining cash flows.
		////
				b_old_is_satisfied = false
				counter = 1
				for i = 1 to ds_rcf_loop.rowCount()
				////
				////	Get the data from the row.
				////
						cash_flow = ds_rcf_loop.getItemDecimal( i, "cash_flow" )
						month_number = ds_rcf_loop.getItemNumber( i, "month_number" )
						
				////
				////	Process based on the cash flow amount.
				////				
						if cash_flow <= total_rem_cash_flow_new and total_rem_cash_flow >= 0 and not b_old_is_satisfied then
							//Cash Flow can be satisfied with the New Rate
							insert into aro_wam_rate_calc 
							( aro_id, layer_id, calc_layer_id, cash_flow, annual_eff_rate, month_yr, num_months )
							select 	:i_aro_id, 
										:i_layer_id, 
										-1 * :counter, 
										:cash_flow, 
										:weighted_ann_rate, 
										to_date( :month_number, 'yyyymm' ), 	
										months_between( to_date( :month_number, 'yyyymm' ), :gl_posting_mo_yr )
							from 		dual;
							
							if sqlca.SQLCode <> 0 then
								messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc for Prior Layers (Loop 2) while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
								destroy ds_rcf_loop
								rollback;
								return -1
							end if
							
							total_rem_cash_flow_new = total_rem_cash_flow_new - cash_flow
							cash_flow = 0
							counter++
						elseif cash_flow > total_rem_cash_flow_new and total_rem_cash_flow_new > 0 and not b_old_is_satisfied then
							//Cash Flow can be more than satisfied with the New Rate
							insert into aro_wam_rate_calc 
							( aro_id, layer_id, calc_layer_id, cash_flow, annual_eff_rate, month_yr, num_months )
							select 	:i_aro_id, 
										:i_layer_id, 
										-1 * :counter, 
										:total_rem_cash_flow_new, 
										:weighted_ann_rate, 
										to_date( :month_number, 'yyyymm' ), 	
										months_between( to_date( :month_number, 'yyyymm' ), :gl_posting_mo_yr )
							from 		dual;
							
							if sqlca.SQLCode <> 0 then
								messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc for Prior Layers (Loop 2) while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
								destroy ds_rcf_loop
								rollback;
								return -1
							end if
							
							cash_flow = cash_flow - total_rem_cash_flow_new
							total_rem_cash_flow_new = 0
							counter++		
							b_old_is_satisfied = true			
						end if
					
						if b_old_is_satisfied then
							//Used Old Rate for the remainder of the cash flows
							insert into aro_wam_rate_calc 
							( aro_id, layer_id, calc_layer_id, cash_flow, annual_eff_rate, month_yr, num_months )
							select 	:i_aro_id, 
										:i_layer_id, 
										-1 * :counter, 
										:cash_flow, 
										:discount_rate, 
										to_date( :month_number, 'yyyymm' ), 	
										months_between( to_date( :month_number, 'yyyymm' ), :gl_posting_mo_yr )
							from 		dual;
							
							if sqlca.SQLCode <> 0 then
								messageBox( "Error", "Error occurred while inserting into ARO WAM Rate Calc for Prior Layers (Loop 2) while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
								destroy ds_rcf_loop
								rollback;
								return -1
							end if
						end if
				next //ds_rcf_loop loop
		end if //i_weighted_avg
		
////
////	We're finished with the datastore.
////
		destroy ds_rcf_loop

the_end:

////
////	If this is an "up" calculation, compute the discount rate.
////
		if a_up_down = "up" then
		////
		////	If this is a weighted average ARO, calculate the discount rate with the data we have.
		////	If this is a time average ARO, retrieve data from ARO WAM Rate Calc and call the trial function on the Estimate window.
		////
				if i_weighted_avg = i_rate_type_weighted then
				////
				////	Calculate the rate.
				////
						discount_rate = ( ( total_rem_cash_flow / total_rem_cash_flow_new ) * discount_rate ) + ( ( ( total_rem_cash_flow_new - total_rem_cash_flow ) / total_rem_cash_flow_new ) * weighted_ann_rate )
				elseif i_weighted_avg = i_rate_type_time then	
				////
				////	Retrieve the cash flows.
				////
						ds_cash_flows_time_avg = create uo_ds_top
						
						sqls =	"	select 	sum( cash_flow ) cash_flow, " + "~r~n" + &
								"				annual_eff_rate, " + "~r~n" + &
								"				num_months " + "~r~n" + &
								" 	from 		aro_wam_rate_calc " + "~r~n" + &
								" 	where 	aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
								" 	and 		layer_id = " + string( i_layer_id ) + " " + "~r~n" + &
								" 	and 		calc_layer_id <= 0 " + "~r~n" + &
								" 	group by	annual_eff_rate, num_months "
								
						s_rtn = f_create_dynamic_ds( ds_cash_flows_time_avg, "grid", sqls, sqlca, true )
						
						if s_rtn <> "OK" then
							messageBox( "Error", "Error occurred while creating the cash flows datastore for time average calculation.~n~nError: "+s_rtn )
							destroy ds_cash_flows_time_avg
							rollback;
							return -1
						end if
					
				////
				////	Clear out the arrays.
				////
						months_between = null_array
						rem_cash_flow[] = null_array2[]
						disc_rate[] = null_array8[]
					
				////
				////	Loop through the rows and store the data in the arrays.
				////
						for i = 1 to ds_cash_flows_time_avg.rowCount()
							months_between[i] = ds_cash_flows_time_avg.getItemNumber( i, "num_months" )
							rem_cash_flow[i] = ds_cash_flows_time_avg.getItemDecimal( i, "cash_flow" )
							disc_rate[i] = ds_cash_flows_time_avg.getItemDecimal( i, "annual_eff_rate" )
						next
					
				////
				////	Call the function on the Estimate window.
				////
						discount_rate = uf_calc_rate_trial( rem_cash_flow[], disc_rate[], months_between[], discount_rate, "prior" )
					
				////
				////	We're finished with the datastore.
				////
						destroy ds_cash_flows_time_avg
				end if //weighted or time
		end if //up

////
////	If this is a weighted average ARO, update the cash flow and annual effective rate on the calculation table.
////
		if i_weighted_avg = i_rate_type_weighted then 
			update 	aro_wam_rate_calc
			set 		cash_flow = :total_rem_cash_flow_new,
						annual_eff_rate = :weighted_ann_rate
			where 	aro_id = :i_aro_id
			and 		layer_id = :i_layer_id
			and 		calc_layer_id = :i_layer_id;
			
			if sqlca.SQLCode <> 0 then
				messageBox( "Error", "Error occurred while updating Cash Flow and Annual Effective Rate on ARO WAM Rate Calc while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
				rollback;
				return -1
			end if
		end if

////
////	Update the WAM rate on the calculation table.
////
		update 	aro_wam_rate_calc
		set 		wam_rate = :discount_rate, 
					up_down = :a_up_down
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if sqlca.SQLCode <> 0 then
			messageBox( "Error", "Error occurred while updating WAM Rate and Up/Down on ARO WAM Rate Calc while calculating the weighted average rate.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if

////
////	Return the calculated discount rate.
////
		return discount_rate
end function

public function integer uf_calc_settle_adj (ref datawindow a_dw_streams, longlong a_aro_id, longlong a_layer_id);/************************************************************************************************************************************************************
 **
 **	uf_calc_settle_adj()
 **	
 **	Calculates the settlement adjustments.
 **
 **	This function is currently called from:
 **		- w_aro_estimate - wf_calc_settle_adj()
 **		- w_aro_pp_approval - cb_calc.clicked()
 **	
 **	Parameters	:	datawindow	:	( a_dw_streams ) The datawindow with the streams data.
 **						longlong	:	( a_aro_id ) The ARO to calculate.
 **						longlong	:	( a_layer_id ) The layer to calculate.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 			row, month_number, layer_id, i, j, periods, layer_status, pre_month_number
decimal{2} 	cur_liab, remaining_cash_flow, layer_date_liab, settle_adj, new_remaining_cash_flow, temp_settle_adj, orig_liab, settled
decimal{8} 	discount_rate
string 		sqls, s_rtn, sqls2, mm, dd, yyyy, s_dt_compare
datetime 	gl_posting_mo_yr, month_yr, approval_date
boolean 		b_adjusted

uo_ds_top 	ds_layers
uo_ds_top	ds_layer_dates

////
////	Set the instance variables.
////
		i_aro_id =a_aro_id
		i_layer_id =a_layer_id

////
////	Get the data from the layer.
////
		setNull( gl_posting_mo_yr )
		setNull( approval_date )
		setNull( layer_status )
		
		select		gl_posting_mo_yr, 
					approval_date,
					aro_status_id
		into 		:gl_posting_mo_yr, 
					:approval_date,
					:layer_status
		from 		aro_layer 
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
////
////	Determine additional variables.
////	If "detailed rates" is null, default to "No" (0).
////	If "skip layer adj" is null, default to "No" (0).  This means new layers use rates from other layers.
////
		setNull( i_detailed_rates )
		setNull( i_skip_layer_adj )
		setNull( i_aro_type )
		
		select 	detailed_rates, 
					skip_layer_adj, 
					aro_type_id
		into 		:i_detailed_rates, 
					:i_skip_layer_adj, 
					:i_aro_type
		from 		aro
		where 	aro_id = :i_aro_id;

		if isNull( i_detailed_rates ) then i_detailed_rates = 0
		if isNull( i_skip_layer_adj ) then i_skip_layer_adj = 0 

////
////	If we aren't processing an expected datawindow, return.
////
		if a_dw_streams.dataobject <> "dw_aro_streams_settle_adj" and a_dw_streams.dataobject <> "dw_aro_pp_streams" and a_dw_streams.dataobject <> "dw_aro_streams_wam" then return 1
		
////
////	If the layer status isn't "Setup", return.
////	Should we look at the historic settleadj and include it here on an approved layer??
////
		if layer_status <> i_status_setup then return 1
	
////
////	If there is an approval date, return.
////
		if not isNull( approval_date ) then return 1

////
////	Zero out the current settlement adjustment on all rows.
////
		for i = 1 to a_dw_streams.rowCount()
			a_dw_streams.setItem( i, "settle_adj", 0 )
		next

////
////	Calculate the month number.
////
		month_number = year( date( gl_posting_mo_yr ) ) * 100 + month( date( gl_posting_mo_yr ) )

////
////	Delete any existing rows from ARO Settlement Adj.
////
		delete from aro_settlement_adj
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while deleting from ARO Settlement Adj.~n~nError: " + sqlca.SQLErrText )
			rollback;
			return -1
		end if

////
////	Get the previous layer's month.
////	If this is the first layer, set the month number to 190001.
////	Otherwise, convert the GL Posting Mo Yr from the previous layer.
////
		if i_layer_id = 1 then 
			pre_month_number = 190001
		else
			select 	to_char( gl_posting_mo_yr, 'yyyymm' )
			into 		:pre_month_number
			from 		aro_layer
			where 	aro_id = :i_aro_id
			and 		layer_id = :i_layer_id - 1;
		end if
		
////
////	Create the layers datastore.
////
		ds_layers = create uo_ds_top

////
////	If we're using the WAM version of the streams datawindow, add prior settlements where month_yr < month_number.
////	Then return.
////
		if a_dw_streams.dataobject = "dw_aro_streams_wam" then 
		////
		////	Create a datastore to retrieve prior settlements by month.
		////
				sqls =	"	select 	sum( settled ) settled, " + "~r~n" + &
						"				month_yr " + "~r~n" + &
						" 	from 		aro_liability " + "~r~n" + &
						" 	where 	aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
						" 	and 		to_char( month_yr, 'yyyymm' ) < " + string( month_number ) + " " + "~r~n" + &
						" 	and 		to_char( month_yr, 'yyyymm' ) >= " + string( pre_month_number ) + " " + "~r~n" + &
						" 	group by	month_yr " + &
						" 	having	sum( settled ) <> 0 " + &
						" 	order by	2 "
						
				s_rtn = f_create_dynamic_ds( ds_layers, "grid", sqls, sqlca, true )
				if s_rtn <> "OK" then
					messageBox( "Error", "Error occurred while creating the settlement datastore.~n~nError: "+s_rtn )
					destroy ds_layers
					rollback;
					return -1
				end if
			
		////
		////	Loop through the rows.
		////
				for i = 1 to ds_layers.rowCount() 
				////
				////	On the first iteration, delete the last row of the passed in DW.
				////
						if i = 1 then
							a_dw_streams.deleterow( a_dw_streams.rowCount() )
						end if
					
				////
				////	Get the data from the row.
				////
						month_yr = ds_layers.getItemDateTime( i, "month_yr" )
						settled = ds_layers.getItemDecimal( i, "settled" )
						
				////
				////	Get the month, date, and year and create a string of the date.
				////					
						select 	lpad( to_char( :month_yr, 'mm' ), 2, '0' ) mm, 
									lpad( to_char( :month_yr, 'dd' ), 2, '0' ) dd, 
									to_char( :month_yr, 'yyyy' ) yyyy
						into 		:mm, 
									:dd, 
									:yyyy
						from 		dual;
						
						s_dt_compare = mm + "/" + dd + "/" + yyyy
					
				////
				////	Find the matching row in the DW for this month_yr.
				////
						row = a_dw_streams.find( "month_yr = date( '" + s_dt_compare + "' )", 1, a_dw_streams.rowCount() )
					
				////
				////	If we found a match, set the settlement amount.
				////	Otherwise, insert a new row.
				////
						if row > 0 then 
							a_dw_streams.setItem( row, "settle_adj", settled )
						else
							row = a_dw_streams.insertRow( 1 )
							
							a_dw_streams.setItem( row, "month_yr", month_yr )
							a_dw_streams.setItem( row, "settle_adj", settled )
							a_dw_streams.SetItem( row, "current_estimate", 0 )
							a_dw_streams.SetItem( row, "stream1", 0 )
							a_dw_streams.SetItem( row, "stream2", 0 )
							a_dw_streams.SetItem( row, "stream3", 0 )
							a_dw_streams.SetItem( row, "prob1", 1 )
							a_dw_streams.SetItem( row, "prob2", 0 )
							a_dw_streams.SetItem( row, "prob3", 0 )
							a_dw_streams.SetItem( row, "aro_id", i_aro_id )
							a_dw_streams.SetItem( row, "layer_id", i_layer_id )
							a_dw_streams.SetItem( row, "net_change", 0 )
							a_dw_streams.SetItem( row, "not_changed", 0 )
							a_dw_streams.SetItem( row, "date_disc_rate", 0 )
							
							a_dw_streams.sort()
						end if
					
				////
				////	If this is the last row, insert a row with all zeroes.
				////
						if i = ds_layers.rowCount() then
							row = a_dw_streams.insertRow( 0 )
							
							a_dw_streams.setItem( row, "settle_adj", 0 )
							a_dw_streams.SetItem( row, "current_estimate", 0 )
							a_dw_streams.SetItem( row, "stream1", 0 )
							a_dw_streams.SetItem( row, "stream2", 0 )
							a_dw_streams.SetItem( row, "stream3", 0 )
							a_dw_streams.SetItem( row, "prob1", 1 )
							a_dw_streams.SetItem( row, "prob2", 0 )
							a_dw_streams.SetItem( row, "prob3", 0 )
							a_dw_streams.SetItem( row, "aro_id", i_aro_id )
							a_dw_streams.SetItem( row, "layer_id", i_layer_id )
							a_dw_streams.SetItem( row, "net_change", 0 )
							a_dw_streams.SetItem( row, "not_changed", 0 )
							a_dw_streams.SetItem( row, "date_disc_rate", 0 )		
							
							a_dw_streams.groupcalc()
							a_dw_streams.groupcalc()
						end if
					
				next //ds_layers loop

		////
		////	We're finished.
		////
				destroy ds_layers
				return 1	
		end if //a_dw_streams.dataobject = "dw_aro_streams_wam"
			
////
////	Process based on whether or not this ARO uses detailed rates.
////
		if i_detailed_rates <> 1 then 
		////
		////	Process adjustment based on layer data and use the Last Date/First Off approach.
		////	Only adjust due to layers posted before the current month.
		////
				sqls =	"	select 	aro_liability.layer_id layer_id, " + "~r~n" + &
						"				( aro_liability.beg_liability + aro_liability.new_incurred + aro_liability.accreted + aro_liability.revised + aro_liability.input_gain_loss + aro_liability.settled + aro_liability.accretion_adjust + aro_liability.liability_adjust ) cur_liab, " + "~r~n" + &
						"				aro_layer.discount_rate discount_rate " + "~r~n" + &
						"	from 		aro_liability, " + "~r~n" + &
						"				aro_layer " + "~r~n" + &
						"	where 	to_char( aro_liability.month_yr, 'yyyymm' ) = " + string( month_number ) + " " + "~r~n" + &
						"	and 		aro_liability.aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
						"	and 		aro_liability.aro_id = aro_layer.aro_id " + "~r~n" + &
						"	and 		aro_liability.layer_id = aro_layer.layer_id " + "~r~n" + &
						"	and 		to_number( to_char( aro_layer.gl_posting_mo_yr, 'yyyymm' ) ) < " + string( month_number ) + " " + "~r~n" + &
						"	order by	aro_liability.layer_id"
			
				s_rtn = f_create_dynamic_ds( ds_layers, "grid", sqls, sqlca, true )
				if s_rtn <> "OK" then
					messageBox( "Error", "Error occurred while creating the settlement datastore.~n~nError: "+s_rtn )
					destroy ds_layers
					rollback;
					return -1
				end if
				
		////
		////	Create the layer dates datastore.
		////
				ds_layer_dates = create uo_ds_top
				
		////
		//// Loop through th layers.
		////
				for i = 1 to ds_layers.rowCount()
				////
				////	Initialize the boolean.
				////
						b_adjusted = false
					
				////
				////	Get the data from the row.
				////
						layer_id = ds_layers.getItemNumber( i, "layer_id" )
						cur_liab = ds_layers.getItemDecimal( i, "cur_liab" )
						discount_rate = ds_layers.getItemDecimal( i, "discount_rate" )
						
				////
				////	Set the original liability.
				////
						orig_liab = cur_liab
			 
				////
				////	Build a datastore to retrieve the remaining cash flow from the streams for this layer.
				////
						sqls2 =	"	select 	month_yr, " + "~r~n" + &
									"				remaining_cash_flow " + "~r~n" + &
									"	from 		aro_layer_stream " + "~r~n" + &
									"	where 	aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
									"	and 		layer_id = " + string( layer_id ) + " " + "~r~n" + &
									"	and 		stream_id = 0 " + "~r~n" + &
									"	and 		remaining_cash_flow <> 0 " + "~r~n" + &
									"	order by	month_yr desc "
						
						s_rtn = f_create_dynamic_ds( ds_layer_dates, "grid", sqls2, sqlca, true )
						if s_rtn <> "OK" then
							messageBox( "Error", "Error occurred while creating the cash flow dates datastore.~n~nError: "+s_rtn )
							destroy ds_layers
							destroy ds_layer_dates
							rollback;
							return -1
						end if
						
				////
				////	If there are no rows, continue.
				////
						if ds_layer_dates.rowCount() = 0 then
							if cur_liab = 0 then continue
							messageBox( "Warning", "Liability exists on layer " + string( layer_id ) + " but no remaining cash flows exist, so a settlement adjustment could not be computed.  Please make liability adjustments to move the liability to a layer with remaining cash flows.", StopSign! )
							continue
						end if
						
				////
				//// 	Loop through remaining cash flow dates.
				////
						for j = 1 to ds_layer_dates.rowCount()
						////
						////	Get the data from the row.
						////
								month_yr = ds_layer_dates.getItemDateTime( j, "month_yr" )
								remaining_cash_flow = ds_layer_dates.getItemDecimal( j, "remaining_cash_flow" )
							
						////
						////	Get the month, date, and year and create a string of the date.
						////					
								select 	lpad( to_char( :month_yr, 'mm' ), 2, '0' ) mm, 
											lpad( to_char( :month_yr, 'dd' ), 2, '0' ) dd, 
											to_char( :month_yr, 'yyyy' ) yyyy
								into 		:mm, 
											:dd, 
											:yyyy
								from 		dual;
								
								s_dt_compare = mm + "/" + dd + "/" + yyyy
								
						////
						////	Get the periods.
						////
								select 	months_between( :month_yr, :gl_posting_mo_yr ) 
								into 		:periods 
								from 		dual;
							
						////
						////	Calculate the liability for this layer date.
						////
								layer_date_liab = round( ( remaining_cash_flow / ( ( 1 + discount_rate / 12 ) ^ periods ) ), 2 )
							
						////
						////	Deal with negatives and positives.
						////
								if ( cur_liab <= layer_date_liab and orig_liab >= 0 ) or ( cur_liab >= layer_date_liab and orig_liab < 0 ) then 
								////
								//// 	Need to have a Settlement Adjustment for this Layer/Date.
								//// 	Need to find the Present Value of the Remaining Cash Flow X such that
								//// 	X discounted back from Month Yr to GL Posting Mo Yr @ Layer Discount Rate = Cur Liab
								//// 	==> The Future Value of Cur Liab to Month Yr = X
								////
										b_adjusted = true
									
								////
								////	Calculate the new remaining cash flow and settlement adjustment.
								////	The entire remaining liab is now accounted for, so make it zero.
								////
										new_remaining_cash_flow = round( ( cur_liab * ( ( 1 + discount_rate / 12 ) ^ periods ) ), 2 )
										settle_adj = new_remaining_cash_flow - remaining_cash_flow 
										cur_liab = 0
									
								////
								////	Find the matching row for this month_yr in the passed-in DW.
								////
										row = a_dw_streams.find( "month_yr = date( '" + s_dt_compare + "' )", 1, a_dw_streams.rowCount() )
										
								////
								////	Get the settlement adjustment from the row and add it to the settlement adjustment calculated above.
								////
										temp_settle_adj = a_dw_streams.getItemDecimal( row, "settle_adj" )
										if isNull( temp_settle_adj ) then temp_settle_adj = 0
										temp_settle_adj = temp_settle_adj + settle_adj
							
								////
								////	If there is a value, insert into ARO Settlement Adj.
								////
										if settle_adj <> 0 then 
											insert into aro_settlement_adj 
											( aro_id, layer_id, layer_adj_id, stream_id, month_yr, remaining_cash_flow, settlement_adj )
											values 
											( :i_aro_id, :i_layer_id, :layer_id, 0, :month_yr, :remaining_cash_flow, :settle_adj );
											
											if sqlca.SQLCode < 0 then 
												messageBox( "Error", "Error occurred while inserting into ARO Settlement Adj.~n~nError: " + sqlca.SQLErrText )
												destroy ds_layers
												destroy ds_layer_dates
												rollback;
												return -1
											end if
										end if
										
								////
								////	Set the temp value on the passed-in DW.
								////
										a_dw_streams.setItem( row, "settle_adj", temp_settle_adj )
								else 
								////
								////	No settlement adjustment.
								////
										settle_adj = 0
										cur_liab = cur_liab - layer_date_liab
								end if
							
						////
						////	Add more cases for orig liab signs ( this appears to be for minor rounding amounts ).
						////
								if cur_liab < 0 and orig_liab > 0 then cur_liab = 0
								if cur_liab > 0 and orig_liab < 0 then cur_liab = 0
						next //ds_layer_dates loop
			 
				////
				////	If we haven't adjusted and we have a date, check to see if we need to adjust the remaining cash flow upwards.
				////	( i.e., the liability is greater than the sum of the Discounted Remaining Cash Flows )
				//// 	- Current Month_Yr variables is the date we want to adjust ( and Periods )
				//// 	- Cur_Liab currently has the remaining liability we need to account for.
				////
						if not b_adjusted and s_dt_compare <> "" and not isNull( s_dt_compare ) then
						////
						////	Find the matching row for this month_yr in the passed-in DW.
						////
								row = a_dw_streams.find( "month_yr = date( '" + s_dt_compare + "' )", 1, a_dw_streams.rowCount() )
							
						////
						////	Compute the settlement adjustment.
						////
								settle_adj = round( ( cur_liab * ( ( 1 + discount_rate / 12 ) ^ periods ) ), 2 )
								temp_settle_adj = a_dw_streams.getItemDecimal( row, "settle_adj" )
								if isNull( temp_settle_adj ) then temp_settle_adj = 0
								temp_settle_adj = temp_settle_adj + settle_adj
								
						////
						////	If there is a value, insert into ARO Settlement Adj.
						////
								if settle_adj <> 0 then 
									insert into aro_settlement_adj 
									( aro_id, layer_id, layer_adj_id, stream_id, month_yr, remaining_cash_flow, settlement_adj )
									values 
									( :i_aro_id, :i_layer_id, :layer_id, 0, :month_yr, :remaining_cash_flow, :settle_adj );
									
									if sqlca.SQLCode < 0 then 
										messageBox( "Error", "Error occurred while inserting into ARO Settlement Adj.~n~nError: " + sqlca.SQLErrText )
										destroy ds_layers
										destroy ds_layer_dates
										rollback;
										return -1
									end if
								end if		
						
						////
						////	Set the temp value on the passed-in DW.
						////
								a_dw_streams.setItem( row, "settle_adj", temp_settle_adj )
						end if
				next //ds_layers loop
				
		////
		////	We're finished with the datastore.
		////
				destroy ds_layer_dates
		else 
		////
		////	If using detailed rates, check liability on accretion detail against liability on header.
		////	Use Header Rate if Detailed Rate is Null.
		////
				sqls =	"	select 	aro_liability_accr_dtl.layer_id layer_id, " + "~r~n" + &
						"				aro_layer_stream.month_yr month_yr, " + "~r~n" + &
						"				( aro_liability_accr_dtl.beg_liability + aro_liability_accr_dtl.liability_alloc + aro_liability_accr_dtl.accreted + aro_liability_accr_dtl.revised + aro_liability_accr_dtl.settled + aro_liability_accr_dtl.accretion_adjust + aro_liability_accr_dtl.liability_adjust ) cur_liab, " + "~r~n" + &
						"				nvl( aro_layer_stream.date_disc_rate, aro_layer.annual_eff_rate ) date_disc_rate, " + "~r~n" + &
						"				aro_layer_stream.remaining_cash_flow remaining_cash_flow "  + "~r~n" + &
						"	from 		aro_liability_accr_dtl, " + "~r~n" + &
						"				aro_layer_stream, " + "~r~n" + &
						"				aro_layer " + "~r~n" + &
						"	where 	to_char( aro_liability_accr_dtl.month_yr, 'yyyymm' ) = " + string( month_number ) + " " + "~r~n" + &
						"	and 		aro_liability_accr_dtl.aro_id = " + string( i_aro_id ) + " " + "~r~n" + &
						" 	and 		aro_layer_stream.aro_id = aro_layer.aro_id " + "~r~n" + &
						" 	and 		aro_layer_stream.layer_id = aro_layer.layer_id " + "~r~n" + &
						"	and 		aro_liability_accr_dtl.aro_id = aro_layer_stream.aro_id " + "~r~n" + &
						"	and 		aro_liability_accr_dtl.layer_id = aro_layer_stream.layer_id " + "~r~n" + &
						"	and 		aro_liability_accr_dtl.layer_stream_date = aro_layer_stream.month_yr " + "~r~n" + &
						"	and 		aro_layer_stream.stream_id = 0 " + "~r~n" + &
						"	order by	aro_liability_accr_dtl.layer_id, " + "~r~n" + &
						"				aro_layer_stream.month_yr"
				
				s_rtn = f_create_dynamic_ds( ds_layers, "grid", sqls, sqlca, true )
				if s_rtn <> "OK" then
					messageBox( "Error", "Error occurred while creating the settlement datastore.~n~nError: "+s_rtn )
					destroy ds_layers
					rollback;
					return -1
				end if
				
		////
		////	Loop through the layers/months.
		////
				for i = 1 to ds_layers.rowCount()
				////
				////	Initialize the boolean.
				////
						b_adjusted = false
							
				////
				////	Get the data from the row.
				////
						layer_id = ds_layers.getItemNumber( i, "layer_id" )
						cur_liab = ds_layers.getItemDecimal( i, "cur_liab" )
						discount_rate = ds_layers.getItemDecimal( i, "date_disc_rate" )
						
				////
				////	Set the original liability.
				////
						orig_liab = cur_liab
				 
				////
				////	Convert rate from annual to monthly.
				////
						discount_rate = ( ( ( ( 1 + discount_rate ) ^ ( 1 / 12 ) ) - 1 ) * 12 )
						month_yr = ds_layers.getItemDateTime( i, "month_yr" )
						remaining_cash_flow = ds_layers.getItemDecimal( i, "remaining_cash_flow" )	
							
				////
				////	Get the month, date, and year and create a string of the date.
				////					
						select 	lpad( to_char( :month_yr, 'mm' ), 2, '0' ) mm, 
									lpad( to_char( :month_yr, 'dd' ), 2, '0' ) dd, 
									to_char( :month_yr, 'yyyy' ) yyyy
						into 		:mm, 
									:dd, 
									:yyyy
						from 		dual;
						
						s_dt_compare = mm + "/" + dd + "/" + yyyy
								
				////
				////	Get the periods.
				////						
						select 	months_between( :month_yr, :gl_posting_mo_yr ) 
						into 		:periods 
						from 		dual;
					
				////
				////	Calculate the layer date liability.
				////			
						layer_date_liab = round( ( remaining_cash_flow / ( ( 1 + discount_rate / 12 ) ^ periods ) ), 2 )
						
				////
				////	If the current liability is not equal to the layer date liability (with an allowance of 0.05), we need to create an adjustment.
				////
						if abs( cur_liab - layer_date_liab ) > .05 then 
						////
						////	Need to have a Settlement Adjustment for this Layer/Date
						//// 	Need to find the Present Value of the Remaining Cash Flow X such that
						//// 	X discounted back from Month Yr to GL Posting Mo Yr @ Layer Discount Rate = Cur Liab
						//// 	==> The Future Value of Cur Liab to Month Yr = X
								b_adjusted = true
								
						////
						////	Calculate the new remaining cash flow and settlement adjustment.
						////	The entire remaining liab is now accounted for, so make it zero.
						////
								new_remaining_cash_flow = round( ( cur_liab * ( ( 1 + discount_rate / 12 ) ^ periods ) ), 2 )
								settle_adj = new_remaining_cash_flow - remaining_cash_flow 
						
						////
						////	Find the matching row for this month_yr in the passed-in DW.
						////
								row = a_dw_streams.find( "month_yr = date( '" + s_dt_compare + "' )", 1, a_dw_streams.rowCount() )
								
						////
						////	Get the settlement adjustment from the row and add it to the settlement adjustment calculated above.
						////
								temp_settle_adj = a_dw_streams.getItemDecimal( row, "settle_adj" )
								if isNull( temp_settle_adj ) then temp_settle_adj = 0
								temp_settle_adj = temp_settle_adj + settle_adj		
								
						////
						////	If there is a value, insert into ARO Settlement Adj.
						////
								if settle_adj <> 0 then 
									insert into aro_settlement_adj 
									( aro_id, layer_id, layer_adj_id, stream_id, month_yr, remaining_cash_flow, settlement_adj )
									values 
									( :i_aro_id, :i_layer_id, :layer_id, 0, :month_yr, :remaining_cash_flow, :settle_adj );
									
									if sqlca.SQLCode < 0 then 
										messageBox( "Error", "Error occurred while inserting into ARO Settlement Adj.~n~nError: " + sqlca.SQLErrText )
										destroy ds_layers
										rollback;
										return -1
									end if
								end if
									
						////
						////	Set the temp value on the passed-in DW.
						////
								a_dw_streams.setItem( row, "settle_adj", temp_settle_adj )	
						else 
						////
						////	No settlement adjustment.
						////
								settle_adj = 0
						end if
				next /// loop through layers/months
		end if // detailed_rates

////
////	Function completed successfully.
////
		destroy ds_layers
		return 1
end function

public function longlong uf_add_settlements ();////
////	Function completed successfully.
////
		return 1
end function

public function integer uf_approve_layer (longlong a_aro_id, longlong a_layer_id);/************************************************************************************************************************************************************
 **
 **	uf_approve_layer()
 **	
 **	Marks the layer as approved.
 **
 **	This function is not currently being called.
 **	
 **	Parameters	:	longlong	:	(a_aro_id) The ARO to approve.
 **						longlong	:	(a_layer_id) The layer to approve.
 **	
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
 
////
////	Update ARO Layer.
////
		update 	aro_layer
		set 		approval_date = sysdate
		where 	aro_id = :a_aro_id
		and 		layer_id = :a_layer_id;

		if sqlca.SQLCode < 0 then
			messageBox( "Error", "Error occurred while updating Approval Date on ARO Layer.~n~nError: " + sqlca.SQLErrText )
			return -1
		end if

////
////	Update ARO Stream.
////
		update 	aro_stream
		set 		approval_date = sysdate
		where 	aro_id = :a_aro_id
		and 		layer_id = :a_layer_id;
		
		if sqlca.SQLCode < 0 then
			messageBox( "Error", "Error occurred while updating Approval Date on ARO Stream.~n~nError: " + sqlca.SQLErrText )
			return -1
		end if

////
////	Function completed successfully.
////
		return 1
end function

public function integer uf_book_layer (longlong a_aro_id, longlong a_layer_id, longlong a_trans_forcst);/************************************************************************************************************************************************************
 **
 **	uf_book_layer()
 **	
 **	Creates the transaction for the layer.
 **
 **	This function is currently called from:
 **		- w_aro_mass_ye - cb_new_layers.clicked()
 **	
 **	Parameters	:	longlong	:	( a_aro_id ) The ARO to book.
 **						longlong	:	( a_layer_id ) The layer to book.
 **						longlong	:	( a_trans_forcst ) Indicates whether this ARO is a transition or forecast.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/
 
longlong 		rtn, life, i, trans, mass_remaining_life, jj, vintages, vintage
decimal	discount_rate, asset_dollars, temp_dollars, depr_exp, accr_exp, check_dollars, asset_value
datetime	gl_posting_mo_yr, initial_date, end_life, today, discount_year

uo_ds_top 	ds_aro_mass_trans_calc

////
////	Create the datastores.
////		
		ds_aro_mass_trans_calc = create uo_ds_top
		ds_aro_mass_trans_calc.dataobject = "dw_aro_mass_trans_calc"
		ds_aro_mass_trans_calc.setTransObject( sqlca )

////
////	Set the instance variables.
////
		i_aro_id = a_aro_id
		i_layer_id = a_layer_id

////
////	Get some data from the ARO and layer.
////
		setNull( gl_posting_mo_yr )
		setNull( initial_date )
		setNull( discount_rate )
		setNull( asset_dollars )
		setNull( i_aro_type )
		
		select 	gl_posting_mo_yr,
					initial_date, 
					discount_rate, 
					layer_asset_value
		into 		:gl_posting_mo_yr, 
					:initial_date, 
					:discount_rate, 
					:asset_dollars
		from 		aro_layer
		where 	aro_id = :i_aro_id
		AND 		layer_id = :i_layer_id;
		
		select 	aro_type_id 
		into 		:i_aro_type
		from 		aro
		where 	aro_id = :i_aro_id;

		if asset_dollars = 0 then
			f_wo_status_box( "ARO", "Asset Value is Zero, No Pending Transaction Created" )
			destroy ds_aro_mass_trans_calc
			return 1
		end if
		
		if isNull( asset_dollars ) then
			f_wo_status_box( "ARO", "Asset Value is Null, No Pending Transaction Created" )
			destroy ds_aro_mass_trans_calc
			return 1
		end if

////
////	Set the microhelp.
////
		////w_top_frame.setMicroHelp( "Creating Pending Transactions..." )

////
////	Call the function to create the pending transaction.
////
		if a_trans_forcst = 0 then
		////
		////	Not a transition or forecast.  Write the transaction.
		////
				rtn = f_aro_pend_trans( i_aro_id, i_layer_id, false, i_aro_type, true )
				if rtn <> 1 then
					messageBox( "Error", "Error occurred while creating the pending transaction." )
					rollback;
					destroy ds_aro_mass_trans_calc
					////w_top_frame.setMicroHelp( "Ready" ) 
					return -1
				end if
				
		elseif a_trans_forcst = 2 then
		////
		////	Forecast.  Don't write the transaction.
		////
				rtn = f_aro_pend_trans( i_aro_id, i_layer_id, true, i_aro_type, false )
				if rtn <> 1 then
					messageBox( "Error", "Error occurred while creating the pending transaction." )
					rollback;
					destroy ds_aro_mass_trans_calc
					//w_top_frame.setMicroHelp( "Ready" ) 
					return -1
				end if
		elseif a_trans_forcst = 1 then
		////
		////	Transition.  Don't write the transaction.  Call the function to validate the information.
		////
				rtn = f_aro_pend_trans( i_aro_id, i_layer_id, true, i_aro_type, false )
				if rtn <> 1 then
					messageBox( "Error", "Neccessary information was not provided to create the transition entry." )
					destroy ds_aro_mass_trans_calc
					//w_top_frame.setMicroHelp( "Ready" ) 
					return -1
				end if
				
		////
		////	Get information needed for the aro_transition table.
		////
				//w_top_frame.setMicroHelp( "Calculating Transition Information..." ) 
				
				if year( date( gl_posting_mo_yr ) ) = 1900 or isNull( gl_posting_mo_yr ) then
					if isNull( gl_posting_mo_yr ) then
						messageBox( "Error", "Error obtaining dates for calculation.  Transition month is 00/0000." )
					else
						messageBox( "Error", "Error obtaining dates for calculation.  Transition month is " + string( gl_posting_mo_yr ) + "." )
					end if
					
					destroy ds_aro_mass_trans_calc
					//w_top_frame.setMicroHelp( "Ready" ) 
					return -1
				end if
				
				if year( date( initial_date ) ) = 1900 or isNull( initial_date ) then
					if isNull( initial_date ) then
						messageBox( "Error", "Error obtaining dates for calculation.  Initial month is 00/0000." )
					else
						messageBox( "Error", "Error obtaining dates for calculation.  Initial month is " + string( initial_date ) + "." )
					end if
				
					destroy ds_aro_mass_trans_calc
					//w_top_frame.setMicroHelp( "Ready" ) 
					return -1
				end if
			
		////
		////	Process based on the the ARO type.
		////	
				if i_aro_type <> i_type_mass then
				////
				////	COMPONENT/SITE
				////
					
				////
				////	Validate the discount rate and asset dollars.
				////
						if isNull( discount_rate ) or discount_rate = 0 then
							rtn = messageBox( "Question", "Discount Rate is 0%.  Would you like to continue?", Question!, YesNo! )
							if rtn <> 1 then 
								destroy ds_aro_mass_trans_calc
								//w_top_frame.setMicroHelp( "Ready" ) 
								return -1
							end if
							discount_rate = 0
						end if
				
						if isNull( asset_dollars ) or asset_dollars = 0 then
							rtn = messageBox( "Question", "Asset Dollars are 0.  Would you like to continue?", Question!, YesNo! )
							if rtn <> 1 then 
								destroy ds_aro_mass_trans_calc
								//w_top_frame.setMicroHelp( "Ready" ) 
								return -1
							end if
							asset_dollars = 0
						end if

				////
				////	Get the end life.
				////
						setNull( end_life )
						select 	end_life
						into 		:end_life
						from 		aro_temp_asset
						where 	aro_id = :i_aro_id;
						
						if ( isNull( end_life ) or year( date( end_life ) ) = 1900 ) then
							messageBox( "Error", "End Life not specified on the ARO Temp Asset Record." )
							destroy ds_aro_mass_trans_calc
							//w_top_frame.setMicroHelp( "Ready" ) 
							return -1
						end if 
						
				////
				////	Get the months between.
				////		
						select 	months_between( :end_life, :initial_date ) + 1 
						into 		:life 
						from 		dual;
	
						select 	months_between( :gl_posting_mo_yr, :initial_date ) 
						into 		:trans 
						from 		dual;
		
				////
				////	Compute the depreciation expense per month.
				////
						depr_exp = asset_dollars / life
		
				////
				////	Delete any existing rows in aro_transition.
				////
						delete from aro_transition
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
						
						if sqlca.SQLCode <> 0 then 
							messageBox( "Error", "Error occurred while deleting from ARO Transition.~n~nError: " + sqlca.SQLErrText )
							destroy ds_aro_mass_trans_calc
							//w_top_frame.setMicroHelp( "Ready" ) 
							return -1
						end if
	
				////
				////	Loop through the months and insert into aro_transition.
				////
						//w_top_frame.setMicroHelp( "Inserting Rows in ARO Transition..." ) 
			
						temp_dollars = asset_dollars
						
						for i = 0  to trans - 1
							//If we're past the life, then there is no depreciation.
							if i > life then depr_exp = 0
							
							//Calculate the accretion.  There is no accretion the first month, but the row is needed for reporting.
							if i = 0 then
								accr_exp = 0 
							else
								accr_exp = round( temp_dollars * ( discount_rate / 12 ), 2 )
							end if
							
							//Insert
							insert into aro_transition 
							( aro_id, layer_id, gl_month_yr, depr_expense, accretion_expense )
							values 
							( :i_aro_id, :i_layer_id, add_months( :initial_date, :i ), :depr_exp, :accr_exp );
						
							if sqlca.SQLCode <> 0 then 
								messageBox( "Error", "Error occurred while inserting into ARO Transition.~n~nError: " + sqlca.SQLErrText )
								destroy ds_aro_mass_trans_calc
								//w_top_frame.setMicroHelp( "Ready" ) 
								return -1
							end if
							
							temp_dollars = temp_dollars + accr_exp
						next
				else 
				////
				////	MASS
				////
				
				////
				////	Validate the discount rate and asset dollars.
				////
						if isNull( discount_rate ) or discount_rate = 0 then
							rtn = messageBox( "Question", "Discount Rate is 0%.  Would you like to continue?", Question!, YesNo! )
							if rtn <> 1 then 
								destroy ds_aro_mass_trans_calc
								//w_top_frame.setMicroHelp( "Ready" ) 
								return -1
							end if
							discount_rate = 0
						end if
				
				////
				////	Update the expected remaining life.
				////
						setNull( mass_remaining_life )
						
						select 	round( sum( ( months_between( to_date( activity_year||'12', 'yyyymm' ), :gl_posting_mo_yr ) ) * retire_quantity ) / sum( retire_quantity ), 0 )
						into 		:mass_remaining_life
						from 		aro_mass_calc
						where 	stream_id = 1
						and		aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
	
						update 	aro_layer
						set 		expected_remaining_life = :mass_remaining_life
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
						
						if sqlca.SQLCode <> 0 then 
							messageBox( "Error", "Error occurred while updating ARO Layer with the Expected Remaining Life.~n~nError: " + sqlca.SQLErrText )
							destroy ds_aro_mass_trans_calc
							//w_top_frame.setMicroHelp( "Ready" ) 
							return -1
						end if
		
				////
				////	Delete existing transition rows.
				////
						delete from aro_transition_mass
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
						
						if sqlca.SQLCode <> 0 then 
							messageBox( "Error", "Error occurred while deleting from ARO Transition Mass.~n~nError: " + sqlca.SQLErrText )
							destroy ds_aro_mass_trans_calc
							//w_top_frame.setMicroHelp( "Ready" ) 
							return -1
						end if
	
						delete from aro_transition
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
								
						if sqlca.SQLCode <> 0 then 
							messageBox( "Error", "Error occurred while deleting from ARO Transition.~n~nError: " + sqlca.SQLErrText )
							destroy ds_aro_mass_trans_calc
							//w_top_frame.setMicroHelp( "Ready" ) 
							return -1
						end if
		
				////
				////	Retrieve the mass trans calc DW.
				////	Loop through the rows and insert into aro_transition.
				////
						vintages = ds_aro_mass_trans_calc.retrieve( i_aro_id, i_layer_id )
						
						for jj = 1 to vintages
							//Initialize.
							life = 0
							depr_exp =0
							temp_dollars = 0
							
							//Get the data from the row.
							vintage = ds_aro_mass_trans_calc.getItemNumber( jj, "vintage" )
							discount_year = ds_aro_mass_trans_calc.getItemDateTime( jj, "discount_year" )
							asset_value = ds_aro_mass_trans_calc.getItemNumber( jj, "discounted_cash_flow" )
							
							//Compute the months between.
							select 	months_between( :gl_posting_mo_yr, :discount_year ) 
							into 		:trans 
							from 		dual;
							
							//Compute the life.
							life = trans + mass_remaining_life
							
							//Calculate monthly depreciation expense.
							depr_exp = asset_value / life
							
							//Loop through the months and insert into aro_transition_mass.
							//w_top_frame.setMicroHelp( "Inserting Rows in ARO Transition...Vintage " + string( vintage ) ) 
							
							temp_dollars = asset_value

							for i = 0  to trans - 1
								//If we're past the life, then there is no depreciation.
								if i > life then depr_exp = 0
								
								//Calculate the accretion.  There is no accretion the first month, but the row is needed for reporting.
								if i = 0 then
									accr_exp = 0 
								else
									accr_exp = round( temp_dollars * ( discount_rate / 12 ), 2 )
								end if
		
								//Insert
								insert into aro_transition_mass 
								( aro_id, layer_id, vintage, gl_month_yr, depr_expense, accretion_expense )
								values 
								( :i_aro_id, :i_layer_id, :vintage, add_months( :discount_year, :i ), :depr_exp, :accr_exp );
							
								if sqlca.SQLCode <> 0 then 
									messageBox( "Error", "Error occurred while inserting into ARO Transition Mass.~n~nError: " + sqlca.SQLErrText )
									destroy ds_aro_mass_trans_calc
									//w_top_frame.setMicroHelp( "Ready" ) 
									return -1
								end if
				
								temp_dollars = temp_dollars + accr_exp
							next //loop through life months
						next //loop through ds_aro_mass_trans_calc
						
				////
				////	Insert into aro_transition.
				////
						insert into aro_transition 
						( aro_id, layer_id, gl_month_yr, depr_expense, accretion_expense )
						 	select 	aro_id, 
										layer_id,
										gl_month_yr,
										sum( depr_expense ), 
										sum( accretion_expense )
							from 		aro_transition_mass
							where 	aro_id = :i_aro_id
							and 		layer_id = :i_layer_id
							group by	aro_id, 
										layer_id,
										gl_month_yr;
										
						if sqlca.SQLCode <> 0 then 
							messageBox( "Error", "Error occurred while inserting into ARO Transition.~n~nError: " + sqlca.SQLErrText )
							destroy ds_aro_mass_trans_calc
							//w_top_frame.setMicroHelp( "Ready" ) 
							return -1
						end if
				end if //ARO type
		end if //transition/forecast

////
////	Update the status on the layer.
////
		if a_trans_forcst = 1 then
			update 	aro_layer
			set 		aro_status_id = :i_status_transition
			where 	aro_id = :i_aro_id
			and 		layer_Id = :i_layer_id;
			
			if sqlca.SQLCode <> 0 then 
				messageBox( "Error", "Error occurred while updating the status to Transition on ARO Layer.~n~nError: " + sqlca.SQLErrText )
				destroy ds_aro_mass_trans_calc
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		elseif a_trans_forcst = 2 then
			update 	aro_layer
			set 		aro_status_id = :i_status_forecast
			where 	aro_id = :i_aro_id
			and 		layer_Id = :i_layer_id;
			
			if sqlca.SQLCode <> 0 then 
				messageBox( "Error", "Error occurred while updating the status to Forecast on ARO Layer.~n~nError: " + sqlca.SQLErrText )
				destroy ds_aro_mass_trans_calc
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		else
			update 	aro_layer
			set 		aro_status_id = :i_status_pending
			where 	aro_id = :i_aro_id
			and 		layer_Id = :i_layer_id;
			
			if sqlca.SQLCode <> 0 then 
				messageBox( "Error", "Error occurred while updating the status to Pending on ARO Layer.~n~nError: " + sqlca.SQLErrText )
				destroy ds_aro_mass_trans_calc
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		end if


////
////	Check the dollars to see if they are negative.
////
		select 	cpr_ledger.accum_cost
		into 		:check_dollars
		from 		aro, 
					cpr_ledger
		where 	aro.aro_id = :i_aro_id
		and 		aro.asset_id = cpr_ledger.asset_id;

		if isNull( check_dollars ) then check_dollars = 0

		if check_dollars + asset_dollars < 0 then
//			messageBox( "Information", "This ARO asset layer will create a negative value for the ARC asset.  After posting, an asset adjustment may be desired." )
		end if
		
////
////	Update the status and status date.
////
		today = datetime( today(  ),now(  ) )
		
		if i_layer_id < 2 and a_trans_forcst = 0 then
			update 	aro
			set 		aro_status_id = :i_status_pending, 
						status_date = :today	
			where 	aro_id = :i_aro_id;
			
			if sqlca.SQLCode <> 0 then 
				messageBox( "Error", "Error occurred while updating ARO with the status and status date (pending).~n~nError: " + sqlca.SQLErrText )
				destroy ds_aro_mass_trans_calc
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		elseif a_trans_forcst = 0 then
			update 	aro
			set 		aro_status_id = :i_status_active_pending, 
						status_date = :today	
			where 	aro_id = :i_aro_id
			and 		aro_status_id = :i_status_active;
			
			if sqlca.SQLCode <> 0 then 
				messageBox( "Error", "Error occurred while updating ARO with the status and status date (active with pending).~n~nError: " + sqlca.SQLErrText )
				destroy ds_aro_mass_trans_calc
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		elseif a_trans_forcst = 1 then
			update 	aro
			set 		aro_status_id = :i_status_transition, 
						status_date = :today	
			where 	aro_id = :i_aro_id;
			
			if sqlca.SQLCode <> 0 then 
				messageBox( "Error", "Error occurred while updating ARO with the status and status date (transition).~n~nError: " + sqlca.SQLErrText )
				destroy ds_aro_mass_trans_calc
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		elseif a_trans_forcst = 2 then
			update 	aro
			set 		aro_status_id = :i_status_forecast, 
						status_date = :today	
			where 	aro_id = :i_aro_id;
			
			if sqlca.SQLCode <> 0 then 
				messageBox( "Error", "Error occurred while updating ARO with the status and status date (forecast).~n~nError: " + sqlca.SQLErrText )
				destroy ds_aro_mass_trans_calc
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		end if

////
////	Set the microhelp.
////
		//w_top_frame.setMicroHelp( "Ready" ) 

////
////	Function completed successfully.
////
		destroy ds_aro_mass_trans_calc
		return 1
end function

public function integer uf_calc_layer_balance (longlong a_aro_id, longlong a_layer_id, longlong a_trans_forcst);/************************************************************************************************************************************************************
 **
 **	uf_calc_layer_balance()
 **	
 **	Calculates the layer balance.
 **
 **	This function is currently called from:
 **		- w_aro_estimate - cb_calc_layer.clicked()
 **		- w_aro_mass_ye - cb_new_layers.clicked()
 **		- w_aro_pp_approval - cb_calc.clicked()
 **	
 **	Parameters	:	longlong	:	( a_aro_id ) The ARO to calculate.
 **						longlong	:	( a_layer_id ) The layer to calculate.
 **						longlong	:	( a_trans_forcst ) Indicates whether this ARO is a transition or forecast.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/


longlong 			i, periods, rtn, rows, j, aro_status_id, applied_layer_id, applied_stream_id
decimal{2}	cash_flow, init_bal, temp_cash_flow, applied_cash_flow, stream_flow, stream_discounted, neg_discounted, prior_cash_flow, cur_cash_flow, cur_cash_flow_temp, new_liability, old_liability, stream1, stream2, stream3, settle_adj
decimal{8} 	discount_rate, neg_discount_rate, ann_rate, date_disc_rate, this_rate, detailed_wam_rate, prob1, prob2, prob3
datetime 	gl_posting_mo_yr, month_yr, initial_date, start_date
boolean 		zero_rate_msg

uo_ds_top 	ds_aro_get_neg_discount_rates

////
////	Initialize.
////
		//w_top_frame.setMicroHelp( "Calculating Layer Balance...." )
		zero_rate_msg = false

////
////	Create the datastores.
////	The 'i_ds_aro_streams' datastore will be used by other functions that are called from this function,
////	so it is stored as an instance variable.
////
		i_ds_aro_streams = create uo_ds_top
		i_ds_aro_streams.dataobject = "dw_aro_streams_settle_adj"
		i_ds_aro_streams.setTransObject( sqlca )
		i_ds_aro_streams.retrieve( a_aro_id, a_layer_id )

		ds_aro_get_neg_discount_rates = create uo_ds_top
		ds_aro_get_neg_discount_rates.dataobject = "dw_aro_get_neg_discount_rates_str"
		ds_aro_get_neg_discount_rates.setTransObject( sqlca )

////
////	Set the instance variables.
////
		i_aro_id = a_aro_id
		i_layer_id = a_layer_id

////
////	Get the data from the ARO and layer.
////
		setNull( gl_posting_mo_yr )
		setNull( initial_date )
		setNull( discount_rate )
		setNull( ann_rate )
		setNull( aro_status_id )
		
		select		gl_posting_mo_yr, 
					initial_date,
					discount_rate,
					annual_eff_rate
		into 		:gl_posting_mo_yr, 
					:initial_date,
					:discount_rate,
					:ann_rate
		from 		aro_layer 
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		select 	nvl( aro_status_id, :i_status_setup )
		into 		:aro_status_id
		from 		aro_layer
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;

		if isNull( aro_status_id ) then aro_status_id = i_status_setup

////
////	Delete any existing records.
////
		delete from aro_layer_discounting
		where 	aro_id = :i_aro_id
		and	 	layer_id = :i_layer_id;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while deleting from ARO Layer Discounting.~n~nError: " + sqlca.SQLErrText )
			destroy i_ds_aro_streams
			destroy ds_aro_get_neg_discount_rates
			//w_top_frame.setMicroHelp( "Ready" )
			return -1
		end if

		delete from aro_layer_stream_downward_adj
		where aro_id = :i_aro_id
		and layer_id = :i_layer_id;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while deleting from ARO Layer Stream Downward Adj.~n~nError: " + sqlca.SQLErrText )
			destroy i_ds_aro_streams
			destroy ds_aro_get_neg_discount_rates
			//w_top_frame.setMicroHelp( "Ready" )
			return -1
		end if

////
////	Validate the dates and set the start date, based on the transition/forecast type.
////
		if a_trans_forcst = 1 then
		////
		////	Transition.
		////
				if year( date( gl_posting_mo_yr ) ) = 1900 or isNull( gl_posting_mo_yr ) then
					if isNull( gl_posting_mo_yr ) then
						messageBox( "Error", "Error obtaining dates for calculation.  Transition month is 00/0000." )
					else
						messageBox( "Error", "Error obtaining dates for calculation.  Transition month is " + string( gl_posting_mo_yr ) + "." )
					end if
					
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if
				
				if year( date( initial_date ) ) = 1900 or isNull( initial_date ) then
					if isNull( initial_date ) then
						rtn = messageBox( "Error", "Error obtaining dates for calculation.  Initial month is 00/0000." )
					else
						rtn = messageBox( "Error", "Error obtaining dates for calculation.  Initial month is " + string( initial_date ) + "." )
					end if
				
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if
				
				start_date = initial_date
		elseif a_trans_forcst = 2 then
		////
		////	Forecast.
		////
				if year( date( gl_posting_mo_yr ) ) = 1900 or isNull( gl_posting_mo_yr ) then
					if isNull( gl_posting_mo_yr ) then
						messageBox( "Error", "Error obtaining dates for calculation.  Forecast month is 00/0000." )
					else
						messageBox( "Error", "Error obtaining dates for calculation.  Forecast month is " + string( gl_posting_mo_yr ) + "." )
					end if
					
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if
			
				start_date = gl_posting_mo_yr
		else
		////
		////	Regular.
		////
				if year( date( gl_posting_mo_yr ) ) = 1900 or isNull( gl_posting_mo_yr ) then
					if isNull( gl_posting_mo_yr ) then
						messageBox( "Error", "Error obtaining dates for calculation.  GL Posting Mo Yr has not been set." )
					else
						messageBox( "Error", "Error obtaining dates for calculation.  GL Posting Mo Yr is " + string( gl_posting_mo_yr ) + "." )
					end if
					
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if
				
				start_date = gl_posting_mo_yr
		end if

////
////	Set the accounting month.  This will be used by uf_set_old_liability(), which we call later in this function.
////
		i_accounting_month = gl_posting_mo_yr
	
////
////	Set Weighted Average and other calculation variables.
////	Determine if the ARO is using the Weighted Average Rate Method.
////	If the status is not "Setup", get the rate type from the layer.
////	Then get it from the ARO if it's still null.
////
		setNull( i_weighted_avg )
		
		if aro_status_id <> i_status_setup then
			select 	decode( aro.aro_type_id, :i_type_mass, 0, nvl( aro_layer.weighted_average_rate, 0 ) ) 
			into 		:i_weighted_avg
			from 		aro_layer, 
						aro
			where 	aro_layer.aro_id = :i_aro_id
			and 		aro_layer.layer_id = :i_layer_id
			and 		aro_layer.aro_id = aro.aro_id;
		end if

		if isNull( i_weighted_avg ) then
			select 	decode( aro_type_id, :i_type_mass, 0, nvl( weighted_average_rate, 0 ) ) 
			into 		:i_weighted_avg
			from 		aro
			where 	aro_id = :i_aro_id;
		end if

		if isNull( i_weighted_avg ) then i_weighted_avg = 0

////
////	Determine additional variables.
////	If "detailed rates" is null, default to "No" (0).
////	If "skip layer adj" is null, default to "No" (0).  This means new layers use rates from other layers.
////
		setNull( i_detailed_rates )
		setNull( i_skip_layer_adj )
		setNull( i_aro_type )
		
		select 	detailed_rates, 
					skip_layer_adj, 
					aro_type_id
		into 		:i_detailed_rates, 
					:i_skip_layer_adj, 
					:i_aro_type
		from 		aro
		where 	aro_id = :i_aro_id;

		if isNull( i_detailed_rates ) then i_detailed_rates = 0
		if isNull( i_skip_layer_adj ) then i_skip_layer_adj = 0 

////
////	Now process layer based on the options:
////	Weighted Average, Detail Rates, and Skip Layer Adjustments
////
		if i_weighted_avg = i_rate_type_weighted or i_weighted_avg = i_rate_type_time then 
		////
		////	WEIGHTED OR TIME AVERAGE
		////
		
		////
		////	Get the prior cash flow.
		////
				setNull( prior_cash_flow )
				
				select 	sum( aro_layer_stream.remaining_cash_flow )
				into 		:prior_cash_flow
				from 		aro_layer_stream, 
							aro_layer
				where 	aro_layer_stream.aro_id = :i_aro_id
				and 		aro_layer_stream.layer_id < :i_layer_id
				and 		aro_layer.aro_status_id not in ( :i_status_setup, :i_status_inactive )
				and 		aro_layer_stream.aro_id = aro_layer.aro_id
				and 		aro_layer_stream.layer_id = aro_layer.layer_id;
			
				if isNull( prior_cash_flow ) then prior_cash_flow = 0
				
		////
		////	Loop through the streams for this ARO and layer and calculate the current cash flow.
		////
				for i = 1 to i_ds_aro_streams.rowCount()
					stream1 = i_ds_aro_streams.getItemDecimal( i, "stream1" )
					stream2 = i_ds_aro_streams.getItemDecimal( i, "stream2" )
					stream3 = i_ds_aro_streams.getItemDecimal( i, "stream3" )
					prob1 = i_ds_aro_streams.getItemDecimal( i, "prob1" )
					prob2 = i_ds_aro_streams.getItemDecimal( i, "prob2" )
					prob3 = i_ds_aro_streams.getItemDecimal( i, "prob3" )
					settle_adj = i_ds_aro_streams.getItemDecimal( i, "settle_adj" )
					
					if isNull( stream1 ) then stream1 = 0
					if isNull( stream2 ) then stream2 = 0
					if isNull( stream3 ) then stream3 = 0
					if isNull( prob1 ) then prob1 = 0
					if isNull( prob2 ) then prob2 = 0
					if isNull( prob3 ) then prob3 = 0
					if isNull( settle_adj ) then settle_adj = 0
					
					cur_cash_flow_temp = stream1 * prob1 + stream2 * prob2 + stream3 * prob3 + settle_adj
					
					if isNull( cur_cash_flow_temp ) then cur_cash_flow_temp = 0
					
					cur_cash_flow += cur_cash_flow_temp
				next 
			 
		////
		////	Calcuate the rates based on if the prior cash flow is greater than the current cash flow.
		////	Call the function to compute the annual rate.
		////
				if prior_cash_flow > cur_cash_flow then 
					ann_rate = uf_calc_weighted_avg_rate( "down" )
					discount_rate = ( ( ( ( 1 + ann_rate ) ^ ( 1 / 12 ) ) - 1 ) * 12 )
				else
					ann_rate = uf_calc_weighted_avg_rate( "up" )
					discount_rate = ( ( ( ( 1 + ann_rate ) ^ ( 1 / 12 ) ) - 1 ) * 12 )
				end if
			
		////
		////	Update the layer with the rates.
		////
				update 	aro_layer
				set 		annual_eff_rate = :ann_rate, 
							discount_rate = :discount_rate
				where 	aro_id = :i_aro_id
				and 		layer_id = :i_layer_id;
				
				if sqlca.SQLCode < 0 then 
					messageBox( "Error", "Error occurred while updating Annual Eff Rate and Discount Rate on ARO Layer.~n~nError: " + sqlca.SQLErrText )
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					rollback;
					return -1
				end if
	
		////
		////	Call the function to get the new liability.
		////
				new_liability = uf_set_new_liability( discount_rate, start_date )
				if isNull( new_liability ) then new_liability = 0
				
				if new_liability = -1 then 
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if
	
		////
		////	Call the function to get the old liability.
		////
				old_liability = uf_set_old_liability()
				if isNull( old_liability ) then old_liability = 0
				
				if old_liability = -1 then 
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if
	
		////
		////	Update the layer with the liabilities.
		////
				update 	aro_layer
				set 		new_liability = :new_liability, 
							old_liability = :old_liability
				where 	aro_id = :i_aro_id
				and 		layer_id = :i_layer_id;
				
				if sqlca.SQLCode < 0 then 
					messageBox( "Error", "Error occurred while updating New Liability and Old Liability on ARO Layer.~n~nError: " + sqlca.SQLErrText )
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					rollback;
					return -1
				end if
	
		////
		//// 	Change in Asset Amount is New Liability - Current Liability 
		//// 	Offset to Liability Change = Asset
		////
				init_bal = new_liability - old_liability
				
		elseif i_weighted_avg = 2 then
		////
		////	NEW RATE
		////
		
		////
		////	Validate the discount rate.
		////
				if isNull( discount_rate ) or discount_rate = 0 then
					rtn = messageBox( "Question", "Discount Rate is 0%.  Would you like to continue?", Question!, YesNo! )
					if rtn <> 1 then 
						destroy i_ds_aro_streams
						destroy ds_aro_get_neg_discount_rates
						//w_top_frame.setMicroHelp( "Ready" )
						return -1
					end if
					discount_rate = 0
				end if

		////
		////	Call the function to get the new liability.
		////
				new_liability = uf_set_new_liability( discount_rate, start_date )
				if isNull( new_liability ) then new_liability = 0
				
				if new_liability = -1 then 
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if

		////
		////	Call the function to get the old liability.
		////
				old_liability = uf_set_old_liability()
				if isNull( old_liability ) then old_liability = 0
				
				if old_liability = -1 then 
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					return -1
				end if

		////
		////	Update the layer with the liabilities.
		////
				update 	aro_layer
				set 		new_liability = :new_liability, 
							old_liability = :old_liability
				where 	aro_id = :i_aro_id
				and 		layer_id = :i_layer_id;
				
				if sqlca.SQLCode < 0 then 
					messageBox( "Error", "Error occurred while updating New Liability and Old Liability on ARO Layer.~n~nError: " + sqlca.SQLErrText )
					destroy i_ds_aro_streams
					destroy ds_aro_get_neg_discount_rates
					//w_top_frame.setMicroHelp( "Ready" )
					rollback;
					return -1
				end if

		////
		//// 	Change in Asset Amount is New Liability - Current Liability 
		//// 	Offset to Liability Change = Asset
		////
				init_bal = new_liability - old_liability

		elseif i_weighted_avg = 0 then 
		////
		////	STANDARD
		////
		
		////
		////	Validate the discount rate if we're not using detailed rates.
		////
				if i_detailed_rates = 0 and ( isNull( discount_rate ) or discount_rate = 0 ) then
					rtn = messageBox( "Question", "Discount Rate is 0%.  Would you like to continue?", Question!, YesNo! )
					if rtn <> 1 then 
						destroy i_ds_aro_streams
						destroy ds_aro_get_neg_discount_rates
						//w_top_frame.setMicroHelp( "Ready" )
						return -1
					end if
					discount_rate = 0
				end if

		////
		////	Initialize the intial balance.
		////	
				init_bal = 0

		////
		////	Process based on the type and transition/forecast.
		////
				if i_aro_type = i_type_mass and a_trans_forcst = 1 then
				////
				////	MASS - TRANSITION
				////
		
				////
				////	Detailed rates can't be used on mass AROs.
				////
						if i_detailed_rates = 1 then
							messageBox( "Error", "Detail Rates are not supported for Mass Transition Processing." )
							destroy i_ds_aro_streams
							destroy ds_aro_get_neg_discount_rates
							//w_top_frame.setMicroHelp( "Ready" )
							return -1 
						end if
				
				////
				////	Update the discount year.
				////
						update 	aro_mass_calc
						set 		discount_year = greatest( :start_date, to_date( vintage||'12', 'yyyymm' ) )
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
					
						if sqlca.SQLCode < 0 then 
							messageBox( "Error", "Error occurred while updating Discount Year on ARO Mass Calc.~n~nError: " + sqlca.SQLErrText )
							destroy i_ds_aro_streams
							destroy ds_aro_get_neg_discount_rates
							//w_top_frame.setMicroHelp( "Ready" )
							return -1
						end if
					
				////
				////	Update the discounted cash flow.
				////
						update 	aro_mass_calc
						set 		discounted_cash_flow = 
									( cash_flow * 
										( 	select 	probability 
											from 		aro_stream 
											where 	aro_id = :i_aro_id
											and 		layer_id = :i_layer_id
											and 		aro_stream.stream_id = aro_mass_calc.stream_id 
										)
										/ ( power( ( 1 + :discount_rate / 12 ), round( months_between( to_date( activity_year||'12', 'yyyymm' ), discount_year ), 0 ) ) )
						 			)
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
					
						if sqlca.SQLCode < 0 then 
							messageBox( "Error", "Error occurred while updating Discounted Cash Flow on ARO Mass Calc.~n~nError: " + sqlca.SQLErrText )
							destroy i_ds_aro_streams
							destroy ds_aro_get_neg_discount_rates
							//w_top_frame.setMicroHelp( "Ready" )
							return -1
						end if
		
				////
				////	Retrieve the newly calculated discounted cash flow.
				////
						select 	sum( discounted_cash_flow ) 
						into 		:init_bal
						from 		aro_mass_calc
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
		
				////
				////	Insert into the discounting table.
				////
						insert into aro_layer_discounting
						( aro_id, layer_id, stream_id, month_yr, start_month, periods, cash_flow, discount_rate, discounted_flow, init_bal )
						(	select 	aro_id, 
										layer_id, 
										0, 
										to_date( activity_year||'12', 'yyyymm' ),
										null,
										null,
										sum( cash_flow * to_number( 
												(	select 	probability 
													from 		aro_stream 
													where 	aro_id = :i_aro_id
													and 		layer_id = :i_layer_id
													and 		aro_stream.stream_id = aro_mass_calc.stream_id 
												) ) ), 
										:discount_rate,
										sum( discounted_cash_flow ), 
										0
							from 		aro_mass_calc
							where 	aro_id = :i_aro_id
							and 		layer_id = :i_layer_id
							group by	aro_id, 
										layer_id, 
										0, 
										to_date( activity_year||'12', 'yyyymm' )
						);
							
						if sqlca.SQLCode < 0 then 
							messageBox( "Error", "Error occurred while inserting into ARO Layer Discounting.~n~nError: " + sqlca.SQLErrText )
							destroy i_ds_aro_streams
							destroy ds_aro_get_neg_discount_rates
							//w_top_frame.setMicroHelp( "Ready" )
							return -1
						end if
				
				////
				////	Call the function to get the old liability.
				////
						old_liability = uf_set_old_liability()
						if isNull( old_liability ) then old_liability = 0
						
						if old_liability = -1 then 
							destroy i_ds_aro_streams
							destroy ds_aro_get_neg_discount_rates
							//w_top_frame.setMicroHelp( "Ready" )
							return -1
						end if
		
				////
				////	Calculate the new liability.
				////
						new_liability = old_liability + init_bal
						
				////
				////	Update the layer with the liabilities.
				////
						update 	aro_layer
						set 		new_liability = :new_liability, 
									old_liability = :old_liability
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id;
						
						if sqlca.SQLCode < 0 then 
							messageBox( "Error", "Error occurred while updating New Liability and Old Liability on ARO Layer.~n~nError: " + sqlca.SQLErrText )
							destroy i_ds_aro_streams
							destroy ds_aro_get_neg_discount_rates
							//w_top_frame.setMicroHelp( "Ready" )
							rollback;
							return -1
						end if			
				else
				////
				////	NOT MASS/TRANSITION
				////	
		
				////
				////	Loop through the streams for this ARO and layer.
				////
						for i = 1 to i_ds_aro_streams.RowCount()
						////
						////	Initialize.
						////
								//w_top_frame.setMicroHelp( "Calculating for Month ( " + string( i ) + "/" + string( i_ds_aro_streams.RowCount() ) + " )..." )
								stream_discounted = 0
							
						////
						////	Get the data from the row.  If there is no month_yr, skip this row.
						////
								cash_flow = i_ds_aro_streams.getItemNumber( i, "net_change" )
								month_yr = i_ds_aro_streams.getItemDateTime( i, "month_yr" )
								date_disc_rate = i_ds_aro_streams.getItemdecimal( i, "date_disc_rate" )
								
								if isnull( cash_flow ) then cash_flow = 0
								if isnull( month_yr ) then continue
							
						////
						////	Compute the periods.
						////
								select 	months_between( :month_yr, :start_date ) 
								into 		:periods 
								from 		dual;
					
						////
						////	Get the rate to use.
						////	If this ARO is using detailed rates, convert the date disc rate.
						////	Otherwise, use the discount rate.
						////
								if i_detailed_rates = 1 then
									//Using detailed rates.
									if isNull( date_disc_rate ) or date_disc_rate = 0 then
										//Use the annual rate.
										if ann_rate <> -999 then
											update 	aro_layer_stream
											set 		date_disc_rate = :ann_rate
											where 	aro_id = :i_aro_id
											and 		layer_id = :i_layer_id
											and 		month_yr = :month_yr
											and 		stream_id = 0;
											
											if sqlca.SQLCode < 0 then 
												messageBox( "Error", "Error occurred while updating Date Discount Rate on ARO Layer Stream for " + string( month_yr ) + ".~n~nError: " + sqlca.SQLErrText )
												destroy i_ds_aro_streams
												destroy ds_aro_get_neg_discount_rates
												//w_top_frame.setMicroHelp( "Ready" )
												return -1
											end if
										
											//Applied rate for this row is the discount rate on the layer.
											this_rate = discount_rate
										else
											messageBox( "Error", "No detail rate found for " + string( month_yr ) + "." )
											destroy i_ds_aro_streams
											destroy ds_aro_get_neg_discount_rates
											//w_top_frame.setMicroHelp( "Ready" )
											return -1
										end if
									else 
										//Convert the rate.
										this_rate = ( ( ( ( 1 + date_disc_rate ) ^ ( 1 / 12 ) ) - 1 ) * 12 )
									end if
				
								else
									//Not using detailed rates.
									this_rate = discount_rate
				
									update 	aro_layer_stream
									set 		date_disc_rate = :ann_rate
									where 	aro_id = :i_aro_id
									and 		layer_id = :i_layer_id
									and 		month_yr = :month_yr
									and 		stream_id = 0;
									
									if sqlca.SQLCode < 0 then 
										messageBox( "Error", "Error occurred while updating Date Discount Rate on ARO Layer Stream for " + string( month_yr ) + ".~n~nError: " + sqlca.SQLErrText )
										destroy i_ds_aro_streams
										destroy ds_aro_get_neg_discount_rates
										//w_top_frame.setMicroHelp( "Ready" )
										return -1
									end if
								end if //detailed rates
								
						////
						////	If we're using detailed rates, check for a zero rate.
						////
								if not zero_rate_msg and i_detailed_rates = 1 and ( isNull( this_rate ) or this_rate = 0 ) and cash_flow > 0 then
									rtn = messageBox( "Question", "Discount Rate is 0% for " + string( month_yr ) + ".  Would you like to continue?", Question!, YesNo! )
									if rtn <> 1 then 
										destroy i_ds_aro_streams
										destroy ds_aro_get_neg_discount_rates
										//w_top_frame.setMicroHelp( "Ready" )
										return -1
									else
										zero_rate_msg = true
									end if
									if isNull( this_rate ) then this_rate = 0
								end if
								
								if isNull( this_rate ) then this_rate = 0
	
						////
						////	If there is a positive cash flow, insert into ARO Layer Discounting.
						////	Otherwise, process the negative balance according to the variables we retrieved.
						////
								if cash_flow >= 0 then
								////
								////	Positive cash flow
								////
								
								////
								////	Compute the balances.
								////
										init_bal = init_bal + round( ( cash_flow / ( ( 1 + this_rate / 12 ) ^ periods ) ), 2 )
										stream_discounted += round( ( cash_flow / ( ( 1 + this_rate / 12 ) ^ periods ) ), 2 )
										
								////
								////	Insert the discount record.
								////
										insert into aro_layer_discounting
										( aro_id, layer_id, stream_id, month_yr, start_month, periods, cash_flow, discount_rate, discounted_flow, init_bal )
										values 
										( :i_aro_id, :i_layer_id, 0, :month_yr, :start_date, :periods, :cash_flow, :this_rate, :stream_discounted, :init_bal );
											
										if sqlca.SQLCode < 0 then 
											messageBox( "Error", "Error occurred while inserting into ARO Layer Discounting for " + string( month_yr ) + ".~n~nError: " + sqlca.SQLErrText )
											destroy i_ds_aro_streams
											destroy ds_aro_get_neg_discount_rates
											//w_top_frame.setMicroHelp( "Ready" )
											return -1
										end if
								else
								////
								////	Negative cash flow
								////
								
								////
								////	If using detailed rates, we need to get rates from detailed previous layers.
								////	Unless the option to skip layer adjustments is selected, in which case a weighted average of detailed rates is applied.
								////	Get the appropriate WAM rate and update the layer stream.
								////
										if i_detailed_rates = 1 and i_skip_layer_adj = 1 then
											//Get the new weighted average detailed rate.
											//If the ARO hasn't been approved, call the function to calculate the weighted average detail rate.
											//If the ARO has been approved, get the rate from the database.
											if i_pp_approve <> 1 then
												detailed_wam_rate = uf_wam_detail_rate( month_yr )
												if detailed_wam_rate = 0 then
													detailed_wam_rate = ann_rate
												end if
											else
												 select 	date_disc_rate
												 into 		:detailed_wam_rate
												 from 		aro_layer_stream
												 where 	aro_id = :i_aro_id
												 and 		layer_id = :i_layer_id
												 and 		month_yr = :month_yr
												 and 		stream_id = 0;
											end if 
					
											//Update the layer stream with the WAM rate.
											update 	aro_layer_stream
											set 		date_disc_rate = :detailed_wam_rate
											where 	aro_id = :i_aro_id
											and 		layer_id = :i_layer_id
											and 		month_yr = :month_yr;
											
											if sqlca.SQLCode < 0 then 
												messageBox( "Error", "Error occurred while updating Date Discount Rate on ARO Layer Stream for " + string( month_yr ) + ".~n~nError: " + sqlca.SQLErrText )
												destroy i_ds_aro_streams
												destroy ds_aro_get_neg_discount_rates
												//w_top_frame.setMicroHelp( "Ready" )
												return -1
											end if
			
											//Calculate the rate to use.
											this_rate = ( ( ( ( 1 + detailed_wam_rate ) ^ ( 1 / 12 ) ) - 1 ) * 12 )
					
											//Compute the balances.
											init_bal = init_bal + round( ( cash_flow / ( ( 1 + this_rate / 12 ) ^ periods ) ), 2 )
											stream_discounted += round( ( cash_flow / ( ( 1 + this_rate / 12 ) ^ periods ) ), 2 )
											
											//Insert the discount record.
											insert into aro_layer_discounting
											( aro_id, layer_id, stream_id, month_yr, start_month, periods, cash_flow, discount_rate, discounted_flow, init_bal )
											values 
											( :i_aro_id, :i_layer_id, 0, :month_yr, :start_date, :periods, :cash_flow, :this_rate, :stream_discounted, :init_bal );
												
											if sqlca.SQLCode < 0 then 
												messageBox( "Error", "Error occurred while inserting into ARO Layer Discounting for " + string( month_yr ) + ".~n~nError: " + sqlca.SQLErrText )
												destroy i_ds_aro_streams
												destroy ds_aro_get_neg_discount_rates
												//w_top_frame.setMicroHelp( "Ready" )
												rollback;
												return -1
											end if
										else 
											//Not detailed rates
											temp_cash_flow = cash_flow 
											j = 1
											
											//Loop through and absorb the negative cash flow.
											rows = ds_aro_get_neg_discount_rates.retrieve( i_aro_id, month_yr, i_layer_id )
											
											do while temp_cash_flow <> 0
												neg_discounted = 0
												
												//If this iteration is less than or equal to the number of rows, use the row to absorb the cost.
												if j <= rows then
													neg_discount_rate = ds_aro_get_neg_discount_rates.getItemNumber( j, "discount_rate" )
													stream_flow = ds_aro_get_neg_discount_rates.getItemNumber( j, "remaining_cash_flow" )
													applied_layer_id = ds_aro_get_neg_discount_rates.getItemNumber( j, "layer_id" )
													applied_stream_id = ds_aro_get_neg_discount_rates.getItemNumber( j, "stream_id" )
													
													if stream_flow = 0 then 
														j++
														continue
													end if
													
													if stream_flow + temp_cash_flow > 0 then
														applied_cash_flow = temp_cash_flow
														init_bal = init_bal + round( ( applied_cash_flow / ( ( 1 + neg_discount_rate / 12 ) ^ periods ) ) ,2 )
														temp_cash_flow = temp_cash_flow - applied_cash_flow
														stream_discounted += round( ( applied_cash_flow / ( ( 1 + neg_discount_rate / 12 ) ^ periods ) ), 2 )
														neg_discounted = round( ( applied_cash_flow / ( ( 1 + neg_discount_rate / 12 ) ^ periods ) ), 2 )
													else
														applied_cash_flow = -stream_flow
														init_bal = init_bal + round( ( applied_cash_flow / ( ( 1 + neg_discount_rate / 12 ) ^ periods ) ), 2 )
														temp_cash_flow = temp_cash_flow - applied_cash_flow
														stream_discounted += round( ( applied_cash_flow / ( ( 1 + neg_discount_rate / 12 ) ^ periods ) ), 2 )
														neg_discounted = round( ( applied_cash_flow / ( ( 1 + neg_discount_rate / 12 ) ^ periods ) ), 2 )
													end if
													
													insert into aro_layer_stream_downward_adj 
													( aro_id, layer_id, month_yr, applied_layer_id, applied_stream_id, cash_flow_amount, layer_asset_amount, neg_rate, month_end_update )
													values 
													( :i_aro_id, :i_layer_id, :month_yr, :applied_layer_id, :applied_stream_id, :applied_cash_flow, :neg_discounted, :neg_discount_rate, 0 );
													
													if sqlca.SQLCode < 0 then 
														messageBox( "Error", "Error occurred while inserting into ARO Layer Stream Downward Adj for " + string( month_yr ) + " and iteration " + string( j ) + ".~n~nError: " + sqlca.SQLErrText )
														destroy i_ds_aro_streams
														destroy ds_aro_get_neg_discount_rates
														//w_top_frame.setMicroHelp( "Ready" )
														rollback;
														return -1
													end if
												else 
													//Not enough cash flows to cover this negative - use current discount rate
													//Don't add to the neg adj table since this is not adjusting an old layer - these costs stay on the new layer at the new rates
													applied_cash_flow = temp_cash_flow
													init_bal = init_bal + round( ( applied_cash_flow / ( ( 1 + this_rate / 12 ) ^ periods ) ), 2 )
													temp_cash_flow = temp_cash_flow - applied_cash_flow
													stream_discounted += round( ( applied_cash_flow / ( ( 1 + this_rate / 12 ) ^ periods ) ), 2 )
													neg_discounted = round( ( applied_cash_flow / ( ( 1 + this_rate / 12 ) ^ periods ) ), 2 )
													neg_discount_rate = this_rate
												end if
						
												//Insert the discount record.
												insert into aro_layer_discounting
												( aro_id, layer_id, stream_id, month_yr, start_month, periods, cash_flow, discount_rate, discounted_flow, init_bal )
												values 
												( :i_aro_id, :i_layer_id, 0, :month_yr, :start_date, :periods, :applied_cash_flow, :neg_discount_rate, :neg_discounted, :init_bal );
													
												if sqlca.SQLCode < 0 then 
													messageBox( "Error", "Error occurred while inserting into ARO Layer Discounting for " + string( month_yr ) + " and iteration " + string( j ) + ".~n~nError: " + sqlca.SQLErrText )
													destroy i_ds_aro_streams
													destroy ds_aro_get_neg_discount_rates
													//w_top_frame.setMicroHelp( "Ready" )
													rollback;
													return -1
												end if
											
												//Increment
												j++
											loop
										end if//Neg Net Change Application Rules
								end if //Postive or negative cash flow
			
								if isNull( init_bal ) then
									messageBox( "Init Bal Zero", "Init Bal Zero, " + string( i ) + " " + string( month_yr ) )
								end if
						next //i_ds_aro_streams loop
				end if //mass/transition
		end if //i_weighted_avg

////
////	If the initial balance is zero, warn the user.
////
		if init_bal = 0 then
			messageBox( "Error", "Inital Balance for this layer was calculated to be zero, and a layer with a balance of zero has no effect.  Please adjust your streams accordingly." )
		end if

////
////	Update the layer with the intial balance.
////
		update 	aro_layer
		set 		initial_expected_bal = :init_bal,
					layer_asset_value = :init_bal
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while updating Initial Expected Balance and Layer Asset Value on ARO Layer.~n~nError: " + sqlca.SQLErrText )
			destroy i_ds_aro_streams
			destroy ds_aro_get_neg_discount_rates
			//w_top_frame.setMicroHelp( "Ready" )
			return -1
		end if
		
////
////	Call the function to get the old liability.
////
		old_liability = uf_set_old_liability()
		if isNull( old_liability ) then old_liability = 0
		
		if old_liability = -1 then 
			destroy i_ds_aro_streams
			destroy ds_aro_get_neg_discount_rates
			//w_top_frame.setMicroHelp( "Ready" )
			return -1
		end if

////
////	Calculate the new liability.
////
		new_liability = old_liability + init_bal
		
////
////	Update the layer with the liabilities.
////
		update 	aro_layer
		set 		new_liability = :new_liability, 
					old_liability = :old_liability
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while updating New Liability and Old Liability on ARO Layer.~n~nError: " + sqlca.SQLErrText )
			destroy i_ds_aro_streams
			destroy ds_aro_get_neg_discount_rates
			//w_top_frame.setMicroHelp( "Ready" )
			rollback;
			return -1
		end if
		
////
////	Function completed successfully.
////
		destroy i_ds_aro_streams
		destroy ds_aro_get_neg_discount_rates
		//w_top_frame.setMicroHelp( "Ready" )
		return 1
end function

public function integer uf_mass_layer_calc (longlong a_aro_id, longlong a_layer_id, longlong a_stream_id, decimal a_prob, decimal a_cost_per_unit, decimal a_inf_rate, datetime a_gl_posting_mo_yr);/************************************************************************************************************************************************************
 **
 **	uf_mass_layer_calc()
 **	
 **	Calculates the layers for mass AROs.
 **
 **	This function is currently called from:
 **		- w_aro_mass_ye - cb_new_layers.clicked()
 **	
 **	Parameters	:	longlong	:	( a_aro_id ) The ARO to be processed.
 **						longlong	:	( a_layer_id ) The layer to be processed.
 **						longlong	:	( a_stream_id ) The stream to be processed.
 **						longlong	:	( a_prob ) The probability for this stream.
 **						decimal		:	( a_cost_per_unit ) The cost per unit.
 **						decimal		:	( a_inf_rate ) The inflation rate.
 **						datetime		:	( a_gl_posting_mo_yr ) The month for this ARO.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/


longlong 			periods, curve_id, retire_unit_id, month_number, gl_month_number, rows, i, j, vintage, year, lncnt, vrows, m, comp_id
longlong 			reserve_ratio_id, check_curve, rem_life, check_rem_life, curve_id1, curve_pts, expected_life, start_year, last_rr
decimal{2} 	totalret, vint_total, current_estimate, gross_cash, cash_flow, direct_cost, allo_cost, inf_cost, mass_remaining_life
decimal{8}	avg_life, check_life
decimal 		psurv[600], agepct
double 		curve_detail[600,4], retire_ratio[], ps, vint_summ[500,3], bal[500,250], ret[500,250], sumret[250], summary[3,250] 
integer 		lp1, lp2
date 			act_date
string 		curve, curve_def
boolean 		recalc

uo_ds_top 	ds_aro_vintage_qty_summary

////
////	Set the boolean.
////
		recalc = false
		
////
////	Set the instance variables.
////
		i_aro_id = a_aro_id
		i_layer_id = a_layer_id
		
////
////	Validate the passed-in variables.
////
		if isNull( a_cost_per_unit ) then a_cost_per_unit = 0 
		if isNull( a_inf_rate ) then a_inf_rate = 0 

////
////	Set the microhelp.
////
		//w_top_frame.setMicroHelp( "Calculating Mass Cash Flows..." ) 

////
////	Update ARO Stream.
////
		update 	aro_stream
		set 		unit_cost = :a_cost_per_unit,
					inflation_rate = :a_inf_rate
		where 	aro_id = :i_aro_id
		and	 	layer_id = :i_layer_id
		and 		stream_id = :a_stream_id;
		
		if sqlca.SQLCode < 0 then
			messageBox( "Error", "Error occurred while updating Unit Cost and Inflation Rate on ARO Stream.~n~nError: " + sqlca.SQLErrText )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if
		
////
////	Set the start year.
////
		start_year = year( date( a_gl_posting_mo_yr ) )

////
////	Determine curve from the retirement unit for this ARO.
////	Make sure this ARO has a retirement unit assigned.
////
		setNull( retire_unit_id )
		
		select 	count( * )
		into 		:retire_unit_id
		from 		aro_mass_ru
		where 	aro_id = :i_aro_id;
		
		if isNull( retire_unit_id ) or retire_unit_id <= 0 then
			messageBox( "Retirement Unit Required", "For Mass AROs, you must select the retirement unit related to this ARO first.  Please go to the Details Screen to assign the retirement unit." )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if
		
////
////	Get the company for this ARO.  Then retrieve the control value for mortality curve.
////	- "Yes" means associate a mortality curve with the retirement based on the account (for curve retires). 
////	- "No" means use the curve assigned to the retirement unit.  "
////	- "Company Account" means use the company account curves table. 
////	- "Depr Method" means use the Depreciation Method Rates table.
////
		setNull( comp_id )
		
		select 	company_id 
		into 		:comp_id
		from 		aro
		where 	aro_id = :i_aro_id;

		curve_def = f_pp_system_control_company( "Mortality Curve Account Lookup", comp_id )
		
////
////	Get the curve and life for this ARO.
////
		setNull( curve_id )
		setNull( expected_life )
		
		select  	mortality_curve_id, 
					expected_life 
		into 		:curve_id, 
					:expected_life
		from 		aro    
		where   	aro_id = :i_aro_id;

		if curve_id < 0 or isNull( curve_id ) then 
			messageBox( "Mortality Curve Required", "There is no curve assigned to this ARO.  Please assign a mortality curve." )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if
		
		if expected_life <= 0 or isNull( expected_life ) then 
			messageBox( "Expected Life Required", "There is no expected life assigned to this ARO.  Please assign an expected life." )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if

////
////	Get the curve points.
////
		curve_pts = f_get_surv_pct( curve_id, psurv )

////
////	Establish an array that will have percent for retiring based on an age.
////
////	The array has the following columns:
////	1 - life index
////	2 - life
////	3 - percent surviving
////	4 - percent retiring
////
		curve_detail[1,3] = 1

		//w_top_frame.setMicroHelp( "Retrieving Mortaility Curve Points..." )
		
		for i = 2 to curve_pts
			curve_detail[i - 1,1] = i - 2
			curve_detail[i,2] = i - 1.5
			agepct = curve_detail[i,2] / expected_life * 100
			lp1 = agepct + 1
			lp2 = lp1 - 1
			
			if lp1 >= curve_pts then
				ps = 0
			else
				ps = psurv[lp1] + ( psurv[lp1 + 1] - psurv[lp1] ) * ( agepct - lp2 )
			end if
			
			curve_detail[i,3] = ps
			if curve_detail[i,3] <= 0 then
				//You have gotten to the end of the curve for the specified life
				curve_detail[i - 1,4] = curve_detail[i - 1,3]
				retire_ratio[i - 1] = 1
				lncnt = i - 1
				exit
			else
				curve_detail[i - 1,4] = curve_detail[i - 1,3] - curve_detail[i,3]
				//retire_ratio, percent of the qty that should be retired at this point in time
				retire_ratio[i - 1] = curve_detail[i - 1,4]/curve_detail[i - 1,3] 
			end if
		next//Curve Points
		
////
////	Get vintage statistics into an array.
////
////	The array has the following columns:
////	1 - vintage
////	2 - starting point for curve
////	3 - current quantity on the CPR
////
		ds_aro_vintage_qty_summary = create uo_ds_top
		ds_aro_vintage_qty_summary.dataobject = "dw_aro_vintage_qty_summary_aro"
		ds_aro_vintage_qty_summary.setTransObject( sqlca )

		vrows = ds_aro_vintage_qty_summary.retrieve( i_aro_id, a_gl_posting_mo_yr, comp_id )
		if vrows = 0 then 
			messageBox( "No Rows", "There are no Vintage Summary rows for this ARO." )
			//w_top_frame.setMicroHelp( "Ready" ) 
			destroy ds_aro_vintage_qty_summary
			return - 1
		elseif vrows < 0 then
			messageBox( "Error", "There was an error retrieving the datastore for Vintage Summary~n~nError: " + sqlca.SQLErrText )
			//w_top_frame.setMicroHelp( "Ready" ) 
			destroy ds_aro_vintage_qty_summary
			return - 1
		end if

		//w_top_frame.setMicroHelp( "Retrieving Vintage Information..." )
		
		vint_total = 0
		for j = 1 to vrows 
			if ds_aro_vintage_qty_summary.getItemNumber( j, "total_quantity" ) > 0 then
				vint_summ[j,1] = ds_aro_vintage_qty_summary.getItemNumber( j, "vintage" )
				vint_summ[j,2] = start_year - vint_summ[j,1] - 0.5 
				vint_summ[j,3] = ds_aro_vintage_qty_summary.getItemNumber( j, "total_quantity" )
				vint_total = vint_total + vint_summ[j,3]
			end if
		next //Vintage Row
		
		destroy ds_aro_vintage_qty_summary
		
////
////	Delete any existing rows from the calc table.
////
		delete from aro_mass_calc
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id
		and 		stream_id = :a_stream_id;
		
		if sqlca.SQLCode < 0 then
			messageBox( "Error", "Error occurred while deleting from ARO Mass Calc.~n~nError: " + sqlca.SQLErrText )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if
		
////
////	Based on Vintage Statistics and ps array determine 2D array of Vintage and Activity Year.
////
////	ret[] holds the number of retirements each period.  It has columns:
////	1 - vintage
////	2 - activity year
////
////	bal[] holds the balance to keep track of cumulative retirements.  It has columns:
////	1 - vintage
////	2 - activity year
////
		last_rr = upperBound( retire_ratio[] )
		
		for i = 1 to vrows
			//w_top_frame.setMicroHelp( "Calculating Vintage ( " + string( i ) + "/" + string( vrows ) + " )..." ) 
			
			m = vint_summ[i,2] + 1.5
			
			if m > last_rr then
				//Outside of curve, retire immediately
				ret[i,1] = round( vint_summ[i,3] * 1,2 )
			else
				ret[i,1] = round( vint_summ[i,3] * retire_ratio[m],2 )
			end if
			
			bal[i,1] = vint_summ[i,3] - ret[i,1]
			sumret[1] += ret[i,1]
			
			if ret[i,1] > 0 then 
				//retirement number exists
				//vintage = vint_summ[i,1]
				//activity_year = start_year
				
				insert into aro_mass_calc
				( aro_id, layer_id, stream_id, vintage, activity_year, retire_quantity, unit_cost, cash_flow, discounted_cash_flow )
				values 
				( :i_aro_id, :i_layer_id, :a_stream_id, :vint_summ[i,1], :start_year, :ret[i,1], 0, 0, 0 );
				
				if sqlca.SQLCode < 0 then 
					messageBox( "Error", "Error occurred while inserting into ARO Mass Calc (1).~n~nError: " + sqlca.SQLErrText )
					//w_top_frame.setMicroHelp( "Ready" ) 
					return -1
				end if
			end if
			
			for j = 2 to 250
				m = vint_summ[i,2] + j + 0.5
				if m > last_rr then exit
				
				ret[i,j] = round( bal[i,j - 1] * retire_ratio[m],2 )
				bal[i,j] = bal[i,j - 1] - ret[i,j]
				sumret[j] += ret[i,j]
				
				if ret[i,j] > 0 then 
					//retirement number exists
					//vintage = vint_summ[i,1]
					//activity_year = start_year + j - 1
					
					insert into aro_mass_calc
					( aro_id, layer_id, stream_id, vintage, activity_year, retire_quantity, unit_cost, cash_flow, discounted_cash_flow )
					values 
					( :i_aro_id, :i_layer_id, :a_stream_id, :vint_summ[i,1], :start_year + :j - 1, :ret[i,j], 0, 0, 0 );
								
					if sqlca.SQLCode < 0 then 
						messageBox( "Error", "Error occurred while inserting into ARO Mass Calc (2).~n~nError: " + sqlca.SQLErrText )
						//w_top_frame.setMicroHelp( "Ready" ) 
						return -1
					end if
				end if
			next // j - 2 to 250
		next // i - vrows
		
////
////	Check total for audit.
////
		totalret = 0
		
		for i = 1 to 250
			totalret = sumret[i] + totalret
		next	
		
		if vint_total <> totalret then 
			messageBox( "Error", "Vintage Total does not equal the Final Total.~nVintage Total: " + string( vint_total ) + "~nAllocated Total: " + string( totalret ) )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if

////
////	Delete the other rows from ARO Layer Stream.
////
		//w_top_frame.setMicrohelp( "Deleting Current Cash Flows..." ) 
		
		delete from aro_layer_stream
		where 	aro_id = :i_aro_id
		and		layer_id = :i_layer_id
		and 		stream_id = :a_stream_id;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while deleting from ARO Layer Stream.~n~nError: " + sqlca.SQLErrText )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if
		
////
////	Based on activity year, calculate the inflated cost per unit for each year.
////	Start year is the Current Year for GL Posting Mo Yr.
////	sumret[1] is current year
////
		gl_month_number = ( start_year ) * 100 + 12
		
		for i = 1 to last_rr
			//w_top_frame.setMicrohelp( "Inserting New Cash Flow Items ( " + string( i ) + "/" + string( last_rr ) + " )..." ) 
			
			month_number = ( start_year + i - 1 ) * 100 + 12
		
			select 	months_between( to_date( :month_number,'yyyymm' ), to_date( :gl_month_number,'yyyymm' ) ) 
			into 		:periods 
			from 		dual;
	
			current_estimate = 0
			
			inf_cost = round( ( a_cost_per_unit ) * ( ( 1 + ( a_inf_rate ) ) ^ ( truncate( ( periods / 12 ), 0 ) ) ), 2 )

			update 	aro_mass_calc
			set 		unit_cost = :inf_cost, 
						cash_flow = retire_quantity * :inf_cost
			where 	activity_year = ( :start_year + :i - 1 )
			and 		aro_id = :i_aro_id
			and 		layer_id = :i_layer_id
			and 		stream_id = :a_stream_id;
			
			if sqlca.SQLCode < 0 then 
				messageBox( "Error", "Error occurred while updating Unit Cost and Cash Flow on ARO Mass Calc.~n~nError: " + sqlca.SQLErrText )
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
	
			select 	sum( retire_quantity ), 
						sum( cash_flow )
			into 		:summary[1,i], 
						:summary[3,i]
			from 		aro_mass_calc
			where 	activity_year = ( :start_year + :i - 1 )
			and 		aro_id = :i_aro_id
			and 		layer_id = :i_layer_id
			and 		stream_id = :a_stream_id;
			
			if isNull( summary[1,i] ) then summary[1,i] = 0
			if isNull( summary[3,i] ) then summary[3,i] = 0
	
			summary[2,i] = inf_cost
			gross_cash = summary[3,i]
			
			if summary[1,i] + summary[3,i] = 0 then continue
			
			uf_stream_update_mass( a_stream_id, date( string( ( start_year + i - 1 ) ) + "-12-01" ), inf_cost, a_inf_rate, summary[3,i], " " , 0, 0, summary[1,i] )
		next //last_rr

////
////	Assign and check reserve ratios.
////		
		select 	mortality_curve_id, 
					expected_life, 
					reserve_ratio_id 
		into 		:curve_id1, 
					:avg_life, 
					:reserve_ratio_id
		from 		aro
		where 	aro_id = :i_aro_id;
		
		rem_life = -1
		
		if isNull( curve_id1 ) or isNull( avg_life ) then
			reserve_ratio_id = 0
		else
			if reserve_ratio_id <> 0 and not isNull( reserve_ratio_id ) then
				check_curve = 0
				check_life = 0
				check_rem_life = 0
				
				select 	mortality_curve_id, 
							expected_average_life, 
							integer_rem_life
				into   		:check_curve,       
							:check_life,           
							:check_rem_life
				from 		reserve_ratios
				where 	reserve_ratio_id = :reserve_ratio_id 
				and 		rownum = 1;
				
				if check_curve <> curve_id1 or check_life <> avg_life or check_rem_life <> rem_life then 
					reserve_ratio_id = f_build_reserve_ratios( curve_id1, avg_life, rem_life )
				end if
			else
				reserve_ratio_id = f_build_reserve_ratios( curve_id1, avg_life, rem_life )
			end if
		end if
		
		if reserve_ratio_id > 0 then
			update 	aro
			set 		reserve_ratio_id = :reserve_ratio_id
			where 	aro_id = :i_aro_id;
			
			if sqlca.SQLCode < 0 then 
				messageBox( "Error", "Error occurred while updating Reserve Ratio ID on ARO.~n~nError: " + sqlca.SQLErrText )
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		else
			//Reset fields so they are included in dollar age ratio
			update 	aro
			set 		reserve_ratio_id = null
			where 	aro_id = :i_aro_id;
			
			if sqlca.SQLCode < 0 then 
				messageBox( "Error", "Error occurred while updating Reserve Ratio ID to null on ARO.~n~nError: " + sqlca.SQLErrText )
				//w_top_frame.setMicroHelp( "Ready" ) 
				return -1
			end if
		end if

////
////	Update the remaining life.
////
		select 	sum( calc_view.qty * reserve_ratios.remaining_life * 12 ) / sum( calc_view.qty )
		into 		:mass_remaining_life
		from 		aro, 
					reserve_ratios,
					( 	select 	vintage, 
									sum( retire_quantity ) qty 
						from 		aro_mass_calc
						where 	aro_id = :i_aro_id
						and 		layer_id = :i_layer_id
						group by	vintage
					 ) calc_view
		where 	aro.aro_id = :i_aro_id
		and 		aro.reserve_ratio_id = reserve_ratios.reserve_ratio_id
		and 		reserve_ratios.integer_age = ( to_number( to_char( :a_gl_posting_mo_yr,'yyyy' ) ) - calc_view.vintage + 1 );
		
		update 	aro_layer
		set 		expected_remaining_life = :mass_remaining_life
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id;
		
		if sqlca.SQLCode < 0 then 
			messageBox( "Error", "Error occurred while updating Expected Remaining Life on ARO Layer.~n~nError: " + sqlca.SQLErrText )
			//w_top_frame.setMicroHelp( "Ready" ) 
			return -1
		end if		 
			
////
////	Function completed successfully.
////
		//w_top_frame.setMicroHelp( "Ready" ) 
		return 1
end function

public function integer uf_stream_update_mass (longlong a_stream_id, date a_month_yr, decimal a_cost_per_unit, decimal a_in_rate, decimal a_gross_cash_flow, string a_comments, decimal a_cash_flow, decimal a_remain_cf, decimal a_qty);/************************************************************************************************************************************************************
 **
 **	uf_stream_update_mass()
 **	
 **	Calculates the layers for mass AROs.  When this function is called, the instance variables i_aro_id and i_layer_id have already been set.
 **
 **	This function is currently called from:
 **		- uf_mass_layer_calc()
 **	
 **	Parameters	:	longlong	:	( a_stream_id ) The stream to be processed.
 **						datetime		:	( a_month_yr ) The month for this calculation.
 **						decimal		:	( a_cost_per_unit ) The cost per unit.
 **						decimal		:	( a_in_rate ) The inflation rate.
 **						decimal		:	( a_gross_cash_flow ) The gross cash flow.
 **						string			:	( a_comments ) Comments.
 **						decimal		:	( a_cash_flow ) The cash flow.
 **						decimal		:	( a_remain_cf ) The remaining cash flow.
 **						decimal		:	( a_qty ) The quantity.
 **						
 **	Returns		:	any			:	1		:	The calculation was successful
 **											-1		:	An error occurred.
 **
 ************************************************************************************************************************************************************/

longlong 	check_row

////
////	See if there is a row.
////
		setNull( check_row )
		
		select 	count(*)
		into 		:check_row
		from 		aro_layer_stream
		where 	aro_id = :i_aro_id
		and 		layer_id = :i_layer_id
		and 		stream_id = :a_stream_id
		and 		month_yr = :a_month_yr;
		
////
////	Process based on if there is a row or not.
////
		if check_row > 0 then
		////
		////	If there is a row and the gross cash flow is zero, delete the row.
		////	If there is a row and the gross cash flow is not zero, update the row.
		////
				if a_gross_cash_flow = 0 then
					delete from aro_layer_stream
					where 	aro_id = :i_aro_id
					and 		layer_id = :i_layer_id
					and 		stream_id = :a_stream_id
					and 		month_yr = :a_month_yr;
					
					if sqlca.SQLCode <> 0 then
						messageBox( "Error", "Error occurred while deleting from ARO Layer Stream.~n~nError: " + sqlca.SQLErrText )
						return -1
					end if
				else
					update 	aro_layer_stream
					set 		cash_flow = :a_cash_flow, 
								remaining_cash_flow = :a_remain_cf,
								direct_cost = 0, 
								allocated_cost = 0, 
								mark_up_rate = 0, 
								inflation_rate = :a_in_rate,
								risk_premium_rate = 0,
								gross_cash_flow = :a_gross_cash_flow, 
								comments = :a_comments, 
								cost_per_unit = :a_cost_per_unit,
								mass_quantity = :a_qty
					where 	aro_id = :i_aro_id
					and 		layer_id = :i_layer_id
					and 		stream_id = :a_stream_id
					and 		month_yr = :a_month_yr;
					
					if sqlca.SQLCode <> 0 then
						messageBox( "Error", "Error occurred while updating ARO Layer Stream.~n~nError: " + sqlca.SQLErrText )
						return -1
					end if
				end if
		else
		////
		////	If there is not a row and the gross cash flow is not zero, insert.
		////
			if a_gross_cash_flow <> 0 then
				insert into aro_layer_stream
				( aro_id, layer_id, stream_id, month_yr, cash_flow, remaining_cash_flow, direct_cost, allocated_cost, mark_up_rate, 
					inflation_rate, risk_premium_rate, gross_cash_flow, comments, cost_per_unit, mass_quantity )
				values 
				( :i_aro_id, :i_layer_id, :a_stream_id, :a_month_yr, :a_cash_flow, :a_remain_cf, 0, 0, 0, 
					:a_in_rate, 0, :a_gross_cash_flow, :a_comments, :a_cost_per_unit, :a_qty );
					
				if sqlca.SQLCode <> 0 then
					messageBox( "Error", "Error occurred while inserting into ARO Layer Stream.~n~nError: " + sqlca.SQLErrText )
					return -1
				end if
			end if
		end if

////
////	Function completed successfully.
////
		return 1
end function

public function decimal uf_calc_rate_trial (decimal a_gross_cash_flow[], decimal a_ann_disc_rate[], longlong a_months_between[], decimal a_starting_rate, string a_same_or_prior);////
////	This function is called from wf_calc_cur_layer_weighed_rate() when process a Time Average rate type.
////

longlong 			i, t, iterations,  nt[]
string 		same_or_prior
decimal{2}	ct[] 
decimal  		drt[],  dra1,  csum, ft, ftp, psum, tolerance
boolean 		again

////
////	Initialize the variables.
////
		again = true
		ct[] = a_gross_cash_flow[]
		drt[] = a_ann_disc_rate[]
		nt[] = a_months_between[]
		dra1 = a_starting_rate
		same_or_prior = a_same_or_prior
		tolerance = 0.0001
		ft = 0
		ftp = 0
		iterations = 0
		csum = 0
		
////
////	Validate that arrays are not empty
////
		if upperbound(nt) = 0 or upperbound(drt) = 0 or upperbound(nt) = 0 then
			return 0
		end if

////
////	Loop through the months and calculate.
////
		t = upperbound( nt[] )
		for i = 1 to t
			psum = ( ct[i] / ( ( ( 1 + drt[i] ) ^ ( 1 / 12 ) ) ^ nt[i] ) )  
			csum = csum + psum
		next

////
////	Loop through until we meet the tolerance.
////
		do while again
			iterations ++ 
			ft = 0
			ftp = 0
				
			for i = 1 to t
				//Calculate the functional value.
				ft  = ft + ( ct[i] / ( ( ( 1 + dra1 ) ^ ( 1 / 12 ) ) ^ nt[i] ) )     
				
				//Calculate the derivative value.
				ftp = ftp + ( ( -1 / 12 ) * ( ct[i] * nt[i] ) ) / ( ( ( 1 + dra1 ) ^ ( 1 / 12 ) ) ^ ( nt[i] + 1 ) )
			next
			
			ft = ft - csum
			
			if ftp <> 0 then
				dra1 = dra1 - ( ft / ftp)
			else
				messageBox( "Error", "Could not determine time average discount rate due to zero derivative, possibly due to a cash flow month being the same as the GL posting month." )
				return -1
			end if
		 
			if abs( ft ) < tolerance then
				again = false
			end if
			
			if iterations > 200 then
				messagebox( "Error", "Could not determine time average discount rate due to not converging." )
				return -1
			end if
		loop 

////
////	Return the rate.
////
		return dra1
end function

on uo_aro_estimates.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_aro_estimates.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;////
////	This user object contains common functions used through the ARO module.  It is currently used in the following objects:
////		- f_aro_calc_settlement
////		- w_aro_estimate
////		- w_aro_mass_ye
////		- w_aro_pp_approval
////
////	The calling objects will take care of rolling back or committing.
////
end event

