HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$Standard Interface Object
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
string i_exe_name = 'ssp_wo_retirements.exe'


nvo_wo_control i_nvo_wo
end variables

forward prototypes
public function longlong uf_read ()
public function longlong uf_ws_example ()
public function longlong uf_auto_retirements (longlong a_company_id, date a_month)
end prototypes

public function longlong uf_read ();//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_read
//
//   Purpose  :  This function is called from the ppinterface application object, and is 
//               the starting point for custom code for all PowerPlant client interfaces.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************


//	
//	Example code to illustrate technique of using variable return values from the 
//	pp_processes_return_values table instead of hard coding the return values.
//	
longlong rtn_success, rtn_failure, month_number, rtn
string process_msg

// initially, default these values in case the pp_processes_return_values records are not setup
rtn_success = 0
rtn_failure = -1

// pull the numeric return value for a successful run of this interface
SELECT return_value
into :rtn_success
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'SUCCESS'
;

// pull the numeric return value for a failed run of this interface
SELECT return_value
into :rtn_failure
from pp_processes_return_values
where process_id = :g_process_id
and upper(description) = 'FAILURE'
;

//declare w_datawindows to get them to compile.
w_datawindows w_not_used

/***************************************************************/
//Copy arguments into local arrays with better names
/***************************************************************/
longlong company_ids[]
date processing_month
company_ids = g_ssp_parms.long_arg
processing_month = g_ssp_parms.date_arg[1]

/***************************************************************/
//Get company descriptions and begin logging
/***************************************************************/
string company_descriptions[]
longlong i
f_pp_msgs("Beginning Work Order Retirements process for the following companies: ")
for i = 1 to upperbound(company_ids) 
	SELECT description
	INTO :company_descriptions[i]
	FROM company
	WHERE company_id = :company_ids[i];
	
	f_pp_msgs("	" + company_descriptions[i])
next 
f_pp_msgs("Processing month: " + string(processing_month))

/***************************************************************/
//setup i_nvo_wo for the functions from the window. 
/***************************************************************/
i_nvo_wo.of_constructor()
rtn = i_nvo_wo.of_setupfromcompaniesandmonth(company_ids, processing_month)
if rtn <> 1 then
	// of_getDescriptionsFromIds logs the appropriate error
	return rtn_failure
end if

/***************************************************************/
// lock the process for concurrency purposes
/***************************************************************/
month_number = year(date(processing_month))*100 + month(date(processing_month))
if (i_nvo_wo.of_lockprocess( i_exe_name,  company_ids,month_number, process_msg)) = false then
          f_pp_msgs("Retirement Transactions - " + &
                         "There has been a concurrency error. Please check that processes are not currently running")
          return rtn_failure
end if 


/***************************************************************/
//loop over companies and do actual processing
/***************************************************************/
longlong pp_stat_id
for i = 1 to upperbound(company_ids)	
	//	it's selected, update the rest of the window
	i_nvo_wo.of_companychanged(i, datetime(processing_month))
	
	f_pp_msgs("Month: " + string(i_nvo_wo.i_month_number) + &
		" Company:  " + company_descriptions[i] + " Comp Id: " + string(company_ids[i])+ ' ' + string(now()))
	
	f_pp_msgs("Retirement Transactions Processing Started for company " + company_descriptions[i] +  " at " + String(Today(), "hh:mm:ss"))
	
	// Make sure the month isn't closed
	if not isnull(i_nvo_wo.i_ds_wo_control.GetItemDateTime(1, 'powerplant_closed')) then
		f_pp_msgs("This month is closed.")
		return rtn_failure
	end if
	
	pp_stat_id = f_pp_statistics_start("WO Monthly Close", "Retirement Transactions: " + company_descriptions[i])
	
	rtn = uf_auto_retirements(company_ids[i], processing_month)
	
	if rtn <> 1 then
		f_pp_msgs("Exiting due to error.")
		return rtn_failure
	end if
	
	i_nvo_wo.i_ds_wo_control.setitem(1, 'auto_retirements', today()) 
	
	rtn = i_nvo_wo.of_updatedw()
	if rtn < 0 then 
		return rtn_failure
	end if
	
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)

next

/***************************************************************/
// Send email
/***************************************************************/
i_nvo_wo.of_cleanup( 4, 'email wo close: retirement transactions', "Retirement Transactions")

f_pp_msgs("Done!"+ ' ' + string(now()))

process_msg = ""
i_nvo_wo.of_releaseprocess( process_msg)
f_pp_msgs("Release Process Status: " + process_msg)
return rtn_success
end function

public function longlong uf_ws_example ();//*****************************************************************************************
//
//	CUSTOM CODE SHOULD ONLY GO IN THE BLOCK AS NOTED FOR CUSTOM CODE BELOW.  ALL OTHER CODE IS REQUIRED
//		FOR THE WEBSERVICE TO FUNCTION LIKE A NORMAL PP INTERFACE.
//
//*****************************************************************************************
string exception_msg, exception_msg_nvo
longlong rtn, i

TRY 
	 // create global variables
	g_string_func	= create nvo_func_string		
	g_ds_func		= create nvo_func_datastore
	g_db_func		= create nvo_func_database 
	g_io_func		= create nvo_pp_func_io

	g_rte = CREATE nvo_runtimeerror 	
	g_rte_app = create nvo_runtimeerror_app
	g_uo_ppint = CREATE uo_ppinterface
	g_uo_client = CREATE uo_client_interface
	g_sqlca_logs = CREATE uo_sqlca_logs
	g_prog_interface = CREATE u_prog_interface
	g_ssp_nvo = create nvo_server_side_request
	
	// i_exe_name = 'executable_field_name|version'
	i_exe_name = 'web_service.exe|10.3.0.0'
	
	//  Create database connections, handle versioning, etc
	rtn = g_uo_ppint.uf_connect()
	if rtn > 0 then
		//*****************************************************************************************
		//
		// CUSTOM CODE GOES HERE
		//
		//*****************************************************************************************	
		f_pp_msgs(' ')
		f_pp_msgs('******************** Begin Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		this.uf_read()
		
		f_pp_msgs(' ')
		f_pp_msgs('******************** End Interface Custom Code ********************')
		f_pp_msgs(' ')
		
		//*****************************************************************************************
		//
		// CUSTOM CODE ENDS HERE
		//
		//*****************************************************************************************	
	end if


// The CATCH block traps any exceptions or runtime errors.
// This will prevent PowerBuilder's "popup" messages from displaying on the screen. 
catch (nvo_runtimeerror nvo_e)
	// For standard components, error information may be logged in g_rte_app instead.  Pull that information
	//	here.
//	if isnull(nvo_e.i_rtn) then
//		uf_synch_rte()
//	end if
	
	g_rtn_code = nvo_e.i_rtn
	g_rtn_failure = nvo_e.i_rtn
	
	if isNull(nvo_e.text) then nvo_e.text = ''
	
	exception_msg_nvo  = 'ERROR: An interface error occurred with message "' + nvo_e.text + '"'
	f_pp_msgs_detail(exception_msg_nvo,string(nvo_e.i_pp_error_code),string(nvo_e.i_sqlca_sqldbcode))
	
	exception_msg = 'Additional Information:'
	
	// Additional information
	for i = 1 to upperbound(nvo_e.i_args)
		if not isNull(nvo_e.i_args[i]) and trim(nvo_e.i_args[i]) <> '' then exception_msg += '~r~n   '         + string(nvo_e.i_args[i])
	next
	
	// SQL Information
	if not isNull(nvo_e.i_sqlca_sqlcode) then exception_msg += '~r~n   SQLCode: '         + string(nvo_e.i_sqlca_sqlcode)
	if nvo_e.i_sqlca_sqlcode >= 0 and not isNull(nvo_e.i_sqlca_sqlnrows) then exception_msg += '~r~n   SQLNRows: '         + string(nvo_e.i_sqlca_sqlnrows)
	if nvo_e.i_sqlca_sqlcode < 0 and not isNull(nvo_e.i_sqlca_sqlerrtext) then exception_msg += '~r~n   SQLErrText: '         + string(nvo_e.i_sqlca_sqlerrtext)
	
	// append more RTE details
	if not isNull(nvo_e.line) then exception_msg += '~r~n   Line: '         + string(nvo_e.line)
	if not isNull(nvo_e.number) then exception_msg += '~r~n   Number: '       + string(nvo_e.number)
	if not isNull(nvo_e.class) then exception_msg += '~r~n   Class: '        + nvo_e.class
	if not isNull(nvo_e.ObjectName)  then exception_msg += '~r~n   Object Name: '  + nvo_e.ObjectName
	if not isNull(nvo_e.RoutineName) then exception_msg += '~r~n   Routine Name: ' + nvo_e.RoutineName
	
	if g_db_connected then 
		rollback;
		
		// Lock the interface if necessary.
		if g_uo_ppint.i_system_lock_enabled = 1 then
			update pp_processes set system_lock  = 1
			where process_id = :g_process_id
			using g_sqlca_logs;
		end if;
	end if
		
catch (Exception e)
	exception_msg  = 'ERROR: An unhandled ' + upper(e.classname()) + ' occurred with message "' + e.getmessage() + '"'
catch (RunTimeError rte)
	exception_msg  = 'ERROR: An unhandled ' + upper(rte.classname()) + ' occurred with message "' + rte.getmessage() + '"'
	
	// append more RTE details
	if not isNull(rte.line) then exception_msg += '~r~n   Line: '         + string(rte.line)
	if not isNull(rte.number) then exception_msg += '~r~n   Number: '       + string(rte.number)
	if not isNull(rte.class) then exception_msg += '~r~n   Class: '        + rte.class
	if not isNull(rte.ObjectName)  then exception_msg += '~r~n   Object Name: '  + rte.ObjectName
	if not isNull(rte.RoutineName) then exception_msg += '~r~n   Routine Name: ' + rte.RoutineName
	
	// this code will always get executed, regardless of an exception or not
finally
END TRY

//  Disconnect and end
g_rtn_code = g_uo_ppint.uf_disconnect()

return g_rtn_code
end function

public function longlong uf_auto_retirements (longlong a_company_id, date a_month);//*****************************************************************************************
//
//	Function:	wf_auto_retirements
//
//*****************************************************************************************

string submitter_option, manual_retire_in_serv, year, month
longlong num_month, ret_rows
uo_ds_top ds_rets

// if an error occurs for which we continue processing, just keep track that there was an error
longlong l_all_success
l_all_success = 1


ds_rets = CREATE uo_ds_top
manual_retire_in_serv = f_pp_system_control_company("Manual Blanket Retires-In Srvc Date", -1)
submitter_option =  f_pp_system_control_company("AUTORET-PENDING Submitter Option", -1) //###NTA maint 5573 11/03/10

if manual_retire_in_serv = 'yes' then
	ds_rets.DataObject = "dw_auto101_retirements_manual_bkts"
else 
	ds_rets.DataObject = "dw_auto101_retirements"
end if

ds_rets.SetTransObject(sqlca)

uo_ds_top ds_memo
ds_memo = CREATE uo_ds_top
ds_memo.DataObject = "dw_wo_unit_item_memo_retrieve"
ds_memo.SetTransObject(sqlca)

num_month = month(date(a_month))
year      = string(year(date(a_month)))

if num_month < 10 then
	month = "0" + string(num_month)
else
	month = string(num_month)
end if

uo_ds_top ds_pend_trans
ds_pend_trans = CREATE uo_ds_top
ds_pend_trans.DataObject = "dw_pend_trans_update"
ds_pend_trans.SetTransObject(sqlca)

uo_ds_top ds_sub_account
ds_sub_account = CREATE uo_ds_top
ds_sub_account.DataObject = "dw_sub_account_select"
ds_sub_account.SetTransObject(sqlca)
ret_rows = ds_sub_account.RETRIEVE()
if ret_rows < 0 then 
	f_pp_msgs("ds_sub_account.retrieve() returned an error.")
	return -1
end if 

/* Mark Yuan Equip Ledger Code 3/20/2009 */
//make the pend_equip datastore
// Determine if they are using Equipment Ledger 02/21/2011
// Used to Skip processing of Equipment Ledger if not being used
boolean lb_has_equip_ledger
select count(*) into :lb_has_equip_ledger from CPR_EQUIP_LEDGER where rownum = 1;

if lb_has_equip_ledger then
   uo_ds_top ds_pend_equip
   ds_pend_equip = create uo_ds_top
   ds_pend_equip.DataObject = "dw_pend_equip"
   ds_pend_equip.SetTransObject(sqlca)
   ret_rows = ds_pend_equip.RETRIEVE()
	if ret_rows < 0 then 
		f_pp_msgs("ds_pend_equip.retrieve() returned an error.")
		return -1
	end if 

// make cpr_equip_retire_relate datastore  02/21/2011
   uo_ds_top ds_cpr_retire_relate
   ds_cpr_retire_relate = create uo_ds_top
   ds_cpr_retire_relate.DataObject = "dw_cpr_retire_relate"
   ds_cpr_retire_relate.SetTransObject(sqlca)
end if
/* end equip ledger */


//  GL_JE_CODE ...
longlong je_id
select je_id into :je_id
  from gl_je_control where upper(ltrim(rtrim(process_id))) = 'AUTOMATIC RETIREMENTS';

if isnull(je_id) or je_id < 1 then
	f_pp_msgs("Cannot process Automatic Retirements - No je_code exists.")

	DESTROY ds_rets
	DESTROY ds_memo
	DESTROY ds_sub_account
	DESTROY ds_pend_trans
	return -1
end if

string gl_je_code
select gl_je_code into :gl_je_code
  from standard_journal_entries where je_id = :je_id;

//JRD 9/18/07 - retrieve retirements by month doesn't pick up next months retirements if present at close
if manual_retire_in_serv = 'yes' then
   ret_rows = ds_rets.RETRIEVE(a_company_id)
else
   ret_rows = ds_rets.RETRIEVE(a_company_id, datetime(a_month))
end if

//  If there are no retirements, split ...
if ret_rows < 1 then 
	DESTROY ds_rets
	DESTROY ds_memo
	DESTROY ds_sub_account
	DESTROY ds_pend_trans
	if ret_rows = 0 then 
		f_pp_msgs("There are no rows to process for company_id " + string(a_company_id))
		return 1
	else 
		f_pp_msgs("ds_rets.retrieve() returned an error. Manual Blanket Retires-In Srvc Date system control = " + manual_retire_in_serv)
		return -1
	end if 
end if

longlong i, ii, k, work_order_id,check_count, current_row, new_trans_id, utility_account_id, bus_segment_id, id, asset_id, disp_code_id, ru_id, &
	retire_method_id, books_schema_id, unit_ret_method_id, charge_id, asset_acct_meth_id, unit_item_id, memo_rows, memo_ru_id, e_current_row
decimal{2} replace_amt, total_value, total_quantity, memo_qty//, reserve, temp_gain_loss, reserve_for_pend_trans
date out_of_service_date
string wo_number, sa_filter, id_str, activity_code, descr, memo_descr
for i = 1 to ret_rows

	if i = 1 or mod(i,100) = 0 or i = ret_rows then
		f_pp_msgs("Retirements: " + string(i) + " of " + string(ret_rows) + "...")
	end if
	
  	///// $$$ARE 4/29/2005 do not allow double posting or rets if pend trans exists and is unposted
  	work_order_id = ds_rets.GetItemNumber(i, "work_order_id")
  	wo_number = ds_rets.GetItemString(i, "work_order_number")  
	  
//  	select count(p.pend_trans_id) into :check_count
//  	from pend_transaction p, work_order_control w
//  	where w.work_order_number = p.work_order_number and w.company_id = p.company_id
//  		and w.work_order_id = :work_order_id and p.ferc_activity_code = 2;
	  
  	select count(p.pend_trans_id) into :check_count
  	from pend_transaction p 
  	where p.work_order_number = :wo_number 
	and p.company_id = :a_company_id
	and p.ferc_activity_code = 2;
  	
  	if check_count > 0 then  
		
		f_pp_msgs("Retirements not processed for work order " + wo_number + &
		  " because there are already pending transaction retirements for the work order. They must be posted or deleted first")
		continue
  	end if
  	
	
	current_row = ds_pend_trans.InsertRow(0)

  	new_trans_id = f_topkey_no_dw("pend_transaction", "pend_trans_id")

   ds_pend_trans.SetItem(current_row, "pend_trans_id", new_trans_id)

  	utility_account_id = ds_rets.GetItemNumber(i, "utility_account_id")
	bus_segment_id     = ds_rets.GetItemNumber(i, "bus_segment_id")
	
	ds_pend_trans.SetItem(current_row, "utility_account_id", utility_account_id)
   ds_pend_trans.SetItem(current_row, "bus_segment_id",     bus_segment_id)

	id = 0
	select func_class_id into :id
	  from utility_account 
	 where utility_account_id = :utility_account_id and
	  		 bus_segment_id     = :bus_segment_id;

  	ds_pend_trans.SetItem(current_row, "func_class_id",      id)

	id = ds_rets.GetItemNumber(i, "sub_account_id")
	 
	if isnull(id) or id < 1 then
		//  Be careful, bus_segment_id can be null ...
		if isnull(bus_segment_id) or bus_segment_id < 1 then
			sa_filter = "utility_account_id = " + string(utility_account_id)
		else
			sa_filter = "utility_account_id = " + string(utility_account_id) + " and " + &
							"bus_segment_id = " + string(bus_segment_id)
		end if
		ds_sub_account.SetFilter(sa_filter)
		ds_sub_account.Filter()
		//  If there are no sub accounts for this utility_account and bus_segment,
		//  we cannot build a unit_item ... it will cause an error when trying to
		//  create the pending transactions.
		if ds_sub_account.RowCount() < 1 then
			//wo_number = trim(ds_wo_to_unitize.GetItemString(i, "work_order_number"))
//			rtn = messagebox("Automatic Retirement Processing", &
//								  "Cannot create pending transactions " + &
//						   		" ... " + &
//								  "No sub account exists for Utility Account " + &
//								   string(utility_account_id) + "  Continue ?", &
//									Exclamation!, YesNo!)
									
			f_pp_msgs("Cannot create pending transactions " + &
						   		" ... " + &
								  "No sub account exists for Utility Account " + &
								   string(utility_account_id))
			
			f_wo_status_box("", 	"Error No sub account exists for Utility Account for work order " + wo_number + ": "+ sqlca.SQLErrText)
			rollback;
			DESTROY ds_rets
			DESTROY ds_memo
			DESTROY ds_sub_account
			DESTROY ds_pend_trans
			return -1 
		else
			id = ds_sub_account.GetItemNumber(1, "sub_account_id")
		end if
		ds_sub_account.SetFilter("")
		ds_sub_account.Filter()				
	end if	 
	 
   ds_pend_trans.SetItem(current_row, "sub_account_id",     id)

	id = ds_rets.GetItemNumber(i, "ldg_asset_id")
	setnull(asset_id)
	asset_id = id
    ds_pend_trans.SetItem(current_row, "ldg_asset_id", id)
	
	disp_code_id = ds_rets.GetItemNumber(i, "disposition_code")
	ds_pend_trans.SetItem(current_row, "disposition_code",disp_code_id)

	//  If a ldg_asset_id exists, enter a 1 for the retire_method_id, per Luis ...
	//  Otherwise, it is mass .. get the retire_method_id from the property_unit.
	ru_id = ds_rets.GetItemNumber(i, "retirement_unit_id")
	
	if asset_id > 0 and not isnull(asset_id) then
		retire_method_id =  1
		ds_pend_trans.SetItem(current_row, "retire_method_id", 1)
		
		//get books schema for existing asset
		books_schema_id = 0
		select books_schema_id into :books_schema_id
		from 	cpr_ledger
		where	asset_id = :asset_id;
		if books_schema_id = 0 or isnull(books_schema_id) then books_schema_id = 1	

	else
		// get retire method from unitized work order if not null
		
		unit_ret_method_id = ds_rets.GetItemNumber(i, "retire_method_id")
		
		if isnull(unit_ret_method_id) then
		
			retire_method_id = 0
			
			select retire_method_id into :retire_method_id
			  from property_unit 
			 where property_unit_id = (
				select property_unit_id from retirement_unit where retirement_unit_id = :ru_id);
			
			if retire_method_id = 0 then setnull(retire_method_id)
			
		else
			
			retire_method_id = unit_ret_method_id
			
		end if
		
		//Maint 29380:  need to make retire_method_id = null for retire_method_id = 9
		if retire_method_id = 9 then
			setnull(retire_method_id)
		end if 
		ds_pend_trans.SetItem(current_row, "retire_method_id", retire_method_id)
		
		books_schema_id = 1	
		
		replace_amt =  ds_rets.GetItemNumber(i, "replacement_amount")
		if not isnull(replace_amt) then
				ds_pend_trans.SetItem(current_row, "replacement_amount", replace_amt)
		end if
		
	end if

	ds_pend_trans.SetItem(current_row, "retirement_unit_id", ru_id)
	
	//### CDM 3-23-2006 - Add "submit_user_id" to pending transaction" ###//
	//JRD ### 5573 submitter_option
	if isnull(submitter_option) then submitter_option= 'ORIGINAL'
	if upper(submitter_option) = 'PROCESSOR' then
		id_str = s_user_info.user_id
		ds_pend_trans.setitem(current_row, "user_id1", id_str)
	else
		setnull(id_str)
		id_str = ds_rets.getitemstring(i, "submit_user_id")
		if isnull(id_str) then id_str = s_user_info.user_id
		ds_pend_trans.setitem(current_row, "user_id1", id_str)
	end if //JRD ### 5573 submitter_option


	id = ds_rets.GetItemNumber(i, "property_group_id")
  	ds_pend_trans.SetItem(current_row, "property_group_id",  id)

 	id = ds_rets.GetItemNumber(i, "asset_location_id")
 	      
   if isnull(id) then
         f_pp_msgs(  "Retirements not processed for work order " + wo_number + &
              " because because the asset location is NULL")
		
		rollback;
		DESTROY ds_rets
		DESTROY ds_memo
		DESTROY ds_sub_account
		DESTROY ds_pend_trans
		return -1 
   end if
	
	id = ds_rets.GetItemNumber(i, "asset_location_id")
	ds_pend_trans.SetItem(current_row, "asset_location_id",  id)
                   
 	id = ds_rets.GetItemNumber(i, "gl_account_id")
  	ds_pend_trans.SetItem(current_row, "gl_account_id",      id)
	
 	id = ds_rets.GetItemNumber(i, "company_id")
  	ds_pend_trans.SetItem(current_row, "company_id",         id)
	  
 	id = ds_rets.GetItemNumber(i, "charge_group_id")
  	ds_pend_trans.SetItem(current_row, "ldg_activity_id",    id)	  

	//  Update this charge_group's pend_transaction with 100 (set to pending) ...
	//
	//  FYI ... the charge_group_control.pend_transaction indicators are:
	//
	//		0 or NULL = Not yet sent to pend_transaction
	//		      100 = Sent to pend_transaction but not yet posted
	//				  1 = Posted
	//
	work_order_id = ds_rets.GetItemNumber(i, "work_order_id")
	
	update charge_group_control set pend_transaction = 100
	 where work_order_id = :work_order_id and charge_group_id = :id;	
	 
	
		if sqlca.SQLCode <> 0 then
			f_pp_msgs("Error updating charge_group_control for work order " + wo_number + ": "+ sqlca.SQLErrText)
			rollback;  //  Any inserts to pend_transaction_memo ...
			DESTROY ds_rets
			DESTROY ds_memo
			DESTROY ds_sub_account
			DESTROY ds_pend_trans
			return -1
		end if

	//
	//  out_of_service_date:
	//
	charge_id       = 0
	select charge_id into :charge_id
	  from work_order_charge_group 
	 where work_order_id = :work_order_id and charge_group_id = :id;
		
	setnull(out_of_service_date)
	select payment_date into :out_of_service_date
	  from cwip_charge where charge_id = :charge_id;
	  
	// JAW 20070228 - first look at WOC and then at payment date
   select nvl(out_of_service_date,:out_of_service_date) into :out_of_service_date
   from work_order_control where work_order_id = :work_order_id;
 	
	if not isnull(out_of_service_date) then
		ds_pend_trans.SetItem(current_row, "in_service_year", out_of_service_date)
	else
		//  If the retirement does not have a payment_date (out of service date),
		//  use the work order's in_service_date.
		setnull(out_of_service_date)
		select nvl(out_of_service_date,in_service_date) into :out_of_service_date
		  from work_order_control where work_order_id = :work_order_id;
		  
		ds_pend_trans.SetItem(current_row, "in_service_year", out_of_service_date)
	end if


	id = ds_rets.GetItemNumber(i, "property_group_id")
  	ds_pend_trans.SetItem(current_row, "property_group_id",    id)	  

	activity_code      = upper(trim(ds_rets.GetItemString(i, "activity_code")))
	asset_acct_meth_id = ds_rets.GetItemNumber(i, "asset_acct_meth_id")
	
	total_value    = ds_rets.GetItemNumber(i, "amount")
  	total_quantity = ds_rets.GetItemNumber(i, "quantity")

	//  If $$$ are zero, pass pend_transaction a NULL so specific retirements will
	//  be priced ...
//	if total_value = 0 then setnull(total_value)

   ds_pend_trans.SetItem(current_row, "posting_amount",   -total_value)        
  	ds_pend_trans.SetItem(current_row, "posting_quantity", -total_quantity)     
  	ds_pend_trans.SetItem(current_row, "reserve_credits", 0)  
	  
 	 //Maint 29376
	if not isnull(asset_id) and activity_code = 'MRET' then
		//retire_method_id already set to 1 in code above
		activity_code = 'URET'
	end if      
	  
	//  The activity_code on the pending transaction should be retained from the unit
	//  item to preserve transaction like specific retirements on mass property ...
	if isnull(activity_code) or activity_code = "" then
		
		if isnull(asset_acct_meth_id) then asset_acct_meth_id = 1	// Better be specific ...
		
		if asset_acct_meth_id = 1 then  // Specific
			ds_pend_trans.SetItem(current_row, "activity_code", "URET")
			ds_pend_trans.SetItem(current_row, "gain_loss", 0)
		else									  // Mass
			ds_pend_trans.SetItem(current_row, "activity_code", "MRET")
			ds_pend_trans.SetItem(current_row, "gain_loss", 0)
		end if
		
	else
		ds_pend_trans.SetItem(current_row, "activity_code", activity_code)
		if upper(activity_code) = 'URET' or upper(activity_code) = 'SALE' or upper(activity_code) = 'MRET' then
			ds_pend_trans.SetItem(current_row, "gain_loss", 0)
		end if
		
		//Maint 38693:  comment out this section of code.  uret with no asset_id errors will be caught in post.exe for the specific retirement record.
		//uret with no asset_id is an invalid combination that is not supported in the post.exe processing logic.
		//do not halt the entire process for validation error on a work order.  validation errors should be done prior to creating the pend transaction.
//		if upper(activity_code) = 'URGL' or upper(activity_code) = 'SAGL' then
//			//Maint 2868:  remove reserve and gain loss calc 
//		else 
//			//Maint 9891:  need to overwrite in_service_year with the Retire Vintage on the OCR
//			if upper(activity_code) = 'URET' and isnull(asset_id) then 	
//				setnull(retire_vintage)
//				retire_vintage = ds_rets.GetItemNumber(i, "retire_vintage") 
//				if retire_vintage > 0 then
//					
//					//verify retirement amount is not $0 if retire method is Specific
//					if total_value = 0 and retire_method_id = 1 then
//						f_status_box("", "A vintaged URET for Specific retire method with no asset_id cannot have posting_amount = 0" )
//						f_status_box("", "Please review retire amount on retirements with URET for Specific retire method and no asset_id on work order = " + wo_number) 
//						
//						if i_process_id > 0 then
//							f_pp_msgs(  "Retirements not processed for work order " + wo_number + &
//						  " : A vintaged URET for Specific retire method with no asset_id cannot have posting_amount = 0")
//						end if
//						
//						f_wo_status_box("", 	"Error vintaged URET for Specific retire method with no asset_id cannot have posting_amount = 0 for work order " + wo_number + ": "+ sqlca.SQLErrText)
//						rollback;
//						DESTROY ds_rets
//						DESTROY ds_memo
//						DESTROY ds_sub_account
//						DESTROY ds_pend_trans
//						return -1 
//					end if 
//					
//					select to_date(:retire_vintage||01, 'yyyymm') into :retire_vintage_date from dual;
//	
//					if sqlca.SQLCODE <> 0 then  
//						f_status_box("", "Unable to convert Retire Vintage to a Date: " +  sqlca.SQLErrtext )
//						f_status_box("", "Please review retire vintage on retirements with URET and no asset_id on work order = " + wo_number) 						
//						
//						if i_process_id > 0 then
//							f_pp_msgs(  "Retirements not processed for work order " + wo_number + &
//						  " : Unable to convert Retire Vintage to a Date: " +  sqlca.SQLErrtext)
//						end if					
//			
//						f_wo_status_box("", 	"Error Unable to convert Retire Vintage to a Date for work order " + wo_number + ": "+ sqlca.SQLErrText)
//						rollback;
//						DESTROY ds_rets
//						DESTROY ds_memo
//						DESTROY ds_sub_account
//						DESTROY ds_pend_trans
//						return -1 
//					else
//						ds_pend_trans.SetItem(current_row, "in_service_year", retire_vintage_date)
//					end if  				
//				end if 
//			end if 
//		end if
	end if

	ds_pend_trans.SetItem(current_row, "ferc_activity_code", 2)

//	select ltrim(rtrim(work_order_number)) into :wo_number
//	  from work_order_control where work_order_id = :work_order_id;
  	ds_pend_trans.SetItem(current_row, "work_order_number",   wo_number)

   ds_pend_trans.SetItem(current_row, "books_schema_id",     books_schema_id)             // 1 = Financial
   ds_pend_trans.SetItem(current_row, "gl_je_code",          gl_je_code)
		
  	ds_pend_trans.SetItem(current_row, "posting_status",      1 )            // pending 
   ds_pend_trans.SetItem(current_row, "subledger_indicator", 0)
  	ds_pend_trans.SetItem(current_row, "gl_posting_mo_yr",    a_month)

	//  Descriptions cannot be NULL ...
	descr = ds_rets.GetItemString(i, "short_description")
	if isnull(descr) then descr = "Work Order Retirement"
	ds_pend_trans.SetItem(current_row, "description",      descr)
	descr = ds_rets.GetItemString(i, "description")
	if isnull(descr) then descr = "Work Order Retirement"
	ds_pend_trans.SetItem(current_row, "long_description", descr)	


	//******************************************************
	//  Memo Retirement Units:  
	//    Only pass the memo data for ADDS in this code.
	//    Retirement memo information goes as part of the
	//    retirement processing.
	//******************************************************
	unit_item_id = ds_rets.GetItemNumber(i, "unit_item_id")
	
	memo_rows = ds_memo.RETRIEVE(unit_item_id, work_order_id)
	if memo_rows < 0 then 
		f_pp_msgs("ds_memo.retrieve() returned an error.")
		return -1
	end if 

	for ii = 1 to memo_rows
		
		memo_ru_id = ds_memo.GetItemNumber(ii, "retirement_unit_id")
		memo_descr = ds_memo.GetItemString(ii, "description")
		memo_qty   = ds_memo.GetItemNumber(ii, "quantity")
		
		insert into pend_transaction_memo
			(pend_trans_id, retirement_unit_id, description, quantity)
		values
			(:new_trans_id, :memo_ru_id, :memo_descr, :memo_qty);
		
			if sqlca.SQLCode <> 0 then
				f_pp_msgs("Error updating pending_transaction.~n~n" + &
							   sqlca.SQLErrText)
				rollback;  //  Any inserts to pend_transaction_memo ...
				DESTROY ds_rets
				DESTROY ds_memo
				DESTROY ds_sub_account
				DESTROY ds_pend_trans
				return -1
			end if
		
		next

	//**************************************
	//  End of Memo Retirement Units:
	//**************************************
 	
    //******************************************************
   // Equip Ledger Pend Equip:   Mark Yuan 3/20/2009
   // Any equipment associated with the unit items must also be retired
  // Skip processing if no records in CPR_EQUIP_LEDGER 02/21/2011
   //******************************************************

   if lb_has_equip_ledger then
		longlong related_rows
	related_rows = ds_cpr_retire_relate.RETRIEVE(unit_item_id, work_order_id)		
	if related_rows < 0 then 
		f_pp_msgs("ds_memo.retrieve() returned an error.")
		return -1
	end if 
//      sqle = 'select equip_id, quantity ' +&
//            ' from cpr_equip_retire_relate ' +&
//            ' where unit_item_id = ' +string(unit_item_id)  +&
//            ' and work_order_id = ' + string(work_order_id)
//      ds_cpr_retire_relate = create uo_ds_top
//      if f_create_dynamic_ds(ds_cpr_retire_relate, 'grid', sqle,sqlca,true) <> "OK" then
//         messagebox("Automatic Retirement Processing", &
//                 "Error retrieving cpr_retire_relate_equip datastore: " + &
//                  ds_pend_trans.i_sqlca_sqlerrtext, Exclamation!)
//         if i_process_id > 0 then
//          f_pp_msgs("Error retrieving cpr_retire_relate_equip datastore: " + &
//                     ds_pend_trans.i_sqlca_sqlerrtext)
//         end if
//         rollback;
//         DESTROY ds_rets
//         DESTROY ds_memo
//         DESTROY ds_sub_account
//         DESTROY ds_pend_trans
//         return -1
//      end if
   
      for k = 1 to ds_cpr_retire_relate.rowcount()
         e_current_row = ds_pend_equip.insertrow(0)
   
         ds_pend_equip.setitem(e_current_row, 'pend_trans_id', new_trans_id)
         ds_pend_equip.setitem(e_current_row, 'equip_id', ds_cpr_retire_relate.getitemnumber(k,'equip_id'))
         ds_pend_equip.setitem(e_current_row, 'quantity', ds_cpr_retire_relate.getitemnumber(k,'quantity'))
      next
   end if   
   //******************************************************
   // End Equip Ledger Pend Equip
   //******************************************************

//destroy ds_cpr_retire_relate

next

destroy ds_cpr_retire_relate

longlong rtn
rtn = ds_pend_trans.Update( )
 
If rtn <> 1 then
	f_pp_msgs("Error updating pending_transaction: " + &
				   ds_pend_trans.i_sqlca_sqlerrtext)
	rollback;
	string filedir
	filedir = f_get_temp_dir()
//	ds_pend_trans.SaveAs("c:\pp_failed_retirements.xls", Excel!, True)
	ds_pend_trans.SaveAs(filedir + "\pp_failed_retirements.xls", Excel!, True)
	messagebox("Automatic Retirement Processing", &
				  "The retirement transactions have been dumped to " + &
				  filedir + "\pp_failed_retirements.xls")
	DESTROY ds_rets
	DESTROY ds_memo
	DESTROY ds_sub_account
	DESTROY ds_pend_trans
   return -1
end if 

//update pend equip datastore
// Skip processing if no records in CPR_EQUIP_LEDGER 02/21/2011
if lb_has_equip_ledger then
   rtn = ds_pend_equip.Update( )
   If rtn <> 1 then
	f_pp_msgs("Error updating pend_equip: " + &
                  ds_pend_equip.i_sqlca_sqlerrtext)
      rollback;
      DESTROY ds_rets
      DESTROY ds_memo
      DESTROY ds_sub_account
      DESTROY ds_pend_trans
      return -1
   end if 
end if	


//maint 29376:  insert pend basis for retirements with asset_id = not null and accum_cost <> 0
insert into  pend_basis (pend_trans_id, 
basis_1, basis_2,basis_3, basis_4, basis_5, basis_6, basis_7, basis_8, basis_9, basis_10,
basis_11, basis_12, basis_13, basis_14,basis_15, basis_16, basis_17, basis_18, basis_19, basis_20,
basis_21, basis_22, basis_23, basis_24, basis_25, basis_26, basis_27, basis_28, basis_29, basis_30,
basis_31, basis_32, basis_33, basis_34,basis_35, basis_36, basis_37, basis_38, basis_39, basis_40,
basis_41, basis_42, basis_43, basis_44,basis_45, basis_46, basis_47, basis_48, basis_49, basis_50,
basis_51, basis_52, basis_53, basis_54,basis_55, basis_56, basis_57, basis_58, basis_59, basis_60,
basis_61, basis_62, basis_63, basis_64,basis_65, basis_66, basis_67, basis_68, basis_69, basis_70 ) 	
select pend_trans_id, 
 round(basis_1*(a.posting_amount/b.accum_cost),2), round(basis_2*(a.posting_amount/b.accum_cost),2), round(basis_3*(a.posting_amount/b.accum_cost),2), round(basis_4*(a.posting_amount/b.accum_cost),2), round(basis_5*(a.posting_amount/b.accum_cost),2), round(basis_6*(a.posting_amount/b.accum_cost),2), round(basis_7*(a.posting_amount/b.accum_cost),2), round(basis_8*(a.posting_amount/b.accum_cost),2), round(basis_9*(a.posting_amount/b.accum_cost),2), round(basis_10*(a.posting_amount/b.accum_cost),2),
round(basis_11*(a.posting_amount/b.accum_cost),2), round(basis_12*(a.posting_amount/b.accum_cost),2), round(basis_13*(a.posting_amount/b.accum_cost),2), round(basis_14*(a.posting_amount/b.accum_cost),2), round(basis_15*(a.posting_amount/b.accum_cost),2), round(basis_16*(a.posting_amount/b.accum_cost),2), round(basis_17*(a.posting_amount/b.accum_cost),2), round(basis_18*(a.posting_amount/b.accum_cost),2), round(basis_19*(a.posting_amount/b.accum_cost),2), round(basis_20*(a.posting_amount/b.accum_cost),2),
round(basis_21*(a.posting_amount/b.accum_cost),2), round(basis_22*(a.posting_amount/b.accum_cost),2), round(basis_23*(a.posting_amount/b.accum_cost),2), round(basis_24*(a.posting_amount/b.accum_cost),2), round(basis_25*(a.posting_amount/b.accum_cost),2), round(basis_26*(a.posting_amount/b.accum_cost),2), round(basis_27*(a.posting_amount/b.accum_cost),2), round(basis_28*(a.posting_amount/b.accum_cost),2), round(basis_29*(a.posting_amount/b.accum_cost),2), round(basis_30*(a.posting_amount/b.accum_cost),2),
round(basis_31*(a.posting_amount/b.accum_cost),2), round(basis_32*(a.posting_amount/b.accum_cost),2), round(basis_33*(a.posting_amount/b.accum_cost),2), round(basis_34*(a.posting_amount/b.accum_cost),2), round(basis_35*(a.posting_amount/b.accum_cost),2), round(basis_36*(a.posting_amount/b.accum_cost),2), round(basis_37*(a.posting_amount/b.accum_cost),2), round(basis_38*(a.posting_amount/b.accum_cost),2), round(basis_39*(a.posting_amount/b.accum_cost),2), round(basis_40*(a.posting_amount/b.accum_cost),2),
round(basis_41*(a.posting_amount/b.accum_cost),2), round(basis_42*(a.posting_amount/b.accum_cost),2), round(basis_43*(a.posting_amount/b.accum_cost),2), round(basis_44*(a.posting_amount/b.accum_cost),2), round(basis_45*(a.posting_amount/b.accum_cost),2), round(basis_46*(a.posting_amount/b.accum_cost),2), round(basis_47*(a.posting_amount/b.accum_cost),2), round(basis_48*(a.posting_amount/b.accum_cost),2), round(basis_49*(a.posting_amount/b.accum_cost),2), round(basis_50*(a.posting_amount/b.accum_cost),2),
round(basis_51*(a.posting_amount/b.accum_cost),2), round(basis_52*(a.posting_amount/b.accum_cost),2), round(basis_53*(a.posting_amount/b.accum_cost),2), round(basis_54*(a.posting_amount/b.accum_cost),2), round(basis_55*(a.posting_amount/b.accum_cost),2), round(basis_56*(a.posting_amount/b.accum_cost),2), round(basis_57*(a.posting_amount/b.accum_cost),2), round(basis_58*(a.posting_amount/b.accum_cost),2), round(basis_59*(a.posting_amount/b.accum_cost),2), round(basis_60*(a.posting_amount/b.accum_cost),2),
round(basis_61*(a.posting_amount/b.accum_cost),2), round(basis_62*(a.posting_amount/b.accum_cost),2), round(basis_63*(a.posting_amount/b.accum_cost),2), round(basis_64*(a.posting_amount/b.accum_cost),2), round(basis_65*(a.posting_amount/b.accum_cost),2), round(basis_66*(a.posting_amount/b.accum_cost),2), round(basis_67*(a.posting_amount/b.accum_cost),2), round(basis_68*(a.posting_amount/b.accum_cost),2), round(basis_69*(a.posting_amount/b.accum_cost),2), round(basis_70*(a.posting_amount/b.accum_cost),2)
from pend_transaction a, cpr_ledger b, cpr_ldg_basis c
where a.ldg_asset_id = b.asset_id
and b.asset_id = c.asset_id
and a.ferc_activity_code = 2
and a.gl_je_code = :gl_je_code
and a.company_id = :a_company_id
and a.gl_posting_mo_yr = :a_month
and a.posting_amount <> 0
and b.accum_cost <> 0 
and a.ldg_asset_id is not null
and not exists (select 1 from pend_basis d where a.pend_trans_id = d.pend_trans_id);

longlong nrows
nrows = sqlca.sqlnrows

if sqlca.SQLCODE <> 0 then 
	// keep track that there was an error
	l_all_success = -1
	f_pp_msgs("ERROR:  Unable to insert into pend_basis for wo retirements: " +  sqlca.SQLErrtext ) 
	rollback;  
end if 

//maint 29376 and 7911:  insert pend basis for retirements with asset_id = not null and (accum_cost = 0  or posting_amount = 0)
insert into  pend_basis (pend_trans_id, 
basis_1, basis_2,basis_3, basis_4, basis_5, basis_6, basis_7, basis_8, basis_9, basis_10,
basis_11, basis_12, basis_13, basis_14,basis_15, basis_16, basis_17, basis_18, basis_19, basis_20,
basis_21, basis_22, basis_23, basis_24, basis_25, basis_26, basis_27, basis_28, basis_29, basis_30,
basis_31, basis_32, basis_33, basis_34,basis_35, basis_36, basis_37, basis_38, basis_39, basis_40,
basis_41, basis_42, basis_43, basis_44,basis_45, basis_46, basis_47, basis_48, basis_49, basis_50,
basis_51, basis_52, basis_53, basis_54,basis_55, basis_56, basis_57, basis_58, basis_59, basis_60,
basis_61, basis_62, basis_63, basis_64,basis_65, basis_66, basis_67, basis_68, basis_69, basis_70 ) 	
select pend_trans_id, 
	basis_1*-1, basis_2*-1,basis_3*-1, basis_4*-1, basis_5*-1, basis_6*-1, basis_7*-1, basis_8*-1, basis_9*-1, basis_10*-1,
	basis_11*-1, basis_12*-1, basis_13*-1, basis_14*-1,basis_15*-1, basis_16*-1, basis_17*-1, basis_18*-1, basis_19*-1, basis_20*-1,
	basis_21*-1, basis_22*-1, basis_23*-1, basis_24*-1, basis_25*-1, basis_26*-1, basis_27*-1, basis_28*-1, basis_29*-1, basis_30*-1,
	basis_31*-1, basis_32*-1, basis_33*-1, basis_34*-1,basis_35*-1, basis_36*-1, basis_37*-1, basis_38*-1, basis_39*-1, basis_40*-1,
	basis_41*-1, basis_42*-1, basis_43*-1, basis_44*-1,basis_45*-1, basis_46*-1, basis_47*-1, basis_48*-1, basis_49*-1, basis_50*-1,
	basis_51*-1, basis_52*-1, basis_53*-1, basis_54*-1,basis_55*-1, basis_56*-1, basis_57*-1, basis_58*-1, basis_59*-1, basis_60*-1,
	basis_61*-1, basis_62*-1, basis_63*-1, basis_64*-1,basis_65*-1, basis_66*-1, basis_67*-1, basis_68*-1, basis_69*-1, basis_70*-1 
from pend_transaction a, cpr_ledger b, cpr_ldg_basis c
where a.ldg_asset_id = b.asset_id
and b.asset_id = c.asset_id
and a.ferc_activity_code = 2
and a.gl_je_code = :gl_je_code
and a.company_id = :a_company_id
and a.gl_posting_mo_yr = :a_month
and a.ldg_asset_id is not null
and (b.accum_cost = 0 or a.posting_amount = 0)
and not exists (select 1 from pend_basis d where a.pend_trans_id = d.pend_trans_id);

nrows = sqlca.sqlnrows

if sqlca.SQLCODE <> 0 then  
	// keep track that there was an error
	l_all_success = -1
	f_pp_msgs("ERROR:  Unable to insert into pend_basis for wo retirements (net 0): " +  sqlca.SQLErrtext ) 
	rollback;  
end if 

//plug rounding differences into basis_1 for set_of_books_id = 1.  the other set of books don't need a round plug, because the posting_amount is derived after basis amounts are determined.  
longlong num_buckets
select max(book_summary_id) into :num_buckets from book_summary;

string sqls
sqls = ''
sqls = 'update pend_basis c set c.basis_1 = nvl(c.basis_1,0) - (select sum( '
for k = 1 to num_buckets
	if k = 1 then
		sqls+= 'nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	else
		sqls+= ' + nvl(a.basis_' + string(k) + ',0) * nvl(b.basis_' + string(k) + '_indicator,0) '
	end if
next

sqls += ') - sum(d.posting_amount) '
sqls += ' from pend_basis a, set_of_books b, pend_transaction d '
sqls += ' where c.pend_trans_id =  a.pend_trans_id '
sqls += ' and a.pend_trans_id = d.pend_trans_id '
sqls += ' and d.ldg_asset_id is not null '
sqls += " and d.gl_je_code = '" + gl_je_code + "' "
sqls += ' and d.company_id = ' + string(a_company_id)
sqls += " and to_char(d.gl_posting_mo_yr, 'yyyymm') = " + string(year(a_month)*100+month(a_month))
sqls += ' and b.set_of_books_id = 1 '
sqls += ' ) ' 
sqls += ' where exists (select 1 from pend_transaction dd where dd.pend_trans_id = c.pend_trans_id ' 
sqls += ' and dd.ldg_asset_id is not null '
sqls += " and dd.gl_je_code = '" + gl_je_code + "' "
sqls += ' and dd.company_id = ' + string(a_company_id)
sqls += " and to_char(dd.gl_posting_mo_yr, 'yyyymm') = " + string(year(a_month)*100+month(a_month)) + ')'
	
execute immediate :sqls;

nrows = sqlca.sqlnrows

if sqlca.sqlcode <> 0 then  
	// keep track that there was an error
	l_all_success = -1
	f_pp_msgs("ERROR:  Unable to update pend_basis for rounding: " +  sqlca.SQLErrtext ) 
	rollback;  
end if

commit;

DESTROY ds_rets
DESTROY ds_memo
DESTROY ds_sub_account
DESTROY ds_pend_trans

return l_all_success
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

