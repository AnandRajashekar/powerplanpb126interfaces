HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_pp_interface_dates_check from datawindow within w_datawindows
end type
type dw_wo_process_afudc_ovh from datawindow within w_datawindows
end type
type dw_wo_interface_dates_all from datawindow within w_datawindows
end type
type dw_cpr_act_month from datawindow within w_datawindows
end type
type dw_wo_process_control from datawindow within w_datawindows
end type
type dw_sub_account_select from datawindow within w_datawindows
end type
type dw_pend_trans_update from datawindow within w_datawindows
end type
type dw_pend_equip from datawindow within w_datawindows
end type
type dw_cpr_retire_relate from datawindow within w_datawindows
end type
type dw_wo_unit_item_memo_retrieve from datawindow within w_datawindows
end type
type dw_auto101_retirements_manual_bkts from datawindow within w_datawindows
end type
type dw_auto101_retirements from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer width = 4754
integer height = 1980
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_pp_interface_dates_check dw_pp_interface_dates_check
dw_wo_process_afudc_ovh dw_wo_process_afudc_ovh
dw_wo_interface_dates_all dw_wo_interface_dates_all
dw_cpr_act_month dw_cpr_act_month
dw_wo_process_control dw_wo_process_control
dw_sub_account_select dw_sub_account_select
dw_pend_trans_update dw_pend_trans_update
dw_pend_equip dw_pend_equip
dw_cpr_retire_relate dw_cpr_retire_relate
dw_wo_unit_item_memo_retrieve dw_wo_unit_item_memo_retrieve
dw_auto101_retirements_manual_bkts dw_auto101_retirements_manual_bkts
dw_auto101_retirements dw_auto101_retirements
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_pp_interface_dates_check=create dw_pp_interface_dates_check
this.dw_wo_process_afudc_ovh=create dw_wo_process_afudc_ovh
this.dw_wo_interface_dates_all=create dw_wo_interface_dates_all
this.dw_cpr_act_month=create dw_cpr_act_month
this.dw_wo_process_control=create dw_wo_process_control
this.dw_sub_account_select=create dw_sub_account_select
this.dw_pend_trans_update=create dw_pend_trans_update
this.dw_pend_equip=create dw_pend_equip
this.dw_cpr_retire_relate=create dw_cpr_retire_relate
this.dw_wo_unit_item_memo_retrieve=create dw_wo_unit_item_memo_retrieve
this.dw_auto101_retirements_manual_bkts=create dw_auto101_retirements_manual_bkts
this.dw_auto101_retirements=create dw_auto101_retirements
this.Control[]={this.dw_pp_interface_dates_check,&
this.dw_wo_process_afudc_ovh,&
this.dw_wo_interface_dates_all,&
this.dw_cpr_act_month,&
this.dw_wo_process_control,&
this.dw_sub_account_select,&
this.dw_pend_trans_update,&
this.dw_pend_equip,&
this.dw_cpr_retire_relate,&
this.dw_wo_unit_item_memo_retrieve,&
this.dw_auto101_retirements_manual_bkts,&
this.dw_auto101_retirements}
end on

on w_datawindows.destroy
destroy(this.dw_pp_interface_dates_check)
destroy(this.dw_wo_process_afudc_ovh)
destroy(this.dw_wo_interface_dates_all)
destroy(this.dw_cpr_act_month)
destroy(this.dw_wo_process_control)
destroy(this.dw_sub_account_select)
destroy(this.dw_pend_trans_update)
destroy(this.dw_pend_equip)
destroy(this.dw_cpr_retire_relate)
destroy(this.dw_wo_unit_item_memo_retrieve)
destroy(this.dw_auto101_retirements_manual_bkts)
destroy(this.dw_auto101_retirements)
end on

type dw_pp_interface_dates_check from datawindow within w_datawindows
integer x = 3378
integer y = 1252
integer width = 686
integer height = 400
integer taborder = 100
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_afudc_ovh from datawindow within w_datawindows
integer x = 2633
integer y = 1260
integer width = 686
integer height = 400
integer taborder = 100
string title = "none"
string dataobject = "dw_wo_process_afudc_ovh"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_interface_dates_all from datawindow within w_datawindows
integer x = 1829
integer y = 1252
integer width = 686
integer height = 400
integer taborder = 90
string title = "none"
string dataobject = "dw_wo_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_act_month from datawindow within w_datawindows
integer x = 1001
integer y = 1240
integer width = 686
integer height = 400
integer taborder = 80
string title = "none"
string dataobject = "dw_cpr_act_month"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_process_control from datawindow within w_datawindows
integer x = 187
integer y = 1240
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_wo_process_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_sub_account_select from datawindow within w_datawindows
integer x = 1847
integer y = 720
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "dw_sub_account_select"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pend_trans_update from datawindow within w_datawindows
integer x = 974
integer y = 792
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_pend_trans_update"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pend_equip from datawindow within w_datawindows
integer x = 165
integer y = 760
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_pend_equip"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_retire_relate from datawindow within w_datawindows
integer x = 2761
integer y = 180
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_cpr_retire_relate"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_wo_unit_item_memo_retrieve from datawindow within w_datawindows
integer x = 1865
integer y = 152
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_wo_unit_item_memo_retrieve"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_auto101_retirements_manual_bkts from datawindow within w_datawindows
integer x = 987
integer y = 128
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_auto101_retirements_manual_bkts"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_auto101_retirements from datawindow within w_datawindows
integer x = 114
integer y = 84
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_auto101_retirements"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

