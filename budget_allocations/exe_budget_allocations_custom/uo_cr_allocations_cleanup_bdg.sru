HA$PBExportHeader$uo_cr_allocations_cleanup_bdg.sru
$PBExportComments$FECR.pbl - Budget Allocations ...
forward
global type uo_cr_allocations_cleanup_bdg from nonvisualobject
end type
end forward

global type uo_cr_allocations_cleanup_bdg from nonvisualobject
end type
global uo_cr_allocations_cleanup_bdg uo_cr_allocations_cleanup_bdg

forward prototypes
public function longlong uf_cleanup1 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[])
public function longlong uf_cleanup2 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[])
public function longlong uf_cleanup3 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[])
end prototypes

public function longlong uf_cleanup1 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[]);//******************************************************************************************
//
//  User Object Function  :  uf_cleanup1
//
//  Notes                 :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!
//
//******************************************************************************************
longlong allocation_id, month_number, month_period, counter, clearing_indicator, &
		 processing_priority, intercompany_accounting, i, pp_company_id
longlong tbl_id		 
string journal_name, pp_wo_fields, sqls, table_name, ferc_gl_jrnl_cat, gl_jcat, bwo
dec {2} amount
dec {8} bad_percent

datetime finished
integer  whole_month_int
date     ddate
time     ttime
string   s_date


//allocation_id = a_uo_alloc.i_allocation_id
month_number  = a_long_array_arg[2]
month_period  = a_long_array_arg[3]
//
//clearing_indicator      = 0
//processing_priority     = 0
//intercompany_accounting = 0
//select clearing_indicator, processing_priority, intercompany_accounting, gl_journal_category
//  into :clearing_indicator, :processing_priority, :intercompany_accounting, :gl_jcat
//  from cr_allocation_control_bdg where allocation_id = :allocation_id;
//if isnull(clearing_indicator)      then clearing_indicator      = 0
//if isnull(intercompany_accounting) then intercompany_accounting = 0
//if isnull(gl_jcat)                 then gl_jcat                 = ""
//
//
////******************************************************************************************
////	 SETUP:
////		Get the table name.
////******************************************************************************************
//if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	if upper(g_test_or_actual) = "TEST" then
//		table_name = "cr_budget_data_test"
//	else
//		table_name = "cr_budget_data"
//	end if
//else                                                          //  Other Allocations ...
//	if upper(g_test_or_actual) = "TEST" then
//		table_name = "cr_budget_data_test"
//	else
//		table_name = "cr_budget_data"
//	end if
//end if
//
//
////******************************************************************************************
////	 BILLINGS:
////		Delete the original targets since derivations are creating the real targets.
////
////    The "maximo_wo" clause is VERY important!  The MWO billings are a completely
////    separate allocation from the BWO Actual Billing allocations.  The MWO billing is
////    NOT using an Insert derivation type, but rather an Update derivation type.  So,
////    the original targets from the allocation are the correct billing records to the
////    OPCO.  Thus, we cannot delete them.
////******************************************************************************************
//
//if gl_jcat <> "BILLINGS" then goto after_billings
//
//sqls = "delete from " + table_name + " " + &
//		  "where target_credit = 'TARGET' and cr_derivation_rate is null " + &
//		    "and allocation_id = " + string(allocation_id) + " " + &
//			 "and month_number = " + string(month_number) + " " + &
//			 "and month_period = " + string(month_period) + " " + &
//			 "and budget_version = '" + g_budget_version + "' " + &
//			 "and maximo_wo = ' '"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: deleting original targets for billings !")
//	f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	f_pp_msgs("SQLS = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//after_billings:
//
//
//
//
//
//
//////******************************************************************************************
//////	 ALL CAPITAL:
//////		
//////		Need to change the removal ferc subs to include 4 nines due to CASPR:
//////				3XXXXXXX --> 3XX9999X
//////
//////		Do this only when the ferc sub starts with a '3', the last character of the ferc sub
//////		is between '1' and '7', and when the gl_journal_category is 'ALLOCCAPITAL'.  
//////		This applies to both TARGET and CREDIT records.
//////
//////    NOTE: The GL Journal Category determines whether or not the update fires.
//////
//////    DMJ: 9/11/09: IT SEEMED DUMB TO ONLY DO THIS FOR A CERTAIN GL_JOURNAL_CATEGORY
//////    SO I REMOVED THAT FROM THE WHERE CLAUSE.
//////
//////******************************************************************************************
////if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
////	
////	if upper(g_test_or_actual) = "TEST" then  //  Test ...
////		
////		if g_me_validations = "YES" or g_combo_validations = "YES" or &
////			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
////			g_custom_validations = "YES" &
////		then
////			table_name = 'cr_budget_data_test'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data_test
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		else
////			table_name = 'cr_budget_data_test'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data_test
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		end if
////		
////	else  											   //  Actual ...
////		
////		if g_me_validations = "YES" or g_combo_validations = "YES" or &
////			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
////			g_custom_validations = "YES" &
////		then
////			table_name = 'cr_budget_data'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		else
////			table_name = 'cr_budget_data'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		end if
////		
////	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
////	
////else
////	
////	if upper(g_test_or_actual) = "TEST" then  //  Test ...
////		
////		if g_me_validations = "YES" or g_combo_validations = "YES" or &
////			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
////			g_custom_validations = "YES" &
////		then
////			table_name = 'cr_budget_data_test'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data_test
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		else
////			table_name = 'cr_budget_data_test'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data_test
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		end if
////		
////	else  											   //  Actual ...
////		
////		if g_me_validations = "YES" or g_combo_validations = "YES" or &
////			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
////			g_custom_validations = "YES" &
////		then
////			table_name = 'cr_budget_data'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		else
////			table_name = 'cr_budget_data'
////			select min(gl_journal_category) into :ferc_gl_jrnl_cat
////			  from cr_budget_data
////			 where allocation_id = :allocation_id and budget_version = :g_budget_version
////			 	and rownum = 1;
////		end if
////	
////	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
////
////end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
////
////////if ferc_gl_jrnl_cat <> 'ALLOCCAPITAL' then
////////	goto after_capital_ferc_update
////////else
////	sqls = "update " + table_name  + " " + &
////			 "set ferc_sub = substr(ferc_sub,1,3)||'9999'||substr(ferc_sub,8,1), orig_ferc_sub = substr(ferc_sub,1,3)||'9999'||substr(ferc_sub,8,1) " + &
////			 "where allocation_id = " + string(allocation_id) + " " + &
////			 "and substr(ferc_sub,1,1) = '3' " + &
////			 "and substr(ferc_sub,8,1) between '1' and '7' " + &
////			 "and budget_version = '" + g_budget_version + "'"
////			 
////	execute immediate :sqls;		 
////			
////	if sqlca.SQLCode < 0 then
////		f_pp_msgs("  ")
////		f_pp_msgs("ERROR: Updating Capital Ferc Sub values to 3XX9999X !!!")
////		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
////		f_pp_msgs("  ")
////		return -1
////	end if
////////end if	//	if ferc_gl_jrnl_cat <> 'ALLOCCAPITAL' then ...
////
////
////////after_capital_ferc_update:
//
//
//
//
//
//
////******************************************************************************************
////  CAPITAL - E&S:
////
////    Get the PRCN and RORG for the targets from the work order header.
////
////    Two separate updates - PRCN comes from a class code and RORG comes from
////    the department_id.
////
////******************************************************************************************
//
////
////  RORG:  From department_id
////
//setnull(pp_wo_fields)
//select pp_wo_fields into :pp_wo_fields from cr_so_allocation_defaults_bdg 
// where allocation_id = :allocation_id and target_credit = 'TARGET'
//	and rrcn = 'dptid';
//if isnull(pp_wo_fields) then goto after_woc_dept_id_update
//
//setnull(pp_company_id)
//select c.powerplant_company_id into :pp_company_id
//  from cr_allocation_control a, cr_company c
// where a.allocation_id = :allocation_id
// 	and a.cr_company_id = c.cr_company_id;
//
//
////  If we get this far, the allocation is set up to get the RORG from the
////  department_id on work_order_control.
//if pp_wo_fields = "location||ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			else
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//			
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating RORG values from the work order header (location||ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "location||ewo" then ...
//
//
//if pp_wo_fields = "ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			else
//				
//				update cr_budget_data_test a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							),
//															orig_rorg = 
//							(
//							select substr(nvl(external_department_code,' '),1,5)
//							  from work_order_control b, company_setup c, department d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.department_id = d.department_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating RORG values from the work order header (ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "ewo" then
//
//
//after_woc_dept_id_update:
//
//
////
////  PRCN:  From class_code_id 36
////
//setnull(pp_wo_fields)
//select pp_wo_fields into :pp_wo_fields from cr_so_allocation_defaults_bdg 
// where allocation_id = :allocation_id and target_credit = 'TARGET'
//	and prcn = '36';
//if isnull(pp_wo_fields) then goto after_woc_36_update
//
//
////  If we get this far, the allocation is set up to get the PRCN from class_code_id 36
////  on work_order_class_code.
//if pp_wo_fields = "location||ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//				
//				
//			else
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//								
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//		
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating PRCN values from the work order header (location||ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "location||ewo" then
//
//
//if pp_wo_fields = "ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//					
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							),
//															orig_prcn = 
//							(
//							select substr(nvl(d."VALUE",' '),1,5)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 36
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 36 and bb.funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//		
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating PRCN values from the work order header (ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "ewo" then
//
//after_woc_36_update:
//
//
//
////
////  ACTIVITY:  From class_code_id 540
////
//setnull(pp_wo_fields)
//select pp_wo_fields into :pp_wo_fields from cr_so_allocation_defaults_bdg 
// where allocation_id = :allocation_id and target_credit = 'TARGET'
//	and activity = '540';
//if isnull(pp_wo_fields) then goto after_woc_540_update
//
//
////  If we get this far, the allocation is set up to get the ACTIVITY from class_code_id 540
////  on work_order_class_code.
//if pp_wo_fields = "location||ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//				
//				
//			else
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//								
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//		
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating ACTIVITY values from the work order header (location||ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "location||ewo" then
//
//
//if pp_wo_fields = "ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//					
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							),
//															orig_activity = 
//							(
//							select substr(nvl(d."VALUE",' '),1,7)
//							  from work_order_control b, company_setup c, work_order_class_code d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.work_order_id = d.work_order_id and d.class_code_id = 540
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control bb, work_order_class_code dd
//								  where bb.work_order_id = dd.work_order_id and dd.class_code_id = 540 and bb.funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//		
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating ACTIVITY values from the work order header (ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "ewo" then
//
//after_woc_540_update:
//
//
//
////
////  PROJECT:  From funding project
////
//setnull(pp_wo_fields)
//select pp_wo_fields into :pp_wo_fields from cr_so_allocation_defaults_bdg 
// where allocation_id = :allocation_id and target_credit = 'TARGET'
//	and project = 'fp';
//if isnull(pp_wo_fields) then goto after_woc_fp_update
//
//
////  If we get this far, the allocation is set up to get the PROJECT from the
////  funding project on work_order_control.
//if pp_wo_fields = "location||ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			else
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.location||a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.location||a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//			
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating PROJECT values from the work order header (location||ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "location||ewo" then ...
//
//
//if pp_wo_fields = "ewo" then
//	
//	if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//			
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//	else
//		
//		if upper(g_test_or_actual) = "TEST" then  //  Test ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			else
//				
//				update cr_budget_data_test a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//			
//			end if
//			
//		else  											   //  Actual ...
//			
//			if g_me_validations = "YES" or g_combo_validations = "YES" or &
//				g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//				g_custom_validations = "YES" &
//			then
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			else
//				
//				update cr_budget_data a set project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							),
//															orig_project = 
//							(
//							select substr(nvl(d.work_order_number,' '),1,6)
//							  from work_order_control b, company_setup c, work_order_control d
//							 where a.ewo = b.work_order_number and b.funding_wo_indicator = 0
//								and b.company_id = c.company_id and b.funding_wo_id = d.work_order_id
//								and c.gl_company_no = a.company
//							)
//				 where allocation_id = :allocation_id and target_credit = 'TARGET' and budget_version = :g_budget_version
//					and a.ewo in
//								(select work_order_number from work_order_control where funding_wo_indicator = 0);
//				
//			end if
//		
//		end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//	end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating PROJECT values from the work order header (ewo) !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		return -1
//	end if
//	
//end if  //  if pp_wo_fields = "ewo" then
//
//
//after_woc_fp_update:
//
//
//
////******************************************************************************************
////	 SETUP (FOR THE NEXT FEW FERC_SUB DERIVATIONS):
////		
////		Table names.
////
////******************************************************************************************
//if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//	
//	if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			table_name = 'cr_budget_data_test'
//		else
//			table_name = 'cr_budget_data_test'
//		end if
//		
//	else  											   //  Actual ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			table_name = 'cr_budget_data'
//		else
//			table_name = 'cr_budget_data'
//		end if
//		
//	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//	
//else
//	
//	if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			table_name = 'cr_budget_data_test'
//		else
//			table_name = 'cr_budget_data_test'
//		end if
//		
//	else  											   //  Actual ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//			g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			table_name = 'cr_budget_data'
//		else
//			table_name = 'cr_budget_data'
//		end if
//	
//	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//
//
////******************************************************************************************
////	 JO CREDITS (FERC_SUB):
////		
////		Need to change the Ferc Sub value based on the AI.  Each of the other owners are
////    booked to a separate 232 account.
////
////******************************************************************************************
//
//setnull(counter)
//select count(*) into :counter from cr_so_allocation_defaults_bdg
// where allocation_id = :allocation_id and target_credit = 'CREDIT' and rownum = 1;
//if isnull(counter) or counter = 0 then goto after_jo_credit_ferc_sub_update
//
////
////  DMJ: 6/25/09: There were some O&M allocations that needed to use this logic, so I completely
////  opened it up.
////
////if ferc_gl_jrnl_cat <> 'ALLOCCAPITAL' then
////	goto after_jo_credit_ferc_sub_update
////else
//	sqls = "update " + table_name  + " a set ferc_sub = (" + &
//			 		"select ferc_sub from cr_so_allocation_defaults_bdg b " + &
//			 		 "where b.allocation_id = " + string(allocation_id) + " and b.target_credit = 'CREDIT' " + &
//			 			"and a.allocation_id = b.allocation_id and a.ai = b.ai), " + &
//						 								 "orig_ferc_sub = (" + &
//			 		"select ferc_sub from cr_so_allocation_defaults_bdg b " + &
//			 		 "where b.allocation_id = " + string(allocation_id) + " and b.target_credit = 'CREDIT' " + &
//			 			"and a.allocation_id = b.allocation_id and a.ai = b.ai) " + &
//			  "where allocation_id = " + string(allocation_id) + " and target_credit = 'CREDIT' and budget_version = '" + g_budget_version + "' " + &
//			    "and ai in (select ai from cr_so_allocation_defaults_bdg " + &
//				 				 "where allocation_id = " + string(allocation_id) + " and target_credit = 'CREDIT')"
//			 
//	execute immediate :sqls;		 
//			
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: Updating JO Ferc Sub Credit values !!!")
//		f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//		f_pp_msgs("SQLS = " + sqls)
//		f_pp_msgs("  ")
//		return -1
//	end if
////end if	//	if ferc_gl_jrnl_cat <> 'ALLOCCAPITAL' then ...
//
//
//after_jo_credit_ferc_sub_update:
//
//
////******************************************************************************************
////	 JO CREDITS (EWO):
////		
////		Need to change the EWO value based on the AI.  For Southern Power O&M.
////
////******************************************************************************************
//
//setnull(counter)
//select count(*) into :counter from cr_so_allocation_defaults_bdg
// where allocation_id = :allocation_id and target_credit = 'CREDIT' and trim(ewo) is not null and rownum = 1;
//if isnull(counter) or counter = 0 then goto after_jo_credit_ewo_update
//
//sqls = "update " + table_name  + " a set ewo = (" + &
//				"select ewo from cr_so_allocation_defaults_bdg b " + &
//				 "where b.allocation_id = " + string(allocation_id) + " and b.target_credit = 'CREDIT' " + &
//					"and a.allocation_id = b.allocation_id and a.ai = b.ai), " + &
//													 "orig_ewo = (" + &
//				"select ewo from cr_so_allocation_defaults_bdg b " + &
//				 "where b.allocation_id = " + string(allocation_id) + " and b.target_credit = 'CREDIT' " + &
//					"and a.allocation_id = b.allocation_id and a.ai = b.ai) " + &
//		  "where allocation_id = " + string(allocation_id) + " and target_credit = 'CREDIT' and budget_version = '" + g_budget_version + "' " + &
//			 "and ai in (select ai from cr_so_allocation_defaults_bdg " + &
//							 "where allocation_id = " + string(allocation_id) + " and target_credit = 'CREDIT')"
//		 
//execute immediate :sqls;		 
//		
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Updating JO EWO Credit values !!!")
//	f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//	f_pp_msgs("SQLS = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//
//after_jo_credit_ewo_update:
//
//
////******************************************************************************************
////	 JO TARGETS (FERC_SUB):
////		
////		Need to change the Ferc Sub value based on the AI.  Each of the other owners are
////    booked to a separate 232 account.
////
////******************************************************************************************
//
//setnull(counter)
//select count(*) into :counter from cr_so_allocation_defaults_bdg
// where allocation_id = :allocation_id and target_credit = 'TARGET' and rownum = 1;
//if isnull(counter) or counter = 0 then goto after_jo_target_ferc_sub_update
//
//sqls = "update " + table_name  + " a set ferc_sub = (" + &
//				"select ferc_sub from cr_so_allocation_defaults_bdg b " + &
//				 "where b.allocation_id = " + string(allocation_id) + " and b.target_credit = 'TARGET' " + &
//					"and a.allocation_id = b.allocation_id and a.ai = b.ai), " + &
//													 "orig_ferc_sub = (" + &
//				"select ferc_sub from cr_so_allocation_defaults_bdg b " + &
//				 "where b.allocation_id = " + string(allocation_id) + " and b.target_credit = 'TARGET' " + &
//					"and a.allocation_id = b.allocation_id and a.ai = b.ai) " + &
//		  "where allocation_id = " + string(allocation_id) + " and target_credit = 'TARGET' and budget_version = '" + g_budget_version + "' " + &
//			 "and ai in (select ai from cr_so_allocation_defaults_bdg " + &
//							 "where allocation_id = " + string(allocation_id) + " and target_credit = 'TARGET')"
//		 
//execute immediate :sqls;		 
//		
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Updating JO Ferc Sub Target values !!!")
//	f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//	f_pp_msgs("SQLS = " + sqls)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//
//after_jo_target_ferc_sub_update:
//
//
//
//
//// -----------------  I WANT THESE JOURNAL_NAME EXTENSIONS AT THE BOTTOM  ------------------
//// -----------------  THEY WOULD NEED TO OVERRIDE ANYTHING THAT COULD     ------------------
//// -----------------  OCCUR ABOVE (SINCE THE LAST STEP HERE IS BALANCING  ------------------
//
//
//
//
////******************************************************************************************
////  INTERCOMPANY JOURNAL_NAME:
////
////    The journal_name at Southern is essentially a balancing field that is in the ACK.
////    Traditional balancing logic violates this principle since the journal_name is
////    obtained from cr_alloc_interco_criteria, and is thus hard-coded for every payable
////    and receivable, regardless of the type of transaction that generated the "journal".
////    Extension code in uo_cr_interco_balancing is handling areas like journal entries
////    and/or interfaces, but the allocations have their own balancing code due to the
////    cross_charge_company concept.
////
////    This extension will select the first journal_name value found on the targets and
////    update all INT_CO transactions with that value (where target_credit = 'INT_CO').
////
////    This must fire for all intercompany allocations.
////
////******************************************************************************************
//if intercompany_accounting = 1 then
//	//  Continue on ...
//else
//	goto after_int_co_journal_name
//end if
//
//setnull(journal_name)
//
//if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//
//	if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			select journal_name into :journal_name from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data_test set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		else
//			
//			select journal_name into :journal_name from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data_test set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		end if
//		
//	else  											   //  Actual ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			select journal_name into :journal_name from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		else
//			
//			select journal_name into :journal_name from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		end if
//		
//	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//else
//
//	if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			select journal_name into :journal_name from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data_test set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		else
//			
//			select journal_name into :journal_name from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data_test set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		end if
//		
//	else  											   //  Actual ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			select journal_name into :journal_name from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		else
//			
//			select journal_name into :journal_name from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'TARGET' and rownum = 1;
//			if isnull(journal_name) then journal_name = " "
//			
//			update cr_budget_data set journal_name = :journal_name
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//				and target_credit = 'INT_CO';
//			
//		end if
//		
//	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: Updating intercopmany Journal Name values !!!")
//	f_pp_msgs("ERROR_MSG: " + sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	return -1
//end if
//
//after_int_co_journal_name:
//
//
//
////******************************************************************************************
////  BALANCE BY JOURNAL_NAME:
////
////    The base code already has appropriate balancing on fields like company, MN, et.
////    Southern added a field to the ACK that is also a critical balancing field from the
////    GL's perspective called journal_name.
////
////    This must fire for all allocations.
////
////******************************************************************************************
//if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//
//	if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		else
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		end if
//		
//	else  											   //  Actual ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		else
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		end if
//		
//	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//else
//
//	if upper(g_test_or_actual) = "TEST" then  //  Test ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		else
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data_test
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		end if
//		
//	else  											   //  Actual ...
//		
//		if g_me_validations = "YES" or g_combo_validations = "YES" or &
//		   g_proj_based_validations = "YES" or g_mn_validations = "YES" or &
//			g_custom_validations = "YES" &
//		then
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		else
//			
//			counter = 0
//			select count(*) into :counter from (
//			select journal_name, sum(amount) from cr_budget_data
//			 where allocation_id = :allocation_id and budget_version = :g_budget_version
//				and month_number  = :month_number
//				and month_period  = :month_period
//			 group by journal_name having sum(amount) <> 0);
//			if isnull(counter) then counter = 0
//			
//		end if
//		
//	end if  //  if upper(g_test_or_actual) = "TEST" then  //  Test ...
//
//end if  //  if clearing_indicator = 4 or clearing_indicator = 5 then  //  Inter-Company ...
//
//if counter > 0 then
//	//  Need to save the records or it may be impossible to determine the cause of
//	//  the problem.  Cannot just commit since with suspense accounting, the stg tables
//	//  are temp tables.  Create a table here, and the DBA will have to aid in retrieving
//	//  the records.
//	//
//	//  Need to then delete the allocation results since they will be committed when the
//	//  create table fires !!!
//	tbl_id = 0
//	select crmaint.nextval into :tbl_id from dual;
//	if isnull(tbl_id) then tbl_id = 0
//	sqls = "create table crallocbdgoob_" + string(tbl_id) + " as "  + &
//			 "select * from cr_budget_data where allocation_id = " + string(allocation_id) + &
//			   " and budget_version = '" + g_budget_version + "'" // I'll just get everything for this
//					//  allocation_id and version in case I'm curious about prior months.
//	execute immediate :sqls;
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: This allocation is out of balance by Journal Name !!!")
//	f_pp_msgs("  ")
//	delete from cr_budget_data where allocation_id = :allocation_id and budget_version = :g_budget_version;
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: deleting comitted records from cr_budget_data: " + sqlca.SQLErrText)
//		f_pp_msgs("  ")
//		rollback;
//	end if
//	if sqlca.SQLCode = 100 then
//		f_pp_msgs("  ")
//		f_pp_msgs("ERROR: could not find any comitted records to delete in cr_budget_data !!!")
//		f_pp_msgs("  ")
//		rollback;
//	end if
//	commit;
//	return -1
//end if
//
//
////******************************************************************************************
////	 BILLINGS:  CUSTOM CHECK TO ENSURE THAT 100% WAS ALLOCATED BASED ON CDC PERCENTS
////		
////    SCS wants to make sure that they always have 100% entered in the CDC records.
////    Note that this is NOT a check to make sure 100% gets billed (our rounding function
////    actually ensures that).  Instead, they want to make sure the original percents were
////    right.  This SQL accomplishes that.  Note also: there is an alert doing the same
////    thing that they are supposed to run prior to running the billings.  Thus, I'm putting
////    this code here instead of way up-front in the allocations so there is some pain if
////    they get sloppy and quit running the alert.
////
////    A local datastore should be OK since there are very few billings and we should not
////    hit any memory issues.
////    
////******************************************************************************************
//if gl_jcat <> "BILLINGS" or processing_priority = 622 or processing_priority = 626 or processing_priority = 950 &
//	or processing_priority = 19560 &
//	or processing_priority = 49560 or processing_priority = 59560 or processing_priority = 59561 &
//	or processing_priority = 69560 &
//	or processing_priority = 95640 or processing_priority = 95643 &
//	or processing_priority = 96642 or processing_priority = 96643 or processing_priority = 96644 &
//	or processing_priority = 97642 or processing_priority = 97643 or processing_priority = 97644 &
//	or processing_priority = 98642 or processing_priority = 98643 or processing_priority = 98644 &
//	                               or processing_priority = 99643 &
//then
//	goto after_billings_percent_check
//end if
//
//datastore ds_bill
//ds_bill = CREATE datastore
//
//sqls = "select orig_bwo, sum(cr_derivation_rate) from " + table_name + " " + &
//		 " where allocation_id = " + string(allocation_id) + " " + &
//		 	 "and budget_version = '" + g_budget_version + "' " + &
//		 	 "and target_credit = 'TARGET' " + &
//		 	 "and month_number = " + string(month_number) + " " + &
//		 	 "and month_period = " + string(month_period) + " " + &
//		 " group by orig_bwo having sum(cr_derivation_rate) <> 1"
//
//f_create_dynamic_ds(ds_bill, "grid", sqls, sqlca, true)
//
//if ds_bill.RowCount() > 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: The following BWO's had derivation percentages that did not sum to 1.00 !")
//	f_pp_msgs("  ")
//	for i = 1 to ds_bill.RowCount()
//		bwo         = ds_bill.GetItemString(i, 1)
//		bad_percent = ds_bill.GetItemNumber(i, 2)
//		f_pp_msgs("BWO = " + bwo + ", percent = " + string(bad_percent))
//	next
//	f_pp_msgs("  ")
//	
//   if a_uo_alloc.i_whole_month then
//      whole_month_int = 1
//   else
//      whole_month_int = 0
//   end if
//   
//   s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
//   ddate  = date(left(s_date, 10))
//   ttime  = time(right(s_date, 8))
//   finished = datetime(ddate, ttime)
//      
//   if a_uo_alloc.i_run_as_a_test then  //  TESTing an allocation ...
//      insert into cr_alloc_process_control_bdg
//         (allocation_id, month_number, month_period, finished, 
//          source_sql, test_or_actual, whole_month, budget_version)
//      values
//         (:a_uo_alloc.i_allocation_id, :a_uo_alloc.i_month_number, :a_uo_alloc.i_month_period, :finished, 
//          :a_uo_alloc.i_source_sqls, 'TEST', :whole_month_int, :a_uo_alloc.i_budget_version);
//   else                     //  ACTUAL allocation being run ...
//      insert into cr_alloc_process_control_bdg
//         (allocation_id, month_number, month_period, finished, 
//          source_sql, test_or_actual, whole_month, budget_version)
//      values
//         (:a_uo_alloc.i_allocation_id, :a_uo_alloc.i_month_number, :a_uo_alloc.i_month_period, :finished, 
//          :a_uo_alloc.i_source_sqls, 'ACTUAL', :whole_month_int, :a_uo_alloc.i_budget_version);
//   end if
//   if sqlca.SQLCode < 0 then
//      f_pp_msgs("  ")
//      f_pp_msgs("Error inserting cr_alloc_process_control_bdg " + &
//                  sqlca.SQLErrText)
//      f_pp_msgs("  ")            
//      rollback;
//   end if
//   
//   // SEK 052110: Don't want to commit until the entry is in cr_alloc_process_control,
//   //      otherwise the results for this month can't be deleted.
//   commit; // I want the records saved in cr_budget_data if this occurs so we can audit !
//	
//	return -1
//end if
//
//after_billings_percent_check:
//
//
return 1
end function

public function longlong uf_cleanup2 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[]);
return 1
end function

public function longlong uf_cleanup3 (uo_client_interface a_uo_alloc, longlong a_long_array_arg[]);
return 1
end function

on uo_cr_allocations_cleanup_bdg.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cr_allocations_cleanup_bdg.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

