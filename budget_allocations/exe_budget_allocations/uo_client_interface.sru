HA$PBExportHeader$uo_client_interface.sru
$PBExportComments$v10.3 MR 7093; FECR.pbl - Budget Allocations ...
forward
global type uo_client_interface from nonvisualobject
end type
end forward

global type uo_client_interface from nonvisualobject
end type
global uo_client_interface uo_client_interface

type variables
longlong i_allocation_id
longlong i_month_number
longlong i_month_period
longlong i_clearing_indicator, i_apply_wo_types
longlong i_cr_company_id
longlong i_clearing_mn[]
longlong i_clearing_mp[]
string            i_clearing_periods_wc
string            i_source_sqls, i_balance_sqls, i_grouping_sqls
string            i_wo_sqls
string            i_budget_version
boolean      i_run_as_a_test
boolean      i_run_all
boolean      i_whole_month
boolean      i_msgbox
string       i_interco_trigger_col
string       i_original_interco_credit_sql
longlong i_ytd_indicator
string		 i_staging_table, i_department_field, i_gl_journal_category, i_insert_table, &
				 i_prefixed_where
longlong i_elim_flag, i_derivation_flag, i_allocate_quantity, i_num_elements, i_target_id, &
				 i_interco_acctg, i_auto_reverse_cross_years, i_exclude_negatives, i_credit_id, &
				 i_ignore_header_co

uo_cr_derivation i_uo_cr_derivation
uo_ds_top i_cr_deriver_type_sources, i_ds_oob, i_cr_deriver_type_sources_alloc, &
			 i_cr_deriver_override2, i_cr_deriver_override2_alloc, i_ds_elements

// JAK: Dec 17, 2007:  Change all datastores to instances and remove the destroys
uo_ds_top 	i_ds_cr_alloc_months,i_ds_where_clause,i_ds_balance,i_ds_1,i_ds_2,i_ds_credit_co, &
	i_ds_source,i_ds_credit,i_ds_credit_insert,i_ds_target,i_ds_target_val,i_ds_interco,i_ds_interco_insert, &
	i_ds_group_by,i_ds_alloc_list,i_ds_company,i_ds_target_trpo,i_ds_credit_trpo

uo_cr_allocations_cleanup_bdg	i_uo_cleanup_bdg

longlong i_session_id

string i_suspense_accounting, i_concurrent_processing, i_priority, i_priority2
boolean i_suspense_processed
string i_closings_where_clause, i_afudc_calc_on_negative_base, i_use_new_structures_table, i_company_field, &
	i_run_by_dept_credits_method, i_wo_field_for_uf_cap, i_ifb_id, i_balance_where_clause, i_test_or_actual, &
	i_me_validations, i_combo_validations, i_proj_based_validations, i_custom_validations, i_mn_validations, &
	i_allow_priority_range
longlong i_budget_afudc_transfers, i_start_month, i_where_source_id, i_actuals_month, i_start_year, i_end_month, &
	i_max_late_chg_wait_period
boolean i_rate_field_exists, i_ifb_field_exists, i_validation_kickouts
uo_ds_top i_ds_sum_amount, i_ds_sum_amount2
uo_cr_validation_bdg i_uo_cr_validation_bdg

// ### 42972: JAK: 2015-02-17:  Updated exe name to match compiled name.  Allows for debugging
string i_exe_name = 'budget_allocations.exe'

// ### 8142: JAK: 2011-11-01:  Consistent return codes
boolean i_errors = false

/*### - MDZ - 29485 - 20130307*/
/*Add instance variable for start month of the fiscal year*/
longlong i_fy_start_month

end variables

forward prototypes
public function longlong uf_start_allocations ()
public function string uf_save_report_sql (string a_allocation_sqls, boolean a_custom_dw)
public function integer uf_update_cr (longlong a_month_number, longlong a_month_period)
public subroutine uf_msg (string a_msg_header, string a_msg)
public function decimal uf_lookup_rate (longlong a_rate_id, string a_company_value, longlong a_month_number)
public function integer uf_clearing ()
public subroutine uf_capital_work_orders (longlong a_clearing_id)
public function longlong uf_parse (string a_stringtoparse, string a_separator, ref string a_array_to_hold[])
public function integer uf_clearing_get_periods ()
public function string uf_build_sqls (string a_type)
public function longlong uf_afudc ()
public function integer uf_validate (longlong a_group_by_id, longlong a_target_id, longlong a_credit_id)
public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field)
public function integer uf_loading1 (longlong a_month_number, longlong a_month_period)
public function decimal uf_clearing_balance (longlong a_cr_company_id, string a_company_field)
public function decimal uf_clearing_balance_xcc (longlong a_cr_company_id, string a_company_field, longlong a_allocation_id, longlong a_month_number, string a_charging_cost_center, string a_dept_field)
public function longlong uf_auto_reverse ()
public function decimal uf_sources_and_targets_xcc (uo_ds_top a_ds_source, longlong a_group_by_id, string a_descr, longlong a_month_number, longlong a_month_period, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, longlong a_cr_company_id, string a_company_field, boolean a_custom_dw, string a_charging_cost_center, string a_dept_field)
public function longlong uf_run_by_department (longlong a_alloc_id, longlong a_mn, longlong a_mp, longlong a_clear_ind, longlong a_cr_co_id, string a_co_field, string a_gl_jcat, uo_ds_top a_ds_source, longlong a_group_by_id, string a_descr, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, boolean a_custom_dw)
public function string uf_rates_by_structure_element (longlong a_structure_id)
public function longlong uf_rates_by_structure_structure_id (longlong a_rate_id)
public function decimal uf_rates_by_structure_rate (longlong a_structure_id, string a_v_for_rates)
public function longlong uf_run_by_department_lookup_field ()
public function longlong uf_derivations ()
public function string uf_save_report_sql_balance (string a_allocation_sqls, boolean a_custom_dw)
public function decimal uf_clearing_balance_qty (longlong a_cr_company_id, string a_company_field)
public function decimal uf_clearing_balance_xcc_qty (longlong a_cr_company_id, string a_company_field, longlong a_allocation_id, longlong a_month_number, string a_charging_cost_center, string a_dept_field)
public function longlong uf_validations ()
public function string uf_save_report_sql_grouping (string a_allocation_sqls, boolean a_custom_dw)
public function longlong uf_fast_targets (decimal a_rate_for_insert, longlong a_source_id)
public function longlong uf_fast_credits (decimal a_rate_for_insert, longlong a_source_id)
public function integer uf_read ()
public function longlong uf_unlock (longlong a_allocation_id)
public function longlong uf_budget_afudc_defaults ()
public function longlong uf_run ()
public function string uf_getcustomversion (string a_pbd_name)
end prototypes

public function longlong uf_start_allocations ();//******************************************************************************************
//
//  User Object Function:  uf_start_allocations
//
//
//
//******************************************************************************************
integer  rtn, whole_month_int
string   sqls, description, co_sqls, s_date
longlong num_allocs, i, j, dw_id, month_number_check, lookup_month_number,&
         num_cos, c, temp_company_id
datetime finished, msg_dt
date     ddate
time     ttime

if i_whole_month then i_month_period = 0

//*****************************************************************************
//  Get a list of the allocation types, sorted by priority.
//  Also retrieve the cr_alloc_process_control_bdg table to determine which 
//  months and periods have not had the allocations ...
//*****************************************************************************
//datastore i_ds_alloc_list
//i_ds_alloc_list = i_ds_alloc_list datastore
i_ds_alloc_list.reset()
i_ds_alloc_list.DataObject = "dw_temp_dynamic"
i_ds_alloc_list.SetTransObject(sqlca)

//  The caller must set the i_run_all variable ... TRUE = run all allocations ...
//  FALSE = run only the allocation that the caller set in i_allocaiton_id.
if i_run_all then
	sqls = "select * from cr_allocation_control_bdg " + &
	       "order by processing_priority, description";
else
	sqls = "select * from cr_allocation_control_bdg where allocation_id = " + &
			 string(i_allocation_id) + " order by processing_priority, description";
end if

f_create_dynamic_ds(i_ds_alloc_list, "grid", sqls, sqlca, false)

i_ds_alloc_list.SetTransObject(sqlca)
num_allocs = i_ds_alloc_list.RETRIEVE()

for i = 1 to num_allocs
	
	i_allocation_id      = i_ds_alloc_list.GetItemNumber(i, "allocation_id")
	i_clearing_indicator = i_ds_alloc_list.GetItemNumber(i, "clearing_indicator")
	description          = i_ds_alloc_list.GetItemString(i, "description")
	dw_id                = i_ds_alloc_list.GetItemNumber(i, "dw_id")
	i_cr_company_id      = i_ds_alloc_list.GetItemNumber(i, "cr_company_id")
	i_apply_wo_types     = i_ds_alloc_list.GetItemNumber(i, "apply_wo_types")
	
	if isnull(i_apply_wo_types) then i_apply_wo_types = 0
	
	//
	//  i_month_number and i_month_period must be set by the caller ...
	//
	
	//  Insert the next month_number into cr_month_number_bdg, if it does not exist ...
	if right(string(i_month_number), 2) = "12" then
		lookup_month_number = i_month_number + 89
	else
		lookup_month_number = i_month_number + 1
	end if
	
	month_number_check = 0
	select month_number into :month_number_check
	  from cr_month_number_bdg where month_number = :lookup_month_number;
	
	if isnull(month_number_check) or month_number_check = 0 then
		insert into cr_month_number_bdg (month_number) values (:lookup_month_number)
		 using sqlca;
		if sqlca.SQLCode = 0 then
			commit using sqlca;
		else
			rollback using sqlca;
		end if
	end if
	
//	uo_ds_top i_ds_company
//	i_ds_company = CREATE uo_ds_top
	i_ds_company.reset()
	
	if i_cr_company_id = 10000 then
		//  Run the allocation for all companies ... this gives them the ability to 
		//  set up and allocation once that would run for all companies, but keep the
		//  pots and calculated rates whole by company.
		co_sqls = "select * from cr_company where owned = 1 and cr_company_id < 10000" + &
		          " order by external_company_id"
	else
		//  Run the allocation for the company the user specified ... or if <none>,
		//  the allocation gets the pot and calculated clearing rates for all companies,
		//  but does not keep them whole by company.
		co_sqls = "select * from cr_company where cr_company_id = " + &
		           string(i_cr_company_id) + " order by external_company_id"
	end if
	
	f_create_dynamic_ds(i_ds_company, "grid", co_sqls, sqlca, true)
	
	num_cos = i_ds_company.RowCount()
	
	if isnull(i_cr_company_id) then
		if not i_msgbox then
			s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
			ddate  = date(left(s_date, 10))
			ttime  = time(right(s_date, 8))
			msg_dt = datetime(ddate, ttime)
			//THIS MESSAGE IS IN W_BUDGET_ALLOCATIONS.WF_RUN
//			uf_msg("", "*******************************************")
//			uf_msg("", "Started: " + description + " for month: " + string(i_month_number) + &
//			           " at " + string(msg_dt))
		end if
		choose case i_clearing_indicator
			case 1
				rtn = uf_loading1(i_month_number, i_month_period)
			case 2
				rtn = uf_loading1(i_month_number, i_month_period)
			case 3
				rtn = uf_loading1(i_month_number, i_month_period)
			case 4
				rtn = uf_loading1(i_month_number, i_month_period)
			case 5
				rtn = uf_loading1(i_month_number, i_month_period)
			case 6
				rtn = uf_loading1(i_month_number, i_month_period)
			case else
				uf_msg("HEY!!!", "Set up the code in uf_start_allocations")
				rtn = 0
		end choose
		
		//  The commit, rollback, and return command are after this 
		//  if-then-else block ...
		
	else
		if not i_msgbox then
			s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
			ddate  = date(left(s_date, 10))
			ttime  = time(right(s_date, 8))
			msg_dt = datetime(ddate, ttime)
			//THIS MESSAGE IS IN W_BUDGET_ALLOCATIONS.WF_RUN
//			uf_msg("", "*******************************************")
//			uf_msg("", "Started: " + description + " at " + string(msg_dt))
		end if
		for c = 1 to num_cos
			temp_company_id = i_ds_company.GetItemNumber(c, "cr_company_id")
			update cr_allocation_control_bdg set cr_company_id = :temp_company_id
			 where allocation_id = :i_allocation_id using sqlca;
			if sqlca.SQLCode < 0 then
				uf_msg("Allocation - " + string(i_allocation_id), &
				           "Error updating the cr_company_id in uf_start_allocations: " + &
							   sqlca.SQLErrText)
				rollback using sqlca;
//				DESTROY ds_cr_alloc_months
//				DESTROY ds_alloc_list
//				DESTROY ds_company
				return -1
			end if

			//  Temporarily replace the cr_company_id on the allocation so uf_loading1
			//  will run the allocation for just that company.
			choose case i_clearing_indicator
				case 1
					rtn = uf_loading1(i_month_number, i_month_period)
				case 2
					rtn = uf_loading1(i_month_number, i_month_period)
				case 3
					rtn = uf_loading1(i_month_number, i_month_period)
				case 4
					rtn = uf_loading1(i_month_number, i_month_period)
				case 5
					rtn = uf_loading1(i_month_number, i_month_period)
				case 6
					rtn = uf_loading1(i_month_number, i_month_period)
				case else
					uf_msg("HEY!!!", "Set up the code in uf_start_allocations")
					rtn = 0
			end choose
		
			//  If the rtn code is < 0 then a bad error happened ... rollback the 
			//  transactions and exit out ... 0 is OK since that just means neutral
			//  things like no dollars in the pot ... 1 means the allocation ran
			//  successfully.
			if rtn < 0 then
				rollback using sqlca;
//				DESTROY ds_cr_alloc_months
//				DESTROY ds_alloc_list
//				DESTROY ds_company
				return rtn
			else
				if rtn = 1 then
					//  Must commit here if we are looping through companies.  Otherwise,
					//  the last company might return a 0 and the code below will
					//  perform a rollback!
					commit;
				end if
			end if
		
		next  //  for c = 1 to num_cos ...
	end if  //  if isnull(i_cr_company_id) then ...


//
// NOT FOR BUDGET ALLOCATIONS ...
//
// 1/21/09: Sort of ... there is no uf_update_cr(), but if we don't fire the rtn = 1 part,
// then cr_alloc_process_control_bdg will not have records inserted below.
//
//	//  Must update the cr_cost_xrepository table here, since it does not occur
//	//  for cr_company_id = 10000 in uf_loading ...
//	if not i_run_as_a_test then
//		if i_cr_company_id = 10000 then
//			w_top_frame.SetMicroHelp("Updating cr_cost_xrepository ...")
//			rtn = uf_update_cr(i_month_number, i_month_period)
//		end if
//	end if

	//  Must update the cr_cost_repository table here, since it does not occur
	//  for cr_company_id = 10000 in uf_loading ...
	if not i_run_as_a_test then
		if i_cr_company_id = 10000 then
			//uf_msg("","Updating cr_cost_repository ...")
			//rtn = uf_update_cr(i_month_number, i_month_period)
		else
			if rtn = 0 then rtn = 1
		end if
	else
		//  If they are running an allocation for company 10000 "ALL COMPANIES",
		//  the last company in the loop may return no rows ... this is OK and
		//  the rtn variable should be set to 1 so the transactions will be 
		//  committed below.
		if rtn = 0 then rtn = 1
	end if

	
	//*************************************************************
	//  The commit and rollback are managed here, not in any
	//  other function ...
	//*************************************************************
	if rtn = 1 then
		commit using sqlca;
	else
		if rtn < 0 then
			//  A return code of 0 is not an error ... it just means
			//  there was no balance to clear ...
			rollback using sqlca;
//			DESTROY ds_cr_alloc_months
//			DESTROY ds_alloc_list
//			DESTROY ds_company
			return rtn
		end if
	end if

	
	//  Change the cr_company_id back to its original value ...
	update cr_allocation_control_bdg set cr_company_id = :i_cr_company_id
	 where allocation_id = :i_allocation_id using sqlca;
	if sqlca.SQLCode < 0 then
		uf_msg("Allocation - " + string(i_allocation_id), &
		           "Error setting the cr_company_id back to its original value " + &
					  "in uf_start_allocations " + &
					   sqlca.SQLErrText)
		rollback using sqlca;
//		DESTROY ds_cr_alloc_months
//		DESTROY ds_alloc_list
//		DESTROY ds_company
		return -1
	end if
	

	s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
	ddate  = date(left(s_date, 10))
	ttime  = time(right(s_date, 8))
	msg_dt = datetime(ddate, ttime)
	g_finished_at = msg_dt
	
	//  Update cr_alloc_process_control_bdg ...  always for cr_company_id = 10000 since
	//  there may be results for companies that run successfully ...
	if rtn = 1 or i_cr_company_id = 10000 then
//		f_pp_msgs(string(i_allocation_id) + ": Updating cr_alloc_process_control at " + &
//		           string(today(), "mm/dd/yyyy hh:mm:ss"))
		finished = msg_dt
		if i_whole_month then
			whole_month_int = 1
		else
			whole_month_int = 0
		end if
		//  Update the cr_alloc_process_control_bdg table ... Type 3 allocations must have
		//  all the dates filled in ...
		choose case i_clearing_indicator
// *****  OLD METHOD  ***** USED WITH OLD METHOD OF DETERMINING MONTHS FOR TYPE 3 IN
//                          UF_LOADING1...
//			case 3
//				for j = 1 to upperbound(i_clearing_mn)
//					if i_run_as_a_test then  //  TESTing an allocation ...
//						insert into cr_alloc_process_control_bdg
//							(allocation_id, month_number, month_period, finished, 
//							 source_sql, test_or_actual)
//						values
//							(:i_allocation_id, :i_clearing_mn[j], :i_clearing_mp[j], :finished,
//							 :i_source_sqls, 'TEST');
//					else                     //  ACTUAL allocation being run ...
//						insert into cr_alloc_process_control_bdg
//							(allocation_id, month_number, month_period, finished, 
//							 source_sql, test_or_actual)
//						values
//							(:i_allocation_id, :i_clearing_mn[j], :i_clearing_mp[j], :finished,
//							 :i_source_sqls, 'ACTUAL');
//					end if
//					if sqlca.SQLCode = 0 then
//						commit using sqlca;
//					else
//						uf_msg("Allocations", "Error inserting cr_alloc_process_control_bdg~n~n" + &
//									   sqlca.SQLErrText)
//						rollback using sqlca;
//					end if					
//				next
// *****  OLD METHOD  ***** USED WITH OLD METHOD OF DETERMINING MONTHS FOR TYPE 3 IN
//                          UF_LOADING1...
			case 0
				//  Everything should go through the "case else" ...
			case else
				if i_run_as_a_test then  //  TESTing an allocation ...
					insert into cr_alloc_process_control_bdg
						(allocation_id, month_number, month_period, finished, 
						 source_sql, test_or_actual, whole_month, budget_version)
					values
						(:i_allocation_id, :i_month_number, :i_month_period, :finished, 
						 :i_source_sqls, 'TEST', :whole_month_int, :i_budget_version)
					 using sqlca;
				else                     //  ACTUAL allocation being run ...
					insert into cr_alloc_process_control_bdg
						(allocation_id, month_number, month_period, finished, 
						 source_sql, test_or_actual, whole_month, budget_version)
					values
						(:i_allocation_id, :i_month_number, :i_month_period, :finished, 
						 :i_source_sqls, 'ACTUAL', :whole_month_int, :i_budget_version)
				    using sqlca;
				end if
				if sqlca.SQLCode = 0 then
					commit using sqlca;
				else
					uf_msg("Allocations", "Error inserting cr_alloc_process_control_bdg " + &
								   sqlca.SQLErrText)
					rollback using sqlca;
				end if
				
				if i_run_as_a_test then  //  TESTing an allocation ...
					insert into cr_alloc_process_control2_bdg
						(allocation_id, month_number, month_period, 
						 balance_sql, test_or_actual, budget_version)
					values
						(:i_allocation_id, :i_month_number, :i_month_period,
						 :i_balance_sqls, 'TEST', :i_budget_version)
					 using sqlca;
				else                     //  ACTUAL allocation being run ...
					insert into cr_alloc_process_control2_bdg
						(allocation_id, month_number, month_period, 
						 balance_sql, test_or_actual, budget_version)
					values
						(:i_allocation_id, :i_month_number, :i_month_period, 
						 :i_balance_sqls, 'ACTUAL', :i_budget_version)
				    using sqlca;
				end if
				if sqlca.SQLCode = 0 then
					commit using sqlca;
				else
					uf_msg("Allocations", "  ")
					uf_msg("Allocations", "Error inserting cr_alloc_process_control2_bdg " + &
								   sqlca.SQLErrText)
					uf_msg("Allocations", "  ")
					rollback using sqlca;
				end if
				
				if i_run_as_a_test then  //  TESTing an allocation ...
					insert into cr_alloc_process_control3_bdg
						(allocation_id, month_number, month_period, 
						 grouping_sql, test_or_actual, budget_version)
					values
						(:i_allocation_id, :i_month_number, :i_month_period,
						 :i_grouping_sqls, 'TEST', :i_budget_version)
					 using sqlca;
				else                     //  ACTUAL allocation being run ...
					insert into cr_alloc_process_control3_bdg
						(allocation_id, month_number, month_period, 
						 grouping_sql, test_or_actual, budget_version)
					values
						(:i_allocation_id, :i_month_number, :i_month_period, 
						 :i_grouping_sqls, 'ACTUAL', :i_budget_version)
				    using sqlca;
				end if
				if sqlca.SQLCode = 0 then
					commit using sqlca;
				else
					uf_msg("Allocations", "  ")
					uf_msg("Allocations", "Error inserting cr_alloc_process_control3_bdg " + &
								   sqlca.SQLErrText)
					uf_msg("Allocations", "  ")
					rollback using sqlca;
				end if
				
		end choose
		
	end if
	
next  //  for i = 1 to num_allocs ...

//DESTROY ds_cr_alloc_months
//DESTROY ds_alloc_list
//DESTROY ds_company

if not i_msgbox then
	uf_msg("", "Finished: " + string(i_allocation_id) + ": " + &
	            description + " at " + string(msg_dt))
	uf_msg("", "*******************************************")
end if

//w_top_frame.SetMicroHelp("Ready")

return rtn
end function

public function string uf_save_report_sql (string a_allocation_sqls, boolean a_custom_dw);//******************************************************************************************
//
//  User Object Function  :  uf_save_report_sql
//
//  Description           :  Build the string used to save the source SQL for reporting
//                           the allocation results.
//  
//******************************************************************************************
longlong where_pos, group_pos, source_sql_counter, source_sql_rows, i, mn, mp

//  Save the source SQL for reporting ...
if a_custom_dw then
	setnull(i_source_sqls)
else
	i_source_sqls = a_allocation_sqls
	where_pos       = pos(i_source_sqls, "where")
	if where_pos > 0 then
		i_source_sqls = right(i_source_sqls, len(i_source_sqls) - (where_pos - 1))
		group_pos     = pos(i_source_sqls, "group by")
		if group_pos > 0 then
			i_source_sqls = left(i_source_sqls, group_pos - 1)
		end if
	end if
	if i_whole_month then
		//  Must add the list of month_periods to the reporting SQL, just in case another
		//  month_period is run ...
		//  NO, NO, NO !!!
//		source_sql_counter = 0
//		if isvalid(w_cr_control) then
//			datawindow d
//			d = w_cr_control.tab_control.tabpage_allocations1.dw_1
//			source_sql_rows = d.RowCount()
//		end if
//		for i = 1 to source_sql_rows
//			mn = d.GetItemNumber(i, 1)
//			if mn = i_month_number then
//				source_sql_counter++
//				mp = d.GetItemNumber(i, 2)
//				if source_sql_counter = 1 then
//					i_source_sqls = i_source_sqls + "and (month_period = " + string(mp)
//				else
//					i_source_sqls = i_source_sqls + " or month_period = " + string(mp)
//				end if
//			end if
//		next
//		if source_sql_counter > 0 then
//			i_source_sqls = i_source_sqls + ") "
//		end if
	end if
end if

return i_source_sqls
end function

public function integer uf_update_cr (longlong a_month_number, longlong a_month_period);//
// NOT FOR BUDGET ALLOCATIONS ...
//

return 1
end function

public subroutine uf_msg (string a_msg_header, string a_msg);//******************************************************************************************
//
//  User Object Function  :  uf_msg
//
//  Description           :  Manages the messages ... does nothing if i_msgbox is 
//                           true ... otherwise, it writes to pp_processes_messages.
//
//******************************************************************************************


if i_msgbox then
	//  CANNOT HAVE THIS IN THE STAND-ALONE EXE ...

else
	f_pp_msgs(a_msg)
end if
end subroutine

public function decimal uf_lookup_rate (longlong a_rate_id, string a_company_value, longlong a_month_number);//******************************************************************************************
//
//  User Object Function  :  uf_lookup_rate
//
//  Description  :  Looks up predefined rates from the cr_rates_input table.
//
//                  The user may define the rates in one of 3 ways:
//                    1.  By company and month
//                    2.  By company for ALL months (e.g. billings by customers)
//                    3.  For ALL companies by month (e.g. payroll accruals)
//
//                  Lookup the rates in this order ...
//
//******************************************************************************************
decimal {10} rate
string      mn_string


//  The cr_rates_input.month_number field is a VarChar(35) to support the
//  entry of "ALL" ...
mn_string = string(a_month_number)

//  This MUST BE set to NULL.  A null value after selects will mean "no rate found"
setnull(rate)


//
//  1.  Company and Month:
//
select rate into :rate from cr_rates_input
 where rate_id       = :a_rate_id and 
       company_field = :a_company_value and 
		 month_number  = :mn_string;

if not isnull(rate) then return rate


//
//  2.  Company for ALL Months:
//
select rate into :rate from cr_rates_input
 where rate_id       = :a_rate_id and 
       company_field = :a_company_value and 
		 month_number  = 'ALL';

if not isnull(rate) then return rate


//
//  3.  ALL Companies by Month:
//
select rate into :rate from cr_rates_input
 where rate_id       = :a_rate_id and 
       company_field = ' ALL' and 
		 month_number  = :mn_string;

if not isnull(rate) then return rate


//  DMJ: 04/19/2005
//
//  4.  ALL Companies ALL Months:
//
select rate into :rate from cr_rates_input
 where rate_id       = :a_rate_id and 
       company_field = ' ALL' and 
		 month_number  = 'ALL';

if not isnull(rate) then return rate


//  If we get here, we found no rates ... return a NULL.  There is code in
//  uf_loading1 that is expecting a NULL ...

return rate
end function

public function integer uf_clearing ();//******************************************************************************************
//
//  User Object Function:  uf_clearing
//
//
//
//******************************************************************************************



return 1
end function

public subroutine uf_capital_work_orders (longlong a_clearing_id);////*****************************************************************************************
////
////*****************************************************************************************
//
////  SAVE THIS HERE ...
////    It is the syntax that will retrieve the correct work orders for a
////    given clearing_id ... this keeps it from cluttering up the allocation script.
//i_wo_sqls = &
//"select work_order_number " + &
//  "from wo_type_clear_dflt, work_order_control " + &
// "where wo_type_clear_dflt.clearing_id = " + string(a_clearing_id) + " and " + &
//       "wo_type_clear_dflt.work_order_type_id = work_order_control.work_order_type_id " + &
//"minus " + &
//"select work_order_number " + &
//  "from wo_clear_over, work_order_control " + &
// "where wo_clear_over.clearing_id = " + string(a_clearing_id) + " and " + &
//       "wo_clear_over.include_exclude = 0 and " + &
//       "wo_clear_over.work_order_id = work_order_control.work_order_id " + &
//"union " + &
//"select work_order_number " + &
//  "from wo_clear_over, work_order_control " + &
// "where wo_clear_over.clearing_id = " + string(a_clearing_id) + " and " + &
//       "wo_clear_over.include_exclude = 1 and " + &
//       "wo_clear_over.work_order_id = work_order_control.work_order_id "

//*****************************************************************************************
//  All code from this function has been moved to f_cr_wo_clear, so it can be customized
//  at clients (statuses and so forth) without touching the base pbl.
//*****************************************************************************************

f_cr_wo_clear_bdg(i_cr_company_id, i_month_number, i_allocation_id, a_clearing_id)

end subroutine

public function longlong uf_parse (string a_stringtoparse, string a_separator, ref string a_array_to_hold[]);//*****************************************************************************************
//
//  Window Function  :  wf_parse
//
//  Description      :  This function will parse string  into an array of elements denoted
//								by the separator.  The number of elements parsed is the return 
//								value of the function.  For example, if ~t were the separator, 
//								"red~twhite~tblue" would be parsed into an array containing red, 
//								white and blue.  The value 3 would be returned.
//
//  Notes            :  A MODIFIED VERSION OF:  f_parsestringintoarray from ppsystem.
//								This functions MUST INCLUDE the "separator" in the string that is
//								placed in the array (The "{").
//
//  Arguments :	a_stringtoparse :		string			      : string to parse
//						a_separator	    :		string			      : what to look for
//						a_array_to_hold : 	string array(by ref) : gets filled with values
//
//   
//  Returns   :  longlong :  The number of elements returned into the array.
//								  If the separator is not found then the original string is 
//                        placed in array_to_hold and 1 is returned.
//
//******************************************************************************************
longlong separator_pos, separator_len, count, start_pos
string holder

start_pos     = 1
separator_len = Len(a_separator)
separator_pos = Pos(a_stringtoparse, a_separator, start_pos)
IF Trim(a_stringtoparse) = "" or IsNull(a_stringtoparse) THEN 
	RETURN 0
ELSEIF separator_pos = 0 THEN 
	a_array_to_hold[1] = trim(a_stringtoparse)
	RETURN 1
END IF
DO WHILE separator_pos > 0
	if count = 0 then  //  Modified
		count++         //  Modified
		goto bottom		 //  Modified
	end if				 //  Modified
	holder = Mid(a_stringtoparse, start_pos - 1, (separator_pos - start_pos) + 1)  //  Modified
	a_array_to_hold[count] = trim(holder)
	count++  //  Moved below the previous line
	bottom:  //  Modified
	start_pos = separator_pos + separator_len
	separator_pos =  Pos(a_stringtoparse, a_separator, start_pos)	
LOOP
holder = Mid(a_stringtoparse, start_pos - 1, Len(a_stringtoparse))
//  NOT IN THIS WINDOW FUNCTION ... //count = count + 1
a_array_to_hold[count] = trim(holder)

RETURN count


end function

public function integer uf_clearing_get_periods ();string sqls, where_sqls, left_paren, col_name, operator, value1, between_and, value2, &
		 right_paren, and_or, descr, where_clause, orig_value1
longlong i, where_clause_id, num_rows, month_number, month_period, counter

select description into :descr
  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;

//  ds_1:  All months and periods in the cr_budget_data that meet the source criteria
//         for this allocation ...
//uo_ds_top i_ds_1
//i_ds_1 = CREATE uo_ds_top
i_ds_1.reset()

//uo_ds_top i_ds_where_clause
//i_ds_where_clause = CREATE uo_ds_top
i_ds_where_clause.reset()

where_clause_id = 0
select where_clause_id into :where_clause_id
  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;
  
where_sqls = "select distinct month_number, month_period from cr_budget_data " 

if where_clause_id = 0 then goto create_dw

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(where_clause_id) + " order by row_id"

f_create_dynamic_ds(i_ds_where_clause, "grid", sqls, sqlca, true)

num_rows = i_ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		where_sqls = where_sqls + " where ( budget_version = '" + i_budget_version + "' and "
	end if
	
	left_paren  = upper(trim(i_ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(i_ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(i_ds_where_clause.GetItemString(i, "operator")))
	
	orig_value1 = upper(i_ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		value1 = upper(trim(i_ds_where_clause.GetItemString(i, "value1")))
	end if
	
	between_and = upper(trim(i_ds_where_clause.GetItemString(i, "between_and")))
	value2      = upper(trim(i_ds_where_clause.GetItemString(i, "value2")))
	right_paren = upper(trim(i_ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(i_ds_where_clause.GetItemString(i, "and_or")))
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	
	col_name = f_cr_clean_string(col_name)
	
	if between_and = "" then
		where_sqls = where_sqls + &
						 left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
						 value1 + "' " + &
						 right_paren + " " + and_or
	else
		where_sqls = where_sqls + &
						 left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
						 value1 + "' " + between_and + " '" + value2 + "' " + &
						 right_paren + " " + and_or		
	end if
	
	if i = num_rows then
		where_sqls = where_sqls + " )"
	end if
	
next

create_dw:

f_create_dynamic_ds(i_ds_1, "grid", where_sqls, sqlca, true)

if i_ds_1.RowCount() < 1 then
	uf_msg("Allocation - " + descr, &
				  "No records meet the source criteria.~n" + &
				  "Cannot run this allocation.")
//	DESTROY ds_where_clause
//	DESTROY ds_1
	return -1
end if

//  ds_2:  All months which this allocation has fun for ...
//uo_ds_top i_ds_2
//i_ds_2 = CREATE uo_ds_top
i_ds_2.reset()

if i_run_as_a_test then
	sqls = "select month_number, month_period from cr_alloc_process_control_bdg " + &
			 "where allocation_id = " + string(i_allocation_id) + " and " + &
			 "budget_version = '" + i_budget_version + "' and " + &
			 "upper(test_or_actual) = 'TEST'"	
else
	sqls = "select month_number, month_period from cr_alloc_process_control_bdg " + &
			 "where allocation_id = " + string(i_allocation_id) + " and " + &
			 "budget_version = '" + i_budget_version + "' and " + &
			 "upper(test_or_actual) = 'ACTUAL'"
end if

f_create_dynamic_ds(i_ds_2, "grid", sqls, sqlca, true)


//  Determine all months and periods that this allocation has not run for and build
//  a where clause ...
where_clause = " where ( "

for i = 1 to i_ds_1.RowCount()
	month_number = i_ds_1.GetItemNumber(i, 1)
	month_period = i_ds_1.GetItemNumber(i, 2)
	
	counter = 0
	if i_run_as_a_test then
		select count(*) into :counter
		  from cr_alloc_process_control_bdg
		 where allocation_id = :i_allocation_id and
		       budget_version = :i_budget_version and
		       month_number  = :month_number    and 
	   	    month_period  = :month_period    and
				 upper(test_or_actual) = 'TEST';		
	else
		select count(*) into :counter
		  from cr_alloc_process_control_bdg
		 where allocation_id = :i_allocation_id and
		       budget_version = :i_budget_version and
		       month_number  = :month_number    and 
	   	    month_period  = :month_period    and
				 upper(test_or_actual) = 'ACTUAL';
	end if
	 
	if counter = 0 then
		i_clearing_mn[upperbound(i_clearing_mn) + 1] = month_number
		i_clearing_mp[upperbound(i_clearing_mp) + 1] = month_period
		where_clause = where_clause + "(month_number = " + string(month_number) + &
						   " and month_period = " + string(month_period) + ") or "
	end if
	
next

if trim(where_clause) = "where (" then
	uf_msg("Allocation - " + descr, &
				  "This allocation has already been run for all months and periods.")	
//	DESTROY ds_2
//	DESTROY ds_where_clause
//	DESTROY ds_1
	return -1
end if

//  Trim off the trailing "or " ...
where_clause = left(where_clause, len(where_clause) - 3)

i_clearing_periods_wc = where_clause + ") "

//DESTROY ds_2
//DESTROY ds_where_clause
//DESTROY ds_1

return 1

end function

public function string uf_build_sqls (string a_type);string sqls


sqls = ""


//****************************************************************
//
//  PATTERN CLOSINGS - XFER
//
//****************************************************************
if upper(a_type) = "PATTERN CLOSINGS - XFER" then
	
sqls = + &
" " + &
"update cr_budget_afudc_calc a set closings = ( " + &
" " + &
	"select nvl(closings,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "(total_amount_spent - total_closings) * percent_close as closings " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and closing_pattern_id is not null) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number), " + &
" " + &	
											" closings_tax = ( " + &
" " + &
	"select nvl(closings_tax,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "(total_amount_spent_tax - total_closings_tax) * percent_close as closings_tax " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and closing_pattern_id is not null) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number), " + &
" " + &
											" afudc_transferred_closings = ( " + &
" " + &
	"select nvl(afudc_transferred_closings,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "(total_transferred_afudc_spent - total_transferred_afudc_closed) " + &
				 	"* percent_close as afudc_transferred_closings " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and closing_pattern_id is not null) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number) " + &
" " + &
"where budget_version = '" + i_budget_version + "' " + &
	i_closings_where_clause + " " + &
  "and closings = 0 " + &
  "and closing_pattern_id is not null "
	
end if


//****************************************************************
//
//  ISD CLOSINGS - XFER
//
//****************************************************************
if upper(a_type) = "ISD CLOSINGS - XFER" then
	
sqls = + &
" " + &
"update cr_budget_afudc_calc a set closings = ( " + &
" " + &
	"select nvl(closings,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "total_amount_spent - total_closings as closings " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and in_service_date is not null and closing_pattern_id is null " + &
			"and " + string(i_month_number) + " >= to_number(to_char(in_service_date,'yyyymm'))) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number), " + &
" " + &
											" closings_tax = ( " + &
" " + &
	"select nvl(closings_tax,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "total_amount_spent_tax - total_closings_tax as closings_tax " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and in_service_date is not null and closing_pattern_id is null " + &
			"and " + string(i_month_number) + " >= to_number(to_char(in_service_date,'yyyymm'))) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number), " + &
" " + &
											" afudc_transferred_closings = ( " + &
" " + &
	"select nvl(afudc_transferred_closings,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "total_transferred_afudc_spent - total_transferred_afudc_closed as afudc_transferred_closings " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and in_service_date is not null and closing_pattern_id is null " + &
			"and " + string(i_month_number) + " >= to_number(to_char(in_service_date,'yyyymm'))) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number) " + &
" " + &
"where budget_version = '" + i_budget_version + "' " + &
	i_closings_where_clause + " " + &
  "and closings = 0 " + &
  "and in_service_date is not null " + &
  "and closing_pattern_id is null "
	
end if


return sqls

end function

public function longlong uf_afudc ();//
//  QUESTIONS/ISSUES:
//
//
longlong i, in_service_mn, month_increment
string sqls, budgeting_element, cpi_sqls


////********************************************************************************************
////  Update the beg_afudc_base and beg_cpi_base.  DO NOT DO THIS FOR THE 1ST MONTH.
////********************************************************************************************
//if i_month_number = i_start_month then goto after_beg_base
//
//f_pp_msgs("Updating beg_afudc_base and beg_cpi_base at " + string(now()))
//
//if right(string(i_month_number), 2) = "01" then
//	sqls = "update cr_budget_afudc_calc a set beg_afudc_base = (" + &
//				"select nvl(end_afudc_base,0) from cr_budget_afudc_calc b " + &
//				" where a.month_number = " + string(i_month_number) + &
//				  " and b.month_number = " + string(i_month_number - 89)
//				  
//	cpi_sqls = "update cr_budget_afudc_calc a set beg_cpi_base = (" + &
//				"select nvl(end_cpi_base,0) from cr_budget_afudc_calc b " + &
//				" where a.month_number = " + string(i_month_number) + &
//				  " and b.month_number = " + string(i_month_number - 89)
//else
//	sqls = "update cr_budget_afudc_calc a set beg_afudc_base = (" + &
//				"select nvl(end_afudc_base,0) from cr_budget_afudc_calc b " + &
//				" where a.month_number = " + string(i_month_number) + &
//				  " and b.month_number = " + string(i_month_number - 1)
//	
//	cpi_sqls = "update cr_budget_afudc_calc a set beg_cpi_base = (" + &
//				"select nvl(end_cpi_base,0) from cr_budget_afudc_calc b " + &
//				" where a.month_number = " + string(i_month_number) + &
//				  " and b.month_number = " + string(i_month_number - 1)
//	
//end if
//
//uo_ds_top i_ds_elements
//i_ds_elements = CREATE uo_ds_top
//i_ds_elements.DataObject = "dw_cr_element_definitions"
//i_ds_elements.SetTransObject(sqlca)
//i_num_elements = i_ds_elements.RETRIEVE()
//
//
////  If intercompany accounting is turned on, then the company field 
////  must be in the group_by.
//for i = 1 to i_ds_elements.RowCount()
//	budgeting_element = upper(trim(i_ds_elements.GetItemString(i, "budgeting_element")))
//	budgeting_element = f_replace_string(budgeting_element, " ", "_", "all")
//	budgeting_element = f_replace_string(budgeting_element, "-", "_", "all")
//	budgeting_element = f_replace_string(budgeting_element, "/", "_", "all")
//	sqls     = sqls     + ' and a."' + budgeting_element + '" = b."' + budgeting_element + '" '
//	cpi_sqls = cpi_sqls + ' and a."' + budgeting_element + '" = b."' + budgeting_element + '" '
//next
//
//DESTROY i_ds_elements
//
//sqls = sqls + " and a.budget_id = b.budget_id "
//sqls = sqls + " and a.gl_journal_category = b.gl_journal_category "
//sqls = sqls + " and a.source_id = b.source_id "
//sqls = sqls + " and a.budget_version = '" + i_budget_version + "' "
//sqls = sqls + " and b.budget_version = '" + i_budget_version + "') "
//sqls = sqls + " where budget_version = '" + i_budget_version + "' "
//sqls = sqls + "   and month_number = " + string(i_month_number)
//
//cpi_sqls = cpi_sqls + " and a.budget_id = b.budget_id "
//cpi_sqls = cpi_sqls + " and a.gl_journal_category = b.gl_journal_category "
//cpi_sqls = cpi_sqls + " and a.source_id = b.source_id "
//cpi_sqls = cpi_sqls + " and a.budget_version = '" + i_budget_version + "' "
//cpi_sqls = cpi_sqls + " and b.budget_version = '" + i_budget_version + "') "
//cpi_sqls = cpi_sqls + " where budget_version = '" + i_budget_version + "' "
//cpi_sqls = cpi_sqls + "   and month_number = " + string(i_month_number)
//
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: updating beg_afudc_base: " + sqlca.SQLErrText)
//	f_pp_msgs("SQLS = " + sqls)
//	rollback;
//	return -1
//end if
//
//execute immediate :cpi_sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: updating beg_cpi_base: " + sqlca.SQLErrText)
//	f_pp_msgs("SQLS = " + cpi_sqls)
//	rollback;
//	return -1
//end if
//
//commit;
//
//
////  Fix any NULLs ...
//update cr_budget_afudc_calc set beg_afudc_base = 0 
// where beg_afudc_base is NULL
//	and budget_version = :i_budget_version
// 	and month_number = :i_month_number;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: updating beg_afudc_base to 0 where NULL: " + sqlca.SQLErrText)
//	rollback;
//	return -1
//end if
//
//update cr_budget_afudc_calc set beg_cpi_base = 0 
// where beg_cpi_base is NULL
//	and budget_version = :i_budget_version
// 	and month_number = :i_month_number;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: updating beg_cpi_base to 0 where NULL: " + sqlca.SQLErrText)
//	rollback;
//	return -1
//end if
//
//commit;
//
//
//after_beg_base:


//********************************************************************************************
//  Update the total_closings, total_amount_spent, beg_cwip from the prior month.
//  DO NOT DO THIS FOR THE 1ST MONTH.
//********************************************************************************************
if i_month_number = i_start_month then goto after_tot_closings_prior

f_pp_msgs("Updating total_closings, total_amount_spent, beg_cwip from prior month at " + string(now()))

if right(string(i_month_number),2) = "01" then
	month_increment = 89
else
	month_increment = 1
end if

update cr_budget_afudc_calc a set beg_afudc_base = (

	select nvl(end_afudc_base,0)
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
		
											 beg_cpi_base = (

	select nvl(end_cpi_base,0)
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),

											 total_closings = (

	select total_closings
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
				
											 total_closings_tax = (

	select total_closings_tax
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
															 
											 total_amount_spent = (
	select total_amount_spent
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
		
											 total_amount_spent_tax = (
	select total_amount_spent_tax
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
		
											 beg_cwip = (
	select end_cwip
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
		
											 beg_cwip_tax = (
	select end_cwip_tax
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
	
											 uncompounded_afudc_balance = (
	select uncompounded_afudc_balance
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
		
											 uncompounded_cpi_balance = (
	select uncompounded_cpi_balance
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
	
											 total_transferred_afudc_spent = (
	select total_transferred_afudc_spent
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment),
	
											 total_transferred_afudc_closed = (
	select total_transferred_afudc_closed
	  from cr_budget_afudc_calc b
	 where b.budget_version = :i_budget_version
		and b.month_number = :i_month_number - :month_increment
		and a.budget_id = b.budget_id
		and a.month_number = b.month_number + :month_increment)
		
 where budget_version = :i_budget_version
	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating total_closings, total_amount_spent, beg_cwip " + &
		"from the prior month: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//  Fix any NULLs ... Copied from above.
f_pp_msgs("Updating beg_afudc_base and beg_cpi_base (where NULL) at " + string(now()))

update cr_budget_afudc_calc set beg_afudc_base = 0 
 where beg_afudc_base is NULL
	and budget_version = :i_budget_version
 	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating beg_afudc_base to 0 where NULL: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

update cr_budget_afudc_calc set beg_cpi_base = 0 
 where beg_cpi_base is NULL
	and budget_version = :i_budget_version
 	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating beg_cpi_base to 0 where NULL: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


after_tot_closings_prior:


//********************************************************************************************
//  Update the total_amount_spent, half_month_charges.
//********************************************************************************************
f_pp_msgs("Updating total_amount_spent, half_month_charges at " + string(now()))

update cr_budget_afudc_calc 
	set total_amount_spent     = total_amount_spent     + amount,
		 total_amount_spent_tax = total_amount_spent_tax + amount_tax,
		 half_month_charges     = amount     * .5, 
		 half_month_charges_tax = amount_tax * .5
 where budget_version = :i_budget_version
	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating total_amount_spent from the prior month: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//  MOVED ABOVE.
////********************************************************************************************
////  Update the half_month_charges.
////********************************************************************************************
//f_pp_msgs("Updating half_month_charges at " + string(now()))
//
//update cr_budget_afudc_calc 
//	set half_month_charges = amount * .5, half_month_charges_tax = amount_tax * .5
// where budget_version = :i_budget_version
// 	and month_number = :i_month_number;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.half_month_charges: " + sqlca.SQLErrText)
//	rollback;
//	return -1
//end if
//
//commit;


//********************************************************************************************
//  CLOSINGS:
//
//  In Service Date Method
//  ----------------------
//  1.  The complex "month_number" in the select performs a decode on the month where the
//      closing should occur - December of that same year.  Since the late_chg_wait_period
//		  simply increments the month_number we can get things like 200113 - 200112.  Thus,
//      anything that goes beyond December (sign = 1) will get pushed into the 
//		  next year (89 - 1).
//
//  Closing Pattern
//  ---------------
//  1.  For now, this method overrides all others.
//
//
//********************************************************************************************

//
//  PATTERN CLOSINGS:
//

f_pp_msgs("Computing pattern closings at " + string(now()))

if i_budget_afudc_transfers <> 0 then
	
	//  If they are not using the transfer logic, omit those closings column from the SQL for
	//  speed.  Otherwise, call the function that builds the SQL.  I used a separate function
	//  because the SQL string was already getting very longlong in this script.
	sqls = trim(uf_build_sqls("Pattern Closings - Xfer"))
	
	//  NULLS would crash the exe!
	if isnull(sqls) or sqls = "" then
		f_pp_msgs("ERROR: Pattern Closings - Xfer returned NULL: " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
		
else

sqls = + &
" " + &
"update cr_budget_afudc_calc a set closings = ( " + &
" " + &
	"select nvl(closings,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "(total_amount_spent - total_closings) * percent_close as closings " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and closing_pattern_id is not null) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number), " + &
" " + &	
											" closings_tax = ( " + &
" " + &
	"select nvl(closings_tax,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "(total_amount_spent_tax - total_closings_tax) * percent_close as closings_tax " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and closing_pattern_id is not null) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number) " + &
" " + &
"where budget_version = '" + i_budget_version + "' " + &
	i_closings_where_clause + " " + &
  "and closings = 0 " + &
  "and closing_pattern_id is not null "
 
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.closings (PERCENT CLOSE): " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


f_pp_msgs("Computing pattern closings (fixing nulls) at " + string(now()))

sqls = + &
"update cr_budget_afudc_calc set closings = 0, closings_tax = 0, " + &
		 "afudc_transferred_closings = 0 " + &
 "where budget_version = '" + i_budget_version + "' " + &
 	i_closings_where_clause + " " + &
   "and closing_pattern_id is not null " + &
	"and closings is NULL "

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.closings to 0 where NULL: " + &
		sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


f_pp_msgs("Computing pattern total_closings at " + string(now()))

update cr_budget_afudc_calc 
	set total_closings      = total_closings                +
			((total_amount_spent     - total_closings)           * percent_close),
						
		 total_closings_tax  = total_closings_tax            + 
		 	((total_amount_spent_tax - total_closings_tax)       * percent_close),
		 
		 total_transferred_afudc_closed = total_transferred_afudc_closed +
		 	((total_transferred_afudc_spent - total_transferred_afudc_closed) * percent_close)
			 
 where budget_version = :i_budget_version
	and month_number = :i_month_number
	and closing_pattern_id is not null;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.total_closings: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//
//  IN SERVICE DATE CLOSINGS:
//
f_pp_msgs("Computing In Service Date closings at " + string(now()))

if i_budget_afudc_transfers <> 0 then
	
	//  If they are not using the transfer logic, omit those closings column from the SQL for
	//  speed.  Otherwise, call the function that builds the SQL.  I used a separate function
	//  because the SQL string was already getting very longlong in this script.
	sqls = trim(uf_build_sqls("ISD Closings - Xfer"))
	
	//  NULLS would crash the exe!
	if isnull(sqls) or sqls = "" then
		f_pp_msgs("ERROR: ISD Closings - Xfer returned NULL: " + sqlca.SQLErrText)
		rollback;
		return -1
	end if
		
else
	
sqls = + &
" " + &
"update cr_budget_afudc_calc a set closings = ( " + &
" " + &
	"select nvl(closings,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "total_amount_spent - total_closings as closings " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and in_service_date is not null and closing_pattern_id is null " + &
			"and " + string(i_month_number) + " >= to_number(to_char(in_service_date,'yyyymm'))) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number), " + &
" " + &
											" closings_tax = ( " + &
" " + &
	"select nvl(closings_tax,0) from ( " + &
" " + &
		"select budget_id,  " + &
				 "decode( " + &
				 	"sign( " + &
					 	"(month_number + nvl(late_chg_wait_period,0))  " + &
					 "- (to_number(substr(month_number,1,4)) * 100 + 12) " + &
					 	 "),  " + &
						 "1, month_number + nvl(late_chg_wait_period,0) + 89 - 1,  " + &
						    "month_number + nvl(late_chg_wait_period,0)) as month_number, " + &
				 "total_amount_spent_tax - total_closings_tax as closings_tax " + &
		  "from cr_budget_afudc_calc " + &
		 "where budget_version = '" + i_budget_version + "' " + &
			"and month_number = " + string(i_month_number) + " " + &
			"and in_service_date is not null and closing_pattern_id is null " + &
			"and " + string(i_month_number) + " >= to_number(to_char(in_service_date,'yyyymm'))) b " + &
" " + &
	 "where a.budget_id = b.budget_id " + &
		"and a.month_number = b.month_number) " + &
" " + &
"where budget_version = '" + i_budget_version + "' " + &
	i_closings_where_clause + " " + &
  "and closings = 0 " + &
  "and in_service_date is not null " + &
  "and closing_pattern_id is null "

end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.closings (ISD METHOD): " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


f_pp_msgs("Computing In Service Date closings (fixing nulls) at " + string(now()))

sqls = + &
"update cr_budget_afudc_calc set closings = 0, closings_tax = 0, " + &
		 "afudc_transferred_closings = 0 " + &
 "where budget_version = '" + i_budget_version + "' " + &
 	i_closings_where_clause + " " + &
   "and closing_pattern_id is null " + &
	"and closings is NULL "

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.closings to 0 where NULL: " + &
		sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


f_pp_msgs("Computing In Service Date total_closings at " + string(now()))

update cr_budget_afudc_calc 
	set total_closings     = total_amount_spent,
	    total_closings_tax = total_amount_spent_tax,
		 total_transferred_afudc_closed = total_transferred_afudc_spent
 where budget_version = :i_budget_version
	and month_number = :i_month_number
	and in_service_date is not null and closing_pattern_id is null
	and :i_month_number >= to_number(to_char(in_service_date,'yyyymm'));

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.total_closings: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//********************************************************************************************
//  Update the rates and indicators.
//********************************************************************************************
f_pp_msgs("Updating rates and indicators at " + string(now()))

update cr_budget_afudc_calc a set 
	
	debt_rate = (
		select debt_rate
		  from afudc_data b, 
				(select afudc_type_id, max(effective_date) as effective_date 
					from afudc_data 
				  where to_number(to_char(effective_date, 'yyyymm')) <= :i_month_number 
				  group by afudc_type_id) c
		 where a.afudc_type_id = b.afudc_type_id
			and b.afudc_type_id = c.afudc_type_id
			and b.effective_date = c.effective_date),
		
	equity_rate = (
		select equity_rate
		  from afudc_data b, 
				(select afudc_type_id, max(effective_date) as effective_date 
					from afudc_data 
				  where to_number(to_char(effective_date, 'yyyymm')) <= :i_month_number 
				  group by afudc_type_id) c
		 where a.afudc_type_id = b.afudc_type_id
			and b.afudc_type_id = c.afudc_type_id
			and b.effective_date = c.effective_date),
	
	cpi_rate = (
		select cpi_rate
		  from afudc_data b, 
				(select afudc_type_id, max(effective_date) as effective_date 
					from afudc_data 
				  where to_number(to_char(effective_date, 'yyyymm')) <= :i_month_number 
				  group by afudc_type_id) c
		 where a.afudc_type_id = b.afudc_type_id
			and b.afudc_type_id = c.afudc_type_id
			and b.effective_date = c.effective_date),
	
	afudc_compound_indicator = (
		select decode(substr(to_char(:i_month_number),5,2),
				'01',afudc_jan_com_ind, '02',afudc_feb_com_ind, '03',afudc_mar_com_ind,
				'04',afudc_apr_com_ind, '05',afudc_may_com_ind, '06',afudc_jun_com_ind,
				'07',afudc_jul_com_ind, '08',afudc_aug_com_ind, '09',afudc_sep_com_ind,
				'10',afudc_oct_com_ind, '11',afudc_nov_com_ind, '12',afudc_dec_com_ind, 0)
		  from afudc_data b, 
				(select afudc_type_id, max(effective_date) as effective_date 
					from afudc_data 
				  where to_number(to_char(effective_date, 'yyyymm')) <= :i_month_number 
				  group by afudc_type_id) c
		 where a.afudc_type_id = b.afudc_type_id
			and b.afudc_type_id = c.afudc_type_id
			and b.effective_date = c.effective_date),
	
	cpi_compound_indicator = (
		select decode(substr(to_char(:i_month_number),5,2),
				'01',cpi_jan_com_ind, '02',cpi_feb_com_ind, '03',cpi_mar_com_ind,
				'04',cpi_apr_com_ind, '05',cpi_may_com_ind, '06',cpi_jun_com_ind,
				'07',cpi_jul_com_ind, '08',cpi_aug_com_ind, '09',cpi_sep_com_ind,
				'10',cpi_oct_com_ind, '11',cpi_nov_com_ind, '12',cpi_dec_com_ind, 0)
		  from afudc_data b, 
				(select afudc_type_id, max(effective_date) as effective_date 
					from afudc_data 
				  where to_number(to_char(effective_date, 'yyyymm')) <= :i_month_number 
				  group by afudc_type_id) c
		 where a.afudc_type_id = b.afudc_type_id
			and b.afudc_type_id = c.afudc_type_id
			and b.effective_date = c.effective_date)
	
		
 where budget_version = :i_budget_version
	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc (rates): " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//********************************************************************************************
//  Update the afudc_base_used_in_calc and cpi_base_used_in_calc.
//********************************************************************************************
f_pp_msgs("Updating afudc_base_used_in_calc, cpi_base_used_in_calc at " + string(now()))

update cr_budget_afudc_calc set

		 afudc_base_used_in_calc    = beg_afudc_base + half_month_charges     + 
				decode(afudc_compound_indicator, 1, uncompounded_afudc_balance, 0),
				
		 cpi_base_used_in_calc      = beg_cpi_base   + half_month_charges_tax +
		 		decode(cpi_compound_indicator,   1, uncompounded_cpi_balance, 0),
				 
		 afudc_compounded_into_base = 
		 		decode(afudc_compound_indicator, 1, uncompounded_afudc_balance, 0),
		 
		 cpi_compounded_into_base   = 
		 		decode(cpi_compound_indicator,   1, uncompounded_cpi_balance, 0)
		 
 where budget_version = :i_budget_version
 	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.afudc_base_used_in_calc: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//********************************************************************************************
//  Update the afudc_debt, afudc_equity, and cpi.  The 2-step calc simulates the application
//  of a 1/2 month of AFUDC on closings.  The 1st piece will calculate AFUDC on any 
//  remaining base.  The 2nd piece is the 1/2 month on closings.  Add them together for the
//  total amount.  We perform a two-step method because it is easier to handle percentage
//  closings.
//********************************************************************************************
f_pp_msgs("Updating afudc_debt, afudc_equity, and cpi at " + string(now()))

if upper(i_afudc_calc_on_negative_base) = "YES" then
	
update cr_budget_afudc_calc set 

		 afudc_debt   = ((afudc_base_used_in_calc - closings)     * debt_rate   * eligible_for_afudc)
		 				    + closings     * debt_rate   * eligible_for_afudc / 2,
		 
		 afudc_equity = ((afudc_base_used_in_calc - closings)     * equity_rate * eligible_for_afudc)
		 					 + closings     * equity_rate * eligible_for_afudc / 2,
							 		 
		 cpi          = ((cpi_base_used_in_calc   - closings_tax) * cpi_rate    * eligible_for_cpi)
		 					 + closings_tax * cpi_rate    * eligible_for_cpi   / 2
									  
 where budget_version = :i_budget_version
 	and month_number = :i_month_number;
	
else

update cr_budget_afudc_calc set 

		afudc_debt   = decode(sign(
		 					 ((afudc_base_used_in_calc - closings)     * debt_rate * eligible_for_afudc)
		 					 + closings     * debt_rate * eligible_for_afudc / 2
							 ), 1, 
							 ((afudc_base_used_in_calc - closings)     * debt_rate * eligible_for_afudc)
		 					 + closings     * debt_rate * eligible_for_afudc / 2,
							 0),

		afudc_equity = decode(sign(
		 					 ((afudc_base_used_in_calc - closings)     * equity_rate * eligible_for_afudc)
		 					 + closings     * equity_rate * eligible_for_afudc / 2
							 ), 1, 
							 ((afudc_base_used_in_calc - closings)     * equity_rate * eligible_for_afudc)
		 					 + closings     * equity_rate * eligible_for_afudc / 2,
							 0),
		 
		 cpi          = decode(sign(
		 					 ((cpi_base_used_in_calc   - closings_tax) * cpi_rate    * eligible_for_cpi)
		 					 + closings_tax * cpi_rate    * eligible_for_cpi   / 2
							 ), 1, 
							 ((cpi_base_used_in_calc   - closings_tax) * cpi_rate    * eligible_for_cpi)
		 					 + closings_tax * cpi_rate    * eligible_for_cpi   / 2,
							 0)

 where budget_version = :i_budget_version
 	and month_number = :i_month_number;
	 
 end if

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.afudc_base_used_in_calc: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//********************************************************************************************
//  Update the afudc_debt_transferred, afudc_equity_transferred.
//
//  This was added to handle topics like:
//
//    1)  Transferring capitalized interest to parent entities.
//    2)  Splitting off state AFUDC vs. Ferc AFUDC.
//********************************************************************************************
if i_budget_afudc_transfers <> 0 then
	
	
	f_pp_msgs("Updating afudc_debt_transferred, afudc_equity_transferred at " + string(now()))
		
	update cr_budget_afudc_calc
	
		set afudc_debt_transferred   = round(afudc_debt   * afudc_transfer_rate, 2),
			 afudc_equity_transferred = round(afudc_equity * afudc_transfer_rate, 2)
			 
	 where budget_version = :i_budget_version
		and month_number = :i_month_number;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating afudc_debt_transferred, afudc_equity_transferred: " + &
			sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
	
	f_pp_msgs("Updating (due to transfer) afudc_debt, afudc_equity at " + string(now()))
	
	update cr_budget_afudc_calc
	
		set afudc_debt   = afudc_debt   - afudc_debt_transferred,
			 afudc_equity = afudc_equity - afudc_equity_transferred
			 
	 where budget_version = :i_budget_version
		and month_number = :i_month_number;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating afudc_debt_transferred, afudc_equity_transferred: " + &
			sqlca.SQLErrText)
		rollback;
		return -1
	end if
	
		
	commit;
	
end if


//********************************************************************************************
//  Update the total_amount_spent to include the afudc_debt and afudc_equity.
//********************************************************************************************
f_pp_msgs("Updating total_amount_spent, end_afudc_base, end_cpi_base at " + string(now()))

update cr_budget_afudc_calc

   set total_amount_spent     = total_amount_spent     + afudc_debt + afudc_equity,
	
		 total_amount_spent_tax = total_amount_spent_tax + cpi,
		 
		 end_afudc_base = beg_afudc_base + amount     - closings     + 
			 decode(afudc_compound_indicator, 1, uncompounded_afudc_balance, 0),
		 			 
		 end_cpi_base   = beg_cpi_base   + amount_tax - closings_tax + 
		 	decode(cpi_compound_indicator,    1, uncompounded_cpi_balance,   0),
		 
		 total_transferred_afudc_spent = total_transferred_afudc_spent + afudc_debt_transferred 
		 										 + afudc_equity_transferred
		 
 where budget_version = :i_budget_version
	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating total_amount_spent with afudc_debt and afudc_equity: " + &
		sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//  MOVED ABOVE.
////********************************************************************************************
////  Update the end_afudc_base and end_cpi_base.
////********************************************************************************************
//f_pp_msgs("Updating end_afudc_base, end_cpi_base at " + string(now()))
//
//update cr_budget_afudc_calc set 
//
//		 end_afudc_base = beg_afudc_base + amount     - closings     + 
//			 decode(afudc_compound_indicator, 1, uncompounded_afudc_balance, 0),
//		 			 
//		 end_cpi_base   = beg_cpi_base   + amount_tax - closings_tax + 
//		 	decode(cpi_compound_indicator,    1, uncompounded_cpi_balance,   0)
//		 		 
// where budget_version = :i_budget_version
// 	and month_number = :i_month_number;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: updating end_afudc_base, end_cpi_base: " + sqlca.SQLErrText)
//	rollback;
//	return -1
//end if
//
//commit;


//********************************************************************************************
//  Update the uncompounded_afudc_balance and uncompounded_cpi_balance.  They will be the 
//  rolling balance, except in months where the afudc or cpi is compounded, where they will
//  simply equal that month's calcuated amount.
//
//  NOTE:  These 2 variables are balances that will roll in at the BEGINNGING of a month
//         to be used in that month's calc.
//********************************************************************************************
f_pp_msgs("Updating uncompounded_afudc_balance, end_cwip at " + string(now()))

update cr_budget_afudc_calc set 
				 
		 uncompounded_afudc_balance = 
		    decode(afudc_compound_indicator, 1, afudc_debt + afudc_equity, 
					  uncompounded_afudc_balance + afudc_debt + afudc_equity),
		 
		 uncompounded_cpi_balance   = 
		 	 decode(cpi_compound_indicator, 	 1, cpi, 
					  uncompounded_cpi_balance   + cpi),
		
		 end_cwip     = beg_cwip     + amount     + afudc_debt + afudc_equity - closings,
		 
		 end_cwip_tax = beg_cwip_tax + amount_tax + cpi                       - closings_tax
		 
 where budget_version = :i_budget_version
 	and month_number = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: updating cr_budget_afudc_calc.afudc_base_used_in_calc: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


//  MOVED ABOVE.
////********************************************************************************************
////  Update the end_cwip.
////********************************************************************************************
//f_pp_msgs("Updating end_cwip at " + string(now()))
//
//update cr_budget_afudc_calc set 
//		 end_cwip     = beg_cwip     + amount     + afudc_debt + afudc_equity - closings,
//		 end_cwip_tax = beg_cwip_tax + amount_tax + cpi                       - closings_tax
// where budget_version = :i_budget_version
// 	and month_number = :i_month_number;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("ERROR: updating end_cwip: " + sqlca.SQLErrText)
//	rollback;
//	return -1
//end if
//
//commit;


//********************************************************************************************
//  Negative balance fix for SCANA.
//  --  Update a negative AFUDC base to 0 if the end_cwip = 0, implying that all dollars
//      have been closed.
//  --  The end_afudc_base would go negative by exactly the amount of uncompounded AFUDC 
//      that was closed.
//********************************************************************************************
f_pp_msgs("Zeroing out negative AFUDC base for closed projects at " + string(now()))

update cr_budget_afudc_calc set end_afudc_base = 0
 where end_cwip       = 0 and end_afudc_base < 0
 	and budget_version = :i_budget_version
 	and month_number   = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: Zeroing out negative AFUDC base for closed projects: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


f_pp_msgs("Zeroing out negative CPI base for closed projects at " + string(now()))

update cr_budget_afudc_calc set end_cpi_base = 0
 where end_cwip_tax   = 0 and end_cpi_base < 0
 	and budget_version = :i_budget_version
 	and month_number   = :i_month_number;

if sqlca.SQLCode < 0 then
	f_pp_msgs("ERROR: Zeroing out negative AFUDC base for closed projects: " + sqlca.SQLErrText)
	rollback;
	return -1
end if

commit;


return 1
end function

public function integer uf_validate (longlong a_group_by_id, longlong a_target_id, longlong a_credit_id);//******************************************************************************************
//
//  Function     :  uf_validate
//
//  Description  :  Validate the group by/target/credit relationships.  If fields are 
//                  being not retained consistantly, the allocation will break.
//
//  Arguments    :  a_group_by_id
//                  a_target_id
//	                 a_credit_id
//
//  Returns      :   1 : if the relationships are valid
//                  -1 : otherwise
//
//******************************************************************************************
string sqls, str_value, col_name
longlong num_rows, num_cols, r, c, element_id, counter

////uo_ds_top i_ds_group_by
////i_ds_group_by = CREATE uo_ds_top
//i_ds_group_by.reset()
//sqls = "select * from cr_alloc_group_by_bdg where group_by_id = " + string(a_group_by_id)
//f_create_dynamic_ds(i_ds_group_by, "grid", sqls, sqlca, true)

//uo_ds_top i_ds_target_val
//i_ds_target_val = CREATE uo_ds_top
i_ds_target_val.reset()
sqls = "select * from cr_alloc_target_criteria_bdg where target_id = " + string(a_target_id)
f_create_dynamic_ds(i_ds_target_val, "grid", sqls, sqlca, true)

//uo_ds_top i_ds_credit
//i_ds_credit = CREATE uo_ds_top
//sqls = "select * from cr_alloc_credit_criteria_bdg where credit_id = " + string(a_credit_id)
//f_create_dynamic_ds(i_ds_credit, "grid", sqls, sqlca, true)


//
//  1.  Group By / Target Relationship:
//
num_rows = i_ds_target_val.RowCount()
if num_rows < 1 then 
//	DESTROY ds_group_by
//	DESTROY ds_target
//	DESTROY ds_credit
	return 1
end if
num_cols = long(i_ds_target_val.describe("datawindow.column.count"))
if num_rows > 50 then num_rows = 0 // DMJ: 10/5/09: Performance
for r = 1 to num_rows
	for c = 1 to num_cols
		// ### 7566: JAK: 2011-05-16:  There is a new field on the target table (sort_order) that needs to be skipped as well.
		//	Added num_cols -1 to the case statement below to catch it.
		//  Do not check: target_id (1), row_id (2), time_stamp (3), user_id (4), rate (num_cols - 1), sort_order (num_cols)
		choose case c
			case 1, 2, 3, 4, num_cols - 1, num_cols
				continue
			case else
		end choose

		str_value = trim(i_ds_target_val.GetItemString(r, c))
		
		if str_value = "*" or pos(str_value, "*") > 0 then // Incrementally added the pos() for partial masking
			col_name = upper(trim(i_ds_target_val.Describe("#" + string(c) + ".Name")))
			col_name = f_replace_string(col_name, "_", " ", "all")
			
			element_id = 0
			select element_id into :element_id
			  from cr_elements
			 where upper(rtrim(replace(replace(budgeting_element,'-',' '),'/',' '))) = :col_name;

		 
			counter = 0
			select count(*) into :counter
			  from cr_alloc_group_by_bdg
			 where group_by_id = :a_group_by_id and element_id = :element_id;
			 
			if counter = 0 then
				uf_msg("  ","  ")
				uf_msg("Update - (* = Same as Source Records)", &
							  "ERROR: " + string(col_name) + " must be in the SOURCE GROUPING criteria. " + &
							  "The allocation cannot run.")
				uf_msg("  ","  ")
//				DESTROY ds_group_by
//				DESTROY ds_target
//				DESTROY ds_credit
				return -1
			end if
		end if
		
	next  //  for c = 1 to num_cols ...
next  //  for r = 1 to num_rows ...


//DESTROY ds_group_by
//DESTROY ds_target
//DESTROY ds_credit

return 1
end function

public function string uf_structure_value (longlong a_structure_id, string a_description, string a_description_edit, longlong a_not_field);//******************************************************************************************
//
//  Window Function  :  wf_structure_value
//
//  Description      :  Builds the piece of the where clause for hierarchical queries
//								that use the cr_structures.
//
//******************************************************************************************
longlong comma_pos, start_pos, counter, the_last_comma_pos, string_length, i, status
string rollup_value_id, where_clause, description_edit_values[], rollup_value_id_list, &
		 element_description


where_clause         = " "
rollup_value_id_list = " ("


//  DMJ: 1/29/09: Using functions like substr() with structures would bomb because the column
//  alias would end up looking something like "substr(project,1,4)".  Look up the element
//  description instead and use that in the appropriate places.
setnull(element_description)
select description into :element_description 
  from cr_elements
 where element_id in (select element_id from cr_structures where structure_id = :a_structure_id);
if isnull(element_description) then element_description = "NONE"
element_description = f_cr_clean_string(upper(element_description))


//-------  Parse out the rollup values into an array, if the user selected more  -----------
//-------  than 1 rollup value.                                                  -----------
uf_parse(a_description_edit, "{", description_edit_values)


for i = 1 to upperbound(description_edit_values)
	
	//-------  First, must parse out the value_id from the chosen element_value  ------------
	start_pos = 1
	counter   = 0

	if i_use_new_structures_table = "YES" then
		//  These need to be harcoded to 0 in case the element_value contains a comma.
		comma_pos = 0
		the_last_comma_pos = 0
	else
		do while true
		
			counter++
		
			//  Endless loop protection ...
			if counter > 1000 then
				messagebox("!!!", "Endless loop in wf_structure_value.", Exclamation!)
				exit
			end if
		
			comma_pos = pos(description_edit_values[i], ",", start_pos)
		
			if comma_pos = 0 then exit
		
			the_last_comma_pos = comma_pos
			start_pos          = comma_pos + 1
		
		loop
	end if

	string_length = len(description_edit_values[i])
	
	if i_use_new_structures_table = "YES" then
		//  "{CWIP}" goes to "CWIP}"
		rollup_value_id = right(description_edit_values[i], (string_length - the_last_comma_pos) - 1)
	else
		//  "{CWIP, 15}" goes to " 15}"
		rollup_value_id = right(description_edit_values[i], string_length - the_last_comma_pos)
	end if

	rollup_value_id = trim(left(rollup_value_id, len(rollup_value_id) - 1))
	
	status = 0
	if i_use_new_structures_table = "YES" then
		select status into :status from cr_structure_values2
		 where structure_id = :a_structure_id and element_value = :rollup_value_id;
	else
		select status into :status from cr_structure_values
		 where structure_id = :a_structure_id and value_id = :rollup_value_id;
	end if
		
	if isnull(status) or status = 0 then
		uf_msg("", "  ")
		uf_msg("", "ERROR: the value_id (" + rollup_value_id + ") is INACTIVE on " + &
			" structure_id (" + string(a_structure_id) + ")")
		uf_msg("", "  ")
		return "ERROR"
	end if
	
	if i_use_new_structures_table = "YES" then
		rollup_value_id = "'" + rollup_value_id + "'"
	end if

	if i = upperbound(description_edit_values) then
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ") "
	else
		rollup_value_id_list = rollup_value_id_list + rollup_value_id + ","
	end if


	//-------  Build the piece of the where clause to be passed back to the  ---------------- 
	//-------  calling script                                                ----------------
	if i = upperbound(description_edit_values) then
		
		if a_not_field = 0 then
			//  The user clicked the "NOT" checkbox.
			where_clause = where_clause + " " + a_description + " not in ("
//			i_prefixed_where = i_prefixed_where + " a." + a_description + " not in ("
		else
			where_clause = where_clause + " " + a_description + " in ("
//			i_prefixed_where = i_prefixed_where + " a." + a_description + " in ("
		end if
		
		if i = 1 then
			if i_use_new_structures_table = "YES" then
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value = " + rollup_value_id + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
//				i_prefixed_where = i_prefixed_where + &
//				 "select " + element_description + " from " + &
//					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
//					  " from cr_structure_values2 " + &
//				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
//				 " start with structure_id = " + string(a_structure_id) + " and element_value = " + rollup_value_id + &
//			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
//				 " ))"
			else
				//  Single rollup value ... use an "=" ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id = " + rollup_value_id + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
//				i_prefixed_where = i_prefixed_where + &
//				 "select " + element_description + " from " + &
//					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
//					  " from cr_structure_values " + &
//				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
//				 " start with structure_id = " + string(a_structure_id) + " and value_id = " + rollup_value_id + &
//			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
//				 " ))"
			end if
		else
			if i_use_new_structures_table = "YES" then
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values2 " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and element_value in " + rollup_value_id_list + &
			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
				 " ))"
//				i_prefixed_where = i_prefixed_where + &
//				 "select " + element_description + " from " + &
//					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
//					  " from cr_structure_values2 " + &
//				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
//				 " start with structure_id = " + string(a_structure_id) + " and element_value in " + rollup_value_id_list + &
//			  " connect by prior element_value = parent_value and structure_id = " + string(a_structure_id) + &
//				 " ))"
			else
				//  Multiple rollup values ... use an "IN" list ...
				where_clause = where_clause + &
				 "select " + element_description + " from " + &
					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
					  " from cr_structure_values " + &
				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
				 " start with structure_id = " + string(a_structure_id) + " and value_id in " + rollup_value_id_list + &
			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
				 " ))"
//				i_prefixed_where = i_prefixed_where + &
//				 "select " + element_description + " from " + &
//					"(select substr(element_value, 1, decode(instr(element_value, ':'), 0, length(element_value) + 1, instr(element_value, ':')) - 1) as " + element_description + &
//					  " from cr_structure_values " + &
//				 " where structure_id = " + string(a_structure_id) + " and detail_budget = 1 and status = 1 " + &
//				 " start with structure_id = " + string(a_structure_id) + " and value_id in " + rollup_value_id_list + &
//			  " connect by prior value_id = rollup_value_id and structure_id = " + string(a_structure_id) + &
//				 " ))"
			end if
		end if
	end if

next

return where_clause
end function

public function integer uf_loading1 (longlong a_month_number, longlong a_month_period);string allocation_sqls, sqls, field, left_paren, col_name, operator, value1, &
		 between_and, value2, right_paren, and_or, select_fields, insert_sqls, &
		 element_descr, insert_sqls_start, value, source_value, credit_sqls, &
		 credit_group_sqls, table_name, descr, inter_co_sqls, &
		 work_order_field, interco_trigger_col, interco_credit_sql, interco_trigger_col_val, &
		 interco_company_val, original_interco_credit_sql, &
		 retrieved_company_val, string_row_id, interco_company, external_company_id, &
		 interco_flex_value, cross_company_val, datawindow, report_sqls, &
		 target_co, cv, cr_elements_descr, structure_where, orig_value1, not_upper_value1, &
		 alloc_gl_jc, e_for_rates, v_for_rates, sqls_trpo, trpo_col_name, fixed_value, base_limit_detail, &
		 balancing_sqls, balancing_cv, rtn_f_create_str, group_fields, group_field, hint_syntax, rtns
longlong group_by_id, where_clause_id, num_rows, i, &
		 num_cols, j, num_inserts, k, num_credit_cols, z, source_id, &
		 trow, num_targets, rtn, where_pos, group_pos, start_month, end_month, clearing_id, &
		 interco_id, cr_company_id, co_element_id, &
		 element_id, counter, dw_id, source_sql_counter, source_sql_rows, mn, mp, &
		 rtn_code, cleanup_long_array_arg[], rate_id, dollar_quantity_indicator, &
		 rtn_f_create, structure_id, not_field, run_by_dept, auto_reverse, s_id_for_rates, &
		 row_id, find_row, mask_length, field_width, field_start, rtn_sql
// SEK 082410: Changed id, max_id, and interco_pk_id from longlong to longlong due to volume of
//		data at Southern. 
longlong id, max_id, interco_pk_id
decimal  {2} source_amount, target_amount, credit_amount, &
				 sum_of_target_amount, diff, clearing_balance, sum_of_source_amount, &
				 cleared_amount, interco_total, source_quantity, base_limit, target_qty, credit_qty, &
				 clearing_balance_qty, sum_of_source_qty, cleared_qty, sum_of_target_qty
decimal {10} rate, clearing_rate, rate_for_insert, clearing_rate_qty
boolean      custom_dw, elig_for_plug, partial_masking = false
any id_array[]
longlong id_array_pos

/*### - MDZ - 8597 - 20110809*/
longlong start_period, end_period

DECLARE credit_cursor DYNAMIC CURSOR FOR SQLSA ;

group_by_id          = 0
where_clause_id      = 0
i_target_id          = 0
i_credit_id          = 0
id                   = 0
setnull(alloc_gl_jc)
setnull(base_limit)
setnull(base_limit_detail)
i_ytd_indicator      = 0
run_by_dept          = 0
auto_reverse         = 0
i_derivation_flag    = 0
i_allocate_quantity  = 0
i_exclude_negatives  = 0
i_auto_reverse_cross_years = 0
i_prefixed_where     = ""
hint_syntax          = ""
//  DO NOT initialize base_limit to 0 !  Nulls mean something.

choose case i_clearing_indicator
	case 1, 2, 3, 6
		select gl_journal_category into :i_gl_journal_category
		  from cr_sources where upper(rtrim(description)) = 'ALLOCATIONS';
	case 4, 5
		select gl_journal_category into :i_gl_journal_category
		  from cr_sources where upper(rtrim(description)) = 'INTER-COMPANY';
end choose

// ### - SEK - 061011 - 7742: Removed hint_syntax from this table in order to utilize the base method (using pp_datawindow_hints)
//select where_clause_id, group_by_id, target_id, credit_id, description, clearing_id,
//       intercompany_accounting, intercompany_id, cr_company_id, dw_id, gl_journal_category,
//		 dollar_quantity_indicator, ytd_indicator, run_by_department, auto_reverse, 
//		 base_limit, derivation_flag, allocate_quantity, base_limit_detail, auto_reverse_cross_years,
//		 exclude_negatives, ignore_header_company, hint_syntax
//  into :where_clause_id, :group_by_id, :i_target_id, :i_credit_id, :descr, :clearing_id, &
//       :i_interco_acctg, :interco_id, :cr_company_id, :dw_id, :alloc_gl_jc,
//		 :dollar_quantity_indicator, :i_ytd_indicator, :run_by_dept, :auto_reverse,
//		 :base_limit, :i_derivation_flag, :i_allocate_quantity, :base_limit_detail, :i_auto_reverse_cross_years,
//		 :i_exclude_negatives, :i_ignore_header_co, :hint_syntax
//  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;
select where_clause_id, group_by_id, target_id, credit_id, description, clearing_id,
       intercompany_accounting, intercompany_id, cr_company_id, dw_id, gl_journal_category,
		 dollar_quantity_indicator, ytd_indicator, run_by_department, auto_reverse, 
		 base_limit, derivation_flag, allocate_quantity, base_limit_detail, auto_reverse_cross_years,
		 exclude_negatives, ignore_header_company
  into :where_clause_id, :group_by_id, :i_target_id, :i_credit_id, :descr, :clearing_id, &
       :i_interco_acctg, :interco_id, :cr_company_id, :dw_id, :alloc_gl_jc,
		 :dollar_quantity_indicator, :i_ytd_indicator, :run_by_dept, :auto_reverse,
		 :base_limit, :i_derivation_flag, :i_allocate_quantity, :base_limit_detail, :i_auto_reverse_cross_years,
		 :i_exclude_negatives, :i_ignore_header_co
  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;

select rate_id into :rate_id from cr_alloc_target_bdg
 where target_id = :i_target_id;
if isnull(rate_id) then rate_id = 0

//uf_msg("", "Running the allocation ... " + upper(descr))

if isnull(clearing_id)     then clearing_id   = 0
if isnull(i_interco_acctg) then i_interco_acctg = 0
if isnull(interco_id)      then interco_id    = 0
if isnull(cr_company_id)   then cr_company_id = 0
if isnull(dw_id)           then dw_id         = 0
if isnull(dollar_quantity_indicator) then dollar_quantity_indicator = 0
if isnull(i_ytd_indicator)           then i_ytd_indicator           = 0
if isnull(run_by_dept)   then run_by_dept   = 0
if isnull(auto_reverse)  then auto_reverse  = 0
if isnull(i_derivation_flag) then i_derivation_flag = 0
if isnull(i_allocate_quantity) then i_allocate_quantity = 0
if isnull(i_exclude_negatives)       then i_exclude_negatives = 0
if isnull(i_auto_reverse_cross_years) then i_auto_reverse_cross_years = 0
//  DO NOT set null base_limit to 0 !  Nulls mean something.
if isnull(i_ignore_header_co) then i_ignore_header_co = 0
if isnull(hint_syntax)        then hint_syntax        = ""

if alloc_gl_jc = " " then
	//  Keep it, allowing for a single space.
	i_gl_journal_category = alloc_gl_jc
else
	if isnull(alloc_gl_jc) or trim(alloc_gl_jc) = "" then
		//  Keep the one from cr_sources.
	else
		i_gl_journal_category = alloc_gl_jc
	end if
end if

if dw_id > 0 then
	custom_dw = true
	select datawindow into :datawindow
	  from cr_alloc_custom_dw_bdg where dw_id = :dw_id;
else
	custom_dw = false
end if

//  BUDGET ALLOCATIONS:
if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
	source_id = 0
	select source_id into :source_id from cr_sources
	 where upper(rtrim(description)) = 'INTER-COMPANY';
	if isnull(source_id) or source_id = 0 then
		uf_msg("Budget Allocations", &
	          "No SOURCE_ID was found in CR_SOURCES for INTER-COMPANY.  " + &
				 "No allocation will be calculated.")
		rollback using sqlca;
		return -1
	end if
else
	source_id = 0
	select source_id into :source_id from cr_sources
	 where upper(rtrim(description)) = 'ALLOCATIONS';
	if isnull(source_id) or source_id = 0 then
		uf_msg("Budget Allocations", &
	          "No SOURCE_ID was found in CR_SOURCES for ALLOCATIONS.  " + &
				 "No allocation will be calculated.")
		rollback using sqlca;
		return -1
	end if
end if


//  VERIFY the grouping/target and target/credit relationships ... if there are fields
//  being retained in the targets or credits that are not retained in the group by or
//  target, the allocation will break.
rtn = uf_validate(group_by_id, i_target_id, i_credit_id)

if rtn <> 1 then
	return -1
end if


//  CR_SYSTEM_CONTROL lookups:
setnull(work_order_field)
select upper(control_value) into :work_order_field
  from cr_system_control where upper(control_name) = 'WORK ORDER FIELD';
	
if isnull(work_order_field) or work_order_field = "" then
	uf_msg("Work Order Field", &
	           "No WORK ORDER FIELD was found in cr_system_control ... " + &
				  "No allocation will be calculated.")
	rollback using sqlca;
	return -1
end if

work_order_field = f_cr_clean_string(work_order_field)

//  9/17/07:  CR System Control record needed for "Run by Dept" allocations.
rtn = uf_run_by_department_lookup_field()
if rtn <> 1 then
	rollback using sqlca;
	return -1
end if


//  DMJ: 8/18/08: MOVED TO AN INSTANCE DS.
//uo_ds_top i_ds_elements
//i_ds_elements = CREATE uo_ds_top
//i_ds_elements.DataObject = "dw_cr_element_definitions"
//i_ds_elements.SetTransObject(sqlca)
//i_num_elements = i_ds_elements.RETRIEVE()


//  If intercompany accounting is turned on, then the company field 
//  must be in the group_by.
if i_interco_acctg = 1 then
	for i = 1 to i_ds_elements.RowCount()
		cr_elements_descr = upper(trim(i_ds_elements.GetItemString(i, "budgeting_element")))
		cr_elements_descr = f_replace_string(cr_elements_descr, " ", "_", "all")
		if cr_elements_descr = f_replace_string(i_company_field, " ", "_", "all") then
			co_element_id = i_ds_elements.GetItemNumber(i, "element_id")
			exit
		end if
	next

	element_id = 0
	select element_id into :element_id
	  from cr_alloc_group_by_bdg
	 where group_by_id = :group_by_id and element_id = :co_element_id;
	if isnull(element_id) then element_id = 0

	if element_id = 0 then
		uf_msg(descr, &
	   	        "This allocation requires intercompany transactions ... " + &
					  "The " + upper(i_company_field) + " field must be in the source grouping. " + &
					  "No allocation will be calculated.")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
end if

/*### - MDZ - 11534 - 20121120*/
/*MOVED UP PRIOR TO CLEARING BALANCE*/
//********************************
//
//  Reverse last month:  
//
//********************************
if i_elim_flag = 1 then auto_reverse = 0
if auto_reverse = 1 then
	uf_msg("", "Auto Reverse = YES ... reversing last month's entry at " + string(now()))
	rtn_code = uf_auto_reverse()
	if rtn_code < 0 then
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
	uf_msg("", "Auto Reverse complete at " + string(now()))
end if

//  A Type 3 allocation "Clearing - Calc Rate" requires a pot of dollars to be held
//  in a separate variable.
if i_clearing_indicator = 3 then
	clearing_balance = uf_clearing_balance(cr_company_id, i_company_field)
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	if i_errors then return clearing_balance
	if isnull(clearing_balance) then clearing_balance = 0
	if clearing_balance = 0 then
		select external_company_id into :external_company_id
  	     from cr_company where cr_company_id = :cr_company_id;
		uf_msg("Allocation - " + descr, &
					  "The balance to be cleared is 0. Company = " + external_company_id + &
					  ". Cannot run this allocation.")		
		rtn_code = 0
		goto commit_updates
	end if
	if i_allocate_quantity = 1 then
		clearing_balance_qty = uf_clearing_balance_qty(cr_company_id, i_company_field)
		// ### 8142: JAK: 2011-11-01:  Consistent return codes
		if i_errors then 	return clearing_balance_qty
		if isnull(clearing_balance_qty) then clearing_balance_qty = 0
		//  No check for 0 balance on quantity
	end if
end if

//********************************
//
//  Sources and Targets:  
//
//********************************
//if not i_msgbox then
//	uf_msg("", string(i_allocation_id) + ": Retrieving sources records at " + &
//	           string(today(), "mm/dd/yyyy hh:mm:ss"))
//end if

//uo_ds_top i_ds_group_by
//i_ds_group_by = CREATE uo_ds_top
i_ds_group_by.reset()

// DMJ: 1/5/10: MEMORY LEAK:
//i_ds_group_by.DataObject = "dw_temp_dynamic"
//i_ds_group_by.SetTransObject(sqlca)

sqls = "select cr_alloc_group_by_bdg.element_id, cr_elements.budgeting_element, cr_alloc_group_by_bdg.mask_length, cr_elements.width " + &
		 "from cr_alloc_group_by_bdg , cr_elements " + &
		 "where group_by_id = " + string(group_by_id) + " and " + &
		 "cr_alloc_group_by_bdg.element_id = cr_elements.element_id"

if isnull(sqls) then
	uf_msg(descr, "The GROUP BY SQL is NULL ... No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

// DMJ: 1/5/10: MEMORY LEAK:
//rtn_f_create_str = f_create_dynamic_ds(i_ds_group_by, "grid", sqls, sqlca, true)
rtn_sql = i_ds_group_by.SetSQLSelect(sqls)
rtn_sql = i_ds_group_by.RETRIEVE()

// DMJ: 1/5/10: MEMORY LEAK:
//if rtn_f_create_str <> "OK" then
if rtn_sql < 1 then
	uf_msg(descr, "  " + '  ')
	uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_group_by ...')
	uf_msg(descr, "  " + '  ')
	uf_msg(descr, "sqls: " + 'sqls = ' + sqls)
	uf_msg(descr, "  " + '  ')
end if

num_rows = i_ds_group_by.RowCount()

allocation_sqls = "select "

for i = 1 to num_rows

	field = upper(trim(i_ds_group_by.GetItemString(i, 2)))
	field = f_cr_clean_string(field)
	mask_length = i_ds_group_by.GetItemNumber(i, 3)
	field_width = i_ds_group_by.GetItemNumber(i, 4)
	group_field = upper(trim(i_ds_group_by.GetItemString(i, 2)))
	group_field = f_cr_clean_string(group_field)
	
	if mask_length < 0 then
		partial_masking = true
		mask_length = abs(mask_length)
		field_start = field_width - mask_length + 1
		field       = "'" + fill("0", field_width - mask_length) + "'" + '||substr("' + field + '",' + string(field_start) + "," + string(mask_length) + ') as "' + field + '"'
		group_field = "'" + fill("0", field_width - mask_length) + "'" + '||substr("' + group_field + '",' + string(field_start) + "," + string(mask_length) + ')'
		if i = 1 then
			select_fields = select_fields + '' + field + ''
			group_fields  = group_fields  + '' + group_field + ''
		else
			select_fields = select_fields + ', ' + field + ''
			group_fields  = group_fields  + ', ' + group_field + ''
		end if
		continue
	end if
	
	if mask_length > 0 then
		partial_masking = true
		field       = 'substr("' + field + '",1,' + string(mask_length) + ")" + fill("0", field_width - mask_length) + ' as "' + field + '"'
		group_field = 'substr("' + group_field + '",1,' + string(mask_length) + ")" + fill("0", field_width - mask_length)
		if i = 1 then
			select_fields = select_fields + '' + field + ''
			group_fields  = group_fields  + '' + group_field + ''
		else
			select_fields = select_fields + ', ' + field + ''
			group_fields  = group_fields  + ', ' + group_field + ''
		end if
		continue
	end if
	
	if i = 1 then
		select_fields = select_fields + '"' + field + '"'
		group_fields  = group_fields  + '"' + group_field + '"'
	else
		select_fields = select_fields + ', "' + field + '"'
		group_fields  = group_fields  + ', "' + group_field + '"'
	end if
	
next

//  Add the amount ... Type 3 "Clearing - Calc Rate" should not include mn & mp ... it will
//							  create an unneccessary amount of extra records ...
choose case i_clearing_indicator
	case 3
		if isnull(select_fields) or trim(select_fields) = "" then
			allocation_sqls = allocation_sqls + &
									" budget_version, sum(amount) amount, sum(quantity) quantity from cr_budget_data "		
		else
			if isnull(base_limit) then  //  Null means "no base_limit"
				allocation_sqls = allocation_sqls + &
										select_fields + &
										", budget_version, sum(amount) amount, sum(quantity) quantity from cr_budget_data "		
			else
				//  I.E.  Either the total amount by work order, or, the base limit if that is < the total amount,
				//  ratioed back to the other levels of detail based on their original ratio.
				//  NULLIF is to get around any div-by-zero errors where the parts add to zero by wo, company ... nvl'ing
				//  to 1 then makes the base = to the sum(amount) since the ratio is 1 over 1.
				allocation_sqls = allocation_sqls + &
										select_fields + ", budget_version, " + &
										"round(" + &
											"nvl(nullif(" + &
												"least(sum(sum(amount)) over(partition by " + base_limit_detail + " order by " + base_limit_detail + "), " + &
												 string(base_limit) + "), " + &
											"0), 1) " + &
											"* " + &
											"sum(amount) " + &
											"/ " + &
											"nvl(nullif( sum(sum(amount)) over(partition by " + base_limit_detail + " order by " + base_limit_detail + "), 0 ), 1) " + &
										", 2) " + " amount, " + &
										"sum(quantity) quantity from cr_budget_data "
			end if
		end if
	case 5
		if i_run_as_a_test then
			if isnull(select_fields) or trim(select_fields) = "" then
				if i_whole_month then
					/*### - MDZ - 29485 - 20130311*/
					/*Use the i_month_number variable instead of hardcoding the month number*/
					allocation_sqls = allocation_sqls + &
					                  		", " + string(i_month_number) + " month_number, " + string(i_month_period) + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				else
					/*### - MDZ - 29485 - 20130311*/
					allocation_sqls = allocation_sqls + &
					                  		", " + string(i_month_number) + " month_number, month_period "  + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				end if
			else
				if i_whole_month then
					/*### - MDZ - 29485 - 20130311*/
					allocation_sqls = allocation_sqls + &
											select_fields + &
											", " + string(i_month_number) + " month_number, " + string(i_month_period) + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				else
					/*### - MDZ - 29485 - 20130311*/
					allocation_sqls = allocation_sqls + &
											select_fields + &
											", " + string(i_month_number) + " month_number, month_period" + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				end if
			end if
		else
			if isnull(select_fields) or trim(select_fields) = "" then
				if i_whole_month then
					/*### - MDZ - 29485 - 20130311*/
					allocation_sqls = allocation_sqls + &
					                  		", " + string(i_month_number) + " month_number, " + string(i_month_period) + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				else
					/*### - MDZ - 29485 - 20130311*/
					allocation_sqls = allocation_sqls + &
					                  		", " + string(i_month_number) + " month_number, month_period" + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				end if
			else
				if i_whole_month then
					/*### - MDZ - 29485 - 20130311*/
					allocation_sqls = allocation_sqls + &
											select_fields + &
											", " + string(i_month_number) + " month_number, " + string(i_month_period) + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				else
					/*### - MDZ - 29485 - 20130311*/
					allocation_sqls = allocation_sqls + &
											select_fields + &
											", " + string(i_month_number) + " month_number, month_period" + &
											", budget_version " + &
											", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
				end if
			end if
		end if
	case else
		if i_whole_month then
			/*### - MDZ - 29485 - 20130311*/
			allocation_sqls = allocation_sqls + &
									select_fields + &
									", " + string(i_month_number) + " month_number, " + string(i_month_period) + &
									", budget_version " + &
									", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
		else
			/*### - MDZ - 29485 - 20130311*/
			allocation_sqls = allocation_sqls + &
									select_fields + &
									", " + string(i_month_number) + " month_number, " + string(i_month_period) + &
									", budget_version " + &
									", sum(amount) amount, sum(quantity) quantity from cr_budget_data "
		end if
end choose
						
//  Add the month_number and month_period args ... 
//  Type 3: The user enters the start_month and end_month 
//  for the source records (the records the factors will be calculated on).
//  Note that the Type 3 allocations will not have month_period in the where_clause,
//  only a month_number restriction.  Otherwise, the where clause gets too complicated.
if i_clearing_indicator <> 3 then
	start_month = a_month_number
	end_month = a_month_number
	
	start_period = a_month_period
	end_period = a_month_period
	
	if i_ytd_indicator = 1 then
		/*### - MDZ - 29485 - 20130307*/ 
		/*Use the i_fy_start_month variable so this will work with fiscal year clients*/
		start_month = i_fy_start_month
		start_period = 1
	end if
	
	if i_whole_month then
		allocation_sqls = allocation_sqls + &
							  " where amount_type in (2,5) " + &
							    " and budget_version = '" + i_budget_version + "'" + &
							    " and (month_number >= " + string(start_month) + " and month_number <= " + string(end_month) + ")"
		i_prefixed_where = i_prefixed_where + &
							  " where a.amount_type in (2,5) " + &
							    " and a.budget_version = '" + i_budget_version + "'" + &
							    " and (a.month_number >= " + string(start_month) + " and a.month_number <= " + string(end_month) + ")"
	else
		allocation_sqls = allocation_sqls + &
							  " where amount_type in (2,5) " + &
							    " and budget_version = '" + i_budget_version + "'" + &
							    " and ((month_number <= " + string(end_month) + &
							    " and  month_period <= " + string(end_period) + ") " + &
							    " and (month_number >= " + string(start_month) + &
							    " and month_period >= " + string(start_period) + "))"
		i_prefixed_where = i_prefixed_where + &
							  " where a.amount_type in (2,5) " + &
							    " and a.budget_version = '" + i_budget_version + "'" + &
							    " and ((a.month_number <= " + string(end_month) + &
							    " and  a.month_period <= " + string(end_period) + ") " + &
							    " and (a.month_number >= " + string(start_month) + &
							    " and a.month_period >= " + string(start_period) + "))"
	end if
else
	start_month = 0
	end_month   = 0
	
	select clearing_factor_start_month, clearing_factor_end_month 
	  into :start_month, :end_month
	  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;
	
	//  The user chose "Current" as the month for the rates...
	if start_month = 0 then start_month = i_month_number
	if   end_month = 0 then   end_month = i_month_number
	
	if i_ytd_indicator = 1 then
		/*### - MDZ - 29485 - 20130307*/
		/*Use the i_fy_start_month variable so this will work with fiscal year clients*/
		start_month = i_fy_start_month
	end if
	
	if start_month = 0 or isnull(start_month) or end_month = 0 or isnull(end_month) then
		uf_msg("Error", "  ")
		uf_msg("Error", "ERROR - start_month, end_month = 0 ...")
		uf_msg("Error", "  ")
		rollback using sqlca;
		rtn_code = -1	
		goto commit_updates
	end if
	
	i_clearing_periods_wc = " where amount_type in (2,5) " + &
	                          " and budget_version = '" + i_budget_version + "'" + &
	                          " and (month_number between " + string(start_month) + &
	                          " and " + string(end_month) + ") "

	allocation_sqls = allocation_sqls + i_clearing_periods_wc
end if

//  Where Clause:
//uo_ds_top i_ds_where_clause
//i_ds_where_clause = CREATE uo_ds_top
i_ds_where_clause.reset()

// DMJ: 1/5/10: MEMORY LEAK:
//i_ds_where_clause.DataObject = "dw_temp_dynamic"
//i_ds_where_clause.SetTransObject(sqlca)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(where_clause_id) + " order by row_id"

if isnull(sqls) then
	uf_msg(descr, "The WHERE CLAUSE SQL is NULL ... " + &
				  "No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

// DMJ: 1/5/10: MEMORY LEAK:
//rtn_f_create_str = f_create_dynamic_ds(i_ds_where_clause, "grid", sqls, sqlca, true)
i_ds_where_clause.SetSQLSelect(sqls)
rtn_sql = i_ds_where_clause.RETRIEVE()

// DMJ: 1/5/10: MEMORY LEAK:
//if rtn_f_create_str <> "OK" then
if rtn_sql < 1 then
	uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_where_clause ...')
end if

num_rows = i_ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		allocation_sqls  = allocation_sqls  + " and ( "
		i_prefixed_where = i_prefixed_where + " and ( "
	end if
	
	left_paren  = upper(trim(i_ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(i_ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(i_ds_where_clause.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(i_ds_where_clause.GetItemString(i, "value1"))
	orig_value1 = upper(i_ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{" then
			value1 = not_upper_value1
		else
			value1 = upper(trim(i_ds_where_clause.GetItemString(i, "value1")))
		end if
	end if
	
	between_and = upper(trim(i_ds_where_clause.GetItemString(i, "between_and")))
	value2      = upper(trim(i_ds_where_clause.GetItemString(i, "value2")))
	right_paren = upper(trim(i_ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(i_ds_where_clause.GetItemString(i, "and_or")))
	structure_id = i_ds_where_clause.GetItemNumber(i, "structure_id")
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0

	//  If they manually entered an Oracle function, take it as-is.
	if pos(col_name, "(") = 0 then
		col_name = f_cr_clean_string(col_name)
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = &
			uf_structure_value(structure_id, col_name, value1, not_field)
		if structure_where = "ERROR" then
			rollback using sqlca;
			return -1
		end if
//		allocation_sqls = allocation_sqls + structure_where + " and "  //  BAD !!!
		allocation_sqls = &
			allocation_sqls + left_paren + structure_where + right_paren + " " + and_or + " "
		i_prefixed_where = &
			i_prefixed_where + left_paren + " a." + trim(structure_where) + right_paren + " " + and_or + " "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				allocation_sqls = allocation_sqls + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
				i_prefixed_where = i_prefixed_where + &
										left_paren + ' a.' + col_name + ' ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
			else
				allocation_sqls = allocation_sqls + &
										left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
				i_prefixed_where = i_prefixed_where + &
										left_paren + ' a."' + col_name + '" ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
			end if
		else
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				allocation_sqls = allocation_sqls + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
				i_prefixed_where = i_prefixed_where + &
										left_paren + ' a.' + col_name + ' ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
			else
				allocation_sqls = allocation_sqls + &
										left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
				i_prefixed_where = i_prefixed_where + &
										left_paren + ' a."' + col_name + '" ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
			end if
		end if
	else
		if pos(col_name, "(") <> 0 then
			//  NO DOUBLE QUOTES ON COL_NAME!
			allocation_sqls = allocation_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
			i_prefixed_where = i_prefixed_where + &
									left_paren + ' a.' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
		else
			allocation_sqls = allocation_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
			i_prefixed_where = i_prefixed_where + &
									left_paren + ' a."' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
		end if
	end if
	
	bottom:
	
	if i = num_rows then
		allocation_sqls = allocation_sqls + " )"
		i_prefixed_where = i_prefixed_where + " )"
	end if
	
next


//  Type 4:  Inter-Company (Trigger) ... must add the "Inter-Company criteria" to the
//           where clause ... this is defined in cr_system_control ... the SQL must begin
//           like "select id from cr_budget_data where ...", returning the id's that
//           pass the inter-company criteria ... 
if i_clearing_indicator = 4 then
	
	inter_co_sqls = ""
	select control_value into :inter_co_sqls
	  from cr_system_control where upper(control_name) = 'BUDGET INTER-COMPANY SQL';

	if isnull(inter_co_sqls) or trim(inter_co_sqls) = "" then
		uf_msg("Inter-Company (Trigger)", &
		           "No inter-company criteria was found in cr_system_control ... " + &
					  "No allocation will be calculated.")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
	
	/*### - MDZ - 29485 - 20130307*/
	/*Make it work for fiscal year clients as well*/
	start_month = i_month_number
	end_month = i_month_number
	
	if i_ytd_indicator = 1 then
		start_month = i_fy_start_month
	end if
	
	allocation_sqls = allocation_sqls + " and id in (" + inter_co_sqls + &
	" and budget_version = '" + i_budget_version + "'" + &
	" and month_number <= " + string(end_month) + &
	" and month_number >= " + string(start_month) + &
	" and amount_type in (2,5))"
	
	i_prefixed_where = i_prefixed_where + " and a.id in (" + inter_co_sqls + &
	" and a.budget_version = '" + i_budget_version + "'" + &
	" and a.month_number <= " + string(end_month) + &
	" and a.month_number >= " + string(start_month) + &
	" and a.amount_type in (2,5))"

	//  We will also need this for the credit code ...
	interco_trigger_col = ""
	select control_value into :interco_trigger_col
	  from cr_system_control 
	 where upper(control_name) = 'INTER-COMPANY TRIGGER FIELD';
		  
	if isnull(interco_trigger_col) or trim(interco_trigger_col) = "" then
		uf_msg("Inter-Company (Trigger)", &
		           "No INTER-COMPANY TRIGGER FIELD was found in cr_system_control ... " + &
					  "No allocation will be calculated.")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if	
	
	interco_trigger_col = f_cr_clean_string(interco_trigger_col)

	//  We will need this too ...
	original_interco_credit_sql = ""
	select control_value into :original_interco_credit_sql
	  from cr_system_control 
	 where upper(control_name) = 'INTER-COMPANY CREDIT COMPANY SQL';
		
	if isnull(original_interco_credit_sql) or trim(original_interco_credit_sql) = "" then
		uf_msg("Inter-Company (Trigger)", &
		           "No INTER-COMPANY CREDIT COMPANY SQL was found in " + &
					  "cr_system_control ... No allocation will be calculated.")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if		
	
end if


//  Type 6:  Capital Loading ... The user can set up an allocation that will use the 
//           parameters that are set up in the clearing_wo_control table.
if i_clearing_indicator = 6 or i_apply_wo_types = 1 then
	
	//  Sets i_wo_sqls = to a big select statement ...
	uf_capital_work_orders(clearing_id)
	
	//  OLD TECHNIQUE BEFORE THE CR_WO_CLEAR TABLE:
	//allocation_sqls = allocation_sqls + ' and "' + work_order_field + '" in (' + &
	//                  i_wo_sqls + ")"
	
	i_wo_field_for_uf_cap = work_order_field
	
	f_cr_allocations_custom_bdg("before_uf_capital_work_orders_sqls")
	
	//  NEW TECHNIQUE USING THE NEW CR_WO_CLEAR TABLE:
	/*### - MDZ - 29485 - 20130307*/
	/*Use cr_wo_clear_bdg instead of cr_wo_clear*/
	allocation_sqls = allocation_sqls + ' and "' + i_wo_field_for_uf_cap + '" in (' + &
		" select work_order_number from cr_wo_clear_bdg " + &
		 " where allocation_id = " + string(i_allocation_id) + ") "
	i_prefixed_where = i_prefixed_where + ' and a."' + i_wo_field_for_uf_cap + '" in (' + &
		" select work_order_number from cr_wo_clear_bdg " + &
		 " where allocation_id = " + string(i_allocation_id) + ") "
	
	f_cr_allocations_custom_bdg("after_uf_capital_work_orders_sqls")
	
end if


//  CR_COMPANY_ID:  ADD the company to the where clause if the user picked one ...
report_sqls = allocation_sqls
if not isnull(cr_company_id) and cr_company_id <> 0 then
	if i_ignore_header_co = 1 then
		// Don't add cr_company_id to the where clause
	else
		select external_company_id into :external_company_id
		  from cr_company where cr_company_id = :cr_company_id;
		allocation_sqls = allocation_sqls + " and ( " + i_company_field + " = '" + &
								upper(trim(external_company_id)) + "' ) "
		i_prefixed_where = i_prefixed_where + " and ( a." + i_company_field + " = '" + &
								upper(trim(external_company_id)) + "' ) "
		if i_cr_company_id <> 10000 then 
			report_sqls = allocation_sqls
		end if
	end if
end if


//  GROUP BY ... Type 3 "Clearing - Calc Rate" should not include mn & mp ... it will
//					  create an unneccessary amount of extra records ...
choose case i_clearing_indicator
	case 3
		if isnull(select_fields) or trim(select_fields) = "" then
			//  Nothing to group by ...
		else
			if partial_masking then
				allocation_sqls = allocation_sqls + " group by " + group_fields + &
										", budget_version"
				report_sqls     = report_sqls     + " group by " + group_fields + &
										", budget_version"
			else
				allocation_sqls = allocation_sqls + " group by " + select_fields + &
										", budget_version"
				report_sqls     = report_sqls     + " group by " + select_fields + &
										", budget_version"
			end if
		end if
	case else
		if i_whole_month then
		/*### - MDZ - 29485 - 20130311*/
		/*Since we can do ytd  now,  we need to change the month_number to i_month_number below*/ 
			if partial_masking then
				allocation_sqls = allocation_sqls + " group by " + group_fields + &
										", " + string(i_month_number) + ", budget_version "
				report_sqls     = report_sqls     + " group by " + group_fields + &
										", " + string(i_month_number) + ", budget_version "
			else
				allocation_sqls = allocation_sqls + " group by " + select_fields + &
										", " + string(i_month_number) + ", budget_version "
				report_sqls     = report_sqls     + " group by " + select_fields + &
										", " + string(i_month_number) + ", budget_version "
			end if
		else
			if partial_masking then
				allocation_sqls = allocation_sqls + " group by " + group_fields + &
										", " + string(i_month_number) + ", month_period, budget_version "
				report_sqls     = report_sqls     + " group by " + select_fields + &
										", " + string(i_month_number) + ", month_period, budget_version "
			else
				allocation_sqls = allocation_sqls + " group by " + group_fields + &
										", " + string(i_month_number) +", month_period, budget_version "
				report_sqls     = report_sqls     + " group by " + select_fields + &
										", " + string(i_month_number) + ", month_period, budget_version "
			end if
		end if
end choose

//uo_ds_top i_ds_source
//i_ds_source = CREATE uo_ds_top
i_ds_source.reset()

if custom_dw then
	i_ds_source.DataObject = datawindow
else
	i_ds_source.DataObject = "dw_temp_dynamic"
end if
i_ds_source.SetTransObject(sqlca)

//  If the user did not pick any "group by" fields, the SQL will look like 
//  (SELECT , month_number ... GROUP BY , month_number ...).  Use f_replace_string
//  to correct this.
allocation_sqls = f_replace_string(allocation_sqls, "select ,",   "select ",   "first")
allocation_sqls = f_replace_string(allocation_sqls, "group by ,", "group by ", "first")
report_sqls     = f_replace_string(report_sqls,     "select ,",   "select ",   "first")
report_sqls     = f_replace_string(report_sqls,     "group by ,", "group by ", "first")

//
//  Apply the "exclude negatives" clause if needed.
//
if i_exclude_negatives = 1 then
	allocation_sqls = allocation_sqls + " having sum(amount) > 0 "
	report_sqls     = report_sqls     + " having sum(amount) > 0 "
end if

if isnull(allocation_sqls) then
	uf_msg(descr, "The SOURCE SQL is NULL ... Company = " + external_company_id + &
					  ". No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

////GOTO RUNNING_FAST
if custom_dw then
	i_ds_source.RETRIEVE(i_month_number, i_month_period, i_allocation_id, i_budget_version)
	i_ds_source.GroupCalc()
else
	// ### 10906: JAK: 2012-09-11:  Move f_sql_add_hint lower so it can be applied to latter select statements	
	// ### - SEK - 061311 - 7742: Look up and add any hints using f_sql_add_hint
	hint_syntax = 'bud_alloc_source_' + string(i_allocation_id)
	rtns = f_sql_add_hint(allocation_sqls, hint_syntax)
	
	if isnull(rtns) or rtns = "" then
		uf_msg(descr,"  ")
		uf_msg(descr,"ERROR: Adding hint '" + hint_syntax + "'")
		uf_msg(descr," THE ALLOCATION WILL CONTINUE TO RUN ")
		uf_msg(descr,"  ")
	else 
		allocation_sqls = rtns
	end if
	
	execute immediate :allocation_sqls using sqlca;
	if sqlca.SQLCode < 0 then
	uf_msg(descr, "The SOURCE SQL syntax is invalid ... " + &
				  "No allocation will be calculated.")
	uf_msg(descr, "BAD SYNTAX = " + allocation_sqls)
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
	end if
	rtn_f_create_str = f_create_dynamic_ds(i_ds_source, "grid", allocation_sqls, sqlca, true)
	if rtn_f_create_str <> "OK" then
		uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_source ...')
	end if
end if
////RUNNING_FAST:
//  i_ds_source now contains the source records which the rate must be applied to ...


i_source_sqls = uf_save_report_sql(report_sqls ,custom_dw)

//  Save the grouping SQL for reporting ...
i_grouping_sqls = uf_save_report_sql_grouping(report_sqls, custom_dw)


f_pp_msgs("Inserting Targets at " + string(now()))
//  Get the cr_alloc_target_criteria_bdg record for this allocation type ... Note that this
//  type (loading1) is assuming only 1 target record ...
//uo_ds_top i_ds_target
//i_ds_target = CREATE uo_ds_top
i_ds_target.reset()

// DMJ: 1/5/10: MEMORY LEAK:
//i_ds_target.DataObject = "dw_temp_dynamic"

// DMJ: 1/5/10: MEMORY LEAK:
//uo_ds_top ds_target_trpo
//ds_target_trpo = CREATE uo_ds_top
//ds_target_trpo.DataObject = "dw_temp_dynamic"
i_ds_target_trpo.reset()

sqls = "select * from cr_alloc_target_criteria_bdg where target_id = " + string(i_target_id)
sqls_trpo = "select * from cr_alloc_target_criteria_t_bdg where target_id = " + string(i_target_id)

if isnull(sqls) then
	uf_msg(descr, "The TARGET SQL is NULL ... No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

if isnull(sqls_trpo) then
	uf_msg(descr, "The TARGET SQL (TRPO) is NULL ... No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

// DMJ: 1/5/10: MEMORY LEAK:
//rtn_f_create_str = f_create_dynamic_ds(i_ds_target, "grid", sqls, sqlca, true)
i_ds_target.SetSQLSelect(sqls)
rtn_sql = i_ds_target.RETRIEVE()

// DMJ: 1/5/10: MEMORY LEAK:
//if rtn_f_create_str <> "OK" then
if rtn_sql < 0 then
	uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_target ...')
end if

// DMJ: 1/5/10: MEMORY LEAK:
//rtn_f_create_str = f_create_dynamic_ds(ds_target_trpo, "grid", sqls_trpo, sqlca, true)
i_ds_target_trpo.SetSQLSelect(sqls_trpo)
rtn_sql = i_ds_target_trpo.RETRIEVE()

//if rtn_f_create_str <> "OK" then
if rtn_sql < 0 then
	uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(ds_target_trpo ...')
end if

if run_by_dept = 1 and i_clearing_indicator = 2 then
	//  9/17/07: i_ds_target needs to be sorted by the department field so we can plug rounding
	//  for each "department".  Also sort by the rate so the largest amount gets the plug.
	i_ds_target.SetSort(i_department_field + " a, rate a")
	i_ds_target.Sort()
end if

num_cols    = long(i_ds_target.describe("datawindow.column.count"))
num_targets = i_ds_target.RowCount()

if custom_dw then num_targets = 1

num_rows = i_ds_source.RowCount()

////GOTO RUNNING_FAST2
//  If there are no source records, then scram ...
if num_rows < 1 then 
	uf_msg("allocation_id = " + string(i_allocation_id), &
		"There are no records that meet the source criteria. " + &
		"Company = " + external_company_id + ". No allocation will be calculated.")
	if g_debug = 'YES' then
		uf_msg("allocation_id = " + string(i_allocation_id), &
			"SOURCE CRITERIA SYNTAX = " + allocation_sqls)
	end if
	rtn_code = 0
	goto commit_updates
end if
////RUNNING_FAST2:

//  Begin building the SQL INSERT ...
if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
	if i_run_as_a_test then
		insert_sqls_start = 'insert into cr_budget_data_test ("ID'
		i_insert_table    = "cr_budget_data_test"
	else
		insert_sqls_start = 'insert into cr_budget_data ("ID'
		i_insert_table    = "cr_budget_data"
	end if
else                                                          //  Other Allocations ...
	if i_run_as_a_test then
		insert_sqls_start = 'insert into cr_budget_data_test ("ID'
		i_insert_table    = "cr_budget_data_test"
	else
		insert_sqls_start = 'insert into cr_budget_data ("ID'
		i_insert_table    = "cr_budget_data"
	end if
end if

for i = 1 to i_num_elements
	element_descr     = upper(trim(i_ds_elements.GetItemString(i, "budgeting_element")))
	element_descr     = f_replace_string(element_descr, " ", "_", "all")
	insert_sqls_start = insert_sqls_start + '","' + element_descr
	if i_suspense_accounting = "YES" then
		//  Place the "orig" field right next to its ACK counterpart.
		insert_sqls_start = insert_sqls_start + '","ORIG_' + element_descr
	end if
next

if i_rate_field_exists then
	insert_sqls_start = insert_sqls_start + '","ALLOCATION_RATE'
end if

insert_sqls_start = insert_sqls_start + '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
                                        ',"INCEPTION_TO_DATE_AMOUNT"' + &
									 				 ',"MONTH_NUMBER","MONTH_PERIOD"' + &
													 ',"SOURCE_ID","BUDGET_VERSION"' + &
													 ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
									 				 ',"ALLOCATION_ID","TARGET_CREDIT",' + &
													 '"CROSS_CHARGE_COMPANY") values ('

//  Type 3 allocation "Clearing - Calc Rate" ... Get the denominator for the rate ...
if i_clearing_indicator = 3 and not custom_dw then
	sum_of_source_amount = 0
	sum_of_source_qty    = 0
	for i = 1 to num_rows  //  Loop over source records ...
		source_amount = i_ds_source.GetItemNumber(i, "amount")
		source_quantity = i_ds_source.GetItemNumber(i, "quantity")
//		if not isnull(base_limit) then  //  Null means "no base_limit"
//			if source_amount > base_limit then source_amount = base_limit
//		end if
		if i_exclude_negatives = 1 and source_amount < 0 then
			//  Negatives not included.
		else
			sum_of_source_amount = sum_of_source_amount + source_amount
		end if
		if i_exclude_negatives = 1 and source_quantity < 0 then
			//  Negatives not included.
		else
			sum_of_source_qty    = sum_of_source_qty    + source_quantity
		end if
	next
	if dollar_quantity_indicator = 1 then
		if sum_of_source_qty = 0 then
			clearing_rate = 0
		else
			clearing_rate = clearing_balance / sum_of_source_qty
		end if
	else
		if sum_of_source_amount = 0 then
			clearing_rate = 0
		else
			clearing_rate = clearing_balance / sum_of_source_amount
		end if
	end if
	if sum_of_source_qty = 0 then
		clearing_rate_qty = 0
	else
		clearing_rate_qty = clearing_balance_qty / sum_of_source_qty
	end if
end if

cleared_amount = 0
cleared_qty    = 0

////if run_by_dept = 1 then
////	//  Fast Targets not supported.
////else
////	GOTO FAST_TARGETS
////end if 

for i = 1 to num_rows  //  Loop over source records ...
	// ### 38448:  Move this initialization of id_array up so it is always called.
	// ### 11046: JAK: 2012-09-11: Cache blocks of IDs for performance
	if i = 1 then
		f_get_column(id_array,'select crbudgets.nextval id from dual connect by level <= ' + string(num_rows))
		id_array_pos = 1
	end if
	
	if mod(i, 500) = 0 then f_pp_msgs(string(i) + " of " + string(num_rows))
	if custom_dw then
		if upper(trim(i_ds_source.GetItemString(i, "target_credit"))) <> "TARGET" then
			continue
		end if
	end if
	source_amount        = i_ds_source.GetItemNumber(i, "amount")
	if not custom_dw then
		source_quantity      = i_ds_source.GetItemNumber(i, "quantity")
	else
		source_quantity      = 0
	end if
//	if not isnull(base_limit) then  //  Null means "no base_limit"
//		if source_amount > base_limit then source_amount = base_limit
//	end if
	sum_of_target_amount = 0
	sum_of_target_qty    = 0
	for trow = 1 to num_targets
		row_id = i_ds_target.GetItemNumber(trow, "row_id")
		//  -------------------------------------------------------------------------------------
		//  9/17/07:  Simplify the RUN BY DEPT allocations for Clearing-Input Rate.
		//  Prevents the longlong run time due to the explosion of records.
		//  Fixes problems with intercompany accounting.
		//  10/1/07:  Added in the loading allocation type.
		if run_by_dept = 1 and (i_clearing_indicator = 2 or i_clearing_indicator = 1) then
			elig_for_plug = false
			if i_ds_source.GetItemString(i, i_department_field) <> i_ds_target.GetItemString(trow, i_department_field) then
				continue //  To the next record in the targets ... The source and target values do not match.
			else
				if trow = num_targets then
					elig_for_plug = true //  Last record in the targets is always eligible to be plugged.
				else
					if i_ds_target.GetItemString(trow, i_department_field) <> &
						i_ds_target.GetItemString(trow + 1, i_department_field) &
					then
						elig_for_plug = true //  Last record for this dept value is always eligible to be plugged.
					end if
				end if
			end if
		end if
		// End Of: 9/17/07:  Simplify the RUN BY DEPT allocations for Clearing-Input Rate.
		// ----------------------------------------------------------------------------------------
//		id++
		
		if id_array_pos <= upperbound(id_array[]) then
			id = id_array[id_array_pos]
			id_array_pos++
		else
			select crbudgets.nextval into :id from dual;
		end if
		
		insert_sqls          = insert_sqls_start
		if i_clearing_indicator <> 3 then
			if i_clearing_indicator = 4 then
				//  Type 4: "Inter-Company (Trigger)" - Rate is always -1.00 (full clearing)
				rate           = -i_ds_target.GetItemNumber(trow, "rate")
			else
				
				if rate_id > 0 then
					
					// ---------- Are they using structure-based rates in this rate type ? ---------- //
					// ---------- (This overrides the traditional rate type lookup.        ---------- //
					s_id_for_rates = uf_rates_by_structure_structure_id(rate_id)
					if isnull(s_id_for_rates) or s_id_for_rates = 0 then
						//  They are not using one ... Continue on.
					else
						//  Use this structure for the rate and return.
						e_for_rates = uf_rates_by_structure_element(s_id_for_rates)
						v_for_rates = i_ds_source.GetItemString(i, e_for_rates)
						if isnull(v_for_rates) then v_for_rates = "value_for_rates_was_null"
						rate = uf_rates_by_structure_rate(s_id_for_rates, v_for_rates)
						if sqlca.SQLCode < 0 then
							uf_msg(descr, "ERROR: TARGETS - RATE LOOKUP (FROM STRUCTURE): " + sqlca.sqlerrtext)
							uf_msg(descr, "element = " + e_for_rates)
							uf_msg(descr, "value   = " + v_for_rates)
							rollback using sqlca;
							rtn_code = -1
							goto commit_updates
						end if
						if sqlca.SQLNRows = 0 then
							uf_msg(descr, "ERROR: TARGETS - RATE LOOKUP (STRUCTURE) - No rows returned")
							uf_msg(descr, "element = " + e_for_rates)
							uf_msg(descr, "value   = " + v_for_rates)
							rollback using sqlca;
							rtn_code = -1
							goto commit_updates
						end if
						goto after_uf_lookup_rate
					end if
					// ---------- END OF:                                                  ---------- //
					// ---------- Are they using structure-based rates in this rate type ? ---------- //
					
					target_co = i_ds_target.GetItemString(trow, i_company_field)
					if target_co = "*" then
						target_co = i_ds_source.GetItemString(i, i_company_field)
					end if
					
					rate = uf_lookup_rate(rate_id, target_co, i_month_number)
					after_uf_lookup_rate:
				else
					rate = i_ds_target.GetItemNumber(trow, "rate")
				end if  //  if rate_id > 0 then ...
								
				//  For overrides ... by month or company ...
				if isnull(rate) then
					rate = i_ds_target.GetItemNumber(trow, "rate")
				end if
				
			end if
			if isnull(rate) then rate = 0
			if dollar_quantity_indicator = 1 then
				target_amount     = round(source_quantity * rate, 2)
				rate_for_insert   = rate
				if i_allocate_quantity = 1 then target_qty = round(source_quantity * rate, 2)
			else
				target_amount     = round(source_amount * rate, 2)
				rate_for_insert   = rate
				if i_allocate_quantity = 1 then target_qty = round(source_quantity * rate, 2)
			end if
		else
			if dollar_quantity_indicator = 1 then
				if i_exclude_negatives = 1 and source_quantity  < 0 then
					target_amount = 0
				else
					target_amount     = round(source_quantity * clearing_rate, 2)
				end if
			else
				if i_exclude_negatives = 1 and source_amount < 0 then
					target_amount = 0
				else
					target_amount     = round(source_amount   * clearing_rate, 2)
				end if
			end if
			rate_for_insert   = clearing_rate
			if i_allocate_quantity = 1 then
				if i_exclude_negatives = 1 and source_quantity < 0 then
					target_qty = 0
				else
					target_qty = round(source_quantity * clearing_rate_qty, 2)
				end if
			end if
		end if
		
		cleared_amount       = cleared_amount       + target_amount
		sum_of_target_amount = sum_of_target_amount + target_amount
		
		if i_allocate_quantity = 1 then
			cleared_qty       = cleared_qty + target_qty
			sum_of_target_qty = sum_of_target_qty + target_qty
		end if

		//  Type 2 ... "Clearing - Input Rate" ... Plug any rounding error ...
		//  Open up Run by Dept:  Plugging rounding error would mess things up
		//  royally since the rates no longer add up to 1.
		////if run_by_dept = 1 then goto after_rounding_plug...9/17/07: NO, NOW THAT WE HAVE PLUG LOGIC.
		if i_clearing_indicator = 2 or i_clearing_indicator = 5 then
			if trow = num_targets or (run_by_dept = 1 and elig_for_plug) then
				//  Must be careful ... if the user entered negetive rates,
				//  the calculation of the diff must be with addition ...
				if source_amount * sum_of_target_amount < 0 then
					//  One of the 2 numbers was negative ...
					diff = source_amount + sum_of_target_amount
				else
					diff = source_amount - sum_of_target_amount
				end if
				if diff <> 0 then
					target_amount = target_amount + diff
				end if
				
				if i_allocate_quantity = 1 then
					//  Must be careful ... if the user entered negetive rates,
					//  the calculation of the diff must be with addition ...
					if source_quantity * sum_of_target_qty < 0 then
						//  One of the 2 numbers was negative ...
						diff = source_quantity + sum_of_target_qty
					else
						diff = source_quantity - sum_of_target_qty
					end if
					if diff <> 0 then
						target_qty = target_qty + diff
					end if
				end if
			end if
		end if
		////after_rounding_plug:...9/17/07: NO, NOW THAT WE HAVE PLUG LOGIC.
		
		//  Type 3 ... "Clearing - Calc Rate" ... Plug any rounding error ...
		if i_clearing_indicator = 3 and not custom_dw then
			if i = num_rows then
				diff = clearing_balance - cleared_amount
				if diff <> 0 then
					target_amount = target_amount + diff
				end if
				if i_allocate_quantity = 1 then
					diff = clearing_balance_qty - cleared_qty
					if diff <> 0 then
						target_qty = target_qty + diff
					end if
				end if
			end if
		end if
		
		insert_sqls = insert_sqls + string(id)
		
		for j = 1 to num_cols  //  Loop over the target columns ...
			//  The first 4 fields are: target_id, row_id, time_stamp, user_id ... skip them.
			//  The last field is the rate ... skip it.
			// ### - SEK - 7093 - 031811: Need to skip the last two fields now that sort_order was 
			//		added to cr_alloc_target_criteria_bdg
//			if j > 4 and j <> num_cols then
			if j > 4 and j <> num_cols and j <> num_cols - 1 then
				//  All fields must be filled in to create the targets ... if a field is 
				//  blank, assume that it is a hidden field (such as fields allowing for
				//  future use) ...
				value = i_ds_target.GetItemString(trow, j)
				
				//  DMJ:  5/71/2008:  Need the column name outside the if to support the "+" code.
				col_name     = i_ds_target.Describe(" #" + string(j) + ".Name")
				col_name     = f_replace_string(col_name, " ", "_", "all")
				col_name     = f_replace_string(col_name, "/", "_", "all")
				col_name     = f_replace_string(col_name, "-", "_", "all")
				
				if pos(value, "*") > 0 and value <> "*" then
					//  Partial Masking ...
					if right(value, 1) = "*" then // like '12*'
						fixed_value = left(value, len(value) - 1)
						source_value = i_ds_source.GetItemString(i, col_name)
						source_value = fixed_value + right(source_value, len(source_value) - len(fixed_value))
					end if
					
					if left(value, 1) = "*" then // like '*12'
						fixed_value = right(value, len(value) - 1)
						source_value = i_ds_source.GetItemString(i, col_name)
						source_value = left(source_value, len(source_value) - len(fixed_value)) + fixed_value
					end if
					//DEBUG: uf_msg("PARTIAL MASKING", col_name + " - " + fixed_value)
					insert_sqls = insert_sqls + ",'" + source_value + "'"
					if i_suspense_accounting = "YES" then
						//  The "orig" field is right next to its ACK counterpart.
						insert_sqls = insert_sqls + ",'" + source_value + "'"
					end if
					goto after_target_masking
				end if
				
				if value = "*" or custom_dw then
					//  Get from the source record ...
					source_value = i_ds_source.GetItemString(i, col_name)
					insert_sqls = insert_sqls + ",'" + source_value + "'"
					if i_suspense_accounting = "YES" then
						//  The "orig" field is right next to its ACK counterpart.
						insert_sqls = insert_sqls + ",'" + source_value + "'"
					end if
				else	
					if value = "+" then
						//  Transpose ...
						find_row = i_ds_target_trpo.Find("target_id = " + string(i_target_id) + &
							" and row_id = " + string(row_id), 1, num_targets) // There's no more than the number of i_ds_target rows.
						if find_row <= 0 then
							uf_msg(descr, "  ")
							uf_msg(descr, "ERROR: TARGETS - TRANSPOSE: record not found: find_row = " + string(find_row))
							uf_msg(descr, "--- find string = " + "target_id = " + string(i_target_id) + " and row_id = " + string(row_id))
							uf_msg(descr, "  ")
							rollback using sqlca;
							rtn_code = -1
							goto commit_updates
						end if
						trpo_col_name = i_ds_target_trpo.GetItemString(find_row, col_name)
						if isnull(trpo_col_name) or trim(trpo_col_name) = "" then
							uf_msg(descr, "  ")
							uf_msg(descr, "ERROR: TARGETS - TRANSPOSE: the trpo_col_name is NULL for: " + col_name)
							uf_msg(descr, "  ")
							rollback using sqlca;
							rtn_code = -1
							goto commit_updates
						end if
						trpo_col_name = f_cr_clean_string(trpo_col_name)
						source_value = i_ds_source.GetItemString(i, trpo_col_name) // Masking from the trpo column
						insert_sqls = insert_sqls + ",'" + source_value + "'"
						if i_suspense_accounting = "YES" then
							//  The "orig" field is right next to its ACK counterpart.
							insert_sqls = insert_sqls + ",'" + source_value + "'"
						end if
					else
						//  Use the value from the target ...
						insert_sqls = insert_sqls + ",'" + value + "'"
						if i_suspense_accounting = "YES" then
							//  The "orig" field is right next to its ACK counterpart.
							insert_sqls = insert_sqls + ",'" + value + "'"
						end if
					end if
				end if
				
				after_target_masking:
				
			end if
		next		//  for j = 1 to num_cols
		
		//
		//  Other fields:
		//
		
		if i_rate_field_exists then
			insert_sqls = insert_sqls + "," + string(rate_for_insert)
		end if
		
		//###DMJ: 8/20/09: MOVED FROM BELOW.
		if custom_dw then
			if upper(trim(i_ds_source.GetItemString(i, "target_credit"))) = "TARGET" then
				target_amount = i_ds_source.GetItemNumber(i, "calc_amount")
			end if
		end if
		//  dr_cr_id ... (+) amount = 1 ... (-) amount = -1 ...
		if target_amount >= 0 then
			insert_sqls = insert_sqls + ",1"
		else
			insert_sqls = insert_sqls + ",-1"
		end if 
		//  ledger_sign = 1 ...
		insert_sqls = insert_sqls + ",1"
		//  qty = 0 ...
		if i_allocate_quantity = 1 then
			insert_sqls = insert_sqls + "," + string(target_qty)
		else
			insert_sqls = insert_sqls + ",0"
		end if
		//  amount ...
		//###DMJ: 8/20/09: MOVED ABOVE SINCE WE NEED THE AMOUNT TO DERIVE DR_CR_ID.
		//if custom_dw then
		//	if upper(trim(i_ds_source.GetItemString(i, "target_credit"))) = "TARGET" then
		//		target_amount = i_ds_source.GetItemNumber(i, "calc_amount")
		//	end if
		//end if
		
		if i_allocate_quantity = 1 then
			if target_amount = 0 and target_qty = 0 then continue
		else
			if target_amount = 0 then continue
		end if

		insert_sqls = insert_sqls + "," + string(target_amount)
		
		//  inception_to_date_amount ...
		insert_sqls = insert_sqls + "," + string(target_amount)
		
		//  Type 1, Type 2:  month_number and month_period from the source ...
		//  Type 3        :  month_number and month_period from the instance variables ...
		choose case i_clearing_indicator
			case 3
				insert_sqls = insert_sqls + "," + &
								  string(i_month_number) + &
								  "," + &
								  string(i_month_period)
			case else
				if i_whole_month then
					insert_sqls = insert_sqls + "," + &
									  string(i_ds_source.GetItemNumber(i, "month_number")) + &
					              "," + &
									  string(0)
				else
					insert_sqls = insert_sqls + "," + &
									  string(i_ds_source.GetItemNumber(i, "month_number")) + &
									  "," + &
									  string(i_ds_source.GetItemNumber(i, "month_period"))
				end if
		end choose
		//  source_id ...
		insert_sqls = insert_sqls + ",'" + string(source_id) + "'"
		//  budget_version ...
		insert_sqls = insert_sqls + ",'" + i_budget_version + "'"
		//  gl_journal_category ...
		insert_sqls = insert_sqls + ",'" + i_gl_journal_category + "'"
		//  amount_type ...
		insert_sqls = insert_sqls + "," + string(2)
		//  allocation_id ...
		insert_sqls = insert_sqls + "," + string(i_allocation_id)
		//  target_credit ...
		insert_sqls = insert_sqls + ", 'TARGET'"
		//  cross_charge_company ...
	
		//  Type 4:  "Inter-Company (Trigger)" - First change the company based on the trigger
		//            column.
		if i_clearing_indicator = 4 then

			//  Get the "trigger" column value ...
			interco_trigger_col_val = i_ds_source.GetItemString(i, interco_trigger_col)
			
			//  Build the SQL to retrieve the correct company value ... for example, 
			//  (SELECT company from glc_area_v where area = '5270') 
			interco_credit_sql = original_interco_credit_sql + " '" + &
									   interco_trigger_col_val + "'"
			
			//  NEED A CURSOR TO PERFORM THIS ...
			PREPARE SQLSA FROM :interco_credit_sql;
			if sqlca.sqlcode <> 0 then
		   	uf_msg(descr, "TARGETS - CURSOR PREPARE - Error " + sqlca.sqlerrtext)
				rollback using sqlca;
			   rtn_code = -1
				goto commit_updates
			end if 

			OPEN DYNAMIC credit_cursor;

			if sqlca.sqlcode <> 0 then
				uf_msg(descr, "TARGETS - CURSOR OPEN - Error " + sqlca.sqlerrtext)
				rollback using sqlca;
				rtn_code = -1
				goto commit_updates
			end if 

			FETCH credit_cursor INTO :interco_company_val;

			interco_company_val = trim(interco_company_val)
			
			if sqlca.sqlcode <> 0 then
				uf_msg(descr, "TARGETS - CURSOR FETCH - Error " + sqlca.sqlerrtext)
				rollback using sqlca;
				rtn_code = -1
				goto commit_updates
			end if 
			
			CLOSE credit_cursor;
			
		else    //  if i_clearing_indicator = 4 then ...
			
			if i_interco_acctg = 1 then
				if custom_dw then
					interco_company_val = i_ds_source.GetItemString(i, "cross_charge_company")
				else
					interco_company_val = i_ds_source.GetItemString(i, i_company_field)
				end if
			else
				interco_company_val = " "
			end if
			
		end if  //  if i_clearing_indicator = 4 then ...
		
		insert_sqls = insert_sqls + ", '" + interco_company_val + "'"
		
		insert_sqls = insert_sqls + ")"		
		
		if isnull(insert_sqls) then
				uf_msg(descr, "The (Execute immediate # 1) is NULL ... " + &
						  "No allocation will be calculated.")
			rollback using sqlca;
			rtn_code = -1
			goto commit_updates
		end if
		
		if g_debug = 'YES' then
			uf_msg("debug", "insert_sqls = " + insert_sqls)
		end if
		
		//  Execute immediate # 1
		execute immediate :insert_sqls using sqlca;
		
		if sqlca.SQLCode <> 0 then
			uf_msg(descr, "  ")
			uf_msg(descr, "ERROR - Targets " + sqlca.SQLErrText)
			uf_msg(descr, "  ")
			uf_msg(descr, "Insert SQL = " + insert_sqls)
			uf_msg(descr, "  ")
			rollback using sqlca;
//				w_cr_control_bdg.mle_1.text = insert_sqls
//				w_cr_control_bdg.mle_1.visible = true
			rtn_code = -1
			goto commit_updates
		end if
	next  //  for trow = 1 to num_targets ...
next		//  for i = 1 to num_rows ... 


////FAST_TARGETS:
////if run_by_dept = 1 then
////	//  Fast Targets not supported.
////else
////	rtn = uf_fast_targets(rate_for_insert, source_id) // PROTOTYPING FASTER INSERTS
////	
////	if rtn <> 1 then
////		//  Error msgs are in the function.
////		rollback using sqlca;
////		rtn_code = -1
////		goto commit_updates
////	end if
////end if


//*****************************************************************************************
//
//  Run by Deparment Flag:
//
//  9/17/07: Only call this function for Clearing-Calc Rate allocations.  The methodology
//  is simpler for Clearing-Input Rate allocations and is completely handled in this
//  script.
//
//*****************************************************************************************
if i_run_by_dept_credits_method = "YES" then
	if run_by_dept = 1 and i_clearing_indicator = 3 then
		//
		//  Call the function that calculates departmental-based clearings ...
		//
		rtn = uf_run_by_department(i_allocation_id, i_month_number, i_month_period, &
			i_clearing_indicator, cr_company_id, i_company_field, i_gl_journal_category, i_ds_source, &
			group_by_id, descr, where_clause_id, clearing_id, work_order_field, custom_dw)
		
		if rtn <> 1 then
			uf_msg(descr, "ERROR: in the Run by Department logic")
			rollback using sqlca;
			rtn_code = -1
			goto commit_updates
		end if
	end if
end if


//*********************************************************************
//
//  Check to see if targets were created:
//    Otherwise, the credits will throw an error since they will not
//    be able to find any records.
//
//*********************************************************************
counter = 0

if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
	if i_run_as_a_test then		
		select count(*) into :counter from cr_budget_data_test
		 where allocation_id = :i_allocation_id
		 	and budget_version = :i_budget_version
		 	and month_number = :i_month_number
		   and target_credit = 'TARGET' and rownum = 1;
	else		
		select count(*) into :counter from cr_budget_data
		 where allocation_id = :i_allocation_id
		 	and budget_version = :i_budget_version
		 	and month_number = :i_month_number
		   and target_credit = 'TARGET' and rownum = 1;
	end if
else                                                          //  Other Allocations ...
	if i_run_as_a_test then
		select count(*) into :counter from cr_budget_data_test
		 where allocation_id = :i_allocation_id
		 	and budget_version = :i_budget_version
		 	and month_number = :i_month_number
		   and target_credit = 'TARGET' and rownum = 1;
	else
		select count(*) into :counter from cr_budget_data
		 where allocation_id = :i_allocation_id
		 	and budget_version = :i_budget_version
		 	and month_number = :i_month_number
		   and target_credit = 'TARGET' and rownum = 1;
	end if
end if  //  if i_clearing_indicator = 4 or i_clearing_indicator = 5 then ...

if sqlca.SQLCode < 0 then
	uf_msg(descr, "  ")
	uf_msg(descr, "ERROR: verifying that TARGET records were created: " + sqlca.SQLErrText)
	uf_msg(descr, "  ")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

if isnull(counter) then counter = 0

if counter <= 0 then
	uf_msg("  ", "  ")
	uf_msg("allocation_id = " + string(i_allocation_id), &
          "No TARGET records written.  There are no results for this allocation.")
	uf_msg("  ", "  ")
	rtn_code = 0
	goto commit_updates
end if


//***********************************
//
//  Credits:
//
//***********************************
f_pp_msgs("Inserting Credits at " + string(now()))

//uo_ds_top i_ds_credit
//i_ds_credit = CREATE uo_ds_top
i_ds_credit.reset()

// DMJ: 1/5/10: MEMORY LEAK:
//i_ds_credit.DataObject = "dw_temp_dynamic"

// DMJ: 1/5/10: MEMORY LEAK:
//uo_ds_top ds_credit_trpo
//ds_credit_trpo = CREATE uo_ds_top
//ds_credit_trpo.DataObject = "dw_temp_dynamic"
i_ds_credit_trpo.Reset()

//uo_ds_top i_ds_credit_insert
//i_ds_credit_insert = CREATE uo_ds_top
if custom_dw then
	i_ds_credit_insert = i_ds_source
else
	i_ds_credit_insert.DataObject = "dw_temp_dynamic"
end if

sqls = "select * from cr_alloc_credit_criteria_bdg where credit_id = " + string(i_credit_id)
sqls_trpo = "select * from cr_alloc_credit_criteria_t_bdg where credit_id = " + string(i_credit_id)

if isnull(sqls) then
	uf_msg(descr, "The CREDIT SQL is NULL ... No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

if isnull(sqls_trpo) then
	uf_msg(descr, "The CREDIT SQL (TRPO) is NULL ... No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

// DMJ: 1/5/10: MEMORY LEAK:
//rtn_f_create_str = f_create_dynamic_ds(i_ds_credit, "grid", sqls, sqlca, true)
i_ds_credit.SetSQLSelect(sqls)
rtn_sql = i_ds_credit.RETRIEVE()

// DMJ: 1/5/10: MEMORY LEAK:
//if rtn_f_create_str <> "OK" then
if rtn_sql < 1 then
	uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_credit.DataObject = "dw_temp_dynamic" ...')
end if

// DMJ: 1/5/10: MEMORY LEAK:
//rtn_f_create_str = f_create_dynamic_ds(ds_credit_trpo, "grid", sqls_trpo, sqlca, true)
i_ds_credit_trpo.SetSQLSelect(sqls_trpo)
rtn_sql = i_ds_credit_trpo.RETRIEVE()

// DMJ: 1/5/10: MEMORY LEAK:
//if rtn_f_create_str <> "OK" then
if rtn_sql < 0 then
	uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(ds_credit_trpo ...')
end if

num_rows = i_ds_credit.RowCount()
num_cols = long(i_ds_credit.describe("datawindow.column.count"))

if custom_dw then num_rows = 1
////GOTO FAST_CREDITS
//  NOTE: Currently there can only be 1 credit row per allocation type ...
for i = 1 to num_rows				//  Loop over credit criteria records ...

	row_id = i_ds_credit.GetItemNumber(i, "row_id")
	credit_sqls       = "select "
	credit_group_sqls = ""
	
	for j = 1 to i_num_elements		//  Loop over the code block to build the credits ...
		
		element_descr = upper(trim(i_ds_elements.GetItemString(j, "budgeting_element")))
		element_descr = f_replace_string(element_descr, " ", "_", "all")
		element_descr = f_replace_string(element_descr, "/", "_", "all")
		element_descr = f_replace_string(element_descr, "-", "_", "all")
		value = upper(i_ds_credit.GetItemString(i, element_descr))
		
		if value = " " then
			//  Leave a single space alone ...
		else
			value = trim(value)
		end if
		
		if pos(value, "*") > 0 and value <> "*" then
			//  Partial Masking ...
			if right(value, 1) = "*" then // like '12*' ... 254 hardcoded for performance.
				fixed_value = left(value, len(value) - 1)
				credit_sqls       = credit_sqls       + "'" + fixed_value + "'" + '||substr("' + element_descr + '", 1+' + string(len(value)) + '-1, 254),'
				credit_group_sqls = credit_group_sqls + "'" + fixed_value + "'" + '||substr("' + element_descr + '", 1+' + string(len(value)) + '-1, 254),'
			end if
			
			if left(value, 1) = "*" then // like '*12'
				fixed_value = right(value, len(value) - 1)
				credit_sqls       = credit_sqls       + 'substr("' + element_descr + '", 1, length(' + element_descr + ')-' + string(len(fixed_value)) + ')||' + "'" + fixed_value + "'" + ','
				credit_group_sqls = credit_group_sqls + 'substr("' + element_descr + '", 1, length(' + element_descr + ')-' + string(len(fixed_value)) + ')||' + "'" + fixed_value + "'" + ','
			end if
			//DEBUG: uf_msg("PARTIAL MASKING", col_name + " - " + fixed_value)
			insert_sqls = insert_sqls + ",'" + source_value + "'"
			goto after_credit_masking
		end if
		
		choose case value
			case "*"  //  Get the field from the target records (put the field name in the
						 //  select and group by of the sql ...
				credit_sqls       = credit_sqls + '"' + element_descr + '",'
				credit_group_sqls = credit_group_sqls + '"' + element_descr + '",'
				
			case else
				if value = "+" then
					//  Transpose ...
					find_row = i_ds_credit_trpo.Find("credit_id = " + string(i_credit_id) + &
						" and row_id = " + string(row_id), 1, num_rows) // There's no more than the number of i_ds_credit.DataObject = "dw_temp_dynamic" rows.
					if find_row <= 0 then
						uf_msg(descr, "  ")
						uf_msg(descr, "ERROR: CREDITS - TRANSPOSE: record not found: find_row = " + string(find_row))
						uf_msg(descr, "--- find string = " + "credit_id = " + string(i_credit_id) + " and row_id = " + string(row_id))
						uf_msg(descr, "  ")
						rollback using sqlca;
						rtn_code = -1
						goto commit_updates
					end if
					trpo_col_name = upper(trim(i_ds_credit_trpo.GetItemString(find_row, element_descr)))
					if isnull(trpo_col_name) or trim(trpo_col_name) = "" then
						uf_msg(descr, "  ")
						uf_msg(descr, "ERROR: CREDITS - TRANSPOSE: the trpo_col_name is NULL for: " + element_descr)
						uf_msg(descr, "  ")
						rollback using sqlca;
						rtn_code = -1
						goto commit_updates
					end if
					trpo_col_name = f_cr_clean_string(trpo_col_name)
					credit_sqls       = credit_sqls + '"' + trpo_col_name + '",' // Not the current element, but the transpose value.
					credit_group_sqls = credit_group_sqls + '"' + trpo_col_name + '",' // Not the current element, but the transpose value.
				else
					credit_sqls = credit_sqls + "'" + value + "',"
				end if
				
		end choose
		
		after_credit_masking:
		
	next		//  for j = 1 to i_num_elements ... Loop over the code block to build the credits

	//  The newly inserted allocations will have a NULL drilldown_key ... add the 
	//  where clause and group by ...
	if not isnull(credit_group_sqls) and credit_group_sqls <> "" then
		if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company (Trigger)...
			if i_run_as_a_test then
				credit_sqls = credit_sqls + &
								 "month_number, month_period, budget_version, " + &
								 "cross_charge_company, -sum(amount), -sum(quantity) " + &
								 "from cr_budget_data_test where drilldown_key is null " + &
								 "and budget_version = '" + i_budget_version + "' and " + &
								 "allocation_id = " + string(i_allocation_id) + " and " + &
								 "month_number = " + string(i_month_number) + &
								 " group by " + &
								  credit_group_sqls + &
								 " month_number, month_period, budget_version, cross_charge_company"
			else
				credit_sqls = credit_sqls + &
								 "month_number, month_period, budget_version, " + &
								 "cross_charge_company, -sum(amount), -sum(quantity) " + &
								 "from cr_budget_data where drilldown_key is null " + &
								 "and budget_version = '" + i_budget_version + "' and " + &
								 "allocation_id = " + string(i_allocation_id) + " and " + &
								 "month_number = " + string(i_month_number) + &
								 " group by " + &
								  credit_group_sqls + &
								 " month_number, month_period, budget_version, cross_charge_company"
			end if
		else                                                          //  Other Allocations ...
			if i_run_as_a_test then
				if i_interco_acctg = 1 then
					//  If they are doing intercompany accounting, they should be using
					//  a negative rate with the billable companies as the targets.
					//  This is the opposite of the "Trigger" method and the reason we
					//  don't have this SQL for type 4 ... Mulitple credits must
					//  be booked on the billing company's books with the cross_charge_company
					//  field filled in correctly.
					credit_sqls = credit_sqls + &
									 "month_number, month_period, budget_version, " + &
									 i_company_field + " cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data_test where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by " + &
									  credit_group_sqls + &
									 " month_number, month_period, budget_version, " + i_company_field
				else
					credit_sqls = credit_sqls + &
									 "month_number, month_period, budget_version, " + &
									 "cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data_test where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by " + &
									  credit_group_sqls + &
									 " month_number, month_period, budget_version, cross_charge_company"
				end if
			else
				if i_interco_acctg = 1 then
					//  See the comment above ...
					credit_sqls = credit_sqls + &
									 "month_number, month_period, budget_version, " + &
									 i_company_field + " cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by " + &
									  credit_group_sqls + &
									 " month_number, month_period, budget_version, " + i_company_field
				else
					credit_sqls = credit_sqls + &
									 "month_number, month_period, budget_version, " + &
									 "cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by " + &
									  credit_group_sqls + &
									 " month_number, month_period, budget_version, cross_charge_company"
				end if
			end if
		end if
	else
		if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company (Trigger)...
			if i_run_as_a_test then
				credit_sqls = credit_sqls + &
				             "month_number, month_period, budget_version, " + &
								 "cross_charge_company, -sum(amount), -sum(quantity) " + &
								 "from cr_budget_data_test where drilldown_key is null " + &
								 "and budget_version = '" + i_budget_version + "' and " + &
								 "allocation_id = " + string(i_allocation_id) + " and " + &
								 "month_number = " + string(i_month_number) + &
								 " group by month_number, month_period, budget_version, cross_charge_company"
			else
				
				credit_sqls = credit_sqls + &
				             "month_number, month_period, budget_version, " + &
								 "cross_charge_company, -sum(amount), -sum(quantity) " + &
								 "from cr_budget_data where drilldown_key is null " + &
								 "and budget_version = '" + i_budget_version + "' and " + &
								 "allocation_id = " + string(i_allocation_id) + " and " + &
								 "month_number = " + string(i_month_number) + &
								 " group by month_number, month_period, budget_version, cross_charge_company"
			end if
		else                                                          //  Other Allocations ...
			if i_run_as_a_test then
				if i_interco_acctg = 1 then
					//  See the comment above ...
					credit_sqls = credit_sqls + &
									 "month_number, month_period, budget_version, " + &
									 i_company_field + " cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data_test where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by " + &
									  credit_group_sqls + &
									 " month_number, month_period, budget_version, " + i_company_field
				else
					credit_sqls = credit_sqls + &
					             "month_number, month_period, budget_version, " + &
									 "cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data_test where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by month_number, month_period, budget_version, cross_charge_company"
				end if
			else
				if i_interco_acctg = 1 then
					//  See the comment above ...
					credit_sqls = credit_sqls + &
									 "month_number, month_period, budget_version, " + &
									 i_company_field + " cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by " + &
									  credit_group_sqls + &
									 " month_number, month_period, budget_version, " + i_company_field
				else
					credit_sqls = credit_sqls + &
					             "month_number, month_period, budget_version, " + &
									 "cross_charge_company, -sum(amount), -sum(quantity) " + &
									 "from cr_budget_data where drilldown_key is null " + &
									 "and budget_version = '" + i_budget_version + "' and " + &
									 "allocation_id = " + string(i_allocation_id) + " and " + &
								    "month_number = " + string(i_month_number) + &
									 " group by month_number, month_period, budget_version, cross_charge_company"
				end if
			end if
		end if
	end if
	
//	uf_msg("", string(i_allocation_id) + ": Creating credit entry ... Retrieving Data at " + &
//	           string(today(), "mm/dd/yyyy hh:mm:ss"))
	//  Insert the credits ...
	if isnull(credit_sqls) then
		uf_msg(descr, "The CREDIT INSERT SQL is NULL ... " + &
					  "No allocation will be calculated.")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
	if custom_dw then
	else
	   rtn_f_create_str = f_create_dynamic_ds(i_ds_credit_insert, "grid", credit_sqls, sqlca, true)
		
		if rtn_f_create_str <> "OK" then
			uf_msg(descr, "  ")
			uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_credit_insert ...')
			uf_msg(descr, "credit_sqls = " + credit_sqls)
			uf_msg(descr, "  ")
			rollback using sqlca;
			rtn_code = -1
			goto commit_updates
		end if
	end if

//	uf_msg("", string(i_allocation_id) + ": Creating credit entry ... Inserting transactions at " + &
//	           string(today(), "mm/dd/yyyy hh:mm:ss"))

	num_inserts     = i_ds_credit_insert.RowCount()
	num_credit_cols = long(i_ds_credit_insert.describe("datawindow.column.count"))
	
	if num_inserts = 0 then
		uf_msg(descr, "  ")
		uf_msg(descr, "ERROR: building i_ds_credit_insert!  No rows returned!  No allocation will be calculated.")
		uf_msg(descr, "credit_sqls = " + credit_sqls)
		uf_msg(descr, "  ")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
	
	//  Type 4:  "Inter-Company (Trigger)" - First change the company based on the trigger
	//            column.
	if i_clearing_indicator = 4 then

		for k = 1 to num_inserts  //  Inter-Company ...
			
			//  Get the "trigger" column value ...
			interco_trigger_col_val = i_ds_credit_insert.GetItemString(k, interco_trigger_col)
			
			//  Build the SQL to retrieve the correct company value ... for example, 
			//  (SELECT company from glc_area_v where area = '5270') 
			interco_credit_sql = original_interco_credit_sql + " '" + &
									   interco_trigger_col_val + "'"
			
			//  NEED A CURSOR TO PERFORM THIS ...
			PREPARE SQLSA FROM :interco_credit_sql;
			if sqlca.sqlcode <> 0 then
		   	uf_msg(descr, "CURSOR PREPARE - Error " + sqlca.sqlerrtext)
				rollback using sqlca;
			   rtn_code = -1
				goto commit_updates
			end if 

			OPEN DYNAMIC credit_cursor;

			if sqlca.sqlcode <> 0 then
				uf_msg(descr, "CURSOR OPEN - Error " + sqlca.sqlerrtext)
				rollback using sqlca;
				rtn_code = -1
				goto commit_updates
			end if 

			FETCH credit_cursor INTO :interco_company_val;

			interco_company_val = trim(interco_company_val)

			if sqlca.sqlcode <> 0 then
				uf_msg(descr, &
				       "Credits - CURSOR FETCH - Error " + sqlca.sqlerrtext)
				rollback using sqlca;
				rtn_code = -1
				goto commit_updates
			end if 
			
			CLOSE credit_cursor;
			
			//  Plug the company from the target into the cross_charge_company
			//  field ... Then replace it with the company from the trigger.
			i_ds_credit_insert.SetItem(k, "cross_charge_company", &
				i_ds_credit_insert.GetItemString(k, i_company_field))
			i_ds_credit_insert.SetItem(k, i_company_field, interco_company_val)
			
		next  //  for k = 1 to num_inserts  //  Inter-Company ...
		
	end if  //  if i_clearing_indicator = 4 then ...

	for k = 1 to num_inserts
		// ### 38448:  Move this initialization of id_array up so it is always called.
		// ### 11046: JAK: 2012-09-11: Cache blocks of IDs for performance
		if k = 1 then
			f_get_column(id_array,'select crbudgets.nextval id from dual connect by level <= ' + string(num_inserts))
			id_array_pos = 1
		end if
		
		if custom_dw then
			if upper(trim(i_ds_source.GetItemString(k, "target_credit"))) <> "CREDIT" then
				continue
			end if
		end if
		
		
		if id_array_pos <= upperbound(id_array[]) then
			id = id_array[id_array_pos]
			id_array_pos++
		else
			select crbudgets.nextval into :id from dual;
		end if
		
		insert_sqls = insert_sqls_start + string(id)
		
		//  Need to perform a separate loop for the custom DW allocations or else the
		//  insert will be out of order ... This will avoid having to format the DW SQL
		//  in a particular order.
		if custom_dw then
			for z = 1 to num_cols
				if z <= 4 then continue  //  Skip the credit_id, row_id, time_stamp, user_id
				col_name = i_ds_credit.Describe(" #" + string(z) + ".Name")
				col_name = f_replace_string(col_name, " ", "_", "all")
				value = i_ds_credit_insert.GetItemString(k, col_name)
				insert_sqls = insert_sqls + ",'" + value + "'"			
				if i_suspense_accounting = "YES" then
					//  The "orig" field is right next to its ACK counterpart.
					insert_sqls = insert_sqls + ",'" + value + "'"
				end if
			next
		else
			for z = 1 to num_credit_cols - 6  //  Exclude the month_number,month_period,
														 //  budget_version, cross_charge_company,
														 //  amount columns ... DMJ: 6/11/08: CHANGED FROM 5
				value = i_ds_credit_insert.GetItemString(k, z)
				insert_sqls = insert_sqls + ",'" + value + "'"			
				if i_suspense_accounting = "YES" then
					//  The "orig" field is right next to its ACK counterpart.
					insert_sqls = insert_sqls + ",'" + value + "'"
				end if
			
			next
		end if
				
		//  dr_cr_id ... (+) amount = 1 ... (-) amount = -1 ...
		if i_rate_field_exists then
			insert_sqls = insert_sqls + "," + string(0)
		end if
		
		if custom_dw then
			credit_amount = i_ds_credit_insert.GetItemNumber(k, "calc_amount")
			credit_qty    = 0
		else
			credit_amount = i_ds_credit_insert.GetItemNumber(k, num_credit_cols - 1) // DMJ: 6/11/08: ADDED -1
			if i_allocate_quantity = 1 then
				credit_qty = i_ds_credit_insert.GetItemNumber(k, num_credit_cols)
			else
				 credit_qty = 0
			end if
		end if
		
		if i_allocate_quantity = 1 then
			if credit_amount = 0 and credit_qty = 0 then continue
		else
			if credit_amount = 0 then continue
		end if
		
		if credit_amount >= 0 then
			insert_sqls = insert_sqls + ", 1"
		else
			insert_sqls = insert_sqls + ", -1"
		end if
		//  ledger_sign = 1 ...
		insert_sqls = insert_sqls + ", 1"
		//  qty = 0 ...
		if i_allocate_quantity = 1 then
			insert_sqls = insert_sqls + "," + string(credit_qty)
		else
			insert_sqls = insert_sqls + ",0"
		end if
		//  amount ...
		insert_sqls = insert_sqls + "," + string(credit_amount)
		//  inception_to_date_amount ...
		insert_sqls = insert_sqls + "," + string(credit_amount)
		//  month_number and month_period from the source ...
		insert_sqls = insert_sqls + "," + &
						  string(i_ds_credit_insert.GetItemNumber(k, "month_number")) + &
						  "," + &
						  string(i_ds_credit_insert.GetItemNumber(k, "month_period"))
		//  source_id ...
		insert_sqls = insert_sqls + ",'" + string(source_id) + "'"
		//  budget_version ...
		insert_sqls = insert_sqls + ",'" + i_budget_version + "'"
		//  gl_journal_category ...
		insert_sqls = insert_sqls + ",'" + i_gl_journal_category + "'"
		//  amount_type ...
		insert_sqls = insert_sqls + "," + string(2)
		//  allocation_id ...
		insert_sqls = insert_sqls + "," + string(i_allocation_id)
		//  target_credit ...
		insert_sqls = insert_sqls + ", 'CREDIT'"
		//  cross_charge_company ...
		if isnull(i_ds_credit_insert.GetItemString(k, "cross_charge_company")) then
			insert_sqls = insert_sqls + ",' '"
		else	
			insert_sqls = insert_sqls + ",'" + &
				i_ds_credit_insert.GetItemString(k, "cross_charge_company") + "'"
		end if
		
		insert_sqls = insert_sqls + ")"		 
		
		if isnull(insert_sqls) then
				uf_msg(descr, "The (Execute immediate # 2) is NULL ... " + &
						  "No allocation will be calculated.")
			rollback using sqlca;
			rtn_code = -1
			goto commit_updates
		end if

		//  Execute immediate # 2
		execute immediate :insert_sqls using sqlca;

		if sqlca.SQLCode <> 0 then
			uf_msg(descr, "  ")
			uf_msg("ERROR - Credits", "ERROR - Credits " + sqlca.SQLErrText)
			uf_msg("ERROR - Credits", "insert_sqls = " + insert_sqls)
			uf_msg(descr, "  ")
			rollback using sqlca;
			rtn_code = -1
			goto commit_updates
		end if
		
	next  //  for k = 1 to num_inserts ...
	
next	//  for i = 1 to num_rows ... Loop over credit criteria records ...


////FAST_CREDITS:
////rtn = uf_fast_credits(rate_for_insert, source_id) // PROTOTYPING FASTER INSERTS

////if rtn <> 1 then
////	//  Error msgs are in the function.
////	rollback using sqlca;
////	rtn_code = -1
////	goto commit_updates
////end if


//*****************************************************************************************
//
//  Inter-Company:  Receivable and Payable
//
//                  Use the "credit" variables in this code like credit_sqls and
//                  credit_group_sqls to avoid extra variables.
//
//*****************************************************************************************
select control_value into :cv from cr_system_control
 where upper(rtrim(control_name)) = 'BUDGET - INTERCOMPANY ENTRIES';

if upper(trim(cv)) = "YES" then
	i_interco_acctg = 1
else
	i_interco_acctg = 0
end if

if i_interco_acctg = 1 then
//	uf_msg("", string(i_allocation_id) + ": Creating intercompany entries at " + &
//	           string(today(), "mm/dd/yyyy hh:mm:ss"))

	interco_total = 0
	
//	uo_ds_top i_ds_interco
//	i_ds_interco = CREATE uo_ds_top
	i_ds_interco.reset()
	
//	uo_ds_top i_ds_interco_insert
//	i_ds_interco_insert = CREATE uo_ds_top
	i_ds_interco_insert.reset()
	
	sqls = "select * from cr_alloc_interco_criteria_bdg where intercompany_id = " + &
			  string(interco_id)
	
	if isnull(sqls) then
		uf_msg(descr, "The INTERCO SQL is NULL ... " + &
					  "No allocation will be calculated.")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
	
	if g_debug = 'YES' then uf_msg("debug", "i_ds_interco sqls = " + sqls)
	
	rtn_f_create_str = f_create_dynamic_ds(i_ds_interco, "grid", sqls, sqlca, true)

	if rtn_f_create_str <> "OK" then
		uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_interco ...')
	end if

	num_rows = i_ds_interco.RowCount()

	//  Build the insert statement and perform the insert ...
	for i = 1 to num_rows				//  Loop over intercompany criteria records ...

		credit_sqls       = "select "
		credit_group_sqls = ""
	
		string_row_id   = upper(trim(i_ds_interco.GetItemString(i, "row_id")))
		interco_company = i_ds_interco.GetItemString(i, i_company_field)
		interco_pk_id   = i_ds_interco.GetItemNumber(i, "id")


		for j = 1 to i_num_elements		//  Loop over the code block to build the REC./PAY.
		
			element_descr = upper(trim(i_ds_elements.GetItemString(j, "budgeting_element")))
			element_descr = f_replace_string(element_descr, " ", "_", "all")
		
			value = upper(trim(i_ds_interco.GetItemString(i, element_descr)))
		
			//  COMPANY field must have a * ...  NO LONGER ... all companies are in the table
			if element_descr = i_company_field then
				credit_sqls       = credit_sqls + '"' + element_descr + '",'
				credit_group_sqls = credit_group_sqls + '"' + element_descr + '",'
			else
				choose case value
					case "*"  //  Get the field from the target records (put the field name in the
								 //  select and group by of the sql ...
						credit_sqls       = credit_sqls + '"' + element_descr + '",'
						credit_group_sqls = credit_group_sqls + '"' + element_descr + '",'
				
					case else
						//  Need the value and the column alias for this SQL so the interco DS has
						//  the trigger column named correctly.
						credit_sqls = credit_sqls + &
						              "'" + value + "' " + '"' + element_descr + '"' + ","
				end choose
			end if
		
		next  //  for j = 1 to i_num_elements ... Loop over the code block to build the credits

		//  The newly inserted allocations will have a NULL drilldown_key ... add the 
		//  where clause and group by ...
		if not isnull(credit_group_sqls) and credit_group_sqls <> "" then
			if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
				if i_run_as_a_test then
					credit_sqls = credit_sqls + &
									 "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data_test " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by " + credit_group_sqls + &
									 " month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				else
					credit_sqls = credit_sqls + &
									 "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by " + credit_group_sqls + &
									 " month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				end if
			else                                                          //  Other Allocations ...
				if i_run_as_a_test then
					credit_sqls = credit_sqls + &
									 "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data_test " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by " + credit_group_sqls + &
									 " month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				else
					credit_sqls = credit_sqls + &
									 "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by " + credit_group_sqls + &
									 " month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				end if
			end if
		else
			if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
				if i_run_as_a_test then
					credit_sqls = credit_sqls + &
					             "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data_test " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				else
					credit_sqls = credit_sqls + &
					             "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				end if
			else                                                          //  Other Allocations ...
				if i_run_as_a_test then
					credit_sqls = credit_sqls + &
					             "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data_test " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				else
					credit_sqls = credit_sqls + &
					             "month_number, month_period, target_credit, " + &
									 "budget_version, cross_charge_company, -sum(amount) " + &
									 "from cr_budget_data " + &
									 "where drilldown_key is null and allocation_id = " + &
									 string(i_allocation_id) + " and " + &
								 	 "month_number = " + string(i_month_number) + &
									 " group by month_number, month_period, target_credit, " + &
									 " budget_version, cross_charge_company"
				end if
			end if
		end if
	
		//  Insert the inter-company receivable or payable entry ... 
		//    Records will be retrieved for both companies with the SQL above (from the
		//    target and credit records) ... lookup the correct company and only book
		//    one of the entries.
		if isnull(credit_sqls) then
			uf_msg(descr, "The INTERCO INSERT SQL is NULL ... " + &
						  "No allocation will be calculated.")
			rollback using sqlca;
			rtn_code = -1
			goto commit_updates
		end if		
		
		if g_debug = 'YES' then uf_msg("debug", "i_ds_interco_insert credit_sqls = " + credit_sqls)
		
		rtn_f_create_str = f_create_dynamic_ds(i_ds_interco_insert, "grid", credit_sqls, sqlca, true)
		
		if rtn_f_create_str <> "OK" then
			uf_msg(descr, "ERROR OCCURRED ON: " + 'f_create_dynamic_ds(i_ds_interco_insert ...')
		end if
		
		num_inserts     = i_ds_interco_insert.RowCount()
		num_credit_cols = long(i_ds_interco_insert.describe("datawindow.column.count"))

		for k = 1 to num_inserts
			// ### 38448:  Move this initialization of id_array up so it is always called.
			// ### 11046: JAK: 2012-09-11: Cache blocks of IDs for performance
			if k = 1 then
				f_get_column(id_array,'select crbudgets.nextval id from dual connect by level <= ' + string(num_inserts))
				id_array_pos = 1
			end if
			
			retrieved_company_val = i_ds_interco_insert.GetItemString(k, i_company_field)
			
			//  interco_company is from cr_alloc_interco_criteria_bdg ...
			//  retrieved_company_val is from the sqls that gets the Target's and Credit's
			//    from the allocation ...
			//  If they don't match, don't book an intercompany transaction ...
			if retrieved_company_val <> interco_company then continue
			
			//  TYPE 4: INTER-COMPANY (TRIGGER) ...
			if i_clearing_indicator = 4 then
				//  TARGETS get a receivable, CREDITS get a payable ...
				//  Need this check since the intercompany criteria table will have both the 
				//  payable and receivable parameters that will both pass the trigger check below.
				choose case upper(trim(i_ds_interco_insert.GetItemString(k, "target_credit")))
					case "TARGET"
						if string_row_id <> "RECEIVABLE" then continue
					case "CREDIT"
						if string_row_id <> "PAYABLE" then continue
					case else  //  The previously booked interco REC/PAY. will be in the DS ...
						continue
				end choose
			
				//  Get the "trigger" column value ...
				interco_trigger_col_val = i_ds_interco_insert.GetItemString(k, interco_trigger_col)
			
				//  Build the SQL to retrieve the correct company value ... for example, 
				//  (SELECT company from glc_area_v where area = '5270') 
				interco_credit_sql = original_interco_credit_sql + " '" + &
										   interco_trigger_col_val + "'"			

				//  NEED A CURSOR TO PERFORM THIS ...
				PREPARE SQLSA FROM :interco_credit_sql;
				if sqlca.sqlcode <> 0 then
		   		uf_msg(descr, "CURSOR PREPARE - Error " + sqlca.sqlerrtext)
					rollback using sqlca;
				   rtn_code = -1
					goto commit_updates
				end if 

				OPEN DYNAMIC credit_cursor;

				if sqlca.sqlcode <> 0 then
					uf_msg(descr, "CURSOR OPEN - Error " + sqlca.sqlerrtext)
					rollback using sqlca;
					rtn_code = -1
					goto commit_updates
				end if 

				FETCH credit_cursor INTO :interco_company_val;

				interco_company_val = trim(interco_company_val)

				if sqlca.sqlcode <> 0 then
					uf_msg(descr, &
					       "Interco - CURSOR FETCH - Error " + sqlca.sqlerrtext)
					rollback using sqlca;
					rtn_code = -1
					goto commit_updates
				end if 
			
				CLOSE credit_cursor;
			
				//  The entry should only be booked where the company is valid ...
				//  Retrieving duplicate records by company is easier than trying to build
				//  the where clause with company in it ... This check was put in here 1st.
				//  A bug was found and a different company check was inserted just after
				//  "for k = 1 to num_inserts ..." (above).  This one may now be unnecessary,
				//  but we will keep it here just in case.
				//  THIS MUST BE COMMENTED OUT OR THE "TRIGGER" METHODOLOGY WONT WORK ...
				//  11/17/1998
//				if retrieved_company_val <> interco_company_val then continue
				
			else    //  if i_clearing_indicator = 4 then ...
				
				//  This check was put in here 1st.
				//  A bug was found and a different company check was inserted just after
				//  "for k = 1 to num_inserts ..." (above).  This one may now be unnecessary,
				//  but we will keep it here just in case.
				if retrieved_company_val <> interco_company then continue
				
				if g_debug = 'YES' then
					uf_msg("debug", "retrieved_company_val = " + retrieved_company_val + ", " + &
						"interco_company = " + interco_company + ", " + "string_row_id = " + string_row_id + ", " + &
						"target_credit = " + upper(trim(i_ds_interco_insert.GetItemString(k, "target_credit"))))
				end if
				
				choose case upper(trim(i_ds_interco_insert.GetItemString(k, "target_credit")))
					case "TARGET"
						if string_row_id <> "PAYABLE" then continue
					case "CREDIT"
						if string_row_id <> "RECEIVABLE" then continue
					case else  //  The previously booked interco REC/PAY. will be in the DS ...
						continue
				end choose
				
			end if  //  if i_clearing_indicator = 4 then ...
			
			if id_array_pos <= upperbound(id_array[]) then
				id = id_array[id_array_pos]
				id_array_pos++
			else
				select crbudgets.nextval into :id from dual;
			end if

			insert_sqls = insert_sqls_start + string(id)

			//  The custom DW's may have a mixture of intercompany and non-intercompany
			//  transactions ... need this check to skip certain records.
			if custom_dw then
				if retrieved_company_val = &
				   i_ds_interco_insert.GetItemString(k, "cross_charge_company") &
				then 
					continue
				end if
			end if
			
			if g_debug = 'YES' then uf_msg("debug", &
				"retrieving intercompany accounting from i_ds_interco_insert (id = " + string(interco_pk_id) + ")")
			
			for z = 1 to num_credit_cols - 6  //  Exclude the month_number,month_period,
														 //  target_credit,budget_version,
														 //  cross_charge_company,amount columns ...
				value    = i_ds_interco_insert.GetItemString(k, z)
				col_name = upper(trim(i_ds_interco_insert.Describe("#" + string(z) + ".Name")))
								
				if trim(value) = "%" then  //  cr_alloc_interco_criteria2_bdg table ...
				
					//  The "field" field will have field names with underscores (no
					//  spaces) ... they may be lower case, however.
					cross_company_val = i_ds_interco_insert.GetItemString(k, "cross_charge_company")
					
					select value into :interco_flex_value
					  from cr_alloc_interco_criteria2_bdg
					 where intercompany_id = :interco_id    and
					       upper(row_id)   = :string_row_id and
							 id              = :interco_pk_id and
							 upper(field)    = :col_name      and
							 company_value   = :cross_company_val;
					
					if isnull(interco_flex_value) or trim(interco_flex_value) = "" then 
						uf_msg("Error", "Could not find the " + col_name + " value for " + &
						           "the intercompany transaction.")
						rollback using sqlca;
						rtn_code = -1
						goto commit_updates
					end if
					
					insert_sqls = insert_sqls + ",'" + interco_flex_value + "'"
					if i_suspense_accounting = "YES" then
						//  The "orig" field is right next to its ACK counterpart.
						insert_sqls = insert_sqls + ",'" + interco_flex_value + "'"
					end if
				else
					insert_sqls = insert_sqls + ",'" + value + "'"
					if i_suspense_accounting = "YES" then
						//  The "orig" field is right next to its ACK counterpart.
						insert_sqls = insert_sqls + ",'" + value + "'"
					end if
				end if
				
			next  //  for z = 1 to num_credit_cols - 6 ...
				
			//  dr_cr_id ... (+) amount = 1 ... (-) amount = -1 ...
			if i_rate_field_exists then
				insert_sqls = insert_sqls + "," + string(0)
			end if
			
			credit_amount = i_ds_interco_insert.GetItemNumber(k, num_credit_cols)
			interco_total = interco_total + credit_amount
			if credit_amount >= 0 then
				insert_sqls = insert_sqls + ", 1"
			else
				insert_sqls = insert_sqls + ", -1"
			end if
			//  ledger_sign = 1 ...
			insert_sqls = insert_sqls + ", 1"
			//  qty = 0 ...
			insert_sqls = insert_sqls + ",0"
			//  amount ...
			insert_sqls = insert_sqls + "," + string(credit_amount)
			//  inception_to_date_amount ...
			insert_sqls = insert_sqls + "," + string(credit_amount)
			//  month_number and month_period from the source ...
			insert_sqls = insert_sqls + "," + &
							  string(i_ds_interco_insert.GetItemNumber(k, "month_number")) + &
							  "," + &
							  string(i_ds_interco_insert.GetItemNumber(k, "month_period"))
			//  source_id ...
			insert_sqls = insert_sqls + ",'" + string(source_id) + "'"
			//  budget_version ...
			insert_sqls = insert_sqls + ",'" + i_budget_version + "'"
			//  gl_journal_category ...
			insert_sqls = insert_sqls + ",'" + i_gl_journal_category + "'"
			//  amount_type ...
			insert_sqls = insert_sqls + "," + string(2)
			//  allocation_id ...
			insert_sqls = insert_sqls + "," + string(i_allocation_id)
			//  target_credit ...
			insert_sqls = insert_sqls + ", 'INT_CO'"
			//  cross_charge_company ...
			insert_sqls = insert_sqls + ", '" + cross_company_val + "'"
			
			insert_sqls = insert_sqls + ")"		 
		
			if isnull(insert_sqls) then
				uf_msg(descr, &
	         	  		  "The (Execute immediate # 3) is NULL ... " + &
							  "Check the intercompany definitions for this allocation type. " + &
							  "No allocation will be calculated.")
				rollback using sqlca;
				rtn_code = -1
				goto commit_updates
			end if
		
			//  Execute immediate # 3
			execute immediate :insert_sqls using sqlca;

			if sqlca.SQLCode <> 0 then
				uf_msg(descr, "  ")
				uf_msg("Error - Intercompany Entries", &
				       "ERROR - Intercompany Entries" + sqlca.SQLErrText)
			   uf_msg(descr, "  ")
				uf_msg(descr, "insert_sqls = " + insert_sqls)
				uf_msg(descr, "  ")
				rollback using sqlca;
				rtn_code = -1
				goto commit_updates
			end if
		
		next  //  for k = 1 to num_inserts ...
	
	next	//  for i = 1 to num_rows ... Loop over credit criteria records ...
	
	if interco_total <> 0 then
		uf_msg(descr, "  ")
		uf_msg("", &
		       "ERROR - Intercompany Entries do not balance: " + string(interco_total))
		uf_msg(descr, "  ")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
	
end if


//*****************************************************************************************
//
//  Run by Deparment Flag:
//
//  9/17/07: Only call this function for Clearing-Calc Rate allocations.  The methodology
//  is simpler for Clearing-Input Rate allocations and is completely handled in this
//  script.
//
//*****************************************************************************************
if i_run_by_dept_credits_method <> "YES" then
	if run_by_dept = 1 and i_clearing_indicator = 3 then
		//
		//  Call the function that calculates departmental-based clearings ...
		//
		rtn = uf_run_by_department(i_allocation_id, i_month_number, i_month_period, &
			i_clearing_indicator, cr_company_id, i_company_field, i_gl_journal_category, i_ds_source, &
			group_by_id, descr, where_clause_id, clearing_id, work_order_field, custom_dw)
		
		if rtn <> 1 then
			uf_msg(descr, "ERROR: in the Run by Department logic")
			rollback using sqlca;
			rtn_code = -1
			goto commit_updates
		end if
	end if
end if


//*****************************************************************************************
//
//  Base Derivations:
//
//*****************************************************************************************
if i_derivation_flag = 1 then
	rtn = uf_derivations()
	if rtn <> 1 then
		uf_msg(descr, "  ")
		uf_msg(descr, "ERROR: in the derivations")
		rollback using sqlca;
		rtn_code = -1
		goto commit_updates
	end if
end if


//*****************************************************************************************
//
//  drilldown_key:
//
//*****************************************************************************************
//uf_msg("", string(i_allocation_id) + ": Updating drilldown_key at " + &
//           string(today(), "mm/dd/yyyy hh:mm:ss"))

if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
	if i_run_as_a_test then
		if i_ifb_field_exists then
			sqls = "update cr_budget_data_test set drilldown_key = 'A', interface_batch_id = '" + i_ifb_id + "' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		else
			sqls = "update cr_budget_data_test set drilldown_key = 'A' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		end if
	else
		if i_ifb_field_exists then
			sqls = "update cr_budget_data set drilldown_key = 'A', interface_batch_id = '" + i_ifb_id + "' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		else
			sqls = "update cr_budget_data set drilldown_key = 'A' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		end if
	end if
else                                                          //  Other Allocations ...
	if i_run_as_a_test then
		if i_ifb_field_exists then
			sqls = "update cr_budget_data_test set drilldown_key = 'A', interface_batch_id = '" + i_ifb_id + "' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		else
			sqls = "update cr_budget_data_test set drilldown_key = 'A' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		end if
	else
		if i_ifb_field_exists then
			sqls = "update cr_budget_data set drilldown_key = 'A', interface_batch_id = '" + i_ifb_id + "' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		else
			sqls = "update cr_budget_data set drilldown_key = 'A' " + &
					 " where drilldown_key is null " + &
						" and allocation_id = " + string(i_allocation_id) + &
						" and budget_version = '" + i_budget_version + "'"
		end if
	end if
end if


if isnull(sqls) then
		uf_msg(descr, "The (Execute immediate # 4) is NULL ... " + &
				    "No allocation will be calculated.")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

//  Execute immediate # 4
execute immediate :sqls using sqlca;

if sqlca.SQLCode <> 0 then
	uf_msg(descr, "  ")
	uf_msg(descr, "ERROR - DrillDown Key " + sqlca.SQLErrText)
	uf_msg(descr, "  ")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if


//
//  MOVED INTO THE UPDATE ABOVE BECAUSE IT WAS HARD TO AVOID HITTING PREVIOUS MONTHS' RECORDS
//
////*****************************************************************************************
////
////  interface_batch_id:  updated for validations that will occur in wf_run()
////  
////    Using a unique sequence number and saving permanaently (even though it is not
////    nearly as important as with actuals).  We save permanently so the value can be
////    filtered in the kickouts screen if multiple users are running budget allocations
////    and they both need to deal with validation errors.
////    
////    Changed:  update to some # here ... then update later with the nextval, so each
////    allocation does not get a separate ifb_id !!!
////
////*****************************************************************************************
//if i_ifb_field_exists then
//	//  Continue.
//else
//	goto after_ifb_update
//end if
//
//if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
//	if i_run_as_a_test then
//		sqls = "update cr_budget_data_test set interface_batch_id = " + i_ifb_id + " " + &
//		       " where drilldown_key = 'A' " + &
//				   " and allocation_id = " + string(i_allocation_id) + &
//					" and budget_version = '" + i_budget_version + "'"
//	else
//		sqls = "update cr_budget_data set interface_batch_id = " + i_ifb_id + " " + &
//		       " where drilldown_key = 'A' " + &
//				   " and allocation_id = " + string(i_allocation_id) + &
//					" and budget_version = '" + i_budget_version + "'"
//	end if
//else                                                          //  Other Allocations ...
//	if i_run_as_a_test then
//		sqls = "update cr_budget_data_test set interface_batch_id = " + i_ifb_id + " " + &
//		       " where drilldown_key = 'A' " + &
//				   " and allocation_id = " + string(i_allocation_id) + &
//					" and budget_version = '" + i_budget_version + "'"
//	else
//		sqls = "update cr_budget_data set interface_batch_id = " + i_ifb_id + " " + &
//		       " where drilldown_key = 'A' " + &
//				   " and allocation_id = " + string(i_allocation_id) + &
//					" and budget_version = '" + i_budget_version + "'"
//	end if
//end if
//
//
//if isnull(sqls) then
//	uf_msg(descr, "  ")
//	uf_msg(descr, "ERROR: The (Execute immediate - ifb_id) is NULL ... " + &
//			    "No allocation will be calculated.")
//	uf_msg(descr, "  ")
//	rollback using sqlca;
//	rtn_code = -1
//	goto commit_updates
//end if
//
////  Execute immediate # 5
//execute immediate :sqls using sqlca;
//
//if sqlca.SQLCode <> 0 then
//	uf_msg(descr, "  ")
//	uf_msg(descr, "ERROR - Interface Batch Id " + sqlca.SQLErrText)
//	uf_msg(descr, "  ")
//	rollback using sqlca;
//	rtn_code = -1
//	goto commit_updates
//end if
//
//after_ifb_update:



//uo_cr_allocations_cleanup_bdg uo_cleanup
//uo_cleanup = CREATE uo_cr_allocations_cleanup_bdg
cleanup_long_array_arg[1] = i_allocation_id
cleanup_long_array_arg[2] = i_month_number	
cleanup_long_array_arg[3] = i_month_period
rtn = i_uo_cleanup_bdg.uf_cleanup1(this, cleanup_long_array_arg)
//DESTROY uo_cleanup
if rtn <> 1 then
	uf_msg(descr, "  ")
	uf_msg(descr, "ERROR: in the custom derivations")
	uf_msg(descr, "  ")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if


//*****************************************************************************************
//
//  Balance by Company:  System switched since some companies send allocation results to
//                       their GL's without running intercompany accounting.
//
//*****************************************************************************************
setnull(balancing_cv)
select upper(trim(control_value)) into :balancing_cv from cr_system_control
 where upper(trim(control_name)) = 'ALLOCATIONS-BDG - OOB CHECK';
if isnull(balancing_cv) then balancing_cv = "NO"

if balancing_cv <> "YES" then goto after_balancing_check

balancing_sqls = &
	"select count(*) from (" + &
	'select "' + i_company_field + '", gl_journal_category, month_number, sum(amount) ' + &
	"from "

if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
	if i_run_as_a_test then
		balancing_sqls = balancing_sqls + "cr_budget_data_test "
	else
		balancing_sqls = balancing_sqls + "cr_budget_data "
	end if
else                                                          //  Other Allocations ...
	if i_run_as_a_test then
		balancing_sqls = balancing_sqls + "cr_budget_data_test "
	else
		balancing_sqls = balancing_sqls + "cr_budget_data "
	end if
end if

balancing_sqls = balancing_sqls + "where allocation_id = " + string(i_allocation_id) + " "
balancing_sqls = balancing_sqls + "and month_number = " + string(i_month_number) + " "
balancing_sqls = balancing_sqls + 'group by "' + i_company_field + '", gl_journal_category, month_number '
balancing_sqls = balancing_sqls + "having sum(amount) <> 0)"

i_ds_oob.SetSQLSelect(balancing_sqls)
i_ds_oob.RETRIEVE()

counter = 0
if i_ds_oob.RowCount() > 0 then
	counter = i_ds_oob.GetItemNumber(1, 1)
	if isnull(counter) then counter = 0
end if

if counter > 0 then
	uf_msg(descr, "  ")
	uf_msg(descr, "----------------------------------------------------------------------------------------------------------------------------------------------")
	uf_msg(descr, "ERROR: The allocation results do not balance by " + &
		i_company_field + ", GL Journal Category, and Month Number !")
	uf_msg(descr, "----------------------------------------------------------------------------------------------------------------------------------------------")
	uf_msg(descr, "  ")
	rollback using sqlca;
	rtn_code = -1
	goto commit_updates
end if

after_balancing_check:



rtn_code = 1

commit_updates:

//  THE COMMIT IS IN uf_start_allocations ...

//DESTROY i_ds_elements --- DMJ: 8/18/08: MOVED TO AN INSTANCE DS
//DESTROY ds_group_by
//DESTROY ds_where_clause
//DESTROY ds_source
//DESTROY ds_target
//DESTROY ds_credit
//DESTROY ds_credit_insert
//DESTROY ds_interco
//DESTROY ds_interco_insert


return rtn_code
end function

public function decimal uf_clearing_balance (longlong a_cr_company_id, string a_company_field);//******************************************************************************************
//
//  User Object Function:  uf_clearing_balance
//
//	 Description         :  Retrieves the sum(amount) from cr_budget_data based on
//								   a where clause that the user defines in the 
//									cr_allocation_control_bdg.balance_where_clause_id ...
//
//  Notes               :  This is hardcoded to return only 1 balance (pot).
//									It retrieves it for all months in the cost_repository, so any
//									remaining balance that is inadvertently left in the pot will
//									clear.
//
//                         Added code to restrict the pot by the cr_company_id on
//                         cr_allocation_control_bdg.
//
//******************************************************************************************
longlong num_rows, i, balance_where_clause_id, &
       clearing_balance_start_month, clearing_balance_end_month, rtn, structure_id, &
		 not_field
string sqls, left_paren, col_name, operator, value1, between_and, value2, &
		 balance_sqls, right_paren, and_or, external_company_id, structure_where, &
		 orig_value1, not_upper_value1, rtn_f_create_str, hint_syntax, rtns
decimal {2} balance

clearing_balance_start_month = 0
clearing_balance_end_month   = 0

select balance_where_clause_id,
       clearing_balance_start_month, 
		 clearing_balance_end_month
  into :balance_where_clause_id, 
       :clearing_balance_start_month,
		 :clearing_balance_end_month
  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;

//  The user chose "Current" as the option ...
if clearing_balance_start_month = 0 then clearing_balance_start_month = i_month_number
if   clearing_balance_end_month = 0 then   clearing_balance_end_month = i_month_number

//  The ytd_indicator overrides any selected months.
if i_ytd_indicator = 1 then
	clearing_balance_start_month = long(left(string(i_month_number), 4) + "01")
	clearing_balance_end_month   = i_month_number
end if

balance_sqls = "select sum(amount) from cr_budget_data "

//  Where Clause:
//uo_ds_top ds_where_clause
//ds_where_clause = CREATE uo_ds_top
i_ds_where_clause.reset()

i_ds_where_clause.DataObject = "dw_temp_dynamic"
i_ds_where_clause.SetTransObject(sqlca)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(balance_where_clause_id) + " order by row_id"

f_create_dynamic_ds(i_ds_where_clause, "grid", sqls, sqlca, true)

num_rows = i_ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		if isnull(a_cr_company_id) or a_cr_company_id = 0 then
			//  DO NOT restrict by company ...
			balance_sqls = balance_sqls + &
			             " where month_number >= " + string(clearing_balance_start_month) + &
							 "   and month_number <= " + string(clearing_balance_end_month) + &
							 "   and budget_version = '" + i_budget_version + "'" + &
			             "   and ( "
		else
			if i_ignore_header_co = 1 then
				// DO NOT restrict by company...
				balance_sqls = balance_sqls + &
			             " where month_number >= " + string(clearing_balance_start_month) + &
							 "   and month_number <= " + string(clearing_balance_end_month) + &
							 "   and budget_version = '" + i_budget_version + "'" + &
			             "   and ( "
			else
				//  DO restrict by company ...
				select external_company_id into :external_company_id
				  from cr_company where cr_company_id = :a_cr_company_id;
				balance_sqls = balance_sqls + &
								 " where month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "'" + &
								 "   and " + a_company_field + " = '" + &
									upper(trim(external_company_id)) + "' and ( "
								
			end if			
		end if
	end if

	left_paren  = upper(trim(i_ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(i_ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(i_ds_where_clause.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(i_ds_where_clause.GetItemString(i, "value1"))
	orig_value1 = upper(i_ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{" then
			value1 = not_upper_value1
		else
			value1 = upper(trim(i_ds_where_clause.GetItemString(i, "value1")))
		end if
	end if
	
	between_and = upper(trim(i_ds_where_clause.GetItemString(i, "between_and")))
	value2      = upper(trim(i_ds_where_clause.GetItemString(i, "value2")))
	right_paren = upper(trim(i_ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(i_ds_where_clause.GetItemString(i, "and_or")))
	structure_id = i_ds_where_clause.GetItemNumber(i, "structure_id")
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0
	
	//  If they manually entered an Oracle function, take it as-is.
	if pos(col_name, "(") = 0 then
		col_name = f_cr_clean_string(col_name)
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = &
			uf_structure_value(structure_id, col_name, value1, not_field)
//		balance_sqls = balance_sqls + structure_where + " and "
		balance_sqls = balance_sqls + left_paren + structure_where + right_paren + " " + and_or + " "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			end if
		else
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			end if
		end if
	else
		if pos(col_name, "(") <> 0 then
			//  NO DOUBLE QUOTES ON COL_NAME!
			balance_sqls = balance_sqls + &
								left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
		else
			balance_sqls = balance_sqls + &
								left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or		
		end if
	end if
	
	bottom:
	
	if i = num_rows then
		balance_sqls = balance_sqls + " )"
	end if
	
next

i_ds_balance.reset()

// ### 10906: JAK: 2012-09-11:  Move f_sql_add_hint lower so it can be applied to latter select statements
// ### - SEK - 061311 - 7742: Look up and add any hints using f_sql_add_hint
hint_syntax = 'bud_alloc_balance_' + string(i_allocation_id)
rtns = f_sql_add_hint(balance_sqls, hint_syntax)

if isnull(rtns) or rtns = "" then
	uf_msg("","  ")
	uf_msg("","ERROR: Adding hint '" + hint_syntax + "'")
	uf_msg(""," THE ALLOCATION WILL CONTINUE TO RUN ")
	uf_msg("","  ")
else 
	balance_sqls = rtns
end if

rtn_f_create_str = f_create_dynamic_ds(i_ds_balance, "grid", balance_sqls, sqlca, true)

if rtn_f_create_str <> "OK" then
//if dserror <> "OK" then
	//f_pp_msgs("ERROR in uf_clearing_balance_xcc_qty: " + left(dserror, 224))
	f_pp_msgs("ERROR in uf_clearing_balance: " + string(rtn))
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	i_errors = true
	return -1
end if

//  Save the balance SQL for reporting ...
i_balance_sqls = uf_save_report_sql_balance(balance_sqls, false)

if rtn = -999 then
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	i_errors = true
	return -1
end if
//  i_ds_balance now contains the pot of dollars ...

balance = i_ds_balance.GetItemNumber(1, 1)

//DESTROY ds_where_clause
//DESTROY ds_balance

return balance
end function

public function decimal uf_clearing_balance_xcc (longlong a_cr_company_id, string a_company_field, longlong a_allocation_id, longlong a_month_number, string a_charging_cost_center, string a_dept_field);//******************************************************************************************
//
//  User Object Function:  uf_clearing_balance
//
//	 Description         :  Retrieves the sum(amount) from cr_budget_data based on
//								   a where clause that the user defines in the 
//									cr_allocation_control_bdg.balance_where_clause_id ...
//
//  Notes               :  This is hardcoded to return only 1 balance (pot).
//									It retrieves it for all months in the cost_repository, so any
//									remaining balance that is inadvertently left in the pot will
//									clear.
//
//                         Added code to restrict the pot by the cr_company_id on
//                         cr_allocation_control_bdg.
//
// -------------------------------------------------------------------------------
//
//  THIS FUNCTION IS THE SAME AS THE ONE IN UO_CR_ALLOCATIONS EXCEPT WE ADDED
//  A HARDCODED " XCC = :XCC " TO THE WHERE CLAUSE.
//
// -------------------------------------------------------------------------------
//
//******************************************************************************************
longlong num_rows, i, balance_where_clause_id, &
       clearing_balance_start_month, clearing_balance_end_month, structure_id, &
		 not_field, where_source_id, rtn
string sqls, left_paren, col_name, operator, value1, between_and, value2, &
		 balance_sqls, right_paren, and_or, external_company_id, dserror, structure_where, &
		 orig_value1, table_name, not_upper_value1, department_field, rtn_f_create_str, &
		 hint_syntax, rtns
decimal {2} balance


department_field             = a_dept_field
clearing_balance_start_month = 0
clearing_balance_end_month   = 0

select balance_where_clause_id,
       clearing_balance_start_month, 
		 clearing_balance_end_month
  into :balance_where_clause_id, 
       :clearing_balance_start_month,
		 :clearing_balance_end_month
  from cr_allocation_control_bdg where allocation_id = :a_allocation_id;

//  The user chose "Current" as the option ...
if clearing_balance_start_month = 0 then clearing_balance_start_month = a_month_number
if   clearing_balance_end_month = 0 then   clearing_balance_end_month = a_month_number

//  The i_ytd_indicator overrides any selected months.
if i_ytd_indicator = 1 then
	clearing_balance_start_month = long(left(string(a_month_number), 4) + "01")
	clearing_balance_end_month   = a_month_number
end if


//  Check for a detailed attribute allocation.
where_source_id = 0
select source_id into :where_source_id from cr_alloc_where_bdg
 where where_clause_id = :balance_where_clause_id;
if isnull(where_source_id) then where_source_id = 0

i_where_source_id = where_source_id

if where_source_id = 0 then
	table_name = "CR_BUDGET_DATA"
else
	setnull(table_name)
	select trim(table_name) into :table_name from cr_sources 
	 where source_id = :where_source_id;
	if isnull(table_name) or table_name = "" then
		f_pp_msgs("Could not find the table_name for where_source_id = " + &
			string(where_source_id) + "!  No allocation will be calculated.")
		rollback using sqlca;
		// ### 8142: JAK: 2011-11-01:  Consistent return codes
		i_errors = true
		return -1
	end if
end if


balance_sqls = "select sum(amount) from " + table_name + " "

//  Where Clause:
uo_ds_top ds_where_clause
ds_where_clause = CREATE uo_ds_top
ds_where_clause.DataObject = "dw_temp_dynamic"
ds_where_clause.SetTransObject(sqlca)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(balance_where_clause_id) + " order by row_id"

f_create_dynamic_ds(ds_where_clause, "grid", sqls, sqlca, true)

num_rows = ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		if isnull(a_cr_company_id) or a_cr_company_id = 0 then
			//  DO NOT restrict by company ...
			balance_sqls = balance_sqls + &
			             ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
							 "   and month_number >= " + string(clearing_balance_start_month) + &
							 "   and month_number <= " + string(clearing_balance_end_month) + &
							 "   and budget_version = '" + i_budget_version + "' " + &
			             "   and ( "
			i_balance_where_clause = &
							 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
							 "   and month_number >= " + string(clearing_balance_start_month) + &
							 "   and month_number <= " + string(clearing_balance_end_month) + &
							 "   and budget_version = '" + i_budget_version + "' " + &
			             "   and ( "
		else
			if i_ignore_header_co = 1 then
				//  DO NOT restrict by company ...
				balance_sqls = balance_sqls + &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and ( "
				i_balance_where_clause = &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and ( "
			else
				//  DO restrict by company ...
				select external_company_id into :external_company_id
				  from cr_company where cr_company_id = :a_cr_company_id;
				balance_sqls = balance_sqls + &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and " + a_company_field + " = '" + &
									upper(trim(external_company_id)) + "' and ( "
				i_balance_where_clause = &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and " + a_company_field + " = '" + &
									upper(trim(external_company_id)) + "' and ( "
									
			end if           
		end if
	end if

	left_paren  = upper(trim(ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(ds_where_clause.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(ds_where_clause.GetItemString(i, "value1"))
	orig_value1 = upper(ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{" or &
			col_name = "GL JOURNAL CATEGORY" &
		then
			value1 = not_upper_value1
		else
			value1 = upper(trim(ds_where_clause.GetItemString(i, "value1")))
		end if
	end if
	
	between_and = upper(trim(ds_where_clause.GetItemString(i, "between_and")))
	value2      = upper(trim(ds_where_clause.GetItemString(i, "value2")))
	right_paren = upper(trim(ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(ds_where_clause.GetItemString(i, "and_or")))
	structure_id = ds_where_clause.GetItemNumber(i, "structure_id")
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0
	
	//  If they manually entered an Oracle function, take it as-is.
	if pos(col_name, "(") = 0 then
		col_name = f_cr_clean_string(col_name)
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = &
			uf_structure_value(structure_id, col_name, value1, not_field)
		balance_sqls = balance_sqls + structure_where + " and "
		i_balance_where_clause = i_balance_where_clause + structure_where + " and "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			end if
		else
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			end if
		end if
	else
		if pos(col_name, "(") <> 0 then
			//  NO DOUBLE QUOTES ON COL_NAME!
			balance_sqls = balance_sqls + &
								left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
			i_balance_where_clause = i_balance_where_clause + &
								left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
		else
			balance_sqls = balance_sqls + &
								left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
			i_balance_where_clause = i_balance_where_clause + &
								left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
		end if
	end if
	
	bottom:
	
	if i = num_rows then
		balance_sqls = balance_sqls + " )"
		i_balance_where_clause = i_balance_where_clause + " )"
	end if
	
next

uo_ds_top ds_balance
ds_balance = CREATE uo_ds_top

// ### 10906: JAK: 2012-09-11:  Move f_sql_add_hint lower so it can be applied to latter select statements
// ### - SEK - 061311 - 7742: Look up and add any hints using f_sql_add_hint
hint_syntax = 'bud_alloc_balance_' + string(i_allocation_id)
rtns = f_sql_add_hint(balance_sqls, hint_syntax)

if isnull(rtns) or rtns = "" then
	uf_msg("","  ")
	uf_msg("","ERROR: Adding hint '" + hint_syntax + "'")
	uf_msg(""," THE ALLOCATION WILL CONTINUE TO RUN ")
	uf_msg("","  ")
else 
	balance_sqls = rtns
end if

rtn_f_create_str = f_create_dynamic_ds(ds_balance, "grid", balance_sqls, sqlca, true)
//  ds_balance now contains the pot of dollars ...

if rtn_f_create_str <> "OK" then
//if dserror <> "OK" then
	//f_pp_msgs("ERROR in uf_clearing_balance_xcc: " + left(dserror, 224))
	f_pp_msgs("ERROR in uf_clearing_balance_xcc: " + string(rtn))
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	i_errors = true
	return -1
end if

if ds_balance.RowCount() = 0 then
	return 0
end if

balance = ds_balance.GetItemNumber(1, 1)

DESTROY ds_where_clause
DESTROY ds_balance

return balance
end function

public function longlong uf_auto_reverse ();//*******************************************************************************************
//
//  User Object Function  :  uf_auto_reverse
//
//  Arguments    :  none
//
//  Returns      :  longlong (1=good return, -1=error)
//
//  Description  :  This script will book entries reversing the results of an 
//                  allocation from the cost repository.  
//
//*******************************************************************************************
longlong allocation_row, allocation_id, rtn, month_number, month_period, counter, &
		 month_row, i, source_id, &
		 run_month_number, run_month_period
// SEK 082410: Changed max_id and orig_max_id from longlong to longlong due to volume of
//		data at Southern. 		 
longlong max_id, id
string sqls, element_descr, table_name, field, detail_cv, summary_cv


month_number       = i_month_number
run_month_period   = i_month_period
allocation_id      = i_allocation_id

// Always reverse the previous month, unless we are in January then do not reverse
run_month_number   = i_month_number - 1
if mid(string(run_month_number),5,2) = '00' then
	if i_auto_reverse_cross_years = 1 then
		//  Flag is on  ... allow the auto reversal from the prior december.
		run_month_number = run_month_number - 88 // e.g. 200900 - 88 = 200812
	else
		//  Flag is off ... log a msg and scram.
		uf_msg("","Auto Reverse: This allocation is running for January.  " + &
			"December will not be reversed.")
		return 1
	end if
end if


setnull(detail_cv)
setnull(summary_cv)
select upper(rtrim(control_value)) into :detail_cv from cr_system_control
 where upper(rtrim(control_name)) = 'USE SEQUENCE CR_ALLOC';

select upper(rtrim(control_value)) into :summary_cv from cr_system_control
 where upper(rtrim(control_name)) = 'USE SEQUENCE PWRPLANT4';

if isnull(detail_cv)  then detail_cv  = "NO"
if isnull(summary_cv) then summary_cv = "NO"


month_period = 0

if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
	if i_test_or_actual = "ACTUAL" then
		select max(month_period) into :month_period from cr_budget_data
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number and budget_version = :i_budget_version;
	else
		select max(month_period) into :month_period from cr_budget_data_test
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number and budget_version = :i_budget_version;
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		select max(month_period) into :month_period from cr_budget_data
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number and budget_version = :i_budget_version;
	else
		select max(month_period) into :month_period from cr_budget_data_test
		 where (target_credit = 'RVTARG' or target_credit = 'RVCRED') and
      		  month_number = :month_number and budget_version = :i_budget_version;
	end if
end if

if isnull(month_period) then month_period = 0

month_period++


//  If the allocation has not run for this month and period, there is nothing to do ...
//  However, if the month and period are not found, we must first check of a period 0
//  in case they ran the allocation on a "whole month" basis.
counter = 0

/*### - MDZ - 9058 - 20120119*/
/*Added trim to upper(test_or_actual) to be consistent.*/
// ### 7956: JAK: 2012-02-15:  Add budget version criteria
select count(*) into :counter
  from cr_alloc_process_control_bdg
 where allocation_id         = :allocation_id and
 		 month_number          = :run_month_number  and
		 month_period          = :run_month_period  and
		 upper(trim(test_or_actual)) = :i_test_or_actual and
		  budget_version = :i_budget_version;

if counter <= 0 then

	run_month_period = 0
	counter          = 0
	
	/*### - MDZ - 9058 - 20120119*/
	/*Added trim to upper(test_or_actual) to be consistent.*/
	// ### 7956: JAK: 2012-02-15:  Add budget version criteria
	select count(*) into :counter
	  from cr_alloc_process_control_bdg
	 where allocation_id         = :allocation_id and
 			 month_number          = :run_month_number and
			 month_period          = :run_month_period and
			 upper(trim(test_or_actual)) = :i_test_or_actual and
			 budget_version = :i_budget_version;
	
	if counter <= 0 then
		uf_msg("","Auto Reverse: This allocation has not been run for month " + &
			string(run_month_number) + ", period " + string(run_month_period) + &
				".  Nothing to reverse.")
		return 1
	end if
	
end if


//  If the allocation has already been reversed, do not let them reverse it again ...
counter = 0

/*### - MDZ - 9058 - 20120119*/
/*Added trim to upper(test_or_actual) to be consistent.*/
// ### 7956: JAK: 2012-02-15:  Add budget version criteria
select reversed into :counter
  from cr_alloc_process_control_bdg
 where allocation_id         = :allocation_id     and
 		 month_number          = :run_month_number  and
		 month_period          = :run_month_period  and
		 upper(trim(test_or_actual)) = :i_test_or_actual and
		  budget_version = :i_budget_version;
if isnull(counter) then counter = 0

if counter > 0 then
	uf_msg("","Auto Reverse: This allocation has already been reversed.")
	return 1
end if




//  BEFORE FIRST:  Flush the temporary holding table ...
delete from cr_budget_reversals;
commit;

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: deleting from cr_budget_reversals~n~n" + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	return -1
end if


//  FIRST:  Insert records from the cr_budget_data or cr_budget_data_test table ... or the
//          appropriate inter-company table ... to the cr_budget_reversals table.
//          The "reversal_id is NULL" is EXTREMELY important.  A freshly run allocation
//          will have a NULL reversal_id.  A reversed or undone allocation will have
//          a 0 reversal_id in the original allocation results and a cr_budget_data.id
//          in the reversal_id for the reversal records.  The "is NULL" prevents us
//          from undoing the same allocation more than once!
if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then uf_msg("debug", &
			"Auto Reverse: Inserting cr_budget_data records into cr_budget_reversals ...")
		insert into cr_budget_reversals
			(select * from cr_budget_data
		     where month_number = :run_month_number and 
	   	        month_period = :run_month_period and 
					  budget_version = :i_budget_version and
   	   	     allocation_id = :allocation_id   and
					  reversal_id is NULL);
	else
		if g_debug = 'YES' then uf_msg("debug", &
			"Auto Reverse: Inserting cr_budget_data_test records into cr_budget_reversals ...")
		insert into cr_budget_reversals
			(select * from cr_budget_data_test
		     where month_number = :run_month_number and 
	   	        month_period = :run_month_period and 
					  budget_version = :i_budget_version and
   	   	     allocation_id = :allocation_id   and
					  reversal_id is NULL);
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then uf_msg("debug", &
			"Auto Reverse: Inserting cr_budget_data records into cr_budget_reversals ...")
		insert into cr_budget_reversals
			(select * from cr_budget_data
			  where month_number = :run_month_number and 
	   		     month_period = :run_month_period and 
					  budget_version = :i_budget_version and
   	   		  allocation_id = :allocation_id   and
					  reversal_id is NULL);
	else
		if g_debug = 'YES' then uf_msg("debug", &
			"Auto Reverse: Inserting cr_budget_data_test records into cr_budget_reversals ...")
		insert into cr_budget_reversals
			(select * from cr_budget_data_test
		     where month_number = :run_month_number and 
		           month_period = :run_month_period and 
					  budget_version = :i_budget_version and
   		        allocation_id = :allocation_id   and
					  reversal_id is NULL);	
	end if
end if

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
		if i_test_or_actual = "ACTUAL" then
			uf_msg("","ERROR: Auto Reverse: inserting from cr_budget_data: " + sqlca.SQLErrText)
		else
			uf_msg("","ERROR: Auto Reverse: inserting from cr_budget_data_test: " + sqlca.SQLErrText)
		end if
	else
		if i_test_or_actual = "ACTUAL" then
			uf_msg("","ERROR: Auto Reverse: inserting from cr_budget_data: " + sqlca.SQLErrText)
		else
			uf_msg("","ERROR: Auto Reverse: inserting from cr_budget_data_test: " + sqlca.SQLErrText)
		end if
	end if
	uf_msg("", "  ")
	rollback;
	return -1
end if

//  BEFORE SECOND:  Update cr_budget_reversals ... and insert back
//                  into the appropriate table.  The reversal_id field
//                  allows us to keep track of the reversals against their
//                  original transaction.  Then if they ever delete the allocation,
//                  the reversal will be deleted also instead of being stranded.
update cr_budget_reversals
   set amount = -amount, quantity = -quantity, reversal_id = id,
	    month_number = :month_number, month_period = :month_period,
		 dr_cr_id = -dr_cr_id;
	
if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: updating cr_budget_reversals.amount: " + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	return -1
end if

update cr_budget_reversals
   set target_credit = 'RVTARG'
 where upper(rtrim(target_credit)) = 'TARGET';

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: updating cr_budget_reversals.target: " + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	return -1
end if

update cr_budget_reversals
   set target_credit = 'RVCRED'
 where upper(rtrim(target_credit)) = 'CREDIT';

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: updating cr_budget_reversals.target: " + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	return -1
end if

update cr_budget_reversals
   set target_credit = 'RVINCO'
 where upper(rtrim(target_credit)) = 'INT_CO';

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: updating cr_budget_reversals.int_co: " + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	return -1
end if

update cr_budget_reversals set drilldown_key = drilldown_key || target_credit;

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: updating cr_budget_reversals.drilldown_key: " + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	return -1
end if


//  Change the (PK) ...
id = 0
if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
	if i_test_or_actual = "ACTUAL" then
		select max(id) into :id from cr_budget_data;
	else
		select max(id) into :id from cr_budget_data_test;
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		select max(id) into :id from cr_budget_data;
	else
		select max(id) into :id from cr_budget_data_test;
	end if
end if
if isnull(id) then id = 0

if detail_cv = "NO" then
	update cr_budget_reversals set id = :id + rownum;
else
	update cr_budget_reversals set id = crbudgets.nextval;
end if

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
		if i_test_or_actual = "ACTUAL" then
			uf_msg("","ERROR: Auto Reverse: updating (PK): cr_budget_data: " + sqlca.SQLErrText)
		else
			uf_msg("","ERROR: Auto Reverse: updating (PK): cr_budget_data_test: " + sqlca.SQLErrText)
		end if
	else
		if i_test_or_actual = "ACTUAL" then
			uf_msg("","ERROR: Auto Reverse: updating (PK): cr_budget_data: " + sqlca.SQLErrText)
		else
			uf_msg("","ERROR: Auto Reverse: updating (PK): cr_budget_data_test: " + sqlca.SQLErrText)
		end if
	end if
	uf_msg("", "  ")
	rollback;
	return -1
end if


//  INSERT the reversal ...
if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then uf_msg("debug","Auto Reverse: Inserting cr_budget_data records ...")
		insert into cr_budget_data (select * from cr_budget_reversals);
	else
		if g_debug = 'YES' then uf_msg("debug","Auto Reverse: Inserting cr_budget_data_test records ...")
		insert into cr_budget_data_test (select * from cr_budget_reversals);
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		if g_debug = 'YES' then uf_msg("debug","Auto Reverse: Inserting cr_budget_data records ...")
		insert into cr_budget_data (select * from cr_budget_reversals);
	else
		if g_debug = 'YES' then uf_msg("debug","Auto Reverse: Inserting cr_budget_data_test records ...")
		insert into cr_budget_data_test (select * from cr_budget_reversals);	
	end if
end if

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
		if i_test_or_actual = "ACTUAL" then
			uf_msg("","ERROR: Auto Reverse: inserting to cr_budget_data: " + sqlca.SQLErrText)
		else
			uf_msg("","ERROR: Auto Reverse: inserting to cr_budget_data_test: " + sqlca.SQLErrText)
		end if
	else
		if i_test_or_actual = "ACTUAL" then
			uf_msg("","ERROR: Auto Reverse: inserting to cr_budget_data: " + sqlca.SQLErrText)
		else
			uf_msg("","ERROR: Auto Reverse: inserting to cr_budget_data_test: " + sqlca.SQLErrText)
		end if
	end if
	uf_msg("", "  ")
	rollback;
	return -1
end if


//*******************  N/A FOR BUDGET ALLOCATIONS  *******************
////  SECOND:  Update cr_cost_repository ...
//if g_debug = 'YES' then uf_msg("debug","Auto Reverse: Inserting cr_cost_repository records ...")
//
//uo_ds_top i_ds_elements
//i_ds_elements = CREATE uo_ds_top
//i_ds_elements.DataObject = "dw_cr_element_definitions"
//i_ds_elements.SetTransObject(sqlca)
//i_num_elements = i_ds_elements.RETRIEVE()
////  NEVER update cr_cost_repository if this allocation is being run as a test ...
//if i_test_or_actual <> "ACTUAL" then goto after_cr_update
//
////  1.  INSERT to cr_temp_cr ...
//sqls = "insert into cr_temp_cr ("
//
//for i = 1 to i_num_elements
//	element_descr = upper(trim(i_ds_elements.GetItemString(i, "description")))
//	element_descr = f_cr_clean_string(element_descr)
//	sqls = sqls + '"' + element_descr + '", '
//next
//
//sqls = sqls + "dr_cr_id, ledger_sign, month_number, month_period, " + &
//              "gl_journal_category, amount_type, source_id, " + &
//				  "drilldown_key, quantity, amount) (select "
//
//for i = 1 to i_num_elements
//	element_descr = upper(trim(i_ds_elements.GetItemString(i, "description")))
//	element_descr = f_cr_clean_string(element_descr)
//	sqls = sqls + '"' + element_descr + '", '
//next
//
//source_id = 0
//if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
//	select source_id into :source_id
//	  from cr_sources where upper(rtrim(description)) = 'INTER-COMPANY';
//else                                                          //  Other Allocations ...
//	select source_id into :source_id
//	  from cr_sources where upper(rtrim(description)) = 'ALLOCATIONS';
//end if
//
//if source_id = 0 or isnull(source_id) then
//	uf_msg("", "  ")
//	uf_msg("","ERROR: Auto Reverse: Source not defined.  No reversal will be generated.")
//	uf_msg("", "  ")
//	rollback;
//	DESTROY i_ds_elements
//	return -1	
//end if
//
//sqls = sqls + "dr_cr_id, ledger_sign, month_number, month_period, gl_journal_category," + &
//              "amount_type, " + &
//			      string(source_id)
//
////  amount_type is always 1 ... ACTUALS ...
//sqls = sqls + ", drilldown_key, sum(quantity), sum(amount) from "
//
//select table_name into :table_name
//  from cr_sources where source_id = :source_id;
//  
//sqls = sqls + '"' + table_name + '"'
//
////  Get all records (targets and credits) for this allocation_id, month_number, 
////  month_period combination ...
//sqls = sqls + ' where allocation_id = ' + string(allocation_id) + " and "
//sqls = sqls + ' month_number = ' + string(month_number) + " and "
//sqls = sqls + ' month_period = ' + string(month_period) + " and "
//sqls = sqls + " budget_version = :i_budget_version and and "
//sqls = sqls + " (target_credit = 'RVTARG' or target_credit = 'RVCRED'" + &
//                                         "or target_credit = 'RVINCO')"
//sqls = sqls + ' group by '
//
//for i = 1 to i_num_elements
//	field = i_ds_elements.GetItemString(i, "description")
//	field = f_cr_clean_string(field)
//	sqls = sqls + '"' + upper(field) + '", '
//next
//
//sqls = sqls + "dr_cr_id, ledger_sign, month_number, month_period, " + &
//              "gl_journal_category, amount_type, drilldown_key)"
//
//execute immediate :sqls;
//
//if sqlca.SQLCode <> 0 then
//	uf_msg("", "  ")
//	uf_msg("","ERROR: Auto Reverse: Update cr_cost_repository (1): " + sqlca.SQLErrText)
//	uf_msg("",sqls)
//	uf_msg("", "  ")
//	rollback;
//	DESTROY i_ds_elements
//	return -1
//end if
//
////  2.  UPDATE id (PK) field ...
//max_id = 0
//select max(id) into :max_id from cr_cost_repository;
// 
//if isnull(max_id) then max_id = 0
//
//if summary_cv = "NO" then
//	update cr_temp_cr set id = rownum + :max_id;
//else
//	update cr_temp_cr set id = crsummary.nextval;
//end if
//
//if sqlca.SQLCode <> 0 then
//	uf_msg("", "  ")
//	uf_msg("","ERROR: Auto Reverse: Update cr_cost_repository (2): " + sqlca.SQLErrText)
//	uf_msg("", "  ")
//	rollback;
//	DESTROY i_ds_elements
//	return -1
//end if
//
////  3.  INSERT into the cost repository ...
//insert into cr_cost_repository (select * from cr_temp_cr);
//
//if sqlca.SQLCode <> 0 then
//	uf_msg("", "  ")
//	uf_msg("","ERROR: Auto Reverse: Update cr_cost_repository (3): " + sqlca.SQLErrText)
//	uf_msg("", "  ")
//	rollback;
//	DESTROY i_ds_elements
//	return -1
//end if
//
////  4.  DELETE the cr_temp_cr table ...
//delete from cr_temp_cr;
//
//after_cr_update:

//  THIRD:  Update the reversed flag on cr_alloc_process_control_bdg ...
if g_debug = 'YES' then uf_msg("debug", &
	"Auto Reverse: Update the reversed flag on w_cr_alloc_process_control_bdg ...")
	
/*### - MDZ - 9058 - 20120119*/
/*Added trim to upper(test_or_actual) to be consistent.*/
// ### 7956: JAK: 2012-02-15:  Add budget version criteria
update cr_alloc_process_control_bdg
   set reversed = 1
 where allocation_id         = :allocation_id and
 		 month_number          = :run_month_number  and
		 month_period          = :run_month_period  and
		 upper(trim(test_or_actual)) = :i_test_or_actual and
		  budget_version = :i_budget_version;

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: updating reversed flag: " + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	
	/*### - MDZ - 9349 - 20120118*/
	/*The destroy is not needed.*/
//	DESTROY i_ds_elements
	return -1
end if


commit;



//  FOURTH:  Update the reversal_id to 0 for the original allocation records.

//  08/17/1999 - changed the delete statements below ... they all had
//               "select * from cr_budget_data" ...

if g_debug = 'YES' then uf_msg("debug", &
	"Auto Reverse: Update the reversal_id to 0 for the original allocation records ...")
if i_clearing_indicator = 4 or i_clearing_indicator = 5 then
	if i_test_or_actual = "ACTUAL" then
		update cr_budget_data set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 budget_version = :i_budget_version and
             reversal_id is null and 
             id in (select reversal_id from cr_budget_data
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									budget_version = :i_budget_version and
							      reversal_id is not null);
	else
		update cr_budget_data_test set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 budget_version = :i_budget_version and
             reversal_id is null and 
             id in (select reversal_id from cr_budget_data_test 
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									budget_version = :i_budget_version and
							      reversal_id is not null);
	end if	
else
	if i_test_or_actual = "ACTUAL" then
		update cr_budget_data set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 budget_version = :i_budget_version and
             reversal_id is null and 
             id in (select reversal_id from cr_budget_data
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									budget_version = :i_budget_version and
							      reversal_id is not null);
	else
		update cr_budget_data_test set reversal_id = 0
		 where allocation_id = :allocation_id   and
		       month_number = :run_month_number and 
	          month_period = :run_month_period and
				 budget_version = :i_budget_version and
             reversal_id is null and 
             id in (select reversal_id from cr_budget_data_test 
                     where allocation_id = :allocation_id   and 
									month_number = :month_number and 
						         month_period = :month_period and
									budget_version = :i_budget_version and
							      reversal_id is not null);
	end if
end if

if sqlca.SQLCode <> 0 then
	uf_msg("", "  ")
	uf_msg("","ERROR: Auto Reverse: updating reversal_id to 0: " + sqlca.SQLErrText)
	uf_msg("", "  ")
	rollback;
	
	/*### - MDZ - 9349 - 20120118*/
	/*The destroy is not needed.*/
//	DESTROY i_ds_elements
	return -1
end if


commit;


//
//  Must clean up after ourselves ...
//
delete from cr_budget_reversals;
commit;

/*### - MDZ - 9349 - 20120118*/
/*The datastore, i_ds_elements, is needed later in the process and cannot be destroyed here.*/
//DESTROY i_ds_elements

if g_debug = 'YES' then uf_msg("debug","Auto Reverse: Done with reversal.")

return 1

end function

public function decimal uf_sources_and_targets_xcc (uo_ds_top a_ds_source, longlong a_group_by_id, string a_descr, longlong a_month_number, longlong a_month_period, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, longlong a_cr_company_id, string a_company_field, boolean a_custom_dw, string a_charging_cost_center, string a_dept_field);//******************************************************************************************
//
//  Called from uf_loading2 ... Replaces the code in uf_loading1 from the 
//  SOURCES AND TARGETS section.
//
//******************************************************************************************
longlong num_rows, i, start_month, end_month, structure_id, not_field, where_source_id
string sqls, allocation_sqls, field, select_fields, left_paren, col_name, operator, &
		 value1, between_and, value2, right_paren, and_or, inter_co_sqls, report_sqls, &
		 external_company_id, structure_where, orig_value1, table_name, not_upper_value1, &
		 orig_value2, not_upper_value2, department_field
decimal {2} dept_amount
department_field = a_dept_field

//********************************
//
//  Sources and Targets:  
//
//********************************
uo_ds_top ds_group_by
ds_group_by = CREATE uo_ds_top
ds_group_by.DataObject = "dw_temp_dynamic"
ds_group_by.SetTransObject(sqlca)

sqls = "select cr_alloc_group_by_bdg.element_id, cr_elements.description " + &
		 "from cr_alloc_group_by_bdg , cr_elements " + &
		 "where group_by_id = " + string(a_group_by_id) + " and " + &
		 "cr_alloc_group_by_bdg.element_id = cr_elements.element_id"

if isnull(sqls) then
	uf_msg(a_descr, "The GROUP BY SQL is NULL ... No allocation will be calculated.")
	rollback using sqlca;
	return -1
end if

f_create_dynamic_ds(ds_group_by, "grid", sqls, sqlca, true)

num_rows = ds_group_by.RowCount()

allocation_sqls = "select "

for i = 1 to num_rows

	//  The following column reference was changed from "cr_elements_description" to 2, due
	//  to dynamic naming problems when using PB7 with Oracle 8 DB and OR 7 drivers at SO.
	field = upper(trim(ds_group_by.GetItemString(i, 2)))
	field = f_cr_clean_string(field)

	if i = 1 then
		select_fields = select_fields + '"' + field + '"'
	else
		select_fields = select_fields + ', "' + field + '"'
	end if
	
next

//  Open up Run by Dept: We only want to select the sum(amount) below.
select_fields = ""


//  Check for a detailed attribute allocation.
where_source_id = 0
select source_id into :where_source_id from cr_alloc_where_bdg
 where where_clause_id = :a_where_clause_id;
if isnull(where_source_id) then where_source_id = 0

if where_source_id = 0 then
	table_name = "CR_BUDGET_DATA"
else
	setnull(table_name)
	select trim(table_name) into :table_name from cr_sources 
	 where source_id = :where_source_id;
	if isnull(table_name) or table_name = "" then
		uf_msg(a_descr, "Could not find the table_name for where_source_id = " + &
			string(where_source_id) + "!  No allocation will be calculated.")
		rollback using sqlca;
		return -1
	end if
end if


//  Add the amount ... Type 3 "Clearing - Calc Rate" should not include mn & mp ... it will
//							  create an unneccessary amount of extra records ...
choose case i_clearing_indicator
	case 3
		if isnull(select_fields) or trim(select_fields) = "" then
			allocation_sqls = allocation_sqls + &
									" sum(amount) amount from " + table_name
		else
			allocation_sqls = allocation_sqls + &
									select_fields + &
									" sum(amount) amount from " + table_name
		end if
	case 5
		if i_run_as_a_test then
			if isnull(select_fields) or trim(select_fields) = "" then
				if i_whole_month then
					if i_ytd_indicator = 1 then
						//  Hardcode the MN or we will write records to all months !
						allocation_sqls = allocation_sqls + &
												" sum(amount) amount from cr_budget_data_test "
					else
						allocation_sqls = allocation_sqls + &
												" sum(amount) amount from cr_budget_data_test "
					end if
				else
					allocation_sqls = allocation_sqls + &
											" sum(amount) amount from cr_budget_data_test "
				end if
			else
				if i_whole_month then
					if i_ytd_indicator = 1 then
						//  Hardcode the MN or we will write records to all months !
						allocation_sqls = allocation_sqls + &
												select_fields + &
												" sum(amount) amount from cr_budget_data_test "
					else
						allocation_sqls = allocation_sqls + &
												select_fields + &
												" sum(amount) amount from cr_budget_data_test "
					end if
				else
					allocation_sqls = allocation_sqls + &
											select_fields + &
											" sum(amount) amount from cr_budget_data_test "
				end if
			end if
		else
			if isnull(select_fields) or trim(select_fields) = "" then
				if i_whole_month then
					if i_ytd_indicator = 1 then
						//  Hardcode the MN or we will write records to all months !
						allocation_sqls = allocation_sqls + &
												" sum(amount) amount from cr_budget_data "
					else
						allocation_sqls = allocation_sqls + &
												" sum(amount) amount from cr_budget_data "
					end if
				else
					allocation_sqls = allocation_sqls + &
											" sum(amount) amount from cr_budget_data "
				end if
			else
				if i_whole_month then
					if i_ytd_indicator = 1 then
						//  Hardcode the MN or we will write records to all months !
						allocation_sqls = allocation_sqls + &
												select_fields + &
												" sum(amount) amount from cr_budget_data "
					else
						allocation_sqls = allocation_sqls + &
												select_fields + &
												" sum(amount) amount from cr_budget_data "
					end if
				else
					allocation_sqls = allocation_sqls + &
											select_fields + &
											" sum(amount) amount from cr_budget_data "
				end if
			end if
		end if
	case else
		if i_whole_month then
			if i_ytd_indicator = 1 then
				//  Hardcode the MN or we will write records to all months !
				allocation_sqls = allocation_sqls + &
										select_fields + &
										" sum(amount) amount from " + table_name
			else
				allocation_sqls = allocation_sqls + &
										select_fields + &
										" sum(amount) amount from " + table_name
			end if
		else
			allocation_sqls = allocation_sqls + &
									select_fields + &
									" sum(amount) amount from " + table_name
		end if
end choose
						
//  Add the month_number and month_period args ... 
//  Type 3: The user enters the start_month and end_month 
//  for the source records (the records the factors will be calculated on).
//  Note that the Type 3 allocations will not have month_period in the where_clause,
//  only a month_number restriction.  Otherwise, the where clause gets too complicated.
//
//  Open up Run by Dept: This seems like a great place to add in the SQL for the
//  department_field.
if i_clearing_indicator <> 3 then
	if i_whole_month then
		if i_ytd_indicator = 1 then
			//  YTD:
			allocation_sqls = allocation_sqls + &
								  " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
								     'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
									 " and (month_number between " + left(string(a_month_number),4) + "01" + " and " + string(a_month_number) + ")"
			i_balance_where_clause = &
								  " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
								     'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
									 " and (month_number between " + left(string(a_month_number),4) + "01" + " and " + string(a_month_number) + ")"
		else
			allocation_sqls = allocation_sqls + &
								  " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
								     'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
									 " and (month_number = " + string(a_month_number) + ")"
			i_balance_where_clause = &
								  " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
								     'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
									 " and (month_number = " + string(a_month_number) + ")"
		end if
	else
		//  YTD does not make sense for month_period allocations.
		allocation_sqls = allocation_sqls + &
							  " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
							     'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
							    " and (month_number = " + string(a_month_number) + &
							    " and  month_period = " + string(a_month_period) + ")"
		i_balance_where_clause = &
							  " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
							     'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
							    " and (month_number = " + string(a_month_number) + &
							    " and  month_period = " + string(a_month_period) + ")"
	end if
else
	start_month = 0
	end_month   = 0
	
	select clearing_factor_start_month, clearing_factor_end_month 
	  into :start_month, :end_month
	  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;
	
	//  The user chose "Current" as the month for the rates...
	if start_month = 0 then start_month = i_month_number
	if   end_month = 0 then   end_month = i_month_number
	
	if start_month = 0 or isnull(start_month) or end_month = 0 or isnull(end_month) then
		uf_msg("Error", "Error - start_month, end_month = 0 ...")
		rollback using sqlca;
		return -1
	end if
	
	//  End_month should not go beyond the current month.
	if end_month > i_month_number then end_month = i_month_number
	
	//  If they marked this allocation as YTD, override any months in the pulldown.
	if i_ytd_indicator = 1 then
		start_month = long(left(string(i_month_number), 4) + "01")
		end_month   = i_month_number
	end if
	
	if start_month = end_month then
		//  Use and "=" instead of "between" to improve the explain plan.
		i_clearing_periods_wc = " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
											'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
		                          " and month_number = " + string(start_month) + " "
		i_balance_where_clause = " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
											'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
		                          " and month_number = " + string(start_month) + " "
	else
		i_clearing_periods_wc = " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
											'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
		                          " and (month_number between " + string(start_month) + &
	   	                       " and " + string(end_month) + ") "
		i_balance_where_clause = " where (amount_type = 1 " + f_cr_alloc_amount_type_custom(i_allocation_id) + ") " + &
											'and "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
		                          " and (month_number between " + string(start_month) + &
	   	                       " and " + string(end_month) + ") "
	end if

	allocation_sqls = allocation_sqls + i_clearing_periods_wc
end if

//  Where Clause:
uo_ds_top ds_where_clause
ds_where_clause = CREATE uo_ds_top
ds_where_clause.DataObject = "dw_temp_dynamic"
ds_where_clause.SetTransObject(sqlca)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(a_where_clause_id) + " order by row_id"

if isnull(sqls) then
	uf_msg(a_descr, "The WHERE CLAUSE SQL is NULL ... " + &
				  "No allocation will be calculated.")
	rollback using sqlca;
	return -1
end if

f_create_dynamic_ds(ds_where_clause, "grid", sqls, sqlca, true)

num_rows = ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		allocation_sqls = allocation_sqls + " and ( "
		i_balance_where_clause = i_balance_where_clause + " and ( "
	end if
	
	left_paren  = upper(trim(ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(ds_where_clause.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(ds_where_clause.GetItemString(i, "value1"))
	orig_value1 = upper(ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if (i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{") or &
		    col_name = "GL JOURNAL CATEGORY" &
		then
			value1 = not_upper_value1
		else
			value1 = upper(trim(ds_where_clause.GetItemString(i, "value1")))
		end if
	end if
	
	between_and = upper(trim(ds_where_clause.GetItemString(i, "between_and")))
	
	not_upper_value2 = trim(ds_where_clause.GetItemString(i, "value2"))
	orig_value2 = upper(ds_where_clause.GetItemString(i, "value2"))
	
	if orig_value2 = " " then
		//  Single-space is OK.
		value2 = orig_value2
	else
		if (i_use_new_structures_table = "YES" and left(not_upper_value2, 1) = "{") or &
		    col_name = "GL JOURNAL CATEGORY" &
		then
			value2 = not_upper_value2
		else
			value2 = upper(trim(ds_where_clause.GetItemString(i, "value2")))
		end if
	end if
	
	right_paren = upper(trim(ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(ds_where_clause.GetItemString(i, "and_or")))
	structure_id = ds_where_clause.GetItemNumber(i, "structure_id")
		
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0

	//  If they manually entered an Oracle function, take it as-is.
	if pos(col_name, "(") = 0 then
		col_name = f_cr_clean_string(col_name)
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = &
			uf_structure_value(structure_id, col_name, value1, not_field)
		if structure_where = "ERROR" then
			rollback using sqlca;
			return -1
		end if
//		allocation_sqls = allocation_sqls + structure_where + " and "  //  BAD !!!
		allocation_sqls = &
			allocation_sqls + left_paren + structure_where + right_paren + " " + and_or + " "
		i_balance_where_clause = &
			i_balance_where_clause + left_paren + structure_where + right_paren + " " + and_or + " "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				allocation_sqls = allocation_sqls + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
				i_balance_where_clause = i_balance_where_clause + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
			else
				allocation_sqls = allocation_sqls + &
										left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
				i_balance_where_clause = i_balance_where_clause + &
										left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
										value1 + " " + &
										right_paren + " " + and_or + " "
			end if
		else
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				allocation_sqls = allocation_sqls + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
				i_balance_where_clause = i_balance_where_clause + &
										left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
			else
				allocation_sqls = allocation_sqls + &
										left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
				i_balance_where_clause = i_balance_where_clause + &
										left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
										value1 + "' " + &
										right_paren + " " + and_or + " "
			end if
		end if
	else
		if pos(col_name, "(") <> 0 then
			//  NO DOUBLE QUOTES ON COL_NAME!
			allocation_sqls = allocation_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
			i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
		else
			allocation_sqls = allocation_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
			i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + between_and + " '" + value2 + "' " + &
									right_paren + " " + and_or	+ " "
		end if
	end if
	
	bottom:
	
	if i = num_rows then
		allocation_sqls = allocation_sqls + " )"
		i_balance_where_clause = i_balance_where_clause + " )"
	end if
	
next


//  Type 4:  Inter-Company (Trigger) ... must add the "Inter-Company criteria" to the
//           where clause ... this is defined in cr_system_control ... the SQL must begin
//           like "select id from cr_cost_repository where ...", returning the id's that
//           pass the inter-company criteria ... 
if i_clearing_indicator = 4 then
	
	inter_co_sqls = ""
	select control_value into :inter_co_sqls
	  from cr_system_control where upper(control_name) = 'BUDGET INTER-COMPANY SQL';

	if isnull(inter_co_sqls) or trim(inter_co_sqls) = "" then
		uf_msg("Inter-Company (Trigger)", &
		           "No budget inter-company criteria was found in cr_system_control ... " + &
					  "No allocation will be calculated.")
		rollback using sqlca;
		return -1
	end if
	
	allocation_sqls = allocation_sqls + " and id in (" + inter_co_sqls + ") "
	i_balance_where_clause = i_balance_where_clause + " and id in (" + inter_co_sqls + ") "

	//  We will also need this for the credit code ...
	i_interco_trigger_col = ""
	select control_value into :i_interco_trigger_col
	  from cr_system_control 
	 where upper(control_name) = 'INTER-COMPANY TRIGGER FIELD';
		  
	if isnull(i_interco_trigger_col) or trim(i_interco_trigger_col) = "" then
		uf_msg("Inter-Company (Trigger)", &
		           "No INTER-COMPANY TRIGGER FIELD was found in cr_system_control ... " + &
					  "No allocation will be calculated.")
		rollback using sqlca;
		return -1
	end if	
	
	i_interco_trigger_col = f_cr_clean_string(i_interco_trigger_col)

	//  We will need this too ...
	i_original_interco_credit_sql = ""
	select control_value into :i_original_interco_credit_sql
	  from cr_system_control 
	 where upper(control_name) = 'INTER-COMPANY CREDIT COMPANY SQL';
		
	if isnull(i_original_interco_credit_sql) or trim(i_original_interco_credit_sql) = "" then
		uf_msg("Inter-Company (Trigger)", &
		           "No INTER-COMPANY CREDIT COMPANY SQL was found in " + &
					  "cr_system_control ... No allocation will be calculated.")
		rollback using sqlca;
		return -1
	end if		
	
end if


//  Type 6:  Capital Loading ... The user can set up an allocation that will use the 
//           parameters that are set up in the clearing_wo_control table.
if i_clearing_indicator = 6 or i_apply_wo_types = 1 then
	
	//  Sets i_wo_sqls = to a big select statement ...
	uf_capital_work_orders(a_clearing_id)
	
	//  OLD TECHNIQUE BEFORE THE CR_WO_CLEAR TABLE:
	//	allocation_sqls = allocation_sqls + ' and "' + a_work_order_field + '" in (' + &
	//	                  i_wo_sqls + ")"
	
	i_wo_field_for_uf_cap = a_work_order_field
	
	f_cr_allocations_custom_bdg("before_uf_capital_work_orders_sqls")
	
	//  NEW TECHNIQUE USING THE NEW CR_WO_CLEAR TABLE:
	/*### - MDZ - 29485 - 20130307*/
	/*Changing the below to use cr_wo_clear_bdg instead of cr_wo_clear*/
	allocation_sqls = allocation_sqls + ' and "' + i_wo_field_for_uf_cap + '" in (' + &
		" select work_order_number from cr_wo_clear_bdg " + &
		 " where allocation_id = " + string(i_allocation_id) + ") "
	i_balance_where_clause = i_balance_where_clause + ' and "' + i_wo_field_for_uf_cap + '" in (' + &
		" select work_order_number from cr_wo_clear_bdg " + &
		 " where allocation_id = " + string(i_allocation_id) + ") "
	
	f_cr_allocations_custom_bdg("after_uf_capital_work_orders_sqls")
	
end if


//  CR_COMPANY_ID:  ADD the company to the where clause if the user picked one ...
report_sqls = allocation_sqls
if not isnull(a_cr_company_id) and a_cr_company_id <> 0 then
	select external_company_id into :external_company_id
  	  from cr_company where cr_company_id = :a_cr_company_id;
	allocation_sqls = allocation_sqls + " and ( " + a_company_field + " = '" + &
	                  upper(trim(external_company_id)) + "' ) "
	i_balance_where_clause = i_balance_where_clause + " and ( " + a_company_field + " = '" + &
	                  upper(trim(external_company_id)) + "' ) "
	if i_cr_company_id <> 10000 then 
		report_sqls = allocation_sqls
	end if
end if


//  GROUP BY ... Type 3 "Clearing - Calc Rate" should not include mn & mp ... it will
//					  create an unneccessary amount of extra records ...
choose case i_clearing_indicator
	case 3
		if isnull(select_fields) or trim(select_fields) = "" then
			//  Nothing to group by ...
		else
			//  Open up Run by Dept: Commented out the group by since we want a scalar amount.
			//allocation_sqls = allocation_sqls + " group by " + select_fields
			//report_sqls     = report_sqls     + " group by " + select_fields
		end if
	case else
		//  Open up Run by Dept: Commented out the group by since we want a scalar amount.
		if i_whole_month then
			if i_ytd_indicator = 1 then
				if isnull(trim(select_fields)) or trim(select_fields) = "" then
					//allocation_sqls = allocation_sqls + " group by " + string(a_month_number) + &
					//						" "
					//report_sqls     = report_sqls     + " group by " + &
					//						" "
				else
					//allocation_sqls = allocation_sqls + " group by " + string(a_month_number) + &
					//						", " + select_fields + &
					//						" "
					//report_sqls     = report_sqls     + " group by " + string(a_month_number) + &
					//						", " + select_fields + &
					//						" "
				end if
			else
				//allocation_sqls = allocation_sqls + " group by " + select_fields + &
				//						", month_number"
				//report_sqls     = report_sqls     + " group by " + select_fields + &
				//						", month_number"
			end if
		else
			//allocation_sqls = allocation_sqls + " group by " + select_fields + &
			//						", month_number, month_period"
			//report_sqls     = report_sqls     + " group by " + select_fields + &
			//						", month_number, month_period"
		end if
end choose

//  If the user did not pick any "group by" fields, the SQL will look like 
//  (SELECT , month_number ... GROUP BY , month_number ...).  Use f_replace_string
//  to correct this.
allocation_sqls = f_replace_string(allocation_sqls, "select ,",   "select ",   "first")
allocation_sqls = f_replace_string(allocation_sqls, "group by ,", "group by ", "first")
report_sqls     = f_replace_string(report_sqls,     "select ,",   "select ",   "first")
report_sqls     = f_replace_string(report_sqls,     "group by ,", "group by ", "first")

if isnull(allocation_sqls) then
	uf_msg(a_descr, "The SOURCE SQL is NULL ... Company = " + external_company_id + &
					  ". No allocation will be calculated.")
	rollback using sqlca;
	return -1
end if

if a_custom_dw then
	a_ds_source.RETRIEVE(i_month_number, i_month_period, i_allocation_id)
	
	//  02/23/2005:  Added error checking (complex SQL was blowing the temp space).
	if a_ds_source.i_sqlca_sqlcode < 0 then
		uf_msg(a_descr, "ERROR in the retrieve of the custom DW: " + &
			a_ds_source.i_sqlca_sqlerrtext)
		rollback using sqlca;
		return -1
	end if
	
	a_ds_source.GroupCalc()
else
	if g_debug = 'YES' then
		uf_msg("debug", "The source SQL (allocation_sqls) = " + allocation_sqls)
	end if
	execute immediate :allocation_sqls using sqlca;
	if sqlca.SQLCode < 0 then
		uf_msg(a_descr, "The SOURCE SQL syntax is invalid ... " + &
					  "No allocation will be calculated.")
		uf_msg(a_descr, "BAD SYNTAX = " + allocation_sqls)
		rollback using sqlca;
		return -1
	end if
	f_create_dynamic_ds(a_ds_source, "grid", allocation_sqls, sqlca, true)
end if
//  ds_source now contains the source records which the rate must be applied to ...


//  Open up Run by Dept: Get the amount to return to the caller.
if a_ds_source.RowCount() <> 1 then
	uf_msg(a_descr, "uf_sources_and_targets_xcc: Could not determine the departmental amount ... " + &
					  "No allocation will be calculated.")
	uf_msg(a_descr, "SYNTAX = " + allocation_sqls)
	rollback using sqlca;
	return -1
else
	dept_amount = a_ds_source.GetItemNumber(1, 1)
end if


//  Open up Run by Dept: Commented out.  Seems like the orginal source SQL should
//  be the one that's saved in the table.
////  Save the source SQL for reporting ...
//i_source_sqls = uf_save_report_sql(report_sqls, a_custom_dw)

return dept_amount

end function

public function longlong uf_run_by_department (longlong a_alloc_id, longlong a_mn, longlong a_mp, longlong a_clear_ind, longlong a_cr_co_id, string a_co_field, string a_gl_jcat, uo_ds_top a_ds_source, longlong a_group_by_id, string a_descr, longlong a_where_clause_id, longlong a_clearing_id, string a_work_order_field, boolean a_custom_dw);//******************************************************************************************
//
//  User Object Function  :  uf_run_by_department
//
//  Notes                 :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!
//
//  ***  DEPARTMENT OR COST CENTER BASED CLEARINGS (CHARGING COST CENTER AT EL PASO):  ***
//
//  General Comment       :  This code contains terms and variables like "charging cost 
//                           center".  That is because the code originated from El Paso's
//                           extension functions.  I am leaving these terms as-is, so we
//                           can easily compare to the original El Paso code if necessary.
//                           Don't be fooled by the terms ... genreally speaking, any ACK
//                           field can be used in this function.
//
//  EL PASO COMMENT:
//  ----------------
//    Certain allocations like unproductive time must have cost center based pots cleared
//    to records within the same cost center.  We will accomplish that here since the 
//    regular clearing-calc-rate allocations simply clear a single pot (we don't want to
//    set up 1 allocation per cost center!).  The methodology is:
//
//    1)  Run the traditional clearing-calc-rate allocation where the pot is the sum of
//        all the cost centers.  This will horribly mis-clear the dollars, but at least
//        it will clear SOMETHING to every possible distribution that can be used as
//        a basis for the "source criteria" in #3 below.
//    2)  Select the list of distinct cost centers from the results of #1.
//    3)  Loop over the list of cost centers and re-clear each cost center's pot:
//        -- Re-determine the pot based on the Balance Criteria for the allocation along
//           with " and cost_center = :cc ".
//        -- Perform the clearing using the formula:
//
//                              original cleared amount 
//              XCC Pot *  --------------------------------
//                         total cleared amount for the XCC
//
//    4)  Re-book the Credits by deleting the original credit and inserting 1 record per
//        cost center ... Bill says there will only be a single ACK combination in the
//        unproductive time pots.
//
//  Notes:
//  ------
//  *) We will write code against a clearing-calc-rate allocation only.  Any other type of
//     allocation would not make sense.  This means we can code against 4 tables instead
//     of 8.
//
//  *) By definition (charging_cost_center remains constant) there can be no intercompany
//     effect ... thus no code for intercompany.
//
//  *) The CREDIT code will be forced to book only a single credit record (even if there
//     is more than one balance ACK in the pot) ... per Bill's instructions --- he said 
//     they make sure all unproductive time goes to a single distribution per XCC.
//
//  *) The nature of this code is such that any clearing-calc-rate allocation that needs
//     to keep dollars constant by XCC can be sent through this code by adding the allocation
//     to the cr_epe_cc_based_allocations table through table maintenance.
//
//  *) This code needs to be BEFORE the company/bus seg derivations below.
//
//  *)  El Paso allocated based on charging cost centers, not target cost centers.
//      Atmos   allocated based on ???
//
//******************************************************************************************
longlong allocation_id, month_number, month_period, counter, clearing_indicator, &
        i, num_distinct_x_cc_values, cr_company_id, e, &
		  dr_cr_id_for_credit
// SEK 082410: Changed id from longlong to longlong due to volume of
//		data at Southern. 		  
longlong id		  
string  sqls, company_field, charging_cost_center, element_descr, gl_journal_category, &
        table_name
boolean updating, zero_amount, zero_qty, no_targets
dec {2} amount, xcc_pot, total_cleared_amount_for_the_xcc, &
		  new_total_cleared_amount_for_the_xcc, amount_for_credit, total_cleared_qty_for_the_xcc, &
		  xcc_pot_qty, new_total_cleared_qty_for_the_xcc, qty_for_credit


//
//  This evolved from a client's extension function. Thus I am leaving the general
//  strucutre the same and setting local variables.
//
allocation_id       = a_alloc_id
month_number        = a_mn
month_period        = a_mp
clearing_indicator  = a_clear_ind
cr_company_id       = a_cr_co_id
company_field       = a_co_field
gl_journal_category = a_gl_jcat


//
//  This code only makes sense against a clearing-calc-rate allocation.
//
//  Open up Run by Dept: Commented this out to allow run_by_dept allocations
//  to run for loadings and clearing-input rate allocations.  Also commented
//  out the goto label below.
//
//if clearing_indicator <> 3 then
//	f_pp_msgs("  ")
//	f_pp_msgs('WARNING: This allocation is attempting to "run by deparment"!')
//	f_pp_msgs('-------- It cannot since it is not defined as a "Claring-Calc Rate" allocation!')
//	f_pp_msgs("  ")
//	goto after_cc_based_allocations
//end if


//  NOW PERFORMED IN UF_LOADING2 TO AN I VAR.
////
////  Look up the department field.
////
//setnull(department_field)
//select upper(trim(control_value)) into :department_field
//  from cr_system_control 
// where upper(trim(control_name)) = 'DEPT-BASED ALLOCATIONS: DEPT FIELD';
//
//if isnull(department_field) or trim(department_field) = "" then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: No DEPT-BASED ALLOCATIONS: DEPT FIELD was found in cr_system_control ... " + &
//				  "The department-based allocation cannot be calculated.")
//	f_pp_msgs("  ")
//	rollback using sqlca;
//	return -1
//end if


//  NOW PERFORMED IN UF_LOADING2 TO AN I VAR.
////
////  Get rid of any ' ', '-', or '/' ...
////
//department_field = f_cr_clean_string(trim(department_field))


//  NOW PERFORMED IN UF_LOADING2 TO AN I VAR.
////
////  Is the department field really in the ACK?
////
//counter = 0
//select count(*) into :counter from cr_elements
// where upper(trim(description)) = upper(trim(replace(:department_field, '_', ' ')));
//if isnull(counter) then counter = 0
//
//if counter <= 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("ERROR: " + department_field + " is not in the accounting key ... " + &
//				  "The department-based allocation cannot be calculated.")
//	f_pp_msgs("  ")
//	rollback using sqlca;
//	return -1
//end if


//
//  This datastore will hold the list of the distinct departments for which an allocation
//  needs to be calculated.
//
uo_ds_top ds_distinct_x_cc_values
ds_distinct_x_cc_values = CREATE uo_ds_top


//
//  Get the distinct list of department values.
//
if upper(i_test_or_actual) = "TEST" then
	
	if i_me_validations = "YES" or i_combo_validations = "YES" or &
		i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
		i_custom_validations = "YES" &
	then
		sqls = 'select distinct "' + i_department_field + '" ' + &
					"from cr_budget_data_test " + &
				  "where allocation_id = " + string(allocation_id) + " " + &
					 "and month_number  = " + string(month_number) + " " + &
					 "and month_period  = " + string(month_period) + " " + &
					 "and target_credit = 'TARGET' "
	else
		sqls = 'select distinct "' + i_department_field + '" ' + &
					"from cr_budget_data_test " + &
				  "where allocation_id = " + string(allocation_id) + " " + &
					 "and month_number  = " + string(month_number) + " " + &
					 "and month_period  = " + string(month_period) + " " + &
					 "and target_credit = 'TARGET' "
	end if
	
else
	
	if i_me_validations = "YES" or i_combo_validations = "YES" or &
		i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
		i_custom_validations = "YES" &
	then
		sqls = 'select distinct "' + i_department_field + '" ' + &
					"from cr_budget_data " + &
				  "where allocation_id = " + string(allocation_id) + " " + &
					 "and month_number  = " + string(month_number) + " " + &
					 "and month_period  = " + string(month_period) + " " + &
					 "and target_credit = 'TARGET' "
	else
		sqls = 'select distinct "' + i_department_field + '" ' + &
					"from cr_budget_data " + &
				  "where allocation_id = " + string(allocation_id) + " " + &
					 "and month_number  = " + string(month_number) + " " + &
					 "and month_period  = " + string(month_period) + " " + &
					 "and target_credit = 'TARGET' "
	end if
	
end if

f_create_dynamic_ds(ds_distinct_x_cc_values, "grid", sqls, sqlca, true)

num_distinct_x_cc_values = ds_distinct_x_cc_values.RowCount()

//
//  Loop over each x-cc and re-clear.
//
for i = 1 to num_distinct_x_cc_values
		
	no_targets  = false
	zero_amount = false
	zero_qty    = false


	//
	//  Get the charging_cost_center.
	//
	charging_cost_center = ds_distinct_x_cc_values.GetItemString(i, 1)
	
	
	//
	//  Get this charging_cost_center's pot to be re-spread ("XCC Pot" from above).
	//  This function also saves the balance_where_clause in an instance variable
	//  that will be used to generate the single credit per XCC.
	//
	i_where_source_id = 0
	
	if clearing_indicator = 3 then
		xcc_pot = uf_clearing_balance_xcc( &
			cr_company_id, company_field, allocation_id, month_number, charging_cost_center, &
				i_department_field)
		// ### 8142: JAK: 2011-11-01:  Consistent return codes
		if i_errors then return xcc_pot
		if i_allocate_quantity = 1 then
			xcc_pot_qty = uf_clearing_balance_xcc_qty( &
				cr_company_id, company_field, allocation_id, month_number, charging_cost_center, &
					i_department_field)
			// ### 8142: JAK: 2011-11-01:  Consistent return codes
			if i_errors then return xcc_pot_qty
		end if
	else
		xcc_pot = uf_sources_and_targets_xcc(a_ds_source, a_group_by_id, a_descr, month_number, &
			month_period, a_where_clause_id, a_clearing_id, a_work_order_field, cr_company_id, &
				company_field, a_custom_dw, charging_cost_center, i_department_field)
		//  DMJ:6/18/08: DECIDED THIS DIDN'T MAKE SENSE FOR ALLOCATING QUANTITIES ... WHAT DOES
		//  A LOADING AGAINST A QUANTITY MEAN FOR RUN BY DEPARTMENT ?
		xcc_pot_qty = 0
	end if
	
	if g_debug = 'YES' then uf_msg("debug", "uf_run_by_department: " + charging_cost_center + ": xcc_pot = " + string(xcc_pot))
	if g_debug = 'YES' then uf_msg("debug", "uf_run_by_department: " + charging_cost_center + ": xcc_pot_qty = " + string(xcc_pot_qty))
		
	
	//
	//  If the XCC has a NULL or 0 balance returned from uf_clearing_balance_xcc then
	//  we have incorrectly allocated to this XCC in the original clearing-calc-rate
	//  allocation ... delete those allocation results and go to the next XCC.
	//
	if isnull(xcc_pot) or xcc_pot = 0 then
		
		if i_allocate_quantity = 1 then
			if isnull(xcc_pot_qty) or xcc_pot_qty = 0 then
				//  Allow the delete to fire since both are 0.
			else
				goto after_delete
			end if
		end if
		
		no_targets = true
		
		if upper(i_test_or_actual) = "TEST" then
			if i_me_validations = "YES" or i_combo_validations = "YES" or &
				i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
				i_custom_validations = "YES" &
			then
				sqls = &
					"delete from cr_budget_data_test " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			else
				sqls = &
					"delete from cr_budget_data_test " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
		else
			if i_me_validations = "YES" or i_combo_validations = "YES" or &
				i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
				i_custom_validations = "YES" &
			then
				sqls = &
					"delete from cr_budget_data " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			else
				sqls = &
					"delete from cr_budget_data " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
		end if
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: deleting the 0-balance results: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("charging_cost_center = " + charging_cost_center)
			f_pp_msgs("  ")
			return -1
		end if
						
	end if
	
	after_delete:
	
	
	//
	//  Get the "total cleared amount for the XCC" (the denominator from above).
	//
	total_cleared_amount_for_the_xcc = 0
	total_cleared_qty_for_the_xcc    = 0
	
	
	if upper(i_test_or_actual) = "TEST" then
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			if i_allocate_quantity = 1 then
				sqls = &
					"select sum(amount), sum(quantity) from cr_budget_data_test " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			else
				sqls = &
					"select sum(amount) from cr_budget_data_test " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
		else
			if i_allocate_quantity = 1 then
				sqls = &
					"select sum(amount), sum(quantity) from cr_budget_data_test " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			else
				sqls = &
					"select sum(amount) from cr_budget_data_test " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
		end if
	else
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			if i_allocate_quantity = 1 then
				sqls = &
					"select sum(amount), sum(quantity) from cr_budget_data " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			else
				sqls = &
					"select sum(amount) from cr_budget_data " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
		else
			if i_allocate_quantity = 1 then
				sqls = &
					"select sum(amount), sum(quantity) from cr_budget_data " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			else
				sqls = &
					"select sum(amount) from cr_budget_data " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period)  + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
		end if
	end if
	
	if i_allocate_quantity = 1 then
		i_ds_sum_amount2.SetSQLSelect(sqls)
		i_ds_sum_amount2.RETRIEVE()
		if i_ds_sum_amount2.RowCount() <> 0 then
			total_cleared_amount_for_the_xcc = i_ds_sum_amount2.GetItemNumber(1, 1)
			total_cleared_qty_for_the_xcc    = i_ds_sum_amount2.GetItemNumber(1, 2)
		end if
	else
		i_ds_sum_amount.SetSQLSelect(sqls)
		i_ds_sum_amount.RETRIEVE()
		if i_ds_sum_amount.RowCount() <> 0 then
			total_cleared_amount_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
		end if
	end if
	
	
	if isnull(total_cleared_amount_for_the_xcc) then total_cleared_amount_for_the_xcc = 0
	if isnull(total_cleared_qty_for_the_xcc)    then total_cleared_qty_for_the_xcc    = 0
	
	
	//  There could be cases where a cost center with very few people (like 1)
	//  charged all their time to vacation and such.  With no straight or overtime
	//  payroll, the original allocation --- and thus this re-spread --- cannot
	//  allocate anything.  Check for that condition here and continue so we do
	//  not get a div-by-zero error.
	//if total_cleared_amount_for_the_xcc = 0 then continue -- 6/18/08: CHANGED FOR ALLOCATE QTYS
	if total_cleared_amount_for_the_xcc = 0 then zero_amount = true
	if total_cleared_qty_for_the_xcc    = 0 then zero_qty    = true
	
	
	//
	//  RE-SPREAD (TARGETS).  AMOUNT
	//
	if zero_amount then goto re_spread_qty
	if upper(i_test_or_actual) = "TEST" then
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"update cr_budget_data_test " + &
					"set amount = " + string(xcc_pot) + &
								 " * (amount / " + string(total_cleared_amount_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		else
			sqls = &
				"update cr_budget_data_test " + &
					"set amount = " + string(xcc_pot) + &
								 " * (amount / " + string(total_cleared_amount_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		end if
	else
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"update cr_budget_data " + &
					"set amount = " + string(xcc_pot) + &
								 " * (amount / " + string(total_cleared_amount_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		else
			sqls = &
				"update cr_budget_data " + &
					"set amount = " + string(xcc_pot) + &
								 " * (amount / " + string(total_cleared_amount_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		end if
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating with the RE-SPREAD amount: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs("charging_cost_center = " + charging_cost_center)
		f_pp_msgs("  ")
		return -1
	end if
	
	
	re_spread_qty:
	
	
	//
	//  RE-SPREAD (TARGETS).  QUANTITY
	//
	if i_allocate_quantity = 1 and not zero_qty then
		//  Continue on.
	else
		goto after_re_spread_qty
	end if
	if upper(i_test_or_actual) = "TEST" then
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"update cr_budget_data_test " + &
					"set quantity = " + string(xcc_pot_qty) + &
								 " * (quantity / " + string(total_cleared_qty_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		else
			sqls = &
				"update cr_budget_data_test " + &
					"set quantity = " + string(xcc_pot_qty) + &
								 " * (quantity / " + string(total_cleared_qty_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		end if
	else
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"update cr_budget_data " + &
					"set quantity = " + string(xcc_pot_qty) + &
								 " * (quantity / " + string(total_cleared_qty_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		else
			sqls = &
				"update cr_budget_data " + &
					"set quantity = " + string(xcc_pot_qty) + &
								 " * (quantity / " + string(total_cleared_qty_for_the_xcc) + ") " + &
				 " where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
		end if
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: updating with the RE-SPREAD amount: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs("charging_cost_center = " + charging_cost_center)
		f_pp_msgs("  ")
		return -1
	end if
	
	after_re_spread_qty:
	
	
	//
	//  Fix any rounding error from the re-spread:
	//    XCC Pot - new_total_cleared_amount_for_the_xcc plugged to the record in the
	//    allocations table with the max(id) --- i.e. the last record.
	//
	if zero_amount then goto rounding_error_qty
	
	updating = false
	
	if upper(i_test_or_actual) = "TEST" then
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"select sum(amount) from cr_budget_data_test " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
			
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_amount_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if xcc_pot - new_total_cleared_amount_for_the_xcc <> 0 then
				updating = true
				sqls = &
					"update cr_budget_data_test " + &
						"set amount = amount " + &
									  "+ (" + string(xcc_pot) + &
									 " - " + string(new_total_cleared_amount_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data_test " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		else
			sqls = &
				"select sum(amount) from cr_budget_data_test " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
			
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_amount_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if xcc_pot - new_total_cleared_amount_for_the_xcc <> 0 then
				updating = true
				sqls = &
				"update cr_budget_data_test " + &
						"set amount = amount " + &
									  "+ (" + string(xcc_pot) + &
									 " - " + string(new_total_cleared_amount_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data_test " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		end if
	else
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"select sum(amount) from cr_budget_data " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
			
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_amount_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if g_debug = 'YES' then
				uf_msg("debug", "uf_run_by_department: " + charging_cost_center + ": new_total_cleared_amount_for_the_xcc = " + string(new_total_cleared_amount_for_the_xcc))
			end if
			
			if xcc_pot - new_total_cleared_amount_for_the_xcc <> 0 then
				updating = true
				sqls = &
					"update cr_budget_data " + &
						"set amount = amount " + &
									  "+ (" + string(xcc_pot) + &
									 " - " + string(new_total_cleared_amount_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		else
			sqls = &
				"select sum(amount) from cr_budget_data " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
					"and budget_version = '" + i_budget_version + "' "
			
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_amount_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if g_debug = 'YES' then
				uf_msg("debug", "uf_run_by_department: " + charging_cost_center + ": new_total_cleared_amount_for_the_xcc = " + string(new_total_cleared_amount_for_the_xcc))
			end if
			
			if xcc_pot - new_total_cleared_amount_for_the_xcc <> 0 then
				updating = true
				sqls = &
					"update cr_budget_data " + &
						"set amount = amount " + &
									  "+ (" + string(xcc_pot) + &
									 " - " + string(new_total_cleared_amount_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		end if
	end if  //  if upper(i_test_or_actual) = "TEST" then ...
	
	execute immediate :sqls;
	
	if updating then
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating with the ROUNDING amount: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("charging_cost_center = " + charging_cost_center)
			f_pp_msgs("  ")
			return -1
		end if
	end if
	
	
	rounding_error_qty:
	
	
	//
	//  Fix any rounding error from the re-spread: QUANTITY
	//    XCC Pot - new_total_cleared_amount_for_the_xcc plugged to the record in the
	//    allocations table with the max(id) --- i.e. the last record.
	//
	if i_allocate_quantity = 1 and not zero_qty then
		//  Continue on.
	else
		goto after_rounding_error_qty
	end if
	
	updating = false
	
	if upper(i_test_or_actual) = "TEST" then
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"select sum(quantity) from cr_budget_data_test " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
			
			//  Just re-use the amount DS since it has a single column.
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_qty_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if xcc_pot_qty - new_total_cleared_qty_for_the_xcc <> 0 then
				updating = true
				sqls = &
					"update cr_budget_data_test " + &
						"set quantity = quantity " + &
									  "+ (" + string(xcc_pot_qty) + &
									 " - " + string(new_total_cleared_qty_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data_test " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		else
			sqls = &
				"select sum(quantity) from cr_budget_data_test " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
			
			//  Just re-use the amount DS since it has a single column.
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_qty_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if xcc_pot_qty - new_total_cleared_qty_for_the_xcc <> 0 then
				updating = true
				sqls = &
				"update cr_budget_data_test " + &
						"set quantity = quantity " + &
									  "+ (" + string(xcc_pot_qty) + &
									 " - " + string(new_total_cleared_qty_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data_test " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		end if
	else
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"select sum(quantity) from cr_budget_data " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
			
			//  Just re-use the amount DS since it has a single column.
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_qty_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if xcc_pot_qty - new_total_cleared_qty_for_the_xcc <> 0 then
				updating = true
				sqls = &
					"update cr_budget_data " + &
						"set quantity = quantity " + &
									  "+ (" + string(xcc_pot_qty) + &
									 " - " + string(new_total_cleared_qty_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		else
			sqls = &
				"select sum(quantity) from cr_budget_data " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'TARGET' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' "
			
			//  Just re-use the amount DS since it has a single column.
			i_ds_sum_amount.SetSQLSelect(sqls)
			i_ds_sum_amount.RETRIEVE()
			if i_ds_sum_amount.RowCount() <> 0 then
				new_total_cleared_qty_for_the_xcc = i_ds_sum_amount.GetItemNumber(1, 1)
			end if
			
			if xcc_pot_qty - new_total_cleared_qty_for_the_xcc <> 0 then
				updating = true
				sqls = &
					"update cr_budget_data " + &
						"set quantity = quantity " + &
									  "+ (" + string(xcc_pot_qty) + &
									 " - " + string(new_total_cleared_qty_for_the_xcc) + ") " + &
					 "where allocation_id = " + string(allocation_id) + " " + &
						"and month_number  = " + string(month_number) + " " + &
						"and month_period  = " + string(month_period) + " " + &
						"and target_credit = 'TARGET' " + &
						'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and id = ( " + &
									"select max(id) from cr_budget_data " + &
									 "where allocation_id = " + string(allocation_id) + " " + &
										"and month_number  = " + string(month_number) + " " + &
										"and month_period  = " + string(month_period) + " " + &
										"and target_credit = 'TARGET' " + &
										'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
										"and budget_version = '" + i_budget_version + "' " + &
									") " + &
						"and budget_version = '" + i_budget_version + "' "
			end if
			
		end if
	end if  //  if upper(i_test_or_actual) = "TEST" then ...
	
	execute immediate :sqls;
	
	if updating then
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating with the ROUNDING amount: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			f_pp_msgs("charging_cost_center = " + charging_cost_center)
			f_pp_msgs("  ")
			return -1
		end if
	end if
	
	after_rounding_error_qty:	
	
	
	//  If they have the system switch turned on to obey the Credit Criteria, we don't want to book
	//  the credits here.
	if i_run_by_dept_credits_method = "YES" then goto after_credits
	
	
	//
	//  CREDITS:
	//
	
	//  Delete the original credits.
	if upper(i_test_or_actual) = "TEST" then
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"delete from cr_budget_data_test " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'CREDIT' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
		else
			sqls = &
				"delete from cr_budget_data_test " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'CREDIT' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
		end if
	else
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = &
				"delete from cr_budget_data " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'CREDIT' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
		else
			sqls = &
				"delete from cr_budget_data " + &
				 "where allocation_id = " + string(allocation_id) + " " + &
					"and month_number  = " + string(month_number) + " " + &
					"and month_period  = " + string(month_period) + " " + &
					"and target_credit = 'CREDIT' " + &
					'and "' + i_department_field + '" = ' + "'" + string(charging_cost_center) + "' " + &
						"and budget_version = '" + i_budget_version + "' "
		end if
	end if
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: deleting the original credits: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs("charging_cost_center = " + charging_cost_center)
		f_pp_msgs("  ")
		return -1
	end if
	
	
	
	//
	//  Not Used:
	//  ---------
	//  Get some values from the targets.  By definition the gl_journal_category
	//  will be distinct.  The EPE practice will be to make gl_source distinct
	//  given its use in the gl.  Use a "min" just to be sure and so there will some
	//  rhyme or reason to the select.
	//
	//  Base Note: At EPE we were also selecting the min(gl_source) from the results.  This
	//             was an ACK field that was held constant across an allocation (as we do
	//             with gl_journal_category).  This base function would not be able to do that.
	//             EPE would need extension code if they ever chose to use this new 
	//             functionality.
	//
	//  Base Note: At EPE we were also selecting the min(gl_journal_category) from the results.
	//             Now, I have made this an argument into the function so we don't have to run
	//             the select.
	//
	
	
////////	if upper(i_test_or_actual) = "TEST" then
////////		if i_me_validations = "YES" or i_combo_validations = "YES" or &
////////		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
////////			i_custom_validations = "YES" &
////////		then
////////			select min(gl_journal_category), min(gl_source)
////////			  into :gl_journal_category, :gl_source
////////			  from cr_budget_data
////////			 where allocation_id = :allocation_id
////////				and month_number  = :month_number
////////				and month_period  = :month_period
////////				and target_credit = 'TARGET'
////////				and charging_cost_center = :charging_cost_center;
////////		else
////////			select min(gl_journal_category), min(gl_source)
////////			  into :gl_journal_category, :gl_source
////////			  from cr_budget_data
////////			 where allocation_id = :allocation_id
////////				and month_number  = :month_number
////////				and month_period  = :month_period
////////				and target_credit = 'TARGET'
////////				and charging_cost_center = :charging_cost_center;
////////		end if
////////	else
////////		if i_me_validations = "YES" or i_combo_validations = "YES" or &
////////		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
////////			i_custom_validations = "YES" &
////////		then
////////			select min(gl_journal_category), min(gl_source)
////////			  into :gl_journal_category, :gl_source
////////			  from cr_budget_data
////////			 where allocation_id = :allocation_id
////////				and month_number  = :month_number
////////				and month_period  = :month_period
////////				and target_credit = 'TARGET'
////////				and charging_cost_center = :charging_cost_center;
////////		else
////////			select min(gl_journal_category), min(gl_source)
////////			  into :gl_journal_category, :gl_source
////////			  from cr_budget_data
////////			 where allocation_id = :allocation_id
////////				and month_number  = :month_number
////////				and month_period  = :month_period
////////				and target_credit = 'TARGET'
////////				and charging_cost_center = :charging_cost_center;
////////		end if
////////	end if
////////	
////////	if isnull(gl_journal_category) then
////////		f_pp_msgs("  ")
////////		f_pp_msgs("ERROR: could not determine the gl_journal_category for credits: " + &
////////			sqlca.SQLErrText)
////////		f_pp_msgs("  ")
////////		f_pp_msgs("charging_cost_center = " + charging_cost_center)
////////		f_pp_msgs("  ")
////////		return -1
////////	end if
////////	
////////	if isnull(gl_source) then
////////		f_pp_msgs("  ")
////////		f_pp_msgs("ERROR: could not determine the gl_source for credits: " + &
////////			sqlca.SQLErrText)
////////		f_pp_msgs("  ")
////////		f_pp_msgs("charging_cost_center = " + charging_cost_center)
////////		f_pp_msgs("  ")
////////		return -1
////////	end if
	
	
	
	//
	//  Book the credit.
	//
	if no_targets then continue // Or we'll book CREDITS with no TARGETS
	
	if upper(i_test_or_actual) = "TEST" then
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = 'insert into cr_budget_data_test ("ID'
		else
			sqls = 'insert into cr_budget_data_test ("ID'
		end if
	else
		if i_me_validations = "YES" or i_combo_validations = "YES" or &
		   i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
			i_custom_validations = "YES" &
		then
			sqls = 'insert into cr_budget_data ("ID'
		else
			sqls = 'insert into cr_budget_data ("ID'
		end if
	end if
	
	for e = 1 to i_num_elements
		element_descr     = upper(trim(i_ds_elements.GetItemString(e, "description")))
		element_descr     = f_replace_string(element_descr, " ", "_", "all")
		element_descr     = f_replace_string(element_descr, "/", "_", "all")
		element_descr     = f_replace_string(element_descr, "-", "_", "all")
		sqls = sqls + '","' + element_descr
		if i_suspense_accounting = "YES" then
			//  Place the "orig" field right next to its ACK counterpart.
			sqls = sqls + '","ORIG_' + element_descr
		end if
	next
	
	sqls = sqls + '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
					  ',"MONTH_NUMBER","MONTH_PERIOD"' + &
					  ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
					  ',"ALLOCATION_ID","TARGET_CREDIT",' + &
					  '"CROSS_CHARGE_COMPANY") (select '
	
	select crdetail.nextval into :id from dual;
	
	sqls = sqls + string(id) + ' as "ID'
	
	for e = 1 to i_num_elements
		element_descr     = upper(trim(i_ds_elements.GetItemString(e, "description")))
		element_descr     = f_replace_string(element_descr, " ", "_", "all")
		element_descr     = f_replace_string(element_descr, "/", "_", "all")
		element_descr     = f_replace_string(element_descr, "-", "_", "all")
		
////////		if element_descr = "GL_SOURCE" then
////////			//  Get it from the allocation setup ... retrieved above from the targets.
////////			sqls = sqls + '",' + "'" + gl_source + "' " + 'as "GL_SOURCE'
////////		else
			sqls = sqls + '","' + element_descr
			if i_suspense_accounting = "YES" then
				//  The "orig" field is right next to its ACK counterpart.
				sqls = sqls + '","' + element_descr
			end if
////////		end if
	next
	
	//  For now since the rounding plug will force 100% to clear.
	//  Note that cr_budget_data is hardcoded in uf_clearing_balance.
	amount_for_credit = -xcc_pot
	qty_for_credit    = -xcc_pot_qty
	
	if amount_for_credit < 0 then
		dr_cr_id_for_credit = -1
	else
		dr_cr_id_for_credit =  1
	end if
	
	if i_where_source_id = 0 then
		table_name = "CR_BUDGET_DATA"
	else
		setnull(table_name)
		select trim(table_name) into :table_name from cr_sources 
		 where source_id = :i_where_source_id;
		if isnull(table_name) or table_name = "" then
			f_pp_msgs("  ")
			f_pp_msgs("Could not find the table_name for where_source_id = " + &
				string(i_where_source_id) + "!  No allocation will be calculated.")
			f_pp_msgs("  ")
			f_pp_msgs(i_department_field + " = " + charging_cost_center)
			f_pp_msgs("  ")
			rollback using sqlca;
			return -1
		end if
	end if
	
	if i_allocate_quantity = 1 then
		//  qty_for_credit is good ...
	else
		qty_for_credit = 0
	end if
	
	sqls = sqls + '",' + string(dr_cr_id_for_credit) + ' as DR_CR_ID' + &
					  ',"LEDGER_SIGN",' + string(qty_for_credit) + ' as QUANTITY,' + &
					  string(amount_for_credit) + &
					  ',' + string(month_number) + ' as MONTH_NUMBER,0 as MONTH_PERIOD' + &
					  ', ' + "'" + gl_journal_category + "'" + ' as GL_JOURNAL_CATEGORY' + &
					  ',"AMOUNT_TYPE"' + &
					  ', ' + string(allocation_id) + ' as ALLOCATION_ID,' + &
					  "'CREDIT'" + ' as TARGET_CREDIT,' + &
					  'NULL as CROSS_CHARGE_COMPANY from ' + table_name + " "
	
	//  The " and rownum = 1" is to force only a single credit record.  Note that
	//  the "where" is already embedded in i_balance_where_clause.
	sqls = sqls + " " + i_balance_where_clause + " and rownum = 1)"
	
	if g_debug = 'YES' then uf_msg("debug", "uf_run_by_department: CREDIT sqls = " + sqls)
	
	execute immediate :sqls;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("ERROR: inserting the credit record: " + sqlca.SQLErrText)
		f_pp_msgs("  ")
		f_pp_msgs("sqls = " + sqls)
		f_pp_msgs("  ")
		f_pp_msgs(i_department_field + " = " + charging_cost_center)
		f_pp_msgs("  ")
		return -1
	end if
	
	
	after_credits:
	
	
next  //  for i = 1 to num_distinct_x_cc_values ...


DESTROY ds_distinct_x_cc_values


//after_cc_based_allocations:


return 1
end function

public function string uf_rates_by_structure_element (longlong a_structure_id);string e_for_rates

setnull(e_for_rates)
select upper(b.description) into :e_for_rates
  from cr_structures a, cr_elements b
where a.element_id = b.element_id and a.structure_id = :a_structure_id;
if isnull(e_for_rates) then e_for_rates = "element_for_rates_was_null"

e_for_rates = f_cr_clean_string(e_for_rates)

return e_for_rates
end function

public function longlong uf_rates_by_structure_structure_id (longlong a_rate_id);longlong s_id_for_rates

s_id_for_rates = 0
select structure_id_for_rates into :s_id_for_rates from cr_rates
 where rate_id = :a_rate_id;

return s_id_for_rates
end function

public function decimal uf_rates_by_structure_rate (longlong a_structure_id, string a_v_for_rates);decimal {10} rate

//
//  I'm a big, fat idiot ... a length of 3 was hardcoded into the outer where
//  clause below because I was testing with an ACK field of that length.
//  Changed the SQL to use instr's to always get it right.
//

//setnull(rate)
//select rate into :rate from
//	(
//		select sys_connect_by_path(element_value, '///ddd///') as sys_con_value,
//				 level as the_level, element_value, detail_budget, rate
//		  from cr_structure_values2
//		 where structure_id = :a_structure_id and status = 1 
//		 start with structure_id = :a_structure_id
//		 connect by prior parent_value = element_value and structure_id = :a_structure_id
//		 order by level
//	)
// where (substr(sys_con_value, 10, 11) = :a_v_for_rates || '///ddd///' 
//		  or 
//		  substr(sys_con_value, 10,  3) = :a_v_for_rates || ':')
//	and rate is not null and rownum = 1;
//if isnull(rate) then rate = 0

setnull(rate)
select rate into :rate from
	(
		select sys_connect_by_path(element_value, '///ddd///') as sys_con_value,
				 level as the_level, element_value, detail_budget, rate
		  from cr_structure_values2
		 where structure_id = :a_structure_id and status = 1 
		 start with structure_id = :a_structure_id
		 connect by prior parent_value = element_value and structure_id = :a_structure_id
		 order by level
	)
 where (substr(sys_con_value, 10, instr(sys_con_value, '///ddd///', 1, 2) - 10) = :a_v_for_rates
		  or 
		  substr(sys_con_value, 10, instr(sys_con_value, ':', 10, 1) - 10) = :a_v_for_rates)
	and rate is not null and rownum = 1;
if isnull(rate) then rate = 0

return rate
end function

public function longlong uf_run_by_department_lookup_field ();longlong counter


//
//  Look up the department field.
//
setnull(i_department_field)
select upper(trim(control_value)) into :i_department_field
  from cr_system_control 
 where upper(trim(control_name)) = 'DEPT-BASED ALLOCATIONS: DEPT FIELD';

if isnull(i_department_field) or trim(i_department_field) = "" then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: No DEPT-BASED ALLOCATIONS: DEPT FIELD was found in cr_system_control ... " + &
				  "The department-based allocation cannot be calculated.")
	f_pp_msgs("  ")
	rollback using sqlca;
	return -1
end if


//
//  Get rid of any ' ', '-', or '/' ...
//
i_department_field = f_cr_clean_string(trim(i_department_field))


//
//  Is the department field really in the ACK?
//
counter = 0
select count(*) into :counter from cr_elements
 where upper(trim(description)) = upper(trim(replace(:i_department_field, '_', ' ')));
if isnull(counter) then counter = 0

if counter <= 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: " + i_department_field + " is not in the accounting key ... " + &
				  "The department-based allocation cannot be calculated.")
	f_pp_msgs("  ")
	rollback using sqlca;
	return -1
end if


return 1

end function

public function longlong uf_derivations ();//**********************************************************************************************
//
//  User Object Function  :  uf_derivations
//
//  Description           :  Calls base derivations as identified in cr_deriver_type_sources.
//
//  Notes                 :  NO COMMITS OR ROLLBACKS IN THIS FUNCTION !!!
//
//                           Code ported from cr_allocations and modified for budget.
//
//  cr_deriver_type_sources
//  -----------------------
//  TYPE              :  the derivation type (same as entered in cr_deriver_control)
//  SOURCE_ID         :  from cr_sources ... identifies the table to apply derivations against
//  RUN_ORDER         :  for this source_id, the order in which the derivations should run
//  BASIS             :  for update derivations,
//                          the a_join_field argument ... if multiples, comma-separate them
//                       for insert derivations,
//                          the a_join argument ... which is the where clause ... use
//                          "cr." as the prefix for the transaction table
//  INSERT_OR_UPDATE  :  'Update' or 'Insert' to identify which kind of derivation this is
//  DELIMITER         :  for update types only ... the delimiter separating the basis fields
//  OFFSET_TYPE       :  for insert types only ... the type value for the offsets ... if null
//                       then you need a separate record for the offset type (although both
//                       sets of records would be tagged as TARGET)
//  OFFSET_BASIS      :  see BASIS ... for the offset to an insert derivation
//  INCLUDE_DERIVATION_ROLLLUP  :  FUTURE DEVELOPMENT FOR SAP TYPE DERIVATIONS ???
//
//**********************************************************************************************
longlong rtn, source_id, num_types, t, select_counter, override_counter, counter
// SEK 082410: Changed max_id from longlong to longlong due to volume of
//		data at Southern. 
longlong max_id
string staging_table, sqls, join_fields[], lookup_table, the_type, delimiter, basis, &
       insert_or_update, offset_type, offset_basis, or_name, col_name, suppl_wc, &
		 basis_2nd_arg, basis_6th_arg, id_balancing_basis, null_str_array[]


if g_debug = 'YES' then
	f_pp_msgs("Derivations started at: " + string(now()))
end if


//**********************************************************************************************
//
//  Determine which table we are dealing with.
//  Retrieve the source_id for the table.
//  Retrieve the Type/Sources records.
//
//**********************************************************************************************
if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
	if i_run_as_a_test then
		staging_table = "cr_budget_data_test"
	else
		staging_table = "cr_budget_data"
	end if
else                                                          //  Other Allocations ...
	if i_run_as_a_test then
		staging_table = "cr_budget_data_test"
	else
		staging_table = "cr_budget_data"
	end if
end if

//lookup_table = left(staging_table, len(staging_table) - 4) --- no "_STG" for budget

source_id = -3 // -3 = budget allocations
i_uo_cr_derivation.i_budget_version = i_budget_version // budget allocations only
if g_debug = 'YES' then i_uo_cr_derivation.i_debug = "YES"

//select source_id into :source_id from cr_sources where lower(table_name) = :lookup_table; --- not for budget

//if isnull(source_id) or source_id = -678 then
//	f_pp_msgs("DERIVATIONS: Could not find source_id for: " + staging_table) --- not for budget
//	return -1 // Return and halt if this lookup fails.
//end if

select_counter = 0
select count(*) into :select_counter from cr_deriver_type_sources_allocb
 where allocation_id = :i_allocation_id and source_id = :source_id and rownum = 1;

if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: select_counter = " + string(select_counter))

if isnull(select_counter) or select_counter = 0 then
	//  No override, use the standard setup
	sqls = "select * from cr_deriver_type_sources where source_id = " + string(source_id)
	i_cr_deriver_type_sources.SetSQLSelect(sqls)
	i_cr_deriver_type_sources.RETRIEVE()
	i_cr_deriver_type_sources.SetSort("run_order a")
	i_cr_deriver_type_sources.Sort()
	num_types = i_cr_deriver_type_sources.RowCount()
else
	//  Override for this allocation.  Note: 0 in run_order means don't run !
	sqls = "select * from cr_deriver_type_sources_allocb " + &
		"where allocation_id = " + string(i_allocation_id) + " and source_id = " + string(source_id) + " and run_order <> 0"
	i_cr_deriver_type_sources_alloc.SetSQLSelect(sqls)
	i_cr_deriver_type_sources_alloc.RETRIEVE()
	i_cr_deriver_type_sources_alloc.SetSort("run_order a")
	i_cr_deriver_type_sources_alloc.Sort()
	num_types = i_cr_deriver_type_sources_alloc.RowCount()
end if

if num_types <= 0 then
	if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: No derivations defined for: " + staging_table)
	return 1 // Return, but allow to continue.  Don't want to halt just due to a cbx checked.
end if


//**********************************************************************************************
//
//  Derivations need a real interface_batch_id.  Use the allocation_id.
//
//**********************************************************************************************
sqls = "update " + staging_table + " set interface_batch_id = allocation_id " + &
		  "where budget_version = '" + i_budget_version + "' " + &
			 "and month_number = " + string(i_month_number) + " " + &
			 "and allocation_id = " + string(i_allocation_id)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to allocation_id: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
else
	if g_debug = 'YES' then
		f_pp_msgs('*****DEBUG**** Derivations: Updated interface batch id '+string(sqlca.sqlnrows)+' rows updated ')
	end if
end if



FOR t = 1 to num_types
	
	
	
	if isnull(select_counter) or select_counter = 0 then
		//  No override, use the standard setup
		the_type         = trim(i_cr_deriver_type_sources.GetItemString(t, "type"))
		basis            = trim(i_cr_deriver_type_sources.GetItemString(t, "basis"))
		insert_or_update = upper(trim(i_cr_deriver_type_sources.GetItemString(t, "insert_or_update")))
		delimiter        = i_cr_deriver_type_sources.GetItemString(t, "delimiter")
		offset_type      = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_type"))
		offset_basis     = trim(i_cr_deriver_type_sources.GetItemString(t, "offset_basis"))
	else
		//  Override for this allocation.
		the_type         = trim(i_cr_deriver_type_sources_alloc.GetItemString(t, "type"))
		select basis, upper(trim(insert_or_update)), delimiter, offset_type, offset_basis
		  into :basis, :insert_or_update, :delimiter, :offset_type, :offset_basis
		  from cr_deriver_type_sources
		 where "TYPE" = :the_type and source_id = :source_id;
	end if
	
	if isnull(the_type)  or the_type = "" then continue // useless record
	if isnull(basis)     or basis    = "" then continue // useless record
	if isnull(delimiter)                  then delimiter    = ""
	if isnull(offset_type)                then offset_type  = "" // for g_debug below
	if isnull(offset_basis)               then offset_basis = "" // for g_debug below
	if offset_type  = "" and insert_or_update = "INSERT" then continue // useless record
	if offset_basis = "" and insert_or_update = "INSERT" then continue // useless record
	
	if g_debug = 'YES' then
		f_pp_msgs("type = " + the_type)
		f_pp_msgs("basis = " + basis)
		f_pp_msgs("insert_or_update = " + insert_or_update)
		f_pp_msgs("delimiter = " + delimiter)
		f_pp_msgs("offset_type = " + offset_type)
		f_pp_msgs("offset_basis = " + offset_basis)
	end if
	
	
	
	if insert_or_update = "INSERT" then goto insert_derivations
	//*******************************************************************************************
	//
	//  Update Derivations:
	//
	//*******************************************************************************************
	join_fields = null_str_array
	//  Parse the basis value (e.g. "company,work_order") into an array for the update function.
	f_parsestringintostringarray(basis, ",", join_fields)
	
	rtn = i_uo_cr_derivation.uf_deriver_update_mult( &
		the_type, &
		staging_table, &
		join_fields, &
		string(i_allocation_id), &
		false, &
		delimiter)
	
	if rtn <> 1 then
		return -1
	end if
	
	continue // a derivation can't be both Update AND Insert
	
	
	
	insert_derivations:
	//*******************************************************************************************
	//
	//  Insert Derivations:
	//
	//    "Basis" is the "where clause" for the derivations.  For example, it might be:
	//    "cr_allocations_stg.work_order = cr_deriver_control.string and target_credit = 'TARGET'"
	//    for the target side of the derivation and:
	//    "cr_allocations_stg.work_order = cr_deriver_control.string and cr_txn_type = 'TARGET'"
	//    for the offset side.
	//
	//    CR TXN TYPE --- for speed, we're assuming that field is on all the allocations txn
	//    tables.  If that ends up bad in the future, we'll add a system switch and evaluate
	//    it before we start looping over allocations.
	//
	//*******************************************************************************************
	
	
	//  -----------------------------------  TARGET SECTION:  -----------------------------------
	
	
	//  Need the max(id) so I can fire a cr_txn_type update below.  Just using brute force.
	//  Also, take advantage of this loop to replace "cr." with the correct table name.
	max_id = 0
	
	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_budget_data_test where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			select max(id) into :max_id from cr_budget_data where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_budget_data_test where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			select max(id) into :max_id from cr_budget_data where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	end if
	
	if isnull(max_id) then max_id = 0
	
	//  The basis will almost certainly have "cr." references for the detail table.  Change that here
	//  to the staging_table.
	basis = f_replace_string(basis, "cr.", staging_table + ".", "all")
	
	basis = basis + " and " + staging_table + ".budget_version = '" + i_budget_version        + "' "
	basis = basis + " and " + staging_table + ".month_number   = "  + string(i_month_number)  + " "
	basis = basis + " and " + staging_table + ".interface_batch_id = '"  + string(i_allocation_id) + "' "
	
	//  Call the indicated derivation type.
	rtn = i_uo_cr_derivation.uf_deriver( &
		the_type, &
		staging_table, &
		basis)
	
	if rtn <> 1 then
		return -1
	end if
	
	//  Now update the cr_txn_type.  Just using brute force.
	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
		if i_run_as_a_test then
			update cr_budget_data_test  set cr_txn_type = 'TARGET'
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			update cr_budget_data set cr_txn_type = 'TARGET'
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			update cr_budget_data_test set cr_txn_type = 'TARGET'
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			update cr_budget_data set cr_txn_type = 'TARGET'
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("DERIVATIONS: TARGETS ERROR: updating " + staging_table + ".cr_txn_type: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		return -1
	end if
	
	//  Critical update for the rounding plug.
	counter = 0
	select count(*) into :counter from all_tab_columns
	 where table_name = upper(:staging_table) and column_name = 'CR_DERIVATION_TYPE';
	if isnull(counter) then counter = 0
	
	if counter > 0 then
		if g_debug = 'YES' then
			f_pp_msgs("Performing other updates (cr_derivation_type) at " + string(now()))
		end if
		
		sqls = "update " + staging_table + &
					" set cr_derivation_type = '" + the_type + "' where id > " + string(max_id) + &
					" and budget_version = '" + i_budget_version + "'" + &
					" and interface_batch_id = '" + string(i_allocation_id) + "'"
		
		execute immediate :sqls;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("DERIVATIONS: TARGETS ERROR: updating " + staging_table + ".cr_derivation_type: " + &
				sqlca.SQLErrText)
			f_pp_msgs("  ")
			return -1
		end if
	end if
	
	
	//  -----------------------------------  OFFSET SECTION:  -----------------------------------
	
	
	//  Need the max(id) so I can fire a cr_txn_type update below.  Just using brute force.
	//  Also, take advantage of this loop to replace "cr." with the correct table name.
	max_id = 0
	
	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_budget_data_test where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			select max(id) into :max_id from cr_budget_data where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			select max(id) into :max_id from cr_budget_data_test where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			select max(id) into :max_id from cr_budget_data where allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	end if
	
	if isnull(max_id) then max_id = 0
	
	offset_basis = offset_basis + " and " + staging_table + ".budget_version = '" + i_budget_version        + "' "
	offset_basis = offset_basis + " and " + staging_table + ".month_number   = "  + string(i_month_number)  + " "
	offset_basis = offset_basis + " and " + staging_table + ".interface_batch_id = '"  + string(i_allocation_id) + "' "
	
	rtn = i_uo_cr_derivation.uf_deriver( &
		offset_type, &
		staging_table, &
		offset_basis)
	
	if rtn <> 1 then
		return -1
	end if
	
	//  Now update the cr_txn_type.  Just using brute force.
	if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
		if i_run_as_a_test then
			update cr_budget_data_test set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			update cr_budget_data set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	else                                                          //  Other Allocations ...
		if i_run_as_a_test then
			update cr_budget_data_test set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		else
			update cr_budget_data set cr_txn_type = 'OFFSET', quantity = -quantity
			 where id > :max_id and allocation_id = :i_allocation_id
				and budget_version = :i_budget_version;
		end if
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("  ")
		f_pp_msgs("DERIVATIONS: OFFSETS ERROR: updating " + staging_table + ".cr_txn_type: " + &
			sqlca.SQLErrText)
		f_pp_msgs("  ")
		return -1
	end if
	
	
	//  ----------------------------------  ROUNDING SECTION:  ----------------------------------
	
	
	//  These updates were moved from below since the proper MN is needed for the
	//  rounding function.
	if g_debug = 'YES' then
		f_pp_msgs("Performing rounding adjustment at " + string(now()))
	end if
	
	//  CLEANUP:
	//  Fix any rounding error.  Rounding must be called for each of the a_type/a_join
	//  values passed to uf_deriver.
	//
	//  And example of the basis variable at this point would be:
	//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup = cr_deriver_control.string
	//  and crb_accounts_payable_stg.cr_txn_type = 'ORIGINAL' 
	//  and crb_accounts_payable_stg.amount_type = 1 
	//  and crb_accounts_payable_stg.company = 'CECO'
	//
	//  NOTE: The last arg (a_table_name_field) is assuming that the "join" field in the stg table
	//  is the first part of the basis from cr_batch_derivation_control (so we can strip off
	//  everything to the left of the "=" sign.  For example:
	//      crb_accounts_payable_stg.work_order||crb_accounts_payable_stg.cr_derivation_rollup
	//
	//  NOTE: balancing uses either the traditional technique or the id_balancing technique.
	//  See comments in the UO function.
	//
	
	basis_2nd_arg = f_replace_string(basis, &
		"target_credit = 'TARGET' and cr_derivation_rate is null and interface_batch_id = '" + string(i_allocation_id) + "' " + &
		"and budget_version = '" + i_budget_version + "' and month_number = " + string(i_month_number), &
		"cr_txn_type = 'TARGET' and interface_batch_id = '" + string(i_allocation_id) + "'", "all")
	
	basis_6th_arg = left(basis, pos(basis, "=") - 1)
	
	id_balancing_basis = "cr_derivation_type = '" + the_type + "' and cr_txn_type = 'TARGET' " + &
		"and interface_batch_id = '" + string(i_allocation_id) + "' " + &
		"and budget_version = '" + i_budget_version + "' and month_number = " + string(i_month_number)
			
	//if id_balancing = 1 then
		rtn = i_uo_cr_derivation.uf_deriver_rounding_error_ids( &
			the_type, staging_table, &
			basis         + " and " + staging_table + ".month_number = " + string(i_month_number) + &
				" and " + staging_table + ".interface_batch_id = '" + string(i_allocation_id) + "'", &
			id_balancing_basis + " and " + staging_table + ".month_number = " + string(i_month_number) + &
				" and " + staging_table + ".interface_batch_id = '" + string(i_allocation_id) + "'", &
			"cr_derivation_type = '" + the_type + "' and cr_txn_type = 'TARGET' and month_number = " + string(i_month_number) + " " + &
				"and interface_batch_id = '" + string(i_allocation_id) + "'", &
			basis_6th_arg)
	//else
	//	rtn = i_uo_cr_derivation.uf_deriver_rounding_error( &
	//		i_type, i_stg_table, &
	//		basis         + " and " + i_stg_table + ".month_number = " + string(g_posting_mn) + &
	//						    " and budget_version = '" + g_bv + "'", &
	//		basis_2nd_arg + " and " + i_stg_table + ".month_number = " + string(g_posting_mn) + &
	//						    " and budget_version = '" + g_bv + "'", &
	//		i_stg_table + ".cr_txn_type = 'TARGET' and " + i_stg_table + ".month_number = " + string(g_posting_mn) + &
	//						    " and budget_version = '" + g_bv + "'", &
	//		basis_6th_arg)
	//end if
	
	if rtn <> 1 then
		commit; // Or we will never be able to debug !
		return -1
	end if
	
	
//
//  *****  THIS IS AN OLD VERSION OF THE CODE THAT WAS WORKING IN AN INITIAL TEST ... SAVE IT SINCE WE'VE ADDED  *****
//  *****  ADDITIONAL WHERE CLAUSE ELEMENTS ABOVE THAT MAY INTERFERE.                                            *****
//
////	//  There could be a boatload of rounding error.
//////	basis_2nd_arg = f_replace_string(basis, "cr_txn_type is NULL and target_credit = 'TARGET'", "cr_txn_type = 'TARGET'", "all") // I'M HOPING
//	// THAT WE CAN GET THIS ARG RIGHT IN THE TABLE TO HANDLE BOTH THE TARGETS AND THE BASIS FOR THE ROUDNING ... IT SEEMS LIKE THEY SHOULD BE
//	// THE SAME.
//
//	basis_6th_arg = left(basis, pos(basis, "=") - 1)
//
//	id_balancing_basis = "cr_txn_type = 'TARGET' and budget_version = '" + i_budget_version + "' and month_number = " + string(i_month_number)
//	
//	rtn = i_uo_cr_derivation.uf_deriver_rounding_error_ids( &
//		the_type, staging_table, &
//		basis         + " and " + staging_table + ".month_number = " + string(i_month_number), &
//		id_balancing_basis + " and " + staging_table + ".month_number = " + string(i_month_number), &
//		"cr_txn_type = 'TARGET' and month_number = " + string(i_month_number), &
//		basis_6th_arg)
//	
//	if rtn <> 1 then
//		//  No msgs ... uf_deriver_rounding_error handles its own.
//		rollback;
//		return -1
//	end if
//
//  *****  END OF: THIS IS AN OLD VERSION OF THE CODE ...  *****
//
	
	
NEXT  //  FOR t = 1 to num_types ...


//*******************************************************************************************
//
//  Derivation Overrides:  #2 table
//
//    Re-using variables in this section from above.
//
//*******************************************************************************************
override_counter = 0
select count(*) into :override_counter from cr_deriver_or2_sources_allocb
 where allocation_id = :i_allocation_id and source_id = :source_id and rownum = 1;

if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: override_counter = " + string(override_counter))

if isnull(override_counter) or override_counter = 0 then
	//  No override, use the standard setup
	sqls = "select * from cr_deriver_override2_sources where source_id = " + string(source_id)
	i_cr_deriver_override2.SetSQLSelect(sqls)
	i_cr_deriver_override2.RETRIEVE()
	i_cr_deriver_override2.SetSort("run_order a")
	i_cr_deriver_override2.Sort()
	num_types = i_cr_deriver_override2.RowCount()
else
	//  Override for this allocation.  Note: 0 in run_order means don't run !
	sqls = "select * from cr_deriver_or2_sources_allocb " + &
		"where allocation_id = " + string(i_allocation_id) + " and source_id = " + string(source_id) + " and run_order <> 0"
	i_cr_deriver_override2_alloc.SetSQLSelect(sqls)
	i_cr_deriver_override2_alloc.RETRIEVE()
	i_cr_deriver_override2_alloc.SetSort("run_order a")
	i_cr_deriver_override2_alloc.Sort()
	num_types = i_cr_deriver_override2_alloc.RowCount()
end if

if num_types <= 0 then
	if g_debug = 'YES' then f_pp_msgs("DERIVATIONS: No derivations defined for: " + staging_table)
	return 1 // Return, but allow to continue.  Don't want to halt just due to a cbx checked.
end if



FOR t = 1 to num_types
	
	
	
	if isnull(override_counter) or override_counter = 0 then
		//  No override, use the standard setup
		or_name  = trim(i_cr_deriver_override2.GetItemString(t, "override_name"))
		col_name = trim(i_cr_deriver_override2.GetItemString(t, "col_name"))
		suppl_wc = trim(i_cr_deriver_override2.GetItemString(t, "suppl_where_clause"))
	else
		//  Override for this allocation.
		or_name  = trim(i_cr_deriver_override2_alloc.GetItemString(t, "override_name"))
		col_name = trim(i_cr_deriver_override2_alloc.GetItemString(t, "col_name"))
		select suppl_where_clause
		  into :suppl_wc
		  from cr_deriver_override2_sources
		 where override_name = :or_name and col_name = :col_name and source_id = :source_id;
	end if
	
	if isnull(or_name)  or or_name  = "" then continue // useless record
	if isnull(col_name) or col_name = "" then continue // useless record
	if isnull(suppl_wc) then suppl_wc = "" // good record
		
	if g_debug = 'YES' then
		f_pp_msgs("or_name = " + or_name)
		f_pp_msgs("col_name = " + col_name)
		f_pp_msgs("suppl_wc = " + suppl_wc)
	end if
	
	//  Setting these to the staging_table ought to work just fine since you'd only be using
	//  these references if you're trying to update the table from itself (using the "res_table"
	//  and "src_table" keywords.  Otherwise, you'd have an override set up for allocations that
	//  probably has the "src_table" hardcoded (like work_order_control, a custom table, etc.).
	//  In all cases, I imagine, the "src table" will be referenced in the override_sql and
	//  replaced in the uf call.
	i_uo_cr_derivation.pi_s_res_table       = staging_table
	i_uo_cr_derivation.pi_s_src_table       = staging_table
	i_uo_cr_derivation.pi_s_res_arg         = string(i_allocation_id)
	i_uo_cr_derivation.i_suppl_where_clause = suppl_wc
	
	i_uo_cr_derivation.i_suppl_where_clause = i_uo_cr_derivation.i_suppl_where_clause + &
	" and budget_version = '" + i_budget_version + "'"
	
	rtn = i_uo_cr_derivation.uf_deriver_update_override(or_name, col_name)
	
	if rtn <> 1 then
		return -1
	end if
	
	

NEXT  //  FOR t = 1 to num_types ...




//**********************************************************************************************
//
//  Derivations need a real interface_batch_id.  Change it back so nothing else has a problem.
//
//**********************************************************************************************
sqls = "update " + staging_table + " set interface_batch_id = NULL " + &
		  "where budget_version = '" + i_budget_version + "' " + &
			 "and month_number = " + string(i_month_number) + " " + &
			 "and allocation_id = " + string(i_allocation_id)

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to NULL: " + &
		sqlca.SQLErrText)
	f_pp_msgs("  ")
	return -1
end if


if g_debug = 'YES' then
	f_pp_msgs("Derivations complete at: " + string(now()))
end if


return 1
end function

public function string uf_save_report_sql_balance (string a_allocation_sqls, boolean a_custom_dw);//******************************************************************************************
//
//  User Object Function  :  uf_save_report_sql
//
//  Description           :  Build the string used to save the source SQL for reporting
//                           the allocation results.
//  
//******************************************************************************************
longlong where_pos, group_pos, source_sql_counter, source_sql_rows, i, mn, mp

//  Save the source SQL for reporting ...
if a_custom_dw then
	setnull(i_balance_sqls)
else
	i_balance_sqls = a_allocation_sqls
	where_pos       = pos(i_balance_sqls, "where")
	if where_pos > 0 then
		i_balance_sqls = right(i_balance_sqls, len(i_balance_sqls) - (where_pos - 1))
		group_pos     = pos(i_balance_sqls, "group by")
		if group_pos > 0 then
			i_balance_sqls = left(i_balance_sqls, group_pos - 1)
		end if
	end if
	if i_whole_month then
		//  Must add the list of month_periods to the reporting SQL, just in case another
		//  month_period is run ...
		//  NO, NO, NO !!!
//		source_sql_counter = 0
//		if isvalid(w_cr_control) then
//			datawindow d
//			d = w_cr_control.tab_control.tabpage_allocations1.dw_1
//			source_sql_rows = d.RowCount()
//		end if
//		for i = 1 to source_sql_rows
//			mn = d.GetItemNumber(i, 1)
//			if mn = i_month_number then
//				source_sql_counter++
//				mp = d.GetItemNumber(i, 2)
//				if source_sql_counter = 1 then
//					i_balance_sqls = i_balance_sqls + "and (month_period = " + string(mp)
//				else
//					i_balance_sqls = i_balance_sqls + " or month_period = " + string(mp)
//				end if
//			end if
//		next
//		if source_sql_counter > 0 then
//			i_balance_sqls = i_balance_sqls + ") "
//		end if
	end if
end if

return i_balance_sqls
end function

public function decimal uf_clearing_balance_qty (longlong a_cr_company_id, string a_company_field);//******************************************************************************************
//
//  User Object Function:  uf_clearing_balance_qty
//
//	 Description         :  Retrieves the sum(quantity) from cr_budget_data based on
//								   a where clause that the user defines in the 
//									cr_allocation_control_bdg.balance_where_clause_id ...
//
//  Notes               :  This is hardcoded to return only 1 balance (pot).
//									It retrieves it for all months in the cost_repository, so any
//									remaining balance that is inadvertently left in the pot will
//									clear.
//
//                         Added code to restrict the pot by the cr_company_id on
//                         cr_allocation_control_bdg.
//
//******************************************************************************************
longlong num_rows, i, balance_where_clause_id, &
       clearing_balance_start_month, clearing_balance_end_month, rtn, structure_id, &
		 not_field
string sqls, left_paren, col_name, operator, value1, between_and, value2, &
		 balance_sqls, right_paren, and_or, external_company_id, structure_where, &
		 orig_value1, not_upper_value1, dserror, rtn_f_create_str, &
		 hint_syntax, rtns
decimal {2} balance

clearing_balance_start_month = 0
clearing_balance_end_month   = 0

select balance_where_clause_id,
       clearing_balance_start_month, 
		 clearing_balance_end_month
  into :balance_where_clause_id, 
       :clearing_balance_start_month,
		 :clearing_balance_end_month
  from cr_allocation_control_bdg where allocation_id = :i_allocation_id;

//  The user chose "Current" as the option ...
if clearing_balance_start_month = 0 then clearing_balance_start_month = i_month_number
if   clearing_balance_end_month = 0 then   clearing_balance_end_month = i_month_number

/*### - MDZ - 8597 - 20110908*/
//  The ytd_indicator overrides any selected months.
if i_ytd_indicator = 1 then
	/*### - MDZ - 29485 - 20130307*/
	/*Set the fiscal year start month*/
	clearing_balance_start_month = i_fy_start_month
	clearing_balance_end_month   = i_month_number
end if

balance_sqls = "select sum(quantity) from cr_budget_data "

//  Where Clause:
uo_ds_top ds_where_clause
ds_where_clause = CREATE uo_ds_top
ds_where_clause.DataObject = "dw_temp_dynamic"
ds_where_clause.SetTransObject(sqlca)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(balance_where_clause_id) + " order by row_id"

f_create_dynamic_ds(ds_where_clause, "grid", sqls, sqlca, true)

num_rows = ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		if isnull(a_cr_company_id) or a_cr_company_id = 0 then
			//  DO NOT restrict by company ...
			balance_sqls = balance_sqls + &
			             " where month_number >= " + string(clearing_balance_start_month) + &
							 "   and month_number <= " + string(clearing_balance_end_month) + &
							 "   and budget_version = '" + i_budget_version + "'" + &
			             "   and ( "
		else
			if i_ignore_header_co = 1 then
				//  DO NOT restrict by company ...
				balance_sqls = balance_sqls + &
								 " where month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "'" + &
								 "   and ( "
			else
				//  DO restrict by company ...
				select external_company_id into :external_company_id
				  from cr_company where cr_company_id = :a_cr_company_id;
				balance_sqls = balance_sqls + &
								 " where month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "'" + &
								 "   and " + a_company_field + " = '" + &
									upper(trim(external_company_id)) + "' and ( "
								
			end if            
		end if
	end if

	left_paren  = upper(trim(ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(ds_where_clause.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(ds_where_clause.GetItemString(i, "value1"))
	orig_value1 = upper(ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{" then
			value1 = not_upper_value1
		else
			value1 = upper(trim(ds_where_clause.GetItemString(i, "value1")))
		end if
	end if
	
	between_and = upper(trim(ds_where_clause.GetItemString(i, "between_and")))
	value2      = upper(trim(ds_where_clause.GetItemString(i, "value2")))
	right_paren = upper(trim(ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(ds_where_clause.GetItemString(i, "and_or")))
	structure_id = ds_where_clause.GetItemNumber(i, "structure_id")
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0
	
	//  If they manually entered an Oracle function, take it as-is.
	if pos(col_name, "(") = 0 then
		col_name = f_cr_clean_string(col_name)
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = &
			uf_structure_value(structure_id, col_name, value1, not_field)
//		balance_sqls = balance_sqls + structure_where + " and "
		balance_sqls = balance_sqls + left_paren + structure_where + right_paren + " " + and_or + " "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			end if
		else
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			end if
		end if
	else
		if pos(col_name, "(") <> 0 then
			//  NO DOUBLE QUOTES ON COL_NAME!
			balance_sqls = balance_sqls + &
								left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
		else
			balance_sqls = balance_sqls + &
								left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or		
		end if
	end if
	
	bottom:
	
	if i = num_rows then
		balance_sqls = balance_sqls + " )"
	end if
	
next

uo_ds_top ds_balance
ds_balance = CREATE uo_ds_top

// ### 10906: JAK: 2012-09-11:  Move f_sql_add_hint lower so it can be applied to latter select statements
// ### - SEK - 061311 - 7742: Look up and add any hints using f_sql_add_hint
hint_syntax = 'bud_alloc_balance_' + string(i_allocation_id)
rtns = f_sql_add_hint(balance_sqls, hint_syntax)

if isnull(rtns) or rtns = "" then
	uf_msg("","  ")
	uf_msg("","ERROR: Adding hint '" + hint_syntax + "'")
	uf_msg(""," THE ALLOCATION WILL CONTINUE TO RUN ")
	uf_msg("","  ")
else 
	balance_sqls = rtns
end if

rtn_f_create_str = f_create_dynamic_ds(ds_balance, "grid", balance_sqls, sqlca, true)

////  NOT FOR QUANTITIES !!!
////  Save the balance SQL for reporting ...
//i_balance_sqls = uf_save_report_sql(balance_sqls, false)

if rtn_f_create_str <> "OK" then
//if dserror <> "OK" then
	//f_pp_msgs("ERROR in uf_clearing_balance_xcc_qty: " + left(dserror, 224))
	f_pp_msgs("ERROR in uf_clearing_balance_xcc_qty: " + string(rtn))
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	i_errors = true
	return -1
end if

if rtn = -999 then
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	i_errors = true
	return -1
end if
//  ds_balance now contains the pot of dollars ...

balance = ds_balance.GetItemNumber(1, 1)

DESTROY ds_where_clause
DESTROY ds_balance

return balance
end function

public function decimal uf_clearing_balance_xcc_qty (longlong a_cr_company_id, string a_company_field, longlong a_allocation_id, longlong a_month_number, string a_charging_cost_center, string a_dept_field);//******************************************************************************************
//
//  User Object Function:  uf_clearing_balance
//
//	 Description         :  Retrieves the sum(quantity) from cr_budget_data based on
//								   a where clause that the user defines in the 
//									cr_allocation_control_bdg.balance_where_clause_id ...
//
//  Notes               :  This is hardcoded to return only 1 balance (pot).
//									It retrieves it for all months in the cost_repository, so any
//									remaining balance that is inadvertently left in the pot will
//									clear.
//
//                         Added code to restrict the pot by the cr_company_id on
//                         cr_allocation_control_bdg.
//
// -------------------------------------------------------------------------------
//
//  THIS FUNCTION IS THE SAME AS THE ONE IN UO_CR_ALLOCATIONS EXCEPT WE ADDED
//  A HARDCODED " XCC = :XCC " TO THE WHERE CLAUSE.
//
// -------------------------------------------------------------------------------
//
//******************************************************************************************
longlong num_rows, i, balance_where_clause_id, &
       clearing_balance_start_month, clearing_balance_end_month, structure_id, &
		 not_field, where_source_id, rtn
string sqls, left_paren, col_name, operator, value1, between_and, value2, &
		 balance_sqls, right_paren, and_or, external_company_id, dserror, structure_where, &
		 orig_value1, table_name, not_upper_value1, department_field, rtn_f_create_str, &
		 hint_syntax, rtns
decimal {2} balance


department_field             = a_dept_field
clearing_balance_start_month = 0
clearing_balance_end_month   = 0

select balance_where_clause_id,
       clearing_balance_start_month, 
		 clearing_balance_end_month
  into :balance_where_clause_id, 
       :clearing_balance_start_month,
		 :clearing_balance_end_month
  from cr_allocation_control_bdg where allocation_id = :a_allocation_id;

//  The user chose "Current" as the option ...
if clearing_balance_start_month = 0 then clearing_balance_start_month = a_month_number
if   clearing_balance_end_month = 0 then   clearing_balance_end_month = a_month_number

//  The i_ytd_indicator overrides any selected months.
if i_ytd_indicator = 1 then
	clearing_balance_start_month = long(left(string(a_month_number), 4) + "01")
	clearing_balance_end_month   = a_month_number
end if


//  Check for a detailed attribute allocation.
where_source_id = 0
select source_id into :where_source_id from cr_alloc_where_bdg
 where where_clause_id = :balance_where_clause_id;
if isnull(where_source_id) then where_source_id = 0

i_where_source_id = where_source_id

if where_source_id = 0 then
	table_name = "CR_BUDGET_DATA"
else
	setnull(table_name)
	select trim(table_name) into :table_name from cr_sources 
	 where source_id = :where_source_id;
	if isnull(table_name) or table_name = "" then
		f_pp_msgs("Could not find the table_name for where_source_id = " + &
			string(where_source_id) + "!  No allocation will be calculated.")
		rollback using sqlca;
		// ### 8142: JAK: 2011-11-01:  Consistent return codes
		i_errors = true
		return -1
	end if
end if


balance_sqls = "select sum(quantity) from " + table_name + " "

//  Where Clause:
uo_ds_top ds_where_clause
ds_where_clause = CREATE uo_ds_top
ds_where_clause.DataObject = "dw_temp_dynamic"
ds_where_clause.SetTransObject(sqlca)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = " + &
		  string(balance_where_clause_id) + " order by row_id"

f_create_dynamic_ds(ds_where_clause, "grid", sqls, sqlca, true)

num_rows = ds_where_clause.RowCount()

for i = 1 to num_rows
	
	if i = 1 then
		if isnull(a_cr_company_id) or a_cr_company_id = 0 then
			//  DO NOT restrict by company ...
			balance_sqls = balance_sqls + &
			             ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
							 "   and month_number >= " + string(clearing_balance_start_month) + &
							 "   and month_number <= " + string(clearing_balance_end_month) + &
							 "   and budget_version = '" + i_budget_version + "' " + &
			             "   and ( "
			i_balance_where_clause = &
							 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
							 "   and month_number >= " + string(clearing_balance_start_month) + &
							 "   and month_number <= " + string(clearing_balance_end_month) + &
							 "   and budget_version = '" + i_budget_version + "' " + &
			             "   and ( "
		else
			if i_ignore_header_co = 1 then
				//  DO NOT restrict by company ...
				balance_sqls = balance_sqls + &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and ( "
				i_balance_where_clause = &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and ( "
			else
				//  DO restrict by company ...
				select external_company_id into :external_company_id
				  from cr_company where cr_company_id = :a_cr_company_id;
				balance_sqls = balance_sqls + &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and " + a_company_field + " = '" + &
									upper(trim(external_company_id)) + "' and ( "
				i_balance_where_clause = &
								 ' where "' + department_field + '"' + " = '" + a_charging_cost_center + "' " + &
								 "   and month_number >= " + string(clearing_balance_start_month) + &
								 "   and month_number <= " + string(clearing_balance_end_month) + &
								 "   and budget_version = '" + i_budget_version + "' " + &
								 "   and " + a_company_field + " = '" + &
									upper(trim(external_company_id)) + "' and ( "
									
			end if
		end if
	end if

	left_paren  = upper(trim(ds_where_clause.GetItemString(i, "left_paren")))
	col_name    = upper(trim(ds_where_clause.GetItemString(i, "column_name")))
	operator    = upper(trim(ds_where_clause.GetItemString(i, "operator")))
	
	not_upper_value1 = trim(ds_where_clause.GetItemString(i, "value1"))
	orig_value1 = upper(ds_where_clause.GetItemString(i, "value1"))
	
	if orig_value1 = " " then
		//  Single-space is OK.
		value1 = orig_value1
	else
		if i_use_new_structures_table = "YES" and left(not_upper_value1, 1) = "{" or &
			col_name = "GL JOURNAL CATEGORY" &
		then
			value1 = not_upper_value1
		else
			value1 = upper(trim(ds_where_clause.GetItemString(i, "value1")))
		end if
	end if
	
	between_and = upper(trim(ds_where_clause.GetItemString(i, "between_and")))
	value2      = upper(trim(ds_where_clause.GetItemString(i, "value2")))
	right_paren = upper(trim(ds_where_clause.GetItemString(i, "right_paren")))
	and_or      = upper(trim(ds_where_clause.GetItemString(i, "and_or")))
	structure_id = ds_where_clause.GetItemNumber(i, "structure_id")
	
	if isnull(left_paren)  then left_paren  = ""
	if isnull(col_name)    then col_name    = ""
	if isnull(operator)    then operator    = ""
	if isnull(value1)      then value1      = ""
	if isnull(between_and) then between_and = ""
	if isnull(value2)      then value2      = ""
	if isnull(right_paren) then right_paren = ""
	if isnull(and_or)      then and_or      = ""
	if isnull(structure_id) then structure_id = 0
	
	//  If they manually entered an Oracle function, take it as-is.
	if pos(col_name, "(") = 0 then
		col_name = f_cr_clean_string(col_name)
	end if
	
	if left(trim(value1), 1) = "{" then
		//  The user selected a structure value ...
		choose case operator
			case "=", "IN"
				//  EQUAL TO the structure value ...
				not_field = 1
			case "<>", "NOT IN"
				//  NOT EQUAL TO the structure value ...
				not_field = 0
			case else
		end choose
		structure_where = &
			uf_structure_value(structure_id, col_name, value1, not_field)
		balance_sqls = balance_sqls + structure_where + " and "
		i_balance_where_clause = i_balance_where_clause + structure_where + " and "
		goto bottom
	end if
	
	if between_and = "" then
		if operator = "IN" or operator = "NOT IN" then
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " " + &
									value1 + " " + &
									right_paren + " " + and_or
			end if
		else
			if pos(col_name, "(") <> 0 then
				//  NO DOUBLE QUOTES ON COL_NAME!
				balance_sqls = balance_sqls + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			else
				balance_sqls = balance_sqls + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
				i_balance_where_clause = i_balance_where_clause + &
									left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
									value1 + "' " + &
									right_paren + " " + and_or
			end if
		end if
	else
		if pos(col_name, "(") <> 0 then
			//  NO DOUBLE QUOTES ON COL_NAME!
			balance_sqls = balance_sqls + &
								left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
			i_balance_where_clause = i_balance_where_clause + &
								left_paren + ' ' + col_name + ' ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
		else
			balance_sqls = balance_sqls + &
								left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
			i_balance_where_clause = i_balance_where_clause + &
								left_paren + ' "' + col_name + '" ' + " " + operator + " '" + &
								value1 + "' " + between_and + " '" + value2 + "' " + &
								right_paren + " " + and_or
		end if
	end if
	
	bottom:
	
	if i = num_rows then
		balance_sqls = balance_sqls + " )"
		i_balance_where_clause = i_balance_where_clause + " )"
	end if
	
next

uo_ds_top ds_balance
ds_balance = CREATE uo_ds_top

// ### 10906: JAK: 2012-09-11:  Move f_sql_add_hint lower so it can be applied to latter select statements
// ### - SEK - 061311 - 7742: Look up and add any hints using f_sql_add_hint
hint_syntax = 'bud_alloc_balance_' + string(i_allocation_id)
rtns = f_sql_add_hint(balance_sqls, hint_syntax)

if isnull(rtns) or rtns = "" then
	uf_msg("","  ")
	uf_msg("","ERROR: Adding hint '" + hint_syntax + "'")
	uf_msg(""," THE ALLOCATION WILL CONTINUE TO RUN ")
	uf_msg("","  ")
else 
	balance_sqls = rtns
end if

rtn_f_create_str = f_create_dynamic_ds(ds_balance, "grid", balance_sqls, sqlca, true)
//  ds_balance now contains the pot of dollars ...

if rtn_f_create_str <> "OK" then
//if dserror <> "OK" then
	//f_pp_msgs("ERROR in uf_clearing_balance_xcc_qty: " + left(dserror, 224))
	f_pp_msgs("ERROR in uf_clearing_balance_xcc_qty: " + string(rtn))
	// ### 8142: JAK: 2011-11-01:  Consistent return codes
	i_errors = true
	return -1
end if

if ds_balance.RowCount() = 0 then
	return 0
end if

balance = ds_balance.GetItemNumber(1, 1)

DESTROY ds_where_clause
DESTROY ds_balance

return balance
end function

public function longlong uf_validations ();//*****************************************************************************************
//
//  Function     :  uf_validations
//
//  Description  :  Performs master elements and combo validations against the allocation
//                  staging tables.
//
//  Notes        :  All acctg. key elements are validated against their ME table.  That
//                  means they all must have a ME table.
//
//                  UPDATED FOR THE NEW PARTIAL VALIDATIONS TECHNIQUE.
//
//*****************************************************************************************
longlong num_rows, i, vs_flag, ii, invalid_rowcount, counter, lvalidate_rtn, &
		 validation_counter
string sqls, cr_element, staging_table, me_table_element, me_table, element_descr, &
		 invalid_value, validate_rtn, cv_validations, invalid_ids_table
boolean validation_kickouts


//  NOT FOR BUDGET ALLOCATIONS:  TABLE NAME IS ALWAYS THE TEMP TABLE.
////  Determine which table we are validating.
//if i_clearing_indicator = 4 or i_clearing_indicator = 5 then  //  Inter-Company ...
//	if i_run_as_a_test then
//		staging_table = "cr_budget_data_test"
//	else
//		staging_table = "cr_budget_data"
//	end if
//else                                                          //  Other Allocations ...
//	if i_run_as_a_test then
//		staging_table = "cr_budget_data_test"
//	else
//		staging_table = "cr_budget_data"
//	end if
//end if
staging_table = "cr_budget_data_temp"

if i_run_as_a_test then
	invalid_ids_table = "cr_budget_data_test"
else
	invalid_ids_table = "cr_budget_data"
end if


//  NOT FOR BUDGET ALLOCATIONS:  IFB_ID IS ALREADY POPULATED.
////  Validations need a NOT NULL interface_batch_id.
//sqls = "update " + staging_table + " set interface_batch_id = '0'"
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to 0: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	return -1
//end if


//*****************************************************************************************
//
//	 VALIDATIONS:  Call the new validations user object.  It is assumed that combos have
//						already been created.
//
//*****************************************************************************************
if g_debug = 'YES' then
	f_pp_msgs("Analyzing stg table at: " + string(now()))
end if

sqlca.analyze_table(staging_table)

if g_debug = 'YES' then
	f_pp_msgs("Running validations at: " + string(now()))
end if

//  TO AVOID MEMORY PROBLEMS, MOVED TO THE WINDOW WF_RUN SCRIPT.
//uo_cr_validation uo_cr_validation
//uo_cr_validation = CREATE uo_cr_validation

if g_debug = 'YES' then
	f_pp_msgs(" -- Deleting from cr_validations_invalid_ids at: " + string(now()))
end if

lvalidate_rtn = i_uo_cr_validation_bdg.uf_delete_invalid_ids(invalid_ids_table, i_ifb_id) //  THIS CAN'T
	//  BE THE TEMP TABLE FOR BUDGET.  RECORDS ARE SAVED USING THE REAL TABLE.

if lvalidate_rtn <> 1 then return -1

validation_kickouts = false

setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - COMBO';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations <> "YES" then goto after_combo_validations


if g_debug = 'YES' then
	f_pp_msgs(" -- Validating the Batch (Combos) at: " + string(now()))
end if


//	 Which function is this client using?
setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'VALIDATIONS - CONTROL OR COMBOS';
if isnull(cv_validations) then cv_validations = "CONTROL"

if cv_validations = 'CONTROL' then
	lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_control(staging_table, i_ifb_id)
else
	lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_combos(staging_table, i_ifb_id)
end if

if lvalidate_rtn = -1 then
	//  Error in uf_validate_(x).  Logged by the function.
	return -1
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_counter = 0
	select count(*) into :validation_counter	from cr_validations_invalid_ids
	 where lower(table_name) = :staging_table
	   and id in (select id from cr_budget_data_temp where interface_batch_id = :i_ifb_id);
//	if upper(staging_table) = "CR_ALLOCATIONS_TEST_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_allocations_test_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_INTER_COMPANY_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_inter_company_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_INTER_COMPANY_TEST_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_inter_company_test_stg where interface_batch_id = '0');
//	end if
	if validation_counter = 0 then
	else
		validation_kickouts = true
	end if
end if

after_combo_validations:


//  NOT FOR BUDGET ALLOCATIONS:  NOT CLEAR IF THESE WILL WORK FOR BUDGET.
//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_project_validations
//
//if g_debug = 'YES' then
//	f_pp_msgs(" -- Validating the Batch (Projects) at: " + string(now()))
//end if
//
//lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_projects(staging_table, i_ifb_id)
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 then
//	//  Validation Kickouts.
//	validation_counter = 0
//	if upper(staging_table) = "CR_ALLOCATIONS_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_allocations_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_ALLOCATIONS_TEST_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_allocations_test_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_INTER_COMPANY_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_inter_company_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_INTERCOMPANY_TEST_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_inter_company_test_stg where interface_batch_id = '0');
//	end if
//	if validation_counter = 0 then
//	else
//		validation_kickouts = true
//	end if
//end if
//
//after_project_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS-BDG - ME';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations <> "YES" then goto after_me_validations

if g_debug = 'YES' then
	f_pp_msgs(" -- Validating the Batch (Element Values) at: " + string(now()))
end if

lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_me(staging_table, i_ifb_id)

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	return -1
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_counter = 0
	if upper(staging_table) = "CR_BUDGET_DATA_TEMP" then
		select count(*) into :validation_counter	from cr_validations_invalid_ids
		 where lower(table_name) = :staging_table
		   and id in (select id from cr_budget_data_temp where interface_batch_id = :i_ifb_id);
	end if
//	if upper(staging_table) = "CR_ALLOCATIONS_TEST_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_allocations_test_stg where interface_batch_id = '0');		// EXAMPLE FOR CR_BUDGET_DATA_ENTRY.
//	end if
	if validation_counter = 0 then
	else
		validation_kickouts = true
	end if
end if

after_me_validations:


//  NOT FOR BUDGET ALLOCATIONS:  MN AND ACCT RANGE VALIDATIONS DON'T MAKE SENSE FOR BUDGETING.
//setnull(cv_validations)
//select upper(trim(control_value)) into :cv_validations from cr_system_control 
// where upper(trim(control_name)) = 'ENABLE VALIDATIONS - MONTH NUMBER';
//if isnull(cv_validations) then cv_validations = "NO"
//
//if cv_validations <> "YES" then goto after_mn_validations
//
//if g_debug = 'YES' then
//	f_pp_msgs(" -- Month Number validations at: " + string(now()))
//end if
//
//lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_month_number(staging_table, i_ifb_id)
//
//if lvalidate_rtn = -1 then
//	//  Error in uf_validate.  Logged by the function.
//	return -1
//end if
//
//if lvalidate_rtn = -2 or lvalidate_rtn = -3 then
//	//  Always true here.  The MN validations are not inserted into the ids table since
//	//  MN is not editable.  However, Acct Range validations ARE inserted.  Since we don't
//	//  know which one kicked, we just have to assume TRUE.
//	validation_kickouts = true
//end if
//
//after_mn_validations:


setnull(cv_validations)
select upper(trim(control_value)) into :cv_validations from cr_system_control 
 where upper(trim(control_name)) = 'ENABLE VALIDATIONS - CUSTOM';
if isnull(cv_validations) then cv_validations = "NO"

if cv_validations <> "YES" then goto after_custom_validations

if g_debug = 'YES' then
	f_pp_msgs(" -- Custom validations at: " + string(now()))
end if

lvalidate_rtn = i_uo_cr_validation_bdg.uf_validate_custom(staging_table, i_ifb_id)

if lvalidate_rtn = -1 then
	//  Error in uf_validate.  Logged by the function.
	return -1
end if

if lvalidate_rtn = -2 then
	//  Validation Kickouts.
	validation_counter = 0
	select count(*) into :validation_counter	from cr_validations_invalid_ids
	 where lower(table_name) = :staging_table
	   and id in (select id from cr_budget_data_temp where interface_batch_id = :i_ifb_id);
//	if upper(staging_table) = "CR_ALLOCATIONS_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_allocations_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_ALLOCATIONS_TEST_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_allocations_test_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_INTER_COMPANY_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_inter_company_stg where interface_batch_id = '0');
//	end if
//	if upper(staging_table) = "CR_INTER_COMPANY_TEST_STG" then
//		select count(*) into :validation_counter	from cr_validations_invalid_ids
//		 where lower(table_name) = :staging_table
//		   and id in (select id from cr_inter_company_test_stg where interface_batch_id = '0');
//	end if
	if validation_counter = 0 then
		//  Historical accident from interfaces at other clients ... not sure why I would ever
		//  want to continue on and post the txns if there is a validation kickout!  Especially
		//  with the custom function since it is easy to code SQL in the insert of that fxn that
		//  may fail.
		validation_kickouts = true
	else
		validation_kickouts = true
	end if
end if

after_custom_validations:


//  DMJ: 6/25/08: MOVED BELOW DUE TO THE UF_SUSPENSE CALL.
//  Validations need a NOT NULL interface_batch_id ...


//  TO AVOID MEMORY PROBLEMS, MOVED TO THE WINDOW WF_RUN SCRIPT.
//DESTROY uo_cr_validation


if validation_kickouts then
	
	if i_suspense_accounting = "YES" then
		//  Call the suspense accounting function.  It will return a value of 2 if 
		//  suspense accounting is turned off in cr_system_control.
		i_uo_cr_validation_bdg.i_called_from_allocations_suspense = true
		i_uo_cr_validation_bdg.i_suspense_table = invalid_ids_table
		lvalidate_rtn = i_uo_cr_validation_bdg.uf_suspense(staging_table, i_ifb_id)
		setnull(i_uo_cr_validation_bdg.i_suspense_table)
		i_uo_cr_validation_bdg.i_called_from_allocations_suspense = false
		
		if lvalidate_rtn < 0 then
			//  Error in uf_validate.  Logged by the function.
			return -1
		else
			i_suspense_processed = true  //  For msg at the end of the logs.
		end if
	end if
	

	//  INCORRECT FOR BUDGET ALLOCATIONS
	////  DMJ: 6/25/08: SAME CODE THAT WOULD FIRE BELOW.
	//sqls = "update " + staging_table + " set interface_batch_id = NULL"
	//execute immediate :sqls;
	//
	//if sqlca.SQLCode < 0 then
	//	f_pp_msgs("  ")
	//	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to NULL: " + &
	//		sqlca.SQLErrText)
	//	f_pp_msgs("  ")
	//	return -1
	//end if
	
	//  THESE HAVE ALREADY POSTED, SO THE MESSAGE AND RETURN -1 IS POINTLESS.
	//if i_suspense_accounting = "YES" then
	//else
	//	f_pp_msgs("  ")
	//	f_pp_msgs("   -----------------------------------------------------------------------" + &
	//		"   -------------------------------------------------------------")
	//	f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  THE ALLOCATION WILL NOT RUN!  *****")
	//	f_pp_msgs("   -----------------------------------------------------------------------" + &
	//		"   -------------------------------------------------------------")
	//	f_pp_msgs("  ")
	//	return -1
	//end if
	
	i_validation_kickouts = true
	
	if i_suspense_accounting = "YES" then
		//  For budget validations ... don't want to leave the temp table in the invalid ids tables.
		//  the id where clause is in case multiple users are running allocations and validating.
		update cr_validations_invalid_ids3 set table_name = :invalid_ids_table
		 where table_name = 'cr_budget_data_temp'
			and id in (select id from cr_budget_data_temp);
			 
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_validations_invalid_ids3.table_name: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
	end if
	
	if i_suspense_accounting <> "YES" then
		//  For budget validations ... don't want to leave the temp table in the invalid ids tables.
		//  the id where clause is in case multiple users are running allocations and validating.
		update cr_validations_invalid_ids2 set table_name = :invalid_ids_table
		 where table_name = 'cr_budget_data_temp'
			and id in (select id from cr_budget_data_temp);
			 
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_validations_invalid_ids2.table_name: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
		update cr_validations_invalid_ids set table_name = :invalid_ids_table
		 where table_name = 'cr_budget_data_temp'
			and id in (select id from cr_budget_data_temp);
			 
		if sqlca.SQLCode < 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: updating cr_validations_invalid_ids.table_name: " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
	end if
	
	//  Commits have just fired in priro scripts, and a truncate is about to fire, so we might
	//  as well make sure the kickouts are saved properly here.
	commit;
	
	
end if


//  INCORRECT FOR BUDGET ALLOCATIONS
////  DMJ: 6/25/08: MOVED HERE DUE TO THE UF_SUSPENSE CALL.
////  Validations need a NOT NULL interface_batch_id.  Change it back so the count(*)'s above
////  don't have a problem.
//sqls = "update " + staging_table + " set interface_batch_id = NULL"
//execute immediate :sqls;
//
//if sqlca.SQLCode < 0 then
//	f_pp_msgs("  ")
//	f_pp_msgs("  ERROR: Updating " + staging_table + ".interface_batch_id to NULL: " + &
//		sqlca.SQLErrText)
//	f_pp_msgs("  ")
//	return -1
//end if


if g_debug = 'YES' then
	f_pp_msgs("Validations complete at: " + string(now()))
end if



//after_validations:

return 1
end function

public function string uf_save_report_sql_grouping (string a_allocation_sqls, boolean a_custom_dw);//******************************************************************************************
//
//  User Object Function  :  uf_save_report_sql_grouping
//
//  Description           :  Build the string used to save the source grouping SQL for
//                           reporting the allocation results.
//  
//******************************************************************************************
longlong where_pos, group_pos, source_sql_counter, source_sql_rows, i, mn, mp

//  Save the grouping SQL for reporting ...
if a_custom_dw then
	setnull(i_grouping_sqls)
else
	i_grouping_sqls = a_allocation_sqls
	group_pos       = pos(i_grouping_sqls, "group by")
	if group_pos > 0 then
		i_grouping_sqls = right(i_grouping_sqls, len(i_grouping_sqls) - (group_pos - 1))
	end if
	if i_whole_month then
		//  Must add the list of month_periods to the reporting SQL, just in case another
		//  month_period is run ...
		//  NO, NO, NO !!!
//		source_sql_counter = 0
//		if isvalid(w_cr_control) then
//			datawindow d
//			d = w_cr_control.tab_control.tabpage_allocations1.dw_1
//			source_sql_rows = d.RowCount()
//		end if
//		for i = 1 to source_sql_rows
//			mn = d.GetItemNumber(i, 1)
//			if mn = i_month_number then
//				source_sql_counter++
//				mp = d.GetItemNumber(i, 2)
//				if source_sql_counter = 1 then
//					i_grouping_sqls = i_grouping_sqls + "and (month_period = " + string(mp)
//				else
//					i_grouping_sqls = i_grouping_sqls + " or month_period = " + string(mp)
//				end if
//			end if
//		next
//		if source_sql_counter > 0 then
//			i_grouping_sqls = i_grouping_sqls + ") "
//		end if
	end if
end if

return i_grouping_sqls
end function

public function longlong uf_fast_targets (decimal a_rate_for_insert, longlong a_source_id);longlong i
string sqls, element_descr, gb_clause, outer_sqls
decimal {10} rate_for_insert

rate_for_insert = a_rate_for_insert

sqls       = "insert into cr_budget_data " + '("ID'
outer_sqls = "select crbudgets.nextval"
gb_clause  = ""

for i = 1 to i_num_elements
	
	element_descr = upper(trim(i_ds_elements.GetItemString(i, "description")))
	element_descr = f_cr_clean_string(element_descr)
	sqls          = sqls + '","' + element_descr
	if i = 1 then
		outer_sqls    = outer_sqls + ',"' + element_descr
	else
		outer_sqls    = outer_sqls + '","' + element_descr
	end if
	if i_suspense_accounting = "YES" then
		//  Place the "orig" field right next to its ACK counterpart.
		sqls = sqls + '","ORIG_' + element_descr
		outer_sqls = outer_sqls + '","ORIG_' + element_descr
	end if
	
next

if i_rate_field_exists then
	sqls = sqls + '","ALLOCATION_RATE'
	outer_sqls = outer_sqls + '","ALLOCATION_RATE'
end if

sqls = sqls + '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
				  ',"MONTH_NUMBER","MONTH_PERIOD"' + &
				  ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
				  ',"ALLOCATION_ID","TARGET_CREDIT",' + &
				  '"CROSS_CHARGE_COMPANY","INCEPTION_TO_DATE_AMOUNT",' + &
				  '"SOURCE_ID","BUDGET_VERSION") ('

outer_sqls = outer_sqls + '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
				  ',"MONTH_NUMBER","MONTH_PERIOD"' + &
				  ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
				  ',"ALLOCATION_ID","TARGET_CREDIT",' + &
				  '"CROSS_CHARGE_COMPANY","INCEPTION_TO_DATE_AMOUNT",' + &
				  '"SOURCE_ID","BUDGET_VERSION" from '

sqls = sqls + outer_sqls + ' (select 0 as id'

for i = 1 to i_num_elements
	
	element_descr = upper(trim(i_ds_elements.GetItemString(i, "description")))
	element_descr = f_cr_clean_string(element_descr)
	
	sqls = sqls + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '") as "' + element_descr + '"'
	
	gb_clause = gb_clause + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '")'
	
	if i_suspense_accounting = "YES" then
		//  Place the "orig" field right next to its ACK counterpart.
		sqls = sqls + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '") as "ORIG_' + element_descr + '"'
		
		gb_clause = gb_clause + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '")'
	end if
	
next

if i_rate_field_exists then
	sqls = sqls + ", " + string(rate_for_insert) + " as allocation_rate"
end if

sqls = sqls + ", decode(sign(sum(amount)), -1, -1, 1) as dr_cr_id"
sqls = sqls + ", 1 as ledger_sign"
sqls = sqls + ", 0 as quantity"
sqls = sqls + ", sum(a.amount) as amount"
sqls = sqls + ", " + string(i_month_number) + " as month_number"
sqls = sqls + ", " + string(i_month_period) + " as month_period"
sqls = sqls + ", '" + i_gl_journal_category + "' as gl_journal_category"
sqls = sqls + ", " + string(2) + " as amount_type"
sqls = sqls + ", " + string(i_allocation_id) + " as allocation_id"
sqls = sqls + ", 'TARGET' as target_credit"

//  Not complete.
if i_interco_acctg = 1 then
	sqls = sqls + ', a."' + i_company_field + '" as cross_charge_company'
	gb_clause = gb_clause + ', a."' + i_company_field + '" '
else
	sqls = sqls + ", ' ' as cross_charge_company"
end if

sqls = sqls + ", sum(a.amount) as inception_to_date_amount"
sqls = sqls + ", " + string(a_source_id) + " as source_id"
sqls = sqls + ", '" + i_budget_version + "' as budget_version"

sqls = sqls + " from cr_budget_data a, cr_alloc_target_criteria b "

//sqls = sqls + i_source_sqls + " and b.target_id = " + string(i_target_id) + " "
sqls = sqls + i_prefixed_where + " and b.target_id = " + string(i_target_id) + " "

gb_clause = right(gb_clause, len(gb_clause) - 1)

sqls = sqls + "group by " + gb_clause

sqls = sqls + ") )" // extra paren for outer_sqls

if g_debug = 'YES' then
	f_pp_msgs("uf_fast_targets: sqls = " + sqls)
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: in uf_fast_targets: " + sqlca.SQLErrText)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

return 1
end function

public function longlong uf_fast_credits (decimal a_rate_for_insert, longlong a_source_id);longlong i
string sqls, element_descr, gb_clause, outer_sqls
decimal {10} rate_for_insert

rate_for_insert = a_rate_for_insert

sqls       = "insert into cr_budget_data " + '("ID'
outer_sqls = "select crbudgets.nextval"
gb_clause  = ""

for i = 1 to i_num_elements
	
	element_descr = upper(trim(i_ds_elements.GetItemString(i, "description")))
	element_descr = f_cr_clean_string(element_descr)
	sqls          = sqls + '","' + element_descr
	if i = 1 then
		outer_sqls    = outer_sqls + ',"' + element_descr
	else
		outer_sqls    = outer_sqls + '","' + element_descr
	end if
	if i_suspense_accounting = "YES" then
		//  Place the "orig" field right next to its ACK counterpart.
		sqls = sqls + '","ORIG_' + element_descr
		outer_sqls = outer_sqls + '","ORIG_' + element_descr
	end if
	
next

if i_rate_field_exists then
	sqls = sqls + '","ALLOCATION_RATE'
	outer_sqls = outer_sqls + '","ALLOCATION_RATE'
end if

sqls = sqls + '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
				  ',"MONTH_NUMBER","MONTH_PERIOD"' + &
				  ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
				  ',"ALLOCATION_ID","TARGET_CREDIT",' + &
				  '"CROSS_CHARGE_COMPANY","INCEPTION_TO_DATE_AMOUNT",' + &
				  '"SOURCE_ID","BUDGET_VERSION") ('

outer_sqls = outer_sqls + '","DR_CR_ID","LEDGER_SIGN","QUANTITY","AMOUNT"' + &
				  ',"MONTH_NUMBER","MONTH_PERIOD"' + &
				  ',"GL_JOURNAL_CATEGORY","AMOUNT_TYPE"' + &
				  ',"ALLOCATION_ID","TARGET_CREDIT",' + &
				  '"CROSS_CHARGE_COMPANY","INCEPTION_TO_DATE_AMOUNT",' + &
				  '"SOURCE_ID","BUDGET_VERSION" from '

sqls = sqls + outer_sqls + ' (select 0 as id'

for i = 1 to i_num_elements
	
	element_descr = upper(trim(i_ds_elements.GetItemString(i, "description")))
	element_descr = f_cr_clean_string(element_descr)
	
	sqls = sqls + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '") as "' + element_descr + '"'
	
	gb_clause = gb_clause + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '")'
	
	if i_suspense_accounting = "YES" then
		//  Place the "orig" field right next to its ACK counterpart.
		sqls = sqls + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '") as "ORIG_' + element_descr + '"'
		
		gb_clause = gb_clause + ', decode(b."' + element_descr + '", ' + &
								 "'*', " + &
								  'a."' + element_descr + '", ' + &
								  'b."' + element_descr + '")'
	end if
	
next

if i_rate_field_exists then
	sqls = sqls + ", " + string(rate_for_insert) + " as allocation_rate"
end if

sqls = sqls + ", decode(sign(-sum(amount)), -1, -1, 1) as dr_cr_id"
sqls = sqls + ", 1 as ledger_sign"
sqls = sqls + ", -sum(quantity) as quantity"
sqls = sqls + ", -sum(a.amount) as amount"
sqls = sqls + ", month_number as month_number"
sqls = sqls + ", month_period as month_period"
gb_clause = gb_clause + ", month_number "
gb_clause = gb_clause + ", month_period "
sqls = sqls + ", '" + i_gl_journal_category + "' as gl_journal_category"
sqls = sqls + ", " + string(2) + " as amount_type"
sqls = sqls + ", " + string(i_allocation_id) + " as allocation_id"
sqls = sqls + ", 'CREDIT' as target_credit"

//  Not complete.
if i_interco_acctg = 1 then
	sqls = sqls + ', a.cross_charge_company as cross_charge_company'
	gb_clause = gb_clause + ', a.cross_charge_company '
else
	sqls = sqls + ", ' ' as cross_charge_company"
end if

sqls = sqls + ", -sum(a.amount) as inception_to_date_amount"
sqls = sqls + ", " + string(a_source_id) + " as source_id"
sqls = sqls + ", a.budget_version as budget_version"
gb_clause = gb_clause + ", a.budget_version "

sqls = sqls + " from cr_budget_data a, cr_alloc_credit_criteria b "

//  WILL NEED TO BUILD THIS OUT SINCE THERE IS A HUGE IF-THEN-ELSE IN UF_LOADING !
sqls = sqls + &
	 "where drilldown_key is null " + &
	 "and budget_version = '" + i_budget_version + "' and " + &
	 "allocation_id = " + string(i_allocation_id) + " and " + &
	 "month_number = " + string(i_month_number)

sqls = sqls + " and b.credit_id = " + string(i_credit_id) + " "

gb_clause = right(gb_clause, len(gb_clause) - 1)

sqls = sqls + "group by " + gb_clause

sqls = sqls + ") )" // extra paren for outer_sqls

if g_debug = 'YES' then
	f_pp_msgs("uf_fast_credits: sqls = " + sqls)
end if

execute immediate :sqls;

if sqlca.SQLCode < 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: in uf_fast_credits: " + sqlca.SQLErrText)
	f_pp_msgs("sqls = " + sqls)
	f_pp_msgs("  ")
	rollback;
	return -1
end if

return 1
end function

public function integer uf_read ();longlong rtn, interface_id, source_id, cmd_line_arg_elements, cv_long, running_session_id, &
			session_id, locked, mod, status
string   filename, s_date, cmd_line_arg[], to_user, sqls, path, str, temp_path, exe_name, &
			exe_version, cntrl_val, db_version, str_rtn
date     ddate
time     ttime
datetime started_at, finished_at, file_created, file_written
environment env

w_custom_datawindows w
//w = CREATE w_custom_datawindows
//DESTROY w

//*****************************************************************************************
//
//   DEBUG
//
//*****************************************************************************************
select control_value into :cv_long from cr_alloc_system_control
 where upper(control_name) = 'DEBUG ACTUALS';
if cv_long = 1 then
	g_debug = 'YES'
	f_pp_msgs( "DEBUG MODE")
else
	g_debug = 'NO'
end if

/*### - MDZ - 9043 - 20120209*/
setnull(i_concurrent_processing)
select upper(trim(control_value)) into :i_concurrent_processing
  from cr_system_control
 where upper(trim(control_name)) = 'ALLOCATIONSB: CONCURRENT PROCESSING';
if isnull(i_concurrent_processing) or i_concurrent_processing = "" then
	i_concurrent_processing = "NO"
end if

/*Moved from uf_run*/
setnull(i_suspense_accounting)
select upper(trim(control_value)) into :i_suspense_accounting
  from cr_system_control
 where upper(trim(control_name)) = 'ALLOCATIONS-B - SUSPENSE ACCOUNTING';
if isnull(i_suspense_accounting) or i_suspense_accounting = "" then
	i_suspense_accounting = "NO"
end if



select userenv('sessionid') into :i_session_id from dual;
if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
	// Will check the running session id at an allocation level
else
	//-------  To prevent this process from running  -------
	//-------  multiple times concurrently.          -------
	running_session_id = 0
	select running_session_id into :running_session_id from pp_processes
	 where process_id = :g_process_id;
	
	if isnull(running_session_id) or running_session_id = 0 then		
		update pp_processes set running_session_id = :i_session_id
		 where process_id = :g_process_id;
		
		if sqlca.SQLCode = 0 then
			commit;
		else
			f_pp_msgs(" ")
			f_pp_msgs('ERROR: updating pp_processes.running_session_id: ' + sqlca.SQLErrText)
			f_pp_msgs(" ")
			rollback;
			return -1
		end if
	else
		//  CURRENTLY RUNNING: disconnect and halt.
		f_pp_msgs( ' ')
		f_pp_msgs( ' ')
		f_pp_msgs( 'Another session is currently running this process.')
		f_pp_msgs( '  session_id = ' + string(running_session_id))
		f_pp_msgs( ' ')
		f_pp_msgs( ' ')
		rollback;
		return -1
	end if
end if 


//**********************************************************************************
//
//  Before processing the command line args, get this cr_system_control switch that
//  determines whether or not they are allowed to run a range of priorities.
//
//**********************************************************************************
i_allow_priority_range = "NO"
select upper(trim(control_value)) into :i_allow_priority_range from cr_system_control 
 where upper(trim(control_name)) = 'ALLOC - BDG - ALLOW PRIORITY RANGE';
if isnull(i_allow_priority_range) or i_allow_priority_range = "" then 
	i_allow_priority_range = "NO"
end if


//**********************************************************************************
//
//  The command line argument cannot be NULL ... 
//
//  It will be passed as a comma separated list in the following format:  
//
//  processing_priority,budget_version,test_or_actual
//
//  where test_or_actual is either TEST or ACTUAL.
//
//  The start_month and end_month variables come from a select against
//  cr_alloc_system_control.
//**********************************************************************************
if isnull(g_command_line_args) or g_command_line_args = "" then
	f_write_log(g_log_file, "The command line argument is NULL.  " + &
								   "Cannot run the Budget Allocations!")
	f_pp_msgs("The command line argument is NULL.  " + &
	          "Cannot run the Budget Allocations!")
	rtn = -1
	goto halt_the_app
end if

//  Translate the command line argument into the appropriate
//  variables.  If the 3 arguments are not passed, log an error.
cmd_line_arg_elements = &
	f_parsestringintostringarray(g_command_line_args, ",", cmd_line_arg)

if i_allow_priority_range = "YES" then
	
	if cmd_line_arg_elements < 4 then
		f_pp_msgs("The command line argument contained " + &
					  string(cmd_line_arg_elements) + " argument(s).  " + &
					 "It must have all 4 arguments that are required.  " + &
					 "Cannot run the Budget Allocations!")
		rtn = -1
		goto halt_the_app
	end if
	
	i_priority       = trim(cmd_line_arg[1])
	i_priority2      = trim(cmd_line_arg[2])
	i_budget_version = trim(cmd_line_arg[3])
	
	/*### - MDZ - 9058 - 20120119*/
	/*Set to upper on the way in.*/
//	i_test_or_actual = trim(cmd_line_arg[4])
	i_test_or_actual = upper(trim(cmd_line_arg[4]))
	
else
	
	if cmd_line_arg_elements < 3 then
		f_pp_msgs("The command line argument contained " + &
					  string(cmd_line_arg_elements) + " argument(s).  " + &
					 "It must have all 3 arguments that are required.  " + &
					 "Cannot run the Budget Allocations!")
		rtn = -1
		goto halt_the_app
	end if
	
	
	i_priority       = trim(cmd_line_arg[1])
	i_budget_version = trim(cmd_line_arg[2])
	
	/*### - MDZ - 9058 - 20120119*/
	/*Set to upper on the way in.*/
//	i_test_or_actual = trim(cmd_line_arg[3])
	i_test_or_actual = upper(trim(cmd_line_arg[3]))
	
end if

//
//	 Make sure that the budget_version is not locked in the CR or PowerPlant
//
setnull(locked)
i_actuals_month = 0
i_start_year    = 0

select locked, actuals_month, start_year into :locked, :i_actuals_month, :i_start_year
  from budget_version
 where lower(description) = lower(:i_budget_version);

if isnull(i_actuals_month) then i_actuals_month = 0
if isnull(i_start_year)    then i_start_year    = 0

if locked = 1 then
	f_pp_msgs("  ")
	f_pp_msgs("The budget version (" + i_budget_version + ") is locked for entry.  " + &
				 "Cannot run the Budget Allocations!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

setnull(locked)
select locked into :locked
  from cr_budget_version
 where lower(budget_version) = lower(:i_budget_version);

if locked = 1 then
	f_pp_msgs("  ")
	f_pp_msgs("The budget version (" + i_budget_version + ") is locked for entry.  " + &
				 "Cannot run the Budget Allocations!")
	f_pp_msgs("  ")
	rtn = -1
	goto halt_the_app
end if

//
//  Get the start and end month from cr_alloc_system_control ...
//
if i_concurrent_processing = "YES" then
	//
	//  With concurrent processing, we have to feed the start and end month in as part of
	//  cmd_line_arg_elements.
	//
	if i_allow_priority_range = "YES" then
		i_start_month = long(trim(cmd_line_arg[5]))
		i_end_month   = long(trim(cmd_line_arg[6]))
	else
		i_start_month = long(trim(cmd_line_arg[4]))
		i_end_month   = long(trim(cmd_line_arg[5]))
	end if
else
	if upper(i_priority) = "AFUDC" then
		setnull(i_start_month)
		select rtrim(control_value) into :i_start_month from cr_alloc_system_control
		 where upper(rtrim(control_name)) = 'CR BUDGET AFUDC START MONTH';
		if isnull(i_start_month) then i_start_month = 0
		
		if i_start_month = 0 then
			f_pp_msgs("The argument: " + string(i_start_month) + " for the " + &
				"CR BUDGET AFUDC START MONTH is invalid. Check the " + &
				"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
				"Connot run the CR Budget AFUDC!")
			rtn = -1
			goto halt_the_app
		end if
	
		setnull(i_end_month)
		select rtrim(control_value) into :i_end_month from cr_alloc_system_control
		 where upper(rtrim(control_name)) = 'CR BUDGET AFUDC ENDING MONTH';
		if isnull(i_end_month) then i_end_month = 0
		
		if i_end_month = 0 then
			f_pp_msgs("The argument: "+ string(i_end_month) + " for the " + &
				"CR BUDGET AFUDC ENDING MONTH is invalid. Check the " + &
				"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
				"Connot run the CR Budget AFUDC!")
			rtn = -1
			goto halt_the_app
		end if
	else
		setnull(i_start_month)
		select rtrim(control_value) into :i_start_month from cr_alloc_system_control
		 where upper(rtrim(control_name)) = 'CR BUDGET ALLOCATION START MONTH';
		if isnull(i_start_month) then i_start_month = 0
		
		if i_start_month = 0 then
			f_pp_msgs("The argument: " + string(i_start_month) + " for the " + &
				"CR BUDGET ALLOCATION START MONTH is invalid. Check the " + &
				"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
				"Connot run the CR Budget Allocations!")
			rtn = -1
			goto halt_the_app
		end if
	
		setnull(i_end_month)
		select rtrim(control_value) into :i_end_month from cr_alloc_system_control
		 where upper(rtrim(control_name)) = 'CR BUDGET ALLOCATION ENDING MONTH';
		if isnull(i_end_month) then i_end_month = 0
		
		if i_end_month = 0 then
			f_pp_msgs("The argument: "+ string(i_end_month) + " for the " + &
				"CR BUDGET ALLOCATION ENDING MONTH is invalid. Check the " + &
				"CR_ALLOC_SYSTEM_CONTROL table.  It should be a date with a YYYYMM format.  " + &
				"Connot run the CR Budget Allocations!")
			rtn = -1
			goto halt_the_app
		end if
	end if
end if


update pp_processes_occurrences 
   set batch_id = :i_priority
 where process_id = :g_process_id and occurrence_id = :g_occurrence_id
 using g_sqlca_logs;
	
if g_sqlca_logs.SQLCode = 0 then
	commit using g_sqlca_logs;
else
	rollback using g_sqlca_logs;
end if


// JAK: Dec 17, 2007:  Change all datastores to instances and remove the destroys
//								Will do all of the creates here
i_ds_cr_alloc_months 	= CREATE uo_ds_top
i_ds_where_clause 		= CREATE uo_ds_top
i_ds_balance 				= CREATE uo_ds_top
i_ds_1 						= CREATE uo_ds_top
i_ds_2 						= CREATE uo_ds_top
i_ds_credit_co 			= CREATE uo_ds_top
i_ds_source 				= CREATE uo_ds_top
i_ds_credit 				= CREATE uo_ds_top
i_ds_credit_insert 		= CREATE uo_ds_top
i_uo_cleanup_bdg			= CREATE uo_cr_allocations_cleanup_bdg
i_ds_target 				= CREATE uo_ds_top
i_ds_interco 				= CREATE uo_ds_top
i_ds_interco_insert 		= CREATE uo_ds_top
i_ds_group_by 				= CREATE uo_ds_top
i_ds_alloc_list 			= CREATE uo_ds_top
i_ds_company 				= CREATE uo_ds_top
i_ds_target_val			= CREATE uo_ds_top
i_ds_target_trpo        = CREATE uo_ds_top
i_ds_credit_trpo        = CREATE uo_ds_top

// DS ELEMENTS -- Will do the retrieval here as well since it is used over and over
i_ds_elements = CREATE uo_ds_top
// ### - SEK - 7090 - 031811: Create the ds dynamically instead
//i_ds_elements.DataObject = "dw_cr_element_definitions"
//i_ds_elements.SetTransObject(sqlca)
sqls = 'select * from cr_elements order by "ORDER"'
f_create_dynamic_ds(i_ds_elements,"grid",sqls,sqlca,true)
i_num_elements = i_ds_elements.RETRIEVE()


//----------
sqls = "select cr_alloc_group_by_bdg.element_id, cr_elements.budgeting_element, cr_alloc_group_by_bdg.mask_length, cr_elements.width " + &
		 "from cr_alloc_group_by_bdg , cr_elements " + &
		 "where group_by_id = -678 and " + &
		 "cr_alloc_group_by_bdg.element_id = cr_elements.element_id"

f_create_dynamic_ds(i_ds_group_by, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = -678 " + &
		  " order by row_id"

f_create_dynamic_ds(i_ds_where_clause, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_target_criteria_bdg where target_id = -678"

f_create_dynamic_ds(i_ds_target, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_credit_criteria_bdg where credit_id = -678"

f_create_dynamic_ds(i_ds_credit, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_target_criteria_t_bdg where target_id = -678"

f_create_dynamic_ds(i_ds_target_trpo, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_credit_criteria_t_bdg where credit_id = -678"

f_create_dynamic_ds(i_ds_credit_trpo, "grid", sqls, sqlca, true)
//----------


i_uo_cr_derivation = CREATE uo_cr_derivation

i_cr_deriver_type_sources = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources"
f_create_dynamic_ds(i_cr_deriver_type_sources, "grid", sqls, sqlca, true)

i_ds_oob = CREATE uo_ds_top
sqls = "select count(*) from cr_budget_data where id = -678678 and budget_version = 'asdf'"
f_create_dynamic_ds(i_ds_oob, "grid", sqls, sqlca, true)

i_cr_deriver_type_sources_alloc = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources_allocb"
f_create_dynamic_ds(i_cr_deriver_type_sources_alloc, "grid", sqls, sqlca, true)

i_cr_deriver_override2 = CREATE uo_ds_top
sqls = "select * from cr_deriver_override2_sources"
f_create_dynamic_ds(i_cr_deriver_override2, "grid", sqls, sqlca, true)

i_cr_deriver_override2_alloc = CREATE uo_ds_top
sqls = "select * from cr_deriver_or2_sources_allocb"
f_create_dynamic_ds(i_cr_deriver_override2_alloc, "grid", sqls, sqlca, true)
//----------
sqls = "select cr_alloc_group_by_bdg.element_id, cr_elements.budgeting_element, cr_alloc_group_by_bdg.mask_length, cr_elements.width " + &
		 "from cr_alloc_group_by_bdg , cr_elements " + &
		 "where group_by_id = -678 and " + &
		 "cr_alloc_group_by_bdg.element_id = cr_elements.element_id"

f_create_dynamic_ds(i_ds_group_by, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_where_clause_bdg where where_clause_id = -678 " + &
		  " order by row_id"

f_create_dynamic_ds(i_ds_where_clause, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_target_criteria_bdg where target_id = -678"

f_create_dynamic_ds(i_ds_target, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_credit_criteria_bdg where credit_id = -678"

f_create_dynamic_ds(i_ds_credit, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_target_criteria_t_bdg where target_id = -678"

f_create_dynamic_ds(i_ds_target_trpo, "grid", sqls, sqlca, true)

sqls = "select * from cr_alloc_credit_criteria_t_bdg where credit_id = -678"

f_create_dynamic_ds(i_ds_credit_trpo, "grid", sqls, sqlca, true)
//----------


i_uo_cr_derivation = CREATE uo_cr_derivation

i_cr_deriver_type_sources = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources"
f_create_dynamic_ds(i_cr_deriver_type_sources, "grid", sqls, sqlca, true)

i_ds_oob = CREATE uo_ds_top
sqls = "select count(*) from cr_budget_data where id = -678678 and budget_version = 'asdf'"
f_create_dynamic_ds(i_ds_oob, "grid", sqls, sqlca, true)

i_cr_deriver_type_sources_alloc = CREATE uo_ds_top
sqls = "select * from cr_deriver_type_sources_allocb"
f_create_dynamic_ds(i_cr_deriver_type_sources_alloc, "grid", sqls, sqlca, true)

i_cr_deriver_override2 = CREATE uo_ds_top
sqls = "select * from cr_deriver_override2_sources"
f_create_dynamic_ds(i_cr_deriver_override2, "grid", sqls, sqlca, true)

i_cr_deriver_override2_alloc = CREATE uo_ds_top
sqls = "select * from cr_deriver_or2_sources_allocb"
f_create_dynamic_ds(i_cr_deriver_override2_alloc, "grid", sqls, sqlca, true)

//*****************************************************************************************
//
//  Get the Structures Variable:  New variable to determine which structure values
//                                table is being used.
//
//*****************************************************************************************
setnull(i_use_new_structures_table)
select upper(rtrim(control_value)) into :i_use_new_structures_table from cr_system_control
 where upper(rtrim(control_name)) = 'USE NEW CR STRUCTURE VALUES TABLE';
if isnull(i_use_new_structures_table) then i_use_new_structures_table = "NO"


//*****************************************************************************************
//
//  Other CR System Control Lookups:  Company Field
//
//*****************************************************************************************
setnull(i_company_field)
select upper(control_value) into :i_company_field
  from cr_system_control 
 where upper(control_name) = 'COMPANY FIELD';

if isnull(i_company_field) or trim(i_company_field) = "" then
	f_pp_msgs("  ")
	f_pp_msgs("No COMPANY FIELD was found in cr_system_control ... " + &
				 "No allocation will be calculated.")
	f_pp_msgs("  ")
	rollback using sqlca;
	return -1
end if

i_company_field = f_cr_clean_string(trim(i_company_field))


//*****************************************************************************************
//
//  Other CR System Control Lookups:  Run by Dept - Clearing Calc Rate - Credits Method
//
//*****************************************************************************************
setnull(i_run_by_dept_credits_method)
select upper(trim(control_value)) into :i_run_by_dept_credits_method
  from cr_system_control 
 where upper(trim(control_name)) = 'RUN BY DEPT: CALCRATE: OBEY CREDITS';

if isnull(i_run_by_dept_credits_method) or trim(i_run_by_dept_credits_method) = "" then
	i_run_by_dept_credits_method = "NO"
end if


//*****************************************************************************************
//
//  Need for later ... for example in uf_run_by_department().
//
//*****************************************************************************************
i_ds_sum_amount = CREATE uo_ds_top
sqls = "select sum(amount) from cr_allocations where id = 0"
f_create_dynamic_ds(i_ds_sum_amount, "grid", sqls, sqlca, true)

i_ds_sum_amount2 = CREATE uo_ds_top
sqls = "select sum(amount), sum(quantity) from cr_allocations where id = 0"
f_create_dynamic_ds(i_ds_sum_amount2, "grid", sqls, sqlca, true)


//*****************************************************************************************
//
//  Get the Validations Variables:  System switches determine whether validations are 
// 											enabled or not.  The default is NO.
//
//*****************************************************************************************

//  Switches:
setnull(i_me_validations)
select upper(rtrim(control_value)) into :i_me_validations from cr_system_control 
 where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - ME';
if isnull(i_me_validations) then i_me_validations = "NO"

f_pp_msgs("Master Element Validation = " + i_me_validations)

setnull(i_combo_validations)
select upper(rtrim(control_value)) into :i_combo_validations from cr_system_control 
 where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - COMBO';
if isnull(i_combo_validations) then i_combo_validations = "NO"

f_pp_msgs("Combination Validation    = " + i_combo_validations)

//  N/A FOR BUDGET
//setnull(i_proj_based_validations)
//select upper(rtrim(control_value)) into :i_proj_based_validations from cr_system_control 
// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS - PROJECTS BASED';
//if isnull(i_proj_based_validations) then i_proj_based_validations = "NO"

//f_pp_msgs("Projects-Based Validation = " + i_proj_based_validations)
i_proj_based_validations = "NO"

//  N/A FOR BUDGET
//setnull(i_mn_validations)
//select upper(rtrim(control_value)) into :i_mn_validations from cr_system_control 
// where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - MONTH NUMBER';
//if isnull(i_mn_validations) then i_mn_validations = "NO"
//
//f_pp_msgs("Month Number Validation   = " + i_mn_validations)
i_mn_validations = "NO"

setnull(i_custom_validations)
select upper(rtrim(control_value)) into :i_custom_validations from cr_system_control 
 where upper(rtrim(control_name)) = 'ENABLE VALIDATIONS-BDG - CUSTOM';
if isnull(i_custom_validations) then i_custom_validations = "NO"

f_pp_msgs("Custom Validation         = " + i_custom_validations)


//*****************************************************************************************
//
//  RUN the Budget Allocations:
//
//*****************************************************************************************
i_suspense_processed = false
rtn = uf_run()


//*****************************************************************************************
//
//  HALT the application ...
//
//*****************************************************************************************
halt_the_app:

if rtn = 1 then
	f_pp_msgs("  ")
	if i_validation_kickouts and not i_suspense_processed then
		f_pp_msgs("  ")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"   -------------------------------------------------------------")
		f_pp_msgs("   *****  The budget allocations RAN and")
		f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST !")
		f_pp_msgs("   -----------------------------------------------------------------------" + &
			"   -------------------------------------------------------------")
		f_pp_msgs("  ")
	else
		f_pp_msgs("  ")
		f_pp_msgs("The budget allocations RAN SUCCESSFULLY.")
		f_pp_msgs("  ")
		if i_suspense_processed then
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"   -------------------------------------------------------------")
			f_pp_msgs("   *****  VALIDATION KICKOUTS EXIST!  " + &
				"SUSPENSE ACCOUNTS HAVE BEEN APPLIED AND THE ALLOCATIONS HAVE BEEN POSTED!  *****")
			f_pp_msgs("   -----------------------------------------------------------------------" + &
				"   -------------------------------------------------------------")
			f_pp_msgs("  ")
		end if
	end if
	f_pp_msgs("  ")
	g_rtn_code = 0
else
	g_rtn_code = rtn
	
	//  Mail Logic:
	if upper(i_test_or_actual) <> 'TEST' then
		
		// Find the user (from cr_system_control) to send a failure e-mail to.
		setnull(to_user)
		select trim(control_value) into :to_user from cr_system_control
		 where upper(control_name) = 'ALLOCATIONS ERROR EMAIL RECIPIENT';
		if isnull(to_user) or to_user = "" then to_user = "None"
		
		if upper(to_user) <> "NONE" then
			
			//  Set the correct win API variable.
			getenvironment(env) 
			choose case env.OSType
				case Windows!  
					if env.win16 = true then 
						uo_winapi = create u_external_function_winapi
					else
						uo_winapi = create u_external_function_win32
					end if	
				case WindowsNT!
					if env.win16 = true then 
						uo_winapi = create u_external_function_winapi
					else	 
						uo_winapi = create u_external_function_win32
					end if
			end choose
			
			f_pp_msgs("Sending error email to user: " + to_user)
			g_msmail = create uo_smtpmail
			f_send_mail('pwrplant','BUDGET_ALLOCATIONS.EXE: TERMINATED WITHOUT COMPLETING', &
				'The budget_allocations encountered an error or validation kickouts and terminated ' + &
				'without completing.  See the PowerPlant Online Logs for detailed information.',to_user)
		
			destroy g_msmail
		
		end if  //  if upper(to_user) <> "NONE" then ...
		
	end if  //  if upper(i_test_or_actual) <> 'TEST' then ...
	
end if

//*****************************************************************************************
//
//  If an error occurred, the cr_temp_cr table might have records in it that should
//  be deleted.
//
//*****************************************************************************************
delete from cr_temp_cr_gl_bdg using sqlca;
if sqlca.SQLCode = 0 then
	commit using sqlca;
else
	rollback using sqlca;
end if

return rtn
end function

public function longlong uf_unlock (longlong a_allocation_id);//  The allocation just ran.  A commit or rollback just fired depending on the condition.
//  It should be safe here to free up the running_session_id for this allocation if
//  suspense is set to yes.  THIS IS IN A DIFFERENT PLACE FOR BUDGET ALLOCATIONS SINCE
//  THE UPDATE ABOVE IS OUTSIDE THE MONTH_NUMBER LOOP !!!
update cr_allocation_control_bdg set running_session_id = NULL
 where allocation_id = :a_allocation_id;

if sqlca.SQLCode <> 0 then
	f_pp_msgs("  ")
	f_pp_msgs("ERROR: updating cr_allocation_control_bdg.running_session_id to NULL: " + &
		string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
	f_pp_msgs("  ")
	rollback;
	return -1
else
	commit;
end if

return 1
end function

public function longlong uf_budget_afudc_defaults ();//*****************************************************************************************
//
//  Window Function  :  wf_budget_afudc_defaults
//
//  Description      :  Default the afudc_type_id based on either company or 
//								work_order_type.  Used for those clients that do not have 
//								company_id on their FP work order types.
//
//  Notes  				:  Commits and rollbacks are handled by the caller.
//
//*****************************************************************************************
longlong i, num_rows, co_id, afudc_type_id, wot_id
string sqls
decimal {8} afudc_xfer_rate


uo_ds_top ds_defaults
ds_defaults = CREATE uo_ds_top


//*****************************************************************************************
//
//  Records with a company_id.  No work_order_type_id
//
//  12/23/02  :  Added code for the afudc_transfer_rate by company
//
//*****************************************************************************************
sqls = "select * from budget_afudc_defaults " + &
		  "where company_id is not null and work_order_type_id is null"

f_create_dynamic_ds(ds_defaults, "grid", sqls, sqlca, true)

num_rows = ds_defaults.RowCount()

for i = 1 to num_rows
	
	
	setnull(afudc_type_id)
	setnull(afudc_xfer_rate)
	
	co_id           =  ds_defaults.GetItemNumber(i, "company_id")
	afudc_type_id   =  ds_defaults.GetItemNumber(i, "afudc_type_id")
	afudc_xfer_rate = ds_defaults.GetItemDecimal(i, "afudc_transfer_rate")
	
	
	if isnull(afudc_type_id) or afudc_type_id = 0 then
		//  Do nothing.
	else
		
		f_pp_msgs(" -- Updating budget.afudc_type_id (company = " + &
			string(co_id) + ") at " + string(now()))
		
		update budget set afudc_type_id = :afudc_type_id
		 where company_id = :co_id;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating budget.afudc_type_id (company section): " + &
				sqlca.SQLErrText)
			rollback;
			DESTROY ds_defaults
			return -1
		end if
		
	end if
	
	
	if isnull(afudc_xfer_rate) or afudc_xfer_rate = 0 then
		//  Do nothing.
	else
		
		f_pp_msgs(" -- Updating cr_budget_afudc_calc.afudc_transfer_rate (company = " + &
			string(co_id) + ") at " + string(now()))
		
		update cr_budget_afudc_calc a set afudc_transfer_rate = (

			select :afudc_xfer_rate from budget b
			 where a.budget_id = b.budget_id
				and b.company_id = :co_id)
		
		where budget_version = :i_budget_version
		  and budget_id = 
					(select budget_id from budget 
					  where a.budget_id = budget.budget_id and budget.company_id = :co_id);
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating cr_budget_afudc_calc.afudc_transfer_rate (company section): " + &
				sqlca.SQLErrText)
			rollback;
			DESTROY ds_defaults
			return -1
		end if
		
	end if
		
next


//*****************************************************************************************
//
//  Records with no company_id and have a work_order_type_id:
//
//  12/23/02  :  I am purposely avoiding pushing the afudc_transfer_rate down to the 
//					  work_order_type level due to performance.  We will see if we can get away
//					  with the functionality by company only.
//
//  01/13/03  :  XCEL needed it to be by work order type.
//
//*****************************************************************************************
sqls = "select * from budget_afudc_defaults " + &
		  "where company_id is null and work_order_type_id is not null"

f_create_dynamic_ds(ds_defaults, "grid", sqls, sqlca, true)

num_rows = ds_defaults.RowCount()

for i = 1 to num_rows
		
	setnull(afudc_type_id)
	setnull(afudc_xfer_rate)
		
	wot_id          = ds_defaults.GetItemNumber(i, "work_order_type_id")
	afudc_type_id   = ds_defaults.GetItemNumber(i, "afudc_type_id")
	afudc_xfer_rate = ds_defaults.GetItemDecimal(i, "afudc_transfer_rate")
	
	
	if isnull(afudc_type_id) or afudc_type_id = 0 then
		//  Do nothing.
	else
		
		f_pp_msgs(" -- Updating budget.afudc_type_id (wot = " + &
			string(wot_id) + ") at " + string(now()))
			
		update budget set afudc_type_id = :afudc_type_id
		 where budget_id in (
			select budget_id from work_order_control
			 where funding_wo_indicator = 1 and work_order_type_id = :wot_id);
	
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating budget.afudc_type_id (work_order_type section): " + &
				sqlca.SQLErrText)
			rollback;
			DESTROY ds_defaults
			return -1
		end if
		
	end if
	
	
	if isnull(afudc_xfer_rate) or afudc_xfer_rate = 0 then
		//  Do nothing.
	else
		
		f_pp_msgs(" -- Updating cr_budget_afudc_calc.afudc_transfer_rate (wot = " + &
			string(wot_id) + ") at " + string(now()))
				
		update cr_budget_afudc_calc a set afudc_transfer_rate = :afudc_xfer_rate 
		 where budget_version = :i_budget_version
		   and budget_id in
				(select budget_id from work_order_control
				  where funding_wo_indicator = 1 and work_order_type_id = :wot_id);
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating cr_budget_afudc_calc.afudc_transfer_rate (company section): " + &
				sqlca.SQLErrText)
			rollback;
			DESTROY ds_defaults
			return -1
		end if
		
	end if	
	
next



//*****************************************************************************************
//
//  Records with both a company_id and a work_order_type_id:
//
//*****************************************************************************************
sqls = "select * from budget_afudc_defaults " + &
		  "where company_id is not null and work_order_type_id is not null"

f_create_dynamic_ds(ds_defaults, "grid", sqls, sqlca, true)

num_rows = ds_defaults.RowCount()

for i = 1 to num_rows
	
	setnull(afudc_type_id)
	setnull(afudc_xfer_rate)
	
	co_id           = ds_defaults.GetItemNumber(i, "company_id")
	wot_id          = ds_defaults.GetItemNumber(i, "work_order_type_id")
	afudc_type_id   = ds_defaults.GetItemNumber(i, "afudc_type_id")
	afudc_xfer_rate = ds_defaults.GetItemDecimal(i, "afudc_transfer_rate")
	
	
	if isnull(afudc_type_id) or afudc_type_id = 0 then
		//  Do nothing.
	else
		
		f_pp_msgs(" -- Updating budget.afudc_type_id (co = " + string(co_id) + ", wot = " + &
			string(wot_id) + ") at " + string(now()))
			
		update budget set afudc_type_id = :afudc_type_id
		 where budget_id in (
			select budget_id from work_order_control
			 where funding_wo_indicator = 1 
				and company_id = :co_id and work_order_type_id = :wot_id);
				
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating budget.afudc_type_id (company / work_order_type section): " + &
				sqlca.SQLErrText)
			rollback;
			DESTROY ds_defaults
			return -1
		end if
				
	end if
	
	
	if isnull(afudc_xfer_rate) or afudc_xfer_rate = 0 then
		//  Do nothing.
	else
		
		f_pp_msgs(" -- Updating budget.afudc_transfer_rate (co = " + string(co_id) + ", wot = " + &
			string(wot_id) + ") at " + string(now()))
			
		update cr_budget_afudc_calc set afudc_transfer_rate = :afudc_xfer_rate
		 where budget_version = :i_budget_version
		 	and budget_id in (
				select budget_id from work_order_control
				 where funding_wo_indicator = 1 
					and company_id = :co_id and work_order_type_id = :wot_id);
				
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating budget.afudc_transfer_rate (company / work_order_type section): " + &
				sqlca.SQLErrText)
			rollback;
			DESTROY ds_defaults
			return -1
		end if
				
	end if
	
	
next


DESTROY ds_defaults

return 1
end function

public function longlong uf_run ();//******************************************************************************************
//
//  Window Function  :  cb_run
//
//  Description      :  Runs a batch of budget allocations based on input parameters.
//
//******************************************************************************************
boolean  run_as_a_test
longlong num_rows, i, allocation_id, counter, ii, rtn, iii, wc_mn, beg_bal_mo, b_a_d_counter, &
		   actuals_mn, num_types, audit_type, audit_offset_type, sess
string   sqls, s_date, descr, run_as, cv, the_type, insert_or_update, offset_type, &
			table_name, rtn_f_create_str
datetime started_at
date     ddate
time     ttime


i_validation_kickouts = false

actuals_mn = i_start_year * 100 + i_actuals_month


if upper(i_test_or_actual) = "TEST" then
	run_as_a_test = true
	run_as        = "TEST"
else
	run_as_a_test = false
	run_as        = "ACTUAL"
end if


if run_as_a_test then
	if i_allow_priority_range = "YES" then
		f_pp_msgs("  ")
		f_pp_msgs("THIS IS A TEST RUN.  RESULTS WILL BE WRITTEN TO CR_BUDGET_DATA_TEST.")
		f_pp_msgs("  ")
		f_pp_msgs("Running BUDGET ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "PRIORITY = " + i_priority + " through " + i_priority2 + ", ")
		f_pp_msgs("  ")
	else
		f_pp_msgs("  ")
		f_pp_msgs("THIS IS A TEST RUN.  RESULTS WILL BE WRITTEN TO CR_BUDGET_DATA_TEST.")
		f_pp_msgs("  ")
		f_pp_msgs("Running BUDGET ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "PRIORITY = " + i_priority + ", ")
		f_pp_msgs("  ")
	end if
else
	if i_allow_priority_range = "YES" then
		f_pp_msgs("  ")
		f_pp_msgs("Running BUDGET ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "PRIORITY = " + i_priority + " through " + i_priority2 + ", ")
		f_pp_msgs("  ")
	else
		f_pp_msgs("  ")
		f_pp_msgs("Running BUDGET ALLOCATIONS for " + &
					 "MONTH NUMBER = " + string(i_start_month) + " through " + &
					 "MONTH NUMBER = " + string(i_end_month) + ", " + &
					 "PRIORITY = " + i_priority + ", ")
		f_pp_msgs("  ")
	end if
end if


//
//  SHOULD NOT HAVE TO DO THIS FOR THE STAND-ALONE EXE
//  IT WILL BE RUN WITH THE PWRPLANT ID.  IF THEY DO NOT LET US HAVE
//  THE PWRPLANT ID, CHECK WITH ROGER.
//
////  Must change the role so users can insert into the pp_processes tables ...
//ini_file = ProfileString("win.ini", "Powerplan", "ini_file", "xpwrplant.ini")
//database = "DataBase1"  
//dev_role = ProfileString(ini_file, database, "Developer_Role", "")
//if dev_role = "" then
//   dev_role = 'pwrplant_role_dev'
//end if
//cmd = "begin dbms_session.set_role(~'" + dev_role + " identified by Tr9uI22k~'); end;"
//execute immediate :cmd using g_sqlca_logs;

/*### - MDZ - 9043 - 20120209*/
/*Moved to uf_read*/
//setnull(i_suspense_accounting)
//select upper(trim(control_value)) into :i_suspense_accounting
//  from cr_system_control
// where upper(trim(control_name)) = 'ALLOCATIONS-B - SUSPENSE ACCOUNTING';
//if isnull(i_suspense_accounting) or i_suspense_accounting = "" then
//	i_suspense_accounting = "NO"
//end if


uo_ds_top ds_alloc_list
ds_alloc_list = CREATE uo_ds_top

if i_allow_priority_range = "YES" then
	if  (isnull(i_priority)  or i_priority  = "0") &
	and (isnull(i_priority2) or i_priority2 = "0") then
		//  Not running with a user entered priority ...
		sqls = "select * from cr_allocation_control_bdg order by description"
	else
		//  THEY ARE running with a user entered priority ...
		if upper(i_priority) = "AFUDC" then
			sqls = "select * from cr_allocation_control_bdg where processing_priority = 0" + &
					 " order by description"
		else
			sqls = "select * from cr_allocation_control_bdg where processing_priority between " + &
					  i_priority + " and " + i_priority2 + " order by processing_priority,description"
		end if
	end if
else
	if isnull(i_priority) or i_priority = "0" then
		//  Not running with a user entered priority ...
		sqls = "select * from cr_allocation_control_bdg order by description"
	else
		//  THEY ARE running with a user entered priority ...
		if upper(i_priority) = "AFUDC" then
			sqls = "select * from cr_allocation_control_bdg where processing_priority = 0" + &
					 " order by description"
		else
			sqls = "select * from cr_allocation_control_bdg where processing_priority = " + &
					  i_priority + " order by description"
		end if
	end if
end if

rtn_f_create_str = f_create_dynamic_ds(ds_alloc_list, "grid", sqls, sqlca, true)

if rtn_f_create_str <> "OK" then
	f_pp_msgs("ERROR: f_create_dynamic_ds error: " + rtn_f_create_str)
	DESTROY ds_alloc_list
	return -1
end if

ds_alloc_list.SetSort("processing_priority a, description a")
ds_alloc_list.Sort()

num_rows = ds_alloc_list.RowCount()


//  Run derivation audits up-front so we don't slow down each allocation.
uo_ds_top ds_deriver_audits
ds_deriver_audits = CREATE uo_ds_top
sqls = &
	'select distinct "TYPE", insert_or_update, offset_type, audit_type, audit_offset_type ' + &
	  "from cr_deriver_type_sources where source_id in ( " + &
		"select source_id from cr_sources " + &
		 "where upper(trim(description)) in ('ALLOCATIONS','ALLOCATIONS-TEST','INTER-COMPANY','INTER-COMPANY-TEST'))"
f_create_dynamic_ds(ds_deriver_audits, "grid", sqls, sqlca, true)
num_types = ds_deriver_audits.RowCount()

uo_cr_derivation uo_deriver
uo_deriver = CREATE uo_cr_derivation

i_uo_cr_validation_bdg = CREATE uo_cr_validation_bdg

for i = 1 to num_types
	
	the_type         = ds_deriver_audits.GetItemString(i, 1)
	insert_or_update = ds_deriver_audits.GetItemString(i, 2)
	offset_type      = ds_deriver_audits.GetItemString(i, 3)
	audit_type       = ds_deriver_audits.GetItemNumber(i, 4)
	audit_offset_type= ds_deriver_audits.GetItemNumber(i, 5)
	if isnull(audit_type)        then audit_type        = 0
	if isnull(audit_offset_type) then audit_offset_type = 0
	
	if audit_type <> 1 then goto audit_offsets
	
	//  Currently not auditing "Single Record For Default Type" since we have no default
	//  logic in cr_deriver_type_sources.
	
	if upper(insert_or_update) = "INSERT" then  //  Run "Disallow Negative Percents" ...
		if g_debug = 'YES' then f_pp_msgs("-- Running derivation audit: Disallow Negative Percents, for type: " + the_type + "")
		rtn = uo_deriver.uf_deriver_audits(the_type, "Disallow Negative Percents")
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("FAILED AUDIT: There are negative percentages in cr_deriver_control for " + &
				"derivation type: " + the_type + ".")
			f_pp_msgs("The allocations cannot run in this state.")
			f_pp_msgs("  ")
			return -1
		end if
	end if  //  Run "Disallow Negative Percents" ...
	
	audit_offsets:
	
	if audit_offset_type <> 1 then goto next_type
	
	if not isnull(offset_type) and trim(offset_type) <> "" then  //  Run "Single Record For Offsets" ...
		if g_debug = 'YES' then f_pp_msgs("-- Running derivation audit: Single Record For Offsets, for type: " + offset_type + "")
		rtn = uo_deriver.uf_deriver_audits(offset_type, "Single Record For Offsets")
		if rtn <> 1 then
			f_pp_msgs("  ")
			f_pp_msgs("FAILED AUDIT: There are " + string(uo_deriver.i_counter) + " records in " + &
				"cr_deriver_control for the default targets (TYPE = '" + offset_type + "')")
			f_pp_msgs("There must be one and only one of these records.")
			f_pp_msgs("The allocations cannot run in this state.")
			f_pp_msgs("  ")
			return -1
		end if
	end if  //  Run "Single Record For Offsets" ...
	
	next_type:
next
DESTROY uo_cr_derivation
//  End of derivation audits.


//  Set some global variables.
//  -- i_rate_field_exists: simply check cr_budget_data and assume everything else is
//     config'ed properly.  Need to consider performance.
counter = 0
select count(*) into :counter from all_tab_columns 
 where owner = 'PWRPLANT' and table_name = 'CR_BUDGET_DATA' and column_name = 'ALLOCATION_RATE';
if isnull(counter) or counter = 0 then
	i_rate_field_exists = false
else
	i_rate_field_exists = true
end if


counter = 0
select count(*) into :counter from all_tab_columns
 where owner = 'PWRPLANT' and table_name = 'CR_BUDGET_DATA' and column_name = 'INTERFACE_BATCH_ID';
if isnull(counter) or counter = 0 then
	i_ifb_field_exists = false
else
	i_ifb_field_exists = true
end if



//  2/21/06:  NStar needed a custom function to help with elims.
rtn = f_cr_allocations_custom_bdg("wf_run_before_loop") 
if rtn < 0 then 
	DESTROY ds_alloc_list 
	return -1 
end if 


//uo_client_interface uo_alloc
//uo_alloc = CREATE uo_client_interface


f_pp_msgs("*******************************************")


if upper(i_priority) = "AFUDC" then
	
	//  The process to load cr_budget_afudc_calc needs to have already run.  This runs
	//  from the capital budget version maintenance window (or from an interface).
	
	//  Global updates before entering the calc:
	//    1.  Check for duplicate budget_ids in the table.  There can be no dupls, or
	//        the beginning balances will not initialize properly.
	//	   2.  Synch up the afudc_type_id with the entries in budget_afudc_defaults.
	//        Quite often, the funding project types will not be by company for FP's, thus
	//			 disabling our ability to update the afudc_type_id properly on the FP header.
	//    3.  Zero out any closings that might have been calculated in a previous run.
	//        The closings logic requires zeros to be in this column.
	//    4.  Zero out the total_amount_spent and update it from acutals.
	//			 DO NOT NEED to fill in this field in f_budget_to_cr_budget_data.
	//    5.  Update any blanket's in_service_date with i_start_month.  This will
	//        allow us to use the ISD / closing lag logic and we should not have to
	//        write separate code for blankets.
	//		6.  Update the percent_close for any budget_ids that have a closing_pattern_id.
	//			 The closing_pattern_id is filled in by f_budget_to_cr_budget_data AND SHOULD
	//		    BE LEFT NULL FOR ANY BUDGETS THAT ARE NOT USING A PATTERN !!!  PATTERNS
	//			 OVERRIDE ANY IN_SERVICE_DATES.
	
	i_afudc_calc_on_negative_base = "NO"
	select control_value into :i_afudc_calc_on_negative_base
	  from cr_system_control 
	 where upper(rtrim(control_name)) = 'BDG AFUDC - CALC ON NEGATIVE BASE';
	
	if isnull(i_afudc_calc_on_negative_base) or trim(i_afudc_calc_on_negative_base) = "" then
		i_afudc_calc_on_negative_base = "NO"
	end if


	f_pp_msgs("Checking for duplicate budgets at " + string(now()))
	
	counter = 0
	select count(*) into :counter from 
		(select budget_id,count(*) from cr_budget_afudc_calc 
		  where budget_version = :i_budget_version and month_number = :i_start_month
		  group by budget_id having count(*) > 1);
	if isnull(counter) then counter = 0
	
	if counter > 0 then
		f_pp_msgs("ERROR: cannot update the beginning balances with actuals.  " + &
			"There are duplicate budget_id records for month_number = " + &
			string(i_start_month))
		return -1
	end if
	

	f_pp_msgs("Zeroing out closings from any prior run at " + string(now()))
	
	//  Note the decodes below that have been added so any actual AFUDC or CPI will
	//  not get zeroed out.
	update cr_budget_afudc_calc 
	   set closings = 0, closings_tax = 0, total_closings = 0, total_closings_tax = 0,
			 total_amount_spent = 0, total_amount_spent_tax = 0,
			 beg_cwip = 0, beg_cwip_tax = 0, end_cwip = 0, end_cwip_tax = 0,
			 beg_afudc_base = 0, beg_cpi_base = 0,
			 uncompounded_afudc_balance = 0, uncompounded_cpi_balance = 0,
			 afudc_compounded_into_base = 0, cpi_compounded_into_base = 0,
			 afudc_debt   = decode(sign(month_number - :actuals_mn), 1, 0, afudc_debt),
			 afudc_equity = decode(sign(month_number - :actuals_mn), 1, 0, afudc_equity),
			 cpi          = decode(sign(month_number - :actuals_mn), 1, 0, cpi),
			 half_month_charges = 0,
			 half_month_charges_tax = 0, afudc_base_used_in_calc = 0,
			 cpi_base_used_in_calc = 0, end_afudc_base = 0, end_cpi_base = 0,
			 total_transferred_afudc_spent = 0, total_transferred_afudc_closed = 0,
			 afudc_transfer_rate = 0
	 where budget_version = :i_budget_version;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: in w_budget_allocations.wf_run: updating closings to 0: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	
	//  12/23/02:  This code used to be just above the prior update command.  It was moved
	//             because the zeroing out of the afudc_transfer_rate had to occur before
	//             this code!
	f_pp_msgs("Checking Budget AFUDC Default rates at " + string(now()))
	
	rtn = uf_budget_afudc_defaults()
	
	if rtn <> 1 then
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if

	//  This is only needed if wf_budget_afudc_defaults did something.
	b_a_d_counter = 0
	select count(*) into :b_a_d_counter from budget_afudc_defaults
	 where rownum = 1 and afudc_type_id is not null;
	if isnull(b_a_d_counter) then b_a_d_counter = 0
	
	if b_a_d_counter <> 0 then

		f_pp_msgs("Applying Budget AFUDC Type Id at " + string(now()))
		
		update cr_budget_afudc_calc a set afudc_type_id = (
			select afudc_type_id from budget b
			 where a.budget_id = b.budget_id)
		 where budget_version = :i_budget_version;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: in w_budget_allocations.wf_run: " + &
				"updating cr_budget_afudc_calc.afudc_type_id: " + sqlca.SQLErrText)
			rollback;
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			return -1
		else
			commit;
		end if
		
	end if  //  if b_a_d_counter <> 0 then ...
	
		
	//  Update the total_amount_spent:  this is the total dollars spent to date.
	//
	//  NOTE:  The percent_book_basis and percent_tax_basis indicators are used to compute
	//         these dollar amounts.  The total_amount_spent (book & tax) and the 
	//			  total_closings (book & tax) are then used to compute BOTH the beginning CWIP
	//         AND the beginning afudc base.  Any differences between AFUDC/CPI eligibility and
	//			  the Book/Tax basis indicators are assumed away for budgeting.
	//
	//			  Addition to the above comment:  there is now code that takes AFUDC compounding
	//			  into account for those clients that NEVER compound.  We have a system switch, 
	//			  since it would be nearly impossible to get the beg afudc base right for a mixture 
	//			  of compounding rules by afudc type.  This code (per the switch) will 
	//			  place ALL afudc into the uncompounded_afudc_balance and then back it off of 
	//			  the beg_afudc_base (it must stay in the beg_cwip for closings).
	//			  With the switch set the other way, the actual afudc is "compounded"
	//			  into the base automatically by being set equal to beg_cwip.  We are assuming this
	//			  away, as it should only cause small differences, and can be avoided by setting
	//			  the afudc start month equal to an actual month where afudc WAS compounded.
	//
	//  CHANGED:  Updating the beg_cwip here,  backing into total_closings
	//
	//  06/20/2003 :  THE BIG UPDATE NEVER CAME BACK AT SCANA.  All of a sudden, the update of
	//						4 fields at once hung forever.  I changed the code to fire 4 separate
	//						updates.
	
//	f_pp_msgs("Updating total_amount_spent, beg_cwip at " + string(now()))
	
	
	if right(string(i_start_month), 2) = "01" then
		beg_bal_mo = i_start_month - 89
	else
		beg_bal_mo = i_start_month - 1
	end if
	
	
	f_pp_msgs("Updating total_amount_spent at " + string(now()))
	
	update cr_budget_afudc_calc afc_calc set total_amount_spent = (
	
	select amount from 
	
	(select d.budget_id, sum(a.wo_amount * ctd.percent_book_basis) as amount
	  from wo_summary a, work_order_account c, work_order_control d,
			 (
				select a.* from charge_type_data a, (
					select charge_type_id,max(effective_date) effective_date
					  from charge_Type_data 
					 where to_number(to_char(effective_date,'yyyymm')) <= :i_start_month
					group by charge_type_id) b
				 where a.charge_type_id = b.charge_type_id
					and a.effective_date = b.effective_date) ctd
	 where a.work_order_id = c.work_order_id
		and a.work_order_id = d.work_order_id
		and d.work_order_type_id not in (
			select work_order_type_id from cr_budget_afudc_wotype_exclude)
		and a.charge_type_id = ctd.charge_type_id
		and a.expenditure_type_id = 1
		and a.month_number < :i_start_month
	 group by d.budget_id) actuals_by_budget
	
	 where afc_calc.budget_id = actuals_by_budget.budget_id
		and afc_calc.month_number = :i_start_month)
	
	where budget_version = :i_budget_version
	  and month_number = :i_start_month;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.TOTAL_AMOUNT_SPENT: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
		
	f_pp_msgs("Updating total_amount_spent_tax at " + string(now()))
	
	update cr_budget_afudc_calc afc_calc set 
														  total_amount_spent_tax = (
	
	select amount from 
	
	(select d.budget_id, sum(a.wo_amount * ctd.percent_tax_basis) as amount
	  from wo_summary a, work_order_account c, work_order_control d,
			 (
				select a.* from charge_type_data a, (
					select charge_type_id,max(effective_date) effective_date
					  from charge_Type_data 
					 where to_number(to_char(effective_date,'yyyymm')) <= :i_start_month
					group by charge_type_id) b
				 where a.charge_type_id = b.charge_type_id
					and a.effective_date = b.effective_date) ctd
	 where a.work_order_id = c.work_order_id
		and a.work_order_id = d.work_order_id
		and d.work_order_type_id not in (
			select work_order_type_id from cr_budget_afudc_wotype_exclude)
		and a.charge_type_id = ctd.charge_type_id
		and a.expenditure_type_id = 1
		and a.month_number < :i_start_month
	 group by d.budget_id) actuals_by_budget
	
	 where afc_calc.budget_id = actuals_by_budget.budget_id
		and afc_calc.month_number = :i_start_month)
		
		where budget_version = :i_budget_version
	  and month_number = :i_start_month;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.TOTAL_AMOUNT_SPENT_TAX: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
		
		
	f_pp_msgs("Updating beg_cwip at " + string(now()))
	
	update cr_budget_afudc_calc afc_calc set 
														  beg_cwip = (
	
	select amount from 
	
	(select d.budget_id, sum(a.in_process_balance) as amount
	  from charge_summary a, work_order_control d
	 where a.work_order_id = d.work_order_id
		and d.work_order_type_id not in (
			select work_order_type_id from cr_budget_afudc_wotype_exclude)
		and a.expenditure_type_id = 1
		and a.month_number = :beg_bal_mo
	 group by d.budget_id) actuals_by_budget
	
	 where afc_calc.budget_id = actuals_by_budget.budget_id
		and afc_calc.month_number = :i_start_month)
	
	where budget_version = :i_budget_version
	  and month_number = :i_start_month;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.BEG_CWIP: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
		
	f_pp_msgs("Updating beg_cwip_tax at " + string(now()))
	
	update cr_budget_afudc_calc afc_calc set 	
														  beg_cwip_tax = (
	
	select amount from 
	
	(select d.budget_id, sum(a.in_process_balance) as amount
	  from charge_summary a, work_order_control d
	 where a.work_order_id = d.work_order_id
	   and d.work_order_type_id not in (
			select work_order_type_id from cr_budget_afudc_wotype_exclude)
		and a.expenditure_type_id = 1
		and a.month_number = :beg_bal_mo
	 group by d.budget_id) actuals_by_budget
	
	 where afc_calc.budget_id = actuals_by_budget.budget_id
		and afc_calc.month_number = :i_start_month)
	
	where budget_version = :i_budget_version
	  and month_number = :i_start_month;
		
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.BEG_CWIP_TAX: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
//	update cr_budget_afudc_calc afc_calc set total_amount_spent = (
//	
//	select amount from 
//	
//	(select d.budget_id, sum(a.wo_amount * ctd.percent_book_basis) as amount
//	  from wo_summary a, work_order_account c, work_order_control d,
//			 (
//				select a.* from charge_type_data a, (
//					select charge_type_id,max(effective_date) effective_date
//					  from charge_Type_data 
//					 where to_number(to_char(effective_date,'yyyymm')) <= :i_start_month
//					group by charge_type_id) b
//				 where a.charge_type_id = b.charge_type_id
//					and a.effective_date = b.effective_date) ctd
//	 where a.work_order_id = c.work_order_id
//		and a.work_order_id = d.work_order_id
//		and a.charge_type_id = ctd.charge_type_id
//		and a.expenditure_type_id = 1
//		and a.month_number < :i_start_month
//	 group by d.budget_id) actuals_by_budget
//	
//	 where afc_calc.budget_id = actuals_by_budget.budget_id
//		and afc_calc.month_number = :i_start_month),
//		
//														  total_amount_spent_tax = (
//	
//	select amount from 
//	
//	(select d.budget_id, sum(a.wo_amount * ctd.percent_tax_basis) as amount
//	  from wo_summary a, work_order_account c, work_order_control d,
//			 (
//				select a.* from charge_type_data a, (
//					select charge_type_id,max(effective_date) effective_date
//					  from charge_Type_data 
//					 where to_number(to_char(effective_date,'yyyymm')) <= :i_start_month
//					group by charge_type_id) b
//				 where a.charge_type_id = b.charge_type_id
//					and a.effective_date = b.effective_date) ctd
//	 where a.work_order_id = c.work_order_id
//		and a.work_order_id = d.work_order_id
//		and a.charge_type_id = ctd.charge_type_id
//		and a.expenditure_type_id = 1
//		and a.month_number < :i_start_month
//	 group by d.budget_id) actuals_by_budget
//	
//	 where afc_calc.budget_id = actuals_by_budget.budget_id
//		and afc_calc.month_number = :i_start_month),
//		
//														  beg_cwip = (
//	
//	select amount from 
//	
//	(select d.budget_id, sum(a.in_process_balance) as amount
//	  from charge_summary a, work_order_control d
//	 where a.work_order_id = d.work_order_id
//		and a.expenditure_type_id = 1
//		and a.month_number = :beg_bal_mo
//	 group by d.budget_id) actuals_by_budget
//	
//	 where afc_calc.budget_id = actuals_by_budget.budget_id
//		and afc_calc.month_number = :i_start_month),
//		
//														  beg_cwip_tax = (
//	
//	select amount from 
//	
//	(select d.budget_id, sum(a.in_process_balance) as amount
//	  from charge_summary a, work_order_control d
//	 where a.work_order_id = d.work_order_id
//		and a.expenditure_type_id = 1
//		and a.month_number = :beg_bal_mo
//	 group by d.budget_id) actuals_by_budget
//	
//	 where afc_calc.budget_id = actuals_by_budget.budget_id
//		and afc_calc.month_number = :i_start_month)
//	
//	where budget_version = :i_budget_version
//	  and month_number = :i_start_month;
//	
//	if sqlca.SQLCode < 0 then
//		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.TOTAL_AMOUNT_SPENT: " + &
//			sqlca.SQLErrText)
//		rollback;
//		DESTROY uo_alloc
//		DESTROY ds_alloc_list
//		return -1
//	else
//		commit;
//	end if


	f_pp_msgs("Updating total_amount_spent ... cleaning up NULLs ... at " + string(now()))
	update cr_budget_afudc_calc set total_amount_spent = 0
	 where total_amount_spent is NULL and budget_version = :i_budget_version
	 	and month_number = :i_start_month;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.TOTAL_AMOUNT_SPENT (nulls): " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	f_pp_msgs("Updating beg_cwip ... cleaning up NULLs ... at " + string(now()))
	update cr_budget_afudc_calc set beg_cwip = 0
	 where beg_cwip is NULL and budget_version = :i_budget_version
	 	and month_number = :i_start_month;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.BEG_CWIP (nulls): " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
		
	f_pp_msgs("Updating total_amount_spent_tax ... cleaning up NULLs ... at " + string(now()))
	update cr_budget_afudc_calc set total_amount_spent_tax = 0
	 where total_amount_spent_tax is NULL and budget_version = :i_budget_version
	 	and month_number = :i_start_month;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.TOTAL_AMOUNT_SPENT_TAX (nulls): " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	f_pp_msgs("Updating beg_cwip_tax ... cleaning up NULLs ... at " + string(now()))
	update cr_budget_afudc_calc set beg_cwip_tax = 0
	 where beg_cwip_tax is NULL and budget_version = :i_budget_version
	 	and month_number = :i_start_month;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.BEG_CWIP_TAX (nulls): " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	//  System Switch:  to always back out total afudc and cpi out of the base.  Only appropriate
	//                  for clients that never compound.
	cv = ""
	select upper(rtrim(control_value)) into :cv from cr_system_control 
 	 where upper(rtrim(control_name)) = 'BDG AFUDC - NO AFC IN BEG BASE';
	if isnull(cv) or cv = "" then cv = "NO"
 	
	if cv = "YES" then
		f_pp_msgs("Updating uncompounded_afudc_balance, uncompounded_cpi_balance ... at " + string(now()))
		
		update cr_budget_afudc_calc afc_calc set uncompounded_afudc_balance = (
		
		select amount from 
		
		(select d.budget_id, sum(a.wo_amount) as amount
		  from wo_summary a, work_order_control d,
				(
				select charge_type_id,afudc_elig_indicator,max(effective_date) 
				  from charge_type_data
				 where charge_type_id in 
					(select charge_type_id from cost_element
					  where cost_element_id in 
						(select debt_cost_element from afudc_control)
						  or cost_element_id in 
						(select equity_cost_element from afudc_control))
				 group by charge_type_id,afudc_elig_indicator
				) e
		 where a.work_order_id = d.work_order_id
			and a.charge_type_id = e.charge_type_id
			and a.expenditure_type_id = 1
			and a.month_number <= :beg_bal_mo
		 group by d.budget_id) actuals_by_budget
		
		 where afc_calc.budget_id = actuals_by_budget.budget_id
			and afc_calc.month_number = :i_start_month),
			
															  uncompounded_cpi_balance = (
		
		select amount from 
		
		(select d.budget_id, sum(a.wo_amount) as amount
		  from wo_summary a, work_order_control d,
				(
				select charge_type_id,afudc_elig_indicator,max(effective_date) 
				  from charge_type_data
				 where charge_type_id in 
					(select charge_type_id from cost_element
					  where cost_element_id in 
						(select cpi_cost_element from afudc_control))
				 group by charge_type_id,afudc_elig_indicator
				) e
		 where a.work_order_id = d.work_order_id
			and a.charge_type_id = e.charge_type_id
			and a.expenditure_type_id = 1
			and a.month_number <= :beg_bal_mo
		 group by d.budget_id) actuals_by_budget
		
		 where afc_calc.budget_id = actuals_by_budget.budget_id
			and afc_calc.month_number = :i_start_month)
		
		where budget_version = :i_budget_version
		  and month_number = :i_start_month;
			
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating cr_budget_afudc_calc.UNCOMPOUNDED_AFUDC_BALANCE (nulls): " + &
				sqlca.SQLErrText)
			rollback;
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			return -1
		else
			commit;
		end if
		
		f_pp_msgs("Updating uncompounded_afudc_balance ... cleaning up NULLs ... at " + string(now()))
		update cr_budget_afudc_calc set uncompounded_afudc_balance = 0
		 where uncompounded_afudc_balance is NULL and budget_version = :i_budget_version
			and month_number = :i_start_month;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating cr_budget_afudc_calc.UNCOMPOUNDED_AFUDC_BALANCE (nulls): " + &
				sqlca.SQLErrText)
			rollback;
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			return -1
		else
			commit;
		end if
		
		f_pp_msgs("Updating uncompounded_cpi_balance ... cleaning up NULLs ... at " + string(now()))
		update cr_budget_afudc_calc set uncompounded_cpi_balance = 0
		 where uncompounded_cpi_balance is NULL and budget_version = :i_budget_version
			and month_number = :i_start_month;
		
		if sqlca.SQLCode < 0 then
			f_pp_msgs("ERROR: updating cr_budget_afudc_calc.UNCOMPOUNDED_CPI_BALANCE (nulls): " + &
				sqlca.SQLErrText)
			rollback;
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			return -1
		else
			commit;
		end if
		
		//  In the update of the beg_cpi_base, we will include afudc AND the 
		//  uncompounded_cpi_balance so both CPI and afudc get backed out of the CPI base.  
		//  Otherwise it will look really goofy.  Note that if this system switch is turned off, 
		//  the beg_afudc_base and beg_cpi_base with start out equal and will BOTH include 
		//  afudc (CPI would not be included since the beg base is derived from charge summary 
		//  that by definition excludes tax only items).
		
	end if  //  if cv = "YES" then ...
	
	//  Continuation of system switch from above in this IF block:
	f_pp_msgs("Updating total_closings, beg_afudc_base ... at " + string(now()))
	if cv = "YES" then
		update cr_budget_afudc_calc 
			set total_closings          = total_amount_spent     - beg_cwip,
				 total_closings_tax      = total_amount_spent_tax - beg_cwip_tax,
				 beg_afudc_base          = beg_cwip               - uncompounded_afudc_balance,
				 beg_cpi_base            = beg_cwip_tax           - uncompounded_cpi_balance 
				 															     - uncompounded_afudc_balance
		 where budget_version = :i_budget_version
			and month_number = :i_start_month;
	else
		update cr_budget_afudc_calc 
			set total_closings          = total_amount_spent     - beg_cwip,
				 total_closings_tax      = total_amount_spent_tax - beg_cwip_tax,
				 beg_afudc_base          = beg_cwip,
				 beg_cpi_base            = beg_cwip_tax
		 where budget_version = :i_budget_version
			and month_number = :i_start_month;
	end if
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: updating cr_budget_afudc_calc.TOTAL_CLOSINGS (nulls): " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	//  Update ISD for blankets:
	f_pp_msgs("Updating ISD for blankets at " + string(now()))
	
	update cr_budget_afudc_calc 
	   set in_service_date = to_date(:i_start_month, 'yyyymm')
	 where budget_version = :i_budget_version
	 	and closing_option_id in (6, 7, 8, 10, 11);
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: in w_budget_allocations.wf_run: updating closings to 0: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	f_pp_msgs("Updating percent_close ... at " + string(now()))
	update cr_budget_afudc_calc a set percent_close = (

		select percent_close from closing_pattern_data b
		 where a.closing_pattern_id = b.closing_pattern_id
			and a.month_number = b.month_number
			and budget_version = :i_budget_version)
	
	where budget_version = :i_budget_version
	  and	closing_pattern_id is not null;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: in w_budget_allocations.wf_run: updating percent_close: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	f_pp_msgs("Updating percent_close to 0 where NULL ... at " + string(now()))
	
	update cr_budget_afudc_calc set percent_close = 0 
	 where budget_version = :i_budget_version and percent_close is null;
	
	if sqlca.SQLCode < 0 then
		f_pp_msgs("ERROR: in w_budget_allocations.wf_run: updating percent_close to 0 where NULL: " + &
			sqlca.SQLErrText)
		rollback;
//		DESTROY uo_alloc
		DESTROY ds_alloc_list
		return -1
	else
		commit;
	end if
	
	i_max_late_chg_wait_period = 0
	select max(late_chg_wait_period) into :i_max_late_chg_wait_period 
	  from cr_budget_afudc_calc
	 where budget_version = :i_budget_version and in_service_date is not null;
	if isnull(i_max_late_chg_wait_period) then i_max_late_chg_wait_period = 0
	
	i_budget_afudc_transfers = 0
	select count(*) into :i_budget_afudc_transfers from budget_afudc_defaults
	 where afudc_transfer_rate <> 0 and afudc_transfer_rate is not null;
	if isnull(i_budget_afudc_transfers) then i_budget_afudc_transfers = 0
	
	for ii = i_start_month to i_end_month
		
		//  A month beyond 12 ...
		if 12 - integer(right(string(ii),2)) < 0 or right(string(ii),2) = "00" then continue
		
		f_pp_msgs("Started: AFUDC: for month: " + string(ii) + &
					 " at " + string(today(), "mm/dd/yyyy hh:mm:ss"))
		
		i_run_all        = false  //  They must run 1 priority at a time.
		i_allocation_id  = 0
		i_budget_version = i_budget_version
		i_month_number   = ii
		i_month_period   = 0
		i_msgbox         = false
		i_run_as_a_test  = false
		i_whole_month    = true
		
		i_closings_where_clause = " and (month_number = "
		
		for iii = ii to ii + i_max_late_chg_wait_period
			
			wc_mn = iii
			
			choose case right(string(wc_mn),2)
				case "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"
					wc_mn = wc_mn + 88
			end choose
			
			if iii = ii + i_max_late_chg_wait_period then
				i_closings_where_clause = i_closings_where_clause + string(wc_mn) + ") "
			else
				i_closings_where_clause = i_closings_where_clause + string(wc_mn) + &
					" or month_number = "
			end if
			
		next

		rtn = uf_afudc()
		
		if rtn < 0 then
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			return -1
		end if
						 
	next
		
else
	
	//  DMJ: 7/2/08:  MOVED FROM WITHIN THE LOOP TO CHANGE THE ORDER OF EVENTS.  THE PREVIOUS CODE
	//  WOULD HAMPER THE ABILITY TO DO THINGS LIKE YTD ALLOCATIONS.
	FOR ii = i_start_month TO i_end_month
		
			//  A month beyond 12 ...
			if 12 - integer(right(string(ii),2)) < 0 or right(string(ii),2) = "00" then continue
			
			if i_ifb_field_exists then
				//  Need a sequence number for validations.  Need to pick one off at this point
				//  in the loop since the ifb_id needs to be the "batch" of transactions you are
				//  validating.  In this case we are validating the entire month.
				setnull(i_ifb_id)
				select costrepositorybdg.nextval into :i_ifb_id from dual;
				if isnull(i_ifb_id) or trim(i_ifb_id) = "" then
					f_pp_msgs("  ")
					f_pp_msgs("ERROR: could not determine a i_ifb_id !!!")
					f_pp_msgs("  ")
					DESTROY ds_alloc_list 
					return -1 
				end if
			end if
			
			
	for i = 1 to num_rows
		
		allocation_id = ds_alloc_list.GetItemNumber(i, "allocation_id")
		descr         = ds_alloc_list.GetItemString(i, "description")
		
		f_pp_msgs("Started: " + string(allocation_id) + ": " + &
					  descr + " for month: " + string(ii) + &
					 " at " + string(today(), "mm/dd/yyyy hh:mm:ss"))
		
		//***FOR TESTING CONCURRENT PROCESSING *** messagebox(string(i_session_id), "hold")
		if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
			//  Is anyone running this allocation ?
			sess = 0
			select running_session_id into :sess from cr_allocation_control_bdg
			 where allocation_id = :allocation_id and running_session_id is not null and rownum = 1;
			if isnull(sess) then sess = 0
			
			if sess > 0 then
				// ### 7485:  JAK: 2011-05-04:  Also write out the allocation id when this occurs since it's not always obvious.
				f_pp_msgs("***** Another session (" + string(sess) + ") is running Allocation ID " + string(allocation_id) + ".")
				f_pp_msgs("***** Continuing on to the next allocation in this priority.")
				f_pp_msgs("*******************************************")
				continue // To the next allocation.
			end if
			
			//  Lock this allocation in case they are running suspense accounting
			//  which allows for concurrent processing.  A commit is OK here since
			//  one will be firing shortly in uf_start_allocations
			update cr_allocation_control_bdg set running_session_id = :i_session_id
			 where allocation_id = :allocation_id;
			
			if sqlca.SQLCode <> 0 then
				f_pp_msgs("  ")
				f_pp_msgs("ERROR: updating cr_allocation_control_bdg.running_session_id: " + &
					string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
				f_pp_msgs("  ")
				rollback;
				return -1
			else
				commit;
			end if
		end if  //  if suspense_accounting = "YES" then ...
		
		
		//  DMJ:  7/2/08:  MOVED ABOVE.
		//for ii = i_start_month to i_end_month
		//
		//	//  A month beyond 12 ...
		//	if 12 - integer(right(string(ii),2)) < 0 or right(string(ii),2) = "00" then continue
			

//		f_pp_msgs("Started: " + string(allocation_id) + ": " + &
//					  descr + " for month: " + string(ii) + &
//					 " at " + string(today(), "mm/dd/yyyy hh:mm:ss"))
		
		//  Cannot run an allocation (Test or Actual) more than one time per month if it has
		//  already run as "whole month".
		counter = 0
		select count(*) into :counter
		  from cr_alloc_process_control_bdg
		 where budget_version = :i_budget_version and
				 allocation_id = :allocation_id and
				 month_number  = :ii            and
				 whole_month   = 1              and
				 upper(test_or_actual) = :run_as;

		if counter > 0 then
			f_pp_msgs("This budget allocation has already run for " + string(ii) + &
						  ".  You must delete this month's results to re-run" + &
						  " the allocation.")
			f_pp_msgs("*******************************************")
			if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
				rtn = uf_unlock(allocation_id)
				if rtn = -1 then return -1
			end if
			continue
		end if

			
		//  Cannot run a test allocation if the actual allocation has already run for this
		//  month and period ... potential for misleading results based on source data plus
		//  actual allocation results.
		if run_as = "TEST" then
	
			counter = 0
			select count(*) into :counter
			  from cr_alloc_process_control_bdg
			 where budget_version = :i_budget_version and
					 allocation_id = :allocation_id and
					 month_number  = :ii            and
					 whole_month   = 1              and
					 upper(test_or_actual) = 'ACTUAL';
			 
			if counter > 0 then
				f_pp_msgs("ACTUAL results exist for this budget allocation for " + string(ii) + &
							 ".  Cannot run this TEST allocation.")
				f_pp_msgs("*******************************************")
				if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
					rtn = uf_unlock(allocation_id)
					if rtn = -1 then return -1
				end if
				continue
			end if
	
		end if
		
		
		//  Cannot run an actual allocation if the test allocation has already run for this
		//  month and period ... potential for double counting or leaving a residual result
		//  in the test table.
		if run_as = "ACTUAL" then
	
			counter = 0
			select count(*) into :counter
			  from cr_alloc_process_control_bdg
			 where budget_version = :i_budget_version and
					 allocation_id = :allocation_id and
					 month_number  = :ii            and
					 whole_month   = 1              and
					 upper(test_or_actual) = 'TEST';
			 
			if counter > 0 then
				f_pp_msgs("TEST results exist for this budget allocation for " + string(ii) + &
							 ".  Cannot run this ACTUAL allocation.")
				f_pp_msgs("*******************************************")
				if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
					rtn = uf_unlock(allocation_id)
					if rtn = -1 then return -1
				end if
				continue
			end if
	
		end if
		
	
		i_run_all        = false  //  They must run 1 priority at a time.
		i_allocation_id  = allocation_id
		i_budget_version = i_budget_version
		i_month_number   = ii
		i_month_period   = 0
		i_msgbox         = false
		i_run_as_a_test  = run_as_a_test
		i_whole_month    = true
		
		/*### - MDZ - 29485 - 20130311*/
		/*Since this is the first place that i_month_number is set, let's move the code to figure out the fiscal year start month here*/
		longlong init_mo_fy
		
		select control_value
		into :init_mo_fy
		from cr_alloc_system_control 
		where lower(control_name) = 'initial month of fiscal year'
		;
		if sqlca.sqlcode <> 0 then	
			uf_msg("", "  ")
			uf_msg("","ERROR: figuring out initial month of fiscal year~n~n" + sqlca.SQLErrText)
			uf_msg("", "  ")
			rollback;
			return -1
		end if 
		
		//If they entered an invalid number in the system control do not use it
		if isnull(init_mo_fy) or init_mo_fy < 1 or init_mo_fy > 12 then
			init_mo_fy = 1
		end if 
		
		//Let's now figure out the fytd start month so that we can pass it to the other functions using YTD
		if long(mid(string(i_month_number), 5,2)) < init_mo_fy then
			i_fy_start_month = long(left(string(i_month_number - 100), 4) + f_lpad(string(init_mo_fy), 2, '0'))
		else
			i_fy_start_month = long(left(string(i_month_number), 4) + f_lpad(string(init_mo_fy), 2, '0'))
		end if
	
		rtn = uf_start_allocations()
		
		if rtn < 0 then 
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
			rtn = uf_unlock(allocation_id)
				if rtn = -1 then return -1
			end if
			return -1
		end if
			
			
		//  DMJ:  7/2/08:  SEE COMMENTS ABOVE.
		//next  //  for ii = month_number to month_number2 ...
		
		
		if i_suspense_accounting = "YES" or i_concurrent_processing = "YES" then
			rtn = uf_unlock(allocation_id)
			if rtn = -1 then return -1
		end if
		
	next  //  for i = 1 to num_rows ...
	
	
	//****************************************************************************************
	//
	//  VALIDATIONS:
	//
	//    This technique is non-standard, but done this way for multiple reasons (don't wan
	//    to rip apart all the budget allocation code ... don't want to introduce a staging
	//    table that will potentially impact performance ... don't want to hold transactions
	//    in a staging table if they are invalid - i.e. for budget, just validate the 
	//    posted transactions ... etc.)
	//
	//****************************************************************************************
	if i_ifb_field_exists then
		//  Continue.
	else
		//  Cannot validate without this field.
		goto after_validations
	end if
	
	if i_me_validations = "YES" or i_combo_validations = "YES" or &
		i_proj_based_validations = "YES" or i_mn_validations = "YES" or &
		i_custom_validations = "YES" &
	then
		
		f_pp_msgs("  ")
		f_pp_msgs("Running validations for: " + string(ii) + &
					 " at " + string(today(), "mm/dd/yyyy hh:mm:ss"))
		f_pp_msgs("  ")
		
		//  For budget allocations, we need to first insert the records into cr_budget_data_temp.
		//  That table will be used for validations so we can fire analyze_table without waiting
		//  for the entire cr_budget_data table to have stats built.  This is a global temp
		//  so we can sefely truncate it later without regard for other users.
		if run_as_a_test then
			table_name = "cr_budget_data_test"
		else
			table_name = "cr_budget_data"
		end if
		
		sqls = &
			"insert into cr_budget_data_temp select * from " + table_name + " " + &
		 		"where interface_batch_id = '" + i_ifb_id + "'"
		
		execute immediate :sqls;
		
		if sqlca.SQLCode <> 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: inserting into cr_budget_data_temp: " + &
				string(sqlca.SQLCode) + ": " + sqlca.SQLErrText)
			f_pp_msgs("  ")
			rollback;
			return -1
		end if
		
		i_run_as_a_test = run_as_a_test
		
		rtn = uf_validations()
		
		if rtn <> 1 then
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			return -1
		end if
		
		sqlca.truncate_table('cr_budget_data_temp')
		
		if sqlca.SQLCode <> 0 then
			f_pp_msgs("  ")
			f_pp_msgs("ERROR: truncating cr_budget_data_temp: " + sqlca.SQLErrText)
			f_pp_msgs( "  ")
			rollback using sqlca;
//			DESTROY uo_alloc
			DESTROY ds_alloc_list
			return -1
		end if
		
		f_pp_msgs("  ")
		f_pp_msgs("Finished validations for: " + string(ii) + &
					 " at " + string(today(), "mm/dd/yyyy hh:mm:ss"))
		f_pp_msgs("  ")
			
	end if  //  if validation are turned on ...
	
	//  DO NOT update the interface_batch_id back to NULL.  We are using a new sequence to populate these,
	//  so there is no danger of subsequent validations grabbing these records.  Furthermore, we need to be
	//  able to leverage this value in case multiple users have to filter the kickouts.
	
	after_validations:
	
	
	//  DMJ:  7/2/08:  MOVED MN OUTSIDE THE ALLOCATION LOOP.
	NEXT  //  FOR ii = i_start_month TO i_end_month ...
	
	
end if


//DESTROY uo_alloc
DESTROY ds_alloc_list


rtn = f_cr_allocations_custom_bdg("After Main Loop")
if rtn < 0 then return rtn


return 1
end function

public function string uf_getcustomversion (string a_pbd_name);//***************************************************************************************** 
//  PROPRIETARY INFORMATION OF POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//
//   Subsystem:  system
//
//   Event    :  uo_client_interface.uf_getCustomVersion
//
//   Purpose  :  This function retrieves the custom version of the PBD associated with 
//					this interface.
//                
// 					
//  DATE            NAME                      REVISION               CHANGES
//  --------        --------                  -----------   			----------------------
//  08-13-2008      PowerPlan                 Version 1.0            Initial Version
//
//  PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED 
//*****************************************************************************************

nvo_exe_budget_allocations_custom_version nvo_exe_budget_allocations_custom_version
nvo_exe_budallo_custom_dw_version nvo_exe_budallo_custom_dw_version
nvo_ppcostrp_interface_custom_version nvo_ppcostrp_interface_custom_version

choose case a_pbd_name
	case 'exe_budget_allocations_custom.pbd'
		return nvo_exe_budget_allocations_custom_version.custom_version
	case 'exe_budallo_custom_dw.pbd'
		return nvo_exe_budallo_custom_dw_version.custom_version
	case 'ppcostrp_interface_custom.pbd'
		return nvo_ppcostrp_interface_custom_version.custom_version
end choose


return ""
end function

on uo_client_interface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_client_interface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

