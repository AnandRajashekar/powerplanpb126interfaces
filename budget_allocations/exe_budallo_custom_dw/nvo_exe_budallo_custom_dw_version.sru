HA$PBExportHeader$nvo_exe_budallo_custom_dw_version.sru
forward
global type nvo_exe_budallo_custom_dw_version from nonvisualobject
end type
end forward

global type nvo_exe_budallo_custom_dw_version from nonvisualobject autoinstantiate
end type

type variables
string custom_version = '2016.1.2.0'
end variables

on nvo_exe_budallo_custom_dw_version.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_exe_budallo_custom_dw_version.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

