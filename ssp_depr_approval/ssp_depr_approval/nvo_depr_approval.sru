HA$PBExportHeader$nvo_depr_approval.sru
forward
global type nvo_depr_approval from nonvisualobject
end type
end forward

global type nvo_depr_approval from nonvisualobject
end type
global nvo_depr_approval nvo_depr_approval

type variables
nvo_cpr_control i_nvo_cpr_control //make this an instance variable so we don't have to create a new copy in every function

string i_exe_name = "ssp_depr_approval.exe"
end variables

forward prototypes
private function boolean of_balance_cpr_depr ()
public function integer uf_depr_approval (longlong a_company_ids[], date a_month)
end prototypes

private function boolean of_balance_cpr_depr (); datetime  last_run, max_act_month
decimal {2} difference
longlong rows, month_num, year_num, ret
decimal {2} sum_diff
string save_dir, month

uo_ds_top ds_balance_cpr_depr

// check if this is the last month with activity. If so, there is no need to back down
// activities

 
save_dir = f_get_temp_dir()
	
if save_dir = '' then save_dir = 'c:'

ds_balance_cpr_depr = create uo_ds_top
ds_balance_cpr_depr.dataobject = "dw_balance_cpr_depr"
ds_balance_cpr_depr.settransobject(sqlca)
ret = ds_balance_cpr_depr.retrieve(i_nvo_cpr_control.i_company)
	
ds_balance_cpr_depr.setfilter("depr_diff <> 0")
ds_balance_cpr_depr.filter()
	
ret = ds_balance_cpr_depr.rowcount()

if ret > 0  then 
	
	f_status_box("", " ")	
	f_status_box("", " ")	
	f_status_box("","********************************************************** ")	
	f_status_box("Balance", "Depr Ledger is Not in Balance with CPR Ledger.")		
	f_status_box("Balance", "See differences in file : " + &
	 save_dir + '\deprbal' + '_' + string(i_nvo_cpr_control.i_company) + '.txt')	
	f_status_box("", "********************************************************** ")	
	f_status_box("", " ")	
	f_status_box("", " ")	
	
		
	ds_balance_cpr_depr.saveas(save_dir + '\deprbal' + '_' + string(i_nvo_cpr_control.i_company) + '.txt',Text!,true)
	return false
else
	return true
end if
	

 
end function

public function integer uf_depr_approval (longlong a_company_ids[], date a_month);// In order for the depreciation results to be approved, several conditions must be true:
//			 - this month's depreciation must have been calculated
//			 - this month must not be closed
//        - next month must already be open

// After registering the depreciation calculation as 'approved',  the
// balances must be rolled forward on the depreciation ledger and all 
// depreciation subledgers.

datetime nullval
string   company, je_code, balance_depr, month_string, sqls, depr_group, gl_acct
longlong trans_id, total_company, je_id, pp_stat_id
string ret, msg, comp_descr, ls_month
longlong check, rtn, rows, i, cc, rc, ll_long_args[], ll_month
longlong je_method, je_rows, je, jebooks_rows, b, book, convention, amt_type, je_rows10[], je_rows11[], empty[]
longlong book_count, book_include, depr_ind, je_by_asset, dg_id, num_assets, q
string	find_str, user_id, je_descr
boolean	vfy_users, do_not_update,  je_skip[], emptyb[], depr_balanced
uo_ds_top ds_users
s_gl_trans_parms lst_gl_trans_parms 
nvo_aro_reg reg_aro_engine
string args[]
longlong num_failed_companies, return_code
boolean b_failed_company = false
w_datawindows w

f_pp_msgs("Beginning depreciation approval process for the following companies:")
for cc = 1 to upperbound(a_company_ids)
	f_pp_msgs("	" + string(a_company_ids[cc]))
next

i_nvo_cpr_control.of_constructor()

if i_nvo_cpr_control.of_setupfromcompaniesandmonth(a_company_ids, a_month) <> 1 then 
	return -1 //of_setupfromcompaniesandmonth has required messaging within it, so just return here. 
end if

// Set the month number
i_nvo_cpr_control.i_month_number	= year(a_month)*100 + month(a_month)

// Check for concurrent processes and lock if another process is not already running
string process_msg
if not i_nvo_cpr_control.of_lockprocess(i_exe_name, i_nvo_cpr_control.i_company_idx, i_nvo_cpr_control.i_month_number, process_msg) then
	 f_pp_msgs("There has been a concurrency error. Please check that processes are not currently running.")
	 return -1
end if

// check if the multiple companies chosen can be processed  together
if (i_nvo_cpr_control.of_selectedcompanies(a_company_ids) = -1) then
	f_pp_msgs("Cannot process multiple companies because the open/closed months do not line up")
	
	// Release the concurrency lock
	i_nvo_cpr_control.of_releaseProcess(process_msg)
	f_pp_msgs("Release Process Status: " + process_msg)
	
	return -1
end if 

reg_aro_engine = create nvo_aro_reg

//g_process_id = wf_setup_pp_msgs('Depreciation Approval') //CBS - interface shell takes care of this

setnull(nullval)

f_set_large_rollback_segment()

//total_company = i_nvo_cpr_control.of_getselected() //CBS - use input argument
total_company = upperbound(a_company_ids)
if total_company = 1 then
	msg = " company selected. "  
else
	msg = string(total_company) + " companies selected. "  
end if

for cc = 1 to total_company
	
	
	f_pp_msgs("Beginning loop over companies.")
	
	rc = i_nvo_cpr_control.of_setupcompany(cc)
	
	if rc < 1 then 
		f_pp_msgs("of_setupcompany returned an error.")
		continue // next company
	end if

	do_not_update = false
	je_rows10 = empty
	je_rows11 = empty
	je_skip = emptyb

	//CBS - this validation was commented out in the old window depr_approval, so we will leave it commented out here
//	if isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'cpr_closed')) then
//
//		f_status_box("Information","The CPR Ledger must be closed for the month " &
//					+ "in order for depreciation to be approved.")
//		goto next_company
//	
//	end if
	
	if isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'depr_calculated')) then
	
		f_status_box("Information","The depreciation results must be calculated " &
					+ "in order to be approved.")
					
		b_failed_company = true			
		goto next_company
	
	end if
	
	if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'depr_approved')) then
		f_status_box("Information","The depreciation results have already been approved.")
		
		b_failed_company = true			
		goto next_company
	
	end if
	
	if not isnull(i_nvo_cpr_control.i_ds_cpr_control.getitemdatetime(1,'powerplant_closed')) then
		f_status_box("Information","This month is closed.")
		
		b_failed_company = true			
		goto next_company
	
	end if
	
	// verify that ARO's were approved before the Depr was calc'ed
	string s_aro_message
	if not reg_aro_engine.uf_can_approve_depr(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month, s_aro_message) then
		f_status_box("Information", s_aro_message)
		
		b_failed_company = true
		goto next_company
	end if
	
	
	if i_nvo_cpr_control.i_col_num = 1 then
	
		f_status_box("Information","Next month must be open before approving " &
					+ "the depreciation results.")
					
		b_failed_company = true
		goto next_company
	
	end if
	
	//Maint 8604
	select 1 into :check
	from pend_depr_activity a, depr_group g
	where (gl_post_mo_yr = :i_nvo_cpr_control.i_month or current_mo_yr = :i_nvo_cpr_control.i_month)
	and a.depr_group_id = g.depr_group_id
	and g.company_id = :i_nvo_cpr_control.i_company
	and rownum = 1
	;
	
	if check = 1 then
		
		//Maint 8604
		select  g.description  into :depr_group
		from pend_depr_activity a, depr_group g
		where (gl_post_mo_yr = :i_nvo_cpr_control.i_month or current_mo_yr = :i_nvo_cpr_control.i_month)
		and a.depr_group_id = g.depr_group_id
		and g.company_id = :i_nvo_cpr_control.i_company
		and rownum = 1
		;
	 
		if g_process_id > 0 then
			f_pp_msgs("Cannot Approve Depreciation for the Month While There are Still Unposted Pending " &
				+ " Reserve Transactions for the Month for Depr Group " + depr_group)
		end if 
		
		b_failed_company = true
		goto next_company	
	end if
	
// setup other fields	
	SetNull(company)
	Setnull(je_id)
	Setnull(je_code)
	select gl_company_no into :company from company where company_id = :i_nvo_cpr_control.i_company;
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("Error getting Company Number: " + sqlca.sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if IsNull(company) then
		f_pp_msgs("Company Number not found for ID: " + String(i_nvo_cpr_control.i_company))
		
		b_failed_company = true
		goto next_company
	end if		
	
	select je_id into :je_id from gl_je_control where process_id = 'Depreciation Expense';
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("Error getting Journal ID: " + sqlca.sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if IsNull(je_id) then
		f_pp_msgs("Journal ID not found for 'Depreciation Expense'")
		
		b_failed_company = true
		goto next_company
	end if		
	
	select gl_je_code into :je_code from standard_journal_entries where je_id = :je_id;
	if sqlca.sqlcode <> 0 then
		f_pp_msgs("Error getting GL Journal Code: " + sqlca.sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if IsNull(je_code) then
		f_pp_msgs("GL Journal Code not found for ID: " + String(je_id))
		
		b_failed_company = true
		goto next_company
	end if		
	
	select description  into :comp_descr from company where company_id = :i_nvo_cpr_control.i_company;

	f_pp_msgs("Depr Approval started for company " + comp_descr +  " at " + String(Today(), "hh:mm:ss"))
	
	pp_stat_id = f_pp_statistics_start("CPR Monthly Close", "Depr Approval: " + comp_descr)


	///###sjh 2/9/2006 new
	f_pp_msgs("Calling f_pre_approve_depr_co_custom.")
 	 ret = f_pre_approve_depr_co_custom(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month)
	 if Trim(ret) <> "" then
	  if ret = "next_company" then 
		f_pp_msgs("Error in f_pre_approve_depr_co_custom.")
		
		b_failed_company = true
		goto next_company
	  else
		f_pp_msgs("ERROR: Approval Aborted. See previous message(s).")
		
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
		
		if upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
			i_nvo_cpr_control.of_log_failed_companies("APPROVE DEPR")
		end if
		
		// Release the concurrency lock
		i_nvo_cpr_control.of_releaseProcess(process_msg)
		f_pp_msgs("Release Process Status: " + process_msg)
		
		return -1
	  end if
	  
	 end if 
	 
	 // Call Dynamic Validation option
	f_pp_msgs("Calling dynamic validation.")
	args[1] = string(i_nvo_cpr_control.i_month)
	args[2] = string(i_nvo_cpr_control.i_company)
	args[3] = 'f_pre_approve_depr_co_custom'
	
	rtn = f_wo_validation_control(1005,args)
	
	if rtn < 0 then
		f_pp_msgs("General Ledger Transactions Release (Dynamic Validations) Failed.")
		
		b_failed_company = true
		goto next_company
	end if
 
 	ret = f_approve_depr_co(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month)
	if Trim(ret) <> "" then
		if ret = "next_company" then 
			f_pp_msgs("Error in f_approve_depr_co.")
			
			b_failed_company = true
			goto next_company
		else
			f_pp_msgs("ERROR: Approval Aborted. See previous message(s).")
			
			num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
			i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
			
			if upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
				i_nvo_cpr_control.of_log_failed_companies("APPROVE DEPR")
			end if
			
			// Release the concurrency lock
			i_nvo_cpr_control.of_releaseProcess(process_msg)
			f_pp_msgs("Release Process Status: " + process_msg)
			
			return -1
		end if
	end if
	
	// Generate the General Ledger Transactions.
	

	f_pp_msgs("Generate the Depreciation GL Transactions..."+ ' ' + string(now()))	
	
	//	f_autogen_je_account is used to customize the contents 
	//	of gl_transaction.gl_account.
	//	in order to use the existing f_autogen_je_account
	//	we need to pass it a datastore.
	//	thus, in the base this dw (dw_depr_expense) needs
	//	to be a datastore
	uo_ds_top ds_depr_expense, ds_je_method_trans_type, ds_je_method_set_of_books, ds_depr_journal, ds_gl_transaction
	ds_depr_expense = CREATE uo_ds_top
	ds_depr_expense.DataObject = "dw_depr_expe_gl"
	ds_depr_expense.SetTransObject(sqlca)

	ds_je_method_trans_type = CREATE uo_ds_top
	ds_je_method_trans_type.DataObject = "dw_depr_je_method_trans_type"
	ds_je_method_trans_type.SetTransObject(sqlca)

	ds_je_method_set_of_books = CREATE uo_ds_top
	ds_je_method_set_of_books.DataObject = "dw_depr_je_method_set_of_books"
	ds_je_method_set_of_books.SetTransObject(sqlca)
	
	/*DJL - Maint 4940 - 02/2011 - Add option to create Depreciation Journals from CPR Depr by Asset.
	This datawindow retrieves depreciation expense at an asset level. If assets are group depr(subledger 0), then use an allocation to 
	gather depreciation expense.*/
	ds_depr_journal = CREATE uo_ds_top
	ds_depr_journal.DataObject = "dw_depr_expense_je_by_asset"
	ds_depr_journal.SetTransObject(sqlca)
	/**************************************************************************///End Maint 4940
	
	uo_autogen_je_account uo_autogen
	uo_autogen = create uo_autogen_je_account

	ds_gl_transaction = CREATE uo_ds_top
	ds_gl_transaction.dataobject = 'dw_gl_transaction' // it may have been changes by the customized f_release_je function
	ds_gl_transaction.settransobject(sqlca)
	
	month_string = String(i_nvo_cpr_control.i_month, "MM/YYYY")

	// Create depr. expense transactions
	// before we start let's check that depr exp cr & dr use same je methods so we don't create one-sided entry
	je_rows = ds_je_method_trans_type.Retrieve(10, i_nvo_cpr_control.i_company)
	for je = 1 to je_rows
		je_rows10[je] = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
	next
	je_rows = ds_je_method_trans_type.Retrieve(11, i_nvo_cpr_control.i_company)
	for je = 1 to je_rows
		je_rows11[je] = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
	next
	
	// check totals first
	if UpperBound(je_rows10) <> UpperBound(je_rows11) then
		f_pp_msgs("ERROR: Depr JE Methods do not match." )
		f_pp_msgs("Depr Expense Debits (10): " + String(UpperBound(je_rows10)) + " JE methods" )
		f_pp_msgs("Depr Expense Credits (11): " + String(UpperBound(je_rows11)) + " JE methods")
		
		b_failed_company = true
		goto next_company
	end if
	
	// same counts so verify same method ids
	for je = 1 to UpperBound(je_rows10)
		if je_rows10[je] <> je_rows11[je] then
			f_pp_msgs("ERROR: Depr JE Methods do not match." )
			f_pp_msgs("Depr Expense Debits (10) Method #" + String(je) + " is id=" + String(je_rows10[je]) )
			f_pp_msgs("Depr Expense Credits (11) Method #" + String(je) + " is id=" + String(je_rows11[je]))
			
			b_failed_company = true
			goto next_company
		else
			// make sure ALL books in the JE Method are valid for this company
			// this should not longer be possible because of the new je_method_company_view, but what the heck leave it here...
			select count(*), sum(nvl(include_indicator,0)), je.description
			into :book_count, :book_include, :je_descr
			from je_method_set_of_books j, company_set_of_books c, je_method je
			where j.set_of_books_id = c.set_of_books_id (+)
			and j.je_method_id = :je_rows10[je]
			and c.company_id = :i_nvo_cpr_control.i_company
			and j.je_method_id = je.je_method_id
			group by je.description
			;
			
			if book_count <> book_include then
				je_skip[je] = true
				f_pp_msgs("Warning: Depr JE Method Books do not match.  THIS JE METHOD WILL BE SKIPPED for Depreciation Expense." )
				f_pp_msgs("Method " + je_descr +" has " + String(book_count) + " but company uses " + String(book_include) )
			else
				je_skip[je] = false
			end if			
		
		end if
	next
	
	//get the je methods for depr expense debits
	je_rows = ds_je_method_trans_type.Retrieve(10, i_nvo_cpr_control.i_company)
	if je_rows < 0 then
		f_pp_msgs("ERROR retrieving JE Method-Trans Type data for (10) Depreciation Expense debit entries.~nSQL:" + ds_je_method_trans_type.i_sqlca_sqlerrtext)
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
	if je_rows = 0 then
		f_pp_msgs("ERROR No records round in JE Method-Trans Type data for (10) Depreciation Expense debit entries.")
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
		
	for je = 1 to je_rows
		if je_skip[je] then continue
		
		je_method = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
		amt_type =  ds_je_method_trans_type.GetItemNumber(je, "amount_type")
		
		// now get the set of books for the je method
		jebooks_rows = ds_je_method_set_of_books.Retrieve(je_method)
		if jebooks_rows < 0 then
			f_pp_msgs("Error retrieving JE Method-Set of Books data for (10) Depreciation Expense debit entries.~nSQL:" + ds_je_method_set_of_books.i_sqlca_sqlerrtext)
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
		if jebooks_rows = 0 then
			f_pp_msgs("No records round in JE Method-Set of Books data for (10) Depreciation Expense debit entries.")
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
			
		for b = 1 to jebooks_rows
			book = ds_je_method_set_of_books.GetItemNumber(b, "set_of_books_id")
			convention = ds_je_method_set_of_books.GetItemNumber(b, "reversal_convention")

			// create all the debits
			rows  = ds_depr_expense.retrieve(month_string, i_nvo_cpr_control.i_company, book)
			if rows < 0 then
				f_pp_msgs("Error retrieving Depreciation Expense entries.~nSQL:" + ds_depr_expense.i_sqlca_sqlerrtext)
				ds_gl_transaction.Reset()
				
				b_failed_company = true
				goto next_company
			end if
		
			for i = 1 to rows
		
				f_pp_msgs("Depr Expense Debit: JE Method " + String(je_method) + ", Book" + String(book) + ", Row " + String(i) + " of " + String(rows))
				je_by_asset = ds_depr_expense.getitemnumber(i, 'je_by_asset')
				// ### DJL - 05/2011 Maint 7665 - Properly gather Depr Group
				dg_id = ds_depr_expense.getitemnumber(i, 'depr_group_id')
			
				//DJL - Maint 4940 - 02/2011- No je by asset use original trans type
				if  je_by_asset = 0 then
					
					ds_gl_transaction.insertrow(1)
				
					gl_acct =  f_autogen_je_account(ds_depr_expense, i, 10, ds_depr_expense.getitemnumber(i,'debit_account_id'), je_method ) 
					if left(gl_acct, 5) = "ERROR" then
						f_pp_msgs("Error in f_autogen_je_account:  " + gl_acct)
						do_not_update = true
					end if
					
					trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
				
					ds_gl_transaction.setitem(1,'gl_trans_id',trans_id)
					ds_gl_transaction.setitem(1,'month',i_nvo_cpr_control.i_month)
					ds_gl_transaction.setitem(1,'company_number',company)
					ds_gl_transaction.setitem(1,'amount', & 
														ds_depr_expense.getitemdecimal(i,'depreciation_expense') * convention )
					ds_gl_transaction.setitem(1,'description', & 
														string(ds_depr_expense.getitemnumber(i,'depr_group_id')))
					ds_gl_transaction.setitem(1,'gl_account', gl_acct)
					ds_gl_transaction.setitem(1,'debit_credit_indicator',1)
					ds_gl_transaction.setitem(1,'gl_je_code',je_code)
					ds_gl_transaction.setitem(1,'gl_status_id',1)
					ds_gl_transaction.setitem(1, 'source', 'DEPR APPROVAL')
					ds_gl_transaction.setitem(1, 'amount_type', amt_type)
					ds_gl_transaction.setitem(1, 'je_method_id', je_method)
					ds_gl_transaction.setitem(1, 'trans_type', 10)
				/*DJL - Maint 4940 - 02/2011 - Add option to create Depreciation Journals from CPR Depr by Asset.
				  Detailed asset info, je by asset*/
				// ### DJL - Maint 8827 - 11.2011
				// Depr journals by asset speed fix
				elseif  je_by_asset = 1 then
					
					//Uses new trans type 39 - depr expense debit JE by asset
					lst_gl_trans_parms.a_month = i_nvo_cpr_control.i_month
					lst_gl_trans_parms.a_company = company
					lst_gl_trans_parms.a_description = string(ds_depr_expense.getitemnumber(i,'depr_group_id'))
				     lst_gl_trans_parms.a_debit_credit_indicator = 1
					lst_gl_trans_parms.a_gl_je_code = je_code
					lst_gl_trans_parms.a_gl_status_id = 1
					lst_gl_trans_parms.a_source = 'DEPR APPROVAL'
					lst_gl_trans_parms.a_amount_type = amt_type
					
					ll_month = long(String(i_nvo_cpr_control.i_month, "yyyymm"))
					
					ll_long_args[1] = ll_month
					ll_long_args[2] = i_nvo_cpr_control.i_company
					ll_long_args[3] = dg_id
					ll_long_args[4] = book
					
					//num_assets = ds_depr_journal.retrieve(month_string, i_company, book, dg_id)
					
					rtn = uo_autogen.uf_autogen_je_account(lst_gl_trans_parms, ll_long_args[], 39, je_method , i_nvo_cpr_control.i_company,  ds_depr_expense.getitemnumber(i,'debit_account_id'))
					if rtn  < 0 then
						f_pp_msgs("Error in uf_autogen_je_account depr expense credit by asset: " + gl_acct)
						do_not_update = true
					end if
					
				end if// end maint 4940 je by asset, end maint 8827 JE by asset speed fix
				
			next // transaction
		next  // SOB
	next // je_method
		
	//get the je methods for depr expense credits
	je_rows = ds_je_method_trans_type.Retrieve(11, i_nvo_cpr_control.i_company)
	if je_rows < 0 then
		f_pp_msgs("Error retrieving JE Method-Trans Type data for (11) Depreciation Expense credit entries.~nSQL:" + ds_je_method_trans_type.i_sqlca_sqlerrtext)
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
	if je_rows = 0 then
		f_pp_msgs("No records round in JE Method-Trans Type data for (11) Depreciation Expense credit entries.")
		ds_gl_transaction.Reset()
		
		b_failed_company = true
		goto next_company
	end if
		
	for je = 1 to je_rows
		if je_skip[je] then continue

		je_method = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
		amt_type =  ds_je_method_trans_type.GetItemNumber(je, "amount_type")
		
		// now get the set of books for the je method
		jebooks_rows = ds_je_method_set_of_books.Retrieve(je_method)
		if jebooks_rows < 0 then
			f_pp_msgs("Error retrieving JE Method-Set of Books data for (11) Depreciation Expense credit entries.~nSQL:" + ds_je_method_set_of_books.i_sqlca_sqlerrtext)
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
		if jebooks_rows = 0 then
			f_pp_msgs("No records round in JE Method-Set of Books data for (11) Depreciation Expense credit entries.")
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
			
		for b = 1 to jebooks_rows
			book = ds_je_method_set_of_books.GetItemNumber(b, "set_of_books_id")
			convention = ds_je_method_set_of_books.GetItemNumber(b, "reversal_convention")

			// create all the credits
			rows  = ds_depr_expense.retrieve(month_string, i_nvo_cpr_control.i_company, book)
			if rows < 0 then
				f_pp_msgs("Error retrieving Depreciation Expense entries.~nSQL:" + ds_depr_expense.i_sqlca_sqlerrtext)
				ds_gl_transaction.Reset()
				
				b_failed_company = true
				goto next_company
			end if
		
			for i = 1 to rows
		
				f_pp_msgs("Depr Expense Credit: JE Method " + String(je_method) + ", Book" + String(book) + ", Row " + String(i) + " of " + String(rows))
			
				depr_ind = ds_depr_expense.getitemnumber(i, 'depreciation_indicator')
				je_by_asset = ds_depr_expense.getitemnumber(i, 'je_by_asset')
				dg_id = ds_depr_expense.getitemnumber(i, 'depr_group_id')
				
				
				//DJL - Maint 4940 - 02/2011- No je by asset use original trans type
				if  je_by_asset = 0 then
					
					ds_gl_transaction.insertrow(1)
					gl_acct = f_autogen_je_account(ds_depr_expense, i, 11,  ds_depr_expense.getitemnumber(i,'credit_account_id'), je_method )
					
					if left(gl_acct, 5) = "ERROR" then
						f_pp_msgs("Error in f_autogen_je_account:  " + gl_acct)
						do_not_update = true
					end if
					
					trans_id = f_topkey_no_dw('gl_transaction','gl_trans_id')
				
					ds_gl_transaction.setitem(1,'gl_trans_id',trans_id)
					ds_gl_transaction.setitem(1,'month',i_nvo_cpr_control.i_month)
					ds_gl_transaction.setitem(1,'company_number',company)
					ds_gl_transaction.setitem(1,'amount', & 
														ds_depr_expense.getitemdecimal(i,'depreciation_expense') * convention)
					ds_gl_transaction.setitem(1,'description', & 
														string(ds_depr_expense.getitemnumber(i,'depr_group_id')))
					ds_gl_transaction.setitem(1,'gl_account', gl_acct)
					ds_gl_transaction.setitem(1,'debit_credit_indicator',0)
					ds_gl_transaction.setitem(1,'gl_je_code',je_code)
					ds_gl_transaction.setitem(1,'gl_status_id',1)
					ds_gl_transaction.setitem(1, 'source', 'DEPR APPROVAL')
					ds_gl_transaction.setitem(1, 'amount_type', amt_type)
					ds_gl_transaction.setitem(1, 'je_method_id', je_method)
					ds_gl_transaction.setitem(1, 'trans_type', 11)
					
				/*DJL - Maint 4940 - 02/2011 - Add option to create Depreciation Journals from CPR Depr by Asset.
				  Detailed asset info, je by asset*/
				// ### DJL - Maint 8827 - 11.2011
				// Depr journals by asset speed fix
				elseif  je_by_asset = 1 then
					
					//Uses new trans type 40 - depr expense credit JE by asset
					lst_gl_trans_parms.a_month = i_nvo_cpr_control.i_month
					lst_gl_trans_parms.a_company = company
					lst_gl_trans_parms.a_description = string(ds_depr_expense.getitemnumber(i,'depr_group_id'))
				     lst_gl_trans_parms.a_debit_credit_indicator = 0
					lst_gl_trans_parms.a_gl_je_code = je_code
					lst_gl_trans_parms.a_gl_status_id = 1
					lst_gl_trans_parms.a_source = 'DEPR APPROVAL'
					lst_gl_trans_parms.a_amount_type = amt_type
					
					ll_month = long(String(i_nvo_cpr_control.i_month, "yyyymm"))
					
					ll_long_args[1] = ll_month
					ll_long_args[2] = i_nvo_cpr_control.i_company
					ll_long_args[3] = dg_id
					ll_long_args[4] = book
					
					//num_assets = ds_depr_journal.retrieve(month_string, i_company, book, dg_id)
					
					rtn = uo_autogen.uf_autogen_je_account(lst_gl_trans_parms, ll_long_args[], 40, je_method , i_nvo_cpr_control.i_company, ds_depr_expense.getitemnumber(i,'credit_account_id'))
					if rtn  < 0 then
						f_pp_msgs("Error in uf_autogen_je_account depr expense debit by asset: " + gl_acct)
						do_not_update = true
					end if
					
				end if// end maint 4940 je by asset, end maint 8827 JE by asset speed fix
	
			next // transaction
		next  // SOB
	next // je_method

	if do_not_update then
		f_pp_msgs(" ")
		f_pp_msgs("* * * * Transactions NOT created due to errors! * * * *")
		f_pp_msgs(" ")
		
		b_failed_company = true
		goto next_company
	end if
	rtn = ds_gl_transaction.update()

	if rtn <> 1 then
	
		rollback using sqlca;
	
		f_pp_msgs("Error updating Expense General Ledger Transactions.")
		
		b_failed_company = true
		goto next_company
	end if
	// 
	// Generate the Recurring Reserve General Ledger Transactions.
	
	f_pp_msgs("Generate the Recurring Input Reserve GL Transactions..."+ ' ' + string(now()))	
	
	// create user-object
	uo_depr_activity_post uo_post
	uo_post = CREATE uo_depr_activity_post
	
	// check for recurring transactions already loaded
	// Maint 8604:  change ds_depr_activity to i_ds_depr_activity
	uo_post.i_ds_depr_activity = CREATE uo_ds_top
	uo_post.i_ds_depr_activity.dataobject = "dw_depr_recur2gl"
	uo_post.i_ds_depr_activity.SetTransObject(sqlca)

	//get the je methods for manual transactions
	je_rows = ds_je_method_trans_type.Retrieve(18, i_nvo_cpr_control.i_company)
	if je_rows < 0 then
		f_pp_msgs("ERROR retrieving JE Method-Trans Type data for Manual Reserve Transaction (18 & 19) entries.~nSQL:" + ds_je_method_trans_type.i_sqlca_sqlerrtext)
		
		b_failed_company = true
		goto next_company
	end if
	if je_rows = 0 then
		f_pp_msgs("ERROR No records round in JE Method-Trans Type data for Manual Reserve Transaction (18 & 19) entries.")
		
		b_failed_company = true
		goto next_company
	end if
		
	for je = 1 to je_rows
		je_method = ds_je_method_trans_type.GetItemNumber(je, "je_method_id")
		amt_type =  ds_je_method_trans_type.GetItemNumber(je, "amount_type")
		
		// make sure ALL books in the JE Method are valid for this company
		select count(*), sum(nvl(include_indicator,0)), je.description
		into :book_count, :book_include, :je_descr
		from je_method_set_of_books j, company_set_of_books c, je_method je
		where j.set_of_books_id = c.set_of_books_id (+)
		and j.je_method_id = :je_method
		and c.company_id = :i_nvo_cpr_control.i_company
		and j.je_method_id = je.je_method_id
		group by je.description
		; 
		
		if book_count <> book_include then
			f_pp_msgs("Warning: Depr JE Method Books do not match.  THIS JE METHOD WILL BE SKIPPED." )
			f_pp_msgs("Method " + je_descr +" has " + String(book_count) + " but company uses " + String(book_include) )
			continue
		end if			
		
		// now get the set of books for the je method
		jebooks_rows = ds_je_method_set_of_books.Retrieve(je_method)
		if jebooks_rows < 0 then
			f_pp_msgs("Error retrieving JE Method-Set of Books data for Manual Reserve Transaction (18 & 19) entries.~nSQL:" + ds_je_method_set_of_books.i_sqlca_sqlerrtext)
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
		if jebooks_rows = 0 then
			f_pp_msgs("No records round in JE Method-Set of Books data for Manual Reserve Transaction (18 & 19) entries.")
			ds_gl_transaction.Reset()
			
			b_failed_company = true
			goto next_company
		end if
			
		for b = 1 to jebooks_rows
			book = ds_je_method_set_of_books.GetItemNumber(b, "set_of_books_id")
			convention = ds_je_method_set_of_books.GetItemNumber(b, "reversal_convention")

			rows = uo_post.i_ds_depr_activity.Retrieve(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month, book)
			
			if rows < 0 then
				f_status_box("Error","Error retrieving Recurring Input Reserve entries.~nSQL:" + uo_post.i_ds_depr_activity.i_sqlca_sqlerrtext)
				if g_process_id > 0 then
					f_pp_msgs("Error retrieving Recurring Input Reserve entries.~nSQL:" + uo_post.i_ds_depr_activity.i_sqlca_sqlerrtext)
				end if
				rollback;
				
				b_failed_company = true
				goto next_company
			end if
		
		
			for i = 1 to rows
		
				f_pp_msgs("Recurring Input: " + String(i) + " of " + String(rows))
				
				rtn = uo_post.uf_gl_transaction_post(i, "DEPR RECUR", je_method)
				
				if rtn < 0 then
				
					rollback using sqlca;
				
					f_status_box("Information","Error updating Recurring Reserve General Ledger Transactions.")
					if g_process_id > 0 then
						f_pp_msgs("Error updating Recurring Reserve General Ledger Transactions.")
					end if
					
					b_failed_company = true
					goto next_company
				end if
			
			next
		next // JE book
	next // JE method

	f_status_box("Depr Processing", "     Processing Reg Entries..."+ ' ' + string(now()))
	if g_process_id > 0 then
		f_pp_msgs( "     Processing Reg Entries...")
	end if
	rtn = f_reg_entries_approve(i_nvo_cpr_control.i_company, i_nvo_cpr_control.i_month, 'DEPR')
	if rtn <> 1 or isnull(rtn) then
		f_status_box("ARO Processing", "Error During Approval of Reg Entries ARO's")
		if g_process_id > 0 then
			f_pp_msgs( "Error During Approval of Reg Entries ARO's")
		end if
		rollback;
		
		b_failed_company = true
		goto next_company
	end if
	
	f_status_box("Balance CPR", "Balancing CPR to Depreciation Ledger..."+ ' ' + string(now()))
	if g_process_id > 0 then
		f_pp_msgs("Balancing CPR to Depreciation Ledger..."+ ' ' + string(now()))
	end if
	
	// set the depr_approved date so the balancing routines will pick up
	// the ending balances for the current month. If there is an error
	// the depr_approved date will be reset to null
	i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'depr_approved',today()) 
	i_nvo_cpr_control.of_updatedw(false) //false = do not commit
	
 
	balance_depr = f_pp_system_control_company("Balance DEPR on Close", i_nvo_cpr_control.i_company)

	
	if  balance_depr = "" or balance_depr = "yes" then
		
		depr_balanced = this.of_balance_cpr_depr()
		
		//cb_depr_subl_balance.triggerevent(clicked!) don't do this until we fix the subledger logic
	
		if  depr_balanced = true then
		
			i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'depr_approved',today()) 
			i_nvo_cpr_control.of_updatedw()
			f_status_box("Information", "Finished Depreciation Approval."+ ' ' + string(now()))
			if g_process_id > 0 then
				f_pp_msgs( "Finished Depreciation Approval."+ ' ' + string(now()))
			end if
	
		else
			rollback; 
			i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'depr_approved',nullval) 
			i_nvo_cpr_control.of_updatedw()
		
			f_status_box("Error","Depreciation not Balanced to CPR. Notify PPC.")
			if g_process_id > 0 then
				f_pp_msgs("Depreciation not Balanced to CPR. Notify PPC.")
			end if
		end if
	else
		i_nvo_cpr_control.i_ds_cpr_control.setitem(1,'depr_approved',today()) 
		i_nvo_cpr_control.of_updatedw()
		f_status_box("Information", "Not Running the DEPR Balancing Routines this Month.")
		if g_process_id > 0 then
			f_pp_msgs("Not Running the DEPR Balancing Routines this Month.")
		end if
		f_status_box("Information", "Finished Depreciation Approval."+ ' ' + string(now()))
		if g_process_id > 0 then
			f_pp_msgs( "Finished Depreciation Approval."+ ' ' + string(now()))
		end if
	end if
	if pp_stat_id > 0 then f_pp_statistics_end(pp_stat_id)




	
next_company:

	if b_failed_company = true then
		num_failed_companies = upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) + 1
		i_nvo_cpr_control.i_array_position_of_failed_companies[num_failed_companies] = cc
	end if

	// if "goto next company" is called we should make sure anything done up to that point is undone
	// the wf_update_dw() function just above has the commit statement at which point this rollback will do NOTHING
	rollback;
	
	DESTROY ds_depr_expense
	DESTROY ds_je_method_set_of_books
	DESTROY ds_je_method_trans_type
	//DJL - Maint 8770 - destroy depr je by asset datastore
	DESTROY ds_depr_journal
	DESTROY uo_autogen
	
	if IsValid(uo_post) then DESTROY uo_post

	if not isnull(ds_gl_transaction) and isvalid(ds_gl_transaction) then 
		ds_gl_transaction.Reset()
	end if
	
	b_failed_company = false
next // company


//CBS - interface shell will take care of ending logs. 
//if g_process_id > 0 then
//	
// f_pp_msgs("Process ended on " + String(Today(), "mm/dd/yyyy") + " at " + String(Today(), "hh:mm:ss"))
// rtn = f_pp_msgs_end_log()
// g_sqlca_logs.uf_disconnect()
//
//end if


if i_nvo_cpr_control.of_checksystemcontrol( 5 ) = true then
	ds_users = create uo_ds_top
	vfy_users = false
	select nvl(user_sql, '') into :sqls from pp_verify where lower(description) = 'email cpr close: depreciation approval';
	if sqls <> "" then

		find_str = f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true)
		if f_create_dynamic_ds( ds_users, "grid", sqls, sqlca, true) = "OK" then
			if ds_users.RowCount() > 0 then
				vfy_users = true
			end if
		end if
	end if

	if vfy_users = true then
		for i=1 to ds_users.RowCount()
			user_id = ds_users.GetItemString( i, 1)
			f_send_mail( user_id, "Depreciation Approval completed", &
			"Depreciation Approval completed   on " + String(Today(), "mm/dd/yyyy") + &
			" at " + String(Today(), "hh:mm:ss"), user_id)
		next
	else
		f_send_mail( s_user_info.user_id, "Depreciation Approval completed", &
		"Depreciation Approval completed  on " + String(Today(), "mm/dd/yyyy") + &
		" at " + String(Today(), "hh:mm:ss"), s_user_info.user_id)
	end if
end if

if upperbound(i_nvo_cpr_control.i_array_position_of_failed_companies) > 0 then
	i_nvo_cpr_control.of_log_failed_companies("APPROVE DEPR")
	return_code = -1
else
	return_code = 1
end if

// Release the concurrency lock
i_nvo_cpr_control.of_releaseProcess(process_msg)
f_pp_msgs("Release Process Status: " + process_msg)


return return_code
end function

on nvo_depr_approval.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvo_depr_approval.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

