HA$PBExportHeader$w_datawindows.srw
forward
global type w_datawindows from window
end type
type dw_cr_element_definitions from datawindow within w_datawindows
end type
type dw_pp_interface_dates_check from datawindow within w_datawindows
end type
type dw_vint_net_salvage_update from datawindow within w_datawindows
end type
type dw_depr_recur2gl from datawindow within w_datawindows
end type
type dw_depr_je_method_trans_type from datawindow within w_datawindows
end type
type dw_depr_je_method_set_of_books from datawindow within w_datawindows
end type
type dw_depr_expe_gl from datawindow within w_datawindows
end type
type dw_depr_calc from datawindow within w_datawindows
end type
type dw_company_set_of_books from datawindow within w_datawindows
end type
type dw_subledger_name from datawindow within w_datawindows
end type
type dw_mortality_curve_points_ret from datawindow within w_datawindows
end type
type dw_interface_dates_all from datawindow within w_datawindows
end type
type dw_gl_transaction from datawindow within w_datawindows
end type
type dw_depr_ledger_all from datawindow within w_datawindows
end type
type dw_cpr_control_bal_quantity from datawindow within w_datawindows
end type
type dw_cpr_control from datawindow within w_datawindows
end type
type dw_balance_subl_depr_basis from datawindow within w_datawindows
end type
type dw_balance_depr_subledger from datawindow within w_datawindows
end type
type dw_balance_cpr_subledger from datawindow within w_datawindows
end type
type dw_balance_cpr_depr from datawindow within w_datawindows
end type
type dw_balance_account_summ from datawindow within w_datawindows
end type
end forward

global type w_datawindows from window
integer width = 4754
integer height = 1980
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_cr_element_definitions dw_cr_element_definitions
dw_pp_interface_dates_check dw_pp_interface_dates_check
dw_vint_net_salvage_update dw_vint_net_salvage_update
dw_depr_recur2gl dw_depr_recur2gl
dw_depr_je_method_trans_type dw_depr_je_method_trans_type
dw_depr_je_method_set_of_books dw_depr_je_method_set_of_books
dw_depr_expe_gl dw_depr_expe_gl
dw_depr_calc dw_depr_calc
dw_company_set_of_books dw_company_set_of_books
dw_subledger_name dw_subledger_name
dw_mortality_curve_points_ret dw_mortality_curve_points_ret
dw_interface_dates_all dw_interface_dates_all
dw_gl_transaction dw_gl_transaction
dw_depr_ledger_all dw_depr_ledger_all
dw_cpr_control_bal_quantity dw_cpr_control_bal_quantity
dw_cpr_control dw_cpr_control
dw_balance_subl_depr_basis dw_balance_subl_depr_basis
dw_balance_depr_subledger dw_balance_depr_subledger
dw_balance_cpr_subledger dw_balance_cpr_subledger
dw_balance_cpr_depr dw_balance_cpr_depr
dw_balance_account_summ dw_balance_account_summ
end type
global w_datawindows w_datawindows

on w_datawindows.create
this.dw_cr_element_definitions=create dw_cr_element_definitions
this.dw_pp_interface_dates_check=create dw_pp_interface_dates_check
this.dw_vint_net_salvage_update=create dw_vint_net_salvage_update
this.dw_depr_recur2gl=create dw_depr_recur2gl
this.dw_depr_je_method_trans_type=create dw_depr_je_method_trans_type
this.dw_depr_je_method_set_of_books=create dw_depr_je_method_set_of_books
this.dw_depr_expe_gl=create dw_depr_expe_gl
this.dw_depr_calc=create dw_depr_calc
this.dw_company_set_of_books=create dw_company_set_of_books
this.dw_subledger_name=create dw_subledger_name
this.dw_mortality_curve_points_ret=create dw_mortality_curve_points_ret
this.dw_interface_dates_all=create dw_interface_dates_all
this.dw_gl_transaction=create dw_gl_transaction
this.dw_depr_ledger_all=create dw_depr_ledger_all
this.dw_cpr_control_bal_quantity=create dw_cpr_control_bal_quantity
this.dw_cpr_control=create dw_cpr_control
this.dw_balance_subl_depr_basis=create dw_balance_subl_depr_basis
this.dw_balance_depr_subledger=create dw_balance_depr_subledger
this.dw_balance_cpr_subledger=create dw_balance_cpr_subledger
this.dw_balance_cpr_depr=create dw_balance_cpr_depr
this.dw_balance_account_summ=create dw_balance_account_summ
this.Control[]={this.dw_cr_element_definitions,&
this.dw_pp_interface_dates_check,&
this.dw_vint_net_salvage_update,&
this.dw_depr_recur2gl,&
this.dw_depr_je_method_trans_type,&
this.dw_depr_je_method_set_of_books,&
this.dw_depr_expe_gl,&
this.dw_depr_calc,&
this.dw_company_set_of_books,&
this.dw_subledger_name,&
this.dw_mortality_curve_points_ret,&
this.dw_interface_dates_all,&
this.dw_gl_transaction,&
this.dw_depr_ledger_all,&
this.dw_cpr_control_bal_quantity,&
this.dw_cpr_control,&
this.dw_balance_subl_depr_basis,&
this.dw_balance_depr_subledger,&
this.dw_balance_cpr_subledger,&
this.dw_balance_cpr_depr,&
this.dw_balance_account_summ}
end on

on w_datawindows.destroy
destroy(this.dw_cr_element_definitions)
destroy(this.dw_pp_interface_dates_check)
destroy(this.dw_vint_net_salvage_update)
destroy(this.dw_depr_recur2gl)
destroy(this.dw_depr_je_method_trans_type)
destroy(this.dw_depr_je_method_set_of_books)
destroy(this.dw_depr_expe_gl)
destroy(this.dw_depr_calc)
destroy(this.dw_company_set_of_books)
destroy(this.dw_subledger_name)
destroy(this.dw_mortality_curve_points_ret)
destroy(this.dw_interface_dates_all)
destroy(this.dw_gl_transaction)
destroy(this.dw_depr_ledger_all)
destroy(this.dw_cpr_control_bal_quantity)
destroy(this.dw_cpr_control)
destroy(this.dw_balance_subl_depr_basis)
destroy(this.dw_balance_depr_subledger)
destroy(this.dw_balance_cpr_subledger)
destroy(this.dw_balance_cpr_depr)
destroy(this.dw_balance_account_summ)
end on

type dw_cr_element_definitions from datawindow within w_datawindows
integer x = 2299
integer y = 356
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_cr_element_definitions"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_pp_interface_dates_check from datawindow within w_datawindows
integer x = 3054
integer y = 304
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_pp_interface_dates_check"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_vint_net_salvage_update from datawindow within w_datawindows
integer x = 1669
integer y = 1032
integer width = 686
integer height = 400
integer taborder = 60
string title = "none"
string dataobject = "dw_vint_net_salvage_update"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_recur2gl from datawindow within w_datawindows
integer x = 1829
integer y = 984
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_depr_recur2gl"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_je_method_trans_type from datawindow within w_datawindows
integer x = 1426
integer y = 932
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_depr_je_method_trans_type"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_je_method_set_of_books from datawindow within w_datawindows
integer x = 1655
integer y = 748
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_depr_je_method_set_of_books"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_expe_gl from datawindow within w_datawindows
integer x = 1509
integer y = 940
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_depr_expe_gl"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_calc from datawindow within w_datawindows
integer x = 329
integer y = 964
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_depr_calc"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_company_set_of_books from datawindow within w_datawindows
integer x = 311
integer y = 884
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_company_set_of_books"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_subledger_name from datawindow within w_datawindows
integer x = 1774
integer y = 932
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_subledger_name"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_mortality_curve_points_ret from datawindow within w_datawindows
integer x = 759
integer y = 1192
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_mortality_curve_points_ret"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_interface_dates_all from datawindow within w_datawindows
integer x = 1253
integer y = 1068
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_interface_dates_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_gl_transaction from datawindow within w_datawindows
integer x = 1157
integer y = 1108
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_gl_transaction"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_depr_ledger_all from datawindow within w_datawindows
integer x = 658
integer y = 1184
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
string dataobject = "dw_depr_ledger_all"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_control_bal_quantity from datawindow within w_datawindows
integer x = 471
integer y = 1108
integer width = 686
integer height = 400
integer taborder = 40
string title = "none"
string dataobject = "dw_cpr_control_bal_quantity"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_cpr_control from datawindow within w_datawindows
integer x = 562
integer y = 1252
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_cpr_control"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_subl_depr_basis from datawindow within w_datawindows
integer x = 873
integer y = 1076
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_balance_subl_depr_basis"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_depr_subledger from datawindow within w_datawindows
integer x = 2313
integer y = 1340
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "dw_balance_depr_subledger"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_cpr_subledger from datawindow within w_datawindows
integer x = 1888
integer y = 744
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_balance_cpr_subledger"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_cpr_depr from datawindow within w_datawindows
integer x = 969
integer y = 1192
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "dw_balance_cpr_depr"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_balance_account_summ from datawindow within w_datawindows
integer x = 443
integer y = 472
integer width = 686
integer height = 400
integer taborder = 10
string title = "none"
string dataobject = "dw_balance_account_summ"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

